package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.NumberField;
import com.mes.forms.RadioField;
import com.mes.forms.Selector;
import com.mes.support.StringUtilities;
import com.mes.tools.DropDownTable;
import com.mes.tools.EquipmentKey;

public final class ACRDataBean_EquipUpgrade_CBT_v2 extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_EquipUpgrade_CBT_v2.class);

  // constants

  private String[][] purchaseType =
    {
      {"","none"},
      {"Purchase New","Purchase New"},
      {"Purchase Refurbished","Purchase Refurbished"},
      {"Rental","Rental"}
    };

  private String[][] ampm =
    {
       {"AM","AM"}
      ,{"PM","PM"}
    };

  private String[][] purpose =
    {
      { "Upgrade" , "Upgrade" },
      { "Add" , "Add" },
      { "Loan" , "Loan" }
    };

  private String[][] chargeTo =
    {
      {"","none"},
      { "Merchant" , "Merchant" },
      { "Bank" , "Bank" }
    };

  private String[][] phoneTrain =
    {
      { "" , "No Training" },
      { "MES Help Desk" , "MES Help Desk" },
      { "Sales/Bank Rep" , "Sales/Bank Rep" }
    };

  private String[][] terminalType =
  {
    { "Dial" , "Dial" },
    { "IP" , "IP" }
  };


  /**
   * Factory for selector creation and the selector itself
   */
  private ACRSelectorFactory factory = new ACRSelectorFactory();
  private Selector equipSelector;


  // construction
  public ACRDataBean_EquipUpgrade_CBT_v2()
  {
  }

  public ACRDataBean_EquipUpgrade_CBT_v2(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  /**
   * Selector Generation
   * ST_EQUIPPU_CALLTAGS
   */
  public Selector getEquipmentSelector()
  {
    if (equipSelector == null)
    {
     equipSelector = factory.createSelector(factory.ST_EQUIPUPG,
        getMerchantNumber());

    }
    return equipSelector;
  }

  /**
   * getSelectedEquipment()
   * provides a list of selected equipment for order generation
   * during ACRPump call
   */
  public List getSelectedEquipment()
  {
    return getEquipmentSelector().getSelected();
  }

  protected void setDefaults()
  {
    //no op
  }

  public String renderHtml(String field)
  {
    if(!inSubmitMode() && field.equals("conversion"))
    {
      if(getACRValue(field).equals("Y"))
      {
        return "<font color = red>NOTE:</font> This ACR supports Trident Conversion";
      }
      else
      {
        return "";
      }
    }
    else
    {
      return super.renderHtml(field);
    }
  }

  protected void createExtendedFields()
  {
    if(acr==null)
      return;

    Field field = null;
    Field slave = null;

    int rq = getResidentQueue();

    if(rq==MesQueues.Q_CHANGE_REQUEST_CBT_TSYS) {
      field = new Field("exist_vnum","VNUMs Exist?", true);
      fields.add(field);
    }

    if(!editable)
      return;
    try
    {
      //add Address subbean
      add(getAddressHelper(true, true));

      // existing equipment info
      // (add equipment selector)
      add(getEquipmentSelector());
    }
    catch(Exception e)
    {
      log.debug("Unable to generate sub-bean components.");
    }

    field = new RadioField("purpose","Action To Take", purpose);
    fields.add(field);


    field = new CheckField("conversion","Supports Trident Conversion?","Y",false);
    fields.add(field);

    // new equipment info (nei_)
    field = new DropDownField("nei_1_Terminal Application","Terminal Application",TableFactory.getDropDownTable(TableFactory.TERM_APP),true);
    fields.add(field);

    field = new DropDownField("nei_2_Equipment","Equipment",new NewEquipmentDropDownTable(),true);
    fields.add(field);

    field = new RadioField("nei_3_Purchase Type","Purchase Type", purchaseType);
    fields.add(field);

    field = new Field("nei_4_Equipment Quantity","Equipment Quantity",true);
    fields.add(field);

    field = new NumberField("nei_5_Total Equipment Cost (tax not incl)","Total Equipment Cost (tax not incl)",10,10,true,2);
    fields.add(field);

    field = new DropDownField("nei_6_Split between Number of Months for Charges","Split between # of Months for Charges",new MonthBreakdownDropDownTable(),true);
    fields.add(field);

    field = new RadioField("nei_7_Charge To","Charge To", chargeTo);
    fields.add(field);

    // terminal features (tf)
    // rendered left hand side (l) || right hand side (r_)
    // grouping (a..)
    // order (1..)

    //Access Code
    field = new CheckField("tf_l_a0_Access Code?","Access Code","Y",false);
    fields.add(field);
    IfYesNotBlankValidation val = new IfYesNotBlankValidation(field, "You must provide an Access Code.");
    field = new Field("tf_l_a1_Access Code","Enter Access Code",true);
    fields.add(field);
    field.addValidation(val);

    //Batch
    field = new CheckField("tf_l_b0_Auto Batch Close?","Auto Batch Close","Y",false);
    fields.add(field);
    val = new IfYesNotBlankValidation(field, "You must provide a batch time.");
    field = new Field("tf_l_b1_Auto Batch Hours","Batch Hours",true);
    fields.add(field);
    field.addValidation(val);
    field = new Field("tf_l_b2_Auto Batch Minutes","Batch Minutes",true);
    fields.add(field);
    field.addValidation(val);
    field = new RadioField("tf_l_b3_Auto Batch AMPM","Batch Time",ampm,"");
    fields.add(field);
    field.addValidation(val);

    //Receipt Header 4
    field = new CheckField("tf_l_c0_Receipt Header Line 4?","Receipt Header Line 4","Y",false);
    fields.add(field);
    val = new IfYesNotBlankValidation(field, "You must provide a description.");
    field = new Field("tf_l_c1_Receipt Header Line 4 Description","Line 4 Description",true);
    fields.add(field);
    field.addValidation(val);

    //Receipt Header 5
    field = new CheckField("tf_l_d0_Receipt Header Line 5?","Receipt Header Line 5","Y",false);
    fields.add(field);
    val = new IfYesNotBlankValidation(field, "You must provide a description.");
    field = new Field("tf_l_d1_Receipt Header Line 5 Description","Line 5 Description",true);
    field.addValidation(val);
    fields.add(field);

    //Receipt Footer
    field = new CheckField("tf_l_e0_Receipt Footer?","Receipt Footer","Y",false);
    fields.add(field);
    val = new IfYesNotBlankValidation(field, "You must provide a description.");
    field = new Field("tf_l_e1_Receipt Footer Description","Footer Description",true);
    field.addValidation(val);
    fields.add(field);

    // phone training
    field = new CheckField("tf_l_f0_Phone Training?","Phone Training for Merchant","Y",false);
    fields.add(field);
    field = new RadioField("tf_l_f1_Phone Training Type","Phone Training for Merchant",phoneTrain,"");
    fields.add(field);

    //reset transaction
    field = new CheckField("tf_l_g0_Reset Transaction Number Daily?","Reset Transaction Number Daily","Y",false);
    fields.add(field);

    //invoice prompt
    field = new CheckField("tf_l_h0_Invoice Number Prompt On?","Invoice Number Prompt On","Y",false);
    fields.add(field);

    //fraud control
    field = new CheckField("tf_r_i0_Fraud Control On?","Fraud Control On (Required last 4 digits of card)","Y",false);
    fields.add(field);

    //password protect
    field = new CheckField("tf_r_j0_Password Protect On?","Password Protect On","Y",false);
    fields.add(field);
    field = new CheckField("tf_r_j1+_Password Protect Credit?","Password Protect Credit","Y",false);
    fields.add(field);
    field = new CheckField("tf_r_j2+_Password Protect Void?","Password Protect Void","Y",false);
    fields.add(field);
    field = new CheckField("tf_r_j3+_Password Protect Refund?","Password Protect Refund","Y",false);
    fields.add(field);
    field = new CheckField("tf_r_j4+_Password Protect Settle?","Password Protect Settle","Y",false);
    fields.add(field);

    //purchase card flag
    field = new CheckField("tf_r_k0_Purchase Card Flag On?","Purchase Card Flag On","Y",false);
    fields.add(field);

    //AVS
    field = new CheckField("tf_r_l0_AVS On?","AVS ON","Y",false);
    fields.add(field);

    //CS phone number
    field = new CheckField("tf_r_m0_Customer Service Phone Number?","Customer Service Phone Number","Y",false);
    fields.add(field);
    val =  new IfYesNotBlankValidation(field, "You must provide a phone number.");
    field = new Field("tf_r_m1_Customer Service Phone Number","CS Phone Number",true);
    field.addValidation(val);
    fields.add(field);


    //Terminal Reminder
    field = new CheckField("tf_r_n0_Terminal Reminder To Check Totals?","Terminal Reminder To Check Totals","Y",false);
    fields.add(field);
    val = new IfYesNotBlankValidation(field, "You must provide a reminder time.");
    field = new Field("tf_r_n1_Terminal Reminder Hour","Reminder Hour",true);
    field.addValidation(val);
    fields.add(field);
    field = new Field("tf_r_n2_Terminal Reminder Minute","Reminder Minute",true);
    field.addValidation(val);
    fields.add(field);
    field = new RadioField("tf_r_n3_Terminal Reminder AMPM","Reminder AMPM",ampm,"");
    field.addValidation(val);
    fields.add(field);

    //Tip Option
    field = new CheckField("tf_r_o0_Tip Option On?","Tip Option On","Y",false);
    fields.add(field);

    //Clerk Server prompt
    field = new CheckField("tf_r_p0_Clerk Server Prompt On?","Clerk Server Prompt On","Y",false);
    fields.add(field);

    // Terminal Type
    field = new RadioField("tf_r_q0_Terminal Type","Terminal Type", terminalType);
    fields.add(field);
  }

  public String renderEquipmentHTML()
  {
    //this is hack to reveal new equipment info... to be fixed?!
    //all naming is directly related to the bean and selector item/frame/factory
    boolean hasEquip = false;
    int LIMIT = 100;
    String item = "";
    StringBuffer sb = new StringBuffer();

    for (int i=0; i < LIMIT; i++)
    {
      item = getACRValue(CALLTAG_EQUIPMENT_NAME+"_"+i);
      if ( ! ( item.equals("") || item.equals("n") ) )
      {
        if(!hasEquip)
        {
          hasEquip = true;

          sb.append("<table cellpadding=\"1\" border=\"0\" align=\"center\" width=\"100%\" class=\"tableFillGray\" cellspacing=\"0\">");
          sb.append("<tr>");
          sb.append("<td>");
          sb.append("<table class=\"formTable\" width=\"100%\" border=\"0\" cellspacing=\"0\">");
          sb.append("  <tr>");
          sb.append("  <th colspan=7 class=\"tableColumnHead\">Equipment for Upgrade</th>");
          sb.append("  </tr>");
          sb.append("  <tr>");
          sb.append("   <td class=\"tableData\" align=\"center\">Serial Number</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Description</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Equipment Type</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">V Num</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Terminal App</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Vendor ID</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Call Tag</td>");
          //sb.append("   <td class=\"tableData\" align=\"center\">Repair Form</td>");
          sb.append(" </tr>");
          sb.append(" <tr>");
          sb.append("   <td colspan=7 height=\"1\" bgcolor=\"#cccccc\"><img src=\"/images/spcr_transp.gif\" height=\"1\"></td>");
          sb.append("  </tr>");
        }
        sb.append("<tr>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_serNum")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_description")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_equipType")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_VNum")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_termApp")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_vendorId")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(generateCallTagLink(getACRValue(item+"_callTag"))).append("</td>");
        //sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_repairForm")).append("</td>");
        sb.append("</tr>");
      }
    }

    //finish up
    if(hasEquip)
    {
      sb.append("</table></td></tr></table>");
    }
    else
    {
      sb.append("No equipment selected for this ACR.");
    }

    return sb.toString();
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}

//ASG Mgmt queue
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}

      //TSYS audit queue
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}

      //POS audit queue
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}


      //Help CBT mgmt
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}

      //ASG CBT mgmt
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}

    };
  }

  public String getHeadingName()
  {
    return "Order Additional or Upgraded Equipment for Merchant";
  }


  /*****************
  * DROPDOWNTABLES *
  ******************/
  public final class NewEquipmentDropDownTable extends DropDownTable
  {

    public NewEquipmentDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      addElement("","");

      String              qs      = null;
      PreparedStatement   ps      = null;
      ResultSet           rs      = null;

      try {

        connect();

        int appTypeCode;
        int cnt = 0;

        appTypeCode=StringUtilities.stringToInt(getACRValue("Type Code"),-1);

        // revert to mes equipment if merchant app type renders no allowed equip
        if(appTypeCode>-1)
        {
            qs =
            "select count(*) as count     "+
            "from equipment_application   "+
            "where app_type=?             ";
            ps = con.prepareStatement(qs);
            ps.setInt(1,appTypeCode);
            rs = ps.executeQuery();
            if(rs.next())
            {
              cnt = rs.getInt("count");
            }
        }
        else
        {
          cnt=0;
        }

        if(cnt<1) {
          log.info("Reverting to MES equipment: No equipment available for app type code '"+appTypeCode+"'.");
          appTypeCode=0;
        }

        qs =
          "select    equip_description         "+
          "from      equipment_application ea  "+
          "where     ea.app_type=?             "+
          "order by  equip_description         ";
        ps = con.prepareStatement(qs);
        ps.setInt(1,appTypeCode);
        rs = ps.executeQuery();

        String val,desc;

        while (rs.next()) {
          val = rs.getString("equip_description");
          desc = val;
          addElement(val,desc);
        }
      }
      catch (Exception e) {
        log.error("NewEquipmentDropDownTable.getData() EXCEPTION: '"+e.getMessage()+"'.");
      }
      finally {
        try { rs.close(); } catch (Exception e) {}
        try { ps.close(); } catch (Exception e) {}
        cleanUp();
      }

    }

  } // NewEquipmentDropDownTable class

  public class MonthBreakdownDropDownTable extends DropDownTable
  {
    public MonthBreakdownDropDownTable()
    {
      addElement("","");
      addElement("1","1");
      addElement("2","2");
      addElement("3","3");
      addElement("4","4");
    }

  } // MonthBreakdownDropDownTable class




  /**
   * getCallTaggedEquipment and calltaggedEquipmentExists
   * both necessary to generate call tags, but implemented
   * differently depending on the required status of said tags
   */
  public Vector getCallTaggedEquipment()
  {
    log.debug("START getCallTaggedEquipment()");

    if(acr==null)
    {
      return null;
    }

    ACRItem acrItem;
    Vector ctList = null;

    for(Enumeration myEnum = acr.getItemEnumeration(); myEnum.hasMoreElements();)
    {

      acrItem = (ACRItem)myEnum.nextElement();
      try
      {
        String s = acrItem.getName();

        //check to see if it's a call tag ref
        if( s.indexOf(SI_EquipUpg.CALLTAG_DESC) > -1 &&
            acrItem.getValue().equalsIgnoreCase("y")
          )
        {
          //it is, so strip it's prefix and get the appropriate
          //serial number for key generation
          String prefix = s.substring(0,s.indexOf(SI_EquipUpg.CALLTAG_DESC));
          log.debug("prefix = "+ prefix);
          String sn = acr.getACRValue(prefix + "_serNum");
          log.debug("CREATING KEY...."+sn);
          EquipmentKey key = new EquipmentKey(null,sn);
          if(ctList==null)
          {
            ctList = new Vector();
          }
          ctList.add(key);
        }

      }
      catch(Exception e)
      {
        log.error("getCallTaggedEquipment() EXCEPTION: "+e.toString()+".  Continuing.");
        continue;
      }
    }
    log.debug("END getCallTaggedEquipment()");
    return ctList;
  }

  /**
   * relies on naming convention found in SI_EquipUpg
   */
  public boolean calltaggedEquipmentExists()
  {
    try
    {

      ACRItem acrItem;
      for(Enumeration myEnum = getACR().getItemEnumeration(); myEnum.hasMoreElements();)
      {
        acrItem = (ACRItem)myEnum.nextElement();
        if( acrItem.getName().indexOf(SI_EquipUpg.CALLTAG_DESC) > -1 &&
            acrItem.getValue().equalsIgnoreCase("y")
          )
          return true;
      }
    }
    catch(Exception e)
    {
      log.error("calltaggedEquipmentExists() EXCEPTION: "+e.toString());
    }
    return false;
  }


} // class ACRDataBean_EquipUpgrade_CBT_v2
