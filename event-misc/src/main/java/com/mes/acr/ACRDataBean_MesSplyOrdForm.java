package com.mes.acr;

import java.util.Enumeration;
import java.util.NoSuchElementException;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;


public class ACRDataBean_MesSplyOrdForm extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_MesSplyOrdForm.class);

  // supplies FORMAT: { {seqNum},{partNum},{topGrpTitle},{groupTitle},{title},{unit},{equivalent} }
  private static final String[][] supplies = new String[][]
  {
     {"1","50111","VERIFONE ROLLS AND RIBBONS","VERIFONE P250 & P900 ROLLS & ROLL KITS","50 Rolls per Case","CS","Case"}
    ,{"2","50111-6R","VERIFONE ROLLS AND RIBBONS","VERIFONE P250 & P900 ROLLS & ROLL KITS","6 Rolls/1 P250 Ribbon","KIT","Kit"}
    ,{"3","50111-10R2","VERIFONE ROLLS AND RIBBONS","VERIFONE P250 & P900 ROLLS & ROLL KITS","10 Rolls/2 P250 Ribbons","KIT","Kit"}
    ,{"4","50111-20R3","VERIFONE ROLLS AND RIBBONS","VERIFONE P250 & P900 ROLLS & ROLL KITS","20 Rolls/3 P250 Ribbons","KIT","Kit"}
    ,{"5","250R1","VERIFONE ROLLS AND RIBBONS","VERIFONE P250 & P900 ROLLS & ROLL KITS","P250 Ribbon"," EA ","Each"}
    ,{"6","900R","VERIFONE ROLLS AND RIBBONS","VERIFONE P250 & P900 ROLLS & ROLL KITS","P900 Ribbon"," EA ","Each"}
    ,{"7","50211-6R","VERIFONE ROLLS AND RIBBONS","VERIFONE P900 ROLL KITS","6 Rolls/1 P900 Ribbon","KIT","Kit"}
    ,{"8","50211-10R","VERIFONE ROLLS AND RIBBONS","VERIFONE P900 ROLL KITS","10 Rolls/1 P900 Ribbon","KIT","Kit"}
    ,{"9","50211-20R2","VERIFONE ROLLS AND RIBBONS","VERIFONE P900 ROLL KITS","20 Rolls/2 P900 Ribbons","KIT","Kit"}
    ,{"10","50112","VERIFONE ROLLS AND RIBBONS","VERIFONE P250 & P900 RIBBONLESS ROLLS","50 Rolls per Case","CS","Case"}
    ,{"11","50112-6","VERIFONE ROLLS AND RIBBONS","VERIFONE P250 & P900 RIBBONLESS ROLLS","6 Rolls per Case","CS","Case"}
    ,{"12","50112-10","VERIFONE ROLLS AND RIBBONS","VERIFONE P250 & P900 RIBBONLESS ROLLS","10 Rolls per Case","CS","Case"}
    ,{"13","50112-20","VERIFONE ROLLS AND RIBBONS","VERIFONE P250 & P900 RIBBONLESS ROLLS","20 Rolls per Case","CS","Case"}
    ,{"14","50756","VERIFONE ROLLS AND RIBBONS","OMNI 3200/DASSAULT AT'S/NURITS/OMNI P350","50 Rolls per Case","CS","Case"}
    ,{"15","50756-6","VERIFONE ROLLS AND RIBBONS","OMNI 3200/DASSAULT AT'S/NURITS/OMNI P350","6 Rolls","CS","Case"}
    ,{"16","50756-10","VERIFONE ROLLS AND RIBBONS","OMNI 3200/DASSAULT AT'S/NURITS/OMNI P350","10 Rolls per Case","CS","Case"}
    ,{"17","50756-20","VERIFONE ROLLS AND RIBBONS","OMNI 3200/DASSAULT AT'S/NURITS/OMNI P350","20 Rolls per Case","CS","Case"}

    ,{"17.1","70025","VERIFONE ROLLS AND RIBBONS","Dassault Artema Paper","50 Rolls per Case","CS","Case"}
    ,{"17.2","70025-6","VERIFONE ROLLS AND RIBBONS","Dassault Artema Paper","6 Rolls per Case","CS","Case"}
    ,{"17.3","70025-10","VERIFONE ROLLS AND RIBBONS","Dassault Artema Paper","10 Rolls per Case","CS","Case"}
    ,{"17.4","70025-20","VERIFONE ROLLS AND RIBBONS","Dassault Artema Paper","20 Rolls per Case","CS","Case"}

    ,{"18","50547","VERIFONE ROLLS AND RIBBONS","TRANZ 460/420 ROLLS (WITH RIBBONS)","50 Rolls per Case","CS","Case"}
    ,{"19","50547-6R","VERIFONE ROLLS AND RIBBONS","TRANZ 460/420 ROLLS (WITH RIBBONS)","6 Rolls/1 Ribbon","KIT","Kit"}
    ,{"20","50547-10R","VERIFONE ROLLS AND RIBBONS","TRANZ 460/420 ROLLS (WITH RIBBONS)","10 Rolls/1 Ribbon","KIT","Kit"}
    ,{"21","50547-20R2","VERIFONE ROLLS AND RIBBONS","TRANZ 460/420 ROLLS (WITH RIBBONS)","20 Rolls/2 Ribbons","KIT","Kit"}
    ,{"22","100RB1","VERIFONE ROLLS AND RIBBONS","TRANZ 460/420 ROLLS (WITH RIBBONS)","Tranz 460/420 Ribbon"," EA ","Each"}
    ,{"23","50547A","VERIFONE ROLLS AND RIBBONS","TRANZ 460/420 RIBBONLESS ROLLS","50 Rolls per Case","CS","Case"}
    ,{"24","50547A-6","VERIFONE ROLLS AND RIBBONS","TRANZ 460/420 RIBBONLESS ROLLS","6 Rolls","CS","Case"}
    ,{"25","50547A-10","VERIFONE ROLLS AND RIBBONS","TRANZ 460/420 RIBBONLESS ROLLS","10 Rolls per Case","CS","Case"}
    ,{"26","50547A-20","VERIFONE ROLLS AND RIBBONS","TRANZ 460/420 RIBBONLESS ROLLS","20 Rolls per Case","CS","Case"}
    ,{"27","50053","VERIFONE ROLLS AND RIBBONS","TRANZ 460/420 WHITE ON WHITE IMPACT PAPER","50 Rolls per Case","CS","Case"}
    ,{"28","50053-6","VERIFONE ROLLS AND RIBBONS","TRANZ 460/420 WHITE ON WHITE IMPACT PAPER","6 Rolls","CS","Case"}
    ,{"29","50053-10","VERIFONE ROLLS AND RIBBONS","TRANZ 460/420 WHITE ON WHITE IMPACT PAPER","10 Rolls per Case","CS","Case"}
    ,{"30","50053-20","VERIFONE ROLLS AND RIBBONS","TRANZ 460/420 WHITE ON WHITE IMPACT PAPER","20 Rolls per Case","CS","Case"}
    ,{"31","50133","VERIFONE ROLLS AND RIBBONS","VERIFONE P250/P900 THREE PART ROLLS","50 Rolls per Case","CS","Case"}
    ,{"32","50133-6","VERIFONE ROLLS AND RIBBONS","VERIFONE P250/P900 THREE PART ROLLS","6 Rolls per Case","CS","Case"}
    ,{"33","50133-10","VERIFONE ROLLS AND RIBBONS","VERIFONE P250/P900 THREE PART ROLLS","10 Rolls per Case","CS","Case"}
    ,{"34","50133-20","VERIFONE ROLLS AND RIBBONS","VERIFONE P250/P900 THREE PART ROLLS","20 Rolls per Case","CS","Case"}
    ,{"35","50093","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P ROLLS (WITH RIBBONS)","50 Rolls per Case","CS","Case"}
    ,{"36","50093-6R","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P ROLLS (WITH RIBBONS)","6 Rolls/1 T7P Ribbon","KIT","Kit"}
    ,{"37","50093-10R2","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P ROLLS (WITH RIBBONS)","10 Rolls/2 T7P Ribbons","KIT","Kit"}
    ,{"38","50093-20R3","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P ROLLS (WITH RIBBONS)","20 Rolls/3 T7P Ribbons","KIT","Kit"}
    ,{"39","100RB1","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P ROLLS (WITH RIBBONS)","T7P Ribbon"," EA ","Each"}
    ,{"40","50113","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P RIBBONLESS ROLLS","50 Rolls per Case","CS","Case"}
    ,{"41","50113-6","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P RIBBONLESS ROLLS","6 Rolls per Case","CS","Case"}
    ,{"42","50113-10","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P RIBBONLESS ROLLS","10 Rolls per Case","CS","Case"}
    ,{"43","50113-20","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P RIBBONLESS ROLLS","20 Rolls per Case","CS","Case"}
    ,{"44","70015","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P THERMAL PRINTER ROLLS","75 Rolls per Case","CS","Case"}
    ,{"45","70015-6","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P THERMAL PRINTER ROLLS","6 Rolls per Case","CS","Case"}
    ,{"46","70015-10","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P THERMAL PRINTER ROLLS","10 Rolls per Case","CS","Case"}
    ,{"47","70015-20","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P THERMAL PRINTER ROLLS","20 Rolls per Case","CS","Case"}
    ,{"48","50729","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P THREE PART ROLLS","50 Rolls per Case","CS","Case"}
    ,{"49","50729-6","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P THREE PART ROLLS","6 Rolls per Case","CS","Case"}
    ,{"50","50729-10","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P THREE PART ROLLS","10 Rolls per Case","CS","Case"}
    ,{"51","50729-20","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T7P THREE PART ROLLS","20 Rolls per Case","CS","Case"}
    ,{"52","50111","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 ROLLS & RIBBONS","50 Rolls per Case","CS","Case"}
    ,{"53","50111-6","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 ROLLS & RIBBONS","6 Rolls/no Ribbons","CS","Case"}
    ,{"54","50111-10","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 ROLLS & RIBBONS","10 Rolls/no Ribbons","CS","Case"}
    ,{"55","50111-20","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 ROLLS & RIBBONS","20 Rolls/no Ribbons","CS","Case"}
    ,{"56","P8B","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 ROLLS & RIBBONS","T77 Ribbon"," EA ","Each"}
    ,{"57","50112","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 RIBBONLESS ROLLS","50 Rolls per Case","CS","Case"}
    ,{"58","50112-6","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 RIBBONLESS ROLLS","6 Rolls/no Ribbons","CS","Case"}
    ,{"59","50112-10","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 RIBBONLESS ROLLS","10 Rolls/no Ribbons","CS","Case"}
    ,{"60","50112-20","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 RIBBONLESS ROLLS","20 Rolls/no Ribbons","CS","Case"}
    ,{"61","50133","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 THREE PART ROLLS","50 Rolls per Case","CS","Case"}
    ,{"62","50133-6","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 THREE PART ROLLS","6 Rolls per Case","CS","Case"}
    ,{"63","50133-10","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 THREE PART ROLLS","10 Rolls per Case","CS","Case"}
    ,{"64","50133-20","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 THREE PART ROLLS","20 Rolls per Case","CS","Case"}
    ,{"65","70020","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 THERMAL PRINTER ROLLS","50 Rolls per Case","CS","Case"}
    ,{"66","70020-6","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 THERMAL PRINTER ROLLS","6 Rolls per Case","CS","Case"}
    ,{"67","70020-10","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 THERMAL PRINTER ROLLS","10 Rolls per Case","CS","Case"}
    ,{"68","70020-20","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM T77 THERMAL PRINTER ROLLS","20 Rolls per Case","CS","Case"}
    ,{"69","501R1 ","HYPERCOM PRINTER ROLLS AND RIBBONS","T1E/T7E PRINTER RIBBON","T7E Printer Ribbon","EA","Each"}
    ,{"70","70021","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM ICE 5000 THERMAL PRINTER ROLLS","100 Rolls per Case","CS","Case"}
    ,{"71","70021-20","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM ICE 5000 THERMAL PRINTER ROLLS","20 Rolls per Case","CS","Case"}
    ,{"72","70021-10","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM ICE 5000 THERMAL PRINTER ROLLS (Cont)","10 Rolls per Case","CS","Case"}
    ,{"73","601 RB","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM P7 RIBBON","P7 Ribbon","EA","Each"}
    ,{"74","5.5531CF","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM P7 PAPER","3 Pt. Sprocket Form","PKG","100"}
    ,{"75","5.5531CF","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM P7 PAPER","3 Pt. Sprocket Form","M","1,000"}
    ,{"76","5.5531CF","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM P7 PAPER","3 Pt. Sprocket Form","CS","3,600"}
    ,{"77","50561CF","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM P7 PAPER","2 Pt. Visa Fine Dining Form","PKG","100"}
    ,{"78","50561CF","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM P7 PAPER","2 Pt. Visa Fine Dining Form","M","1,000"}
    ,{"79","50561CF","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM P7 PAPER","2 Pt. Visa Fine Dining Form","CS","4,800"}
    ,{"80","5.5521CF","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM P7 PAPER","2 Pt. Visa Ret/Rest Form","PKG","100"}
    ,{"81","5.5521CF","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM P7 PAPER","2 part sprocket paper","M","1,000"}
    ,{"82","5.5521CF","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM P7 PAPER","2 part sprocket paper","CS","4,800"}
    ,{"83","50695CF","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM P7 PAPER","2 Pt. Visa Fine Dine/Hotel","PKG","100"}
    ,{"84","50695CF","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM P7 PAPER","2 Pt. Visa Fine Dine/Hotel","M","1,000"}
    ,{"85","50695CF","HYPERCOM PRINTER ROLLS AND RIBBONS","HYPERCOM P7 PAPER","2 Pt. Visa Fine Dine/Hotel","CS","48,000"}
    ,{"86","50043","OTHER ROLLS & RIBBONS","CITIZEN 200/IDP 562/IDP 560 Rolls and Roll Kits","50 Rolls per Case","CS","Case"}
    ,{"87","50043-6R","OTHER ROLLS & RIBBONS","CITIZEN 200/IDP 562/IDP 560 Rolls and Roll Kits","6 Rolls/1 P200 Ribbon","KIT","Kit"}
    ,{"88","50043-10R2","OTHER ROLLS & RIBBONS","CITIZEN 200/IDP 562/IDP 560 Rolls and Roll Kits","10 Rolls/2 P200 Ribbons","KIT","Kit"}
    ,{"89","50043-20R3","OTHER ROLLS & RIBBONS","CITIZEN 200/IDP 562/IDP 560 Rolls and Roll Kits","20 Rolls/3 P200 Ribbons","KIT","Kit"}
    ,{"90","200R1","OTHER ROLLS & RIBBONS","CITIZEN 200/IDP 562/IDP 560 Rolls and Roll Kits","P200 Ribbon"," EA ","Each"}
    ,{"91","562R","OTHER ROLLS & RIBBONS","CITIZEN 200/IDP 562/IDP 560 Rolls and Roll Kits","P562 Ribbon"," EA ","Each"}
    ,{"92","70022","OTHER ROLLS & RIBBONS","IVI SCRIBE 612 PRINTER ROLLS","50 Rolls per Case","CS","Case"}
    ,{"93","70022-6","OTHER ROLLS & RIBBONS","IVI SCRIBE 612 PRINTER ROLLS","6 Rolls per Case","CS","Case"}
    ,{"94","70022-10","OTHER ROLLS & RIBBONS","IVI SCRIBE 612 PRINTER ROLLS","10 Rolls per Case","CS","Case"}
    ,{"95","70022-20","OTHER ROLLS & RIBBONS","IVI SCRIBE 612 PRINTER ROLLS","20 Rolls per Case","CS","Case"}
    ,{"96","50731","OTHER ROLLS & RIBBONS","IVI SILENT PARTNER PRINTER ROLLS","48 Rolls per Case","CS","Case"}
    ,{"97","50731-10","OTHER ROLLS & RIBBONS","IVI SILENT PARTNER PRINTER ROLLS","10 Rolls per Case","CS","Case"}
    ,{"98","50731-20","OTHER ROLLS & RIBBONS","IVI SILENT PARTNER PRINTER ROLLS","20 Rolls per Case","CS","Case"}
    ,{"99","50812","OTHER ROLLS & RIBBONS","IVI JIGSAW PRINTER","50 Rolls per Case","CS","Case"}
    ,{"100","50812-10","OTHER ROLLS & RIBBONS","IVI JIGSAW PRINTER","10 Rolls per Case","CS","Case"}
    ,{"101","50812-20","OTHER ROLLS & RIBBONS","IVI JIGSAW PRINTER","20 Rolls per Case","CS","Case"}
    ,{"102","50379","OTHER ROLLS & RIBBONS","PEN PLUS THERMAL PRINTER ROLLS","50 Rolls per Case","CS","Case"}
    ,{"103","50379-6","OTHER ROLLS & RIBBONS","PEN PLUS THERMAL PRINTER ROLLS","6 Rolls per Case","CS","Case"}
    ,{"104","50379-10","OTHER ROLLS & RIBBONS","PEN PLUS THERMAL PRINTER ROLLS","10 Rolls per Case","CS","Case"}
    ,{"105","50379-20","OTHER ROLLS & RIBBONS","PEN PLUS THERMAL PRINTER ROLLS","20 Rolls per Case","CS","Case"}
    ,{"106","8523","OTHER ROLLS & RIBBONS","FORMS","2 Pt. Carbonless Sales","PKG","100"}
    ,{"107","8523","OTHER ROLLS & RIBBONS","FORMS","2 Pt. Carbonless Sales","M","1,000"}
    ,{"108","8523","OTHER ROLLS & RIBBONS","FORMS","2 Pt. Carbonless Sales","CS","6,000"}
    ,{"109","8623","OTHER ROLLS & RIBBONS","FORMS","2 Pt. Carbonless Credits","PKG","100"}
    ,{"110","8623","OTHER ROLLS & RIBBONS","FORMS","2 Pt. Carbonless Credits","M","1,000"}
    ,{"111","8623","OTHER ROLLS & RIBBONS","FORMS","2 Pt. Carbonless Credits","CS","6,000"}
    ,{"112","5521","OTHER ROLLS & RIBBONS","FORMS","2 Pt. Carbonless Sales (Short)","PKG","100"}
    ,{"113","5521","OTHER ROLLS & RIBBONS","FORMS","2 Pt. Carbonless Sales (Short)","M","1,000"}
    ,{"114","5521","OTHER ROLLS & RIBBONS","FORMS","2 Pt. Carbonless Sales (Short)","CS","8,800"}
    ,{"115","55311","OTHER ROLLS & RIBBONS","FORMS","3 Pt. Carbonless Sales (Short)","PKG","100"}
    ,{"116","55311","OTHER ROLLS & RIBBONS","FORMS","3 Pt. Carbonless Sales (Short)","M","1,000"}
    ,{"117","55311","OTHER ROLLS & RIBBONS","FORMS","3 Pt. Carbonless Sales (Short)","CS","7,800"}
    ,{"118","85310","OTHER ROLLS & RIBBONS","FORMS","3 Pt. Carbonless Sales","PKG","100"}
    ,{"119","85310","OTHER ROLLS & RIBBONS","FORMS","3 Pt. Carbonless Sales","M","1,000"}
    ,{"120","85310","OTHER ROLLS & RIBBONS","FORMS","3 Pt. Carbonless Sales","CS","4,000"}
    ,{"121","86310","OTHER ROLLS & RIBBONS","FORMS","3 Pt. Carbonless Credits","PKG","100"}
    ,{"122","85310","OTHER ROLLS & RIBBONS","FORMS","3 Pt. Carbonless Credits","M","1,000"}
    ,{"123","85310","OTHER ROLLS & RIBBONS","FORMS","3 Pt. Carbonless Credits","CS","4,000"}
    ,{"124","872","OTHER ROLLS & RIBBONS","FORMS","Cash Advance 2 ply","PKG","100"}
    ,{"125","872","OTHER ROLLS & RIBBONS","FORMS","Cash Advance 2 ply","FLAT","1000"}
    ,{"126","872","OTHER ROLLS & RIBBONS","FORMS","Cash Advance 2 ply","CASE","6000"}
    ,{"127","878","OTHER ROLLS & RIBBONS","FORMS","Cash Advance 3 ply","PKG","100"}
    ,{"128","878","OTHER ROLLS & RIBBONS","FORMS","Cash Advance 3 ply","FLAT","1000"}
    ,{"129","878","OTHER ROLLS & RIBBONS","FORMS","Cash Advance 3 ply","CASE","6000"}

    ,{"130","P060000"   ,"OTHER ROLLS & RIBBONS","ROLLS","$52 per Case (48 Rolls)","CASE","Case"}
    ,{"131","P060000-10","OTHER ROLLS & RIBBONS","ROLLS","$12.33 per Case"        ,"CASE","Case"}
    ,{"132","P060000-20","OTHER ROLLS & RIBBONS","ROLLS","$23.17 per Case"        ,"CASE","Case"}
  };

  public SupplyDescriptor getSupplyDescriptor(String seqNum)
  {
    if(seqNum==null || seqNum.length()<1)
      return null;

    for(int i=0;i<supplies.length;i++) {
      if(supplies[i][0].equals(seqNum)) {
        SupplyDescriptor sd = new SupplyDescriptor();
        sd.seqNum=supplies[i][0];
        sd.partNum=supplies[i][1];
        sd.topGrpTitle=supplies[i][2];
        sd.groupTitle=supplies[i][3];
        sd.title=supplies[i][4];
        sd.unit=supplies[i][5];
        sd.equivalent=supplies[i][6];
        return sd;
      }
    }

    return null;
  }

  /**
   * class SupplyDescriptor
   *
   * Represents descriptive attributes associated with a single supply item.
   */
  public class SupplyDescriptor
  {
    // data members
    public String    seqNum;
    public String    topGrpTitle;
    public String    groupTitle;
    public String    partNum;
    public String    title;
    public String    unit;
    public String    equivalent;

    // construction
    public SupplyDescriptor()
    {
      seqNum=null;
      topGrpTitle=null;
      groupTitle=null;
      partNum=null;
      title=null;
      unit=null;
      equivalent=null;
    }

  } // class SupplyDescriptors

  public Enumeration getSupplyDescriptorEnumeration()
  {
    return new SupplyDescriptorEnumeration();
  }

  /**
   * class SupplyDescriptorEnumeration
   */
  private class SupplyDescriptorEnumeration implements Enumeration
  {
    int curRow = 0;
    private SupplyDescriptor sd = new SupplyDescriptor();

    public boolean hasMoreElements()
    {
      return (curRow < supplies.length);
    }

    public Object nextElement()
    {
      if(curRow >= supplies.length)
        throw new NoSuchElementException("SupplyDescriptorEnumeration");

      sd.seqNum=supplies[curRow][0];
      sd.partNum=supplies[curRow][1];
      sd.topGrpTitle=supplies[curRow][2];
      sd.groupTitle=supplies[curRow][3];
      sd.title=supplies[curRow][4];
      sd.unit=supplies[curRow][5];
      sd.equivalent=supplies[curRow][6];

      curRow++;

      return sd;
    }

  } // class SupplyDescriptorEnumeration

  // data members
  ACRDataBeanAddressHelper adrsHelper = null;

  // construction
  public ACRDataBean_MesSplyOrdForm()
  {
  }

  public ACRDataBean_MesSplyOrdForm(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  private ACRDataBeanAddressHelper _getAddressHelper()
  {
    if(adrsHelper!=null)
      return adrsHelper; // already configured by virtue of non-null obj ref

    adrsHelper = new ACRDataBeanAddressHelper(this);

    // configure the address helper
    adrsHelper.setAdrsTypeName("Ship To");
    adrsHelper.setAdrsSlctnFldName("staddress");
    //adrsHelper.setAdrsName("");
    adrsHelper.setAdrsAddressFldNme("address");
    adrsHelper.setAdrsAttnFldNme("attn");
    adrsHelper.setAdrsBankNameFldNme("bnknme");
    adrsHelper.setAdrsCityFldNme("city");
    adrsHelper.setAdrsStateFldNme("state");
    adrsHelper.setAdrsZipFldNme("zip");
    //adrsHelper.setAdrsCountry("sh_country");
    adrsHelper.setIsRequired(true);

    return adrsHelper;
  }

  public String renderAddressHtml()
  {
    return _getAddressHelper().renderAddressHtml();
  }

  public void doPull()
  {
    super.doPull();

    if(!editable)
      return;

    _getAddressHelper().pullAddressFields();
  }

  protected void setDefaults()
  {
    // parent related info
    if(getMerchantNumber().length()>0) {

      // merchant address
      _getAddressHelper().setACRAdrsInfo();

    }
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    //ACRDefinition acrd=acr.getDefinition();
    //ACRDefItem acrdi=null;
    SupplyDescriptor sd;
    Field      field=null;
    String     qtyName;

    // address fields
    _getAddressHelper().createAddressFields();

    Vector qty = new Vector(0,20);

    // qty_ related fields
    //for(Enumeration e=acrd.getItemEnumeration();e.hasMoreElements();) {
    for(Enumeration e=getSupplyDescriptorEnumeration();e.hasMoreElements();) {
      //acrdi=(ACRDefItem)e.nextElement();
      sd=(SupplyDescriptor)e.nextElement();

      //if(!acrdi.getName().startsWith("qty_"))
        //continue;

      qtyName = "qty_"+sd.seqNum;

      //field = ACRDefItemToField(acrdi);
      field = new Field(qtyName,4,3,true);
      fields.add(field);

      qty.addElement(field);
    }

    // add "at least one" validation
    fields.addValidation(new FieldBean.AtLeastOneValidation("At least one item quantity must be specified to proceed.",qty));

    return;
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_SUPPLIES}

      ,{MesQueues.Q_CHANGE_REQUEST_SUPPLIES,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_SUPPLIES,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_SUPPLIES,MesQueues.Q_CHANGE_REQUEST_COMPLETED}

      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_SUPPLIES}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
    };
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  public String getHeadingName()
  {
    return "Order Supplies for Merchant";
  }

  /*
  public String renderStatusOrderSuppliesTable()
  {
    StringBuffer sb = new StringBuffer();

    sb.append("<table>\n");

    //ACRDefinition acrd=acr.getDefinition();
    //ACRDefItem acrdi=null;
    SupplyDescriptor sd;
    String nme,val;

    // qty_ related fields
    //for(Enumeration e=acrd.getItemEnumeration();e.hasMoreElements();) {
    for(Enumeration e=getSupplyDescriptorEnumeration();e.hasMoreElements()) {
      //acrdi=(ACRDefItem)e.nextElement();
      sd=(SupplyDescriptor)e.nextElement();

      //val=acr.getACRValue(acrdi.getName());
      val=acr.getACRValue(sd.seqNum);
      if(val.length()<1)
        continue;

      nme=sd.seqNum +", "+sd.partNum+", "+sd.

      sb.append("<tr><td class=\"formFields\">\n");
      sb.append(sd.);
      sb.append(" Qty: ");
      sb.append(val);
      sb.append("</td></tr>\n");

    }

    sb.append("</table>\n");

    return sb.toString();
  }
  */

} // class ACRDataBean_MesSplyOrdForm
