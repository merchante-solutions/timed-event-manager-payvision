/*@lineinfo:filename=ACRMgmtQInfo*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  ACRMgmtQInfo

  Designed to hold ACR Mgmt Queue info and functionality -

  Copyright (C) 2000-2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.acr;

import java.sql.ResultSet;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class ACRMgmtQInfo extends SQLJConnectionBase
{

  /**
   * Log4J Logger
   */
  static Logger log = Logger.getLogger(ACRMgmtQInfo.class);

  private int     id;
  private String  description;
  //needed for email notification in ACRPump
  private int     emailRef;
  //needed to determine queue Ops
  private int     bankNum;
  private boolean mgmtQActive = false;

  //empty constructor solely for the purpose
  //of handling isMgmtQ
  public ACRMgmtQInfo()
  {
    id = -1;
    emailRef = -1;
    bankNum = -1;
    description = "";
  }

  public ACRMgmtQInfo(String merchNum)
  {
    description = "";
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:65^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   q.mgmt_q             Q,
//                   nvl(q.email_ref,-1)  ref,
//                   q.active             is_on,
//                   m.bank_number        bankNum
//          from     acr_mgmt_q         q,
//                   mif                m,
//                   t_hierarchy        h
//          where    m.merchant_number = :merchNum
//                   and m.association_node = h.descendent
//                   and h.ancestor = q.association_node
//          order by h.relation asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   q.mgmt_q             Q,\n                 nvl(q.email_ref,-1)  ref,\n                 q.active             is_on,\n                 m.bank_number        bankNum\n        from     acr_mgmt_q         q,\n                 mif                m,\n                 t_hierarchy        h\n        where    m.merchant_number =  :1 \n                 and m.association_node = h.descendent\n                 and h.ancestor = q.association_node\n        order by h.relation asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRMgmtQInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.acr.ACRMgmtQInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:78^7*/

      rs = it.getResultSet();

      // first item in result set is the q type info
      if(rs.next())
      {
        id       = rs.getInt("Q");
        emailRef = rs.getInt("ref");
        mgmtQActive  = rs.getInt("is_on") == 1? true: false;
        bankNum  = rs.getInt("bankNum");
      }
      else
      {
        id = -1;
        emailRef = -1;
      }

      rs.close();
      it.close();

      description = MesQueues.getACRQueueDescriptor(id);
    }
    catch(Exception e)
    {
      logEntry("ACRMgmtQInfo(" + merchNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

  }

  public int getId()
  {
    return id;
  }

  public String getDescription()
  {
    return description;
  }

  public int getBankNum()
  {
    return bankNum;
  }

  public boolean isCurrentQueue(int compQ)
  {
    if (id == compQ)
    {
      return true;
    }
    return false;
  }

  public boolean isMgmtQOn()
  {
    return mgmtQActive;
  }

  public int getSrcQ(long acrID)
  {
    int srcQ = -1;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:153^7*/

//  ************************************************************
//  #sql [Ctx] it = { select old_type
//          from q_log
//          where id = :acrID
//          and new_type = :id
//          and user_name != 'q_data_aiufer'
//          order by log_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select old_type\n        from q_log\n        where id =  :1 \n        and new_type =  :2 \n        and user_name != 'q_data_aiufer'\n        order by log_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.acr.ACRMgmtQInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,acrID);
   __sJT_st.setInt(2,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.acr.ACRMgmtQInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:161^7*/

      rs = it.getResultSet();

      // first item in result set is the app type
      if(rs.next())
      {
        srcQ = rs.getInt("old_type");
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getSrcQ(" + acrID + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return srcQ;

  }

  public int[][] getMgmtQOps(long acrID, int initQ)
  {

    int _q = getSrcQ(acrID);

    if(_q < 0)
    {
       _q = initQ;
    }

    if(isMgmtQOn())
    {
      return getMgmtQOps(_q);
    }
    else
    {
      return ACRQueueOps.getQueueOps(_q, this);
    }
  }

  public int[][] getMgmtQOps(int initQ)
  {
    int q1 = MesQueues.Q_CHANGE_REQUEST_RESEARCH;
    int q2 = MesQueues.Q_CHANGE_REQUEST_CANCELLED;

    if(bankNum == mesConstants.BANK_ID_BBT)
    {
        q1 = MesQueues.Q_CHANGE_REQUEST_BBT_PENDING;
        q2 = MesQueues.Q_CHANGE_REQUEST_BBT_CANCELLED;
    }

    //ensure that the initial Q wasn't set to the Mgmt Q
    if(id==initQ)
    {
      initQ = q1;
    }

    return new int[][]
    {
      {MesQueues.Q_NONE, id}
      ,{id,initQ}
      ,{id,q2}
    };
  }


  public int getEmailRef()
  {
    return emailRef;
  }

  //this is lame, I know...
  public static boolean isMgmtQueue(int srcQ)
  {
    ACRMgmtQInfo stump = new ACRMgmtQInfo();
    return stump._isMgmtQueue(srcQ);
  }

  public boolean _isMgmtQueue(int srcQ)
  {

    boolean isMgmtQ = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:258^7*/

//  ************************************************************
//  #sql [Ctx] it = { select type
//          from q_types
//          where item_type = 10
//          and status = 'X'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select type\n        from q_types\n        where item_type = 10\n        and status = 'X'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.acr.ACRMgmtQInfo",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.acr.ACRMgmtQInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:264^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        if (srcQ == rs.getInt("type"))
        {
          isMgmtQ = true;
          break;
        }
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getSrcQ(" + srcQ + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return isMgmtQ;
  }
}/*@lineinfo:generated-code*/