package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.Validation;
import com.mes.forms.YesNoRadioField;

public final class ACRDataBean_MiscAcctChng_CBT extends ACRDataBean_MiscAcctChng_Base
{
  static Logger log = Logger.getLogger(ACRDataBean_MiscAcctChng_CBT.class);

  public ACRDataBean_MiscAcctChng_CBT()
  {
  }

  public ACRDataBean_MiscAcctChng_CBT(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  /**
   * Set any default values.
   */
  protected void setDefaults()
  {
    if(editable)
    {

      ResultSet rs          = null;
      PreparedStatement ps  = null;

      try
      {
        connect();

        String a = null;

        String qs = "select                                       "
                  + "nvl(to_char(m.asso_number), 'n/a') as a      "
                  + "from merchant m, mif mif                     "
                  + "where mif.MERCHANT_NUMBER = ?                "
                  + "and mif.MERCHANT_NUMBER = m.merch_number(+)  ";

        ps = con.prepareStatement(qs);
        ps.setString(1,getACR().getMerchantNum());

        rs = ps.executeQuery();
        if (rs.next())
        {
          a = rs.getString("a");
        }

        Field field1 = new HiddenField("o_mAssoc",a);

        fields.add(field1);

        log.debug("setting old values... DONE");

      }
      catch (Exception e)
      {
        log.error("setDefaults() exception: " + e);
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { ps.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }

  /**
   * Generate fields specific to this ACR.
   */
  protected void createExtendedFields()
  {
    if(!editable || acr == null)
    {
      return;
    }

    Field field;
    Validation val;

    //VAR
    field = new CheckboxField("var","Send VAR",false);
    val = new RequiredIfYesValidation(field, "Required for VAR form transmission");
    fields.add(field);

    field = new EmailField("varMail","VAR Email", true);
    field.addValidation(val);
    fields.add(field);

    //VERISIGN
    field = new CheckboxField("verisign","Send Verisign Welcome",false);
    val = new RequiredIfYesValidation(field, "Required for Verisign transmission");
    fields.add(field);

    field = new EmailField("verisignMail","Verisign Email", true);
    field.addValidation(val);
    fields.add(field);

    //Sabre
    //field = new CheckboxField("sabre","Send Sabre Welcome",false);
    //val = new RequiredIfYesValidation(field, "Required for Sabre transmission");
    //fields.add(field);

    //field = new EmailField("sabreMail","Sabre Email", true);
    //field.addValidation(val);
    //fields.add(field);

    //Monthly Statement Update
    YesNoRadioField rField = new YesNoRadioField("emailStmt","Email Monthly Statement");
    fields.add(rField);

    //Association Update
    field = new CheckboxField("mAssoc","Change Merchant Association",false);
    val = new RequiredIfYesValidation(field, "Required for Merchant Association Change");
    fields.add(field);

    field = new Field("n_mAssoc","New Association #",40,12,true);
    field.addValidation(val);
    fields.add(field);

  }

  /**
   * Define legal queue operations.
   */
  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}

//ASG Mgmt queue

      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
    };
  }


}