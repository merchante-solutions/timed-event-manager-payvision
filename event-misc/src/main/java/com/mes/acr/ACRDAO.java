package com.mes.acr;

import java.util.Vector;

/**
 * ACRDAO interface
 * 
 * DAO definition for ACR class
 */
public interface ACRDAO
{
  // constants
  // (none)
  
  // methods
  
  /**
   * createACR()
   * 
   * Encapulates the instantiation of an ACR object 
   *  allowing for DAO specific ops when creating ACRs.
   */
  public ACR              createACR();
  
  public long             insertACR(ACR acr);
  public boolean          deleteACR(ACR acr);
  public ACR              findACR(long pkid);
  
  /**
   * updateACR()
   * 
   * Requires the object to have already been persisted.
   */
  public boolean          updateACR(ACR acr);
  
  /**
   * persistACR()
   * 
   * Akin to save op.
   * Does not require the object to have already been persisted.
   */
  public boolean          persistACR(ACR acr);
  
  public Vector           selectACRs(String fldNme,String fldVal,boolean bLoadItems);
    // collection of ACR objects
  
} // interface ACRDAO
