/*@lineinfo:filename=ACRDataBean_AdrsChng*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.acr;

import java.sql.ResultSet;
import java.util.Iterator;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.support.StringUtilities;
import sqlj.runtime.ResultSetIterator;


public class ACRDataBean_AdrsChng extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_AdrsChng.class);

  // data members
  private ACRDataBeanAddressHelper adrsHelper = null;

  // construction
  public ACRDataBean_AdrsChng()
  {
  }

  public ACRDataBean_AdrsChng(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  protected ACRDataBeanAddressHelper _getAddressHelper()
  {
    if(adrsHelper!=null)
      return adrsHelper; // already configured by virtue of non-null obj ref

    adrsHelper = new ACRDataBeanAddressHelper(this);

    // configure the address helper
    adrsHelper.setAdrsTypeName("Ship To");
    adrsHelper.setAdrsSlctnFldName("staddress");
    //adrsHelper.setAdrsName("");
    adrsHelper.setAdrsAddressFldNme("sh_address");
    adrsHelper.setAdrsAttnFldNme("sh_attn");
    adrsHelper.setAdrsBankNameFldNme("sh_bnknme");
    adrsHelper.setAdrsCityFldNme("sh_city");
    adrsHelper.setAdrsStateFldNme("sh_state");
    adrsHelper.setAdrsZipFldNme("sh_zip");
    //adrsHelper.setAdrsCountry("sh_country");
    adrsHelper.setIsRequired(false);  // require when imprinter plates specified
    adrsHelper.setInternalFieldPrefix("ac_"); // to avoid naming conflicts with this classes address fields

    return adrsHelper;
  }

  public String renderAddressHtml()
  {
    return _getAddressHelper().renderAddressHtml();
  }

  public void doPull()
  {
    super.doPull();

    if(!editable)
      return;

    StringBuffer sb = new StringBuffer();
    Field fld;

    // re-fill the ',' dlm list of old adress types to change
    for(Iterator i = fields.iterator();i.hasNext();) {
      fld = (Field)i.next();

      if(fld.getName().startsWith("oldaddress_") && ((CheckboxField)fld).isChecked()) {
        sb.append(",");
        sb.append(fld.getName().substring(11));
      }
    }
    if(sb.length()>0)
      acr.setACRItem("changeType",sb.substring(1));
    else
      acr.setACRItem("changeType","");

    _getAddressHelper().pullAddressFields();
  }

  protected void setDefaults()
  {
    // parent related info
    if(getAppSeqNum()>0) {

      ResultSetIterator it = null;
      ResultSet         rs = null;

      try {

        connect();

        int i;
        String fldnme,ctdefval;

        // merchant addresses - pull one record for each address type always!
        /*@lineinfo:generated-code*//*@lineinfo:111^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  at.addresstype_code
//                    ,at.ADDRESSTYPE_DESC
//                    ,(a.ADDRESS_LINE1 || ' ' || a.ADDRESS_LINE2) addr
//                    ,a.ADDRESS_CITY
//                    ,a.COUNTRYSTATE_CODE
//                    ,a.ADDRESS_ZIP
//                    ,a.address_phone
//                    ,a.address_fax
//            from    address a, addresstype at
//            where   a.ADDRESSTYPE_CODE(+)=at.ADDRESSTYPE_CODE
//                    and a.app_seq_num(+)=:getAppSeqNum()
//            order by    at.addresstype_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3500 = getAppSeqNum();
  try {
   String theSqlTS = "select  at.addresstype_code\n                  ,at.ADDRESSTYPE_DESC\n                  ,(a.ADDRESS_LINE1 || ' ' || a.ADDRESS_LINE2) addr\n                  ,a.ADDRESS_CITY\n                  ,a.COUNTRYSTATE_CODE\n                  ,a.ADDRESS_ZIP\n                  ,a.address_phone\n                  ,a.address_fax\n          from    address a, addresstype at\n          where   a.ADDRESSTYPE_CODE(+)=at.ADDRESSTYPE_CODE\n                  and a.app_seq_num(+)= :1 \n          order by    at.addresstype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRDataBean_AdrsChng",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3500);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.acr.ACRDataBean_AdrsChng",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:125^9*/

        i=0;
        rs = it.getResultSet();
        while(rs.next()) {
          i++;

          fldnme="adrstypecode_"+i;
          acr.setACRItem(fldnme,rs.getString("addresstype_code"));

          fldnme="adrsname_"+i;
          acr.setACRItem(fldnme,rs.getString("ADDRESSTYPE_DESC")+" ("+rs.getString("addresstype_code")+")");
          fldnme="adrsadrs_"+i;
          acr.setACRItem(fldnme,rs.getString("addr"));
          fldnme="adrscity_"+i;
          acr.setACRItem(fldnme,rs.getString("ADDRESS_CITY"));
          fldnme="adrsstate_"+i;
          acr.setACRItem(fldnme,rs.getString("COUNTRYSTATE_CODE"));
          fldnme="adrszip_"+i;
          acr.setACRItem(fldnme,rs.getString("ADDRESS_ZIP"));
          fldnme="adrsphone_"+i;
          acr.setACRItem(fldnme,rs.getString("address_phone"));
          fldnme="adrsfax_"+i;
          acr.setACRItem(fldnme,rs.getString("address_fax"));

        }
        rs.close();
        it.close();

        // set the number of address just set
        acr.setACRItem("numaddresses",Integer.toString(i));
      }
      catch (Exception e) {
        log.error("setDefaults() EXCEPTION: '"+e.toString()+"'.");
      }
      finally {
        cleanUp();
      }
    }

    // merchant address
    _getAddressHelper().setACRAdrsInfo();
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;

    // address fields
    _getAddressHelper().createAddressFields();

    // old address(es)
    try {
      int numaddresses=Integer.parseInt(acr.getACRValue("numaddresses"));
      String[][] adrsnames = new String[numaddresses][2];
      StringBuffer sb = new StringBuffer();
      String fldnme;

      for(int i=0;i<numaddresses;i++) {
        adrsnames[i][RadioButtonField.RADIO_VALUE]=acr.getACRValue("adrstypecode_"+(i+1));

        fldnme = acr.getACRValue("adrsname_"+(i+1));

        sb.append("<u>"+fldnme+"</u>");

        if(acr.getACRValue("adrsadrs_"+(i+1)).length()>1) {
          sb.append("<div>&nbsp;&nbsp;&nbsp;&nbsp;"+acr.getACRValue("adrsadrs_"+(i+1))+"</div>");
          sb.append("<div>&nbsp;&nbsp;&nbsp;&nbsp;"+acr.getACRValue("adrscity_"+(i+1)));
          if(acr.getACRValue("adrsstate_"+(i+1)).length()>0)
            sb.append(", "+acr.getACRValue("adrsstate_"+(i+1)));
          sb.append("&nbsp;&nbsp;"+acr.getACRValue("adrszip_"+(i+1))+"</div>");
          if(acr.getACRValue("adrsphone_"+(i+1)).length()>0 || acr.getACRValue("adrsfax_"+(i+1)).length()>0)
            sb.append("<div>&nbsp;&nbsp;&nbsp;&nbsp;Phone: "+acr.getACRValue("adrsphone_"+(i+1))+", FAX: "+acr.getACRValue("adrsfax_"+(i+1))+"</div>");
        }

        //adrsnames[i][RadioButtonField.RADIO_LABEL]=sb.toString();
        field = new CheckboxField("oldaddress_"+fldnme,sb.toString(),false);
        fields.add(field);
        sb.delete(0,sb.length()); // reset
      }

      /*
      field = new RadioButtonField("oldaddress",adrsnames,false,"Please specify an old address type to change.");
      ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_VERTICAL);
      fields.add(field);
      */
    }
    catch(Exception e) {
      log.error("Error attempting to create old address fields.");
    }

    StateDropDownTable        stateDrpDwn     = new StateDropDownTable();

    // new address
    if((acrdi=acrd.getACRDefItem("newAdrs"))!=null) {
      field = new TextareaField(acrdi.getName(),100,2,40,true);
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("newCity"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    field = new DropDownField("newState",stateDrpDwn,true);
    field.setShowName("State");
    fields.add(field);
    if((acrdi=acrd.getACRDefItem("newZip"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("newPhone"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("newFax"))!=null)
      fields.add(ACRDefItemToField(acrdi));

    // imprinter plates
    field = new CheckboxField("sendImpPlts","Check here if new imprinter plate is needed and indicate quantity.",false);
    field.setShowName("Send Imprinter Plate?");

    Field _field = new NumberField("ipQty",2,2,true,0,0,99);
    _field.setShowName("Send Imprinter Plate?");
    _field.addValidation(new NumericDataOnlyValidation());


    // validations
    //add validations for the imprinter plate checkbox
    IfYesNotBlankValidation ynbVal = new IfYesNotBlankValidation(field,"You must enter a quantity of imprinter plates.");
    _field.addValidation(ynbVal);

    //simple validation for addresshelperbean
    //NB this will change!!
    field.addValidation(new EntryValidation());

    //add two final fields
    fields.add(field);
    fields.add(_field);
  }


  public String renderHtml(String fname)
  {
    if(fname.equals("sendImpPlts") && inStatusMode()) {
      if(getACRValue("sendImpPlts").equals("y"))
        return "Send Imprinter Plates";
      else
        return "Do NOT send Imprinter Plates.";
    }

    return super.renderHtml(fname);
  }

  protected final class EntryValidation implements Validation
  {
    protected String errorText = "";

    public EntryValidation()
    {
    }

    public String getErrorText()
    {
      return(errorText);
    }

    public boolean validate(String fieldData)
    {
      if( fieldData!=null && fieldData.toUpperCase().equals("Y"))
      {
        // require shipping address
        StringBuffer sb = new StringBuffer();
        if(!adrsHelper.isValidAddress(sb)) {
          errorText = sb.toString();
          return false;
        }

      }

      return true;
    }

  } // EntryValidation class

  public int[][] getAllowedQueueOps()
  {
    int bankNum = StringUtilities.stringToInt(acr.getACRValue("Merchant Bank Number"),-1);

    int initQueue = isMESBank(bankNum)?
          MesQueues.Q_CHANGE_REQUEST_RISK:MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP;

    return new int[][]
    {
       {MesQueues.Q_NONE,initQueue}

      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_COMPLETED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_RISK}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_RISK:
      case MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  public String getHeadingName()
  {
    return "Change Merchant Business and/or Mailing Address";
  }


  /*
  public class AddressTypeDropDownTable extends DropDownTable
  {
    public AddressTypeDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      addElement("","");
      addElement("(00) Physical Address","(00) Physical Address");
      addElement("(01) Mailing Address","(01) Mailing Address");
      addElement("(02) Secondary Address","(01) Secondary Address");
      addElement("Both Physical/Mailing","Both Physical/Mailing");
    }

  }
  */

}/*@lineinfo:generated-code*/