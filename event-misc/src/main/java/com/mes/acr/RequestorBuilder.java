package com.mes.acr;

// log4j classes.
import org.apache.log4j.Logger;
import com.mes.user.UserBean;

/**
 * RequestorBuilder class.
 *
 * Class for building the proper ACRQueueOps sub-class.
 *
 */
public final class RequestorBuilder
{
  // create class log category
  static Logger log = Logger.getLogger(RequestorBuilder.class);

  // constants
  // (none)


  // data members
  // (none)


  // class functions

  public static Requestor build(UserBean userBean)
    throws Exception
  {
    if(userBean==null)
      throw new Exception("Unable to build Requestor instance: Null reference to UserBean specified.");

    Requestor r = new Requestor();

    r.setSourceUserid(userBean.getLoginName());

    return r;
  }


  // object functions
  private RequestorBuilder() { }

} // RequestorBuilder
