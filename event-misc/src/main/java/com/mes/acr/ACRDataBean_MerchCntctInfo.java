/*@lineinfo:filename=ACRDataBean_MerchCntctInfo*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.acr;

import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.PhoneField;


public class ACRDataBean_MerchCntctInfo extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_MerchCntctInfo.class);

  // construction
  public ACRDataBean_MerchCntctInfo()
  {
  }

  public ACRDataBean_MerchCntctInfo(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  protected void setDefaults()
  {
    if(acr==null || getMerchantNumber().length()<1)
      return;

    try {

      int i;
      String fldnme,ctdefval;

      connect();

      // existing merchant contact info
      String phone,fax,email;

      /*@lineinfo:generated-code*//*@lineinfo:49^7*/

//  ************************************************************
//  #sql [Ctx] { select     nvl(phone_1,'---')             phone
//                    ,nvl(phone_2_fax,'---')         fax
//                    ,nvl(EMAIL_ADDR_IND_00,'---')   email
//          
//          from      mif       m
//          where     m.merchant_number = :getMerchantNumber()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3502 = getMerchantNumber();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select     nvl(phone_1,'---')             phone\n                  ,nvl(phone_2_fax,'---')         fax\n                  ,nvl(EMAIL_ADDR_IND_00,'---')   email\n         \n        from      mif       m\n        where     m.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRDataBean_MerchCntctInfo",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3502);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   phone = (String)__sJT_rs.getString(1);
   fax = (String)__sJT_rs.getString(2);
   email = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:59^7*/

      phone = PhoneField.format(phone);
      fax   = PhoneField.format(fax);

      acr.setACRItem("existing_phone",phone);
      acr.setACRItem("existing_fax",fax);
      acr.setACRItem("existing_email",email);
    }
    catch (Exception e) {
      log.error("setDefaults() EXCEPTION: '"+e.toString()+"'.");
    }
    finally {
      cleanUp();
    }
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefinition acrd=acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;

    /*
    for(Enumeration e=acrd.getItemEnumeration(); e.hasMoreElements();) {
      acrdi=(ACRDefItem)e.nextElement();
      fields.add(acrdi);
    }
    */

    Vector v = new Vector(3,1);

    acrdi=acrd.getACRDefItem("business_phone");
    field = new PhoneField(acrdi.getName(),acrdi.getLabel(),true);
    v.add(field);
    fields.add(field);

    acrdi=acrd.getACRDefItem("fax_number");
    field = new PhoneField(acrdi.getName(),acrdi.getLabel(),true);
    v.add(field);
    fields.add(field);

    acrdi=acrd.getACRDefItem("email_address");
    field = new EmailField(acrdi.getName(),acrdi.getLabel(),50,40,true);
    v.add(field);
    fields.add(field);

    // add "at least one" validation
    fields.addValidation(new FieldBean.AtLeastOneValidation("At least one Merchant Contact field must be specified.",v));
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}

      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_RISK:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  public String getHeadingName()
  {
    return "Merchant Contact Information Change";
  }

}/*@lineinfo:generated-code*/