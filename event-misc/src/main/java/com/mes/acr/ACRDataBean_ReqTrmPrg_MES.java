package com.mes.acr;

import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.Selector;
import com.mes.forms.Validation;
import com.mes.forms.YesNoRadioField;
import com.mes.tools.DropDownTable;

public final class ACRDataBean_ReqTrmPrg_MES extends ACRDataBeanBase
{
  static Logger log = Logger.getLogger(ACRDataBean_ReqTrmPrg_MES.class);

  public ACRDataBean_ReqTrmPrg_MES()
  {
  }

  public ACRDataBean_ReqTrmPrg_MES(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP);
  }

  /**
   * Returns title for this acr.
   */
  public String getHeadingName()
  {
    return "Request Terminal Programming Changes";
  }

  /**
   * Set any default values.
   */
  protected void setDefaults()
  {
  }

  private ACRSelectorFactory factory = new ACRSelectorFactory();
  private Selector equipSelector;

  /**
   * Retrieve terminal profile selector, instantiate it if it does not exist.
   */
  public Selector getEquipmentSelector()
  {
    if (equipSelector == null)
    {
     equipSelector = factory.createSelector(factory.ST_REQ_TRM_PRG_MES,
        getMerchantNumber());
    }
    return equipSelector;
  }

  /**
   * Quick and dirty rendering of selected equipment data for post submission
   * mode.
   */
  public String renderSelectedEquipmentHtml()
  {
    //this is hack to reveal new equipment info... to be fixed?!
    //all naming is directly related to the bean and selector item/frame/factory
    boolean hasEquip = false;
    String item = "";
    StringBuffer sb = new StringBuffer();

    // this is a cut and paste of the hack for single selectors (no looping needed)

    item = getACRValue(CALLTAG_EQUIPMENT_NAME);
    if (!item.equals(""))
    {
      if(!hasEquip)
      {
        hasEquip = true;

        sb.append("<table cellpadding=\"1\" border=\"0\" align=\"center\" width=\"100%\" class=\"tableFillGray\" cellspacing=\"0\">");
        sb.append("  <tr>");
        sb.append("    <td>");
        sb.append("      <table class=\"formTable\" width=\"100%\" border=\"0\" cellspacing=\"0\">");
        sb.append("        <tr>");
        sb.append("          <th colspan=5 class=\"tableColumnHead\">Terminal Selection</th>");
        sb.append("        </tr>");
        sb.append("        <tr>");
        sb.append("          <td class=\"tableData\" align=\"center\">Description</td>");
        sb.append("          <td class=\"tableData\" align=\"center\">Serial Number</td>");
        sb.append("          <td class=\"tableData\" align=\"center\">Terminal App</td>");
        sb.append("          <td class=\"tableData\" align=\"center\">Terminal Type</td>");
        sb.append("          <td class=\"tableData\" align=\"center\">V#</td>");
        sb.append("        </tr>");
        sb.append("        <tr>");
        sb.append("          <td colspan=5 height=\"1\" bgcolor=\"#cccccc\"><img src=\"/images/spcr_transp.gif\" height=\"1\"></td>");
        sb.append("        </tr>");
      }
      sb.append(  "        <tr>");
      sb.append(  "           <td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_description")).append("</td>");
      sb.append(  "           <td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_serNum")).append("</td>");
      sb.append(  "           <td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_termApp")).append("</td>");
      sb.append(  "           <td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_termType")).append("</td>");
      sb.append(  "           <td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_vNumber")).append("</td>");
      sb.append(  "        </tr>");
    }

    //finish up
    if(hasEquip)
    {
      sb.append(  "      </table></td></tr></table>");
    }
    else
    {
      sb.append("No equipment selected for this ACR.");
    }

    return sb.toString();
  }

  public static class ChangeableFeatureField extends DropDownField
  {
    public static class ChangeableFeatureTable extends DropDownTable
    {
      public ChangeableFeatureTable()
      {
        addElement("",          "No Change");
        addElement("Disable",   "Disable");
        addElement("Enable",    "Enable");
        addElement("Change",    "Change");
      }
    }

    public static class FeatureDataValidation implements Validation
    {
      Field featureField;

      public FeatureDataValidation(Field ff)
      {
        featureField = ff;
      }

      public String getErrorText()
      {
        return "Required";
      }

      public boolean validate(String fieldData)
      {
        // if feature is marked for enable or change then
        // this field data must be present
        if (featureField.getData().startsWith("Enable") ||
            featureField.getData().startsWith("Change"))
        {
          return fieldData != null && fieldData.length() > 0;
        }
        return true;
      }
    }

    public ChangeableFeatureField(String name, String label)
    {
      super(name,label,new ChangeableFeatureTable(),true);
    }

    public void addFeatureValidationTo(Field dataField)
    {
      dataField.addValidation(new FeatureDataValidation(this));
    }
  }

  public static class StandardFeatureField extends DropDownField
  {
    public static class StandardFeatureTable extends DropDownTable
    {
      public StandardFeatureTable()
      {
        addElement("",        "No Change");
        addElement("Disable", "Disable");
        addElement("Enable",  "Enable");
      }
    }
    public StandardFeatureField(String name, String label)
    {
      super(name,label,new StandardFeatureTable(),true);
    }
  }

  public static class EnableOnlyField extends DropDownField
  {
    public static class EnableOnlyTable extends DropDownTable
    {
      public EnableOnlyTable()
      {
        addElement("",        "No Change");
        addElement("Enable",  "Enable");
      }
    }
    public EnableOnlyField(String name, String label)
    {
      super(name,label,new EnableOnlyTable(),true);
    }
  }

  public static class MerchOwnedPrinterField extends DropDownField
  {
    public static class MerchOwnedPrinterTable extends DropDownTable
    {
      public MerchOwnedPrinterTable()
      {
        addElement("","None");
        addElement("Citizen IDP 560 Roll","Citizen IDP 560 Roll");
        addElement("Citizen IDP 562 Roll","Citizen IDP 562 Roll");
        addElement("Citizen IDP 3530 Roll","Citizen IDP 3530 Roll");
        addElement("DataCard Silent Partner","DataCard Silent Partner");
        addElement("IVI (Ingenico) Scribe 612","IVI (Ingenico) Scribe 612");
        addElement("Hypercom P7 Roll","Hypercom P7 Roll");
        addElement("Hypercom P8 Roll","Hypercom P8 Roll");
        addElement("Hypercom P7 Sprocket","Hypercom P7 Sprocket");
        addElement("Hypercom P8 Sprocket","Hypercom P8 Sprocket");
        addElement("VeriFone Slip Printer 150","VeriFone Slip Printer 150");
        addElement("VeriFone Printer 220","VeriFone Printer 220");
        addElement("VeriFone Printer 250","VeriFone Printer 250");
        addElement("VeriFone Roll 300","VeriFone Roll 300");
        addElement("VeriFone PrintPak 350/355","VeriFone PrintPak 350/355");
        addElement("VeriFone Printer 600","VeriFone Printer 600");
        addElement("VeriFone Printer 900R","VeriFone Printer 900R");
      }
    }
    public MerchOwnedPrinterField(String name, String label)
    {
      super(name,label,new MerchOwnedPrinterTable(),true);
    }
  }

  public static class VendorDropField extends DropDownField
  {
    public static class VendorTable extends DropDownTable
    {
      public VendorTable()
      {
        addElement("","select one");
        addElement("Vital","Vital");
        addElement("VeriCenter","VeriCenter");
        addElement("TermMaster","TermMaster");
      }
    }
    public VendorDropField(String name, String label)
    {
      super(name,label,new VendorTable(),true);
    }
  }

  public static class TerminalTypeField extends DropDownField
  {
    public static class TerminalTypeTable extends DropDownTable
    {
      public TerminalTypeTable()
      {
        addElement("","select one");
        addElement("IP","IP");
        addElement("Dial","Dial");
      }
    }
    public TerminalTypeField(String name, String label)
    {
      super(name,label,new TerminalTypeTable(),false);
    }
  }

  /**
   * Generate fields specific to this ACR.
   */
  protected void createExtendedFields()
  {
    if(!editable || acr == null)
    {
      return;
    }

    try
    {
      // shipping address component
      add(getAddressHelper());

      // terminal profile selector
      add(getEquipmentSelector());

      // wireless terminal id
      Field wirelessLliEsn = new Field("wirelessLliEsn","Wireless LLI/ESN",25,12,true);
      fields.add(wirelessLliEsn);

      // terminal accessories to ship
      Field sendQrg
        = new YesNoRadioField("sendQrg","Send Quick Reference Guide");
      fields.add(sendQrg);
      Field sendOverlay
        = new YesNoRadioField("sendOverlay","Send Overlay");
      fields.add(sendOverlay);
      Field sendKit
        = new YesNoRadioField("sendKit","Send Kit");
      fields.add(sendKit);

      // make address picker required if accessories asked for
      Field addressPicker = fields.getField("addressPicker");
      addressPicker.addValidation(new RequiredIfYesValidation(sendQrg,
        "Required for terminal accessory shipping"));
      addressPicker.addValidation(new RequiredIfYesValidation(sendOverlay,
        "Required for terminal accessory shipping"));
      addressPicker.addValidation(new RequiredIfYesValidation(sendKit,
        "Required for kit shipping"));

      // relabel address picker to shipping address for error display
      addressPicker.setLabel("Shipping Address");

      // terminal features

      //standard template setting
      fields.add(new CheckField("stdPlateSettings", "Standard Template Settings", "Standard Template Settings", "y",false));

      // access code (pabx)
      ChangeableFeatureField accessCodeFeat =
        new ChangeableFeatureField("accessCodeFeat","Access Code (PABX) Feature");
      fields.add(accessCodeFeat);
      Field newAccessCode = new Field("newAccessCode","New Access Code",10,4,true);
      fields.add(newAccessCode);
      accessCodeFeat.addFeatureValidationTo(newAccessCode);

      // batch auto close
      ChangeableFeatureField autoCloseFeat =
        new ChangeableFeatureField("autoCloseFeat","Auto Batch Close Feature");
      fields.add(autoCloseFeat);
      Field newAutoCloseTime =
        new Field("newAutoCloseTime","New Auto Close Time",5,4,true);
      fields.add(newAutoCloseTime);
      autoCloseFeat.addFeatureValidationTo(newAutoCloseTime);

      // password protection
      ChangeableFeatureField passwordProtectionFeat =
        new ChangeableFeatureField("passwordProtectionFeat","Password Protection Feature");
      fields.add(passwordProtectionFeat);

      FieldGroup passFields = new FieldGroup("passFields");

      passFields.add(new Field("newPassword","New Password",4,4,true));
      passFields.add(new CheckField("authOnlyPass", "Auth Only Password", "Auth Only",  "y",false));
      passFields.add(new CheckField("batchPass",    "Batch Password",     "Batch",      "y",false));
      passFields.add(new CheckField("creditPass",   "Credit Password",    "Credit",     "y",false));
      passFields.add(new CheckField("keyboardPass", "Keyboard Password",  "Keyboard",   "y",false));
      passFields.add(new CheckField("voidPass",     "Void Password",      "Void",       "y",false));

      // who knows what kind of validation they are imagining for these
      // fields, so for now they are not validated...
      fields.add(passFields);

      // on/off type features
      fields.add(new StandardFeatureField("tipFlagFeat","Tips Flag Feature"));
      fields.add(new StandardFeatureField("localProgrammingFeat","Local Programming"));
      fields.add(new StandardFeatureField("resetTranNumFeat","Reset Transaction No. Daily Feature"));
      fields.add(new StandardFeatureField("invoiceNumFeat","Invoice No. Prompt Feature"));
      fields.add(new StandardFeatureField("purchasingCardFeat","Purchasing Card Flag Feature"));
      fields.add(new StandardFeatureField("avsFlagFeat","AVS Flag Feature"));

      // turn on only features
      fields.add(new EnableOnlyField("cvv2FlagFeat","CVV2 Flag Feature"));
      fields.add(new EnableOnlyField("cardTruncationFeat","Receipt Card Truncation Feature"));

      // merchant owned printer
      fields.add(new MerchOwnedPrinterField("merchOwnedPrinter","Merchant Owned Printer"));

      // terminal type
      fields.add(new TerminalTypeField("terminalType", "Terminal Type"));

      fields.setHtmlExtra("class=\"formText\"");
    }
    catch (Exception e)
    {
      log.error("createExtendedFields(): "  + e);
    }
  }


  /**
   * Disable editable comments during submission.
   */
  //public boolean hasEditableComments()
  //{
  //  return !editable && super.hasEditableComments();
  //}

  /**
   * Returns true if the indicated acr item has data associated with it.
   */
  public boolean itemPresent(String featureName)
  {
    String itemData = getACRValue(featureName);
    return itemData != null && itemData.length() > 0;
  }
}
