package com.mes.acr;

// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.Field;


public class ACRDataBean_RFNewType_MES extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_RFNewType_MES.class);

  // data members
  ACRDataBeanAddressHelper adrsHelper = null;

  // construction
  public ACRDataBean_RFNewType_MES()
  {
  }

  public ACRDataBean_RFNewType_MES(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_ASG_MGMT);
  }

  protected ACRDataBeanAddressHelper _getAddressHelper()
  {
    if(adrsHelper!=null)
      return adrsHelper; // already configured by virtue of non-null obj ref

    adrsHelper = new ACRDataBeanAddressHelper(this);

    // configure the address helper
    adrsHelper.setAdrsTypeName("Shipping ");
    adrsHelper.setAdrsSlctnFldName("staddress");
    //adrsHelper.setAdrsName("");
    adrsHelper.setAdrsAddressFldNme("sh_address");
    adrsHelper.setAdrsAttnFldNme("sh_attn");
    adrsHelper.setAdrsBankNameFldNme("sh_bnknme");
    adrsHelper.setAdrsCityFldNme("sh_city");
    adrsHelper.setAdrsStateFldNme("sh_state");
    adrsHelper.setAdrsZipFldNme("sh_zip");
    //adrsHelper.setAdrsCountry("sh_country");
    adrsHelper.setIsRequired(false);

    return adrsHelper;
  }

  public String renderAddressHtml()
  {
    return _getAddressHelper().renderAddressHtml();
  }

  public void doPull()
  {
    super.doPull();

    if(!editable)
      return;

    _getAddressHelper().pullAddressFields();
  }

  protected void setDefaults()
  {
    if(getMerchantNumber().length()>0) {

      // merchant address
      _getAddressHelper().setACRAdrsInfo();
    }
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;
    String acrdiname=null;

    // address fields
    _getAddressHelper().createAddressFields();
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_RISK:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Business License not Received"
        };
        break;
      default:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
    }

    return rsns;
  }

  public String getHeadingName()
  {
    return "Request for Type of Change not Listed";
  }

} // class ACRDataBean_RFNewType_MES
