/*@lineinfo:filename=ACRDataBean_FeeChange*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.acr;

import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.Validation;
import com.mes.support.MesMath;
import com.mes.support.StringUtilities;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;


public class ACRDataBean_FeeChange extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_FeeChange.class);


  // constants
  public static final int           NUM_ENTRIES               = 6;


  // class methods
  // (NONE)


  // object methods

  // construction
  public ACRDataBean_FeeChange()
  {
  }

  public ACRDataBean_FeeChange(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public String getPosOrEquipmentRelatedTotalAmountCurrency()
  {
    return MesMath.toCurrency(getPosOrEquipmentRelatedTotalAmount());
  }

  public double getPosOrEquipmentRelatedTotalAmount()
  {
    if(acr==null)
      return 0d;

    double amt  = 0d;
    int    i    = 0;

    while(++i<NUM_ENTRIES) {
      String fat = acr.getACRValue("feealteration_type_"+(i));
      if(fat==null || fat.length()<1)
        break;
      if(!isPosOrEquipmentRelated(fat))
        return 0d;
      String faa = acr.getACRValue("feealteration_action_"+(i));
      if(faa!=null && (faa.equals("Add") || faa.equals("Change"))) {
        double rowAmt = Double.parseDouble(acr.getACRValue("feealteration_amount_"+(i)));
        amt += rowAmt;
      }
    }

    return amt;
  }

  public void doPull()
  {
    super.doPull();

    acr.setACRItem("equipRelated",(isAcrEquipmentRelated()? "1":"0"));
  }

  protected void setDefaults()
  {
    // no-op
  }

  public boolean isAcrEquipmentRelated()
  {
    int i=0;
    while(++i<NUM_ENTRIES) {
      String fat = acr.getACRValue("feealteration_type_"+(i));
      if(fat==null || fat.length()<1)
        break;
      if(!isEquipmentRelated(fat))
        return false;
    }
    return true;
  }

  protected boolean areEntriesEquipmentRelated()
  {
    int i=0;
    while(i<NUM_ENTRIES) {
      String fat = acr.getACRValue("feealteration_type_"+(++i));
      if(fat==null || fat.length()<1)
        break;
      if(!isEntryEquipmentRelated(i))
        return false;
    }
    return true;
  }
  protected boolean isEntryEquipmentRelated(int entryNum)
  {
    FieldGroup fg = (FieldGroup)fields.getField("Fee Alteration "+entryNum);
    if(fg==null)
      return false;
    Field fld = fg.getField("feealteration_type_"+entryNum);
    if(fld==null)
      return false;
    String fat = fld.getData();
    if(fat==null || fat.length()<1)
      return false;

    return isEquipmentRelated(fat);
  }

  protected boolean isEquipmentRelated(String feeAlterationType)
  {
    return isMiscChargeTypeEquipmentRelated(getMiscChargeType(feeAlterationType));
  }
  protected boolean isPosOrEquipmentRelated(String feeAlterationType)
  {
    return isMiscChargeTypePosOrEquipmentRelated(getMiscChargeType(feeAlterationType));
  }
  protected String getMiscChargeType(String feeAlterationType)
  {
    String mct = "";

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:146^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT    MISC_CHRG_TYPE
//          
//          FROM      MISCDESCRS
//          where     instr(:feeAlterationType,misc_description)>0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT    MISC_CHRG_TYPE\n         \n        FROM      MISCDESCRS\n        where     instr( :1 ,misc_description)>0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRDataBean_FeeChange",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,feeAlterationType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mct = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:152^7*/

    }
    catch (Exception e) {
      log.error("getMiscChargeType() EXCEPTION: "+e.toString());
    }
    finally {
      cleanUp();
    }

    return mct;
  }
  protected boolean isMiscChargeTypeEquipmentRelated(String miscChargeType)
  {
    return (miscChargeType!=null && miscChargeType.equals("EQP"));
  }
  protected boolean isMiscChargeTypePosOrEquipmentRelated(String miscChargeType)
  {
    return (miscChargeType!=null && (miscChargeType.equals("EQP") || miscChargeType.equals("POS")));
  }

  public int[][] getAllowedQueueOps()
  {
    String type = acr.getACRValue("Type Code");

    int initQueue = (type.equals("0") || type.equals("19") || type.equals("22") || type.equals("12") || type.equals("16") || type.equals("9") || type.equals("2") || type.equals("7"))?
                      MesQueues.Q_CHANGE_REQUEST_PRICING : MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP;

    if(isAcrEquipmentRelated()) {
      return new int[][]
      {
         {MesQueues.Q_NONE,initQueue}

        ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
        ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
        ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

        ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
        ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
        ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
        ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}

        ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
        ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_PRICING}
        ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
        ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
        ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

        ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
        ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

        ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
        ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      };
    } else {
      return new int[][]
      {
         {MesQueues.Q_NONE,initQueue}

        ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
        ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
        ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

        ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
        ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
        ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
        ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}

        ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
        ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_PRICING}
        ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
        ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
        ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

        ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
        ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      };
    }
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Incomplete Information"
        };
        break;
      case MesQueues.Q_CHANGE_REQUEST_PRICING:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Incomplete Information"
          ,"Request Cancelled"
          ,"Request Declined"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefItem acrdi=null;
    Field field=null;

    String fan=null;
    int i=0;
    String acrdiname=null;
    FieldGroup fldgrp = null;
    Vector feeAlterations = new Vector(NUM_ENTRIES,1);

    FeeTypeDropDownTable feeTypeDrpDwn = new FeeTypeDropDownTable();
    FeeActionDropDownTable feeActionDrpDwn = new FeeActionDropDownTable();
    FeeFrequencyDropDownTable feeFrequencyDrpDwn = new FeeFrequencyDropDownTable();
    MonthDropDownTable monthDrpDwn = new MonthDropDownTable();
    EndPointDropDownTable endPntDrpDwn = new EndPointDropDownTable();

    // define the FieldBean fields from the ACR Definition
    for(Enumeration e=acr.getDefinition().getItemEnumeration();e.hasMoreElements();) {
      acrdi=(ACRDefItem)e.nextElement();

      // create field for this acr def item?
      if(
           acrdi.getName().equals("date")
        || acrdi.getName().equals("app_seq_num")
        || acrdi.getName().equals("dba")
        || acrdi.getName().equals("sales_channel")
        || acrdi.getName().equals("phone_rep")
        || acrdi.getName().equals("extension")
      ) {
        continue;

      // fee alteration field
      } else if(acrdi.getName().indexOf("feealteration_")>=0) {
        acrdiname=acrdi.getName();

        if((i=acrdiname.lastIndexOf('_'))>15) { // (15 = min length of prefix 0-based: "feealteration_1_")

          fan="Fee Alteration "+acrdiname.substring(i+1); // fee alteration name

          if(acrdiname.indexOf("type")>0) {
            // fee alteration type
            field = new DropDownField(acrdi.getName(),feeTypeDrpDwn,true);

          } else if(acrdiname.indexOf("action")>0) {
            // fee alteration action
            field = new DropDownField(acrdi.getName(),feeActionDrpDwn,true);

          } else if(acrdiname.indexOf("frequency")>0) {
            // fee alteration frequency
            field = new DropDownField(acrdi.getName(),feeFrequencyDrpDwn,true);

          } else if(acrdiname.indexOf("startpoint")>0) {
            // fee alteration startpoint
            field = new DropDownField(acrdi.getName(),monthDrpDwn,true);

          } else if(acrdiname.indexOf("end_")>0) {
            // fee alteration endpoint
            field = new DropDownField(acrdi.getName(),endPntDrpDwn,true);

          } else {
            field = ACRDefItemToField(acrdi);
          }

          if((fldgrp=(FieldGroup)fields.getField(fan,false))==null) {
            fldgrp = new FieldGroup(fan);
            fields.add(fldgrp);
          }

          fldgrp.add(field);

          // ammend the distinct collection of fee alteration amounts
          // IMPT: A non-empty fee alteration amount value constitutes an intended fee alteration.
          if(acrdiname.indexOf("amount")>0) {
            feeAlterations.addElement(field);
          }

        } else {
          log.error("Encountered invalid Fee Alteration field '"+field.getName()+"'.  Continuing.");
          continue;
        }

      // non-special field transform
      } else if((field=ACRDefItemToField(acrdi))!=null) {
        fields.add(field);
      }

      //log.debug("Field '"+field.getName()+"' added.");
    }

    // add "at least one" validation for fee alterations
    fields.addValidation(new FieldBean.AtLeastOneValidation("At least one Fee Alteration must be specified to proceed.",feeAlterations));

    // require all listed changes are either all equipment related or all non-equipment related
    fields.addValidation(new AllOfSameTypeValidation());
  }

  public String getHeadingName()
  {
    return "Fee Add/Change";
  }

  private final class AllOfSameTypeValidation implements Validation
  {
    protected String ErrorText = "";

    public AllOfSameTypeValidation()
    {
    }

    public String getErrorText()
    {
      return(ErrorText);
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      boolean allAreSame = false;
      int     numEntries = 0;
      int     numEquipmentEntries = 0;

      for(int i=0; i<NUM_ENTRIES; i++) {
        FieldGroup fg = (FieldGroup)fields.getField("Fee Alteration "+(i+1));
        if(fg==null)
          break;
        Field fld = fg.getField("feealteration_type_"+(i+1));
        if(fld==null)
          break;
        String fat = fld.getData();
        if(fat.length()<1)
          break;
        numEntries++;
        if(isEntryEquipmentRelated(i+1))
          numEquipmentEntries++;
      }

      if(numEntries==numEquipmentEntries || numEquipmentEntries==0)
        return true;
      else {
        ErrorText = "All listed Fee Alterations must either be Equipment related or non-equipment related.";
        return false;
      }
    }
  }

  public final class FeeTypeDropDownTable extends DropDownTable
  {
    public FeeTypeDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      ResultSetIterator   it      = null;
      ResultSet           rs      = null;

      try
      {
        addElement("","");

        connect();

        /*@lineinfo:generated-code*//*@lineinfo:436^9*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT
//                      MISC_CODE
//                      ,MISC_DESCRIPTION
//                      ,MISC_CHRG_TYPE
//            FROM
//                      MISCDESCRS
//            ORDER BY
//                      MISC_DESCRIPTION ASC
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                    MISC_CODE\n                    ,MISC_DESCRIPTION\n                    ,MISC_CHRG_TYPE\n          FROM\n                    MISCDESCRS\n          ORDER BY\n                    MISC_DESCRIPTION ASC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.acr.ACRDataBean_FeeChange",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.acr.ACRDataBean_FeeChange",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:446^9*/

        rs = it.getResultSet();

        String val,desc;

        while (rs.next()) {
          val = StringUtilities.rightJustify(rs.getString("MISC_CODE"),2,'0')+" - "+rs.getString("MISC_DESCRIPTION");
          desc = val + (isMiscChargeTypeEquipmentRelated(rs.getString("MISC_CHRG_TYPE"))? ("[Equip Rltd]"):"");
          addElement(val,desc);  // use desc as actual value to carry over the necessary information
        }

        it.close();
      }
      catch (Exception e) {
        log.error("FeeTypeDropDownTable.getData() EXCEPTION: '"+e.getMessage()+"'.");
      }
      finally {
        cleanUp();
      }

    }

  } // FeeTypeDropDownTable class

  private final class EndPointDropDownTable extends DropDownTable
  {
    public EndPointDropDownTable()
    {
      super();

      addElement("","");
      addElement("Ongoing","Ongoing");
      addElement("Jan","Jan");
      addElement("Feb","Feb");
      addElement("Mar","Mar");
      addElement("Apr","Apr");
      addElement("May","May");
      addElement("Jun","Jun");
      addElement("Jul","Jul");
      addElement("Aug","Aug");
      addElement("Sep","Sep");
      addElement("Oct","Oct");
      addElement("Nov","Nov");
      addElement("Dec","Dec");
    }
  }

  private final class MonthDropDownTable extends DropDownTable
  {
    public MonthDropDownTable()
    {
      super();

      addElement("","");
      addElement("Jan","Jan");
      addElement("Feb","Feb");
      addElement("Mar","Mar");
      addElement("Apr","Apr");
      addElement("May","May");
      addElement("Jun","Jun");
      addElement("Jul","Jul");
      addElement("Aug","Aug");
      addElement("Sep","Sep");
      addElement("Oct","Oct");
      addElement("Nov","Nov");
      addElement("Dec","Dec");
    }

  } // FeeActionDropDownTable class

  public final class FeeActionDropDownTable extends DropDownTable
  {
    public FeeActionDropDownTable()
    {
      super();

      addElement("","");
      addElement("Add","Add");
      addElement("Change","Change");
      addElement("Remove","Remove");
    }

  } // FeeActionDropDownTable class

  public final class FeeFrequencyDropDownTable extends DropDownTable
  {
    public FeeFrequencyDropDownTable()
    {
      super();

      addElement("","");
      addElement("One Time","One Time");
      addElement("Monthly","Monthly");
      addElement("Annual","Annual");
    }

  } // FeeFrequencyDropDownTable class

}/*@lineinfo:generated-code*/