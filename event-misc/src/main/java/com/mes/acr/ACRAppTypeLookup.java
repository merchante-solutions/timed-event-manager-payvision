/*@lineinfo:filename=ACRAppTypeLookup*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.acr;

import java.sql.ResultSet;
// log4j classes.
import org.apache.log4j.Logger;
import com.mes.constants.MesHierarchy;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.StringUtilities;
import sqlj.runtime.ResultSetIterator;


public final class ACRAppTypeLookup extends SQLJConnectionBase
{
  // Singleton
  public static ACRAppTypeLookup getInstance()
  {
    if(_instance==null)
      _instance = new ACRAppTypeLookup();

    return _instance;
  }
  private static ACRAppTypeLookup _instance = null;
  ///

  // create class log category
  static Logger log = Logger.getLogger(ACRAppTypeLookup.class);

  // data members
  private long    merchantNum = -1L;
  private int     acrAppType = -1;

  // construction
  private ACRAppTypeLookup()
  {
  }

  public String getAppTypeCode(int appType)
  {
    String appTypeCode = "";

    try {

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:47^7*/

//  ************************************************************
//  #sql [Ctx] { select distinct appsrctype_code
//          
//          from org_app
//          where app_type = :appType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select distinct appsrctype_code\n         \n        from org_app\n        where app_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRAppTypeLookup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appTypeCode = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:53^7*/
    }
    catch(Exception e) {
      log.error("getAppTypeCode() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      cleanUp();
    }

    return appTypeCode;
  }

  public int getAppType(String merchantNum)
  {
    long mn = StringUtilities.stringToLong(merchantNum,-1L);
    if(mn == -1L)
      return -1;

    return getAppType(mn);
  }

  private String getNativeAppType(long merchantNum)
  {
    String              result  = "UNKN";
    ResultSet rs=null;
    ResultSetIterator rsItr=null;

    try {

      connect();

      // use org_app, t_hierarchy, and mif to determine app type of merchant
      // specified in the acr table for this acr id
      /*@lineinfo:generated-code*//*@lineinfo:86^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { select  oa.appsrctype_code    app_type
//          from    t_hierarchy   th,
//                  org_app       oa,
//                  mif           m
//          where   m.merchant_number=:merchantNum and
//                  m.association_node = th.descendent and
//                  th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                  th.ancestor = oa.hierarchy_node
//          order by th.relation asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  oa.appsrctype_code    app_type\n        from    t_hierarchy   th,\n                org_app       oa,\n                mif           m\n        where   m.merchant_number= :1  and\n                m.association_node = th.descendent and\n                th.hier_type =  :2  and\n                th.ancestor = oa.hierarchy_node\n        order by th.relation asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.acr.ACRAppTypeLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNum);
   __sJT_st.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.acr.ACRAppTypeLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:97^7*/

      rs = rsItr.getResultSet();

      // first item in result set is the app type
      if(rs.next())
      {
        result=processString(rs.getString("app_type"));
      }

    }
    catch(Exception e) {
      log.error("determineNativeAppType() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      try { rs.close(); }    catch(Exception e) {}
      try { rsItr.close(); } catch(Exception e) {}
      cleanUp();
    }

    return result;
  }

  public int getAppType(long merchantNum)
  {
    if(this.merchantNum == merchantNum)
      return this.acrAppType;

    // reset
    this.merchantNum = -1L;
    this.acrAppType = -1;

    // check the "native" app type for exceptions (e.g.: MESA, MESB)
    String nat = getNativeAppType(merchantNum);
    if(nat.equals("MESA") || nat.equals("MESB")) {
      return mesConstants.APP_TYPE_MES;
    }

    ResultSetIterator it = null;
    ResultSet         rs = null;

    try {

      long closest_parent = 9999999999L;  // default: mes

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:144^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  th.ancestor     closest_parent
//          from    t_hierarchy     th,
//                  mif             m,
//                  acr_apptype     acrat
//          where   m.merchant_number = :merchantNum and
//                  m.association_node = th.descendent and
//                  th.ancestor = acrat.hierarchy_node
//          order by th.relation asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  th.ancestor     closest_parent\n        from    t_hierarchy     th,\n                mif             m,\n                acr_apptype     acrat\n        where   m.merchant_number =  :1  and\n                m.association_node = th.descendent and\n                th.ancestor = acrat.hierarchy_node\n        order by th.relation asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.acr.ACRAppTypeLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.acr.ACRAppTypeLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:154^7*/

      rs = it.getResultSet();
      rs.next();

      closest_parent = rs.getLong(1);

      rs.close();
      it.close();

      // now get the associated acr app type
      /*@lineinfo:generated-code*//*@lineinfo:165^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_type
//          
//          from    acr_apptype     acrat
//          where   acrat.hierarchy_node = :closest_parent
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_type\n         \n        from    acr_apptype     acrat\n        where   acrat.hierarchy_node =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.acr.ACRAppTypeLookup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,closest_parent);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   acrAppType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:171^7*/

    }
    catch(Exception e) {
      log.error("ACRAppTypeLookup() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      cleanUp();
    }

    return acrAppType;
  }

}/*@lineinfo:generated-code*/