package com.mes.acr;

import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;


public final class ACRDataBean_CardAdds_CBT extends ACRDataBean_CardAdds
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_CardAdds_CBT.class);

  // construction
  public ACRDataBean_CardAdds_CBT()
  {
  }

  public ACRDataBean_CardAdds_CBT(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  protected void setDefaults()
  {
    // no-op
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;
    String fldnme;

    String[][] AmexSplitRadioButtons =
    {
      { "Yes"  , "Yes"  },
      { "No"   , "No"   }
    };

    Vector cardAdds = new Vector(6,1);

    try {
      acrdi=acrd.getACRDefItem("discover");
      field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),false);
      cardAdds.addElement(field);
      fields.add(field);
      acrdi=acrd.getACRDefItem("jcb");
      field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),false);
      cardAdds.addElement(field);
      fields.add(field);
      acrdi=acrd.getACRDefItem("diners");
      field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),false);
      cardAdds.addElement(field);
      fields.add(field);
      acrdi=acrd.getACRDefItem("debit");
      field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),false);
      cardAdds.addElement(field);
      fields.add(field);
      acrdi=acrd.getACRDefItem("ebt");
      field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),false);
      cardAdds.addElement(field);
      fields.add(field);
      acrdi=acrd.getACRDefItem("americanExpress");
      field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),false);
      cardAdds.addElement(field);
      fields.add(field);

      acrdi=acrd.getACRDefItem("americanExpressNum");
      field = ACRDefItemToField(acrdi);
      fields.add(field);
      acrdi=acrd.getACRDefItem("discoverNum");
      field = ACRDefItemToField(acrdi);
      fields.add(field);
      acrdi=acrd.getACRDefItem("jcbNum");
      field = ACRDefItemToField(acrdi);
      fields.add(field);
      acrdi=acrd.getACRDefItem("dinersNum");
      field = ACRDefItemToField(acrdi);
      fields.add(field);
      acrdi=acrd.getACRDefItem("ebtNum");
      field = ACRDefItemToField(acrdi);
      fields.add(field);

      acrdi=acrd.getACRDefItem("authfee_amex");
      field = ACRDefItemToField(acrdi);
      fields.add(field);
      acrdi=acrd.getACRDefItem("authfee_disc");
      field = ACRDefItemToField(acrdi);
      fields.add(field);
      acrdi=acrd.getACRDefItem("authfee_diners");
      field = ACRDefItemToField(acrdi);
      fields.add(field);
      acrdi=acrd.getACRDefItem("authfee_jcb");
      field = ACRDefItemToField(acrdi);
      fields.add(field);
      acrdi=acrd.getACRDefItem("authfee_ebt");
      field = ACRDefItemToField(acrdi);
      fields.add(field);
      acrdi=acrd.getACRDefItem("authfee_debit");
      field = ACRDefItemToField(acrdi);
      fields.add(field);

      // amex specific
      acrdi=acrd.getACRDefItem("amexSplit");
      field = new RadioButtonField("amexSplit", AmexSplitRadioButtons, -1, !acrdi.isRequired(), "Please specify whether or not amex split dial is used." );
      fields.add(field);
    }
    catch(Exception e) {
      log.error("createFields() EXCEPTION: '"+e.getMessage()+"'.");
      return;
    }

    // require "at least one" card add be checked
    fields.addValidation(new FieldBean.AtLeastOneValidation("At least one Card Type must be selected to proceed.",cardAdds));

    fields.addValidation(new CBTAmExValidation());
    fields.addValidation(new CBTDiscoverValidation());
    fields.addValidation(new DinersValidation());
    fields.addValidation(new JCBValidation());
    fields.addValidation(new EBTValidation());

    return;
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_CBT_NEW}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  public String getHeadingName()
  {
    return "Add Card Types to CB&T Merchant Account";
  }

  public String getJSPIncludeFilename()
  {
    return (acr==null)? "":"acr_incl_"+acr.getDefinition().getShortName()+"_cbt.jsp";
  }

  protected class CBTAmExValidation implements Validation
  {
    private String errorText = "";

    public CBTAmExValidation()
    {
    }

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      Field fld,fld2;

      fld=fields.getField("americanExpress");
      if(fld==null || fld.isBlank())
        return true;

      // selected so do related edits

      // ensure split dial specified
      fld=fields.getField("amexSplit");
      if(fld==null || fld.isBlank()) {
        errorText="Please specify Split Dial for AmEx or provide AmEx merchant number.";
        return false;
      }

      if(fld.getData().equals("No")) {
        fld=fields.getField("americanExpressNum");
        if(fld.isBlank()) {
          errorText="Please specify an AmEx merchant number.";
          return false;
        }
      }

      return true;
    }

  } // class CBTAmExValidation

  protected class CBTDiscoverValidation implements Validation
  {
    private String errorText = "";

    public CBTDiscoverValidation()
    {
    }

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      Field fld,fld2;

      fld=fields.getField("discover");
      if(fld==null || fld.isBlank())
        return true;

      // selected so do related edits

      // ensure merchant number specified if existing card type
      fld2=fields.getField("discoverNum");
      if(fld2==null || fld2.isBlank()) {
        errorText="Please specify the Merchant Number for Discover.";
        return false;
      }

      return true;
    }

  } // class CBTDiscoverValidation

} // class ACRDataBean_CardAdds_CBT
