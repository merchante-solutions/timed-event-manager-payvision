package com.mes.acr;

import org.apache.log4j.Logger;
import com.mes.equipment.EquipSelectorItem;
import com.mes.forms.Condition;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HtmlRenderable;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;

public class SI_AcctClose extends EquipSelectorItem
  implements ACRTranslator
{
  static Logger log = Logger.getLogger(SI_AcctClose.class);

  public SI_AcctClose()
  {
  }

  public void init()
  {
    if (type == T_STANDARD)
    {
      setColumnRenderer("col1", new HtmlRenderable()
        { public String renderHtml() { return getSerialNum(); } } );
      setColumnRenderer("col2", new HtmlRenderable()
        { public String renderHtml() { return getDescription(); } } );
      setColumnRenderer("col3", new HtmlRenderable()
        { public String renderHtml() { return getEquipType(); } } );
      setColumnRenderer("col4", new HtmlRenderable()
        { public String renderHtml() { return getPartNum(); } } );
      setColumnRenderer("col5", new HtmlRenderable()
        { public String renderHtml() { return getDisposition(); } } );
    }
    else if (type == T_OTHER)
    {
      Field serialNum = new Field(value + "_serNum","Serial number",20,10,true);
      setColumnRenderer("col1",serialNum);
      fields.add(serialNum);

      Field description
        = new Field(value + "_description","Equipment description",60,12,true);
      setColumnRenderer("col2",description);
      fields.add(description);

      Field equipType = new DropDownField(value + "_equipType",
        "Equipment type",
        TableFactory.getDropDownTable(TableFactory.EQUIP_TYPE),true);
      setColumnRenderer("col3",equipType);
      fields.add(equipType);

      Field partNum = new Field(value + "_partNum","Part number",20,8,true);
      setColumnRenderer("col4",partNum);
      fields.add(partNum);

      Field disposition = new DropDownField(value + "_disposition",
        "Disposition",
        new _ddDispTable(),
        true);
      setColumnRenderer("col5",disposition);
      fields.add(disposition);
    }

    //TableFactory.getDropDownTable(TableFactory.DISPOSITION)
  }

  public void setControlField(Field controlField)
  {
    super.setControlField(controlField);
    Condition vc = new FieldValueCondition(controlField,value);
    Validation required = new ConditionalRequiredValidation(vc);
    fields.addGroupValidation(required);
  }

  public void doPush(ACR acr)
  {
    log.debug("pretending to push acr item data...");
  }

  public void doPull(ACR acr)
  {
    try
    {
      acr.setACRItem(value+"_serNum",getSerialNum());
      acr.setACRItem(value+"_description",getDescription());
      acr.setACRItem(value+"_equipType",getEquipType());
      acr.setACRItem(value+"_partNum",getPartNum());
      acr.setACRItem(value+"_disposition",getDisposition());
    }
    catch(Exception e)
    {
      //do nothing
    }
  }

  private final class _ddDispTable extends DropDownTable
  {

    public _ddDispTable()
    {
      addElement("","");
      addElement("Deployed - Purchased","Deployed - Purchased");
      addElement("Deployed - Rent","Deployed - Rent");
      addElement("Deployed - Loan","Deployed - Loan");
      addElement("Deployed - PP Exchange","Deployed - PP Exchange");
      addElement("Deployed - Merchant Owned","Deployed - Merchant Owned");
    }
  }
}

