package com.mes.acr;

import java.util.List;
import org.apache.log4j.Logger;
import com.mes.equipment.EquipDataLoader;
import com.mes.forms.Selector;
import com.mes.forms.SelectorFrame;

public class ACRSelectorFactory
{
  // log4j initialization
  static Logger log = Logger.getLogger(ACRSelectorFactory.class);

  public static final int ST_EQUIPPU_CALLTAGS       = 1;
  public static final int ST_EQUIPDISP_SELECTION    = 2;
  public static final int ST_SWAP_CALLTAGS          = 3;
  public static final int ST_ACCT_CLOSE_CALLTAGS    = 4;
  public static final int ST_TERMINAL_ACC           = 5;
  public static final int ST_REQ_TRM_PRG            = 6;
  public static final int ST_EQUIPUPG               = 7;
  public static final int ST_SWAP_CALLTAGS_MES      = 8;
  public static final int ST_REQ_TRM_PRG_MES        = 9;


  //CALLTAG_EQUIPMENT_NAME = "Equipment";
  //this is used in ACR beans to determine calltagged equipment
  //DO NOT CHANGE at risk of losing calltag functionality


  public Selector createSelector(int selectorType, String merchNum)
  {
    Selector selector;
    String name;
    SelectorFrame frame;
    List items;

    /**
     *    CURRENT STANDARD PROCEDURE:
     * 1. provide the selector name
     * 2. generate the frame type
     * 3. generate the preferred equipment list
     * 4. create the type of selector needed
     */
    switch (selectorType)
    {

      case ST_EQUIPPU_CALLTAGS:

        name      = ACRDataBeanBase.CALLTAG_EQUIPMENT_NAME;
        frame     = new SF_EquipPU("Equipment To Pick Up - Call Tag Generation");
        items     = EquipDataLoader.getSelectorList(merchNum,
                                                SI_EquipPU.class,
                                                name,
                                                2);
        selector  = new ACRMultiSelector(name,items,frame);

        break;

      case ST_EQUIPDISP_SELECTION:

        name      = "Equipment";
        frame     = new SF_EquipDisp("Equipment To Change");
        items     = EquipDataLoader.getSelectorList(merchNum,
                                                SI_EquipDisp.class,
                                                name,
                                                2);
        selector  = new ACRMultiSelector(name,items,frame);

        break;

      case ST_SWAP_CALLTAGS:

        name      = ACRDataBeanBase.CALLTAG_EQUIPMENT_NAME;
        frame     = new SF_EquipSwap("Equipment To Swap - Call Tag Generation");
        items     = EquipDataLoader.getSelectorList(merchNum,
                                                    SI_EquipSwap.class,
                                                    name,
                                                    2);
        selector  = new ACRMultiSelector(name,items,frame);

        break;


      case ST_ACCT_CLOSE_CALLTAGS:

        name      = ACRDataBeanBase.CALLTAG_EQUIPMENT_NAME;
        frame     = new SF_AcctClose("Equipment To Pickup - Call Tag Generation");
        items     = EquipDataLoader.getSelectorList(merchNum,
                                                    SI_AcctClose.class,
                                                    name,
                                                    2);
        selector  = new ACRMultiSelector(name,items,frame, false);

        break;

      case ST_TERMINAL_ACC:

        name = "Equipment";

        frame = new SF_TermAcc("Terminal Selection");

        items = EquipDataLoader.getSelectorList(merchNum,
                                                SI_TermAcc.class,
                                                name,
                                                EquipDataLoader.TERMINAL_ONLY,
                                                0
                                                );

        selector = new ACRSingleSelector(name,items,frame);

        break;

      case ST_REQ_TRM_PRG:

        name      = "Equipment";
        frame     = new SF_ReqTrmPrg("Terminal Profiles");
        items     = EquipDataLoader.getSelectorList(merchNum,
                                                    SI_ReqTrmPrg.class,
                                                    name,
                                                    EquipDataLoader.TERMINAL_ONLY,
                                                    1);
        selector  = new ACRSingleSelector(name,items,frame);

        break;


      case ST_EQUIPUPG:

        name      = ACRDataBeanBase.CALLTAG_EQUIPMENT_NAME;
        frame     = new SF_EquipUpg("Upgrade Existing Equipment - Call Tag Generation");
        items     = EquipDataLoader.getSelectorList(merchNum,
                                                    SI_EquipUpg.class,
                                                    name,
                                                    2);
        selector  = new ACRMultiSelector(name,items,frame, false);

        break;

      case ST_SWAP_CALLTAGS_MES:

        name      = ACRDataBeanBase.CALLTAG_EQUIPMENT_NAME;
        frame     = new SF_EquipSwap_mes("Equipment To Swap - Call Tag Generation");
        items     = EquipDataLoader.getSelectorList(merchNum,
                                                    SI_EquipSwap_mes.class,
                                                    name,
                                                    2);
        selector  = new ACRMultiSelector(name,items,frame);

        break;

      case ST_REQ_TRM_PRG_MES:

        name      = "Equipment";
        frame     = new SF_ReqTrmPrg_mes("Terminal Profiles");
        items     = EquipDataLoader.getSelectorList(merchNum,
                                                    SI_ReqTrmPrg_mes.class,
                                                    name,
                                                    1);
        selector  = new ACRSingleSelector(name,items,frame);

        break;

      default:
        String errorMsg = "Unsupported selector type (" + selectorType + ")";
        log.error(errorMsg);
        throw new RuntimeException(errorMsg);
    }


    return selector;
  }


}