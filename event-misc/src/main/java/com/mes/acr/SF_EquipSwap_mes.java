package com.mes.acr;

import com.mes.forms.SelectorColumnDef;

public class SF_EquipSwap_mes extends SF_EquipBase
{
  public SF_EquipSwap_mes(String title)
  {
    super(title);
  }

  public void setupFrame()
  {

    addColumn(new SelectorColumnDef("col1","Serial No.",                93));
    addColumn(new SelectorColumnDef("col2","Description",               82));
    addColumn(new SelectorColumnDef("col3","Equipment Type",            155));
    addColumn(new SelectorColumnDef("col4","VNum/TID",                  62));
    addColumn(new SelectorColumnDef("col5","Terminal<br>App",           104));
    addColumn(new SelectorColumnDef("col6","Vendor ID",                 115));
    addColumn(new SelectorColumnDef("col7","Status",                    89));
  }
}
