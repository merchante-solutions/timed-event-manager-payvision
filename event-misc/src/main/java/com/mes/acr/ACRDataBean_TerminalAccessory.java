package com.mes.acr;

import java.util.Enumeration;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.Field;
import com.mes.forms.NumberField;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;

public class ACRDataBean_TerminalAccessory extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_TerminalAccessory.class);

  // data members
  ACRDataBeanAddressHelper adrsHelper = null;

  // construction
  public ACRDataBean_TerminalAccessory()
  {
  }

  public ACRDataBean_TerminalAccessory(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  private ACRDataBeanAddressHelper _getAddressHelper()
  {
    if(adrsHelper!=null)
      return adrsHelper; // already configured by virtue of non-null obj ref

    adrsHelper = new ACRDataBeanAddressHelper(this);

    // configure the address helper
    adrsHelper.setAdrsTypeName("Ship To");
    adrsHelper.setAdrsSlctnFldName("staddress");
    //adrsHelper.setAdrsName("");
    adrsHelper.setAdrsAddressFldNme("sh_address");
    adrsHelper.setAdrsAttnFldNme("sh_attn");
    adrsHelper.setAdrsBankNameFldNme("sh_bnknme");
    adrsHelper.setAdrsCityFldNme("sh_city");
    adrsHelper.setAdrsStateFldNme("sh_state");
    adrsHelper.setAdrsZipFldNme("sh_zip");
    //adrsHelper.setAdrsCountry("sh_country");
    adrsHelper.setIsRequired(true);

    return adrsHelper;
  }

  public String renderAddressHtml()
  {
    return _getAddressHelper().renderAddressHtml();
  }

  public void doPull()
  {
    super.doPull();

    if(!editable)
      return;

    _getAddressHelper().pullAddressFields();

    //calcTotals();
  }

  protected void setDefaults()
  {
    ACRDefinition acrd=acr.getDefinition();
    ACRDefItem acrdi=null;

    // parent related info
    if(getMerchantNumber().length()>0) {
      // merchant address
      _getAddressHelper().setACRAdrsInfo();
    }

    /*
    // take any default value from the item defs to the acr item value
    for(Enumeration e=acrd.getItemEnumeration();e.hasMoreElements();) {
      acrdi=(ACRDefItem)e.nextElement();
      if(acrdi.hasDefaultValue())
        acr.setACRItem(acrdi.getName(),acrdi.getDefaultValue());
    }

    acr.setACRItem("total","0.00");
    */
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    addDefFields();

    // address fields
    _getAddressHelper().createAddressFields();

    // imprinter plates
    Field      field=null;
    field = new CheckboxField("sendImpPlts","Check here if new imprinter plate is needed and indicate quantity.",false);
    field.setShowName("Send Imprinter Plate?");
    fields.add(field);
    field = new NumberField("ipQty",2,2,true,0,0,99);
    field.setShowName("Imprinter Plate Quantity");
    fields.add(field);

    // validations
    fields.addValidation(new EntryValidation());
  }

 protected void addDefFields()
 {
    Field      field=null;
    ACRDefinition acrd=acr.getDefinition();
    ACRDefItem acrdi=null;

    String     acrdiname       = null;

    QuantityDropDownTable     qtyDrpDwn       = new QuantityDropDownTable();

    // connect to/from fields
    field = new Field("connect_to",75,35,true);
    fields.add(field);
    field = new Field("connect_from",75,35,true);
    fields.add(field);

    AtLeastOneValidation val = new AtLeastOneValidation("At least one Quantity must be set to proceed.");

    // define the FieldBean fields from the ACR Definition
    for(Enumeration e=acrd.getItemEnumeration();e.hasMoreElements();) {
     acrdi=(ACRDefItem)e.nextElement();
     acrdiname= acrdi.getName();

     if(acrdiname.indexOf("_qty")>0)
     {
       field = new NumberField(acrdi.getName(),acrdi.getLabel(),3,3,true,0);
       //field = new DropDownField(acrdi.getName(),qtyDrpDwn,true);
       //field.setShowName(acrdi.getLabel());
       val.addField(field);
     }
     fields.add(field);
    }

    fields.addValidation(val);
  }

  public String renderHtml(String fname)
  {
    if( inStatusMode() )
    {
      if(fname.equals("sendImpPlts"))
      {
        if(getACRValue("sendImpPlts").equals("y"))
          return "Send Imprinter Plates";
        else
          return "Do NOT send Imprinter Plates.";
      }
      if(fname.equals("ipQty"))
      {
        if(getACRValue("sendImpPlts").equals("y"))
          return super.renderHtml(fname);
        else
          return "--";
      }
    }

    return super.renderHtml(fname);
  }

  protected final class EntryValidation implements Validation
  {
    protected String ErrorText = "";

    public EntryValidation()
    {
    }

    public String getErrorText()
    {
      return(ErrorText);
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      Field fld;

      // imprinter plates validation
      fld = fields.getField("sendImpPlts");
      if(fld!=null && ((CheckboxField)fld).isChecked()) {
        fld = fields.getField("ipQty");
        fld.makeRequired();
        if(!fld.isValid()) {
          ErrorText = fld.getErrorText();
          return false;
        }
        fld.removeValidation("required");
      }

      return true;
    }

  } // EntryValidation class


  public void doPush()
  {
    super.doPush();

    if(!editable)
      return;

    //calcTotals();
  }


  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}

      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP:
      case MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  public final class QuantityDropDownTable extends DropDownTable
  {
    public QuantityDropDownTable()
    {
      for(int i=0; i<100; i++)
      {
        addElement(Integer.toString(i),Integer.toString(i));
      }
    }

  } // QuantityDropDownTable class


  public String getHeadingName()
  {
    return "Order Terminal Accessories for Merchant";
  }

} // class ACRDataBean_TerminalAccessory
