package com.mes.acr;

import com.mes.forms.Field;
import com.mes.forms.HiddenField;

public class ACRDataBean_MiscAcctChng_Base extends ACRDataBeanBase
{
  public ACRDataBean_MiscAcctChng_Base()
  {
  }

  public ACRDataBean_MiscAcctChng_Base(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  /**
   * Returns title for this acr.
   */
  public String getHeadingName()
  {
    return "Misc. Account Changes<br>(Monthly Statements, Association Updates, BET Changes, and Deposit Info)";
  }

  public String renderHtml(String fName)
  {
    if (editable)
    {
      if(fName.indexOf("o_")==0)
      {
        //get the field
        Field field = fields.getField(fName);
        if(field != null && field instanceof HiddenField)
        {
          StringBuffer sb = new StringBuffer(64);
          sb.append(((HiddenField)field).renderHtmlField());
          sb.append(field.getData());
          return sb.toString();
        }
        else
        {
          return super.renderHtml(fName);
        }
      }
      else
      {
        return super.renderHtml(fName);
      }
    }
    else
    {
      String render = super.renderHtml(fName);

      if(fName.equals("var"))
      {
        return decode(render,"Send VAR Form");
      }
      else if (fName.equals("verisign"))
      {
        return decode(render,"Send Verisign Welcome");
      }
      else if (fName.equals("sabre"))
      {
        return decode(render,"Send Sabre Welcome");
      }
      else if (fName.equals("emailStmt"))
      {
        return decode(render,"Email Monthly Statement");
      }
      else if (fName.equals("mAssoc"))
      {
        return decode(render,"Change Merchant Association");
      }
      else if (fName.equals("cAssoc"))
      {
        return decode(render,"Create New Chain Association");
      }
      else if (fName.equals("visa"))
      {
        return decode(render,"Change Merchant Visa BET");
      }
      else if (fName.equals("mc"))
      {
        return decode(render,"Change Merchant MC BET");
      }
      else
      {
        return render;
      }
    }
  }

  protected String decode(String yn, String suffix)
  {
    StringBuffer sb = new StringBuffer();
    if(yn!=null && yn.equalsIgnoreCase("y"))
    {
      sb.append("<b>YES</b>:");
    }
    else
    {
      sb.append("NO: ");
    }
    sb.append(suffix);
    return sb.toString();
  }



  /**
   * Set any default values.- no op
   */
  protected void setDefaults()
  {
  }

}