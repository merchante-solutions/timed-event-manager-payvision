package com.mes.acr;

public abstract class DAOFactory
{
  // constants
  // dao factory types
  public static final int       DAO_MESDB         = 1;
  
  // class functions
  // (none)
  
  // object functions
  
  public abstract ACRDefDAO           getACRDefinitionDAO();
  public abstract ACRDAO              getACRDAO();
  public abstract MerchantInfoDAO     getMerchantInfoDAO();
  
} // class DAOFactory
