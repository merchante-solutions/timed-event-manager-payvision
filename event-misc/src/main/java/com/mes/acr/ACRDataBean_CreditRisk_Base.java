package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.TaxIdField;
import com.mes.tools.DropDownTable;

public class ACRDataBean_CreditRisk_Base extends ACRDataBeanBase
{
  public ACRDataBean_CreditRisk_Base()
  {
  }

  public ACRDataBean_CreditRisk_Base(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  /**
   * Returns title for this acr.
   */
  public String getHeadingName()
  {
    return "Request Misc. Credit Risk Changes";
  }

  public String renderHtml(String fName)
  {
    if (editable)
    {
      if(fName.indexOf("o_")==0)
      {
        //get the field
        Field field = fields.getField(fName);
        if(field != null && field instanceof HiddenField)
        {
          StringBuffer sb = new StringBuffer(64);
          sb.append(((HiddenField)field).renderHtmlField());
          sb.append(field.getData());
          return sb.toString();
        }
        else
        {
          return super.renderHtml(fName);
        }
      }
      else
      {
        return super.renderHtml(fName);
      }
    }
    else
    {
      return super.renderHtml(fName);
    }
  }

  /**
   * Set any default values.
   */
  protected void setDefaults()
  {
    log.debug("setting old values...");
    //collect all old information if first time in...
    //otherwise, ACR will hold info in name/value pairing
    //TODO
    if(editable)
    {

      ResultSet rs          = null;
      PreparedStatement ps  = null;

      //SIC Code
      //Federal ID Number
      //SSNumber
      //Ownership
      //Business Type
      //Average Ticket
      //Monthly Volume
      String app_sic_code               = "n/a";
      String merch_federal_tax_id       = "n/a";
      String busowner_ssn               = "n/a";
      String bus_name                   = "n/a";
      String bustype_desc               = "n/a";
      String merch_average_cc_tran      = "n/a";
      String merch_month_tot_proj_sales = "n/a";

      try
      {
        connect();

        String qs = "select                                   "
                  + "m.app_sic_code,                          "
                  + "bt.bustype_desc,                         "
                  + "m.merch_federal_tax_id,                  "
                  + "m.merch_month_tot_proj_sales,            "
                  + "m.merch_average_cc_tran,                 "
                  + "b.busowner_ssn,                          "
                  + "b.busowner_first_name || ' ' ||          "
                  + "b.busowner_last_name as bus_name         "
                  + "from merchant m,                         "
                  + "businessowner b,                         "
                  + "bustype bt                               "
                  + "where m.merch_number = ?                 "
                  + "and m.app_seq_num = b.app_seq_num(+)     "
                  + "and b.busowner_num(+) = 1                "
                  + "and m.bustype_code = bt.bustype_code(+)  ";

        ps = con.prepareStatement(qs);
        ps.setString(1,getACR().getMerchantNum());

        rs = ps.executeQuery();
        if (rs.next())
        {
          app_sic_code = rs.getString("app_sic_code");
          merch_federal_tax_id = rs.getString("merch_federal_tax_id");
          busowner_ssn = rs.getString("busowner_ssn");
          bus_name = rs.getString("bus_name");
          bustype_desc = rs.getString("bustype_desc");
          merch_average_cc_tran = rs.getString("merch_average_cc_tran");
          merch_month_tot_proj_sales = rs.getString("merch_month_tot_proj_sales");
        }

        Field field1 = new HiddenField("o_sic",app_sic_code);
        Field field2 = new HiddenField("o_fedID",merch_federal_tax_id);
        Field field3 = new HiddenField("o_ssn",busowner_ssn);
        Field field4 = new HiddenField("o_owner",bus_name);
        Field field5 = new HiddenField("o_type",bustype_desc);
        Field field6 = new HiddenField("o_tick",merch_average_cc_tran);
        Field field7 = new HiddenField("o_vol",merch_month_tot_proj_sales);

        fields.add(field1);
        fields.add(field2);
        fields.add(field3);
        fields.add(field4);
        fields.add(field5);
        fields.add(field6);
        fields.add(field7);


        log.debug("setting old values... DONE");

      }
      catch (Exception e)
      {
        log.error("setDefaults() exception: " + e);
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { ps.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }

  /**
   * Generate fields specific to this ACR.
   */
  protected void createExtendedFields()
  {
    if(!editable || acr == null)
    {
      return;
    }

    Field field;

    //SIC Code
    field = new Field("sic","SIC",40,12,true);
    fields.add(field);

    //Federal ID Number
    field = new TaxIdField("fedID","Federal ID",true);
    fields.add(field);

    //SSNumber
    field = new Field("ssn","Social Security Number",40,12,true);
    fields.add(field);

    //Ownership
    field = new Field("owner","Ownership",40,12,true);
    fields.add(field);

    //Business Type
    field = new _ddBizTypeField("type","Business Type");
    fields.add(field);

    //Average Ticket
    field = new CurrencyField("tick","Average Ticket",40,12,true);
    fields.add(field);

    //Monthly Volume
    field = new CurrencyField("vol","Monthly Volume",40,12,true);
    fields.add(field);

    //Deposit delay
    field = new _ddDelayField("delay","Deposit Delay");
    fields.add(field);

    //Add Secondary Officer
    field = new Field("secOff","Add Secondary Officer",80,12,true);
    fields.add(field);

  }

  public static class _ddDelayField extends DropDownField
  {
    public static class _ddDelayTable extends DropDownTable
    {
      public _ddDelayTable()
      {
        addElement("","select one");
        addElement("1","1");
        addElement("2","2");
        addElement("3","3");
        addElement("30","30");
        addElement("60","60");
      }

    }
    public _ddDelayField(String name, String label)
    {
      super(name,label,new _ddDelayTable(),true);
    }
  }

  public static class _ddBizTypeField extends DropDownField
  {
    public static class _ddBizTypeTable extends DropDownTable
    {
      public _ddBizTypeTable()
      {
        addElement("","select one");
        loadTable();
      }

      private void loadTable()
      {

        ResultSet rs          = null;
        PreparedStatement ps  = null;

        try
        {
          connect();

          String qs = "select                     "
                    + "bustype_code as type,      "
                    + "bustype_desc as descr      "
                    + "from bustype               "
                    + "where bustype_code < 9     "
                    + "order by bustype_desc asc  ";

          ps = con.prepareStatement(qs);
          rs = ps.executeQuery();
          while (rs.next())
          {
            addElement(rs.getString("type"),rs.getString("descr"));
          }

        }
        catch (Exception e)
        {
          log.error("loadTable() exception: " + e);
        }
        finally
        {
          try { rs.close(); } catch (Exception e) {}
          try { ps.close(); } catch (Exception e) {}
          cleanUp();
        }
      }
    }

    public _ddBizTypeField(String name, String label)
    {
      super(name,label,new _ddBizTypeTable(),true);
    }
  }

}