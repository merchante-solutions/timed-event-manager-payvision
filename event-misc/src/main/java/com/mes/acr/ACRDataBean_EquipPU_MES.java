package com.mes.acr;

import java.util.List;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.Selector;
import com.mes.tools.DropDownTable;

public class ACRDataBean_EquipPU_MES extends ACRDataBeanBase
{
  /**
   * Log4J Logger
   */
  static Logger log = Logger.getLogger(ACRDataBean_EquipPU_MES.class);

  /**
   * Factory for selector creation and the selector itself
   */
  private ACRSelectorFactory factory = new ACRSelectorFactory();
  private Selector equipSelector;

  /**
   * Standard Constructor
   */
  public ACRDataBean_EquipPU_MES()
  {
  }

  public ACRDataBean_EquipPU_MES(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }


  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT);
  }

  /**
   * Selector Generation
   * ST_EQUIPPU_CALLTAGS
   */
  public Selector getEquipmentSelector()
  {
    if (equipSelector == null)
    {
     equipSelector = factory.createSelector(factory.ST_EQUIPPU_CALLTAGS,
        getMerchantNumber());
    }
    return equipSelector;
  }

  /**
   * getSelectedEquipment()
   * provides a list of selected equipment for order generation
   * during ACRPump call
   */
  public List getSelectedEquipment()
  {
    return getEquipmentSelector().getSelected();
  }

  protected void setDefaults()
  {
    // default to rush request
    acr.setACRItem("Rush","y");
  }

  protected void createExtendedFields()
  {

    if(acr==null)
      return;

    //removal charge fields
    Field field;
    for(int i = 0; i < 4; i++)
    {
      field = new Field("rDesc_"+i, 50, 30, true);
      field.setData(getACRValue("rDesc_"+i));
      fields.add(field);
      field = new CurrencyField("rCurrFrom_"+i,10,10,true);
      field.setData(getACRValue("rCurrFrom_"+i));
      fields.add(field);
      field = new CurrencyField("rCurrTo_"+i,10,10,true);
      field.setData(getACRValue("rCurrTo_"+i));
      fields.add(field);
    }

    DropDownTable rTable= new ReturnTable();
    fields.add(new DropDownField("returnReason", rTable ,false));

    if(!editable)
      return;

    try
    {
      //add Address subbean
      add(getAddressHelper());

      //add equipment selector
      add(getEquipmentSelector());

    }
    catch(Exception e)
    {
      log.error("createExtendedFields() = " + e.toString());
      logEntry("createExtendedFields()",e.toString());
    }
  }

  private class ReturnTable extends DropDownTable
  {
    public ReturnTable()
    {
      addElement("Closure",      "Closure");
      addElement("Rental Return",   "Rental Return");
      addElement("Defective",    "Defective");
      addElement("Other (see Comments)",    "Other (use Comments)");
    }

  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT:
      case MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP:
      case MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP:
      case MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  public String getHeadingName()
  {
    return "Order Equipment Pickup from Merchant <br><font class='semiSmallGrey'>(Request Call Tag)</font>";
  }

  public String renderEquipmentHTML()
  {
    //this is hack to reveal new equipment info... to be fixed?!
    //all naming is directly related to the bean and selector item/frame/factory
    boolean hasEquip = false;
    int LIMIT = 1000;
    String item = "";
    StringBuffer sb = new StringBuffer();

    for (int i=0; i < LIMIT; i++)
    {
      item = getACRValue(CALLTAG_EQUIPMENT_NAME+"_"+i);
      if ( ! ( item.equals("") || item.equals("n") ) )
      {
        if(!hasEquip)
        {
          hasEquip = true;

          sb.append("<table cellpadding=\"1\" border=\"0\" align=\"center\" width=\"100%\" class=\"tableFillGray\" cellspacing=\"0\">");
          sb.append("<tr>");
          sb.append("<td>");
          sb.append("<table class=\"formTable\" width=\"100%\" border=\"0\" cellspacing=\"0\">");
          sb.append("  <tr>");
          sb.append("  <th colspan=5 class=\"tableColumnHead\">Equipment for Pick-up</th>");
          sb.append("  </tr>");
          sb.append("  <tr>");
          sb.append("   <td class=\"tableData\" align=\"center\">Serial Number</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Description</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Part Num</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Disposition</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Reason for Change:</td>");
          sb.append(" </tr>");
          sb.append(" <tr>");
          sb.append("   <td colspan=5 height=\"1\" bgcolor=\"#cccccc\"><img src=\"/images/spcr_transp.gif\" height=\"1\"></td>");
          sb.append("  </tr>");
        }
        sb.append("<tr>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(generateCallTagLink(getACRValue(item+"_serNum"))).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_description")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_partNum")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_disposition")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_reason")).append("</td>");
        sb.append("</tr>");
      }
    }

    //finish up
    if(hasEquip)
    {
      sb.append("</table></td></tr></table>");
    }
    else
    {
      sb.append("No equipment selected for this ACR.");
    }

    return sb.toString();
  }

  /** OLD
   * getCallTaggedEquipment and calltaggedEquipmentExists
   * both necessary to generate call tags, but implemented
   * differently depending on the required status of said tags
   */
  public Vector getCallTaggedEquipment()
  {
    return ACRUtility.processCallTaggedEquipment(getACR());
  }

  public boolean calltaggedEquipmentExists()
  {
    //by very nature - must be true
    return true;
  }

} // class ACRDataBean_EquipPU
