package com.mes.acr;

public interface ACRTranslator
{
  public void doPush(ACR acr);
  public void doPull(ACR acr);
}