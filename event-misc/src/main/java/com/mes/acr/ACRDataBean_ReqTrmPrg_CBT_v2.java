package com.mes.acr;

import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckField;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.PhoneField;
import com.mes.forms.RadioField;
import com.mes.forms.Selector;
import com.mes.forms.Validation;
import com.mes.forms.YesNoRadioField;
import com.mes.tools.DropDownTable;

public final class ACRDataBean_ReqTrmPrg_CBT_v2 extends ACRDataBeanBase
{
  static Logger log = Logger.getLogger(ACRDataBean_ReqTrmPrg_CBT_v2.class);

  public ACRDataBean_ReqTrmPrg_CBT_v2()
  {
  }

  public ACRDataBean_ReqTrmPrg_CBT_v2(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  /**
   * Returns title for this acr.
   */
  public String getHeadingName()
  {
    return "Request Terminal Programming Changes";
  }

  /**
   * Set any default values.
   */
  protected void setDefaults()
  {
  }

  private ACRSelectorFactory factory = new ACRSelectorFactory();
  private Selector equipSelector;

  /**
   * Retrieve terminal profile selector, instantiate it if it does not exist.
   */
  public Selector getEquipmentSelector()
  {
    if (equipSelector == null)
    {
     equipSelector = factory.createSelector(factory.ST_REQ_TRM_PRG,
        getMerchantNumber());
    }
    return equipSelector;
  }

  /**
   * Quick and dirty rendering of selected equipment data for post submission
   * mode.
   */
  public String renderSelectedEquipmentHtml()
  {
    //this is hack to reveal new equipment info... to be fixed?!
    //all naming is directly related to the bean and selector item/frame/factory
    boolean hasEquip = false;
    String item = "";
    StringBuffer sb = new StringBuffer();

    // this is a cut and paste of the hack for single selectors (no looping needed)

    item = getACRValue(CALLTAG_EQUIPMENT_NAME);
    if (!item.equals(""))
    {
      if(!hasEquip)
      {
        hasEquip = true;

        sb.append("<table cellpadding=\"1\" border=\"0\" align=\"center\" width=\"100%\" class=\"tableFillGray\" cellspacing=\"0\">");
        sb.append("  <tr>");
        sb.append("    <td>");
        sb.append("      <table class=\"formTable\" width=\"100%\" border=\"0\" cellspacing=\"0\">");
        sb.append("        <tr>");
        sb.append("          <th colspan=5 class=\"tableColumnHead\">Terminal Selection</th>");
        sb.append("        </tr>");
        sb.append("        <tr>");
        sb.append("          <td class=\"tableData\" align=\"center\">Description</td>");
        sb.append("          <td class=\"tableData\" align=\"center\">Serial Number</td>");
        sb.append("          <td class=\"tableData\" align=\"center\">Terminal App</td>");
        sb.append("          <td class=\"tableData\" align=\"center\">Terminal Type</td>");
        sb.append("          <td class=\"tableData\" align=\"center\">V#</td>");
        sb.append("        </tr>");
        sb.append("        <tr>");
        sb.append("          <td colspan=5 height=\"1\" bgcolor=\"#cccccc\"><img src=\"/images/spcr_transp.gif\" height=\"1\"></td>");
        sb.append("        </tr>");
      }
      sb.append(  "        <tr>");
      sb.append(  "           <td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_description")).append("</td>");
      sb.append(  "           <td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_serNum")).append("</td>");
      sb.append(  "           <td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_termApp")).append("</td>");
      sb.append(  "           <td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_termType")).append("</td>");
      sb.append(  "           <td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_vNumber")).append("</td>");
      sb.append(  "        </tr>");
    }

    //finish up
    if(hasEquip)
    {
      sb.append(  "      </table></td></tr></table>");
    }
    else
    {
      sb.append("No equipment selected for this ACR.");
    }

    return sb.toString();
  }

  public static class ChangeableFeatureField extends DropDownField
  {
    public static class ChangeableFeatureTable extends DropDownTable
    {
      public ChangeableFeatureTable()
      {
        addElement("",          "No Change");
        addElement("Disable",   "Disable");
        addElement("Enable",    "Enable");
        addElement("Change",    "Change");
      }
    }

    public static class FeatureDataValidation implements Validation
    {
      Field featureField;

      public FeatureDataValidation(Field ff)
      {
        featureField = ff;
      }

      public String getErrorText()
      {
        return "Required";
      }

      public boolean validate(String fieldData)
      {
        // if feature is marked for enable or change then
        // this field data must be present
        if (featureField.getData().startsWith("Enable") ||
            featureField.getData().startsWith("Change"))
        {
          return fieldData != null && fieldData.length() > 0;
        }
        return true;
      }
    }

    public ChangeableFeatureField(String name, String label)
    {
      super(name,label,new ChangeableFeatureTable(),true);
    }

    public void addFeatureValidationTo(Field dataField)
    {
      dataField.addValidation(new FeatureDataValidation(this));
    }
  }

  public static class StandardFeatureField extends DropDownField
  {
    public static class StandardFeatureTable extends DropDownTable
    {
      public StandardFeatureTable()
      {
        addElement("",        "No Change");
        addElement("Disable", "Disable");
        addElement("Enable",  "Enable");
      }
    }
    public StandardFeatureField(String name, String label)
    {
      super(name,label,new StandardFeatureTable(),true);
    }
  }

  public static class EnableOnlyField extends DropDownField
  {
    public static class EnableOnlyTable extends DropDownTable
    {
      public EnableOnlyTable()
      {
        addElement("",        "No Change");
        addElement("Enable",  "Enable");
      }
    }
    public EnableOnlyField(String name, String label)
    {
      super(name,label,new EnableOnlyTable(),true);
    }
  }

  public static class MerchOwnedPrinterField extends DropDownField
  {
    public static class MerchOwnedPrinterTable extends DropDownTable
    {
      public MerchOwnedPrinterTable()
      {
        addElement("","None");
        addElement("Citizen IDP 560 Roll","Citizen IDP 560 Roll");
        addElement("Citizen IDP 562 Roll","Citizen IDP 562 Roll");
        addElement("Citizen IDP 3530 Roll","Citizen IDP 3530 Roll");
        addElement("DataCard Silent Partner","DataCard Silent Partner");
        addElement("IVI (Ingenico) Scribe 612","IVI (Ingenico) Scribe 612");
        addElement("Hypercom P7 Roll","Hypercom P7 Roll");
        addElement("Hypercom P8 Roll","Hypercom P8 Roll");
        addElement("Hypercom P7 Sprocket","Hypercom P7 Sprocket");
        addElement("Hypercom P8 Sprocket","Hypercom P8 Sprocket");
        addElement("VeriFone Slip Printer 150","VeriFone Slip Printer 150");
        addElement("VeriFone Printer 220","VeriFone Printer 220");
        addElement("VeriFone Printer 250","VeriFone Printer 250");
        addElement("VeriFone Roll 300","VeriFone Roll 300");
        addElement("VeriFone PrintPak 350/355","VeriFone PrintPak 350/355");
        addElement("VeriFone Printer 600","VeriFone Printer 600");
        addElement("VeriFone Printer 900R","VeriFone Printer 900R");
      }
    }
    public MerchOwnedPrinterField(String name, String label)
    {
      super(name,label,new MerchOwnedPrinterTable(),true);
    }
  }

  public static class VendorDropField extends DropDownField
  {
    public static class VendorTable extends DropDownTable
    {
      public VendorTable()
      {
        addElement("","select one");
        addElement("Vital","Vital");
        addElement("VeriCenter","VeriCenter");
        addElement("TermMaster","TermMaster");
      }
    }
    public VendorDropField(String name, String label)
    {
      super(name,label,new VendorTable(),true);
    }
  }

  public static class _indTypeField extends DropDownField
  {
    public static class _indTypeTable extends DropDownTable
    {
      public _indTypeTable()
      {
        addElement("","select one");
        addElement("Retail","Retail");
        addElement("Direct Marketing","Direct Marketing");
      }
    }
    public _indTypeField(String name, String label)
    {
      super(name,label,new _indTypeTable(),true);
    }
  }

  public static class _sslField extends DropDownField
  {
    public static class _sslTable extends DropDownTable
    {
      public _sslTable()
      {
        addElement("","select one");
        addElement("Dial","Dial");
        addElement("SSL","SSL");
      }
    }
    public _sslField(String name, String label)
    {
      super(name,label,new _sslTable(),true);
    }
  }

  public static class _terminalTypeField extends DropDownField
  {
    public static class _terminalTypeTable extends DropDownTable
    {
      public _terminalTypeTable()
      {
        addElement("","select one");
        addElement("IP","IP");
        addElement("Dial","Dial");
      }
    }
    public _terminalTypeField(String name, String label)
    {
      super(name,label,new _terminalTypeTable(),false);
    }
  }

  public static class _pcLevelField extends DropDownField
  {
    public static class _pcLevelTable extends DropDownTable
    {
      public _pcLevelTable()
      {
        addElement("","select one");
        addElement("Level II","Level II");
        addElement("Level III","Level III");
      }
    }
    public _pcLevelField(String name, String label)
    {
      super(name,label,new _pcLevelTable(),true);
    }
  }


  public static class _networkField extends DropDownField
  {
    public static class _networkTable extends DropDownTable
    {
      public _networkTable()
      {
        addElement("","select one");
        addElement("Stand Alone","Stand Alone");
        addElement("Network","Network");
      }
    }
    public _networkField(String name, String label)
    {
      super(name,label,new _networkTable(),true);
    }
  }

  public static class _acctStatusField extends DropDownField
  {
    public static class _acctStatusTable extends DropDownTable
    {
      public _acctStatusTable()
      {
        addElement("","select one");
        addElement("Existing","Existing");
        addElement("New","New");
      }
    }
    public _acctStatusField(String name, String label)
    {
      super(name,label,new _acctStatusTable(),true);
    }
  }

  public static class _softwareField extends DropDownField
  {
    public static class _softwareTable extends DropDownTable
    {
      public _softwareTable()
      {
        addElement("","select one");
        addElement("Pro","Pro");
        addElement("Link","Link");
      }
    }
    public _softwareField(String name, String label)
    {
      super(name,label,new _softwareTable(),true);
    }
  }

  /**
   * Generate fields specific to this ACR.
   */
  protected void createExtendedFields()
  {
    if(!editable || acr == null)
    {
      return;
    }

    try
    {
      // shipping address component
      add(getAddressHelper());

      // terminal profile selector
      add(getEquipmentSelector());

      // wireless terminal id
      Field wirelessLliEsn = new Field("wirelessLliEsn","Wireless LLI/ESN",25,12,true);
      fields.add(wirelessLliEsn);

      // terminal accessories to ship
      Field sendQrg
        = new YesNoRadioField("sendQrg","Send Quick Reference Guide");
      fields.add(sendQrg);
      Field sendOverlay
        = new YesNoRadioField("sendOverlay","Send Overlay");
      fields.add(sendOverlay);
      Field sendKit
        = new YesNoRadioField("sendKit","Send Kit");
      fields.add(sendKit);
      Field sendPlates
        = new YesNoRadioField("sendPlates","Send Plates");
      fields.add(sendPlates);


      // make address picker required if accessories asked for
      Field addressPicker = fields.getField("addressPicker");
      addressPicker.addValidation(new RequiredIfYesValidation(sendQrg,
        "Required for terminal accessory shipping"));
      addressPicker.addValidation(new RequiredIfYesValidation(sendOverlay,
        "Required for terminal accessory shipping"));

      // reliable address picker to shipping address for error display
      addressPicker.setLabel("Shipping Address");

      //terminal type
      RadioField termType =
        new RadioField("termType",
                       "termType",
                       new String[][]{{ "Dial Terminal", "Dial Terminal" }, { "Software", "Software" }}
                      );
      fields.add(termType);

      CheckboxField pcCharge = new CheckboxField("pcCharge","PC Charge",false);
      fields.add(pcCharge);

      //POS Partner
      CheckboxField posPartner = new CheckboxField("posPartner","POS Partner",false);
      Validation posVal = new RequiredIfYesValidation(posPartner, "Required for POS Partner");
      fields.add(posPartner);

      Field opSystem = new Field("opSystem","Operating System",25,12,true);
      opSystem.addValidation(posVal);
      fields.add(opSystem);

      DropDownField indType = new _indTypeField("indType","Industry Type");
      indType.addValidation(posVal);
      fields.add(indType);

      DropDownField dialssl = new _sslField("dialSSL","Dial/SSL");
      dialssl.addValidation(posVal);
      fields.add(dialssl);

      DropDownField pcLevel = new _pcLevelField("pcLevel","Purchase Card Level");
      pcLevel.addValidation(posVal);
      fields.add(pcLevel);

      DropDownField network = new _networkField("network","Stand Alone/Network");
      network.addValidation(posVal);
      fields.add(network);

      DropDownField terminalType = new _terminalTypeField("terminalType","Terminal Type");
      terminalType.addValidation(posVal);
      fields.add(terminalType);

      //Verisign
      CheckboxField verisign = new CheckboxField("verisign","Verisign",false);
      Validation verisignVal = new RequiredIfYesValidation(verisign, "Required for Verisign");
      fields.add(verisign);

      DropDownField acctStatus = new _acctStatusField("acctStatus","Account Status");
      acctStatus.addValidation(verisignVal);
      fields.add(acctStatus);

      DropDownField software = new _softwareField("software","Software Type");
      software.addValidation(verisignVal);
      fields.add(software);

      Field email = new Field("email","Email",25,12,true);
      email.addValidation(verisignVal);
      fields.add(email);

      Field url = new Field("url","URL",25,12,true);
      url.addValidation(verisignVal);
      fields.add(url);

      // terminal features

      //overall copy
      CheckboxField copyFeatures = new CheckboxField("copyProfile","Copy Profile",false);
      fields.add(copyFeatures);
      Field midToCopy = new Field("midToCopy","Merchant ID",20,20,true);
      midToCopy.addValidation(new RequiredIfYesValidation(copyFeatures,
        "Required for terminal profile copying"));
      fields.add(midToCopy);

      // access code (pabx)
      ChangeableFeatureField accessCodeFeat =
        new ChangeableFeatureField("accessCodeFeat","Access Code (PABX) Feature");
      fields.add(accessCodeFeat);
      Field newAccessCode = new Field("newAccessCode","New Access Code",10,4,true);
      fields.add(newAccessCode);
      accessCodeFeat.addFeatureValidationTo(newAccessCode);

      // batch auto close
      ChangeableFeatureField autoCloseFeat =
        new ChangeableFeatureField("autoCloseFeat","Auto Batch Close Feature");
      fields.add(autoCloseFeat);
      Field newAutoCloseTime =
        new Field("newAutoCloseTime","New Auto Close Time",5,4,true);
      fields.add(newAutoCloseTime);
      autoCloseFeat.addFeatureValidationTo(newAutoCloseTime);

      // receipt line 4
      ChangeableFeatureField receiptLine4Feat =
        new ChangeableFeatureField("receiptLine4Feat","Receipt Header Line 4 Feature");
      fields.add(receiptLine4Feat);
      Field newReceiptLine4 =
        new Field("newReceiptLine4","New Receipt Header Line 4",20,13,true);
      fields.add(newReceiptLine4);
      receiptLine4Feat.addFeatureValidationTo(newReceiptLine4);

      // receipt line 5
      ChangeableFeatureField receiptLine5Feat =
        new ChangeableFeatureField("receiptLine5Feat","Receipt Header Line 5 Feature");
      fields.add(receiptLine5Feat);
      Field newReceiptLine5 =
        new Field("newReceiptLine5","New Receipt Header Line 5",20,13,true);
      fields.add(newReceiptLine5);
      receiptLine5Feat.addFeatureValidationTo(newReceiptLine5);

      // receipt footer
      ChangeableFeatureField receiptFooterFeat =
        new ChangeableFeatureField("receiptFooterFeat","Receipt Footer Feature");
      fields.add(receiptFooterFeat);
      Field newReceiptFooter =
        new Field("newReceiptFooter","New Receipt Footer",20,13,true);
      fields.add(newReceiptFooter);
      receiptFooterFeat.addFeatureValidationTo(newReceiptFooter);

      // customer service phone no.
      ChangeableFeatureField custServicePhoneFeat =
        new ChangeableFeatureField("custServicePhoneFeat","Customer Service Phone No. Feature");
      fields.add(custServicePhoneFeat);
      Field newCustServicePhone =
        new PhoneField("newCustServicePhone","New Customer Service Phone No.",true);
      fields.add(newCustServicePhone);
      custServicePhoneFeat.addFeatureValidationTo(newCustServicePhone);

      // password protection
      ChangeableFeatureField passwordProtectionFeat =
        new ChangeableFeatureField("passwordProtectionFeat","Password Protection Feature");
      fields.add(passwordProtectionFeat);

      FieldGroup passFields = new FieldGroup("passFields");

      passFields.add(new Field("newPassword","New Password",4,4,true));
      passFields.add(new CheckField("authOnlyPass", "Auth Only Password", "Auth Only",  "y",false));
      passFields.add(new CheckField("batchPass",    "Batch Password",     "Batch",      "y",false));
      passFields.add(new CheckField("creditPass",   "Credit Password",    "Credit",     "y",false));
      passFields.add(new CheckField("keyboardPass", "Keyboard Password",  "Keyboard",   "y",false));
      passFields.add(new CheckField("voidPass",     "Void Password",      "Void",       "y",false));

      // who knows what kind of validation they are imagining for these
      // fields, so for now they are not validated...
      fields.add(passFields);

      // on/off type features
      fields.add(new StandardFeatureField("tipFlagFeat","Tips Flag Feature"));
      fields.add(new StandardFeatureField("localProgrammingFeat","Local Programming"));
      fields.add(new StandardFeatureField("resetTranNumFeat","Reset Transaction No. Daily Feature"));
      fields.add(new StandardFeatureField("invoiceNumFeat","Invoice No. Prompt Feature"));
      fields.add(new StandardFeatureField("purchasingCardFeat","Purchasing Card Flag Feature"));
      fields.add(new StandardFeatureField("avsFlagFeat","AVS Flag Feature"));
      fields.add(new StandardFeatureField("clerkServerPrompt","Clerk Server Prompt"));

      // turn on only features
      fields.add(new EnableOnlyField("cvv2FlagFeat","CVV2 Flag Feature"));
      //fields.add(new EnableOnlyField("cardTruncationFeat","Receipt Card Truncation Feature"));

      // merchant owned printer
      fields.add(new MerchOwnedPrinterField("merchOwnedPrinter","Merchant Owned Printer"));

      fields.setHtmlExtra("class=\"formText\"");
    }
    catch (Exception e)
    {
      log.error("createExtendedFields(): "  + e);
    }
  }

  /**
   * Define legal queue operations.
   */
  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}

//ASG Mgmt queue
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}

      //TSYS audit queue
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}

      //POS audit queue
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}


      //Help CBT mgmt
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}

      //ASG CBT mgmt
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}

    };
  }

  /**
   * Disable editable comments during submission.
   */
  //public boolean hasEditableComments()
  //{
  //  return !editable && super.hasEditableComments();
  //}

  /**
   * Returns true if the indicated acr item has data associated with it.
   */
  public boolean itemPresent(String featureName)
  {
    String itemData = getACRValue(featureName);
    return itemData != null && itemData.length() > 0 && !(itemData.equals("n"));
  }
}
