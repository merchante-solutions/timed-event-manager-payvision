package com.mes.acr;

import com.mes.forms.SelectorColumnDef;

public class SF_ReqTrmPrg extends SF_EquipBase
{
  public SF_ReqTrmPrg(String title)
  {
    super(title);
  }

  public void setupFrame()
  {
    addColumn(new SelectorColumnDef("col1","Description",   180));
    addColumn(new SelectorColumnDef("col2","Serial No.",    100));
    addColumn(new SelectorColumnDef("col3","Terminal App",  100));
    addColumn(new SelectorColumnDef("col4","Terminal Type", 180));
    addColumn(new SelectorColumnDef("col5","V#/TID#",       100));
  }
}
