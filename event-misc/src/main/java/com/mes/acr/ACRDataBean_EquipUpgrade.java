/*@lineinfo:filename=ACRDataBean_EquipUpgrade*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.acr;

import java.sql.ResultSet;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;
import com.mes.support.StringUtilities;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class ACRDataBean_EquipUpgrade extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_EquipUpgrade.class);


  // constants
  // (NONE)

  // data members
  ACRDataBeanAddressHelper adrsHelper = null;

  // construction
  public ACRDataBean_EquipUpgrade()
  {
  }

  public ACRDataBean_EquipUpgrade(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  protected ACRDataBeanAddressHelper _getAddressHelper()
  {
    if(adrsHelper!=null)
      return adrsHelper; // already configured by virtue of non-null obj ref

    adrsHelper = new ACRDataBeanAddressHelper(this);

    // configure the address helper
    adrsHelper.setAdrsTypeName("Ship To");
    adrsHelper.setAdrsSlctnFldName("staddress");
    adrsHelper.setAdrsNameFldNme("sh_name");
    adrsHelper.setAdrsAddressFldNme("sh_address");
    adrsHelper.setAdrsAttnFldNme("sh_attn");
    adrsHelper.setAdrsBankNameFldNme("sh_bnknme");
    adrsHelper.setAdrsCityFldNme("sh_city");
    adrsHelper.setAdrsStateFldNme("sh_state");
    adrsHelper.setAdrsZipFldNme("sh_zip");
    //adrsHelper.setAdrsCountry("sh_country");
    adrsHelper.setIsRequired(true);

    return adrsHelper;
  }

  public String renderAddressHtml()
  {
    return _getAddressHelper().renderAddressHtml();
  }

  public void doPull()
  {
    super.doPull();

    if(!editable)
      return;

    _getAddressHelper().pullAddressFields();
  }

  protected void setDefaults()
  {
    // parent related info
    if(getMerchantNumber().length()>0) {

      // merchant address
      _getAddressHelper().setACRAdrsInfo();
    }

  }

  protected void createExtendedFields()
  {
    if(acr==null)
      return;

    Field field=null;
    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    String acrdiname=null;

    int rq = getResidentQueue();
    log.debug("rq="+rq);

    if(rq==MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP) {
      if((acrdi=acrd.getACRDefItem("exist_vnum"))!=null)
        fields.add(ACRDefItemToField(acrdi));
      if((acrdi=acrd.getACRDefItem("exist_appid"))!=null)
        fields.add(ACRDefItemToField(acrdi));
    }

    if(!editable)
      return;

    // address fields
    _getAddressHelper().createAddressFields();

    int merchBankNum = StringUtilities.stringToInt(getACRValue("Merchant Bank Number"),-1);
    boolean isTranscomMerchant = (getMerchantNumber().startsWith("4903"));

    if((acrdi=acrd.getACRDefItem("replace_equip"))!=null) {
      field=new CheckboxField(acrdi.getName(),"Click here if purpose of order is to use new equipment in place of existing equipment (upgrade).",false);
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("add_equip"))!=null) {
      field=new CheckboxField(acrdi.getName(),"Click here if purpose or order is to add equipment.",false);
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("add_refab"))!=null) {
      field=new CheckboxField(acrdi.getName(),"Click here if purpose or order is to add new or refurbished equipment.",false);
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("return_equip"))!=null) {
      field=new CheckboxField(acrdi.getName(),"Click here if merchant is renting equipment that needs to be returned and a call tag needs to be sent.",false);
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }

    // new equipment info

    if((acrdi=acrd.getACRDefItem("newequip_term_app"))!=null) {
      TerminalApplicationDropDownTable ddt = new TerminalApplicationDropDownTable();
      field=new DropDownField(acrdi.getName(),ddt,!acrdi.isRequired());
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("newequip_desc"))!=null) {
      NewEquipmentDropDownTable ddt = new NewEquipmentDropDownTable();
      field=new DropDownField(acrdi.getName(),ddt,!acrdi.isRequired());
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("newequip_purcorrental"))!=null) {
      String[][] arr = null;
      if(merchBankNum==3858 || merchBankNum==3860 || isTranscomMerchant)
        arr = new String[][]
        {
           {"Purchase","Purchase"}
          ,{"Rental","Rental"}
        };
      else
        arr = new String[][]
        {
           {"Purchase","Purchase"}
          ,{"Rental","Rental"}
          ,{"New","New"}
          ,{"Refurbished","Refurbished"}
        };

      field = new RadioButtonField(acrdi.getName(),arr, -1, !acrdi.isRequired(), "Please specify whether the new equipment item is a purchase or rental." );
      field.setShowName(acrdi.getLabel());
      ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("newequip_qty"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("newequip_peritemcost"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("newequip_itemsalestax"))!=null)
      fields.add(ACRDefItemToField(acrdi));

    // existing equipment info
    if((acrdi=acrd.getACRDefItem("exist_vnum"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("exist_appid"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("exist_isleased"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("exist_leasecompany"))!=null) {
      LeaseCompanyDropDownTable ddt = new LeaseCompanyDropDownTable();
      field=new DropDownField(acrdi.getName(),ddt,!acrdi.isRequired());
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("exist_sendcalltag"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("exist_equiptype"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("exist_serialnum"))!=null)
      fields.add(ACRDefItemToField(acrdi));

    // equipment lease info (show only for Bank 3941)
    if(merchBankNum==3941) {
      if((acrdi=acrd.getACRDefItem("exist_leaseauthnum"))!=null)
        fields.add(ACRDefItemToField(acrdi));
      if((acrdi=acrd.getACRDefItem("exist_mnthleaseamt"))!=null)
        fields.add(ACRDefItemToField(acrdi));
      if((acrdi=acrd.getACRDefItem("exist_leaseaccnum"))!=null)
        fields.add(ACRDefItemToField(acrdi));
      if((acrdi=acrd.getACRDefItem("exist_leasenummonths"))!=null) {
        LeaseMonthsDropDownTable ddt = new LeaseMonthsDropDownTable();
        field=new DropDownField(acrdi.getName(),ddt,!acrdi.isRequired());
        field.setShowName(acrdi.getLabel());
        fields.add(field);
      }
      if((acrdi=acrd.getACRDefItem("exist_leasefunding"))!=null)
        fields.add(ACRDefItemToField(acrdi));
    }

    // terminal features
    if((acrdi=acrd.getACRDefItem("tf_accesscode"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_accesscode_code"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_autobatchclose"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_autobatchclose_hh"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_autobatchclose_mm"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_autobatchclose_ampm"))!=null) {
      String[][] arr = new String[][]
      {
         {"AM","AM"}
        ,{"PM","PM"}
      };
      field = new RadioButtonField(acrdi.getName(),arr, -1, !acrdi.isRequired(), "Please specify AM or PM for the Auto Batch Close terminal feature." );
      field.setShowName(acrdi.getLabel());
      ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("tf_rcpthdrl4"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_rcpthdrl4_desc"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_rcpthdrl5"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_rcpthdrl5_desc"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_rcptftr"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_rcptftr_desc"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_resettransnumdaily"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_invnumprompton"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_fraudcntrlon"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_pswdprotecton"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_purchcardflagon"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_avs"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_crdtruncoff"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_cstsvcphone"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_cstsvcphonenum"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_cashbackwdebit"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_cashbacklimit"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_debitsurcharge"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_debitsurcharge_limit"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_trmrmndrtct"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_trmrmndrtct_hh"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_trmrmndrtct_mm"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_trmrmndrtct_ampm"))!=null) {
      String[][] arr = new String[][]
      {
         {"AM","AM"}
        ,{"PM","PM"}
      };
      field = new RadioButtonField(acrdi.getName(),arr, -1, !acrdi.isRequired(), "Please specify AM or PM for the Terminal Reminder terminal feature." );
      field.setShowName(acrdi.getLabel());
      ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("tf_tipoptnon"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_cspo"))!=null)
      fields.add(ACRDefItemToField(acrdi));

    // validation
    fields.addValidation(new EntryValidation());
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}

      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP:
      case MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT:
      case MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  protected final class EntryValidation implements Validation
  {
    protected String ErrorText = "";

    public EntryValidation()
    {
    }

    public String getErrorText()
    {
      return(ErrorText);
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      // allow only one type of equip trans type checkbox to be selected
      int cnt=0;
      Field fld1 = fields.getField("add_refab");
      Field fld2 = fields.getField("replace_equip");
      Field fld3 = fields.getField("add_equip");
      Field fld4 = fields.getField("return_equip");

      if(fld1!=null && !fld1.isBlank())
        cnt++;
      if(fld2!=null && !fld2.isBlank())
        cnt++;
      if(fld3!=null && !fld3.isBlank())
        cnt++;
      if(fld4!=null && !fld4.isBlank())
        cnt++;

      if(cnt>1) {
        ErrorText="Only one equipment transaction type checkbox may be selected.";
        return false;
      } else if(cnt<1) {
        ErrorText="An equipment transaction type checkbox must be selected.";
        return false;
      }

      // require equip type(desc) and serial num be required if send call tag specified
      fld1 = fields.getField("exist_sendcalltag");
      if(((CheckboxField)fld1).isChecked()) {
        fld1=fields.getField("exist_equiptype");
        fld2=fields.getField("exist_serialnum");

        if(fld1.isBlank() || fld2.isBlank()) {
          ErrorText = "Existing equipment 'Equipment Type' and 'Serial Number' must be specified when the Send Call Tag checkbox is checked.";
          return false;
        }
      }

      return true;
    }

  } // EntryValidation

  public String getHeadingName()
  {
    return "Order Additional or Upgraded Equipment for Merchant";
  }

  public final class NewEquipmentDropDownTable extends DropDownTable
  {
    public NewEquipmentDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      addElement("","");

      ResultSetIterator   it      = null;
      ResultSet           rs      = null;

      try {

        connect();

        int appTypeCode,cnt;

        appTypeCode=StringUtilities.stringToInt(getACRValue("Type Code"),-1);

        // revert to mes equipment if merchant app type renders no allowed equip
        if(appTypeCode>-1) {
          /*@lineinfo:generated-code*//*@lineinfo:458^11*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//              
//              from equipment_application
//              where app_type=:appTypeCode
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n             \n            from equipment_application\n            where app_type= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRDataBean_EquipUpgrade",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appTypeCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:464^11*/
        } else
          cnt=0;

        if(cnt<1) {
          log.info("Reverting to MES equipment: No equipment available for app type code '"+appTypeCode+"'.");
          appTypeCode=0;
        }

        /*@lineinfo:generated-code*//*@lineinfo:473^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    equip_description
//            from      equipment_application ea
//            where     ea.app_type=:appTypeCode
//            order by  equip_description
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    equip_description\n          from      equipment_application ea\n          where     ea.app_type= :1 \n          order by  equip_description";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.acr.ACRDataBean_EquipUpgrade",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appTypeCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.acr.ACRDataBean_EquipUpgrade",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:479^9*/

        rs = it.getResultSet();

        String val,desc;

        while (rs.next()) {
          val = rs.getString("equip_description");
          desc = val;
          addElement(val,desc);
        }

        it.close();
      }
      catch (Exception e) {
        log.error("NewEquipmentDropDownTable.getData() EXCEPTION: '"+e.getMessage()+"'.");
      }
      finally {
        cleanUp();
      }

    }

  } // NewEquipmentDropDownTable class

  public class LeaseMonthsDropDownTable extends DropDownTable
  {
    public LeaseMonthsDropDownTable()
    {
      addElement("","");

      addElement("12 Months","12 Months");
      addElement("24 Months","24 Months");
      addElement("36 Months","36 Months");
      addElement("48 Months","48 Months");
    }

  } // LeaseMonthsDropDownTable class

  public class TerminalApplicationDropDownTable extends DropDownTable
  {
    public TerminalApplicationDropDownTable()
    {
      addElement("","");

      addElement("Retail","Retail");
      addElement("Restaurant","Restaurant");
      addElement("Fine Dining","Fine Dining");
      addElement("Lodging","Lodging");
      addElement("Cash Advance","Cash Advance");
    }

  } // TerminalApplicationDropDownTable class

  public class LeaseCompanyDropDownTable extends DropDownTable
  {
    public LeaseCompanyDropDownTable()
    {
      addElement("","");

      addElement("Lease Comm","Lease Comm");
      addElement("Golden Eagle Leasing","Golden Eagle Leasing");
    }

  } // LeaseCompanyDropDownTable class

}/*@lineinfo:generated-code*/