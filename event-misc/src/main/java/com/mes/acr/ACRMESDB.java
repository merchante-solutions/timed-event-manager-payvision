/*@lineinfo:filename=ACRMESDB*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/acr/ACRMESDB.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-01 11:46:29 -0700 (Tue, 01 Jul 2008) $
  Version            : $Revision: 15021 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.BitSet;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Logger;
//import com.mes.user.UserAppType;
import com.mes.constants.MesHierarchy;
import com.mes.supply.SupplyOrder;
import com.mes.supply.SupplyOrderItem;
import sqlj.runtime.ResultSetIterator;

/**
 * ACRMESDB class.
 *
 * Performs all interfacing to the MES db for ACR and ACRDefinition objects and the like.
 * SINGLETON.
 *
 * DB structure:
 * -------------
 *  ACRDEF     <1(ACRDEF_SEQ_NUM)--(ACRDID)M>    ACRDEFITEM
 *  ACR        <1(ACR_SEQ_NUM)--(ACRID)M>        ACRITEM
 *  ACR        <1(ACRDEFID)--(ACRDEF_SEQ_NUM)1>  ACRDEF
 *  ACRITEM    <1(NAME)--(NAME)>                 ACRDEFITEM    (weak link)
 *
 *  ACRDEFINITION         Contains Acc. Chng. Req. definitions
 *  ACRDEFITEM            Contains items constituting an ACR definition
 *  ACR                   Contains the actual Acc. Chng. Request records
 *  ACRITEM               Contains items assoc. with a particular Acc. Chng. Request item
 *
 */
public class ACRMESDB extends com.mes.database.SQLJConnectionBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRMESDB.class);

  // constants

  // db component types
  public static final int     DBOCMPNT_ACR              = 1;
  public static final int     DBOCMPNT_ACRDEF           = 2;

  // constituent ACR/ACRDefinition component types as bits
  public static final int     BIT_ACR_PRIMARY           = 0;
  public static final int     BIT_ACR_ITEM              = 1;
  public static final int     BIT_ACRDEF_PRIMARY        = 2;
  public static final int     BIT_ACRDEF_ITEM           = 3;


  // data members
  // (NONE)

  // construction
  public ACRMESDB()
  {
    super(false);
  }

  // class methods
  // (none)

  // object methods

  /*
  public String getMerchantNumFromAppSeqNum(long appSeqNum)
  {
    try {

      connect();

      String merchNum;

      #sql [Ctx]
      {
        SELECT  MERCH_NUMBER
        INTO    :(merchNum)
        FROM    MERCHANT
        WHERE   APP_SEQ_NUM=:(appSeqNum)
      };

      return merchNum;
    }
    catch(Exception e) {
      return "";
    }
    finally {
      cleanUp();
    }
  }
  */

  public long getAppSeqNumFromMerchantNum(String merchNum)
  {
    try {

      connect();

      long appSeqNum;

      /*@lineinfo:generated-code*//*@lineinfo:134^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT  APP_SEQ_NUM
//          
//          FROM    MERCHANT
//          WHERE   MERCH_NUMBER=:merchNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  APP_SEQ_NUM\n         \n        FROM    MERCHANT\n        WHERE   MERCH_NUMBER= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:140^7*/

      return appSeqNum;
    }
    catch(Exception e) {
      return -1L;
    }
    finally {
      cleanUp();
    }
  }

  /**
   * getNextAvailID()
   *
   * Retreives the next available Primary key for the specified.
   */
  public long getNextAvailID(int acrBit)
  {
    try {

      connect();

      long id=-1L;

      if (acrBit==BIT_ACR_PRIMARY
          || acrBit==BIT_ACR_ITEM
          || acrBit==BIT_ACRDEF_PRIMARY
          || acrBit==BIT_ACRDEF_ITEM)
      {
        /*@lineinfo:generated-code*//*@lineinfo:170^9*/

//  ************************************************************
//  #sql [Ctx] { SELECT  ACR_SEQUENCE.nextval
//            
//            FROM    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  ACR_SEQUENCE.nextval\n           \n          FROM    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.acr.ACRMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   id = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:175^9*/
      } else
        throw new Exception("Unhandled acr bit: '"+acrBit+"'.");

      return id;
    }
    catch(Exception e) {
      log.error("ACRMESDB.getNextAvailID() EXCEPTION: '"+e.getMessage()+"'.");
      return -1L;
    }
    finally {
      cleanUp();
    }
  }

  // INSERT
  public long insert(ACR acr)
  {
    log.error("Currently ACRMESDB.insert(ACR) NOT implemented.");
    return -1L;
  }

  public long insert(ACRDefinition acrd)
  {
    log.error("Currently ACRMESDB.insert(ACRDefinition) NOT implemented.");
    return -1L;
  }

  // DELETE

  /**
   * delete()
   *
   * Deletes db records based on the specified primary key
   *
   * @param       dbBits        Bit flags used to determine which type of atomic unit to delete
   * @param       pkid          Primary key
   */
  public boolean delete(BitSet dbBits,long pkid)
  {
    try {

      connect();

      if (dbBits.get(BIT_ACR_PRIMARY)) {
        // IMPT: Related many items assumed to be deleted at db level
        //       by way of cascade delete constraints
        /*@lineinfo:generated-code*//*@lineinfo:222^9*/

//  ************************************************************
//  #sql [Ctx] { DELETE
//  
//            FROM
//                    ACR
//            WHERE
//                    ACR_SEQ_NUM=:pkid
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "DELETE\n\n          FROM\n                  ACR\n          WHERE\n                  ACR_SEQ_NUM= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:230^9*/
      } else if (dbBits.get(BIT_ACR_ITEM)) {
        /*@lineinfo:generated-code*//*@lineinfo:232^9*/

//  ************************************************************
//  #sql [Ctx] { DELETE
//  
//            FROM
//                    ACRITEM
//            WHERE
//                    ACRI_SEQ_NUM=:pkid
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "DELETE\n\n          FROM\n                  ACRITEM\n          WHERE\n                  ACRI_SEQ_NUM= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:240^9*/
      } else if (dbBits.get(BIT_ACRDEF_PRIMARY)) {
        // IMPT: Related many items assumed to be deleted at db level
        //       by way of cascade delete constraints
        /*@lineinfo:generated-code*//*@lineinfo:244^9*/

//  ************************************************************
//  #sql [Ctx] { DELETE
//  
//            FROM
//                    ACRDEF
//            WHERE
//                    ACRDEF_SEQ_NUM=:pkid
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "DELETE\n\n          FROM\n                  ACRDEF\n          WHERE\n                  ACRDEF_SEQ_NUM= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:252^9*/
      } else if (dbBits.get(BIT_ACRDEF_ITEM)) {
        /*@lineinfo:generated-code*//*@lineinfo:254^9*/

//  ************************************************************
//  #sql [Ctx] { DELETE
//  
//            FROM
//                    ACRDEFITEM
//            WHERE
//                    ACRDI_SEQ_NUM=:pkid
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "DELETE\n\n          FROM\n                  ACRDEFITEM\n          WHERE\n                  ACRDI_SEQ_NUM= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:262^9*/
      }

      // commit the db trans
      commit();

      return true;
    }
    catch(Exception e) {
      log.error("ACRMESDB.delete() (dbBits: '"+dbBits+"') EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
    finally {
      cleanUp();
    }
  }

  // SELECT

  /**
   * load() - ACRDefinition - Single
   *
   * SELECTs ACRDefinition by primary key
   *
   * @param   pkid    primary key of target to load
   * @param   acrd    FILLED IN: object to receive found data.
   * @param   dbBits  Bit Flags: 1..N of BIT_{...} flags.
   *                  Discriminates which atomic data units to affect.
   *
   * @Exception       Throws exception upon error attempting to perform load op.
   */
  public boolean load(long pkid,ACRDefinition acrd,BitSet dbBits)
    throws Exception
  {
    //log.debug("load(ACRDefinition) - START");

    boolean bFound=false;
    ResultSet rs=null;
    ResultSetIterator rsItr=null;

    try {

      connect();

      // retreive acrd primary?
      if(dbBits.get(BIT_ACRDEF_PRIMARY)) {

        /*@lineinfo:generated-code*//*@lineinfo:309^9*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                    :pkid ID_ACRDEF
//                    ,ACRDEF.NAME
//                    ,ACRDEF.SHORT_NAME
//                    ,ACRDEF.DESCRIPTION
//                    ,ACRDEF.TYPE
//                    ,ACRDEF.IS_ON
//  
//                    ,acrdef.bean_class
//                    ,acrdef.rights
//                    ,acrdef.jsp_include
//                    ,acrdef.init_to_mgmt_q
//  
//            FROM
//                    ACRDEF
//            WHERE
//                    ACRDEF.ACRDEF_SEQ_NUM=:pkid
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                   :1  ID_ACRDEF\n                  ,ACRDEF.NAME\n                  ,ACRDEF.SHORT_NAME\n                  ,ACRDEF.DESCRIPTION\n                  ,ACRDEF.TYPE\n                  ,ACRDEF.IS_ON\n\n                  ,acrdef.bean_class\n                  ,acrdef.rights\n                  ,acrdef.jsp_include\n                  ,acrdef.init_to_mgmt_q\n\n          FROM\n                  ACRDEF\n          WHERE\n                  ACRDEF.ACRDEF_SEQ_NUM= :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
   __sJT_st.setLong(2,pkid);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.acr.ACRMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:328^9*/

        rs = rsItr.getResultSet();

        if(!rs.next())
          throw new Exception("ACR Definition record '"+pkid+"' not found.");
        else if(!dbRecToObject(rs,acrd))
          throw new Exception("Load ACR '"+pkid+"' FAILED.");

        bFound=true;

        rs.close();
        rsItr.close();

      }

      // retreive acr def items?
      if(dbBits.get(BIT_ACRDEF_ITEM)) {

        /*@lineinfo:generated-code*//*@lineinfo:347^9*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                    ACRDEFITEM.ACRDI_SEQ_NUM ID_ACRDEFITEM
//                    ,ACRDEFITEM.NAME DEFITEM_NAME
//                    ,ACRDEFITEM.LABEL DEFITEM_LABEL
//                    ,ACRDEFITEM.DESCRIPTION DEFITEM_DESCRIPTION
//                    ,ACRDEFITEM.DATATYPE DEFITEM_DATATYPE
//                    ,ACRDEFITEM.ISREQUIRED DEFITEM_ISREQUIRED
//                    ,ACRDEFITEM.DEFAULTVALUE DEFITEM_DEFAULTVALUE
//                    ,ACRDEFITEM.VALIDATIONPARAMS DEFITEM_VALIDATIONPARAMS
//            FROM
//                    ACRDEFITEM
//            WHERE
//                    ACRDEFITEM.ACRDID=:pkid
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                  ACRDEFITEM.ACRDI_SEQ_NUM ID_ACRDEFITEM\n                  ,ACRDEFITEM.NAME DEFITEM_NAME\n                  ,ACRDEFITEM.LABEL DEFITEM_LABEL\n                  ,ACRDEFITEM.DESCRIPTION DEFITEM_DESCRIPTION\n                  ,ACRDEFITEM.DATATYPE DEFITEM_DATATYPE\n                  ,ACRDEFITEM.ISREQUIRED DEFITEM_ISREQUIRED\n                  ,ACRDEFITEM.DEFAULTVALUE DEFITEM_DEFAULTVALUE\n                  ,ACRDEFITEM.VALIDATIONPARAMS DEFITEM_VALIDATIONPARAMS\n          FROM\n                  ACRDEFITEM\n          WHERE\n                  ACRDEFITEM.ACRDID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.acr.ACRMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:362^9*/

        rs = rsItr.getResultSet();

        int numACRDefItemsLoaded = 0;
        ACRDefItem acrdi = null;

        while(rs.next()) {
          acrdi = new ACRDefItem();
          if(!dbRecToObject(rs,acrdi))
            throw new Exception("Load ACR Definition Item '"+rs.getInt(1)+"' FAILED.");
          else {
            try {
              acrd.setDefItem(acrdi);
              numACRDefItemsLoaded++;
            }
            catch(Exception e) {
              log.error("Unable to attach ACR Def Item to ACR Definition '"+acrd.getID()+"'.  Continuing.");
            }
          }
        }
        //log.debug(numACRDefItemsLoaded+" ACR Definition Items loaded for ACR Definition '"+pkid+"'.");

        rs.close();
        rsItr.close();
      }

    }
    catch(Exception e) {
      log.error("ACRMESDB.load(ACRDefinition) EXCEPTION: '"+e.getMessage()+"'.");
      throw e;
    }
    finally {
      try { rs.close(); }    catch(Exception e) {}
      try { rsItr.close(); } catch(Exception e) {}
      cleanUp();
    }

    return bFound;
  }

  /**
   * load() - ACR - Single
   *
   * SELECTs ACR by primary key
   *
   * @param   pkid    primary key of target to load
   * @param   acr     FILLED IN: object to receive found data.
   * @param   dbBits  Bit Flags: 1..N of BIT_{...} flags.
   *                  Determines which constituent data units to affect.
   *
   * @Exception       Throws exception upon error attempting to perform load op.
   */
  public boolean load(long pkid,ACR acr,BitSet dbBits)
    throws Exception
  {
    boolean bFound=false;
    ResultSet rs=null;
    ResultSetIterator rsItr=null;
    String str=null;

    try {

      connect();

      // retreive acr primary?
      if(dbBits.get(BIT_ACR_PRIMARY)) {
        /*@lineinfo:generated-code*//*@lineinfo:429^9*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                    :pkid ID_ACR
//                    ,ACR.MERCH_NUMBER
//                    ,ACR.APP_SEQ_NUM
//                    ,ACR.ACRDEFID
//                    ,ACR.STATUS
//                    ,ACR.DATE_CREATED
//                    ,ACR.DATE_LAST_MODIFIED
//            FROM
//                    ACR
//            WHERE
//                    ACR.ACR_SEQ_NUM=:pkid
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                   :1  ID_ACR\n                  ,ACR.MERCH_NUMBER\n                  ,ACR.APP_SEQ_NUM\n                  ,ACR.ACRDEFID\n                  ,ACR.STATUS\n                  ,ACR.DATE_CREATED\n                  ,ACR.DATE_LAST_MODIFIED\n          FROM\n                  ACR\n          WHERE\n                  ACR.ACR_SEQ_NUM= :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
   __sJT_st.setLong(2,pkid);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.acr.ACRMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:443^9*/

        rs = rsItr.getResultSet();

        if(!rs.next())
          throw new Exception("Primary ACR '"+pkid+"' record not found.");
        else if(!dbRecToObject(rs,acr))
          throw new Exception("Load ACR '"+pkid+"' FAILED.");

        bFound=true;

        rs.close();
        rsItr.close();
      }

      // retreive acr items?
      if(dbBits.get(BIT_ACR_ITEM)) {
        /*@lineinfo:generated-code*//*@lineinfo:460^9*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                      ACRITEM.ACRI_SEQ_NUM ID_ACRITEM
//                      ,ACRITEM.NAME ACRI_NAME
//                      ,ACRITEM.VALUE ACRI_VALUE
//            FROM
//                      ACRITEM
//            WHERE
//                      ACRITEM.ACRID=:pkid
//            ORDER BY
//                      ACRITEM.NAME asc
//  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                    ACRITEM.ACRI_SEQ_NUM ID_ACRITEM\n                    ,ACRITEM.NAME ACRI_NAME\n                    ,ACRITEM.VALUE ACRI_VALUE\n          FROM\n                    ACRITEM\n          WHERE\n                    ACRITEM.ACRID= :1 \n          ORDER BY\n                    ACRITEM.NAME asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.acr.ACRMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:473^9*/

        rs = rsItr.getResultSet();

        int numACRItemsLoaded = 0;
        ACRItem acri=null;
        ACRDefItem acrdi=null;

        while(rs.next()) {
          log.debug("ACR Item '"+rs.getString("ACRI_NAME")+"::"+rs.getString("ACRI_VALUE"));
          acri = new ACRItem();
          if(!dbRecToObject(rs,acri))
            throw new Exception("Load ACR Item '"+rs.getLong("ID_ACRITEM")+"' FAILED.");
          else {
            acr.setACRItem(acri.getName(),acri.getValue());
            //log.debug("ACR Item '"+acri.getID()+"' added to ACR '"+acr.getID()+"'.");
            numACRItemsLoaded++;
          }
        }
        //log.debug(numACRItemsLoaded+" ACR Items loaded for ACR '"+pkid+"'.");

        rs.close();
        rsItr.close();
      }

    }
    catch(Exception e) {
      log.error("ACRMESDB.load(ACR) EXCEPTION: '"+e.getMessage()+"'.");
      throw e;
    }
    finally {
      try { rs.close(); }    catch(Exception e) {}
      try { rsItr.close(); } catch(Exception e) {}
      cleanUp();
    }

    return bFound;
  }

  /**
   * load() - MerchantInfo - Single
   *
   * SELECTs MerchantInfo by primary key
   *
   * @param   pkid    primary key of target to load
   * @param   acr     FILLED IN: object to receive found data.
   * @param   dbBits  Bit Flags: 1..N of BIT_{...} flags.
   *                  Determines which constituent data units to affect.
   *
   * @Exception       Throws exception upon error attempting to perform load op.
   */
  public boolean load(String merchant_num,MerchantInfo merchantInfo)
    throws Exception
  {
    ResultSet rs=null;
    ResultSetIterator rsItr=null;
    boolean bFound=false;
    int vnumcount=0;
    String[] vnums=null;
    String[] ips_vnums=null;

    try {

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:538^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                  MIF.MERCHANT_NUMBER
//                  ,MERCHANT.APP_SEQ_NUM
//                  ,MIF.DBA_NAME
//                  ,MIF.bank_number
//                  ,MERCHANT.MERC_CNTRL_NUMBER
//                  ,MIF.PHONE_1 MERCHANT_PHONE
//                  ,MIF.DDA_NUM DDA
//                  ,MIF.TRANSIT_ROUTNG_NUM TRN
//                  ,MIF.DMAGENT ASSOCIATION
//          FROM
//                  MERCHANT,MIF
//          WHERE
//                  MERCHANT.MERCH_NUMBER(+)=MIF.MERCHANT_NUMBER
//                  AND MIF.MERCHANT_NUMBER=:merchant_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                MIF.MERCHANT_NUMBER\n                ,MERCHANT.APP_SEQ_NUM\n                ,MIF.DBA_NAME\n                ,MIF.bank_number\n                ,MERCHANT.MERC_CNTRL_NUMBER\n                ,MIF.PHONE_1 MERCHANT_PHONE\n                ,MIF.DDA_NUM DDA\n                ,MIF.TRANSIT_ROUTNG_NUM TRN\n                ,MIF.DMAGENT ASSOCIATION\n        FROM\n                MERCHANT,MIF\n        WHERE\n                MERCHANT.MERCH_NUMBER(+)=MIF.MERCHANT_NUMBER\n                AND MIF.MERCHANT_NUMBER= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchant_num);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.acr.ACRMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:555^7*/


      rs = rsItr.getResultSet();

      if(!rs.next())
        throw new Exception("MerchantInfo of merchant number: '"+merchant_num+"' not found.");
      else if(!dbRecToObject(rs,merchantInfo))
        throw new Exception("Load MerchantInfo '"+merchant_num+"' FAILED.");

      bFound=true;

      rs.close();
      rsItr.close();

      // use org_app, t_hierarchy, and mif to determine app type of merchant
      // specified in the acr table for this acr id
      /*@lineinfo:generated-code*//*@lineinfo:572^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { select  oa.appsrctype_code    app_type,
//                  oa.app_type           app_type_num
//          from    t_hierarchy   th,
//                  org_app       oa,
//                  mif           m
//          where   m.merchant_number=:merchant_num and
//                  m.association_node = th.descendent and
//                  th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                  th.ancestor = oa.hierarchy_node
//          order by th.relation asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  oa.appsrctype_code    app_type,\n                oa.app_type           app_type_num\n        from    t_hierarchy   th,\n                org_app       oa,\n                mif           m\n        where   m.merchant_number= :1  and\n                m.association_node = th.descendent and\n                th.hier_type =  :2  and\n                th.ancestor = oa.hierarchy_node\n        order by th.relation asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchant_num);
   __sJT_st.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.acr.ACRMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:584^7*/

      rs = rsItr.getResultSet();

      // first item in result set is the app type
      if(rs.next())
      {
        merchantInfo.setType(rs.getString("app_type"));
        merchantInfo.setTypeCode(rs.getInt("app_type_num"));
      }

      rs.close();
      rsItr.close();

      if(merchantInfo.getAppSeqNum()>0) {

        /*@lineinfo:generated-code*//*@lineinfo:600^9*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                    (MERCHCONTACT.MERCHCONT_PRIM_FIRST_NAME || ' ' || MERCHCONTACT.MERCHCONT_PRIM_LAST_NAME) MERCHANT_CONTACT_NAME
//            FROM
//                    MERCHCONTACT
//            WHERE
//                    MERCHCONTACT.APP_SEQ_NUM=:merchantInfo.getAppSeqNum()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3507 = merchantInfo.getAppSeqNum();
  try {
   String theSqlTS = "SELECT\n                  (MERCHCONTACT.MERCHCONT_PRIM_FIRST_NAME || ' ' || MERCHCONTACT.MERCHCONT_PRIM_LAST_NAME) MERCHANT_CONTACT_NAME\n          FROM\n                  MERCHCONTACT\n          WHERE\n                  MERCHCONTACT.APP_SEQ_NUM= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3507);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.acr.ACRMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:608^9*/

        rs = rsItr.getResultSet();

        if(rs.next())
          dbRecToObject(rs,merchantInfo);

        rs.close();
        rsItr.close();

        // retreive any associated vnumbers
        vnumcount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:620^9*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//            
//            from v_numbers
//            where app_seq_num = :merchantInfo.getAppSeqNum()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3508 = merchantInfo.getAppSeqNum();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n           \n          from v_numbers\n          where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.acr.ACRMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3508);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vnumcount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:626^9*/

        if(vnumcount>0) {

          /*@lineinfo:generated-code*//*@lineinfo:630^11*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                      V_NUMBER
//              FROM
//                      V_NUMBERS
//              WHERE
//                      APP_SEQ_NUM = :merchantInfo.getAppSeqNum()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3509 = merchantInfo.getAppSeqNum();
  try {
   String theSqlTS = "SELECT\n                    V_NUMBER\n            FROM\n                    V_NUMBERS\n            WHERE\n                    APP_SEQ_NUM =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3509);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.acr.ACRMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:638^11*/

          rs = rsItr.getResultSet();

          vnums = new String[vnumcount];

          for(int i=0;i<vnumcount && rs.next();i++)
            vnums[i] = rs.getString(1);

          rs.close();
          rsItr.close();

        }

      }

      // get ips vnumbers (if any)
      vnumcount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:656^7*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//          
//          from ips_file
//          where merchant_number = :merchant_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n         \n        from ips_file\n        where merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.acr.ACRMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchant_num);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vnumcount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:662^7*/

      if(vnumcount>0) {

        /*@lineinfo:generated-code*//*@lineinfo:666^9*/

//  ************************************************************
//  #sql [Ctx] rsItr = { select disposition
//            from ips_file
//            where merchant_number = :merchant_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select disposition\n          from ips_file\n          where merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchant_num);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.acr.ACRMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:671^9*/

        rs = rsItr.getResultSet();

        ips_vnums = new String[vnumcount];

        for(int i=0;i<vnumcount && rs.next();i++)
          ips_vnums[i] = rs.getString(1);

        rs.close();
        rsItr.close();
      }

      int totnumvnums = (vnums!=null? vnums.length:0) + (ips_vnums!=null? ips_vnums.length:0);
      log.debug("totnumvnums="+totnumvnums);
      if(totnumvnums>0) {
        int j=0;
        String[] all_vnums = new String[totnumvnums];
        if(vnums!=null) {
          for(int i=0;i<vnums.length;i++)
            all_vnums[j++]=vnums[i];
        }
        if(ips_vnums!=null) {
          for(int i=0;i<ips_vnums.length;i++)
            all_vnums[j++]=ips_vnums[i];
        }

        merchantInfo.setVNums(all_vnums);
      }

      // acr app type code
      int acrAppTypeCode = ACRAppTypeLookup.getInstance().getAppType(merchantInfo.getMerchantNum());
      if(acrAppTypeCode >= 0) {
        merchantInfo.setACRAppTypeCode(acrAppTypeCode);
        merchantInfo.setACRAppType(ACRAppTypeLookup.getInstance().getAppTypeCode(acrAppTypeCode));
      }
    }
    catch(Exception e) {
      log.error("ACRMESDB.load(MerchantInfo) EXCEPTION: '"+e.getMessage()+"'.");
      throw e;
    }
    finally {
      try { rs.close(); }    catch(Exception e) {}
      try { rsItr.close(); } catch(Exception e) {}
      cleanUp();
    }

    return bFound;
  }

  /**
   * load() - ACR/ACRDefinition - MULTI
   *
   * SELECTs ACRs by one of two methods:
   *  - by application ID (app seq num)
   *  - by ACR Definition ID
   *
   * @param   fldNme  Criteria: field name
   * @param   fldVal  Criteria: field value
   * @param   dbBits  Bit Flags: 1..N of BIT_{...} flags.
   *                  Determines which constituent data units to affect.
   *
   * @Exception       Throws exception upon error attempting to perform load op.
   */
  public Vector load(String fldNme,String fldVal,BitSet dbBits)
    throws Exception
  {
    ResultSet rs=null;
    ResultSetIterator rsItr=null;
    Vector clctn = null;
    boolean bNew=false;

    try {

      connect();

      Object prnt=null,chld=null;
      long prntID=-1L,crntID=-1L;
      String chldIDstr=null;

      // load ACRs
      if(dbBits.get(BIT_ACR_PRIMARY)) {

        // ACRs by AppSeqNum parent ID
        if(fldNme.equals("APP_SEQ_NUM")) {
          /*@lineinfo:generated-code*//*@lineinfo:756^11*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                      ACR.ACR_SEQ_NUM ID_ACR
//                      ,ACR.MERCH_NUMBER
//                      ,ACR.APP_SEQ_NUM
//                      ,ACR.ACRDEFID
//                      ,ACR.STATUS
//                      ,ACR.DATE_CREATED
//                      ,ACR.DATE_LAST_MODIFIED
//                      ,ACRITEM.ACRI_SEQ_NUM ID_ACRITEM
//                      ,ACRITEM.NAME ACRI_NAME
//                      ,ACRITEM.VALUE ACRI_VALUE
//              FROM
//                      ACR,ACRITEM
//              WHERE
//                      ACR.ACR_SEQ_NUM=ACRITEM.ACRID(+)
//                      AND APP_SEQ_NUM=:fldVal
//              ORDER BY ACRITEM.ACRID DESC
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                    ACR.ACR_SEQ_NUM ID_ACR\n                    ,ACR.MERCH_NUMBER\n                    ,ACR.APP_SEQ_NUM\n                    ,ACR.ACRDEFID\n                    ,ACR.STATUS\n                    ,ACR.DATE_CREATED\n                    ,ACR.DATE_LAST_MODIFIED\n                    ,ACRITEM.ACRI_SEQ_NUM ID_ACRITEM\n                    ,ACRITEM.NAME ACRI_NAME\n                    ,ACRITEM.VALUE ACRI_VALUE\n            FROM\n                    ACR,ACRITEM\n            WHERE\n                    ACR.ACR_SEQ_NUM=ACRITEM.ACRID(+)\n                    AND APP_SEQ_NUM= :1 \n            ORDER BY ACRITEM.ACRID DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,fldVal);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.acr.ACRMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:775^11*/

        // ACRs by Merchant number parent ID
        } else if(fldNme.equals("MERCH_NUMBER")) {
          /*@lineinfo:generated-code*//*@lineinfo:779^11*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                      ACR.ACR_SEQ_NUM ID_ACR
//                      ,ACR.MERCH_NUMBER
//                      ,ACR.APP_SEQ_NUM
//                      ,ACR.ACRDEFID
//                      ,ACR.STATUS
//                      ,ACR.DATE_CREATED
//                      ,ACR.DATE_LAST_MODIFIED
//                      ,ACRITEM.ACRI_SEQ_NUM ID_ACRITEM
//                      ,ACRITEM.NAME ACRI_NAME
//                      ,ACRITEM.VALUE ACRI_VALUE
//              FROM
//                      ACR,ACRITEM
//              WHERE
//                      ACR.ACR_SEQ_NUM=ACRITEM.ACRID(+)
//                      AND MERCH_NUMBER=:fldVal
//              ORDER BY ACRITEM.ACRID DESC
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                    ACR.ACR_SEQ_NUM ID_ACR\n                    ,ACR.MERCH_NUMBER\n                    ,ACR.APP_SEQ_NUM\n                    ,ACR.ACRDEFID\n                    ,ACR.STATUS\n                    ,ACR.DATE_CREATED\n                    ,ACR.DATE_LAST_MODIFIED\n                    ,ACRITEM.ACRI_SEQ_NUM ID_ACRITEM\n                    ,ACRITEM.NAME ACRI_NAME\n                    ,ACRITEM.VALUE ACRI_VALUE\n            FROM\n                    ACR,ACRITEM\n            WHERE\n                    ACR.ACR_SEQ_NUM=ACRITEM.ACRID(+)\n                    AND MERCH_NUMBER= :1 \n            ORDER BY ACRITEM.ACRID DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,fldVal);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.acr.ACRMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:798^11*/

        // ACRs by ACR Definition ID
        } else if(fldNme.equals("ACRDEFID")) {
          /*@lineinfo:generated-code*//*@lineinfo:802^11*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                      ACR_SEQ_NUM ID_ACR
//                      ,ACR.MERCH_NUMBER
//                      ,ACR.APP_SEQ_NUM
//                      ,ACR.ACRDEFID
//                      ,ACR.STATUS
//                      ,ACR.DATE_CREATED
//                      ,ACR.DATE_LAST_MODIFIED
//                      ,ACRITEM.ACRI_SEQ_NUM ID_ACRITEM
//                      ,ACRITEM.NAME ACRI_NAME
//                      ,ACRITEM.VALUE ACRI_VALUE
//              FROM
//                      ACR
//              WHERE
//                      ACRDEFID=:fldVal
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                    ACR_SEQ_NUM ID_ACR\n                    ,ACR.MERCH_NUMBER\n                    ,ACR.APP_SEQ_NUM\n                    ,ACR.ACRDEFID\n                    ,ACR.STATUS\n                    ,ACR.DATE_CREATED\n                    ,ACR.DATE_LAST_MODIFIED\n                    ,ACRITEM.ACRI_SEQ_NUM ID_ACRITEM\n                    ,ACRITEM.NAME ACRI_NAME\n                    ,ACRITEM.VALUE ACRI_VALUE\n            FROM\n                    ACR\n            WHERE\n                    ACRDEFID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,fldVal);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.acr.ACRMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:819^11*/

        // Unsupported ACR criteria
        } else
          throw new Exception("Unsupported ACR criteria field name: '"+fldNme+"'.  Aborted.");

        rs = rsItr.getResultSet();
        clctn = new Vector(1,1);

        // db rec --> obj
        while(rs.next()) {
          crntID=rs.getLong(1);
          if(prntID!=crntID) {
            // new parent record
            prnt = new ACR();
            if(!dbRecToObject(rs,(ACR)prnt))
              throw new Exception("db Record to Object conversion failed for parent ACR '"+crntID+"'.");
            prntID=crntID;
            bNew=true;
            //log.debug("load(MULTI - ACR) - prntID set to crntID.");
          }
          if(dbBits.get(BIT_ACR_ITEM)) {
            chldIDstr = rs.getString("ID_ACRITEM");
            //log.debug("chldIDstr="+chldIDstr);
            if(chldIDstr!=null && chldIDstr.length()>0) {
              chld = new ACRItem();
              if(!dbRecToObject(rs,(ACRItem)chld))
                throw new Exception("db Record to Object conversion failed for child ACR Item of parent ID: '"+crntID+"'.");
              ((ACR)prnt).setACRItem(((ACRItem)chld).getName(),((ACRItem)chld).getValue());
              //log.debug("load(MULTI - ACR) - child '"+chldIDstr+"' set under parent '"+prntID+"'.");
            } else
              log.warn("No ACR Items found for ACR '"+prntID+"'.");
          }
          if(bNew) {
            clctn.addElement(prnt);
            //log.debug("ACR '"+prntID+"' added to collection.");
            bNew=false;
          }
        }

        rs.close();
        rsItr.close();

      // load ACR Definitions
      } else if(dbBits.get(BIT_ACRDEF_PRIMARY)) {

        // ALL ACR Definitions
        if(fldNme.equals("ALL")) {
          /*@lineinfo:generated-code*//*@lineinfo:867^11*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                      ACRDEF.ACRDEF_SEQ_NUM ID_ACRDEF
//                      ,ACRDEF.NAME
//                      ,ACRDEF.SHORT_NAME
//                      ,ACRDEF.DESCRIPTION
//                      ,ACRDEF.TYPE
//                      ,ACRDEF.IS_ON
//  
//                      ,acrdef.bean_class
//                      ,acrdef.rights
//                      ,acrdef.jsp_include
//                      ,acrdef.init_to_mgmt_q
//  
//                      ,ACRDEFITEM.ACRDI_SEQ_NUM ID_ACRDEFITEM
//                      ,ACRDEFITEM.NAME DEFITEM_NAME
//                      ,ACRDEFITEM.LABEL DEFITEM_LABEL
//                      ,ACRDEFITEM.DESCRIPTION DEFITEM_DESCRIPTION
//                      ,ACRDEFITEM.DATATYPE DEFITEM_DATATYPE
//                      ,ACRDEFITEM.ISREQUIRED DEFITEM_ISREQUIRED
//                      ,ACRDEFITEM.DEFAULTVALUE DEFITEM_DEFAULTVALUE
//                      ,ACRDEFITEM.VALIDATIONPARAMS DEFITEM_VALIDATIONPARAMS
//              FROM
//                        ACRDEF,ACRDEFITEM
//              WHERE
//                        ACRDEF.ACRDEF_SEQ_NUM=ACRDEFITEM.ACRDID(+)
//              ORDER BY  ACRDEF.NAME,ACRDEFITEM.ACRDID
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                    ACRDEF.ACRDEF_SEQ_NUM ID_ACRDEF\n                    ,ACRDEF.NAME\n                    ,ACRDEF.SHORT_NAME\n                    ,ACRDEF.DESCRIPTION\n                    ,ACRDEF.TYPE\n                    ,ACRDEF.IS_ON\n\n                    ,acrdef.bean_class\n                    ,acrdef.rights\n                    ,acrdef.jsp_include\n                    ,acrdef.init_to_mgmt_q\n\n                    ,ACRDEFITEM.ACRDI_SEQ_NUM ID_ACRDEFITEM\n                    ,ACRDEFITEM.NAME DEFITEM_NAME\n                    ,ACRDEFITEM.LABEL DEFITEM_LABEL\n                    ,ACRDEFITEM.DESCRIPTION DEFITEM_DESCRIPTION\n                    ,ACRDEFITEM.DATATYPE DEFITEM_DATATYPE\n                    ,ACRDEFITEM.ISREQUIRED DEFITEM_ISREQUIRED\n                    ,ACRDEFITEM.DEFAULTVALUE DEFITEM_DEFAULTVALUE\n                    ,ACRDEFITEM.VALIDATIONPARAMS DEFITEM_VALIDATIONPARAMS\n            FROM\n                      ACRDEF,ACRDEFITEM\n            WHERE\n                      ACRDEF.ACRDEF_SEQ_NUM=ACRDEFITEM.ACRDID(+)\n            ORDER BY  ACRDEF.NAME,ACRDEFITEM.ACRDID";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.acr.ACRMESDB",theSqlTS);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.acr.ACRMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:895^11*/

        // assoc. Definition given ACR ID
        } else if(fldNme.equals("ID_ACR")) {
          /*@lineinfo:generated-code*//*@lineinfo:899^11*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                      ACRDEF.ACRDEF_SEQ_NUM ID_ACRDEF
//                      ,ACRDEF.NAME
//                      ,ACRDEF.SHORT_NAME
//                      ,ACRDEF.DESCRIPTION
//                      ,ACRDEF.TYPE
//                      ,ACRDEF.IS_ON
//  
//                      ,acrdef.bean_class
//                      ,acrdef.rights
//                      ,acrdef.jsp_include
//                      ,acrdef.init_to_mgmt_q
//  
//                      ,ACRDEFITEM.ACRDI_SEQ_NUM ID_ACRDEFITEM
//                      ,ACRDEFITEM.NAME DEFITEM_NAME
//                      ,ACRDEFITEM.LABEL DEFITEM_LABEL
//                      ,ACRDEFITEM.DESCRIPTION DEFITEM_DESCRIPTION
//                      ,ACRDEFITEM.DATATYPE DEFITEM_DATATYPE
//                      ,ACRDEFITEM.ISREQUIRED DEFITEM_ISREQUIRED
//                      ,ACRDEFITEM.DEFAULTVALUE DEFITEM_DEFAULTVALUE
//                      ,ACRDEFITEM.VALIDATIONPARAMS DEFITEM_VALIDATIONPARAMS
//              FROM
//                      ACR,ACRDEF,ACRDEFITEM
//              WHERE
//                      ACR.ACRDEFID=ACRDEF.ACRDEF_SEQ_NUM
//                      AND ACRDEF.ACRDEF_SEQ_NUM=ACRDEFITEM.ACRDID(+)
//                      AND ACR.ACR_SEQ_NUM=:fldVal
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                    ACRDEF.ACRDEF_SEQ_NUM ID_ACRDEF\n                    ,ACRDEF.NAME\n                    ,ACRDEF.SHORT_NAME\n                    ,ACRDEF.DESCRIPTION\n                    ,ACRDEF.TYPE\n                    ,ACRDEF.IS_ON\n\n                    ,acrdef.bean_class\n                    ,acrdef.rights\n                    ,acrdef.jsp_include\n                    ,acrdef.init_to_mgmt_q\n\n                    ,ACRDEFITEM.ACRDI_SEQ_NUM ID_ACRDEFITEM\n                    ,ACRDEFITEM.NAME DEFITEM_NAME\n                    ,ACRDEFITEM.LABEL DEFITEM_LABEL\n                    ,ACRDEFITEM.DESCRIPTION DEFITEM_DESCRIPTION\n                    ,ACRDEFITEM.DATATYPE DEFITEM_DATATYPE\n                    ,ACRDEFITEM.ISREQUIRED DEFITEM_ISREQUIRED\n                    ,ACRDEFITEM.DEFAULTVALUE DEFITEM_DEFAULTVALUE\n                    ,ACRDEFITEM.VALIDATIONPARAMS DEFITEM_VALIDATIONPARAMS\n            FROM\n                    ACR,ACRDEF,ACRDEFITEM\n            WHERE\n                    ACR.ACRDEFID=ACRDEF.ACRDEF_SEQ_NUM\n                    AND ACRDEF.ACRDEF_SEQ_NUM=ACRDEFITEM.ACRDID(+)\n                    AND ACR.ACR_SEQ_NUM= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,fldVal);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.acr.ACRMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:928^11*/

        // Unsupported ACR Definition criteria
        } else
          throw new Exception("Unsupported ACR Definition criteria field name: '"+fldNme+"'.  Aborted.");

        rs = rsItr.getResultSet();
        clctn = new Vector(1,1);

        // db rec --> obj
        while(rs.next()) {
          crntID=rs.getLong(1);
          if(prntID!=crntID) {
            // new parent record
            prnt = new ACRDefinition();
            if(!dbRecToObject(rs,(ACRDefinition)prnt))
              throw new Exception("db Record to Object conversion failed for parent ACR Definition '"+crntID+"'.");
            prntID=crntID;
            bNew=true;
            //log.debug("load(MULTI - ACRDefinition) - prntID set to crntID.");
          }
          if(dbBits.get(BIT_ACRDEF_ITEM)) {
            chldIDstr = rs.getString("ID_ACRDEFITEM");
            //log.debug("chldIDstr="+chldIDstr);
            if(chldIDstr!=null && chldIDstr.length()>0) {
              chld = new ACRDefItem();
              if(!dbRecToObject(rs,(ACRDefItem)chld))
                throw new Exception("db Record to Object conversion failed for child ACR Definition Item of parent ID: '"+crntID+"'.");
              ((ACRDefinition)prnt).setDefItem((ACRDefItem)chld);
              //log.debug("load(MULTI - ACRDefinition) - child '"+chldIDstr+"' set under parent '"+prntID+"'.");
            }
            // else
            //  log.warn("No ACR Def Items found for ACR Def '"+prntID+"'.");
          }
          if(bNew) {
            clctn.addElement(prnt);
            //log.debug("ACR Definition '"+prntID+"' added to collection.");
            bNew=false;
          }
        }

        rs.close();
        rsItr.close();

      } else
        throw new Exception("Unsupported Bit set combination specified for load.");

      //log.debug("load(MULTI) - "+clctn.size()+" objects loaded into collection.");

      return clctn;

    }
    catch(Exception e) {
      log.error("ACRMESDB.load(MULTI) EXCEPTION: '"+e.getMessage()+"'.");
      return null;
    }
    finally {
      try { rs.close(); }    catch(Exception e) {}
      try { rsItr.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  // UPDATE

  /**
   * persist() - ACR
   *
   * Updates ACR related data only and NOT ACRDefinition related data
   *
   * @param   dbBits  Bit Flags: All defined BIT_{...} flags required in declared order required to be present
   *                  Determines which constituent data units to affect.
   *
   * @param   bBatch  true: NO commit performed, false: Commit performed and end of function upon successful updates
   *
   * @conditional     IMPT: Determine existence of ACR record by presence of ACR.id in acr param
   */
  public boolean persist(ACR acr,BitSet dbBits,boolean bBatch)
  {
    try {

      connect();

      boolean bAltered=false;

      // save primary?
      if(dbBits.get(BIT_ACR_PRIMARY)) {
        if(acr.isDirty()) {

          if(acr.getID()<0)
            throw new Exception("Unable to persist ACR: No ACR ID specified.");

          acr.setDateLastModified(new Date());

          java.sql.Timestamp dlm = new java.sql.Timestamp(acr.getDateLastModified().getTime());
          //log.debug("Persisting ACR '"+acr.getID()+"'...");

          int prntCount;

          // determine if need to create parent ACR record
          /*@lineinfo:generated-code*//*@lineinfo:1028^11*/

//  ************************************************************
//  #sql [Ctx] { SELECT  COUNT(*)
//              
//              FROM    ACR
//              WHERE   ACR_SEQ_NUM=:acr.getID()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3510 = acr.getID();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  COUNT(*)\n             \n            FROM    ACR\n            WHERE   ACR_SEQ_NUM= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.acr.ACRMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3510);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   prntCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1034^11*/

          //log.debug("ACR prntCount="+prntCount);
          if(prntCount==0L) {
            // doesn't exist
            java.sql.Timestamp dc = new java.sql.Timestamp(acr.getDateCreated().getTime());

            final long acrdid = acr.definitionExists()? acr.getDefinition().getID():-1L;

            /*@lineinfo:generated-code*//*@lineinfo:1043^13*/

//  ************************************************************
//  #sql [Ctx] { INSERT
//                INTO
//                        ACR
//  
//                        (ACR_SEQ_NUM,APP_SEQ_NUM,ACRDEFID,STATUS,DATE_CREATED,DATE_LAST_MODIFIED,MERCH_NUMBER)
//                VALUES
//                        (
//                          :acr.getID()
//                          ,:acr.getAppSeqNum()
//                          ,:acrdid
//                          ,:acr.getStatus()
//                          ,:dc
//                          ,:dlm
//                          ,:acr.getMerchantNum()
//                        )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3511 = acr.getID();
 long __sJT_3512 = acr.getAppSeqNum();
 int __sJT_3513 = acr.getStatus();
 String __sJT_3514 = acr.getMerchantNum();
  try {
   String theSqlTS = "INSERT\n              INTO\n                      ACR\n\n                      (ACR_SEQ_NUM,APP_SEQ_NUM,ACRDEFID,STATUS,DATE_CREATED,DATE_LAST_MODIFIED,MERCH_NUMBER)\n              VALUES\n                      (\n                         :1 \n                        , :2 \n                        , :3 \n                        , :4 \n                        , :5 \n                        , :6 \n                        , :7 \n                      )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3511);
   __sJT_st.setLong(2,__sJT_3512);
   __sJT_st.setLong(3,acrdid);
   __sJT_st.setInt(4,__sJT_3513);
   __sJT_st.setTimestamp(5,dc);
   __sJT_st.setTimestamp(6,dlm);
   __sJT_st.setString(7,__sJT_3514);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1060^13*/
            log.info("ACR '"+acr.getID()+"' INSERTED.");

          } else {
            // already exists
            /*@lineinfo:generated-code*//*@lineinfo:1065^13*/

//  ************************************************************
//  #sql [Ctx] { UPDATE
//                        ACR
//                SET
//                        ACRDEFID = :acr.definitionExists()? acr.getDefinition().getID():-1L
//                        ,MERCH_NUMBER = :acr.getMerchantNum()
//                        ,APP_SEQ_NUM = :acr.getAppSeqNum()
//                        ,STATUS = :acr.getStatus()
//                        ,DATE_LAST_MODIFIED=:dlm
//                WHERE
//                        ACR_SEQ_NUM = :acr.getID()
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3515 = acr.definitionExists()? acr.getDefinition().getID():-1L;
 String __sJT_3516 = acr.getMerchantNum();
 long __sJT_3517 = acr.getAppSeqNum();
 int __sJT_3518 = acr.getStatus();
 long __sJT_3519 = acr.getID();
  try {
   String theSqlTS = "UPDATE\n                      ACR\n              SET\n                      ACRDEFID =  :1 \n                      ,MERCH_NUMBER =  :2 \n                      ,APP_SEQ_NUM =  :3 \n                      ,STATUS =  :4 \n                      ,DATE_LAST_MODIFIED= :5 \n              WHERE\n                      ACR_SEQ_NUM =  :6";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3515);
   __sJT_st.setString(2,__sJT_3516);
   __sJT_st.setLong(3,__sJT_3517);
   __sJT_st.setInt(4,__sJT_3518);
   __sJT_st.setTimestamp(5,dlm);
   __sJT_st.setLong(6,__sJT_3519);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1077^13*/
            //log.debug("ACR '"+acr.getID()+"' UPDATED.");
          }
          acr.clean();

          bAltered=true;
        }
      }

      // save acr items?
      if(dbBits.get(BIT_ACR_ITEM)) {
        //log.debug("Updating ACR Items...");

        ACRItem acri;
        StringBuffer sb = new StringBuffer(8*acr.getNumItems());  // ensure big enough so buffer doesn't need to grow later
        long seqnum;

        for(Enumeration e=acr.getItemEnumeration();e.hasMoreElements();) {
          acri=(ACRItem)e.nextElement();

          if(acri.isDirty()) {

            //alter data to ensure no Oracle exception4
            if(acri.getName().length()>60)
              acri.setName(acri.getName().substring(0,60));
            if(acri.getValue().length()>1000)
              acri.setValue(acri.getValue().substring(0,1000));

            if(acri.getID()>0) {

             // record presumed to exist
              //log.debug("Persisting-UPDATE ACR Item '"+acri.getID()+"'...");
              /*@lineinfo:generated-code*//*@lineinfo:1109^15*/

//  ************************************************************
//  #sql [Ctx] { UPDATE
//                          ACRITEM
//                  SET
//                          NAME = :acri.getName()
//                          ,VALUE = :acri.getValue()
//                  WHERE
//                          ACRI_SEQ_NUM = :acri.getID()
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3520 = acri.getName();
 String __sJT_3521 = acri.getValue();
 long __sJT_3522 = acri.getID();
  try {
   String theSqlTS = "UPDATE\n                        ACRITEM\n                SET\n                        NAME =  :1 \n                        ,VALUE =  :2 \n                WHERE\n                        ACRI_SEQ_NUM =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3520);
   __sJT_st.setString(2,__sJT_3521);
   __sJT_st.setLong(3,__sJT_3522);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1118^15*/
              acri.clean();

              bAltered=true;

              //log.debug("ACRItem '"+acri.getID()+"' UPDATED.");
            } else {
              // record presumed to NOT exist
              log.debug("Persisting-INSERT ACR Item '"+acri.getName()+"'...");
              // get unique acri pkid
              /*@lineinfo:generated-code*//*@lineinfo:1128^15*/

//  ************************************************************
//  #sql [Ctx] { SELECT  ACR_SEQUENCE.nextval
//                  
//                  FROM    dual
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  ACR_SEQUENCE.nextval\n                 \n                FROM    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.acr.ACRMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   seqnum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1133^15*/

              /*@lineinfo:generated-code*//*@lineinfo:1135^15*/

//  ************************************************************
//  #sql [Ctx] { INSERT
//                  INTO
//                          ACRITEM
//  
//                          (ACRI_SEQ_NUM,ACRID,NAME,VALUE)
//                  VALUES
//                          (
//                            :seqnum
//                            ,:acr.getID()
//                            ,:acri.getName()
//                            ,:acri.getValue()
//                          )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3523 = acr.getID();
 String __sJT_3524 = acri.getName();
 String __sJT_3525 = acri.getValue();
  try {
   String theSqlTS = "INSERT\n                INTO\n                        ACRITEM\n\n                        (ACRI_SEQ_NUM,ACRID,NAME,VALUE)\n                VALUES\n                        (\n                           :1 \n                          , :2 \n                          , :3 \n                          , :4 \n                        )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,seqnum);
   __sJT_st.setLong(2,__sJT_3523);
   __sJT_st.setString(3,__sJT_3524);
   __sJT_st.setString(4,__sJT_3525);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1149^15*/

              // persist object with pkid
              acri.setID(seqnum);
              acri.clean();

              bAltered=true;

              //log.debug("ACRItem '"+acri.getID()+"' INSERTED.");
            }
          }

          // append id to IN clause for later use
          sb.append(',');
          sb.append(acri.getID());

        }

        // IMPT: delete those recs not contained in items collection but in db
        // (dynamic query so have to use JDBC)
        //log.debug("Cleansing related child response records for ACR ID '"+acr.getID()+"'...");
        if(sb.length()>1) {
          final String inclause = sb.substring(1);  // remove preceeding ','
          //log.debug("About to cleanse any orphaned ACR Items not in: '"+inclause+"' for ACR ID '"+acr.getID()+"'...");
          final String dynmcSQL =
              "DELETE"
            +" FROM"
                    +" ACRITEM"
            +" WHERE "
                    +" ACRID="+acr.getID()
                    +" AND NOT ACRI_SEQ_NUM IN ("+inclause+")";

          PreparedStatement ps = getPreparedStatement(dynmcSQL);
          ps.executeQuery();
          ps.close();
        } else {  // i.e. no acr items assoc. w/ acr
          // delete all child recs
          //log.debug("Deleting all child response records for ACR ID '"+acr.getID()+"'...");

          com.mes.support.SyncLog.LogEntry("ACRMESDB.persist(ACR) DELETE ACR ID '"+acr.getID()+"'");

          // Do not perform this to prevent data lost PRF-1088
          //#sql [Ctx]
          //{
          //  DELETE
          //  FROM
          //          ACRITEM
          //  WHERE
          //          ACRID=:(acr.getID())
          //};
        }

      }

      // commit the db trans
      if(bAltered)
        commit();

      return true;
    }
    catch(Exception e) {
      log.error("ACRMESDB.persist(ACR) EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
    finally {
      cleanUp();
    }
  }

  /**
   * persist() - ACRDefinition
   *
   * Updates ACRDefinition related data only and NOT ACR related data.
   *
   * @param   dbBits  Bit Flags: All defined BIT_{...} flags required in declared order required to be present
   *                  Determines which constituent data units to affect.
   *
   * @param   bBatch  true: NO commit performed, false: Commit performed and end of function upon successful updates
   *
   * @conditional     IMPT: Determine existence of ACRDefinition record by presence of ACRDefinition.id in acrd param
   */
  public boolean persist(ACRDefinition acrd,BitSet dbBits,boolean bBatch)
  {
    try {

      connect();

      boolean bAltered=false;

      // save primary?
      if(dbBits.get(BIT_ACRDEF_PRIMARY)) {
        if(acrd.isDirty()) {

          if(acrd.getID()<0)
            throw new Exception("Unable to persist ACR Definition: No ACR Definition ID specified.");

          //log.debug("Persisting ACR Def '"+acrd.getID()+"'...");

          int prntCount;

          // determine if need to create parent ACR record
          /*@lineinfo:generated-code*//*@lineinfo:1250^11*/

//  ************************************************************
//  #sql [Ctx] { SELECT  COUNT(*)
//              
//              FROM    ACRDEF
//              WHERE   ACRDEF_SEQ_NUM=:acrd.getID()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3526 = acrd.getID();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  COUNT(*)\n             \n            FROM    ACRDEF\n            WHERE   ACRDEF_SEQ_NUM= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.acr.ACRMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3526);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   prntCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1256^11*/

          if(prntCount==0L) {
            // doesn't exist
            /*@lineinfo:generated-code*//*@lineinfo:1260^13*/

//  ************************************************************
//  #sql [Ctx] { INSERT
//                INTO
//                        ACRDEF
//  
//                        (ACRDEF_SEQ_NUM,TYPE,IS_ON,NAME,DESCRIPTION,SHORT_NAME,INIT_TO_MGMT_Q)
//                VALUES
//                        (
//                          :acrd.getID()
//                          ,:acrd.getType()
//                          ,:acrd.isOn()
//                          ,:acrd.getName()
//                          ,:acrd.getDescription()
//                          ,:acrd.getShortName()
//  
//                          ,:acrd.getBeanClassName()
//                          ,:acrd.getRightStr()
//                          ,:acrd.getJspInclude()
//                          ,:acrd.initMgmtQ()
//  
//                        )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3527 = acrd.getID();
 String __sJT_3528 = acrd.getType();
 boolean __sJT_3529 = acrd.isOn();
 String __sJT_3530 = acrd.getName();
 String __sJT_3531 = acrd.getDescription();
 String __sJT_3532 = acrd.getShortName();
 String __sJT_3533 = acrd.getBeanClassName();
 String __sJT_3534 = acrd.getRightStr();
 String __sJT_3535 = acrd.getJspInclude();
 boolean __sJT_3536 = acrd.initMgmtQ();
  try {
   String theSqlTS = "INSERT\n              INTO\n                      ACRDEF\n\n                      (ACRDEF_SEQ_NUM,TYPE,IS_ON,NAME,DESCRIPTION,SHORT_NAME,INIT_TO_MGMT_Q)\n              VALUES\n                      (\n                         :1 \n                        , :2 \n                        , :3 \n                        , :4 \n                        , :5 \n                        , :6 \n\n                        , :7 \n                        , :8 \n                        , :9 \n                        , :10 \n\n                      )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3527);
   __sJT_st.setString(2,__sJT_3528);
   __sJT_st.setBoolean(3,__sJT_3529);
   __sJT_st.setString(4,__sJT_3530);
   __sJT_st.setString(5,__sJT_3531);
   __sJT_st.setString(6,__sJT_3532);
   __sJT_st.setString(7,__sJT_3533);
   __sJT_st.setString(8,__sJT_3534);
   __sJT_st.setString(9,__sJT_3535);
   __sJT_st.setBoolean(10,__sJT_3536);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1282^13*/
            //log.debug("ACR Definition '"+acrd.getID()+"' INSERTED.");
          } else {
            // already exists
            /*@lineinfo:generated-code*//*@lineinfo:1286^13*/

//  ************************************************************
//  #sql [Ctx] { UPDATE
//                        ACRDEF
//                SET
//                         NAME         = :acrd.getName()
//                        ,SHORT_NAME   = :acrd.getShortName()
//                        ,DESCRIPTION  = :acrd.getDescription()
//                        ,TYPE         = :acrd.getType()
//                        ,IS_ON        = :acrd.isOn()
//  
//                        ,bean_class   = :acrd.getBeanClassName()
//                        ,rights       = :acrd.getRightStr()
//                        ,jsp_include  = :acrd.getJspInclude()
//                        ,init_to_mgmt_q = :acrd.initMgmtQ()
//  
//                WHERE
//                        ACRDEF_SEQ_NUM = :acrd.getID()
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3537 = acrd.getName();
 String __sJT_3538 = acrd.getShortName();
 String __sJT_3539 = acrd.getDescription();
 String __sJT_3540 = acrd.getType();
 boolean __sJT_3541 = acrd.isOn();
 String __sJT_3542 = acrd.getBeanClassName();
 String __sJT_3543 = acrd.getRightStr();
 String __sJT_3544 = acrd.getJspInclude();
 boolean __sJT_3545 = acrd.initMgmtQ();
 long __sJT_3546 = acrd.getID();
  try {
   String theSqlTS = "UPDATE\n                      ACRDEF\n              SET\n                       NAME         =  :1 \n                      ,SHORT_NAME   =  :2 \n                      ,DESCRIPTION  =  :3 \n                      ,TYPE         =  :4 \n                      ,IS_ON        =  :5 \n\n                      ,bean_class   =  :6 \n                      ,rights       =  :7 \n                      ,jsp_include  =  :8 \n                      ,init_to_mgmt_q =  :9 \n\n              WHERE\n                      ACRDEF_SEQ_NUM =  :10";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3537);
   __sJT_st.setString(2,__sJT_3538);
   __sJT_st.setString(3,__sJT_3539);
   __sJT_st.setString(4,__sJT_3540);
   __sJT_st.setBoolean(5,__sJT_3541);
   __sJT_st.setString(6,__sJT_3542);
   __sJT_st.setString(7,__sJT_3543);
   __sJT_st.setString(8,__sJT_3544);
   __sJT_st.setBoolean(9,__sJT_3545);
   __sJT_st.setLong(10,__sJT_3546);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1304^13*/
            //log.debug("ACR Definition '"+acrd.getID()+"' UPDATED.");
          }
          acrd.clean();

          bAltered=true;
        }
      }

      // save acr items?
      if(dbBits.get(BIT_ACRDEF_ITEM)) {
        //log.debug("Updating ACR Definition Items...");

        ACRDefItem acrdi;
        StringBuffer sb = new StringBuffer(8*acrd.getNumItems());  // ensure big enough so buffer doesn't need to grow later
        long seqnum;

        for(Enumeration e=acrd.getItemEnumeration();e.hasMoreElements();) {
          acrdi=(ACRDefItem)e.nextElement();

          if(acrdi.isDirty()) {
            if(acrdi.getID()>0) {
              // record presumed to exist
              //log.debug("Persisting-UPDATE ACR Def Item '"+acrdi.getID()+"'...");
              /*@lineinfo:generated-code*//*@lineinfo:1328^15*/

//  ************************************************************
//  #sql [Ctx] { UPDATE
//                          ACRDEFITEM
//                  SET
//                          NAME = :acrdi.getName()
//                          ,LABEL = :acrdi.getLabel()
//                          ,DESCRIPTION = :acrdi.getDesc()
//                          ,DATATYPE = :acrdi.getDataType()
//                          ,ISREQUIRED = :acrdi.isRequired()
//                          ,DEFAULTVALUE = :acrdi.getDefaultValue()
//                          ,VALIDATIONPARAMS = :acrdi.getValidationParams()
//                  WHERE
//                          ACRDI_SEQ_NUM = :acrdi.getID()
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3547 = acrdi.getName();
 String __sJT_3548 = acrdi.getLabel();
 String __sJT_3549 = acrdi.getDesc();
 int __sJT_3550 = acrdi.getDataType();
 boolean __sJT_3551 = acrdi.isRequired();
 String __sJT_3552 = acrdi.getDefaultValue();
 String __sJT_3553 = acrdi.getValidationParams();
 long __sJT_3554 = acrdi.getID();
  try {
   String theSqlTS = "UPDATE\n                        ACRDEFITEM\n                SET\n                        NAME =  :1 \n                        ,LABEL =  :2 \n                        ,DESCRIPTION =  :3 \n                        ,DATATYPE =  :4 \n                        ,ISREQUIRED =  :5 \n                        ,DEFAULTVALUE =  :6 \n                        ,VALIDATIONPARAMS =  :7 \n                WHERE\n                        ACRDI_SEQ_NUM =  :8";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3547);
   __sJT_st.setString(2,__sJT_3548);
   __sJT_st.setString(3,__sJT_3549);
   __sJT_st.setInt(4,__sJT_3550);
   __sJT_st.setBoolean(5,__sJT_3551);
   __sJT_st.setString(6,__sJT_3552);
   __sJT_st.setString(7,__sJT_3553);
   __sJT_st.setLong(8,__sJT_3554);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1342^15*/
              acrdi.clean();

              bAltered=true;

              //log.debug("ACR Definition Item '"+acrdi.getID()+"' UPDATED.");

            } else {
              // record presumed to NOT exist
              //log.debug("Persisting-INSERT ACR Def Item '"+acrdi.getName()+"'...");
              // get unique acrdi pkid
              /*@lineinfo:generated-code*//*@lineinfo:1353^15*/

//  ************************************************************
//  #sql [Ctx] { SELECT  ACR_SEQUENCE.nextval
//                  
//                  FROM    dual
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  ACR_SEQUENCE.nextval\n                 \n                FROM    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.acr.ACRMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   seqnum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1358^15*/

              /*@lineinfo:generated-code*//*@lineinfo:1360^15*/

//  ************************************************************
//  #sql [Ctx] { INSERT
//                  INTO
//                          ACRDEFITEM
//  
//                          (ACRDI_SEQ_NUM,ACRDID,NAME,LABEL,DESCRIPTION,DATATYPE,ISREQUIRED,DEFAULTVALUE,VALIDATIONPARAMS)
//                  VALUES
//                          (
//                            :seqnum
//                            ,:acrd.getID()
//                            ,:acrdi.getName()
//                            ,:acrdi.getLabel()
//                            ,:acrdi.getDesc()
//                            ,:acrdi.getDataType()
//                            ,:acrdi.isRequired()
//                            ,:acrdi.getDefaultValue()
//                            ,:acrdi.getValidationParams()
//                          )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3555 = acrd.getID();
 String __sJT_3556 = acrdi.getName();
 String __sJT_3557 = acrdi.getLabel();
 String __sJT_3558 = acrdi.getDesc();
 int __sJT_3559 = acrdi.getDataType();
 boolean __sJT_3560 = acrdi.isRequired();
 String __sJT_3561 = acrdi.getDefaultValue();
 String __sJT_3562 = acrdi.getValidationParams();
  try {
   String theSqlTS = "INSERT\n                INTO\n                        ACRDEFITEM\n\n                        (ACRDI_SEQ_NUM,ACRDID,NAME,LABEL,DESCRIPTION,DATATYPE,ISREQUIRED,DEFAULTVALUE,VALIDATIONPARAMS)\n                VALUES\n                        (\n                           :1 \n                          , :2 \n                          , :3 \n                          , :4 \n                          , :5 \n                          , :6 \n                          , :7 \n                          , :8 \n                          , :9 \n                        )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,seqnum);
   __sJT_st.setLong(2,__sJT_3555);
   __sJT_st.setString(3,__sJT_3556);
   __sJT_st.setString(4,__sJT_3557);
   __sJT_st.setString(5,__sJT_3558);
   __sJT_st.setInt(6,__sJT_3559);
   __sJT_st.setBoolean(7,__sJT_3560);
   __sJT_st.setString(8,__sJT_3561);
   __sJT_st.setString(9,__sJT_3562);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1379^15*/

              acrdi.setID(seqnum);
              acrdi.clean();

              bAltered=true;

              //log.debug("ACR Definition Item '"+acrdi.getID()+"' INSERTED.");
            }
          }

          // append id to IN clause for later use
          sb.append(',');
          sb.append(acrdi.getID());

        }

        // IMPT: delete those recs not contained in items collection but in db
        // (dynamic query so have to use JDBC)
        //log.debug("Cleansing related child response records for ACR Def ID '"+acrd.getID()+"'...");
        if(sb.length()>1) {

          final String inclause = sb.substring(1);  // remove preceeding ','
          //log.debug("About to cleanse any orphaned ACR Def Items not in: '"+inclause+"' for ACR Def ID '"+acrd.getID()+"'...");
          final String dynmcSQL =
              "DELETE"
            +" FROM"
                    +" ACRDEFITEM"
            +" WHERE "
                    +" ACRDID="+acrd.getID()
                    +" AND NOT ACRDI_SEQ_NUM IN ("+inclause+")";

          PreparedStatement ps = getPreparedStatement(dynmcSQL);
          ps.executeQuery();
          ps.close();
        } else {  // i.e. no acr def items assoc. w/ acr def
          // delete all child recs
          //log.debug("Deleting all child response records for ACR Def ID '"+acrd.getID()+"'...");
          /*@lineinfo:generated-code*//*@lineinfo:1417^11*/

//  ************************************************************
//  #sql [Ctx] { DELETE
//              FROM
//                      ACRDEFITEM
//              WHERE
//                      ACRDID=:acrd.getID()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3563 = acrd.getID();
  try {
   String theSqlTS = "DELETE\n            FROM\n                    ACRDEFITEM\n            WHERE\n                    ACRDID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3563);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1424^11*/
        }

      }

      // commit the db trans
      if(bAltered)
        commit();

      return true;
    }
    catch(Exception e) {
      log.error("ACRMESDB.persist(ACRDefinition) EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
    finally {
      cleanUp();
    }
  }

  ////////////////////////

  /**
   * dbRecToObject() - ACRDefinition
   *
   * Maps db recordset field values to ACRDefinition object.
   * ResultSet field ordinality is critical as the order of loading depends on this order.
   */
  protected boolean dbRecToObject(ResultSet rs, ACRDefinition acrDef)
  {
    boolean loadOk = false;

    try
    {
      // load each column into the acr definition
      ResultSetMetaData metaData = rs.getMetaData();
      for (int i = 1; i <= metaData.getColumnCount(); ++i)
      {
        // extract current column name and data from
        String colName = metaData.getColumnName(i).toUpperCase();
        String colData = rs.getString(i);
        colData = (colData == null ? "" : colData);

        //log.debug("colName="+colName+" : colData="+colData);

        // use column name to determine where to put the data in the def
        if (colName.equals("ID_ACRDEF"))
        {
          if (colData.length()>0)
          {
            acrDef.setID(Long.parseLong(colData));
          }
        }
        else if (colName.equals("NAME"))
        {
          acrDef.setName(colData);
        }
        else if (colName.equals("SHORT_NAME"))
        {
          acrDef.setShortName(colData);
        }
        else if (colName.equals("DESCRIPTION"))
        {
          acrDef.setDescription(colData);
        }
        else if (colName.equals("TYPE"))
        {
          acrDef.setType(colData);
        }
        else if (colName.equals("BEAN_CLASS"))
        {
          acrDef.setBeanClassName(colData);
        }
        else if (colName.equals("RIGHTS"))
        {
          acrDef.setRightStr(colData);
        }
        else if (colName.equals("JSP_INCLUDE"))
        {
          acrDef.setJspInclude(colData);
        }
        else if (colName.equals("IS_ON"))
        {
          if (colData.equals("1"))
          {
            acrDef.turnOn();
          }
          else
          {
            acrDef.turnOff();
          }
        }
        else if (colName.equals("INIT_TO_MGMT_Q"))
        {
          if (colData.equalsIgnoreCase("Y"))
          {
            acrDef.setInitMgmtQ(true);
          }
          else
          {
            acrDef.setInitMgmtQ(false);
          }
        }
      }
      loadOk = true;
    }
    catch(Exception e)
    {
      log.error("Error loading acr definition: " + e);
    }


    // object loaded successfully so mark as clean
    acrDef.clean();

    return loadOk;
  }

  /**
   * dbRecToObject() - ACRDefItem
   *
   * Maps db recordset field values to ACR object.
   * ResultSet field ordinality is critical as the order of loading depends on this order.
   */
  protected boolean dbRecToObject(ResultSet rs, ACRDefItem acrdi)
  {
    try {

      String sDta;
      final ResultSetMetaData rsMtaDta = rs.getMetaData();

      for(int i=1; i<=rsMtaDta.getColumnCount(); i++) {

        sDta = rs.getString(i);
        if(null == sDta)
          sDta = "";

        //log.debug("dbRecToObject(ACRDefItem '"+acrdi.getName()+"') - "+rsMtaDta.getColumnName(i)+" = '"+sDta+"'");

        // ACR Definition Item related
        if(rsMtaDta.getColumnName(i).equals("ID_ACRDEFITEM")) {
          if(sDta.length()>0)
            acrdi.setID(Long.parseLong(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("DEFITEM_NAME")) {
          acrdi.setName(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("DEFITEM_LABEL")) {
          acrdi.setLabel(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("DEFITEM_DESCRIPTION")) {
          acrdi.setDesc(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("DEFITEM_DATATYPE")) {
          acrdi.setDataType(ACRDefItem.toDataTypeEnum(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("DEFITEM_ISREQUIRED")) {
          acrdi.setIsRequired(rs.getBoolean(i));
          //log.debug("dbRecToObject(ACRDefItem '"+acrdi.getName()+"' isRequired="+acrdi.isRequired());
        } else if(rsMtaDta.getColumnName(i).equals("DEFITEM_DEFAULTVALUE")) {
          acrdi.setDefaultValue(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("DEFITEM_VALIDATIONPARAMS")) {
          acrdi.setValidationParams(sDta);
        }

      }

    }
    catch(Exception e) {
      log.error("ACRMESDB.dbRecToObject(ACRDefItem) - EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }

    // object loaded successfully so mark as clean
    acrdi.clean();

    return true;
  }

  /**
   * dbRecToObject() - ACR
   *
   * Maps db recordset field values to ACR object.
   * ResultSet field ordinality is critical as the order of loading depends on this order.
   */
  protected boolean dbRecToObject(ResultSet rs, ACR acr)
  {
    try {

      String sDta;
      final ResultSetMetaData rsMtaDta = rs.getMetaData();

      String merchantNum=null;
      long appSeqNum=-1L;

      for(int i=1; i<=rsMtaDta.getColumnCount(); i++) {

        sDta = rs.getString(i);
        if(null == sDta)
          sDta = "";

        if(rsMtaDta.getColumnName(i).equals("ID_ACR")) {
          if(sDta.length()>0)
            acr.setID(Long.parseLong(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("ACRDEFID")) {
          if(sDta.length()>0)
            acr.setACRDefID(Long.parseLong(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("MERCH_NUMBER")) {
          if(sDta.length()>0)
            merchantNum=sDta;
        } else if(rsMtaDta.getColumnName(i).equals("APP_SEQ_NUM")) {
          if(sDta.length()>0)
            appSeqNum=Long.parseLong(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("STATUS")) {
          if(sDta.length()>0)
            acr.setStatus(Integer.parseInt(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("DATE_CREATED")) {
          if(rs.getTimestamp(i) != null)
            acr.setDateCreated((java.util.Date)rs.getTimestamp(i));
        } else if(rsMtaDta.getColumnName(i).equals("DATE_LAST_MODIFIED")) {
          if(rs.getTimestamp(i) != null)
            acr.setDateLastModified((java.util.Date)rs.getTimestamp(i));
        }

      }

      // set parent IDs
      acr.setParentIDs(merchantNum,appSeqNum);

    }
    catch(Exception e) {
      log.error("ACRMESDB.dbRecToObject() - EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }

    // object loaded successfully so mark as clean
    acr.clean();

    return true;
  }

  /**
   * dbRecToObject() - ACRItem
   *
   * Maps db recordset field values to ACR object.
   * ResultSet field ordinality is critical as the order of loading depends on this order.
   */
  protected boolean dbRecToObject(ResultSet rs, ACRItem acri)
  {
    try {

      //log.debug("dbRecToObject(ACRItem) - START");

      String sDta;
      final ResultSetMetaData rsMtaDta = rs.getMetaData();

      for(int i=1; i<=rsMtaDta.getColumnCount(); i++) {

        sDta = rs.getString(i);
        if(null == sDta)
          sDta = "";

        //log.debug("dbRecToObject(ACRItem) - "+rsMtaDta.getColumnName(i)+" = '"+sDta+"'");

        // ACR Item related
        if(rsMtaDta.getColumnName(i).equals("ID_ACRITEM")) {
          if(sDta.length()>0)
            acri.setID(Long.parseLong(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("ACRI_NAME")) {
          acri.setName(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("ACRI_VALUE")) {
          acri.setValue(sDta);
        }

      }

    }
    catch(Exception e) {
      log.error("ACRMESDB.dbRecToObject(ACRItem) - EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }

    // object loaded successfully so mark as clean
    acri.clean();

    return true;
  }

  /**
   * dbRecToObject() - MerchantInfo
   *
   * Maps db recordset field values to ACR object.
   */
  protected boolean dbRecToObject(ResultSet rs, MerchantInfo merchantInfo)
  {
    try {

      //log.debug("dbRecToObject(MerchantInfo) - START");

      String sDta;
      final ResultSetMetaData rsMtaDta = rs.getMetaData();

      for(int i=1; i<=rsMtaDta.getColumnCount(); i++) {

        sDta = rs.getString(i);
        if(null == sDta)
          sDta = "";

        if(rsMtaDta.getColumnName(i).equals("MERCHANT_NUMBER")) {
          merchantInfo.setMerchantNum(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("DBA_NAME")) {
          merchantInfo.setDba(sDta);

        /*
        } else if(rsMtaDta.getColumnName(i).equals("ORG_NUM")) {
          int merchantOrg = StringUtilities.stringToInt(sDta,0);
          UserAppType uat = new UserAppType();
          if(uat.getAppType(merchantOrg,0))
            merchantInfo.setTypeCode(uat.getAppType());
        */

        /*
        } else if(rsMtaDta.getColumnName(i).equals("APP_TYPE_CODE")) {
          if(sDta.length()>0)
            merchantInfo.setTypeCode(Integer.parseInt(sDta));
        */

        } else if(rsMtaDta.getColumnName(i).equals("BANK_NUMBER")) {
          if(sDta.length()>0)
            merchantInfo.setMerchBankNumber(Integer.parseInt(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("MERCHANT_CONTACT_NAME")) {
          merchantInfo.setContactName(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("MERC_CNTRL_NUMBER")) {
          merchantInfo.setControlNum(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("MERCHANT_PHONE")) {
          merchantInfo.setPhone(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("APP_SEQ_NUM")) {
          if(sDta.length()>0)
            merchantInfo.setAppSeqNum(Long.parseLong(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("DDA")) {
          merchantInfo.setDda(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("TRN")) {
          merchantInfo.setTransitRoutingNum(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("ASSOCIATION")) {
          merchantInfo.setAssociation(sDta);
        }

      }

    }
    catch(Exception e) {
      log.error("ACRMESDB.dbRecToObject(MerchantInfo) - EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }

    // object loaded successfully so mark as clean
    merchantInfo.clean();

    return true;
  }

  /**
   * getACRRelatedObject(ACR acr)
   *
   * method that allows any calling body to retrieve
   * a predefined Object tied directly to the ACRID and its
   * corresponding ACRDef, outside of the current ACR scope
   *
   */
  public Object getACRRelatedObject(ACR acr)
  throws Exception
  {
    Object o = null;
    ResultSet rs = null;
    ResultSetIterator rsItr = null;

    int defInt = Long.valueOf(acr.getDefID()).intValue();
    long acrID = acr.getID();

    switch (defInt){

      case 6:
      // this is a new new supply order
      case 37:
        //this is a New Supply Order
      case 70:
        //latest incarnation of Supply Order for MES
        try{

          connect();
          /*@lineinfo:generated-code*//*@lineinfo:1809^11*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                  so.supply_order_id
//                  ,so.handling_cost
//                  ,so.tax_rate
//                  ,so.markup_multiplier
//                  ,so.order_date
//                  ,so.ship_address_name
//                  ,so.ship_address_line1
//                  ,so.ship_address_line2
//                  ,so.ship_address_city
//                  ,so.ship_address_state
//                  ,so.ship_address_zip
//                  ,so.is_rush
//                  ,so.process_sequence
//                  ,so.load_filename
//                  ,so.merch_num
//                  ,so.status
//                  ,soi.supply_item_id
//                  ,soi.quantity
//                  ,soi.cost
//                  ,soi.ship_cost
//                  ,soi.group_name
//                  ,si.partnum
//                  ,si.description
//                  ,sut.name
//                  ,sut.gcf_name
//                FROM
//                  supply_order so
//                  ,supply_order_item soi
//                  ,supply_item si
//                  ,supply_unit_type sut
//                WHERE
//                  so.acrid = :acrID
//                AND
//                  so.supply_order_id = soi.supply_order_id
//                AND
//                  si.supply_item_id = soi.supply_item_id
//                AND
//                  si.unit_type_id = sut.unit_type_id
//              };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n                so.supply_order_id\n                ,so.handling_cost\n                ,so.tax_rate\n                ,so.markup_multiplier\n                ,so.order_date\n                ,so.ship_address_name\n                ,so.ship_address_line1\n                ,so.ship_address_line2\n                ,so.ship_address_city\n                ,so.ship_address_state\n                ,so.ship_address_zip\n                ,so.is_rush\n                ,so.process_sequence\n                ,so.load_filename\n                ,so.merch_num\n                ,so.status\n                ,soi.supply_item_id\n                ,soi.quantity\n                ,soi.cost\n                ,soi.ship_cost\n                ,soi.group_name\n                ,si.partnum\n                ,si.description\n                ,sut.name\n                ,sut.gcf_name\n              FROM\n                supply_order so\n                ,supply_order_item soi\n                ,supply_item si\n                ,supply_unit_type sut\n              WHERE\n                so.acrid =  :1 \n              AND\n                so.supply_order_id = soi.supply_order_id\n              AND\n                si.supply_item_id = soi.supply_item_id\n              AND\n                si.unit_type_id = sut.unit_type_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"35com.mes.acr.ACRMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,acrID);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"35com.mes.acr.ACRMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1850^12*/

          rs = rsItr.getResultSet();

          SupplyOrder order = null;

          while(rs.next()){
            //build SupplyOrder obj
            if(null==order){

             //get the initial order information
              boolean isRush =  (rs.getString("is_rush")!=null&&rs.getString("is_rush").equals("y"))?true:false;
              String merchNum = rs.getString("merch_num");

              //log.debug("creating new order...");
              order = new SupplyOrder(merchNum, isRush);

              order.id = rs.getLong("supply_order_id");
              order.setTaxRate(rs.getDouble("tax_rate"));
              order.setMarkUp(rs.getDouble("markup_multiplier"));
              order.setHandleFee(rs.getDouble("handling_cost"));
              order.setShipName(rs.getString("ship_address_name")==null?"":rs.getString("ship_address_name"));
              order.setShipLine1(rs.getString("ship_address_line1"));
              order.setShipLine2(rs.getString("ship_address_line2")==null?"":rs.getString("ship_address_line2"));
              order.setShipCity(rs.getString("ship_address_city"));
              order.setShipState(rs.getString("ship_address_state"));
              order.setShipZip(rs.getString("ship_address_zip"));
              order.processSeq= rs.getString("process_sequence")==null?"":rs.getString("process_sequence");
              order.loadFileName= rs.getString("load_filename")==null?"":rs.getString("load_filename");
              order.status =  rs.getInt("status");
              order.orderDate = rs.getDate("order_date");
            }
            //populate the order_items
            //log.debug("adding new order items...");
            SupplyOrderItem soi = new SupplyOrderItem();
            soi.id = rs.getString("supply_item_id");
            soi.groupName = rs.getString("group_name");
            soi.quantity = rs.getInt("quantity");
            soi.setPrice(rs.getDouble("cost"));
            soi.shipCost = rs.getDouble("ship_cost");
            soi.description = rs.getString("partnum") +": "+ rs.getString("description")+" ("+rs.getString("gcf_name")+"-"+rs.getString("name")+")";

            order.add(soi);
          }

          o = order;

        }catch(Exception e) {
          log.debug("EXCEPTION: getACRRelatedObject(): "+e.getMessage());
          throw e;
        }finally {
          try { rs.close(); }    catch(Exception e) {}
          try { rsItr.close(); } catch(Exception e) {}
          cleanUp();
        }
        break;
    }
    return o;
  }

}/*@lineinfo:generated-code*/