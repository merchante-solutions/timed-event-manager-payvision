package com.mes.acr;

import org.apache.log4j.Logger;
import com.mes.app.DollarAmountTable;
import com.mes.constants.mesConstants;
import com.mes.tools.DropDownTable;

public class AuthFeeTableFactory
{
  // log4j initialization
  static Logger log = Logger.getLogger(AuthFeeTableFactory.class);

  /**
   * Creates a drop down table with auth fees for the given card type/app type
   * combination.
   */
  public static DropDownTable createFeeTable(int appType, int cardType)
  {
    return createFeeTable(appType, cardType, -1);
  }

  public static DropDownTable createFeeTable(int appType, int cardType, int typeCode)
  {
    String amountStr = null;
    log.debug("appType ="+appType+"cardType ="+cardType+"typeCode ="+typeCode);

    switch (appType)
    {
      case mesConstants.APP_TYPE_CBT_NEW:
      case mesConstants.APP_TYPE_CBT:
      case mesConstants.APP_TYPE_MES:
      case mesConstants.APP_TYPE_NBSC:
      case mesConstants.APP_TYPE_NBSC2:
        switch (cardType)
        {
          case mesConstants.APP_CT_AMEX:
          case mesConstants.APP_CT_DISCOVER:
            switch(typeCode)
            {
              case mesConstants.APP_TYPE_ALPINE:

                amountStr = "0,0.05,0.06,0.07,0.08,0.09,0.1,0.11,0.12,0.13,0.14,"
                          + "0.15,0.16,0.17,0.18,0.19,0.2,0.25,0.27,0.3,0.35,0.4,0.45,"
                          + "0.5,0.55,0.6,0.65,0.7";
                break;

              default:
                amountStr = "0,0.05,0.06,0.07,0.08,0.09,0.1,0.11,0.12,0.13,0.14,"
                          + "0.15,0.16,0.17,0.18,0.19,0.2,0.25,0.3,0.35,0.4,0.45,"
                          + "0.5,0.55,0.6,0.65,0.7";
            }
            break;

          case mesConstants.APP_CT_DINERS_CLUB:
          case mesConstants.APP_CT_JCB:
            amountStr = "0,0.05,0.06,0.07,0.08,0.09,0.1,0.11,0.12,0.13,0.14,"
                      + "0.15,0.16,0.17,0.18,0.19,0.2,0.25,0.3,0.35,0.4,0.45,"
                      + "0.5,0.55,0.6,0.65,0.7";
            break;

          case mesConstants.APP_CT_EBT:
            amountStr = "0,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.70";
            break;

          case mesConstants.APP_CT_DEBIT:

            switch(typeCode)
            {
              case mesConstants.APP_TYPE_SOUTH_VALLEY:

                amountStr = "0,0.6,0.65,0.68,0.70,0.72,0.75,0.80";
                break;

              default:
                amountStr = "0,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.70";
           }
            break;


          default:
            String errMsg = "Unsupported card type " + cardType
              + " for app type " + appType;
            log.error(errMsg);
            throw new RuntimeException(errMsg);
        }
        break;

      default:
        String errMsg = "Unsupported app type " + appType;
        log.error(errMsg);
        throw new RuntimeException(errMsg);
    }

    return new DollarAmountTable(true,false,amountStr);
  }
}
