package com.mes.acr;

import com.mes.forms.CheckboxField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.Validation;

public class MultiMerchantBean extends FieldBean
{

  private ACR acr;

  public MultiMerchantBean(ACR acr)
  {
    this.acr = acr;
  }

  public String renderHtml()
  {
    StringBuffer sb = new StringBuffer(128);
    sb.append("<table width=100%>");
    sb.append("  <tr valign=top>");
    sb.append("    <td>");
    sb.append("    " + renderHtml("isMultiMerchant"));
    sb.append("    </td>");
    sb.append("    <td class='formFields'>");
    sb.append("If checked, please enter merchant numbers:<br>" + renderHtml("mmAccounts"));
    sb.append("    </td>");
    sb.append("  </tr>");
    sb.append("  <tr>");
    sb.append("    <td colspan=2><b>This is for informational purposes only!</b>.<br>Please ensure that a seperate ACR is completed for each multi-merchant.");
    sb.append("    </td>");
    sb.append("  </tr>");
    sb.append("</table>");
    return sb.toString();
  }

  public void createFields()
  {

    Field isMmField = new CheckboxField("isMultiMerchant",
                                        "Is this a Multi-Merchant account?",
                                        false);
    fields.add(isMmField);

    Field mmNotes = new Field("mmAccounts",300,60,true);
    fields.add(mmNotes);

    Validation notBlankVal =
      new IfYesNotBlankValidation(isMmField,
                                  "Please provide multi-merchant account numbers");

    mmNotes.addValidation(notBlankVal,"notBlankVal");

  }

}