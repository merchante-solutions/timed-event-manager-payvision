package com.mes.acr;

import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.tools.DropDownTable;


public class ACRDataBean_EquipSwap extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_EquipSwap.class);

  // constants
  public static final int     MAX_NUM_SWAPS = 2;

  // data members
  protected ACRDataBeanAddressHelper adrsHelper = null;
  protected EquipCallTagTable        ectt       = null;

  // construction
  public ACRDataBean_EquipSwap()
  {
  }

  public ACRDataBean_EquipSwap(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    // equip call tag table
    int[] ecctColumns = new int[]
      {
         EquipCallTagTable.COL_SENDCALLTAG
        ,EquipCallTagTable.COL_EQUIPTYPE
        ,EquipCallTagTable.COL_DESCRIPTION
        ,EquipCallTagTable.COL_SERIALNUMBER
        ,EquipCallTagTable.COL_PARTNUMBER
        ,EquipCallTagTable.COL_DISPOSITION
        ,EquipCallTagTable.COL_VNUMBER
      };
    String[][] ecctMap = new String[][]
      {
         {EquipCallTagTable.NCN_SENDCALLTAG,   "equipsel"}
        ,{EquipCallTagTable.NCN_PARTNUMBER,    "appid"}
        ,{EquipCallTagTable.NCN_SERIALNUMBER,  "serialnum"}
        ,{EquipCallTagTable.NCN_VNUMBER,       "vnum"}
        ,{EquipCallTagTable.NCN_DISPOSITION,   "dsptn"}
        ,{EquipCallTagTable.NCN_DESCRIPTION,   "equip_descriptor"}
        ,{EquipCallTagTable.NCN_EQUIPTYPE,     "tt"}
      };
    ectt = new EquipCallTagTable(this, ecctColumns, ecctMap, MAX_NUM_SWAPS, true);


    // these were commented out because they throw an exception
    // log.info("ACR appSeqNum = "+ acr.getAppSeqNum());
    // log.info("Merchant appSeqNum = "+ merchantInfo.getAppSeqNum());

  }

  public boolean equipmentExists()
  {
    return ectt.equipmentExists();
  }

  public boolean calltaggedEquipmentExists()
  {
    return ectt.calltaggedEquipmentExists();
  }

  public Vector getCallTaggedEquipment()
  {
    return ectt.getCallTaggedEquipment();
  }

  public String renderEquipCallTagTable()
  {
    return ectt.renderHtmlTable();
  }

  protected ACRDataBeanAddressHelper _getAddressHelper()
  {
    if(adrsHelper!=null)
      return adrsHelper; // already configured by virtue of non-null obj ref

    adrsHelper = new ACRDataBeanAddressHelper(this);

    // configure the address helper
    adrsHelper.setAdrsTypeName("Ship To");
    adrsHelper.setAdrsSlctnFldName("staddress");
    adrsHelper.setAdrsNameFldNme("sh_name");
    adrsHelper.setAdrsAddressFldNme("sh_address");
    adrsHelper.setAdrsAttnFldNme("sh_attn");
    adrsHelper.setAdrsBankNameFldNme("sh_bnknme");
    adrsHelper.setAdrsCityFldNme("sh_city");
    adrsHelper.setAdrsStateFldNme("sh_state");
    adrsHelper.setAdrsZipFldNme("sh_zip");
    //adrsHelper.setAdrsCountry("sh_country");
    adrsHelper.setIsRequired(false);

    return adrsHelper;
  }

  public String renderAddressHtml()
  {
    return _getAddressHelper().renderAddressHtml();
  }

  public void doPull()
  {
    super.doPull();

    if(!editable)
      return;

    _getAddressHelper().pullAddressFields();
  }

  protected void setDefaults()
  {
    // parent related info
    if(getMerchantNumber().length()>0) {

      // merchant address
      _getAddressHelper().setACRAdrsInfo();

    }

    // default to rush request
    acr.setACRItem("Rush","y");
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;
    String acrdiname=null;

    // address fields
    _getAddressHelper().createAddressFields();

    SwapReasonDropDownTable srddt = new SwapReasonDropDownTable();
    RepairHandlingDropDownTable rhddt = new RepairHandlingDropDownTable();
    ShippingFeeDropDownTable sfddt = new ShippingFeeDropDownTable();

    if((acrdi=acrd.getACRDefItem("swap_reason"))!=null) {
      field=new DropDownField(acrdi.getName(),srddt,!acrdi.isRequired());
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("swap_reason_details"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("repair_handling"))!=null) {
      field=new DropDownField(acrdi.getName(),rhddt,!acrdi.isRequired());
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("shipping_fee"))!=null) {
      field=new DropDownField(acrdi.getName(),sfddt,!acrdi.isRequired());
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }

    // equip call tag table fields
    ectt.createFields(this.fields);

    int equipCount = ectt.getNumRows();

    // equip swap vector
    Vector swaps = new Vector(equipCount,1);
    for(int i=1;i<=equipCount;i++) {
      field = getField("equipsel_"+i);
      swaps.addElement(field);
    }
/*
    //OLD - based on ops request 6/1/04
    Vector swaps = new Vector(MAX_NUM_SWAPS,1);
    for(int i=1;i<=MAX_NUM_SWAPS;i++) {
      field = getField("equipsel_"+i);
      swaps.addElement(field);
    }
*/

    // add "at least one" validation for swaps
    fields.addValidation(new FieldBean.AtLeastOneValidation("At least one equipment swap must be specified to proceed.",swaps));
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT:
      case MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS:
      case MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  public String getHeadingName()
  {
    return "Order Equipment Swap for Merchant";
  }

  public class SwapReasonDropDownTable extends DropDownTable
  {
    public SwapReasonDropDownTable()
    {
      addElement("","");

      addElement("Equipment not working - merchant owned","Equipment not working - merchant owned");
      addElement("Equipment not working - rented","Equipment not working - rented");
      addElement("Equipment not working - purchased and under warranty","Equipment not working - purchased and under warranty");
      addElement("Equipment not working - leased","Equipment not working - leased");
    }

  } // SwapReasonDropDownTable class

  public class RepairHandlingDropDownTable extends DropDownTable
  {
    public RepairHandlingDropDownTable()
    {
      addElement("","");

      addElement("Send repair form (for merchant owned or out of warranty equipment)","Send repair form (for merchant owned or out of warranty equipment)");
      addElement("Send call tag (for rented, leased or purchased equipment that is under warranty)","Send call tag (for rented, leased or purchased equipment that is under warranty)");
    }

  } // RepairHandlingDropDownTable class

  public class ShippingFeeDropDownTable extends DropDownTable
  {
    public ShippingFeeDropDownTable()
    {
      addElement("","");

      addElement("$25","$25");
      addElement("Waived","Waived");
    }

  } // ShippingFeeDropDownTable class

} // class ACRDataBean_EquipSwap
