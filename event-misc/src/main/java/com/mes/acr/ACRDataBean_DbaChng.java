/*@lineinfo:filename=ACRDataBean_DbaChng*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.acr;

import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.NumberField;
import com.mes.forms.Validation;
import com.mes.support.StringUtilities;
import com.mes.tools.DropDownTable;


public class ACRDataBean_DbaChng extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_DbaChng.class);

  // data members
  ACRDataBeanAddressHelper adrsHelper = null;

  // construction
  public ACRDataBean_DbaChng()
  {
  }

  public ACRDataBean_DbaChng(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  protected ACRDataBeanAddressHelper _getAddressHelper()
  {
    if(adrsHelper!=null)
      return adrsHelper; // already configured by virtue of non-null obj ref

    adrsHelper = new ACRDataBeanAddressHelper(this);

    // configure the address helper
    adrsHelper.setAdrsTypeName("Ship To");
    adrsHelper.setAdrsSlctnFldName("staddress");
    //adrsHelper.setAdrsName("");
    adrsHelper.setAdrsAddressFldNme("sh_address");
    adrsHelper.setAdrsAttnFldNme("sh_attn");
    adrsHelper.setAdrsBankNameFldNme("sh_bnknme");
    adrsHelper.setAdrsCityFldNme("sh_city");
    adrsHelper.setAdrsStateFldNme("sh_state");
    adrsHelper.setAdrsZipFldNme("sh_zip");
    //adrsHelper.setAdrsCountry("sh_country");
    adrsHelper.setIsRequired(false);  // make required only when requesting Imprinter plates

    return adrsHelper;
  }

  public String renderAddressHtml()
  {
    return _getAddressHelper().renderAddressHtml();
  }

  public void doPull()
  {
    super.doPull();

    if(!editable)
      return;

    _getAddressHelper().pullAddressFields();
  }

  protected void setDefaults()
  {
    // parent related info
    if(getMerchantNumber().length()>0) {

      // merchant address
      _getAddressHelper().setACRAdrsInfo();

      try {

        connect();

        String dba_name,corporate_name;

        // merchant addresses - pull one record for each address type always!
        /*@lineinfo:generated-code*//*@lineinfo:89^9*/

//  ************************************************************
//  #sql [Ctx] { select      MERCH_BUSINESS_NAME
//                        ,MERCH_LEGAL_NAME
//            
//            from        merchant
//            where       merch_number=:getMerchantNumber()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3501 = getMerchantNumber();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select      MERCH_BUSINESS_NAME\n                      ,MERCH_LEGAL_NAME\n           \n          from        merchant\n          where       merch_number= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRDataBean_DbaChng",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3501);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   dba_name = (String)__sJT_rs.getString(1);
   corporate_name = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:97^9*/

        acr.setACRItem("existDbaName",dba_name);
        acr.setACRItem("existCorporateName",corporate_name);
      }
      catch (Exception e) {
        log.error("setDefaults() EXCEPTION: '"+e.toString()+"'.");
      }
      finally {
        cleanUp();
      }
    }
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;
    Vector existNames = new Vector(3,1);

    // address fields
    _getAddressHelper().createAddressFields();

    if((acrdi=acrd.getACRDefItem("newDbaName"))!=null) {
      field=ACRDefItemToField(acrdi);
      fields.add(field);
      existNames.addElement(field);
    }
    if((acrdi=acrd.getACRDefItem("existDbaName"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("newCorporateName"))!=null) {
      field=ACRDefItemToField(acrdi);
      fields.add(field);
      existNames.addElement(field);
    }
    if((acrdi=acrd.getACRDefItem("existCorporateName"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("newOtherName"))!=null) {
      field=ACRDefItemToField(acrdi);
      fields.add(field);
      existNames.addElement(field);
    }
    if((acrdi=acrd.getACRDefItem("existOtherName"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("otherDescription"))!=null)
      fields.add(ACRDefItemToField(acrdi));

    // imprinter plates
    field = new CheckboxField("sendImpPlts","Check here if new imprinter plate is needed and indicate quantity.",false);
    field.setShowName("Send Imprinter Plate?");
    fields.add(field);
    field = new NumberField("ipQty",2,2,true,0,0,99);
    field.setShowName("Imprinter Plate Quantity");
    fields.add(field);

    // require "at least one" card add be checked
    fields.addValidation(new FieldBean.AtLeastOneValidation("At least one new name must be specified to proceed.",existNames));
    fields.addValidation(new EntryValidation());
  }

  public String renderHtml(String fname)
  {
    if(fname.equals("sendImpPlts") && inStatusMode()) {
      if(getACRValue("sendImpPlts").equals("y"))
        return "Send Imprinter Plates";
      else
        return "Do NOT send Imprinter Plates.";
    }

    return super.renderHtml(fname);
  }

  protected final class EntryValidation implements Validation
  {
    protected String ErrorText = "";

    public EntryValidation()
    {
    }

    public String getErrorText()
    {
      return(ErrorText);
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      Field fld;

      // imprinter plates validation
      fld = fields.getField("sendImpPlts");
      if(fld!=null && ((CheckboxField)fld).isChecked()) {

        // require quantity
        fld = fields.getField("ipQty");
        fld.makeRequired();
        if(!fld.isValid()) {
          ErrorText = fld.getErrorText();
          return false;
        }
        fld.removeValidation("required");

        // require shipping address
        StringBuffer sb = new StringBuffer();
        if(!adrsHelper.isValidAddress(sb)) {
          ErrorText = sb.toString();
          return false;
        }

      }

      return true;
    }

  } // EntryValidation class

  public int[][] getAllowedQueueOps()
  {
    int bankNum = StringUtilities.stringToInt(acr.getACRValue("Merchant Bank Number"),-1);

    int initQueue = isMESBank(bankNum)?
          MesQueues.Q_CHANGE_REQUEST_RISK:MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP;

    return new int[][]
    {
       {MesQueues.Q_NONE,initQueue}

      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_COMPLETED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_RISK}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_RISK:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Business License Not Received"
          ,"Request Cancelled"
        };
      case MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  public String getHeadingName()
  {
    return "Change Merchant DBA or Corporate Name";
  }

  public boolean showBusinessLicenseDisclaimer()
  {
    // don't show disclaimer for CB&T (1), CB&T2 (31) and NBSC(10) type merchants
    // Type Code refers to application type found in ORG_APP

    if(acr==null)
      return false;

    String tc = acr.getACRValue("Type Code");

    return !( tc.equals("1") ||
              tc.equals("10")||
              tc.equals("31"));
  }

  public class ChangeTypeDropDownTable extends DropDownTable
  {
    public ChangeTypeDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      addElement("","");
      addElement("Dba Name","Dba Name");
      addElement("Corporate Name","Corporate Name");
      addElement("Other","Other");
    }

  }

}/*@lineinfo:generated-code*/