package com.mes.acr;

import org.apache.log4j.Logger;
import com.mes.forms.HtmlContainer;
import com.mes.forms.HtmlDiv;
import com.mes.forms.HtmlRenderable;
import com.mes.forms.HtmlTable;
import com.mes.forms.HtmlTableCell;
import com.mes.forms.HtmlTableRow;
import com.mes.forms.SelectorFrame;

public class EquipSelectorFrame extends SelectorFrame
{
  static Logger log = Logger.getLogger(EquipSelectorFrame.class);

  private HtmlTable frameTable;
  private HtmlTableCell blockCell;
  private HtmlTableCell titleCell;

  public EquipSelectorFrame(String title)
  {
    setTitle(title);
  }

  public void setupFrame()
  {
    frameTable = new HtmlTable();
    frameTable.setProperty("width","100%");
    frameTable.setProperty("cellpadding","1");
    frameTable.setProperty("cellspacing","0");
    frameTable.setProperty("class","tableFillGray");

    HtmlTableRow r = frameTable.add(new HtmlTableRow());
    HtmlTableCell c = r.add(new HtmlTableCell());

    HtmlTable t = new HtmlTable();
    t.setProperty("width","100%");
    t.setProperty("cellpadding","2");
    t.setProperty("cellspacing","0");
    c.add(t);

    r = t.add(new HtmlTableRow());
    titleCell = r.add(new HtmlTableCell());
    titleCell.setProperty("class","tableColumnHead");
    titleCell.setProperty("style","font-weight: bold;");

    r = t.add(new HtmlTableRow());
    c = r.add(new HtmlTableCell());
    c.setProperty("class","tableData");

    HtmlDiv hdrDiv = new HtmlDiv();
    hdrDiv.setProperty("style","float: left; width: 30px");
    hdrDiv.add("&nbsp;");
    c.add(hdrDiv);

    hdrDiv = new HtmlDiv();
    hdrDiv.setProperty("style","float: left; font-weight: bold; width: "
      + ACREquipSelectorItem.COL1 + "px");
    hdrDiv.add("Serial No.");
    c.add(hdrDiv);

    hdrDiv = new HtmlDiv();
    hdrDiv.setProperty("style","float: left; font-weight: bold; width: "
      + ACREquipSelectorItem.COL2 + "px");
    hdrDiv.add("Description");
    c.add(hdrDiv);

    hdrDiv = new HtmlDiv();
    hdrDiv.setProperty("style","float: left; font-weight: bold; width: "
      + ACREquipSelectorItem.COL3 + "px");
    hdrDiv.add("Part No.");
    c.add(hdrDiv);

    hdrDiv = new HtmlDiv();
    hdrDiv.setProperty("style","float: left; font-weight: bold; width: "
      + ACREquipSelectorItem.COL4 + "px");
    hdrDiv.add("Disposition");
    c.add(hdrDiv);

    hdrDiv = new HtmlDiv();
    hdrDiv.setProperty("style","float: left; font-weight: bold; width: "
      + ACREquipSelectorItem.COL5 + "px");
    hdrDiv.add("Reason");
    c.add(hdrDiv);

    r = t.add(new HtmlTableRow());
    blockCell = r.add(new HtmlTableCell());
    blockCell.setProperty("class","tableData");
  }

  public void setTitle(String title)
  {
    titleCell.add(title);
  }

  public void setSelectorBlock(HtmlRenderable selectorBlock)
  {
    blockCell.add(selectorBlock);
  }

  public String renderHtml()
  {
    return frameTable.renderHtml();
  }

@Override
public HtmlContainer generateFrameContainer() {
	// TODO Auto-generated method stub
	return null;
}

@Override
public void addSelectorRenderer(HtmlRenderable hr) {
	// TODO Auto-generated method stub
	
}

@Override
public void addHeaderRenderer(HtmlRenderable hr) {
	// TODO Auto-generated method stub
	
}
}

