package com.mes.acr;

import org.apache.log4j.Logger;
import com.mes.equipment.EquipSelectorItem;
import com.mes.forms.Condition;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HtmlRenderable;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;

public class SI_EquipSwap_mes extends EquipSelectorItem
  implements ACRTranslator
{
  static Logger log = Logger.getLogger(SI_EquipSwap.class);

  public SI_EquipSwap_mes()
  {
  }

  public void init()
  {

    if (type == T_STANDARD)
    {
      setColumnRenderer("col1", new HtmlRenderable()
        { public String renderHtml() { return getSerialNum(); } } );
      setColumnRenderer("col2", new HtmlRenderable()
        { public String renderHtml() { return getDescription(); } } );
      setColumnRenderer("col3", new HtmlRenderable()
        { public String renderHtml() { return getEquipType(); } } );
      setColumnRenderer("col7", new HtmlRenderable()
        { public String renderHtml() { return getDisposition(); } } );
    }
    else if (type == T_OTHER)
    {
      Field serialNum = new Field(value + "_serNum","Serial number",20,12,true);
      setColumnRenderer("col1",serialNum);
      fields.add(serialNum);

      Field description
        = new Field(value + "_description","Equipment description",60,10,true);
      setColumnRenderer("col2",description);
      fields.add(description);

      Field equipType = new DropDownField(value + "_equipType",
        "Equipment type",
        TableFactory.getDropDownTable(TableFactory.EQUIP_TYPE),true);
      equipType.addHtmlExtra("class=\"formField\"");
      setColumnRenderer("col3",equipType);
      fields.add(equipType);

      Field status = new DropDownField(value + "_status",
        "Equipment Status",
        TableFactory.getDropDownTable(TableFactory.EQUIP_STATUS),true);
      status.addHtmlExtra("class=\"formField\"");
      setColumnRenderer("col7",status);
      fields.add(status);

    }

    Field vNum = new Field(value + "_VNum","VNum/TID",20,6,true);
    setColumnRenderer("col4",vNum);
    fields.add(vNum);

    //Field termApp = new Field(value + "_termApp","Terminal Application",20,8,true);
    Field termApp = new DropDownField(value + "_termApp","Terminal Application",TableFactory.getDropDownTable(TableFactory.TERM_APP),true);
    setColumnRenderer("col5",termApp);
    fields.add(termApp);

    DropDownTable ddTable = TableFactory.getDropDownTable(TableFactory.VENDOR);
    ddTable.addElement("TermMaster Vital","TermMaster Vital");
    ddTable.addElement("IP","IP");
    ddTable.addElement("Unknown","Unknown");

    Field vendor = new DropDownField(value + "_vendorId","Vendor",ddTable,true);
    setColumnRenderer("col6",vendor);
    fields.add(vendor);



    fields.setHtmlExtra("class=\"formText\"");

  }

  public void setControlField(Field controlField)
  {
    super.setControlField(controlField);

    // in the case of other items need to make some of the fields required
    if (type == T_OTHER)
    {
      Condition vc = new FieldValueCondition(controlField,value);
      Validation required = new ConditionalRequiredValidation(vc);
      fields.getField(value + "_serNum").addValidation(required);
      fields.getField(value + "_description").addValidation(required);
      fields.getField(value + "_equipType").addValidation(required);
      fields.getField(value + "_status").addValidation(required);
    }
  }

  public void doPush(ACR acr)
  {
    log.debug("pretending to push acr item data...");
  }

  public void doPull(ACR acr)
  {
    try
    {
      acr.setACRItem(value+"_serNum",getSerialNum());
      acr.setACRItem(value+"_description",getDescription());
      acr.setACRItem(value+"_equipType",getEquipType());
      if (type != T_OTHER)
      {
        acr.setACRItem(value+"_status",getDisposition());
      }
    }
    catch(Exception e)
    {
      //do nothing
    }
  }

}
