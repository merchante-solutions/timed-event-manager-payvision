package com.mes.acr;

import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.Condition;
import com.mes.forms.DateStringField;
import com.mes.forms.Field;
import com.mes.forms.RadioField;

public class ACRDataBean_SeasActvt_CBT extends ACRDataBeanBase
{
  // log4j initialization
  static Logger log =
    Logger.getLogger(ACRDataBean_SeasActvt_CBT.class);

  public ACRDataBean_SeasActvt_CBT()
  {
  }

  public ACRDataBean_SeasActvt_CBT(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  /**
   * Creates fields for the acr detail section specific to this acr.
   */
  protected void createExtendedFields()
  {
    try
    {
      log.debug("creating extended fields." );

      // activate or deactivate radio
      Field actOrDeact =
        new RadioField("actOrDeact","Activate or Deactivate",
          new String[][]
          {
            { "activate",   "Activate for Season"   },
            { "deactivate", "Deactivate for Season" },
          });
      fields.add(actOrDeact);

      // activation date, required if activating
      Field activateDate =
        new DateStringField("activateDate","Date to Activate",true,false);
      fields.add(activateDate);

      Condition isActivate = new FieldValueCondition(actOrDeact,"activate");
      activateDate
        .addValidation(new ConditionalRequiredValidation(isActivate));

      // yes no field for equipment deployment
      //Field deployEquip =
      //  new RadioField("deployEquip","Deploy Equipment",
      //    new String[][]
      //    {
      //      { "no",   "No, don't deploy equipment"  },
      //      { "yes",  "Yes, deploy equipment" },
      //    });
      //fields.add(deployEquip);

      // deactivation date, required if deactivating
      Field deactivateDate =
        new DateStringField("deactivateDate","Date to Deactivate",true,false);
      fields.add(deactivateDate);

      Condition isDeactivate =
        new FieldValueCondition(actOrDeact,"deactivate");
      deactivateDate
        .addValidation(new ConditionalRequiredValidation(isDeactivate));

      // yes no field for equipment deployment
      //Field pickupEquip =
      //  new RadioField("pickupEquip","Pick-up Equipment",
      //    new String[][]
      //    {
      //      { "no",   "No, don't pick-up equipment"  },
      //      { "yes",  "Yes, pick-up equipment" }
      //    });
      //fields.add(pickupEquip);


      fields.setHtmlExtra("class=\"formText\"");
    }
    catch (Exception e)
    {
      log.error("createExtendedFields(): "  + e);
    }
  }

  public void setDefaults()
  {
  }

  /**
   * Returns title for this acr.
   */
  public String getHeadingName()
  {
    return "Activate or Deactivate Seasonal Merchant";
  }

  /**
   * Specifies which queue operations are permitted for the acr.
   */
  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
      { MesQueues.Q_NONE, MesQueues.Q_CHANGE_REQUEST_CBT_TSYS }

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,    MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT   }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,    MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS  }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,    MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA  }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,    MesQueues.Q_CHANGE_REQUEST_CBT_MMS      }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,    MesQueues.Q_CHANGE_REQUEST_COMPLETED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,    MesQueues.Q_CHANGE_REQUEST_CANCELLED    }

      ,{ MesQueues.Q_CHANGE_REQUEST_RISK,        MesQueues.Q_CHANGE_REQUEST_CBT_TSYS     }
      ,{ MesQueues.Q_CHANGE_REQUEST_RISK,        MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA  }
      ,{ MesQueues.Q_CHANGE_REQUEST_RISK,        MesQueues.Q_CHANGE_REQUEST_CANCELLED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_RISK,        MesQueues.Q_CHANGE_REQUEST_COMPLETED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_RISK,        MesQueues.Q_CHANGE_REQUEST_CBT_MMS    }

      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_RISK         }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT   }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS  }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_CBT_TSYS     }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_CBT_MMS      }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_COMPLETED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_CANCELLED    }

      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_MMS,     MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT   }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_MMS,     MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS  }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_MMS,     MesQueues.Q_CHANGE_REQUEST_COMPLETED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_MMS,     MesQueues.Q_CHANGE_REQUEST_CANCELLED    }

      ,{ MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,  MesQueues.Q_CHANGE_REQUEST_COMPLETED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,  MesQueues.Q_CHANGE_REQUEST_CANCELLED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,  MesQueues.Q_CHANGE_REQUEST_CBT_MMS    }
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{ MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS, MesQueues.Q_CHANGE_REQUEST_COMPLETED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS, MesQueues.Q_CHANGE_REQUEST_CANCELLED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS, MesQueues.Q_CHANGE_REQUEST_CBT_MMS    }

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}

//ASG Mgmt queue
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
    };
  }

  /**
   * Convenience methods for post submission mode
   */
  public boolean isActivation()
  {
    return getACRValue("actOrDeact").toLowerCase().equals("activate");
  }
  public boolean isDeactivation()
  {
    return getACRValue("actOrDeact").toLowerCase().equals("deactivate");
  }

  /**
   * Disable editable comments during submission.
   */
  public boolean hasEditableComments()
  {
    return !editable && super.hasEditableComments();
  }
}
