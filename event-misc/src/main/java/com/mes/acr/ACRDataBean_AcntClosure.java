/*@lineinfo:filename=ACRDataBean_AcntClosure*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.acr;

import java.sql.ResultSet;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateField;
import com.mes.forms.Field;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;
import com.mes.support.StringUtilities;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;


public class ACRDataBean_AcntClosure extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_AcntClosure.class);

  // data members
  protected ACRDataBeanAddressHelper adrsHelper = null;
  protected EquipCallTagTable        ectt       = null;

  // construction
  public ACRDataBean_AcntClosure()
  {
  }

  public ACRDataBean_AcntClosure(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    // equip call tag table
    int[] ecctColumns = new int[]
      {
         EquipCallTagTable.COL_SENDCALLTAG
        ,EquipCallTagTable.COL_DESCRIPTION
        ,EquipCallTagTable.COL_EQUIPTYPE
        ,EquipCallTagTable.COL_VNUMBER
        ,EquipCallTagTable.COL_PARTNUMBER
        ,EquipCallTagTable.COL_SERIALNUMBER
        ,EquipCallTagTable.COL_DISPOSITION
      };
    String[][] ecctMap = new String[][]
      {
         {EquipCallTagTable.NCN_SENDCALLTAG,   "sct"}
        ,{EquipCallTagTable.NCN_PARTNUMBER,    "part_num"}
        ,{EquipCallTagTable.NCN_SERIALNUMBER,  "serial_num"}
        ,{EquipCallTagTable.NCN_VNUMBER,       "vnum"}
        ,{EquipCallTagTable.NCN_DISPOSITION,   "disposition"}
        ,{EquipCallTagTable.NCN_DESCRIPTION,   "equip_descriptor"}
        ,{EquipCallTagTable.NCN_EQUIPTYPE,     "type"}
      };
    ectt = new EquipCallTagTable(this, ecctColumns, ecctMap, 4, true);
  }

  public boolean equipmentExists()
  {
    return ectt.equipmentExists();
  }

  public boolean calltaggedEquipmentExists()
  {
    return ectt.calltaggedEquipmentExists();
  }

  public Vector getCallTaggedEquipment()
  {
    return ectt.getCallTaggedEquipment();
  }

  public String renderEquipCallTagTable()
  {
    return ectt.renderHtmlTable();
  }

  protected ACRDataBeanAddressHelper _getAddressHelper()
  {
    if(adrsHelper!=null)
      return adrsHelper; // already configured by virtue of non-null obj ref

    adrsHelper = new ACRDataBeanAddressHelper(this);

    // configure the address helper
    adrsHelper.setAdrsTypeName("Call Tag");
    adrsHelper.setAdrsSlctnFldName("ctaddress");
    //adrsHelper.setAdrsName("");
    adrsHelper.setAdrsAddressFldNme("ct_address");
    adrsHelper.setAdrsAttnFldNme("ct_attn");
    adrsHelper.setAdrsBankNameFldNme("ct_bnknme");
    adrsHelper.setAdrsCityFldNme("ct_city");
    adrsHelper.setAdrsStateFldNme("ct_state");
    adrsHelper.setAdrsZipFldNme("ct_zip");
    //adrsHelper.setAdrsCountry("sh_country");
    adrsHelper.setIsRequired(false);

    return adrsHelper;
  }

  public String renderAddressHtml()
  {
    return _getAddressHelper().renderAddressHtml();
  }

  public void doPull()
  {
    super.doPull();

    if(!editable) {

      // grab the userid to associate with the accept amount
      if(residentQueue == MesQueues.Q_CHANGE_REQUEST_RESEARCH) {
        acr.setACRItem("acceptUserId",(user==null? "":user.getLoginName()));
      }

      return;
    }

    _getAddressHelper().pullAddressFields();
  }

  public boolean isValid()
  {
    if(!editable) {
      try {
        fields.getField("requestStatus").setNullAllowed(residentQueue != MesQueues.Q_CHANGE_REQUEST_RESEARCH);
        fields.getField("acceptAmt").setNullAllowed(residentQueue != MesQueues.Q_CHANGE_REQUEST_RESEARCH);
      }
      catch(Exception e) {
        // ok if exception as in some states acc cls acr will not have these fields defined
      }
    }

    return super.isValid();
   }
  protected void setDefaults()
  {
    // parent related info
    if(getMerchantNumber().length()>0) {

      // merchant addresses
      _getAddressHelper().setACRAdrsInfo();

    }

    // set default values for accept/amount fields
    acr.setACRItem("requestStatus","Not set");
    acr.setACRItem("acceptAmt","0");
    acr.setACRItem("acceptUserId","");
  }

  protected void createExtendedFields()
  {
    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;
    String fldnme=null;

    if(!editable) {

      if(residentQueue == MesQueues.Q_CHANGE_REQUEST_RESEARCH) {
        // accepted/denied (editable in status mode)
        String[][] arrReqStat =
        {
           { "Accepted","Accepted" }
          ,{ "Denied","Denied" }
        };
        field = new RadioButtonField("requestStatus",arrReqStat,false,"Please specify whether the request is accepted or denied.");
        field.setLabel("Request Status");
        ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
        fields.add(field);

        // accept amount (editable in status mode)
        field = new CurrencyField("acceptAmt","Accept Amount",9,9,true,-999999f,999999f);
        field.setLabel("Accept Amount");
        field.setData("0");
        fields.add(field);
      }

    } else {

      if(acr==null)
        return;

      String[][] arrCnclFeeType =
      {
        { "Regular Fee"               , "Regular Fee"  },
        { "Fee Waived"                , "Fee Waived"   }
      };

      field = new DateField("closureDate","",false);
      ((DateField)field).setDateFormat("MM/dd/yyyy");
      fields.add(field);

      if((acrdi=acrd.getACRDefItem("cancellation_fee_type"))!=null) {
        int selValue = 0;
        if(isTranscom())
        {
          selValue = 1;
        }
        field = new RadioButtonField("cancellation_fee_type",arrCnclFeeType,selValue,false,"Please specify the cancellation fee type.");
        ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
        fields.add(field);
      }

      // address fields
      _getAddressHelper().createAddressFields();

    }

    // equip call tag table fields
    ectt.createFields(this.fields);

    fields.addValidation(new EntryValidation());
  }

  private boolean isTranscom()
  {
    ResultSet rs = null;
    ResultSetIterator it = null;
    long TRANSCOM = 3941500004L;

    boolean isTranscom=false;
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:242^7*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT
//            th.relation
//          FROM
//            t_hierarchy th,
//            mif m
//          WHERE
//            m.merchant_number = :getMerchantNumber()
//          AND
//            m.association_node = th.descendent
//          AND
//            th.ancestor = :TRANSCOM
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3498 = getMerchantNumber();
  try {
   String theSqlTS = "SELECT\n          th.relation\n        FROM\n          t_hierarchy th,\n          mif m\n        WHERE\n          m.merchant_number =  :1 \n        AND\n          m.association_node = th.descendent\n        AND\n          th.ancestor =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRDataBean_AcntClosure",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3498);
   __sJT_st.setLong(2,TRANSCOM);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.acr.ACRDataBean_AcntClosure",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:255^7*/

      rs = it.getResultSet();
      if(rs.next())
      {
        isTranscom = true;
      }
    }
    catch(Exception e) {}
    finally {
      try{rs.close();}catch(Exception e){}
      try{it.close();}catch(Exception e){}
      cleanUp();
    }
    return isTranscom;
  }

  public String renderRequestStatusHtmlTable(String cssClassname)
  {
    if(editable)
      return "";

    if(cssClassname == null)
      cssClassname = "";

    StringBuffer sb = new StringBuffer();

    sb.append("<tr><td>");
      sb.append("&nbsp;<br>");
      sb.append("<table border=0 cellpadding=0 cellspacing=0>");
        sb.append("<tr>");
          sb.append("<td class=\"");
          sb.append(cssClassname);
          sb.append("\">Request Status</td>");
          sb.append("<td width=50>&nbsp;</td>");
          sb.append("<td class=\"");
          sb.append(cssClassname);
          sb.append("\">Accept Amount</td>");
          sb.append("<td width=50>&nbsp;</td>");

          if(acr.itemHasValue("acceptUserId")) {
            sb.append("<td width=50>&nbsp;</td>");
            sb.append("<td class=\"");
            sb.append(cssClassname);
            sb.append("\">Accept User Id</td>");
          }

        sb.append("</tr>");
        sb.append("<tr>");
          sb.append("<td class=\"formFields\" valign=top>");
          sb.append((residentQueue == MesQueues.Q_CHANGE_REQUEST_RESEARCH)? renderHtml("requestStatus"):getACRValue("requestStatus"));
          sb.append("</td>");
          sb.append("<td width=50>&nbsp;</td>");
          sb.append("<td class=\"formFields\" valign=top>");
          sb.append((residentQueue == MesQueues.Q_CHANGE_REQUEST_RESEARCH)? renderHtml("acceptAmt"):getACRValue("acceptAmt"));
          sb.append("</td>");
          sb.append("<td width=50>&nbsp;</td>");

          if(acr.itemHasValue("acceptUserId")) {
            sb.append("<td width=50>&nbsp;</td>");
            sb.append("<td class=\"formFields\">");
            sb.append(acr.getACRValue("acceptUserId"));
            sb.append("</td>");
          }

        sb.append("</tr>");
      sb.append("</table>");
    sb.append("</td></tr>");

    return sb.toString();
  }

  public int[][] getAllowedQueueOps()
  {
    /*
    Determine initial queue according to:
     - All accounts except MES have a one year contract.  Route Closures opened > 1 year straight to TSYS bypassing Research
     - MES has a three year contract.  Route Closures opened > 3 years straight to TSYS bypassing Research
    */
    int bankNum = StringUtilities.stringToInt(acr.getACRValue("Merchant Bank Number"),-1);
    int numMonthsOpen = 0;

    try {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:340^7*/

//  ************************************************************
//  #sql { select months_between(sysdate,ACTIVATION_DATE)
//          
//          from   MIF
//          where  MERCHANT_NUMBER = :getMerchantNumber()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = sqlj.runtime.ref.DefaultContext.getDefaultContext(); if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3499 = getMerchantNumber();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select months_between(sysdate,ACTIVATION_DATE)\n         \n        from   MIF\n        where  MERCHANT_NUMBER =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.acr.ACRDataBean_AcntClosure",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3499);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   numMonthsOpen = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:346^7*/
    }
    catch(Exception e) {
    }
    finally {
      cleanUp();
    }

    int initQueue = MesQueues.Q_NONE;

    if(isMESBank(bankNum)) {
      if(numMonthsOpen > 12)
        initQueue = MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP;
      else
        initQueue = MesQueues.Q_CHANGE_REQUEST_RESEARCH;
    } else {
      if(numMonthsOpen > 36)
        initQueue = MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP;
      else
        initQueue = MesQueues.Q_CHANGE_REQUEST_RESEARCH;
    }

    return new int[][]
    {
       {MesQueues.Q_NONE,initQueue}

      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP:
      case MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP:
      case MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  protected class EntryValidation implements Validation
  {
    protected String ErrorText = "";

    public EntryValidation()
    {
    }

    public String getErrorText()
    {
      return(ErrorText);
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      boolean rval=true;
      Field fld;

      if(!editable) {

        // validate the request status/amount fields
        if(residentQueue == MesQueues.Q_CHANGE_REQUEST_RESEARCH) {
          if((fld = fields.getField("requestStatus"))!=null) {
            boolean bAccepted = fld.getData().equals("Accepted");
            fld=fields.getField("acceptAmt");
            double acptAmt = (fld!=null? fld.asDouble():0d);
            if(bAccepted) {
              if(acptAmt == 0d) {
                rval=false;
                ErrorText = "The Accept Amount must be non-zero if the Request Status is Accepted.";
              }
            } else {
              if(acptAmt != 0d) {
                rval=false;
                ErrorText = "The Accept Amount must be zero if the Request Status is Denied.";
              }
            }
          }
        }

        return rval;
      }

      // require call tag address to be specifed if at least one call tag item specified
      try {
        boolean bCallTag=false;

        for(int i=1;i<=4;i++) {
          fld=fields.getField("sct_"+i);
          if(fld!=null && fld.getData().equals("Yes")) {
            bCallTag=true;
            break;
          }
        }

        if(bCallTag) {
          StringBuffer sb = new StringBuffer();
          if(!adrsHelper.isValidAddress(sb)) {
            rval=false;
            ErrorText = sb.toString();
          }
        }

      }
      catch(Exception e) {
        ErrorText = "The entry could not be validated.";
        rval=false;
      }

      return rval;
    }

  } // EntryValidation

  public String getHeadingName()
  {
    return "Close Merchant Account";
  }

  public class CancellationFeeTypeDropDownTable extends DropDownTable
  {
    public CancellationFeeTypeDropDownTable()
    {
      addElement("","");

      addElement("Regular Fee","Regular Fee");
      addElement("Fee Waived","Fee Waived");
    }

  } // FeeActionDropDownTable class

}/*@lineinfo:generated-code*/