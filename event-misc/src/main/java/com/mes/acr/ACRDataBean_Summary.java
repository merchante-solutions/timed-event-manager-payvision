package com.mes.acr;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;


public final class ACRDataBean_Summary extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_Summary.class);


  // constants
  // (none)


  // data members
  private MerchantInfo  merchantInfo;
  private Hashtable     submitBeans;            // ref. to collection of acr beans to submit
  private int           numValidACRs;           // number of valid ACRs to be submitted
  private int           numInvalidACRs;         // number of invalid ACRs
  private Vector        submitAcrNames;         // list of names representing the valid ACRs for submission
  private Vector        submitAcrIDs;
  private boolean       submitted;


  // class methods
  // (none)


  // object methods

  // construction
  public ACRDataBean_Summary()
  {
    super();

    editable=true;

    submitBeans=null;
    numValidACRs=0;
    numInvalidACRs=0;
    submitAcrNames = new Vector(0,1);
    submitAcrIDs = new Vector(0,1);
    submitted=false;
  }
  public ACRDataBean_Summary(MerchantInfo merchantInfo,Hashtable submitBeans,boolean submitted)
    throws Exception
  {
    super();

    if(merchantInfo==null)
      throw new Exception("Unable to instantiate ACR data bean 'Summary': Null merchant info parameter specified.");

    if(submitBeans==null)
      throw new Exception("Unable to instantiate ACR data bean 'Summary': Null submit beans parameter specified.");

    this.merchantInfo=merchantInfo;
    this.editable=true;

    this.submitBeans=submitBeans;
    this.submitAcrNames = new Vector(0,1);
    this.submitAcrIDs = new Vector(0,1);
    this.submitted=submitted;

    ACRDataBeanBase bean;
    ACR acr;

    // calculate summary-based data member values

    numValidACRs=0;
    numInvalidACRs=0;

    StringBuffer sb = new StringBuffer(512);

    for(Enumeration e=submitBeans.elements();e.hasMoreElements();) {
      bean=(ACRDataBeanBase)e.nextElement();
      acr=bean.getACR();

      if(acr.getStatus()==ACR.ACRSTATUS_ABORTED)
        continue;

      sb.append(acr.getDefinition().getName());

      if(acr.isValid()) {
        this.numValidACRs++;
      } else {
        this.numInvalidACRs++;
        sb.append(" - INVALID");
      }
      submitAcrNames.addElement(sb.toString());
      sb.delete(0,512); // reset

      sb.append(String.valueOf(acr.getID()));
      submitAcrIDs.addElement(sb.toString());
      sb.delete(0,512); // reset

    }

  }

  public long getAppSeqNum()
  {
    return merchantInfo.getAppSeqNum();
  }
  public String getMerchantNumber()
  {
    return merchantInfo.getMerchantNum();
  }
  public String getAppCntrlNum()
  {
    return merchantInfo.getControlNum();
  }

  /**
   * renderHtml()
   *
   * Sub-class override
   */
  public String renderHtml(String fname)
  {
    if(merchantInfo!=null) {
      if(fname.equals("Merchant Account Number")) {
        return merchantInfo.getMerchantNum();
      } else if(fname.equals("Merchant DBA")) {
        return merchantInfo.getDba();
      } else if(fname.equals("Type")) {
        return merchantInfo.getType();
      } else if(fname.equals("ACR Type")) {
        return merchantInfo.getACRAppType();
      } else if(fname.equals("Application Control #")) {
        return merchantInfo.getControlNum();
      }
    }

    return super.renderHtml(fname);
  }

  public String getHTMLBackLink(String cssClassName)
  {
    if(!wasSubmitted()) {
      StringBuffer sb = new StringBuffer(512);

      sb.append("<a");
      if(cssClassName!=null && cssClassName.length()>0) {
        sb.append(" class=\"");
        sb.append(cssClassName);
        sb.append("\" ");
      }

      sb.append("href=\"/srv/acr?method=vdefs\">");
      sb.append("Back to Change Request Options</a>");

      return sb.toString();
    }

    return getACRSearchLink(cssClassName);
  }

  protected void setDefaults()
  {
    // no-op
  }

  public int[][] getAllowedQueueOps()
  {
    return null;
  }

  public String getHeadingName()
  {
    return wasSubmitted()? "Post Submission Summary" : "PRE Submission Summary";
  }

  // accessors
  public int      getNumValidACRs()    { return numValidACRs; }
  public int      getNumInvalidACRs()  { return numInvalidACRs; }
  public Vector   getSubmitAcrNames()  { return submitAcrNames; }
  public Vector   getSubmitAcrIDs()    { return submitAcrIDs; }
  public boolean  wasSubmitted()       { return submitted; }

} // class ACRDataBean_Summary
