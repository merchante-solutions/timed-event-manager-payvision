/*@lineinfo:filename=ACRUserTools*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/banner/Pricing.sqlj $

  Description:

  Pricing

  Banner Bank online app pricing page bean.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/18/04 11:45a $
  Version            : $Revision: 10 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.acr;

import java.sql.ResultSet;
import com.mes.database.SQLJConnectionBase;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class ACRUserTools extends SQLJConnectionBase
{
  public boolean _userAllowedCommand(UserBean user, ACRDataBeanBase bean)
  {
    boolean               result  = false;
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      connect();
      
      // get rights necessary to view items in resident queue
      /*@lineinfo:generated-code*//*@lineinfo:49^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  right
//          from    q_acr_command_restrictions
//          where   type = :bean.getResidentQueue()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_3586 = bean.getResidentQueue();
  try {
   String theSqlTS = "select  right\n        from    q_acr_command_restrictions\n        where   type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRUserTools",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_3586);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.acr.ACRUserTools",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:54^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        if(user.hasRight(rs.getLong("right")))
        {
          result = true;
          break;
        }
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("_userAllowedCommand()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  public static boolean userAllowedCommand(UserBean user, ACRDataBeanBase bean)
  {
    boolean result = false;
    
    try
    {
      result = (new ACRUserTools())._userAllowedCommand(user, bean);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("ACRUserTools::userAllowedCommand()", e.toString());
    }
    
    return result;
  }
}/*@lineinfo:generated-code*/