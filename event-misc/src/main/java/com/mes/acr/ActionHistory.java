/*@lineinfo:filename=ActionHistory*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.acr;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

/**
 * ActionHistory class.
 *
 * Back end support class for presenting ACR specific queue log data.
 */
public final class ActionHistory extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ActionHistory.class);

  // constants
  // (NONE)


  // data members
  private Vector rows = new Vector();
  private ACRDataBeanBase bean = null;


  // class methods
  // (NONE)


  // object methods

  public ActionHistory(ACRDataBeanBase bean)
    throws Exception
  {
    super();

    if(bean==null || bean.getACR()==null || bean.getACR().getID()<1)
      throw new Exception("Unable to construct ActionHistory instance: Invalid ACR Data bean specified.");

    this.bean=bean;
    this.editable=false;

    load(bean.getACR().getID());
  }

  public String getHeadingName()
  {
    return "Action History";
  }

  protected void setDefaults()
  {
    // no-op
  }

  public int[][] getAllowedQueueOps()
  {
    return null;
  }

  public String getBeanACRID()
  {
    return Long.toString(bean.getACR().getID());
  }

  public String getACRStatus()
  {
    return bean.getACRStatus();
  }

  public String getResidentQueueDesc()
  {
    return bean.getResidentQueueDesc();
  }

  public String getStatusSourceLink(String paramName, String paramValue)
  {
    return bean.getStatusSourceLink(paramName, paramValue);
  }

  public String getQueueMerchantType()
  {
    return bean.getQueueMerchantType();
  }

  public String getQueueDateCreated()
  {
    return bean.getQueueDateCreated();
  }

  public String getQueueLastChanged()
  {
    return bean.getQueueLastChanged();
  }

  public String getStatusLastWorkedByLink(String paramName, String paramValue)
  {
    return bean.getStatusLastWorkedByLink(paramName, paramValue);
  }

  public class Row
  {
    private String logDate;
    private String eventDescription;
    private String userLogin;

    public Row(ResultSet rs)
    {
      loadData(rs);
    }

    private void loadData(ResultSet rs)
    {
      try
      {
        logDate             = DateTimeFormatter
                                .getFormattedDate(rs.getTimestamp("log_date"),
                                  "MM/dd/yy hh:mm:ss a");
        userLogin           = rs.getString("user_login");
        eventDescription    = rs.getString("event_description");

        int oldQueue        = rs.getInt   ("old_type");
        int newQueue        = rs.getInt   ("new_type");
        int intAction       = rs.getInt   ("action");
      }
      catch (Exception e)
      {
        log.error("ActionHistory.Row.loadData() EXCEPTION: '"+e.getMessage()+"'.");
      }
    }

    public String getLogDate()
    {
      return logDate;
    }
    public String getEventDescription()
    {
      return eventDescription;
    }
    public String getUserLogin()
    {
      return userLogin;
    }
  }

  private void load(long qId)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:164^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    l.log_seq_num,
//                    l.log_date                  log_date,
//                    l.action                    action,
//                    l.old_type                  old_type,
//                    l.new_type                  new_type,
//                    l.description               event_description,
//                    l.user_name                 user_login,
//                    nvl(l.description,'')       description
//          from      q_log             l
//          where     l.id = :qId
//                    and l.user_name not like 'q_data%'
//                    and (l.new_type >= :MesQueues.Q_CHANGE_REQUEST_START and l.new_type <= :MesQueues.Q_CHANGE_REQUEST_END)
//          order by  log_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    l.log_seq_num,\n                  l.log_date                  log_date,\n                  l.action                    action,\n                  l.old_type                  old_type,\n                  l.new_type                  new_type,\n                  l.description               event_description,\n                  l.user_name                 user_login,\n                  nvl(l.description,'')       description\n        from      q_log             l\n        where     l.id =  :1 \n                  and l.user_name not like 'q_data%'\n                  and (l.new_type >=  :2  and l.new_type <=  :3 )\n        order by  log_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ActionHistory",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,qId);
   __sJT_st.setInt(2,MesQueues.Q_CHANGE_REQUEST_START);
   __sJT_st.setInt(3,MesQueues.Q_CHANGE_REQUEST_END);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.acr.ActionHistory",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:179^7*/

      rs = it.getResultSet();
      while (rs.next())
      {
        rows.add(new Row(rs));
      }
    }
    catch (Exception e)
    {
      log.error("load() EXCEPTION: "+e.getMessage());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { rs.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  public Iterator iterator()
  {
    return rows.iterator();
  }

}/*@lineinfo:generated-code*/