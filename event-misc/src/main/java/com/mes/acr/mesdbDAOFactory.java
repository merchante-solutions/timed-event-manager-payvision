package com.mes.acr;

/**
 * mesdbDAOFactory class.
 * 
 * Manages the creation of ACR related DAO objects.
 * SINGLETON.
 */
public class mesdbDAOFactory extends DAOFactory
{
  
  // Singleton
  public static mesdbDAOFactory getInstance()
  {
    if(_instance==null)
      _instance = new mesdbDAOFactory();
    
    return _instance;
  }
  private static mesdbDAOFactory _instance = null;
  ///
  
  // constants
  // (none)
  
  // data members
  // (none)
  
  // class functions
  // (none)
  
  // object functions
  
  // construction
  private mesdbDAOFactory()
  {
  }
  
  // creational methods
  
  public ACRDefDAO    getACRDefinitionDAO()
  {
    return new mesDBACRDefDAO();
  }
  
  public ACRDAO       getACRDAO()
  {
    return new mesDBACRDAO();
  }
  
  public MerchantInfoDAO       getMerchantInfoDAO()
  {
    return new mesDBMerchantInfoDAO();
  }

} // class DAOFactory
