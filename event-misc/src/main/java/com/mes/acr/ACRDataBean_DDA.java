package com.mes.acr;

// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CityStateZipField;
import com.mes.forms.Field;

public class ACRDataBean_DDA extends ACRDataBean_DDA_Base
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_DDA.class);

  //for bank info
  private AddressHelper addressHelper;

  // construction
  public ACRDataBean_DDA()
  {
  }

  public ACRDataBean_DDA(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefinition acrd=acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;

    if((acrdi=acrd.getACRDefItem("new_merchantDDA"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("new_transRtNum"))!=null)
      fields.add(ACRDefItemToField(acrdi));

    // validate new transit routing number
    fields.addValidation(new ACRDataBean_DDA_Base.DdaTransRtValidation());

    // RELEASE 2.8.05
    Field bankName   = new Field("bankName","Bank Name",30,25,false);
    Field bankLine1  = new Field("bankLine1","Bank Address",30,25,false);
    Field bankZip    = new CityStateZipField("bankCsz",
                                              "City ST Zip",
                                              25,false);
    fields.add(bankName);
    fields.add(bankLine1);
    fields.add(bankZip);
    //*/

    return;
  }

  /**
   * Instantiates an address helper, stores internal reference to it.
   */

//*RELEASE 2.8.05

  private AddressHelper _getAddressHelper()
  {
    if (addressHelper == null)
    {
      addressHelper = new AddressHelper(getMerchantNumber(),"",
                                        AddressHelper.FT_NONE);
    }
    return addressHelper;
  }

  public String renderCurrentBankAddress()
  {
    try
    {
      return _getAddressHelper().getAddress("addr9").renderHtml();
    }
    catch(Exception e)
    {
      log.debug(e.getMessage());
      return "";
    }
  }

  public String renderHtml(String name)
  {
    if(editable || !(name.equals("bankCsz")) )
    {
      return super.renderHtml(name);
    }
    else
    {
      //need to add up three fields/acrItems
      StringBuffer sb = new StringBuffer();
      sb.append(renderHtml("bankCszCity")).append(", ");
      sb.append(renderHtml("bankCszState")).append(" ");
      sb.append(renderHtml("bankCszZip"));
      return sb.toString();
    }
  }

  public void doPull()
  {

    super.doPull();

    // store copy of old bank address
    acr.setACRItem("oldBank","Old " + renderCurrentBankAddress());

  }
//*/

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_RISK}

      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_RISK}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

} // class ACRDataBean_DDA
