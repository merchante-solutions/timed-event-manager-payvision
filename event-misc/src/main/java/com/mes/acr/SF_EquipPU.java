package com.mes.acr;

import com.mes.forms.SelectorColumnDef;

public class SF_EquipPU extends SF_EquipBase
{
  public SF_EquipPU(String title)
  {
    super(title);
  }

  public void setupFrame()
  {
    addColumn(new SelectorColumnDef("col1","Serial No.",  100));
    addColumn(new SelectorColumnDef("col2","Description", 100));
    addColumn(new SelectorColumnDef("col3","Part No.",    100));
    addColumn(new SelectorColumnDef("col4","Disposition", 220));
    addColumn(new SelectorColumnDef("col5","Reason",      165));
  }
}

