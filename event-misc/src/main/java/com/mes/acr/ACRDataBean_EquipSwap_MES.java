package com.mes.acr;

import java.util.List;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.RadioField;
import com.mes.forms.Selector;
import com.mes.tools.DropDownTable;

public final class ACRDataBean_EquipSwap_MES extends ACRDataBeanBase
{
  // create class log
  static Logger log = Logger.getLogger(ACRDataBean_EquipSwap_MES.class);

  /**
   * Standard Constructor
   */
  public ACRDataBean_EquipSwap_MES()
  {
  }

  public ACRDataBean_EquipSwap_MES(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {

    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP);
    //log.debug("get app code = "+ merchantInfo.getTypeCode() +" and constant =" +mesConstants.APP_TYPE_TRANSCOM);
    /*
    if(merchantInfo!=null)
    {
      if(merchantInfo.getTypeCode()==mesConstants.APP_TYPE_TRANSCOM)
      {
        setInitialQueue(MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT);
      }
      else
      {
        setInitialQueue(MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP);
      }
    }
    */
  }

  /**
   * Factory for selector creation and the selector itself
   */
  private ACRSelectorFactory factory = new ACRSelectorFactory();
  private Selector equipSelector;

  /**
   * Selector Generation
   * ST_EQUIPDISP_FLAG
   */
  public Selector getEquipmentSelector()
  {
    if (equipSelector == null)
    {
     equipSelector = factory.createSelector(factory.ST_SWAP_CALLTAGS_MES,
        getMerchantNumber());
    }
    return equipSelector;
  }

  /**
   * getSelectedEquipment()
   * provides a list of selected equipment for order generation
   * during ACRPump call
   */
  public List getSelectedEquipment()
  {
    return getEquipmentSelector().getSelected();
  }

  /**
   * Turn off Multi-Merchant component
   */
   //asked to be renabled 04/08 PRF-1015
  //public boolean hasMultiMerchantComp()
  //{
    //return false;
  //}

  protected void setDefaults()
  {
    //no-op
  }

  public boolean getQueueIsRush()
  {
    return true;
  }

  public String renderHtml(String name)
  {
    if(name.equals("Rush"))
    {
      StringBuffer sb = new StringBuffer("<font color=\"Red\">RUSH</font>");
      if(editable)
      {
        sb.append(super.renderHtml(name));
      }
      return sb.toString();
    }
    else if(!inSubmitMode() && name.equals("conversion"))
    {
      if(getACRValue(name).equals("Y"))
      {
        return "<font color = red>NOTE:</font> This ACR supports Trident Conversion";
      }
      else
      {
        return "";
      }
    }
    else
    {
      return super.renderHtml(name);
    }
  }

  protected void createExtendedFields()
  {

    if(acr==null)
      return;

    CurrencyField cField = new CurrencyField("swapBillAmt", "Shipping charge",7,10,false,0.0f,9999.0f);
    fields.add(cField);

    if(!editable)
      return;

    try
    {

      // default to rush request
      fields.deleteField("Rush");
      fields.add(new HiddenField("Rush","y"));

      DropDownField swapDeets = new DropDownField("swapDetails", "Swap Details",new SwapDetsTable() ,false);
      fields.add(swapDeets);

      String [][] billToButtons =
      {
        { "Merchant" , "Merchant" },
        { "Bank"  , "Bank" },
        { ""  , "off" }
      };

      Field field = new RadioField("swapBillTo", "Swap Fee Payer", billToButtons,"");
      fields.add(field);

      field = new CheckField("conversion","Supports Trident Conversion?","Y",false);
      fields.add(field);

      field = new CheckField("deployQueue","Route Directly to Deployment","Y",false);
      fields.add(field);

      //add Address subbean
      add(getAddressHelper());

      //add equipment selector
      add(getEquipmentSelector());

    }
    catch(Exception e)
    {
      log.error("createExtendedFields() = " + e.toString());
      logEntry("createExtendedFields()",e.toString());
    }
  }

  public void doPull()
  {
    super.doPull();

    //look for the checkbox determining conversion efforts
    if(getACRValue("deployQueue").toUpperCase().equals("Y"))
    {
      log.debug("Changing initial Q to Deployment...");
      setInitialQueue(MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT);
    }
  }


  private class SwapDetsTable extends DropDownTable
  {
    public SwapDetsTable()
    {
      addElement("","Select One");

      addElement("Case","Case");
      addElement("Display","Display");
      addElement("Keyboard","Keyboard");
      addElement("Mag Reader","Mag Reader");
      addElement("Modem","Modem");
      addElement("Printer","Printer");
      addElement("PinPad Encryption","PinPad Encryption");
      addElement("Programming","Programming");
      addElement("Other: see Comments","Other: see Comments");

    }

  } // SwapReasonDropDownTable class


  public String getHeadingName()
  {
    return "Order Equipment Swap for Merchant";
  }

  public String renderEquipmentHTML()
  {
    //this is hack to reveal new equipment info... to be fixed?!
    //all naming is directly related to the bean and selector item/frame/factory
    boolean hasEquip = false;
    int LIMIT = 500;
    String item = "";
    StringBuffer sb = new StringBuffer();

    for (int i=0; i < LIMIT; i++)
    {
      item = getACRValue(CALLTAG_EQUIPMENT_NAME+"_"+i);
      if ( ! ( item.equals("") || item.equals("n") ) )
      {
        if(!hasEquip)
        {
          hasEquip = true;

          sb.append("<table cellpadding=\"1\" border=\"0\" align=\"center\" width=\"100%\" class=\"tableFillGray\" cellspacing=\"0\">");
          sb.append("<tr>");
          sb.append("<td>");
          sb.append("<table class=\"formTable\" width=\"100%\" border=\"0\" cellspacing=\"0\">");
          sb.append("  <tr>");
          sb.append("  <th colspan=7 class=\"tableColumnHead\">Equipment for Upgrade</th>");
          sb.append("  </tr>");
          sb.append("  <tr>");
          sb.append("   <td class=\"tableData\" align=\"center\">Serial Number</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Description</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Equipment Type</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">V Num</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Terminal App</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Vendor ID</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Status</td>");
          sb.append(" </tr>");
          sb.append(" <tr>");
          sb.append("   <td colspan=7 height=\"1\" bgcolor=\"#cccccc\"><img src=\"/images/spcr_transp.gif\" height=\"1\"></td>");
          sb.append("  </tr>");
        }

        //to allow and show elementary changes to these fields
        Field _VNum = new Field(item+"_VNum",item+"_VNum", true);
        _VNum.setData(getACRValue(item+"_VNum"));
        fields.add(_VNum);

        Field _termApp = new Field(item+"_termApp",item+"_termApp", true);
        _termApp.setData(getACRValue(item+"_termApp"));
        fields.add(_termApp);

        Field _vendorId = new Field(item+"_vendorId",item+"_vendorId", true);
        _vendorId.setData(getACRValue(item+"_vendorId"));
        fields.add(_vendorId);

        Field _status = new DropDownField(item + "_status",
        "Equipment status",
        TableFactory.getDropDownTable(TableFactory.EQUIP_STATUS),true);
        _status.setData(getACRValue(item+"_status"));
        fields.add(_status);

        sb.append("<tr>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(generateCallTagLink(getACRValue(item+"_serNum"))).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_description")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_equipType")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(_VNum.renderHtml()).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(_termApp.renderHtml()).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(_vendorId.renderHtml()).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(_status.renderHtml()).append("</td>");
        sb.append("</tr>");
      }
    }

    //finish up
    if(hasEquip)
    {
      sb.append("</table></td></tr></table>");
    }
    else
    {
      sb.append("No equipment selected for this ACR.");
    }

    return sb.toString();
  }

  /**
   * getCallTaggedEquipment and calltaggedEquipmentExists
   * both necessary to generate call tags, but implemented
   * differently depending on the required status of said tags
   */
  public Vector getCallTaggedEquipment()
  {
    return ACRUtility.processCallTaggedEquipment(getACR());
  }

  public boolean calltaggedEquipmentExists()
  {
    //by very nature - must be true
    return true;
  }

} // class ACRDataBean_EquipSwap_MES
