package com.mes.acr;

public interface ACRDataBeanSubcomponent
{
  public void doPush(ACR acr);
  public void doPull(ACR acr);
}