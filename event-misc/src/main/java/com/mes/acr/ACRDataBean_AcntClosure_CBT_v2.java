package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.RadioField;
import com.mes.forms.Selector;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import com.mes.tools.DBGrunt;
import com.mes.tools.DropDownTable;

public final class ACRDataBean_AcntClosure_CBT_v2 extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_AcntClosure_CBT_v2.class);

  /**
   * Standard Constructor
   */
  public ACRDataBean_AcntClosure_CBT_v2()
  {
  }

  public ACRDataBean_AcntClosure_CBT_v2(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {

    String merchNum = getMerchantNumber();
    if (merchNum.length() == 0)
    {
      return;
    }

    generateTermInfo(merchNum);
    generateUSAEPay();

    if(!editable)
    {
      generateDateInfo(merchNum);
    }
  }

  /**
   * Factory for selector creation and the selector itself
   */
  private ACRSelectorFactory factory = new ACRSelectorFactory();
  private Selector equipSelector;

  /**
   * Can only be dated that day's current date
   */
  private String cancelDate;

  /**
   * Info for post submit only
   */
  private String dateOpened = "";
  private String dateLastDeposit= "";
  private int termContract = 0;
  private int currentTermLength = 0;
  private double earlyCancelFee = 0.0;

  private boolean epayFlag = false;

  public String getDateOpened()
  {
    return dateOpened;
  }

  public String getDateLastDeposit()
  {
    return dateLastDeposit;
  }

  public int getTermContract()
  {
    return termContract;
  }

  public double getEarlyCancelFee()
  {
    return earlyCancelFee;
  }

  public String getEarlyCancelFeeStr()
  {
    return NumberFormatter.getDoubleString(earlyCancelFee,NumberFormatter.CURRENCY_FORMAT);
  }

  /**
   * Selector Generation
   * ST_EQUIPDISP_FLAG
   */
  public Selector getEquipmentSelector()
  {
    if (equipSelector == null)
    {
     equipSelector = factory.createSelector(factory.ST_ACCT_CLOSE_CALLTAGS,
        getMerchantNumber());
    }
    return equipSelector;
  }

  /**
   * getSelectedEquipment()
   * provides a list of selected equipment for order generation
   * during ACRPump call
   */
  public List getSelectedEquipment()
  {
    return getEquipmentSelector().getSelected();
  }


  /**
   * Turn off Comments component
   */

 /* Comments activated as of 1/24/05 (per Keni Bush)
  public boolean hasEditableComments()
  {
    return !editable && super.hasEditableComments();
  }
  */

  protected void setDefaults()
  {

    try
    {
      cancelDate = DateTimeFormatter.getFormattedDate( DBGrunt.getCurrentDate(),
                                                       "MM/dd/yy");
    }
    catch(Exception e)
    {
      try
      {
        cancelDate = DateTimeFormatter.getCurDateString();
      }
      catch (Exception e1)
      {
        cancelDate = "";
      }
    }
  }

  protected void createExtendedFields()
  {
    if(acr==null)
      return;

    if(!editable)
    {

      //if(residentQueue == MesQueues.Q_CHANGE_REQUEST_RESEARCH)
      //{
        Field field;

        // accepted/denied (editable in status mode)
        String[][] arrReqStat =
        {
           { "Accepted","Accepted" }
          ,{ "Denied","Denied" }
        };

        field = new RadioField("requestStatus","Request Status",arrReqStat);
        fields.add(field);

        // accept amount (editable in status mode)
        field = new CurrencyField("acceptAmt","Accept Amount",9,9,true,-999999f,999999f);
        field.setLabel("Accept Amount");

       //String fee = getACRValue("cancelFee").equals("WAIVED")?"0.00":getACRValue("cancelFee");
        //field.setData(fee);
        //populate with the current fee amount
        String fee = determineCurrentFee();
        field.setData(fee);
        fields.add(field);
     // }

    }
    else
    {
      try
      {

        //add equipment selector
        add(getEquipmentSelector());

        //add Address subbean
        add(getAddressHelper());

        // make address required if equipment is selected
        getAddressHelper().requireIfSelected(getEquipmentSelector());

      }
      catch (Exception e)
      {
        log.debug("Failed to load sub-bean components");
      }

      String[][] masterButtons =
      {
         { "No","No" }
        ,{ "Yes","Yes" }
      };

      Field master = new RadioField("isMasterAccount","Is the Master Account being closed?",masterButtons);
      Field slave = new Field("slaveAccount","Slave Account",16,10,true);
      slave.addValidation(new ConditionalRequiredValidation
                            (new FieldValueCondition(master, "Yes"),
                            "You must supply a new Master Account Number."
                            )
                          );

      fields.add(master);
      fields.add(slave);
      fields.add(new HiddenField("cancelDate",cancelDate));
      fields.add(new DropDownField("cancelReason", "Reason for Cancellation", new _ddTable(), false));
      fields.add(new DropDownField("cancelType", "Cancellation Type", new _ddTypeTable(), false));

      String _name = "No Fee - Term Met.";
      String _value = "0.00";

      if(isEarlyCancellation())
      {
        _value = NumberFormatter.getDoubleString(earlyCancelFee,NumberFormatter.CURRENCY_FORMAT);
        _name = _value +": Early Termination Fee";
      }

      String[][] cancelButtons =
      {
        {"WAIVED", "Fee Waived"},
        {_value,_name}
      };

      fields.add(new RadioField("cancelFee","Cancellation Fee", cancelButtons));

    }
  }

  private void generateTermInfo(String merchNum)
  {


    PreparedStatement ps = null;
    ResultSet         rs = null;

    try
    {
      connect();
      String qs =
        "select                                                             "+
        "  nvl(months_between(sysdate,nvl(mif.TERM_CONTRACT_START_DATE,mif_date_opened(mif.date_opened))),0) numMonthsOpen,"+
        "  nvl(m.termination_months,0)                        term,         "+
        "  nvl(m.termination_fee,0)                           fee           "+
        "from                                                               "+
        "  mif mif,                                                         "+
        "  merchant m                                                       "+
        "where                                                              "+
        "  mif.merchant_number = ?                                          "+
        "and                                                                "+
        "  mif.merchant_number = m.merch_number(+)                          ";

      ps = con.prepareStatement(qs);
      ps.setString(1,merchNum);
      rs = ps.executeQuery();
      if (rs.next())
      {
        termContract = rs.getInt("term");
        currentTermLength = rs.getInt("numMonthsOpen");
        earlyCancelFee = rs.getDouble("fee");
      }

    }
    catch(Exception e)
    {
      log.error(this.getClass().getName() + ".generateTermInfo(): " + e);
    }
    finally {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  public boolean isEarlyCancellation()
  {
    return currentTermLength < termContract;
  }

  private void generateUSAEPay()
  {
    ResultSet         rs    = null;
    PreparedStatement ps    = null;

    try
    {
      connect();

      String qs =

        " select count(1) as counter      "+
        " from merch_pos                  "+
        " where app_seq_num = ?           "+
        " and pos_code = 215              ";

      ps = con.prepareStatement(qs);
      ps.setLong(1, this.getAppSeqNum());
      rs = ps.executeQuery();

      if (rs.next())
      {
        //set the flag to true for the auto email generation
        if(rs.getInt("counter") > 0)
        {
          epayFlag = true;
        }
      }

    }
    catch(Exception e)
    {
      log.error(this.getClass().getName() + ".generateUSAEPay(): " + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }

  }

  public boolean hasUSAEpay()
  {
    return epayFlag;
  }

/*

The new columns in the MERCHANT table are:

TERMINATION_FEE_MONTHS -- length of contract with merchant
TERMINATION_FEE -- amount merchant should be charged if they close before the contract is up

*/

  //returns the current fee, either the term agreement or the accepted amount
  //as found in the ACR as it's being processed.
  private String determineCurrentFee()
  {
    String fee = getACRValue("acceptAmt");
    if(fee.equals(""))
    {
      fee = getACRValue("cancelFee").equals("WAIVED")?"0.00":getACRValue("cancelFee");
    }
    return fee;
  }


  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

//CBT Mgmt queue
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}

//ASG Mgmt queue

      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}

      //TSYS audit queue
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}

      //POS audit queue
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}

      //Help CBT mgmt
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}

      //ASG CBT mgmt
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}

    };
  }

  public String getHeadingName()
  {
    return "Close Merchant Account";
  }

  public String renderHtml(String name)
  {
    if(name.equals("cancelDate") && editable)
    {
      return cancelDate;
    }
    else
    {
      return super.renderHtml(name);
    }
  }

  public class _ddTable extends DropDownTable
  {
    public _ddTable()
    {
      super();
      addElement("","Select One");
      addElement("Merchant Request - Business Closed","Merchant Request - Business Closed");
      addElement("Merchant Request - Re-Pricing","Merchant Request - Re-Pricing");
      addElement("Merchant Request - Multi-Merchant Closing","Merchant Request - Multi-Merchant Closing");
      addElement("Merchant Request - New Business Ownership","Merchant Request - New Business Ownership");
      addElement("Merchant Request - Do Not Need Card Services","Merchant Request - Do Not Need Card Services");
      addElement("Merchant Request - Chose Different Processor","Merchant Request - Chose Different Processor");
      addElement("Merchant Request - Poor Service from Sales Rep","Merchant Request - Poor Service from Sales Rep");
      addElement("Merchant Request - Other","Merchant Request - Other");
      addElement("Internal Request - Risk","Internal Request - Risk");
      addElement("Internal Request - DDA/TR","Internal Request - DDA/TR");
      addElement("Internal Request - NSF Fees","Internal Request - NSF Fees");
      addElement("Internal Request - Lack Merch of Response to Contact","Internal Request - Lack of Merch Response to Contact");
      addElement("Internal Request - Low Volume","Internal Request - Low Volume");
      addElement("Internal Request - Other","Internal Request - Other");
    }
  }

  public class _ddTypeTable extends DropDownTable
  {
    public _ddTypeTable()
    {
      super();
      addElement("","Select One");
      addElement("Cancel account on TSYS and Block & Delete in MMS","Cancel account on TSYS and Block & Delete in MMS");
      //addElement("Block & Delete V# in MMS only","Block & Delete V# in MMS only");
      addElement("Block V# only","Block V# only");
    }
  }

  public String renderEquipmentHTML()
  {
    //this is hack to reveal new equipment info... to be fixed?!
    //all naming is directly related to the bean and selector item/frame/factory
    boolean hasEquip = false;
    int LIMIT = 100;
    String item = "";
    StringBuffer sb = new StringBuffer();

    for (int i=0; i < LIMIT; i++)
    {
      item = getACRValue(CALLTAG_EQUIPMENT_NAME+"_"+i);
      if ( ! ( item.equals("") || item.equals("n") ) )
      {
        if(!hasEquip)
        {
          hasEquip = true;

          sb.append("<table cellpadding=\"1\" border=\"0\" align=\"center\" width=\"100%\" class=\"tableFillGray\" cellspacing=\"0\">");
          sb.append("<tr>");
          sb.append("<td>");
          sb.append("<table class=\"formTable\" width=\"100%\" border=\"0\" cellspacing=\"0\">");
          sb.append("  <tr>");
          sb.append("  <th colspan=5 class=\"tableColumnHead\">Equipment to Pick up - Call Tag Generation</th>");
          sb.append("  </tr>");
          sb.append("  <tr>");
          sb.append("   <td class=\"tableData\" align=\"center\">Equipment Type</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Serial Number</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Description</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Part Num</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Disposition</td>");
          sb.append(" </tr>");
          sb.append(" <tr>");
          sb.append("   <td colspan=5 height=\"1\" bgcolor=\"#cccccc\"><img src=\"/images/spcr_transp.gif\" height=\"1\"></td>");
          sb.append("  </tr>");
        }
        sb.append("<tr>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(generateCallTagLink(getACRValue(item+"_equipType"))).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_serNum")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_description")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_partNum")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_disposition")).append("</td>");
        sb.append("</tr>");
      }
    }

    //finish up
    if(hasEquip)
    {
      sb.append("</table></td></tr></table>");
    }
    else
    {
      sb.append("No equipment selected for this ACR.");
    }

    return sb.toString();
  }

  private void generateDateInfo(String merchNum)
  {

    PreparedStatement ps = null;
    ResultSet         rs = null;

    try
    {
      connect();

      String qs = "select "+
                  "nvl(date_opened,'--') as openDate, "+
                  "nvl(to_char(last_deposit_date,'mm/dd/yy') ,'--') as depDate "+
                  "from mif "+
                  "where merchant_number = ?";
      ps = con.prepareStatement(qs);
      ps.setString(1,merchNum);

      rs = ps.executeQuery();
      String temp;
      if (rs.next())
      {
        temp       = processString(rs.getString("openDate"));
        if(temp!=null && temp.length()==6)
        {
          dateOpened = temp.substring(0,2)+"/"+temp.substring(2,4)+"/"+temp.substring(4);
        }

        dateLastDeposit  = processString(rs.getString("depDate"));
      }
    }
    catch (Exception e)
    {
      log.error(this.getClass().getName() + ".setDefaults(): " + e);
      dateOpened = "--";
      dateLastDeposit = "--";
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  /**
   * getCallTaggedEquipment and calltaggedEquipmentExists
   * both necessary to generate call tags, but implemented
   * differently depending on the required status of said tags
   */
  public Vector getCallTaggedEquipment()
  {
    return ACRUtility.processCallTaggedEquipment(getACR());
  }

  public boolean calltaggedEquipmentExists()
  {
    //by very nature - must be true
    return true;
  }

} // class ACRDataBean_AcntClosure
