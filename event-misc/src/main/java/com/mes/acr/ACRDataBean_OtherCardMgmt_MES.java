package com.mes.acr;

import java.util.Calendar;
import java.util.Enumeration;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.forms.CheckField;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.RadioField;
import com.mes.tools.DBGrunt;
import com.mes.tools.DropDownTable;
import com.mes.tools.EquipmentKey;

public class ACRDataBean_OtherCardMgmt_MES extends ACRDataBeanBase
{
  /**
   * Log4J Logger
   */
  static Logger log = Logger.getLogger(ACRDataBean_OtherCardMgmt_MES.class);

  /**
   * Standard Constructor
   */
  public ACRDataBean_OtherCardMgmt_MES()
  {
  }

  public ACRDataBean_OtherCardMgmt_MES(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
      Requestor requestor, boolean editable) throws Exception
  {
    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP);
  }
  /**
   * multi-purpose String[][] for field construction
   */
  protected String [][] yesNoButtons =
  {
    { "Yes" , "Yes" },
    { "No"  , "No" },
    { ""  , "off" }
  };

  protected String [][] yesNoButtons2 =
  {
    { "Yes" , "Yes" },
    { "No"  , "No" }
  };

  protected String [][] yesNoSwapButtons =
  {
    { "Yes - $75" , "Yes - $75 Swap Fee" },
    { "Yes - $0" , "Yes - $0 Swap Fee" },
    { "No"  , "No" },
  };

  protected String [][] billToButtons =
  {
    { "Merchant" , "Merchant" },
    { "Bank"  , "Bank" },
    { ""  , "off" }
  };

  protected String [][] statusButtons =
  {
    { "Add" , "Add" },
    { "Remove" , "Remove" }
  };

  protected String [][] statusButtons2 =
  {
    { "Add" , "Add" },
    { "Remove" , "Remove" },
    { "Update" , "Update" },
  };

  protected String [][] statusButtons3=
  {
    { "Add" , "Add" },
    { "Remove" , "Remove" },
    { "Update" , "Update" },
    { "Apply For" , "Apply For" }

  };

  protected String[][] ampm =
  {
    {"AM","AM"},
    {"PM","PM"},
    { ""  , "off" }
  };

  /**
   * ints for equipment switch
   */
  private final int TERMINAL = 1;
  private final int PRINTER = 2;
  private final int PINPAD = 3;
  private final int PERIPHERAL = 4;

  protected void setDefaults()
  {
    // no-op
  }

  /**
   * Simple flag for JSP to determine whether or not to show certain elements
   * done this way so that code doesn't have to have to be recompiled to turn
   * back on...
   */
  public boolean isOpen()
  {

    Calendar openDate, currentDate;

    openDate = Calendar.getInstance();

    //set calendar to feb 1, 2005, 12:00:01 AM
    openDate.set(2005, 2, 1, 0, 0, 1);

    //gotta get current date from DB
    currentDate = Calendar.getInstance();

    //use DBGrunt
    currentDate.setTime(DBGrunt.getCurrentDate());

    //compare the dates
    if(currentDate.before(openDate))
    {
      return false;
    }

    return true;
  }



  protected void createExtendedFields()
  {
    if(acr==null)
      return;

    try
    {

      if(editable)
      {
        //add Address subbean
        add(getAddressHelper(true, true));

        // require "at least one" card add be checked
        // pass into those methods that need it
        AtLeastOneValidation atLeastVal = new AtLeastOneValidation("At least one Card Type must be selected to proceed.");

        /*
         * Debit
         */
        makeDebitFields(atLeastVal);


        /*
         * Certegy
         */
        makeCertegyFields(atLeastVal);

        /*
         * Valutec
         */
        makeValutecFields(atLeastVal);


        /*
         * Equipment
         */
        makeEquipmentFields();

        /*
         * Terminal Features
         */
        makeFeaturesFields();
      }

      //as of 9-24-05 these are general fields, not just post-submission
        makePostSubmissionFields();

    }
    catch(Exception e)
    {
      log.error("createExtendedFields() = " + e.toString());
      logEntry("createExtendedFields()",e.toString());
    }
  }

  /**
   * All make...Fields below contain 2 types of validation:
   * IfYesNotBlankValidation - there is a control field (checkbox)
   * that determines what has been selected and this tiggers 'must have'
   * fields.
   * ConditionalRequiredValidation - a specialized validation that is used
   * to conditionally check certain field values and determine related required
   * fields based on the chosen value of the initial field
   */

  private final void makeDebitFields(AtLeastOneValidation atLeastVal)
  throws Exception
  {
    Field field;

    CheckboxField controlField1 = new CheckboxField("debit","Debit",false);
    controlField1.addHtmlExtra("class=\"semiSmallBold\"");
    atLeastVal.addField(controlField1);
    controlField1.addValidation(atLeastVal);
    IfYesNotBlankValidation d_ynbVal = new IfYesNotBlankValidation(controlField1,"Field required");
    fields.add(controlField1);

    field = createFeeField("debitAuthFee","Debit Auth Fee", mesConstants.APP_CT_DEBIT);
    field.addValidation(d_ynbVal);
    fields.add(field);

    field = new RadioField("debitPPad", "Debit Pin Pad", yesNoSwapButtons,"");
    ((RadioField)field).setRenderStyle(RadioField.RS_VERTICAL);
    field.addValidation(d_ynbVal);
    //for debitPPType, debitSerNum
    ConditionalRequiredValidation crVal =
      new ConditionalRequiredValidation(new FieldValueCondition(field,"Yes"),
                                        "Field required");
    fields.add(field);

    field = new RadioField("swapPPBillTo", "Bill PinPad Swap Fee", billToButtons,"");
    ((RadioField)field).setRenderStyle(RadioField.RS_VERTICAL);
    field.addValidation(crVal);
    fields.add(field);

    field = new RadioField("debitCardStatus", "Debit Card Status", statusButtons,"");
    field.addValidation(d_ynbVal);
    fields.add(field);

    /*
    field = new DropDownField("debitPPType","Debit Pin Pad Type", new _ddPinPadTable(), true);
    //add specialized validation
    field.addValidation(crVal);
    fields.add(field);
    */
    field = new Field("debitPPType", "Debit Pin Pad Type", true);
    //add specialized validation
    field.addValidation(crVal);
    fields.add(field);

    field = new Field("debitSerNum", "Debit Serial Number", true);
    //add specialized validation
    field.addValidation(crVal);
    fields.add(field);

    field = new Field("debitTID", "Debit TID", true);
    //add specialized validation
    field.addValidation(crVal);
    fields.add(field);

    field = new Field("debitApp", "Debit App", true);
    //add specialized validation
    field.addValidation(crVal);
    fields.add(field);

  }


  private final void makeCertegyFields(AtLeastOneValidation atLeastVal)
  throws Exception
  {
    Field field;

   String [][] certegyProduct =
    {
      { "Certegy Welcome Check" , "Certegy Welcome Check" },
      { "Certegy Elec Check" , "Certegy Elec Check" }
    };

    CheckboxField controlField2 = new CheckboxField("certegy","Certegy",false);
    controlField2.addHtmlExtra("class=\"semiSmallBold\"");
    atLeastVal.addField(controlField2);
    IfYesNotBlankValidation c_ynbVal = new IfYesNotBlankValidation(controlField2,"Field required");
    fields.add(controlField2);

    field = new RadioField("certegyDebitCard", "Merchant accepts Debit", yesNoButtons,"");
    field.addValidation(c_ynbVal);
    fields.add(field);

    field = new RadioButtonField("certegyProduct", "Certegy Product", certegyProduct,true,"Please select Certegy Product Type.");
    field.addValidation(c_ynbVal);
    fields.add(field);

    ConditionalRequiredValidation crVal1 =
      new ConditionalRequiredValidation(new FieldValueCondition(field,"Certegy Welcome Check"),
                                        "Field required");

    ConditionalRequiredValidation crVal2 =
      new ConditionalRequiredValidation(new FieldValueCondition(field,"Certegy Elec Check"),
                                        "Field required");

    field = new RadioField("certegyCardStatus", "Certegy Card Status", statusButtons2,"");
    field.addValidation(c_ynbVal);
    fields.add(field);

    field = new Field("certegyNum", "Certegy Acct Number", true);
    field.addValidation(c_ynbVal);
    fields.add(field);

    field = new Field("certegyEF1", "Certegy EF1", true);
    field.addValidation(crVal2);
    fields.add(field);

    field = new Field("certegyB0", "Certegy B.0 Number", true);
    field.addValidation(crVal1);
    fields.add(field);

    field = new RadioField("certegyOwnEquip", "Certegy Own Equipment", yesNoButtons2,"");
    field.addValidation(c_ynbVal);
    fields.add(field);

    IfYesNotBlankValidation c_ynbVal2 =
      new IfYesNotBlankValidation(field,"Field required");

    field = new DropDownField("certegyCompEquip","Certegy Compatible Equipment", new _ddCompEquipTable_cert(), true);
    field.addValidation(c_ynbVal2);
    fields.add(field);

    field = new DropDownField("certegyPeripheral","Certegy Peripheral", new _ddPeripheralTable(), true);
    field.addValidation(c_ynbVal2);
    fields.add(field);
  }


  private final void makeValutecFields(AtLeastOneValidation atLeastVal)
  throws Exception
  {
    Field field;

    CheckboxField controlField3 = new CheckboxField("valutec","Valutec",false);
    controlField3.addHtmlExtra("class=\"semiSmallBold\"");
    atLeastVal.addField(controlField3);
    IfYesNotBlankValidation v_ynbVal = new IfYesNotBlankValidation(controlField3,"Field required");
    fields.add(controlField3);

    field = new RadioField("valutecCardStatus", "Valutec Card Status", statusButtons2,"");
    field.addValidation(v_ynbVal);
    fields.add(field);

    field = new Field("valutecNum", "Valutec Merchant Number", true);
    field.addValidation(v_ynbVal);
    fields.add(field);

    field = new Field("valutecTID", "Valutec Terminal ID", true);
    field.addValidation(v_ynbVal);
    fields.add(field);

    field = new DropDownField("valutecCompEquip","Valutec Compatible Equipment", new _ddCompEquipTable(), true);
    field.addValidation(v_ynbVal);
    fields.add(field);
  }

  private final void makeEquipmentFields()
  throws Exception
  {
    Field field;
    Field valField;

    //only 4 types of equipment
    for(int i = 1; i < 5; i++)
    {
      switch(i)
      {
        case TERMINAL:
          field = new DropDownField("equip_"+i, "Terminal Equipment", new _ddCompEquipTable(), true );
          fields.add(field);
          valField = fields.getField("certegyCompEquip");
          if(valField!=null)
          {
            field.addValidation(new ConditionalRequiredValidation
                                  (
                                  new FieldValueCondition(valField,"Other"),
                                  "Field required"
                                  )
                               );
          }
          valField = fields.getField("valutecCompEquip");
          if(valField!=null)
          {
            field.addValidation(new ConditionalRequiredValidation
                                  (
                                  new FieldValueCondition(valField,"Other"),
                                  "Field required"
                                  )
                               );
          }
          break;

        case PINPAD:
          field = new DropDownField("equip_"+i, "PinPad Equipment", new _ddPinPadTable(), true );
          fields.add(field);
          valField = fields.getField("debitPPType");
          if(valField!=null)
          {
            field.addValidation(new ConditionalRequiredValidation
                                  (
                                  new FieldValueCondition(valField,"Need to Purchase"),
                                  "Field required"
                                  )
                               );
          }
          break;

        case PRINTER:
          field = new DropDownField("equip_"+i, "Printer Equipment", new _ddPrinterTable(), true );
          fields.add(field);
          break;

        case PERIPHERAL:
          field = new DropDownField("equip_"+i, "Peripheral Equipment", new _ddPeripheralTable(), true );
          fields.add(field);
          /*
          valField = fields.getField("certegyPeripheral");
          if(valField!=null)
          {
            field.addValidation(new ConditionalRequiredValidation
                                  (
                                  new FieldValueCondition(valField,"Other"),
                                  "Field required"
                                  )
                               );
          }
          */
          break;

        default:
          throw new RuntimeException("Equipment type not supported.");
      }

      ConditionalRequiredValidation crVal =
        new ConditionalRequiredValidation ( new NotCondition(new FieldValueCondition(field,"") ),
                                          "Field required"
                                          );

      field = new DropDownField("acquire_"+i, "Lend Method", new _ddAcquireTable(),true);
      field.addValidation(crVal);
      fields.add(field);

      field = new NumberField("price_"+i,"Price", 10,10, true, 2);
      field.addValidation(crVal);
      fields.add(field);

      field = new DropDownField("chargeTo_"+i, "Charge To", new _ddChargeTable(),true);
      field.addValidation(crVal);
      fields.add(field);

      field = new DropDownField("months_"+i, "Months to Pay", new _ddMonthTable(), true );
      fields.add(field);

    }

  }

  private final void makeFeaturesFields()
  throws Exception
  {
    Field field;

    field = new DropDownField("Terminal Application","Terminal Application",TableFactory.getDropDownTable(TableFactory.TERM_APP),true);
    fields.add(field);

    //Access Code
    field = new CheckField("Access Code","Access Code","Y",false);
    fields.add(field);
    IfYesNotBlankValidation val = new IfYesNotBlankValidation(field, "You must provide an Access Code.");
    field = new Field("Access Code Num","Enter Access Code",true);
    fields.add(field);
    field.addValidation(val);

    //Auto Batch Close
    field = new CheckField("Auto Batch Close","Auto Batch Close","Y",false);
    fields.add(field);
    val = new IfYesNotBlankValidation(field, "You must provide a batch time.");
    field = new Field("Auto Batch Hours","Batch Hours",true);
    fields.add(field);
    field.addValidation(val);
    field = new Field("Auto Batch Minutes","Batch Minutes",true);
    fields.add(field);
    field.addValidation(val);
    field = new RadioField("Auto Batch AMPM","Batch Time",ampm,"");
    fields.add(field);
    field.addValidation(val);

    //Receipt Header 4
    field = new CheckField("Receipt Header Line 4","Receipt Header Line 4","Y",false);
    fields.add(field);
    val = new IfYesNotBlankValidation(field, "You must provide a description.");
    field = new Field("Receipt Header Line 4 Description","Line 4 Description",true);
    fields.add(field);
    field.addValidation(val);

    //Receipt Header 5
    field = new CheckField("Receipt Header Line 5","Receipt Header Line 5","Y",false);
    fields.add(field);
    val = new IfYesNotBlankValidation(field, "You must provide a description.");
    field = new Field("Receipt Header Line 5 Description","Line 5 Description",true);
    field.addValidation(val);
    fields.add(field);

    //Receipt Footer
    field = new CheckField("Receipt Footer","Receipt Footer","Y",false);
    fields.add(field);
    val = new IfYesNotBlankValidation(field, "You must provide a description.");
    field = new Field("Receipt Footer Description","Footer Description",true);
    field.addValidation(val);
    fields.add(field);

    /*
    // phone training
    field = new CheckField("tf_l_f0_Phone Training?","Phone Training for Merchant","Y",false);
    fields.add(field);
    field = new RadioField("tf_l_f1_Phone Training Type","Phone Training for Merchant",phoneTrain,"");
    fields.add(field);
    */

    //reset transaction
    //field = new CheckField("Reset Transaction Number Daily","Reset Transaction Number Daily","Y",false);
    //fields.add(field);

    //invoice prompt
    field = new CheckField("Invoice Number Prompt On","Invoice Number Prompt On","Y",false);
    fields.add(field);

    //fraud control
    field = new CheckField("Fraud Control On","Fraud Control On (Required last 4 digits of card)","Y",false);
    fields.add(field);

    //password protect
    field = new CheckField("Password Protect On","Password Protect On","Y",false);
    fields.add(field);
    val =  new IfYesNotBlankValidation(field, "You must provide password info.");
    field = new Field("Password","Password",true);
    field.addValidation(val);
    fields.add(field);
    field = new Field("Password On","Password On",true);
    field.addValidation(val);
    fields.add(field);

    //purchase card flag
    field = new CheckField("Purchase Card Flag On","Purchase Card Flag On","Y",false);
    fields.add(field);

    //AVS
    field = new CheckField("AVS On","AVS ON","Y",false);
    fields.add(field);

    //CS phone number
    field = new CheckField("Customer Service","Customer Service Phone Number","Y",false);
    fields.add(field);
    val =  new IfYesNotBlankValidation(field, "You must provide a phone number.");
    field = new Field("Customer Service Phone Number","CS Phone Number",true);
    field.addValidation(val);
    fields.add(field);

    /*
    //Terminal Reminder
    field = new CheckField("tf_r_n0_Terminal Reminder To Check Totals?","Terminal Reminder To Check Totals","Y",false);
    fields.add(field);
    val = new IfYesNotBlankValidation(field, "You must provide a reminder time.");
    field = new Field("tf_r_n1_Terminal Reminder Hour","Reminder Hour",true);
    field.addValidation(val);
    fields.add(field);
    field = new Field("tf_r_n2_Terminal Reminder Minute","Reminder Minute",true);
    field.addValidation(val);
    fields.add(field);
    field = new RadioField("tf_r_n3_Terminal Reminder AMPM","Reminder AMPM",ampm,"");
    field.addValidation(val);
    fields.add(field);
    */

    //Tip Option
    field = new CheckField("Tip Option On","Tip Option On","Y",false);
    fields.add(field);

    //Clerk Server prompt
    field = new CheckField("Clerk Server Prompt On","Clerk Server Prompt On","Y",false);
    fields.add(field);

  }

  public void makePostSubmissionFields()
  {
    Field field;

    //field = new DropDownField("App Type","App Type",TableFactory.getDropDownTable(TableFactory.APP_TYPE),true);
    field = new Field("App Type","App Type",20,20,true);
    if(hasItem("App Type"))
    {
      field.setData(getACRValue("App Type"));
    }
    fields.add(field);

    field = new DropDownField("Vendor","Vendor",TableFactory.getDropDownTable(TableFactory.VENDOR),true);
    if(hasItem("Vendor"))
    {
      field.setData(getACRValue("Vendor"));
    }
    fields.add(field);

    field = new Field("VNum","VNum",true);
    if(hasItem("VNum"))
    {
      field.setData(getACRValue("VNum"));
    }
    fields.add(field);

  }

  public String renderEquipmentHTML()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<table border=0 cellpadding=0 cellspacing=0 width=100%>");
    sb.append("<tr>");
    sb.append("<td colspan=2 class=\"semiSmallBold\">Equipment</td>");
    sb.append("<td class=\"semiSmallBold\">Lend Type</td>");
    sb.append("<td class=\"semiSmallBold\">Price</td>");
    sb.append("<td class=\"semiSmallBold\">Charge to</td>");
    sb.append("<td class=\"semiSmallBold\">Months</td>");
    sb.append("</tr>");
    sb.append("<tr><td colspan=6 height=1><img src=\"/images/spcr_transp.gif\" height=1></td></tr>");
    if(editable)
    {
      for(int i = 1; i < 5; i++)
      {

        sb.append("<tr>");
        sb.append("<td class=\"semiSmallBold\">");
        switch(i)
        {
          case TERMINAL:
            sb.append("Terminal");
            break;

          case PINPAD:
            sb.append("PinPads");
            break;

          case PRINTER:
            sb.append("Printer");
            break;

          case PERIPHERAL:
            sb.append("Peripherals");
            break;

          default:
            throw new RuntimeException("Equipment type not supported.");
        }
        sb.append("</td>");

        sb.append("<td>").append(renderHtml("equip_"+i)).append("</td>");
        sb.append("<td>").append(renderHtml("acquire_"+i)).append("</td>");
        sb.append("<td>").append(renderHtml("price_"+i)).append("</td>");
        sb.append("<td>").append(renderHtml("chargeTo_"+i)).append("</td>");
        sb.append("<td>").append(renderHtml("months_"+i)).append("</td>");
        sb.append("</tr>");
      }
    }
    else
    {
      if(hasItem("equip"))
      {
        //build HTML from acri items involved.
        for(int i = 1; i < 5; i++)
        {
          if(hasItem("equip_"+i))
          {
            sb.append("<tr>");
            sb.append("<td colspan=2>").append(renderHtml("equip_"+i)).append("</td>");
            sb.append("<td>").append(renderHtml("acquire_"+i)).append("</td>");
            sb.append("<td>").append(renderHtml("price_"+i)).append("</td>");
            sb.append("<td>").append(renderHtml("chargeTo_"+i)).append("</td>");
            sb.append("<td>").append(renderHtml("months_"+i)).append("</td>");
            sb.append("</tr>");
          }
        }
      }
      else
      {
        sb.append("No new equipment selected.");
      }
    }

    sb.append("</table>");

    return sb.toString();
  }


  public String renderFeatureHTML()
  {

    StringBuffer sb = new StringBuffer();
    sb.append("<table border=0 cellpadding=0 cellspacing=0 width=100%>");
    if(editable || hasItem("Terminal Application"))
    {
      sb.append("<tr>");
      sb.append("<td class=\"semiSmallBold\">Terminal Application</td>");
      sb.append("<td colspan=2>").append(renderHtml("Terminal Application")).append("</td>");
      sb.append("</tr>");
    }

    if(editable || hasItem("Access Code"))
    {
      sb.append("<tr>");
      sb.append("<td>").append(renderHtml("Access Code")).append("</td>");
      sb.append("<td colspan=2>").append(renderHtml("Access Code Num")).append("</td>");
      sb.append("</tr>");
    }

    if(editable || hasItem("Auto Batch Close"))
    {
      sb.append("<tr>");
      sb.append("<td><div style=\"float: left;\">").append(renderHtml("Auto Batch Close")).append("</div><div style=\"text-valign: middle; text-align: right;\">HH:&nbsp;</div></td>");
      sb.append("<td>").append(renderHtml("Auto Batch Hours")).append("&nbsp;&nbsp;MM:").append(renderHtml("Auto Batch Minutes")).append("</td>");
      sb.append("<td>").append(renderHtml("Auto Batch AMPM")).append("</td>");
      sb.append("</tr>");
    }

    if(editable || hasItem("Access"))
    {
      sb.append("<tr>");
      sb.append("<td>").append(renderHtml("Access")).append("</td>");
      sb.append("<td colspan=2>").append(renderHtml("Access")).append("</td>");
      sb.append("</tr>");
    }

    if(editable || hasItem("Receipt Header Line 4"))
    {
      sb.append("<tr>");
      sb.append("<td>").append(renderHtml("Receipt Header Line 4")).append("</td>");
      sb.append("<td colspan=2>").append(renderHtml("Receipt Header Line 4 Description")).append("</td>");
      sb.append("</tr>");
    }

    if(editable || hasItem("Receipt Header Line 5"))
    {
      sb.append("<tr>");
      sb.append("<td>").append(renderHtml("Receipt Header Line 5")).append("</td>");
      sb.append("<td colspan=2>").append(renderHtml("Receipt Header Line 5 Description")).append("</td>");
      sb.append("</tr>");
    }

    if(editable || hasItem("Receipt Footer"))
    {
      sb.append("<tr>");
      sb.append("<td>").append(renderHtml("Receipt Footer")).append("</td>");
      sb.append("<td colspan=2>").append(renderHtml("Receipt Footer Description")).append("</td>");
      sb.append("</tr>");
    }

    if(editable || hasItem("Fraud Control On"))
    {
      sb.append("<tr>");
      sb.append("<td colspan=3>").append(renderHtml("Fraud Control On")).append("</td>");
      sb.append("</tr>");
    }

    if(editable || hasItem("Password Protect On"))
    {
      sb.append("<tr>");
      sb.append("<td><div style=\"float: left;\">").append(renderHtml("Password Protect On")).append("</div><div  style=\"text-align: right;\">Password?&nbsp;</div></td>");
      sb.append("<td><div style=\"float: left;\">").append(renderHtml("Password")).append("</div><div style=\"text-align: right;\">Put password on:&nbsp;</div></td>");
      sb.append("<td>").append(renderHtml("Password On")).append("</td>");
      sb.append("</tr>");
    }

    if(editable || hasItem("Purchase Card Flag On"))
    {
      sb.append("<tr>");
      sb.append("<td colspan=3>").append(renderHtml("Purchase Card Flag On")).append("</td>");
      sb.append("</tr>");
    }

    if(editable || hasItem("AVS On"))
    {
      sb.append("<tr>");
      sb.append("<td colspan=3>").append(renderHtml("AVS On")).append("</td>");
      sb.append("</tr>");
    }

    if(editable || hasItem("Customer Service"))
    {
      sb.append("<tr>");
      sb.append("<td>").append(renderHtml("Customer Service")).append("</td>");
      sb.append("<td colspan=2>").append(renderHtml("Customer Service Phone Number")).append("</td>");
      sb.append("</tr>");
    }

    if(editable || hasItem("Tip Option On"))
    {
      sb.append("<tr>");
      sb.append("<td colspan=3>").append(renderHtml("Tip Option On")).append("</td>");
      sb.append("</tr>");
    }

    if(editable || hasItem("Clerk Server Prompt On"))
    {
      sb.append("<tr>");
      sb.append("<td colspan=3>").append(renderHtml("Clerk Server Prompt On")).append("</td>");
      sb.append("</tr>");
    }

    sb.append("</table>");

    return sb.toString();

  }

  public boolean calltaggedEquipmentExists()
  {
    if(hasItem("debitPPad") &&
      ! ( getACRValue("debitSerNum").equals("") )  )
    {
      return true;
    }
   return false;
  }

 public Vector getCallTaggedEquipment()
 {
   Vector v = null;
   String serNum = getACRValue("debitSerNum");
   if(serNum.length()>0)
   {
     v = new Vector();
     v.add(new EquipmentKey(null,serNum));
   }
   return v;
 }

  public boolean hasItem(String itemName)
  {
    if(editable)
    {
      return false;
    }
    else
    {
      //check the ACRIItems...
      //assumption here is that the itemName must be
      //the first part of the name
      ACRItem item;
      for(Enumeration e = getACR().getItemEnumeration(); e.hasMoreElements();)
      {
        item = (ACRItem)e.nextElement();
        if( item.getName().indexOf(itemName)==0 &&
            !item.getValue().equalsIgnoreCase("n") )
        {
          return true;
        }
      }
      return false;
    }
  }

  public String renderHtml(String itemName)
  {

    if( editable ||
        fields.getField(itemName)!=null
       )
    {
      return super.renderHtml(itemName);
    }
    else
    {
      String s = getACR().getACRValue(itemName);
      if( s.equalsIgnoreCase("n") )
      {
        return "";
      }
      else if( s.equalsIgnoreCase("y") )
      {
        return itemName;
      }
      else
      {
        return s;
      }
    }
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT:
      case MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP:
      case MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP:
      case MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  public String getHeadingName()
  {
    return "Debit, Check Services, Gift Cards (Value Added Services)";
  }

  public class _ddPinPadTable extends DropDownTable
  {
    public _ddPinPadTable()
    {
      addElement("","Select One");
      addElement("Hypercom 1300 (15 ft. cord)", "Hypercom 1300 (15 ft. cord)");
      addElement("Hypercom 1300 (6 ft. cord)", "Hypercom 1300 (6 ft. cord)");
      addElement("VeriFone PinPad 1000se", "VeriFone PinPad 1000se");
      addElement("VeriFone PP1000se PCI", "VeriFone PP1000se PCI");
    }
  }

  public class _ddCompEquipTable extends DropDownTable
  {
    public _ddCompEquipTable()
    {
      addElement("","Select One");
      addElement("Hypercom T7P Thermal", "Hypercom T7P Thermal");
      addElement("Hypercom T7plus 1.0mb", "Hypercom T7plus 1.0mb");
      addElement("Hypercom T4205", "Hypercom T4205");
      addElement("Hypercom T4210", "Hypercom T4210");
      addElement("Hypercom T4220", "Hypercom T4220");
      addElement("Hypercom M4230", "Hypercom M4230");
      addElement("Hypercom M4100", "Hypercom M4100");
      addElement("VeriFone Omni 3730LE", "VeriFone Omni 3730LE");
      addElement("Verifone Vx510LE", "Verifone Vx510LE");
      addElement("Verifone Vx570", "Verifone Vx570");
      addElement("Verifone 3750 4.0mg", "Verifone 3750 4.0mg");
      addElement("VeriFone Omni 3750 4.0mg/Dualcom", "VeriFone Omni 3750 4.0mg/Dualcom");
      addElement("VeriFone Vx570 DualCom", "VeriFone Vx570 DualCom");
      addElement("Verifone Omni 3200se", "Verifone Omni 3200se");
    }
  }

  public class _ddCompEquipTable_cert extends DropDownTable
  {
    public _ddCompEquipTable_cert()
    {
      addElement("","Select One");
      addElement("Hypercom T7P 1.0 mb","Hypercom T7P 1.0 mb");
      addElement("Hypercom T7Plus 1.0 mb","Hypercom T7Plus 1.0 mb");
      addElement("Ice 5500 Plus 1.5 mb","Ice 5500 Plus 1.5 mb");
      addElement("Ice 5700 Plus 1.5 mb w/checkreader","Ice 5700 Plus 1.5 mb w/checkreader");
      addElement("Verifone Omni 3750","Verifone Omni 3750");
      addElement("Other","Other Terminal Type");
    }
  }

  public class _ddPeripheralTable extends DropDownTable
  {
    public _ddPeripheralTable()
    {
      addElement("","Select One");
      addElement("VeriFone CR600 Checkreader","VeriFone CR600 Checkreader");
      addElement("Mag Tek Mini MICR Checkreader","Mag Tek Mini MICR Checkreader");
      addElement("Other Terminal Type","Other Terminal Type");
      addElement("Internal","Internal");
    }
  }

  public class _ddPeripheralAdvancedTable extends DropDownTable
  {
    public _ddPeripheralAdvancedTable()
    {
      addElement("","Select One");
      addElement("--","----VeriFone Terminals Only----");
      addElement("VeriFone CR600 Checkreader","VeriFone CR600 Checkreader");
      addElement("--","----Hypercom Terminals Only----");
      addElement("Mag Tek Mini MICR Checkreader - T7P with pinpad","Mag Tek Mini MICR Checkreader - T7P with pinpad");
      addElement("Mag Tek Mini MICR Checkreader - T7P no pinpad","Mag Tek Mini MICR Checkreader - T7P no pinpad");
      addElement("Mag Tek Mini MICR Checkreader - T77","Mag Tek Mini MICR Checkreader - T77");
      addElement("Mag Tek Mini MICR Checkreader - T7Plus","Mag Tek Mini MICR Checkreader - T7Plus");
      addElement("Mag Tek Mini MICR Checkreader - ICE 5500 Plus","Mag Tek Mini MICR Checkreader - ICE 5500 Plus");
    }
  }

  public class _ddPrinterTable extends DropDownTable
  {
    public _ddPrinterTable()
    {
      addElement("","Select One");
      /*
      addElement("Citizen IDP 560 Roll","Citizen IDP 560 Roll");
      addElement("Citizen IDP 562 Roll","Citizen IDP 562 Roll");
      addElement("DataCard Silent Partner","DataCard Silent Partner");
      addElement("IVI (Ingenico) Scribe 612","IVI (Ingenico) Scribe 612");
      addElement("Hypercom P7 Roll","Hypercom P7 Roll");
      addElement("Hypercom P8 Roll","Hypercom P8 Roll");
      addElement("Hypercom P8 Sprocket","Hypercom P8 Sprocket");
      addElement("Verifone Printer 250","Verifone Printer 250");
      */
      addElement("Verifone Printer 900R","Verifone Printer 900R");
    }
  }

  public class _ddMonthTable extends DropDownTable
  {
    public _ddMonthTable()
    {
      addElement("","");
      addElement("1","1");
      addElement("2","2");
      addElement("3","3");
      addElement("4","4");
      addElement("ongoing","ongoing");
    }

  } // _ddMonthTable class

  public class _ddChargeTable extends DropDownTable
  {
    public _ddChargeTable()
    {
      addElement("","");
      addElement("Merchant","Merchant");
      addElement("Bank","Bank");
    }

  } // _ddChargeTable class

  public class _ddAcquireTable extends DropDownTable
  {
    public _ddAcquireTable()
    {
      addElement("","");
      addElement("Purchase","Purchase");
      addElement("Rent","Rent");
    }

  } // _ddAcquireTable class


  /**
   * Uses the auth fee table factory to create a drop down field with the
   * given card type's allowed auth fees.
   * (copied from ..CardAdds_CBT..)
   */
  protected Field createFeeField(String fieldName, String fieldLabel,
    int cardType)
  {
    // pull the app type out of "universal" acr item storage
    int appType =
      Integer.parseInt(acr.getItemFromDefName("ACR Type Code").getValue());

    int typeCode =
      Integer.parseInt(acr.getItemFromDefName("Type Code").getValue());

    // create a fee table based on the app type and card type
    DropDownTable feeTable =
      AuthFeeTableFactory.createFeeTable(appType,cardType,typeCode);

    // return a drop down field
    return new DropDownField(fieldName,fieldLabel,feeTable,true);
  }


} // class ACRDataBean_OtherCardMgmt
