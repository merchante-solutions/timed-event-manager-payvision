package com.mes.acr;

import java.util.Vector;

/**
 * ACRDefinitionDAO interface
 * 
 * DAO definition for ACRDefinition class.
 */
public interface ACRDefDAO
{
  // constants
  // (none)
  
  // methods
  
  /**
   * createACRDefinition()
   * 
   * Encapulates the instantiation of an ACRDefinition object 
   *  allowing for DAO specific ops when creating ACRDefinitions.
   */
  public ACRDefinition      createACRDefinition();
  
  public long               insertACRDefinition(ACRDefinition acrd);
  public boolean            deleteACRDefinition(ACRDefinition acrd);
  public ACRDefinition      findACRDefinition(long pkid);
  
  /**
   * updateACRDefinition()
   * 
   * Requires the object to have already been persisted.
   */
  public boolean          updateACRDefinition(ACRDefinition acrd);
  
  /**
   * persistACRDefinition()
   * 
   * Akin to save op.
   * Does not require the object to have already been persisted.
   */
  public boolean            persistACRDefinition(ACRDefinition acrd);
  
  public Vector             selectACRDefinitions(String fldNme,String fldVal,boolean bLoadItems);
    // collection of ACRDefinition objects
  public ACRDefinition      findACRDefinitionByACRID(long pkid);

} // interface ACRDefDAO
