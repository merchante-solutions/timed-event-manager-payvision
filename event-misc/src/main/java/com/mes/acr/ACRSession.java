package com.mes.acr;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.NoSuchElementException;
import org.apache.log4j.Logger;
import com.mes.user.UserBean;


/**
 * ACRSession
 *
 * Intended to be implemented having session scope.  I.e. this bean to represent state for either an ACR submission (more than one ACR possible) or ACR status request.
 */
public final class ACRSession extends Object
{
  // create class log category
  static Logger log = Logger.getLogger(ACRSession.class);

  // constants

  // data members
  private MerchantInfo      merchantInfo;
  private Requestor         requestor;
  private UserBean          loginUser;
  private boolean           defsLoaded;
  private Hashtable         submitBeans = null;
            // name:  {definition short name}
            // value: { ACRDataBean object }
  private ACRDataBeanBase   crntBean;
  private boolean           postSubmit;
    // above members apply to submit mode only

  //private Vector            acrDefs;        // master listing of available ACR Definitions
  private ACRDataBeanBase   statBean;

  private String            dfltRequestorName;
  private String            dfltRequestorTitle;
  private String            dfltRequestorPhone;

  private DAOFactory        daof;           // persistence mechanism (non-state related)

  private ACRDefSets        defSets;        // new definition manager


  // class functions


  // object functions

  // construction
  public ACRSession()
  {
    merchantInfo = null;
    requestor = null;
    loginUser = null;

    defsLoaded=false;
    //acrDefs=null;
    submitBeans=new Hashtable();
    crntBean=null;
    statBean=null;

    dfltRequestorName="";
    dfltRequestorTitle="";
    dfltRequestorPhone="";

    daof = DAOFactoryBuilder.getDAOFactory(DAOFactory.DAO_MESDB);

    defSets = null;
  }

  private void clear()
  {
    defsLoaded=false;

    if(merchantInfo!=null)
      merchantInfo.clear();

    if(requestor!=null)
      requestor.clear();

    if(loginUser!=null)
      loginUser.clear();

    //if(acrDefs!=null)
    //  acrDefs.removeAllElements();

    if(submitBeans!=null)
      submitBeans.clear();

    crntBean=null;
    statBean=null;

    postSubmit=false;

    dfltRequestorName="";
    dfltRequestorTitle="";
    dfltRequestorPhone="";

    defSets = null;
    // NOTE: Retain DAO Factory instance (not state related)
  }

  /**
   * getDAOFactory()
   *
   * Returns the member instance of the DAOFactory.
   * Hides the knowledge of which concrete factory is in use.
   */
  public DAOFactory getDAOFactory()
  {
    return daof;
  }

  public boolean loadDefinitions()
  {
    // make sure the new def sets object exists
    if (defSets == null)
    {
      defSets = new ACRDefSets();
      defsLoaded = true;
    }
    return true;

    /*
    // if defs already loaded, don't do it again
    if (!defsLoaded)
    {
      log.debug("Loading ACR definitions in session...");
      try
      {
        // don't allow defs to be loaded if user is null
        if (loginUser == null)
        {
          // is this really necessary?
          // user has nothing to do with loading defs...
          throw new Exception("loginUser is null");
        }

        // load acr definitions from database
        ACRDefDAO defDao = daof.getACRDefinitionDAO();
        acrDefs = defDao.selectACRDefinitions("ALL",null,true);
        if (acrDefs == null)
        {
          throw new Exception("defDAO.selectACRDefinitions"
            + "(\"ALL\",null,true) returned null");
        }
        defsLoaded = true;
        log.debug("ACRs loaded into session.");
      }
      catch (Exception e)
      {
        log.error("loadDefinitions() error: " + e);
      }
      finally
      {
        if (!defsLoaded)
        {
          clear();
        }
      }
    }

    return defsLoaded;
    */
  }

  /**
   * OBSOLETE: acr def sets manager now takes over "filtering"
   *
   * Turns off any acr defs that are flagged off in the database, that require
   * a right the user does not have or that are not ok for the merchant acct
   * app type.  Any others are turned on.
   */
  public void filterDefinitions()
  {
    log.debug("call to obsolete filterDefinitions() method");
    /*
    for (Iterator i = acrDefs.iterator(); i.hasNext();)
    {
      ACRDefinition acrDef = (ACRDefinition)i.next();

      boolean dbFlag    = acrDef.isOn();
      boolean hasRight  = loginUser.hasRight(acrDef.getAssociatedUserRight());
      boolean okAppType =
        acrDef.isAvailableByOrgAppType(merchantInfo.getACRAppTypeCode());

      boolean isOn      = dbFlag && hasRight && okAppType;

      if (isOn)
      {
        acrDef.turnOn();
      }
      else
      {
        acrDef.turnOff();
      }

      log.debug((isOn ? "ON:  " : "OFF: ") + acrDef.getShortName()
        + (isOn ? "" : " ( db: " + dbFlag + " right: " + hasRight
        + " appType: " + okAppType + " )"));
    }
    */
  }

  public void unload()
  {
    clear();
  }

  public void unloadSubmitBeans()
  {
    if(submitBeans!=null)
      submitBeans.clear();

    crntBean=null;

    dfltRequestorName="";
    dfltRequestorTitle="";
    dfltRequestorPhone="";
  }

  public void unloadSubmitBean(String acrDefShrtNme)
  {
    if(submitBeans==null || acrDefShrtNme==null)
       return;

    if(submitBeans.containsKey(acrDefShrtNme))
      submitBeans.remove(acrDefShrtNme);

    if(crntBean!=null && crntBean.getACR().getDefinition().getShortName().equals(acrDefShrtNme))
      crntBean=null;
  }

  public boolean areDefsLoaded()
  {
    return defsLoaded;
  }

  public boolean isMerchantInfoLoaded()
  {
    return (merchantInfo!=null);
  }

  public String toString()
  {
    final String nl = System.getProperty("line.separator");

    StringBuffer sb = new StringBuffer();

    if(merchantInfo!=null)
      sb.append(nl+"MerchantInfo: "+merchantInfo.toString());

    if(requestor!=null)
      sb.append(nl+"Requestor: "+requestor.toString());

    sb.append(nl+"defsLoaded="+defsLoaded);
    sb.append(nl+"crntBean="+((crntBean==null)?"NULL":crntBean.getACRID()));
    sb.append(nl+"statBean="+((statBean==null)?"NULL":statBean.getACRID()));
    sb.append(nl+"postSubmit="+postSubmit);
    sb.append(nl+"dfltRequestorName="+dfltRequestorName);
    sb.append(nl+"dfltRequestorTitle="+dfltRequestorTitle);
    sb.append(nl+"dfltRequestorPhone="+dfltRequestorPhone);
    //sb.append(nl+"acrDefs:"+acrDefs.toString()+nl);
    if(submitBeans!=null)
      sb.append(nl+"submitBeans:"+submitBeans.toString());

    return sb.toString();
  }

  public MerchantInfo getMerchantInfo()
  {
    return merchantInfo;
  }

  public Requestor getRequestor()
  {
    return requestor;
  }

  public UserBean getLoginUser()
  {
    return loginUser;
  }

  public String getDfltRqstrName()
  {
    return dfltRequestorName;
  }
  public String getDfltRqstrTitle()
  {
    return dfltRequestorTitle;
  }
  public String getDfltRqstrPhone()
  {
    return dfltRequestorPhone;
  }

  public void setDfltRqstrName(String v)
  {
    if(v!=null)
      dfltRequestorName=v;
  }
  public void setDfltRqstrTitle(String v)
  {
    if(v!=null)
      dfltRequestorTitle=v;
  }
  public void setDfltRqstrPhone(String v)
  {
    if(v!=null)
      dfltRequestorPhone=v;
  }

  public void setMerchantInfo(String mn)
  {
    if(mn==null || (merchantInfo!=null && mn.equals(merchantInfo.getMerchantNum())))
      return;

    MerchantInfo temp = daof.getMerchantInfoDAO().findMerchantInfo(mn);
    if(temp==null)
      return;

    unloadSubmitBeans();

    merchantInfo=temp;
    log.debug("ACRSession.merchantInfo data member set:\r\n"+merchantInfo.toString());
  }

  public void setRequestor(Requestor requestor)
  {
    if(requestor==null)
      return;

    this.requestor=requestor;
  }

  public void setLoginUser(UserBean loginUser)
  {
    if(loginUser!=null)
      this.loginUser=loginUser;
  }

  private String lastMerchNum = null;
  private List lastDefSet = null;

  /**
   * New version of acr defs accessor.  The def sets manager object is now used
   * to retrieve the subset of acr defs that are associated with the merchant
   * account indicated by merchant info.  This means merchant info must be set
   * in order for the defs to be retrievable.  The list is cached and will not
   * be refetched if the merch info has not changed since the last call to
   * avoid db overhead.
   */
  public List getACRDefs()
  {
    // make sure a merch num is available
    if (merchantInfo == null)
    {
      log.error("Attempt to access acr def set but merchant info is not set");
      return null;
    }

    // is it ok to return the cached list?
    String merchNum = merchantInfo.getMerchantNum();
    if (lastMerchNum != null && merchNum.equals(lastMerchNum) &&
        lastDefSet != null)
    {
      log.debug("Cached def set returned from getACRDefs()");
      return lastDefSet;
    }

    // need to fetch a new def set
    List acrDefs = defSets.getAccountDefSet(merchNum);
    if (acrDefs != null)
    {
      // cache the data
      lastMerchNum = merchNum;
      lastDefSet = acrDefs;

      log.debug("Def set selected and cached for merch num " + merchNum);
    }
    else
    {
      log.debug("Unable to retrieve acr def set for merch num " + merchNum);
    }

    return acrDefs;
  }

  /**
   * OBSOLETE: ACRDefSets now manages acr defs.
   */
  public Enumeration getACRDefsEnumerator()
  {
    return null;
    /*
    if(!defsLoaded || acrDefs==null)
      return null;

    return acrDefs.elements();
    */
  }

  public Hashtable getSubmitBeans()
  {
    return submitBeans;
  }

  /**
   * getACRDefinition()
   *
   * Retreives the ACR Definition of the specified name.
   */
  public ACRDefinition getACRDefinition(String shortName)
  {
    ACRDefinition acrDef = null;

    if (defSets == null)
    {
      log.warn("getACRDefinition('" + shortName + "') failed because"
        + " acr def sets has not been loaded.");
    }
    else if ((acrDef = defSets.getDef(shortName)) == null)
    {
      log.error("getACRDefinition('" + shortName + "') failed");
    }

    return acrDef;
  }

  /**
   * getSubmitBean()
   *
   * Retreives the submit bean corres. to the spec. ACR definition short name if present.
   */
  public ACRDataBeanBase getSubmitBean(String acrDefShrtNme)
  {
    if(!defsLoaded || submitBeans==null) {
      log.warn("getSubmitBean() failed.");
      return null;
    }

    if(!submitBeans.containsKey(acrDefShrtNme))
      return null;

    return (ACRDataBeanBase)submitBeans.get(acrDefShrtNme);
  }

  public ACR getSubmitAcr(String acrDefShrtNme)
  {
    ACRDataBeanBase bean;

    if((bean=getSubmitBean(acrDefShrtNme))==null)
      return null;

    return bean.getACR();
  }

  public ACR getStatusAcr()
  {
    if(statBean==null)
      return null;

    return statBean.getACR();
  }

  public boolean isPostSubmit()
  {
    return postSubmit;
  }

  public void setIsPostSubmit(boolean v)
  {
    postSubmit=v;
  }

  /**
   * setStatusBean()
   *
   * Sets the statBean data member given its primary key (acrId).
   *
   * @param   acrId      ACR ID: ACR.ACR_SEQ_NUM
   *
   * @return  Returns the bean reference if successful null otherwise.
   */
  public ACRDataBeanBase setStatusBean(long acrId)
  {
    // this line removed so that the acr is refreshed from the database every time
    /*
    if(statBean!=null && statBean.getACR().getID()==acrId)
      return statBean;
    */

    if(!defsLoaded) {
      if(!loadDefinitions()) {
        log.error("Unable to set status bean: Unable to load ACR definitions.");
        return null;
      }
    }

    ACR acr=null;

    // attempt to load
    ACRDAO acrDAO = daof.getACRDAO();
    if((acr=acrDAO.findACR(acrId))==null) {
      log.error("Set Status Bean failed: Unable to find ACR of ID '"+acrId+"'.");
      return null;
    }

    // NEW: retrieve acr def from def set, set it in the acr
    ACRDefinition acrDef = defSets.getDef(acr.getDefID());
    if (acrDef == null)
    {
      log.error("Definition not found for ACR with id " + acrId
        + " with def id " + acr.getDefID());
      return null;
    }
    acr.setDefinition(acrDef);

    /*
    // bind just loaded acr to assoc. acr def
    // NOTE: all ACRs MUST have an assoc. definition to be retained!
    for(int i=0;i<acrDefs.size();i++) {
      if(((ACRDefinition)acrDefs.elementAt(i)).getID()==acr.getDefID()) {
        acrd=(ACRDefinition)acrDefs.elementAt(i);
        break;
      }
    }
    */

    /*
    if(acrd==null) {
      // ACR Def not loaded so attempt to load
      ACRDefDAO acrDefDAO = daof.getACRDefinitionDAO();
      if((acrd=acrDefDAO.findACRDefinition(acr.getDefID()))==null) {
        log.error("Unable to set status Bean: Could not find associated ACR Definition of ID '"+acr.getDefID()+"'.");
        return null;
      }
      acrDefs.addElement(acrd);
    }
    */

    if(acrDef == null) {
      log.error("Set Status Bean failed: Unable to find associated definition.");
      return null;
    }

    acr.setDefinition(acrDef);

    ACRDataBeanBase bean;

    if((bean=ACRDataBeanBuilder.buildDetailInStatusMode(acr,loginUser))==null) {
      log.error("Unable to set status bean: Could not build bean.  Aborted.");
      return null;
    }

    // retain just loaded bean
    statBean=bean;

    return statBean;
  }

  public ACRDataBeanBase setCurrentBean(ACRDataBeanBase bean)
  {
    if(bean!=null)
      crntBean=bean;

    return crntBean;
  }

  public ACRDataBeanBase setCurrentBean(long acrId)
  {
    if(!defsLoaded || submitBeans==null) {
      log.warn("Attempt to call setCurrentBean() failed.");
      return null;
    }

    if(crntBean!=null && crntBean.getACR().getID()==acrId)
      return crntBean;

    ACRDataBeanBase bean;

    // is acr present in acr collection?
    for(Enumeration e=submitBeans.elements();e.hasMoreElements();) {
      if((bean=(ACRDataBeanBase)e.nextElement()).getACR().getID()==acrId) {
        crntBean=bean;
        log.debug("Current ACRDataBeanBase of ACRID: '"+acrId+"' already loaded.");
        return crntBean;
      }
    }

    ACR acr=null;

    // attempt to load the acr
    ACRDAO acrDAO = daof.getACRDAO();
    if((acr=acrDAO.findACR(acrId))==null) {
      log.error("setCurrentACR failed: Unable to find ACR of ID '"+acrId+"'.");
      log.error("Unable to find ACR of ID '"+acrId+"'.");
      return null;
    }

    // NEW: retrieve acr def from def set, set it in the acr
    ACRDefinition acrDef = defSets.getDef(acr.getDefID());
    if (acrDef == null)
    {
      log.error("Definition not found for ACR with id " + acrId
        + " with def id " + acr.getDefID());
      return null;
    }
    acr.setDefinition(acrDef);

    /*
    ACRDefinition acrd=null;

    // bind just loaded acr to assoc. acr def
    // NOTE: all ACRs MUST have an assoc. definition to be retained!
    for(int i=0;i<acrDefs.size();i++) {
      if(((ACRDefinition)acrDefs.elementAt(i)).getID()==acr.getDefID()) {
        acrd=(ACRDefinition)acrDefs.elementAt(i);
        break;
      }
    }
    if(acrd==null) {
      // ACR Def not loaded so attempt to load
      ACRDefDAO acrDefDAO = daof.getACRDefinitionDAO();
      if((acrd=acrDefDAO.findACRDefinition(acr.getDefID()))==null) {
        log.error("Unable to set current ACR: Could not find associated ACR Definition of ID '"+acr.getDefID()+"'.");
        return null;
      }
      acrDefs.addElement(acrd);
    }
    acr.setDefinition(acrd);
    */

    // get the bean type from the acr def
    bean = ACRDataBeanBuilder.buildDetailInSubmitMode(acr,requestor,merchantInfo,loginUser);
    if(bean==null) {
      log.error("setCurrentBean() - ACRDataBeanBuilder.buildDetail() FAILED.");
      return null;
    }

    // retain just loaded acr
    crntBean=bean;
    submitBeans.put(acrDef.getShortName(),crntBean);

    return crntBean;
  }

  public ACRDataBeanBase getCurrentBean()
  {
    if(!defsLoaded || crntBean==null)
      return null;

    return crntBean;
  }

  public ACRDataBeanBase getStatusBean()
  {
    return statBean;
  }

  /**
   * getSubmitBeansEnumerator()
   *
   * Retreives the enumerator for the outside world to use to iterate the submit Beans.
   */
  public Enumeration getSubmitBeansEnumerator()
  {
    return new submitBeansEnumerator();
  }

  private class submitBeansEnumerator implements Enumeration
  {
    ACRDataBeanBase current;
    List acrDefs;

    public submitBeansEnumerator()
    {
      current=null;

      if(!defsLoaded || submitBeans==null)
        return;

      acrDefs = getACRDefs();

      ACRDefinition acrd;

      for(int i=0;i<acrDefs.size();i++) {
        acrd=(ACRDefinition)acrDefs.get(i);
        if(submitBeans.containsKey(acrd.getShortName())) {
          current=(ACRDataBeanBase)submitBeans.get(acrd.getShortName());
          break;
        }
      }

    }

    public boolean hasMoreElements()
    {
      return (current!=null);
    }

    public Object nextElement()
    {
      if(current==null)
        throw new NoSuchElementException("Submit Beans");

      Object value=current;
      ACRDefinition acrd;

      current=null;

      if(submitBeans==null)
        return value;

      for(int i=acrDefs.indexOf(((ACRDataBeanBase)value).getACRDefinition())+1;i<acrDefs.size();i++) {
        acrd=(ACRDefinition)acrDefs.get(i);
        if(submitBeans.containsKey(acrd.getShortName())) {
          current=(ACRDataBeanBase)submitBeans.get(acrd.getShortName());
          break;
        }
      }

      return value;
    }

  } // class submitAcrsEnumerator

  public boolean addSubmitBean(ACR acr)
  {
    if(!defsLoaded) {
      log.error("Unable to add ACR: ACRSession not loaded.");
      return false;
    }

    if(acr==null) {
      log.error("Unable to add bean: NULL ACR ref. specified.");
      return false;
    }
    if(acr.getDefinition()==null) {
      log.error("Unable to add bean: No bound ACR definition.");
      return false;
    }
    if(!getACRDefs().contains(acr.getDefinition())) {
      log.error("Unable to add submit bean: Bound definition '"+acr.getDefinition().getName()+"' not found in loaded definitions.");
      return false;
    }

    ACRDataBeanBase bean;

    // build the bean from the ACR ref.
    if((bean=ACRDataBeanBuilder.buildDetailInSubmitMode(acr,requestor,merchantInfo,loginUser))==null) {
      log.error("Add submit bean failed: Unable to build detail bean.");
      return false;
    }

    // retain bean (removing any previous acr in its place)
    try {
      String acrDefShrtNme = bean.getACRDefinition().getShortName();
      if(submitBeans.containsKey(acrDefShrtNme))
        submitBeans.remove(acrDefShrtNme);
      submitBeans.put(acrDefShrtNme,bean);
    }
    catch(Exception e) {
      log.error("ACRSession.addSubmitBean() EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }

    return true;
  }

} // class ACRSession
