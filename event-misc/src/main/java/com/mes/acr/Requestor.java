package com.mes.acr;

// log4j classes.
import org.apache.log4j.Logger;
import com.mes.database.ValueObjectBase;

/**
 * Requestor class
 *
 * Represents the person responsible for initiating an ACR.
 */
public class Requestor extends ValueObjectBase
{
  // create class log category
  static Logger log = Logger.getLogger(Requestor.class);

  // constants


  // data members
  protected String          requestor_name;
  protected String          requestor_title;
  protected String          requestor_phone;
  protected String          sales_channel;
  protected String          source_userid;

  // class methods
  // (NONE)

  // object methods

  // construction
  public Requestor()
  {
    super(true);

    clear();
  }

  public void clear()
  {
    requestor_name="";
    requestor_title="";
    requestor_phone="";
    sales_channel="";
    source_userid="";

    is_dirty=true;
  }

  // accessors

  public String         getRequestorName() { return requestor_name; }
  public String         getRequestorTitle() { return requestor_title; }
  public String         getRequestorPhone() { return requestor_phone; }
  public String         getSalesChannel() { return sales_channel; }
  public String         getSourceUserid() { return source_userid; }

  // mutators

  public void setRequestorName(String v)
  {
    if(v==null || v.equals(requestor_name))
      return;

    requestor_name=v;
    is_dirty=true;
  }
  public void setRequestorTitle(String v)
  {
    if(v==null || v.equals(requestor_title))
      return;

    requestor_title=v;
    is_dirty=true;
  }
  public void setRequestorPhone(String v)
  {
    if(v==null || v.equals(requestor_phone))
      return;

    requestor_phone=v;
    is_dirty=true;
  }
  public void setSalesChannel(String v)
  {
    if(v==null || v.equals(sales_channel))
      return;

    sales_channel=v;
    is_dirty=true;
  }
  public void setSourceUserid(String v)
  {
    if(v==null || v.equals(source_userid))
      return;

    source_userid=v;
    is_dirty=true;
  }

  ///////////////////

  public String toString()
  {
    final String nl = System.getProperty("line.separator");

    StringBuffer sb = new StringBuffer(256);

    sb.append(nl);
    sb.append("Requestor: ");

    sb.append("requestor_name="+requestor_name);
    sb.append(", requestor_title="+requestor_title);
    sb.append(", requestor_phone="+requestor_phone);
    sb.append(", sales_channel="+sales_channel);
    sb.append(" - "+(is_dirty? "DIRTY":"CLEAN"));

    return sb.toString();
  }

} // class Requestor
