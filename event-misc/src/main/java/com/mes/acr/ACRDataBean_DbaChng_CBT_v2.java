package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.NumberField;
import com.mes.tools.DropDownTable;

public class ACRDataBean_DbaChng_CBT_v2 extends ACRDataBeanBase
{
  // log4j initialization
  static Logger log = Logger.getLogger(ACRDataBean_DbaChng_CBT_v2.class);

  public ACRDataBean_DbaChng_CBT_v2()
  {
  }

  public ACRDataBean_DbaChng_CBT_v2(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  /**
   * Options for new business type drop down
   */
  public class BusinessTypeTable extends DropDownTable
  {
    public BusinessTypeTable()
    {
      addElement("",                        "no change");
      addElement("New Corporation",         "New Corporation");
      addElement("New LLC",                 "New LLC");
      addElement("New Partnership",         "New Partnership");
      addElement("New Med/Legal/Corp",      "New Med/Legal/Corp");
      addElement("New Assoc/Estate/Trust",  "New Assoc/Estate/Trust");
      addElement("Tax Exempt",              "Tax Exempt");
    }
  }

  /**
   * Returns title for this acr.
   */
  public String getHeadingName()
  {
    return "Change DBA, Legal Name or Business Type";
  }

  /**
   * Loads current dba and legal name from database
   */
  protected void setDefaults()
  {
    String merchNum = getMerchantNumber();
    if (merchNum.length() == 0)
    {
      return;
    }

    PreparedStatement ps = null;
    ResultSet         rs = null;

    try
    {
      connect();

      String qs = " select  nvl(dba_name,'--')      dba_name,   "
                + "         nvl(fdr_corp_name,'--') legal_name  "
                + " from    mif                                 "
                + " where   merchant_number = ?                 ";
      ps = con.prepareStatement(qs);
      ps.setString(1,merchNum);

      rs = ps.executeQuery();
      String oldDba   = "--";
      String oldLegal = "--";
      if (rs.next())
      {
        oldDba    = rs.getString("dba_name");
        oldLegal  = rs.getString("legal_name");
      }
      acr.setACRItem("oldDba",oldDba);            acr.setACRItem("oldLegal",oldLegal);

    }
    catch (Exception e)
    {
      log.error(this.getClass().getName() + ".setDefaults(): " + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  /**
   * Creates fields for the acr detail section specific to this acr.
   */
  protected void createExtendedFields()
  {
    if (!editable)
    {
      return;
    }

    try
    {
      // shipping address component
      add(getAddressHelper());

      AtLeastOneValidation atLeastVal = new AtLeastOneValidation("At least one option must be selected to proceed.");

      // new legal name
      Field cb
          = new CheckboxField("changeLegal","Change Legal Name",false);
      Field newVal
          = new Field("newLegal","New Legal Name",50,30,true);
      fields.add(cb);
      fields.add(newVal);
      atLeastVal.addField(cb);
      //for display
      cb.addValidation(atLeastVal);
      newVal.addValidation(new IfYesNotBlankValidation(cb,"Field required"));

      // new business type
      cb  = new CheckboxField("changeType","Change Business Type",false);
      newVal
          = new DropDownField("newType","New Business Type",
                              new BusinessTypeTable(),true);
      fields.add(cb);
      fields.add(newVal);
      atLeastVal.addField(cb);
      newVal.addValidation(new IfYesNotBlankValidation(cb,"Field required"));

      // new dba and imprinter plate quantity
      cb  = new CheckboxField("changeDba","Change DBA",false);
      newVal
          = new Field("newDba","New DBA",25,30,true);
      Field plates
          = new NumberField("plateQuantity","New Imprinter Plate Quantity",
                            2,5,true,0);
      fields.add(cb);
      fields.add(newVal);
      fields.add(plates);
      atLeastVal.addField(cb);
      newVal.addValidation(new IfYesNotBlankValidation(cb,"Field required"));
      plates.addValidation(new IfYesNotBlankValidation(cb,"Field required"));

      // also make shipping address required for dba changes
      Field addressPicker = fields.getField("addressPicker");
      addressPicker
        .addValidation(new IfYesNotBlankValidation(cb,"Field required"));

      // relabel address picker to shipping address for error display
      addressPicker.setLabel("Shipping Address");

      // slight hack here...
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch (Exception e)
    {
      log.error("createExtendedFields(): "  + e);
    }
  }

  /**
   * Specifies which queue operations are permitted for the acr.
   */
  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
      { MesQueues.Q_NONE, MesQueues.Q_CHANGE_REQUEST_CBT_MGMT }

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,    MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT   }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,    MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS  }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,    MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA  }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,    MesQueues.Q_CHANGE_REQUEST_CBT_MMS      }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,    MesQueues.Q_CHANGE_REQUEST_COMPLETED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,    MesQueues.Q_CHANGE_REQUEST_CANCELLED    }

      ,{ MesQueues.Q_CHANGE_REQUEST_RISK,        MesQueues.Q_CHANGE_REQUEST_CBT_TSYS     }
      ,{ MesQueues.Q_CHANGE_REQUEST_RISK,        MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA  }
      ,{ MesQueues.Q_CHANGE_REQUEST_RISK,        MesQueues.Q_CHANGE_REQUEST_CANCELLED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_RISK,        MesQueues.Q_CHANGE_REQUEST_CBT_MMS    }
      ,{ MesQueues.Q_CHANGE_REQUEST_RISK,        MesQueues.Q_CHANGE_REQUEST_COMPLETED    }

      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_RISK         }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT   }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS  }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_CBT_TSYS     }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_CBT_MMS      }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_COMPLETED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_CANCELLED    }

      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_MMS,     MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT   }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_MMS,     MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS  }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_MMS,     MesQueues.Q_CHANGE_REQUEST_COMPLETED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_MMS,     MesQueues.Q_CHANGE_REQUEST_CANCELLED    }

      ,{ MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,  MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS  }
      ,{ MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,  MesQueues.Q_CHANGE_REQUEST_COMPLETED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,  MesQueues.Q_CHANGE_REQUEST_CANCELLED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,  MesQueues.Q_CHANGE_REQUEST_CBT_MMS    }
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{ MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS, MesQueues.Q_CHANGE_REQUEST_COMPLETED    }
      ,{ MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS, MesQueues.Q_CHANGE_REQUEST_CBT_MMS    }
      ,{ MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS, MesQueues.Q_CHANGE_REQUEST_CANCELLED    }

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}

//ASG Mgmt queue
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}

      //TSYS audit queue
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS_AUDIT}

      //POS audit queue
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_POS_AUDIT}

      //Help CBT mgmt
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_HELP_CBT_MGMT}

      //ASG CBT mgmt
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_ASG_CBT_MGMT}


    };
  }

  /**
   * Convenience methods for post submission mode
   */
  public boolean newDbaRequested()
  {
    return getACRValue("changeDba").toLowerCase().equals("y");
  }
  public boolean newLegalNameRequested()
  {
    return getACRValue("changeLegal").toLowerCase().equals("y");
  }
  public boolean newBusinessTypeRequested()
  {
    return getACRValue("changeType").toLowerCase().equals("y");
  }

  /**
   * Disable editable comments during submission.
   */
  public boolean hasEditableComments()
  {
    return !editable && super.hasEditableComments();
  }
}
