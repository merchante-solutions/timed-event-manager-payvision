package com.mes.acr;

import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;

public final class ACRDataBean_CreditRisk_MES extends ACRDataBean_CreditRisk_Base
{
  static Logger log = Logger.getLogger(ACRDataBean_CreditRisk_MES.class);

  public ACRDataBean_CreditRisk_MES()
  {
  }

  public ACRDataBean_CreditRisk_MES(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
      Requestor requestor, boolean editable) throws Exception
  {
    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP);
  }

}