package com.mes.acr;

import com.mes.forms.SelectorColumnDef;

public class SF_ReqTrmPrg_mes extends SF_EquipBase
{
  public SF_ReqTrmPrg_mes(String title)
  {
    super(title);
  }

  public void setupFrame()
  {
    addColumn(new SelectorColumnDef("col1","Description",   180));
    addColumn(new SelectorColumnDef("col2","Serial No.",    110));
    addColumn(new SelectorColumnDef("col3","Terminal App",  110));
    addColumn(new SelectorColumnDef("col4","Vendor Type",   150));
    addColumn(new SelectorColumnDef("col5","V#/TID#",       110));
  }
}
