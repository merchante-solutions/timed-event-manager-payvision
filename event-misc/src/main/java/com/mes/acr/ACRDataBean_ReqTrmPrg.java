package com.mes.acr;

// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.Field;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;
import com.mes.support.StringUtilities;

public class ACRDataBean_ReqTrmPrg extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_ReqTrmPrg.class);

  // constants
  // (NONE)

  // data members
  ACRDataBeanAddressHelper adrsHelper = null;

  // construction
  public ACRDataBean_ReqTrmPrg()
  {
  }

  public ACRDataBean_ReqTrmPrg(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  protected ACRDataBeanAddressHelper _getAddressHelper()
  {
    if(adrsHelper!=null)
      return adrsHelper; // already configured by virtue of non-null obj ref

    adrsHelper = new ACRDataBeanAddressHelper(this);

    // configure the address helper
    adrsHelper.setAdrsTypeName("Destination");
    adrsHelper.setAdrsSlctnFldName("staddress");
    adrsHelper.setAdrsNameFldNme("sh_name");
    adrsHelper.setAdrsAddressFldNme("sh_address");
    adrsHelper.setAdrsAttnFldNme("sh_attn");
    adrsHelper.setAdrsBankNameFldNme("sh_bnknme");
    adrsHelper.setAdrsCityFldNme("sh_city");
    adrsHelper.setAdrsStateFldNme("sh_state");
    adrsHelper.setAdrsZipFldNme("sh_zip");
    //adrsHelper.setAdrsCountry("sh_country");
    adrsHelper.setIsRequired(false);

    return adrsHelper;
  }

  public String renderAddressHtml()
  {
    return _getAddressHelper().renderAddressHtml();
  }

  public void doPull()
  {
    super.doPull();

    if(!editable)
      return;

    _getAddressHelper().pullAddressFields();
  }

  protected void setDefaults()
  {
    if(getMerchantNumber().length()>0) {

      // merchant address
      _getAddressHelper().setACRAdrsInfo();
    }
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;
    String acrdiname=null;

    // address fields
    _getAddressHelper().createAddressFields();

    if((acrdi=acrd.getACRDefItem("type_of_change"))!=null) {
      String[][] arr = new String[][]
      {
         {"Build New Terminal Profile","Build New Terminal Profile"}
        ,{"Modify Existing Terminal Profile","Modify Existing Terminal Profile"}
      };
      field = new RadioButtonField(acrdi.getName(),arr, -1, !acrdi.isRequired(), "Please specify the type of terminal programming change." );
      field.setShowName("Type of Change");
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("trm_modelnum"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("trm_vnum"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("trm_appid"))!=null)
      fields.add(ACRDefItemToField(acrdi));

    if((acrdi=acrd.getACRDefItem("request_description"))!=null)
      fields.add(ACRDefItemToField(acrdi));

    fields.addValidation(new EntryValidation());

    return;
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}

      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP:
      case MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  private class EntryValidation implements Validation
  {
    protected String ErrorText = "";

    public EntryValidation()
    {
    }

    public String getErrorText()
    {
      return(ErrorText);
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      Field field;

      field=fields.getField("type_of_change");
      if(field==null)
        return true;  // no penalty

      boolean[] vlds = new boolean[3];

      if(field.getData().equals("Build New Terminal Profile")) {
        vlds[0]=true;
        vlds[1]=true;
        vlds[2]=false;
      } else if(field.getData().equals("Modify Existing Terminal Profile")) {
        vlds[0]=true;
        vlds[1]=true;
        vlds[2]=true;
      } else {
        vlds[0]=false;
        vlds[1]=false;
        vlds[2]=false;
      }

      StringBuffer ertx = new StringBuffer();
      boolean bOk=true;

      // require terminal?
      if(vlds[0]==true) {
        field=fields.getField("trm_modelnum");
        if(field==null || field.isBlank()) {
          bOk=false;
          ertx.append("The Terminal must be specified.\n");
        }
      }
      // require vnum?
      if(vlds[2]==true) {
        field=fields.getField("trm_vnum");
        if(field==null || field.isBlank()) {
          bOk=false;
          ertx.append("The Terminal V-Number must be specified.\n");
        }
      }
      // require app id?
      if(vlds[1]==true) {
        field=fields.getField("trm_appid");
        if(field==null || field.isBlank()) {
          bOk=false;
          ertx.append("The Application ID must be specified.\n");
        }
      }

      if(ertx.length()>0)
        ErrorText=StringUtilities.replace(ertx.toString(),"\n","<br>");
      else
        ErrorText="";

      return bOk;
    }
  } // EntryValidation

  public String getHeadingName()
  {
    return "Request Terminal Programming";
  }

} // class ACRDataBean_ReqTrmPrg
