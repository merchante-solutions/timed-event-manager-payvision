/*@lineinfo:filename=ACRDataBeanAddressHelper*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.acr;

import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.RadioButtonField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.Validation;
import com.mes.support.StringUtilities;
import sqlj.runtime.ResultSetIterator;


/**
 * ACRDataBeanAddressHelper class
 *
 * Encapsulates the logic of managing address info for ACRs.
 */
class ACRDataBeanAddressHelper extends SQLJConnectionBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBeanAddressHelper.class);

  // constants

  // address type codes
  public static String          ADRSTYPECODE_MERCH_BUSADDRESS         = "1";
  public static String          ADRSTYPECODE_MERCH_MAILINGADDRESS     = "2";
  public static String          ADRSTYPECODE_MERCH_EQUIPDELADDRESS    = "3";
  public static String          ADRSTYPECODE_MERCH_OWNER1ADDRESS      = "4";
  public static String          ADRSTYPECODE_MERCH_OWNER2ADDRESS      = "5";
  public static String          ADRSTYPECODE_MERCH_OWNER3ADDRESS      = "6";
  public static String          ADRSTYPECODE_MERCH_FULFILLADDRESS     = "7";
  public static String          ADRSTYPECODE_MERCH_CONTACTADDRESS     = "8";
  public static String          ADRSTYPECODE_MERCH_BANDADDRESS        = "9";
  public static String          ADRSTYPECODE_MIF_DBA                  = "00";
  public static String          ADRSTYPECODE_MIF_BT1                  = "01";
  public static String          ADRSTYPECODE_MIF_BT2                  = "02";

  // data members
  protected ACRDataBeanBase     acrdb = null;
  protected String              adrsTypeName = "";
  protected String              adrsSlctnFldNme = null; // the name of the radio button collection field
  protected String              acrFldNme_adrsName = "";
  protected String              acrFldNme_address = "";
  protected String              acrFldNme_attn = "";
  protected String              acrFldNme_bankname = "";
  protected String              acrFldNme_city = "";
  protected String              acrFldNme_state = "";
  protected String              acrFldNme_zip = "";
  protected String              acrFldNme_country = "";
  protected boolean             isRequired = false;
  protected String              ifp = ""; // internal field prefix (to avoid field naming conflicts)

  private   String[][]          mifAdrsNames =
                                  {
                                     {ADRSTYPECODE_MIF_DBA,"DBA Address-00"}  // {'system (orig) name','show name'}
                                    ,{ADRSTYPECODE_MIF_BT1,"BT1-01"}
                                    ,{ADRSTYPECODE_MIF_BT2,"BT2-02"}
                                  };

  private   Vector              adrsFilter = new Vector(0,1);  // list of address codes to NOT display

  // construction
  public ACRDataBeanAddressHelper(ACRDataBeanBase acrdb)
  {
    this.acrdb=acrdb;
  }

  //public void setACRDataBean(ACRDataBeanBase acrdb) { this.acrdb=acrdb; }
  public void setAdrsTypeName(String v) { if(v!=null) adrsTypeName=v; }
  public void setAdrsSlctnFldName(String v) { if(v!=null) adrsSlctnFldNme=v; }
  public void setAdrsNameFldNme(String v) { if(v!=null) acrFldNme_adrsName=v; }
  public void setAdrsAddressFldNme(String v) { if(v!=null) acrFldNme_address=v; }
  public void setAdrsAttnFldNme(String v) { if(v!=null) acrFldNme_attn=v; }
  public void setAdrsBankNameFldNme(String v) { if(v!=null) acrFldNme_bankname=v; }
  public void setAdrsCityFldNme(String v) { if(v!=null) acrFldNme_city=v; }
  public void setAdrsStateFldNme(String v) { if(v!=null) acrFldNme_state=v; }
  public void setAdrsZipFldNme(String v) { if(v!=null) acrFldNme_zip=v; }
  public void setAdrsCountryFldNme(String v) { if(v!=null) acrFldNme_country=v; }
  public void setIsRequired(boolean v) { isRequired=v; }
  public void setInternalFieldPrefix(String v) { if(v!=null) ifp=v; }

  public void filterAddress(String adrsCode)
  {
    if(adrsCode!=null && adrsCode.length()>0 && adrsFilter.indexOf(adrsCode) < 0) {
      log.debug("Adding adrs code '"+adrsCode+"' to address filter.");
      adrsFilter.add(adrsCode);
    }
  }

  public void setMifAdrsName(String origName, String showName)
  {
    for(int i=0;i<mifAdrsNames.length;i++) {
      if(mifAdrsNames[i][0].equals(origName)) {
        mifAdrsNames[i][1] = showName;
        break;
      }
    }
  }

  private String getMifAdrsName(String origName)
  {
    for(int i=0;i<mifAdrsNames.length;i++) {
      if(mifAdrsNames[i][0].equals(origName))
        return mifAdrsNames[i][1];
    }
    return "";
  }

  public boolean isValidAddress(StringBuffer sb)
  {
    AddressRequiredValidation arv = new AddressRequiredValidation();
    if(!arv.validate()) {
      if(sb!=null)
        sb.append(arv.getErrorText());
      return false;
    } else
      return true;
  }

  /**
   * AddressRequiredValidation
   *
   * Requires an address be selected: either a pre-existing one or other address fields be filled in.
   */
  private final class AddressRequiredValidation implements Validation
  {
    protected String ErrorText = "";

    public AddressRequiredValidation()
    {
    }

    public String getErrorText()
    {
      return(ErrorText);
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      Field fld;
      FieldGroup fields = acrdb.getBaseGroup();
      StringBuffer sb = new StringBuffer();
      boolean bValidateOtherAddress = false;
      boolean bInvalidAddress = false;

      // only need to validate other address if selected
      fld = fields.getField(adrsSlctnFldNme);

      if(fld==null || fld.isBlank())
        bValidateOtherAddress=true;
      else
        bValidateOtherAddress = (fld!=null && fld.getData().equals("Other Address"));

      if(bValidateOtherAddress) {
        // require address, city and state fields if other address specified
        Field fldOAdrs = fields.getField(ifp+"oa_adrs");
        Field fldOCity = fields.getField(ifp+"oa_city");
        Field fldOState = fields.getField(ifp+"oa_state");
        Field fldOZip = fields.getField(ifp+"oa_zip");
        Field fldOAttn = fields.getField(ifp+"oa_attn");
        Field fldOName = fields.getField(ifp+"oa_bnknme");

        bInvalidAddress=( fldOAdrs.isBlank() ||
                          fldOCity.isBlank() ||
                          fldOZip.isBlank()  ||
                          (fldOAttn.isBlank() && fldOName.isBlank()) ||
                          fldOState.isBlank());
      }

      if(bInvalidAddress) {
       // sb.append(adrsTypeName);
       // sb.append(": All 'Other Address' fields must be entered.");
       // ErrorText = sb.toString();
        ErrorText = "'Other Address' fields are missing.";
        return false;
      }

      return true;
    }

  } // AddressRequiredValidation class

  public void setACRAdrsInfo()  // called in setDefaults()
  {
    String mn = acrdb.getMerchantNumber();

    if(mn.length()<1)
      return;

    ResultSetIterator it = null;
    ResultSet         rs = null;

    try {

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:206^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    addresstype.addresstype_code
//                    ,addresstype.addresstype_desc
//                    ,(address.ADDRESS_LINE1 || ' ' || address.ADDRESS_LINE2) address
//                    ,address.ADDRESS_CITY
//                    ,address.COUNTRYSTATE_CODE
//                    ,address.ADDRESS_ZIP
//                    ,address.country_code
//          from      address,addresstype,merchant
//          where     merchant.app_seq_num=address.APP_SEQ_NUM
//                    and address.address_line1 is not null
//                    and address.addresstype_code=addresstype.ADDRESSTYPE_CODE
//                    and merchant.merch_number=:mn
//          order by  addresstype_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    addresstype.addresstype_code\n                  ,addresstype.addresstype_desc\n                  ,(address.ADDRESS_LINE1 || ' ' || address.ADDRESS_LINE2) address\n                  ,address.ADDRESS_CITY\n                  ,address.COUNTRYSTATE_CODE\n                  ,address.ADDRESS_ZIP\n                  ,address.country_code\n        from      address,addresstype,merchant\n        where     merchant.app_seq_num=address.APP_SEQ_NUM\n                  and address.address_line1 is not null\n                  and address.addresstype_code=addresstype.ADDRESSTYPE_CODE\n                  and merchant.merch_number= :1 \n        order by  addresstype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRDataBeanAddressHelper",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,mn);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.acr.ACRDataBeanAddressHelper",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:221^7*/

      String address1 = null,mifAddress00 = null,crntAddress = "";
      String fldnme;
      int i=0;
      ACR acr = acrdb.getACR();

      rs = it.getResultSet();

      while(rs.next()) {

        if(adrsFilter.indexOf(rs.getString("addresstype_code"))>=0) {
          log.debug("Skipping adrs code '"+rs.getString("addresstype_code")+"' (found in filter).");
          continue;
        }

        if(address1 == null)
          address1 = rs.getString("address").toLowerCase().trim();
        else
          crntAddress = rs.getString("address").toLowerCase().trim();

        if(!crntAddress.equals(address1)) {
          i++;

          fldnme=ifp+"adrsname_"+i;
          acr.setACRItem(fldnme,rs.getString("addresstype_desc"));
          fldnme=ifp+"adrsadrs_"+i;
          acr.setACRItem(fldnme,rs.getString("address"));
          fldnme=ifp+"adrscity_"+i;
          acr.setACRItem(fldnme,rs.getString("address_city"));
          fldnme=ifp+"adrsstate_"+i;
          acr.setACRItem(fldnme,rs.getString("COUNTRYSTATE_CODE"));
          fldnme=ifp+"adrszip_"+i;
          acr.setACRItem(fldnme,rs.getString("ADDRESS_ZIP"));
          fldnme=ifp+"adrscountry_"+i;
          acr.setACRItem(fldnme,rs.getString("country_code"));
        }

      }

      it.close();
      rs.close();

      // get the MIF addresses too

      /*@lineinfo:generated-code*//*@lineinfo:266^7*/

//  ************************************************************
//  #sql [Ctx] it = { select
//                  m.dba_name                                name_00,
//                  m.dmaddr || ' ' || m.address_line_3       addr_00,
//                  m.dmcity                                  city_00,
//                  m.dmstate                                 state_00,
//                  m.dmzip                                   zip_00,
//  
//                  m.name1_line_1                            name_01,
//                  m.addr1_line_1 || ' ' || m.addr1_line_2   addr_01,
//                  m.city1_line_4                            city_01,
//                  m.state1_line_4                           state_01,
//                  m.zip1_line_4                             zip_01,
//  
//                  m.name2_line_1                            name_02,
//                  m.addr2_line_1 || ' ' || m.addr2_line_2   addr_02,
//                  m.city2_line_4                            city_02,
//                  m.state2_line_4                           state_02,
//                  m.zip2_line_4                             zip_02
//  
//          from    mif m
//  
//          where   m.merchant_number = :mn
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select\n                m.dba_name                                name_00,\n                m.dmaddr || ' ' || m.address_line_3       addr_00,\n                m.dmcity                                  city_00,\n                m.dmstate                                 state_00,\n                m.dmzip                                   zip_00,\n\n                m.name1_line_1                            name_01,\n                m.addr1_line_1 || ' ' || m.addr1_line_2   addr_01,\n                m.city1_line_4                            city_01,\n                m.state1_line_4                           state_01,\n                m.zip1_line_4                             zip_01,\n\n                m.name2_line_1                            name_02,\n                m.addr2_line_1 || ' ' || m.addr2_line_2   addr_02,\n                m.city2_line_4                            city_02,\n                m.state2_line_4                           state_02,\n                m.zip2_line_4                             zip_02\n\n        from    mif m\n\n        where   m.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.acr.ACRDataBeanAddressHelper",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,mn);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.acr.ACRDataBeanAddressHelper",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:290^7*/

      rs = it.getResultSet();

      while(rs.next()) {

        mifAddress00 = rs.getString("addr_00").toLowerCase().trim();

        if(adrsFilter.indexOf(ADRSTYPECODE_MIF_DBA)<0) {
          crntAddress = rs.getString("addr_00").toLowerCase().trim();
          if(!crntAddress.equals(address1)) {
            i++;
            fldnme=ifp+"adrsname_"+i;
            acr.setACRItem(fldnme,getMifAdrsName(ADRSTYPECODE_MIF_DBA));
            fldnme=ifp+"adrsadrs_"+i;
            acr.setACRItem(fldnme,rs.getString("addr_00"));
            fldnme=ifp+"adrscity_"+i;
            acr.setACRItem(fldnme,rs.getString("city_00"));
            fldnme=ifp+"adrsstate_"+i;
            acr.setACRItem(fldnme,rs.getString("state_00"));
            fldnme=ifp+"adrszip_"+i;
            acr.setACRItem(fldnme,rs.getString("zip_00"));
          }
        }

        if(adrsFilter.indexOf(ADRSTYPECODE_MIF_BT1)<0) {
          crntAddress = rs.getString("addr_01").toLowerCase().trim();
          if(!crntAddress.equals(address1) && !crntAddress.equals(mifAddress00)) {
            i++;
            fldnme=ifp+"adrsname_"+i;
            acr.setACRItem(fldnme,getMifAdrsName(ADRSTYPECODE_MIF_BT1));
            fldnme=ifp+"adrsadrs_"+i;
            acr.setACRItem(fldnme,rs.getString("addr_01"));
            fldnme=ifp+"adrscity_"+i;
            acr.setACRItem(fldnme,rs.getString("city_01"));
            fldnme=ifp+"adrsstate_"+i;
            acr.setACRItem(fldnme,rs.getString("state_01"));
            fldnme=ifp+"adrszip_"+i;
            acr.setACRItem(fldnme,rs.getString("zip_01"));
          }
        }

        if(adrsFilter.indexOf(ADRSTYPECODE_MIF_BT2)<0) {
          crntAddress = rs.getString("addr_02").toLowerCase().trim();
          if(!crntAddress.equals(address1) && !crntAddress.equals(mifAddress00)) {
            i++;
            fldnme=ifp+"adrsname_"+i;
            acr.setACRItem(fldnme,getMifAdrsName(ADRSTYPECODE_MIF_BT2));
            fldnme=ifp+"adrsadrs_"+i;
            acr.setACRItem(fldnme,rs.getString("addr_02"));
            fldnme=ifp+"adrscity_"+i;
            acr.setACRItem(fldnme,rs.getString("city_02"));
            fldnme=ifp+"adrsstate_"+i;
            acr.setACRItem(fldnme,rs.getString("state_02"));
            fldnme=ifp+"adrszip_"+i;
            acr.setACRItem(fldnme,rs.getString("zip_02"));
          }
        }

      }

      it.close();
      rs.close();

      // provision for manually specified address
      i++;
      acr.setACRItem(ifp+"adrsname_"+i,"");
      acr.setACRItem(ifp+"adrsattn_"+i,"");
      acr.setACRItem(ifp+"adrsbnknme_"+i,"");
      acr.setACRItem(ifp+"adrsadrs_"+i,"");
      acr.setACRItem(ifp+"adrscity_"+i,"");
      acr.setACRItem(ifp+"adrsstate_"+i,"");
      acr.setACRItem(ifp+"adrscountry_"+i,"");
      acr.setACRItem(ifp+"adrszip_"+i,"");

      // finally set the number of address just set
      acr.setACRItem(ifp+"numaddresses",Integer.toString(i));

    }
    catch(Exception e) {
      log.error(e.toString());
    }
    finally {
      try { it.close(); } catch(Exception e) {}
      try { rs.close(); } catch(Exception e) {}
      cleanUp();
    }

  }

  public void createAddressFields()
  {
    try {

      ACR acr = acrdb.getACR();
      Field field;
      int numaddresses=StringUtilities.stringToInt(acr.getACRValue(ifp+"numaddresses"),0);

      if(numaddresses>1) {

        StringBuffer sb = new StringBuffer();
        String[][] adrsnames = new String[numaddresses][2];

        for(int i=0;i<numaddresses;i++) {
          adrsnames[i][RadioButtonField.RADIO_VALUE]=acr.getACRValue(ifp+"adrsname_"+(i+1));

          sb.append("<u>"+acr.getACRValue(ifp+"adrsname_"+(i+1))+"</u>");
          sb.append("<div>&nbsp;&nbsp;&nbsp;&nbsp;"+acr.getACRValue(ifp+"adrsadrs_"+(i+1))+"</div>");
          sb.append("<div>&nbsp;&nbsp;&nbsp;&nbsp;"+acr.getACRValue(ifp+"adrscity_"+(i+1))+", "+acr.getACRValue(ifp+"adrsstate_"+(i+1))+"&nbsp;&nbsp;"+acr.getACRValue(ifp+"adrszip_"+(i+1))+"</div>");
          adrsnames[i][RadioButtonField.RADIO_LABEL]=sb.toString();
          sb.delete(0,sb.length()); // reset
        }
        adrsnames[numaddresses-1][RadioButtonField.RADIO_VALUE]="Other Address";
        adrsnames[numaddresses-1][RadioButtonField.RADIO_LABEL]="<u>Other:</u><div>(fill in at right)</div><div>&nbsp;</div>";

        field = new RadioButtonField(adrsSlctnFldNme,adrsnames,-1,!isRequired,"A "+adrsTypeName+" address must be specified.");
        field.setShowName(adrsTypeName);

        // add the address validation
        if(isRequired)
          field.addValidation(new AddressRequiredValidation());

        ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_TABLE);
        ((RadioButtonField)field).setNumTableColumns(numaddresses>3?3:numaddresses);
        acrdb.getBaseGroup().add(field);
      }

      field = new Field(ifp+"oa_adrs",75,35,true);
      field.setShowName("Address");
      acrdb.getBaseGroup().add(field);

      if(acrFldNme_adrsName.length()>0) {
        field = new Field(ifp+"oa_adrsname",75,35,true);
        field.setShowName("Name");
        acrdb.getBaseGroup().add(field);
      }
      if(acrFldNme_city.length()>0) {
        field = new Field(ifp+"oa_city",50,20,true);
        field.setShowName("City");
        acrdb.getBaseGroup().add(field);
      }
      if(acrFldNme_attn.length()>0) {
        field = new Field(ifp+"oa_attn",30,20,true);
        field.setShowName("Attention");
        acrdb.getBaseGroup().add(field);
      }
      if(acrFldNme_bankname.length()>0) {
        field = new Field(ifp+"oa_bnknme",50,20,true);
        field.setShowName("Bank Name");
        acrdb.getBaseGroup().add(field);
      }
      if(acrFldNme_state.length()>0) {
        StateDropDownTable stateDrpDwn = new StateDropDownTable();
        field = new DropDownField(ifp+"oa_state",stateDrpDwn,true);
        field.setShowName("State");
        acrdb.getBaseGroup().add(field);
      }
      if(acrFldNme_zip.length()>0) {
        field = new Field(ifp+"oa_zip",20,9,true);
        field.setShowName("Zip");
        acrdb.getBaseGroup().add(field);
      }
      if(acrFldNme_country.length()>0) {
        field = new Field(ifp+"oa_country",50,20,true);
        field.setShowName("Country");
        acrdb.getBaseGroup().add(field);
      }

      // add the address validation OLD
      //if(isRequired)
        //acrdb.getBaseGroup().addValidation(new AddressRequiredValidation());
    }
    catch(Exception e) {
      log.error("Error attempting to create address fields.");
    }
  }

  public void pullAddressFields()
  {
    try {

      ACR acr = acrdb.getACR();
      boolean bOthrAdrs=false;
      int numaddresses=StringUtilities.stringToInt(acr.getACRValue(ifp+"numaddresses"),0);

      if(numaddresses>1) {
        Field fld = acrdb.getField(adrsSlctnFldNme);
        String adrsname = fld.getData();
        if(adrsname.equals("Other Address")) {
          bOthrAdrs=true;
        } else {
          for(int i=1;i<=numaddresses;i++) {
            String crnt=acr.getACRValue(ifp+"adrsname_"+i);
            if(adrsname.equals(crnt)) {
              if(acrFldNme_adrsName.length()>0)
                acr.setACRItem(acrFldNme_adrsName,acr.getACRValue(ifp+"adrsname_"+i));
              if(acrFldNme_attn.length()>0)
                acr.setACRItem(acrFldNme_attn,acr.getACRValue(ifp+"adrsattn_"+i));
              if(acrFldNme_bankname.length()>0)
                acr.setACRItem(acrFldNme_bankname,acr.getACRValue(ifp+"adrsbnknme_"+i));
              if(acrFldNme_address.length()>0)
                acr.setACRItem(acrFldNme_address,acr.getACRValue(ifp+"adrsadrs_"+i));
              if(acrFldNme_city.length()>0)
                acr.setACRItem(acrFldNme_city,acr.getACRValue(ifp+"adrscity_"+i));
              if(acrFldNme_state.length()>0)
                acr.setACRItem(acrFldNme_state,acr.getACRValue(ifp+"adrsstate_"+i));
              if(acrFldNme_country.length()>0)
                acr.setACRItem(acrFldNme_country,acr.getACRValue(ifp+"adrscountry_"+i));
              if(acrFldNme_zip.length()>0)
                acr.setACRItem(acrFldNme_zip,acr.getACRValue(ifp+"adrszip_"+i));
              break;
            }
          }
        }
      } else
        bOthrAdrs=true;

      if(bOthrAdrs) {
        if(acrFldNme_adrsName.length()>0)
          acr.setACRItem(acrFldNme_adrsName,acr.getACRValue(ifp+"oa_adrsname"));
        if(acrFldNme_attn.length()>0)
          acr.setACRItem(acrFldNme_attn,acr.getACRValue(ifp+"oa_attn"));
        if(acrFldNme_bankname.length()>0)
          acr.setACRItem(acrFldNme_bankname,acr.getACRValue(ifp+"oa_bnknme"));
        if(acrFldNme_address.length()>0)
          acr.setACRItem(acrFldNme_address,acr.getACRValue(ifp+"oa_adrs"));
        if(acrFldNme_city.length()>0)
          acr.setACRItem(acrFldNme_city,acr.getACRValue(ifp+"oa_city"));
        if(acrFldNme_state.length()>0)
          acr.setACRItem(acrFldNme_state,acr.getACRValue(ifp+"oa_state"));
        if(acrFldNme_country.length()>0)
          acr.setACRItem(acrFldNme_country,acr.getACRValue(ifp+"oa_country"));
        if(acrFldNme_zip.length()>0)
          acr.setACRItem(acrFldNme_zip,acr.getACRValue(ifp+"oa_zip"));
      }
    }
    catch(Exception e) {
      log.error("Error occurred attempting to pull address field data.");
    }
  }

  public String renderAddressHtml()
  {
    StringBuffer sb = new StringBuffer(2048);

    int numaddresses = StringUtilities.stringToInt(acrdb.getACRValue(ifp+"numaddresses"),0);

    if(adrsTypeName.length()>0) {
      sb.append("<div>");
        sb.append("<span class=\"semiSmallBold\">&nbsp;<br>");
        sb.append(adrsTypeName);
        sb.append(" Address</span>");
      sb.append("</div>");
    }

    if(acrdb.inSubmitMode()) {

      sb.append("<table width=100% border=0 cellpadding=0 cellspacing=0>");
      sb.append("<tr>");

      if(numaddresses>1) {
        sb.append("<td valign=top>");
        sb.append("<span class=\"formFields\">");
        sb.append(acrdb.renderHtml(adrsSlctnFldNme));
        sb.append("</span>");
        sb.append("</td>");
        sb.append("<td width=15>&nbsp;</td>");
      }
      sb.append("<td valign=top>");
      sb.append("<div>");
      if(numaddresses>1)
      {
        sb.append("<span class=\"formFields\">Other Address:</span>");
      }
      sb.append("<table border=0 cellpadding=0 cellspacing=0>");

      if(acrFldNme_adrsName.length()>0) {
        sb.append("<tr>");
          sb.append("<td class=\"formFields\">Ship To Name</td>");
          sb.append("<td width=5>&nbsp;</td>");
          sb.append("<td class=\"formFields\">");
          sb.append(acrdb.renderHtml(ifp+"oa_adrsname"));
          sb.append("</td>");
        sb.append("</tr>");
      }
        sb.append("<tr>");
          sb.append("<td class=\"formFields\">Address</td>");
          sb.append("<td width=5>&nbsp;</td>");
          sb.append("<td class=\"formFields\">");
          sb.append(acrdb.renderHtml(ifp+"oa_adrs"));
          sb.append("</td>");
        sb.append("</tr>");

      if(acrFldNme_city.length()>0) {
            sb.append("<tr>");
              sb.append("<td class=\"formFields\">City</td>");
              sb.append("<td>&nbsp;</td>");
              sb.append("<td class=\"formFields\">");
              sb.append(acrdb.renderHtml(ifp+"oa_city"));
              sb.append("</td>");
            sb.append("</tr>");
      }
      if(acrFldNme_state.length()>0) {
            sb.append("<tr>");
              sb.append("<td class=\"formFields\">State</td>");
              sb.append("<td>&nbsp;</td>");
              sb.append("<td class=\"formFields\">");
              sb.append(acrdb.renderHtml(ifp+"oa_state"));
              sb.append("</td>");
            sb.append("</tr>");
      }
      if(acrFldNme_zip.length()>0) {
            sb.append("<tr>");
              sb.append("<td class=\"formFields\">Zip</td>");
              sb.append("<td>&nbsp;</td>");
              sb.append("<td class=\"formFields\">");
              sb.append(acrdb.renderHtml(ifp+"oa_zip"));
              sb.append("</td>");
            sb.append("</tr>");
      }
      if(acrFldNme_attn.length()>0) {
            sb.append("<tr>");
              sb.append("<td class=\"formFields\">Attn</td>");
              sb.append("<td width=5>&nbsp;</td>");
              sb.append("<td class=\"formFields\">");
                sb.append(acrdb.renderHtml(ifp+"oa_attn"));
              sb.append("</td>");
            sb.append("</tr>");
      }
      if(acrFldNme_bankname.length()>0) {
            sb.append("<tr>");
              sb.append("<td class=\"formFields\">Bank or Merchant Name</td>");
              sb.append("<td>&nbsp;</td>");
              sb.append("<td class=\"formFields\">");
                sb.append(acrdb.renderHtml(ifp+"oa_bnknme"));
              sb.append("</td>");
            sb.append("</tr>");
      }
      if(acrFldNme_country.length()>0) {
            sb.append("<tr>");
              sb.append("<td class=\"formFields\">Zip</td>");
              sb.append("<td>&nbsp;</td>");
              sb.append("<td class=\"formFields\">");
              sb.append(acrdb.renderHtml(ifp+"oa_country"));
              sb.append("</td>");
            sb.append("</tr>");
      }
          sb.append("</table>");
          sb.append("</div>");

        sb.append("</td>");
      sb.append("</tr>");
    sb.append("</table>");

    } else if(acrFldNme_address.length()>0 && acrdb.getACRValue(acrFldNme_address).length()>0) {

  sb.append("<table border=0 cellpadding=0 cellspacing=0 width=100%>");
    sb.append("<tr>");
      sb.append("<td class=\"formFields\">");

      if(acrFldNme_adrsName.length()>0 && acrdb.getACRValue(acrFldNme_adrsName).length()>0) {
        sb.append("<div>[");
        sb.append(acrdb.renderHtml(acrFldNme_adrsName));
        sb.append("]</div>");
      }

      sb.append("<div>");
      sb.append(acrdb.renderHtml(acrFldNme_address));
      sb.append("</div>");

      if(acrFldNme_city.length()>0 && acrdb.getACRValue(acrFldNme_city).length()>0) {
        sb.append("<div>");
        sb.append(acrdb.renderHtml(acrFldNme_city));
      }
      if(acrFldNme_state.length()>0 && acrdb.getACRValue(acrFldNme_state).length()>0) {
        sb.append(", ");
        sb.append(acrdb.renderHtml(acrFldNme_state));
        sb.append("</div>");
      }

      if(acrFldNme_zip.length()>0 && acrdb.getACRValue(acrFldNme_zip).length()>0) {
        sb.append("<div>");
        sb.append(acrdb.renderHtml(acrFldNme_zip));
        sb.append("</div>");
      }

      if(acrFldNme_country.length()>0 && acrdb.getACRValue(acrFldNme_country).length()>0) {
        sb.append("<div>");
        sb.append(acrdb.renderHtml(acrFldNme_country));
        sb.append("</div>");
      }

      if(acrFldNme_attn.length()>0 && acrdb.getACRValue(acrFldNme_attn).length()>0) {
        sb.append("<div>Attn: ");
        sb.append(acrdb.renderHtml(acrFldNme_attn));
        sb.append("</div>");
      }

      if(acrFldNme_bankname.length()>0 && acrdb.getACRValue(acrFldNme_bankname).length()>0) {
        sb.append("<div>Bank Name: ");
        sb.append(acrdb.renderHtml(acrFldNme_bankname));
        sb.append("</div>");
      }

      sb.append("</td>");
    sb.append("</tr>");
  sb.append("</table>");

    } else {
  sb.append("<span class=\"formFields\">No address specified.</span>");
    }

    return sb.toString();
  }

  public void removeInternalAddressACRItems()
  {
    ACR acr = acrdb.getACR();

    for(Enumeration e=acrdb.getACR().getItemEnumeration();e.hasMoreElements();) {
      ACRItem acri = (ACRItem)e.nextElement();

      if(acri.getName().equals(ifp+"numaddresses")
         || acri.getName().startsWith(ifp+"adrsname_")
         || acri.getName().startsWith(ifp+"adrsattn_")
         || acri.getName().startsWith(ifp+"adrsbnknme_")
         || acri.getName().startsWith(ifp+"adrsadrs_")
         || acri.getName().startsWith(ifp+"adrscity_")
         || acri.getName().startsWith(ifp+"adrsstate_")
         || acri.getName().startsWith(ifp+"adrszip_")
         || acri.getName().startsWith(ifp+"adrscountry_")

         || acri.getName().startsWith(ifp+"oa_adrsname")
         || acri.getName().startsWith(ifp+"oa_attn")
         || acri.getName().startsWith(ifp+"oa_bnknme")
         || acri.getName().startsWith(ifp+"oa_adrs")
         || acri.getName().startsWith(ifp+"oa_city")
         || acri.getName().startsWith(ifp+"oa_state")
         || acri.getName().startsWith(ifp+"oa_zip")
         || acri.getName().startsWith(ifp+"oa_country")
        )
          acr.removeItem(acri.getName());
    }
  }


}/*@lineinfo:generated-code*/