package com.mes.acr;

// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.Field;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;
import com.mes.support.StringUtilities;

public final class ACRDataBean_ReqTrmPrg_CBT extends ACRDataBean_ReqTrmPrg
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_ReqTrmPrg_CBT.class);

  // constants
  // (NONE)

  // construction
  public ACRDataBean_ReqTrmPrg_CBT()
  {
  }

  public ACRDataBean_ReqTrmPrg_CBT(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;
    String acrdiname=null;

    // address fields
    _getAddressHelper().createAddressFields();

    if((acrdi=acrd.getACRDefItem("type_of_change"))!=null) {
      String[][] arr = new String[][]
      {
         {"Build New Terminal Profile","Build New Terminal Profile"}
        ,{"Modify Existing Terminal Profile","Modify Existing Terminal Profile"}
      };
      field = new RadioButtonField(acrdi.getName(),arr, -1, !acrdi.isRequired(), "Please specify the type of terminal programming change." );
      field.setShowName("Type of Change");
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("trm_modelnum"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("trm_vnum"))!=null)
      fields.add(ACRDefItemToField(acrdi));

    if((acrdi=acrd.getACRDefItem("request_description"))!=null)
      fields.add(ACRDefItemToField(acrdi));

    String[][] ynarr = new String[][]
                        {
                           {"Yes","Yes"}
                          ,{"No", "No"}
                        };

    // terminal phone training for merchant field
    field = new RadioButtonField("Terminal Phone Training for Merchant",ynarr,1,false,"");
    ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
    fields.add(field);

    fields.addValidation(new CBTEntryValidation());

    return;
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_CBT_NEW}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  private class CBTEntryValidation implements Validation
  {
    protected String ErrorText = "";

    public CBTEntryValidation()
    {
    }

    public String getErrorText()
    {
      return(ErrorText);
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      Field field;

      field=fields.getField("type_of_change");
      if(field==null)
        return true;  // no penalty

      boolean[] vlds = new boolean[3];

      if(field.getData().equals("Build New Terminal Profile")) {
        vlds[0]=true;
        vlds[1]=true;
        vlds[2]=false;
      } else if(field.getData().equals("Modify Existing Terminal Profile")) {
        vlds[0]=true;
        vlds[1]=true;
        vlds[2]=true;
      } else {
        vlds[0]=false;
        vlds[1]=false;
        vlds[2]=false;
      }

      StringBuffer ertx = new StringBuffer();
      boolean bOk=true;

      // require terminal?
      if(vlds[0]==true) {
        field=fields.getField("trm_modelnum");
        if(field==null || field.isBlank()) {
          bOk=false;
          ertx.append("The Terminal must be specified.\n");
        }
      }
      // require vnum?
      if(vlds[2]==true) {
        field=fields.getField("trm_vnum");
        if(field==null || field.isBlank()) {
          bOk=false;
          ertx.append("The Terminal V-Number must be specified.\n");
        }
      }

      if(ertx.length()>0)
        ErrorText=StringUtilities.replace(ertx.toString(),"\n","<br>");
      else
        ErrorText="";

      return bOk;
    }

  } // CBTEntryValidation

  public String getHeadingName()
  {
    return "Request Terminal Programming for CB&T Merchant";
  }

  public String getJSPIncludeFilename()
  {
    return (acr==null)? "":"acr_incl_"+acr.getDefinition().getShortName()+"_cbt.jsp";
  }

} // class ACRDataBean_ReqTrmPrg_CBT
