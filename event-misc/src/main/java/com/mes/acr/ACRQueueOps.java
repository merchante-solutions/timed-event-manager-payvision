/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/acr/ACRQueueOps.java $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-02-03 10:39:36 -0800 (Tue, 03 Feb 2009) $
  Version            : $Revision: 15771 $

  Change History:
     See VSS database

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.acr;

import java.util.ArrayList;
// log4j classes.
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.queues.QueueTools;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;


/**
 * ACRQueueOps class.
 *
 * Performs ACR related queue operations.- UPDATE 11-05: now works in tandem with
 * ACRMgmtQInfo, a component of Merchant Info that defines how a
 * ACR should move through Qs, specifically mgmt Qs
 */
public final class ACRQueueOps
{
  // create class log category
  static Logger log = Logger.getLogger(ACRQueueOps.class);


  // constants
  // (NONE)


  // data members
  protected       ACRDataBeanBase         bean;
  protected       int[][]                 queueOps;
  protected       UserBean                user;
  protected       String                  lastStatusMsg;
  protected       QueueTools              tools;


  // class functions

  public static String getQueueOpDescriptor(int srcQ,int dstQ)
  {
    return getQueueOpDescriptor(srcQ,dstQ,false);
  }
  public static String getQueueOpDescriptor(int srcQ,int dstQ,boolean showFrom)
  {
    if(srcQ==MesQueues.Q_NONE)
      return "Initiate Change Request";

    StringBuffer sb = new StringBuffer(64);

    if(dstQ==MesQueues.Q_CHANGE_REQUEST_CANCELLED ||
       dstQ==MesQueues.Q_CHANGE_REQUEST_BBT_CANCELLED  )
    {
      sb.append("Cancel Request");
    }
    else if(dstQ==MesQueues.Q_CHANGE_REQUEST_COMPLETED||
            dstQ==MesQueues.Q_CHANGE_REQUEST_BBT_COMPLETED)
    {
      sb.append("Complete Request");
    }
    else if ( ACRMgmtQInfo.isMgmtQueue(srcQ) )
    {
      sb.append("Accept (return to originating queue)");
    }
    else
    {
      if(showFrom) {
        sb.append("Move from ");
        sb.append(getACRQueueDescriptor(srcQ));
        sb.append(" to ");
      } else
        sb.append("Move to ");
      sb.append(getACRQueueDescriptor(dstQ));
    }

    return sb.toString();
  }


  public static String getACRQueueDescriptor(int q)
  {
    String x = QueueTools.getQueueDescription(q);
    if(x.length()>0)
    {
      return x.substring(6);
    }
    else
    {
      return x;
    }
  }

  // object functions

  // construction
  public ACRQueueOps()
  {
    reset(null,null);
  }
  public ACRQueueOps(ACRDataBeanBase bean,UserBean user)
  {
    reset(bean,user);
  }

  public void reset(ACRDataBeanBase bean,UserBean user)
  {
    this.bean=bean;
    if(bean!=null)
    {
      this.queueOps = bean.getAllowedQueueOps();
    }
    else
    {
      this.queueOps = null;
    }
    this.user=user;
    this.lastStatusMsg="";
  }

  private boolean isValidState()
  {
    return (bean!=null &&
            bean.getACR()!=null &&
            user!=null &&
            queueOps!=null &&
            queueOps.length>0);
  }

  public String getOpStatusMsg()
  {
    return this.lastStatusMsg;
  }

  public final void QueueOpsToCommands()
  {
    //log.debug("QueueOpsToCommands - START");

    if(!isValidState())
      return;

    int[][] aqo = null;

    try {
      aqo = getAvailableQueueOps();
    }
    catch(Exception e) {
      log.error(e.toString());
      //tmm.wrn("Currently, unable to retreive the available queue operations.");
    }

    if(aqo==null)
      return;

    // IMPT: clear out existing commands
    bean.clearCommands();

    String qo,baseDesc,desc;
    StringBuffer sb = new StringBuffer(64);
    int type;

    // add commands according to their heretofor imposed "category": move, complete, cancel

    for(int i=0;i<aqo.length;i++) {

      baseDesc=getQueueOpDescriptor(aqo[i][0],aqo[i][1]);

      if(baseDesc.indexOf("Move to ")>=0 ||
         baseDesc.indexOf("Accept")>=0)
        type=1;
      else if(baseDesc.indexOf("Complete")>=0)
        type=2;
      else if(baseDesc.indexOf("Cancel")>=0)
        type=3;
      else {
        log.warn("Encountered unknown queue operation: '"+baseDesc+"'.  Skipping.");
        continue;
      }

      qo=HttpHelper.urlEncode(String.valueOf(aqo[i][0])+":"+String.valueOf(aqo[i][1]));
      //log.debug("QueueOpsToCommands() qo='"+qo+"': "+baseDesc);

      if(type==3) {
        log.debug("found a cancel queue op...");
        String[] cr = bean.getCancellationReasons(aqo[i][0]);
        log.debug("cr.length="+cr.length);
        if(cr.length>0) {
          for(int j=0;j<cr.length;j++) {
            log.debug("cr["+j+"]="+cr[j]);
            sb.delete(0,sb.length()); // reset
            sb.append(baseDesc);
            sb.append(" - ");
            sb.append(cr[j]);
            desc=sb.toString();
            bean.addCommand(ACRDataBeanBase.RELPATH_ACR_CONTROLLER+"?method=dqo&qo="+qo+"&on="+HttpHelper.urlEncode(cr[j]),desc,type);
          }
        // default: single non-descript cancel link
        } else
          bean.addCommand(ACRDataBeanBase.RELPATH_ACR_CONTROLLER+"?method=dqo&qo="+qo,baseDesc,type);
      // TODO: provide same mechanism for moving into completed queue
      } else
        bean.addCommand(ACRDataBeanBase.RELPATH_ACR_CONTROLLER+"?method=dqo&qo="+qo,baseDesc,type);
    }
  }

  /**
   * queueInitiate()
   *
   * Initiates an ACR to the queue system placing the ACR in one or more initial queues.
   * This initiates the queue process for a given ACR.
   */
  public boolean queueInitiate(String opNotes)
  {
    if(!isValidState()) {
      log.debug("queueInitiate() - INVALID STATE.  Aborted.");
      return false;
    }

    int numInitiated=0;

    try {

      log.debug("queueInitiate() - getting avail queue ops...");
      final int[][] availQOps = getAvailableQueueOps(new int[] {MesQueues.Q_NONE});

      if(availQOps==null || availQOps.length<1)
        return true;  // no available ops and no penalty since no exception thrown

      log.debug("queueInitiate() - availQOps.toString()="+availQOps.toString());

      for(int i=0;i<availQOps.length;i++) {
        if(!doQueueOp(availQOps[i][0],availQOps[i][1],opNotes)) {
          log.error("Attempt to do queue (src:'"+availQOps[i][0]+", dst:"+availQOps[i][1]+"' failed for ACR '"+bean.getACR().getID()+"' and User '"+user.getEmail()+"'.  Continuing.");
        } else
          numInitiated++;
      }

    }
    catch(Exception e) {
      log.error("Exception occurred while initiating ACR '"+bean.getACR().getID()+"' to the queues: '"+e.getMessage()+"'.");
    }

    return (numInitiated>0);  // return true when at least one ok since unable to do all or nothing trans from here
  }


  public int[][] getAvailableQueueOps()
    throws Exception
  {
    if(!isValidState())
      return null;

    return getAvailableQueueOps(QueueTools.getResidentQueues(bean.getACR().getID(),MesQueues.Q_ITEM_TYPE_CHANGE_REQUEST));
  }

  private int[][] getAvailableQueueOps(int[] srcQueues)
    throws Exception
  {
    log.debug("ACRQueueOps.getAvailableQueueOps - START - srcQueues.length="+srcQueues.length);

    int cnt = 0;
    //int[][] queueOps = bean.getAllowedQueueOps();
    try
    {
      for(int i=0;i<srcQueues.length;i++) {
        for(int j=0;j<queueOps.length;j++) {
          if(queueOps[j][0]==srcQueues[i])
            cnt++;
        }
      }
    }
    catch(NullPointerException e){}

    log.debug("ACRQueueOps.getAvailableQueueOps - cnt="+cnt);

    int[][] aqo = null;

    if(cnt==0) {

      // offer a potential queue orphan a way to an end point queue...
      boolean srcQisEndPntQueue = false;
      for(int i=0;i<srcQueues.length;i++) {
        if(srcQueues[i] == MesQueues.Q_CHANGE_REQUEST_COMPLETED
        || srcQueues[i] == MesQueues.Q_CHANGE_REQUEST_CANCELLED
        || srcQueues[i] == MesQueues.Q_CHANGE_REQUEST_BBT_COMPLETED
        || srcQueues[i] == MesQueues.Q_CHANGE_REQUEST_BBT_CANCELLED)
        {
          srcQisEndPntQueue = true;
          break;
        }
      }
      if(!srcQisEndPntQueue) {
        aqo = new int[srcQueues.length*2][2];
        for(int i=0;i<srcQueues.length;i+=2) {
          aqo[i][0]   = srcQueues[i];
          aqo[i][1]   = MesQueues.Q_CHANGE_REQUEST_COMPLETED;
          aqo[i+1][0] = srcQueues[i];
          aqo[i+1][1] = MesQueues.Q_CHANGE_REQUEST_CANCELLED;
        }
        return aqo;
      }
    }

    aqo = new int[cnt][2];
    cnt=0;

    for(int i=0;i<srcQueues.length;i++) {
      for(int j=0;j<queueOps.length;j++) {
        if(queueOps[j][0]==srcQueues[i]) {
          log.debug("ACRQueueOps.getAvailableQueueOps - found src queues");
          aqo[cnt][0]=queueOps[j][0];
          aqo[cnt][1]=queueOps[j][1];
          cnt++;
        }
      }
    }

    log.debug("ACRQueueOps.getAvailableQueueOps aqo.length="+aqo.length);

    return aqo;
  }

  public boolean doQueueOp(int srcQ, int dstQ, String opNotes)
  {
    log.debug("ACRQueueOps.doQueueOp() - START (isValidState()="+isValidState()+").");

    if(!isValidState())
      return false;

    boolean bOp = true;
    StringBuffer sb = new StringBuffer(); // for status msg

    sb.append(getQueueOpDescriptor(srcQ,dstQ,true));

    // do actual queue operation
    try {
      if(srcQ==MesQueues.Q_NONE) {
        final boolean is_rush = bean.getACRValue("Rush").equals("y");
        QueueTools.insertQueue(bean.getACR().getID(),dstQ,user,is_rush);
      } else if(dstQ!=MesQueues.Q_NONE) { // validate destination which must be an existant queue
        final boolean is_rush = bean.getACR().getACRValue("Rush").equals("y");
        QueueTools.moveQueueItem(bean.getACR().getID(),srcQ,dstQ,user,opNotes,is_rush);
      } else
        bOp=false;
    }
    catch(Exception e) {
      log.error("Do Queue Op EXCEPTION: '"+e.getMessage()+"'.");
      bOp=false;
    }

    // update acr status?
    if(bOp) {
      if(dstQ==MesQueues.Q_CHANGE_REQUEST_COMPLETED ||
         dstQ==MesQueues.Q_CHANGE_REQUEST_BBT_COMPLETED)
      {
        bean.getACR().setStatus(ACR.ACRSTATUS_COMPLETE);
      }
      else if(dstQ==MesQueues.Q_CHANGE_REQUEST_CANCELLED ||
              dstQ==MesQueues.Q_CHANGE_REQUEST_BBT_CANCELLED )
      {
        bean.getACR().setStatus(ACR.ACRSTATUS_CANCELLED);
      }
      else
      {
        bean.getACR().setStatus(ACR.ACRSTATUS_INPROCESS);
      }
    }

    sb.append(bOp? " successful":" FAILED");
    sb.append(" for ACR '");
    sb.append(bean.getACR().getID());
    sb.append("'.");

    lastStatusMsg=sb.toString();

    return bOp;
  }

  public static int[][] removeQueueOp(int queueOpToRemove, int[][] queueOps)
  {
    if(queueOps == null)
    {
      return queueOps;
    }

    //start with current size
    int newSize = queueOps.length;

    //change size for new array,
    //based on queue type submitted
    for(int i=0; i<queueOps.length; i++)
    {
      if(queueOps[i][0] == queueOpToRemove ||
         queueOps[i][1] == queueOpToRemove)
      {
        newSize--;
      }
    }

    //create new int array
    //based on new size
    int[][] newQueueOps = new int[newSize][2];
    int index = 0;

    //fill new array (order never mattered in queueOps[][])
    for(int i=0; i<queueOps.length; i++)
    {
      if(queueOps[i][0] == queueOpToRemove ||
         queueOps[i][1] == queueOpToRemove)
      {
        //do nothing;
      }
      else
      {
        newQueueOps[index][0] = queueOps[i][0];
        newQueueOps[index][1] = queueOps[i][1];
        index++;
      }
    }

    return newQueueOps;
  }

  //allows for indivudual bank num definition of overall queue ops
  //defaults to MES config if bankNum not defined
  public static int[][] getQueueOps (int initQueue, ACRMgmtQInfo mgmtQ)
  {

    int[][] queueOps = null;

    switch(mgmtQ.getBankNum())
    {

      case mesConstants.BANK_ID_BBT:
        queueOps = getBBTQueueOps(initQueue, mgmtQ.getId(), mgmtQ.isMgmtQOn());
        break;

      default:
        log.debug("Not defined - will default to MES");
        queueOps = getMESQueueOps(initQueue, mgmtQ.getId(), mgmtQ.isMgmtQOn());
        break;
    }

    return queueOps;
  }

  public static int[][] getBBTQueueOps (int initQueue, int mgmtQueue)
  {
    return getBBTQueueOps(initQueue, mgmtQueue, false);
  }

  public static int[][] getBBTQueueOps (int initQueue, int mgmtQueue, boolean mgmtQIsOn)
  {

    int[][] queueOps = null;

    //list of active Qs... should probably get from DB...
    ArrayList queues = new ArrayList();
    queues.add(MesQueues.Q_CHANGE_REQUEST_BBT_PENDING);
    queues.add(MesQueues.Q_CHANGE_REQUEST_BBT_DATA_ENTRY);
    queues.add(MesQueues.Q_CHANGE_REQUEST_BBT_QA);
    queues.add(MesQueues.Q_CHANGE_REQUEST_BBT_OUTBOUND_CALL);
    queues.add(MesQueues.Q_CHANGE_REQUEST_BBT_DATA_ENTRY_EXCP);
    queues.add(MesQueues.Q_CHANGE_REQUEST_BBT_CLIENT_SERVICE_EXCP);
    queues.add(MesQueues.Q_CHANGE_REQUEST_BBT_ACCOUNTING);
    queues.add(MesQueues.Q_CHANGE_REQUEST_BBT_ACCT_MGR);

    //make two lists
    ArrayList sQueues = new ArrayList(queues);
    ArrayList dQueues = new ArrayList(queues);

    //add complete/cancel/mgmt to destination qs
    dQueues.add(MesQueues.Q_CHANGE_REQUEST_BBT_COMPLETED);
    dQueues.add(MesQueues.Q_CHANGE_REQUEST_BBT_CANCELLED);
    dQueues.add(mgmtQueue);

    if(!mgmtQIsOn)
    {
      //disabled the "one-way out" option
      //so we need ways out - increase the src count by mgmt Q
      sQueues.add(mgmtQueue);
    }

    //This is for a single entry moving from MesQueues.Q_NONE --> initQueue
    int noneToInitQCount = 1;

    //get complete set count... minus the srcQ==destQ crossover
    int qOptionCount = (sQueues.size()*(dQueues.size()-1));

    queueOps = new int[qOptionCount + noneToInitQCount][2];

    log.debug("sQueues = "+sQueues.size());
    log.debug("dQueues = "+dQueues.size());
    log.debug("noneToInitQCount = "+noneToInitQCount);
    log.debug("qOptionCount = "+qOptionCount);

    //First entry is always the newly created ACR... going to it's
    //defined initial Q
    queueOps[0][0] = MesQueues.Q_NONE;
    queueOps[0][1] = initQueue;

    int counter = 1;
    int src, dest;
    for(int i = 0; i <sQueues.size(); i++)
    {
      try
      {

        src = ((Integer)sQueues.get(i)).intValue();

        for(int j = 0; j < dQueues.size(); j++)
        {
          dest = ((Integer)dQueues.get(j)).intValue();
          if(src != dest)
          {
            queueOps[counter][0] = src;
            queueOps[counter][1] = dest;

            //increase counter independent of other qs
            counter++;
            //log.debug("counter++ = "+ counter);
          }
        }
      }
      catch(NumberFormatException nfe)
      {
        //ignore and move on
      }
    }

    log.debug("counter = "+counter);

    return queueOps;
  }

  public static int[][] getMESQueueOps (int initQueue, int mgmtQueue)
  {
    return getMESQueueOps(initQueue, mgmtQueue, false);
  }

  public static int[][] getMESQueueOps (int initQueue, int mgmtQueue, boolean mgmtQIsOn)
  {

    int[][] queueOps = null;

    //list of active Qs... should probably get from DB...
    ArrayList queues = new ArrayList();
    queues.add(MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP);
    queues.add(MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP);
    queues.add(MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT);
    queues.add(MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW);
    queues.add(MesQueues.Q_CHANGE_REQUEST_ASG_MGMT);
    queues.add(MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS);
    queues.add(MesQueues.Q_CHANGE_REQUEST_RESEARCH);
    queues.add(MesQueues.Q_CHANGE_REQUEST_RISK);
    queues.add(MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA);
    queues.add(MesQueues.Q_CHANGE_REQUEST_MES_CONVERSION);
    queues.add(MesQueues.Q_CHANGE_REQUEST_NON_BANKCARD);

    int duplicates = queues.size();

    //make two lists
    ArrayList sQueues = new ArrayList(queues);
    ArrayList dQueues = new ArrayList(queues);

    //add complete/cancel to destination qs
    dQueues.add(MesQueues.Q_CHANGE_REQUEST_COMPLETED);
    dQueues.add(MesQueues.Q_CHANGE_REQUEST_CANCELLED);
    //dQueues.add(mgmtQueue);

    if(mgmtQIsOn)
    {
      //only add mgmt queue as a destination if it's on
      dQueues.add(mgmtQueue);
    }
    else
    {
      //disabled the "one-way out" option
      //so we need ways out - increase the src count by mgmt Q
      sQueues.add(mgmtQueue);
    }

    //This is for a single entry moving from MesQueues.Q_NONE --> initQueue
    int noneToInitQCount = 1;

    //get complete set count... minus the srcQ==destQ crossover
    int qOptionCount = ( (sQueues.size() * dQueues.size()) - duplicates + noneToInitQCount);

    queueOps = new int[qOptionCount][2];

    log.debug("sQueues = "+sQueues.size());
    log.debug("dQueues = "+dQueues.size());
    log.debug("noneToInitQCount = "+noneToInitQCount);
    log.debug("qOptionCount = "+qOptionCount);

    //First entry is always the newly created ACR... going to it's
    //defined initial Q
    queueOps[0][0] = MesQueues.Q_NONE;
    queueOps[0][1] = initQueue;

    int counter = 1;
    int src, dest;
    for(int i = 0; i <sQueues.size(); i++)
    {
      try
      {

        src = ((Integer)sQueues.get(i)).intValue();

        for(int j = 0; j < dQueues.size(); j++)
        {
          dest = ((Integer)dQueues.get(j)).intValue();
          if(src != dest)
          {
            //log.debug("queueOp=["+src+"]:["+dest+"]");
            queueOps[counter][0] = src;
            queueOps[counter][1] = dest;

            //increase counter independent of other qs
            counter++;
            //log.debug("counter++ = "+ counter);
          }
        }
      }
      catch(NumberFormatException nfe)
      {
        //ignore and move on
      }
    }

    log.debug("counter = "+counter);

    return queueOps;

/*
    return new int[][]
    {
       {MesQueues.Q_NONE,initQueue}

      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RISK}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_PRICING}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,mgmtQueue}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_RISK}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_PRICING}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,mgmtQueue}

      //Q_CHANGE_REQUEST_RESEARCH_QA aka "Mgmt Review queue"
      //,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      //,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      //,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      //,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      //,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      //,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      //,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      //,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_RISK}
      //,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_PRICING}
      //,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      //,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,mgmtQueue}

      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RISK}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_PRICING}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,mgmtQueue}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_RISK}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_PRICING}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,mgmtQueue}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RISK}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_PRICING}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,mgmtQueue}

      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_PRICING}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,mgmtQueue}


      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_RISK}
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,mgmtQueue}

       //ASG Mgmt queue
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_RISK}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_PRICING}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,mgmtQueue}
    };

    */
  }

} // ACRQueueOps
