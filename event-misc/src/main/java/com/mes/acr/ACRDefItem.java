package com.mes.acr;

import com.mes.database.ValueObjectBase;

/**
 * ACRDefItem class
 * 
 * Represents a single Account Change Request Definition Item.
 */
public class ACRDefItem extends ValueObjectBase
{
  // constants
  private static final int  DATATYPE_UNDEFINED                     = 0;
  private static final int  DATATYPE_START                         = 1;
  public static final int   DATATYPE_STRING                        = DATATYPE_START;
  public static final int   DATATYPE_BOOLEAN                       = 2;
  public static final int   DATATYPE_NUMERIC                       = 3;
  public static final int   DATATYPE_MONEY                         = 4;
  public static final int   DATATYPE_DATE                          = 5;
  private static final int  DATATYPE_END                           = DATATYPE_DATE;
  
  // data members
  protected long        id;
  protected String      name;
  protected String      label;
  protected String      description;
  protected int         dataType;               // DATATYPE_{...}
  protected boolean     is_required;
  protected String      defaultValue;           // in string form where parsing according to dataType is necessary
  protected String      validationParams;
  
  
  // class methods
  
  public static int toDataTypeEnum(String s)
  {
    return (s!=null && s.length()==1)? toDataTypeEnum(s.charAt(0)):DATATYPE_UNDEFINED;
  }
  
  public static int toDataTypeEnum(char c)
  {
    switch(c) {
      case 's':   return DATATYPE_STRING;
      case 'b':   return DATATYPE_BOOLEAN;
      case 'n':   return DATATYPE_NUMERIC;
      case 'm':   return DATATYPE_MONEY;
      case 'd':   return DATATYPE_DATE;
      default:    return DATATYPE_UNDEFINED;
    }
  }
  
  
  // object methods
  
  // construction
  public ACRDefItem()
  {
    super(true);
    
    clear();
  }
  
  public boolean equals(Object obj)
  {
    if(!(obj instanceof ACRDefItem))
      return false;
    
    return (((ACRDefItem)obj).getID()==this.id);
  }
  
  public void clear()
  {
    id=-1L;
    name="";
    label="";
    description="";
    dataType=DATATYPE_UNDEFINED;
    is_required=false;
    defaultValue="";
    validationParams="";
    
    is_dirty=true;
  }
  
  public ACRDefItem(String name,String label,String description,int dataType,boolean is_required,String defaultValue,String validationParams)
  {
    super(true);

    this.id=-1L;
    this.name=((name!=null)? name:"");
    this.label=((label!=null)? label:"");
    this.description=((description!=null)? description:"");
    this.dataType=dataType;
    this.is_required=is_required;
    this.defaultValue=((defaultValue!=null)? defaultValue:"");
    this.validationParams=((validationParams!=null)? validationParams:"");
  }
  
  // accessors
  public long     getID() { return id; }
  public String   getName() { return name; }
  public String   getLabel() { return label; }
  public String   getDesc() { return description; }
  public int      getDataType() { return dataType; }
  public boolean  isRequired() { return is_required; }
  public String   getDefaultValue() { return defaultValue; }
  public boolean  hasDefaultValue() { return (defaultValue.length()>0); }
  public String   getValidationParams() { return validationParams; }
  
  // mutators
  public void setID(long v)
  {
    if(id==v)
      return;
    id=v;
    is_dirty=true;
  }
  public void setName(String v)
  { 
    if(v==null)
      return;
    name=v;
    is_dirty=true;
  }
  public void setLabel(String v)
  { 
    if(v==null)
      return;
    label=v;
    is_dirty=true;
  }
  public void setDesc(String v)
  { 
    if(v==null)
      return;
    description=v;
    is_dirty=true;
  }
  public void setDataType(int v)
  { 
    if(v==dataType)
      return;
    dataType=v;
    is_dirty=true;
  }
  public void setIsRequired(boolean v)
  { 
    if(v==is_required)
      return;
    is_required=v;
    is_dirty=true;
  }
  public void setDefaultValue(String v)
  { 
    if(v==null)
      return;
    defaultValue=v;
    is_dirty=true;
  }
  public void setValidationParams(String v)
  { 
    if(v==null)
      return;
    validationParams=v;
    is_dirty=true;
  }
  
  public String toString()
  {
    StringBuffer sb = new StringBuffer(512);
    
    sb.append("id="+id);
    sb.append(", name="+name);
    sb.append(", label="+label);
    sb.append(", description="+description);
    sb.append(", dataType="+dataType);
    sb.append(", is_required="+is_required);
    sb.append(", defaultValue="+defaultValue);
    sb.append(", validationParams="+validationParams);
    sb.append(" - "+(is_dirty? "DIRTY":"CLEAN"));
    
    return sb.toString();
  }
  
} // class ACRDefItem
