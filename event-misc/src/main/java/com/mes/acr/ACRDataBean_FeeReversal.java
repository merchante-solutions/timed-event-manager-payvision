package com.mes.acr;

import java.util.Enumeration;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;


public class ACRDataBean_FeeReversal extends ACRDataBean_FeeReversalBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_FeeReversal.class);

  // constants
  public static final int                           MAX_ALLOWED_ENTRIES         = 1;
    // number of available fee reversals per single ACR.

  public static final String[] mpfx =
  {
     "feerev_amt_jan_"
    ,"feerev_amt_feb_"
    ,"feerev_amt_mar_"
    ,"feerev_amt_apr_"
    ,"feerev_amt_may_"
    ,"feerev_amt_jun_"
    ,"feerev_amt_jul_"
    ,"feerev_amt_aug_"
    ,"feerev_amt_sep_"
    ,"feerev_amt_oct_"
    ,"feerev_amt_nov_"
    ,"feerev_amt_dec_"
  };


  // data members
  private float[] feeRvslAmt = null;

  // construction
  public ACRDataBean_FeeReversal()
  {
  }

  public ACRDataBean_FeeReversal(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    // fee reversal amounts array (for show only)
    feeRvslAmt = new float[MAX_ALLOWED_ENTRIES];
    if(editable) {
      for(int i=0;i<feeRvslAmt.length;i++)
        feeRvslAmt[i]=0f;
    } else {
      StringBuffer sb = new StringBuffer(32);
      for(int i=0;i<feeRvslAmt.length;i++) {
        sb.delete(0,sb.length()); // reset
        sb.append("feerev_amt_total_");
        sb.append(i+1);
        feeRvslAmt[i]=Float.valueOf(acr.getACRValue(sb.toString())).floatValue();
      }
    }
  }

  public int getMaxEntries()
  {
    return MAX_ALLOWED_ENTRIES;
  }

  protected void createExtendedFields()
  {
    if(acr==null)
      return;

    ACRDefItem acrdi=null;
    Field field=null;

    if(!editable) {

      super.createExtendedFields();

    } else {

      String fan=null;
      int i=0;
      String acrdiname=null,entrynum;
      FieldGroup fg_entry = null;

      FeeTypeDropDownTable feeTypeDrpDwn = new FeeTypeDropDownTable();
      FeeActionDropDownTable feeActionDrpDwn = new FeeActionDropDownTable();

      // define the FieldBean fields from the ACR Definition
      for(Enumeration e=acr.getDefinition().getItemEnumeration();e.hasMoreElements();) {
        acrdi=(ACRDefItem)e.nextElement();

        // fee alteration field
        if(acrdi.getName().indexOf("feerev_")>=0) {
          acrdiname=acrdi.getName();

          i=acrdiname.lastIndexOf('_');
          entrynum=acrdiname.substring(i+1);
          fan="Fee Reversal "+entrynum;
          //log.debug("fan="+fan);

          if((fg_entry=(FieldGroup)fields.getField(fan,false))==null) {
            fg_entry = new FieldGroup(fan);
            fields.add(fg_entry);
          }

          if(acrdiname.indexOf("type")>0) {
            // fee alteration type
            field = new DropDownField(acrdi.getName(),feeTypeDrpDwn,true);

          } else if(acrdiname.indexOf("action")>0) {
            // fee alteration action
            field = new DropDownField(acrdi.getName(),feeActionDrpDwn,true);

          } else if(acrdiname.indexOf("revamt")>0) {
            // fee reversal amount
            field = ACRDefItemToField(acrdi);

          } else if(acrdiname.indexOf("amt")>0) {
            // fee month amount
            field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),false);

          } else {
            field = ACRDefItemToField(acrdi);
          }

          fg_entry.add(field);
          //log.debug("Field '"+field.getName()+"' added to fee reversal group '"+fg_entry.getName()+"'.");

        // non-special field transform
        } else if((field=ACRDefItemToField(acrdi))!=null) {
          fields.add(field);
        }

        //log.debug("Field '"+field.getName()+"' added.");
      }

      // add AtLeastOneEntry valiation
      fields.addValidation(new AtLeastOneEntry());
    }

    // EntryValidation
    fields.addValidation(new EntryValidation());

    return;
  }

  public int[][] getAllowedQueueOps()
  {
    int initQueue;

    initQueue = MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA;

    return new int[][]
    {
       {MesQueues.Q_NONE,initQueue}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_PRICING}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}

      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_PRICING,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RESEARCH}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }


  public String getHeadingName()
  {
    return "Reverse Merchant Fees";
  }

  public boolean doCommand(String method)
  {
    log.debug("doCommand() method='"+method+"'");
    if(method.equals("Calculate Fee Reversals")) {
      super.doPull();
      calcTotalFeeReversals();
      return true;
    } else {
      log.error("Unhandled Fee Reversal Request: '"+method+"'.");
      return false;
    }
  }

  public final String renderHtml(String fname)
  {
    if(editable || !fname.startsWith("feerev_amt_"))
      return super.renderHtml(fname);

    // convert months checkbox values to short month names
    if(!getACRValue(fname).equals("y"))
      return "";

    return fname.substring(11,fname.lastIndexOf('_'));
  }


  public void doPull()
  {
    super.doPull();

    StringBuffer sb = new StringBuffer(32);

    // fee reversal specific logic
    calcTotalFeeReversals();
    for(int i=0;i<getMaxEntries();i++) {
      sb.delete(0,sb.length()); // reset
      sb.append("feerev_amt_total_");
      sb.append(i+1);
      acr.setACRItem(sb.toString(),Float.toString(feeRvslAmt[i]));
    }
  }

  public float getTotalFeeReversalAmount()
  {
    float tfr=0f;

    for(int i=0;i<getMaxEntries();i++)
      tfr += feeRvslAmt[i];

    return tfr;
  }

  public float getFeeReversalAmount(int index)
  {
    try {
      return feeRvslAmt[index];
    }
    catch(Exception e) {
      return 0f;
    }
  }

  protected void calcTotalFeeReversals()
  {
    try
    {
      log.debug("*** calcTotalFeeReversals() starting ***");
      for (int feeIdx = 0; feeIdx < getMaxEntries(); ++feeIdx)
      {
        // get reversal fee monthly amount item name
        String monthlyAmtName = "feerev_revamt_" + (feeIdx + 1);
        //log.debug("monthlyAmtName = " + monthlyAmtName);

        // retrieve amount from field or acr storage depending on edit mode
        float monthlyAmt = 0;
        try
        {
          String monthlyAmtStr = (editable ?
                                  fields.getData(monthlyAmtName) :
                                  getACRValue(monthlyAmtName));
          monthlyAmt = Float.valueOf(monthlyAmtStr).floatValue();
        }
        catch (Exception e) { }
        //log.debug("monthlyAmt = " + monthlyAmt);

        // reset fee amount
        feeRvslAmt[feeIdx] = 0;

        // scan month flags for the item and total the reversal amount
        for (int monthIdx = 0; monthIdx < 12; ++monthIdx)
        {
          String monthFlagName = mpfx[monthIdx] + (feeIdx + 1);
          boolean isFlagged = false;
          try
          {
            String monthFlagStr = (editable ?
                                   fields.getData(monthFlagName) :
                                   getACRValue(monthFlagName));
            isFlagged = monthFlagStr.equals("y");
          }
          catch (Exception e) { }
          if (isFlagged)
          {
            //log.debug(monthFlagName + " is y");
            feeRvslAmt[feeIdx] += monthlyAmt;
          }
        }
        log.debug("feeRvslAmt[" + feeIdx + "] = " + feeRvslAmt[feeIdx]);
      }
    }
    catch (Exception e)
    {
      log.error(e);
      e.printStackTrace();
    }
  }

  /*
  protected void calcTotalFeeReversals()
  {
    try {
      float mnthAmt = 0f;
      StringBuffer srevamt = new StringBuffer(64);
      StringBuffer smonth = new StringBuffer(64);
      String fval;

      log.debug("*** calTotalFeeReversals() starting ***");
      for(int i=0;i<getMaxEntries();i++) {

        log.debug("entry " + i);
        feeRvslAmt[i]=0f;  // reset

        srevamt.delete(0,srevamt.length()); // reset
        srevamt.append("feerev_revamt_");
        srevamt.append(i+1);

        log.debug("srevamt = " + srevamt.toString());

        for(int j=0;j<12;j++) {
          fval=fields.getData(srevamt.toString());
          log.debug("fval = " + fval);
          try {
            mnthAmt=Float.valueOf(fval).floatValue();
          }
          catch(Exception e) {
            mnthAmt=0f;
          }
          log.debug("mnthAmt = " + mnthAmt);
          smonth.delete(0,smonth.length()); // reset
          smonth.append(mpfx[j]);
          smonth.append(i+1);
          log.debug("smonth = " + smonth.toString());
          fval=fields.getData(smonth.toString());
          log.debug("fval = " + fval);
          if(fval.equals("y"))
            feeRvslAmt[i] += mnthAmt;
        }
        log.debug("feeRvslAmt[" + i + "] = " + feeRvslAmt[i]);
      }
    }
    catch(Exception e) {
      log.error(e);
      e.printStackTrace();
    }
  }
  */

  private class AtLeastOneEntry implements Validation
  {
    public AtLeastOneEntry()
    {
    }

    public String getErrorText()
    {
      return "At least one Fee Reversal must be specified.";
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      // require at least one entry have a non-blank type

      int numNonBlank = 0;
      StringBuffer entryname = new StringBuffer(64);
      StringBuffer type = new StringBuffer(64);
      FieldGroup entry;

      for(int i=0;i<getMaxEntries();i++) {
        entryname.delete(0,entryname.length()); // reset
        entryname.append("Fee Reversal ");
        entryname.append(i+1);
        entry = (FieldGroup)fields.getField(entryname.toString());
        type.delete(0,type.length()); // reset
        type.append("feerev_type_");
        type.append(i+1);

        if(!entry.getField(type.toString()).isBlank())
          numNonBlank++;
      }

      return (numNonBlank>0);
    }

  } // AtLeastOneEntry

  /**
   * EntryValidation
   *
   * Validate a single entry: Fee Reversal
   */
  private class EntryValidation implements Validation
  {
    protected String ErrorText = "";

    public EntryValidation()
    {
    }

    public String getErrorText()
    {
      return(ErrorText);
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      boolean rval = true;
      Field field;

      if(!editable) {

        // validate the request status/amount fields
        if(residentQueue == MesQueues.Q_CHANGE_REQUEST_RESEARCH) {
          if((field = fields.getField("requestStatus"))!=null) {
            boolean bAccepted = field.getData().equals("Accepted");
            field=fields.getField("acceptAmt");
            double acptAmt = (field!=null? field.asDouble():0d);
            if(bAccepted) {
              if(acptAmt == 0d) {
                rval=false;
                ErrorText = "The Accept Amount must be non-zero if the Request Status is Accepted.";
              }
            } else {
              if(acptAmt != 0d) {
                rval=false;
                ErrorText = "The Accept Amount must be zero if the Request Status is Denied.";
              }
            }
          }
        }

        return rval;
      }

      FieldGroup entry;
      StringBuffer entryname = new StringBuffer(64);
      StringBuffer type = new StringBuffer(64);
      StringBuffer amt = new StringBuffer(64);
      StringBuffer action = new StringBuffer(64);
      StringBuffer etb = new StringBuffer(128);   // error text buffer
      StringBuffer revamt = new StringBuffer(64);
      int numNonBlank;

      // require at least one month to be selected IF a type specified
      for(int i=0;i<MAX_ALLOWED_ENTRIES;i++) {
        numNonBlank=0;  // reset
        entryname.delete(0,entryname.length()); // reset
        entryname.append("Fee Reversal ");
        entryname.append(i+1);
        type.delete(0,type.length()); // reset
        type.append("feerev_type_");
        type.append(i+1);

        entry = (FieldGroup)fields.getField(entryname.toString());

        if(!entry.getField(type.toString()).isBlank()) {
          // ensure an action specified
          action.delete(0,action.length()); // reset
          action.append("feerev_action_");
          action.append(i+1);
          if(entry.getField(action.toString()).isBlank()) {
            rval=false;
            etb.append("An Action must be selected for entry number ");
            etb.append(i+1);
            break;
          }
          // ensure a valid fee rev amt specified
          revamt.delete(0,revamt.length()); // reset
          revamt.append("feerev_revamt_");
          revamt.append(i+1);
          field=entry.getField(revamt.toString());
          if(field.isBlank() || !field.isValid()) {
            rval=false;
            etb.append("A Fee Reversal Amount must be specified for entry number ");
            etb.append(i+1);
            break;
          }
          // ensure at least one month selected
          for(int j=0;j<12;j++) {
            amt.delete(0,amt.length()); // reset
            amt.append(mpfx[j]);
            amt.append(i+1);

            if(!entry.getField(amt.toString()).isBlank())
             numNonBlank++;
          }
          if(numNonBlank==0) {
            rval=false;
            etb.append("At least one month must be selected for entry number ");
            etb.append(i+1);
            break;
          }
        }
      }

      // require the comments fields be non-empty
      if(rval) {
        field=fields.getField("Comments");
        if(field.isBlank()) {
          rval=false;
          etb.append("The Comments fields must be specified.");
        }
      }

      if(etb.length()>0)
        ErrorText = etb.toString();
      else
        ErrorText="";

      return rval;
    }
  } // EntryValidation


  public class FeeActionDropDownTable extends DropDownTable
  {
    public FeeActionDropDownTable()
    {
      addElement("","");

      addElement("Refund","Refund");
      //addElement("Remove","Remove");
      //addElement("Both Refund & Remove","Both Refund & Remove");
    }

  } // FeeActionDropDownTable class

} // class ACRDataBean_FeeReversal
