package com.mes.acr;

// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.NumberField;
import com.mes.forms.RadioField;
import com.mes.tools.DropDownTable;


public final class ACRDataBean_CardAdds_MES extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_CardAdds_MES.class);

  // construction
  public ACRDataBean_CardAdds_MES()
  {
  }

  public ACRDataBean_CardAdds_MES(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  protected void setDefaults()
  {
    // no-op
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_NON_BANKCARD);
  }

  /**
   * Uses the auth fee table factory to create a drop down field with the
   * given card type's allowed auth fees.
   */
  protected Field createFeeField(String fieldName, String fieldLabel,
    int cardType)
  {
    // pull the app type out of "universal" acr item storage
    int appType =
      Integer.parseInt(acr.getItemFromDefName("ACR Type Code").getValue());

    // create a fee table based on the app type and card type
    DropDownTable feeTable =
      AuthFeeTableFactory.createFeeTable(appType,cardType);

    // return a drop down field
    return new DropDownField(fieldName,fieldLabel,feeTable,true);
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    // default to rush request
    //fields.deleteField("Rush");
    //fields.add(new CheckboxField("Rush","Rush?",true));

    String[][] YesNoRadioButtons =
    {
      { "No"   , "No"   },
      { "Yes"  , "Yes"  }
    };

    String [][] cardStatusButtons1 =
    {
      { "Add new card number"          , "Add new card number" },
      { "Update existing card number"  , "Update existing card number" }
    };

    String [][] cardStatusButtons2 =
    {
      { "MeS to apply for new card"           , "MeS to apply for new card" },
      { "Merchant provided new card number"   , "Merchant provided new card number" },
      { "Update existing card number"         , "Update existing card number" }
    };

    try {

      Field field;

      fields.deleteField("Rush");

      fields.add(new RadioField("verisign","Verisign Merchant?",YesNoRadioButtons));

      // require "at least one" card add be checked
      // this is for ALL controlFieldx fields below
      AtLeastOneValidation atLeastVal = new AtLeastOneValidation("At least one Card Type must be selected to proceed.");

      /*
       * AMEX
       */
      CheckboxField controlField1 = new CheckboxField("amex","American Express",false);
      controlField1.addHtmlExtra("class=\"semiSmallBold\"");
      atLeastVal.addField(controlField1);
      controlField1.addValidation(atLeastVal);
      fields.add(controlField1);

      field = createFeeField("amexAuthFee","Amex Auth Fee",mesConstants.APP_CT_AMEX);
      field.addValidation(new IfYesNotBlankValidation(controlField1,"Field required"));
      fields.add(field);

      //field = new NumberField("amexESARate", "Amex ESA Rate" ,5,20,true,2);
      //field.addValidation(new IfYesNotBlankValidation(controlField1,"Field required"));
      //fields.add(field);

      field = new RadioField("amexSplit", "amexSplit", YesNoRadioButtons);
      fields.add(field);

      RadioField amexCSRField = new RadioField("amexCardStatus", "amexCardStatus", cardStatusButtons2);
      amexCSRField.setRenderStyle(RadioField.RS_VERTICAL);
      fields.add(amexCSRField);

      field = new Field("amexNum", "Amex Number",10,20,true);
      //field.addValidation(new IfYesNotBlankValidation(controlField1,"Field required"));

      field.addValidation(new ConditionalRequiredValidation(
                            new NotCondition(
                               new FieldValueCondition(amexCSRField,"MeS to apply for new card")
                            )
                         ), "Field required");

      fields.add(field);

      /*
       * DISCOVER
       */
      CheckboxField controlField2 = new CheckboxField("disc","Discover",false);
      controlField2.addHtmlExtra("class=\"semiSmallBold\"");
      atLeastVal.addField(controlField2);
      fields.add(controlField2);

      field = createFeeField("discAuthFee","Discover Auth Fee",mesConstants.APP_CT_DISCOVER);
      field.addValidation(new IfYesNotBlankValidation(controlField2,"Field required"));
      fields.add(field);

      //field = new NumberField("discRAPRate","Discover RAP Rate",5,20,true,2);
      //field.addValidation(new IfYesNotBlankValidation(controlField2,"Field required"));
      //fields.add(field);

      RadioField discCSRField = new RadioField("discCardStatus", "discCardStatus", cardStatusButtons2);
      discCSRField.setRenderStyle(RadioField.RS_VERTICAL);
      fields.add(discCSRField);

      field = new Field("discNum","Discover Number",15,20,true);
      //field.addValidation(new IfYesNotBlankValidation(controlField2,"Field required"));

      field.addValidation(new ConditionalRequiredValidation(
                            new NotCondition(
                               new FieldValueCondition(discCSRField,"MeS to apply for new card")
                            )
                         ), "Field required");

      fields.add(field);

      /*
       * DINERS
       */
/*
//removed 10.6.05
      CheckboxField controlField3 = new CheckboxField("diners","Diners",false);
      controlField3.addHtmlExtra("class=\"semiSmallBold\"");
      atLeastVal.addField(controlField3);
      fields.add(controlField3);

      field = new NumberField("dinersNum","Diners Number",10,20,true,0);
      field.addValidation(new IfYesNotBlankValidation(controlField3,"Field required"));
      fields.add(field);

      field = createFeeField("dinersAuthFee","Diners Auth Fee",mesConstants.APP_CT_DINERS_CLUB);
      field.addValidation(new IfYesNotBlankValidation(controlField3,"Field required"));
      fields.add(field);

      field = new RadioField("dinersCardStatus", "dinersCardStatus", cardStatusButtons1);
      ((RadioField)field).setRenderStyle(RadioField.RS_VERTICAL);
      fields.add(field);
*/
      /*
       * JCB
       */
      CheckboxField controlField4 = new CheckboxField("jcb","JCB",false);
      controlField4.addHtmlExtra("class=\"semiSmallBold\"");
      atLeastVal.addField(controlField4);
      fields.add(controlField4);

      field = new NumberField("jcbNum","JCB Number",16,20,true,0);
      field.addValidation(new IfYesNotBlankValidation(controlField4,"Field required"));
      fields.add(field);

      field = createFeeField("jcbAuthFee","JCB Auth Fee",mesConstants.APP_CT_JCB);
      field.addValidation(new IfYesNotBlankValidation(controlField4,"Field required"));
      fields.add(field);

      field = new RadioField("jcbCardStatus", "jcbCardStatus", cardStatusButtons1);
      ((RadioField)field).setRenderStyle(RadioField.RS_VERTICAL);
      fields.add(field);

      /*
       * EBT
       */
      CheckboxField controlField5 = new CheckboxField("ebt","EBT",false);
      controlField5.addHtmlExtra("class=\"semiSmallBold\"");
      atLeastVal.addField(controlField5);
      fields.add(controlField5);

      field = new NumberField("ebtNum","EBT Number" ,25,20,true,0);
      field.addValidation(new IfYesNotBlankValidation(controlField5,"Field required"));
      fields.add(field);

      field = createFeeField("ebtAuthFee","EBT Auth Fee",mesConstants.APP_CT_EBT);
      field.addValidation(new IfYesNotBlankValidation(controlField5,"Field required"));
      fields.add(field);

      field = new RadioField("ebtCardStatus", "ebtCardStatus", cardStatusButtons1);
      ((RadioField)field).setRenderStyle(RadioField.RS_VERTICAL);
      fields.add(field);

    }
    catch(Exception e) {
      log.error("createFields() EXCEPTION: '"+e.getMessage()+"'.");
      return;
    }

    return;
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP:
      case MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Declined by Amex or Discover"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  /**
   * Disable editable comments during submission.
   */
  public boolean hasEditableComments()
  {
    return !editable && super.hasEditableComments();
  }

  public String getHeadingName()
  {
    return "Add Card Types to Merchant Account";
  }

} // class ACRDataBean_CardAdds_CBT
