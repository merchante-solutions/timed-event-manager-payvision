package com.mes.acr;

// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.CityStateZipField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.NumberField;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;


public final class ACRDataBean_AdrsChng_MES extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_AdrsChng_MES.class);

  private AddressBean mAddress;
  private AddressBean pAddress;
  private AddressBean sAddress;
  private AddressHelper addressHelper;

  // construction
  public ACRDataBean_AdrsChng_MES()
  {
  }

  public ACRDataBean_AdrsChng_MES(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    //make the address beans for presentation
    mAddress = new AddressBean("m_");
    pAddress = new AddressBean("p_");
    sAddress = new AddressBean("s_");

    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_RISK);

  }

  //Overrides method from ..BeanBase to allow for the handling of
  //inner AddressBean Class
  public String renderHtml(String fName)
  {
    if( fName.equals("m_Address") || fName.equals("p_Address")||fName.equals("s_Address"))
    {

      if(editable)
      {
        AddressBean addy;
        if(fName.indexOf("m_")==0)
        {
          addy = (AddressBean)getMailingAddress();
        }
        else if(fName.indexOf("p_")==0)
        {
          addy = (AddressBean)getPhysicalAddress();
        }
        else
        {
          addy = (AddressBean)getAddress();
        }
        return addy.renderHtml();
      }
      else
      {
        //simply rebuilding each part from the ACRItem (DB)
        String prefix = fName.substring(0,2);

        StringBuffer sb = new StringBuffer();

        sb.append(super.renderHtml(prefix+"Adrs")).append("<br>");

        //CityStateZipField actually breaks each comp into its elements...
        //so when retrieving the info we actually have to grap each piece
        sb.append(super.renderHtml(prefix+"CityEtAlCity")).append(", ");
        sb.append(super.renderHtml(prefix+"CityEtAlState")).append(" ");
        sb.append(super.renderHtml(prefix+"CityEtAlZip"));

        return sb.toString();
      }
    }
    else if ( fName.equals("sendImpPlts") )
    {
      if(editable)
      {
        return super.renderHtml(fName);
      }
      else
      {
        if ( getACRValue(fName).equalsIgnoreCase("y") )
        {
          return "SEND Imprinter Plates: ";
        }
        else
        {
          return "DO NOT SEND Imprinter Plates: ";
        }
      }

    }
    else
    {
      return super.renderHtml(fName);
    }

  }

  /**
   * Disable editable comments during submission.
   */
  public boolean hasEditableComments()
  {
    return !editable && super.hasEditableComments();
  }

  protected void setDefaults()
  {
    // no-op
  }

  public FieldBean getPhysicalAddress()
  {
    return pAddress;
  }

  public FieldBean getMailingAddress()
  {
    return mAddress;
  }

  public FieldBean getAddress()
  {
    return sAddress;
  }

  /**
   * Instantiates an address helper, stores internal reference to it.
   */

  private AddressHelper _getAddressHelper()
  {
    if (addressHelper == null)
    {
      addressHelper = new AddressHelper(getMerchantNumber(),"ship",
                                        AddressHelper.FT_NONE);
    }
    return addressHelper;
  }

  public String renderAddress00()
  {
    return _getAddressHelper().getAddress("mif00").renderHtml();
  }

  public String renderAddress01()
  {
    return _getAddressHelper().getAddress("mif01").renderHtml();
  }

  public String renderAddress02()
  {
    return _getAddressHelper().getAddress("mif02").renderHtml();
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    //super.createExtendedFields();

    try
    {
      fields.deleteField("Rush");
      
      /**
       * Dup'd from parent... TODO: fix
       * so we don't have to create a mess of fields not used
       **/
      // imprinter plates
      Field imPlates = new CheckboxField("sendImpPlts","Check here if new imprinter plate is needed and indicate quantity.",false);
      imPlates.setShowName("Send Imprinter Plate?");

      Field imQuantity = new NumberField("ipQty",2,2,true,0,1,99);
      imQuantity.setShowName("Send Imprinter Plate?");

      // validations
      //add validations for the imprinter plate checkbox
      IfYesNotBlankValidation ynbVal = new IfYesNotBlankValidation(imPlates,"You must enter a quantity of imprinter plates.");
      imQuantity.addValidation(ynbVal);

      //add two final fields
      fields.add(imQuantity);
      fields.add(imPlates);
      /** DONE dup code **/

      //create new sub-bean version
      add(_getAddressHelper());

      AtLeastOneValidation atLeast1 = new AtLeastOneValidation("You must select at least one address to change.");

      /*****************************
      *(s) address generation
      ******************************/
      Field generic = new CheckboxField("isShippingAddress",
                                "Change Shipping Address:",
                                 false);
      generic.setHtmlExtra("class='tableData'");
      atLeast1.addField(generic);
      generic.addValidation(atLeast1);
      fields.add(generic);

      add(sAddress);

      Validation aNotBlankVal =
        new IfYesNotBlankValidation(generic,
                                    "Please provide all address information");

      sAddress.addValidation(aNotBlankVal,"aNotBlankVal");

      /*****************************
      *mailing (m) address generation
      ******************************/
      Field mailing = new CheckboxField("isMailingAddress",
                                "Change Mailing Address:",
                                 false);
      mailing.setHtmlExtra("class='tableData'");
      atLeast1.addField(mailing);
      fields.add(mailing);

      add(mAddress);

      Validation mNotBlankVal =
        new IfYesNotBlankValidation(mailing,
                                    "Please provide all address information");

      mAddress.addValidation(mNotBlankVal,"mNotBlankVal");


    /*****************************
    //physical (p) address generation
    ******************************/
    Field physical = new CheckboxField("isPhysicalAddress",
                              "Change Physical Address:",
                               false);

    physical.setHtmlExtra("class='tableData'");
    atLeast1.addField(physical);
    fields.add(physical);

      add(pAddress);

      Validation pNotBlankVal =
        new IfYesNotBlankValidation(physical,
                                    "Please provide all address information");

      pAddress.addValidation(pNotBlankVal,"pNotBlankVal");

      //we also need to tie validation from the physical address
      //to the imprinter plates... existing validation exists once that
      //condition is met

      IfCheckedIsCheckedValidation ccVal = new IfCheckedIsCheckedValidation(physical,"If physical address is selected, you must provide imprinter information.");
      ((Field)fields.getField("sendImpPlts")).addValidation(ccVal);

      // also make shipping address required for dba changes
      Field addressPicker = fields.getField("addressPicker");

      //removed 1-27-05 (per Keni Bush)
      //addressPicker.addValidation(new IfYesNotBlankValidation(physical,"Field required"));

      // relabel address picker to shipping address for error display
      addressPicker.setLabel("Shipping Address");

      // slight hack here...
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch (Exception e)
    {
      log.error("createExtendedFields(): "  + e);
    }

  }

  private class IfCheckedIsCheckedValidation implements Validation
  {
    private String errorText;
    private Field field;

    public IfCheckedIsCheckedValidation(Field field, String errorText)
    {
      this.field = field;
      this.errorText = errorText;
    }

    public String getErrorText()
    {
      return(errorText);
    }

    public boolean validate(String fieldData)
    {
      if(field!=null && field.getData().toUpperCase().equals("Y"))
      {
        if(fieldData != null && !fieldData.toUpperCase().equals("Y"))
        {
          return false;
        }
      }
      return true;
    }

  }

  public String getHeadingName()
  {
    return "Change Merchant Physical and/or Mailing Address";
  }

  private class AddressBean extends FieldBean
  {

    private String prefix;

    public AddressBean(String prefix)
    {
      this.prefix = prefix;
      initFields();
    }

    private void initFields()
    {

      Field field;

      //address
      field = new TextareaField(prefix+"Adrs","Address",100,2,40,true);
      field.setHtmlExtra("class='tableData'");
      fields.add(field);

      //city
      field = new CityStateZipField(prefix+"CityEtAl","City, State Zip",50,true);
      field.setHtmlExtra("class='tableData'");
      fields.add(field);

    }

    public String renderHtml()
    {
      StringBuffer sb = new StringBuffer(128);

      sb.append("<table width=100%>");

      sb.append("  <tr valign=top>");
      sb.append("    <td>");
      sb.append("    Address:");
      sb.append("    </td>");
      sb.append("    <td>");
      sb.append("    " + this.renderHtml(prefix+"Adrs"));
      sb.append("    </td>");
      sb.append("  </tr>");


      sb.append("  <tr valign=top>");
      sb.append("    <td>");
      sb.append("    City, State Zip:");
      sb.append("    </td>");
      sb.append("    <td>");
      sb.append("    " + this.renderHtml(prefix+"CityEtAl"));
      sb.append("    </td>");
      sb.append("  </tr>");

      sb.append("</table>");

      return sb.toString();
    }

    public void addValidation(Validation val, String valName)
    {
      getField(prefix+"Adrs").addValidation(val,"Please add a valid address.");
      getField(prefix+"CityEtAl").addValidation(val,"Please add a valid city, state, and zip code.");
    }

  }

  /**
   * Needs to determine which addresses are being changed and store the current
   * version of them as original addresses in the form of an html blob stuffed
   * in an acr item.
   */
  public void doPull()
  {
    super.doPull();

    if (editable)
    {
      // check for mailing address change
      if (fields.getData("isMailingAddress").toLowerCase().equals("y"))
      {
        // store copy of old mailing address
        acr.setACRItem("oldMailing","Old " + renderAddress02());
      }

      // check for physical address change
      if (fields.getData("isPhysicalAddress").toLowerCase().equals("y"))
      {
        // store copy of old physical address
        acr.setACRItem("oldPhysical","Old " + renderAddress00());
      }

      // check for shipping address change
      if (fields.getData("isShippingAddress").toLowerCase().equals("y"))
      {
        // store copy of old physical address
        acr.setACRItem("oldShipping","Old " + renderAddress01());
      }
    }
  }
} // class ACRDataBean_AdrsChng_MES
