package com.mes.acr;

import org.apache.log4j.Logger;
import com.mes.equipment.EquipSelectorItem;
import com.mes.forms.Condition;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HtmlRenderable;
import com.mes.forms.Validation;

public class SI_EquipSwap extends EquipSelectorItem
  implements ACRTranslator
{
  static Logger log = Logger.getLogger(SI_EquipSwap.class);

  public SI_EquipSwap()
  {
  }

  public void init()
  {

    if (type == T_STANDARD)
    {
      setColumnRenderer("col1", new HtmlRenderable()
        { public String renderHtml() { return getSerialNum(); } } );
      setColumnRenderer("col2", new HtmlRenderable()
        { public String renderHtml() { return getDescription(); } } );
      setColumnRenderer("col3", new HtmlRenderable()
        { public String renderHtml() { return getEquipType(); } } );
    }
    else if (type == T_OTHER)
    {
      Field serialNum = new Field(value + "_serNum","Serial number",20,12,true);
      setColumnRenderer("col1",serialNum);
      fields.add(serialNum);

      Field description
        = new Field(value + "_description","Equipment description",60,10,true);
      setColumnRenderer("col2",description);
      fields.add(description);

/*
      Field description = new DropDownField(value + "_description",
        "Equipment Description",
        TableFactory.getDropDownTable(TableFactory.EQUIP_DESC),true);
      description.addHtmlExtra("class=\"formField\"");
      setColumnRenderer("col2",description);
      fields.add(description);
*/

      Field equipType = new DropDownField(value + "_equipType",
        "Equipment type",
        TableFactory.getDropDownTable(TableFactory.EQUIP_TYPE),true);
      equipType.addHtmlExtra("class=\"formField\"");
      setColumnRenderer("col3",equipType);
      fields.add(equipType);
    }

    Field vNum = new Field(value + "_VNum","VNum/TID",20,6,true);
    setColumnRenderer("col4",vNum);
    fields.add(vNum);

    Field termApp = new Field(value + "_termApp","Terminal Application",20,8,true);
    setColumnRenderer("col5",termApp);
    fields.add(termApp);

    Field vendorId = new Field(value + "_vendorId","Vendor ID",20,8,true);
    setColumnRenderer("col6",vendorId);
    fields.add(vendorId);

  }

  public void setControlField(Field controlField)
  {
    super.setControlField(controlField);

    // in the case of other items need to make some of the fields required
    if (type == T_OTHER)
    {
      Condition vc = new FieldValueCondition(controlField,value);
      Validation required = new ConditionalRequiredValidation(vc);
      fields.getField(value + "_serNum").addValidation(required);
      fields.getField(value + "_description").addValidation(required);
      fields.getField(value + "_equipType").addValidation(required);
    }
  }

  public void doPush(ACR acr)
  {
    log.debug("pretending to push acr item data...");
  }

  public void doPull(ACR acr)
  {
    try
    {
      acr.setACRItem(value+"_serNum",getSerialNum());
      acr.setACRItem(value+"_description",getDescription());
      acr.setACRItem(value+"_equipType",getEquipType());
    }
    catch(Exception e)
    {
      //do nothing
    }
  }

/*
  //class for equipment description dropdown
  public final class NewEquipmentDropDownTable extends DropDownTable
  {
    public NewEquipmentDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      addElement("","");

      ResultSetIterator   it      = null;
      ResultSet           rs      = null;

      try {

        connect();

        int appTypeCode,cnt;

        appTypeCode=StringUtilities.stringToInt(getACRValue("Type Code"),-1);

        // revert to mes equipment if merchant app type renders no allowed equip
        if(appTypeCode>-1) {
          #sql [Ctx]
          {
            select count(*)
            into :cnt
            from equipment_application
            where app_type=:(appTypeCode)
          };
        } else
          cnt=0;

        if(cnt<1) {
          log.info("Reverting to MES equipment: No equipment available for app type code '"+appTypeCode+"'.");
          appTypeCode=0;
        }

        #sql [Ctx] it =
        {
          select    equip_description
          from      equipment_application ea
          where     ea.app_type=:appTypeCode
          order by  equip_description
        };

        rs = it.getResultSet();

        String val,desc;

        while (rs.next()) {
          val = rs.getString("equip_description");
          desc = val;
          addElement(val,desc);
        }

        it.close();
      }
      catch (Exception e) {
        log.error("NewEquipmentDropDownTable.getData() EXCEPTION: '"+e.getMessage()+"'.");
      }
      finally {
        cleanUp();
      }

    }

  } // NewEquipmentDropDownTable class



  public SI_EquipSwap()
  {
  }

  public void init()
  {
    if (type == T_STANDARD)
    {
      setColumnRenderer("col1", new HtmlRenderable()
        { public String renderHtml() { return getEquipType(); } } );
      setColumnRenderer("col2", new HtmlRenderable()
        { public String renderHtml() { return getSerialNum(); } } );
      setColumnRenderer("col3", new HtmlRenderable()
        { public String renderHtml() { return getDescription(); } } );
//      setColumnRenderer("col4", new HtmlRenderable()
//        { public String renderHtml() { return getPartNum(); } } );

      Field termApp = new DropDownField(value + "_termApp",
                                        "Term App",
                                        TableFactory.getDropDownTable(TableFactory.APP_TYPE),
                                        true);
      setColumnRenderer("col4",termApp);
      fields.add(termApp);

      setColumnRenderer("col5", new HtmlRenderable()
        { public String renderHtml() { return getDisposition(); } } );
    }
    else if (type == T_OTHER)
    {
      Field equipType = new DropDownField(value + "_equipType",
        "Equipment Type",
        TableFactory.getDropDownTable(TableFactory.EQUIP_TYPE),true);
      setColumnRenderer("col1",equipType);
      fields.add(equipType);

      Field serialNum = new Field(value + "_serNum","Serial number",20,10,true);
      setColumnRenderer("col2",serialNum);
      fields.add(serialNum);

      Field description
        = new Field(value + "_description","Equipment description",60,10,true);
      setColumnRenderer("col3",description);
      fields.add(description);

      Field termApp = new DropDownField(value + "_termApp",
                                        "Term App",
                                        TableFactory.getDropDownTable(TableFactory.APP_TYPE),
                                        true);
      setColumnRenderer("col4",termApp);
      fields.add(termApp);

      Field disposition = new DropDownField(value + "_disposition",
        "Disposition",
        TableFactory.getDropDownTable(TableFactory.DISPOSITION),true);
      setColumnRenderer("col5",disposition);
      fields.add(disposition);
    }
  }

  public void setControlField(Field controlField)
  {
    super.setControlField(controlField);
    Condition vc = new FieldValueCondition(controlField,value);
    Validation required = new ConditionalRequiredValidation(vc);
    fields.addGroupValidation(required);
  }

  public void doPush(ACR acr)
  {
    log.debug("pretending to push acr item data...");
  }

  public void doPull(ACR acr)
  {

    try
    {
      acr.setACRItem(value+"_serNum",getSerialNum());
      acr.setACRItem(value+"_description",getDescription());
      acr.setACRItem(value+"_equipType",getEquipType());
      //acr.setACRItem(value+"_termApp",getPartNum());
      acr.setACRItem(value+"_disposition",getDisposition());
    }
    catch(Exception e)
    {
      //do nothing
    }
  }
  */
}
