package com.mes.acr;

// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.RadioButtonField;
import com.mes.support.StringUtilities;

public final class ACRDataBean_EquipUpgrade_CBT extends ACRDataBean_EquipUpgrade
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_EquipUpgrade_CBT.class);


  // constants
  // (NONE)

  // construction
  public ACRDataBean_EquipUpgrade_CBT()
  {
  }

  public ACRDataBean_EquipUpgrade_CBT(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  protected ACRDataBeanAddressHelper _getAddressHelper()
  {
    if(adrsHelper!=null)
      return adrsHelper; // already configured by virtue of non-null obj ref

    adrsHelper = new ACRDataBeanAddressHelper(this);

    // configure the address helper
    adrsHelper.setAdrsTypeName("Ship To");
    adrsHelper.setAdrsSlctnFldName("staddress");
    //adrsHelper.setAdrsName("");
    adrsHelper.setAdrsAddressFldNme("sh_address");
    adrsHelper.setAdrsAttnFldNme("sh_attn");
    adrsHelper.setAdrsBankNameFldNme("sh_bnknme");
    adrsHelper.setAdrsCityFldNme("sh_city");
    adrsHelper.setAdrsStateFldNme("sh_state");
    adrsHelper.setAdrsZipFldNme("sh_zip");
    //adrsHelper.setAdrsCountry("sh_country");
    adrsHelper.setIsRequired(true);

    // cb&t specific filtering
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_BUSADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_MAILINGADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_EQUIPDELADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_OWNER1ADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_OWNER2ADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_OWNER3ADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_FULFILLADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_CONTACTADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_BANDADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MIF_BT1);

    return adrsHelper;
  }

  protected void createExtendedFields()
  {
    if(acr==null)
      return;

    Field field=null;
    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    String acrdiname=null;

    int rq = getResidentQueue();

    if(rq==MesQueues.Q_CHANGE_REQUEST_CBT_TSYS) {
      if((acrdi=acrd.getACRDefItem("exist_vnum"))!=null)
        fields.add(ACRDefItemToField(acrdi));
    }

    if(!editable)
      return;

    // address fields
    _getAddressHelper().createAddressFields();

    int merchBankNum = StringUtilities.stringToInt(getACRValue("Merchant Bank Number"),-1);
    boolean isTranscomMerchant = (getMerchantNumber().startsWith("4903"));

    if((acrdi=acrd.getACRDefItem("replace_equip"))!=null) {
      field=new CheckboxField(acrdi.getName(),"Click here if purpose of order is to use new equipment in place of existing equipment (upgrade).",false);
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("add_equip"))!=null) {
      field=new CheckboxField(acrdi.getName(),"Click here if purpose or order is to add equipment.",false);
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("add_refab"))!=null) {
      field=new CheckboxField(acrdi.getName(),"Click here if purpose or order is to add new or refurbished equipment.",false);
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("return_equip"))!=null) {
      field=new CheckboxField(acrdi.getName(),"Click here if merchant is renting equipment that needs to be returned and a call tag needs to be sent.",false);
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }

    // new equipment info

    if((acrdi=acrd.getACRDefItem("newequip_term_app"))!=null) {
      TerminalApplicationDropDownTable ddt = new TerminalApplicationDropDownTable();
      field=new DropDownField(acrdi.getName(),ddt,!acrdi.isRequired());
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("newequip_desc"))!=null) {
      NewEquipmentDropDownTable ddt = new NewEquipmentDropDownTable();
      field=new DropDownField(acrdi.getName(),ddt,!acrdi.isRequired());
      field.setShowName(acrdi.getLabel());
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("newequip_purcorrental"))!=null) {
      String[][] arr = null;
      if(merchBankNum==3858 || merchBankNum==3860 || isTranscomMerchant)
        arr = new String[][]
        {
           {"Purchase","Purchase"}
          ,{"Rental","Rental"}
          ,{"Owned","Owned"}
        };
      else
        arr = new String[][]
        {
           {"Purchase","Purchase"}
          ,{"Rental","Rental"}
          ,{"New","New"}
          ,{"Refurbished","Refurbished"}
          ,{"Owned","Owned"}
        };

      field = new RadioButtonField(acrdi.getName(),arr, -1, !acrdi.isRequired(), "Please specify whether the new equipment item is a purchase or rental." );
      field.setShowName(acrdi.getLabel());
      ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("newequip_qty"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("newequip_peritemcost"))!=null)
      fields.add(ACRDefItemToField(acrdi));

    // existing equipment info
    if((acrdi=acrd.getACRDefItem("exist_vnum"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("exist_sendcalltag"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("exist_equiptype"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("exist_serialnum"))!=null)
      fields.add(ACRDefItemToField(acrdi));

    // equipment lease info (show only for Bank 3941)
    if(isMESBank(merchBankNum)) {
      if((acrdi=acrd.getACRDefItem("exist_leaseauthnum"))!=null)
        fields.add(ACRDefItemToField(acrdi));
      if((acrdi=acrd.getACRDefItem("exist_mnthleaseamt"))!=null)
        fields.add(ACRDefItemToField(acrdi));
      if((acrdi=acrd.getACRDefItem("exist_leaseaccnum"))!=null)
        fields.add(ACRDefItemToField(acrdi));
      if((acrdi=acrd.getACRDefItem("exist_leasenummonths"))!=null) {
        LeaseMonthsDropDownTable ddt = new LeaseMonthsDropDownTable();
        field=new DropDownField(acrdi.getName(),ddt,!acrdi.isRequired());
        field.setShowName(acrdi.getLabel());
        fields.add(field);
      }
      if((acrdi=acrd.getACRDefItem("exist_leasefunding"))!=null)
        fields.add(ACRDefItemToField(acrdi));
    }

    // terminal features
    if((acrdi=acrd.getACRDefItem("tf_accesscode"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_accesscode_code"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_autobatchclose"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_autobatchclose_hh"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_autobatchclose_mm"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_autobatchclose_ampm"))!=null) {
      String[][] arr = new String[][]
      {
         {"AM","AM"}
        ,{"PM","PM"}
      };
      field = new RadioButtonField(acrdi.getName(),arr, -1, !acrdi.isRequired(), "Please specify AM or PM for the Auto Batch Close terminal feature." );
      field.setShowName(acrdi.getLabel());
      ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("tf_rcpthdrl4"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_rcpthdrl4_desc"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_rcpthdrl5"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_rcpthdrl5_desc"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_rcptftr"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_rcptftr_desc"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_resettransnumdaily"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_invnumprompton"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_fraudcntrlon"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_pswdprotecton"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_purchcardflagon"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_avs"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_trmrmndrtct"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_trmrmndrtct_hh"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_trmrmndrtct_mm"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_trmrmndrtct_ampm"))!=null) {
      String[][] arr = new String[][]
      {
         {"AM","AM"}
        ,{"PM","PM"}
      };
      field = new RadioButtonField(acrdi.getName(),arr, -1, !acrdi.isRequired(), "Please specify AM or PM for the Terminal Reminder terminal feature." );
      field.setShowName(acrdi.getLabel());
      ((RadioButtonField)field).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
      fields.add(field);
    }
    if((acrdi=acrd.getACRDefItem("tf_tipoptnon"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("tf_cspo"))!=null)
      fields.add(ACRDefItemToField(acrdi));

    // CVV2
    field = new CheckboxField("tf_cvv2","CVV2",false);
    fields.add(field);
    // phone train
    field = new CheckboxField("tf_phonetrain","Phone Training for Merchant",false);
    fields.add(field);


    // validation
    fields.addValidation(new EntryValidation());
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_CBT_NEW}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  public String getHeadingName()
  {
    return "Order Additional or Upgraded Equipment for CB&T Merchant";
  }

  public String getJSPIncludeFilename()
  {
    return (acr==null)? "":"acr_incl_"+acr.getDefinition().getShortName()+"_cbt.jsp";
  }

} // class ACRDataBean_EquipUpgrade_CBT
