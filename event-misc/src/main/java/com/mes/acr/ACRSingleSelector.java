package com.mes.acr;

import java.util.Iterator;
import java.util.List;
import com.mes.forms.SelectorFrame;
import com.mes.forms.SingleSelector;

public class ACRSingleSelector extends SingleSelector implements ACRTranslator
{
  public ACRSingleSelector(String name, List selectorItems, 
    SelectorFrame selectorFrame)
  {
    super(name,selectorItems,selectorFrame);
  }
  
  public ACRSingleSelector(String name, List selectorItems)
  {
    super(name,selectorItems);
  }
  
  public void doPush(ACR acr)
  {
    List selected = getSelected();
    for (Iterator i = selected.iterator(); i.hasNext();)
    {
      ((ACRTranslator)i.next()).doPush(acr);
    }
  }
  
  public void doPull(ACR acr)
  {
    List selected = getSelected();
    for (Iterator i = selected.iterator(); i.hasNext();)
    {
      ((ACRTranslator)i.next()).doPull(acr);
    }
  }
}