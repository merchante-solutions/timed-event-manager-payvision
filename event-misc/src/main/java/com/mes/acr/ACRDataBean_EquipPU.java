package com.mes.acr;

import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.Field;
import com.mes.forms.Validation;

public class ACRDataBean_EquipPU extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_EquipPU.class);


  // constants
  // (NONE)

  // data members
  protected ACRDataBeanAddressHelper adrsHelper = null;
  protected EquipCallTagTable        ectt       = null;

  // construction
  public ACRDataBean_EquipPU()
  {
  }

  public ACRDataBean_EquipPU(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    // equip call tag table
    int[] ecctColumns = new int[]
      {
         EquipCallTagTable.COL_SENDCALLTAG
        ,EquipCallTagTable.COL_DESCRIPTION
        ,EquipCallTagTable.COL_EQUIPTYPE
        ,EquipCallTagTable.COL_PARTNUMBER
        ,EquipCallTagTable.COL_SERIALNUMBER
        ,EquipCallTagTable.COL_DISPOSITION
      };
    String[][] ecctMap = new String[][]
      {
         {EquipCallTagTable.NCN_SENDCALLTAG,   "ssh"}
        ,{EquipCallTagTable.NCN_PARTNUMBER,    "part_num"}
        ,{EquipCallTagTable.NCN_SERIALNUMBER,  "serial_num"}
        ,{EquipCallTagTable.NCN_VNUMBER,       "vnum"}
        ,{EquipCallTagTable.NCN_DISPOSITION,   "disposition"}
        ,{EquipCallTagTable.NCN_DESCRIPTION,   "desc"}
        ,{EquipCallTagTable.NCN_EQUIPTYPE,     "type"}
      };
    //the 4 is completely arbitrary.. it will be reset in the constr
    ectt = new EquipCallTagTable(this, ecctColumns, ecctMap, 4, true);
  }

  public boolean equipmentExists()
  {
    return ectt.equipmentExists();
  }

  public boolean calltaggedEquipmentExists()
  {
    return ectt.calltaggedEquipmentExists();
  }

  public Vector getCallTaggedEquipment()
  {
    return ectt.getCallTaggedEquipment();
  }

  public String renderEquipCallTagTable()
  {
    return ectt.renderHtmlTable();
  }

  protected ACRDataBeanAddressHelper _getAddressHelper()
  {
    if(adrsHelper!=null)
      return adrsHelper; // already configured by virtue of non-null obj ref

    adrsHelper = new ACRDataBeanAddressHelper(this);

    // configure the address helper
    adrsHelper.setAdrsTypeName("Ship To");
    adrsHelper.setAdrsSlctnFldName("staddress");
    //adrsHelper.setAdrsName("");
    adrsHelper.setAdrsAddressFldNme("sh_address");
    adrsHelper.setAdrsAttnFldNme("sh_attn");
    adrsHelper.setAdrsBankNameFldNme("sh_bnknme");
    adrsHelper.setAdrsCityFldNme("sh_city");
    adrsHelper.setAdrsStateFldNme("sh_state");
    adrsHelper.setAdrsZipFldNme("sh_zip");
    //adrsHelper.setAdrsCountry("sh_country");
    adrsHelper.setIsRequired(true);

    return adrsHelper;
  }

  public String renderAddressHtml()
  {
    return _getAddressHelper().renderAddressHtml();
  }

  public void doPull()
  {
    super.doPull();

    if(!editable)
      return;

    _getAddressHelper().pullAddressFields();
  }

  protected void setDefaults()
  {
    if(getMerchantNumber().length()>0) {

      // merchant address
      _getAddressHelper().setACRAdrsInfo();

    }

  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;
    String fldnme=null;

    // address fields
    _getAddressHelper().createAddressFields();

    // equip call tag table fields
    ectt.createFields(this.fields);

    if((acrdi=acrd.getACRDefItem("reason_for_calltag"))!=null)
    {
      field = ACRDefItemToField(acrdi);
      field.makeRequired();
      fields.add(field);
    }

    // validation
    fields.addValidation(new EntryValidation(ectt.getNumRows()));
  }

  protected final class EntryValidation implements Validation
  {
    protected String ErrorText = "";
    protected int fieldNum = 4;

    public EntryValidation(int fieldNum)
    {
      this.fieldNum = fieldNum;
    }

    public String getErrorText()
    {
      return(ErrorText);
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      boolean bCallTagsExist = false;
      for(int i=1; i <= fieldNum; i++)
      {
        if(!fields.getField("ssh_"+i).isBlank())
        {
          bCallTagsExist = true;
          break;
        }
      }

      // require at least one piece of equipment to be picked up
      if(!bCallTagsExist) {
        ErrorText = "At least one piece of equipment must be selected for call tag pickup.";
        return false;
      }

      Field fld;

      // for each selected piece of equipment, require the description and serial number
      for(int i=1;i<=fieldNum;i++) {
        fld = fields.getField("ssh_"+i);

        if(!fld.isBlank()) {
          if( fields.getField("desc_"+i).isBlank()
              || fields.getField("serial_num_"+i).isBlank()
              || fields.getField("type" + "_" + i).isBlank()
            )
           {
            ErrorText = "The equipment description, type, and serial number must be specified for equipment row number "+i+".";
            return false;
          }
        }
      }

      return true;
    }

  } // EntryValidation

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT:
      case MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP:
      case MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP:
      case MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  public String getHeadingName()
  {
    return "Order Equipment Pickup from Merchant";
  }

} // class ACRDataBean_EquipPU
