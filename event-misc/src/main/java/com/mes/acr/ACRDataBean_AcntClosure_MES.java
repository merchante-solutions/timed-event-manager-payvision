package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.RadioField;
import com.mes.forms.Selector;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DBGrunt;
import com.mes.tools.DropDownTable;

public final class ACRDataBean_AcntClosure_MES extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_AcntClosure_MES.class);

  /**
   * Standard Constructor
   */
  public ACRDataBean_AcntClosure_MES()
  {
  }

  public ACRDataBean_AcntClosure_MES(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    generateDateInfo();
    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP);
  }

  /**
   * Factory for selector creation and the selector itself
   */
  private ACRSelectorFactory factory = new ACRSelectorFactory();
  private Selector equipSelector;

  /**
   * Can only be dated that day's current date
   */
  private String cancelDate;

  /**
   * Info for post submit only
   */
  private String dateOpened = "";
  private String dateLastDeposit= "";

  public String getDateOpened()
  {
    return dateOpened;
  }

  public String getDateLastDeposit()
  {
    return dateLastDeposit;
  }

  /**
   * Selector Generation
   * ST_EQUIPDISP_FLAG
   */
  public Selector getEquipmentSelector()
  {
    if (equipSelector == null)
    {
     equipSelector = factory.createSelector(factory.ST_ACCT_CLOSE_CALLTAGS,
        getMerchantNumber());
    }
    return equipSelector;
  }

  /**
   * getSelectedEquipment()
   * provides a list of selected equipment for order generation
   * during ACRPump call
   */
  public List getSelectedEquipment()
  {
    return getEquipmentSelector().getSelected();
  }

  /**
   * Turn off Comments component
   */

 /* Comments activated as of 1/24/05 (per Keni Bush)
  public boolean hasEditableComments()
  {
    return !editable && super.hasEditableComments();
  }
  */

  protected void setDefaults()
  {

    try
    {
      cancelDate = DateTimeFormatter.getFormattedDate( DBGrunt.getCurrentDate(),
                                                       "MM/dd/yy");
    }
    catch(Exception e)
    {
      try
      {
        cancelDate = DateTimeFormatter.getCurDateString();
      }
      catch (Exception e1)
      {
        cancelDate = "";
      }
    }
  }

  protected void createExtendedFields()
  {
    if(acr==null)
      return;

    // accept amount (editable in status mode)
    Field acceptAmt = new CurrencyField("acceptAmt","Accept Amount",9,9,true,-999999f,999999f);
    acceptAmt.setLabel("Accept Amount");


    if(!editable)
    {

      //if(residentQueue == MesQueues.Q_CHANGE_REQUEST_RESEARCH)
      //{
        Field field;

        // accepted/denied (editable in status mode)
        String[][] arrReqStat =
        {
           { "Accepted","Accepted" }
          ,{ "Denied","Denied" }
        };

        field = new RadioField("requestStatus","Request Status",arrReqStat);
        fields.add(field);

        acceptAmt.setData(getACRValue("acceptAmt"));

     // }

    }
    else
    {

      acceptAmt.setData("0");

      try
      {

        //add equipment selector
        add(getEquipmentSelector());

        //add Address subbean
        add(getAddressHelper());

        // make address required if equipment is selected
        getAddressHelper().requireIfSelected(getEquipmentSelector());

      }
      catch (Exception e)
      {
        log.debug("Failed to load sub-bean components");
      }

      // default to rush request
      //fields.deleteField("Rush");
      //fields.add(new CheckboxField("Rush","Rush?",true));

      String[][] masterButtons =
      {
         { "No","No" }
        ,{ "Yes","Yes" }
      };

      Field master = new RadioField("isMasterAccount","Is the Master Account being closed?",masterButtons);
      Field slave = new Field("slaveAccount","Slave Account",16,10,true);
      slave.addValidation(new ConditionalRequiredValidation
                            (new FieldValueCondition(master, "Yes"),
                            "You must supply a new Master Account Number."
                            )
                          );

      fields.add(master);
      fields.add(slave);
      fields.add(new HiddenField("cancelDate",cancelDate));
      fields.add(new DropDownField("cancelReason", "Reason for Cancellation", new _ddTable(), false));
      fields.add(new DropDownField("cancelType", "Cancellation Type", new _ddTypeTable(), false));
    }

    fields.add(acceptAmt);
  }
 /*
  public int[][] getAllowedQueueOps()
  {
   /*START OLD
    Determine initial queue according to:
     - All accounts except MES have a one year contract.  Route Closures opened > 1 year straight to TSYS bypassing Research
     - MES has a three year contract.  Route Closures opened > 3 years straight to TSYS bypassing Research

    int bankNum = StringUtilities.stringToInt(acr.getACRValue("Merchant Bank Number"),-1);
    int numMonthsOpen = 0;

    try {
      connect();

      #sql
      {
        select months_between(sysdate,ACTIVATION_DATE)
        into   :numMonthsOpen
        from   MIF
        where  MERCHANT_NUMBER = :(getMerchantNumber())
      };
    }
    catch(Exception e) {
    }
    finally {
      cleanUp();
    }

    int initQueue = MesQueues.Q_CHANGE_REQUEST_RESEARCH;

    if(bankNum == 3941)
    {
      if(numMonthsOpen > 12)
      {
        initQueue = MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP;
      }
    }
    else
    {
      if(numMonthsOpen > 36)
      {
        initQueue = MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP;
      }
    }
    //END OLD

    if( mgmtQ.isCurrentQueue(getResidentQueue()) )
    {
      return mgmtQ.getMgmtQOps(acr.getID());
    }
    else
    {
      return ACRQueueOps.getMESQueueOps(initialQueue, mgmtQ.getId(), mgmtQ.isMgmtQOn());
    }

  }
 */
  public String getHeadingName()
  {
    return "Close Merchant Account";
  }

  public String renderHtml(String name)
  {
    if(name.equals("cancelDate") && editable)
    {
      return cancelDate;
    }
    else
    {
      return super.renderHtml(name);
    }
  }

  public class _ddTable extends DropDownTable
  {
    public _ddTable()
    {
      super();
      addElement("","Select One");
      addElement("Merchant Request - Business Closed","Merchant Request - Business Closed");
      addElement("Merchant Request - Re-Pricing","Merchant Request - Re-Pricing");
      addElement("Merchant Request - Multi-Merchant Closing","Merchant Request - Multi-Merchant Closing");
      addElement("Merchant Request - New Business Ownership","Merchant Request - New Business Ownership");
      addElement("Merchant Request - Do Not Need Card Services","Merchant Request - Do Not Need Card Services");
      addElement("Merchant Request - Chose Different Processor","Merchant Request - Chose Different Processor");
      addElement("Merchant Request - Poor Service from Sales Rep","Merchant Request - Poor Service from Sales Rep");
      addElement("Merchant Request - Letter/Increase","Merchant Request - Letter/Increase");
      addElement("Merchant Request - Chargeback","Merchant Request - Chargeback");
      addElement("Merchant Request - Product/Equipment","Merchant Request - Product/Equipment");
      addElement("Merchant Request- Discount Rate/Fees" ,"Merchant Request- Discount Rate/Fees" );
      addElement("Merchant Request - Other","Merchant Request - Other");
      addElement("Internal Request - Risk","Internal Request - Risk");
      addElement("Internal Request - DDA/TR","Internal Request - DDA/TR");
      addElement("Internal Request - ACH Rejects/NSF from NSF Fees","Internal Request - ACH Rejects/NSF from NSF Fees");
      addElement("Internal Request - Lack Merch of Response to Contact","Internal Request - Lack of Merch Response to Contact");
      addElement("Internal Request - Low Volume","Internal Request - Low Volume");
      addElement("Internal Request - Account Open In Error","Internal Request - Account Open In Error");
      addElement("Internal Request - Collection","Internal Request - Collection");
      addElement("Internal Request - Inactivity" ,"Internal Request - Inactivity" );
      addElement("Internal Request - Opening new ID; Closing old acct","Internal Request - Opening new ID; Closing old acct");
      addElement("Internal Request - Bank Termination","Internal Request - Bank Termination");
      addElement("Internal Request - Returned Mail","Internal Request - Returned Mail");
      addElement("Internal Request - Credit Policy","Internal Request - Credit Policy");
      addElement("Internal Request - MasterCard ECP","Internal Request - MasterCard ECP");
      addElement("Internal Request - Visa MCMP","Internal Request - Visa MCMP");
      addElement("Internal Request - Other","Internal Request - Other");
    }
  }

/*old
  public class _ddTypeTable extends DropDownTable
  {
    public _ddTypeTable()
    {
      super();
      addElement("","Select One");
      addElement("Cancel account on TSYS and Block & Delete in MMS","Cancel account on TSYS and Block & Delete in MMS");
      addElement("Block & Delete V# in MMS only","Block & Delete V# in MMS only");
      addElement("Block V# only","Block V# only");
    }
  }
*/

  //New table as of 02/10
  public class _ddTypeTable extends DropDownTable
  {
    public _ddTypeTable()
    {
      super();
      addElement("","Select One");
      addElement("Close Account and Block the TID(s)","Close Account and Block the TID(s)");
      addElement("Block TID(s) only","Block TID(s) only");
      addElement("Remove MMS profile only","Remove MMS profile only");
      addElement("Close Account and leave TID(s) open","Close Account and leave TID(s) open");
    }
  }

  public String renderEquipmentHTML()
  {
    //this is hack to reveal new equipment info... to be fixed?!
    //all naming is directly related to the bean and selector item/frame/factory
    boolean hasEquip = false;
    int LIMIT = 100;
    String item = "";
    StringBuffer sb = new StringBuffer();

    for (int i=0; i < LIMIT; i++)
    {
      item = getACRValue(CALLTAG_EQUIPMENT_NAME+"_"+i);
      if ( ! ( item.equals("") || item.equals("n") ) )
      {
        if(!hasEquip)
        {
          hasEquip = true;

          sb.append("<table cellpadding=\"1\" border=\"0\" align=\"center\" width=\"100%\" class=\"tableFillGray\" cellspacing=\"0\">");
          sb.append("<tr>");
          sb.append("<td>");
          sb.append("<table class=\"formTable\" width=\"100%\" border=\"0\" cellspacing=\"0\">");
          sb.append("  <tr>");
          sb.append("  <th colspan=5 class=\"tableColumnHead\">Equipment to Pick up - Call Tag Generation</th>");
          sb.append("  </tr>");
          sb.append("  <tr>");
          sb.append("   <td class=\"tableData\" align=\"center\">Equipment Type</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Serial Number</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Description</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Part Num</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Disposition</td>");
          sb.append(" </tr>");
          sb.append(" <tr>");
          sb.append("   <td colspan=5 height=\"1\" bgcolor=\"#cccccc\"><img src=\"/images/spcr_transp.gif\" height=\"1\"></td>");
          sb.append("  </tr>");
        }
        sb.append("<tr>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(generateCallTagLink(getACRValue(item+"_equipType"))).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_serNum")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_description")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_partNum")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_disposition")).append("</td>");
        sb.append("</tr>");
      }
    }

    //finish up
    if(hasEquip)
    {
      sb.append("</table></td></tr></table>");
    }
    else
    {
      sb.append("No equipment selected for this ACR.");
    }

    return sb.toString();
  }

  private void generateDateInfo()
  {
    String merchNum = getMerchantNumber();
    if (merchNum.length() == 0)
    {
      return;
    }

    PreparedStatement ps = null;
    ResultSet         rs = null;

    try
    {
      connect();

      String qs = "select "+
                  "nvl(date_opened,'--') as openDate, "+
                  "nvl(to_char(last_deposit_date,'mm/dd/yy') ,'--') as depDate "+
                  "from mif "+
                  "where merchant_number = ?";
      ps = con.prepareStatement(qs);
      ps.setString(1,merchNum);

      rs = ps.executeQuery();
      String temp;
      if (rs.next())
      {
        temp       = processString(rs.getString("openDate"));
        if(temp!=null && temp.length()==6)
        {
          dateOpened = temp.substring(0,2)+"/"+temp.substring(2,4)+"/"+temp.substring(4);
        }

        dateLastDeposit  = processString(rs.getString("depDate"));
      }
    }
    catch (Exception e)
    {
      log.error(this.getClass().getName() + ".setDefaults(): " + e);
      dateOpened = "--";
      dateLastDeposit = "--";
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  /**
   * getCallTaggedEquipment and calltaggedEquipmentExists
   * both necessary to generate call tags, but implemented
   * differently depending on the required status of said tags
   */
  public Vector getCallTaggedEquipment()
  {
    return ACRUtility.processCallTaggedEquipment(getACR());
  }

  public boolean calltaggedEquipmentExists()
  {
    //by very nature - must be true
    return true;
  }

} // class ACRDataBean_AcntClosure
