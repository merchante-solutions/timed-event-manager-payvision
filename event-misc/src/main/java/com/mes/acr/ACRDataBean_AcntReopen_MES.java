package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.RadioButtonField;
import com.mes.forms.StateDropDownTable;

public final class ACRDataBean_AcntReopen_MES extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_AcntReopen_MES.class);

  String closureDate;
  boolean isOverdue;

  // construction
  public ACRDataBean_AcntReopen_MES()
  {
  }

  public ACRDataBean_AcntReopen_MES(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_RISK);
  }

  public void setDefaults()
  {

    String merchNum = getMerchantNumber();

    //little SQL to get closure date (if it exists.)
    if(acr==null || merchNum.length()<1)
    {
      closureDate = "No date available.";
      return;
    }

    PreparedStatement ps = null;
    ResultSet         rs = null;

    try
    {
      connect();

      String qs = "select"
                + " nvl(to_char(DATE_STAT_CHGD_TO_DCB,'mm/dd/yyyy'),"
                + " 'Account does not appear to be closed.')  as close_date,"
                + " trunc(sysdate) - trunc(nvl(DATE_STAT_CHGD_TO_DCB,sysdate)) as is_overdue"
                + " from mif"
                + " where merchant_number = ?";

      ps = con.prepareStatement(qs);
      ps.setString(1,merchNum);

      rs = ps.executeQuery();

      if (rs.next())
      {
        closureDate = processString(rs.getString("close_date"));
        isOverdue = ( rs.getInt("is_overdue") > 60 );
      }
      else
      {
        closureDate = "Unable to find Merchant Closure Date.";
      }

    }
    catch (Exception e)
    {
      log.error("setDefaults() exception: " + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }

  }

  //turn off multimerchant if overdue
  public boolean hasMultiMerchantComp()
  {
    if(isOverdue)
    {
      return false;
    }
    else
    {
      return super.hasMultiMerchantComp();
    }
  }

 //turn off hasEditableComments
  public boolean hasEditableComments()
  {
    return false;
    /*
    if(isOverdue)
    {
      return false;
    }
    else
    {
      return super.hasEditableComments();
    }
    */
  }

 //turn on disableParentButtons if overdue
  public boolean disableParentButtons()
  {
    if(isOverdue)
    {
      return true;
    }
    else
    {
      return super.disableParentButtons();
    }
  }

  protected void createExtendedFields()
  {

    if(acr==null)
      return;

    //DropDownTable refCloseField= new RefundTable();
    CurrencyField field = new CurrencyField("refCloseFee","refCloseFee",10,10,false,0.0f,9999.99f);

    if(!editable)
    {
      //fields.add(new DropDownField("refCloseFee","refCloseFee", refCloseField , false, getACRValue("refCloseFee")));
      field.setData(getACRValue("refCloseFee"));
      fields.add(field);
      return;
    }

    fields.add(field);
    //fields.add(new DropDownField("refCloseFee","refCloseFee", refCloseField , false, "None"));

    //Create a radio selection for account type to change
    //defaults to "Both Accounts"
    String[][] buttons = new String[][]
    {
      {"Closed in Error","Closed in Error"},
      {"Merchant Changed Mind","Merchant Changed Mind"},
      {"Merchant Changed Banking Information","Merchant Changed Banking Information"}
    };

    RadioButtonField reopenReason
    = new RadioButtonField( "reopenReason",
                            "Reason for Re-Open",
                            buttons,
                            0,
                            true,
                            "Please select an reason for account to be re-opened.");
    fields.add(reopenReason);




    //Create a radio selection for account type to change
    //defaults to "Both Accounts"
    buttons = new String[][]
    {
      {"Deposit and Fee","Deposit and Fee Accounts"},
      {"Deposit","Deposit Account"},
      {"Fee","Fee Account"},
      {"Not Applicable","Not Applicable"}
    };

    RadioButtonField accountTypes
    = new RadioButtonField( "accntType",
                            "Account Type",
                            buttons,
                            0,
                            true,
                            "Please select an Account Type.");
    fields.add(accountTypes);
    fields.add(new Field("vtcNums","V T C Numbers",true));

    fields.add(new Field("bankName","Bank Name",true));
    fields.add(new Field("openYears","Years Open",true));
    fields.add(new Field("cAcct","Checking Account",true));
    fields.add(new Field("tRouting","Transit Routing",true));
    fields.add(new Field("bAddress","Address",40,25,true));
    fields.add(new Field("bCity","City",true));
    fields.add(new DropDownField("bState","State",new StateDropDownTable(),true));
    fields.add(new Field("bZip","Zip",true));

    buttons = new String[][]
    {
      { "No","No" },
      { "Yes","Yes" }
    };

    RadioButtonField equipment
    = new RadioButtonField( "equip",
                            "Need Equipment",
                            buttons,
                            0,
                            true,
                            "Does Merchant need equipment?",
                            RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
    fields.add(equipment);

    RadioButtonField pricing
    = new RadioButtonField( "pricing",
                            "Need New Pricing",
                            buttons,
                            0,
                            true,
                            "Does Merchant need new pricing?",
                            RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
    fields.add(pricing);
  }

/*
  public class RefundTable extends DropDownTable
  {
    public RefundTable()
    {
      addElement("None",      "None");
      addElement("$100",   "$100");
      addElement("$300",    "$300");
    }

  }
*/

  public String renderHtml(String fName)
  {
    //override the display of the closure Date...
    if(fName.equals("closeDate"))
    {
      return getClosureDate();
    }

    return super.renderHtml(fName);

  }

  public String getClosureDate()
  {
    return closureDate;
  }

  public boolean isOverdue()
  {
    return isOverdue;
  }

  public int[][] getAllowedQueueOps()
  {
    int[][] queues = super.getAllowedQueueOps();
    return ACRQueueOps.removeQueueOp(MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,queues);
  }

  public String getHeadingName()
  {
    return "Re-Open Merchant Account";
  }


} // class ACRDataBean_AcntReopen_MES
