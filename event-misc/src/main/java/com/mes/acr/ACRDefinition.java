package com.mes.acr;

import java.util.Enumeration;
import java.util.Hashtable;
import org.apache.log4j.Logger;
import com.mes.constants.MesUsers;
import com.mes.database.ValueObjectBase;
import com.mes.user.PermissionsMap;
import com.mes.user.UserBean;

/**
 * ACRDefinition class
 *
 * Represents a single Account Change Request Defintion.
 */
public class ACRDefinition extends ValueObjectBase
{
  static Logger log = Logger.getLogger(ACRDefinition.class);

  // constants
  public static final String  FUNC_SUBMIT                 = "submit";
  public static final String  FUNC_VIEW                   = "view";

  // multiplicity types
  public static final int     MULTTYPE_UNDEFINED          = 0;
  public static final int     MULTTYPE_ONEPERACCOUNT      = 1;
  public static final int     MULTTYPE_ONEOPENPERACCOUNT  = 2;

  // data members
  protected long              id;
  protected String            type;               // descriptive name for organizing into groups
  protected String            name;
  protected String            short_name;
  protected String            description;

  protected String            beanClassName;
  protected String            rightStr;
  protected String            jspInclude;

  protected boolean           is_on;              // on/off switch to support notion of availability
  protected boolean           initMgmtQ;          //determines if ACR should route to Mgmt Q first

  protected int               multiplicity_type;  // constraints for co-existence usu. for a given account (merchant)
  protected Hashtable         acrd_items;         // 1..N possible def. items

  protected PermissionsMap    permissions;

  // class methods
  private static long x_getAssociatedUserRight(String short_name)
  {
    log.warn("Call to obsolete method getAssociatedUserRight('"
      + short_name + "')");

    if(short_name.equals("feechng"))
      return MesUsers.RIGHT_ACR_TYPE_FEECHNG;
    else if(short_name.equals("termacces"))
      return MesUsers.RIGHT_ACR_TYPE_TERMACCESS;
    else if(short_name.equals("termacces_v2"))
      return MesUsers.RIGHT_ACR_TYPE_TERMACCESS_V2;
    else if(short_name.equals("ddachng"))
      return MesUsers.RIGHT_ACR_TYPE_DDACHNG;
    else if(short_name.equals("ddachng_v2"))
      return MesUsers.RIGHT_ACR_TYPE_DDACHNG_V2;
    else if(short_name.equals("cardadds"))
      return MesUsers.RIGHT_ACR_TYPE_CARDADDS;
    else if(short_name.equals("cardadds_v2"))
      return MesUsers.RIGHT_ACR_TYPE_CARDADDS_V2;
    else if(short_name.equals("acccls"))
      return MesUsers.RIGHT_ACR_TYPE_ACCCLS;
    else if(short_name.equals("acccls_v2"))
      return MesUsers.RIGHT_ACR_TYPE_ACCCLS_V2;
    else if(short_name.equals("supordfrm"))
      return MesUsers.RIGHT_ACR_TYPE_SUPORDFRM;
    else if(short_name.equals("newsupordfrm"))
      return MesUsers.RIGHT_ACR_TYPE_SUPORDFRM_NEW;
    else if(short_name.equals("adrchng"))
      return MesUsers.RIGHT_ACR_TYPE_ADRCHNG;
    else if(short_name.equals("adrchng_v2"))
      return MesUsers.RIGHT_ACR_TYPE_ADRCHNG_V2;
    else if(short_name.equals("dbachng"))
      return MesUsers.RIGHT_ACR_TYPE_DBACHNG;
    else if(short_name.equals("dbachng_v2"))
      return MesUsers.RIGHT_ACR_TYPE_DBACHNG_V2;
    else if(short_name.equals("feervsl"))
      return MesUsers.RIGHT_ACR_TYPE_FEERVSL;
    else if(short_name.equals("feervsl_v2"))
      return MesUsers.RIGHT_ACR_TYPE_FEERVSL_V2;
    else if(short_name.equals("reopcl"))
      return MesUsers.RIGHT_ACR_TYPE_REOPCL;
    else if(short_name.equals("reopen_v2"))
      return MesUsers.RIGHT_ACR_TYPE_ACCREOP_V2;
    else if(short_name.equals("equipswap"))
      return MesUsers.RIGHT_ACR_TYPE_EQUIPSWAP;
    else if(short_name.equals("equipswap_v2"))
      return MesUsers.RIGHT_ACR_TYPE_EQUIPSWAP_V2;
    else if(short_name.equals("equipupg"))
      return MesUsers.RIGHT_ACR_TYPE_EQUIPUPG;
    else if(short_name.equals("equipupg_v2"))
      return MesUsers.RIGHT_ACR_TYPE_EQUIPUPG_V2;
    else if(short_name.equals("reqtrmprg"))
      return MesUsers.RIGHT_ACR_TYPE_REQTRMPRG;
    else if(short_name.equals("reqtrmprg_v2"))
      return MesUsers.RIGHT_ACR_TYPE_REQTRMPRG_V2;
    else if(short_name.equals("equippu"))
      return MesUsers.RIGHT_ACR_TYPE_EQUIPPU;
    else if(short_name.equals("equippu_v2"))
      return MesUsers.RIGHT_ACR_TYPE_EQUIPPU_V2;
    else if(short_name.equals("rfnewtype"))
      return MesUsers.RIGHT_ACR_TYPE_RFNEWTYPE;
    else if(short_name.equals("prcchng"))
      return MesUsers.RIGHT_ACR_TYPE_PRICECHANGE;
    else if(short_name.equals("prcchng_v2"))
      return MesUsers.RIGHT_ACR_TYPE_PRICECHANGE_V2;
    else if(short_name.equals("merccntctinfo"))
      return MesUsers.RIGHT_ACR_TYPE_MERCCNTCTINFO;
    else if(short_name.equals("merccntctinfo_v2"))
      return MesUsers.RIGHT_ACR_TYPE_MERCCNTCTINFO_V2;
    else if(short_name.equals("supbilconf"))
      return MesUsers.RIGHT_ACR_TYPE_SUPBILCONF;
    else if(short_name.equals("seasactvt_v2"))
      return MesUsers.RIGHT_ACR_TYPE_SEASACTVT_V2;
    else if(short_name.equals("equipdisp_v2"))
      return MesUsers.RIGHT_ACR_TYPE_EQUIPDISP_V2;
    else if(short_name.equals("othercardmgmt_v2"))
      return MesUsers.RIGHT_ACR_TYPE_OTHERCARDS_V2;
    else if(short_name.equals("newsupordfrm_v2"))
      return MesUsers.RIGHT_ACR_TYPE_SUPORDFRM_NEW_V2;
    else
      return MesUsers.RIGHT_ACR_TYPE_UNDEFINED;
  }

  // object methods

  // construction
  public ACRDefinition()
  {
    super(true);

    acrd_items=null;

    clear();
  }

  public boolean x_isAvailableByOrgAppType(int orgAppType)
  {
    log.warn("Call to obsolete method isAvailableByOrgAppType("
      + orgAppType + ")");

    return true;

    /*
    boolean rval = true;  // default

    if(this.short_name.equals("prcchng"))
      // this definition is only for CB&T
      rval=(orgAppType == 1);
    */

    /*
    else if(this.short_name.equals("merccntctinfo"))
      // this definition is for 3941 folks only
      rval=(orgAppType == 0);
    */

    //return rval;
  }

  public long x_getAssociatedUserRight()
  {
    log.warn("Call to obsolete method getAssociatedUserRight()");
    return x_getAssociatedUserRight(short_name);
  }

  public boolean equals(Object obj)
  {
    if(!(obj instanceof ACRDefinition))
      return false;

    return (((ACRDefinition)obj).getID()==this.id);
  }

  /**
   * Determines if user has permission to perform the indicated function with
   * this definition.  Replaces the old getAssociatedUserRight() scheme.  The
   * PermissionsMap is loaded with the function string contained in the
   * acrdef.rights column.
   */
  public boolean hasPermissions(UserBean user, String funcName)
  {
    boolean hasPermissions = permissions.hasPermissions(user,funcName);
    log.debug("hasPermissions(" + user.getLoginName() + ", " + funcName
      + ") = " + hasPermissions + " for " + short_name);
    return hasPermissions;
  }

  public void clear()
  {
    id=-1L;
    type="";
    name="";
    short_name="";
    description="";
    is_on=false;
    initMgmtQ = false;
    multiplicity_type = MULTTYPE_UNDEFINED;

    if(acrd_items!=null)
      acrd_items.clear();

    is_dirty=true;
  }

  // accessors
  public long     getID()               { return id; }
  public String   getType()             { return type; }
  public String   getName()             { return name; }
  public String   getShortName()        { return short_name; }
  public String   getDescription()      { return description; }

  public String   getBeanClassName()    { return beanClassName; }
  public String   getJspInclude()       { return jspInclude; }

  public boolean  isOn()                { return is_on; }
  public boolean  initMgmtQ()           { return initMgmtQ; }
  public int      getMultiplicityType() { return multiplicity_type; }

  public String   getRightStr()
  {
    if (permissions != null)
    {
      return permissions.getFunctionStr();
    }
    return null;
  }

  public boolean itemExists(String name)
  {
    if(acrd_items==null)
      return false;

    return acrd_items.containsKey(name);
  }

  public ACRDefItem getACRDefItem(String name)
 {
    if(name==null || acrd_items==null)
      return null;

    return (ACRDefItem)acrd_items.get(name);
  }

  public int getNumItems()
  {
    return acrd_items==null? 0:acrd_items.size();
  }

  public Enumeration getItemEnumeration()
  {
    if(acrd_items==null)
      acrd_items = new Hashtable();

    return acrd_items.elements();
  }

  // mutators
  public void setID(long v)
  {
    if(v==id)
      return;
    id=v;
    is_dirty=true;
  }
  public void setType(String v)
  {
    if(v==null)
      return;
    type=v;
    is_dirty=true;
  }
  public void setName(String v)
  {
    if(v==null)
      return;
    name=v;
    is_dirty=true;
  }
  public void setShortName(String v)
  {
    if(v==null)
      return;
    short_name=v;
    is_dirty=true;
  }
  public void setDescription(String v)
  {
    if(v==null)
      return;
    description=v;
    is_dirty=true;
  }

  public void setBeanClassName(String beanClassName)
  {
    this.beanClassName = beanClassName;
  }

  public void setRightStr(String functionStr)
  {
    permissions = new PermissionsMap(functionStr);
  }

  public void setJspInclude(String jspInclude)
  {
    this.jspInclude = jspInclude;
  }

  public void turnOn()
  {
    if(is_on)
      return;
    is_on=true;
    is_dirty=true;
  }
  public void turnOff()
  {
    if(!is_on)
      return;
    is_on=false;
    is_dirty=true;
  }

  public void setInitMgmtQ(boolean _initMgmtQ)
  {
    if(initMgmtQ==_initMgmtQ)
    {
      return;
    }
    initMgmtQ = _initMgmtQ;
    is_dirty=true;
  }

  public void setMutliplicityType(int v)
  {
    this.multiplicity_type = v;
  }

  public void setDefItem(ACRDefItem acrdi)
    throws Exception
  {
    if(acrdi==null)
      return;

    if(acrd_items==null)
      acrd_items = new Hashtable();

    if(acrd_items.contains(acrdi.getName()))
      throw new Exception("Unable to add ACR Def Item to ACR definition: Item '"+acrdi.getName()+"' already exists in this definition");

    acrd_items.put(acrdi.getName(),acrdi);

    acrdi.dirty();
    is_dirty=true;

    //log.debug("ACR Def Item '"+acrdi.getName()+"' added to ACR Def '"+id+"'.");
  }

  public String toString()
  {
    final String nl = System.getProperty("line.separator");

    StringBuffer sb = new StringBuffer(256);

    sb.append(nl);
    sb.append("ACR DEFINITION: ");

    sb.append("id="+id);
    sb.append(", type="+type);
    sb.append(", name="+name);
    sb.append(", short_name="+short_name);
    sb.append(", description="+description);

    sb.append(", beanClassName=" + beanClassName);
    sb.append(", rightStr=" + rightStr);
    sb.append(", jspInclude=" + jspInclude);

    sb.append(", is_on="+is_on);
    sb.append(", initMgmtQ="+initMgmtQ);

    sb.append(", multiplicity_type="+this.multiplicity_type);
    sb.append(" - "+(is_dirty? "DIRTY":"CLEAN"));
    sb.append(nl);

    if(acrd_items!=null && acrd_items.size()>0) {
      sb.append(" ITEMS:");
      ACRDefItem acrdi=null;
      for(Enumeration e=acrd_items.elements();e.hasMoreElements();) {
        sb.append(nl+"  - ");
        sb.append(((ACRDefItem)e.nextElement()).toString());
      }
    } else {
      sb.append(" *NO DEF ITEMS.*");
    }

    sb.append(", " + permissions);

    return sb.toString();
  }

} // class ACRDefinition
