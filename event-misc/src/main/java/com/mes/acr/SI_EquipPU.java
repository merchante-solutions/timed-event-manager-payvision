package com.mes.acr;

import org.apache.log4j.Logger;
import com.mes.equipment.EquipSelectorItem;
import com.mes.forms.Condition;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HtmlRenderable;
import com.mes.forms.Validation;

public class SI_EquipPU extends EquipSelectorItem
  implements ACRTranslator
{
  static Logger log = Logger.getLogger(SI_EquipPU.class);

  public SI_EquipPU()
  {
  }

  public void init()
  {
    if (type == T_STANDARD)
    {
      setColumnRenderer("col1", new HtmlRenderable()
        { public String renderHtml() { return getSerialNum(); } } );
      setColumnRenderer("col2", new HtmlRenderable()
        { public String renderHtml() { return getDescription(); } } );
      setColumnRenderer("col3", new HtmlRenderable()
        { public String renderHtml() { return getPartNum(); } } );
      setColumnRenderer("col4", new HtmlRenderable()
        { public String renderHtml() { return getDisposition(); } } );
    }
    else if (type == T_OTHER)
    {

      Field serialNum = new Field(value + "_serNum","Serial number",20,10,true);
      setColumnRenderer("col1",serialNum);
      fields.add(serialNum);

      Field description
        = new Field(value + "_description","Equipment description",60,10,true);
      setColumnRenderer("col2",description);
      fields.add(description);

      Field partNum = new Field(value + "_partNum","Part Num",20,10,true);
      setColumnRenderer("col3",partNum);
      fields.add(partNum);

      Field disposition = new DropDownField(value + "_disposition",
        "Disposition",
        TableFactory.getDropDownTable(TableFactory.DISPOSITION),true);
      setColumnRenderer("col4",disposition);
      fields.add(disposition);
    }

    //reason field is same for both
    Field reason = new Field(value + "_reason","Reason for calltag",60,20,true);
    setColumnRenderer("col5",reason);
    fields.add(reason);
  }

  public void setControlField(Field controlField)
  {
    super.setControlField(controlField);
    Condition vc = new FieldValueCondition(controlField,value);
    Validation required = new ConditionalRequiredValidation(vc);
    fields.addGroupValidation(required);
  }

  public void doPush(ACR acr)
  {
    log.debug("pretending to push acr item data...");
  }

  public void doPull(ACR acr)
  {
    try
    {
      //reason is a field, so will be saved via that mech.
      acr.setACRItem(value+"_serNum",getSerialNum());
      acr.setACRItem(value+"_description",getDescription());
      acr.setACRItem(value+"_partNum",getPartNum());
      acr.setACRItem(value+"_disposition",getDisposition());
    }
    catch(Exception e)
    {
      //do nothing
    }
  }

  public static SI_EquipPU generateOtherSelector(String prefix, int idx)
  {
    SI_EquipPU other = new SI_EquipPU();



    return other;
  }
}

