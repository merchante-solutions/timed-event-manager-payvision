package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;

public class ACRDataBean_DbaChng_MES extends ACRDataBeanBase
{
  // log4j initialization
  static Logger log = Logger.getLogger(ACRDataBean_DbaChng_MES.class);

  public ACRDataBean_DbaChng_MES()
  {
  }

  public ACRDataBean_DbaChng_MES(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr, merchantInfo, requestor, editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_RISK);
  }

  /**
   * Options for new business type drop down
   */
  public class BusinessTypeTable extends DropDownTable
  {
    public BusinessTypeTable()
    {
      addElement("New Sole Proprietor",     "New Sole Proprietor");
      addElement("New Corporation",         "New Corporation");
      addElement("New LLC",                 "New LLC");
      addElement("New Partnership",         "New Partnership");
      addElement("New Med/Legal/Corp",      "New Med/Legal/Corp");
      addElement("New Assoc/Estate/Trust",  "New Assoc/Estate/Trust");
      addElement("Tax Exempt",              "Tax Exempt");
      addElement("No Change",               "No Change");
    }
  }

  /**
   * Returns title for this acr.
   */
  public String getHeadingName()
  {
    return "Change DBA, Legal Name or Business Type Change";
  }

  /**
   * Loads current dba and legal name from database
   */
  protected void setDefaults()
  {
    String merchNum = getMerchantNumber();
    if (merchNum.length() == 0)
    {
      return;
    }

    PreparedStatement ps = null;
    ResultSet         rs = null;

    try
    {
      connect();

      String qs = " select  nvl(dba_name,'--')      dba_name,   "
                + "         nvl(fdr_corp_name,'--') legal_name  "
                + " from    mif                                 "
                + " where   merchant_number = ?                 ";
      ps = con.prepareStatement(qs);
      ps.setString(1,merchNum);

      rs = ps.executeQuery();
      String oldDba   = "--";
      String oldLegal = "--";
      if (rs.next())
      {
        oldDba    = rs.getString("dba_name");
        oldLegal  = rs.getString("legal_name");
      }
      acr.setACRItem("oldDba",oldDba);            acr.setACRItem("oldLegal",oldLegal);

    }
    catch (Exception e)
    {
      log.error(this.getClass().getName() + ".setDefaults(): " + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }


  public class IfYesAtLeastOneValidation implements Validation
  {
    String          ErrorMessage              = "Field missing value";
    Vector          CheckFields                   = new Vector();

    public IfYesAtLeastOneValidation( String msg )
    {
      ErrorMessage    = msg;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

  public void addField(Field fg)
  {
    if(fg!=null)
    {
    if(fg instanceof FieldGroup)
    {
          Iterator i = ((FieldGroup)fg).iterator();
          while(i.hasNext())
           CheckFields.add((Field)i.next());
      }
      else
      {
        CheckFields.add(fg);
      }
    }
    }

    public boolean validate(String fieldData)
    {
      boolean             retVal        = true;

      try
      {
    log.debug("field data = "+fieldData);
    //check fieldData data
    if ( fieldData != null && fieldData.toUpperCase().equals("Y") )
    {
        boolean found = false;
      for (Iterator i = CheckFields.iterator(); i.hasNext();)
      {
      Field field = (Field)i.next();
      if (field.getData().toUpperCase().equals("Y") )
      {
        log.debug("internal field data = "+field.getData());
        found = true;
        break;
      }
      }
      retVal = found;
    }
      }
      catch( Exception e )
      {
        // ingore and consider this to be failed validation
        e.printStackTrace();
      }
      return( retVal );
    }
  }

  /**
   * Creates fields for the acr detail section specific to this acr.
   */
  protected void createExtendedFields()
  {

    try
    {

      if (inStatusMode())
      {
        //after submission, when other fields aren't editable
        //we need to offer up the IRS data for internal use
        buildIRSFields();
      }
      else
      {

        // shipping address component
        add(getAddressHelper());

        //new place checkboxes to indicate systemic change
        FieldGroup places = new FieldGroup("places");

    //validations - if certain CBs are checked,
    //then at least one of the systemic places must be chosen
    IfYesAtLeastOneValidation yes1 = new IfYesAtLeastOneValidation("You must select at least one system area below for this change.");
    IfYesAtLeastOneValidation yes2 = new IfYesAtLeastOneValidation("You must select at least one system area below for this change.");

        Field cb  = new CheckboxField("p_all","<font color='red'>ALL</font>",false);
        places.add(cb);
        cb = new CheckboxField("p_mad_pa","Merchant Account Detail / Physical Address",false);
        places.add(cb);
        cb  = new CheckboxField("p_receipt","Receipt",false);
        places.add(cb);
        cb  = new CheckboxField("p_ms_ma","Merchant Statement / Mailing Address",false);
        places.add(cb);
        cb  = new CheckboxField("p_ship","Shipment Address",false);
        places.add(cb);
        cb  = new CheckboxField("p_legal","Legal Name only",false);
        places.add(cb);

        //add vals
    yes1.addField(places);
    yes2.addField(places);

        //add system group
        fields.add(places);

        AtLeastOneValidation atLeastVal = new AtLeastOneValidation("At least one option must be selected to proceed.");

        // new legal name
        cb      = new CheckboxField("changeLegal","Change Legal Name",false);
        Field newVal  = new Field("newLegal","New Legal Name",40,30,true);
        fields.add(cb);
        fields.add(newVal);
        atLeastVal.addField(cb);
        //for display
        cb.addValidation(atLeastVal);
        cb.addValidation(yes1);
        newVal.addValidation(new IfYesNotBlankValidation(cb,"Field required"));

        // new business type
        cb  = new CheckboxField("changeType","Change Business Type",false);
        newVal
            = new DropDownField("newType","New Business Type", new BusinessTypeTable(),true, "No Change");
        Field taxNo = new Field("newTax","New Tax ID",true);
        fields.add(cb);
        fields.add(newVal);
        fields.add(taxNo);
        atLeastVal.addField(cb);
        newVal.addValidation(new IfYesNotBlankValidation(cb,"Field required"));
        taxNo.addValidation(new IfYesNotBlankValidation(cb,"Field required"));

        // new dba and imprinter plate quantity
        cb      = new CheckboxField("changeDba","Change DBA",false);
        cb.addValidation(yes2);
        newVal  = new Field("newDba","New DBA",25,30,true);
        Field plates
                = new NumberField("plateQuantity","New Imprinter Plate Quantity",
                              2,5,true,0);
        fields.add(cb);
        fields.add(newVal);
        fields.add(plates);
        atLeastVal.addField(cb);
        newVal.addValidation(new IfYesNotBlankValidation(cb,"Field required"));
        plates.addValidation(new IfYesNotBlankValidation(cb,"Field required"));

/*
        //new place checkboxes to indicate systemic change
        FieldGroup places = new FieldGroup("places");

        cb  = new CheckboxField("p_all","All",false);
        cb.setHtmlExtra("class='msgErr'");
        places.add(cb);
        cb = new CheckboxField("p_mad_pa","Merchant Account Detail / Physical Address",false);
        places.add(cb);
        cb  = new CheckboxField("p_receipt","Receipt",false);
        places.add(cb);
        cb  = new CheckboxField("p_ms_ma","Merchant Statement / Mailing Address",false);
        places.add(cb);
        cb  = new CheckboxField("p_ship","Shipment Address",false);
        places.add(cb);
        cb  = new CheckboxField("p_legal","Legal Name only",false);
        places.add(cb);

        //add val
        AtLeastOneValidation selectPlaces = new AtLeastOneValidation("You must select at least one system area for name change", places);

        //for display
        places.addGroupValidation(selectPlaces);
*/

        // also make shipping address required for dba changes -- removed for PRF-945
        Field addressPicker = fields.getField("addressPicker");
        //addressPicker
        //  .addValidation(new IfYesNotBlankValidation(cb,"Field required"));

        // relabel address picker to shipping address for error display
        addressPicker.setLabel("Shipping Address");
      }

      // slight hack here...
      fields.setHtmlExtra("class=\"formText\"");

      //trying to turn this field red
      fields.getField("p_all").setHtmlExtra("class='msgErr'");

    }
    catch (Exception e)
    {
    e.printStackTrace();
      log.error("createExtendedFields(): "  + e);
    }
  }

  public void buildIRSFields()
  {
    log.debug("building IRS Fields...");
    //1099 section
    fields.add(new Field("rLegal","New Legal Name",40,30,true));
    fields.add(new Field("rFedId","New Tax ID",true));
    fields.add(new RadioButtonField("rFedType", "", idClasses, true, "Please select a Tax ID Classification"));
    fields.add(new DropDownField( "rResult","IRS Match Results", new IRSMatchTable(), true ));
    fields.add(new DropDownField( "rExempt","IRS Exemption Status", new IRSExemptionTable(), true ));
    fields.add(new RadioButtonField("rNotice", "", genericYN, 1, true, ""));

    ((RadioButtonField)fields.getField("rFedType" )).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);
    ((RadioButtonField)fields.getField("rNotice"  )).setLayoutSpec(RadioButtonField.LAYOUT_SPEC_HORIZONTAL);

    fields.setHtmlExtra("class=\"formText\"");

  }

  /**
   * Convenience methods for post submission mode
   */
  public boolean newDbaRequested()
  {
    return getACRValue("changeDba").toLowerCase().equals("y");
  }
  public boolean newLegalNameRequested()
  {
    return getACRValue("changeLegal").toLowerCase().equals("y");
  }
  public boolean newBusinessTypeRequested()
  {
    return getACRValue("changeType").toLowerCase().equals("y");
  }

  public boolean locationsListed()
  {
    if( !getACRValue("p_mad_pa").equals("")  ||
        !getACRValue("p_receipt").equals("") ||
        !getACRValue("p_ms_ma").equals("")   ||
        !getACRValue("p_ship").equals("")    ||
        !getACRValue("p_all").equals("")     ||
        !getACRValue("p_legal").equals("")
      )
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  public String getACRValue(String fName, String alt)
  {
    String data     = getACRValue(fName);

    if(fName != null && ( fName.equals("newTax") || fName.equals("newType") ))
    {
      if("".equals(data))
      {
        data =  "None";
      }
    }

    return data;
  }

  public String renderHtml(String fName)
  {
    if(fName != null && fName.equals("systemPlaces"))
    {
      //build the list from internal naming
      StringBuffer sb = new StringBuffer("<ul>");

      if(getACRValue("p_all").equalsIgnoreCase("y"))
      {
        sb.setLength(0);
        sb.append("<ul>");
        sb.append("<li>Merchant Account Detail / Physical Address</li>");
        sb.append("<li>Receipt</li>");
        sb.append("<li>Merchant Statement / Mailing Address</li>");
        sb.append("<li>Shipment Address</li>");
        sb.append("<li>Legal Name</li>");
      }
      else
        {
          if(getACRValue("p_mad_pa").equalsIgnoreCase("y") )
        {
          sb.append("<li>Merchant Account Detail / Physical Address</li>");
        }

        if(getACRValue("p_receipt").equalsIgnoreCase("y"))
        {
          sb.append("<li>Receipt</li>");
        }

        if(getACRValue("p_ms_ma").equalsIgnoreCase("y"))
        {
          sb.append("<li>Merchant Statement / Mailing Address</li>");
        }

        if(getACRValue("p_ship").equalsIgnoreCase("y"))
        {
          sb.append("<li>Shipment Address</li>");
        }

        if(getACRValue("p_legal").equalsIgnoreCase("y"))
        {
          sb.append("<li>Legal Name</li>");
        }
      }

      sb.append("</ul>");

      return sb.toString();
    }
    else
    {
      return super.renderHtml(fName);
    }
  }

  /**
   * Disable editable comments during submission.
   */
  public boolean hasEditableComments()
  {
    return( super.hasEditableComments() );
    //return !editable && super.hasEditableComments();
  }

  //both radio button and dropdown here in case we flip-flop
  private String[][] idClasses =
  {
    {"Unknown" ,"0"},
    {"EIN"     ,"1"},
    {"SSN"     ,"2"}
  };

  protected class TaxTypeTable extends DropDownTable
  {
    public TaxTypeTable()
    {
      addElement("Unknown", "Unknown");
      addElement("EIN", "EIN");
      addElement("SSN", "SSN");
    }
  }

  protected class IRSExemptionTable extends DropDownTable
  {
    public IRSExemptionTable()
    {
      addElement("", "");
      addElement("Reporting", "Reporting");
      addElement("1099 and Reporting", "1099 and Reporting");
      addElement("No Exemptions", "No Exemptions");
    }
  }

  protected class IRSMatchTable extends DropDownTable
  {
    public IRSMatchTable()
    {
        addElement("",  "--");
        addElement("0-TIN and Name combination matches IRS",  "0-TIN and Name combination matches IRS");
        //addElement("1",  "1-TIN was missing or TIN not 9-digit number");
        //addElement("2",  "2-TIN entered is not currently issued");
        //addElement("3",  "3-TIN and Name combination does not match IRS");
        //addElement("4",  "4-Invalid TIN Matching request");
        //addElement("5",  "5-Duplicate TIN Matching request");
        addElement("6-TIN & Name combination matches IRS SSN",  "6-TIN & Name combination matches IRS SSN");
        addElement("7-TIN & Name combination matches IRS EIN",  "7-TIN & Name combination matches IRS EIN");
        addElement("88-TIN & Name combination matches IRS SSN & EIN",  "8-TIN & Name combination matches IRS SSN & EIN");
    }
  }

  private String[][] genericYN =
  {
    {"Yes"   ,"Y"},
    {"No"    ,"N"}
  };


}
