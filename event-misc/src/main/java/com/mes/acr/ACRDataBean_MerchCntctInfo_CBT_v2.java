package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.PhoneField;

public class ACRDataBean_MerchCntctInfo_CBT_v2 extends ACRDataBeanBase
{
  // log4j initialization
  static Logger log =
    Logger.getLogger(ACRDataBean_MerchCntctInfo_CBT_v2.class);

  public ACRDataBean_MerchCntctInfo_CBT_v2()
  {
  }

  public ACRDataBean_MerchCntctInfo_CBT_v2(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  /**
   * Loads current mif contact info data into acr existing fields.
   */
  protected void setDefaults()
  {
    String merchNum = getMerchantNumber();
    if(acr==null || merchNum.length()<1)
      return;

    PreparedStatement ps = null;
    ResultSet         rs = null;

    try
    {
      connect();
/*
//OLD
      String qs = " select  nvl(phone_1,'---')            phone,  "
                + "         nvl(phone_2_fax,'---')        fax,    "
                + "         nvl(email_addr_ind_00,'null') ind0,   "
                + "         nvl(email_addr_00,'---')      val0,   "
                + "         nvl(email_addr_01,'---')      val1,   "
                + "         nvl(other_name,'---')         contact "
                + "         nvl(to_char(mc.merchcont_prim_phone),'---') cphone "
                + " from    mif                                   "
                + " where   merchant_number = ?                   ";
*/
      String qs = "select   nvl(m.phone_1,'---')                    phone,    "
                + "         nvl(m.phone_2_fax,'---')                fax,      "
                + "         nvl(m.email_addr_ind_00,'null')         ind0,     "
                + "         nvl(m.email_addr_00,'---')              val0,     "
                + "         nvl(m.email_addr_01,'---')              val1,     "
                + "         nvl(m.other_name,nvl(mc.merchcont_prim_first_name "
                + "          ||' '||mc.merchcont_prim_last_name,'---'))       "
                + "                                                 contact,  "
                + "        nvl(to_char(mc.merchcont_prim_phone),'---') cphone "
                + "from    mif m, merchcontact mc, merchant mer               "
                + "where   m.merchant_number = ?                              "
                + "and     m.merchant_number = mer.MERCH_NUMBER(+)            "
                + "and     mer.app_seq_num = mc.app_seq_num(+)                ";


      ps = con.prepareStatement(qs);
      ps.setString(1,merchNum);

      rs = ps.executeQuery();
      if (rs.next())
      {
        acr.setACRItem("existing_phone",
                       PhoneField.format(rs.getString("phone")));
        acr.setACRItem("existing_fax",
                       PhoneField.format(rs.getString("fax")));
        acr.setACRItem("existing_cphone",
                       PhoneField.format(rs.getString("cphone")));
        acr.setACRItem("existing_contact",
                       rs.getString("contact"));

        //00 = E-MAIL; 01 = URL
        String email,url;

        if(rs.getString("ind0").equals("00"))
        {
          email = rs.getString("val0");
          url = rs.getString("val1");
        }
        else
        {
          email = rs.getString("val1");
          url = rs.getString("val0");
        }

        acr.setACRItem("existing_email", email);
        acr.setACRItem("existing_web", url);

      }
    }
    catch (Exception e)
    {
      log.error("setDefaults() exception: " + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    try
    {

      Field field=null;

      Vector v = new Vector(3,1);

      field = new PhoneField("business_phone","Business Phone",true);
      v.add(field);
      fields.add(field);

      field = new PhoneField("fax_number","Fax Number",true);
      v.add(field);
      fields.add(field);

      field = new EmailField("email_address","E-mail Address",50,40,true);
      v.add(field);
      fields.add(field);

      field = new Field("contact_name","Contact Name",25,40,true);
      v.add(field);
      fields.add(field);

      field = new PhoneField("contact_phone","Contact Phone",true);
      v.add(field);
      fields.add(field);

      field = new Field("web_info","Web Addess",25,50,true);
      v.add(field);
      fields.add(field);

      // add "at least one" validation
      fields.addValidation(new FieldBean.AtLeastOneValidation("At least one Merchant Contact field must be specified.",v));
    }
    catch (Exception e)
    {
      log.error("Error in createExtendedFields(): " + e);
    }
  }

  /**
   * Overrides base class behavior to not allow comments to be edited during
   * initial acr creation (assumed to be indicated by editable == true).
   */
  public boolean hasEditableComments()
  {
    return !editable && super.hasEditableComments();
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
      { MesQueues.Q_NONE, MesQueues.Q_CHANGE_REQUEST_CBT_TSYS }

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}

      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS, MesQueues.Q_CHANGE_REQUEST_CBT_MMS }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS, MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS, MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS, MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS, MesQueues.Q_CHANGE_REQUEST_COMPLETED }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_TSYS, MesQueues.Q_CHANGE_REQUEST_CANCELLED }

      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH, MesQueues.Q_CHANGE_REQUEST_CBT_TSYS }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH, MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH, MesQueues.Q_CHANGE_REQUEST_COMPLETED }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH, MesQueues.Q_CHANGE_REQUEST_CANCELLED }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH, MesQueues.Q_CHANGE_REQUEST_CBT_MMS }

      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_CBT_TSYS }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_CBT_MMS }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_COMPLETED }
      ,{ MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_CANCELLED }

      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_MMS, MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_MMS, MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_MMS, MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_MMS, MesQueues.Q_CHANGE_REQUEST_COMPLETED }
      ,{ MesQueues.Q_CHANGE_REQUEST_CBT_MMS, MesQueues.Q_CHANGE_REQUEST_CANCELLED }

      ,{ MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS, MesQueues.Q_CHANGE_REQUEST_COMPLETED }
      ,{ MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS, MesQueues.Q_CHANGE_REQUEST_CANCELLED }
      ,{ MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS, MesQueues.Q_CHANGE_REQUEST_CBT_MMS }
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW}

      ,{ MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT, MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA }
      ,{ MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT, MesQueues.Q_CHANGE_REQUEST_COMPLETED }
      ,{ MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT, MesQueues.Q_CHANGE_REQUEST_CANCELLED }
      ,{ MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT, MesQueues.Q_CHANGE_REQUEST_CBT_MMS }

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOY_REVIEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}


      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}

//ASG Mgmt queue
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ASG_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_ASG_MGMT}
    };
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_RISK:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  public String getHeadingName()
  {
    return "Merchant Contact Information Change";
  }

} // class ACRDataBean_MerchCntctInfo_CBT_v2
