package com.mes.acr;

// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.PhoneField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TextareaField;
import com.mes.tools.DropDownTable;


public final class ACRDataBean_AdrsChng_CBT extends ACRDataBean_AdrsChng
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_AdrsChng_CBT.class);


  // construction
  public ACRDataBean_AdrsChng_CBT()
  {
  }

  public ACRDataBean_AdrsChng_CBT(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  protected void setDefaults()
  {
    // no-op
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;

    DropDownTable atddt = new AddressTypeDropDownTable();
    StateDropDownTable        stateDrpDwn     = new StateDropDownTable();

    // old address type
    field = new DropDownField("adrsChngType",atddt,false);
    field.setShowName("Address Change Type");
    fields.add(field);

    // old address fields
    field = new TextareaField("oldAdrs",100,2,40,false);
    field.setShowName("old Address");
    fields.add(field);
    field = new Field("oldCity",50,50,false);
    field.setShowName("old City");
    fields.add(field);
    field = new DropDownField("oldState",stateDrpDwn,false);
    field.setShowName("old State");
    fields.add(field);
    field = new Field("oldZip",20,15,true);
    field.setShowName("old Zip");
    fields.add(field);
    field = new PhoneField("oldPhone",true);
    field.setShowName("old Phone");
    fields.add(field);
    field = new PhoneField("oldFax",true);
    field.setShowName("old FAX");
    fields.add(field);

    // new address
    field = new TextareaField("newAdrs",100,2,40,false);
    field.setShowName("new Address");
    fields.add(field);
    field = new Field("newCity",50,50,false);
    field.setShowName("new City");
    fields.add(field);
    field = new DropDownField("newState",stateDrpDwn,false);
    field.setShowName("new State");
    fields.add(field);
    field = new Field("newZip",20,15,true);
    field.setShowName("new Zip");
    fields.add(field);
    field = new PhoneField("newPhone",true);
    field.setShowName("new Phone");
    fields.add(field);
    field = new PhoneField("newFax",true);
    field.setShowName("new Fax");
    fields.add(field);
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_CBT_NEW}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_RISK}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_COMPLETED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_RISK}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  public String getHeadingName()
  {
    return "Change CB&T Merchant Business and/or Mailing Address";
  }

  public String getJSPIncludeFilename()
  {
    return (acr==null)? "":"acr_incl_"+acr.getDefinition().getShortName()+"_cbt.jsp";
  }

  public class AddressTypeDropDownTable extends DropDownTable
  {
    public AddressTypeDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      addElement("","");
      addElement("(00) Physical Address","(00) Physical Address");
      addElement("(01) Mailing Address","(01) Mailing Address");
      addElement("Both Physical/Mailing","Both Physical/Mailing");
    }

  }

} // class ACRDataBean_AdrsChng_CBT
