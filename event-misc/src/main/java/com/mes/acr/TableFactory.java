package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import com.mes.tools.DropDownTable;

public class TableFactory
{

  static Logger log = Logger.getLogger(TableFactory.class);

  public static final int EQUIP_TYPE  = 1;
  public static final int DISPOSITION = 2;
  public static final int TERM_APP = 3;
  public static final int APP_TYPE = 4;
  public static final int VENDOR = 5;
  public static final int EQUIP_DESC = 6;
  public static final int EQUIP_STATUS = 7;
  public static final int MONTH = 8;
  public static final int CHARGE = 9;



  public static DropDownTable getDropDownTable(int type)
  {

    DropDownTable table;

    switch(type)
    {
      case EQUIP_TYPE:
        table = new _ddTypeTable();
        break;

      case DISPOSITION:
        table = new _ddDispTable();
        break;

      case TERM_APP:
        table = new _ddTermAppTable();
        break;

      case APP_TYPE:
        table = new _ddAppTypeTable();
        break;

      case VENDOR:
        table = new _ddVendorTable();
        break;

      case EQUIP_DESC:
        table = new _ddDescriptionTable();
        break;

      case EQUIP_STATUS:
        table = new _ddStatusTable();
        break;

      case MONTH:
        table = new _ddMonthDropDownTable();
        break;

      case CHARGE:
        table = new _chargeTable();
        break;

      default:
        throw new RuntimeException("DropDownTable type: "+type+" is not supported.");
    }

    return table;
  }

  private static final class _ddTypeTable extends DropDownTable
  {

    public _ddTypeTable()
    {

      PreparedStatement ps    = null;
      ResultSet         rs    = null;

      addElement("","");

      try
      {
        connect();

        String qs =
          "select    equiptype_description "+
          "from      equiptype "+
          "order by  equiptype_description asc";

        ps = con.prepareStatement(qs);
        rs = ps.executeQuery();

        while(rs.next())
        {
          String desc = rs.getString(1);
          addElement(desc, desc);
        }

      }
      catch(Exception e)
      {
        log.error("_ddTypeTable: " + e);
        throw new RuntimeException(" _ddTypeTable() error: "
          + e);
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { ps.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }

  private static final class _ddDispTable extends DropDownTable
  {

    public _ddDispTable()
    {

      PreparedStatement ps    = null;
      ResultSet         rs    = null;

      addElement("","");

      try
      {
        connect();

        String qs =
        "select equip_status_desc "+
        "from equip_status "+
        "order by equip_status_order asc, equip_status_id asc";

        ps = con.prepareStatement(qs);
        rs = ps.executeQuery();

        String desc;

        while(rs.next())
        {
          desc = rs.getString(1);
          addElement(desc, desc);
        }
      }
      catch(Exception e)
      {
        log.error("_ddDispTable: " + e);
        throw new RuntimeException(" _ddDispTable() error: "
          + e);
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { ps.close(); } catch (Exception e) {}
        cleanUp();
      }
    }
  }

  private static class _ddTermAppTable extends DropDownTable
  {
    public _ddTermAppTable()
    {
      addElement("","Select One");

      addElement("Retail","Retail");
      addElement("Restaurant","Restaurant");
      addElement("Fine Dining","Fine Dining");
      addElement("Lodging","Lodging");
      addElement("Dial Pay","Dial Pay");
      addElement("MOTO","MOTO");
      addElement("POS Partner","POS Partner");
      addElement("Stage Only","Stage Only");
      addElement("Cash Advance","Cash Advance");
      addElement("SPS Gift Card","SPS Gift Card");
      addElement("256K","256K");
      addElement("SPOS","SPOS");
    }

  } // _ddTermAppTable class

  private static class _ddAppTypeTable extends DropDownTable
  {
    public _ddAppTypeTable()
    {
      addElement("DHTL","DHTL");
      addElement("DIALPAY","DIALPAY");
      addElement("DICERR","DICERR");
      addElement("DLIPRST","DLIPRST");
      addElement("DLIPRTL","DLIPRTL");
      addElement("DRR","DRR");
      addElement("DRR-MM","DRR-MM");
      addElement("DRR512K","DRR512K");
      addElement("FD","FD");
      addElement("HTL","HTL");
      addElement("ICERR","ICERR");
      addElement("LIPRST","LIPRST");
      addElement("LIPRTL","LIPRTL");
      addElement("RR","RR");
      addElement("RR-MM","RR-MM");
      addElement("RR512K","RR512K");
      addElement("SPS GIFTCARD", "SPS GIFTCARD");
      addElement("STAGE","STAGE");
      addElement("TCDP04","TCDP04");
      addElement("TLT002","TLT002");
      addElement("TLT004","TLT004");
      addElement("TM","TM");
      addElement("TVTDTC02","TVTDTC02");
      addElement("UEVA120","UEVA120");
      addElement("UEVA231","UEVA231");
      addElement("UEVA231R","UEVA231R");
      addElement("UEVS120","UEVS120");
      addElement("VC","VC");
      addElement("VSTDB01","VSTDB01");
      addElement("VSVR035","VSVR035");
      addElement("VSVR045","VSVR045");
      addElement("VSVR046","VSVR046");
      addElement("VSVR45U","VSVR45U");
      addElement("VSVR46X","VSVR46X");
      addElement("VT13DA7","VT13DA7");
      addElement("VT13DA8","VT13DA8");
      addElement("VT1CD02","VT1CD02");
      addElement("VT1DB01","VT1DB01");
      addElement("VT1H385","VT1H385");
      addElement("VT1TP08","VT1TP08");
      addElement("VT1TPA6","VT1TPA6");
      addElement("VT1TPA7","VT1TPA7");
      addElement("VT1TPH6","VT1TPH6");
      addElement("VXL0P32","VXL0P32");
      addElement("VXP0P16","VXP0P16");
    }
  }

  private static class _ddVendorTable extends DropDownTable
  {
    public _ddVendorTable()
    {
      addElement("","Select one");
      addElement("Vital","Vital");
      addElement("VeriCentre","VeriCentre");
      addElement("TermMaster","TermMaster");
    }

  } // _ddVendorTable class

  private static class _ddDescriptionTable extends DropDownTable
  {
    public _ddDescriptionTable()
    {
      getData();
    }

    private void getData()
    {
      addElement("","");

      PreparedStatement   ps      = null;
      ResultSet           rs      = null;

      try {

        connect();
/*
        int appTypeCode,cnt;

        appTypeCode=StringUtilities.stringToInt(getACRValue("Type Code"),-1);

        // revert to mes equipment if merchant app type renders no allowed equip
        if(appTypeCode>-1) {
          #sql [Ctx]
          {
            select count(*)
            into :cnt
            from equipment_application
            where app_type=:(appTypeCode)
          };
        } else
          cnt=0;

        if(cnt<1) {
          log.info("Reverting to MES equipment: No equipment available for app type code '"+appTypeCode+"'.");
          appTypeCode=0;
        }
*/

        //Only using mes defaults for now (app_type = 0)
        String qs =
        " select    equip_description               " +
        " from      equipment_application ea        " +
        " where     ea.app_type=0                   " +//:appTypeCode
        " order by  equip_description               ";

        ps = con.prepareStatement(qs);
        rs = ps.executeQuery();

        String val;

        while (rs.next()) {
          val = rs.getString("equip_description");
          addElement(val,val);
        }
      }
      catch (Exception e) {
        log.error("_ddDescriptionTable.getData() EXCEPTION: '"+e.getMessage()+"'.");
      }
      finally {
        try { rs.close(); } catch (Exception e) {}
        try { ps.close(); } catch (Exception e) {}
        cleanUp();
      }

    }

  } // _ddDescriptionTable class


  private static class _ddStatusTable extends DropDownTable
  {
    public _ddStatusTable()
    {
      addElement("","");

      addElement("RENT","Rental");
      addElement("SOLD","Purchase");
      addElement("Lease","Lease");
      addElement("MOE","MerchOwn");
      addElement("Unknown","Unknown");
    }

  } // _ddTermAppTable class

  /*
  public class LeaseMonthsDropDownTable extends DropDownTable
  {
    public LeaseMonthsDropDownTable()
    {
      addElement("","");

      addElement("12 Months","12 Months");
      addElement("24 Months","24 Months");
      addElement("36 Months","36 Months");
      addElement("48 Months","48 Months");
    }

  } // LeaseMonthsDropDownTable class


  public class LeaseCompanyDropDownTable extends DropDownTable
  {
    public LeaseCompanyDropDownTable()
    {
      addElement("","");

      addElement("Lease Comm","Lease Comm");
      addElement("Golden Eagle Leasing","Golden Eagle Leasing");
    }

  } // LeaseCompanyDropDownTable class

  */

  private static class _ddMonthDropDownTable extends DropDownTable
  {

    public _ddMonthDropDownTable()
    {
      getData();
    }

    private void getData()
    {
      addElement("","  --  ");

      addElement("jan","JAN");
      addElement("feb","FEB");
      addElement("mar","MAR");
      addElement("apr","APR");
      addElement("may","MAY");
      addElement("jun","JUN");
      addElement("jul","JUL");
      addElement("aug","AUG");
      addElement("sep","SEP");
      addElement("oct","OCT");
      addElement("nov","NOV");
      addElement("dec","DEC");

    }

  }

  private static class _chargeTable extends DropDownTable
  {

    public _chargeTable()
    {
      addElement("","");
      addElement("Merchant","Merchant");
      addElement("Bank","Bank");
    }

  }

}