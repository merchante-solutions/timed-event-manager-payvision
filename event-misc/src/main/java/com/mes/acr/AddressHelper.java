package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import org.apache.log4j.Logger;
import com.mes.forms.CityStateZipField;
import com.mes.forms.Condition;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HtmlRenderable;
import com.mes.forms.HtmlTable;
import com.mes.forms.HtmlTableCell;
import com.mes.forms.HtmlTableRow;
import com.mes.forms.RadioField;
import com.mes.forms.Selector;
import com.mes.forms.Validation;

/**
 * AddressHelper class
 *
 * A field bean that loads addresses associated with a merchant account.
 * Intended to be used as a sub-bean helper in acr beans.
 */
public class AddressHelper extends FieldBean implements ACRTranslator
{
  static Logger log =
    Logger.getLogger(AddressHelper.class);

  public static final int FT_NONE           = 0;
  public static final int FT_CBT_SHIPPING   = 1;

  private Hashtable addresses         = new Hashtable();
  private String    acrItemPrefix     = null;
  private Hashtable filteredAddresses = null;
  private int filterType              = FT_NONE;
  private boolean noAddrOption        = false;

  public AddressHelper(String merchNum, String acrItemPrefix, int filterType)
  {
    this(merchNum,acrItemPrefix,filterType,true);
  }

  public AddressHelper(String merchNum, String acrItemPrefix, int filterType, boolean noAddrOption)
  {
    if (filterType != FT_NONE && filterType != FT_CBT_SHIPPING)
    {
      throw new RuntimeException("Invalid filter type: " + filterType);
    }

    this.filterType = filterType;
    this.noAddrOption = noAddrOption;

    loadAddressData(merchNum,filterType);
    createFields();

    this.acrItemPrefix = acrItemPrefix;
  }

  //need to be able to return this to a pre-selection mode
  public void reset(ACR acr)
  {
    log.debug("Resetting AddressHelper");

    //remove these items, in the event they've been assigned (doPull)
    acr.removeItem("addressPicker");
    acr.removeItem(acrItemPrefix + "AddrName");
    acr.removeItem(acrItemPrefix + "AddrLine1");
    acr.removeItem(acrItemPrefix + "AddrLine2");
    acr.removeItem(acrItemPrefix + "AddrCsz");

    log.debug("Resetting AddressHelper - DONE");
  }

  /**
   * Internal address bean class used to hold address info loaded from the db.
   */
  public class Address
  {
    private String code;
    private String label;
    private String name;
    private String line1;
    private String line2;
    private String city;
    private String state;
    private String zip;

    public void setCode   (String code)   { this.code   = code;   }
    public void setLabel  (String label)  { this.label  = label;  }
    public void setName   (String name)   { this.name   = name;   }
    public void setLine1  (String line1)  { this.line1  = line1;  }
    public void setLine2  (String line2)  { this.line2  = line2;  }
    public void setCity   (String city)   { this.city   = city;   }
    public void setState  (String state)  { this.state  = state;  }
    public void setZip    (String zip)    { this.zip    = zip;    }

    public String getCode()   { return code;  }
    public String getLabel()  { return label; }
    public String getName()   { return name;  }
    public String getLine1()  { return line1; }
    public String getLine2()  { return line2; }
    public String getCity()   { return city;  }
    public String getState()  { return state; }
    public String getZip()    { return zip;   }

    public String getCsz()
    {
      return city + ", " + state + " " + zip;
    }

    public String toString()
    {
      return code + "\n" + label + "\n" + name + "\n" + line1 + "\n"
        + (line2 != null ? line2 + "\n" : "") + city + ", " + state
        + " " + zip;
    }

    public String renderHtml()
    {
      return label + "<br>" + name + "<br>" + line1 + "<br>"
        + (line2 != null ? line2 + "<br>" : "") + city + ", " + state
        + " " + zip;
    }
  }

  /**
   * Loads all available addresses into internal storage (Address object
   * defined above).  Address sources are mif table and the OLA generated
   * address table records.  A filter type is specified, currently
   * this is primarily used to filter out most addresses (except dba/mif00 and
   * mif02) for CB&T shipping.
   */
  private void loadAddressData(String merchNum, int filterType)
  {
    PreparedStatement ps = null;
    ResultSet         rs = null;

    try
    {
      connect();

      /*
      // these two mif addrs are always allowed
      String mif01Flag = "true";
      String mif02Flag = "true";

      // if cb&t shipping filter out any other addresses
      String mif00Flag = (filterType != FT_CBT_SHIPPING ? "true" : "false");
      String addrsFlag = (filterType != FT_CBT_SHIPPING ? "true" : "false");
      */

      String qs
        = "select 'mif00'                        code,          " // mif 00
        + "       'DBA Address'                  label,         "
        + "       dba_name                       name,          "
        + "       dmaddr                         line1,         "
        + "       address_line_3                 line2,         "
        + "       dmcity                         city,          "
        + "       dmstate                        state,         "
        + "       dmzip                          zip            "
        + "from   mif                                           "
        + "where  merchant_number = ?                           " // var 1
        + "union                                                "
        + "select 'mif01'                        code,          " // mif 01
        + "       'MIF-01'                       label,         "
        + "       name1_line_1                   name,          "
        + "       addr1_line_1                   line1,         "
        + "       addr1_line_2                   line2,         "
        + "       city1_line_4                   city,          "
        + "       state1_line_4                  state,         "
        + "       zip1_line_4                    zip            "
        + "from   mif                                           "
        + "where  merchant_number = ?                           " // var 2
        + "union                                                "
        + "select 'mif02'                        code,          " // mif 02
        + "       'MIF-02'                       label,         "
        + "       name2_line_1                   name,          "
        + "       addr2_line_1                   line1,         "
        + "       addr2_line_2                   line2,         "
        + "       city2_line_4                   city,          "
        + "       state2_line_4                  state,         "
        + "       zip2_line_4                    zip            "
        + "from   mif                                           "
        + "where  merchant_number = ?                           " // var 3
        + "union                                                "
        + "select 'addr' || a.addresstype_code   code,          " // non-bank
        + "       at.addresstype_desc            label,         "
        + "       nvl(a.address_name,                           "
        + "           mif.dba_name)              name,          "
        + "       a.address_line1                line1,         "
        + "       a.address_line2                line2,         "
        + "       a.address_city                 city,          "
        + "       a.countrystate_code            state,         "
        + "       a.address_zip                  zip            "
        + "from   address       a,                              "
        + "       addresstype   at,                             "
        + "       merchant      m,                              "
        + "       mif                                           "
        + "where  mif.merchant_number = ?                       " // var 4
        + "       and mif.merchant_number = m.merch_number      "
        + "       and m.app_seq_num = a.app_seq_num             "
        + "       and a.addresstype_code = at.addresstype_code  "
        + "       and a.addresstype_code <> 9                   "
        + "union                                                "
        + "select 'addr' || a.addresstype_code   code,          " // bank
        + "       at.addresstype_desc            label,         "
        + "       nvl(mb.merchbank_name,'(none)') name,         "
        + "       nvl(a.address_line1,'(none)')  line1,         "
        + "       a.address_line2                line2,         "
        + "       a.address_city                 city,          "
        + "       a.countrystate_code            state,         "
        + "       a.address_zip                  zip            "
        + "from   address       a,                              "
        + "       merchbank     mb,                             "
        + "       addresstype   at,                             "
        + "       merchant      m,                              "
        + "       mif                                           "
        + "where  mif.merchant_number = ?                       " // var 5
        + "       and mif.merchant_number = m.merch_number      "
        + "       and m.app_seq_num = a.app_seq_num             "
        + "       and m.app_seq_num = mb.app_seq_num(+)         "
        + "       and a.addresstype_code = at.addresstype_code  "
        + "       and a.addresstype_code = 9                    "
        + "order by code                                        ";

      ps = con.prepareStatement(qs);
      ps.setString(1,merchNum);
      ps.setString(2,merchNum);
      ps.setString(3,merchNum);
      ps.setString(4,merchNum);
      ps.setString(5,merchNum);

      rs = ps.executeQuery();

      while (rs.next())
      {
        Address address = new Address();

        address.setCode (rs.getString("code"));
        address.setName (rs.getString("name"));
        address.setLine1(rs.getString("line1"));
        address.setLine2(rs.getString("line2"));
        address.setCity (rs.getString("city"));
        address.setState(rs.getString("state"));
        address.setZip  (rs.getString("zip"));

        /* HACK: set certain labels to CB&T variations

        3858: 00=Mailing,  01=Physical, 02=Shipping
        3941: 00=Physical, 01=Mailing,  02=Shipping
        */
        if (address.getCode().equals("mif00"))
        {
          if(filterType == FT_CBT_SHIPPING)
          {
            address.setLabel("Mailing Address");
          }
          else
          {
            address.setLabel("Physical Address");
          }
        }
        else if (address.getCode().equals("mif01"))
        {
          if(filterType == FT_CBT_SHIPPING)
          {
            address.setLabel("Physical Address");
          }
          else
          {
            address.setLabel("Mailing Address");
          }
        }
        else if (address.getCode().equals("mif02"))
        {
          address.setLabel("Shipping Address");
        }
        else
        {
          address.setLabel(rs.getString("label"));
        }

        log.debug("address.getCode() = "+ address.getCode());

        addresses.put(address.getCode(),address);
      }

    }
    catch (Exception e)
    {
      log.error("loadAddressData(): " + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  /**
   * Given an array of fields this will generate a renderable html object
   * containing the field labels and html form controls in the form of a
   * two column table.  Used to generate the "other" address radio button
   * field element label (see createFields() below).
   */
  private HtmlRenderable getOtherLabel(Field[] addressFields)
  {
      // create a table with a header row
      HtmlTable t = new HtmlTable();
      t.setProperty("cellpadding","0");
      t.setProperty("cellspacing","1");
      HtmlTableRow r = t.add(new HtmlTableRow());
      HtmlTableCell c = r.add(new HtmlTableCell());
      c.setProperty("colspan","2");
      c.setProperty("class","formText"); // HACK
      c.add("Other Address");

      // add the field control rows
      for (int i = 0; i < addressFields.length; ++i)
      {
        r = t.add(new HtmlTableRow());
        c = r.add(new HtmlTableCell());
        c.setProperty("class","formText"); // HACK
        c.add(addressFields[i].getLabel());
        c = r.add(new HtmlTableCell());
        c.add(addressFields[i]);
      }

      return t;
  }

  public void makeRequired()
  {
    try{
      getField("addressPicker").makeRequired();
    }
    catch(Exception e)
    {
      //not going to do anything about this yet
    }
  }

  /**
   * Validator class to make address picker conditionally required if an
   * associated selector has any selections.  i.e. an equipment selector
   * will require a shipping address when equipment has been selected.
   */
  public class SelectorValidation implements Validation
  {
    private Selector selector;

    public SelectorValidation(Selector selector)
    {
      this.selector = selector;
    }

    public boolean validate(String fieldData)
    {
      if (!selector.getSelected().isEmpty() &&
          (fieldData == null || fieldData.length() == 0))
      {
        return false;
      }
      return true;
    }

    public String getErrorText()
    {
      return "Field required";
    }
  }

  /**
   * Make address required if the given selector has anything selected,
   * useful for making shipping address required for shipping equipment.
   */
  public void requireIfSelected(Selector selector)
  {
    try
    {
      getField("addressPicker").addValidation(new SelectorValidation(selector));
    }
    catch (Exception e)
    {
    }
  }

  /**
   * Creates fields to allow selection of an address.  The fields consist of
   * a radio button field with an element for each known address as well as
   * an "other" option to allow user to specify another unknown address.
   * Fields are created to allow entry of the other address.  The "other"
   * fields are loaded into an html renderable object and placed in the
   * label of the "other" radio button element.
   */
  private void createFields()
  {
    try
    {
      // create the address picker radio field
      RadioField addressPicker = new RadioField("addressPicker","Address");
      addressPicker.setRenderCols(3);
      fields.add(addressPicker);

      // none option
      if(noAddrOption)
      {
        addressPicker.addButton("","Not Applicable");
      }

      //lazy inst
      if(filteredAddresses == null)
      {
        filteredAddresses = getFilteredAddressHash(filterType);
      }

      // add the known static addresses
      for (Iterator i = filteredAddresses.keySet().iterator(); i.hasNext();)
      {
        String code = (String)i.next();
        Address address = (Address)addresses.get(code);
        addressPicker.addButton(code,address.renderHtml());
      }

      // create some fields for the "other" address option, add them
      Field otherName   = new Field("otherName","Name",30,25,true);
      Field otherLine1  = new Field("otherLine1","Line 1",30,25,true);
      Field otherLine2  = new Field("otherLine2","Line 2",30,25,true);
      Field otherZip    = new CityStateZipField("otherCsz",
                                                "City ST Zip",
                                                25,true);
      fields.add(otherName);
      fields.add(otherLine1);
      fields.add(otherLine2);
      fields.add(otherZip);

      // make the other address fields optionally required if the user
      // selects the other option in the address picker
      Condition isOther = new FieldValueCondition(addressPicker,"other");
      Validation isRequired = new ConditionalRequiredValidation(isOther);
      otherName.addValidation(isRequired,"otherVal1");
      otherLine1.addValidation(isRequired,"otherVal2");
      otherZip.addValidation(isRequired,"otherVal3");

      // make an array of the fields for the other radio button label creation
      Field[] otherFields =
        new Field[] { otherName, otherLine1, otherLine2, otherZip };

      // set the "other" radio element label to be a dynamic html renderable
      // object that contains the other fields
      addressPicker.addButton("other",getOtherLabel(otherFields));
    }
    catch (Exception e)
    {
      log.error("createFields(): " + e);
    }
  }

  /**
   * Responsible for deriving the actual selected address from the static
   * and field data and moving it into the selected address acr item storage.
   * The selected address is stored in the "selectedAddr*" acr items.
   */
  public void doPull(ACR acr)
  {
    log.debug("...Calling AddressHelper.doPull()...");

    // determine which address has been selected (if any)
    String addressType = fields.getData("addressPicker");

    String selAddrName  = null;
    String selAddrLine1 = null;
    String selAddrLine2 = null;
    String selAddrCsz   = null;

    // pull data from other fields
    if(addressType != null)
    {
      if (addressType.equals("other"))
      {
        acr.setACRItem(acrItemPrefix + "AddrName",  fields.getData("otherName"));
        acr.setACRItem(acrItemPrefix + "AddrLine1", fields.getData("otherLine1"));
        acr.setACRItem(acrItemPrefix + "AddrLine2", fields.getData("otherLine2"));
        acr.setACRItem(acrItemPrefix + "AddrCsz",   fields.getData("otherCsz"));

        // remove the redundant acr item data that was created by the
        // base bean field translation
        acr.removeItem("otherName");
        acr.removeItem("otherLine1");
        acr.removeItem("otherLine2");
        acr.removeItem("otherCszCity");
        acr.removeItem("otherCszState");
        acr.removeItem("otherCszZip");
      }
      // pull data from static address storage
      else if (addressType.length() > 0)
      {
        Address address = (Address)addresses.get(addressType);
        acr.setACRItem(acrItemPrefix + "AddrName",  address.getName());
        acr.setACRItem(acrItemPrefix + "AddrLine1", address.getLine1());
        acr.setACRItem(acrItemPrefix + "AddrLine2", address.getLine2()==null?"":address.getLine2());
        acr.setACRItem(acrItemPrefix + "AddrCsz",   address.getCsz());
      }
    }

    log.debug("END...Calling AddressHelper.doPull()...");
  }

  public void doPush(ACR acr)
  {
    // does nothing now
  }

  public Address getAddress(String code)
  {
    return (Address)addresses.get(code);
  }

  /**
   * Filter mechanism to allow for the returning of different types
   * of address lists, based on different types of filters
   */
  private Hashtable getFilteredAddressHash(int filterType)
  {

    Hashtable temp = new Hashtable();
    ArrayList keys = null;

    switch(filterType)
    {

      case FT_CBT_SHIPPING:
        keys = new ArrayList();
        keys.add("mif01");
        //keys.add("mif02");
        keys.add("addr4");
        break;

      default:
        //do nothing
        //leave keys null
    }

    if(keys==null)
    {
      //return all of them
      temp = addresses;
    }
    else
    {
      //only return the filtered ones
      String key;
      for(int i=0; i<keys.size();i++)
      {
        key = (String)keys.get(i);
        //need this since CB&T merchants filtered might not have
        //addresses of filter type
        if(addresses.get(key)!=null)
        {
          temp.put(key,(Address)addresses.get(key));
        }
      }
    }

    return temp;

  }

}
