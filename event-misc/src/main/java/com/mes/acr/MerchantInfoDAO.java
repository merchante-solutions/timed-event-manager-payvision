package com.mes.acr;

/**
 * MerchantInfoDAO interface
 * 
 * DAO definition for MerchantInfo class which is READ ONLY
 */
public interface MerchantInfoDAO
{
  // constants
  // (none)
  
  // methods
  
  public MerchantInfo     findMerchantInfo(String merchant_num);
  
  
} // interface MerchantInfoDAO
