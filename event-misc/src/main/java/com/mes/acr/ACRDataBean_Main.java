package com.mes.acr;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.forms.CheckboxField;
import com.mes.forms.FieldBean;
import com.mes.user.UserBean;

public final class ACRDataBean_Main extends ACRDataBeanBase
{
  static Logger log = Logger.getLogger(ACRDataBean_Main.class);
  
  // constants
  // (none)
  
  
  // data members
  private MerchantInfo  merchantInfo;
  private List          acrDefs;
  private Hashtable     submitBeans;
  
  
  // class methods
  // (none)
  
  
  // object methods
  
  // construction
  public ACRDataBean_Main(MerchantInfo merchantInfo, List acrDefs, 
    Hashtable submitBeans, UserBean user) throws Exception
  {
    if(merchantInfo == null)
    {
      throw new Exception(
        "Unable to construct Main ACR form: No merchant specified.");
    }
    if(acrDefs==null)
    {
      throw new Exception(
        "Unable to construct Main ACR form: No ACR Definitions specified.");
    }
    
    this.merchantInfo = merchantInfo;
    this.acrDefs      = acrDefs;
    this.submitBeans  = submitBeans;
    this.user         = user;
    
    this.editable     = true;
    
    createExtendedFields();
  }
  
  public long getAppSeqNum()
  {
    return merchantInfo.getAppSeqNum();
  }
  public String getMerchantNumber()
  {
    return merchantInfo.getMerchantNum();
  }
  public String getAppCntrlNum()
  {
    return merchantInfo.getControlNum();
  }

  protected void setDefaults()
  {
    // no-op
  }
  
  public int[][] getAllowedQueueOps()
  {
    return null;
  }
  
  /**
   * renderHtml()
   * 
   * Sub-class override
   */
  public String renderHtml(String fname)
  {
    if(merchantInfo!=null) {
      if(fname.equals("Merchant Account Number")) {
        return merchantInfo.getMerchantNum();
      } else if(fname.equals("Merchant DBA")) {
        return merchantInfo.getDba();
      } else if(fname.equals("Type")) {
        return merchantInfo.getType();
      } else if(fname.equals("ACR Type")) {
        return merchantInfo.getACRAppType();
      } else if(fname.equals("Application Control #")) {
        return merchantInfo.getControlNum();
      }
    }
    
    return super.renderHtml(fname);
  }
  
  protected void createExtendedFields()
  {
    // map all available acr defs in session to this bean
    for (Iterator i = acrDefs.iterator(); i.hasNext();)
    {
      ACRDefinition acrDef = (ACRDefinition)i.next();

      // see if the user has rights to submit acr of this type
      if (!acrDef.hasPermissions(user,acrDef.FUNC_SUBMIT))
      {
        continue;
      }

      // see if there is a bean/acr already associated with this acr def
      // in order to see if the check box should be checked
      ACRDataBeanBase bean = 
        (ACRDataBeanBase)submitBeans.get(acrDef.getShortName());
      ACR acr = (bean == null ? null : bean.getACR());
      boolean isChecked = 
        (acr != null && acr.getStatus() != ACR.ACRSTATUS_ABORTED);

      // create the check box field
      fields.add(new CheckboxField(acrDef.getShortName(),acrDef.getName(),
        isChecked));
    }
    
    // require at least one ACR be selected to submit
    FieldBean.AtLeastOneValidation alov = new FieldBean.AtLeastOneValidation(
      "At least one ACR must be selected to proceed.",fields.getFieldsVector());
    fields.addValidation(alov);
    
    return;
  }
  
  public String getHeadingName()
  {
    return "Submission Selection";
  }
  
  public int getNumAvailableACRDefs()
  {
    int num = 0;
    ACRDefinition acrd;

    for (Iterator i = acrDefs.iterator(); i.hasNext();)
    {
      acrd=(ACRDefinition)i.next();
      if(acrd.isOn())
        num++;
    }
    
    return num;
  }
  
  public String getACRDefDescription(String short_name)
  {
    ACRDefinition acrd;
    
    for (Iterator i = acrDefs.iterator(); i.hasNext();)
    {
      acrd=(ACRDefinition)i.next();
      if(acrd.getShortName().equals(short_name))
        return acrd.getDescription();
    }
    return "";
  }

} // class ACRDataBean_Main
