/*@lineinfo:filename=ACRDataBean_MesSplyOrdFormNew*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/acr/ACRDataBean_MesSplyOrdFormNew.sqlj $

  Description:

    ACRDataBean_MesSplyOrdFormNew

    New version of Supply Order Bean.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-05-31 14:24:22 -0700 (Thu, 31 May 2012) $
  Version            : $Revision: 20222 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.acr;


import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.CityStateZipField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.Validation;
import com.mes.supply.SupplyDescriptor;
import com.mes.supply.SupplyOrder;
import com.mes.support.StringUtilities;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;


public class ACRDataBean_MesSplyOrdFormNew extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_MesSplyOrdFormNew.class);

  private String groupTitle;
  private TreeMap supplyGroups=null;
  private ArrayList masterSupplyList=null;
  private ArrayList supplyList;
  private SupplyOrder order=null;

  private int currentStage;
  private boolean hasBeenProcessed = false;
  private static final int TOTAL_STAGES = 3;
  private final String ADDY_PREFIX = "ship";
  private boolean isClosed = false;
  private boolean addressAdded = false;

  public ACRDataBean_MesSplyOrdFormNew()
  {
  }

  public ACRDataBean_MesSplyOrdFormNew(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {

    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_COMPLETED);

    if(editable)
    {
      currentStage=1;
      order = new SupplyOrder(merchantInfo.getMerchantNum());
    }
    else
    {
      log.debug("Rebuilding existing order");
      //need to get the existing order
      currentStage=TOTAL_STAGES+1;
      order = (SupplyOrder)((mesDBACRDefDAO)getDAOFactory().getACRDefinitionDAO()).getACRRelatedObject(acr);
      if( order==null ||
          ( order.processSeq != null && order.processSeq.length()>0) )
      {
        hasBeenProcessed = true;
      }
    }

    //build supply info
    groupTitle = "";
    buildMasterList();
    buildSupplyGroupMap();
    supplyList = masterSupplyList;

    //check to see if account is closed
    if(merchantInfo!=null)
    {
      determineStatus(merchantInfo.getMerchantNum());
    }
  }

  private DAOFactory getDAOFactory()
  {
    // employ the mes db for this bean
    return DAOFactoryBuilder.getDAOFactory(DAOFactory.DAO_MESDB);
  }

  public boolean isClosed()
  {
    return isClosed;
  }

  public boolean hasFields()
  {
    if(!editable)
    {
      return false;
    }
    return super.hasFields();
  }

  public boolean hasMultiMerchantComp()
  {
    return false;
  }

  public boolean isClubMerchant()
  {
    if(order!=null)
    {
      return order.isClub();
    }
    else
    {
      return false;
    }
  }
  public int getCurrentStage(){
    return currentStage;
  }

  public String getGroupTitle(){
    return groupTitle;
  }

  public ArrayList getSupplyList(){
    return supplyList;
  }

  public boolean hasMoreSteps(){
    return(currentStage<TOTAL_STAGES);
  }

  public boolean hasBeenProcessed()
  {
    return hasBeenProcessed;
  }

  public SupplyOrder getOrder(){
    return order;
  }

  public boolean doCommand(String method)
  {
    if(   method.equals("Select Equipment Type")
       || method.equals("Calculate Order Cost"))
    {

      doPull();

      if(isValid())
      {

        acr.setStatus(ACR.ACRSTATUS_VALID);
        //prep the next stage...

        doNext();
      }
      else
      {

        acr.setStatus(ACR.ACRSTATUS_INVALID);
      }

      return true;
    }
    else if (method.equals("Start Over"))
    {
      doOver();
      return true;
    }
    else
    {
      log.error("Unhandled Supply Order Request: '"+method+"'.");
      return false;
    }

  }

/**
 * doNext():
 * will process the defined stages, as indicated by the code
 */

  private void doNext()
  {

    //advance the stage count
    currentStage++;

    //do the next stage
    createExtendedFields();

  }

/**
 * doOver():
 * goes back to beginning - removes all data, either
 * from Field or from ACRItem
 */
  private void doOver()
  {

    log.debug("STARTING... doOver()");
    //reset stage
    currentStage=1;

    //reset group title
    acr.removeItem("groupTitle");

    //reset address helper and validations that were handed over
    getAddressHelper(false, false).reset(acr);

    fields.removeValidation("otherVal1");
    fields.removeValidation("otherVal2");
    fields.removeValidation("otherVal3");

    //remove possible validation conflicts
    fields.removeValidation("quantityVal");

    //reset the order
    order.reset();

    //reset the Rush tag
    acr.setACRItem("Rush","");

    //reset any relative items previously selected
    //(ignore the general ones we want to keep)
    ACRItem item;
    for(Enumeration myEnum = getACR().getItemEnumeration(); myEnum.hasMoreElements();)
    {
      item = (ACRItem)myEnum.nextElement();
      if(item.getName().indexOf("qty_")==0 )
      {
        item.setValue("");
      }
    }

    log.debug("ENDING... doOver()");

  }

  protected void createExtendedFields()
  {

    if(!editable)
      return;

    if(acr==null)
      return;

    Field field=null;

    switch(currentStage){

      //REQUESTOR INFO and SUPPLY CATEGORY TYPE
      case 1:

        //create the parent group field
        field = new DropDownField("groupTitle","groupTitle",new SupplyGroupDropDownTable(),false,"");
        fields.add(field);
        fields.deleteField("Rush");

        break;

      //SUPPLY LIST and ADDRESS
      case 2:

        // address fields
        try
        {
         //add Address subbean - only once (will be reset if needed)
         if(!addressAdded)
          {
            add(getAddressHelper(false, false));
            addressAdded = true;
          }
        }
        catch (Exception e)
        {
          log.debug("Failed to load address sub-bean component");
        }

        fields.add(new CheckboxField("Rush","Rush?",false));

        //supply list
        SupplyDescriptor sd;
        String qtyName;
        groupTitle = acr.getACRValue("groupTitle");
        supplyList = (ArrayList)supplyGroups.get(groupTitle);

        //for now...
        if(null==supplyList)
        {
          supplyList=masterSupplyList;
        }

        //create vector for mirror - validation of like fields
        Vector vQty = new Vector(supplyList.size());

        // qty_ related fields
        for(int i=0;i<supplyList.size();i++)
        {

          sd=(SupplyDescriptor)supplyList.get(i);
          qtyName = "qty_"+sd.seqNum;
          field = new Field(qtyName,4,3,true);
          //for display
          fields.add(field);
          //validation
          vQty.add(field);
        }

        Validation val = new FieldBean.AtLeastOneValidation("At least one item quantity must be specified to proceed.", vQty);

        // add "at least one" validation
        fields.addValidation(val,"quantityVal");

        break;

      //FINAL COST
      case 3:

        //update order
        setOrder();
        break;

    }//switch
  }

  private void setOrder()
  {

    log.debug("...setting Order Address Info...");

    //update the order based on the rush status
    order.setRush(acr.getACRValue("Rush").equalsIgnoreCase("y")?true:false);

    //the doPull has already stepped in and renamed the
    //ACR Values...max willb e 30 as per Field limit in AddressHelper
    order.setShipName(acr.getACRValue(ADDY_PREFIX+"AddrName"));
    order.setShipLine1(acr.getACRValue(ADDY_PREFIX+"AddrLine1"));
    order.setShipLine2(acr.getACRValue(ADDY_PREFIX+"AddrLine2"));

    //HACK to get seperate field data - doPull has already fired and
    //changed the location of the data we need.
    CityStateZipField temp = new CityStateZipField("temp","temp",10,true);
    temp.setData(acr.getACRValue(ADDY_PREFIX+"AddrCsz"));
    fields.add(temp);
    order.setShipCity(fields.getData("tempCity"));
    order.setShipState(fields.getData("tempState"));
    order.setShipZip(fields.getData("tempZip"));
    fields.deleteField("temp");

    //set the tax rate
    order.setTaxRate();

    int qty;
    SupplyDescriptor sd;
    String qtyName;

    //cycle the current supplyList for selected acr items
    for(int i=0;i<supplyList.size();i++)
    {
      //look for the chosen supplies
      sd=(SupplyDescriptor)supplyList.get(i);
      qtyName = "qty_"+sd.seqNum;
      qty = StringUtilities.stringToInt(acr.getACRValue(qtyName),0);

      //if field has a value - put it in the order...
      if(qty==0)
      {
        //no quantity, so remove it from fields
        fields.deleteField(qtyName);
      }
      else
      {
       //add selected SupplyDescriptor to order
       //(this translates the SD to a SupplyOrderItem)
        order.add(sd, qty);
      }

    }

    //finally, add up the order costs
    order.calculateSumCosts();

  }

  public class SupplyGroupDropDownTable extends DropDownTable
  {
    public SupplyGroupDropDownTable()
    {
      String s;
      Set keys = supplyGroups.keySet();
      for(Iterator e = keys.iterator();e.hasNext();){
        s=(String)e.next();
        addElement(s,s);
      }
    }

  } // SupplyGroupDropDownTable class

  public String renderHtml(String fieldName){

    //override that "rush" field
    if(fieldName.equals("Rush")
       && !hasMoreSteps()){
        if( (null!=order && order.isRush) ){
          //change the display
          return "<font color=\"Red\">RUSH</font>";
        }else{
          return "";
        }
    }else{
      return super.renderHtml(fieldName);
    }
  }

  public SupplyDescriptor getSupplyDescriptor(String seqNum)
  {
    if(seqNum==null || seqNum.length()<1)
      return null;
      SupplyDescriptor sd;
      for(int i=0;i<masterSupplyList.size();i++) {
        sd = (SupplyDescriptor)masterSupplyList.get(i);
        if(sd.seqNum.equals(seqNum)) {
          return sd;
        }
    }
    return null;
  }

  protected final void buildMasterList(){

    log.debug("build supplyList...START");
    masterSupplyList = new ArrayList();
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      connect();

      // get list of supplies
      /*@lineinfo:generated-code*//*@lineinfo:485^7*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT
//            si.supply_item_id as seqNum,
//            si.partnum        as partNum,
//            si.description    as title,
//            si.unit_cost      as cost,
//            si.ship_cost_stan as shipCostStandard,
//            si.ship_cost_rush as shipCostRush,
//            sig.title         as groupTitle,
//            sut.name          as equivalent,
//            sut.gcf_name      as unit
//          FROM
//            supply_item si,
//            supply_unit_type sut,
//            supply_item_group sig,
//            supply_item_group_map sigm
//          WHERE
//            si.supply_item_id=sigm.supply_item_id
//          AND
//            sig.group_id=sigm.group_id
//          AND
//            si.unit_type_id=sut.unit_type_id
//          ORDER BY
//            sig.title asc, si.partnum asc, si.unit_cost asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n          si.supply_item_id as seqNum,\n          si.partnum        as partNum,\n          si.description    as title,\n          si.unit_cost      as cost,\n          si.ship_cost_stan as shipCostStandard,\n          si.ship_cost_rush as shipCostRush,\n          sig.title         as groupTitle,\n          sut.name          as equivalent,\n          sut.gcf_name      as unit\n        FROM\n          supply_item si,\n          supply_unit_type sut,\n          supply_item_group sig,\n          supply_item_group_map sigm\n        WHERE\n          si.supply_item_id=sigm.supply_item_id\n        AND\n          sig.group_id=sigm.group_id\n        AND\n          si.unit_type_id=sut.unit_type_id\n        ORDER BY\n          sig.title asc, si.partnum asc, si.unit_cost asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRDataBean_MesSplyOrdFormNew",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.acr.ACRDataBean_MesSplyOrdFormNew",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:510^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        //build SD list
        SupplyDescriptor sd = new SupplyDescriptor();
        sd.seqNum=rs.getString("seqNum");
        sd.partNum=rs.getString("partNum");
        sd.topGrpTitle="";
        sd.groupTitle=rs.getString("groupTitle");
        sd.title=rs.getString("title");
        sd.setCost(rs.getDouble("cost"));
        sd.shipCostStandard=rs.getDouble("shipCostStandard");
        sd.shipCostRush=rs.getDouble("shipCostRush");
        sd.unit=rs.getString("unit");
        sd.equivalent=rs.getString("equivalent");
        masterSupplyList.add(sd);
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      log.debug("buildMasterList()" + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}

      cleanUp();
      log.debug("build supplyList...STOP (="+masterSupplyList.size()+")");
    }
  }

  protected final void buildSupplyGroupMap(){

  //basically build a map of vectors (supplies)
  //keyed on groupTitle (map for sorted display)
    log.debug("building supply tree...START");
    supplyGroups = new TreeMap();
    ArrayList supplyGroup=null;
    SupplyDescriptor sd;
    String title = "";

    for(int j=0;j<masterSupplyList.size();j++){
      sd=(SupplyDescriptor)masterSupplyList.get(j);

      if(!title.equals(sd.groupTitle)) {

        if(supplyGroup!=null){
          //hash the supplyGroup as soon as group changes
          supplyGroups.put(title,supplyGroup);
        }

        //create new vector and change name
        supplyGroup = new ArrayList();
        title=sd.groupTitle;
      }
      //build new vector
      supplyGroup.add(sd);

    }
    //final put
    supplyGroups.put(title,supplyGroup);
    log.debug("building supply tree...DONE");
  }

  protected final void determineStatus(String merchNum){

    try
    {
      connect();

      String code = null;

      // get list of supplies
      /*@lineinfo:generated-code*//*@lineinfo:590^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT
//            merchant_status 
//          FROM
//            mif
//          WHERE
//            merchant_number = :merchNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT\n          merchant_status  \n        FROM\n          mif\n        WHERE\n          merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.acr.ACRDataBean_MesSplyOrdFormNew",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   code = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:598^7*/

      //if the code is either B, C or D the account is closed
      if( code != null &&
          ( code.equals("B") || code.equals("C") || code.equals("D"))
        )
      {
        isClosed = true;
      }

    }
    catch(Exception e)
    {
      log.debug("determineStatus()" + e.toString());
    }
    finally
    {
      cleanUp();
      log.debug("determineStatus...STOP");
    }
  }

  //closed? remove comments
  public boolean hasEditableComments()
  {
    if(isClosed)
    {
      return false;
    }
    else
    {
      return super.hasEditableComments();
    }
  }

  //closed? disable Parent Buttons
  public boolean disableParentButtons()
  {
    if(isClosed)
    {
      return true;
    }
    else
    {
      return super.disableParentButtons();
    }
  }

  public String renderAddressHtml()
  {
    //OLD
    //return getAddressHelper().renderAddressHtml();
    return "";
  }

  protected void setDefaults()
  {
  }

  public int[][] getAllowedQueueOps()
  {
    //log.debug("in getAllowedQueueOps()");
    int[][] qOps = new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
    };
    //log.debug("src: "+qOps[0][0]+", des:"+qOps[0][1]);
    return qOps;
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  public String getHeadingName()
  {
    return "Supply Ordering for Merchant";
  }


  private boolean orderIsOpen()
  {
    if (order==null)
      return false;

    boolean isOpen = false;
    log.info("in orderIsOpen");
    try
    {
      connect();
      String status=null;
      /*@lineinfo:generated-code*//*@lineinfo:706^7*/

//  ************************************************************
//  #sql [Ctx] { select process_sequence 
//          from supply_order
//          where supply_order_id = :order.id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select process_sequence  \n        from supply_order\n        where supply_order_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.acr.ACRDataBean_MesSplyOrdFormNew",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,order.id);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   status = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:710^7*/

      if(status==null)
      {
        isOpen=true;
      }
    }
    catch(Exception e)
    {
    }
    finally
    {
      cleanUp();
    }

    return isOpen;
  }

  public boolean cancelOrder()
  {
    boolean result= false;
    log.info("cancelling order #"+order.id+" - ACR#"+ acr.getID());

    if(orderIsOpen())
    {
      log.info("orderIsOpen==true");
      try
      {
        connect(false);

        /*@lineinfo:generated-code*//*@lineinfo:740^9*/

//  ************************************************************
//  #sql [Ctx] { delete from supply_order_item
//            where supply_order_id=:order.id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from supply_order_item\n          where supply_order_id= :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.acr.ACRDataBean_MesSplyOrdFormNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,order.id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:743^9*/

        /*@lineinfo:generated-code*//*@lineinfo:745^9*/

//  ************************************************************
//  #sql [Ctx] { delete from supply_order
//            where supply_order_id=:order.id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from supply_order\n          where supply_order_id= :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.acr.ACRDataBean_MesSplyOrdFormNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,order.id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:748^9*/

        /*@lineinfo:generated-code*//*@lineinfo:750^9*/

//  ************************************************************
//  #sql [Ctx] { update q_data
//            set type = 1009
//            where id = :acr.getID() and
//            item_type = 10
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3503 = acr.getID();
   String theSqlTS = "update q_data\n          set type = 1009\n          where id =  :1  and\n          item_type = 10";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.acr.ACRDataBean_MesSplyOrdFormNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3503);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:755^9*/

        /*@lineinfo:generated-code*//*@lineinfo:757^9*/

//  ************************************************************
//  #sql [Ctx] { update acr
//            set status = 6
//            where acr_seq_num = :acr.getID()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3504 = acr.getID();
   String theSqlTS = "update acr\n          set status = 6\n          where acr_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.acr.ACRDataBean_MesSplyOrdFormNew",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_3504);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:761^9*/

        commit();
        order=null;
        result=true;
        log.info("result=true");
      }
      catch(Exception e)
      {
        rollback();
      }
      finally
      {
        cleanUp();
      }
    }

    return result;
  }



/* DISABLED
  private String getShippingName()
  {
    StringBuffer addressDisplay = new StringBuffer();
    String attn = acr.getACRValue("attn");
    boolean hasAttn = ( attn !=null && !attn.equals("") );
    String bankName = acr.getACRValue("bnknme");
    boolean hasBankName = ( bankName !=null && !bankName.equals("") );

    if( hasAttn || hasBankName )
    {
      if( hasAttn )
      {
        addressDisplay.append(attn);
      }

      if( hasBankName )
      {
        if(hasAttn)
        {
          addressDisplay.append(" c/o ");
        }

        addressDisplay.append(bankName);
      }
    }
    else
    {
      addressDisplay.append(getDBShippingName());
    }

    return addressDisplay.toString();

  }


  private String getDBShippingName()
  {
    String name     = null;
    String merchNum = acr.getMerchantNum();
    try
      {
        connect();

        #sql[Ctx]
        {
          select merch_business_name into :name
          from merchant
          where merch_number = :merchNum
        };
      }
      catch(Exception e)
      {
        name = acr.getACRValue("staddress");
      }
      finally
      {
        cleanUp();
      }

    return name;
  }
*/

}/*@lineinfo:generated-code*/