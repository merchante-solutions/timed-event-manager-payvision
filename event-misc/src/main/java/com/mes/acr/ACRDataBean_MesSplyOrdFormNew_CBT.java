package com.mes.acr;

public class ACRDataBean_MesSplyOrdFormNew_CBT extends ACRDataBean_MesSplyOrdFormNew
{
  // construction
  public ACRDataBean_MesSplyOrdFormNew_CBT()
  {
  }

  public ACRDataBean_MesSplyOrdFormNew_CBT(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public String getHeadingName()
  {
    return "Order Supplies for Merchant";
  }

} // class ACRDataBean_MesSplyOrdFormNew_CBT