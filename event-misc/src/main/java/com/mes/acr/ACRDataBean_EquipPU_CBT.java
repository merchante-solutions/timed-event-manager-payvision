package com.mes.acr;

import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.Field;

public final class ACRDataBean_EquipPU_CBT extends ACRDataBean_EquipPU
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_EquipPU_CBT.class);


  // constants
  // (NONE)

  // construction
  public ACRDataBean_EquipPU_CBT()
  {
  }

  public ACRDataBean_EquipPU_CBT(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    // equip call tag table
    int[] ecctColumns = new int[]
      {
         EquipCallTagTable.COL_DESCRIPTION
        ,EquipCallTagTable.COL_EQUIPTYPE
        ,EquipCallTagTable.COL_VNUMBER
        ,EquipCallTagTable.COL_SENDCALLTAG
      };
    String[][] ecctMap = new String[][]
      {
         {EquipCallTagTable.NCN_DESCRIPTION,   "desc"}
        ,{EquipCallTagTable.NCN_EQUIPTYPE,     "type"}
        ,{EquipCallTagTable.NCN_VNUMBER,       "vnum"}
        ,{EquipCallTagTable.NCN_SENDCALLTAG,   "ssh"}
      };
    ectt = new EquipCallTagTable(this, ecctColumns, ecctMap, 4, true);
  }

  protected ACRDataBeanAddressHelper _getAddressHelper()
  {
    if(adrsHelper!=null)
      return adrsHelper; // already configured by virtue of non-null obj ref

    adrsHelper = new ACRDataBeanAddressHelper(this);

    // configure the address helper
    adrsHelper.setAdrsTypeName("Ship To");
    adrsHelper.setAdrsSlctnFldName("staddress");
    //adrsHelper.setAdrsName("");
    adrsHelper.setAdrsAddressFldNme("sh_address");
    adrsHelper.setAdrsAttnFldNme("sh_attn");
    adrsHelper.setAdrsBankNameFldNme("sh_bnknme");
    adrsHelper.setAdrsCityFldNme("sh_city");
    adrsHelper.setAdrsStateFldNme("sh_state");
    adrsHelper.setAdrsZipFldNme("sh_zip");
    //adrsHelper.setAdrsCountry("sh_country");
    adrsHelper.setIsRequired(true);

    // cb&t specific filtering
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_BUSADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_MAILINGADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_EQUIPDELADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_OWNER1ADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_OWNER2ADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_OWNER3ADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_FULFILLADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_CONTACTADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MERCH_BANDADDRESS);
    adrsHelper.filterAddress(ACRDataBeanAddressHelper.ADRSTYPECODE_MIF_BT1);

    return adrsHelper;
  }

  protected void setDefaults()
  {
    if(getMerchantNumber().length()>0) {

      // merchant address
      _getAddressHelper().setACRAdrsInfo();

    }

  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;
    String fldnme=null;

    // address fields
    _getAddressHelper().createAddressFields();

    // equip call tag table fields
    ectt.createFields(this.fields);

    if((acrdi=acrd.getACRDefItem("reason_for_calltag"))!=null)
      fields.add(ACRDefItemToField(acrdi));

  }

  public boolean equipmentExists()
  {
    return ectt.equipmentExists();
  }

  public boolean calltaggedEquipmentExists()
  {
    return ectt.calltaggedEquipmentExists();
  }

  public Vector getCallTaggedEquipment()
  {
    return ectt.getCallTaggedEquipment();
  }

  public String renderEquipCallTagTable()
  {
    return ectt.renderHtmlTable();
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_CBT_NEW}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MMS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  public String getHeadingName()
  {
    return "Order Equipment Pickup from CB&T Merchant";
  }

  public String getJSPIncludeFilename()
  {
    return (acr==null)? "":"acr_incl_"+acr.getDefinition().getShortName()+"_cbt.jsp";
  }

} // class ACRDataBean_EquipPU_CBT
