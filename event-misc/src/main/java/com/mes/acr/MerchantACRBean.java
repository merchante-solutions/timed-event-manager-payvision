/*@lineinfo:filename=MerchantACRBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/acr/MerchantACRBean.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-04-27 09:44:03 -0700 (Fri, 27 Apr 2007) $
  Version            : $Revision: 13704 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.acr;

import java.sql.ResultSet;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

/**
 * class MerchantACRBean
 *
 * Class for parent (merchant) to employ to get info on associtated ACR(s).
 */
public final class MerchantACRBean extends SQLJConnectionBase
{
  // create class log category
  static Logger log = Logger.getLogger(MerchantACRBean.class);
  // constants
  
  // data members
  //protected long      app_seq_num;
  protected long        merchantNumber;
  protected Vector      acrs;
  protected Vector      supplyOrders;
  protected int         numToShow;
  protected boolean     is_loaded;

  // class methods

  // object methods

  // construction
  public MerchantACRBean()
  {
    clear();
  }

  public void clear()
  {
    is_loaded=false;

    merchantNumber = 0L;
    
    if(acrs == null)
    {
      acrs = new Vector();
    }
    else
    {
      acrs.clear();
    }
    
    if(supplyOrders == null)
    {
      supplyOrders = new Vector();
    }
    else
    {
      supplyOrders.clear();
    }
    
    numToShow=-1; // i.e. All
  }

  // accessors

  public String getMerchantNumber()
  {
    return Long.toString(merchantNumber);
  }

  public int getNumToShow()
  {
    return numToShow;
  }

  public Vector getACRs(boolean loadAllObj)
  {
    if(!is_loaded)
    {
      load(loadAllObj);
    }

    return acrs;
  }
  
  public Vector getSupplyOrders()
  {
    if( ! is_loaded)
    {
      load(false);
    }
    
    return supplyOrders;
  }

  public Vector getACRs()
  {
    return getACRs(false);
  }

  // mutators
  public void setMerchantNumber(long merchantNumber)
  {
    this.merchantNumber = merchantNumber;
    is_loaded=false;
  }

  public void setNumToShow(int numToShow)
  {
    this.numToShow=numToShow;
  }

  ////////////////////

  private void load(boolean loadAllObj)
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      connect();
    
      // NOTE: default sort order is most recent first
      if( ! is_loaded )
      {
        Vector            v  = new Vector();
        
        /*@lineinfo:generated-code*//*@lineinfo:154^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  a.acr_seq_num                                                   id,
//                    ad.name                                                         name,
//                    to_char(a.date_last_modified, 'Dy, Mon DD, YYYY - hh:mi:ss PM') date_last_modified,
//                    to_char(a.date_created, 'Dy, Mon DD, YYYY - hh:mi:ss PM')       date_created,
//                    nvl(ast.description,'Undefined')                                status,
//                    a.acrdefid                                                      acrdefid
//            from    acr a,
//                    acrdef ad,
//                    acrstatus ast
//            where   a.merch_number = :merchantNumber and
//                    a.acrdefid = ad.acrdef_seq_num and
//                    a.status = ast.status(+)
//            order by a.date_last_modified desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  a.acr_seq_num                                                   id,\n                  ad.name                                                         name,\n                  to_char(a.date_last_modified, 'Dy, Mon DD, YYYY - hh:mi:ss PM') date_last_modified,\n                  to_char(a.date_created, 'Dy, Mon DD, YYYY - hh:mi:ss PM')       date_created,\n                  nvl(ast.description,'Undefined')                                status,\n                  a.acrdefid                                                      acrdefid\n          from    acr a,\n                  acrdef ad,\n                  acrstatus ast\n          where   a.merch_number =  :1  and\n                  a.acrdefid = ad.acrdef_seq_num and\n                  a.status = ast.status(+)\n          order by a.date_last_modified desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.MerchantACRBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.acr.MerchantACRBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:169^9*/
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          v.add(new ACRListItem(rs));
        }
        rs.close();
        it.close();
        
        acrs = v;
        
        // get supply orders
        /*@lineinfo:generated-code*//*@lineinfo:183^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  a.acr_seq_num                                         id,
//                    to_char(a.date_created, 'Dy, Mon DD, YYYY')           date_created,
//                    nvl(ast.description,'Undefined')                      status,
//                    '<li>'||si.partnum||': '||si.description||' ('||sut.gcf_name||'-'||sut.name||') ('||soi.quantity||'): '||ltrim(to_char(soi.cost, '$99.99'))||'</li>'  description,
//                    nvl(ltrim(to_char(so.total_cost, '$9990.99')), '--')  total_order_cost
//            from    acr a,
//                    acrstatus ast,
//                    supply_order so,
//                    supply_order_item soi,
//                    supply_item si,
//                    supply_unit_type sut
//            where   a.merch_number = :merchantNumber and
//                    a.status = ast.status and
//                    a.acr_seq_num = so.acrid and
//                    so.supply_order_id = soi.supply_order_id and
//                    soi.supply_item_id = si.supply_item_id and
//                    si.unit_type_id = sut.unit_type_id
//            order by a.date_created desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  a.acr_seq_num                                         id,\n                  to_char(a.date_created, 'Dy, Mon DD, YYYY')           date_created,\n                  nvl(ast.description,'Undefined')                      status,\n                  '<li>'||si.partnum||': '||si.description||' ('||sut.gcf_name||'-'||sut.name||') ('||soi.quantity||'): '||ltrim(to_char(soi.cost, '$99.99'))||'</li>'  description,\n                  nvl(ltrim(to_char(so.total_cost, '$9990.99')), '--')  total_order_cost\n          from    acr a,\n                  acrstatus ast,\n                  supply_order so,\n                  supply_order_item soi,\n                  supply_item si,\n                  supply_unit_type sut\n          where   a.merch_number =  :1  and\n                  a.status = ast.status and\n                  a.acr_seq_num = so.acrid and\n                  so.supply_order_id = soi.supply_order_id and\n                  soi.supply_item_id = si.supply_item_id and\n                  si.unit_type_id = sut.unit_type_id\n          order by a.date_created desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.acr.MerchantACRBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.acr.MerchantACRBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:203^9*/
        
        rs = it.getResultSet();
        
        int curId = -1;
        SupplyOrderItem curItem = null;
        while(rs.next())
        {
          if(curId == rs.getInt("id"))
          {
            // next row of existing item -- add additional description
            curItem.orderDesc.append(rs.getString("description"));
          }
          else
          {
            if(curItem != null)
            {
              // store current item
              supplyOrders.add(curItem);
            }
            
            // create new item
            curItem = new SupplyOrderItem(rs);
            curId = rs.getInt("id");
          }
        }
        
        if(curItem != null)
        {
          // add last item found to vector
          supplyOrders.add(curItem);
        }
        
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("load()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    //don't load the underlying Obj if not required
    is_loaded=true;
  }

  public class ACRListItem
  {
    public String id = "";
    public String name = "";
    public String lastModified = "";
    public String created = "";
    public String status = "";
    public int    acrdefid = 0;
    
    public ACRListItem(ResultSet rs)
    {
      try
      {
        id = rs.getString("id");
        name = rs.getString("name");
        lastModified = rs.getString("date_last_modified");
        created = rs.getString("date_created");
        status = rs.getString("status");
        acrdefid = rs.getInt("acrdefid");
      }
      catch(Exception e)
      {
        logEntry("ACRListItem() constructor", e.toString());
      }
    }
  }
  
  public class SupplyOrderItem
  {
    public String       id = "";
    public String       created = "";
    public String       status = "";
    public StringBuffer orderDesc = new StringBuffer("");
    public String       orderCost = "";
    
    public SupplyOrderItem(ResultSet rs)
    {
      try
      {
        id = rs.getString("id");
        created = rs.getString("date_created");
        status = rs.getString("status");
        orderDesc.append(rs.getString("description"));
        orderCost = rs.getString("total_order_cost");
      }
      catch(Exception e)
      {
        logEntry("SupplyOrderItem() constructor", e.toString());
      }
    }
  }

}/*@lineinfo:generated-code*/