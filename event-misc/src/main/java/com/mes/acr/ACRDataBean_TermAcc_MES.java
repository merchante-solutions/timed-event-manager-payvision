/*************************************************************************
  Description:  ACRDataBean_TermAcc_MES

    ACR bean to allow user to order terminal accessories

  Copyright (C) 2000-2004 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.RadioField;
import com.mes.forms.Selector;
import com.mes.tools.DropDownTable;

public class ACRDataBean_TermAcc_MES extends ACRDataBeanBase
{
  /**
   * Log4J Logger
   */
  static Logger log = Logger.getLogger(ACRDataBean_TermAcc_MES.class);

  /**
   * Factory for selector creation and the selector itself
   */
  private ACRSelectorFactory factory = new ACRSelectorFactory();
  private Selector equipSelector;

  /**
   * Discover number (will change if applicable)
   */
  private String discoverMerchantNumber = "N/A";
  public String getDiscMerchNum() { return discoverMerchantNumber; }

  /**
   * Address bean for shipping address
   */
  private HashMap itemMap = null;

  /**
   * Standard Constructor
   */
  public ACRDataBean_TermAcc_MES()
  {
  }

  public ACRDataBean_TermAcc_MES(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT);
  }

  /**
   * Selector Generation
   * ST_EQUIPDISP_FLAG
   */
  public Selector getEquipmentSelector()
  {
    if (equipSelector == null)
    {
     equipSelector = factory.createSelector(factory.ST_TERMINAL_ACC,
        getMerchantNumber());

      //equipSelector = factory.createSelector(factory.ST_DBA_EQUIP_CALLTAGS,
        //getMerchantNumber());
    }
    return equipSelector;
  }

  protected void setDefaults()
  {

    String merchNum = getMerchantNumber();

    if (merchNum.length() == 0)
    {
      return;
    }

    if(editable)
    {
      setDiscoverMerchantNumber(merchNum);
    }
  }

  protected void createExtendedFields()
  {

    if(acr==null)
      return;

    if(editable)
    {
      try
      {
        //add Address subbean
        add(getAddressHelper(true, true));

        //add equipment selector
        add(getEquipmentSelector());

        //add the hidden discover value
        HiddenField hidden = new HiddenField("discoverMerchantNumber", discoverMerchantNumber);
        fields.add(hidden);

        //add Tips field
        String yesNo [][] =
        {
         { "No", "No" },
         { "Yes", "Yes" }
        };

        RadioField tips = new RadioField("tipsPrompt", "Tips Prompt", yesNo);
        fields.add(tips);


        //add connect to/from fields
        Field field = new Field("connect_to",75,35,true);
        fields.add(field);
        field = new Field("connect_from",75,35,true);
        fields.add(field);

        //private method to mimic ACRDef of legacy code
        addStaticFields();


        //new fields for user to count and total selections
        field = new Field("totalItems",20,20,true);
        fields.add(field);
        field = new Field("totalCostItems",20,20,true);
        fields.add(field);
        field = new Field("monthsToCharge",20,20,true);
        fields.add(field);
        field = new DropDownField("whoToCharge","whoToCharge", TableFactory.getDropDownTable(TableFactory.CHARGE),true);
        fields.add(field);
        //add imprinter fields
         /**
         * TODO: fix this cut/paste
         * so we don't have the same code everywhere
         * similar problem in AdrsChng

          // imprinter plates
          Field imPlates = new CheckboxField("sendImpPlts","Check here if new imprinter plate is needed and indicate quantity.",false);
          imPlates.setShowName("Send Imprinter Plate?");

          Field imQuantity = new NumberField("ipQty",2,2,true,0,0,99);
          imQuantity.setShowName("Send Imprinter Plate?");

          // validations
          //add validations for the imprinter plate checkbox
          IfYesNotBlankValidation ynbVal = new IfYesNotBlankValidation(imPlates,"You must enter a quantity of imprinter plates.");
          imQuantity.addValidation(ynbVal);

          //add two final fields
          fields.add(imQuantity);
          fields.add(imPlates);
        // DONE dup code **/
      }
      catch(Exception e)
      {
        log.error("createExtendedFields() = " + e.toString());
        logEntry("createExtendedFields()",e.toString());
      }
    }
    else
    {
      //post-submisison fields
      DropDownTable vddTable = TableFactory.getDropDownTable(TableFactory.VENDOR);
      vddTable.addElement("TermMaster Vital","TermMaster Vital");
      Field field = new DropDownField("Vendor","Vendor",vddTable,true);
      fields.add(field);
    }

    fields.setHtmlExtra("class=\"formText\"");
  }


  /**
   * addStaticFields()
   *
   * ugly yes, but that's the way it is...
   * need to create a bunch of like fields
   * that have nothing to do with any other ACR
   * and ACRDefItem really doesn't help
   */
  private void addStaticFields()
  {

    String[][] staticFields = new String[][]
    {
       { "HYPERCOM" , "T7P Pwrpk", "($35.00)" },
       { "HYPERCOM" , "T77 Pwrpk", "($35.00)"  },
       { "HYPERCOM" , "T7 Plus Pwrpk", "($35.00)" },
       //{ "IMPRINTER" , "Imprinter", "($22.00)"  },
       { "IMPRINTER" , "Imprinter Plate", "($0.00)"  },
       //{ "IMPRINTER" , "Portable Imprinter", "($26.00)"  },
       //{ "IMPRINTER" , "Mini Mate Imprinter", "($26.00)"  },
       { "MISC" , "Swivel Stand for the Talento T-One", "($95.00)"  },
       { "MISC" , "Small Dual Window Decal", "($6.50)"  },
       { "MISC" , "Tent Sign", "($6.50)"  },
       { "VERIFONE" , "Tranz 330/380 Pwrp", "($20.00)"  },
       { "VERIFONE" , "Zon XL300 Pwrpk", "($20.00)"  },
       { "VERIFONE" , "Zon XL Pwrpk", "($20.00)"  },
       { "VERIFONE" , "P250/220 Pwrpk", "($27.50)"  },
       { "VERIFONE" , "T460 Pwrpk", "($35.00)"  },
       { "VERIFONE" , "P900 Pwrpk", "($27.50)"  },
       { "VERIFONE" , "OMNI 3200 Pwrpk", "($35.00)"  },
       { "VERIFONE" , "PINpad Interface Cord", "(see pricing table)"  },
       { "VERIFONE" , "Any Vfone Term/Prt Interface", "(see pricing table)"  },
    };

    String tempName = "@@";
    String currentName = null;
    boolean firstIn = true;
    ArrayList list = null;
    Field field;
    AtLeastOneValidation val =
      new AtLeastOneValidation("You must enter a quantity for at least one item");

    for(int i = 0; i < staticFields.length;i++)
    {

      currentName = staticFields[i][0];

      //check the currect record name against existing name
      if( !tempName.equals(currentName) )
      {
        if(!firstIn)
        {
          //add to the hash
          itemMap.put(tempName,list);
        }

       //change the name
        tempName = currentName;

        //reset the list
        list = new ArrayList();
      }

      //add next item to list - the format
      //is quite important, as we'll be parsing it
      //when we retrieve the ACR sub info later
      //see renderItemHTML() below
      field = new NumberField(  "item_" + staticFields[i][1]+"|"+currentName,
                                staticFields[i][1]+" "+staticFields[i][2],
                                3,
                                3,
                                true,
                                0
                              );
      //add it to this list
      list.add(field);

      //add it to the fields
      fields.add(field);

      //connect validation
      val.addField(field);

      //special validation for tips prompt
      if(staticFields[i][1].equals("QRG"))
      {
        field.addValidation(new ConditionalRequiredValidation
                              (new FieldValueCondition( fields.getField("tipsPrompt"),
                                                        "Yes"
                                                       ),
                              "If requesting Tips prompt, you must enter a QRG amount."
                              )
                            );
      }

      //first time through
      if(firstIn)
      {
        field.addValidation(val);
        itemMap = new HashMap();
        firstIn = false;
      }
    }

    //final list addition
    itemMap.put(currentName,list);
  }

  public String renderEquipmentHTML()
  {
    //this is hack to reveal new equipment info... to be fixed?!
    //all naming is directly related to the bean and selector item/frame/factory
    boolean hasEquip = false;
    String item = "";
    StringBuffer sb = new StringBuffer();

    item = getACRValue(CALLTAG_EQUIPMENT_NAME);
    if (!item.equals(""))
    {
      if(!hasEquip)
      {
        hasEquip = true;

        sb.append("<table cellpadding=\"1\" border=\"0\" align=\"center\" width=\"100%\" class=\"tableFillGray\" cellspacing=\"0\">");
        sb.append("<tr>");
        sb.append("<td>");
        sb.append("<table class=\"formTable\" width=\"100%\" border=\"0\" cellspacing=\"0\">");
        sb.append("  <tr>");
        sb.append("  <th colspan=3 class=\"tableColumnHead\">Terminal Selection</th>");
        sb.append("  </tr>");
        sb.append("  <tr>");
        sb.append("   <td class=\"tableData\" align=\"center\">Serial Number</td>");
        sb.append("   <td class=\"tableData\" align=\"center\">Description</td>");
        sb.append("   <td class=\"tableData\" align=\"center\">Equipment Type</td>");
        sb.append(" </tr>");
        sb.append(" <tr>");
        sb.append("   <td colspan=5 height=\"1\" bgcolor=\"#cccccc\"><img src=\"/images/spcr_transp.gif\" height=\"1\"></td>");
        sb.append("  </tr>");
      }
      sb.append("<tr>");
      sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_serNum")).append("</td>");
      sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_description")).append("</td>");
      sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_equipType")).append("</td>");
      sb.append("</tr>");
    }

    //finish up
    if(hasEquip)
    {
      sb.append("</table></td></tr></table>");
    }
    else
    {
      sb.append("No equipment selected for this ACR.");
    }

    return sb.toString();
  }

  public String renderItemHTML(String itemGroup)
  {
    if( itemMap == null )
    {
      return "unable to locate item map.";
    }

    ArrayList list = (ArrayList)itemMap.get(itemGroup.toUpperCase());

    if( list == null )
    {
      return "unable to locate item lists.";
    }

    Field field;
    StringBuffer sb = new StringBuffer(128);

    sb.append("<table cellpadding=\"1\" border=\"0\" align=\"center\" width=\"100%\" cellspacing=\"0\">");
    sb.append("<tr><td colspan=\"2\" class=\"semiSmallBold\">").append(itemGroup).append("</td></tr>");
    for(Iterator it = list.iterator(); it.hasNext();)
    {
      field = (Field)it.next();

      sb.append("<tr>");
      sb.append("  <td width=100>").append(field.renderHtml()).append("</td>");
      sb.append("  <td>").append(field.getLabel()).append("</td>");
      sb.append("</tr>");
    }
    sb.append("</table>");

    return sb.toString();
  }

  public String renderItemSummaryHTML()
  {
    String fName;
    ACRItem acri;
    StringBuffer sb = new StringBuffer(128);

    sb.append("<table cellpadding=\"1\" border=\"0\" align=\"center\" width=\"100%\" cellspacing=\"0\">");
    sb.append("<tr><td colspan=\"2\" class=\"semiSmallBold\">Items Selected:</td></tr>");

    //loop through ACRItems
    for(Enumeration e=getACR().getItemEnumeration();e.hasMoreElements();) {

      acri=(ACRItem)e.nextElement();
      fName = acri.getName();

      //check for items
      if(fName.indexOf("item_")==0)
      {
        sb.append("<tr>");
        sb.append("  <td width=50>").append(acri.getValue()).append("</td>");
        sb.append("  <td width=250>").append(fName.substring(5,fName.indexOf("|"))).append("</td>");
        sb.append("  <td>(").append(fName.substring(fName.indexOf("|")+1).toLowerCase()).append(")</td>");
        sb.append("</tr>");
      }
    }
    sb.append("</table>");

    return sb.toString();
  }

  private void setDiscoverMerchantNumber(String merchNum)
  {
    PreparedStatement ps    = null;
    ResultSet         rs    = null;

    try
    {
      connect();

      //1. first see if merchant numbers are in MIF
      String qs =
      "select nvl(to_char(dmdsnum), 'N/A') AS discover "+
      "from mif "+
      "where merchant_number = ?";

      ps = con.prepareStatement(qs);
      ps.setString(1,merchNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        discoverMerchantNumber = rs.getString("discover");
      }

      //2. data not in mif so look in application database
      if(discoverMerchantNumber.equals("N/A"))
      {

        rs.close();
        ps.close();


        qs ="select nvl(to_char(mpo.merchpo_card_merch_number), 'N/A') AS discover "+
            "from merchpayoption mpo, "+
            "merchant m "+
            "where m.merch_number = ? and "+
            "m.app_seq_num = mpo.app_seq_num and "+
            "mpo.cardtype_code = ?";

        ps = con.prepareStatement(qs);
        ps.setString(1,merchNum);
        ps.setInt(2,mesConstants.APP_CT_DISCOVER);
        rs = ps.executeQuery();

        if(rs.next())
        {
          discoverMerchantNumber = rs.getString("discover");
        }

      }

      //3. only place left for data is in mif_non_bank_cards
      if(discoverMerchantNumber.equals("N/A"))
      {
        rs.close();
        ps.close();

        qs ="select nvl(discover, 'N/A') AS discover "+
            "from mif_non_bank_cards "+
            "where merchant_number = ?";

        ps = con.prepareStatement(qs);
        ps.setString(1,merchNum);
        rs = ps.executeQuery();

        if(rs.next())
        {
          discoverMerchantNumber = rs.getString("discover");
        }
      }
    }
    catch(Exception e)
    {
      log.error("setDefaults: " + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  public String getHeadingName()
  {
    return "Order Terminal Accessories for Merchant";
  }

} // class ACRDataBean_TermAcc_MES