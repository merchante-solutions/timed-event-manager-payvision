package com.mes.acr;

// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CityStateZipField;
import com.mes.forms.Field;
import com.mes.forms.RadioButtonField;

public final class ACRDataBean_DDA_MES extends ACRDataBean_DDA_Base
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_DDA_MES.class);

  // construction
  public ACRDataBean_DDA_MES()
  {
  }

  public ACRDataBean_DDA_MES(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_RISK);
  }


  protected void setDefaults()
  {
    //null op
  }

  protected void createExtendedFields()
  {

    //super.createExtendedFields();

    if(!editable)
      return;

    if(acr==null)
      return;

    // default to rush request
    fields.deleteField("Rush");

    //Create a radio selection for account type to change
    //defaults to "Both Accounts"
    String[][] buttons = new String[][]
    {
      {"Deposit and Fee","Deposit and Fee Accounts"},
      {"Deposit","Deposit Account"},
      {"Fee","Fee Account"}
    };

    RadioButtonField accountTypes
    = new RadioButtonField( "accntType",
                            "Account Type",
                            buttons,
                            0,
                            true,
                            "Please select an Account Type.");
    fields.add(accountTypes);

    Field field = new Field("new_merchantDDA","Merchant DDA",false);
    fields.add(field);

    field = new Field("new_transRtNum","Trans Rt Num",false);
    fields.add(field);

    // validate new transit routing number
    fields.addValidation(new ACRDataBean_DDA_Base.DdaTransRtValidation());

    AllOrNoneValidation val = new AllOrNoneValidation("Both new name and city fields must be completed for new bank information");

    field = new Field("newBank","New Bank Name",true);
    val.addField(field);
    field.addValidation(val);
    fields.add(field);

    field = new CityStateZipField("newBankDeets", "Bank Location Info", 40 ,true);
    val.addField(field);
    field.addValidation(val);
    fields.add(field);

    //fields.addValidation(val);

  }

  public String renderHtml(String fieldname)
  {
    if(!editable && fieldname.equals("newBankDeets"))
    {
      StringBuffer sb = new StringBuffer();
      sb.append(getACRValue("newBankDeetsCity")).append(", ");
      sb.append(getACRValue("newBankDeetsState")).append(" ");
      sb.append(getACRValue("newBankDeetsZip"));
      return sb.toString();
    }
    else
    {
      return super.renderHtml(fieldname);
    }
  }



} // class ACRDataBean_DDA_MES
