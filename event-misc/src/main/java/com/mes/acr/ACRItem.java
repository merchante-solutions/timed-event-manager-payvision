package com.mes.acr;

// log4j classes.
import org.apache.log4j.Logger;
import com.mes.database.ValueObjectBase;

/**
 * ACRItem class
 *
 * Represents a single Account Change Request Item.
 */
public class ACRItem extends ValueObjectBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRItem.class);

  // constants
  // (none)

  // data members
  protected long          id;
  protected String        name;           // corres. to ACRDefItem.name
  protected String        value;
  protected ACRDefItem    itemDef;        // corres. ACR def item


  // class functions
  // (none)


  // object functions

  // construction
  public ACRItem()
  {
    super(true);

    id=-1L;
    name="";
    value="";
    itemDef=null;
  }
  public ACRItem(String name,String value)
  {
    super(true);

    id=-1L;
    this.name=((name!=null)? name:"");
    this.value=((value!=null)? value:"");
    itemDef=null;
  }
  public ACRItem(ACRDefItem itemDef,String value)
  {
    super(true);

    this.id=-1L;
    this.name=((itemDef!=null)? itemDef.getName():"");
    this.value=((value!=null)? value:"");
    this.itemDef=itemDef;
  }

  public boolean equals(Object obj)
  {
    if(!(obj instanceof ACRItem))
      return false;

    return (((ACRItem)obj).getID()==this.id);
  }

  public void clear()
  {
    id=-1L;
    name="";
    value="";

    is_dirty=true;

    //NOTE: related ACR Item Def is NOT cleared!
  }

  // accessors
  public long       getID() { return id; }
  public String     getName() { return name; }
  public String     getValue() { return value; }
  public ACRDefItem getItemDef() { return itemDef; }

  public boolean    hasValue() { return value.length()>0; }

  // mutators
  public void setID(long v)
  {
    if(v==id)
      return;
    id=v;
    is_dirty=true;
  }
  public void setName(String v)
  {
    if(v==null)
      return;
    name=v;
    is_dirty=true;
  }
  public void setValue(String v)
  {
    if(v==null)
      return;
    value=v;
    is_dirty=true;
  }
  public void setItemDef(ACRDefItem v)
  {
    if(v==null || v==itemDef)
      return;
    itemDef=v;
    is_dirty=true;
  }
  public void clearItemDef()
  {
    itemDef=null;
    is_dirty=true;
  }

  public String toString()
  {
    final String nl = System.getProperty("line.separator");

    StringBuffer sb = new StringBuffer(256);

    sb.append("id="+id);
    sb.append(",name="+name);
    sb.append(",value="+value);
    sb.append(" - "+(is_dirty? "DIRTY":"CLEAN"));

    sb.append(nl);

    if(itemDef!=null) {
      sb.append("  - ITEM DEF: itemDef ID="+itemDef.getID());
      sb.append(", itemDef Name="+itemDef.getName());
    } else
      sb.append("  *NO ITEM DEF*");

    return sb.toString();
  }

} // class ACRItem
