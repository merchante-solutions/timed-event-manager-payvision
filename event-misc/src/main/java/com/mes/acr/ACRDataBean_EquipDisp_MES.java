/*************************************************************************
  Description:  ACRDataBean_EquipDisp_CBT

    ACR bean to allow user to order a disposition change of existing
    equipment - as created, unique to CBT POS deployment integration
    RIGHT_ACR_TYPE_EQUIPDISP = 3132

  Copyright (C) 2000-2004 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.acr;

// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.Selector;


public class ACRDataBean_EquipDisp_MES extends ACRDataBeanBase
{
  /**
   * Log4J Logger
   */
  static Logger log = Logger.getLogger(ACRDataBean_EquipDisp_MES.class);

  /**
   * Factory for selector creation and the selector itself
   */
  private ACRSelectorFactory factory = new ACRSelectorFactory();
  private Selector equipSelector;

  /**
   * Address bean for shipping address

  private AddressHelper addressHelper;
  */

  /**
   * Standard Constructor
   */
  public ACRDataBean_EquipDisp_MES()
  {
  }

  public ACRDataBean_EquipDisp_MES(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP);
  }

  /**
   * Selector Generation
   * ST_EQUIPDISP_FLAG
   */
  public Selector getEquipmentSelector()
  {
    if (equipSelector == null)
    {
     equipSelector = factory.createSelector(factory.ST_EQUIPDISP_SELECTION,
        getMerchantNumber());

      //equipSelector = factory.createSelector(factory.ST_DBA_EQUIP_CALLTAGS,
        //getMerchantNumber());
    }
    return equipSelector;
  }


  /**
   * Instantiates an address helper, stores internal reference to it.

  private AddressHelper getAddressHelper()
  {
    if (addressHelper == null)
    {
      addressHelper = new AddressHelper(getMerchantNumber(),"ship",
                                        AddressHelper.FT_NONE);
    }
    return addressHelper;
  }
*/
  protected void setDefaults()
  {
    // no-op
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    // default to rush request
    fields.deleteField("Rush");
    fields.add(new CheckboxField("Rush","Rush?",true));

    try
    {
      //add Address subbean
      //add(getAddressHelper());

      //add equipment selector
      add(getEquipmentSelector());

      // slight hack here...
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch(Exception e)
    {
      log.error("createExtendedFields() = " + e.toString());
      logEntry("createExtendedFields()",e.toString());
    }
  }

  public String getHeadingName()
  {
    return "Change Equipment Disposition";
  }


  public String renderEquipmentHTML()
  {
    //this is hack to reveal new equipment info... to be fixed?!
    //all naming is directly related to the bean and selector item/frame/factory
    boolean hasEquip = false;
    int LIMIT = 50;
    String item = "";
    StringBuffer sb = new StringBuffer();

    for (int i=0; i < LIMIT; i++)
    {
      item = getACRValue(CALLTAG_EQUIPMENT_NAME+"_"+i);
      if (!item.equals(""))
      {
        if(!hasEquip)
        {
          hasEquip = true;

          sb.append("<table cellpadding=\"1\" border=\"0\" align=\"center\" width=\"100%\" class=\"tableFillGray\" cellspacing=\"0\">");
          sb.append("<tr>");
          sb.append("<td>");
          sb.append("<table class=\"formTable\" width=\"100%\" border=\"0\" cellspacing=\"0\">");
          sb.append("  <tr>");
          sb.append("  <th colspan=5 class=\"tableColumnHead\">Equipment for Disposition Change</th>");
          sb.append("  </tr>");
          sb.append("  <tr>");
          sb.append("   <td class=\"tableData\" align=\"center\">Serial Number</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Description</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Disposition</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Change Disposition to:</td>");
          sb.append("   <td class=\"tableData\" align=\"center\">Charge Record Amount:</td>");
          sb.append(" </tr>");
          sb.append(" <tr>");
          sb.append("   <td colspan=5 height=\"1\" bgcolor=\"#cccccc\"><img src=\"/images/spcr_transp.gif\" height=\"1\"></td>");
          sb.append("  </tr>");
        }
        sb.append("<tr>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_serNum")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_description")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_disposition")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_newDisp")).append("</td>");
        sb.append("<td class=\"tableData\" align=\"center\">").append(getACRValue(item+"_chrgAmt")).append("</td>");
        sb.append("</tr>");
      }
    }

    //finish up
    if(hasEquip)
    {
      sb.append("</table></td></tr></table>");
    }
    else
    {
      sb.append("No equipment selected for this ACR.");
    }

    return sb.toString();
  }

}

