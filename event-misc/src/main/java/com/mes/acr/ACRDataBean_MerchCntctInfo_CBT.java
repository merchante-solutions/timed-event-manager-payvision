package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.PhoneField;

public class ACRDataBean_MerchCntctInfo_CBT extends ACRDataBeanBase
{
  // log4j initialization
  static Logger log =
    Logger.getLogger(ACRDataBean_MerchCntctInfo_CBT.class);

  public ACRDataBean_MerchCntctInfo_CBT()
  {
  }

  public ACRDataBean_MerchCntctInfo_CBT(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  /**
   * Loads current mif contact info data into acr existing fields.
   */
  protected void setDefaults()
  {
    String merchNum = getMerchantNumber();
    if(acr==null || merchNum.length()<1)
      return;

    PreparedStatement ps = null;
    ResultSet         rs = null;

    try
    {
      connect();

      String qs = " select  nvl(phone_1,'---')            phone,  "
                + "         nvl(phone_2_fax,'---')        fax,    "
                + "         nvl(email_addr_ind_00,'---')  email,  "
                + "         nvl(other_name,'---')         contact "
                + " from    mif                                   "
                + " where   merchant_number = ?                   ";
      ps = con.prepareStatement(qs);
      ps.setString(1,merchNum);

      rs = ps.executeQuery();
      if (rs.next())
      {
        acr.setACRItem("existing_phone",
                       PhoneField.format(rs.getString("phone")));
        acr.setACRItem("existing_fax",
                       PhoneField.format(rs.getString("fax")));
        acr.setACRItem("existing_email",
                       rs.getString("email"));
        acr.setACRItem("existing_contact",
                       rs.getString("contact"));
      }
    }
    catch (Exception e)
    {
      log.error("setDefaults() exception: " + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    try
    {
      ACRDefinition acrd=acr.getDefinition();
      ACRDefItem acrdi=null;
      Field field=null;

      Vector v = new Vector(3,1);

      acrdi=acrd.getACRDefItem("business_phone");
      field = new PhoneField(acrdi.getName(),acrdi.getLabel(),true);
      v.add(field);
      fields.add(field);

      acrdi=acrd.getACRDefItem("fax_number");
      field = new PhoneField(acrdi.getName(),acrdi.getLabel(),true);
      v.add(field);
      fields.add(field);

      acrdi=acrd.getACRDefItem("email_address");
      field = new EmailField(acrdi.getName(),acrdi.getLabel(),50,40,true);
      v.add(field);
      fields.add(field);

      acrdi=acrd.getACRDefItem("contact_name");
      field = new Field(acrdi.getName(),acrdi.getLabel(),25,40,true);
      v.add(field);
      fields.add(field);

      // add "at least one" validation
      fields.addValidation(new FieldBean.AtLeastOneValidation("At least one Merchant Contact field must be specified.",v));
    }
    catch (Exception e)
    {
      log.error("Error in createExtendedFields(): " + e);
    }
  }

  /**
   * Maps a different detail include jsp name for the CB&T version of the acr.
   */
  public String getJSPIncludeFilename()
  {
    if (acr == null)
    {
      return "";
    }
    return "acr_incl_" + acr.getDefinition().getShortName() + "_cbt.jsp";
  }

  /**
   * Overrides base class behavior to not allow comments to be edited during
   * initial acr creation (assumed to be indicated by editable == true).
   */
  public boolean hasEditableComments()
  {
    return !editable && super.hasEditableComments();
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
      { MesQueues.Q_NONE, MesQueues.Q_CHANGE_REQUEST_CBT_NEW },

      { MesQueues.Q_CHANGE_REQUEST_CBT_NEW, MesQueues.Q_CHANGE_REQUEST_RESEARCH },
      { MesQueues.Q_CHANGE_REQUEST_CBT_NEW, MesQueues.Q_CHANGE_REQUEST_CBT_MMS },
      { MesQueues.Q_CHANGE_REQUEST_CBT_NEW, MesQueues.Q_CHANGE_REQUEST_CBT_TSYS },
      { MesQueues.Q_CHANGE_REQUEST_CBT_NEW, MesQueues.Q_CHANGE_REQUEST_COMPLETED },
      { MesQueues.Q_CHANGE_REQUEST_CBT_NEW, MesQueues.Q_CHANGE_REQUEST_CANCELLED },
      {MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT},

      {MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA},
      {MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_COMPLETED},
      {MesQueues.Q_CHANGE_REQUEST_CBT_MGMT,MesQueues.Q_CHANGE_REQUEST_CANCELLED},

      {MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS},
      {MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA},
      {MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CANCELLED},
      {MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_COMPLETED},

      { MesQueues.Q_CHANGE_REQUEST_CBT_TSYS, MesQueues.Q_CHANGE_REQUEST_CBT_MMS },
      { MesQueues.Q_CHANGE_REQUEST_CBT_TSYS, MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT },
      { MesQueues.Q_CHANGE_REQUEST_CBT_TSYS, MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA },
      { MesQueues.Q_CHANGE_REQUEST_CBT_TSYS, MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS },
      { MesQueues.Q_CHANGE_REQUEST_CBT_TSYS, MesQueues.Q_CHANGE_REQUEST_COMPLETED },
      { MesQueues.Q_CHANGE_REQUEST_CBT_TSYS, MesQueues.Q_CHANGE_REQUEST_CANCELLED },

      { MesQueues.Q_CHANGE_REQUEST_RESEARCH, MesQueues.Q_CHANGE_REQUEST_CBT_TSYS },
      { MesQueues.Q_CHANGE_REQUEST_RESEARCH, MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA },
      { MesQueues.Q_CHANGE_REQUEST_RESEARCH, MesQueues.Q_CHANGE_REQUEST_COMPLETED },
      { MesQueues.Q_CHANGE_REQUEST_RESEARCH, MesQueues.Q_CHANGE_REQUEST_CANCELLED },

      { MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT },
      { MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS },
      { MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_CBT_TSYS },
      { MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_CBT_MMS },
      { MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_COMPLETED },
      { MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA, MesQueues.Q_CHANGE_REQUEST_CANCELLED },

      { MesQueues.Q_CHANGE_REQUEST_CBT_MMS, MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT },
      { MesQueues.Q_CHANGE_REQUEST_CBT_MMS, MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA },
      { MesQueues.Q_CHANGE_REQUEST_CBT_MMS, MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS },
      { MesQueues.Q_CHANGE_REQUEST_CBT_MMS, MesQueues.Q_CHANGE_REQUEST_COMPLETED },
      { MesQueues.Q_CHANGE_REQUEST_CBT_MMS, MesQueues.Q_CHANGE_REQUEST_CANCELLED },

      { MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS, MesQueues.Q_CHANGE_REQUEST_COMPLETED },
      { MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS, MesQueues.Q_CHANGE_REQUEST_CANCELLED },

      { MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT, MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA },
      { MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT, MesQueues.Q_CHANGE_REQUEST_COMPLETED },
      { MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT, MesQueues.Q_CHANGE_REQUEST_CANCELLED }

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_MMS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CBT_MGMT}
    };
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_RISK:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  public String getHeadingName()
  {
    return "Merchant Contact Information Change";
  }

} // class ACRDataBean_MerchCntctInfo_CBT
