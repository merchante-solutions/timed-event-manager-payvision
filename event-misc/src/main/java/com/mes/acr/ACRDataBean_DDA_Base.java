/*@lineinfo:filename=ACRDataBean_DDA_Base*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.acr;

import com.mes.constants.MesQueues;
import com.mes.forms.Validation;


public abstract class ACRDataBean_DDA_Base extends ACRDataBeanBase
{

  // construction
  public ACRDataBean_DDA_Base()
  {
  }

  public ACRDataBean_DDA_Base(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  //force children to implement
  protected abstract void createExtendedFields();

  public boolean showDisclaimer()
  {
    // don't show disclaimer for banks: 3858 and 3860

    if(acr==null)
      return false;

    String sbn = acr.getACRValue("Merchant Bank Number");

    return !(sbn.equals("3858") || sbn.equals("3860"));
  }


  protected void setDefaults()
  {
    // default to rush request
    acr.setACRItem("Rush","y");
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_RISK:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
          ,"No Voided Check/Bank Letter Received"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  public String getHeadingName()
  {
    return "DDA (Checking Account Number) Change";
  }

  public final class DdaTransRtValidation implements Validation
  {
    private String    newTransRoute = "";
    private String    newDda        = "";
    private String    errorMsg      = "";

    DdaTransRtValidation()
    {
    }

    public boolean validate(String fieldData)
    {
      try {
        newDda         = getField("new_merchantDDA").getData();
        newTransRoute  = getField("new_transRtNum").getData();
      }
      catch(Exception e) {}

      return isTrnMod10Compliant() && !isFraudulentTrn();
    }

    public String getErrorText()
    {
      return errorMsg;
    }

    private boolean isFraudulentTrn()
    {
      boolean rval = false;
      String fraudFlag = null;

      try {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:110^9*/

//  ************************************************************
//  #sql [Ctx] { select  fraud_flag
//            
//            from    RAP_APP_BANK_ABA
//            where   transit_routing_num = :newTransRoute
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  fraud_flag\n           \n          from    RAP_APP_BANK_ABA\n          where   transit_routing_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRDataBean_DDA_Base",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,newTransRoute);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   fraudFlag = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:116^9*/

        rval = fraudFlag.equals("Y");
      }
      catch (Exception e) {
        log.error("isFraudulentTrn() EXCEPTION: "+e.toString());
      }
      finally {
        cleanUp();
      }

      if(rval)
        errorMsg = "The specified new Transit Routing Number is fraudulent.";

      return rval;
    }

    private boolean isTrnMod10Compliant() // i.e. MOD 10 check
    {
      if(newTransRoute.length()<1)
        return true;

      boolean result = false;

      try
      {
        String  strFirstTwo = newTransRoute.substring(0,2);
        int     intFirstTwo = Integer.parseInt(strFirstTwo);

        //System.out.println("first two digits in int form is: " + intFirstTwo);
        if((intFirstTwo < 1 || intFirstTwo > 12) && (intFirstTwo < 21 || intFirstTwo > 32))
        {
          throw new Exception("First two digits were deemed invalid.");
        }

        StringBuffer sb = new StringBuffer(newTransRoute);

        int sum = 0;
        for(int m=0; m<8; m++)
        {
          int multiplier = 0;
          switch(m)
          {
            case 0:
            case 3:
            case 6:
              multiplier = 3;
            break;

            case 1:
            case 4:
            case 7:
              multiplier = 7;
            break;

            case 2:
            case 5:
              multiplier = 1;
            break;
          }
          sum += (Integer.parseInt(String.valueOf(sb.charAt(m))) * multiplier);

          log.debug("THE SUM IS: " + sum);
        }

        String  strCheckDigit     = Integer.toString(sum);

        log.debug("The Final Sum is: " + strCheckDigit);

        int     checkDigitLength  = strCheckDigit.length();

        log.debug("check digit length is: " + checkDigitLength);

                strCheckDigit     = strCheckDigit.substring(checkDigitLength - 1);

        log.debug("THE CHECK DIGIT IS: " + strCheckDigit);

        int     intCheckDigit     = 10 - Integer.parseInt(strCheckDigit);

        log.debug("ok this is the number that should equal the last digit: " + intCheckDigit);

        if(intCheckDigit == 10)
        {
          intCheckDigit = 0;
        }

        strCheckDigit = Integer.toString(intCheckDigit);

        log.debug("ok this is the number that should equal the last digit: " + strCheckDigit);

        log.debug("ok this IS the last digit: " + sb.charAt(8));

        if(strCheckDigit.equals(String.valueOf(sb.charAt(8))))
        {
          result = true;
          log.debug("everything checked out");
        }

      }
      catch(Exception e)
      {
        result = false;
        log.error("isTrnMod10Compliant() EXCEPTION: "+e.toString());
      }

      if(!result && errorMsg.length()<1)
        errorMsg = "Invalid Transit Routing Number: Check digit routine failed.";

      return result;
    }

  } // class DdaTransRtValidation

}/*@lineinfo:generated-code*/