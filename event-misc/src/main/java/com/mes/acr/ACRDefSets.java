package com.mes.acr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;

public class ACRDefSets extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(ACRDefSets.class);

  private HashMap defs = new HashMap();
  private HashMap sets = new HashMap();

  /**
   * Loads definitions into the def map and into hierarchy based sets.
   */
  public ACRDefSets()
  {
    loadDefinitions();
    loadSets();
  }

  /**
   * Loads all acr definitions into the defs map.  Both the id and short name
   * are mapped (each def may be retrieved by id or short name).
   */
  private void loadDefinitions()
  {
    // load the definitions from the database using the dao stuff
    DAOFactory factory = DAOFactoryBuilder.getDAOFactory(DAOFactory.DAO_MESDB);
    ACRDefDAO defDb = factory.getACRDefinitionDAO();
    List defList = defDb.selectACRDefinitions("ALL",null,true);

    // map the defs by name and id
    for (Iterator i = defList.iterator(); i.hasNext();)
    {
      ACRDefinition def = (ACRDefinition)i.next();
      defs.put(String.valueOf(def.getID()),def);
      defs.put(def.getShortName(),def);
    }
  }

  /**
   * Loads hierarchy node based lists, places them in the set hash mapped
   * by hierarchy node.
   */
  private void loadSets()
  {
    PreparedStatement ps = null;
    ResultSet         rs = null;

    try
    {
      connect();

      // select all def id's in the acr acct type table
      // ordered by linked hierarchy node then name
      String qs = "select t.def_id,                   "
                + "       t.hierarchy_node            "
                + "from   acr_acct_type t,            "
                + "       acrdef d                    "
                + "where  t.def_id = d.acrdef_seq_num "
                + "order by t.hierarchy_node, d.name  ";

      ps = con.prepareStatement(qs);

      rs = ps.executeQuery();

      // build lists of acr defs mapped to hierarchy nodes
      String lastNode = null;
      List defList = null;
      while (rs.next())
      {
        String hierNode   = rs.getString("hierarchy_node");
        String defId      = rs.getString("def_id");

        // make sure a def exists for the def id
        // and that it is enabled
        ACRDefinition def = (ACRDefinition)defs.get(defId);
        if (def != null && def.isOn())
        {
          // see if we need to fetch/add def list
          if (defList == null || !lastNode.equals(hierNode))
          {
            defList = (List)sets.get(hierNode);

            // if list doesn't exist for this node, make one
            if (defList == null)
            {
              defList = new ArrayList();
              sets.put(hierNode,defList);
            }
            lastNode = hierNode;
          }

          // add the def to the list
          defList.add(def);
        }
      }
    }
    catch (Exception e)
    {
      log.error("loadSets(): " + e);
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { ps.close(); } catch (Exception e) {}
      cleanUp();
    }
  }

  /**
   * Retrieves the def set whose hierarchy node is the closest parent to the
   * merchant account.  If no matching set is found then null is returned.
   */
  public List getAccountDefSet(String merchNum)
  {
    //first try and get a merchant-level def list
    List defList = (List)sets.get(merchNum);

    if(defList==null)
    {

      PreparedStatement ps = null;
      ResultSet         rs = null;

      try
      {
        connect();

        // select parents of the given merch num ordered closest to farthest
        String qs = "select   c.hierarchy_node                      "
                  + "from     ( select  distinct hierarchy_node     "
                  + "           from    acr_acct_type ) c,          "
                  + "         mif                       m,          "
                  + "         t_hierarchy               h           "
                  + "where    m.merchant_number = ?                 "
                  + "         and m.association_node = h.descendent "
                  + "         and h.ancestor = c.hierarchy_node     "
                  + "order by h.relation                            ";

        ps = con.prepareStatement(qs);
        ps.setString(1,merchNum);

        // grab first row in result to find closest parent
        rs = ps.executeQuery();
        if (rs.next())
        {
          defList = (List)sets.get(rs.getString("hierarchy_node"));
        }
      }
      catch (Exception e)
      {
        log.error("getAccountDefSet(): " + e);
      }
      finally
      {
        try { rs.close(); } catch (Exception e) {}
        try { ps.close(); } catch (Exception e) {}
        cleanUp();
      }
    }

    return defList;
  }

  /**
   * Convenience getter that takes a long version of the id.
   */
  public ACRDefinition getDef(long id)
  {
    return getDef(String.valueOf(id));
  }

  /**
   * Looks for a definition in the defs hash that is mapped to the given
   * String identifier.  The id may be the actual numeric id of of the def
   * or its short name.
   */
  public ACRDefinition getDef(String id)
  {
    return (ACRDefinition)defs.get(id);
  }
}