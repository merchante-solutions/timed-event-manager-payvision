package com.mes.acr;

import com.mes.forms.SelectorColumnDef;

public class SF_EquipSwap extends SF_EquipBase
{
  public SF_EquipSwap(String title)
  {
    super(title);
  }

  public void setupFrame()
  {

    addColumn(new SelectorColumnDef("col1","Serial No.",                100));
    addColumn(new SelectorColumnDef("col2","Description",               110));
    addColumn(new SelectorColumnDef("col3","Equipment Type",            200));
    addColumn(new SelectorColumnDef("col4","VNum/TID",                  100));
    addColumn(new SelectorColumnDef("col5","Terminal<br>App",           100));
    addColumn(new SelectorColumnDef("col6","Vendor ID",                 90));

    /*
    addColumn(new SelectorColumnDef("col1","Equipment Type",  200));
    addColumn(new SelectorColumnDef("col2","Serial No.",      105));
    addColumn(new SelectorColumnDef("col3","Description",     105));
    addColumn(new SelectorColumnDef("col4","Term App",        105));
    addColumn(new SelectorColumnDef("col5","Disposition",     200));
    */

  }
}
