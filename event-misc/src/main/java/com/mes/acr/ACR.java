package com.mes.acr;

import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
// log4j classes.
import org.apache.log4j.Logger;
import com.mes.database.ValueObjectBase;

/**
 * ACR class
 *
 * Represents a single Account Change Request.
 */
public class ACR extends ValueObjectBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACR.class);

  // constants
  public static final int       ACRSTATUS_UNDEFINED             = 0;
  protected static final int    ACRSTATUS_START                 = 1;
  public static final int       ACRSTATUS_ABORTED               = ACRSTATUS_START;
  public static final int       ACRSTATUS_INVALID               = 2;
  public static final int       ACRSTATUS_VALID                 = 3;
    // i.e. Valid ACR but not yet submitted for processing
  public static final int       ACRSTATUS_INPROCESS             = 4;
  public static final int       ACRSTATUS_COMPLETE              = 5;
  public static final int       ACRSTATUS_CANCELLED             = 6;
  protected static final int    ACRSTATUS_END                   = ACRSTATUS_CANCELLED;


  // data members
  protected long            id;
  protected String          merchantNum;    // primary parent id.
  protected long            appSeqNum;      // alternate parent id.
  protected long            acrdef_id;      // id of assoc. ACR definition
  protected ACRDefinition   definition;     // ref. to assoc. ACR definition
  protected int             status;         // ACRSTATUS_{...}
  protected Date            date_created;
  protected Date            date_last_modified;
  protected Hashtable       acr_items;      // key: ACRDefItem.name, val: {ACRItem}

  // NOTE: data member: 'date_last_modified' is maintained (updated) w/in this class

  // class methods

  public static String getStatusDescriptor(int status)
  {
    switch(status) {
      case ACRSTATUS_ABORTED:     return "Aborted";
      case ACRSTATUS_INVALID:     return "Invalid";
      case ACRSTATUS_VALID:       return "Valid";
      case ACRSTATUS_INPROCESS:   return "In Process";
      case ACRSTATUS_COMPLETE:    return "Complete";
      case ACRSTATUS_CANCELLED:   return "Cancelled";

      default:
      case ACRSTATUS_UNDEFINED:   return "Undefined";
    }
  }

  public static int toStatusEnum(String s)
  {
    return (s!=null && s.length()==1)? toStatusEnum(s.charAt(0)):ACRSTATUS_UNDEFINED;
  }

  public static int toStatusEnum(char c)
  {
    switch(c) {
      case 'x':   return ACRSTATUS_ABORTED;
      case 'a':   return ACRSTATUS_INVALID;
      case 'b':   return ACRSTATUS_VALID;
      case 'i':   return ACRSTATUS_INPROCESS;
      case 'c':   return ACRSTATUS_COMPLETE;
      case 'y':   return ACRSTATUS_CANCELLED;
      default:    return ACRSTATUS_UNDEFINED;
    }
  }

  // object methods

  // construction
  public ACR()
  {
    super(true);

    definition=null;
    date_created=null;
    date_last_modified=null;

    clear();
  }

  public boolean equals(Object obj)
  {
    if(!(obj instanceof ACR))
      return false;

    return (((ACR)obj).getID()==this.id);
  }

  public void clear()
  {
    id=-1L;
    merchantNum="";
    appSeqNum=-1L;

    acrdef_id=-1L;
    definition=null;  // NOTE: don't clear the assoc. def as it may be ref'd by someone else

    status=ACRSTATUS_UNDEFINED;

    if(date_created==null)
      date_created=new Date(0);
    else
      date_created.setTime(0);

    if(date_last_modified==null)
      date_last_modified=new Date(0);
    else
      date_last_modified.setTime(0);

    if(acr_items!=null)
      acr_items.clear();

    is_dirty=true;
  }

  public void removeEmptyValuedItems()
  {
    ACRItem acri;

    for(Enumeration e=acr_items.elements();e.hasMoreElements();) {
      acri = (ACRItem)e.nextElement();
      if(acri.getValue().equals(""))
        acr_items.remove(acri.getName());
    }

  }

  // accessors

  public boolean isValid()
  {
    return (  status==ACR.ACRSTATUS_VALID
              || status==ACR.ACRSTATUS_INPROCESS
              || status==ACR.ACRSTATUS_COMPLETE);
  }

  /**
   * getItemFromDefName()
   *
   * Retreives the ACR Item assoc. w/ the given ACR Def Item name.
   * ASSUMPTION: Both ACR Item name and assoc. ACR Def Item name are equal.
   */
  public ACRItem getItemFromDefName(String acrDefItemName)
  {
    ACRItem acri=null;

    if(definition==null)
      return null;

    for(Enumeration e=acr_items.elements();e.hasMoreElements();) {
      acri=(ACRItem)e.nextElement();
      if(acri.getName().equals(acrDefItemName))
        return acri;
    }

    return null;
  }

  public long           getID() { return id; }
  public String         getMerchantNum() { return merchantNum; }
  public long           getAppSeqNum() { return appSeqNum; }
  public long           getDefID() { return acrdef_id; }
  public ACRDefinition  getDefinition() { return definition; }
  public int            getStatus() { return status; }

  public Date           getDateCreated()
  {
    if(date_created==null) {
      date_created = new Date();
      is_dirty=true;
    }

    return date_created;
  }
  public Date           getDateLastModified()
  {
    if(date_last_modified==null) {
      if(date_created!=null)
        date_last_modified = new Date(date_created.getTime());
      else
        date_last_modified = new Date();
      is_dirty=true;
    }

    return date_last_modified;
  }

  public boolean itemHasValue(String name)
  {
    return acr_items.containsKey(name)? ((ACRItem)acr_items.get(name)).hasValue():false;
  }

  public boolean itemExists(String name)
  {
    if(acr_items==null)
      return false;

    return acr_items.containsKey(name);
  }

  public String         getACRValue(String name)
  {
    if(name==null)
      return "";

    if(acr_items==null)
      return "";

    return acr_items.containsKey(name)? ((ACRItem)acr_items.get(name)).getValue():"";
  }

  public int getNumItems()
  {
    return acr_items==null? 0:acr_items.size();
  }

  public Enumeration getItemEnumeration()
  {
    if(acr_items==null)
      acr_items = new Hashtable();

    return acr_items.elements();
  }

  public boolean definitionExists()
  {
    return (definition!=null);
  }

  // mutators

  public void setID(long v)
  {
    if(v==id)
      return;
    id=v;
    is_dirty=true;
  }

  /**
   * setParentIDs()
   *
   * Sets the parent IDs: app seq num and the merchant num.
   */
  public void setParentIDs(String merchantNum,long appSeqNum)
  {
    if((merchantNum==null || merchantNum.length()<1) && appSeqNum<0)
      return;

    if(merchantNum!=null)
      this.merchantNum=merchantNum;
    if(appSeqNum>=0)
      this.appSeqNum=appSeqNum;

    //date_last_modified = new Date();  // update dlm

    is_dirty=true;
  }

  public void setACRDefID(long acrdef_id)
  {
    if(this.acrdef_id==acrdef_id)
      return;

    this.acrdef_id=acrdef_id;
    definition=null;
    // NOTE: this op does not dirty this object
  }

  public void setDefinition(ACRDefinition v)
  {
    if(v==null || v==definition)
      return;

    //log.debug("ACR.setDefinition() - START");

    definition=v;
    acrdef_id=v.getID();  // keep in sync
    is_dirty=true;

    // IMPT: bind all current items to newly added definition items that match
    if(acr_items!=null) {

      ACRItem acri;
      ACRDefItem acrdi;
      boolean bFound;

      for(Enumeration e=acr_items.elements();e.hasMoreElements();) {
        bFound=false; // reset
        acri=(ACRItem)e.nextElement();
        log.debug("Attempting to bind ACR item '"+acri.getName()+"'...");
        for(Enumeration e2=definition.getItemEnumeration();e2.hasMoreElements();) {
          acrdi=(ACRDefItem)e2.nextElement();
          if(acrdi.getName().equals(acri.getName())) {
            // found match!
            acri.setItemDef(acrdi);
            bFound=true;
            //log.debug("Bound ACRDefItem to ACR Item by name '"+acri.getName()+"'.");
            break;
          }
        }
        if(!bFound)
          acri.clearItemDef();
      }
    }

  }

  /**
   * setACRDefItem()
   *
   * Sets ACR Def Item attaching it to ACR Definition data member.
   * If ACR Def item already present (det. by name match), overwrite only if specified to do so else no-op.
   */
  /*
  public void setACRDefItem(ACRDefItem acrdi,boolean bOverwrite)
  {
    if(acrdi==null || definition==null)
      return;

    ACRDefItem dacrdi=null;
    boolean bExistsByName=false;

    for(Enumeration e=definition.getItemEnumeration();e.hasMoreElements();) {
      dacrdi=(ACRDefItem)e.nextElement();
      if(dacrdi.getName().equals(acrdi.getName())) {
        bExistsByName=true;
        break;
      }
    }

    if(bExistsByName) {
      if(bOverwrite) {
        dacrdi.setID(acrdi.getID());
        dacrdi.setLabel(acrdi.getLabel());
        dacrdi.setDesc(acrdi.getDesc());
        dacrdi.setDataType(acrdi.getDataType());
        dacrdi.setIsRequired(acrdi.IsRequired());
        dacrdi.setDefaultValue(acrdi.getDefaultValue());
        dacrdi.setValidationParams(acrdi.getValidationParams());
      }
    } else {
      try {
        definition.setDefItem(acrdi);
      }
      catch(Exception e) {
        log.error("setACRDefItem EXCEPTION: '"+e.getMessage()+"'.");
        return;
      }
    }
  }
  */

  public void setStatus(int v)
  {
    if(v<ACRSTATUS_START || v>ACRSTATUS_END || v==status)
      return;
    status=v;
    is_dirty=true;
    //date_last_modified = new Date();  // update dlm
  }

  public void setDateCreated(Date v)
  {
    if(v==null || v==date_created)
      return;
    date_created=v;
    is_dirty=true;
  }

  public void setDateLastModified(Date v)
  {
    if(v==null || v==date_last_modified)
      return;
    date_last_modified=v;
    is_dirty=true;
  }

  /**
   * setACRItem()
   *
   * Adds ACR item name/value pair to this ACR.
   * Binds the item to the assoc. ACR Definition item IF found to exist in this object.
   *
   * @return  Ref. to the ACR Item that was set.
   */
  public ACRItem setACRItem(String name,String value)
  {
    if(name==null || value==null || name.length()==0)
      return null;

    ACRDefItem acrdi = null;
    boolean bFoundDefItem=false;

    // get def item from name if present
    if(definition!=null) {
      for(Enumeration e=definition.getItemEnumeration();e.hasMoreElements();) {
        if((acrdi=(ACRDefItem)e.nextElement()).getName().equals(name)) {
          bFoundDefItem=true;
          //log.debug("ACR.setACRItem() ACR Item '"+name+"' found related with ACR Def Item of same name.");
          break;
        }
      }
    }
    if(!bFoundDefItem) {
      //log.debug("ACR.setACRItem() ACR Item '"+name+"' NOT related to any ACR Def Item of associated ACR Defintition!");
      acrdi=null;
    }

    if(acr_items==null)
      acr_items = new Hashtable();

    ACRItem acri;

    if((acri=(ACRItem)acr_items.get(name))==null) {
      if(acrdi==null)
        acri=new ACRItem(name,value);     // unbounded def
      else
        acri=new ACRItem(acrdi,value);    // bounded def
      acr_items.put(acri.getName(),acri);
    } else {
      acri.setValue(value);
      acri.setItemDef(acrdi);
    }

    //date_last_modified = new Date();  // update dlm
    is_dirty=true;

    return acri;
  }

  public void removeItem(String name)
  {
    if(acr_items==null || name==null)
      return;

    if(acr_items.containsKey(name))
      acr_items.remove(name);
  }


  public String toString()
  {
    final String nl = System.getProperty("line.separator");

    StringBuffer sb = new StringBuffer(256);

    sb.append(nl);
    sb.append("ACR: ");

    sb.append("id="+id);
    sb.append(", merchantNum="+merchantNum);
    sb.append(", appSeqNum="+appSeqNum);
    sb.append(", status="+status);
    sb.append(", date_created="+date_created);
    sb.append(", date_last_modified="+date_last_modified);
    sb.append(" - "+(is_dirty? "DIRTY":"CLEAN"));

    sb.append(nl);
    if(definition!=null) {
      sb.append("definition ID="+definition.getID());
      sb.append(",Name"+definition.getName());
    } else
      sb.append(" *NO ASSOC. DEFINITION*");

    sb.append(nl);
    if(acr_items!=null && acr_items.size()>0) {
      sb.append("ITEMS:");
      ACRItem acri=null;
      for(Enumeration e=acr_items.elements();e.hasMoreElements();) {
        sb.append(nl+"  - ");
        sb.append(((ACRItem)e.nextElement()).toString());
      }
    } else {
      sb.append(" *NO ACR ITEMS.*");
    }

    return sb.toString();
  }

} // class ACR
