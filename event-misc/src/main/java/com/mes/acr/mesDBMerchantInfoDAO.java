package com.mes.acr;

// log4j classes.
import org.apache.log4j.Logger;

/**
 * mesDBMerchantInfoDAO class.
 *
 * MerchantInfoDAO implementation for the MES db.
 */
class mesDBMerchantInfoDAO implements MerchantInfoDAO
{
  // create class log category
  static Logger log = Logger.getLogger(mesDBMerchantInfoDAO.class);

  // constants

  // data members
  protected ACRMESDB acrMesDb;

  // construction
  public mesDBMerchantInfoDAO()
  {
    acrMesDb = new ACRMESDB();
  }

  // class methods
  // (none)

  // object methods

  public MerchantInfo findMerchantInfo(String merchant_num)
  {
    MerchantInfo merchantInfo = new MerchantInfo();

    try {
      if(!acrMesDb.load(merchant_num,merchantInfo))
        return null;
    }
    catch(Exception e) {
      log.error("mesDBMerchantInfoDAO.findMerchantInfo() EXCEPTION: '"+e.getMessage()+"'.");
      merchantInfo=null;
    }

    return merchantInfo;
  }
} // class mesDBMerchantInfoDAO
