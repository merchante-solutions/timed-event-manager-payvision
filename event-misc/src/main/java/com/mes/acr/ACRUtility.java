/*************************************************************************

  ACRUtility

  Designed to hold multi-purpose ACR functionality -

  Copyright (C) 2000-2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.acr;

import java.util.Enumeration;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.tools.EquipmentKey;

public class ACRUtility
{

  /**
   * Log4J Logger
   */
  static Logger log = Logger.getLogger(ACRUtility.class);


  public static String ORDER_FIELD_TAG = "orderId";


  /**
   * put here in the parent class as those new CBT
   * beans will use this instead of the fall through
   * reliance on legacy equipment component
   */
  public static Vector processCallTaggedEquipment(ACR acr)
  {
    log.debug("START processCallTaggedEquipment()");

    if(acr==null)
    {
      return null;
    }

    ACRItem acrItem;
    Vector ctList = null;

    for(Enumeration myEnum = acr.getItemEnumeration(); myEnum.hasMoreElements();)
    {

      acrItem = (ACRItem)myEnum.nextElement();
      try
      {
        String s = acrItem.getName();
        //check to see if it's equipment-related
        if( ! (s.indexOf(ACRDataBeanBase.CALLTAG_EQUIPMENT_NAME) == 0) )
        {
          continue;
        }

        //check for both single and multi selector versions
        String v = acrItem.getValue();
        if( s.equals(v) )
        {
          String sn = acr.getACRValue(s + "_serNum");
          log.debug("CREATING KEY...."+sn);
          EquipmentKey key = new EquipmentKey(null,sn);
          if(ctList==null)
          {
            ctList = new Vector();
          }
          ctList.add(key);
        }

      }
      catch(Exception e)
      {
        log.error("processCallTaggedEquipment() EXCEPTION: "+e.toString()+".  Continuing.");
        continue;
      }
    }
    log.debug("END processCallTaggedEquipment()");
    return ctList;
  }

  public static String generateOrderLink(String orderId)
  {
    if(orderId!=null)
    {
      StringBuffer sb = new StringBuffer("<a href='/srv/dst?action=viewOrder&ordId=");
      sb.append(orderId);
      sb.append("'>Order Number: ");
      sb.append(orderId);
      sb.append("</a>");
      return sb.toString();
    }
    else
    {
      return "";
    }
  }

}