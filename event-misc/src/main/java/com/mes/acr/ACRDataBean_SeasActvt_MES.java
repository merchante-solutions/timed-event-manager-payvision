package com.mes.acr;

import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.Condition;
import com.mes.forms.DateStringField;
import com.mes.forms.Field;
import com.mes.forms.RadioField;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DBGrunt;

public class ACRDataBean_SeasActvt_MES extends ACRDataBeanBase
{
  // log4j initialization
  static Logger log =
    Logger.getLogger(ACRDataBean_SeasActvt_MES.class);

  public ACRDataBean_SeasActvt_MES()
  {
  }

  public ACRDataBean_SeasActvt_MES(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    setInitialQueue(MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP);
  }

  /**
   * Creates fields for the acr detail section specific to this acr.
   */
  protected void createExtendedFields()
  {
    try
    {
      log.debug("creating extended fields." );

      // activate or deactivate radio
      Field actOrDeact =
        new RadioField("actOrDeact","Activate or Deactivate",
          new String[][]
          {
            { "activate",   "Activate for Season"   },
            { "deactivate", "Deactivate for Season" },
          });
      fields.add(actOrDeact);

      Date currentDate = DBGrunt.getCurrentDate();
      String dateString = DateTimeFormatter.getFormattedDate(currentDate,"MM/dd/yyyy");

      // activation date, required if activating
      Field activateDate =
        new DateStringField("activateDate","Date to Activate",true,false);
      activateDate.setData(dateString);
      fields.add(activateDate);

      Condition isActivate = new FieldValueCondition(actOrDeact,"activate");
      activateDate
        .addValidation(new ConditionalRequiredValidation(isActivate));
      ((DateStringField)activateDate).setCurrentDateValidation();

      // yes no field for equipment deployment
      Field deployEquip =
        new RadioField("deployEquip","Deploy Equipment",
          new String[][]
          {
            { "no",   "No, don't deploy equipment"  },
            { "yes",  "Yes, deploy equipment" },
          });
      fields.add(deployEquip);

      // deactivation date, required if deactivating
      Field deactivateDate =
        new DateStringField("deactivateDate","Date to Deactivate",true,false);
      deactivateDate.setData(dateString);
      fields.add(deactivateDate);

      Condition isDeactivate =
        new FieldValueCondition(actOrDeact,"deactivate");
      deactivateDate
        .addValidation(new ConditionalRequiredValidation(isDeactivate));
      ((DateStringField)deactivateDate).setCurrentDateValidation();

      // yes no field for equipment deployment
      Field pickupEquip =
        new RadioField("pickupEquip","Pick-up Equipment",
          new String[][]
          {
            { "no",   "No, don't pick-up equipment"  },
            { "yes",  "Yes, pick-up equipment" }
          });
      fields.add(pickupEquip);


      fields.setHtmlExtra("class=\"formText\"");
    }
    catch (Exception e)
    {
      log.error("createExtendedFields(): "  + e);
    }
  }

  public void setDefaults()
  {
  }

  /**
   * Returns title for this acr.
   */
  public String getHeadingName()
  {
    return "Activate or Deactivate Seasonal Merchant";
  }

  /**
   * Convenience methods for post submission mode
   */
  public boolean isActivation()
  {
    return getACRValue("actOrDeact").toLowerCase().equals("activate");
  }
  public boolean isDeactivation()
  {
    return getACRValue("actOrDeact").toLowerCase().equals("deactivate");
  }

  /**
   * Disable editable comments during submission.
   */
  public boolean hasEditableComments()
  {
    return !editable && super.hasEditableComments();
  }
}
