package com.mes.acr;

import org.apache.log4j.Logger;
import com.mes.equipment.EquipSelectorItem;
import com.mes.forms.Condition;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HtmlRenderable;
import com.mes.forms.Validation;
import com.mes.tmg.util.PartDispositionTable;
import com.mes.tools.DropDownTable;

public class SI_EquipDisp extends EquipSelectorItem
  implements ACRTranslator
{
  static Logger log = Logger.getLogger(SI_EquipDisp.class);

  public SI_EquipDisp()
  {
  }

  public void init()
  {
    if (type == T_STANDARD)
    {
      setColumnRenderer("col1", new HtmlRenderable()
        { public String renderHtml() { return getSerialNum(); } } );
      setColumnRenderer("col2", new HtmlRenderable()
        { public String renderHtml() { return getDescription(); } } );
      setColumnRenderer("col3", new HtmlRenderable()
        { public String renderHtml() { return getDisposition(); } } );
    }
    else if (type == T_OTHER)
    {
      Field serialNum = new Field(value + "_serNum","Serial number",20,10,true);
      setColumnRenderer("col1",serialNum);
      fields.add(serialNum);

      Field description = new Field(value + "_description","Equipment description",60,20,true);
      setColumnRenderer("col2",description);
      fields.add(description);

      Field partNum = new Field(value + "_disposition","Disposition",20,10,true);
      setColumnRenderer("col3",partNum);
      fields.add(partNum);
    }

    Field newDispField
      = new DropDownField(value + "_newDisp","New Disposition",
                          new PartDispositionTable(false),true);
    setColumnRenderer("col4",newDispField);
    fields.add(newDispField);

    Field chrgAmtField
      = new Field(value + "_chrgAmt","Charge Amount",100,15,true);
    setColumnRenderer("col5",chrgAmtField);
    fields.add(chrgAmtField);
  }

  public void setControlField(Field controlField)
  {
    super.setControlField(controlField);
    Condition vc = new FieldValueCondition(controlField,value);
    Validation required = new ConditionalRequiredValidation(vc);
    fields.addGroupValidation(required);
  }

  public void doPush(ACR acr)
  {
    log.debug("pretending to push acr item data...");
  }

  public void doPull(ACR acr)
  {
    try
    {
      //newDisp and reason are already fields...
      acr.setACRItem(value+"_serNum",getSerialNum());
      acr.setACRItem(value+"_description",getDescription());
      acr.setACRItem(value+"_disposition",getDisposition());
    }
    catch(Exception e)
    {
      //do nothing
    }
  }

  protected static final class _dropdowntable extends DropDownTable
  {
    public _dropdowntable()
    {
      super();
      addElement("","Select One");
      addElement("Change to Purchased", "Change to Purchased");
      addElement("Change to Rental", "Change to Rental");
    }
  }
}
