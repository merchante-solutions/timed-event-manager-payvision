package com.mes.acr;

import java.util.BitSet;
import java.util.Date;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Logger;

/**
 * mesDBACRDAO class.
 *
 * ACRDAO implementation for the MES db.
 */
class mesDBACRDAO implements ACRDAO
{
  // create class log category
  static Logger log = Logger.getLogger(mesDBACRDAO.class);

  // constants

  // data members
  protected ACRMESDB acrMesDb;

  // construction
  public mesDBACRDAO()
  {
    acrMesDb = new ACRMESDB();
  }

  // class methods
  // (none)

  // object methods

  public ACR createACR()
  {
    final long id = acrMesDb.getNextAvailID(ACRMESDB.BIT_ACR_PRIMARY);
    if(id==-1L)
      return null;

    ACR acr = new ACR();
    acr.setID(id);
    acr.setDateCreated(new Date());

    return acr;
  }

  public long insertACR(ACR acr)
  {
    log.error("mesDBACRDAO.insertACR() NOT IMPLEMENTED.");
    return -1L;
  }

  public boolean deleteACR(ACR acr)
  {
    if(acr.getID()<0) {
      log.error("Unable to delete ACR: No ID specified.");
      return false;
    }

    BitSet dbBits = new BitSet(4);
    dbBits.set(ACRMESDB.BIT_ACR_PRIMARY);
    // ASSUMPTION: ACR items assumed to be cascade deleted at db level

    return acrMesDb.delete(dbBits,acr.getID());
  }

  public ACR findACR(long pkid)
  {
    ACR acr = new ACR();

    // retreive primary and all assoc. items
    BitSet dbBits = new BitSet(4);
    dbBits.set(ACRMESDB.BIT_ACR_PRIMARY);
    dbBits.set(ACRMESDB.BIT_ACR_ITEM);

    try {
      if(!acrMesDb.load(pkid,acr,dbBits))
        return null;
    }
    catch(Exception e) {
      log.error("mesDBACRDAO.findACR() EXCEPTION: '"+e.getMessage()+"'.");
      acr=null;
    }

    return acr;
  }

  public boolean updateACR(ACR acr)
  {
    if(acr.getID()<0) {
      log.error("Unable to update ACR: No ID specified.");
      return false;
    }

    return persistACR(acr);
  }

  public boolean persistACR(ACR acr)
  {
    BitSet dbBits = new BitSet(4);
    dbBits.set(ACRMESDB.BIT_ACR_PRIMARY);
    dbBits.set(ACRMESDB.BIT_ACR_ITEM);

    return acrMesDb.persist(acr,dbBits,false);
  }

  public Vector selectACRs(String fldNme,String fldVal,boolean bLoadItems)
  {
    BitSet dbBits = new BitSet(4);
    dbBits.set(ACRMESDB.BIT_ACR_PRIMARY);
    if(bLoadItems)
      dbBits.set(ACRMESDB.BIT_ACR_ITEM);

    try {
      return acrMesDb.load(fldNme,fldVal,dbBits);
    }
    catch(Exception e) {
      log.error("mesDBACRDAO.selectACRs() EXCEPTION: '"+e.getMessage()+"'.");
      return null;
    }
  }

  /*
  public MerchantRecord getParentInfo(String merch_num)
  {
    try {
      MerchantRecord mr = new MerchantRecord();
      mr.getData(acrMesDb.getAppSeqNumFromMerchantNum(merch_num));
      return mr;
    }
    catch(Exception e) {
      return null;
    }
  }
  */

} // class mesDBACRDAO
