/*@lineinfo:filename=ACRDataBean_CardAdds*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/acr/ACRDataBean_CardAdds.sqlj $

  Description:


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 9/24/04 12:00p $
  Version            : $Revision: 17 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.acr;

import java.sql.ResultSet;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.app.BusinessBase;
import com.mes.constants.MesQueues;
import com.mes.forms.CheckboxField;
import com.mes.forms.ConstantField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;


public class ACRDataBean_CardAdds extends ACRDataBeanBase
{


  //*************************************************************************
  // Main Class
  //*************************************************************************

  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_CardAdds.class);

  // construction
  public ACRDataBean_CardAdds()
  {
  }

  public ACRDataBean_CardAdds(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  protected void setDefaults()
  {
    // no-op
  }

  protected void createExtendedFields()
  {
    try
    {
      if( (editable == true) && (acr != null) )
      {
        ACRDefinition   acrd    = acr.getDefinition();
        ACRDefItem      acrdi   = null;
        Field           field   = null;
        String          fldnme  = null;

        String[][] ApplyExistingRadioButtons =
        {
           {"Apply for card type.", "Apply for card type."}
          ,{"Existing card type (Merchant Number required)." , "Existing card type (Merchant Number required)."}
        };

        String[][] AmexSplitRadioButtons =
        {
          { "Yes"  , "Yes"  },
          { "No"   , "No"   }
        };

        Vector cardAdds = new Vector(6,1);

        acrdi=acrd.getACRDefItem("discover");
        field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),false);
        cardAdds.addElement(field);
        fields.add(field);
        acrdi=acrd.getACRDefItem("jcb");
        field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),false);
        cardAdds.addElement(field);
        fields.add(field);
        acrdi=acrd.getACRDefItem("diners");
        field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),false);
        cardAdds.addElement(field);
        fields.add(field);
        acrdi=acrd.getACRDefItem("debit");
        field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),false);
        cardAdds.addElement(field);
        fields.add(field);
        acrdi=acrd.getACRDefItem("ebt");
        field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),false);
        cardAdds.addElement(field);
        fields.add(field);
        acrdi=acrd.getACRDefItem("americanExpress");
        field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),false);
        cardAdds.addElement(field);
        fields.add(field);
        acrdi=acrd.getACRDefItem("americanExpressNum");
        field = ACRDefItemToField(acrdi);
        fields.add(field);
        acrdi=acrd.getACRDefItem("discoverNum");
        field = ACRDefItemToField(acrdi);
        fields.add(field);
        acrdi=acrd.getACRDefItem("jcbNum");
        field = ACRDefItemToField(acrdi);
        fields.add(field);
        acrdi=acrd.getACRDefItem("dinersNum");
        field = ACRDefItemToField(acrdi);
        fields.add(field);
        acrdi=acrd.getACRDefItem("ebtNum");
        field = ACRDefItemToField(acrdi);
        fields.add(field);

        acrdi=acrd.getACRDefItem("authfee_amex");
        field = ACRDefItemToField(acrdi);
        fields.add(field);
        acrdi=acrd.getACRDefItem("authfee_disc");
        field = ACRDefItemToField(acrdi);
        fields.add(field);
        acrdi=acrd.getACRDefItem("authfee_diners");
        field = ACRDefItemToField(acrdi);
        fields.add(field);
        acrdi=acrd.getACRDefItem("authfee_jcb");
        field = ACRDefItemToField(acrdi);
        fields.add(field);
        acrdi=acrd.getACRDefItem("authfee_ebt");
        field = ACRDefItemToField(acrdi);
        fields.add(field);
        acrdi=acrd.getACRDefItem("authfee_debit");
        field = ACRDefItemToField(acrdi);
        fields.add(field);

        // amex specific
        acrdi=acrd.getACRDefItem("applyOrExistingCard_amex");
        field = new RadioButtonField("applyOrExistingCard_amex", ApplyExistingRadioButtons, -1, true, "Please specify whether applying for new card type or altering existing card type." );
        fields.add(field);
        acrdi=acrd.getACRDefItem("amexSplit");
        field = new RadioButtonField("amexSplit", AmexSplitRadioButtons, -1, !acrdi.isRequired(), "Please specify Split Dial for AmEx or provide AmEx merchant number." );
        fields.add(field);

        // discover specific
        acrdi=acrd.getACRDefItem("applyOrExistingCard_disc");
        field = new RadioButtonField("applyOrExistingCard_disc", ApplyExistingRadioButtons, -1, true, "Please specify whether applying for new card type or altering existing card type" );
        fields.add(field);

        // add valutec options
        acrdi = acrd.getACRDefItem("valutec");
        field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),false);
        cardAdds.addElement(field);
        fields.add(field);
        acrdi = acrd.getACRDefItem("valutecTermId");
        field = ACRDefItemToField(acrdi);
        field.setMinLength(5);
        field.addValidation( new BusinessBase.ValutecTermIdValidation( fields.getField("valutec") ) );
        fields.add(field);
        acrdi = acrd.getACRDefItem("valutecMerchId");
        field = ACRDefItemToField(acrdi);
        fields.add(field);
        acrdi = acrd.getACRDefItem("valutecEquip");
        field = new DropDownField(acrdi.getName(),acrdi.getLabel(),new ValutecEquipmentTable(),true);
        field.addValidation( new EquipmentRequiredValidation( fields.getField("valutec") ) );
        fields.add(field);

        // add certegy check program
        acrdi = acrd.getACRDefItem("check");
        field = new CheckboxField(acrdi.getName(),acrdi.getLabel(),false);
        cardAdds.addElement(field);
        fields.add(field);
        acrdi = acrd.getACRDefItem("checkProvider");
        field = new DropDownField(acrdi.getName(),acrdi.getLabel(),new BusinessBase.CheckProviderTable(false), true);
        field.addValidation( new BusinessBase.CheckProviderValidation( fields.getField("check"), null ) );
        fields.add(field);
        acrdi = acrd.getACRDefItem("checkMerchId");
        field = ACRDefItemToField(acrdi);
        field.addValidation( new BusinessBase.WelcomeCheckValidation( fields.getField("checkProvider") ) );
        field.addValidation( new BusinessBase.ElecCheckValidation( fields.getField("checkProvider") ) );
        fields.add(field);
        acrdi = acrd.getACRDefItem("checkTermId");
        field = ACRDefItemToField(acrdi);
        field.addValidation( new BusinessBase.ElecCheckTermIdValidation( fields.getField("checkProvider") ) );
        fields.add(field);
        acrdi = acrd.getACRDefItem("checkEquip");
        field = new DropDownField(acrdi.getName(),acrdi.getLabel(),new CertegyCheckEquipmentTable(),true);
        field.addValidation( new EquipmentRequiredValidation( fields.getField("check") ) );
        fields.add(field);

        // require "at least one" card add be checked
        fields.addValidation(new FieldBean.AtLeastOneValidation("At least one Card Type must be selected to proceed.",cardAdds));

        fields.addValidation(new AmExValidation());
        fields.addValidation(new DiscoverValidation());
        fields.addValidation(new DinersValidation());
        fields.addValidation(new JCBValidation());
        fields.addValidation(new EBTValidation());
      }
      else
      {
        System.out.println("here");//@
        System.out.println(getACRValue("checkEquip"));//@
        fields.add( new ConstantField("checkEquip", getEquipDesc( getACRValue("checkEquip") ), "formFields" ) );
        System.out.println("there");//@
      }
    }
    catch(Exception e)
    {
      logEntry("createExtendedFields()",e.toString());
    }
  }

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}

      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP:
      case MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Declined by Amex or Discover"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }

  public String getEquipDesc( String pn )
  {
    String    retVal      = null;

    try
    {
      connect();
      /*@lineinfo:generated-code*//*@lineinfo:300^7*/

//  ************************************************************
//  #sql [Ctx] { select  eq.equip_descriptor 
//          from    equipment   eq
//          where   eq.equip_model = :pn
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  eq.equip_descriptor  \n        from    equipment   eq\n        where   eq.equip_model =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRDataBean_CardAdds",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,pn);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:305^7*/
    }
    catch(Exception e)
    {
      retVal = pn;
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }

  public String getHeadingName()
  {
    return "Add Card Types to Merchant Account";
  }

  public String renderHtml(String fname)
  {
    BusinessBase.CheckProviderTable   cpTable     = null;
    String                            retVal      = null;
    String                            value       = null;

    if( inStatusMode() )
    {
      value = getACRValue(fname);

      if ( fname.equals("checkProvider") )
      {
        cpTable = new BusinessBase.CheckProviderTable(false);
        retVal  = cpTable.getDescription(value);
      }
      else if ( fname.equals("checkEquip") || fname.equals("valutecEquip") )
      {
        retVal = getEquipDesc( value );
      }
    }

    if ( retVal == null )
    {
      retVal = super.renderHtml(fname);
    }

    return( retVal );
  }

  // DROP DOWN CLASSES
  public class CertegyCheckEquipmentTable extends DropDownTable
  {
    public CertegyCheckEquipmentTable()
      throws java.sql.SQLException
    {
      ResultSetIterator       it        = null;
      ResultSet               resultSet = null;

      try
      {
        connect();

        addElement("","select one");

        /*@lineinfo:generated-code*//*@lineinfo:367^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_model,
//                    equip_descriptor
//            from    equipment  eq
//            where   nvl(eq.certegy_check_supported,'N') = 'Y'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_model,\n                  equip_descriptor\n          from    equipment  eq\n          where   nvl(eq.certegy_check_supported,'N') = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.acr.ACRDataBean_CardAdds",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.acr.ACRDataBean_CardAdds",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:373^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); }catch(Exception e){}
        cleanUp();
      }
    }
  }

  public class ValutecEquipmentTable extends DropDownTable
  {
    public ValutecEquipmentTable()
      throws java.sql.SQLException
    {
      ResultSetIterator       it        = null;
      ResultSet               resultSet = null;

      try
      {
        connect();

        addElement("","select one");

        /*@lineinfo:generated-code*//*@lineinfo:405^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_model,
//                    equip_descriptor
//            from    equipment  eq
//            where   nvl(eq.valutec_gift_card_supported,'N') = 'Y'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_model,\n                  equip_descriptor\n          from    equipment  eq\n          where   nvl(eq.valutec_gift_card_supported,'N') = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.acr.ACRDataBean_CardAdds",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.acr.ACRDataBean_CardAdds",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:411^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); }catch(Exception e){}
        cleanUp();
      }
    }
  }

  //*************************************************************************
  // Validations
  //*************************************************************************
  protected class EquipmentRequiredValidation implements Validation
  {
    protected Field       CardAccepted      = null;
    protected String      ErrorMessage      = null;

    public EquipmentRequiredValidation( Field accepted )
    {
      CardAccepted = accepted;
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }

    public boolean validate(String fdata)
    {
      int           ptype     = 0;

      ErrorMessage = null;

      try
      {
        if ( CardAccepted.getData().equals("y") )
        {
          if ( fdata == null || fdata.equals("") )
          {
            ErrorMessage = "Must specify the type of equipment";
          }
        }
        else
        {
          if ( fdata != null && !fdata.equals("") )
          {
            ErrorMessage = "Card type not accepted, clear equipment field";
          }
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Invalid equipment setting";
      }
      return( (ErrorMessage == null) );
    }
  }

  protected class AmExValidation implements Validation
  {
    private String errorText = "";

    public AmExValidation()
    {
    }

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      Field fld,fld2;

      fld=fields.getField("americanExpress");
      if(fld==null || fld.isBlank())
        return true;

      // selected so do related edits

      // ensure split dial specified
      fld=fields.getField("amexSplit");
      if(fld==null || fld.isBlank()) {
        errorText="Please specify Split Dial for AmEx.";
        return false;
      }

      // ensure card status specified
      fld=fields.getField("applyOrExistingCard_amex");
      if(fld==null || fld.isBlank()) {
        errorText="Please specify the AmEx Card Status.";
        return false;
      }

      // ensure merchant number specified if existing card type
      fld2=fields.getField("americanExpressNum");
      if(fld2==null || (fld.getData().startsWith("Existing card type") && fld2.isBlank())) {
        errorText="Please specify the Merchant Number for American Express.";
        return false;
      }

      return true;
    }

  } // class AmExValidation

  protected class DiscoverValidation implements Validation
  {
    private String errorText = "";

    public DiscoverValidation()
    {
    }

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      Field fld,fld2;

      fld=fields.getField("discover");
      if(fld==null || fld.isBlank())
        return true;

      // selected so do related edits

      // ensure card type specified
      fld=fields.getField("applyOrExistingCard_disc");
      if(fld==null || fld.isBlank()) {
        errorText="Please specify the Discover Card Type.";
        return false;
      }

      // ensure merchant number specified if existing card type
      fld2=fields.getField("discoverNum");
      if(fld2==null || (fld.getData().startsWith("Existing card type") && fld2.isBlank())) {
        errorText="Please specify the Merchant Number for Discover.";
        return false;
      }

      return true;
    }

  } // class DiscoverValidation

  protected class DinersValidation implements Validation
  {
    private String errorText = "";

    public DinersValidation()
    {
    }

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      Field fld,fld2;

      fld=fields.getField("diners");
      if(fld==null || fld.isBlank())
        return true;

      // selected so do related edits

      // ensure merchant number specified if existing card type
      fld=fields.getField("dinersNum");
      if(fld==null || fld.isBlank()) {
        errorText="Please specify the Merchant Number for Diners.";
        return false;
      }

      return true;
    }

  } // class DinersValidation

  protected class JCBValidation implements Validation
  {
    private String errorText = "";

    public JCBValidation()
    {
    }

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      Field fld,fld2;

      fld=fields.getField("jcb");
      if(fld==null || fld.isBlank())
        return true;

      // selected so do related edits

      // ensure merchant number specified if existing card type
      fld=fields.getField("jcbNum");
      if(fld==null || fld.isBlank()) {
        errorText="Please specify the Merchant Number for JCB.";
        return false;
      }

      return true;
    }

  } // class JCBValidation

  protected class EBTValidation implements Validation
  {
    private String errorText = "";

    public EBTValidation()
    {
    }

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      Field fld,fld2;

      fld=fields.getField("ebt");
      if(fld==null || fld.isBlank())
        return true;

      // selected so do related edits

      // ensure merchant number specified if existing card type
      fld=fields.getField("ebtNum");
      if(fld==null || fld.isBlank()) {
        errorText="Please specify the FCS Number for EBT.";
        return false;
      }

      return true;
    }

  } // class EBTValidation
}/*@lineinfo:generated-code*/