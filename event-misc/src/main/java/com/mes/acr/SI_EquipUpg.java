package com.mes.acr;

import org.apache.log4j.Logger;
import com.mes.equipment.EquipSelectorItem;
import com.mes.forms.CheckField;
import com.mes.forms.Condition;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.HtmlRenderable;
import com.mes.forms.Validation;

public class SI_EquipUpg extends EquipSelectorItem
  implements ACRTranslator
{
  static Logger log = Logger.getLogger(SI_EquipUpg.class);

  //made static constant so that ACRbean can use it to find
  //applicable call tagged info
  public final static String CALLTAG_DESC = "_callTag";

  public SI_EquipUpg()
  {
  }

  public void init()
  {

    if (type == T_STANDARD)
    {
      setColumnRenderer("col1", new HtmlRenderable()
        { public String renderHtml() { return getSerialNum(); } } );
      setColumnRenderer("col2", new HtmlRenderable()
        { public String renderHtml() { return getDescription(); } } );
      setColumnRenderer("col3", new HtmlRenderable()
        { public String renderHtml() { return getEquipType(); } } );
    }
    else if (type == T_OTHER)
    {
      Field serialNum = new Field(value + "_serNum","Serial number",20,8,true);
      setColumnRenderer("col1",serialNum);
      fields.add(serialNum);

      Field description
        = new Field(value + "_description","Equipment description",60,10,true);
      setColumnRenderer("col2",description);
      fields.add(description);

      Field equipType = new DropDownField(value + "_equipType",
        "Equipment type",
        TableFactory.getDropDownTable(TableFactory.EQUIP_TYPE),true);
      equipType.addHtmlExtra("class=\"formField\"");
      setColumnRenderer("col3",equipType);
      fields.add(equipType);
    }

    Field vNum = new Field(value + "_VNum","VNum/TID",20,6,true);
    setColumnRenderer("col4",vNum);
    fields.add(vNum);

    Field termApp = new Field(value + "_termApp","Terminal Application",20,6,true);
    setColumnRenderer("col5",termApp);
    fields.add(termApp);

    Field vendorId = new Field(value + "_vendorId","Vendor ID",20,6,true);
    setColumnRenderer("col6",vendorId);
    fields.add(vendorId);

    CheckField callTag = new CheckField(value + CALLTAG_DESC,"","Y",false);
    setColumnRenderer("col7",callTag);
    fields.add(callTag);

    //CheckField repairForm = new CheckField(value + "_repairForm","","Y",false);
    //setColumnRenderer("col8",repairForm);
    //fields.add(repairForm);

  }

  public void setControlField(Field controlField)
  {
    super.setControlField(controlField);
    Condition vc = new FieldValueCondition(controlField,value);
    Validation required = new ConditionalRequiredValidation(vc);
    fields.addGroupValidation(required);
  }

  public void doPush(ACR acr)
  {
    log.debug("pretending to push acr item data...");
  }

  public void doPull(ACR acr)
  {
    try
    {
      acr.setACRItem(value+"_serNum",getSerialNum());
      acr.setACRItem(value+"_description",getDescription());
      acr.setACRItem(value+"_equipType",getEquipType());
    }
    catch(Exception e)
    {
      //do nothing
    }
  }
}