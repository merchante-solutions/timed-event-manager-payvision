/*@lineinfo:filename=ACRDataBean_PriceChng_CBT*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.acr;

import java.sql.ResultSet;
import java.util.ArrayList;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.forms.CheckboxField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class ACRDataBean_PriceChng_CBT extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_PriceChng_CBT.class);

  private int APP_TYPE = mesConstants.APP_TYPE_CBT_NEW; //for dropdown generation purposes
  private ArrayList interchangeTypes=null;

  // construction
  public ACRDataBean_PriceChng_CBT()
  {
  }

  public ACRDataBean_PriceChng_CBT(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  protected void setDefaults()
  {
    // no-op
  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;
    String fldnme;

    field = new NumberField("discRate_from",7,7,true,2);
    field.setShowName("Discount Rate Credit (From)");
    fields.add(field);
    field = new NumberField("discRate_to",7,7,true,2);
    field.setShowName("Discount Rate Credit (To)");
    fields.add(field);
    field = new NumberField("discRate_check_from",7,7,true,2);
    field.setShowName("Discount Rate Check (From)");
    fields.add(field);
    field = new NumberField("discRate_check_to",7,7,true,2);
    field.setShowName("Discount Rate Check (To)");
    fields.add(field);
    field = new NumberField("perItem_from",7,7,true,2);
    field.setShowName("Per Item Credit (From)");
    fields.add(field);
    field = new NumberField("perItem_to",7,7,true,2);
    field.setShowName("Per Item Credit (To)");
    fields.add(field);
    field = new NumberField("perItem_check_from",7,7,true,2);
    field.setShowName("Per Item Check (From)");
    fields.add(field);
    field = new NumberField("perItem_check_to",7,7,true,2);
    field.setShowName("Per Item Check (To)");
    fields.add(field);
    field = new NumberField("minDisc_from",7,7,true,2);
    field.setShowName("Minimum Discount (From)");
    fields.add(field);
    field = new NumberField("minDisc_to",7,7,true,2);
    field.setShowName("Minimum Discount (To)");
    fields.add(field);

    String[][] interchngTypesFrom = getInterchngTypes();
    String[][] interchngTypesTo = getInterchngTypes();

    field = new RadioButtonField("interchang_from_type",interchngTypesFrom,0,false,"Please specify an Interchange Change From Type.");
    field.setShowName("Interchange Change From Type");
    fields.add(field);
    field = new RadioButtonField("interchang_to_type",interchngTypesTo,-1,true,"Please specify an Interchange Change To Type.");
    field.setShowName("Interchange Change To Type");
    fields.add(field);

    DropDownTable ddtIntcRtl = new InterchangeRetailDropDownTable();
    field = new DropDownField("intc_retail_from",ddtIntcRtl,true);
    field.setShowName("Interchange Retail From Selection");
    fields.add(field);
    field = new DropDownField("intc_retail_to",ddtIntcRtl,true);
    field.setShowName("Interchange Retail To Selection");
    fields.add(field);

    DropDownTable ddtIntcMoto = new InterchangeMotoDropDownTable();
    field = new DropDownField("intc_moto_from",ddtIntcMoto,true);
    field.setShowName("Interchange MOTO From Selection");
    fields.add(field);
    field = new DropDownField("intc_moto_to",ddtIntcMoto,true);
    field.setShowName("Interchange MOTO To Selection");
    fields.add(field);

    DropDownTable ptIntc = new PassThroughInterchangeDropDownTable();
    field = new DropDownField("pt_intc_from",ptIntc,true);
    field.setShowName("From Pass Thru Interchange Selection");
    fields.add(field);
    field = new DropDownField("pt_intc_to",ptIntc,true);
    field.setShowName("To Pass Thru Interchange Selection");
    fields.add(field);

    field = new CheckboxField("af_visamc","Visa/MC",false);
    field.setShowName(((CheckboxField)field).getLabelText());
    fields.add(field);
    field = new CheckboxField("af_amex","Amex",false);
    field.setShowName(((CheckboxField)field).getLabelText());
    fields.add(field);
    field = new CheckboxField("af_disc","Discover",false);
    field.setShowName(((CheckboxField)field).getLabelText());
    fields.add(field);
    field = new CheckboxField("af_diners","Diners",false);
    field.setShowName(((CheckboxField)field).getLabelText());
    fields.add(field);
    field = new CheckboxField("af_jcb","JCB",false);
    field.setShowName(((CheckboxField)field).getLabelText());
    fields.add(field);
    field = new CheckboxField("af_aru","ARU",false);
    field.setShowName(((CheckboxField)field).getLabelText());
    fields.add(field);
    field = new CheckboxField("af_voice","Voice",false);
    field.setShowName(((CheckboxField)field).getLabelText());
    fields.add(field);
    field = new CheckboxField("af_referral","Referral",false);
    field.setShowName(((CheckboxField)field).getLabelText());
    fields.add(field);
    field = new CheckboxField("af_debit","Debit",false);
    field.setShowName(((CheckboxField)field).getLabelText());
    fields.add(field);

    field = new CurrencyField("af_visamc_from",7,7,true,0,9999);
    field.setShowName("Change From Visa/MC Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_visamc_to",7,7,true,0,9999);
    field.setShowName("Change To Visa/MC Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_amex_from",7,7,true,0,9999);
    field.setShowName("Change From Amex Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_amex_to",7,7,true,0,9999);
    field.setShowName("Change To Amex Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_disc_from",7,7,true,0,9999);
    field.setShowName("Change From Discover Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_disc_to",7,7,true,0,9999);
    field.setShowName("Change To Discover Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_diners_from",7,7,true,0,9999);
    field.setShowName("Change From Diners Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_diners_to",7,7,true,0,9999);
    field.setShowName("Change To Diners Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_jcb_from",7,7,true,0,9999);
    field.setShowName("Change From JCB Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_jcb_to",7,7,true,0,9999);
    field.setShowName("Change To JCB Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_aru_from",7,7,true,0,9999);
    field.setShowName("Change From ARU Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_aru_to",7,7,true,0,9999);
    field.setShowName("Change To ARU Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_voice_from",7,7,true,0,9999);
    field.setShowName("Change From Voice Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_voice_to",7,7,true,0,9999);
    field.setShowName("Change To Voice Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_referral_from",7,7,true,0,9999);
    field.setShowName("Change From Referral Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_referral_to",7,7,true,0,9999);
    field.setShowName("Change To Referral Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_debit_from",7,7,true,0,9999);
    field.setShowName("Change From Debit Authorization Fee");
    fields.add(field);
    field = new CurrencyField("af_debit_to",7,7,true,0,9999);
    field.setShowName("Change To Debit Authorization Fee");
    fields.add(field);

    field = new Field("miscfee_1_desc",50,30,true);
    field.setShowName("Miscellaneous Fee Description 1");
    fields.add(field);
    field = new CurrencyField("miscfee_1_from",7,7,true,0,9999);
    field.setShowName("Change From Miscellaneous Fee");
    fields.add(field);
    field = new CurrencyField("miscfee_1_to",7,7,true,0,9999);
    field.setShowName("Change To Miscellaneous Fee");
    fields.add(field);
    field = new Field("miscfee_2_desc",50,30,true);
    field.setShowName("Miscellaneous Fee Description 2");
    fields.add(field);
    field = new CurrencyField("miscfee_2_from",7,7,true,0,9999);
    field.setShowName("Change From Miscellaneous Fee");
    fields.add(field);
    field = new CurrencyField("miscfee_2_to",7,7,true,0,9999);
    field.setShowName("Change To Miscellaneous Fee");
    fields.add(field);
    field = new Field("miscfee_3_desc",50,30,true);
    field.setShowName("Miscellaneous Fee Description 3");
    fields.add(field);
    field = new CurrencyField("miscfee_3_from",7,7,true,0,9999);
    field.setShowName("Change From Miscellaneous Fee");
    fields.add(field);
    field = new CurrencyField("miscfee_3_to",7,7,true,0,9999);
    field.setShowName("Change To Miscellaneous Fee");
    fields.add(field);
    field = new Field("miscfee_4_desc",50,30,true);
    field.setShowName("Miscellaneous Fee Description 4");
    fields.add(field);
    field = new CurrencyField("miscfee_4_from",7,7,true,0,9999);
    field.setShowName("Change From Miscellaneous Fee");
    fields.add(field);
    field = new CurrencyField("miscfee_4_to",7,7,true,0,9999);
    field.setShowName("Change To Miscellaneous Fee");
    fields.add(field);

    // validations
    fields.addValidation(new EntryValidation());

    // set form control class
    fields.setHtmlExtra("class=\"formText\"");
  }

  private String[][] getInterchngTypes()
  {
    if(null==interchangeTypes)
    {
      createInterchngTypes();
    }
    return (String[][])interchangeTypes.toArray(new String[0][0]);
  }

  private void createInterchngTypes()
  {
    ResultSetIterator it=null;
    interchangeTypes = new ArrayList();
    interchangeTypes.add(new String[] {"No Change",""});
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:266^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  label
//          from    appo_ic_bet_types
//          where   app_type = :APP_TYPE
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  label\n        from    appo_ic_bet_types\n        where   app_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRDataBean_PriceChng_CBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,APP_TYPE);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.acr.ACRDataBean_PriceChng_CBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:271^7*/

      ResultSet rs = it.getResultSet();

      //for ACR records, the value isn't the pricing_type, it's the
      //actual label - so that it shows up in historical reviews
      while (rs.next())
      {
        interchangeTypes.add(new String[] { rs.getString("label"), rs.getString("label") });
      }
    }
    catch(Exception e)
    {
      //any mech here to warn user?
      log.debug("getInterchngTypes() Exception:"+e.getMessage());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) { }
      cleanUp();
    }
  }

  public String renderHtmlInterchangeTable()
  {
    StringBuffer sb = new StringBuffer();

    sb.append("<tr><td class=\"semiSmallBold\">");
    sb.append("&nbsp;<br>");
    sb.append("Interchange");
    sb.append("</td></tr>");
    sb.append("<tr><td>");

    if(inStatusMode() &&!hasInterchangeFeeChanges()) {
      sb.append("<span class=\"formFields\">No Interchange Fee changes specified.</span>");
      sb.append("</td></tr>");
      return sb.toString();
    }

    sb.append("<table cellpadding=\"1\" border=\"0\" align=\"center\" width=\"100%\" height=\"29\" class=\"tableFillGray\" cellspacing=\"0\">");
    sb.append("<tr>");
      sb.append("<td height=\"24\">");
        sb.append("<table border=0 cellpadding=2 cellspacing=0 width=100%>");
          sb.append("<tr>");
            sb.append("<th class=\"tableColumnHead\">Change From</th>");
            sb.append("<th class=\"tableColumnHead\">Change To</th>");
          sb.append("</tr>");

    if(inStatusMode()) {

      sb.append("<tr>");
      sb.append("<td class=\"tableData\">");

      sb.append(renderHtml("interchang_from_type"));
      sb.append("&nbsp;&nbsp;");

      String ift = getACRValue("interchang_from_type");

      if(ift.equals("No Change")) {
        sb.append("<i>No Change</i>");
      } else if(ift.indexOf("Retail")>-1) {
        sb.append(getACRValue("intc_retail_from"));
      } else if(ift.indexOf("MOTO")>-1) {
        sb.append(getACRValue("intc_moto_from"));
      } else if(ift.indexOf("Pass")>-1) {
        sb.append(getACRValue("pt_intc_from"));
      } else if(ift.indexOf("Expense")>-1) {
        sb.append(getACRValue("intc_exp_from"));
      }

      sb.append("</td>");
      sb.append("<td class=\"tableData\">");

      sb.append(renderHtml("interchang_to_type"));
      sb.append("&nbsp;&nbsp;");

      String itt = getACRValue("interchang_to_type");

      if(itt.indexOf("Retail")>-1) {
        sb.append(getACRValue("intc_retail_to"));
      } else if(itt.indexOf("MOTO")>-1) {
        sb.append(getACRValue("intc_moto_to"));
      } else if(itt.indexOf("Pass")>-1) {
        sb.append(getACRValue("pt_intc_to"));
      } else if(itt.indexOf("Expense")>-1) {
        sb.append(getACRValue("intc_exp_to"));
      }

    } else {

      // submit mode:

      sb.append("<tr>");
      sb.append("<td colspan=2 class=\"tableData\">");
      sb.append("<i>");
      sb.append(renderHtml("interchang_from_type",0));
      sb.append("</i>");
      sb.append("</td>");
      sb.append("</tr>");

      sb.append("<tr>");
      sb.append("<td class=\"tableData\">");
      sb.append(renderHtml("interchang_from_type",1));
      sb.append("<br>");
      sb.append(renderHtml("pt_intc_from"));
      sb.append("</td>");
      sb.append("<td class=\"tableData\">");
      sb.append(renderHtml("interchang_to_type",1));
      sb.append("<br>");
      sb.append(renderHtml("pt_intc_to"));
      sb.append("</td>");
      sb.append("</tr>");

      sb.append("<tr>");
      sb.append("<td class=\"tableData\">");
      sb.append(renderHtml("interchang_from_type",2));
      sb.append("<br>");
      sb.append(renderHtml("intc_retail_from"));
      sb.append("</td>");
      sb.append("<td class=\"tableData\">");
      sb.append(renderHtml("interchang_to_type",2));
      sb.append("<br>");
      sb.append(renderHtml("intc_retail_to"));
      sb.append("</td>");
      sb.append("</tr>");

      sb.append("<tr>");
      sb.append("<td class=\"tableData\">");
      sb.append(renderHtml("interchang_from_type",3));
      sb.append("<br>");
      sb.append(renderHtml("intc_moto_from"));
      sb.append("</td>");
      sb.append("<td class=\"tableData\">");
      sb.append(renderHtml("interchang_to_type",3));
      sb.append("<br>");
      sb.append(renderHtml("intc_moto_to"));
      sb.append("</td>");
      sb.append("</tr>");

      sb.append("<tr>");
      sb.append("<td class=\"tableData\">");
      sb.append(renderHtml("interchang_from_type",4));
      sb.append("&nbsp;&nbsp;");
      sb.append(renderHtml("intc_exp_from"));
      sb.append("</td>");
      sb.append("<td class=\"tableData\">");
      sb.append(renderHtml("interchang_to_type",4));
      sb.append("&nbsp;&nbsp;");
      sb.append(renderHtml("intc_exp_to"));
      sb.append("</td>");
      sb.append("</tr>");

    }

    sb.append("</table>");
    sb.append("</td></tr>");
    sb.append("</table>");
    sb.append("</td></tr>");

    return sb.toString();
  }

  /**
   * leaving the following three classes as separate,
   * in case further development is needed
   **/
  private final class InterchangeRetailDropDownTable extends DropDownTable
  {
    InterchangeRetailDropDownTable()
    {
      addElement("","- select one -");

      ResultSetIterator it=null;
      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:448^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  label
//            from    appo_ic_bets
//            where   bet_type = :mesConstants.CBT_APP_INTERCHANGE_TYPE_RETAIL
//                    and app_type = :APP_TYPE
//                    and trunc(sysdate) between valid_from and valid_to
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  label\n          from    appo_ic_bets\n          where   bet_type =  :1 \n                  and app_type =  :2 \n                  and trunc(sysdate) between valid_from and valid_to";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.acr.ACRDataBean_PriceChng_CBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.CBT_APP_INTERCHANGE_TYPE_RETAIL);
   __sJT_st.setInt(2,APP_TYPE);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.acr.ACRDataBean_PriceChng_CBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:455^9*/

        ResultSet rs = it.getResultSet();

        //for ACR records, the value isn't the pricing_type, it's the
        //actual label - so that shows up in historical reviews
        while (rs.next())
        {
          addElement(rs.getString("label"), rs.getString("label"));
        }
      }
      catch(Exception e)
      {
        //any mech here to warn user?
        log.debug("InterchangeRetailDropDownTable() Exception:"+e.getMessage());
      }
      finally
      {
        try{ it.close(); } catch(Exception e) { }
        cleanUp();
      }
    }

  } // InterchangeRetailDropDownTable

  private final class InterchangeMotoDropDownTable extends DropDownTable
  {
    InterchangeMotoDropDownTable()
    {
      addElement("","- select one -");

      ResultSetIterator it=null;
      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:491^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  label
//            from    appo_ic_bets
//            where   bet_type = :mesConstants.CBT_APP_INTERCHANGE_TYPE_MOTO_LODGING
//                    and app_type = :APP_TYPE
//                    and trunc(sysdate) between valid_from and valid_to
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  label\n          from    appo_ic_bets\n          where   bet_type =  :1 \n                  and app_type =  :2 \n                  and trunc(sysdate) between valid_from and valid_to";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.acr.ACRDataBean_PriceChng_CBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.CBT_APP_INTERCHANGE_TYPE_MOTO_LODGING);
   __sJT_st.setInt(2,APP_TYPE);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.acr.ACRDataBean_PriceChng_CBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:498^9*/

        ResultSet rs = it.getResultSet();

        //for ACR records, the value isn't the pricing_type, it's the
        //actual label - so that shows up in historical reviews
        while (rs.next())
        {
          addElement(rs.getString("label"), rs.getString("label"));
        }
      }
      catch(Exception e)
      {
        //any mech here to warn user?
        log.debug("InterchangeMotoDropDownTable() Exception:"+e.getMessage());
      }
      finally
      {
        try{ it.close(); } catch(Exception e) { }
        cleanUp();
      }
    }
  } // InterchangeMotoDropDownTable

  private final class PassThroughInterchangeDropDownTable extends DropDownTable
  {
    PassThroughInterchangeDropDownTable()
    {
      addElement("","- select one -");

      ResultSetIterator it=null;
      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:533^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  label
//            from    appo_ic_bets
//            where   bet_type = :mesConstants.CBT_APP_INTERCHANGE_TYPE_PASSTHRU
//                    and app_type = :APP_TYPE
//                    and trunc(sysdate) between valid_from and valid_to
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  label\n          from    appo_ic_bets\n          where   bet_type =  :1 \n                  and app_type =  :2 \n                  and trunc(sysdate) between valid_from and valid_to";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.acr.ACRDataBean_PriceChng_CBT",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.CBT_APP_INTERCHANGE_TYPE_PASSTHRU);
   __sJT_st.setInt(2,APP_TYPE);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.acr.ACRDataBean_PriceChng_CBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:540^9*/

        ResultSet rs = it.getResultSet();

        //for ACR records, the value isn't the pricing_type, it's the
        //actual label - so that shows up in historical reviews
        while (rs.next())
        {
          addElement(rs.getString("label"), rs.getString("label"));
        }
      }
      catch(Exception e)
      {
        //any mech here to warn user?
        log.debug("PassThroughInterchangeDropDownTable() Exception:"+e.getMessage());
      }
      finally
      {
        try{ it.close(); } catch(Exception e) { }
        cleanUp();
      }
    }
  } // PassThroughInterchangeDropDownTable

  public int[][] getAllowedQueueOps()
  {
    return new int[][]
    {
       {MesQueues.Q_NONE,MesQueues.Q_CHANGE_REQUEST_CBT_NEW}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_NEW,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
      ,{MesQueues.Q_CHANGE_REQUEST_CBT_TSYS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CBT_TSYS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  /*
  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      case MesQueues.Q_CHANGE_REQUEST_CBT_TSYS:
      case MesQueues.Q_CHANGE_REQUEST_CBT_MMS:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Declined by Amex or Discover"
          ,"Request Cancelled"
        };
        break;

      // default: defer to super
      default:
        rsns = super.getCancellationReasons(crntQ);
    }

    return rsns;
  }
  */

  public String getHeadingName()
  {
    return "Pricing Changes for CB&T Merchant";
  }

  protected class EntryValidation implements Validation
  {
    private String errorText = "";

    public EntryValidation()
    {
    }

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      Field fld;
      String fname;

      // interchange

      // from
      fld=fields.getField("interchang_from_type");
      if(fld.getData().indexOf("Retail")>-1) {
        fld=fields.getField("intc_retail_from");
        if(fld.isBlank()) {
          errorText = "Please specify a Retail Interchange From Selection.";
          return false;
        }
      } else if(fld.getData().indexOf("MOTO")>-1) {
        fld=fields.getField("intc_moto_from");
        if(fld.isBlank()) {
          errorText = "Please specify a MOTO Interchange From Selection.";
          return false;
        }
      } else if(fld.getData().indexOf("Pass")>-1) {
        fld=fields.getField("pt_intc_from");
        if(fld.isBlank()) {
          errorText = "Please specify an Interchange Pass Thru From Selection.";
          return false;
        }
      }

      // to
      fld=fields.getField("interchang_to_type");
      if(fld.getData().indexOf("Pass")>-1) {
        fld=fields.getField("pt_intc_to");
        if(fld.isBlank()) {
          errorText = "Please specify an Interchange Pass Thru To Selection.";
          return false;
        }
      }


      // auth fees

      for(int i=1;i<=9;i++) {

        switch(i) {
          case 1: fname="af_visamc";  break;
          case 2: fname="af_amex";  break;
          case 3: fname="af_disc";  break;
          case 4: fname="af_diners";  break;
          case 5: fname="af_jcb";  break;
          case 6: fname="af_aru";  break;
          case 7: fname="af_voice";  break;
          case 8: fname="af_referral";  break;
          case 9: fname="af_debit";  break;
          default: fname=""; break;
        }

        fld=fields.getField(fname);
        if(((CheckboxField)fld).isChecked()) {
          fld=fields.getField(fname+"_from");
          if(fld.isBlank()) {
            errorText = "Please specify a Visa/MC Authorization Change From Fee.";
            return false;
          }
          fld=fields.getField(fname+"_to");
          if(fld.isBlank()) {
            errorText = "Please specify a Visa/MC Authorization Change To Fee.";
            return false;
          }
        }
      }

      // misc. fees

      for(int i=1;i<=4;i++) {

        fname="miscfee_"+i;

        fld=fields.getField(fname+"_desc");
        if(!fld.isBlank()) {
          fld=fields.getField(fname+"_from");
          if(fld.isBlank()) {
            errorText = "Please specify a Miscellaneous Change From Fee for item "+i+".";
            return false;
          }
          fld=fields.getField(fname+"_to");
          if(fld.isBlank()) {
            errorText = "Please specify a Miscellaneous Change To Fee for item "+i+".";
            return false;
          }
        }
      }

      return true;
    }

  } // class EntryValidation

  public boolean hasRootChanges()
  {
    return (acr.itemHasValue("discRate_to")
            || acr.itemHasValue("perItem_to")
            || acr.itemHasValue("minDisc_to")
            || acr.itemHasValue("discRate_check_to")
            || acr.itemHasValue("perItem_check_to"));
  }

  public boolean hasInterchangeFeeChanges()
  {
    return acr.itemHasValue("interchang_from_type");
  }

  public boolean hasAuthFeeChanges()
  {
    return (acr.getACRValue("af_visamc").equals("y")
            || acr.getACRValue("af_amex").equals("y")
            || acr.getACRValue("af_disc").equals("y")
            || acr.getACRValue("af_diners").equals("y")
            || acr.getACRValue("af_jcb").equals("y")
            || acr.getACRValue("af_aru").equals("y")
            || acr.getACRValue("af_voice").equals("y")
            || acr.getACRValue("af_referral").equals("y")
            || acr.getACRValue("af_debit").equals("y")
           );
  }

  public boolean hasMiscFeeChanges()
  {
    return (acr.itemHasValue("miscfee_1_desc")
            || acr.itemHasValue("miscfee_2_desc")
            || acr.itemHasValue("miscfee_3_desc")
            || acr.itemHasValue("miscfee_4_desc")
           );
  }

  public String getJSPIncludeFilename()
  {
    return (acr==null)? "":"acr_incl_"+acr.getDefinition().getShortName()+"_cbt.jsp";
  }


}/*@lineinfo:generated-code*/