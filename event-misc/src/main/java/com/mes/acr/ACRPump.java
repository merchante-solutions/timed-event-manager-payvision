/*@lineinfo:filename=ACRPump*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/acr/ACRPump.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-03-05 16:53:22 -0800 (Wed, 05 Mar 2008) $
  Version            : $Revision: 14644 $

  Change History:
     See VSS database

  Copyright (C) 2000-2003,2004 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.acr;

import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.calltag.CallTagBuilder;
import com.mes.calltag.CallTagDAO;
import com.mes.calltag.CallTagDAOFactory;
import com.mes.calltag.CallTagDetailQueueBean;
import com.mes.calltag.CallTagItem;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.equipment.EquipSelectorItem;
import com.mes.equipment.EquipmentDataBuilder;
import com.mes.equipment.EquipmentData_EquipInv;
import com.mes.net.MailMessage;
import com.mes.supply.SupplyOrder;
import com.mes.supply.SupplyOrderItem;
import com.mes.support.MesCalendar;
import com.mes.support.TextMessageManager;
//new inventory system
import com.mes.tmg.AcrActionHelper;
import com.mes.tmg.CallTagPartStatus;
import com.mes.tmg.CallTagReason;
import com.mes.tmg.CallTagStatus;
import com.mes.tmg.DeployReason;
import com.mes.tmg.Order;
import com.mes.tmg.TmgDb;
import com.mes.tools.EquipmentActionKey;
import com.mes.tools.EquipmentKey;
import com.mes.user.UserBean;

/**
 * ACRPump class.
 *
 * delegates tasks that emanate from ACR ops to the appropriate handler.
 */
public final class ACRPump extends SQLJConnectionBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRPump.class);

  // constants
  // (NONE)

  // data members
  private   Vector          actions       = null; // coll. of AcrAction objects
  private   Vector          results       = null; // coll. of AcrActionResult objects


  // construction
  public ACRPump()
  {
    super();

    initialize();
  }


  private void initialize()
  {
    actions   = new Vector(0,1);
    results   = new Vector(0,1);

    // populate the actions list
    actions.addElement(new BankServPosting());
    actions.addElement(new ShippingAddressUpdate());
    actions.addElement(new CallTagGeneration());
    actions.addElement(new SupplyOrderPosting());
    actions.addElement(new ContactInfoUpdate());
    actions.addElement(new SupplyBillingConfig());
    actions.addElement(new CertegyCheckUpdate());
    actions.addElement(new ValutecUpdate());
    actions.addElement(new NotifyMgmtByEmail());
    actions.addElement(new GenerateInventoryOrder());
    actions.addElement(new EmailOnAcctClose());
  }

  private void clear()
  {
    if(results!=null)
    {
      results.removeAllElements();
    }
  }

  public void pump(AcrOp acrOp)
  {
    clear();

    for(Enumeration e=actions.elements();e.hasMoreElements();) {
      AcrAction action = (AcrAction)e.nextElement();

      if(action.opHasAction(acrOp)) {
        log.debug("pump() - Performing action: '"+action.toString()+"' on current op...");
        action.doAction(acrOp);

        log.debug("pump() - Posting action result...");
        AcrActionResult result = action.getActionResult();
        results.addElement(result);

        log.info("ACR Op '"+acrOp.toString()+"' performed.  Results: '"+result.toString()+"'");
      }

    }
  }

  public void transferResultsToMsgMngr(TextMessageManager tmm)
  {
    if(tmm==null || results==null || results.size()<1)
      return;

    for(Enumeration e=results.elements(); e.hasMoreElements();) {
      AcrActionResult result = (AcrActionResult)e.nextElement();
      for(Enumeration e2=result.resultDescrs.elements(); e2.hasMoreElements();) {
        tmm.msg((String)e2.nextElement());
      }
    }
  }

  /**
   * AcrOp class
   *
   * Encapsulates data representing an ACR operation.
   */
  public static final class AcrOp
  {
    public AcrOp(ACRDataBeanBase bean, int srcQ, int dstQ)
    {
      this.bean=bean;
      this.srcQ=srcQ;
      this.dstQ=dstQ;
    }

    //used only from ACRController pacr - initiate
    public AcrOp(ACRDataBeanBase bean)
    {
      this.bean=bean;
      this.srcQ=MesQueues.Q_NONE;

     //set destination Q to either the Mgmt Q, if on both for merchant and ACR
      //or the default initial Q (MesQueues.NONE if not otherwise defined)
      if(bean.getMgmtQInfo().isMgmtQOn() &&
          bean.getACR().getDefinition().initMgmtQ()
         )
      {
        this.dstQ=bean.getMgmtQInfo().getId();
      }
      else
      {
        this.dstQ=bean.getInitialQueue();
      }
      log.debug("creating INIT AcrOp: srcQ = "+srcQ+" : dstQ = "+dstQ);

    }
    public  ACRDataBeanBase     bean;
    public  int                 srcQ;
    public  int                 dstQ;

    public String toString()
    {
      StringBuffer sb = new StringBuffer();

      sb.append("bean-ACR Id: ");
      sb.append(bean.getACRID());

      sb.append(", bean-ACR Type: ");
      sb.append(bean.getACRDefinition().getShortName());

      sb.append(", srcQ: ");
      sb.append(srcQ);

      sb.append(", dstQ: ");
      sb.append(dstQ);

      return sb.toString();
    }
  }

  /**
   * AcrAction interface
   *
   * Defines the methods necessary for checking and performing an acr related action
   */
  public interface AcrAction
  {
    public boolean              opHasAction(AcrOp acrOp);
    public void                 doAction(AcrOp acrOp);
    public AcrActionResult      getActionResult();
    public String               toString();
  }

  /**
   * AcrActionResult class
   *
   * Encapsulates data representing the result of an ACR action.
   */
  public static final class AcrActionResult
  {
    public  Vector              resultDescrs = new Vector(0,1);

    public AcrActionResult()
    {
    }

    public boolean isDescriptorSet()
    {
      return (resultDescrs!=null && resultDescrs.size()>0);
    }

    public void setResultDescriptor(String s)
    {
      resultDescrs.add(0,s);
    }
    public String getResultDescriptor()
    {
      return resultDescrs.get(0).toString();
    }

    public void addResultDescriptor(String ctEquipKeys)
    {
      if(ctEquipKeys!=null && ctEquipKeys.length()>0)
        resultDescrs.add(ctEquipKeys);
    }

    public Vector getResultDescriptors()
    {
      return resultDescrs;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer();

      for(Enumeration e=resultDescrs.elements(); e.hasMoreElements();) {
        String s = (String)e.nextElement();
        if(s!=null && s.length()>0) {
          sb.append(',');
          sb.append(s);
        }
      }

      if(sb.length()>0)
        return sb.substring(1);
      else
        return "";
    }

}

  /**
   * BankServPosting
   */
  public final class BankServPosting implements AcrAction
  {
    private AcrActionResult result = null;
    private boolean isFeeReversal = false;
    private boolean isAccClosure = false;

    public boolean opHasAction(AcrOp acrOp)
    {
      log.debug("BankServPosting.opHasAction() - START");

      try
      {

        String acrDefShrtNme    = acrOp.bean.getACRDefinition().getShortName();
        String merchBankNum     = acrOp.bean.getACR().getACRValue("Merchant Bank Number");

        log.debug("BankServPosting.opHasAction() - acrDefShrtNme="+acrDefShrtNme+", srcQ="+acrOp.srcQ+", dstQ="+acrOp.dstQ+", merchBankNum="+merchBankNum);

        isFeeReversal = (acrDefShrtNme.indexOf("feervsl") == 0);
        isAccClosure = (acrDefShrtNme.indexOf("acccls") == 0);

        return
        (
          acrOp.dstQ == MesQueues.Q_CHANGE_REQUEST_COMPLETED
          && (merchBankNum.equals("3941")||merchBankNum.equals("3942"))
          &&
          (
            ( (isFeeReversal || isAccClosure )
              && acrOp.bean.getACR().getACRValue("requestStatus").equals("Accepted")
              && (Double.parseDouble(acrOp.bean.getACR().getACRValue("acceptAmt")) > 0d)
            )
            ||
            (
              ( isFeeReversal ||
                acrDefShrtNme.equals("feechng")||
                acrDefShrtNme.equals("feechng_v1")
              )
              && isAccountClosed(acrOp.bean.getACR().getMerchantNum())
            )
          )
        );
      }
      catch(Exception e)
      {
        log.error("BankServAction.opHasAction() EXCEPTION: "+e.toString());
        return false;
      }
    }

    private boolean isAccountClosed(String mn)
    {
      boolean isAcntClosed = false;

      try {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:338^9*/

//  ************************************************************
//  #sql [Ctx] { select DATE_STAT_CHGD_TO_DCB
//            
//            from   MIF
//            where  merchant_number = :mn
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select DATE_STAT_CHGD_TO_DCB\n           \n          from   MIF\n          where  merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.acr.ACRPump",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,mn);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   isAcntClosed = __sJT_rs.getBoolean(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:344^9*/
      }
      catch(Exception e) {
        log.error("BankServAction.isAccountClosed() EXCEPTION: "+e.toString());
      }
      finally {
        cleanUp();
      }

      return isAcntClosed;
    }

    public void doAction(AcrOp acrOp)
    {
      log.debug("BankServPosting.doAction() - START");

      result = new AcrActionResult();

      try {

        double acptAmt            = 0d;
        String merchNum           = acrOp.bean.getACR().getMerchantNum();
        String createdBy          = acrOp.bean.getACR().getACRValue("acceptUserId");
        String acrDefShrtNme      = acrOp.bean.getACRDefinition().getShortName();
        long   acrId              = acrOp.bean.getACR().getID();
        String creditDebitInd     = "";

        isFeeReversal = (acrDefShrtNme.indexOf("feervsl") == 0);
        isAccClosure = (acrDefShrtNme.indexOf("acccls") == 0);

        if(isFeeReversal) {
          // post a Batch Maintenance (charge record) transaction
          creditDebitInd     = "C";
          acptAmt            = Double.parseDouble(acrOp.bean.getACR().getACRValue("acceptAmt"));
          try {
            connect();
            /*@lineinfo:generated-code*//*@lineinfo:380^13*/

//  ************************************************************
//  #sql [Ctx] { insert into   billing_auto_cg
//                (
//                 merchant_number
//                ,statement_message
//                ,charge_amount
//                )
//                values
//                (
//                 :merchNum
//                ,'FEE REVERSAL'
//                ,abs(:acptAmt) * -1
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into   billing_auto_cg\n              (\n               merchant_number\n              ,statement_message\n              ,charge_amount\n              )\n              values\n              (\n                :1 \n              ,'FEE REVERSAL'\n              ,abs( :2 ) * -1\n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   __sJT_st.setDouble(2,acptAmt);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:394^13*/

            result.addResultDescriptor("BankServ Charge record posted from ACR (Id: "+acrOp.bean.getACRID()+") Fee Reversal initiation.");

          }
          catch(Exception e) {
            log.error("BankServPosting.doAction(feervsl): "+e.toString());
            result.addResultDescriptor("BankServPosting.doAction(feervsl) EXCEPTION: "+e.getMessage());
          }
          finally {
            cleanUp();
          }

        } else if(isAccClosure) {
          // post a BankServ transaction
          creditDebitInd     = "D";
          acptAmt            = Double.parseDouble(acrOp.bean.getACR().getACRValue("acceptAmt"));
          try {
            connect();
            /*@lineinfo:generated-code*//*@lineinfo:413^13*/

//  ************************************************************
//  #sql [Ctx] { insert into bankserv_ach_detail
//                (
//                  merchant_number,
//                  amount,
//                  entry_description,
//                  credit_debit_ind,
//                  created_by,
//                  source_type,
//                  source_id
//                )
//                values
//                (
//                  :merchNum,
//                  :acptAmt,
//                  'FEE CLOSRE',
//                  :creditDebitInd,
//                  :createdBy,
//                  :mesConstants.BANKSERV_SOURCE_ACR,
//                  :acrId
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into bankserv_ach_detail\n              (\n                merchant_number,\n                amount,\n                entry_description,\n                credit_debit_ind,\n                created_by,\n                source_type,\n                source_id\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                'FEE CLOSRE',\n                 :3 ,\n                 :4 ,\n                 :5 ,\n                 :6 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   __sJT_st.setDouble(2,acptAmt);
   __sJT_st.setString(3,creditDebitInd);
   __sJT_st.setString(4,createdBy);
   __sJT_st.setInt(5,mesConstants.BANKSERV_SOURCE_ACR);
   __sJT_st.setLong(6,acrId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:435^13*/

            result.addResultDescriptor("BankServ Debit record posted from ACR (Id: "+acrOp.bean.getACRID()+") Account Closure initiation.");
          }
          catch(Exception e) {
            log.error("BankServPosting.doAction(" + acrDefShrtNme + "): " + e);
            result.addResultDescriptor("BankServPosting.doAction("
              + acrDefShrtNme + ") EXCEPTION: "+e.getMessage());
          }
          finally {
            cleanUp();
          }
        } else if(acrDefShrtNme.equals("feechng")
                  || acrDefShrtNme.equals("feechng_v1")) {
          // post a BankServ transaction
          creditDebitInd     = "D";
          try {

            acptAmt = ((ACRDataBean_FeeChange)acrOp.bean).getPosOrEquipmentRelatedTotalAmount();

            connect();
            /*@lineinfo:generated-code*//*@lineinfo:456^13*/

//  ************************************************************
//  #sql [Ctx] { insert into bankserv_ach_detail
//                (
//                  merchant_number,
//                  amount,
//                  entry_description,
//                  credit_debit_ind,
//                  created_by,
//                  source_type,
//                  source_id
//                )
//                values
//                (
//                  :merchNum,
//                  :acptAmt,
//                  'ACH PR ITM',
//                  :creditDebitInd,
//                  :createdBy,
//                  :mesConstants.BANKSERV_SOURCE_ACR,
//                  :acrId
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into bankserv_ach_detail\n              (\n                merchant_number,\n                amount,\n                entry_description,\n                credit_debit_ind,\n                created_by,\n                source_type,\n                source_id\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                'ACH PR ITM',\n                 :3 ,\n                 :4 ,\n                 :5 ,\n                 :6 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   __sJT_st.setDouble(2,acptAmt);
   __sJT_st.setString(3,creditDebitInd);
   __sJT_st.setString(4,createdBy);
   __sJT_st.setInt(5,mesConstants.BANKSERV_SOURCE_ACR);
   __sJT_st.setLong(6,acrId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:478^13*/

            result.addResultDescriptor("BankServ Debit record posted from ACR (Id: "+acrOp.bean.getACRID()+") Fee Change initiation.");
          }
          catch(Exception e) {
            log.error("BankServPosting.doAction(" + acrDefShrtNme + "): " + e);
            result.addResultDescriptor("BankServPosting.doAction("
              + acrDefShrtNme + ") EXCEPTION: " + e.getMessage());
          }
          finally {
            cleanUp();
          }
        }

      }
      catch(Exception e) {
        log.error("BankServ doAction() EXCEPTION: "+e.toString());
        if(!result.isDescriptorSet())
          result.addResultDescriptor("BankServ doAction() EXCEPTION: "+e.getMessage());
      }
    }

    public AcrActionResult   getActionResult()
    {
      return result;
    }

    public String toString()
    {
      return "BankServ Posting";
    }

  } // BankServAction class



  /**
   * CallTagGeneration class
   *
   * Automatically generates call tags from the appropriate ACR upon completion
   */
  public class CallTagGeneration implements AcrAction
  {
    private final int         BILLING_DAYSTOWAIT              = 28;
    private AcrActionResult   result                          = null;

    public boolean              opHasAction(AcrOp acrOp)
    {

      return ( acrOp.dstQ == MesQueues.Q_CHANGE_REQUEST_COMPLETED &&
               acrOp.bean.calltaggedEquipmentExists()
              );


      //for test only - fires right away
      //return acrOp.bean.calltaggedEquipmentExists();

    }

    public void                 doAction(AcrOp acrOp)
    {
      result = new AcrActionResult();

      try {

        String acrDefShrtNme = acrOp.bean.getACRDefinition().getShortName();
        CallTagDAO ctdao = CallTagDAOFactory.getInstance().getCallTagDAO();
        Vector ctEquipKeys = null;
        int ctiEquipStatus = CallTagItem.STATUS_UNDEFINED;

        log.debug("IN CallTagGeneration: acrDefShrtNme = "+acrDefShrtNme);

        if(acrDefShrtNme.equals("acccls")) {
          ctEquipKeys = ((ACRDataBean_AcntClosure)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("acccls_v1")) {
          ctEquipKeys = ((ACRDataBean_AcntClosure_CBT)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("equippu")) {
          ctEquipKeys = ((ACRDataBean_EquipPU)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("equippu_v1")) {
          ctEquipKeys = ((ACRDataBean_EquipPU_CBT)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("equipswap")) {
          ctEquipKeys = ((ACRDataBean_EquipSwap)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("equipswap_v1")) {
          ctEquipKeys = ((ACRDataBean_EquipSwap_CBT)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("equipswap_nbsc")) {
          ctEquipKeys = ((ACRDataBean_EquipSwap_NBSC)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("reopcl")) {
          ctEquipKeys = ((ACRDataBean_AcntCloseReopen)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("reopcl_v1")) {
          ctEquipKeys = ((ACRDataBean_AcntCloseReopen_CBT)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        //All new ACRs for CBT deployment... 12/10/2004
        else if(acrDefShrtNme.equals("acccls_v2")) {
          ctEquipKeys = ((ACRDataBean_AcntClosure_CBT_v2)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("equippu_v2")) {
          ctEquipKeys = ((ACRDataBean_EquipPU_CBT_v2)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("equipswap_v2")) {
          ctEquipKeys = ((ACRDataBean_EquipSwap_CBT_v2)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("equipupg_v2")) {
          ctEquipKeys = ((ACRDataBean_EquipUpgrade_CBT_v2)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("othercardmgmt_v2")) {
          if( ((ACRDataBean_OtherCardMgmt_CBT)acrOp.bean).calltaggedEquipmentExists() )
          {
            ctEquipKeys = ((ACRDataBean_OtherCardMgmt_CBT)acrOp.bean).getCallTaggedEquipment();
            ctiEquipStatus = CallTagItem.STATUS_INCOMING;
          }
        }

        //new MES ACRs
        else if(acrDefShrtNme.equals("acccls_mes")) {
          ctEquipKeys = ((ACRDataBean_AcntClosure_MES)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("equippu_mes")) {
          ctEquipKeys = ((ACRDataBean_EquipPU_MES)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("equipswap_mes")) {
          ctEquipKeys = ((ACRDataBean_EquipSwap_MES)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("equipupg_mes")) {
          ctEquipKeys = ((ACRDataBean_EquipUpgrade_MES)acrOp.bean).getCallTaggedEquipment();
          ctiEquipStatus = CallTagItem.STATUS_INCOMING;
        }

        else if(acrDefShrtNme.equals("othercardmgmt_mes")) {
          if( ((ACRDataBean_OtherCardMgmt_CBT)acrOp.bean).calltaggedEquipmentExists() )
          {
            ctEquipKeys = ((ACRDataBean_OtherCardMgmt_MES)acrOp.bean).getCallTaggedEquipment();
            ctiEquipStatus = CallTagItem.STATUS_INCOMING;
          }
        }

        if(ctEquipKeys!=null && ctEquipKeys.size()>0) {

          // create call tag
          com.mes.calltag.CallTag ct = CallTagBuilder.getInstance().buildCallTag();
          ct.setMerchNumber(acrOp.bean.getMerchantNumber());
          EquipmentActionKey eak = new EquipmentActionKey(mesConstants.ACTION_CODE_ACR,acrOp.bean.getACR().getID());
          ct.setParentKey(eak);

          double billAmount = 0d;

          for(Enumeration e=ctEquipKeys.elements();e.hasMoreElements();) {
            EquipmentKey key = (EquipmentKey)e.nextElement();

            // auto-set call tag billing date and amount if possible
            if(key.getEquipType()==mesConstants.EQUIPTYPE_EQUIPINV) {
              try {
                EquipmentData_EquipInv ei = (EquipmentData_EquipInv)EquipmentDataBuilder.getInstance().load(key,false);
                billAmount += ei.getUnitCost();
              }
              catch(Exception ex) {
                log.error("auto-set call tag billing date EXCEPTION: "+ex.toString()+"  Continuing.");
              }
            }

            // create call tag item
            CallTagItem cti = CallTagBuilder.getInstance().buildCallTagItem();
            cti.setEquipmentKey(key);
            cti.setStatusCode(ctiEquipStatus);
            ct.addItem(cti);
          }

          // set call tag billing amount
          //  Bill Amount = Unit Cost Sum + 40% of Unit Cost Sum
          billAmount *= 1.4d;
          ct.setBillAmount(billAmount);

          Date billDate = new Date();
          ct.setBillDate(MesCalendar.addDays(billDate,BILLING_DAYSTOWAIT));

          // persist call tag
          if(!ctdao.persistCallTag(ct))
            throw new Exception("An exception occurred while persisting generated Call Tag.");

          // add call tag to queueing system
          if(CallTagDetailQueueBean.queueInitiate(ct.getId(),null,false,acrOp.bean.getUser(),null)) {
            result.addResultDescriptor("Call Tag of Id: "+ct.getId()+" created.");
          } else
            throw new Exception("An exception occurred while adding Call Tag of Id: "+ct.getId()+" to the queueing system.");
        }

      }
      catch(Exception e) {
        log.error("CallTagGeneration.doAction() EXCEPTION: "+e.toString());
        result.addResultDescriptor("CallTagGeneration.doAction() EXCEPTION: "+e.getMessage());
      }
    }

    public AcrActionResult     getActionResult()
    {
      return result;
    }

    public String toString()
    {
      return "Call Tag Generation";
    }

  } // CallTagGeneration

  /**
   * SupplyOrderPosting class
   *
   * Saves the appropriate supply order info in the DB.
   */

  public class SupplyOrderPosting implements AcrAction
  {
    SupplyOrder order = null;
    SupplyOrderItem soi = null;
    private AcrActionResult result = null;

    public boolean opHasAction(AcrOp acrOp){
      boolean hasAction =
        acrOp.bean.getACRDefinition().getShortName().equals("newsupordfrm") ||
        acrOp.bean.getACRDefinition().getShortName().equals("newsupordfrm_v1") ||
        acrOp.bean.getACRDefinition().getShortName().equals("newsupordfrm_v2");
      return hasAction;
    }

    public AcrActionResult getActionResult(){
      return result;
    }

    public String toString(){
      return "Supply Order Posting";
    }

    public void doAction(AcrOp acrOp)
    {

      result = new AcrActionResult();

      //switch on queOp
      switch(acrOp.dstQ)
      {

        case MesQueues.Q_NONE:
        case MesQueues.Q_CHANGE_REQUEST_COMPLETED:

          //add the new supply order to the DB:
          long newId = 0L;

          //we made it this far, so the bean must have an order...
          order = ((ACRDataBean_MesSplyOrdFormNew)acrOp.bean).getOrder();

          try{
            //create a new unique SUPPLY_ORDER id in DB
            connect();
            /*@lineinfo:generated-code*//*@lineinfo:771^13*/

//  ************************************************************
//  #sql [Ctx] { select supply_order_sequence.nextval  from dual
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select supply_order_sequence.nextval   from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.acr.ACRPump",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   newId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:774^13*/
          }catch(Exception e){
            result.addResultDescriptor("SupplyOrderPosting.doAction(): newId EXCEPTION: "+e.getMessage());
          }finally{
            cleanUp();
          }

          //create a new SUPPLY_ORDER listing in DB
          try{

            connect();
            /*@lineinfo:generated-code*//*@lineinfo:785^13*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO supply_order
//                  (supply_order_id
//                  ,acrid
//                  ,handling_cost
//                  ,tax_rate
//                  ,markup_multiplier
//                  ,order_date
//                  ,ship_address_name
//                  ,ship_address_line1
//                  ,ship_address_line2
//                  ,ship_address_city
//                  ,ship_address_state
//                  ,ship_address_zip
//                  ,merch_num
//                  ,is_rush
//                  ,requestor_name
//                  ,requestor_phone
//                  ,ship_cost_constant
//                  ,total_cost
//                  ,ship_cost
//                  )
//                VALUES
//                  (:newId
//                  ,:acrOp.bean.getACR().getID()
//                  ,:order.getHandleFee()
//                  ,:order.getTaxRate()
//                  ,:order.getMarkUp()
//                  ,sysdate
//                  ,:order.getShipName()
//                  ,:order.getShipLine1()
//                  ,:order.getShipLine2()
//                  ,:order.getShipCity()
//                  ,:order.getShipState()
//                  ,:order.getShipZip()
//                  ,:acrOp.bean.getACR().getMerchantNum()
//                  ,:order.isRush?"y":"n"
//                  ,:acrOp.bean.getACR().getACRValue("Requestor's Name")
//                  ,:acrOp.bean.getACR().getACRValue("Requestor's Phone #")
//                  ,:order.getBaseShipRate()
//                  ,:order.getTotalCost()
//                  ,:order.getShipCost()
//                  )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3564 = acrOp.bean.getACR().getID();
 double __sJT_3565 = order.getHandleFee();
 double __sJT_3566 = order.getTaxRate();
 double __sJT_3567 = order.getMarkUp();
 String __sJT_3568 = order.getShipName();
 String __sJT_3569 = order.getShipLine1();
 String __sJT_3570 = order.getShipLine2();
 String __sJT_3571 = order.getShipCity();
 String __sJT_3572 = order.getShipState();
 String __sJT_3573 = order.getShipZip();
 String __sJT_3574 = acrOp.bean.getACR().getMerchantNum();
 String __sJT_3575 = order.isRush?"y":"n";
 String __sJT_3576 = acrOp.bean.getACR().getACRValue("Requestor's Name");
 String __sJT_3577 = acrOp.bean.getACR().getACRValue("Requestor's Phone #");
 double __sJT_3578 = order.getBaseShipRate();
 double __sJT_3579 = order.getTotalCost();
 double __sJT_3580 = order.getShipCost();
   String theSqlTS = "INSERT INTO supply_order\n                (supply_order_id\n                ,acrid\n                ,handling_cost\n                ,tax_rate\n                ,markup_multiplier\n                ,order_date\n                ,ship_address_name\n                ,ship_address_line1\n                ,ship_address_line2\n                ,ship_address_city\n                ,ship_address_state\n                ,ship_address_zip\n                ,merch_num\n                ,is_rush\n                ,requestor_name\n                ,requestor_phone\n                ,ship_cost_constant\n                ,total_cost\n                ,ship_cost\n                )\n              VALUES\n                ( :1 \n                , :2 \n                , :3 \n                , :4 \n                , :5 \n                ,sysdate\n                , :6 \n                , :7 \n                , :8 \n                , :9 \n                , :10 \n                , :11 \n                , :12 \n                , :13 \n                , :14 \n                , :15 \n                , :16 \n                , :17 \n                , :18 \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,newId);
   __sJT_st.setLong(2,__sJT_3564);
   __sJT_st.setDouble(3,__sJT_3565);
   __sJT_st.setDouble(4,__sJT_3566);
   __sJT_st.setDouble(5,__sJT_3567);
   __sJT_st.setString(6,__sJT_3568);
   __sJT_st.setString(7,__sJT_3569);
   __sJT_st.setString(8,__sJT_3570);
   __sJT_st.setString(9,__sJT_3571);
   __sJT_st.setString(10,__sJT_3572);
   __sJT_st.setString(11,__sJT_3573);
   __sJT_st.setString(12,__sJT_3574);
   __sJT_st.setString(13,__sJT_3575);
   __sJT_st.setString(14,__sJT_3576);
   __sJT_st.setString(15,__sJT_3577);
   __sJT_st.setDouble(16,__sJT_3578);
   __sJT_st.setDouble(17,__sJT_3579);
   __sJT_st.setDouble(18,__sJT_3580);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:829^15*/

            result.addResultDescriptor("Supply Order #"+newId+" created.");

            //now, cycle through the items and insert the pertinent info
            try{
              Iterator it = order.getItemIterator();
              while (it.hasNext()){
                soi = (SupplyOrderItem)it.next();
                /*@lineinfo:generated-code*//*@lineinfo:838^17*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO supply_order_item
//                      (supply_item_id
//                      ,supply_order_id
//                      ,quantity
//                      ,cost
//                      ,ship_cost
//                      ,group_name
//                      )
//                    VALUES
//                      (
//                      :soi.id
//                      ,:newId
//                      ,:soi.quantity
//                      ,:soi.getPrice()
//                      ,:soi.shipCost
//                      ,:soi.groupName
//                      )
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_3581 = soi.getPrice();
   String theSqlTS = "INSERT INTO supply_order_item\n                    (supply_item_id\n                    ,supply_order_id\n                    ,quantity\n                    ,cost\n                    ,ship_cost\n                    ,group_name\n                    )\n                  VALUES\n                    (\n                     :1 \n                    , :2 \n                    , :3 \n                    , :4 \n                    , :5 \n                    , :6 \n                    )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,soi.id);
   __sJT_st.setLong(2,newId);
   __sJT_st.setInt(3,soi.quantity);
   __sJT_st.setDouble(4,__sJT_3581);
   __sJT_st.setDouble(5,soi.shipCost);
   __sJT_st.setString(6,soi.groupName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:857^17*/
              }
            }catch(Exception e){
              result.addResultDescriptor("SupplyOrderPosting.doAction(): newOrderItems EXCEPTION: "+e.getMessage());
            }

          }catch(Exception e){
            result.addResultDescriptor("SupplyOrderPosting.doAction(): newOrder EXCEPTION: "+e.getMessage());
            /*
            log.debug(""+newId);
            log.debug(""+acrOp.bean.getACR().getID());
            log.debug(""+order.getHandleFee());
            log.debug(""+order.getTaxRate());
            log.debug(""+order.getMarkUp());
            log.debug(""+System.currentTimeMillis());
            log.debug(""+order.getShipName());
            log.debug(""+order.getShipLine1());
            log.debug(""+order.getShipLine2());
            log.debug(""+order.getShipCity());
            log.debug(""+order.getShipState());
            log.debug(""+order.getShipZip());
            log.debug(""+acrOp.bean.getACR().getMerchantNum());
            log.debug(""+ (order.isRush?"y":"n"));
            log.debug(""+acrOp.bean.getACR().getACRValue("Requestor's Name"));
            log.debug(""+acrOp.bean.getACR().getACRValue("Requestor's Phone #"));
            */
          }finally{
            cleanUp();
          }
          break;

        case MesQueues.Q_CHANGE_REQUEST_CANCELLED:

          //NOT USED!!
          //update supply order to cancelled.
          //using ACRID
          int status = SupplyOrder.CANCELLED;
          try{
            connect();
            /*@lineinfo:generated-code*//*@lineinfo:896^13*/

//  ************************************************************
//  #sql [Ctx] { UPDATE supply_order
//                SET status=:status
//                WHERE acrid=:acrOp.bean.getACR().getID()
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3582 = acrOp.bean.getACR().getID();
   String theSqlTS = "UPDATE supply_order\n              SET status= :1 \n              WHERE acrid= :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,status);
   __sJT_st.setLong(2,__sJT_3582);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:901^13*/
          }catch(Exception e){
            result.addResultDescriptor("SupplyOrderPosting.doAction(): cancelOrder EXCEPTION: "+e.getMessage());
          }finally{
            cleanUp();
          }
          break;

      }//end switch
    }//end doAction

  }//SupplyOrderPosting

  /**
   * ContactInfoUpdate class
   * updates USERS table in db to reflect
   * ACR email address change (if present)
   **/
  public class ContactInfoUpdate implements AcrAction
  {
    private AcrActionResult result = null;

    public boolean opHasAction(AcrOp acrOp){
      return (
          acrOp.bean.getACRDefinition().getShortName().equals("merccntctinfo") ||
          acrOp.bean.getACRDefinition().getShortName().equals("merccntctinfo_v1") ||
          acrOp.bean.getACRDefinition().getShortName().equals("merccntctinfo_v2") ||
          acrOp.bean.getACRDefinition().getShortName().equals("merccntctinfo_mes")
      );
    }

    public AcrActionResult getActionResult(){
      return result;
    }

    public String toString(){
      return "Merchant Contact Info Update";
    }

    public void doAction(AcrOp acrOp)
    {

      result = new AcrActionResult();

      //switch on queOp
      switch(acrOp.dstQ)
      {
        case MesQueues.Q_CHANGE_REQUEST_COMPLETED:

          String email = acrOp.bean.getACR().getACRValue("email_address");
          if(email != null && email.length()>0)
          {
            try{
              connect();
              /*@lineinfo:generated-code*//*@lineinfo:955^15*/

//  ************************************************************
//  #sql [Ctx] { UPDATE
//                    users
//                  SET
//                    email = :email
//                  WHERE
//                    hierarchy_node =:acrOp.bean.getMerchantNumber()
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3583 = acrOp.bean.getMerchantNumber();
  try {
   String theSqlTS = "UPDATE\n                  users\n                SET\n                  email =  :1 \n                WHERE\n                  hierarchy_node = :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,email);
   __sJT_st.setString(2,__sJT_3583);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:963^15*/
              result.addResultDescriptor("ContactInfoUpdate completed.");
            }catch(Exception e){
              result.addResultDescriptor("ContactInfoUpdate.doAction(): EXCEPTION: "+e.getMessage());
            }finally{
              cleanUp();
            }
          }
          else
          {
            result.addResultDescriptor("ContactInfoUpdate.doAction(): EXCEPTION: email is null");
          }
          break;

        default:
          break;
      }
    }

  }//ContactInfoUpdate class

  /**
   * Certegy Check Update class
   */
  //TODO othercardmgmt_cbt
  public class CertegyCheckUpdate implements AcrAction
  {
    private   AcrActionResult     Result    = null;

    public void doAction(AcrOp acrOp)
    {
      long            appSeqNum     = 0L;
      int             cardNum       = 0;
      String          cardTid       = null;
      String          checkProvider = null;

      try
      {
        connect();

        Result = new AcrActionResult();

        switch(acrOp.dstQ)
        {
          case MesQueues.Q_CHANGE_REQUEST_COMPLETED:    // request completed
            appSeqNum = acrOp.bean.getAppSeqNum();

            /*@lineinfo:generated-code*//*@lineinfo:1010^13*/

//  ************************************************************
//  #sql [Ctx] { delete
//                from    merchpayoption mpo
//                where   mpo.app_seq_num = :appSeqNum and
//                        mpo.cardtype_code = :mesConstants.APP_CT_CHECK_AUTH
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n              from    merchpayoption mpo\n              where   mpo.app_seq_num =  :1  and\n                      mpo.cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_CHECK_AUTH);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1016^13*/

            /*@lineinfo:generated-code*//*@lineinfo:1018^13*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(max(mpo.card_sr_number),0)+1 
//                from    merchpayoption mpo
//                where   mpo.app_seq_num = :appSeqNum
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(max(mpo.card_sr_number),0)+1  \n              from    merchpayoption mpo\n              where   mpo.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.acr.ACRPump",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cardNum = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1023^13*/
            cardTid       = acrOp.bean.getACR().getACRValue("checkTermId");
            checkProvider = acrOp.bean.getACR().getACRValue("checkProvider");

            /*@lineinfo:generated-code*//*@lineinfo:1027^13*/

//  ************************************************************
//  #sql [Ctx] { insert into  merchpayoption
//                (
//                  app_seq_num,
//                  cardtype_code,
//                  merchpo_card_merch_number,
//                  merchpo_provider_name,
//                  card_sr_number,
//                  merchpo_tid
//                )
//                values
//                (
//                  :appSeqNum,
//                  :mesConstants.APP_CT_CHECK_AUTH,
//                  :cardTid,
//                  :checkProvider,
//                  :cardNum,
//                  :cardTid
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into  merchpayoption\n              (\n                app_seq_num,\n                cardtype_code,\n                merchpo_card_merch_number,\n                merchpo_provider_name,\n                card_sr_number,\n                merchpo_tid\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                 :5 ,\n                 :6 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_CHECK_AUTH);
   __sJT_st.setString(3,cardTid);
   __sJT_st.setString(4,checkProvider);
   __sJT_st.setInt(5,cardNum);
   __sJT_st.setString(6,cardTid);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1047^13*/
            break;

          default:
            break;
        }
      }
      catch( Exception e )
      {
        Result.addResultDescriptor("CertegyCheckUpdate: EXCEPTION: " + e.toString() );
      }
      finally
      {
        cleanUp();
      }
    }

    public AcrActionResult getActionResult()
    {
      return( Result );
    }

    public boolean opHasAction(AcrOp acrOp)
    {
      boolean     retVal      = false;

      if ( acrOp.bean.getACRDefinition().getShortName().equals("cardadds") ||
           acrOp.bean.getACRDefinition().getShortName().equals("cardadds_mes")  )
      {
        String addingCheck = acrOp.bean.getACR().getACRValue("check");

        if ( addingCheck != null && addingCheck.equals("y") )
        {
          retVal = true;
        }
      }
      /*
      else if ( acrOp.bean.getACRDefinition().getShortName().equals("cardadds_v2") )
      {
        String addingCheck = acrOp.bean.getACR().getACRValue("certegy");

        if ( addingCheck != null && addingCheck.equals("y") )
        {
          retVal = true;
        }
      }
      */
      return( retVal );
    }

    public String toString()
    {
      return "Certegy Check Update";
    }
  } // Certegy Check Update

  /**
   * Valutec Update class
   */
  public class ValutecUpdate implements AcrAction
  {
    private   AcrActionResult     Result    = null;

    public void doAction(AcrOp acrOp)
    {
      long            appSeqNum     = 0L;
      int             cardNum       = 0;
      String          cardTid       = null;

      try
      {
        connect();

        Result = new AcrActionResult();

        switch(acrOp.dstQ)
        {
          case MesQueues.Q_CHANGE_REQUEST_COMPLETED:    // request completed
            appSeqNum = acrOp.bean.getAppSeqNum();

            /*@lineinfo:generated-code*//*@lineinfo:1127^13*/

//  ************************************************************
//  #sql [Ctx] { delete
//                from    merchpayoption mpo
//                where   mpo.app_seq_num = :appSeqNum and
//                        mpo.cardtype_code = :mesConstants.APP_CT_VALUTEC_GIFT_CARD
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n              from    merchpayoption mpo\n              where   mpo.app_seq_num =  :1  and\n                      mpo.cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VALUTEC_GIFT_CARD);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1133^13*/

            /*@lineinfo:generated-code*//*@lineinfo:1135^13*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(max(mpo.card_sr_number),0)+1 
//                from    merchpayoption mpo
//                where   mpo.app_seq_num = :appSeqNum
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(max(mpo.card_sr_number),0)+1  \n              from    merchpayoption mpo\n              where   mpo.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.acr.ACRPump",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cardNum = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1140^13*/
            cardTid = acrOp.bean.getACR().getACRValue("valutecTermId");

            /*@lineinfo:generated-code*//*@lineinfo:1143^13*/

//  ************************************************************
//  #sql [Ctx] { insert into  merchpayoption
//                (
//                  app_seq_num,
//                  cardtype_code,
//                  merchpo_card_merch_number,
//                  card_sr_number,
//                  merchpo_tid
//                )
//                values
//                (
//                  :appSeqNum,
//                  :mesConstants.APP_CT_VALUTEC_GIFT_CARD,
//                  :cardTid,
//                  :cardNum,
//                  :cardTid
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into  merchpayoption\n              (\n                app_seq_num,\n                cardtype_code,\n                merchpo_card_merch_number,\n                card_sr_number,\n                merchpo_tid\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                 :5 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_VALUTEC_GIFT_CARD);
   __sJT_st.setString(3,cardTid);
   __sJT_st.setInt(4,cardNum);
   __sJT_st.setString(5,cardTid);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1161^13*/
            break;

          default:
            break;
        }
      }
      catch( Exception e )
      {
        Result.addResultDescriptor("ValutecUpdate: EXCEPTION: " + e.toString() );
      }
      finally
      {
        cleanUp();
      }
    }

    public AcrActionResult getActionResult()
    {
      return( Result );
    }

    public boolean opHasAction(AcrOp acrOp)
    {
      boolean     retVal      = false;

      if ( acrOp.bean.getACRDefinition().getShortName().equals("cardadds") ||
           acrOp.bean.getACRDefinition().getShortName().equals("cardadds_mes")  )
      {
        String addingValutec = acrOp.bean.getACR().getACRValue("valutec");

        if ( addingValutec != null && addingValutec.equals("y") )
        {
          retVal = true;
        }
      }
      return( retVal );
    }

    public String toString()
    {
      return "ValutecUpdate";
    }
  } // Valutec Update

  /**
   * SupplyBillingConfig class
   * updates USERS table in db to reflect
   * ACR email address change (if present)
   **/
  public class SupplyBillingConfig implements AcrAction
  {
    public double MONTHLY_BILL_RATE = 8.95d;
    public double _MARK_UP = 1.3d;
    public double _HANDLE_RUSH = 10d;
    public double _HANDLE_STANDARD = 5d;

    public String ACH_STATEMENT_MESSAGE = "MONTHLY FEE - SUPPLIES";

    private AcrActionResult result = null;

    public boolean opHasAction(AcrOp acrOp){
      return acrOp.bean.getACRDefinition().getShortName().equals("supbilconf");
    }

    public AcrActionResult getActionResult(){
      return result;
    }

    public String toString(){
      return "Supply Billing Configuration";
    }

    public void doAction(AcrOp acrOp)
    {

      result = new AcrActionResult();

      //switch on queOp
      switch(acrOp.dstQ)
      {
        case MesQueues.Q_CHANGE_REQUEST_COMPLETED:
          String billingConfig = acrOp.bean.getACR().getACRValue("supplyBillingConfig");
          String merchantNumber = acrOp.bean.getMerchantNumber();
          long acrid = acrOp.bean.getACR().getID();

          if(billingConfig != null)
          {
            try{

              //want to ensure that everything gets done before we commit;
              connect(false);

              cleanBillingInfo(merchantNumber);

              if(billingConfig.equals("per_order"))
              {
                insertConfig(acrid, merchantNumber, _MARK_UP, _HANDLE_RUSH, _HANDLE_STANDARD);
              }
              else
              {
                //do this in case an ancestor has billing applied
                //we could search that, and leave the config alone if
                //it matches, but then we wouldn't have a quick record
                //of this ACR transaction in the supply_config table
                insertConfig(acrid, merchantNumber, 0, 0, 0);

                if (billingConfig.equals("monthly"))
                {
                  insertAutoBilling(merchantNumber);
                }

              }

              commit();
              result.addResultDescriptor("SupplyBillingConfig completed.");

            }
            catch(Exception e)
            {
              rollback();
              result.addResultDescriptor("SupplyBillingConfig.doAction(): EXCEPTION: "+e.getMessage());
            }
            finally
            {
              cleanUp();
            }
          }
          else
          {
            result.addResultDescriptor("SupplyBillingConfig.doAction(): EXCEPTION: Billing Configuration is invalid");
          }
          break;

        default:
          break;
      }
    }

    private void cleanBillingInfo(String merchantNumber)
    throws Exception
    {
      /*@lineinfo:generated-code*//*@lineinfo:1303^7*/

//  ************************************************************
//  #sql [Ctx] { delete from supply_config
//          where association_node = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from supply_config\n        where association_node =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1307^7*/

      /*@lineinfo:generated-code*//*@lineinfo:1309^7*/

//  ************************************************************
//  #sql [Ctx] { delete from billing_auto_cg
//          where merchant_number = :merchantNumber
//          and statement_message = :ACH_STATEMENT_MESSAGE
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from billing_auto_cg\n        where merchant_number =  :1 \n        and statement_message =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   __sJT_st.setString(2,ACH_STATEMENT_MESSAGE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1314^7*/
    }

    private void insertAutoBilling(String merchantNumber)
    throws Exception
    {

      /*@lineinfo:generated-code*//*@lineinfo:1321^7*/

//  ************************************************************
//  #sql [Ctx] { insert into billing_auto_cg
//          (
//            merchant_number,
//            charge_amount,
//            statement_message,
//            billing_months,
//            date_expiration
//          )
//          values
//          (
//            :merchantNumber,
//            :MONTHLY_BILL_RATE,
//            :ACH_STATEMENT_MESSAGE,
//            'YYYYYYYYYYYY', -- indicates that this charge should fire every month
//            '31-Dec-9999' -- indicates no expiration
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into billing_auto_cg\n        (\n          merchant_number,\n          charge_amount,\n          statement_message,\n          billing_months,\n          date_expiration\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n          'YYYYYYYYYYYY', -- indicates that this charge should fire every month\n          '31-Dec-9999' -- indicates no expiration\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   __sJT_st.setDouble(2,MONTHLY_BILL_RATE);
   __sJT_st.setString(3,ACH_STATEMENT_MESSAGE);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1339^7*/

    }

    private void insertConfig(long acrid,
                              String merchantNumber,
                              double markUp,
                              double handleRush,
                              double handleStandard)
    throws Exception
    {
      /*@lineinfo:generated-code*//*@lineinfo:1350^7*/

//  ************************************************************
//  #sql [Ctx] { insert into supply_config
//          (
//          markup_multiplier
//          ,association_node
//          ,handling_standard
//          ,handling_rush
//          ,acrid
//          ,date_initialized
//          )
//          values
//          (
//          :markUp
//          ,:merchantNumber
//          ,:handleStandard
//          ,:handleRush
//          ,:acrid
//          ,sysdate
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into supply_config\n        (\n        markup_multiplier\n        ,association_node\n        ,handling_standard\n        ,handling_rush\n        ,acrid\n        ,date_initialized\n        )\n        values\n        (\n         :1 \n        , :2 \n        , :3 \n        , :4 \n        , :5 \n        ,sysdate\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,markUp);
   __sJT_st.setString(2,merchantNumber);
   __sJT_st.setDouble(3,handleStandard);
   __sJT_st.setDouble(4,handleRush);
   __sJT_st.setLong(5,acrid);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1370^7*/
    }

  }//SupplyBillingConfig class


  /**
   * ShippingAddressUpdate class
   * updates ADDRESS table in db to reflect
   * ACR 'other address' insertion
   **/


  public class ShippingAddressUpdate implements AcrAction
  {

    private AcrActionResult result = null;
    long appSeqNum = -1L;
    boolean updateTblPackingSlip = false;
    boolean updateTblAddress = false;
    boolean old = true;

    public boolean opHasAction(AcrOp acrOp){

      if (acrOp.bean.getACRDefinition().getShortName().equals("equipupg") ||
              acrOp.bean.getACRDefinition().getShortName().equals("equipupg_v1") ||
              acrOp.bean.getACRDefinition().getShortName().equals("reqtrmprg") ||
              acrOp.bean.getACRDefinition().getShortName().equals("reqtrmprg_v1") ||
              acrOp.bean.getACRDefinition().getShortName().equals("equipswap") ||
              acrOp.bean.getACRDefinition().getShortName().equals("equipswap_v1") ||
              acrOp.bean.getACRDefinition().getShortName().equals("equipswap_nbsc"))
      {
        return true;
      }
      else if (acrOp.bean.getACRDefinition().getShortName().equals("equipupg_v2") ||
              acrOp.bean.getACRDefinition().getShortName().equals("reqtrmprg_v2") ||
              acrOp.bean.getACRDefinition().getShortName().equals("equipswap_v2")||
              acrOp.bean.getACRDefinition().getShortName().equals("equipupg_mes") ||
              acrOp.bean.getACRDefinition().getShortName().equals("reqtrmprg_mes") ||
              acrOp.bean.getACRDefinition().getShortName().equals("equipswap_mes")
              )
      {
        old = false;
        return true;
      }
      else
      {
        return false;
      }
    }

    public AcrActionResult getActionResult(){
      return result;
    }

    public String toString(){
      return "ACR Shipping Address Update";
    }

    private boolean isAddressUpdateRequired(AcrOp acrOp, AcrActionResult result)
    {

      log.debug("STARTING: isAddressUpdateRequired");
      boolean isRequired = true;
      String address1 = null;

      try
      {

        connect();

        //look at PACKING_SLIP_INFO first based on how address is
        //determined in NewPackingSlipBean.getShipping Address
        /*@lineinfo:generated-code*//*@lineinfo:1443^9*/

//  ************************************************************
//  #sql [Ctx] { select
//              ship_address_line1 
//            from
//              packing_slip_info
//            where
//              app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select\n            ship_address_line1  \n          from\n            packing_slip_info\n          where\n            app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.acr.ACRPump",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   address1 = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1451^9*/

        if(address1 == null)
        {
          //look at ADDRESS next
          /*@lineinfo:generated-code*//*@lineinfo:1456^11*/

//  ************************************************************
//  #sql [Ctx] { select
//                address_line1 
//              from
//                address
//              where
//                app_seq_num = :appSeqNum
//              and
//                addresstype_code = mesConstants.ADDR_TYPE_SHIPPING
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select\n              address_line1  \n            from\n              address\n            where\n              app_seq_num =  :1 \n            and\n              addresstype_code = mesConstants.ADDR_TYPE_SHIPPING";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.acr.ACRPump",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   address1 = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1466^11*/
        }
        else
        {
          //flag this for later - need to know which table to update.
          updateTblPackingSlip = true;
        }

        if(address1 != null)
        {
          address1 = address1.trim();

          //check this address out versus the one held by the ACR
          String acrAddress1;

          if(old)
          {
            acrAddress1 = acrOp.bean.getACR().getACRValue("sh_address");
          }
          else
          {
            acrAddress1 = acrOp.bean.getACR().getACRValue("ShipAddrLine1");
          }

          acrAddress1.trim();

          if(acrAddress1.equalsIgnoreCase(address1))
          {
            //address needs to be exact, or else we change it
            isRequired = false;
          }

        }

      }catch(Exception e){

        log.debug("Exception: "+e.getMessage());
        result.addResultDescriptor("ShippingAddressUpdate.isAddressUpdateRequired(): EXCEPTION: "+e.getMessage());

      }finally{

        cleanUp();

      }
      log.debug("ENDING: isAddressUpdateRequired: \nisRequired = "+isRequired+"\nupdateTblPackingSlip = "+updateTblPackingSlip+"\nupdateTblAddress = "+updateTblAddress);
      return isRequired;

    }

    public void doAction(AcrOp acrOp)
    {

      result = new AcrActionResult();
      ACR acr = acrOp.bean.getACR();
      appSeqNum = acrOp.bean.getACR().getAppSeqNum();

      log.debug("TESTING: appSeqNum (from ACR) = "+appSeqNum);

      if(appSeqNum < 0)
      {
        //not there in ACR, try merchant table
        try
        {
          connect();

          /*@lineinfo:generated-code*//*@lineinfo:1531^11*/

//  ************************************************************
//  #sql [Ctx] { select
//                app_seq_num 
//              from
//                merchant
//              where
//                merch_number = :acrOp.bean.getMerchantNumber()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3584 = acrOp.bean.getMerchantNumber();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select\n              app_seq_num  \n            from\n              merchant\n            where\n              merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.acr.ACRPump",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3584);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1539^11*/

        }
        catch(Exception e)
        {
          //leave as -1
          log.debug("Exception: "+e.getMessage());

          //for testing only
          //appSeqNum = 1363;

        }
        finally
        {
          cleanUp();
        }

      }

      //switch on queOp
      switch(acrOp.dstQ)
      {
        case MesQueues.Q_CHANGE_REQUEST_COMPLETED:

          if(appSeqNum > 0 && isAddressUpdateRequired(acrOp, result))
          {
            try{

              String addressName = null;
              String addressLine1 = null;
              String addressLine2 = null;
              String addressCity = null;
              String addressState = null;
              String addressZip = null;
              String addressContact = null;

              //find the ACR address depending on old version of addressbean
              //or new version (AddressHelper)
              if(old)
              {
                addressName = acr.getACRValue("sh_name");
                if(addressName.equals(""))
                {
                  addressName = acr.getACRValue("sh_bnknme");
                }
                addressLine1 = acr.getACRValue("sh_address");
                addressLine2 = "";
                addressCity = acr.getACRValue("sh_city");
                addressState = acr.getACRValue("sh_state");
                addressZip = acr.getACRValue("sh_zip");
                addressContact = acr.getACRValue("sh_attn");
                if(addressContact.equals(""))
                {
                  addressContact = addressName;
                }
              }
              else
              {
                addressName = acr.getACRValue("shipAddrName");
                addressLine1 = acr.getACRValue("shipAddrLine1");
                addressLine2 = acr.getACRValue("shipAddrLine2");
                StringBuffer cityBuf = new StringBuffer();
                StringTokenizer tok = new StringTokenizer(acr.getACRValue("shipAddrCsz")," ,");
                LinkedList list = new LinkedList();
                while (tok.hasMoreTokens())
                {
                  list.add(tok.nextToken());
                }

                while (!list.isEmpty())
                {
                  switch (list.size())
                  {
                    case 1:
                      addressZip = (String)list.removeFirst();
                      break;

                    case 2:
                      addressState = (String)list.removeFirst();
                      break;

                    default:
                      if (cityBuf.length() > 0)
                      {
                        cityBuf.append(" ");
                      }
                      cityBuf.append((String)list.removeFirst());
                      break;
                  }
                }

                if (cityBuf.length() > 0)
                {
                  addressCity = cityBuf.toString();
                }

                addressContact = addressName;
              }


              connect();

              if(updateTblPackingSlip)
              {
                /*@lineinfo:generated-code*//*@lineinfo:1643^17*/

//  ************************************************************
//  #sql [Ctx] { update
//                      packing_slip_info
//                    set
//                      ship_name = :addressName,
//                      ship_address_line1 = :addressLine1,
//                      ship_address_line2 = :addressLine2,
//                      ship_city = :addressCity,
//                      ship_state = :addressState,
//                      ship_zip = :addressZip,
//                      ship_contact_name = :addressContact
//                    where
//                      app_seq_num = :appSeqNum
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "update\n                    packing_slip_info\n                  set\n                    ship_name =  :1 ,\n                    ship_address_line1 =  :2 ,\n                    ship_address_line2 =  :3 ,\n                    ship_city =  :4 ,\n                    ship_state =  :5 ,\n                    ship_zip =  :6 ,\n                    ship_contact_name =  :7 \n                  where\n                    app_seq_num =  :8";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,addressName);
   __sJT_st.setString(2,addressLine1);
   __sJT_st.setString(3,addressLine2);
   __sJT_st.setString(4,addressCity);
   __sJT_st.setString(5,addressState);
   __sJT_st.setString(6,addressZip);
   __sJT_st.setString(7,addressContact);
   __sJT_st.setLong(8,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1657^17*/
              }
              else
              {
                if(updateTblAddress)
                {
                  /*@lineinfo:generated-code*//*@lineinfo:1663^19*/

//  ************************************************************
//  #sql [Ctx] { update
//                        address
//                      set
//                        address_name = :addressName,
//                        address_line1 = :addressLine1,
//                        address_line2 = :addressLine2,
//                        address_city = :addressCity,
//                        countrystate_code = :addressState,
//                        address_zip = :addressZip
//                      where
//                        app_seq_num = :appSeqNum
//                      and
//                        addresstype_code = mesConstants.ADDR_TYPE_SHIPPING
//                     };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "update\n                      address\n                    set\n                      address_name =  :1 ,\n                      address_line1 =  :2 ,\n                      address_line2 =  :3 ,\n                      address_city =  :4 ,\n                      countrystate_code =  :5 ,\n                      address_zip =  :6 \n                    where\n                      app_seq_num =  :7 \n                    and\n                      addresstype_code = mesConstants.ADDR_TYPE_SHIPPING";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,addressName);
   __sJT_st.setString(2,addressLine1);
   __sJT_st.setString(3,addressLine2);
   __sJT_st.setString(4,addressCity);
   __sJT_st.setString(5,addressState);
   __sJT_st.setString(6,addressZip);
   __sJT_st.setLong(7,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1678^19*/
                }
                else
                {
                  /*@lineinfo:generated-code*//*@lineinfo:1682^19*/

//  ************************************************************
//  #sql [Ctx] { insert into address
//                      (
//                        app_seq_num,
//                        addresstype_code,
//                        address_name,
//                        address_line1,
//                        address_city,
//                        countrystate_code,
//                        address_zip
//                      )
//                      values
//                      (
//                        :appSeqNum,
//                        :mesConstants.ADDR_TYPE_SHIPPING,
//                        :addressName,
//                        :addressLine1,
//                        :addressCity,
//                        :addressState,
//                        :addressZip
//                      )
//                     };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into address\n                    (\n                      app_seq_num,\n                      addresstype_code,\n                      address_name,\n                      address_line1,\n                      address_city,\n                      countrystate_code,\n                      address_zip\n                    )\n                    values\n                    (\n                       :1 ,\n                       :2 ,\n                       :3 ,\n                       :4 ,\n                       :5 ,\n                       :6 ,\n                       :7 \n                    )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_SHIPPING);
   __sJT_st.setString(3,addressName);
   __sJT_st.setString(4,addressLine1);
   __sJT_st.setString(5,addressCity);
   __sJT_st.setString(6,addressState);
   __sJT_st.setString(7,addressZip);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1704^19*/
                }
              }

              result.addResultDescriptor("ShippingAddressUpdate completed.");

            }catch(Exception e){
              log.debug("Exception: "+e.getMessage());
              result.addResultDescriptor("ShippingAddressUpdate.doAction(): EXCEPTION: "+e.getMessage());

            }finally{

              cleanUp();
            }
          }
          break;

        default:
          break;
      }

      log.debug("ENDING: doAction");
    }

  }//ShippingAddressUpdate class

  /**
   * Email Notification for Mgmt Queue (includes original CBT func)
   */
  public class NotifyMgmtByEmail implements AcrAction
  {

    private   AcrActionResult     Result    = null;
    private   int                 addresses;

    public boolean opHasAction(AcrOp acrOp)
    {

      //fires if mgmt q notification is on, and item is being moved to mgmt Q
      if ( acrOp.bean.getMgmtQInfo().isCurrentQueue(acrOp.dstQ) &&
           acrOp.bean.getMgmtQInfo().getEmailRef() > -1)
      {
        addresses = acrOp.bean.getMgmtQInfo().getEmailRef();
        return true;
      }

      //OR new CBT risk mgmt Bean/Q (4/18/06)
      //ACRDEF table has 78 as this ACR def ID, srcQ is -1 as this only goes to risk Queue
      //and EMAIL_REF = 82 and 93 respectively
      if ( acrOp.srcQ == -1)
      {
        if(acrOp.bean.getACRDefinition().getID() == 78)
        {
          addresses = ACRDataBean_CreditRisk_CBT.EMAIL_REF;
          return true;
        }
        if(acrOp.bean.getACRDefinition().getID() == 35)
        {
          addresses = ACRDataBean_PriceChng_CBT_v2.EMAIL_REF;
          return true;
        }
      }

      return false;
    }

    public void doAction(AcrOp acrOp)
    {
      try
      {
        Result = new AcrActionResult();

        String subject  =  "Merchant "
                          + acrOp.bean.getACR().getMerchantNum()
                          + ": "
                          + "ACR #" + acrOp.bean.getACR().getID()
                          + " ("+ acrOp.bean.getACRDefinition().getName()+ ")";

        StringBuffer message  = new StringBuffer("");
        message.append("Please review your Management Queue regarding the above referenced ACR.");
        message.append("\n\n");
        message.append("Thank you,");
        message.append("\n");
        message.append("Merchant e-Solutions");
        message.append("\n");
        message.append("(Auto-generated email: Please DO NOT reply.)");

        log.debug("preparing email in NotifyMgmtByEmail...");

        MailMessage msg = new MailMessage();
        msg.setAddresses(addresses);
        msg.setSubject(subject.toString());
        msg.setText(message.toString());
        msg.send();


      }
      catch( Exception e )
      {
        Result.addResultDescriptor("NotifyMgmtByEmail: EXCEPTION: " + e.toString() );
        //e.printStackTrace();
      }
    }

    public AcrActionResult getActionResult()
    {
      return( Result );
    }

    public String toString()
    {
      return "Mgmt Email Notification";
    }
  } // NotifyMgmtByEmail


  /**
   * Generate inventory order in new inventory system
   * (as of 4-30-07)
   */
  public class GenerateInventoryOrder implements AcrAction
  {
    private AcrActionResult     Result  = null;
    private Order               order   = null;
    private EquipSelectorItem   item    = null;

    public boolean opHasAction(AcrOp acrOp)
    {
      // old logic:
      //  fires if acr has callTagged Items and srcQ = -1
      //  ignore potential problem on Debit ACRs for now
      // new logic:
      //  don't check for call tagged equipment, only make sure
      //  srcQ is -1 and this is not 'othercardmgmt' ACR
      // new new logic:
      //  return true if apps is one of the subset of deployment
      //  related acrs and srcQ is -1 (new ACR)
      int acrDefId = (int)(acrOp.bean.getACRDefinition().getID());
      switch(acrDefId)
      {
        //account close
        case 4:
        case 30:
        case 41:
        case 63:
        //equipment pickup
        case 28:
        case 69:
        //equipment swap
        case 12:
        case 29:
        case 49:
        case 56:
        case 76:
        //equipment add/upgrade
        case 13:
        case 33:
        case 50:
        case 68:
          // return true if this is new ACR
          return acrOp.srcQ == -1;
      }
      // non deployment ACR, no action
      return false;
    }

    /**
     * Tries to derive an acr action type code from the acr.
     *
     * TODO: change individual ACR pages to use com.mes.tmg.CallTagReason
     *       codes so this translator function doesn't have to exist.
     *       associate each individual piece of equipment on the ACR with
     *       it's own action type code (modify the equip selector class)
     *       see notes in doAction regarding this...
     */
    private String generateCtrCode(AcrOp acrOp)    
    {
      int acrDefId = (int)(acrOp.bean.getACRDefinition().getID());
      switch(acrDefId)
      {
        //account close
        case 4:
        case 30:
        case 41:
        case 63:
          return CallTagReason.CTRC_ACCT_CLOSE;

        //equipment pickup
        case 28:
        case 69:
          String rr = acrOp.bean.getACRValue("returnReason").toLowerCase();
          if (rr.equals("closure"))
          {
            return CallTagReason.CTRC_ACCT_CLOSE;
          }
          else if (rr.equals("rental return"))
          {
            return CallTagReason.CTRC_RENT_RETURN;
          }
          else if (rr.equals("defective"))
          {
            return CallTagReason.CTRC_DEFECTIVE;
          }
          break;

        //equipment swap
        case 12:
        case 29:
        case 49:
        case 56:
        case 76:
          String sd = acrOp.bean.getACRValue("swapDetails").toLowerCase();
          if (sd.equals("case"))
          {
            return CallTagReason.CTRC_CASE;
          }
          else if (sd.equals("display"))
          {
            return CallTagReason.CTRC_DISPLAY;
          }
          else if (sd.equals("keyboard"))
          {
            return CallTagReason.CTRC_KEYBOARD;
          }
          else if (sd.toLowerCase().equals("mag reader"))
          {
            return CallTagReason.CTRC_MAG_RDR;
          }
          else if (sd.equals("modem"))
          {
            return CallTagReason.CTRC_MODEM;
          }
          else if (sd.equals("printer"))
          {
            return CallTagReason.CTRC_PRINTER;
          }
          else if (sd.toLowerCase().equals("pinpad encryption"))
          {
            return CallTagReason.CTRC_ENCRYPT;
          }
          else if (sd.equals("programming"))
          {
            return CallTagReason.CTRC_PROGRAM;
          }
          break;

        //equipment upgrade
        case 13:
        case 33:
        case 50:
        case 68:
          return CallTagReason.CTRC_UPGRADE;

        // additional equipment
        default:
          return CallTagReason.CTRC_NEW;
      }

      // fall back to other
      return CallTagReason.CTRC_OTHER;
    }

    /**
     * default deployment reason generation
     * loosely based on ACR - deployreason mapping
     *
     *   DeployReason.DRC_NEW      = "NEW";
     *   DeployReason.DRC_ADD      = "ADD";
     *   DeployReason.DRC_SWAP     = "SWAP";
     *   DeployReason.DRC_LOAN     = "LOAN";
     *   DeployReason.DRC_CLOSE    = "CLOSE";
     *   DeployReason.DRC_EXCHANGE = "EXCHANGE";
     *   DeployReason.DRC_RETURN   = "RETURN";
     **/
    private String generateDrCode(long acrDef)
    {

      String drCode;

      switch((int)acrDef)
      {
        //account close
        case 4:
        case 30:
        case 41:
        case 63:
          drCode = DeployReason.DRC_CLOSE;
          break;

        //equipment pickup
        case 28:
        case 69:
          drCode = DeployReason.DRC_RETURN;
          break;

        //equipment swap
        case 12:
        case 29:
        case 49:
        case 56:
        case 76:
          drCode = DeployReason.DRC_SWAP;
          break;

        //equipment upgrade
        case 13:
        case 33:
        case 50:
        case 68:
          drCode = DeployReason.DRC_EXCHANGE;
          break;

        default:
          drCode = DeployReason.DRC_NEW;
      }

      return drCode;
    }

    private void generateOrderLink(ACRDataBeanBase bean) throws Exception
    {
      log.debug("Generating order link to ACR");
      long seqnum;
      String orderId = ""+order.getOrdId();
      String fieldName = ACRUtility.ORDER_FIELD_TAG;

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:2032^7*/

//  ************************************************************
//  #sql [Ctx] { select  acr_sequence.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  acr_sequence.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.acr.ACRPump",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   seqnum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2037^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2039^7*/

//  ************************************************************
//  #sql [Ctx] { insert into acritem
//          (acri_seq_num,acrid,name,value)
//          values
//          (
//            :seqnum
//            ,:bean.getACR().getID()
//            ,:fieldName
//            ,:orderId
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3585 = bean.getACR().getID();
   String theSqlTS = "insert into acritem\n        (acri_seq_num,acrid,name,value)\n        values\n        (\n           :1 \n          , :2 \n          , :3 \n          , :4 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.acr.ACRPump",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,seqnum);
   __sJT_st.setLong(2,__sJT_3585);
   __sJT_st.setString(3,fieldName);
   __sJT_st.setString(4,orderId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2050^7*/
    }

    /**
     * New version using AcrActionHelper to cause new acr action records
     * to be generated along with the order and call tag records.
     */
    public void doAction(AcrOp acrOp)
    {
      try
      {
        Result = new AcrActionResult();

        List items = acrOp.bean.getSelectedEquipment();

        String ctrCode = generateCtrCode(acrOp);
        String drCode = generateDrCode(
          acrOp.bean.getACRDefinition().getID());

        // if this is an account close acr, don't generate dst order if
        // there are no equipment items (PRF-943)
        if (drCode.equals(DeployReason.DRC_CLOSE) &&
            (items == null || items.size() == 0))
        {
          return;
        }

        // use acr action helper to start a new order
        AcrActionHelper helper = new AcrActionHelper();
        order = helper.startOrder(acrOp.bean.getACR().getID(),
                                  acrOp.bean.getUser());

        //process selected items in new inv sys
        if(items!=null && items.size()>0)
        {
          //for each item selected, generate an associated CallTagPart in CallTag
          for(int i=0;i<items.size();i++)
          {
            item = (EquipSelectorItem)items.get(i);
            try
            {
              // TODO: add getCallTagReason() to EquipSelectorItem
              //       and modify ACR's that generate call tags to
              //       have a drop down selector describing reason
              //       for part being call tagged (instead of current
              //       single drop down above part selector section)
              //       method must return a valid CallTagReason code
              String serialNum = item.getSerialNum();
              helper.addCallTagPart(drCode,serialNum,ctrCode);
            }
            catch (Exception e)
            {
              log.debug("Error adding call tag part: " + e);
              e.printStackTrace();
              Result.addResultDescriptor("Part SN#:" + item.getSerialNum()
                + " was NOT added to order as it does not currently exist"
                +" in inventory. Please view order and add manually.");
            }
          }//end for items.size()
        }

        // have helper finalize/persist order to db
        helper.finishOrder();

        //link orderId to the ACRItem table as standard field item
        generateOrderLink(acrOp.bean);
      }
      catch( Exception e )
      {
        Result.addResultDescriptor("GenerateInventoryOrder: EXCEPTION: " + e.toString() );
      }
      finally
      {
        cleanUp();
      }
    }

    public void originaldoAction(AcrOp acrOp)
    {
      try
      {
        Result = new AcrActionResult();

        List items = acrOp.bean.getSelectedEquipment();

        //process selected items in new inv sys
        if(items!=null && items.size()>0)
        {
          //create DB obj
          TmgDb db = new TmgDb();

          //makes order, using ACR and user
          order = db.createAcrOrder(acrOp.bean.getACR().getID(), acrOp.bean.getUser());

          //link orderId to the ACRItem table as standard field item
          generateOrderLink(acrOp.bean);

          //generate CallTag for the order
          com.mes.tmg.CallTag ct = generateCallTag(acrOp.bean.getUser(), db);

          //for each item selected, generate an associated CallTagPart in CallTag
          com.mes.tmg.CallTagPart ctPart;
          com.mes.tmg.Part part;
          long ctPartId;

          for(int i=0;i<items.size();i++)
          {
            item = (EquipSelectorItem)items.get(i);
            //log.debug(item.toString());
            ctPartId = db.getNewId();
            ctPart = new com.mes.tmg.CallTagPart();
            ctPart.setCtPartId(ctPartId);
            ctPart.setCtId(ct.getCtId());
            ctPart.setOrdId(order.getOrdId());
            ctPart.setCreateDate(db.getCurDate());
            ctPart.setUserName(acrOp.bean.getUser().getLoginName());
            ctPart.setCtpStatCode(CallTagPartStatus.CTPS_NEW);

            ctPart.setDrCode(generateDrCode(acrOp.bean.getACRDefinition().getID()));

            // if serial num is present, load part type info from the part state
            if (item.getSerialNum()!= null && !item.getSerialNum().equals(""))
            {
              try
              {
                part = db.getPartWithSerialNum(item.getSerialNum());
                //if(part !=null)
                //{
                  ctPart.setPartId(part.getPartId());
                  ctPart.setPtId(part.getPtId());
                  ctPart.setSerialNum(part.getSerialNum());
                //}
                //else
                //{
                  //currently NO PartType is required in ACRs...??
                  //ctPart.setPtId(??)
                  //ctPart.setSerialNum(item.getSerialNum());
                //}

                //put this here as we only can save those CallTagParts that
                //have a PartTypeId
                db.persist(ctPart,acrOp.bean.getUser());
              }
              catch(Exception noPart)
              {
                Result.addResultDescriptor("Part SN#:"+item.getSerialNum()+" was NOT added to order as it does not currently exist in inventory. Please view order and add manually.");
                //do nothing else - allow process to continue...
              }
            }

          }//end for items.size()

        }
        else
        {
          log.debug("No selected items found.");
        }

      }
      catch( Exception e )
      {
        Result.addResultDescriptor("GenerateInventoryOrder: EXCEPTION: " + e.toString() );
      }
      finally
      {
        cleanUp();
      }
    }

    private com.mes.tmg.CallTag generateCallTag(UserBean user, TmgDb db )
    throws Exception
    {
      long ctId = db.getNewId();
      com.mes.tmg.CallTag ct = new com.mes.tmg.CallTag();
      ct.setCtId(ctId);
      ct.setOrdId(order.getOrdId());
      ct.setUserName(user.getLoginName());
      ct.setCreateDate(db.getCurDate());
      ct.setCtStatCode(CallTagStatus.CTS_NEW);
      ct.setTrackingNum(null);
      db.persist(ct,user);
      return ct;
    }

    public AcrActionResult getActionResult()
    {
      return( Result );
    }

    public String toString()
    {
      return "Generate Inventory Order";
    }

  }//end GenerateInventoryOrder

  //

  /**
   * Email Notification for CBT Account Closure (30)
   * ONLY on early termination
   */
  public class EmailOnAcctClose implements AcrAction
  {

    private   AcrActionResult     Result    = null;
    private   int                 addresses = 90;

    public boolean opHasAction(AcrOp acrOp)
    {
      if( acrOp.bean.getACRDefinition().getID() == 30 &&
          acrOp.dstQ == MesQueues.Q_CHANGE_REQUEST_COMPLETED &&
          ((ACRDataBean_AcntClosure_CBT_v2)acrOp.bean).isEarlyCancellation()
        )

      {
        return true;
      }

      return false;

    }

    public void doAction(AcrOp acrOp)
    {
      try
      {
        Result = new AcrActionResult();

        ACRDataBean_AcntClosure_CBT_v2 bean = (ACRDataBean_AcntClosure_CBT_v2)acrOp.bean;

        String subject  =  "Merchant "
                          + bean.getACR().getMerchantNum()
                          + ": "
                          + "ACR #" + bean.getACR().getID()
                          + " ("+ bean.getACRDefinition().getName()+ ")";

        StringBuffer message  = new StringBuffer("");
        message.append("This merchant has been flagged for an Early Termination Fee (includes $0 fees):");
        message.append("\n\n");
        message.append("Merchant:\t\t\t").append(bean.getACR().getMerchantNum()).append("\n");
        message.append("DBA:\t\t\t\t").append(bean.getData("Merchant DBA")).append("\n");
        message.append("Term Contract:\t\t").append(bean.getTermContract()).append("\n");
        message.append("Termination Fee:\t\t").append(bean.getEarlyCancelFeeStr()).append("\n");
        message.append("ACR submission date:\t").append(bean.getACR().getDateCreated()).append("\n");
        message.append("\n\n");
        message.append("Thank you,");
        message.append("\n");
        message.append("Merchant e-Solutions");
        message.append("\n");
        message.append("(Auto-generated email: Please DO NOT reply.)");

        log.debug("preparing email in EmailOnAcctClose...");

        MailMessage msg = new MailMessage();
        msg.setAddresses(addresses);
        msg.setSubject(subject.toString());
        msg.setText(message.toString());
        msg.send();


      }
      catch( Exception e )
      {
        Result.addResultDescriptor("EmailOnAcctClose: EXCEPTION: " + e.toString() );
        e.printStackTrace();
      }
    }

    public AcrActionResult getActionResult()
    {
      return( Result );
    }

    public String toString()
    {
      return "Early Termination Notification";
    }
  } // EmailOnAcctClose

}/*@lineinfo:generated-code*/