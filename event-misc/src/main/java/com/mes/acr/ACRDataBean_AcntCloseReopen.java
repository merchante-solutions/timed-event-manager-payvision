package com.mes.acr;

import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.Validation;
import com.mes.support.StringUtilities;
import com.mes.tools.DropDownTable;

public class ACRDataBean_AcntCloseReopen extends ACRDataBeanBase
{
  // create class log category
  static Logger log = Logger.getLogger(ACRDataBean_AcntCloseReopen.class);

  // data members
  protected ACRDataBeanAddressHelper adrsHelper = null;
  protected EquipCallTagTable        ectt       = null;

  // construction
  public ACRDataBean_AcntCloseReopen()
  {
  }

  public ACRDataBean_AcntCloseReopen(ACR acr,MerchantInfo merchantInfo,
    Requestor requestor,boolean editable) throws Exception
  {
    super(acr,merchantInfo,requestor,editable);
  }

  public void extendedInit(ACR acr, MerchantInfo merchantInfo,
    Requestor requestor, boolean editable) throws Exception
  {
    // equip call tag table
    int[] ecctColumns = new int[]
      {
         EquipCallTagTable.COL_SENDCALLTAG
        ,EquipCallTagTable.COL_EQUIPTYPE
        ,EquipCallTagTable.COL_DESCRIPTION
        ,EquipCallTagTable.COL_SERIALNUMBER
        ,EquipCallTagTable.COL_PARTNUMBER
        ,EquipCallTagTable.COL_DISPOSITION
        ,EquipCallTagTable.COL_VNUMBER
      };
    String[][] ecctMap = new String[][]
      {
         {EquipCallTagTable.NCN_SENDCALLTAG,   "sct"}
        ,{EquipCallTagTable.NCN_PARTNUMBER,    "part_num"}
        ,{EquipCallTagTable.NCN_SERIALNUMBER,  "serial_num"}
        ,{EquipCallTagTable.NCN_VNUMBER,       "vnum"}
        ,{EquipCallTagTable.NCN_DISPOSITION,   "disposition"}
        ,{EquipCallTagTable.NCN_DESCRIPTION,   "equip_descriptor"}
        ,{EquipCallTagTable.NCN_EQUIPTYPE,     "type"}
      };
    ectt = new EquipCallTagTable(this, ecctColumns, ecctMap, 4, true);
  }

  public boolean equipmentExists()
  {
    return ectt.equipmentExists();
  }

  public boolean calltaggedEquipmentExists()
  {
    return ectt.calltaggedEquipmentExists();
  }

  public Vector getCallTaggedEquipment()
  {
    return ectt.getCallTaggedEquipment();
  }

  public String renderEquipCallTagTable()
  {
    return ectt.renderHtmlTable();
  }

  protected ACRDataBeanAddressHelper _getAddressHelper()
  {
    if(adrsHelper!=null)
      return adrsHelper; // already configured by virtue of non-null obj ref

    adrsHelper = new ACRDataBeanAddressHelper(this);

    // configure the address helper
    adrsHelper.setAdrsTypeName("Call Tag");
    adrsHelper.setAdrsSlctnFldName("ctaddress");
    //adrsHelper.setAdrsName("");
    adrsHelper.setAdrsAddressFldNme("ct_address");
    adrsHelper.setAdrsAttnFldNme("ct_attn");
    adrsHelper.setAdrsBankNameFldNme("ct_bnknme");
    adrsHelper.setAdrsCityFldNme("ct_city");
    adrsHelper.setAdrsStateFldNme("ct_state");
    adrsHelper.setAdrsZipFldNme("ct_zip");
    //adrsHelper.setAdrsCountry("ct_country");
    adrsHelper.setIsRequired(false);  // based on run-time form input data

    return adrsHelper;
  }

  public String renderAddressHtml()
  {
    return _getAddressHelper().renderAddressHtml();
  }

  public void doPull()
  {
    super.doPull();

    if(!editable)
      return;

    _getAddressHelper().pullAddressFields();
  }

  protected void setDefaults()
  {
    // parent related info
    if(getMerchantNumber().length()>0) {

      // merchant address
      _getAddressHelper().setACRAdrsInfo();

    }

  }

  protected void createExtendedFields()
  {
    if(!editable)
      return;

    if(acr==null)
      return;

    ACRDefinition acrd = acr.getDefinition();
    ACRDefItem acrdi=null;
    Field field=null;
    String fldnme;

    // address fields
    _getAddressHelper().createAddressFields();

    // equip call tag table fields
    ectt.createFields(this.fields);

    final MonthDropDownTable mddt = new MonthDropDownTable();

    if((acrdi=acrd.getACRDefItem("close"))!=null) {
      fields.add(ACRDefItemToField(acrdi));
      fields.add(new DropDownField("close_month",mddt,true));
    }
    if((acrdi=acrd.getACRDefItem("reopen"))!=null) {
      fields.add(ACRDefItemToField(acrdi));
      fields.add(new DropDownField("reopen_month",mddt,true));
    }

    if((acrdi=acrd.getACRDefItem("ct_address"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("ct_city"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("ct_state"))!=null)
      fields.add(ACRDefItemToField(acrdi));
    if((acrdi=acrd.getACRDefItem("ct_zip"))!=null)
      fields.add(ACRDefItemToField(acrdi));

    fields.addValidation(new EntryValidation());
  }

  public int[][] getAllowedQueueOps()
  {
    int bankNum = StringUtilities.stringToInt(acr.getACRValue("Application Bank Number"),-1);

    int initQueue = MesQueues.Q_CHANGE_REQUEST_RISK;
    //bankNum==3941? MesQueues.Q_CHANGE_REQUEST_RISK:MesQueues.Q_CHANGE_REQUEST_BBT_PENDING;

    return new int[][]
    {
       {MesQueues.Q_NONE,initQueue}

      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_RISK,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_RISK}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_TSYS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_MMS_ACCOUNT_SETUP,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_DEPLOYMENT,MesQueues.Q_CHANGE_REQUEST_CANCELLED}

      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_COMPLETED}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_RESEARCH_QA}
      ,{MesQueues.Q_CHANGE_REQUEST_ACTIVATIONS,MesQueues.Q_CHANGE_REQUEST_CANCELLED}
    };
  }

  protected String[] getCancellationReasons(int crntQ)
  {
    String[] rsns;

    switch(crntQ) {
      default:
        rsns = new String[]
        {
           "Duplicate Request"
          ,"Request Cancelled"
        };
        break;
    }

    return rsns;
  }

  protected class EntryValidation implements Validation
  {
    protected String ErrorText = "";

    public EntryValidation()
    {
    }

    public String getErrorText()
    {
      return(ErrorText);
    }

    public boolean validate()
    {
      return( validate(null) );
    }

    public boolean validate(String fieldData)
    {
      Field close,reopen;
      boolean rval=true;
      StringBuffer et = new StringBuffer();


      // at least one re-open or close must be specifed
      close=fields.getField("close");
      reopen=fields.getField("reopen");

      if((close!=null && close.isBlank()) && (reopen!=null && reopen.isBlank())) {
        et.append("At least one of Close or Re-Open checkboxes must be selected to proceed.");
        rval=false;
      }

      Field month;

      // require corresponding month
      if(close!=null && close.getData().equals("y")) {
        month=fields.getField("close_month");
        if(month==null || month.isBlank()) {
          et.append("A Close month must be specified.\n");
          rval=false;
        }
      }
      if(reopen!=null && reopen.getData().equals("y")) {
        month=fields.getField("reopen_month");
        if(month==null || month.isBlank()) {
          et.append("A Re-Open month must be specified.\n");
          rval=false;
        }
      }

      // require call tag address if at least one piece of equipment specified
      if(rval) {

        log.debug("EntryValidation() Require address if at least one piece of equip specified...");

        boolean bAtLeastOneEquipItemExists = false;
        StringBuffer sb2 = new StringBuffer();
        int i=0;
        String key;
        Field fld;

        while(++i < 100) {

          sb2.delete(0,sb2.length());  // reset
          sb2.append("sct_"+i);
          key = sb2.toString();
          fld = fields.getField(key);

          log.debug("EntryValidation(): sct_"+i+" = "+(fld==null? "{null}":fld.getData())+".");

          if(fld == null)
            break;
          else if(fld.getData().equals("Yes")) {
            bAtLeastOneEquipItemExists = true;
            break;
          }

        }

        if(bAtLeastOneEquipItemExists == true && !adrsHelper.isValidAddress(et))
          rval=false;
      }

      if(et.length()>0)
        ErrorText=StringUtilities.replace(et.toString(),"\n","<br>");
      else
        ErrorText="";

      return rval;
    }

  } // EntryValidation

  public String getHeadingName()
  {
    return "Seasonal Merchant Activate/Deactivate Account";
  }

  protected final class MonthDropDownTable extends DropDownTable
  {
    public MonthDropDownTable()
    {
      super();

      addElement("","");
      addElement("Jan", "Jan");
      addElement("Feb", "Feb");
      addElement("Mar", "Mar");
      addElement("Apr", "Apr");
      addElement("May", "May");
      addElement("Jun", "Jun");
      addElement("Jul", "Jul");
      addElement("Aug", "Aug");
      addElement("Sep", "Sep");
      addElement("Oct", "Oct");
      addElement("Nov", "Nov");
      addElement("Dec", "Dec");
    }

  }

} // class ACRDataBean_AcntCloseReopen
