package com.mes.acr;

import com.mes.forms.HtmlContainer;
import com.mes.forms.HtmlRenderable;
import com.mes.forms.HtmlTable;
import com.mes.forms.HtmlTableCell;
import com.mes.forms.HtmlTableRow;
import com.mes.forms.SelectorFrame;

public abstract class SF_EquipBase extends SelectorFrame
{
  // this is a hack to make title bar rendering dynamic
  public class TitleRenderer implements HtmlRenderable
  {
    public String renderHtml()
    {
      return title;
    }
  }
  
  private String title;
  
  public SF_EquipBase(String title)
  {
    this.title = title;
  }
  
  // table to add selectors to, nested in the outer frame container
  protected HtmlTable innerTable;
  
  public HtmlContainer generateFrameContainer()
  {
    // setup outer table (for border/background color effects)
    HtmlTable outerTable = new HtmlTable();
    outerTable.setProperty("width","100%");
    outerTable.setProperty("cellpadding","1");
    outerTable.setProperty("cellspacing","0");
    outerTable.setProperty("class","tableFillGray");
    HtmlTableCell outerCell
      = outerTable.add(new HtmlTableRow()).add(new HtmlTableCell());

    // setup inner table (where selector items will actually be added)
    innerTable = (HtmlTable)outerCell.add(new HtmlTable());
    innerTable.setProperty("width","100%");
    innerTable.setProperty("cellpadding","2");
    innerTable.setProperty("cellspacing","0");
    
    // setup a title bar
    HtmlTableCell titleCell
      = innerTable.add(new HtmlTableRow()).add(new HtmlTableCell());
    titleCell.setProperty("class","tableColumnHead");
    titleCell.setProperty("style","font-weight: bold;");
    titleCell.add(new TitleRenderer());

    return outerTable;
  }
  
  protected HtmlContainer addRenderer()
  {
    HtmlTableCell renderer
      = innerTable.add(new HtmlTableRow()).add(new HtmlTableCell());
    renderer.setProperty("class","tableData");
    return renderer;    
  }

  public void addSelectorRenderer(HtmlRenderable selRenderer)
  {
    HtmlContainer hc = addRenderer();
    hc.add(selRenderer);
  }

  public void addHeaderRenderer(HtmlRenderable hdrRenderer)
  {
    HtmlContainer hc = addRenderer();
    hc.setProperty("style","font-weight: bold");
    hc.add(hdrRenderer);
  }
}  
