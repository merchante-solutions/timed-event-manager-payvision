package com.mes.acr;

import com.mes.forms.SelectorColumnDef;

public class SF_EquipDisp extends SF_EquipBase
{
  public SF_EquipDisp(String title)
  {
    super(title);
  }

  public void setupFrame()
  {
    addColumn(new SelectorColumnDef("col1","Serial No.",  105));
    addColumn(new SelectorColumnDef("col2","Description", 170));
    addColumn(new SelectorColumnDef("col3","Status",      105));
    addColumn(new SelectorColumnDef("col4","Change to:",  165));
    addColumn(new SelectorColumnDef("col5","Charge Amount",150));
  }
}
