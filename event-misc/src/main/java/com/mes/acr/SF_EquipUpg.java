package com.mes.acr;

import com.mes.forms.SelectorColumnDef;

public class SF_EquipUpg extends SF_EquipBase
{
  public SF_EquipUpg(String title)
  {
    super(title);
  }

  public void setupFrame()
  {
    addColumn(new SelectorColumnDef("col1","Serial No.",                100));
    addColumn(new SelectorColumnDef("col2","Description",               100));
    addColumn(new SelectorColumnDef("col3","Equipment Type",            200));
    addColumn(new SelectorColumnDef("col4","V Num / TID",                80));
    addColumn(new SelectorColumnDef("col5","Terminal<br>App",            80));
    addColumn(new SelectorColumnDef("col6","Vendor ID",                  80));
    addColumn(new SelectorColumnDef("col7","Call<br>Tag",                65));
    //addColumn(new SelectorColumnDef("col8","Send<br>Repair<br>Form",     55));
  }
}
