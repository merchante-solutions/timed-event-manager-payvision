/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/menus/MenuItem.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-05-31 14:24:22 -0700 (Thu, 31 May 2012) $
  Version            : $Revision: 20222 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.menus;

import java.io.InputStream;
import java.sql.ResultSet;
import java.util.StringTokenizer;
import java.util.Vector;
import com.mes.support.TestServer;
import com.mes.support.WebLogicPropertiesFile;
import com.mes.user.Right;
import com.mes.user.UserBean;

public class MenuItem
{
  protected String        BaseUrl                 = "";
  protected String        Description             = "";
  protected String        DisplayName             = "";
  protected boolean       ProductionEnabled       = false;
  protected Vector        RequiredRights          = null;
  protected boolean       SecureServerRequired    = false;
  protected String        Target                  = null;
  protected boolean       MerchantsOnly           = false;
  
  public MenuItem( String displayName, String desc, String baseUrl, 
                   String requiredRightsList, String target, String prodEnable )
  {
    int               numTokens = 0;
    StringTokenizer   tokenizer = new StringTokenizer( requiredRightsList, "," );

    
    RequiredRights = new Vector();
    while( tokenizer.hasMoreTokens() )
    {
      try
      {
        RequiredRights.addElement( new Right(Long.parseLong(tokenizer.nextToken()) ) );
      }
      catch( NumberFormatException e )
      {
        // ignore invalid rights
      }
    }      
    
    BaseUrl         = TestServer.transformDotNetURL(baseUrl);
    Description     = desc;
    DisplayName     = displayName;
    Target          = target;
    
    try
    {
      // set the production enable flag
      ProductionEnabled = ( prodEnable.charAt(0) == 'Y' || prodEnable.charAt(0) == 'y' );
    }
    catch( Exception e )
    {
      ProductionEnabled = false;
    }      
  }

  public MenuItem( ResultSet resultSet )
    throws java.sql.SQLException
  {
    int               numTokens = 0;
    String            temp      = null;
    StringTokenizer   tokenizer = new StringTokenizer( resultSet.getString("required_right"), "," );
    
    RequiredRights = new Vector();
    while( tokenizer.hasMoreTokens() )
    {
      try
      {
        RequiredRights.addElement( new Right(Long.parseLong(tokenizer.nextToken()) ) );
      }
      catch( NumberFormatException e )
      {
        // ignore invalid rights
      }
    }      
    
    BaseUrl         = TestServer.transformDotNetURL(resultSet.getString("base_url"));
    Description     = resultSet.getString("description");
    DisplayName     = resultSet.getString("display_name");
    Target          = resultSet.getString("target");
    MerchantsOnly   = resultSet.getString("merchants_only").equals("Y");
    
    try
    {
      temp = resultSet.getString("production_enable");
      // set the production enable flag
      ProductionEnabled = ( temp.charAt(0) == 'Y' || temp.charAt(0) == 'y' );
    }
    catch( Exception e )
    {
      ProductionEnabled = false;
    }      
    
    try
    {
      temp = resultSet.getString("secure_server_required");
      // set the production enable flag
      SecureServerRequired = ( temp.charAt(0) == 'Y' || temp.charAt(0) == 'y' );
    }
    catch( Exception e )
    {
      SecureServerRequired = false;
    }      
  }
  
  public String getBaseUrl( )
  {
    return( BaseUrl );
  }
  
  public String getDescription( )
  {
    return( Description );
  }
    
  public String getDisplayName( )
  {
    return( DisplayName );
  }
  
  // overload this method to add any dynamic 
  // params to the request.
  public String getEncodedUrl( )
  {
    return( getBaseUrl() );
  }
  
  public Vector getRequiredRights()
  {
    return( RequiredRights );
  }
  
  public String getTarget()
  {
    return( Target );
  }
  
  public boolean isItemDisplayed( UserBean user )
  {
    WebLogicPropertiesFile  propFile  = null;
    boolean                 retVal    = false;
    
    try
    {
      propFile = new WebLogicPropertiesFile( "mes.properties" );
    }
    catch( Exception e )
    {
      InputStream inputStream = Thread.currentThread().getContextClassLoader()
        .getResourceAsStream("resources/mes.properties");      
        
      propFile = new WebLogicPropertiesFile( inputStream );
      
      try
      {
        inputStream.close();
      }
      catch(Exception re) {}
    }
  
    for ( int i = 0; i < RequiredRights.size(); ++i )
    {
      if ( ( retVal = user.hasRight( (Right)RequiredRights.elementAt(i) ) ) == true )
      {
        break;
      }
    }
    
    if( retVal == true && MerchantsOnly == true )
    {
      // only show this menu item if the user is a merchant user
      retVal = user.isMerchantUser();
    }
    
    // user has the necessary rights to view the
    // menu item, now see if the menu is disabled
    // by the system.
    if ( retVal == true ) 
    {
      if (!propFile.getBoolean("com.mes.development",false))
      {
        // this is a production server
        retVal = ProductionEnabled;
      }
    }
    
    // user has rights and the production flag
    // has been tested, check to see if the item
    // is only displayed on secure servers.
    if ( retVal == true )
    {
      if ( propFile.getBoolean("com.mes.secure",false) == false )
      {
        // this is not a secure server so if 
        // secure required then hide the menu
        retVal = !SecureServerRequired;
      }
    }
    
    return( retVal );
  }
  
  public void setBaseUrl( String baseUrl )
  {
    BaseUrl = baseUrl;
  }
  
  public void setDescription( String desc )
  {
    Description = desc;
  }
  
  public void setDisplayName( String displayName )
  {
    DisplayName = displayName;
  }
}