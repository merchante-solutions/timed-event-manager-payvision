/*@lineinfo:filename=MenuDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/menus/MenuDataBean.sqlj $

  Description:  
    Base class for report data beans
    
    This class should maintain any data that is common to all of the 
    possible report data beans.  The inheritors of this class should 
    maintain any page-specific report data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-31 11:13:04 -0800 (Mon, 31 Jan 2011) $
  Version            : $Revision: 18354 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.menus;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;

public class MenuDataBean extends SQLJConnectionBase
{
  public static final int     SB_MENU_ORDER   = 0;
  public static final int     SB_ALPHA        = 1;
  
  protected static final String SKM_CHILD_STYLE_DEFAULT   = "style=\"display:none;\"";

  private   String      menuTitle     = "";
  
  public MenuDataBean()
  {
  }
  
  public MenuDataBean( String connectionString )
  {
    super( connectionString );
  }    
  
  public String encodeSkmMenu( String menuName, int parentId, int[] childIds )
  {
    return( encodeSkmMenu(menuName, parentId, childIds, null, null) );
  }
  
  public String encodeSkmMenu( String menuName, int parentId, int[] childIds, String classId )
  {
    return( encodeSkmMenu(menuName, parentId, childIds, classId, null) );
  }
    
  public String encodeSkmMenu( String menuName, int parentId, int[] childIds, String classId, String queryString )
  {
    StringBuffer  buffer        = new StringBuffer();
    String        crlf          = "\r\n";
    String        menuId        = null;
    MenuItem      menuItem      = null;
    Vector        menuItems     = null;
    Vector        parentItems   = null;
    StringBuffer  parentTable   = new StringBuffer();
    String        tableClass    = ((classId == null) ? "" : (" class=\"" + classId + "\" "));
    
    parentItems = loadMenu( parentId );
    
    parentTable.append("<table id=\"" + menuName + "\" cellpadding=\"0\" cellspacing=\"3\" width=\"100%\" border=\"0\"" + tableClass + ">" + crlf);
    parentTable.append("  <tr>" + crlf);
    for( int parentIdx = 0; parentIdx < parentItems.size(); ++parentIdx )
    {
      menuItem = (MenuItem)parentItems.elementAt(parentIdx);
      
      parentTable.append("    <td class=\"tpgMenuItem\" id=\"");
      parentTable.append(menuName);
      parentTable.append("-menuItem");
      parentTable.append(NumberFormatter.getPaddedInt(parentIdx,3));
      parentTable.append("\"" + crlf);
      
      // setup the on click event to go to the main menu URL
      parentTable.append("        ");
      parentTable.append("onclick=\"javascript:skm_closeSubMenus(document.getElementById('");
      parentTable.append( menuName );
      parentTable.append("'));location.href='");

      StringBuffer  buff   = new StringBuffer(menuItem.getBaseUrl());
      if( queryString != null && menuItem.getBaseUrl().indexOf("#") == -1 )
      {
        if(buff.toString().indexOf("?") == -1)
        {
          buff.append("?");
        }
        else
        {
          buff.append("&");
        }
        buff.append(queryString);
      }
      
      parentTable.append( buff.toString() );
      parentTable.append("';\"");
      parentTable.append(crlf);
      parentTable.append("        ");
      parentTable.append("onmouseover=\"javascript:skm_mousedOverMenu('");
      parentTable.append( menuName );
      parentTable.append("',this,document.getElementById('");
      parentTable.append( menuName );
      parentTable.append("'), false, '');\"");
      parentTable.append(crlf);
      
      parentTable.append("        ");
      parentTable.append("onmouseout=\"javascript:skm_mousedOutMenu('");
      parentTable.append( menuName );
      parentTable.append("',this,'');this.className='tpgMenuItem';\"");
      parentTable.append(">" + crlf);
      
      parentTable.append("      ");
      parentTable.append( menuItem.getDisplayName() +crlf );
      parentTable.append("    </td>" + crlf);
      
      if ( parentIdx < childIds.length && childIds[parentIdx] != 0 )
      {
        // load the menu data from Oracle
        menuItems = loadMenu( childIds[parentIdx] );
        
        // skip empty menus
        if ( menuItems.size() == 0 )
        {
          continue;
        }
      
        // reset the buffer and encode the results as an HTML table
        // with the necessary skmMenu ids.
        menuId = menuName + "-menuItem" + NumberFormatter.getPaddedInt(parentIdx,3) + "-subMenu"; 
        buffer.append("<table id=\"" + menuId + "\" cellpadding=\"0\" cellspacing=\"3\" border=\"0\" style=\"display:none;\"" + tableClass + ">" + crlf);
        for( int subMenuId = 0; subMenuId < menuItems.size(); ++subMenuId )
        {
          menuItem = (MenuItem)menuItems.elementAt(subMenuId);
          
          buffer.append("  <tr>" + crlf);
          buffer.append("    <td class=\"tpgMenuItem\" id=\"");
          buffer.append(menuId);
          buffer.append("-menuItem");
          buffer.append(NumberFormatter.getPaddedInt(subMenuId,3));
          buffer.append("\"" + crlf);
          
          // setup the on click event to go to the main menu URL
          buffer.append("        ");
          buffer.append("onclick=\"javascript:skm_closeSubMenus(document.getElementById('");
          buffer.append( menuName );
          buffer.append("'));location.href='");
          
          StringBuffer  buf   = new StringBuffer(menuItem.getBaseUrl()); 
          if( queryString != null && menuItem.getBaseUrl().indexOf("#") == -1 )
          {
            if(buf.toString().indexOf("?") == -1)
            {
              buf.append("?");
            }
            else
            {
              buf.append("&");
            }
            buf.append(queryString);
          }

          buffer.append( buf.toString() );
          buffer.append("';\"");
          buffer.append(crlf);
          
          // mouse over 
          buffer.append("        ");
          buffer.append("onmouseover=\"javascript:skm_mousedOverMenu('");
          buffer.append( menuName );
          buffer.append("',this,document.getElementById('");
          buffer.append( menuId );
          buffer.append("'), true, '');\"");
          buffer.append(crlf);
          
          // mouse out
          buffer.append("        ");
          buffer.append("onmouseout=\"javascript:skm_mousedOutMenu('");
          buffer.append( menuName );
          buffer.append("',this,'');this.className='tpgMenuItem';\"");
          buffer.append(">" + crlf);
          
          buffer.append("      ");
          buffer.append( menuItem.getDisplayName() + crlf );
          buffer.append("    </td>" + crlf);
          buffer.append("  </tr>" + crlf);
        }
        buffer.append("</table>" + crlf);
        buffer.append(crlf);
      }
    }
    parentTable.append("  </tr>" + crlf);
    parentTable.append("</table>" + crlf);
    
    if ( buffer.length() > 0 )
    {
      buffer.append(crlf);
    }
    buffer.append(parentTable);

    return( buffer.toString() );    
  }
  
  public Vector loadMenu( int menuId, int sortBy )
  {
    ResultSetIterator     it        = null;
    Vector                menuItems = new Vector();
    ResultSet             resultSet = null;
    
    try
    {
      connect();
      
      if(sortBy == SB_ALPHA)
      {
        /*@lineinfo:generated-code*//*@lineinfo:230^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  display_name,
//                    description,
//                    base_url,
//                    required_right,
//                    target,
//                    production_enable,
//                    secure_server_required,
//                    nvl(merchants_only, 'N') merchants_only
//            from    menu_items
//            where   menu_id = :menuId
//            order by display_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  display_name,\n                  description,\n                  base_url,\n                  required_right,\n                  target,\n                  production_enable,\n                  secure_server_required,\n                  nvl(merchants_only, 'N') merchants_only\n          from    menu_items\n          where   menu_id =  :1 \n          order by display_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.menus.MenuDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,menuId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.menus.MenuDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:243^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:247^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  display_name,
//                    description,
//                    base_url,
//                    required_right,
//                    target,
//                    production_enable,
//                    secure_server_required,
//                    nvl(merchants_only, 'N') merchants_only
//            from    menu_items
//            where   menu_id = :menuId
//            order by item_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  display_name,\n                  description,\n                  base_url,\n                  required_right,\n                  target,\n                  production_enable,\n                  secure_server_required,\n                  nvl(merchants_only, 'N') merchants_only\n          from    menu_items\n          where   menu_id =  :1 \n          order by item_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.menus.MenuDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,menuId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.menus.MenuDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:260^9*/
      }
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        menuItems.addElement( new MenuItem( resultSet ) );
      }
      resultSet.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:271^7*/

//  ************************************************************
//  #sql [Ctx] { select  menu_title 
//          from    menu_types
//          where   menu_id = :menuId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  menu_title  \n        from    menu_types\n        where   menu_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.menus.MenuDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,menuId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   menuTitle = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:276^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadMenu(" + menuId + ")", e.toString() );
    }
    finally
    {
      try{ resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch( Exception e ) {}
      cleanUp();
    }
    return( menuItems );
  }  
  
  public Vector loadMenu(int menuId)
  {
    return loadMenu(menuId, SB_MENU_ORDER);
  }
  
  public String getMenuTitle()
  {
    return this.menuTitle;
  }
  
  public static void main(String[] args)
  {
    MenuDataBean    test  = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      test = new MenuDataBean();
      
      test.connect();
      String buf = test.encodeSkmMenu("myMenu",200, new int[]{ 0,202 } );
      System.out.println("result:");
      System.out.println(buf);
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception ee ) {}
    }
  }
}/*@lineinfo:generated-code*/