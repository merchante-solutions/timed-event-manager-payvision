package com.mes.forms;

/**
 * VitalField
 *
 * Imposes Vital's edits given the type of vital field specified.
 */
public class VitalField extends Field
{
  // constants
  public static final int     FIELD_TYPE_UNDEFINED            = 0;
  public static final int     FIELD_TYPE_DBANAME              = 1;
  public static final int     FIELD_TYPE_LEGALNAME            = 2;
  public static final int     FIELD_TYPE_OWNERNAME            = 3;
  
  
  // data members
  protected int               vitalFieldType                  = FIELD_TYPE_UNDEFINED;
  protected String            errorText                       = null;
  
  
  // class methods
  public static VitalField buildVitalDbaNameField(String fname, String label, boolean nullAllowed)
  {
    return new VitalField(FIELD_TYPE_DBANAME,fname,label,nullAllowed);
  }
  public static VitalField buildVitalLegalNameField(String fname, String label, boolean nullAllowed)
  {
    return new VitalField(FIELD_TYPE_LEGALNAME,fname,label,nullAllowed);
  }
  public static VitalField buildVitalOnwerNameField(String fname, String label, boolean nullAllowed)
  {
    return new VitalField(FIELD_TYPE_OWNERNAME,fname,label,nullAllowed);
  }
  
  protected static final int getVitalFieldLength(int vitalFieldType)
  {
    switch(vitalFieldType) {
      case FIELD_TYPE_DBANAME:
      case FIELD_TYPE_OWNERNAME:
        return 25;
      case FIELD_TYPE_LEGALNAME:
        return 32;
      default:
        return 30;
    }
  }
  
  
  // object methods
  
  // construction
  protected VitalField(int vitalFieldType, String fname, String label, boolean nullAllowed)
  {
    super(fname, label, 1, 1, nullAllowed);
    
    setVitalFieldType(vitalFieldType);

    addValidation(new VitalFieldValidation());
  }
  
  public int  getVitalFieldType() 
  { 
    return vitalFieldType; 
  }
  
  public void setVitalFieldType(int v)
  {
    removeValidation("maxlength");
    
    this.length         = getVitalFieldLength(v);
    this.htmlSize       = length + 1;
    this.vitalFieldType = v;
    
    addValidation(new MaxLengthValidation(length),"maxlength");
  }
  
  public class VitalFieldValidation implements Validation
  {
    public boolean validate(String fieldData)
    {
      boolean rval = true;
      
      try {
      
        fieldData = fieldData.toUpperCase();
        
        switch(vitalFieldType) {
        
          // dba/legal name
          case FIELD_TYPE_DBANAME:
          case FIELD_TYPE_LEGALNAME:
            final String validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,-'&*# ";
            StringBuffer invalidCharsFound = new StringBuffer();
            for(int i = 0; i<fieldData.length(); i++) {
              char c = Character.toUpperCase(fieldData.charAt(i));
              if(validChars.indexOf(c) < 0) {
                if(invalidCharsFound.toString().indexOf(c) < 0)
                  invalidCharsFound.append(",'" + c + "'");
                rval = false;
              }
            }
            if(invalidCharsFound.length() > 0) {
              errorText = "Invalid character(s) "+invalidCharsFound.substring(1)+" found in "+getLabel();
            }
            break;
        
          // owner name
          case FIELD_TYPE_OWNERNAME:
            final String invalidChars = "&,#0123456789";
            for(int i = 0; i<fieldData.length(); i++) {
              char c = fieldData.charAt(i);
              if(invalidChars.indexOf(c) >= 0) {
                rval = false;
                errorText = "Invalid character '"+c+"' found in "+getLabel();
              }
            }
            break;
        }
      
      }    
      catch(Exception e) {
        System.out.println("VitalField.validate() EXCEPTION: "+e.toString());
      }
      
      return rval;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
  }
  
}
