/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/HtmlBlock.java $

  Description:

  HtmlBlock
  
  A generic HtmlContainer that is transparent.  It may contain any number
  of HtmlRenderable objects, but does not itself render any start or end
  tags.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class HtmlBlock extends HtmlContainer
{
  public String getName()
  {
    return "block";
  }
  
  protected String renderStartTag()
  {
    return "";
  }
  
  protected String renderEndTag()
  {
    return "";
  }
  
  public HtmlRenderable get(int idx)
  {
    return getContent(idx);
  }
}
