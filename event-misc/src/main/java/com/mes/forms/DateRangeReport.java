/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/DateRangeReport.java $

  Description:  
  
  Extends SortableReport to provide support to child beans for generating 
  reports between two dates.


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-31 11:13:04 -0800 (Mon, 31 Jan 2011) $
  Version            : $Revision: 18354 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.forms;

import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.DateTimeFormatter;
import com.mes.user.UserBean;

public class DateRangeReport extends SortableReport
{
  protected ComboDateField  fromField;
  protected ComboDateField  toField;
  private   Field           dateSetField;
  
  private boolean useDay;
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fromField = new ComboDateField("fromDate",useDay,false);
      toField = new ComboDateField("toDate",useDay,false);
      dateSetField = new HiddenField("dateSet");
    
      fields.add(fromField);
      fields.add(toField);
      fields.add(dateSetField);
    
      fields.add(new ButtonField("submit"));
    
      fields.setGroupHtmlExtra("class=\"smallText\"");
    }
    catch( Exception e )
    {
      System.out.println(this.getClass().getName() + "::createFields(): "
        + e.toString());
      logEntry("createFields()",e.toString());
    }
  }

  protected void postHandleRequest(HttpServletRequest request)
  {
    if (!fields.getData("dateSet").equals("y"))
    {
      fromField.addDays(-6);
      dateSetField.setData("y");
    }
  }
  
  protected DateRangeReport()
  {
    useDay = true;
  }
  
  public DateRangeReport(UserBean user, boolean useDay)
  {
    super(user);
    this.useDay = useDay;
  }
  public DateRangeReport(UserBean user)
  {
    this(user,true);
  }
  
  public static final int DT_FROM = 0;    // from date
  public static final int DT_TO   = 1;    // to date
  
  public String renderDateControlsRow(int dateType)
  {
    StringBuffer renderData = new StringBuffer();
    
    try
    {
      Field dateField = ( dateType == DT_FROM ? fromField : toField );
    
      renderData.append("          <tr>\n");
      renderData.append("            <td class=\"smallText\" align=\"right\">\n");
      renderData.append("              " + (dateType == DT_FROM ? "From" : "To") + ":&nbsp;\n");
      renderData.append("            </td>\n");
      renderData.append("            <td>\n");
      renderData.append("              <nobr>" + dateField.renderHtml() + "</nobr>");
      renderData.append("            </td>\n");
      renderData.append("            <td width=\"100%\">\n");
      renderData.append("              &nbsp;\n");
      renderData.append("            </td>\n");
      renderData.append("          </tr>\n");
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::renderDateControlsRow()";
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    
    return renderData.toString();
  }
  
  public String renderSubmitButtonRow()
  {
    StringBuffer renderData = new StringBuffer();
    
    try
    {
      renderData.append("          <tr>\n");
      renderData.append("            <td></td>\n");
      renderData.append("            <td>\n");
      renderData.append("              " + fields.renderHtml("submit") + "\n");
      renderData.append("            </td>\n");
      renderData.append("            <td></td>\n");
      renderData.append("          </tr>\n");
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::renderSubmitButtonRow()"; 
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    
    return renderData.toString();
  }
  
  public String renderOtherControlsRow()
  {
    return "";
  }
  
  public String renderDateRangeHtml()
  {
    StringBuffer renderData = new StringBuffer();
    
    try
    {
      renderData.append("        <table align=\"center\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\n");
      renderData.append("          <tr>\n");
      renderData.append("          </tr>\n");
      renderData.append(renderDateControlsRow(DT_FROM));
      renderData.append(renderDateControlsRow(DT_TO));
      renderData.append(renderOtherControlsRow());
      renderData.append(renderSubmitButtonRow());
      renderData.append("\n" + dateSetField.renderHtml() + "\n");
      renderData.append("        </table>\n");
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::renderDateRangeHtml()"; 
      String desc = e.toString();
      System.out.println(func + ": " + desc);
      logEntry(func,desc);
    }
    
    return renderData.toString();
  }
  
  public String renderHiddenDateFields()
  {
    StringBuffer renderData = new StringBuffer();
    
    renderData.append(fromField.renderAsHidden() + "\n");
    renderData.append(toField.renderAsHidden() + "\n");
    
    return renderData.toString();
  }
  
  public String getDatesAsParms()
  {
    StringBuffer parmBuf = new StringBuffer();
    
    parmBuf.append( "fromDate.day="   + fields.getData("fromDate.day"));
    parmBuf.append("&fromDate.month=" + fields.getData("fromDate.month"));
    parmBuf.append("&fromDate.year="  + fields.getData("fromDate.year"));
    parmBuf.append("&toDate.day="     + fields.getData("toDate.day"));
    parmBuf.append("&toDate.month="   + fields.getData("toDate.month"));
    parmBuf.append("&toDate.year="    + fields.getData("toDate.year"));
    if (dateSetField.getData().equals("y"))
    {
      parmBuf.append("&dateSet=y");
    }
    
    return parmBuf.toString();
  }
  
  public void addFromDays(int days)
  {
    fromField.addDays(days);
  }

  public void addToDays(int days)
  {
    toField.addDays(days);
  }
  
  public int getDayRange()
  {
    int dayRange = 0;
    
    Date fromDate = fromField.getUtilDate();
    Date toDate = toField.getUtilDate();
    
    Calendar cal1 = Calendar.getInstance();
    Calendar cal2 = Calendar.getInstance();
    
    if (fromDate.before(toDate))
    {
      cal1.setTime(fromDate);
      cal2.setTime(toDate);
    }
    else
    {
      cal1.setTime(toDate);
      cal2.setTime(fromDate);
    }
    
    while (cal1.before(cal2))
    {
      cal1.add(Calendar.DATE,1);
      ++dayRange;
    }
    
    return dayRange + 1;
  }

  public String formatDate(java.util.Date date)
  {
   return DateTimeFormatter.getFormattedDate(date,
    DateTimeFormatter.DEFAULT_DATE_FORMAT);
  }
  
  public String getFromDateString()
  {
    return formatDate(fromField.getUtilDate());
  }
  
  public String getToDateString()
  {
    return formatDate(toField.getUtilDate());
  }
}
