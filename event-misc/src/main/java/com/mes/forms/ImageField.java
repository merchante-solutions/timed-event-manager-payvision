/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/ImageField.java $

  Description:
  
  ImageField
  
  Extends Field to define an html form image submit button.  Image buttons
  submit their name as their value when pressed.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class ImageField extends Field
{
  private String imageUrl;
  
  public ImageField(String fname, String label, String imageUrl)
  {
    super(fname,label,0,0,true);
    this.imageUrl = imageUrl;
  }
  public ImageField(String fname, String imageUrl)
  {
    this(fname,fname,imageUrl);
  }
    
  protected void validateData()
  {
    hasError = false;
  }
  
  /*
  ** public String renderHtmlField()
  **
  ** Generates html form input field.
  **
  ** RETURNS: html form field as a String.
  */
  public String renderHtmlField()
  {
    StringBuffer html = new StringBuffer();
    html.append("<input type=\"image\" ");
    html.append("name=\"" + fname + "\" ");
    html.append("src=\"" + imageUrl + "\" ");
    html.append(getHtmlExtra());
    html.append(">");
    return html.toString();
  }
}
