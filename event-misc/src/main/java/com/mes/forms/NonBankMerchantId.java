/*************************************************************************

  Description: /com/mes/forms/NonBankMerchantId
  
  Base class for field that provides specific validations for third party 
  merchant numbers (Discover, Amex, etc.)
  
  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.forms;

public abstract class NonBankMerchantId extends Field
{
  public NonBankMerchantId(String fname, String label, int htmlSize,
    boolean nullAllowed, int MIDLength, Field acceptField)
  {
    super(fname, label, MIDLength, htmlSize, nullAllowed);
    
    // set up basic validation
    if(acceptField != null)
    {
      addValidation(new MIDFormatValidation(acceptField, MIDLength));
    }
  }
  
  protected abstract boolean isValidMID(String fdata);
  
  public class MIDFormatValidation implements Validation
  {
    private Field   acceptField;
    private int     MIDLength;
    private String  errorText;
    
    public MIDFormatValidation(Field acceptField, int MIDLength)
    {
      this.acceptField = acceptField;
      this.MIDLength = MIDLength;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean result = true;
      
      try
      {
        if(acceptField.getData().toLowerCase().equals("y"))
        {
          // validate number in field
          if(fdata == null || fdata.equals(""))
          {
            result = false;
            errorText = "Field cannot be blank if this card is accepted";
          }
          else if(fdata.length() != MIDLength)
          {
            result = false;
            errorText = "Field must be " + MIDLength + " digits in length";
          }
          else if(!isValidMID(fdata))
          {
            result = false;
            errorText = "Invalid merchant number format for this card type";
          }
        }
        else
        {
          if(fdata != null && !fdata.equals(""))
          {
            result = false;
            errorText = "Field must be empty if this card is not accepted";
          }
        }
      }
      catch(Exception e)
      {
        result = false;
        errorText = e.toString();
      }
      
      return result;
    }
  }
}
