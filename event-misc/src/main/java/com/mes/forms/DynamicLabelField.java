/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/DynamicLabelField.java $

  Description:
  
  Read-only field to serve the task of showing text 
  that is dependent on the state of other fields.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class DynamicLabelField extends Field
{
  public DynamicLabelField(String fname)
  {
    this(fname,"","");
  }
  public DynamicLabelField(String fname, String htmlExtra)
  {
    this(fname,htmlExtra,"");
  }
  public DynamicLabelField(String fname, String htmlExtra, String fdata)
  {
    super(fname,fname,0,0,true);
    this.readOnly = true;
    
    if(htmlExtra != null && htmlExtra.length() > 0) 
      setHtmlExtra(htmlExtra);    
      
    setData(fdata);
  }
  
  protected String renderHtmlField()
  {
    StringBuffer html = new StringBuffer();
    html.append("<span ");
    html.append(getHtmlExtra());
    html.append(">");
    html.append(getRenderData());
    html.append("</span>");
    return html.toString();
  }
  
}
