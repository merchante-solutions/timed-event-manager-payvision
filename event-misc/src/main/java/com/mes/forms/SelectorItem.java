package com.mes.forms;

import java.util.HashMap;
import java.util.Map;

public abstract class SelectorItem implements HtmlRenderable
{
  public static final int T_STANDARD  = 1;
  public static final int T_OTHER     = 2;

  protected String value;
  protected HtmlContainer htmlContainer = new HtmlBlock();
  protected Field controlField;
  protected int type = T_STANDARD;

  public SelectorItem()
  {
  }
  public SelectorItem(int type)
  {
    this.type = type;
  }
  public SelectorItem(String value)
  {
    this.value = value;
  }
  public SelectorItem(String value, int type)
  {
    this.value = value;
    this.type = type;
  }

  public void setValue(String value)
  {
    this.value = value;
  }
  public String getValue()
  {
    return value;
  }

  public int getType()
  {
    return type;
  }
  public void setType(int type)
  {
    this.type = type;
  }

  public void setHtmlContainer(HtmlContainer htmlContainer)
  {
    this.htmlContainer = htmlContainer;
  }
  public HtmlContainer getHtmlContainer()
  {
    return htmlContainer;
  }

  public String renderHtml()
  {
    return htmlContainer.renderHtml();
  }

  public void setControlField(Field controlField)
  {
    this.controlField = controlField;
  }

  public boolean isSelected()
  {
    return controlField.getData().equals(value);
  }

  protected Map columnRenderers = new HashMap();

  public void setColumnRenderer(String columnName, HtmlRenderable renderer)
  {
    columnRenderers.put(columnName,renderer);
  }

  public HtmlRenderable getColumnRenderer(SelectorColumnDef column)
  {
    String columnName = column.getName();

    HtmlRenderable renderer
      = (HtmlRenderable)columnRenderers.get(columnName);

    if (renderer == null)
    {
      throw new RuntimeException("No column renderer defined for "
        + columnName);
    }

    return renderer;
  }

  public abstract FieldGroup getFields();

  public abstract void init();
}