package com.mes.forms;

public class YesNoRadioField extends RadioField
{
  /**
  * A general purpose yes/no radio field.  Should probably be moved out to
  * the forms package since it is a pretty common field type.
  */

  public YesNoRadioField(String name, String label)
  {
    super(name,label,new String[][] { { "", "No" }, { "Yes", "Yes" } } );
  }

}