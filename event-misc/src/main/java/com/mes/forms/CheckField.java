package com.mes.forms;

public class CheckField extends Field
{
  private String checkedValue;
  private HtmlRenderable label;

  public CheckField(String fname, String fieldLabel, HtmlRenderable label,
    String checkedValue, boolean isChecked)
  {
    super(fname,fieldLabel,0,0,true);

    this.label = label;
    this.checkedValue = checkedValue;
    fdata = (isChecked ? checkedValue : "n");
  }
  public CheckField(String fname, String fieldLabel, String labelText,
    String checkedValue, boolean isChecked)
  {
    this(fname,fieldLabel,new HtmlText(labelText),checkedValue,isChecked);
  }
  public CheckField(String fname, HtmlRenderable label, String checkedValue,
    boolean isChecked)
  {
    this(fname,fname,label,checkedValue,isChecked);
  }
  public CheckField(String fname, String labelText, String checkedValue,
    boolean isChecked)
  {
    this(fname,labelText,new HtmlText(labelText),checkedValue,isChecked);
  }

  public boolean isBlank()
  {
    return super.isBlank() || fdata.toLowerCase().equals("n");
  }

  public boolean isChecked()
  {
    return fdata != null && !isBlank() && !fdata.toLowerCase().equals("n");
  }

  public String renderInputTag()
  {
    StringBuffer html = new StringBuffer();

    html.append("<input type=\"checkbox\"");
    html.append(" name=\"" + fname + "\" ");
    html.append(" value=\"" + checkedValue + "\"");
    if (tabIndex > 0)
    {
      html.append("tabindex=\"" + tabIndex + "\" ");
    }
    html.append(isChecked() ? " checked" : "");
    html.append(">");

    return html.toString();
  }

  public String renderLabel()
  {
    return label.renderHtml() + renderErrorIndicator();
  }

  public String renderHtml()
  {
    return renderInputTag() + renderLabel();
  }

  public HtmlRenderable getCheckLabel()
  {
    return label;
  }

  public void setCheckLabel(HtmlRenderable label)
  {
    this.label = label;
  }

  public void reset()
  {
    fdata = "n";
  }
}
