/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/FieldDropDownTable.java $

  Description:  
  
  FieldDropDownTable object.
  
  Extends DropDownTable to allow for late loading of table elements.  An
  abstract method called loadTable must be implemented by child classes.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.forms;

import com.mes.tools.DropDownTable;

public abstract class FieldDropDownTable extends DropDownTable
{
  boolean isLoaded      = false;
  boolean alwaysReload  = false;
  
  public FieldDropDownTable()
  {
  }
  
  public FieldDropDownTable(boolean alwaysReload)
  {
    this.alwaysReload = alwaysReload;
  }
  
  public String getCurrentValue()
  {
    if (!isLoaded || alwaysReload)
    {
      doLoad();
    }
    return super.getCurrentValue();
  }
  
  public String getCurrentDescription()
  {
    if (!isLoaded || alwaysReload)
    {
      doLoad();
    }
    return super.getCurrentDescription();
  }
  
  public void doLoad()
  {
    loadTable();
    isLoaded = true;
  }
  
  public abstract void loadTable();
}