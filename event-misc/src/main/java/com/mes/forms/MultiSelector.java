package com.mes.forms;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 * Defines a set of selections of which any number may be chosen (via check
 * box fields).
 */
public class MultiSelector extends Selector
{
  static Logger log = Logger.getLogger(MultiSelector.class);

  public MultiSelector(String name, List selectorItems,
    SelectorFrame selectorFrame)
  {
    super(name,selectorItems,selectorFrame, true);
  }

  public MultiSelector(String name, List selectorItems,
    SelectorFrame selectorFrame, boolean isRequired)
  {
    super(name,selectorItems,selectorFrame, isRequired);
  }


  public MultiSelector(String name, List selectorItems)
  {
    super(name,selectorItems);
  }

  protected Map checkFields;

  protected Map getCheckFields()
  {
    if (checkFields == null)
    {
      checkFields = new HashMap();
    }
    return checkFields;
  }

  /**
   * Creates a new check box field to associate with the given item, adds it
   * into internal field storage.  Creates a new row containing the selector
   * item and adds it into selTable (which is in the selectorBlock and will
   * be rendered by the selecotr)
   */
  protected Field processSelector(SelectorItem item)
  {
    // create a check field option for this selector (with empty label text)
    CheckField cf = new CheckField(name + "_" + (selectorItems.size() - 1),
                                   name,
                                   "",
                                   item.getValue(),
                                   false);

    // field bean storage
    fields.add(cf);

    // store the check field in the item -> check field map
    getCheckFields().put(item,cf);

    return cf;
  }

  /**
   * Returns a renderable container to wrap a control tag or control header in.
   */
  protected HtmlContainer getControlContainer(boolean isHeader)
  {
    HtmlDiv div = new HtmlDiv();
    div.setProperty("style","float: left; width: 24px; padding-top: 2px; padding-left: 1px");
    return div;
  }

  /**
   * Returns the check field associated with the given selector item.
   */
  protected HtmlRenderable getControlTag(SelectorItem item)
  {
    HtmlContainer c = getControlContainer(false);
    c.add((HtmlRenderable)checkFields.get(item));
    return c;
  }
}
