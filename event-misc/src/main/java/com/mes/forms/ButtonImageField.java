/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/ButtonField.java $

  Description:
  
  ButtonField
  
  Extends field to define an html form submit button.  Buttons submit there
  own name as their value when pressed.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class ButtonImageField extends ButtonField
{
  protected String imgSrc = "";
  
  private Field xCoord;
  private Field yCoord;
  
  public ButtonImageField(String fname, String imgSrc, FieldGroup fields)
  {
    super(fname);
    setCoords(imgSrc, fields);
  }
  
  public ButtonImageField(String fname, String label, String imgSrc, FieldGroup fields)
  {
    super(fname, label);
    setCoords(imgSrc, fields);
  }
  
  public ButtonImageField(String fname, String label, String buttonText, String imgSrc, FieldGroup fields)
  {
    super(fname, label, buttonText);
    setCoords(imgSrc, fields);
  }
  
  public ButtonImageField(String fname, String label, String buttonText, String buttonName, String imgSrc, FieldGroup fields)
  {
    super(fname, label, buttonText, buttonName);
    setCoords(imgSrc, fields);
  }
  
  private void setCoords(String imgSrc, FieldGroup fields)
  {
    this.imgSrc = imgSrc;
    
    xCoord = new Field(fname+".x", 10, 10, true);
    yCoord = new Field(fname+".y", 10, 10, true);
    
    fields.add(xCoord);
    fields.add(yCoord);
  }
  
  public String getData()
  {
    StringBuffer result = new StringBuffer("");
    
    result.append(super.getData());
    
    if(!xCoord.getData().equals(""))
    {
      result.append("x=" + xCoord.getData());
    }
    
    if(!yCoord.getData().equals(""))
    {
      result.append("y=" + yCoord.getData());
    }
    
    return result.toString();
  }
  
  /*
  ** public String renderHtmlField()
  **
  ** Generates html form input field.
  **
  ** RETURNS: html form field as a String.
  */
  public String renderHtmlField()
  {
    StringBuffer html = new StringBuffer();
    html.append("<input type=\"image\" ");
    html.append("name=\"" + bname + "\" ");
    html.append("src=\"" + imgSrc + "\" ");
    html.append("alt=\"" + buttonText + "\" ");
    html.append("value=\"" + buttonText + "\" ");
    html.append("border=\"0\" ");
    html.append("align=\"absmiddle\" ");
    html.append(getHtmlExtra());
    html.append(">");
    return html.toString();
  }
  
}