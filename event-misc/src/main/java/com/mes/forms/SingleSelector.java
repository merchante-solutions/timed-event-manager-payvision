package com.mes.forms;

import java.util.List;
import org.apache.log4j.Logger;

/**
 * Defines a radio field based selector component.  Selection is handled by
 * a RadioField so that one and only one selection may be chosen.
 */
public class SingleSelector extends Selector
{
  static Logger log = Logger.getLogger(SingleSelector.class);

  public SingleSelector(String name, List selectorItems,
    SelectorFrame selectorFrame)
  {
    super(name,selectorItems,selectorFrame, true);
  }

  public SingleSelector(String name, List selectorItems)
  {
    super(name,selectorItems);
  }

  private RadioField radioField;

  /**
   * Returns the radio field associated with this single selector.  If the
   * radio field doesn't already exist it is created and added into field
   * storage.
   *
   * was: "the field itself is added into the selectorBlock for rendering."
   */
  private RadioField getRadioField()
  {
    if (radioField == null)
    {
      radioField = new RadioField(name,name);
      fields.add(radioField);
      //radioField.setRenderStyle(RadioField.RS_VERTICAL);
      //selectorBlock.add(radioField);
    }
    return radioField;
  }
  

  /**
   * Adds a button for the given selector item to the radio field.  Returns
   * the radio field.
   */
  protected Field processSelector(SelectorItem item)
  {
    // add the item as an option to the radio field
    RadioField radioField = getRadioField();
    radioField.addButton(item.getValue(),item);
    
    // the one radio field is every selector item's control field
    return radioField;
  }
  
  /**
   * Returns a renderable container to wrap a control tag or control header in.
   */
  protected HtmlContainer getControlContainer(boolean isHeader)
  {
    HtmlDiv div = new HtmlDiv();
    div.setProperty("style","float: left; width: 25px; padding-top: 3px");
    return div;
  }

  /**
   * Returns the radio button input tag associated with the given selector
   * item in a div tag with float, left styling.
   */
  protected HtmlRenderable getControlTag(SelectorItem item)
  {
    HtmlContainer c = getControlContainer(false);
    c.add(radioField.getInputTag(item.getValue()));
    return c;
  }
}
