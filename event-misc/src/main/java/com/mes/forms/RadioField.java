/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/RadioButtonField.java $

  Description:

  RadioField

  Extends field to define an html form radio button set.  This field
  is interchangeable with the older RadioButtonField class...

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-02-22 10:31:52 -0800 (Thu, 22 Feb 2007) $
  Version            : $Revision: 13480 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.Iterator;
import java.util.Vector;

public class RadioField extends Field
{
  /**
   * Storage for the information comprising an individual radio button
   * field.  Holds the value associated with a radio element and a label,
   * which is an HtmlRenderable object to be displayed next to the radio
   * button itself.
   */
  public static class RadioFieldElement
  {
    private String value;
    private HtmlRenderable label;

    public RadioFieldElement(String value, HtmlRenderable label)
    {
      this.value = value;
      this.label = label;
    }

    public String getValue()          { return value; }
    public HtmlRenderable getLabel()  { return label; }

    public void setValue(String value)          { this.value = value; }
    public void setLabel(HtmlRenderable label)  { this.label = label; }
    public void setLabel(String labelText)
    {
      this.label = new HtmlText(labelText);
    }
  }

  private Vector buttonData = new Vector();
  private int renderCols = -1;

  public static final int RS_HORIZONTAL = 0;
  public static final int RS_VERTICAL   = 1;

  private int renderStyle = RS_HORIZONTAL;

  /**
   * Constructors currently take a String array containing radio values
   * and label strings.  These are converted into RadioFieldElements in
   * the buttonData vector.
   */
  public RadioField(String name, String label, String[][] buttonStrs,
    String value)
  {
    super(name,label,0,0,true);

    if (buttonStrs != null)
    {
      for (int i = 0; i < buttonStrs.length; ++i)
      {
        addButton(buttonStrs[i][0],buttonStrs[i][1]);
      }

      // set default, use provided value or first option
      setData((value != null ? value : getValue(0)));
    }
  }
  public RadioField(String name, String label, String[][] buttonStrs)
  {
    this(name,label,buttonStrs,null);
  }
  public RadioField(String name,String label)
  {
    this(name,label,null,null);
  }

  public void addButton(String value, String labelText)
  {
    addButton(value,new HtmlText(labelText));
  }
  public void addButton(String value, HtmlRenderable label)
  {
    buttonData.add(new RadioFieldElement(value,label));

    // set the button as default if this is first button to be added
    if (buttonData.size() == 1)
    {
      setData(value);
    }
  }

  public void setRenderStyle(int renderStyle)
  {
    switch (renderStyle)
    {
      case RS_HORIZONTAL:
      case RS_VERTICAL:
        this.renderStyle = renderStyle;
        break;

      default:
        throw new RuntimeException("Invalid RadioField render style");
    }
  }

  /************************************************************************
   *
   * Radio field data accessors
   *
   ************************************************************************/

  /**
   * Processes a vector to make sure it contains RadioElementField refs.
   * Throws an exception if incorrect data is in the vector.
   */
  protected void setButtonData(Vector buttonData)
  {
    // make sure button data contains only RadioFieldElement's
    for (Iterator i = buttonData.iterator(); i.hasNext();)
    {
      Object item = i.next();
      if (!(item instanceof RadioFieldElement))
      {
        throw new RuntimeException("Invalid button data: " + item.toString());
      }
    }
    this.buttonData = buttonData;
  }

  /**
   * Returns the number of radio buttons defined.
   */
  public int getSize()
  {
    return buttonData.size();
  }

  /**
   * Returns the index of a given value, or -1 if no match found.
   */
  public int getIndexFromValue(String value)
  {
    for (int i = 0; i < buttonData.size(); ++i)
    {

      RadioFieldElement rfe = (RadioFieldElement)buttonData.elementAt(i);
      if (rfe.getValue().equals(value))
      {
        return i;
      }
    }
    return -1;
  }

  /**
   * Returns the radio field element at the given index.  If index is invalid
   * null is returned.
   */
  protected RadioFieldElement getRadioElement(int idx)
  {
    try
    {
      return (RadioFieldElement)buttonData.elementAt(idx);
    }
    catch (Exception e) {}

    return null;
  }

  /**
   * Searches for a radio field element by a given value.  Returns the
   * element if found, null if not.
   */
  protected RadioFieldElement getRadioElement(String value)
  {
    return getRadioElement(getIndexFromValue(value));
  }

  /**
   * Returns value of the element at given index
   *
   * Throws NullPointerException if index invalid
   */
  public String getValue(int idx)
  {
    return getRadioElement(idx).getValue();
  }

  /**
   * Returns label of the element at given index
   *
   * Throws NullPointerException if index invalid
   */
  public String getLabel(int idx)
  {
    return getRadioElement(idx).getLabel().renderHtml();
  }

  /**
   * Returns value of the element with the given value
   *
   * Throws NullPointerException if value invalid
   */
  public String getLabel(String value)
  {
    return getRadioElement(value).getLabel().renderHtml();
  }

  public void setLabel(int idx, HtmlRenderable label)
  {
    getRadioElement(idx).setLabel(label);
  }
  public void setLabel(int idx, String labelText)
  {
    getRadioElement(idx).setLabel(labelText);
  }
  public void setLabel(String value, HtmlRenderable label)
  {
    setLabel(getIndexFromValue(value),label);
  }
  public void setLabel(String value, String labelText)
  {
    setLabel(getIndexFromValue(value),labelText);
  }

  /**
   * Returns the label of the current radio button selected.
   */
  public String getSelectedLabel()
  {
    return getLabel(getData());
  }

  /************************************************************************
   *
   * Html rendering
   *
   ************************************************************************/

  /**
   * Convenience methods for wrapping up the input tag in an html renderable
   * object.  Returns the rendered input tag wrapped in an HtmlText.
   */
  public HtmlRenderable getInputTag(final int idx)
  {
    return new HtmlRenderable() {
      public String renderHtml() { return renderInputTag(idx); }
    };
  }
  public HtmlRenderable getInputTag(String value)
  {
    return getInputTag(getIndexFromValue(value));
  }

  /**
   * Renders an actual radion button html input tag for the radio field
   * element of the given index.
   */
  public String renderInputTag(int idx)
  {
    StringBuffer html = new StringBuffer();

    String idxValue = getValue(idx);

    html.append("<input type=\"radio\"");
    html.append(" name=\"" + fname + "\" ");
    html.append(" value=\"" + idxValue + "\" ");
    if (tabIndex > 0)
    {
      html.append("tabindex=\"" + tabIndex + "\" ");
    }
    html.append(fdata.equals(idxValue) ? " checked" : "");
    html.append(">");

    return html.toString();
  }

  /**
   * Generates an html snippet containing the label associated with a
   * radio button with the given index in a <span> tag with html
   * extra thrown in for style purposes (legacy).
   */
  public String renderLabel(int idx)
  {
    return "<span " + getHtmlExtra() + ">" + getLabel(idx) + "</span>";
  }

  /**
   * Generates html snippet containing the radio button and it's associated
   * label.
   */
  public String renderOption(int idx)
  {
    return renderInputTag(idx) + renderLabel(idx);
  }


  private int errorTagIdx = -1;

  private String renderHtmlHorizontally()
  {
    StringBuffer html = new StringBuffer();

    // setup some inline style defs
    // to get horizontal type layout style float: left is used
    String outerStyle = "float: left;";
    String radioStyle = "float: left;";
    String labelStyle = "float: left; line-height: 1.5em";

    // if renderCols has been set, derive some widths from the requested value
    if (renderCols > 0)
    {
      outerStyle += " width: " + (100 / renderCols) + "%;";
    }

    // render each radio button one after the other
    // as a series of <div> html blocks
    for (int idx = 0; idx < getSize(); ++idx)
    {
      html.append("<div style=\""+ outerStyle + "\">\n");
      html.append("  <div style=\"" + radioStyle + "\">" + renderInputTag(idx)
       + "</div>\n");
      html.append("  <div style=\"" + labelStyle + "\">" + renderLabel(idx));
      if (hasError && (errorTagIdx == idx || (errorTagIdx == -1 && idx == 0)))
      {
        html.append(renderErrorIndicator());
      }
      html.append("</div>\n");
      html.append("</div>\n");
    }

    return html.toString();
  }

  private String renderHtmlVertically()
  {
    StringBuffer html = new StringBuffer(64);

    html.append("\n");
    html.append("<table>\n");

    for (int idx = 0; idx < getSize(); ++idx)
    {
      html.append("  <tr>\n");
      html.append("    <td valign=\"top\">" + renderInputTag(idx) + "</td>\n");
      html.append("    <td valign=\"top\" width=\"100%\">" + renderLabel(idx));
      if (hasError && (errorTagIdx == idx || (errorTagIdx == -1 && idx == 0)))
      {
        html.append(renderErrorIndicator());
      }
      html.append("</td>\n");
      html.append("  </tr>\n");
    }

    html.append("</table>");

    return html.toString();
  }

  /**
   * Generic radio button field rendering.  Iterates through the radio button
   * field elements and renders the html for the input tag and corresponding
   * label data.  This render method attempts to do some generic layout
   * as a convenience.  Users needing more control may use the above rendering
   * methods to more directly control the layout by rendering individual
   * elements by index.
   */
  public String renderHtml()
  {
    if (getSize() == 0)
    {
      return "No radio buttons defined";
    }

    if (renderStyle == RS_VERTICAL)
    {
      return renderHtmlVertically();
    }

    return renderHtmlHorizontally();
  }

  /**
   * Setting renderCols causes the generic render method to size radio button
   * elements into the given number of columns (see renderHtml()).
   */
  public void setRenderCols(int renderCols)
  {
    this.renderCols = renderCols;
  }

  /**
   * Returns the column rendering setting.
   */
  public int getRenderCols()
  {
    return renderCols;
  }

  /**
   * Makes sure that the given value has a corresponding radio field element.
   *
   * Throws RuntimeException if value invalid
   */
  protected String processData(String value)
  {
    if (value != null && value.length() > 0 && getRadioElement(value) == null)
    {
      throw new RuntimeException("Invalid radio button data value: '"
        + value + "'");
    }
    return value;
  }
}
