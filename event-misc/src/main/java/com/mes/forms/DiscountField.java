/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/DiscountField.java $

  Description:
  
  DiscountField
  
  Customizes a NumberField to be a discount rate field allowing 2 right
  digits and 3 left digits.  Data is zero padded.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.StringTokenizer;

public class DiscountField extends NumberField
{
  public DiscountField(String fname,
                      String label,
                      boolean nullAllowed)
  {
    super(fname,label,6,6,nullAllowed,3);
  }
  public DiscountField(String fname,
                      boolean nullAllowed)
  {
    this(fname,fname,nullAllowed);
  }
  
  /*
  ** protected String processData(String rawData)
  **
  ** Tries to format the data into a discount rate.
  **
  ** RETURNS: formatted data if successful, else data as is.
  */
  protected String processData(String rawData)
  {
    try
    {
      StringTokenizer st = new StringTokenizer(
        Float.toString(Float.parseFloat(rawData)),".");
      String front = st.nextToken();
      StringBuffer back = new StringBuffer("");
      if (st.hasMoreTokens())
      {
         back.append(st.nextToken());
      }
      while (back.length() < 1)
      {
        back.append("0");
      }
      
      return (front + "." + back.toString());
    }
    catch (Exception e) { }
    
    return rawData;
  }
}
