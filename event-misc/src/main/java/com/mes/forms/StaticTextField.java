/*************************************************************************

  FILE: $URL: /Java/beans/com/mes/maintenance/TridentProfile.sqlj $
  
  StaticTextField
  
  Field that contains static text for display only (no editing)
  
  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.forms;

public class StaticTextField extends Field
{
  public StaticTextField(String fname, String label)
  {
    super(fname, label, true);
  }
  
  protected String renderHtmlField()
  {
    StringBuffer html = new StringBuffer("");
    
    if( ! getHtmlExtra().equals("") )
    {
      html.append("<span ");
      html.append(getHtmlExtra());
      html.append(">");
    }
    html.append(getData());
    if( ! getHtmlExtra().equals(""))
    {
      html.append("</span>");
    }
    html.append("<input type=\"hidden\" name=\""+getName()+"\" ");
    html.append("value=\"" + getData() + "\" ");
    html.append(">");
    return( html.toString() );
  }
  
}
