/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/ConstantField.java $

  Description:
  
  ConstantField
  
  Extends Field to define a field that has a fixed value.  The field may
  be rendered as html, it will simply display it's value as text.  The
  displayed text will be contained within a span container that may be
  assigned a class for formatting purposes.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class ConstantField extends Field
{
  private String styleClass;
  
  public ConstantField(String fname, String label, String value, String styleClass)
  {
    super(fname,label,0,0,true);
    this.styleClass = styleClass;
    setData(value);
  }
  public ConstantField(String fname, String value, String styleClass)
  {
    this(fname,fname,value,styleClass);
  }
  
  public void setStyleClass(String styleClass)
  {
    this.styleClass = styleClass;
  }
  
  protected void validateData()
  {
    hasError = false;
  }
  
  /*
  ** public String renderHtmlField()
  **
  ** Generates html to render the field's constant value in a span with the
  ** given style class.
  **
  ** RETURNS: html of the constant value.
  */
  public String renderHtmlField()
  {
    HtmlSpan span = new HtmlSpan();
    span.add(getData());
    span.setProperty("class",styleClass);
    return span.renderHtml();
  }
}
