/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/MerchantTypeTaxIdField.java $

  Description:
  
  MerchantTypeTaxIdField
  
  A field containing a 4-digit merchant type 
  with validation according allowed numbers in all character positions.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class MerchantTypeTaxIdField extends Field
{
  /*
  ** class MerchantTypeTaxIdValidation
  **
  ** Validates a tax ID number.  May be configured to do validations
  ** specific to either SSN or Federal Tax ID rules.
  */
  public class MerchantTypeTaxIdValidation implements Validation
  {
    private String errorText = "";
    
    private final String[] allowedChars = 
    {
       "123456789"
      ,"01234"
      ,"012345678"
      ,"0123456789"
    };
    
    public boolean validate(String fieldData)
    {
      // don't try to validate missing or empty data
      if (fieldData == null || fieldData.length() == 0)
      {
        return true;
      }
      
      try
      {
        // check for invalid length
        if (fieldData.length() != 4)
        {
          throw new Exception("Mechant Type Tax ID must be 4 digits long");
        }
        
        // validate each character in the merchant type tax id
        StringBuffer eb = new StringBuffer();
        char[] ca = fieldData.toCharArray();
        for(int i=0; i<fieldData.length(); i++) {
          
          String ac = allowedChars[i];
          if(ac.indexOf(ca[i]) < 0) {
            throw new Exception("Not a valid Merchant Type Tax ID");
          }
          
        }
        
        // no problems consider data fine
        return true;
      }
      catch (Exception e)
      {
        errorText = e.getMessage();
        if (errorText == null || errorText.length() == 0)
        {
          errorText = e.toString();
        }
      }
      
      return false;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }
  
  public MerchantTypeTaxIdField(String fname, String label, boolean nullAllowed)
  {
    super(fname,label,4,4,nullAllowed);
    addValidation(new MerchantTypeTaxIdValidation());
  }

}
