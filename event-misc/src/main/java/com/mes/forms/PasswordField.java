/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/PasswordField.java $

  Description:
  
  PasswordField

  Customizes the Field class to be a password entry field.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class PasswordField extends Field
{
  public PasswordField(String fname, 
                       String label,
                       int length, 
                       int htmlSize, 
                       boolean nullAllowed)
  {
    super(fname,label,length,htmlSize,nullAllowed);
  }
  public PasswordField(String fname, 
                       int length, 
                       int htmlSize, 
                       boolean nullAllowed)
  {
    this(fname,fname,length,htmlSize,nullAllowed);
  }

  protected String renderHtmlField()
  {
    StringBuffer html = new StringBuffer();
    html.append("<input type=\"password\" ");
    html.append("size=\"" + (htmlSize > 0 ? htmlSize : length) + "\" ");
    html.append("maxlength=\"" + length + "\" ");
    html.append("name=\"" + fname + "\" ");
    if (fdata != null)
    {
      html.append("value=\"" + getRenderData() + "\" ");
    }
    html.append(getHtmlExtra());
    html.append(">");
    return html.toString();
  }
}
    