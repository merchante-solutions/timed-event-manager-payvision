/*************************************************************************

  Description: /com/mes/forms/TridentTidField
  
  TridentTidField
  
  Specialized field group that contains three fields representing the three
  segments of a Vital TID (12, 4, 4)
  
  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.forms;

public class TridentTidField extends Field
{
  public TridentTidField(String fname, String label, int htmlSize,
    boolean nullAllowed)
  {
    super(fname, label, nullAllowed);
    
    init(htmlSize);
    
  }
  
  public TridentTidField()
  {
    super("tid_default", "tid_default", true);
    
    init(20);
  }
  
  private void init(int htmlSize)
  {
    this.length = 20;
    this.htmlSize = htmlSize;
    
    this.readOnly = true;
    
    if(!nullAllowed)
    {
      addValidation(new RequiredValidation(), "required");
    }
    
    addValidation(new TridentTidValidation(), "tridentTid");
  }
  
  public String getRenderData()
  {
    StringBuffer data = new StringBuffer();
    
    data.append(fdata.substring(0,12));
    data.append("&nbsp;&nbsp;");
    data.append(fdata.substring(12,16));
    data.append("&nbsp;&nbsp;");
    data.append(fdata.substring(16,20));
    
    return data.toString();
  }
  
  public String getMerchantNumber()
  {
    String result = "";
    
    try
    {
      result = fdata.substring(0, 12);
    }
    catch(Exception e)
    {
      result = e.toString();
    }
    
    return result;
  }
  
  public String getStoreNumber()
  {
    String result = "";
    
    try
    {
      result = fdata.substring(12, 16);
    }
    catch(Exception e)
    {
      result = e.toString();
    }
    
    return result;
  }
  
  public String getTerminalNumber()
  {
    String result = "";
    
    try
    {
      result = fdata.substring(16, 20);
    }
    catch(Exception e)
    {
      result = e.toString();
    }
    
    return result;
  }
  
  public class TridentTidValidation implements Validation
  {
    private String errorText;
    
    public boolean validate(String fdata)
    {
      boolean result = true;
      if(fdata == null || fdata.length() != 20)
      {
        result = false;
        errorText = "Invalid Trident TID (Must be 20 digits)";
      }
      return result;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }
  
  public void createTid(String merchantNumber, String terminalNumber)
  {
    StringBuffer fullTid = new StringBuffer("");
    
    if(merchantNumber == null || terminalNumber == null)
    {
      setData(null);
    }
    else
    {
      // fill first 16 with merchant number (padded right with '0'
      fullTid.append(merchantNumber);
      for(int i=0; i<16-merchantNumber.length(); ++i)
      {
        fullTid.append("0");
      }
      
      // fill last 4 with zero-padded terminal number
      for(int i=0; i<4-terminalNumber.length(); ++i)
      {
        fullTid.append("0");
      }
      fullTid.append(terminalNumber);
      
      setData(fullTid.toString());
    }
  }
}
