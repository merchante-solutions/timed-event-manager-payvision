package com.mes.forms;

import com.mes.tools.DropDownTable;

public class BBTBusinessTypeTable extends DropDownTable
{
  public BBTBusinessTypeTable()
  {
    // value/name pairs
    addElement("","select one");
    addElement("1","Sole Proprietorship");
    addElement("3","Partnership");
    addElement("2","Corporation");
    addElement("6","Non-Profit");
  }
}