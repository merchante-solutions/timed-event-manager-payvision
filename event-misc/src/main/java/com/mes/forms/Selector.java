package com.mes.forms;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Defines a field bean based component that represents a renderable set
 * of selection options.  Selection data is represented by a list of
 * SelectorItem's that may range from static text to more complex dynamic
 * renderable collections of information.
 *
 * Selector merges the functionality of a field bean with an html container
 * by extending HtmlFieldBean and wrapping an HtmlContainer.
 */
public abstract class Selector extends HtmlFieldBean
{
  static Logger log = Logger.getLogger(Selector.class);

  protected String                name;
  protected SelectorFrame         selectorFrame;
  protected List                  selectorItems = new ArrayList();
  protected HtmlRenderable        controlHeader = new HtmlText("&nbsp;");
  
  private   boolean               required      = true;
  protected AtLeastOneValidation  reqVal        = null;
  
  /**
   * A default selector frame for simple selectors.  It simply creates an
   * HtmlBlock to wrap around selector content.
   */
  public static class DefaultSelectorFrame extends SelectorFrame
  {
    /**
     * Setup default frame column definitions: a single column for the
     * entire selector item named "item".
     */
    public void setupFrame()
    {
      addColumn(new SelectorColumnDef("item"));
    }

    /**
     * Create the frame container.  This default frame container will be an
     * HtmlTable, with individual selector items added as rows in the table.
     */
    public HtmlContainer generateFrameContainer()
    {
      HtmlTable frameTable = new HtmlTable();
      frameTable.setProperty("width","100%");
      // debug
      frameTable.setProperty("border","1");
            
      return frameTable;
    }
    
    private HtmlContainer addRenderer()
    {
      // add new row to table
      HtmlTableRow selRow 
        = ((HtmlTable)frameContainer).add(new HtmlTableRow());
        
      // add a cell to the new row and return it
      return selRow.add(new HtmlTableCell()); 
    }

    /**
     * Adds a new row to the frame table and places the given renderable item
     * in it.
     */
    public void addSelectorRenderer(HtmlRenderable selRenderer)
    {
      HtmlContainer hc = addRenderer();
      hc.add(selRenderer);
    }

    /**
     * Adds a new row to the frame table and places the given renderable item
     * in it.
     */
    public void addHeaderRenderer(HtmlRenderable hdrRenderer)
    {
      HtmlContainer hc = addRenderer();
      hc.setProperty("style","text-align: center");
      hc.add(hdrRenderer);
    }
  }

  /**
   * Constructor.
   *
   * Stores the selector object name and selection item list, turns off
   * the default html container's formatting (disables indentation, and
   * other white space that makes the rendered html easier to read), calls
   * init().
   */
  public Selector(String name, List newItems, SelectorFrame selectorFrame, 
    boolean required)
  {
    this.name = name;

    //test
    //non-mutable: can only be set via constructor
    this.required = required;
    log.debug("required = "+required);

    setSelectorFrame(selectorFrame);
    
    selectorFrame.setControlHeader(getControlHeader());

    for (Iterator i = newItems.iterator(); i.hasNext();)
    {
      addSelector((SelectorItem)i.next());
    }
  }

  /**
   * Constructor.
   *
   * Creates a selector with the list of selector items and the given name
   * using the internally defined default selector frame.
   */
  public Selector(String name, List newItems)
  {
    this(name,newItems,new DefaultSelectorFrame(),true);
  }

  /**
   * Adds a selector item.  Extracts the item's field(s) and places them into
   * the bean field pool, applies required validation if needed, sets the
   * control field in the selector item so that the item can tie internal
   * validation to it, etc.
   */
  public final void addSelector(SelectorItem item)
  {
      // initialize the selector item
      item.init();
      
      // add item to the internal item list
      selectorItems.add(item);
      
      // extract the item's field(s), add to bean storage
      fields.add(item.getFields());
      
      // do implementation specific handling of item
      // (gets the control field back)
      Field field = processSelector(item);

      // pass the control field into the item
      item.setControlField(field);
      
      // add the item to the frame along with it's control tag
      selectorFrame.addSelector(getControlTag(item),item);
      
      //high level validation for all control fields
      //single selectors are by nature always true
      if(required)
      {
        if(reqVal==null)
        {
          log.debug("creating AtLeastOne validation...");
          reqVal = new AtLeastOneValidation("You must select at least one "
            + "option from the list");
          field.addValidation(reqVal);
        }
        log.debug("adding -- "+field.getName()+" -- to AtLeastOne...");
        reqVal.addField(field);
      }
  }

  /**
   * Sets the internal selector frame to the new given frame.  Any selector
   * items are added to the new frame.
   */
  public void setSelectorFrame(SelectorFrame selectorFrame)
  {
    // add all selector items to the new frame
    for (Iterator i = selectorItems.iterator(); i.hasNext();)
    {
      SelectorItem item = (SelectorItem)i.next();
      selectorFrame.addSelector(getControlTag(item),item);
    }
    
    // set internal frame to be the new frame
    this.selectorFrame = selectorFrame;
  }

  /**
   * Does implementation specific handling of a newly added selector item.
   * Needs to return the "control field" or the field that indicates that the
   * particular selector has been selected.  Single selectors would always
   * return the radio field that determines the one selector item that has
   * been chosen, multi selectors would return an individual check field
   * associated with a particular item.
   */
  protected abstract Field processSelector(SelectorItem item);
  
  
  /**
   * Returns a container suitable for containing the control header.  Should 
   * also be used by getControlTag for consistency.
   */
  protected abstract HtmlContainer getControlContainer(boolean isHeader);
  
  /**
   * Does implementation specific handling of getting the control tag 
   * associated with a particular selector item.  The control tag is
   * plugged into the selector frame control column during rendering.
   * Examples are a radio button input tag extracted from a radio field
   * for single selectors or an actual check field for multi selectors.
   */
  protected abstract HtmlRenderable getControlTag(SelectorItem item);
  
  /**
   * Returns a renderable control header.
   */
  public HtmlRenderable getControlHeader()
  {
    HtmlContainer c = getControlContainer(true);
    c.add(controlHeader);
    return c;
  }
  
  /**
   * Sets the control header.
   */
  public void setControlHeader(HtmlRenderable controlHeader)
  {
    this.controlHeader = controlHeader;
    selectorFrame.setControlHeader(getControlHeader());
  }
  public void setControlHeader(String controlLabel)
  {
    setControlHeader(new HtmlText(controlLabel));
  }

  /**
   * Renders the selector by rendering selectorFrame.
   */
  public String renderHtml()
  {
    return selectorFrame.renderHtml();
  }

  /**
   * Returns a List containing any selected SelectorItems
   */
  public List getSelected()
  {
    List selected = new ArrayList();
    for (Iterator i = selectorItems.iterator(); i.hasNext();)
    {
      SelectorItem item = (SelectorItem)i.next();
      if (item.isSelected())
      {
        selected.add(item);
      }
    }
    return selected;
  }
}
