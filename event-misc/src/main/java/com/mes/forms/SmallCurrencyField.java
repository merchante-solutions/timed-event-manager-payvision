/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/SmallCurrencyField.java $

  Description:
  
  CurrencyField

  Customizes a NumberField to be a currency field with two digits of 
  precision.  Data is zero padded.  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.StringTokenizer;

public class SmallCurrencyField extends CurrencyField
{
  public SmallCurrencyField(String fname,
                            String label,
                            int length,
                            int htmlSize,
                            boolean nullAllowed)
  {
    super(fname,label,length,htmlSize,nullAllowed);
  }
  
  public SmallCurrencyField(String fname,
                            String label,
                            int length,
                            int htmlSize,
                            boolean nullAllowed,
                            float lbound,
                            float ubound)
  {
    super(fname,label,length,htmlSize,nullAllowed,lbound,ubound);
  }
  
  public SmallCurrencyField(String fname,
                      int length,
                      int htmlSize,
                      boolean nullAllowed)
  {
    this(fname,fname,length,htmlSize,nullAllowed);
  }
  
  public SmallCurrencyField(String fname,
                      int length,
                      int htmlSize,
                      boolean nullAllowed,
                      float lbound,
                      float ubound)
  {
    this(fname,fname,length,htmlSize,nullAllowed,lbound,ubound);
  }

  /*
  ** protected String processData(String rawData)
  **
  ** Formats the raw data into dollars and cents.  Implied decimal if no
  ** decimal given (1 = 0.01, 23 = 0.23, 456 = 4.56, etc).
  **
  ** RETURNS: formatted currency amount if able, else raw data as is.
  */
  protected String processData(String rawData)
  {
    try
    {
      // test to make sure the data is a parsable float
      Float.toString(Float.parseFloat(rawData));
      
      String newData = rawData;
      
      // if no decimal is given then it is implied:
      // 
      if (rawData.indexOf('.') < 0)
      {
        // force new data to have the implied decimal
        newData = Float.toString(Float.parseFloat(rawData) / 100);
      }
      
      // make sure there is a decimal followed by two digits
      StringTokenizer st = new StringTokenizer(newData,".");
      
      // get the dollar portion
      String dollar = "0";
      if (newData.charAt(0) != '.')
      {
        dollar = st.nextToken();
      }

      // get the cents portion      
      String cents = "";
      if (st.hasMoreTokens())
      {
         cents += st.nextToken();
      }
      
      // pad cents if under 2 digits      
      while (cents.length() < 2)
      {
        cents += "0";
      }
      
      // return currency formatted as dollars and cents
      return (dollar + "." + cents);
    }
    catch (Exception e) { }
    
    // must've been problem with rawData, return it as is
    return rawData;
  }
}
