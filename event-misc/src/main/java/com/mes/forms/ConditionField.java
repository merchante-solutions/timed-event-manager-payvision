/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/ConditionField.java $

  Description:
  
  ConditionField
  
  A field thats value is based on a condition.  If the condition is true
  than one value is given, otherwise another is given.  The values are
  set by the user.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class ConditionField extends Field
{
  private Condition condition;
  private String trueVal;
  private String falseVal;
  
  public ConditionField(String fname,
                        Condition condition,
                        String trueVal,
                        String falseVal)
  {
    super(fname,0,0,true);
    this.condition = condition;
    this.trueVal = trueVal;
    this.falseVal = falseVal;
  }                    
  public ConditionField(String fname,
                        Condition condition)
  {
    super(fname,0,0,true);
    this.condition = condition;
    this.trueVal = "Y";
    this.falseVal = "N";
  }

  protected void validateData()
  {
    hasError = false;
  }
  
  public String getData()
  {
    return (condition.isMet() ? trueVal : falseVal);
  }
}
