package com.mes.forms;

import com.mes.user.UserBean;
import sqlj.runtime.ref.DefaultContext;

public class HtmlFieldBean extends FieldBean implements HtmlRenderable
{
  public HtmlFieldBean()
  {
  }
  
  public HtmlFieldBean(DefaultContext defCtx)
  {
    super(defCtx);
  }
  
  public HtmlFieldBean(UserBean user)
  {
    super(user);
  }
  
  public String renderHtml()
  {
    return "";
  }
}