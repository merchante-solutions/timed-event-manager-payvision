package com.mes.forms;

public class SelectorColumnDef
{
  private String name;
  private HtmlRenderable header;
  private int width;
  
  public SelectorColumnDef(String name, HtmlRenderable header, int width)
  {
    this.name = name;
    this.header = header;
    this.width = width;
  }
  public SelectorColumnDef(String name, String headerLabel, int width)
  {
    this(name,new HtmlText(headerLabel),width);
  }
  public SelectorColumnDef(String name, int width)
  {
    this.name = name;
    this.width = width;
  }
  public SelectorColumnDef(String name)
  {
    this.name = name;
  }
  
  public String getName()
  {
    return name;
  }
  
  public HtmlRenderable generateHeader()
  {
    HtmlContainer container = generateContainer();
    if (header != null)
    {
      container.add(header);
    }
    return container;
  }
  
  public HtmlContainer generateContainer()
  {
    HtmlDiv div = new HtmlDiv();
    div.setProperty("style",
      "float: left" + (width > 0 ? "; width: " + width + "px" : ""));
    return div;
  }
  
  public boolean hasHeader()
  {
    return header != null;
  }
  
  public int getWidth()
  {
    return width;
  }
}