package com.mes.forms;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;

public abstract class SelectorFrame implements HtmlRenderable
{
  static Logger log = Logger.getLogger(SelectorFrame.class);
  
  // this is a hack to make control header rendering dynamic
  public class ControlHeaderRenderer implements HtmlRenderable
  {
    public String renderHtml()
    {
      return controlHeader.renderHtml();
    }
  }
  
  protected List columns;
  protected HtmlContainer frameContainer;
  protected boolean headersPresent;
  protected boolean isEmpty = true;
  protected HtmlRenderable controlHeader;
  
  public SelectorFrame()
  {
    initialize();
  }
  
  private void initialize()
  {
    columns = new ArrayList();
    
    // implementation specific column definition
    setupFrame();
    
    // implementation specific frame container generation
    frameContainer = generateFrameContainer();
  }
  
  public void setControlHeader(HtmlRenderable controlHeader)
  {
    this.controlHeader = controlHeader;
  }
  
  /**
   * Implementation specific, generally where column definition will be done.
   */
  public abstract void setupFrame();
  
  /**
   * Implementation specific, generate the overall renderable frame container.
   */
  public abstract HtmlContainer generateFrameContainer();
  
  /**
   * Implementation specific, adds a selector renderer to the frame container.
   *
   * Example: generateFrameContainer() generates a table, so this method would
   * be responsible for creating a row containing the renderer passed in and
   * adding it to the table.
   */
  public abstract void addSelectorRenderer(HtmlRenderable hr);
  

  /**
   * Implementation specific, adds a header renderer to the frame container.
   *
   * Example: generateFrameContainer() generates a table, so this method would
   * be responsible for creating a row containing the renderer passed in and
   * adding it to the table.
   *
   * NOTE: this method exists so that special styling can be applied to the
   *       header row of the selector.
   */
  public abstract void addHeaderRenderer(HtmlRenderable hr);
  
  /**
   * Generates a header renderer from the columns and adds it to the frame
   */
  protected void addHeaderRenderer()
  {
    // make sure a control header was defined
    if (controlHeader == null)
    {
      throw new RuntimeException("Cannot render header with no control header"
        + " component defined");
    }
    
    // add each column's header to a header container
    HtmlBlock headerRenderer = new HtmlBlock();
    headerRenderer.add(new ControlHeaderRenderer());
    for (Iterator i = columns.iterator(); i.hasNext();)
    {
      SelectorColumnDef column = (SelectorColumnDef)i.next();
      headerRenderer.add(column.generateHeader());
    }
    
    // add the header renderer to the frame
    addHeaderRenderer(headerRenderer);
  }

  /**
   * Generates renderable object from a combination of the column defs and
   * the new selector item and places it into the frame container.
   */
  public final void addSelector(HtmlRenderable controlTag, SelectorItem item)
  {
    log.debug("adding selector item " + item.getValue() + " to selector frame");
    
    // may need to generate a header row if this is first selector item
    if (isEmpty)
    {
      if (headersPresent)
      {
        addHeaderRenderer();
      }
      isEmpty = false;
    }
    
    // create a new renderable container for this selection item
    HtmlBlock itemRenderer = new HtmlBlock();
    
    // place the control tag at the front of the selector item renderer
    itemRenderer.add(controlTag);
      
    // for each defined column, grab the item's column renderer and
    // add it to the item container
    for (Iterator i = columns.iterator(); i.hasNext(); )
    {
      SelectorColumnDef column = (SelectorColumnDef)i.next();
      
      // create a column container from the column def and add the
      // item column renderer into it
      HtmlContainer columnContainer = column.generateContainer();
      columnContainer.add(item.getColumnRenderer(column));
      
      // add to the selector item container
      itemRenderer.add(columnContainer);
    }
    
    // add the new selection item render into the frame container
    addSelectorRenderer(itemRenderer);
  }

  /**
   * Adds a column definition.  If column has a defined header then the
   * header flag is turned on.
   */
  public void addColumn(SelectorColumnDef column)
  {
    // don't allow columns to be added after any selectors have been added
    // since this would cause issues with headers and selector renderers...
    if (!isEmpty)
    {
      throw new RuntimeException("Adding column definitions after"
        + " selector items have been added is not allowed");
    }
    
    columns.add(column);
    
    // turn on headers flag if column has header
    if (column.hasHeader())
    {
      headersPresent = true;
    }
  }

  /**
   * Renders the contents of the frame container.
   */
  public String renderHtml()
  {
    return frameContainer.renderHtml();
  }
}