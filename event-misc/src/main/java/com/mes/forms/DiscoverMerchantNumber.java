/*************************************************************************

  Description: /com/mes/forms/DiscoverMerchantNumber
  
  DiscoverMerchantNumber
  
  Field that provides specific validations for Discover Merchant Numbers
  
  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.forms;

import com.mes.support.TridentTools;

public class DiscoverMerchantNumber extends NonBankMerchantId
{
  public static int   DISCOVER_NUMBER_LENGTH    = 15;
  
  // contstructor is pretty normal except that it takes an 'acceptField' that
  // controls the type of validation that occurs
  public DiscoverMerchantNumber(String fname, String label, int htmlSize,
    boolean nullAllowed, Field acceptField)
  {
    super(fname, label, htmlSize, nullAllowed, DISCOVER_NUMBER_LENGTH, acceptField);
  }
  
  protected boolean isValidMID(String fdata)
  {
    return TridentTools.mod10Check(fdata.substring(5, fdata.length()));
  }
  
}
