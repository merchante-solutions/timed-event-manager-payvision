/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/HtmlLink.java $

  Description:
  
  HtmlLink

  Extends HtmlContainer to define an html anchor tag.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-02-22 10:31:52 -0800 (Thu, 22 Feb 2007) $
  Version            : $Revision: 13480 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class HtmlLink extends HtmlContainer
{
  {
    addProperty("href");
    addProperty("name");
    addProperty("target");
    isTransparent = false;
  }
  
  public HtmlLink()
  {
  }
  
  public HtmlLink(String href)
  {
    setProperty("href",href);
  }
  
  public HtmlLink(String href, String text)
  {
    setProperty("href",href);
    add(new HtmlText(text));
  }
  
  public HtmlRenderable get(int idx)
  {
    return getContent(idx);
  }
  
  public String getName()
  {
    return "a";
  }
}
