/*************************************************************************

  Description: /com/mes/forms/AmexSENumber
  
  AmexSENumber
  
  Field that provides specific validations for Amex SE Numbers (merchant
  numbers)
  
  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.forms;

import com.mes.support.TridentTools;

public class AmexSENumber extends NonBankMerchantId
{
  public static int   SE_NUMBER_LENGTH    = 10;
  
  // contstructor is pretty normal except that it takes an 'acceptField' that
  // controls the type of validation that occurs
  public AmexSENumber(String fname, String label, int htmlSize,
    boolean nullAllowed, Field acceptField)
  {
    super(fname, label, htmlSize, nullAllowed, SE_NUMBER_LENGTH, acceptField);
  }
  
  protected boolean isValidMID(String fdata)
  {
    return TridentTools.isValidSeNumber(fdata);
  }
  
}
