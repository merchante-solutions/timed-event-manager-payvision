/*************************************************************************

  Description: /com/mes/forms/AmexSENumber
  
  JCBMerchantNumber
  
  Field that provides specific validations for JCB Merchant Numbers
  
  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.forms;

public class JCBMerchantNumber extends NonBankMerchantId
{
  public static int JCB_MID_LENGTH = 10;
  
  public JCBMerchantNumber(String fname, String label, int htmlSize,
    boolean nullAllowed, Field acceptField)
  {
    super(fname, label, htmlSize, nullAllowed, JCB_MID_LENGTH, acceptField);
  }
  
  protected boolean isValidMID(String fdata)
  {
    return true;
    //return (TridentTools.mod10Check(fdata));
  }
}
