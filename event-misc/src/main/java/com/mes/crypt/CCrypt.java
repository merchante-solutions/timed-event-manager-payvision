/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/crypt/CCrypt.java $

  Description:  

  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 3/12/02 2:40p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.crypt;

class CCrypt
{
  public static String decodeCard( long longVal )
  {
    int           intVal    = 0;
    char[]        chars     = new char[2];
    StringBuffer  strVal    = new StringBuffer("");
    
    for( int i = 0; i < 16; ++i )
    {
      intVal  = (int)((longVal >> (4*i)) & 0x0f);
      System.out.println(intVal);
      chars[(1-(i%2))] = Character.forDigit(intVal,16);
      
      ++i;
      intVal  = (int)((longVal >> (4*i)) & 0x0f);
      System.out.println(intVal);
      chars[(1-(i%2))] = Character.forDigit(intVal,16);
      
      System.out.print( chars[0] );
      System.out.print( chars[1] );
      System.out.println();
      
      strVal.append( chars[0] );
      strVal.append( chars[1] );
    }

    // strip leading 0's
    while( strVal.charAt(0) == '0' )
    {
      strVal.deleteCharAt(0);
    }
    return( strVal.toString() );
  }

  public static long encodeCard( String strVal )
    throws Exception
  {
    char      charVal     = 0;
    long      longVal     = 0L;

    // if this is an old style 13-digit card number
    // then add leading 0's to the value
    if ( strVal.length() < 16 )
    {
      StringBuffer      newVal    = new StringBuffer(strVal);
      
      while( newVal.length() < 16 )
      {
        // insert a leading 0
        newVal.insert(0,'0');
      }
      strVal = newVal.toString();
    }
    
    if ( strVal.length() != 16 )
    {
      throw( new Exception("Invalid string length") );
    }
    for( int i = 0; i < 8; ++i )
    {
      charVal = (char) Integer.parseInt( strVal.substring((2*i),((2*i)+2)),16 );
      longVal |= ((long)charVal << (8*i));
    }
    return( longVal );
  }
  
  public static void main( String[] args )
  {
    try
    {
      long      ec = encodeCard(args[0]);
      System.out.println( ec );
      System.out.println( decodeCard(ec) );
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }      
  }
}