package com.mes.controlscan;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.mes.constants.CSConstants;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class SmartSyncAPI extends SQLJConnectionBase{

    private static final long serialVersionUID = 1L;

    static Logger logger = Logger.getLogger(SmartSyncAPI.class);
    
    private MultivaluedMap<String, String> formData;
    boolean monthEnd;
    String portfolio;
    
    public void callAPI(String controlScanURL, String userName, String pwd, boolean monthEndcheck ){
        boolean hasRecords = false;
        Client client;
        WebResource wr;
        ClientResponse cr;
        logger.info("calling smart sync api for portfolio:" + userName);
        
        try{
            connect(true);
            com.mes.net.MesNetTools.disableSSLCertificateValidation();
            Calendar now = Calendar.getInstance();
            now.add(Calendar.DAY_OF_MONTH, -1);
            monthEnd = monthEndcheck;
            portfolio = userName;
            
            formData = new MultivaluedMapImpl();
            formData.add("api_username", userName);
            formData.add("api_password", pwd);

            /*
             * Pull all merchants on Month end day run (25th). 
             * Remaining days, pull only records that have changed since the last_updated date 
             */
            if(monthEndcheck){
                logger.info("Monthend billing run");
            } else {
                logger.info("Pulls records that have changed since previous day");
                formData.add("last_updated", DateTimeFormatter.getFormattedDate(now.getTime(),"yyyy-MM-dd") );
            }
             
            client = Client.create();
            logger.info("Sending request to: " + controlScanURL);
            wr = client.resource(controlScanURL);
 
            /*
             * Repeat calling controlscan API until you get record count as zero
             */           
            do{
                logger.info("Form data: "+formData.keySet());
                cr = wr.accept(MediaType.APPLICATION_JSON).type("application/x-www-form-urlencoded").post(ClientResponse.class, formData);
                String output = cr.getEntity(String.class);
                if(cr.getStatus() != 200) {
                    logger.debug("Failed Control Scan API call");
                }
                else
                    hasRecords = processResp(output,monthEndcheck);
            }while(hasRecords);
           
        }catch(Exception e){
            logEntry("callAPI(String controlScanURL, String userName, String pwd, boolean monthEndcheck )", e.toString());
        } finally{
            cleanUp();
        }
                
    }
    
    @SuppressWarnings("unchecked")
    public boolean processResp(String output, boolean monthEndcheck){
        String apiId;
        int recCount = 0;
        logger.info("Response from Control Scan: " +output);
        logger.info("Processing response from Control Scan");
        try{
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(output);
            JSONObject results = (JSONObject)jsonObject.get("t_extended");
            JSONObject data = (JSONObject)results.get("data");
            apiId = String.valueOf(data.get("api_last_id"));
            recCount = Integer.valueOf(String.valueOf(data.get("record_count")));            
            JSONArray jsonarray = (JSONArray) results.get("results");
            ControlScanRecord csr;
            logger.info("Results count: "+jsonarray.size());
            for (JSONObject aJsonarray : (Iterable<JSONObject>) jsonarray) {
                csr = new ControlScanRecord(aJsonarray);
                if(csr != null && csr.merchant_number > 0){
                    storeData(csr);
                    if (monthEndcheck) {
                    	chargeMerchants(csr.merchant_number,(csr.pci_compliant.equalsIgnoreCase("No") ? "Y": "N"));
                    	if(!"".equals(csr.submids.trim()))
                    		chargeSubMerchants(csr.submids, (csr.pci_compliant.equalsIgnoreCase("No") ? "Y": "N"));
                    } else
                        updateMerchPci(csr.merchant_number,csr);
                    if(!StringUtils.isEmpty(csr.submids))
                        updateSubmidsPci(csr.submids,csr);
                }
            }
            // set api_last_id to make additional call to retrieve the remaining records starting at the first record after the api_last_id
            formData.add("api_last_id", apiId);
            
        }catch(Exception e){
            logEntry("processResp(String output, boolean monthEndcheck)::", e.toString());
        }
        return recCount != 0;
    }
    
    public void storeData(ControlScanRecord csr){
        logger.info("Storing data to Controlscan_data table");
        PreparedStatement ps = null;
        String sqlText = "insert into controlscan_data " +
                "(merchant_name,controlscan_id,agent_name,merchant_number,submids," +
                "contact_name,contact_email,phone,address,city,state,zipcode,creation_date," +
                "customer_sign_up_date,sua_date,saq_type,overall_pci_compliant_date,compliant_saq_date," +
                "compliant_scan_date,scan_status,hosts_supplied,offline_compliance,scan_not_reqed_for_compliance," +
                "scan_expiration_date,overall_pci_compliant,saq_status,mes_scan_date,monthend_run,portfolio) values " +
                " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,sysdate,?,?)";
        try{
            ps = con.prepareStatement(sqlText);
            ps.setString(1, csr.merchant_name);
            ps.setLong(2, csr.controlscan_id);
            ps.setString(3, csr.agent_name);
            ps.setLong(4, csr.merchant_number);
            ps.setString(5, csr.submids);
            ps.setString(6, csr.contact_name);
            ps.setString(7, csr.contact_email);
            ps.setString(8, csr.phone);
            ps.setString(9, csr.address);
            ps.setString(10, csr.city);
            ps.setString(11, csr.state);
            ps.setString(12, csr.zipcode);
            ps.setDate(13, csr.creation_date);
            ps.setDate(14, csr.cust_signup_date );
            ps.setDate(15, csr.sua_date);
            ps.setString(16, csr.saq_type);
            ps.setDate(17, csr.pci_compliant_date);
            ps.setDate(18, csr.compliant_saq_date);
            ps.setDate(19, csr.compliant_scan_date);
            ps.setString(20, csr.scan_status);
            ps.setInt(21, csr.hosts_supplied);
            ps.setString(22, csr.offline_compliance);
            ps.setInt(23, csr.scan_nr_compliance);
            ps.setDate(24, csr.scan_exp_date);
            ps.setString(25, csr.pci_compliant);
            ps.setString(26, csr.saq_status);
            if(monthEnd)
                ps.setString(27, "Yes");
            else
                ps.setString(27, "No");
            ps.setString(28, portfolio);
            
            ps.executeUpdate();
        }catch(Exception e){
        	if(e.getMessage().contains(CSConstants.EXCEPTION_ORA_12899)||e.getMessage().contains(CSConstants.EXCEPTION_ORA_01461)){
        		sendEmailnotification(csr.submids);
        		logger.error("List of Controlscan SubMID failed to insert for controlscan_id:"+csr.controlscan_id +
        				" for merchant_id :"+csr.merchant_number+": \n"+csr.submids);
        	}
        	logEntry("storeData(ControlScanRecord csr)", e.toString());
        } finally{
            try{
                if (ps != null) {
                    ps.close();
                }
            } catch( Exception e ) {}
        }
        
    }
    
    public void sendEmailnotification(String subMids){
      logger.debug("SmartSyncAPI::sendEmailnotification():Sending email...");
    	StringBuffer sb = null;
    	MailMessage msg = null;
    	try{
    		sb = new StringBuffer();
    		msg = new MailMessage();
    		msg.setAddresses(MesEmails.MSG_ADDRS_CONTROLSCAN_ERROR_NOTIFY);
    		msg.setSubject("Controlscan Retrieve merchants - Sub Merchant Id's");
    		sb.append("Problem storing Controlscan SubMID due to short data length, please increase data storage for table CONTROLSCAN_DATA for column SUBMIDS");
    		sb.append("Below are the submids received from API call \n");
    		sb.append(subMids);
    		msg.setText(sb.toString());
    		msg.send();
    	}catch(Exception e){
    		logEntry("SmartSyncAPI::sendEmailnotification()",e.toString());
    	}
    	logger.debug("SmartSyncAPI::sendEmailnotification():Mail has been sent");
    }
    
    public void updateMerchPci(long merchantNumber, ControlScanRecord csr){
        PreparedStatement ps = null;
        ResultSet rs = null;
        long qid;
        String trackNumber;
        
        try{
            logger.info("updating merchant table for merchant id" + merchantNumber);
            ps = con.prepareStatement("update merchant set risk_pci_vendor_code = ?, risk_pci_compliant = ?, risk_pci_last_scan_date = ? where merch_number = ?");
            ps.setString(1, CSConstants.RISK_PCI_VENDOR_CODE);
            
            if( (csr.pci_compliant).equalsIgnoreCase("yes"))
            {
                ps.setString(2, CSConstants.RISK_PCI_COMPLIANCE_YES);
            } else {
                if(checkGracePeriod(merchantNumber))
                    ps.setString(2,CSConstants.RISK_PCI_COMPLIANCE_GRACE);
                else
                    ps.setString(2, CSConstants.RISK_PCI_COMPLIANCE_NO);
            }
            ps.setDate(3, csr.compliant_scan_date);
            ps.setLong(4, merchantNumber);
            ps.executeUpdate();
            int scanStatus = -1;
            if ((csr.scan_status).equalsIgnoreCase(CSConstants.SCAN_STATUS_NA) || csr.scan_status.equals(""))
                scanStatus = 3;
            else if((CSConstants.SCAN_STATUS_INPROGRESS).contains(csr.scan_status))
                scanStatus = 2;
            else if (CSConstants.SCAN_STATUS_YES.contains(csr.scan_status))
                scanStatus = 1;
            else if(CSConstants.SCAN_STATUS_NO.contains(csr.scan_status))
                scanStatus = 0;
            ps.close();
                
            ps = con.prepareStatement("select status, trunc(last_scan_date) as last_scan_date, trunc(sysdate) as today from pci_scan_compliance where merchant_number = ? order by last_scan_date desc");
            ps.setLong(1, merchantNumber);
            rs = ps.executeQuery();
            boolean pciScanCheck = true;
            if(rs.next()){
                if(rs.getInt("status") == scanStatus && rs.getDate("last_scan_date").equals( rs.getDate("today")))
                    pciScanCheck = false;
                }
            rs.close();
            ps.close();
                
            if(pciScanCheck){
                logger.info("inserting to pci_scan_compliance");
                ps = con.prepareStatement("insert into pci_scan_compliance( merchant_number, status, last_scan_date, username) values ( ?,?,?,?)");
                ps.setLong(1, merchantNumber);
                ps.setInt(2, scanStatus);
                ps.setDate(3, csr.compliant_scan_date);
                ps.setString(4, "ControlScan");
                ps.executeUpdate();
                }
           ps.close(); 
           boolean addQuizHist = true;
           String pciStatus = csr.saq_status.equalsIgnoreCase(CSConstants.QUESTIONNAIRE_STATUS) ? "Complete" : "Incomplete";
           ps = con.prepareStatement("select result, trunc(date_taken) as date_taken, trunc(sysdate) as today from quiz_history where merch_number = ? order by date_taken desc");
           ps.setLong(1, merchantNumber);
           rs = ps.executeQuery();
                
          if(rs.next()) {
              if(rs.getString("result") == pciStatus && rs.getDate("date_taken").equals(rs.getDate("today")))
                  addQuizHist = false;                    
              }
          rs.close();
          ps.close();
          
          if(addQuizHist){
              logger.info("inserting to quiz history");
              ps = con.prepareStatement("Select quiz_history_sequence.nextval qid from dual ");
              rs = ps.executeQuery();
              if (rs.next()) {
                  qid = rs.getLong("qid");
                  trackNumber = "PCI*" + String.format("%010d", qid);
                  ps = con.prepareStatement("insert into quiz_history (id, quiz_id, merch_number, " +
                                "date_taken, result, tracking_number, username) values (?,?,?,?,?,?,?)");
                  ps.setLong(1, qid);
                  ps.setInt(2, 14);
                  ps.setLong(3, merchantNumber);
                  ps.setDate(4, csr.compliant_saq_date);
                  ps.setString(5, pciStatus);
                  ps.setString(6, trackNumber);
                  ps.setString(7, "ControlScan");
                  ps.executeUpdate();
                  
                  ps.close();
                  rs.close();
              }
          }

       logger.info("Updating pci data is complete");
        }catch(Exception e){
            logEntry("updateMerchPCI(ControlScanRecord csr)", e.toString());
        }finally{
            try{ 
                if (ps != null) ps.close();
                if (rs != null) rs.close();
            } catch( Exception e ) {
                logger.error("Inside Finally Clause updateMerchPCI():" +e.getMessage());
            }
        }
    }
  
    public void chargeSubMerchants(String smids,String chargeInd){
    	String[] submids = smids.split(",");
    	for(int i=0;i<submids.length;i++){
    		chargeMerchants(Long.parseLong(submids[i]),chargeInd);
    	}
    }
    
    
    public void updateSubmidsPci(String smids, ControlScanRecord csr){
        String[] submids = smids.split(",");
        for(String submid : submids){
            if(!StringUtils.isEmpty(submid)){
               updateMerchPci(Long.parseLong(submid), csr);
            }
        }
      }
    
    
     public void chargeMerchants(long merchantNumber, String chargeInd){
         logger.info("Updating billing frequency in mbs charge records");
         PreparedStatement ps = null;
         ResultSet rs = null;
         int month = Calendar.getInstance().get(Calendar.MONTH);
         try{
             if(chargeInd.equalsIgnoreCase("Y")){
                 if(checkGracePeriod(merchantNumber))
                     chargeInd = "N";
             }
             ps = con.prepareStatement("select substr(billing_frequency,?, 1) as bInd from mbs_charge_records " +
                         "where merchant_number = ? and enabled = 'Y' and end_date >= sysdate and statement_message = 'PCI NON-COMPLIANCE FEE'");
             ps.setInt(1, month+1);
             ps.setLong(2, merchantNumber);
             rs = ps.executeQuery();
             String bInd = "";
             if(rs.next())
                 bInd = rs.getString("bInd");
             ps.close();
             rs.close();
             if(!bInd.equals("") && !chargeInd.equalsIgnoreCase(bInd))
                 {
                 ps = con.prepareStatement("update mbs_charge_records set billing_frequency = substr(billing_frequency,1,?)||?||substr(billing_frequency,?) " +
                              " where merchant_number = ? and enabled = 'Y' and statement_message = 'PCI NON-COMPLIANCE FEE' ");
                 ps.setInt(1, month);//7
                 ps.setString(2, chargeInd);
                 ps.setInt(3, month+2);//9
                 ps.setLong(4, merchantNumber);
                 ps.executeUpdate();
                 logBillingChange(merchantNumber,month, bInd, chargeInd,"BILLING");
                } else {
                  logger.info("No change in billing for MID:" + merchantNumber);
                	logBillingChange(merchantNumber,month, bInd, chargeInd,"NON-BILLING");
                 }
             
         }catch(Exception e){
        	 logEntry("updateMbsChargeRecords(long merch_number, String chargeInd )", e.toString());
            }finally{
                try{
                    if (ps != null) ps.close();
                    if (rs != null) rs.close();
                } catch( Exception e ) {
                    logger.error("Inside Finally Clause : "+e.getMessage());
                }
            }
     }
     
     public void logBillingChange(long merchNumber, int month, String oldV, String newV,String changeBy){
         logger.info("adding to csan_log table");
         PreparedStatement ps = null;
         String sqlText = "insert into controlscan_log (" +
                 "merchant_number, log_date, field_changed, old_value, new_value, change_type, changed_by)" +
                 "values (?,sysdate,?,?,?,?,?)";
         try{
             ps = con.prepareStatement(sqlText);
             ps.setLong(1, merchNumber);
             ps.setString(2, "Billing frequency for the month of " + month);
             ps.setString(3, oldV);
             ps.setString(4, newV);
             ps.setString(5, "UPDATE");
             ps.setString(6, changeBy);
             ps.executeUpdate();
             logger.info("added log record to cscan_table");
         }catch(Exception e){
             logEntry("logBillingChange(long merch_number, int month)", e.toString());
         }finally{
                try{
                    if (ps != null) {
                        ps.close();
                    }
                } catch( Exception e ) {}
            }
     }
     
      public boolean checkGracePeriod(long merch_number){
	     boolean result = false;
	     PreparedStatement ps = null;
	     ResultSet rs = null;
	     Calendar now = Calendar.getInstance();
	     Date date_opened ;
	     Date grace_limit ;
	     try{
	         ps = con.prepareStatement("select mif_date_opened(mif.date_opened) as date_opened, trunc(add_months(sysdate,-6)) as grace_limit from mif where merchant_number = ? ");
	         ps.setLong(1, merch_number);
	         rs = ps.executeQuery();
	         if(rs.next()) {
	        	 date_opened = rs.getDate("date_opened");
	            grace_limit = rs.getDate("grace_limit");
	            if(date_opened == null)
	                result = true;
	            else
	                result = date_opened.after(grace_limit);
	         }
	         
	     }catch(Exception e){
	         logEntry("checkGracePeriod(long merch_number)", e.toString());
	     }finally{
	         try{ 
	             if (ps != null) {
	                 ps.close();
	                 rs.close();
	             }
	         } catch( Exception e ) {}
	     }
	     return result;
	 }

	public static void main(String args[]){
        SmartSyncAPI ssa = new SmartSyncAPI();
     }
   
}
