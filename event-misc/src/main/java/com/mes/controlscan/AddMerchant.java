package com.mes.controlscan;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import com.mes.database.SQLJConnectionBase;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

public class AddMerchant extends SQLJConnectionBase{
	Client client;
    WebResource wr;
    ClientResponse cr;
    MultivaluedMap<String, String> formData;
    
		public void addFromMIF(int bank){
		PreparedStatement ps = null;
		String sqlText = "select mif.merchant_number, MIF.NAME1_LINE_1, MIF.PHONE_1, MIF.DBA_NAME,MIF.ADDR1_LINE_1, MIF.CITY1_LINE_4, MIF.STATE1_LINE_4, " +
				"MIF.ZIP1_LINE_4 from mif where mif.test_account = 'Y' and bank_number = ?" ;
		ResultSet rs = null;
		formData = new MultivaluedMapImpl();
		try{
			
			com.mes.net.MesNetTools.disableSSLCertificateValidation();
			client = Client.create();
			wr = client.resource("https://sandbox.smartscan.controlscan.com/api/boarding/setup");
	        connect(true);
			ps = con.prepareStatement(sqlText);
			ps.setInt(1, bank);
			rs = ps.executeQuery();
			while(rs.next()){
				 formData.add("api_username", "MeS_sandbox_3"); 
		         formData.add("api_password", "jwefuDE834");
		         formData.add("merchant_id",rs.getString("merchant_number"));
		         formData.add("first_name",rs.getString("NAME1_LINE_1").split(" ", 2)[0]);
		         formData.add("last_name",rs.getString("NAME1_LINE_1").split(" ", 2)[1]);
		         formData.add("phone",rs.getString("PHONE_1"));
		         formData.add("dba_name",rs.getString("DBA_NAME"));
		         formData.add("dba_address",rs.getString("ADDR1_LINE_1"));
		         formData.add("dba_city",rs.getString("CITY1_LINE_4"));
		         formData.add("dba_state",rs.getString("STATE1_LINE_4"));
		         formData.add("dba_zipcode",rs.getString("ZIP1_LINE_4"));
		         formData.add("products[]", "[{\"quantity\":\"1\",\"id\":\"a0AW00000072sWPMAY\"}]");
  		         cr = wr.accept(MediaType.APPLICATION_JSON).type("application/x-www-form-urlencoded").post(ClientResponse.class, formData);
  		         String output = cr.getEntity(String.class);
  		         System.out.println("response:" + output);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		} finally{
			try{ ps.close(); rs.close(); } catch( Exception e ) {}
  	      cleanUp();
		}
		
	}
	
	public static void main(String args[]){
		AddMerchant a = new AddMerchant();
		a.addFromMIF(3943);
		// [{"quantity":"1","id":"a0AW00000072sWPMAY"}]
		//String ac = "[{\"quantity\":\"1\",\"id\":\"a0AW00000072sWPMAY\"}]";
		//String ab = "[{\"quantity\":\"1\",";
		System.exit(0);		
	}

}
