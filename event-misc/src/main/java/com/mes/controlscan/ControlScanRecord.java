package com.mes.controlscan;

import java.sql.Date;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

public class ControlScanRecord {
    static Logger log = Logger.getLogger(ControlScanRecord.class);
    protected String merchant_name;
    protected long   controlscan_id;
    protected String agent_name;
    protected long merchant_number;
    protected String submids;
    protected String contact_name;
    protected String contact_email;
    protected String phone;
    protected String address;
    protected String city;
    protected String state;
    protected String zipcode;
    protected Date   creation_date; 
    protected Date   cust_signup_date;
    protected Date   sua_date;
    protected String saq_type;
    protected Date   pci_compliant_date; 
    protected Date   compliant_saq_date; 
    protected Date   compliant_scan_date;
    protected String scan_status;
    protected int    hosts_supplied;
    protected String offline_compliance;
    protected int    scan_nr_compliance; 
    protected Date   scan_exp_date; 
    protected String pci_compliant;
    protected String saq_status;
    protected Date   mes_scan_date;

    public ControlScanRecord(JSONObject jobj) {
        
        try{
            this.merchant_name         = String.valueOf(jobj.get("merchant_name")); 
            this.controlscan_id        = Long.valueOf((String.valueOf(jobj.get("id"))));
            this.agent_name            = String.valueOf(jobj.get("agent_name"));
            if(String.valueOf(jobj.get("mid")).length()>16)
                this.merchant_number = -1;
            else
                this.merchant_number   = Long.valueOf(String.valueOf(jobj.get("mid")));
            this.submids               = String.valueOf(jobj.get("submids"));
            this.contact_name          = String.valueOf(jobj.get("contact_name"));
            this.contact_email         = String.valueOf(jobj.get("contact_email"));
            this.phone                 = String.valueOf(jobj.get("phone")).trim();
            this.address               = String.valueOf(jobj.get("address")).trim();
            if(address.length()>50)
                this.address = address.substring(0, 50);
            this.city                  = String.valueOf(jobj.get("city"));
            this.state                 = String.valueOf(jobj.get("state"));
            this.zipcode               = String.valueOf(jobj.get("zipcode")).trim();
            this.creation_date         = Date.valueOf((String.valueOf(jobj.get("creation_date"))));
            this.cust_signup_date      = Date.valueOf((String.valueOf(jobj.get("customer_sign_up_date"))));            
            if(!jobj.get("sua_date").equals("") )
                this.sua_date          = Date.valueOf((String.valueOf(jobj.get("sua_date"))));
            this.saq_type              = String.valueOf(jobj.get("saq_type")).trim();
            if(!jobj.get("overall_pci_compliant_date").equals("") )
                this.pci_compliant_date = Date.valueOf((String.valueOf(jobj.get("overall_pci_compliant_date"))).substring(0, 10));
            if(!jobj.get("compliant_saq_date").equals("") )
                this.compliant_saq_date  = Date.valueOf((String.valueOf(jobj.get("compliant_saq_date"))).substring(0, 10));
            if(!jobj.get("compliant_scan_date").equals("") )
                this.compliant_scan_date = Date.valueOf((String.valueOf(jobj.get("compliant_scan_date"))));
            this.scan_status           = String.valueOf(jobj.get("scan_status"));
            this.hosts_supplied        = Integer.valueOf((String.valueOf(jobj.get("hosts_supplied"))));
            this.offline_compliance    = String.valueOf(jobj.get("offline_compliance"));
            this.scan_nr_compliance    = Integer.valueOf((String.valueOf(jobj.get("scan_not_required_for_compliance"))));
            if(jobj.containsKey("scan_expiration_date")){
                this.scan_exp_date     = Date.valueOf((String.valueOf(jobj.get("scan_expiration_date")))); 
            } 
            this.pci_compliant         = String.valueOf(jobj.get("overall_pci_compliant"));
            this.saq_status            = String.valueOf(jobj.get("saq_status")); 
        } catch (Exception e){
            log.debug(e.getMessage());
        }
    }
}
