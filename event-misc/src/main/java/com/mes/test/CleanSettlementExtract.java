/*@lineinfo:filename=CleanSettlementExtract*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.test;

import com.mes.database.SQLJConnectionBase;

public class CleanSettlementExtract extends SQLJConnectionBase
{
  public CleanSettlementExtract( )
  {
  }
  
  public void cleanProdExtract( String loadFilename )
  {
    long          loadFileId        = 0L;
    String[]      processTables     = { 
                                        "amex_settlement_process",
                                        "discover_settlement_process",
                                      };
    String[]      reportTables      = { 
                                        "daily_detail_file_dt",
                                        "daily_detail_file_ext_dt",
                                      };
    String[]      settlementTables  = { 
                                        "visa_settlement",
                                        "mc_settlement",
                                        "amex_settlement",
                                        "discover_settlement",
                                      };
    String[]      achTables         = { 
                                        "ach_trident_statement",
                                      };
    String[]      summaryTables     = { 
    									"daily_detail_ic_summary",
    									"daily_detail_file_ext_summary",
    									"daily_detail_disc_summary",
    									"daily_detail_file_summary"
          							  };
                                      
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:55^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:loadFilename)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1 )\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.test.CleanSettlementExtract",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:60^7*/
      
      for( int i = 0; i < settlementTables.length; ++i )
      {
        System.out.print("Cleaning prod rejects from 'reject_record' ");
        /*@lineinfo:generated-code*//*@lineinfo:65^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    reject_record   rr
//            where   rr.rec_id in 
//                    (
//                      select  s.rec_id
//                      from    :settlementTables[i]  s
//                      where   s.load_file_id = :loadFileId
//                              and s.test_flag = 'N'
//                              and s.output_filename is null
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4946 = settlementTables[i];
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("delete\n          from    reject_record   rr\n          where   rr.rec_id in \n                  (\n                    select  s.rec_id\n                    from     ");
   __sjT_sb.append(__sJT_4946);
   __sjT_sb.append("   s\n                    where   s.load_file_id =  ? \n                            and s.test_flag = 'N'\n                            and s.output_filename is null\n                  )");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "1com.mes.test.CleanSettlementExtract:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:77^9*/
        System.out.println("(" + Ctx.getExecutionContext().getUpdateCount() + ")");
        
        System.out.print("Cleaning prod rejects from 'q_data' ");
        /*@lineinfo:generated-code*//*@lineinfo:81^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    q_data    qd
//            where   qd.item_type = 44   -- package rejects
//                    and qd.id in 
//                    (
//                      select  s.rec_id
//                      from    :settlementTables[i]  s
//                      where   s.load_file_id = :loadFileId
//                              and s.test_flag = 'N'
//                              and s.output_filename is null
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4947 = settlementTables[i];
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("delete\n          from    q_data    qd\n          where   qd.item_type = 44   -- package rejects\n                  and qd.id in \n                  (\n                    select  s.rec_id\n                    from     ");
   __sjT_sb.append(__sJT_4947);
   __sjT_sb.append("   s\n                    where   s.load_file_id =  ? \n                            and s.test_flag = 'N'\n                            and s.output_filename is null\n                  )");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "2com.mes.test.CleanSettlementExtract:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:94^9*/
        System.out.println("(" + Ctx.getExecutionContext().getUpdateCount() + ")");
      
      
        System.out.print("Cleaning prod transactions in '" + settlementTables[i] + "_line_items' ");
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:101^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    :settlementTables[i] + "_line_items"   li
//              where   li.rec_id in 
//                      (
//                        select  s.rec_id
//                        from    :settlementTables[i]  s
//                        where   s.load_file_id = :loadFileId
//                                and s.test_flag = 'N'
//                                and s.output_filename is null
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4948 = settlementTables[i] + "_line_items";
 String __sJT_4949 = settlementTables[i];
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("delete\n            from     ");
   __sjT_sb.append(__sJT_4948);
   __sjT_sb.append("    li\n            where   li.rec_id in \n                    (\n                      select  s.rec_id\n                      from     ");
   __sjT_sb.append(__sJT_4949);
   __sjT_sb.append("   s\n                      where   s.load_file_id =  ? \n                              and s.test_flag = 'N'\n                              and s.output_filename is null\n                    )");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "3com.mes.test.CleanSettlementExtract:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:113^11*/
          System.out.println("(" + Ctx.getExecutionContext().getUpdateCount() + ")");
        }
        catch( Exception ee )
        {
          System.out.println("- No line items table");
        }
      
        System.out.print("Cleaning prod transactions in '" + settlementTables[i] + "' ");
        /*@lineinfo:generated-code*//*@lineinfo:122^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    :settlementTables[i]
//            where   load_file_id = :loadFileId
//                    and output_filename is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4950 = settlementTables[i];
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("delete\n          from     ");
   __sjT_sb.append(__sJT_4950);
   __sjT_sb.append(" \n          where   load_file_id =  ? \n                  and output_filename is null");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "4com.mes.test.CleanSettlementExtract:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:128^9*/
        System.out.println("(" + Ctx.getExecutionContext().getUpdateCount() + ")");
      }
      
      for( int i = 0; i < reportTables.length; ++i )
      {
        System.out.print("Cleaning '" + reportTables[i] + "' ");
        
        // cdf and 256 files are only loaded into the DT table
        if (  !reportTables[i].equals("daily_detail_file_dt") &&
              (loadFilename.startsWith("cdf") || loadFilename.startsWith("256")) )
        {
          System.out.println("skipped");
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:144^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    :reportTables[i]
//              where   load_filename = :loadFilename
//                      --and load_file_id = :loadFileId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4951 = reportTables[i];
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("delete\n            from     ");
   __sjT_sb.append(__sJT_4951);
   __sjT_sb.append(" \n            where   load_filename =  ? \n                    --and load_file_id = :loadFileId");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "5com.mes.test.CleanSettlementExtract:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:150^11*/
          System.out.println("(" + Ctx.getExecutionContext().getUpdateCount() + ")");
        }          
      }
      
      for( int i = 0; i < achTables.length; ++i )
      {
        System.out.print("Cleaning '" + achTables[i] + "' ");
        /*@lineinfo:generated-code*//*@lineinfo:158^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    :achTables[i]
//            where   load_file_id = :loadFileId
//                    and ach_sequence = 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4952 = achTables[i];
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("delete\n          from     ");
   __sjT_sb.append(__sJT_4952);
   __sjT_sb.append(" \n          where   load_file_id =  ? \n                  and ach_sequence = 0");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "6com.mes.test.CleanSettlementExtract:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:164^9*/
        System.out.println("(" + Ctx.getExecutionContext().getUpdateCount() + ")");
      }
      
      for( int i = 0; i < processTables.length; ++i )
      {
        System.out.print("Cleaning '" + processTables[i] + "' ");
        /*@lineinfo:generated-code*//*@lineinfo:171^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    :processTables[i]
//            where   load_filename = :loadFilename
//                    and nvl(process_sequence,0) = 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4953 = processTables[i];
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("delete\n          from     ");
   __sjT_sb.append(__sJT_4953);
   __sjT_sb.append(" \n          where   load_filename =  ? \n                  and nvl(process_sequence,0) = 0");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "7com.mes.test.CleanSettlementExtract:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:177^9*/
        System.out.println("(" + Ctx.getExecutionContext().getUpdateCount() + ")");
      }
      
      for( int i = 0; i < summaryTables.length; ++i )
      {
        System.out.print("Cleaning '" + summaryTables[i] + "' ");
        /*@lineinfo:generated-code*//*@lineinfo:184^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    :summaryTables[i]
//            where   load_file_id = :loadFileId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4954 = summaryTables[i];
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("delete\n          from     ");
   __sjT_sb.append(__sJT_4954);
   __sjT_sb.append(" \n          where   load_file_id =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "8com.mes.test.CleanSettlementExtract:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:189^9*/
        System.out.println("(" + Ctx.getExecutionContext().getUpdateCount() + ")");
      }
    }
    catch( Exception e )
    {
      logEntry("cleanProdExtract(" + loadFilename + ")",e.toString());
      System.out.println(e.toString());
    }
    finally
    {
    }
  }
  
  public void cleanTestExtract( String loadFilename )
  {
    long          loadFileId        = 0L;
    String[]      settlementTables  = { 
                                        "visa_settlement",
                                        "mc_settlement",
                                        "amex_settlement",
                                        "discover_settlement",
                                        "modbii_settlement",
                                      };

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:216^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:loadFilename)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1 )\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.test.CleanSettlementExtract",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:221^7*/
      
      for( int i = 0; i < settlementTables.length; ++i )
      {
        System.out.print("Cleaning test transactions in '" + settlementTables[i] + "_line_items' ");
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:228^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    :settlementTables[i] + "_line_items"   li
//              where   li.rec_id in 
//                      (
//                        select  s.rec_id
//                        from    :settlementTables[i]  s
//                        where   s.load_file_id = :loadFileId
//                                and s.test_flag = 'Y'
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4955 = settlementTables[i] + "_line_items";
 String __sJT_4956 = settlementTables[i];
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("delete\n            from     ");
   __sjT_sb.append(__sJT_4955);
   __sjT_sb.append("    li\n            where   li.rec_id in \n                    (\n                      select  s.rec_id\n                      from     ");
   __sjT_sb.append(__sJT_4956);
   __sjT_sb.append("   s\n                      where   s.load_file_id =  ? \n                              and s.test_flag = 'Y'\n                    )");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "10com.mes.test.CleanSettlementExtract:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:239^11*/
          System.out.println("(" + Ctx.getExecutionContext().getUpdateCount() + ")");
        }
        catch( Exception ee )
        {
          System.out.println("- No line items table");
        }
        
        
        System.out.print("Cleaning test transactions in '" + settlementTables[i] + "' ");
        /*@lineinfo:generated-code*//*@lineinfo:249^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    :settlementTables[i]
//            where   load_file_id = :loadFileId
//                    and test_flag = 'Y'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4957 = settlementTables[i];
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("delete\n          from     ");
   __sjT_sb.append(__sJT_4957);
   __sjT_sb.append(" \n          where   load_file_id =  ? \n                  and test_flag = 'Y'");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "11com.mes.test.CleanSettlementExtract:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:255^9*/
        System.out.println("(" + Ctx.getExecutionContext().getUpdateCount() + ")");
      }
    }
    catch( Exception e )
    {
      logEntry("cleanTestExtract(" + loadFilename + ")",e.toString());
      System.out.println(e.toString());
    }
    finally
    {
    }
  }

  public static void main( String[] args )
  {
    CleanSettlementExtract      bean      = null;
    
    SQLJConnectionBase.initStandalone("DEBUG");
    
    try
    {
      bean = new CleanSettlementExtract();
      bean.connect(true);
      
      
      if ("cleanProdExtract".equals(args[0]))
      {
        bean.cleanProdExtract(args[1]);
      }
      else if ("cleanTestExtract".equals(args[0]))
      {
        bean.cleanTestExtract(args[1]);
      }
    }
    catch( Exception e )
    {
    }
    finally
    {
      try{ bean.cleanUp(); }catch( Exception e ) { }
    }
  }  
}/*@lineinfo:generated-code*/