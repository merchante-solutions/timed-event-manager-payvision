/*@lineinfo:filename=VsServiceCallBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/VsServiceCallBean.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.maintenance;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Time;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class VsServiceCallBean extends SQLJConnectionBase
{
  public static Vector    callTypes         = null;
  public static Vector    callDescs         = null;
  public static Vector    statusTypes       = null;
  public static Vector    statusDescs       = null;

  /*
  ** Member variables for CallTrackingBean
  */
  private String      merchantNumber          = "";
  private long        appSeqNum               = -1;
  private String      merchantName            = "";
  private String      callDate                = "";
  private String      callTime                = "";
  private int         callType                = 0;
  private String      callTypeDescription     = "";
  private String      otherDescription        = "";
  private String      callBack                = "N";
  private String      callNotes               = "";

  private String      loginName               = "";
  private String      sequence                = "";
  private String      returnURL               = "";
  private String      closeNotes              = "";
  private String      closeLoginName          = "";
  private int         status                  = 0;
  private String      callStatusDescription   = "";
  private String      closeDate               = "";
  private String      closeTime               = "";

  private boolean     submitted               = false;

  public VsServiceCallBean()
  {
    setAutoCommit(true);

    if(callTypes == null)
    {
      fillVectors();
    }
  }
  
  public void updateCall(String userName)
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:91^7*/

//  ************************************************************
//  #sql [Ctx] { update  vs_service_calls
//          set     close_notes       = :this.closeNotes,
//                  close_login_name  = :userName,
//                  status            = :mesConstants.CALL_STATUS_CLOSED,
//                  close_date        = sysdate
//          where   sequence          = :this.sequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  vs_service_calls\n        set     close_notes       =  :1 ,\n                close_login_name  =  :2 ,\n                status            =  :3 ,\n                close_date        = sysdate\n        where   sequence          =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.maintenance.VsServiceCallBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.closeNotes);
   __sJT_st.setString(2,userName);
   __sJT_st.setInt(3,mesConstants.CALL_STATUS_CLOSED);
   __sJT_st.setString(4,this.sequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:99^7*/
    }
    catch(Exception e)
    {
      logEntry("updateCall(" + userName + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void submitNewCall(String userName)
  {
    int newStatus;
    try
    {
      if(callType == mesConstants.CALL_TYPE_INVALID || callType == mesConstants.CALL_TYPE_OTHER)
      {
        callType = mesConstants.CALL_TYPE_OTHER;
        if(otherDescription == null || otherDescription.equals(""))
        {
          otherDescription = "Other";
        }
      }

      if(callBack != null && callBack.equals("Y"))
      {
        newStatus = mesConstants.CALL_STATUS_OPEN;
      }
      else
      {
        newStatus = mesConstants.CALL_STATUS_CLOSED;
      }

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:136^7*/

//  ************************************************************
//  #sql [Ctx] { insert into vs_service_calls
//                      (
//                        app_seq_num,
//                        merchant_number,
//                        call_date,
//                        type,
//                        other_description,
//                        login_name,
//                        call_back,
//                        status,
//                        notes
//                      )
//                      values
//                      (
//                        :appSeqNum,
//                        :merchantNumber,
//                        sysdate,
//                        :callType,
//                        :otherDescription,
//                        :userName,
//                        :callBack,
//                        :newStatus,
//                        :callNotes
//                      )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into vs_service_calls\n                    (\n                      app_seq_num,\n                      merchant_number,\n                      call_date,\n                      type,\n                      other_description,\n                      login_name,\n                      call_back,\n                      status,\n                      notes\n                    )\n                    values\n                    (\n                       :1 ,\n                       :2 ,\n                      sysdate,\n                       :3 ,\n                       :4 ,\n                       :5 ,\n                       :6 ,\n                       :7 ,\n                       :8 \n                    )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.maintenance.VsServiceCallBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,merchantNumber);
   __sJT_st.setInt(3,callType);
   __sJT_st.setString(4,otherDescription);
   __sJT_st.setString(5,userName);
   __sJT_st.setString(6,callBack);
   __sJT_st.setInt(7,newStatus);
   __sJT_st.setString(8,callNotes);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:162^7*/
    }
    catch(Exception e)
    {
      logEntry("submitData(" + userName + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void loadData()
  {
    ResultSetIterator     it      = null;
    try
    {
      connect();
      /*@lineinfo:generated-code*//*@lineinfo:180^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sc.merchant_number      merchant_number,
//                  sc.call_date            call_date,
//                  sc.type                 call_type,
//                  sc.other_description    other_description,
//                  sc.login_name           login_name,
//                  sc.call_back            call_back,
//                  sc.status               call_status,
//                  sc.notes                notes,
//                  sc.close_date           close_date,
//                  sc.close_notes          close_notes,
//                  sc.close_login_name     close_login_name,
//                  sct.description         type_description,
//                  scs.description         status_description,
//                  m.merch_business_name   dba_name,
//                  m.app_seq_num           app_seq_num
//  
//          from    vs_service_calls           sc,
//                  vs_service_call_types      sct,
//                  vs_service_call_statuses   scs,
//                  merchant                   m
//  
//          where   sc.sequence = :this.sequence and
//                  sc.type = sct.type and
//                  sc.status = scs.status and
//                  m.app_seq_num = sc.app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sc.merchant_number      merchant_number,\n                sc.call_date            call_date,\n                sc.type                 call_type,\n                sc.other_description    other_description,\n                sc.login_name           login_name,\n                sc.call_back            call_back,\n                sc.status               call_status,\n                sc.notes                notes,\n                sc.close_date           close_date,\n                sc.close_notes          close_notes,\n                sc.close_login_name     close_login_name,\n                sct.description         type_description,\n                scs.description         status_description,\n                m.merch_business_name   dba_name,\n                m.app_seq_num           app_seq_num\n\n        from    vs_service_calls           sc,\n                vs_service_call_types      sct,\n                vs_service_call_statuses   scs,\n                merchant                   m\n\n        where   sc.sequence =  :1  and\n                sc.type = sct.type and\n                sc.status = scs.status and\n                m.app_seq_num = sc.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.VsServiceCallBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.sequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.maintenance.VsServiceCallBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:207^7*/
      
      ResultSet rs = it.getResultSet();
      
      if(rs.next())
      {
        setAppSeqNum(rs.getLong("app_seq_num"));
        setMerchantNumber(rs.getString("merchant_number"));
        if(rs.getDate("call_date") != null)
        {
          setCallDate(rs.getDate("call_date"));
          setCallTime(rs.getTime("call_date"));
        }
        setCallType(rs.getInt("call_type"));
        setOtherDescription(rs.getString("other_description"));
        setLoginName(rs.getString("login_name"));
        setCallBack(rs.getString("call_back"));
        setStatus(rs.getInt("call_status"));
        setCallNotes(rs.getString("notes"));
        if(rs.getDate("close_date") != null)
        {
          setCloseDate(rs.getDate("close_date"));
          setCloseTime(rs.getTime("close_date"));
        }
        setCloseNotes(rs.getString("close_notes"));
        setCloseLoginName(rs.getString("close_login_name"));
        
        if(getCallType() == mesConstants.CALL_TYPE_OTHER)
        {
          setCallTypeDescription(getOtherDescription());
        }
        else
        {
          setCallTypeDescription(rs.getString("type_description"));
        }
        
        setCallStatusDescription(rs.getString("status_description"));
        
        setMerchantName(rs.getString("dba_name"));
      }
    }
    catch(Exception e)
    {
      logEntry("loadData(" + this.sequence + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void fillVectors()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;

    // fill the type and description vectors
    try
    {
      connect();

      // callTypes, callDescs
      callTypes = new Vector();
      callDescs = new Vector();

      /*@lineinfo:generated-code*//*@lineinfo:272^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    type,
//                    description
//          from      vs_service_call_types
//          where     type > 0
//          order by  type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    type,\n                  description\n        from      vs_service_call_types\n        where     type > 0\n        order by  type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.VsServiceCallBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.maintenance.VsServiceCallBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:279^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        callTypes.add(rs.getString("type"));
        callDescs.add(rs.getString("description"));
      }

      it.close();

      // statusTypes, statusDescs
      statusTypes = new Vector();
      statusDescs = new Vector();

      /*@lineinfo:generated-code*//*@lineinfo:295^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    status,
//                    description
//          from      vs_service_call_statuses
//          where     status > 0
//          order by  status
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    status,\n                  description\n        from      vs_service_call_statuses\n        where     status > 0\n        order by  status";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.maintenance.VsServiceCallBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.maintenance.VsServiceCallBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:302^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        statusTypes.add(rs.getString("status"));
        statusDescs.add(rs.getString("description"));
      }

      it.close();
    }
    catch(Exception e)
    {
      logEntry("fillVectors(callTypes)", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public Vector getCallTypes()
  {
    return this.callTypes;
  }
  public Vector getCallDescs()
  {
    return this.callDescs;
  }
  public void setMerchant(String merchantNumber)
  {
    this.merchantNumber = merchantNumber;
  }
  public void setAppSeqNum(String appSeqNum)
  {
    try
    {
      setAppSeqNum(Long.parseLong(appSeqNum));
    }
    catch(Exception e)
    {
      logEntry("setAppSeqNum(" + appSeqNum + ")", e.toString());
    }
  }
  public void setAppSeqNum(long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
  }
  public long getAppSeqNum()
  {
    return this.appSeqNum;
  }

  public void setMerchantNumber(String merchantNumber)
  {
    this.merchantNumber = merchantNumber;
  }
  public String getMerchantNumber()
  {
    return this.merchantNumber;
  }
  
  public void setCallDate(Date callDate)
  {
    this.callDate = DateTimeFormatter.getFormattedDate(callDate, "MM/dd/yy");
  }
  public String getCallDate()
  {
    return this.callDate;
  }
  
  public void setCallTime(Time callTime)
  {
    this.callTime = DateTimeFormatter.getFormattedDate(callTime, "HH:mm:ss");
  }
  public String getCallTime()
  {
    return this.callTime;
  }
  
  public void setCallType(String callType)
  {
    try
    {
      this.callType = Integer.parseInt(callType);
    }
    catch(Exception e)
    {
      logEntry("setCallType(" + callType + ")", e.toString());
    }
  }
  
  public void setCallType(int callType)
  {
    this.callType = callType;
  }
  public int getCallType()
  {
    return this.callType;
  }
  
  public void setOtherDescription(String otherDescription)
  {
    this.otherDescription = otherDescription;
  }
  public String getOtherDescription()
  {
    return this.otherDescription;
  }
  
  public void setLoginName(String loginName)
  {
    this.loginName = loginName;
  }
  public String getLoginName()
  {
    return this.loginName;
  }
  
  public void setCallBack(String callBack)
  {
    this.callBack = callBack;
  }
  public String getCallBack()
  {
    return this.callBack;
  }
  
  public void setStatus(String status)
  {
    try
    {
      this.status = Integer.parseInt(status);
    }
    catch(Exception e)
    {
      logEntry("setStatus(" + callType + ")", e.toString());
    }
  }
  public void setStatus(int status)
  {
    this.status = status;
  }
  public int getStatus()
  {
    return this.status;
  }
  
  public void setCallNotes(String callNotes)
  {
    if(callNotes != null)
    {
      this.callNotes = (callNotes.length() > 260) ? callNotes.substring(0, 259) : callNotes;
    }
  }
  public String getCallNotes()
  {
    return this.callNotes;
  }
  
  public void setCloseDate(Date closeDate)
  {
    this.closeDate = DateTimeFormatter.getFormattedDate(closeDate, "MM/dd/yy");
  }
  public String getCloseDate()
  {
    return this.closeDate;
  }
  
  public void setCloseTime(Time closeTime)
  {
    this.closeTime = DateTimeFormatter.getFormattedDate(closeTime, "HH:mm:ss");
  }
  public String getCloseTime()
  {
    return this.closeTime;
  }
  
  public void setCloseNotes(String closeNotes)
  {
    this.closeNotes = closeNotes;
  }
  public String getCloseNotes()
  {
    return this.closeNotes;
  }
  
  public void setCloseLoginName(String closeLoginName)
  {
    this.closeLoginName = closeLoginName;
  }
  public String getCloseLoginName()
  {
    return this.closeLoginName;
  }
  
  public void setCallTypeDescription(String callTypeDescription)
  {
    this.callTypeDescription = callTypeDescription;
  }
  public String getCallTypeDescription()
  {
    return this.callTypeDescription;
  }
  
  public void setSubmitted(String submitted)
  {
    this.submitted = (submitted != null && submitted.equals("true"));
  }
  public boolean isSubmitted()
  {
    return this.submitted;
  }
  
  public void setSequence(String sequence)
  {
    this.sequence = sequence;
  }
  public String getSequence()
  {
    return this.sequence;
  }
  
  public void setReturnURL(String returnURL)
  {
    this.returnURL = returnURL;
  }
  public String getReturnURL()
  {
    return this.returnURL;
  }
  public void setMerchantName(String merchantName)
  {
    this.merchantName = merchantName;
  }
  public String getMerchantName()
  {
    return this.merchantName;
  }
  public void setCallStatusDescription(String callStatusDescription)
  {
    this.callStatusDescription = callStatusDescription;
  }
  public String getCallStatusDescription()
  {
    return this.callStatusDescription;
  }
}/*@lineinfo:generated-code*/