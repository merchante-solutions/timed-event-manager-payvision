/*@lineinfo:filename=CallBacks*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/CallBacks.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/10/03 5:03p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.maintenance;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.RadioButtonField;
import com.mes.support.DateTimeFormatter;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class CallBacks extends FieldBean
{
  static Logger log = Logger.getLogger(CallBacks.class);
  
  public final static int       CALLBACKS_OPEN      = 1;
  public final static int       CALLBACKS_CLOSED    = 2;
  public final static int       CALLBACKS_BOTH      = 3;
  
  
  public boolean                submitted           = false;
  public Vector                 lookupResults       = new Vector();
  
  protected static final String[][] callTypeList = 
  {
    { "Open Call Backs",    "1" },
    { "Closed Call Backs",  "2" },
    { "Both",               "3" }
  };
  
  public void logEntry(String method, String message)
  {
    super.logEntry(method, message);
    
    // log4j this bad boy
    log.debug(method + ": " + message);
  }
  
  public CallBacks()
  {
    setAutoCommit(true);
  }
  
  public void setProperties(HttpServletRequest request, UserBean user)
  {
    try
    {
      connect();
      
      this.user = user;
      register(user.getLoginName(), request);
      
      addFields();
      
      setFields(request);
      
      // process submission
      if(getData("submitSearch").equals("Search"))
      {
        if(isValid())
        {
          // do the search
          doSearch();
          submitted = true;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void addFields()
  {
    try
    {
      fields.add(new Field("lookupValue", 40, 20, true));
      fields.add(new RadioButtonField("whichCalls", callTypeList, 0, false, "Required"));
      fields.add(new DateStringField("callDateBegin", "From", true, false));
      fields.add(new DateStringField("callDateEnd", "To", true, false));
      fields.add(new ButtonField("submitSearch", "Search"));
      
      fields.addHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      logEntry("addFields()", e.toString());
    }
  }
  
  private void doSearch()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    int callBack1 = CALLBACKS_OPEN;
    int callBack2 = CALLBACKS_CLOSED;
    
    try
    {
      if(getData("callDateBegin").equals(""))
      {
        setData("callDateBegin", "01/01/2000");
      }
      
      if(getData("callDateEnd").equals(""))
      {
        setData("callDateEnd", DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(), "MM/dd/yyyy"));
      }
      
      // determine call type
      switch(Integer.parseInt(getData("whichCalls")))
      {
        case CALLBACKS_OPEN:
          callBack1 = callBack2 = CALLBACKS_OPEN;
          break;
        case CALLBACKS_CLOSED:
          callBack1 = callBack2 = CALLBACKS_CLOSED;
          break;
      }
      
      java.sql.Date fromDate = new java.sql.Date(((DateStringField)(fields.getField("callDateBegin"))).getUtilDate().getTime());
      java.sql.Date toDate   = new java.sql.Date(((DateStringField)(fields.getField("callDateEnd"))).getUtilDate().getTime());
      
      if(getData("lookupValue").equals(""))
      {
        /*@lineinfo:generated-code*//*@lineinfo:169^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sc.merchant_number    merchant_number,
//                    sc.call_date          call_date,
//                    sc.type               type,
//                    sc.other_description  other_description,
//                    sc.login_name         login_name,
//                    sc.call_back          call_back,
//                    sc.status             status,
//                    sc.notes              notes,
//                    sc.close_notes        close_notes,
//                    sc.sequence           sequence,
//                    sct.description       type_description,
//                    scs.description       status_description,
//                    m.dba_name            dba_name
//            from    service_calls         sc,
//                    service_call_types    sct,
//                    service_call_statuses scs,
//                    mif                   m,
//                    t_hierarchy           th
//            where   th.ancestor = :user.getHierarchyNode() and
//                    th.descendent = m.association_node and
//                    m.merchant_number = sc.merchant_number and
//                    (
//                      sc.status = :callBack1 or
//                      sc.status = :callBack2
//                    ) and
//                    (sc.status != 1 or call_back_date is null or call_back_date <= sysdate) and
//                    trunc(sc.call_date) between :fromDate and :toDate and
//                    sct.type = sc.type and
//                    scs.status = sc.status
//            order by sc.call_date desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_817 = user.getHierarchyNode();
  try {
   String theSqlTS = "select  sc.merchant_number    merchant_number,\n                  sc.call_date          call_date,\n                  sc.type               type,\n                  sc.other_description  other_description,\n                  sc.login_name         login_name,\n                  sc.call_back          call_back,\n                  sc.status             status,\n                  sc.notes              notes,\n                  sc.close_notes        close_notes,\n                  sc.sequence           sequence,\n                  sct.description       type_description,\n                  scs.description       status_description,\n                  m.dba_name            dba_name\n          from    service_calls         sc,\n                  service_call_types    sct,\n                  service_call_statuses scs,\n                  mif                   m,\n                  t_hierarchy           th\n          where   th.ancestor =  :1  and\n                  th.descendent = m.association_node and\n                  m.merchant_number = sc.merchant_number and\n                  (\n                    sc.status =  :2  or\n                    sc.status =  :3 \n                  ) and\n                  (sc.status != 1 or call_back_date is null or call_back_date <= sysdate) and\n                  trunc(sc.call_date) between  :4  and  :5  and\n                  sct.type = sc.type and\n                  scs.status = sc.status\n          order by sc.call_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.CallBacks",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_817);
   __sJT_st.setInt(2,callBack1);
   __sJT_st.setInt(3,callBack2);
   __sJT_st.setDate(4,fromDate);
   __sJT_st.setDate(5,toDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.CallBacks",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:201^9*/
      }
      else
      {
        long longLookup = 0L;
        
        try
        {
          longLookup = Long.parseLong(getData("lookupValue"));
        }
        catch(Exception parseException)
        {
          longLookup = 0L;
        }
        
        String stringLookup = "%" + getData("lookupValue").toUpperCase() + "%";
        
        /*@lineinfo:generated-code*//*@lineinfo:218^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sc.merchant_number    merchant_number,
//                    sc.call_date          call_date,
//                    sc.type               type,
//                    sc.other_description  other_description,
//                    sc.login_name         login_name,
//                    sc.call_back          call_back,
//                    sc.status             status,
//                    sc.notes              notes,
//                    sc.close_notes        close_notes,
//                    sc.sequence           sequence,
//                    sct.description       type_description,
//                    scs.description       status_description,
//                    m.dba_name            dba_name
//            from    service_calls         sc,
//                    service_call_types    sct,
//                    service_call_statuses scs,
//                    mif                   m,
//                    t_hierarchy           th
//            where   th.ancestor = :user.getHierarchyNode() and
//                    th.descendent = m.association_node and
//                    m.merchant_number = sc.merchant_number and
//                    (
//                      sc.status = :callBack1 or
//                      sc.status = :callBack2
//                    ) and
//                    (
//                      upper(sc.login_name)  like :stringLookup or
//                      upper(m.dba_name)     like :stringLookup or
//                      sc.merchant_number    = :longLookup
//                    ) and
//                    (sc.status != 1 or call_back_date is null or call_back_date <= sysdate) and
//                    trunc(sc.call_date) between :fromDate and :toDate and
//                    sc.type = sct.type and
//                    sc.status = scs.status
//            order by sc.call_date desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_818 = user.getHierarchyNode();
  try {
   String theSqlTS = "select  sc.merchant_number    merchant_number,\n                  sc.call_date          call_date,\n                  sc.type               type,\n                  sc.other_description  other_description,\n                  sc.login_name         login_name,\n                  sc.call_back          call_back,\n                  sc.status             status,\n                  sc.notes              notes,\n                  sc.close_notes        close_notes,\n                  sc.sequence           sequence,\n                  sct.description       type_description,\n                  scs.description       status_description,\n                  m.dba_name            dba_name\n          from    service_calls         sc,\n                  service_call_types    sct,\n                  service_call_statuses scs,\n                  mif                   m,\n                  t_hierarchy           th\n          where   th.ancestor =  :1  and\n                  th.descendent = m.association_node and\n                  m.merchant_number = sc.merchant_number and\n                  (\n                    sc.status =  :2  or\n                    sc.status =  :3 \n                  ) and\n                  (\n                    upper(sc.login_name)  like  :4  or\n                    upper(m.dba_name)     like  :5  or\n                    sc.merchant_number    =  :6 \n                  ) and\n                  (sc.status != 1 or call_back_date is null or call_back_date <= sysdate) and\n                  trunc(sc.call_date) between  :7  and  :8  and\n                  sc.type = sct.type and\n                  sc.status = scs.status\n          order by sc.call_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.CallBacks",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_818);
   __sJT_st.setInt(2,callBack1);
   __sJT_st.setInt(3,callBack2);
   __sJT_st.setString(4,stringLookup);
   __sJT_st.setString(5,stringLookup);
   __sJT_st.setLong(6,longLookup);
   __sJT_st.setDate(7,fromDate);
   __sJT_st.setDate(8,toDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.CallBacks",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:255^9*/
      }
      
      rs = it.getResultSet();
      
      DateFormat  df = new SimpleDateFormat("MM/dd/yy");
      DateFormat  tf = new SimpleDateFormat("HH:mm:ss");
      String      dateString    = "";
      String      timeString    = "";
      
      while(rs.next())
      {
        dateString = df.format(rs.getDate("call_date"));
        timeString = tf.format(rs.getTime("call_date"));
          
        lookupResults.add(new CallBackData(dateString,
                                           timeString,
                                           rs.getString("merchant_number"),
                                           rs.getString("dba_name"),
                                           rs.getString("type_description"),
                                           rs.getString("notes"),
                                           rs.getString("close_notes"),
                                           rs.getString("login_name"),
                                           rs.getString("status_description"),
                                           rs.getString("sequence")));
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("doSearch()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  public Vector getLookupResults()
  {
    return this.lookupResults;
  }
  
  public class CallBackData
  {
    private String    callDate          = "";
    private String    callTime          = "";
    private String    merchantNumber    = "";
    private String    merchantName      = "";
    private String    callType          = "";
    private String    callNotes         = "";
    private String    closeNotes        = "";
    private String    loginName         = "";
    private String    status            = "";
    private String    sequence          = "";
    private String    allNotes          = "";
    
    public CallBackData()
    {
    }
    
    public CallBackData(String callDate,
                        String callTime,
                        String merchantNumber,
                        String merchantName,
                        String callType,
                        String callNotes,
                        String closeNotes,
                        String loginName,
                        String status,
                        String sequence)
    {
      setCallDate(callDate);
      setCallTime(callTime);
      setMerchantNumber(merchantNumber);
      setMerchantName(merchantName);
      setCallType(callType);
      setCallNotes(callNotes);
      setCloseNotes(closeNotes);
      setLoginName(loginName);
      setStatus(status);
      setSequence(sequence);
      setAllNotes(callNotes, closeNotes);
    }
    
    public void setAllNotes(String notes, String closeNotes)
    {
      StringBuffer      result      = new StringBuffer("");
      try
      {
        result.append(notes);
        
        if(closeNotes != null && !closeNotes.equals(""))
        {
          result.append("<br></br>");
          result.append("<font color=\"blue\">");
          result.append(closeNotes);
          result.append("</font>");
        }
      }
      catch(Exception e)
      {
        logEntry("setAllNotes()", e.toString());
      }
      
      this.allNotes = result.toString();
    }
    public void setCloseNotes(String closeNotes)
    {
      this.closeNotes = closeNotes;
    }
    public String getCloseNotes()
    {
      return this.closeNotes;
    }
    public void setCallDate(String callDate)
    {
      this.callDate = callDate;
    }
    public String getCallDate()
    {
      return this.callDate;
    }
    public void setCallTime(String callTime)
    {
      this.callTime = callTime;
    }
    public String getCallTime()
    {
      return this.callTime;
    }
    public void setMerchantNumber(String merchantNumber)
    {
      this.merchantNumber = merchantNumber;
    }
    public String getMerchantNumber()
    {
      return this.merchantNumber;
    }
    public void setMerchantName(String merchantName)
    {
      this.merchantName = merchantName;
    }
    public String getMerchantName()
    {
      return this.merchantName;
    }
    public void setCallType(String callType)
    {
      this.callType = callType;
    }
    public String getCallType()
    {
      return this.callType;
    }
    public void setCallNotes(String callNotes)
    {
      this.callNotes = callNotes;
    }
    public String getCallNotes()
    {
      return this.allNotes;
    }
    public void setLoginName(String loginName)
    {
      this.loginName = loginName;
    }
    public String getLoginName()
    {
      return this.loginName;
    }
    public void setStatus(String status)
    {
      this.status = status;
    }
    public String getStatus()
    {
      return this.status;
    }
    public void setSequence(String sequence)
    {
      this.sequence = sequence;
    }
    public String getSequence()
    {
      return this.sequence;
    }
  }
}/*@lineinfo:generated-code*/