/*@lineinfo:filename=CallTrackingBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/CallTrackingBean.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 6/07/04 1:39p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.SyncLog;
import sqlj.runtime.ResultSetIterator;

public class CallTrackingBean extends SQLJConnectionBase
{
  public class MerchantCallDetail
  {
    public String   merchantNumber            = "";
    public String   callDate                  = "";
    public String   callTime                  = "";
    public String   callDescription           = "";
    public String   loginName                 = "";
    public String   callBack                  = "N";
    public String   callStatus                = "";
    public String   notes                     = "";
    public String   closeDate                 = "";
    public String   closeNotes                = "";
    public String   sequence                  = "";
    public String   allNotes                  = "";
    
    public boolean  billable                  = false;
    public boolean  clientGenerated           = false;
    public boolean  displayBold               = false;
    
    public MerchantCallDetail()
    {
    }
    
    public MerchantCallDetail(String  merchantNumber,
                              Date    callDate,
                              Time    callTime,
                              String  callDescription,
                              String  loginName,
                              String  callBack,
                              String  callStatus,
                              String  notes,
                              Date    closeDate,
                              String  closeNotes,
                              String  sequence,
                              boolean billable,
                              boolean clientGenerated,
                              boolean displayBold)
    {
      setMerchantNumber(merchantNumber);
      setCallDate(callDate);
      setCallTime(callTime);
      setCallDescription(callDescription);
      setLoginName(loginName);
      setCallBack(callBack);
      setCallStatus(callStatus);
      setNotes(notes);
      if(closeDate != null)
      {
        setCloseDate(closeDate);
      }
      setCloseNotes(closeNotes);
      setSequence(sequence);
      setAllNotes(notes, closeNotes);
      setIsBillable(billable);
      setWasClientGenerated(clientGenerated);
      setDisplayBold(displayBold);
    }

    public void setMerchantNumber(String merchantNumber)
    {
      this.merchantNumber = merchantNumber;
    }
    public String getMerchantNumber()
    {
      return this.merchantNumber;
    }
    public void setCallDate(Date callDate)
    {
      DateFormat    df      = new SimpleDateFormat("MM/dd/yy");
      
      try
      {
        this.callDate = df.format(callDate);
      }
      catch(Exception e)
      {
        SyncLog.LogEntry(this.getClass().getName() + "::setCallDate()", e.toString());
      }
    }
    public String getCallDate()
    {
      return this.callDate;
    }
    public void setCallTime(Time callTime)
    {
      DateFormat df         = new SimpleDateFormat("HH:mm:ss");
      try
      {
        this.callTime = df.format(callTime);
      }
      catch(Exception e)
      {
        SyncLog.LogEntry(this.getClass().getName() + "::setCallTime()", e.toString());
      }
    }
    public String getCallTime()
    {
      return this.callTime;
    }
    public void setCallDescription(String callDescription)
    {
      this.callDescription = callDescription;
    }
    public String getCallDescription()
    {
      return this.callDescription;
    }
    public void setLoginName(String loginName)
    {
      this.loginName = loginName;
    }
    public String getLoginName()
    {
      return this.loginName;
    }
    public void setCallBack(String callBack)
    {
      this.callBack = callBack;
    }
    public String getCallBack()
    {
      return this.callBack;
    }
    public void setCallStatus(String callStatus)
    {
      this.callStatus = callStatus;
    }
    public String getCallStatus()
    {
      return this.callStatus;
    }
    public void setNotes(String notes)
    {
      this.notes = notes;
    }
    public String getNotes()
    {
//      return this.notes;
        return this.allNotes;
    }
    public void setCloseDate(Date closeDate)
    {
      DateFormat    df      = new SimpleDateFormat("MM/dd/yy");
      try
      {
        this.closeDate = df.format(closeDate);
      }
      catch(Exception e)
      {
        SyncLog.LogEntry(this.getClass().getName() + "::setCloseDate()", e.toString());
      }
    }
    public String getCloseDate()
    {
      return this.closeDate;
    }
    public void setCloseNotes(String closeNotes)
    {
      this.closeNotes = closeNotes;
    }
    public String getCloseNotes()
    {
      return this.closeNotes;
    }
    public void setSequence(String sequence)
    {
      this.sequence = sequence;
    }
    public String getSequence()
    {
      return this.sequence;
    }
    
    public void setIsBillable(boolean billable)
    {
      this.billable = billable;
    }
    
    public boolean IsBillable()
    {
      return billable;
    }

    public void setWasClientGenerated(boolean clientGenerated)
    {
      this.clientGenerated = clientGenerated;
    }
    
    public boolean wasClientGenerated()
    {
      return clientGenerated;
    }
    
    public void setDisplayBold(boolean displayBold)
    {
      this.displayBold = displayBold;
    }
    
    public boolean getDisplayBold()
    {
      return displayBold;
    }
  
    public void setAllNotes(String notes, String closeNotes)
    {
      StringBuffer      result      = new StringBuffer("");
      try
      {
        result.append(notes);
        
        if(closeNotes != null && !closeNotes.equals(""))
        {
          result.append("<br></br>");
          result.append("<font color=\"blue\">");
          result.append(closeNotes);
          result.append("</font>");
        }
      }
      catch(Exception e)
      {
        SyncLog.LogEntry(this.getClass().getName() + "::setAllNotes()", e.toString());
      }
      
      this.allNotes = result.toString();
    }
  }
  
  
  /*********************************************************************
  *
  * Start of main class (CallTrackingBean)
  *
  **********************************************************************/
  public Vector     serviceCalls        = null;
  public String     merchantNumber      = "";
  
  
  public CallTrackingBean()
  {
    serviceCalls = new Vector();
  }
  
  public void loadCalls()
  {
    // load all the calls for this merchant number into a vector
    String                callDescription     = "";
    String                callStatus          = "";
    ResultSetIterator     it                  = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:297^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    sc.merchant_number    merchant_number,
//                    sc.call_date          call_date,
//                    sc.type               type,
//                    sc.other_description  other_description,
//                    sc.login_name         login_name,
//                    sc.call_back          call_back,
//                    sc.status             status,
//                    sc.notes              notes,
//                    sc.close_date         close_date,
//                    sc.close_notes        close_notes,
//                    sc.billable           billable,
//                    sc.client_generated   clientGenerated,
//                    sc.sequence           sequence,
//                    sc.display_bold       display_bold,
//                    sct.description       type_description,
//                    scs.description       status_description
//          from      service_calls sc,
//                    service_call_types sct,
//                    service_call_statuses scs
//          where     sc.merchant_number = :this.merchantNumber and
//                    nvl(sc.hidden, 'N') != 'Y' and
//                    sc.type = sct.type and
//                    sc.status = scs.status
//          order by  call_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    sc.merchant_number    merchant_number,\n                  sc.call_date          call_date,\n                  sc.type               type,\n                  sc.other_description  other_description,\n                  sc.login_name         login_name,\n                  sc.call_back          call_back,\n                  sc.status             status,\n                  sc.notes              notes,\n                  sc.close_date         close_date,\n                  sc.close_notes        close_notes,\n                  sc.billable           billable,\n                  sc.client_generated   clientGenerated,\n                  sc.sequence           sequence,\n                  sc.display_bold       display_bold,\n                  sct.description       type_description,\n                  scs.description       status_description\n        from      service_calls sc,\n                  service_call_types sct,\n                  service_call_statuses scs\n        where     sc.merchant_number =  :1  and\n                  nvl(sc.hidden, 'N') != 'Y' and\n                  sc.type = sct.type and\n                  sc.status = scs.status\n        order by  call_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.CallTrackingBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,this.merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.CallTrackingBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:323^7*/
      
      ResultSet rs = it.getResultSet();
      
      while(rs.next())
      {
        // determine the call description from the call type
        int callType = rs.getInt("type");
        
        if(callType == mesConstants.CALL_TYPE_OTHER)
        {
          callDescription = rs.getString("other_description");
        }
        else
        {
          callDescription = rs.getString("type_description");
        }
        
        serviceCalls.add(new MerchantCallDetail
                                ( rs.getString("merchant_number"),
                                  rs.getDate  ("call_date"),
                                  rs.getTime  ("call_date"),
                                  callDescription,
                                  rs.getString("login_name"),
                                  rs.getString("call_back"),
                                  rs.getString("status_description"),
                                  rs.getString("notes"),
                                  rs.getDate  ("close_date"),
                                  rs.getString("close_notes"),
                                  rs.getString("sequence"),
                                  (rs.getString("billable")!=null&&rs.getString("billable").equals("N")?false:true),
                                  (rs.getString("clientGenerated")!=null&&rs.getString("clientGenerated").equals("Y")?true:false),
                                  (rs.getString("display_bold") != null && rs.getString("display_bold").equals("Y")?true:false)
                                )
                          );
      }
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadCalls(" + this.merchantNumber + ")", e.toString());
      e.printStackTrace();
    }
    finally
    {
      cleanUp();
    }
  }
  
  public Vector getServiceCalls()
  {
    return getServiceCalls(false,false,true,true);
  }
  
  public Vector getServiceCalls(boolean canViewClientNotes, 
                                boolean canSubmitClientNotes, 
                                boolean canViewMESNotes, 
                                boolean canSubmitMESNotes){
    
    Vector v;
    MerchantCallDetail mcd;
    
    if((canViewClientNotes || canSubmitClientNotes) && !canViewMESNotes && !canSubmitMESNotes )
    {
      // <true> false false
      //show only client-generated notes
      v = new Vector();
      for(int i=0; i < serviceCalls.size() ; ++i)
      {
        mcd = (MerchantCallDetail)serviceCalls.elementAt(i);
        if(mcd.wasClientGenerated())
        {
          v.addElement(mcd);
        }
      }
    }
    else if ((canViewClientNotes || canSubmitClientNotes) && (canViewMESNotes || canSubmitMESNotes)) 
    {
      //<true> <true>
      //can return all selections
      v = this.serviceCalls;
    }
    else
    {
      // false false <true> - show only MeS generated
      v = new Vector();
      for(int i=0; i < serviceCalls.size() ; ++i)
      {
        mcd = (MerchantCallDetail)serviceCalls.elementAt(i);
        if(!mcd.wasClientGenerated())
        {
          v.addElement(mcd);
        }
      } 
    }
    
    return v;
  }
  
  public void setMerchant(String merchantNumber)
  {
    this.merchantNumber = merchantNumber;
  }
  public void setMerchantNumber(String merchantNumber)
  {
    this.merchantNumber = merchantNumber;
  }
  public String getMerchantNumber()
  {
    return this.merchantNumber;
  }
}/*@lineinfo:generated-code*/