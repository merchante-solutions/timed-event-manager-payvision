/*@lineinfo:filename=VsAppLookup*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/VsAppLookup.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/08/03 11:37a $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.maintenance;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class VsAppLookup extends DateSQLJBean
{
  private String            lookupValue         = "";
  private String            lastLookupValue     = "";
  
  private int               action              = mesConstants.LU_ACTION_INVALID;
  private String            actionDescription   = "Invalid Action";
  
  private AppDataComparator adc                 = new AppDataComparator();
  private Vector            lookupResults       = new Vector();
  private TreeSet           sortedResults       = null;
  
  private boolean           submitted           = false;
  private boolean           refresh             = true;
  
  private int               appType             = -1;
  private int               lastAppType         = -1;
  private int               appStatus           = -1;
  private int               lastAppStatus       = -1;
  
  private int               lastFromMonth       = -1;
  private int               lastFromDay         = -1;
  private int               lastFromYear        = -1;
  private int               lastToMonth         = -1;
  private int               lastToDay           = -1;
  private int               lastToYear          = -1;
  
  public  Vector            appStatuses         = new Vector();
  public  Vector            appStatusValues     = new Vector();
  public  Vector            appTypes            = new Vector();
  public  Vector            appTypeValues       = new Vector();
  
  public void AccountLookup()
  {
    fillDropDowns();
  }
  
  public synchronized void forceRefresh(boolean refresh)
  {
    this.refresh = refresh;
  }
  
  public synchronized boolean hasLookupChanged()
  {
    boolean result = false;
    
    if( refresh     ||
        appType     != lastAppType      ||
        appStatus   != lastAppStatus    ||
        fromMonth   != lastFromMonth    ||
        fromDay     != lastFromDay      ||
        fromYear    != lastFromYear     ||
        toMonth     != lastToMonth      ||
        toDay       != lastToDay        ||
        toYear      != lastToYear       ||
        ! lookupValue.equals(lastLookupValue)   
      )
    {
      result = true;
    }
    
    return result;
  }
  
  /*
  ** METHOD fillDropDowns
  **
  ** Fills the "App Status" and "App Type" drop down boxes
  */  
  public synchronized void fillDropDowns()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      if(appStatuses.size() == 0)
      {
        appStatuses.clear();
        appStatusValues.clear();
        appTypes.clear();
        appTypeValues.clear();
      
        // app statuses
        /*@lineinfo:generated-code*//*@lineinfo:131^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_credit_status status,
//                    status_desc         description,
//                    item_order          item_order
//            from    merch_credit_statuses
//            order by item_order asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_credit_status status,\n                  status_desc         description,\n                  item_order          item_order\n          from    merch_credit_statuses\n          order by item_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.VsAppLookup",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.VsAppLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:138^9*/
      
        rs = it.getResultSet();
        while(rs.next())
        {
          appStatuses.add(rs.getString("description"));
          appStatusValues.add(rs.getString("status"));
        }
      
        rs.close();
        it.close();
      
        // app types
        appTypes.add("All App Types");
        appTypeValues.add("-1");
        /*@lineinfo:generated-code*//*@lineinfo:153^9*/

//  ************************************************************
//  #sql [Ctx] it = { select   app_type  app_type,
//                     app_name  description
//            from     org_app
//            where    app_type in (2,7)
//            order by app_type asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   app_type  app_type,\n                   app_name  description\n          from     org_app\n          where    app_type in (2,7)\n          order by app_type asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.VsAppLookup",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.VsAppLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:160^9*/
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          appTypes.add(rs.getString("description"));
          appTypeValues.add(rs.getString("app_type"));
        }
        
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("fillDropDowns()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD setDefaultAction
  **
  ** Sets up a default action type if one has not already been assigned
  */
  public synchronized void setDefaultAction()
  {
    if(action == mesConstants.LU_ACTION_INVALID)
    {
      setAction(mesConstants.LU_ACTION_CALL_TRACKING);
    }
  }
  
  /*
  ** METHOD public void getData()
  **
  */
  public synchronized void getData()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      connect();
      
      if (hasLookupChanged())
      {
        // set up longLookup value (numeric version of lookup value)
        long longLookup;
        try
        {
          longLookup = Long.parseLong(lookupValue.trim());
        }
        catch (Exception e) 
        {
          longLookup = -2;
        }
      
        // add wildcards to stringLookup so we can match partial strings
        String stringLookup = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup = "passall";
        }
        else
        {
          stringLookup = "%" + lookupValue.toUpperCase() + "%";
        }
        
        Date fromDate = getSqlFromDate();
        Date toDate   = getSqlToDate();
        
        /*@lineinfo:generated-code*//*@lineinfo:239^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ RULE */
//                    app.app_created_date                app_date,
//                    app.app_seq_num                     app_seq_num,
//                    app.app_type                        app_type,
//                    m.merc_cntrl_number                 control_num,
//                    m.merch_business_name               dba_name,
//                    m.merch_number                      merchant_number,
//                    m.merch_credit_status               merch_credit_status,
//                    u.name                              user_name,
//                    u.hierarchy_node                    hierarchy_node,
//                    u.login_name                        user_login,
//                    mcs.status_desc                     status_string,
//                    oa.app_name                         app_name,
//                    mf.dmdsnum                          mif_disc_num,
//                    mf.damexse                          mif_amex_num,
//                    mpo_disc.merchpo_card_merch_number  disc_merch_number,
//                    mpo_disc.merchpo_rate               disc_disc_rate,
//                    mpo_amex.merchpo_card_merch_number  amex_merch_number,
//                    mpo_amex.merchpo_rate               amex_disc_rate,
//                    mda.transmission_method             disc_transmit,
//                    mda.app_date                        disc_transmit_date,
//                    mdar.decision_status                disc_transmit_status,
//                    amex.submit_date                    amex_transmit_date
//            from    application                         app,
//                    merchant                            m,
//                    mif                                 mf,
//                    users                               u,
//                    merch_credit_statuses               mcs,
//                    org_app                             oa,
//                    merchpayoption                      mpo_disc,
//                    merchpayoption                      mpo_amex,
//                    merchant_discover_app               mda,
//                    merchant_discover_app_response      mdar,
//                    amex_esa_submissions                amex
//            where   trunc(app.app_created_date) between :fromDate and :toDate and
//                    m.app_seq_num = app.app_seq_num and
//                    u.user_id = app.app_user_id and
//                    mcs.merch_credit_status = m.merch_credit_status and
//                    m.merch_number          = mf.merchant_number(+) and
//                    m.app_seq_num           = mda.app_seq_num(+) and
//                    m.app_seq_num           = mdar.app_seq_num(+) and
//                    m.app_seq_num           = amex.app_seq_num(+) and
//                    oa.app_type = app.app_type and
//                    (
//                      app.app_seq_num = mpo_disc.app_seq_num(+) and
//                      14 = mpo_disc.cardtype_code(+)
//                    ) and
//                    (
//                      app.app_seq_num = mpo_amex.app_seq_num(+) and
//                      16 = mpo_amex.cardtype_code(+)
//                    ) and
//                    (-1 = :appStatus or m.merch_credit_status = :appStatus) and
//                    ((-1 = :appType and app.app_type in (2,7)) or app.app_type = :appType) and
//                    (
//                      'passall'             = :stringLookup or
//                      m.merch_number        = :longLookup or
//                      m.merc_cntrl_number   = :longLookup or
//                      mod(u.hierarchy_node, 1000000) = :longLookup or
//                      u.hierarchy_node      = :longLookup or
//                      app.app_seq_num       = :longLookup or
//                      upper(m.merch_business_name) like :stringLookup or
//                      upper(u.name) like :stringLookup or
//                      upper(u.login_name) like :stringLookup
//                    ) 
//            group by  app.app_created_date,
//                      app.app_seq_num,
//                      app.app_type,
//                      m.merc_cntrl_number,
//                      m.merch_business_name,
//                      m.merch_number,
//                      m.merch_credit_status,
//                      u.name,
//                      u.hierarchy_node,
//                      u.login_name,
//                      mcs.status_desc,
//                      oa.app_name,
//                      mf.dmdsnum,
//                      mf.damexse,
//                      mpo_disc.merchpo_card_merch_number,
//                      mpo_disc.merchpo_rate,
//                      mpo_amex.merchpo_card_merch_number,
//                      mpo_amex.merchpo_rate,
//                      mda.transmission_method,
//                      mda.app_date,
//                      mdar.decision_status,
//                      amex.submit_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ RULE */\n                  app.app_created_date                app_date,\n                  app.app_seq_num                     app_seq_num,\n                  app.app_type                        app_type,\n                  m.merc_cntrl_number                 control_num,\n                  m.merch_business_name               dba_name,\n                  m.merch_number                      merchant_number,\n                  m.merch_credit_status               merch_credit_status,\n                  u.name                              user_name,\n                  u.hierarchy_node                    hierarchy_node,\n                  u.login_name                        user_login,\n                  mcs.status_desc                     status_string,\n                  oa.app_name                         app_name,\n                  mf.dmdsnum                          mif_disc_num,\n                  mf.damexse                          mif_amex_num,\n                  mpo_disc.merchpo_card_merch_number  disc_merch_number,\n                  mpo_disc.merchpo_rate               disc_disc_rate,\n                  mpo_amex.merchpo_card_merch_number  amex_merch_number,\n                  mpo_amex.merchpo_rate               amex_disc_rate,\n                  mda.transmission_method             disc_transmit,\n                  mda.app_date                        disc_transmit_date,\n                  mdar.decision_status                disc_transmit_status,\n                  amex.submit_date                    amex_transmit_date\n          from    application                         app,\n                  merchant                            m,\n                  mif                                 mf,\n                  users                               u,\n                  merch_credit_statuses               mcs,\n                  org_app                             oa,\n                  merchpayoption                      mpo_disc,\n                  merchpayoption                      mpo_amex,\n                  merchant_discover_app               mda,\n                  merchant_discover_app_response      mdar,\n                  amex_esa_submissions                amex\n          where   trunc(app.app_created_date) between  :1  and  :2  and\n                  m.app_seq_num = app.app_seq_num and\n                  u.user_id = app.app_user_id and\n                  mcs.merch_credit_status = m.merch_credit_status and\n                  m.merch_number          = mf.merchant_number(+) and\n                  m.app_seq_num           = mda.app_seq_num(+) and\n                  m.app_seq_num           = mdar.app_seq_num(+) and\n                  m.app_seq_num           = amex.app_seq_num(+) and\n                  oa.app_type = app.app_type and\n                  (\n                    app.app_seq_num = mpo_disc.app_seq_num(+) and\n                    14 = mpo_disc.cardtype_code(+)\n                  ) and\n                  (\n                    app.app_seq_num = mpo_amex.app_seq_num(+) and\n                    16 = mpo_amex.cardtype_code(+)\n                  ) and\n                  (-1 =  :3  or m.merch_credit_status =  :4 ) and\n                  ((-1 =  :5  and app.app_type in (2,7)) or app.app_type =  :6 ) and\n                  (\n                    'passall'             =  :7  or\n                    m.merch_number        =  :8  or\n                    m.merc_cntrl_number   =  :9  or\n                    mod(u.hierarchy_node, 1000000) =  :10  or\n                    u.hierarchy_node      =  :11  or\n                    app.app_seq_num       =  :12  or\n                    upper(m.merch_business_name) like  :13  or\n                    upper(u.name) like  :14  or\n                    upper(u.login_name) like  :15 \n                  ) \n          group by  app.app_created_date,\n                    app.app_seq_num,\n                    app.app_type,\n                    m.merc_cntrl_number,\n                    m.merch_business_name,\n                    m.merch_number,\n                    m.merch_credit_status,\n                    u.name,\n                    u.hierarchy_node,\n                    u.login_name,\n                    mcs.status_desc,\n                    oa.app_name,\n                    mf.dmdsnum,\n                    mf.damexse,\n                    mpo_disc.merchpo_card_merch_number,\n                    mpo_disc.merchpo_rate,\n                    mpo_amex.merchpo_card_merch_number,\n                    mpo_amex.merchpo_rate,\n                    mda.transmission_method,\n                    mda.app_date,\n                    mdar.decision_status,\n                    amex.submit_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.VsAppLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromDate);
   __sJT_st.setDate(2,toDate);
   __sJT_st.setInt(3,appStatus);
   __sJT_st.setInt(4,appStatus);
   __sJT_st.setInt(5,appType);
   __sJT_st.setInt(6,appType);
   __sJT_st.setString(7,stringLookup);
   __sJT_st.setLong(8,longLookup);
   __sJT_st.setLong(9,longLookup);
   __sJT_st.setLong(10,longLookup);
   __sJT_st.setLong(11,longLookup);
   __sJT_st.setLong(12,longLookup);
   __sJT_st.setString(13,stringLookup);
   __sJT_st.setString(14,stringLookup);
   __sJT_st.setString(15,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.maintenance.VsAppLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:327^9*/
        
        lookupResults.clear();
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          String discString   = "";
          String amexString   = "";
          
          // discover status
          if(rs.getString("mif_disc_num") != null && rs.getString("mif_disc_num").length() > 1)
          {
            // mif shows a discover number so we should be good to go
            discString = rs.getString("mif_disc_num");
          }
          else if(rs.getString("disc_merch_number") != null && rs.getString("disc_merch_number").length() > 1)
          {
            // app shows a discover number, we are still good to go
            discString = rs.getString("disc_merch_number");
          }
          else if(rs.getString("disc_disc_rate") != null && rs.getString("disc_transmit") == null)
          {
            // no merchant number and not sent to discover yet
            discString = "D?";
          }
          else if(rs.getString("disc_disc_rate") != null && rs.getString("disc_transmit") != null)
          {
            // no merchant number and sent to discover
            discString = DateTimeFormatter.getFormattedDate(rs.getTimestamp("disc_transmit_date"), "MM/dd/y HH:mm");
            
            if(rs.getString("disc_transmit_status") != null)
            {
              discString += " (" + rs.getString("disc_transmit_status") + ")";
            }
          }
          else
          {
            // no discover at all
            discString = "---";
          }
          
          // amex status
          if(rs.getString("mif_amex_num") != null && rs.getString("mif_amex_num").length() > 1)
          {
            // mif shows an amex number so we are good to go
            amexString = rs.getString("mif_amex_num");
          }
          else if(rs.getString("amex_merch_number") != null && rs.getString("amex_merch_number").length() > 1)
          {
            // app shows an amex number so we are good to go
            amexString = rs.getString("amex_merch_number");
          }
          else if(rs.getString("amex_disc_rate") != null && rs.getString("amex_transmit_date") == null)
          {
            // amex number is pending
            amexString = "A?";
          }
          else if(rs.getString("amex_disc_rate") != null && rs.getString("amex_transmit_date") != null)
          {
            // no merchant number but already sent to amex
            amexString = DateTimeFormatter.getFormattedDate(rs.getTimestamp("amex_transmit_date"), "MM/dd/y HH:mm");
          }
          else
          {
            // no amex at all
            amexString = "---";
          }
          
          lookupResults.add(new AppData(rs.getString("app_seq_num"),
                                        rs.getString("merchant_number"),
                                        DateTimeFormatter.getFormattedDate(rs.getTimestamp("app_date"), "MM/dd/yy HH:mm"),
                                        rs.getTimestamp("app_date"),
                                        rs.getString("dba_name"),
                                        rs.getString("control_num"),
                                        rs.getString("status_string"),
                                        rs.getString("app_name"),
                                        rs.getString("user_name"),
                                        rs.getString("user_login"),
                                        discString,
                                        amexString));
        }
      
        rs.close();
        it.close();
        
        lastAppType     = appType;
        lastAppStatus   = appStatus;
        lastFromMonth   = fromMonth;
        lastFromDay     = fromDay;
        lastFromYear    = fromYear;
        lastToMonth     = toMonth;
        lastToDay       = toDay;
        lastToYear      = toYear;
        lastLookupValue = lookupValue;
      }
      
      // now create the sorted tree
      if(sortedResults != null)
      {
        sortedResults.clear();
      }
      
      sortedResults = null;
      
      sortedResults = new TreeSet(adc);
      sortedResults.addAll(lookupResults);
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public synchronized Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    return result;
  }
  
  /*
  ** METHOD getActionDescription
  **
  ** Returns the description of the current action
  */
  public synchronized String getActionDescription()
  {
    return this.actionDescription;
  }
  
  /*
  ** ACCESSORS
  */
  public synchronized String getLookupValue()
  {
    return lookupValue;
  }
  public synchronized void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }
  
  public synchronized int getAction()
  {
    return this.action;
  }
  public synchronized void setAction(int action)
  {
    this.actionDescription = "Application Status";
  }
  public void setAction(String action)
  {
    int       actionNum;
    try
    {
      actionNum = Integer.parseInt(action);
    }
    catch(Exception e)
    {
      actionNum = mesConstants.LU_ACTION_INVALID;
    }
    
    setAction(actionNum);
  }
  
  public synchronized void setSortBy(String sortBy)
  {
    try
    {
      adc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }
  
  public synchronized void setAppType(String appType)
  {
    try
    {
      this.appType = Integer.parseInt(appType);
    }
    catch(Exception e)
    {
    }
  }
  public synchronized String getAppType()
  {
    return Integer.toString(this.appType);
  }
  public synchronized void setAppStatus(String appStatus)
  {
    try
    {
      this.appStatus = Integer.parseInt(appStatus);
    }
    catch(Exception e)
    {
    }
  }
  public synchronized String getAppStatus()
  {
    return Integer.toString(this.appStatus);
  }
  public synchronized void setSubmitted(String submitted)
  {
    this.submitted = true;
  }
  public synchronized boolean isSubmitted()
  {
    return this.submitted;
  }
  
  public class AppData
  {
    private String      appSeqNum;
    private String      merchantNumber;
    private String      appDate;
    private Timestamp   appDateSorted;
    private String      dbaName;
    private String      vnumber;
    private String      controlNumber;
    private String      appStatus;
    private String      appType;
    private String      appUser;
    private String      appUserLogin;
    private String      discStatus;
    private String      amexStatus;
    
    public AppData()
    {
      appSeqNum         = "";
      merchantNumber    = "";
      appDate           = "";
      appDateSorted     = null;
      dbaName           = "";
      vnumber           = "---";
      controlNumber     = "";
      appStatus         = "";
      appType           = "";
      appUser           = "";
      discStatus        = "";
      amexStatus        = "";
    }
    
    public AppData(String     appSeqNum,
                   String     merchantNumber,
                   String     appDate,
                   Timestamp  appDateSorted,
                   String     dbaName,
                   String     controlNumber,
                   String     appStatus,
                   String     appType,
                   String     appUser,
                   String     appUserLogin,
                   String     discStatus,
                   String     amexStatus)
    {
      setAppSeqNum(appSeqNum);
      setMerchantNumber(merchantNumber);
      setAppDate(appDate);
      setAppDateSorted(appDateSorted);
      setDbaName(dbaName);
      //setVnumber(vnumber);
      setControlNumber(controlNumber);
      setAppStatus(appStatus);
      setAppType(appType);
      setAppUser(appUser);
      setAppUserLogin(appUserLogin);
      setDiscStatus(discStatus);
      setAmexStatus(amexStatus);
    
      // fill the vnumber data member
      if(! isConnectionStale())
      {
        generateVnumberDescriptor();
      }
    }
    
    private void generateVnumberDescriptor()
    {
      /* 
        FORMAT: "V{[?][#]}[-{{profgen prefix}{[?][#]}}]"
         
         where,
                profgen prefix corresponds to MESDB.EQUIP_PROFILE_GENERATORS.PFX
      */
      
      ResultSetIterator   it      = null;
      ResultSet           rs      = null;
      
      try {

        // generate the tid summary descriptor string
        /*@lineinfo:generated-code*//*@lineinfo:639^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    decode(mv.VNUMBER,null,'?','@VITAL','?','#')   vnum_suffix,
//                      nvl(epg.pfx,'V')                               profgen_prefix,
//                      decode(mv.PROFILE_COMPLETED_DATE,null,'?','#') profgen_suffix
//            from      merch_vnumber             mv,
//                      EQUIP_PROFILE_GENERATORS epg
//            where     epg.CODE(+) = mv.PROFILE_GENERATOR_CODE and
//                      mv.app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    decode(mv.VNUMBER,null,'?','@VITAL','?','#')   vnum_suffix,\n                    nvl(epg.pfx,'V')                               profgen_prefix,\n                    decode(mv.PROFILE_COMPLETED_DATE,null,'?','#') profgen_suffix\n          from      merch_vnumber             mv,\n                    EQUIP_PROFILE_GENERATORS epg\n          where     epg.CODE(+) = mv.PROFILE_GENERATOR_CODE and\n                    mv.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.VsAppLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.maintenance.VsAppLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:648^9*/
        rs = it.getResultSet();

        StringBuffer sb = new StringBuffer();

        while(rs.next()) {
          
          sb.append("|");
          
          // mms staging section
          sb.append("V");
          sb.append(rs.getString("vnum_suffix"));
          
          // tid (profile generation section)
          String profgenPfx = rs.getString("profgen_prefix");
          if(!profgenPfx.equals("V")) {
            sb.append("-");
            sb.append(profgenPfx);
            sb.append(rs.getString("profgen_suffix"));
          }
        }

        it.close();
        rs.close();

        vnumber = (sb.length()>0? sb.substring(1):"---");
      }
      catch(Exception e) {
        logEntry("AppData.generateVnumberDescriptor()", e.toString());
      }
      finally 
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
      }
    }
    
    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }
    
    public void setAppSeqNum(String appSeqNum)
    {
      this.appSeqNum = processStringField(appSeqNum);
    }
    public String getAppSeqNum()
    {
      return this.appSeqNum;
    }
    public void setMerchantNumber(String merchantNumber)
    {
      this.merchantNumber = processStringField(merchantNumber);
    }
    public String getMerchantNumber()
    {
      return this.merchantNumber;
    }
    public void setAppDate(String appDate)
    {
      this.appDate = processStringField(appDate);
    }
    public String getAppDate()
    {
      return this.appDate;
    }
    public void setAppDateSorted(Timestamp appDateSorted)
    {
      this.appDateSorted = appDateSorted;
    }
    public String getAppDateSorted()
    {
      return DateTimeFormatter.getFormattedDate(appDateSorted, "yyyyMMddHHmmss");
    }
    public void setDbaName(String dbaName)
    {
      this.dbaName = processStringField(dbaName);
    }
    public String getDbaName()
    {
      return this.dbaName;
    }
    public String getVnumber()
    {
      return this.vnumber;
    }
    public void setControlNumber(String controlNumber)
    {
      this.controlNumber = processStringField(controlNumber);
    }
    public String getControlNumber()
    {
      return this.controlNumber;
    }
    public void setAppStatus(String appStatus)
    {
      this.appStatus = processStringField(appStatus);
    }
    public String getAppStatus()
    {
      return this.appStatus;
    }
    public void setAppType(String appType)
    {
      this.appType = processStringField(appType);
    }
    public String getAppType()
    {
      return this.appType;
    }
    public void setAppUser(String appUser)
    {
      this.appUser = processStringField(appUser);
    }
    public String getAppUser()
    {
      return this.appUser;
    }
    public void setAppUserLogin(String appUserLogin)
    {
      this.appUserLogin = processStringField(appUserLogin);
    }
    public String getAppUserLogin()
    {
      return this.appUserLogin;
    }
    public void setDiscStatus(String discStatus)
    {
      this.discStatus = processStringField(discStatus);
    }
    public String getDiscStatus()
    {
      return this.discStatus;
    }
    public void setAmexStatus(String amexStatus)
    {
      this.amexStatus = processStringField(amexStatus);
    }
    public String getAmexStatus()
    {
      return this.amexStatus;
    }
  }
  
  public class AppDataComparator
    implements Comparator
  {
    public final static int   SB_MERCHANT_NUMBER  = 0;
    public final static int   SB_APP_DATE         = 1;
    public final static int   SB_DBA_NAME         = 2;
    public final static int   SB_VNUMBER          = 3;
    public final static int   SB_CONTROL_NUMBER   = 4;
    public final static int   SB_APP_STATUS       = 5;
    public final static int   SB_APP_TYPE         = 6;
    public final static int   SB_APP_USER         = 7;
    public final static int   SB_APP_SEQ_NUM      = 8;
    public final static int   SB_DISC_NUM         = 9;
    public final static int   SB_AMEX_NUM         = 10;
    
    private int               sortBy;
    
    private boolean           sortAscending       = false;
    
    public AppDataComparator()
    {
      this.sortBy = SB_APP_DATE;
    }
    
    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }
    
    public void setSortBy(int sortBy)
    {
      if(sortBy >= SB_MERCHANT_NUMBER && sortBy <= SB_AMEX_NUM)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }
        
        this.sortBy = sortBy;
      }
    }
  
    int compare(AppData o1, AppData o2)
    {
      int result    = 0;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.getMerchantNumber() + o1.getAppDateSorted();
            compareString2 = o2.getMerchantNumber() + o2.getAppDateSorted();
            break;
            
          case SB_APP_DATE:
            compareString1 = o1.getAppDateSorted() + o1.getDbaName();
            compareString2 = o2.getAppDateSorted() + o2.getDbaName();
            break;
          
          case SB_DBA_NAME:
            compareString1 = o1.getDbaName() + o1.getAppDateSorted();
            compareString2 = o2.getDbaName() + o2.getAppDateSorted();
            break;
            
          case SB_VNUMBER:
            compareString1 = o1.getVnumber() + o1.getAppDateSorted();
            compareString2 = o2.getVnumber() + o2.getAppDateSorted();
            break;
            
          case SB_CONTROL_NUMBER:
            compareString1 = o1.getControlNumber();
            compareString2 = o2.getControlNumber();
            break;
            
          case SB_APP_STATUS:
            compareString1 = o1.getAppStatus() + o1.getAppDateSorted();
            compareString2 = o2.getAppStatus() + o2.getAppDateSorted();
            break;
            
          case SB_APP_TYPE:
            compareString1 = o1.getAppType() + o1.getAppDateSorted();
            compareString2 = o2.getAppType() + o2.getAppDateSorted();
            break;
            
          case SB_APP_USER:
            compareString1 = o1.getAppUser() + o1.getAppDateSorted();
            compareString2 = o2.getAppUser() + o2.getAppDateSorted();
            break;
            
          case SB_APP_SEQ_NUM:
            compareString1 = o1.getAppSeqNum();
            compareString2 = o2.getAppSeqNum();
            
          case SB_DISC_NUM:
            compareString1 = o1.getDiscStatus() + o1.getAppDateSorted();
            compareString2 = o2.getDiscStatus() + o2.getAppDateSorted();
            break;
            
          case SB_AMEX_NUM:
            compareString1 = o1.getAmexStatus() + o1.getAppDateSorted();
            compareString2 = o2.getAmexStatus() + o2.getAppDateSorted();
            break;
          
          default:
            break;
        }
        
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(AppData o1, AppData o2)
    {
      boolean result    = false;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.getMerchantNumber() + o1.getAppDate();
            compareString2 = o2.getMerchantNumber() + o2.getAppDate();
            break;
            
          case SB_APP_DATE:
            compareString1 = o1.getAppDate() + o1.getDbaName();
            compareString2 = o2.getAppDate() + o2.getDbaName();
            break;
          
          case SB_DBA_NAME:
            compareString1 = o1.getDbaName() + o1.getAppDate();
            compareString2 = o2.getDbaName() + o2.getAppDate();
            break;
            
          case SB_VNUMBER:
            compareString1 = o1.getVnumber() + o1.getAppDate();
            compareString2 = o2.getVnumber() + o2.getAppDate();
            break;
            
          case SB_CONTROL_NUMBER:
            compareString1 = o1.getControlNumber() + o1.getAppDate();
            compareString2 = o2.getControlNumber() + o2.getAppDate();
            break;
            
          case SB_APP_STATUS:
            compareString1 = o1.getAppStatus() + o1.getAppDate();
            compareString2 = o2.getAppStatus() + o2.getAppDate();
            break;
            
          case SB_APP_TYPE:
            compareString1 = o1.getAppType() + o1.getAppDate();
            compareString2 = o2.getAppType() + o2.getAppDate();
            break;
            
          case SB_APP_USER:
            compareString1 = o1.getAppUser() + o1.getAppDate();
            compareString2 = o2.getAppUser() + o2.getAppDate();
            break;
          
          case SB_DISC_NUM:
            compareString1 = o1.getDiscStatus() + o1.getAppDateSorted();
            compareString2 = o2.getDiscStatus() + o2.getAppDateSorted();
            break;
            
          case SB_AMEX_NUM:
            compareString1 = o1.getAmexStatus() + o1.getAppDateSorted();
            compareString2 = o2.getAmexStatus() + o2.getAppDateSorted();
            break;
          
          default:
            break;
        }

        result = compareString1.equals(compareString2);        
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((AppData)o1, (AppData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((AppData)o1, (AppData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }
}/*@lineinfo:generated-code*/