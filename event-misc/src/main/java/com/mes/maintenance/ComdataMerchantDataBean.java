/*@lineinfo:filename=ComdataMerchantDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/ComdataMerchantDataBean.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/05/03 3:24p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000-2004,2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;

public class ComdataMerchantDataBean extends MerchantDataBean
{
  // comdata-specific merchant data
  protected String          ChainCode         = null;
  protected String          CorpCode          = null;
  protected String          Phone             = null;
  protected String          LocationCode      = null;
  protected String          LocationName      = null;
  protected String          Address           = null;
  protected String          City              = null;
  protected String          State             = null;
  protected String          ZipCode           = null;
  protected String          ContactName       = null;
  protected int             BankNumber        = 1010;

  public void loadData(String merchantNumber)
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:56^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    mfc.merchant_number                 as merchant_number,
//                    mfc.location_name                   as dba_name,
//                    mfc.corporate_code                  as corp_code,
//                    mfc.chain_code                      as chain_code,
//                    mfc.location_code                   as loc_code,
//                    mfc.location_name                   as loc_name,
//                    mfc.contact_name                    as contact_name,
//                    mfc.address                         as address,
//                    mfc.city                            as city,
//                    mfc.state                           as state,
//                    mfc.zip                             as zip,
//                    mfc.phone1                          as phone
//          from      mif_comdata       mfc
//          where     mfc.merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mfc.merchant_number                 as merchant_number,\n                  mfc.location_name                   as dba_name,\n                  mfc.corporate_code                  as corp_code,\n                  mfc.chain_code                      as chain_code,\n                  mfc.location_code                   as loc_code,\n                  mfc.location_name                   as loc_name,\n                  mfc.contact_name                    as contact_name,\n                  mfc.address                         as address,\n                  mfc.city                            as city,\n                  mfc.state                           as state,\n                  mfc.zip                             as zip,\n                  mfc.phone1                          as phone\n        from      mif_comdata       mfc\n        where     mfc.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.ComdataMerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.ComdataMerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:72^7*/
      resultSet = it.getResultSet();

      if( resultSet.next() )
      {
        // set base class stuff
        setData("merchantNumber", resultSet.getString("merchant_number"));
        setData("dbaName", resultSet.getString("merchant_number"));

        // set comdata-specific fields
        CorpCode      = resultSet.getString("corp_code");
        ChainCode     = resultSet.getString("chain_code");
        LocationCode  = resultSet.getString("loc_code");
        LocationName  = resultSet.getString("loc_name");
        ContactName   = resultSet.getString("contact_name");
        Address       = resultSet.getString("address");
        City          = resultSet.getString("city");
        State         = resultSet.getString("state");
        ZipCode       = resultSet.getString("zip");
        Phone         = resultSet.getString("phone");
      }
      
      resultSet.close();
    }
    catch(Exception e)
    {
      logEntry("loadData(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public void setCorpCode(String data)
  {
    CorpCode = data;
  }
  public String getCorpCode()
  {
    return( CorpCode );
  }
  
  public void setChainCode(String data)
  {
    ChainCode = data;
  }
  public String getChainCode()
  {
    return( ChainCode );
  }
  
  public void setLocationCode(String data)
  {
    LocationCode = data;
  }
  public String getLocationCode()
  {
    return( LocationCode );
  }

  public void setLocationName(String data)
  {
    LocationName = data;
  }
  public String getLocationName()
  {
    return( LocationName );
  }

  public void setAddress(String data)
  {
    Address = data;
  }
  public String getAddress()
  {
    return( Address );
  }

  public void setCity(String data)
  {
    City = data;
  }
  public String getCity()
  {
    return( City );
  }

  public void setState(String data)
  {
    State = data;
  }
  public String getState()
  {
    return( State );
  }

  public void setZipCode(String data)
  {
    ZipCode = data;
  }
  public String getZipCode()
  {
    return( ZipCode );
  }
  
  public void setPhone(String data)
  {
    Phone = data;
  }
  public String getPhone()
  {
    return( Phone );
  }

  public void setContactName(String data)
  {
    ContactName = data;
  }
  public String getContactName() 
  {
    return( ContactName );
  }
}/*@lineinfo:generated-code*/