/*@lineinfo:filename=MerchantRecord*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/MerchantRecord.sqlj $

  Description:  
  
    MerchantRecord

    Encapsulates a merchant record and other data associated with a 
    merchant account.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.maintenance;

import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class MerchantRecord extends SQLJConnectionBase
{
  private boolean           loaded                = false;
  
  private long              appSeqNum             = -1;
  private String            dbaName               = null;
  private String            merchNum              = null;
  private String            ctrlNum               = null;
  private String            assocNum              = null;
  private String            bankNum               = null;
  
  private String            addr1                 = null;
  private String            addr2                 = null;
  private String            city                  = null;
  private String            state                 = null;
  private String            zip                   = null;
  private String            phone                 = null;
  private String            fax                   = null;
  
  private String            contactName           = null;
  private String            contactPhone          = null;
  
  private boolean           amexAccepted          = false;
  private boolean           discoverAccepted      = false;
  private boolean           dinersAccepted        = false;
  private boolean           jcbAccepted           = false;
  
  /*
  ** METHOD public void getData()
  **
  ** Loads merchant data associated with the given application sequence number.
  */
  public void getData()
  {
    if (appSeqNum != -1)
    {
      try
      {
        connect();
        
        ResultSetIterator it = null;
        
        // merchant data
        /*@lineinfo:generated-code*//*@lineinfo:82^9*/

//  ************************************************************
//  #sql [Ctx] { select  m.merch_business_name, 
//                    m.merch_number,
//                    m.merc_cntrl_number,
//                    mif.dmagent,
//                    mif.bank_number
//                    
//            
//  
//            from    merchant m,
//                    mif
//                    
//            where   m.app_seq_num = :appSeqNum and
//                    m.merch_number = mif.merchant_number(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  m.merch_business_name, \n                  m.merch_number,\n                  m.merc_cntrl_number,\n                  mif.dmagent,\n                  mif.bank_number\n                  \n           \n\n          from    merchant m,\n                  mif\n                  \n          where   m.app_seq_num =  :1  and\n                  m.merch_number = mif.merchant_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.MerchantRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 5) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(5,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   dbaName = (String)__sJT_rs.getString(1);
   merchNum = (String)__sJT_rs.getString(2);
   ctrlNum = (String)__sJT_rs.getString(3);
   assocNum = (String)__sJT_rs.getString(4);
   bankNum = (String)__sJT_rs.getString(5);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:101^9*/
        
        // address data
        /*@lineinfo:generated-code*//*@lineinfo:104^9*/

//  ************************************************************
//  #sql [Ctx] { select  address_line1,
//                    address_line2,
//                    address_city,
//                    countrystate_code,
//                    address_zip,
//                    address_phone,
//                    address_fax
//                    
//            
//                    
//            from    address
//            
//            where   app_seq_num = :appSeqNum and
//                    addresstype_code = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  address_line1,\n                  address_line2,\n                  address_city,\n                  countrystate_code,\n                  address_zip,\n                  address_phone,\n                  address_fax\n                  \n           \n                  \n          from    address\n          \n          where   app_seq_num =  :1  and\n                  addresstype_code = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.MerchantRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 7) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(7,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   addr1 = (String)__sJT_rs.getString(1);
   addr2 = (String)__sJT_rs.getString(2);
   city = (String)__sJT_rs.getString(3);
   state = (String)__sJT_rs.getString(4);
   zip = (String)__sJT_rs.getString(5);
   phone = (String)__sJT_rs.getString(6);
   fax = (String)__sJT_rs.getString(7);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:126^9*/
        
        // card data
        /*@lineinfo:generated-code*//*@lineinfo:129^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code
//            
//            from    merchpayoption
//            
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code\n          \n          from    merchpayoption\n          \n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.MerchantRecord",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.maintenance.MerchantRecord",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:136^9*/
        
        ResultSet rs = it.getResultSet();
        while (rs.next())
        {
          switch (rs.getInt(1))
          {
            case mesConstants.APP_CT_AMEX:
              amexAccepted = true;
              break;
              
            case mesConstants.APP_CT_JCB:
              jcbAccepted = true;
              break;
              
            case mesConstants.APP_CT_DISCOVER:
              discoverAccepted = true;
              break;
              
            case mesConstants.APP_CT_DINERS_CLUB:
              dinersAccepted = true;
              break;
          }
        }
        
        // merchant contact
        String contactFirst = null;
        String contactLast = null;
        /*@lineinfo:generated-code*//*@lineinfo:164^9*/

//  ************************************************************
//  #sql [Ctx] { select  merchcont_prim_first_name,
//                    merchcont_prim_last_name,
//                    merchcont_prim_phone
//                    
//            
//                    
//            from    merchcontact
//            
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merchcont_prim_first_name,\n                  merchcont_prim_last_name,\n                  merchcont_prim_phone\n                  \n           \n                  \n          from    merchcontact\n          \n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.MerchantRecord",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   contactFirst = (String)__sJT_rs.getString(1);
   contactLast = (String)__sJT_rs.getString(2);
   contactPhone = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:177^9*/
        contactName = contactFirst + " " + contactLast;
                  
        loaded = true;
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName() + 
          "::getData " + e.toString());
        logEntry("getData",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    else
    {
      loaded = false;
    }
  }
  public void getData(long newAppSeqNum)
  {
    setAppSeqNum(newAppSeqNum);
    getData();
  }
  
  /*
  ** ACCESSORS
  */
  public boolean isLoaded()
  {
    return loaded;
  }
  
  public long getAppSeqNum()
  {
    return appSeqNum;
  }
  public void setAppSeqNum(String newAppSeqNum)
  {
    try
    {
      setAppSeqNum(Long.parseLong(newAppSeqNum));
    }
    catch (Exception e) {}
  }
  public void setAppSeqNum(long newAppSeqNum)
  {
    appSeqNum = newAppSeqNum;
  }

  public String getDbaName()
  {
    return dbaName;
  }
  public String getMerchNum()
  {
    return merchNum;
  }
  public String getCtrlNum()
  {
    return ctrlNum;
  }
  public String getAssocNum()
  {
    return assocNum;
  }
  public String getBankNum()
  {
    return bankNum;
  }
  public String getAddr1()
  {
    return addr1;
  }
  public String getAddr2()
  {
    return addr2;
  }
  public String getCity()
  {
    return city;
  }
  public String getState()
  {
    return state;
  }
  public String getZip()
  {
    return zip;
  }
  public String getPhone()
  {
    return phone;
  }
  public String getFax()
  {
    return fax;
  }
  
  public String getContactName()
  {
    return contactName;
  }
  public String getContactPhone()
  {
    return contactPhone;
  }
  public boolean getAmexAccepted()
  {
    return amexAccepted;
  }
  public boolean getDiscoverAccepted()
  {
    return discoverAccepted;
  }
  public boolean getDinersAccepted()
  {
    return dinersAccepted;
  }
  public boolean getJcbAccepted()
  {
    return jcbAccepted;
  }
}/*@lineinfo:generated-code*/