/*@lineinfo:filename=ChargebackAssignMidBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/ChargebackAssignMidBean.sqlj $

  Description:  

    Support bean to allow users to assign merchant numbers to chargebacks
    that come in without and fail to resolve using the automated resolution.  
    
  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 10/28/04 3:16p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2004,2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.HierarchyNodeField;
import com.mes.forms.RadioButtonField;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class ChargebackAssignMidBean extends SQLJConnectionBase
{
  public static final int     LT_CHARGEBACK   = 0;
  public static final int     LT_RETRIEVAL    = 1;

  public static final int     ST_IDLE         = 0;
  public static final int     ST_ARMED        = 1;
  public static final int     ST_SUBMIT       = 2;
  
  public class ChargebackData
  {
    public Date               BatchDate         = null;
    public int                BankNumber        = 0;
    public String             CardNumber        = null;
    public long               ControlNumber     = 0L;
    public Date               IncomingDate      = null;
    public String             MerchantName      = null;
    public String             RefNum            = null;
    public double             TranAmount        = 0.0;
    
    public ChargebackData( ResultSet resultSet )
      throws java.sql.SQLException
    {      
      ControlNumber = resultSet.getLong("load_sec");
      BankNumber    = resultSet.getInt("bank_number");
      MerchantName  = resultSet.getString("merchant_name");
      CardNumber    = resultSet.getString("card_number");
      IncomingDate  = resultSet.getDate("incoming_date");
      BatchDate     = resultSet.getDate("batch_date");
      TranAmount    = resultSet.getDouble("tran_amount");
      RefNum        = resultSet.getString("ref_num");
    }
  }      
  
  public class MatchData
  {
    public long               MerchantId        = 0L;
    public String             DbaName           = null;
    public String             CardNumber        = null;
    public Date               TranDate          = null;
    public double             TranAmount        = 0.0;
    public Date               BatchDate         = null;
    public String             RefNum            = null;
//@    public CheckboxField      Selected          = null;
    
    public MatchData( ResultSet resultSet )
      throws java.sql.SQLException
    {      
      MerchantId    = resultSet.getLong("merchant_number");
      DbaName       = resultSet.getString("dba_name");
      CardNumber    = resultSet.getString("card_number");
      TranDate      = resultSet.getDate("tran_date");
      TranAmount    = resultSet.getDouble("tran_amount");
      BatchDate     = resultSet.getDate("batch_date");
      RefNum        = resultSet.getString("ref_num");
//@      Selected      = new CheckboxField(("cb"+MerchantId), "", false);
    }
  }
  
  public class MerchantData
  {
    public long               MerchantId        = 0L;
    public String             DbaName           = null;
    
    public MerchantData( ResultSet resultSet )
      throws java.sql.SQLException
    {      
      MerchantId    = resultSet.getLong("merchant_number");
      DbaName       = resultSet.getString("dba_name");
    }
  }
  
  protected   ChargebackData    Chargeback          = null;
  protected   FieldGroup        Fields              = new FieldGroup("basefields");
  protected   int               LookupType          = LT_CHARGEBACK;
  protected   Vector            PossibleMatches     = new Vector();
  protected   long              SelectedMerchantId  = 0L;  
  protected   int               SubmitState         = ST_IDLE;
  protected   UserBean          User                = null;
  
  public ChargebackAssignMidBean()
  {
  }
  
  public ChargebackData getChargeback( )
  {
    return( Chargeback );
  }
  
  public Vector getFieldsVector( )
  {
    return( Fields.getFieldsVector() );
  }
  
  public Field getField( String fieldName )
  {
    return( getField( fieldName, false ) );
  }
  
  public Field getField( String fieldName, boolean ignoreCase )
  {
    return( Fields.getField(fieldName,ignoreCase) );
  }
  
  public MerchantData getMerchantData( )
  {
    ResultSetIterator it          = null;
    ResultSet         resultSet   = null;
    MerchantData      retVal      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:169^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number      as merchant_number,
//                  mf.dba_name             as dba_name
//          from    mif       mf
//          where   mf.merchant_number = :SelectedMerchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number      as merchant_number,\n                mf.dba_name             as dba_name\n        from    mif       mf\n        where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.ChargebackAssignMidBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,SelectedMerchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.ChargebackAssignMidBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:175^7*/
      resultSet = it.getResultSet();
      resultSet.next();
      retVal = new MerchantData(resultSet);
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
    }
    finally
    {
      try{it.close();}catch(Exception ee){}
    }
    return( retVal );
  }
  
  public Vector getPossibleMatches( )
  {
    return( PossibleMatches );
  }
  
  public int getSubmitState()
  {
    return( SubmitState );
  }
  
  public void initFields( HttpServletRequest request )
  {
    try
    { 
      Fields.removeAllFields();
      Fields.add( new HiddenField("loadSec") );
      Fields.add( new HiddenField("itemType") );
      Fields.add( new HierarchyNodeField(Ctx,User.getHierarchyNode(),"assignedMid","Merchant Number",false) );
      
      if ( SubmitState == ST_IDLE )
      {
        Fields.add( new RadioButtonField("selectedMid",new String[][]{{"User Assigned","0"}}, false, "Must select a merchant number to assign") );
      }        
    }
    catch( Exception e )
    {
      logEntry( "initFields()", e.toString() );
    }
    finally
    {
    }
  }
  
  public boolean isValid( )
  {
    return( Fields.isValid() );
  }
  
  public void loadData( )
  {
    ResultSetIterator   it          = null;
    long                loadSec     = 0L;
    ResultSet           resultSet   = null;
  
    try
    {
      connect();
      
      loadSec = Fields.getField("loadSec").asLong();
      
      if ( LookupType == LT_RETRIEVAL )
      {
        /*@lineinfo:generated-code*//*@lineinfo:244^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  rt.retr_load_sec      as load_sec,
//                    rt.bank_number        as bank_number,
//                    rt.merchant_name      as merchant_name,
//                    rt.card_number        as card_number,
//                    rt.incoming_date      as incoming_date,
//                    to_date(substr(rt.reference_number,8,4),'yddd') as batch_date,
//                    rt.tran_amount        as tran_amount,
//                    rt.reference_number   as ref_num
//            from    network_retrievals    rt                
//            where   rt.retr_load_sec = :loadSec                
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rt.retr_load_sec      as load_sec,\n                  rt.bank_number        as bank_number,\n                  rt.merchant_name      as merchant_name,\n                  rt.card_number        as card_number,\n                  rt.incoming_date      as incoming_date,\n                  to_date(substr(rt.reference_number,8,4),'yddd') as batch_date,\n                  rt.tran_amount        as tran_amount,\n                  rt.reference_number   as ref_num\n          from    network_retrievals    rt                \n          where   rt.retr_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.ChargebackAssignMidBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.ChargebackAssignMidBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:256^9*/
      }   
      else    // assume chargeback
      {
        /*@lineinfo:generated-code*//*@lineinfo:260^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.cb_load_sec        as load_sec,
//                    cb.bank_number        as bank_number,
//                    cb.merchant_name      as merchant_name,
//                    cb.card_number        as card_number,
//                    cb.incoming_date      as incoming_date,
//                    to_date(substr(cb.reference_number,8,4),'yddd') as batch_date,
//                    cb.tran_amount        as tran_amount,
//                    cb.reference_number   as ref_num
//            from    network_chargebacks   cb                
//            where   cb.cb_load_sec = :loadSec                
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.cb_load_sec        as load_sec,\n                  cb.bank_number        as bank_number,\n                  cb.merchant_name      as merchant_name,\n                  cb.card_number        as card_number,\n                  cb.incoming_date      as incoming_date,\n                  to_date(substr(cb.reference_number,8,4),'yddd') as batch_date,\n                  cb.tran_amount        as tran_amount,\n                  cb.reference_number   as ref_num\n          from    network_chargebacks   cb                \n          where   cb.cb_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.ChargebackAssignMidBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.maintenance.ChargebackAssignMidBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:272^9*/
      }
      resultSet = it.getResultSet();
      resultSet.next();
      Chargeback = new ChargebackData(resultSet);
      resultSet.close();
      it.close();      
      
      if ( SubmitState == ST_IDLE )
      {
        Vector      ids     = new Vector();
        
        // if we are hear then using the card number did
        // not work, we are resorting to picking using
        // only the reference number
        /*@lineinfo:generated-code*//*@lineinfo:287^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_account_number      as merchant_number,
//                    mf.dba_name                     as dba_name,
//                    dt.cardholder_account_number    as card_number,
//                    dt.transaction_date             as tran_date,
//                    ( decode(dt.debit_credit_indicator,'C',-1,1) *
//                      dt.transaction_amount )       as tran_amount,
//                    dt.batch_date                   as batch_date,
//                    dt.reference_number             as ref_num
//            from    mif                     mf,
//                    daily_detail_file_dt    dt
//            where   mf.bank_number = :Chargeback.BankNumber and
//                    dt.merchant_account_number = mf.merchant_number and
//                    dt.batch_date between :Chargeback.BatchDate and
//                                          (:Chargeback.BatchDate+2) and
//                    dt.reference_number = :Chargeback.RefNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_account_number      as merchant_number,\n                  mf.dba_name                     as dba_name,\n                  dt.cardholder_account_number    as card_number,\n                  dt.transaction_date             as tran_date,\n                  ( decode(dt.debit_credit_indicator,'C',-1,1) *\n                    dt.transaction_amount )       as tran_amount,\n                  dt.batch_date                   as batch_date,\n                  dt.reference_number             as ref_num\n          from    mif                     mf,\n                  daily_detail_file_dt    dt\n          where   mf.bank_number =  :1  and\n                  dt.merchant_account_number = mf.merchant_number and\n                  dt.batch_date between  :2  and\n                                        ( :3 +2) and\n                  dt.reference_number =  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.ChargebackAssignMidBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,Chargeback.BankNumber);
   __sJT_st.setDate(2,Chargeback.BatchDate);
   __sJT_st.setDate(3,Chargeback.BatchDate);
   __sJT_st.setString(4,Chargeback.RefNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.maintenance.ChargebackAssignMidBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:304^9*/
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          ids.addElement(resultSet.getString("merchant_number"));
          PossibleMatches.addElement( new MatchData( resultSet ) );
          
          String[][]  radioButtons = new String[ids.size()+1][2];
          for( int i = 0; i < ids.size(); ++i )
          {
            radioButtons[i][0] = "";
            radioButtons[i][1] = (String)ids.elementAt(i);
          }
          // user specified
          radioButtons[ids.size()][0] = "";
          radioButtons[ids.size()][1] = "0";
          ((RadioButtonField)getField("selectedMid")).resetRadioButtons(radioButtons);
          getField("selectedMid").setData( Long.toString(SelectedMerchantId) );
        }
        resultSet.close();
        it.close();
      }
    }
    catch( Exception e )
    {
      logEntry("loadData()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      cleanUp();
    }
  }
  
  public String renderHiddenFields( )
  {
    Vector        allFields = getFieldsVector();
    Field         f         = null;
    StringBuffer  retVal    = new StringBuffer();
    
    for( int i = 0; i < allFields.size(); ++i )
    {
      f = (Field)allFields.elementAt(i);
      if ( f instanceof HiddenField )
      {
        retVal.append("\n");
        retVal.append(f.renderHtml());
      }
    }
    return( retVal.toString() );
  }
  
  public String renderHtml(String fname)
  {
    return(Fields.renderHtml(fname));
  }
  
  public String renderHtml(String fname, int radioIndex)
  {
    return(Fields.renderHtml(fname,radioIndex));
  }
  
  public void setProperties(HttpServletRequest request, UserBean user)
  {
    Field       field         = null;
    String      name          = null;
    String      temp          = null;
  
    try
    {
      // store the user
      User = user;
      
      // register bean with MesSystem
      register(user.getLoginName(), request);
      
      temp = HttpHelper.getString(request,"itemType","C");
      
      switch( temp.charAt(0) )
      {
        case 'R':   // retrievals
          LookupType = LT_RETRIEVAL;
          break;
          
        default:
//        case 'C':
          LookupType = LT_CHARGEBACK;
          break;
      }
      
      SubmitState = HttpHelper.getInt(request,"submitState",ST_IDLE);
      
      // instantiate all the editor fields
      initFields(request);
    
      // apply request parameters to the fields
      for (Enumeration names = request.getParameterNames(); 
           names.hasMoreElements(); )
      {
        name  = (String)names.nextElement();

        // if the edit field exists, store the value
        field = getField(name);
    
        if ( field != null )
        {
          field.setData(request.getParameter(name));
        }
      }   

      // when we are armed we only need to ask the
      // user to confirm the selection they have already made.      
      switch( SubmitState )
      {
        case ST_ARMED:
          SelectedMerchantId = HttpHelper.getLong(request,"selectedMid",-1L);
      
          if ( SelectedMerchantId == 0L )
          {
            SelectedMerchantId = HttpHelper.getLong(request,"assignedMid",-1L);
          }
        
          if ( SelectedMerchantId != -1L )
          {
            // be certain that the mid field has the value we are planning
            // to assign so that the validation will test it for accuracy.
            getField("assignedMid").setData(Long.toString(SelectedMerchantId));
          }
          break;
          
        case ST_SUBMIT:
          SelectedMerchantId = HttpHelper.getLong(request,"assignedMid",-1L);
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
    }
  }
  
  public void submitData( )
  {
    long            loadSec     = 0L;
    
    try
    {
      connect();
    
      loadSec = Fields.getField("loadSec").asLong();
      
      if ( SubmitState == ST_SUBMIT && SelectedMerchantId > 0L )
      {
        if ( LookupType == LT_RETRIEVAL )
        {
          /*@lineinfo:generated-code*//*@lineinfo:463^11*/

//  ************************************************************
//  #sql [Ctx] { update  network_retrievals rt
//              set     rt.merchant_number = :SelectedMerchantId
//              where   rt.retr_load_sec = :loadSec and
//                      rt.merchant_number = 0
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  network_retrievals rt\n            set     rt.merchant_number =  :1 \n            where   rt.retr_load_sec =  :2  and\n                    rt.merchant_number = 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.maintenance.ChargebackAssignMidBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,SelectedMerchantId);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:469^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:473^11*/

//  ************************************************************
//  #sql [Ctx] { update  network_chargebacks   cb
//              set     cb.merchant_number = :SelectedMerchantId
//              where   cb.cb_load_sec = :loadSec and
//                      cb.merchant_number = 0
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  network_chargebacks   cb\n            set     cb.merchant_number =  :1 \n            where   cb.cb_load_sec =  :2  and\n                    cb.merchant_number = 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.maintenance.ChargebackAssignMidBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,SelectedMerchantId);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:479^11*/
        }
      
        /*@lineinfo:generated-code*//*@lineinfo:482^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:485^9*/
      }
    }
    catch( Exception e )
    {
      logEntry("submitData()",e.toString());
    }
    finally
    {      
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/