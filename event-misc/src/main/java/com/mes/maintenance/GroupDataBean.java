/*@lineinfo:filename=GroupDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/GroupDataBean.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/28/03 4:17p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.maintenance;

import com.mes.database.SQLJConnectionBase;
import com.mes.user.UserBean;

public class GroupDataBean extends SQLJConnectionBase
{
  private long    node      = 0L;
  private int     nodeNum   = 0;
  private String  nodeName  = "";
  
  public void setNode(String node)
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:45^7*/

//  ************************************************************
//  #sql [Ctx] { select  org_num,
//                  org_name,
//                  org_group
//          
//          from    organization
//          where   org_group = :node
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  org_num,\n                org_name,\n                org_group\n         \n        from    organization\n        where   org_group =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.GroupDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,node);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   this.nodeNum = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   this.nodeName = (String)__sJT_rs.getString(2);
   this.node = __sJT_rs.getLong(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:55^7*/
    }
    catch(Exception e)
    {
      logEntry("setNode(" + node + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public boolean userNotAllowedToSeeNode(UserBean user, String node)
  {
    boolean retVal    = false;
    long    userNode  = 0L;
    
    try
    {
      connect();
      
      int nodeCount = 0;
      
      if(user.getReportHierarchyNode() > 0)
      {
        userNode = user.getReportHierarchyNode();
      }
      else
      {
        userNode = user.getHierarchyNode();
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:87^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(descendent)
//          
//          from    t_hierarchy
//          where   ancestor = :userNode and
//                  descendent = :node
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(descendent)\n         \n        from    t_hierarchy\n        where   ancestor =  :1  and\n                descendent =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.GroupDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,userNode);
   __sJT_st.setString(2,node);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   nodeCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:94^7*/
      
      if(nodeCount == 0)
      {
        retVal = true;
      }
    }
    catch(Exception e)
    {
      logEntry("userNotAllowedToSeeNode()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return retVal;
  }
  
  public long getNode()
  {
    return this.node;
  }
  
  public int getNodeNum()
  {
    return this.nodeNum;
  }
  
  public String getNodeName()
  {
    return this.nodeName;
  }
}/*@lineinfo:generated-code*/