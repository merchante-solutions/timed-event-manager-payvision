/*@lineinfo:filename=BankServACHDetails*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/BankServACHDetails.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/03/03 5:30p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;

public class BankServACHDetails extends SQLJConnectionBase
{
  public Vector     achs            = new Vector();
  public long       merchantNumber  = 0L;
  
  public BankServACHDetails()
  {
  }
  
  public void setProperties(HttpServletRequest request)
  {
    try
    {
      merchantNumber = Long.parseLong(HttpHelper.getString(request, "merchant"));
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
  }
  
  public void loadData()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:71^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  created_by            created_by,
//                  date_created          date_created,
//                  authorized_by         authorized_by,
//                  date_authorized       date_authorized,
//                  date_transmitted      date_transmitted,
//                  amount * decode(credit_debit_ind, 'D', -1, 1) amount,
//                  entry_description     entry_description
//          from    bankserv_ach_detail
//          where   merchant_number = :merchantNumber and
//                  date_transmitted is not null
//          order by date_transmitted desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  created_by            created_by,\n                date_created          date_created,\n                authorized_by         authorized_by,\n                date_authorized       date_authorized,\n                date_transmitted      date_transmitted,\n                amount * decode(credit_debit_ind, 'D', -1, 1) amount,\n                entry_description     entry_description\n        from    bankserv_ach_detail\n        where   merchant_number =  :1  and\n                date_transmitted is not null\n        order by date_transmitted desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.BankServACHDetails",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.BankServACHDetails",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:84^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        achs.add(new BankServACH(rs));
      }
    }
    catch(Exception e)
    {
      logEntry("loadData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public class BankServACH
  {
    public String createdBy;
    public String dateCreated;
    public String authorizedBy;
    public String dateAuthorized;
    public String dateTransmitted;
    public String amount;
    public String description;
    
    public BankServACH(ResultSet rs)
    {
      try
      {
        createdBy = rs.getString("created_by");
        dateCreated = DateTimeFormatter.getFormattedDate(rs.getDate("date_created"), "MM/dd/yyyy");
        authorizedBy = rs.getString("authorized_by");
        dateAuthorized = DateTimeFormatter.getFormattedDate(rs.getDate("date_authorized"), "MM/dd/yyyy");
        dateTransmitted = DateTimeFormatter.getFormattedDate(rs.getDate("date_transmitted"), "MM/dd/yyyy");
        amount = NumberFormatter.getDoubleString(rs.getDouble("amount"), NumberFormatter.CURRENCY_FORMAT);
        description = rs.getString("entry_description");
      }
      catch(Exception e)
      {
        logEntry("constructor()", e.toString());
      }
    }
  }
}/*@lineinfo:generated-code*/