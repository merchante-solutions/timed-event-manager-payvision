/*@lineinfo:filename=DistrictMaintenanceBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/DistrictMaintenanceBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 1/06/03 1:35p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesHierarchy;
import com.mes.database.SQLJConnectionBase;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class DistrictMaintenanceBean extends SQLJConnectionBase
{
  private UserBean    user          = null;
  
  public boolean      isAssocUser   = false;
  
  public Vector       districts     = new Vector();
  
  public DistrictMaintenanceBean()
  {
  }
  
  public DistrictMaintenanceBean(String connectionString)
  {
    super(connectionString);
  }
  
  public void setProperties(HttpServletRequest request, UserBean user)
  {
    try
    {
      connect();
      
      this.user = user;
      
      // register bean with MesSystem
      register(user.getLoginName(), request);
      
      // determine if this is an association user or a higher-level user
      int   entityType = 0;
      /*@lineinfo:generated-code*//*@lineinfo:71^7*/

//  ************************************************************
//  #sql [Ctx] { select  entity_type
//          
//          from    t_hierarchy
//          where   hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                  ancestor = :user.getHierarchyNode() and
//                  descendent = :user.getHierarchyNode()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_831 = user.getHierarchyNode();
 long __sJT_832 = user.getHierarchyNode();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  entity_type\n         \n        from    t_hierarchy\n        where   hier_type =  :1  and\n                ancestor =  :2  and\n                descendent =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.DistrictMaintenanceBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(2,__sJT_831);
   __sJT_st.setLong(3,__sJT_832);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   entityType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:79^7*/
      
      isAssocUser = (entityType == MesHierarchy.ET_ASSN_NODE);
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void buildDistricts()
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;
    try
    {
      connect();
      
      if(isAssocUser)
      {
        // establish the association number
        long assocNumber = user.getHierarchyNode() % 1000000;
        long bankNumber = user.getHierarchyNode() / 1000000;
        
        // get merchants and their districts
        /*@lineinfo:generated-code*//*@lineinfo:108^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchant_number,
//                    dba_name,
//                    nvl(district, 0)  district
//            from    mif
//            where   bank_number = :bankNumber and
//                    dmagent     = :assocNumber
//            order by nvl(district, 0) asc, merchant_number asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merchant_number,\n                  dba_name,\n                  nvl(district, 0)  district\n          from    mif\n          where   bank_number =  :1  and\n                  dmagent     =  :2 \n          order by nvl(district, 0) asc, merchant_number asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.DistrictMaintenanceBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,bankNumber);
   __sJT_st.setLong(2,assocNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.DistrictMaintenanceBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:117^9*/
        
        // build the district vector
        rs = it.getResultSet();
        
        int       curDistrictId = -1;
        District  curDistrict   = null;
        
        while(rs.next())
        {
          int districtId = rs.getInt("district");
          
          if(districtId != curDistrictId)
          {
            // new District
            curDistrict = new District(districtId);
            
            // add to Vector
            districts.add(curDistrict);
            
            curDistrictId = districtId;
          }
          
          // add merchant to district
          curDistrict.merchants.add(new Merchant(rs));
        }
      }
    }
    catch(Exception e)
    {
      logEntry("buildDistricts()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public void updateMerchants(HttpServletRequest request)
  {
    try
    {
      connect();
      Enumeration   params = request.getParameterNames();
      
      while(params.hasMoreElements())
      {
        String name = (String)params.nextElement();
        String value = request.getParameter(name);
        
        if(!name.equals("submit") && !value.equals(""))
        {
          // this is a valid merchant update
          try
          {
            if(Integer.parseInt(value) == 0)
            {
              // update as null so that this shows up as unassigned instead
              // of District 0
              /*@lineinfo:generated-code*//*@lineinfo:178^15*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//                  set     district = null
//                  where   merchant_number = :name
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif\n                set     district = null\n                where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.maintenance.DistrictMaintenanceBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,name);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:183^15*/
            }
            else
            {
              /*@lineinfo:generated-code*//*@lineinfo:187^15*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//                  set     district = :value
//                  where   merchant_number = :name
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif\n                set     district =  :1 \n                where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.maintenance.DistrictMaintenanceBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,value);
   __sJT_st.setString(2,name);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:192^15*/
            }
          }
          catch(Exception e)
          {
            // ignore bad data
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("updateMerchants()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public class District
  {
    public int      districtId;
    public String   districtName;
    
    public Vector   merchants   = new Vector();
    public boolean  expanded    = false;
    
    public District(int districtId)
    {
      this.districtId = districtId;
      
      if(this.districtId == 0)
      {
        districtName = "Unassigned Merchants";
      }
      else
      {
        districtName = "District " + districtId;
      }
    }
  }
  
  public class Merchant
  {
    public long   merchantNumber;
    public String merchantName;
    
    public Merchant(ResultSet rs)
    {
      try
      {
        this.merchantNumber = rs.getLong("merchant_number");
        this.merchantName   = rs.getString("dba_name");
      }
      catch(Exception e)
      {
        logEntry("Merchant()", e.toString());
      }
    }
  }
}/*@lineinfo:generated-code*/