/*@lineinfo:filename=UserLookup*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/UserLookup.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/27/04 4:04p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesHierarchy;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;
import com.mes.user.UserType;
import sqlj.runtime.ResultSetIterator;

public class UserLookup extends SQLJConnectionBase
  implements Serializable
{
  private String              lookupValue         = "";
  private String              lastLookupValue     = "";
  
  private UserDataComparator  udc                 = new UserDataComparator();
  private Vector              lookupResults       = new Vector();
  private TreeSet             sortedResults       = null;
  
  private boolean             submitted           = false;
  private UserType            userType;
  
  private long                userNode            = 0L;
  private int                 userOrgNum          = 0;
  private int                 userBankNum         = 0;
  
  // this is for the merchant detail screen which shows all users assigned to a merchant
  private String              merchantNumber      = "";
  
  public void UserLookup()
  {
  }
  
  public synchronized void setProperties(HttpServletRequest request, UserBean user)
  {
    // register the request with MesSystem
    register(user.getLoginName(), request);
    
    // set internal user-specific variables
    this.userNode     = user.getHierarchyNode();
    this.userOrgNum   = user.getOrgId();
    this.userBankNum  = user.getOrgBankId();
    this.userType     = user.getUserType();
    
    // set request parameters
    setLookupValue(HttpHelper.getString(request, "lookupValue"));
    setSortBy(HttpHelper.getString(request, "sortBy"));
    setSubmitted(HttpHelper.getString(request, "submitted"));
    
    if(! HttpHelper.getString(request, "merchant").equals(""))
    {
      setMerchantNumber(HttpHelper.getString(request, "merchant"));
    }
  }
  
  private boolean hasLookupChanged()
  {
    boolean result = true;
    
    try
    {
      if(lookupValue.equals(lastLookupValue))
      {
        result = false;
      }
    }
    catch(Exception e)
    {
      logEntry("hasLookupChanged()", e.toString());
    }
    
    return result;
  }
  
  public synchronized boolean hasResults()
  {
    boolean result = false;
    
    if(isSubmitted() || lookupResults.size() > 0)
    {
      result = true;
    }
    
    return result;
  }
  
  /*
  ** METHOD loadMerchantUsers
  **
  ** Loads all users pointed to the merchant number in question
  */
  public synchronized void loadMerchantUsers()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      connect();
      
      lookupResults.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:145^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  u.login_name                  login_name,
//                  u.name                        user_name,
//                  u.hierarchy_node              hierarchy_node,
//                  u.enabled                     enabled,
//                  u.type_id                     type_id,
//                  ut.name                       type_name,
//                  u.user_id                     user_id,
//                  to_char(u.create_date,
//                    'MM/dd/yyyy')               created,
//                  to_char(u.last_login_date,
//                    'MM/dd/yyyy hh24:mm:ss')    last_login,
//                  nvl(u.create_user, 'SYSTEM')  create_user
//          from    users                   u,
//                  user_types              ut
//          where   u.hierarchy_node = :merchantNumber and
//                  u.type_id = ut.type_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  u.login_name                  login_name,\n                u.name                        user_name,\n                u.hierarchy_node              hierarchy_node,\n                u.enabled                     enabled,\n                u.type_id                     type_id,\n                ut.name                       type_name,\n                u.user_id                     user_id,\n                to_char(u.create_date,\n                  'MM/dd/yyyy')               created,\n                to_char(u.last_login_date,\n                  'MM/dd/yyyy hh24:mm:ss')    last_login,\n                nvl(u.create_user, 'SYSTEM')  create_user\n        from    users                   u,\n                user_types              ut\n        where   u.hierarchy_node =  :1  and\n                u.type_id = ut.type_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.UserLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.UserLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:163^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        lookupResults.add(new UserData(rs, true));
      }
      
      rs.close();
      it.close();
      
      // create the sorted tree
      if(sortedResults != null)
      {
        sortedResults.clear();
      }
      
      sortedResults = null;
      
      sortedResults = new TreeSet(udc);
      sortedResults.addAll(lookupResults);
    }
    catch(Exception e)
    {
      logEntry("loadMerchantUsers()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD public void getData()
  **
  */
  public synchronized void getData()
  {
    ResultSetIterator it        = null;
    ResultSet         rs        = null;
    boolean           restrict  = false;
    
    try
    {
      connect();
      
      // don't do the lookup if the user did not specify search criteria
      if(hasLookupChanged() && ! lookupValue.trim().equals(""))
      {
        // set up longLookup value (numeric version of lookup value)
        long longLookup;
        try
        {
          longLookup = Long.parseLong(lookupValue.trim());
        }
        catch (Exception e) 
        {
          longLookup = -1;
        }
      
        // add wildcards to stringLookup so  partial strings will match
        String stringLookup = "";
        if(lookupValue.trim().equals(""))
        {
          stringLookup = "passall";
        }
        else
        {
          stringLookup = "%" + lookupValue.toUpperCase() + "%";
        }
        
        // determine if there are restricted user types for this user
        /*@lineinfo:generated-code*//*@lineinfo:238^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ucar.allowed_type allowed_type
//            from    user_client_attr_restrict ucar
//            where   ucar.user_type = :userType.getUserTypeId()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1106 = userType.getUserTypeId();
  try {
   String theSqlTS = "select  ucar.allowed_type allowed_type\n          from    user_client_attr_restrict ucar\n          where   ucar.user_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.UserLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1106);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.UserLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:243^9*/
        
        rs = it.getResultSet();
        
        HashSet allowedTypes = new HashSet();
        while(rs.next())
        {
          restrict = true;
          allowedTypes.add(rs.getLong("allowed_type"));
        }
        
        rs.close();
        it.close();
      
        // get non-merchant users first
        /*@lineinfo:generated-code*//*@lineinfo:258^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  u.login_name                login_name,
//                    u.name                      user_name,
//                    u.hierarchy_node            hierarchy_node,
//                    u.enabled                   enabled,
//                    u.type_id                   type_id,
//                    ut.name                     type_name,
//                    to_char(u.last_login_date,
//                      'MM/DD/YY hh24:mm:ss')    last_login,
//                    u.user_id                   user_id,
//                    to_char(u.create_date,
//                      'MM/dd/yyyy')             created,
//                    nvl(u.create_user,'SYSTEM') create_user
//            from    users                   u,
//                    user_types              ut,
//                    t_hierarchy             th
//            where   th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                    th.ancestor = :userNode and
//                    th.descendent = u.hierarchy_node and
//                    u.type_id = ut.type_id and
//                    (
//                      upper(u.login_name)  like :stringLookup or
//                      u.hierarchy_node = :longLookup or
//                      upper(u.name) like :stringLookup
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  u.login_name                login_name,\n                  u.name                      user_name,\n                  u.hierarchy_node            hierarchy_node,\n                  u.enabled                   enabled,\n                  u.type_id                   type_id,\n                  ut.name                     type_name,\n                  to_char(u.last_login_date,\n                    'MM/DD/YY hh24:mm:ss')    last_login,\n                  u.user_id                   user_id,\n                  to_char(u.create_date,\n                    'MM/dd/yyyy')             created,\n                  nvl(u.create_user,'SYSTEM') create_user\n          from    users                   u,\n                  user_types              ut,\n                  t_hierarchy             th\n          where   th.hier_type =  :1  and\n                  th.ancestor =  :2  and\n                  th.descendent = u.hierarchy_node and\n                  u.type_id = ut.type_id and\n                  (\n                    upper(u.login_name)  like  :3  or\n                    u.hierarchy_node =  :4  or\n                    upper(u.name) like  :5 \n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.UserLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(2,userNode);
   __sJT_st.setString(3,stringLookup);
   __sJT_st.setLong(4,longLookup);
   __sJT_st.setString(5,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.maintenance.UserLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:284^9*/
      
        lookupResults.clear();
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          if(restrict)
          {
            if(allowedTypes.contains(rs.getLong("type_id")))
            {
              lookupResults.add(new UserData(rs, false));
            }
          }
          else
          {
            lookupResults.add(new UserData(rs, false));
          }
        }
      
        rs.close();
        it.close();
        
        // get merchant users next
        /*@lineinfo:generated-code*//*@lineinfo:309^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  u.login_name                login_name,
//                    u.name                      user_name,
//                    u.hierarchy_node            hierarchy_node,
//                    u.enabled                   enabled,
//                    u.type_id                   type_id,
//                    ut.name                     type_name,
//                    to_char(u.last_login_date,
//                      'MM/DD/YY hh24:mm:ss')    last_login,
//                    u.user_id                   user_id,
//                    to_char(u.create_date,
//                      'MM/dd/yyyy')             created,
//                    nvl(u.create_user,'SYSTEM') create_user
//            from    users                   u,
//                    user_types              ut,
//                    group_merchant          gm
//            where   gm.org_num = :userOrgNum and
//                    gm.merchant_number = u.hierarchy_node and
//                    u.type_id = ut.type_id and
//                    (
//                      upper(u.login_name)  like :stringLookup or
//                      u.hierarchy_node = :longLookup or
//                      u.name like :stringLookup
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  u.login_name                login_name,\n                  u.name                      user_name,\n                  u.hierarchy_node            hierarchy_node,\n                  u.enabled                   enabled,\n                  u.type_id                   type_id,\n                  ut.name                     type_name,\n                  to_char(u.last_login_date,\n                    'MM/DD/YY hh24:mm:ss')    last_login,\n                  u.user_id                   user_id,\n                  to_char(u.create_date,\n                    'MM/dd/yyyy')             created,\n                  nvl(u.create_user,'SYSTEM') create_user\n          from    users                   u,\n                  user_types              ut,\n                  group_merchant          gm\n          where   gm.org_num =  :1  and\n                  gm.merchant_number = u.hierarchy_node and\n                  u.type_id = ut.type_id and\n                  (\n                    upper(u.login_name)  like  :2  or\n                    u.hierarchy_node =  :3  or\n                    u.name like  :4 \n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.UserLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,userOrgNum);
   __sJT_st.setString(2,stringLookup);
   __sJT_st.setLong(3,longLookup);
   __sJT_st.setString(4,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.maintenance.UserLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:334^9*/
      
        rs = it.getResultSet();
      
        while(rs.next())
        {
          lookupResults.add(new UserData(rs, true));
        }
      
        rs.close();
        it.close();
        
        lastLookupValue   = lookupValue;
      }
      
      // create the sorted tree
      if(sortedResults != null)
      {
        sortedResults.clear();
      }
    
      sortedResults = null;
    
      sortedResults = new TreeSet(udc);
      sortedResults.addAll(lookupResults);
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public synchronized Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    return result;
  }
  
  /*
  ** ACCESSORS
  */
  public synchronized String getLookupValue()
  {
    return lookupValue;
  }
  public synchronized void setLookupValue(String newLookupValue)
  {
    if(newLookupValue.equals("") && ! lookupValue.equals(""))
    {
      // don't reset an existing lookup value with a null one
    }
    else
    {
      lookupValue = newLookupValue;
    }
  }
  
  public synchronized void setSortBy(String sortBy)
  {
    try
    {
      if(! sortBy.equals(""))
      {
        udc.setSortBy(Integer.parseInt(sortBy));
      }
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }
  
  public synchronized void setSubmitted(String submitted)
  {
    this.submitted = (submitted != null && !submitted.equals(""));
  }
  
  public synchronized boolean isSubmitted()
  {
    return this.submitted;
  }
  
  public synchronized void setMerchantNumber(String merchantNumber)
  {
    this.merchantNumber = merchantNumber;
  }
  
  public synchronized String getMerchantNumber()
  {
    return this.merchantNumber;
  }
  
  /*
  ** NESTED CLASSES
  */
  public class UserData
  {
    private String      loginName;
    private String      userName;
    private String      hierarchyNode;
    private String      enabled;
    private String      typeName;
    private String      lastLogin;
    private boolean     merchantUser;
    private String      userId;
    private String      created;
    private String      createdBy;
    
    public UserData()
    {
      loginName     = "";
      userName      = "";
      hierarchyNode = "";
      enabled       = "";
      typeName      = "";
      merchantUser  = false;
      createdBy     = "SYSTEM";
    }
    
    public UserData(ResultSet rs, boolean isMerchantUser)
    {
      try
      {
        loginName     = rs.getString("login_name");
        userName      = rs.getString("user_name");
        hierarchyNode = rs.getString("hierarchy_node");
        enabled       = rs.getString("enabled");
        typeName      = rs.getString("type_name");
        lastLogin     = rs.getString("last_login");
        userId        = rs.getString("user_id");
        created       = rs.getString("created");
        createdBy     = rs.getString("create_user");
        merchantUser  = isMerchantUser;
      }
      catch(Exception e)
      {
        logEntry("UserData constructor", e.toString());
      }
      
    }
    
    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }
    
    public String getLoginName()
    {
      return loginName;
    }
    
    public String getUserId()
    {
      return userId;
    }
    
    public String getUserName()
    {
      return userName;
    }
    
    public String getHierarchyNode()
    {
      return hierarchyNode;
    }
    
    public String getEnabled()
    {
      return enabled;
    }
    
    public String getTypeName()
    {
      return typeName;
    }
    
    public boolean isMerchantUser()
    {
      return merchantUser;
    }
    
    public String getLastLogin()
    {
      return processStringField(lastLogin);
    }
    
    public String getCreated()
    {
      return processStringField(created);
    }
    
    public String getCreatedBy()
    {
      return processStringField(createdBy);
    }
  }
  
  public class UserDataComparator
    implements Comparator, Serializable
  {
    public final static int   SB_LOGIN_NAME       = 0;
    public final static int   SB_USER_NAME        = 1;
    public final static int   SB_HIERARCHY_NODE   = 2;
    public final static int   SB_ENABLED          = 3;
    public final static int   SB_TYPE_NAME        = 4;
    
    private int               sortBy;
    
    private boolean           sortAscending       = false;
    
    public UserDataComparator()
    {
      this.sortBy = SB_LOGIN_NAME;
    }
    
    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }
    
    public void setSortBy(int sortBy)
    {
      if(sortBy >= SB_LOGIN_NAME && sortBy <= SB_TYPE_NAME)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }
        
        this.sortBy = sortBy;
      }
    }
    
    private String getCompareString(UserData obj)
    {
      String result = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_LOGIN_NAME:
            result = obj.getLoginName();
            break;
            
          case SB_USER_NAME:
            result = obj.getUserName() + obj.getLoginName();
            break;
            
          case SB_HIERARCHY_NODE:
            result = obj.getHierarchyNode() + obj.getLoginName();
            break;
            
          case SB_ENABLED:
            result = obj.getEnabled() + obj.getLoginName();
            break;
            
          case SB_TYPE_NAME:
            result = obj.getTypeName() + obj.getLoginName();
            break;
          
          default:
            break;
        }
      }
      catch(Exception e)
      {
        logEntry("getCompareString()", e.toString());
      }
      
      return result;
    }
  
    int compare(UserData o1, UserData o2)
    {
      int result    = 0;
      
      String compareString1 = getCompareString(o1);
      String compareString2 = getCompareString(o2);
      
      try
      {
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        logEntry("compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(UserData o1, UserData o2)
    {
      boolean result    = false;
      
      String compareString1 = getCompareString(o1);
      String compareString2 = getCompareString(o2);
      
      try
      {
        result = compareString1.equals(compareString2);        
      }
      catch(Exception e)
      {
        logEntry("equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((UserData)o1, (UserData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((UserData)o1, (UserData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }
  
}/*@lineinfo:generated-code*/