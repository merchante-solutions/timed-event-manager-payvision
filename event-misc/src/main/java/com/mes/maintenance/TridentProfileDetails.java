/*@lineinfo:filename=TridentProfileDetails*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  Description:  
  
  TridentProfileDetails
  
  Bean for retrieving trident profiles for display on the CIF screen.
  
  Copyright (C) 2004,2005,2006 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.TridentTidField;
import sqlj.runtime.ResultSetIterator;

public class TridentProfileDetails extends FieldBean
{
  static Logger log = Logger.getLogger(TridentProfileDetails.class);
  
  public Vector profiles = new Vector();
  
  public TridentProfileDetails()
  {
  }
  
  public void setProperties(HttpServletRequest request)
  {
    try
    {
      connect();
      
      setFields(request);
      
      register(user.getLoginName(), request);
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
      log.error("setProperties(): " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // only one field in this bean
      fields.add(new HiddenField("merchant"));
    }
    catch(Exception e)
    {
      logEntry("createFields()", e.toString());
      log.error("createFields(): " + e.toString());
    }
  }
  
  protected void postHandleRequest(HttpServletRequest request)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      // retrieve trident profiles
      /*@lineinfo:generated-code*//*@lineinfo:86^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tp.terminal_id      terminal_id,
//                  tp.merchant_number  merchant_number,
//                  tp.catid            load_id,
//                  decode(tp.profile_status,
//                    'A', 'Active',
//                    'S', 'Suspended',
//                    'C', 'Closed',
//                    'Unknown')        profile_status,
//                  decode(tp.risk_status,
//                    'WB', 'Warning',
//                    'RM', 'Actively Monitoring',
//                    'None')           risk_status
//          from    trident_profile tp
//          where   tp.merchant_number = :fields.getData("merchant")
//          order by tp.terminal_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1101 = fields.getData("merchant");
  try {
   String theSqlTS = "select  tp.terminal_id      terminal_id,\n                tp.merchant_number  merchant_number,\n                tp.catid            load_id,\n                decode(tp.profile_status,\n                  'A', 'Active',\n                  'S', 'Suspended',\n                  'C', 'Closed',\n                  'Unknown')        profile_status,\n                decode(tp.risk_status,\n                  'WB', 'Warning',\n                  'RM', 'Actively Monitoring',\n                  'None')           risk_status\n        from    trident_profile tp\n        where   tp.merchant_number =  :1 \n        order by tp.terminal_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.TridentProfileDetails",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1101);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.TridentProfileDetails",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:103^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        profiles.add(new ProfileDetail(rs));
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("postHandleRequest()", e.toString());
      log.error("postHandleRequest(): " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  public class ProfileDetail
  {
    public TridentTidField  terminalId = new TridentTidField("tid", "Terminal ID", 20, false);
    public String           merchantNumber;
    public String           loadId;
    public String           profileStatus;
    public String           riskStatus;

    public ProfileDetail(ResultSet rs)
    {
      try
      {
        terminalId.setData(rs.getString("terminal_id"));
        merchantNumber = rs.getString("merchant_number");
        loadId = rs.getString("load_id");
        profileStatus = rs.getString("profile_status");
        riskStatus = rs.getString("risk_status");
      }
      catch(Exception e)
      {
        logEntry("ProfileDetail()", e.toString());
      }
    }
  }
}/*@lineinfo:generated-code*/