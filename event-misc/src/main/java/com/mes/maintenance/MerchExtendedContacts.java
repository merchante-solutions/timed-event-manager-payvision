/*@lineinfo:filename=MerchExtendedContacts*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/ServiceCallBean.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-17 12:05:53 -0700 (Tue, 17 Jul 2007) $
  Version            : $Revision: 13887 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.NameField;
import com.mes.forms.PhoneField;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class MerchExtendedContacts extends FieldBean
{
  public MerchExtendedContacts()
  {
  }
  
  public void setProperties(HttpServletRequest request, UserBean user)
  {
    try
    {
      connect();
      
      this.user = user;
      this.request = request;
      
      register(user.getLoginName(), request);
      
      addFields();
      
      setFields(request);
      
      if( ! getData("submit").equals("") )
      {
        // submit new/updated contacts
        submitData();
      }
      
      loadData();
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void addFields()
  {
    try
    {
      fields.add(new HiddenField("merchant"));
      
      fields.add(new NameField("contact1", "Name", 40, true, true));
      fields.add(new Field("title1", "Title", 40, 40, true));
      fields.add(new PhoneField("phone1", "Phone", true));
      fields.add(new EmailField("email1", "Email Address", 75, 40, true));
      
      fields.add(new NameField("contact2", "Name", 40, true, true));
      fields.add(new Field("title2", "Title", 40, 40, true));
      fields.add(new PhoneField("phone2", "Phone", true));
      fields.add(new EmailField("email2", "Email Address", 75, 40, true));
      
      fields.add(new NameField("contact3", "Name", 40, true, true));
      fields.add(new Field("title3", "Title", 40, 40, true));
      fields.add(new PhoneField("phone3", "Phone", true));
      fields.add(new EmailField("email3", "Email Address", 75, 40, true));
      
      fields.add(new NameField("contact4", "Name", 40, true, true));
      fields.add(new Field("title4", "Title", 40, 40, true));
      fields.add(new PhoneField("phone4", "Phone", true));
      fields.add(new EmailField("email4", "Email Address", 75, 40, true));
      
      fields.add(new NameField("contact5", "Name", 40, true, true));
      fields.add(new Field("title5", "Title", 40, 40, true));
      fields.add(new PhoneField("phone5", "Phone", true));
      fields.add(new EmailField("email5", "Email Address", 75, 40, true));
      
      fields.add(new NameField("contact6", "Name", 40, true, true));
      fields.add(new Field("title6", "Title", 40, 40, true));
      fields.add(new PhoneField("phone6", "Phone", true));
      fields.add(new EmailField("email6", "Email Address", 75, 40, true));
      
      fields.add(new ButtonField("submit", "Add/Update"));
      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
    }
  }
  
  private void submitData()
  {
    // this is ugly
    insertUpdate("1");
    insertUpdate("2");
    insertUpdate("3");
    insertUpdate("4");
    insertUpdate("5");
    insertUpdate("6");
  }
  
  private void setData(ResultSet rs)
  {
    try
    {
      String idx = rs.getString("contact_num");
      setData("contact" + idx, rs.getString("contact"));
      setData("title" + idx, rs.getString("title"));
      setData("phone" + idx, rs.getString("phone"));
      setData("email" + idx, rs.getString("email"));
    }
    catch(Exception e)
    {
      logEntry("setData()", e.toString());
    }
  }
  
  private void loadData()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:154^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mec.contact_num     contact_num,
//                  mec.contact_name    contact,
//                  mec.contact_title   title,
//                  mec.contact_phone   phone,
//                  mec.contact_email   email
//          from    merch_extended_contacts mec
//          where   mec.merchant_number = :getData("merchant")
//          order by mec.contact_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_850 = getData("merchant");
  try {
   String theSqlTS = "select  mec.contact_num     contact_num,\n                mec.contact_name    contact,\n                mec.contact_title   title,\n                mec.contact_phone   phone,\n                mec.contact_email   email\n        from    merch_extended_contacts mec\n        where   mec.merchant_number =  :1 \n        order by mec.contact_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.MerchExtendedContacts",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_850);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.MerchExtendedContacts",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:164^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        setData(rs);
      }
    }
    catch(Exception e) 
    {
      logEntry("loadData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void insertUpdate(String index)
  {
    try
    {
      String  contactName = getData("contact"+index);
      String  title       = getData("title"+index);
      String  phone       = getData("phone"+index);
      String  email       = getData("email"+index);
      
      // first remove existing record
      /*@lineinfo:generated-code*//*@lineinfo:194^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merch_extended_contacts
//          where   merchant_number = :getData("merchant") and
//                  contact_num = :index
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_851 = getData("merchant");
  try {
   String theSqlTS = "delete\n        from    merch_extended_contacts\n        where   merchant_number =  :1  and\n                contact_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.MerchExtendedContacts",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_851);
   __sJT_st.setString(2,index);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:200^7*/
      
      if( ! contactName.equals("") )
      {
        /*@lineinfo:generated-code*//*@lineinfo:204^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_extended_contacts
//            (
//              merchant_number,
//              contact_num,
//              contact_name,
//              contact_title,
//              contact_phone,
//              contact_email
//            )
//            values
//            (
//              :getData("merchant"),
//              :index,
//              :contactName,
//              :title,
//              :phone,
//              :email
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_852 = getData("merchant");
   String theSqlTS = "insert into merch_extended_contacts\n          (\n            merchant_number,\n            contact_num,\n            contact_name,\n            contact_title,\n            contact_phone,\n            contact_email\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.maintenance.MerchExtendedContacts",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_852);
   __sJT_st.setString(2,index);
   __sJT_st.setString(3,contactName);
   __sJT_st.setString(4,title);
   __sJT_st.setString(5,phone);
   __sJT_st.setString(6,email);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:224^9*/
        
        commit();
      }
    }
    catch(Exception e)
    {
      logEntry("insertUpdate(" + index + ")", e.toString());
    }
  }
}/*@lineinfo:generated-code*/