/*@lineinfo:filename=GroupLookup*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/GroupLookup.sqlj $

  Description:  
  
    GroupLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.MesHierarchy;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class GroupLookup extends SQLJConnectionBase
  implements Serializable
{
  private String                  lookupValue         = null;
  private String                  lastLookupValue     = null;
  
  private GroupDataComparator     gdc                 = new GroupDataComparator();
  private Vector                  lookupResults       = new Vector();
  private TreeSet                 sortedResults       = null;
  
  public void GroupLookup()
  {
  }
    
  /*
  ** METHOD getMerchantData
  **
  ** Gets all merchants that satisfy the criteria and places them in the
  ** lookupResults Vector
  */
  private void getGroupData(String stringLookup, long longLookup, long userNode)
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:70^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    group_number hierarchy_node,
//                    group_name   group_name
//          from      group_names
//          where     (
//                      group_number                  = :longLookup or
//                      trunc(group_number / 1000000) = :longLookup or
//                      mod(group_number, 1000000)    = :longLookup or
//                      upper(group_name) like :stringLookup
//                    ) and
//                    group_number in
//                    (
//                      select  descendent
//                      from    t_hierarchy
//                      where   hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                              ancestor = :userNode
//                    )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    group_number hierarchy_node,\n                  group_name   group_name\n        from      group_names\n        where     (\n                    group_number                  =  :1  or\n                    trunc(group_number / 1000000) =  :2  or\n                    mod(group_number, 1000000)    =  :3  or\n                    upper(group_name) like  :4 \n                  ) and\n                  group_number in\n                  (\n                    select  descendent\n                    from    t_hierarchy\n                    where   hier_type =  :5  and\n                            ancestor =  :6 \n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.GroupLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setLong(2,longLookup);
   __sJT_st.setLong(3,longLookup);
   __sJT_st.setString(4,stringLookup);
   __sJT_st.setInt(5,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(6,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.GroupLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:88^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        lookupResults.add(new GroupData(rs.getString("hierarchy_node"),
                                        rs.getString("group_name")));
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getGroupData(" + stringLookup + ", " + longLookup + ", " + userNode + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  /*
  ** METHOD public void getData()
  **
  ** Gets a result set iterator containing all merchant account records that
  ** match the lookup value.  The lookup value is matched against control 
  ** numbers, merchant numbers, sequence numbers and dba names.  Anything that
  ** matches is included.  
  */
  public synchronized void getData(long userNode)
  {
    try
    {
      connect();
      
      if (lookupValue != null)
      {
        if(! lookupValue.equals(lastLookupValue))
        {
          long longLookup;
          try
          {
            longLookup = Long.parseLong(lookupValue.trim());
          }
          catch (Exception e) 
          {
            longLookup = -1;
          }
        
          String stringLookup = "%" + lookupValue.toUpperCase() + "%";
          
          lookupResults.clear();
          
          // get merchants
          getGroupData(stringLookup, longLookup, userNode);
          
          lastLookupValue = lookupValue;
        }
      
        // now create the sorted tree
        if(sortedResults != null)
        {
          sortedResults.clear();
        }
        
        sortedResults = null;
      
        sortedResults = new TreeSet(gdc);
        sortedResults.addAll(lookupResults);
      }
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public synchronized Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    return result;
  }
  
  /*
  ** ACCESSORS
  */
  public synchronized String getLookupValue()
  {
    return lookupValue;
  }
  public synchronized void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }
  
  public synchronized void setSortBy(String sortBy)
  {
    try
    {
      gdc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }
  
  public class GroupData
  {
    private String  hierarchyNode;
    private String  groupName;
    
    public GroupData()
    {
      hierarchyNode = "";
      groupName     = "";
    }
    
    public GroupData(String hierarchyNode, String groupName)
    {
      setHierarchyNode(hierarchyNode);
      setGroupName(groupName);
    }
    
    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }
    
    public void setHierarchyNode(String hierarchyNode)
    {
      this.hierarchyNode = hierarchyNode;
    }
    public String getHierarchyNode()
    {
      return this.hierarchyNode;
    }
    
    public void setGroupName(String groupName)
    {
      this.groupName = groupName;
    }
    public String getGroupName()
    {
      return this.groupName;
    }
  }
  
  public class GroupDataComparator
    implements Comparator, Serializable
  {
    public final static int   SB_NODE     = 0;
    public final static int   SB_NAME     = 1;
    
    private int               sortBy;
    private boolean           sortAscending = true;
    
    public GroupDataComparator()
    {
      this.sortBy = SB_NODE;
    }
    
    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }
    
    public void setSortBy(int sortBy)
    {
      if(sortBy >= 0 && sortBy <= SB_NAME)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }
        
        this.sortBy = sortBy;
      }
    }
    
    int compare(GroupData o1, GroupData o2)
    {
      int result = 0;
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_NODE:
            compareString1 = o1.getHierarchyNode() + o1.getGroupName();
            compareString2 = o2.getHierarchyNode() + o2.getGroupName();
            break;
            
          case SB_NAME:
            compareString1 = o1.getGroupName() + o1.getHierarchyNode();
            compareString2 = o2.getGroupName() + o2.getHierarchyNode();
            break;
          
          default:
            break;
        }
        
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(GroupData o1, GroupData o2)
    {
      boolean result = false;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_NODE:
            compareString1 = o1.getHierarchyNode() + o1.getGroupName();
            compareString2 = o2.getHierarchyNode() + o2.getGroupName();
            break;
            
          case SB_NAME:
            compareString1 = o1.getGroupName() + o1.getHierarchyNode();
            compareString2 = o2.getGroupName() + o2.getHierarchyNode();
            break;
        
          default:
            break;
        }
        
        result = compareString1.equals(compareString2);
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((GroupData)o1, (GroupData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((GroupData)o1, (GroupData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }
}/*@lineinfo:generated-code*/