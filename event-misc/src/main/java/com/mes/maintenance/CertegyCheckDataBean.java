/*@lineinfo:filename=CertegyCheckDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/CertegyCheckDataBean.sqlj $

  Description:
    Utility for mapping Certegy Station ID and product to Merchant


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/14/04 12:22p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class CertegyCheckDataBean extends com.mes.database.SQLJConnectionBase
{

  private String productType="N/A";
  private String stationId="";
  private long merchantNumber = 0L;

  public String getDisplayInfo()
  {
    if(productType.equals("N/A"))
    {
      return productType;
    }
    else
    {
      return productType+": "+stationId;
    }
  }

  public void setProperties(HttpServletRequest request)
  {
    try
    {
      merchantNumber = Long.parseLong(HttpHelper.getString(request, "merchant"));
      loadData();
    }
    catch(Exception e)
    {
      //do nothing
    }
  }

  private void loadData()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;

    try
    {
      connect();

      // first see if merchant numbers are in MIF
      /*@lineinfo:generated-code*//*@lineinfo:76^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cpt.product_name  p_type,
//                  cmpm.station_id sid
//          from    certegy_product_type cpt,
//                  certegy_merchant_product_map cmpm
//          where   cmpm.merchant_number = :merchantNumber
//          and     cpt.certegy_product_id = cmpm.certegy_product_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cpt.product_name  p_type,\n                cmpm.station_id sid\n        from    certegy_product_type cpt,\n                certegy_merchant_product_map cmpm\n        where   cmpm.merchant_number =  :1 \n        and     cpt.certegy_product_id = cmpm.certegy_product_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.CertegyCheckDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.CertegyCheckDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:84^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        productType = rs.getString("p_type");
        stationId = rs.getString("sid");
      }
    }
    catch(Exception e)
    {
      //do nothing
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

}/*@lineinfo:generated-code*/