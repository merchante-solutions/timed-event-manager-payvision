/*@lineinfo:filename=ServiceCallBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/ServiceCallBean.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-18 14:14:35 -0700 (Wed, 18 Jul 2007) $
  Version            : $Revision: 13890 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.maintenance;

import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.constants.MesUsers;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.DateStringField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.RadioButtonField;
import com.mes.forms.TextareaField;
import com.mes.forms.Validation;
import com.mes.ops.ActivationQueueBean;
import com.mes.ops.QueueConstants;
import com.mes.queues.QueueTools;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class ServiceCallBean extends FieldBean
{
  static Logger log = Logger.getLogger(ServiceCallBean.class);
  
  /*
  ** Member variables for CallTrackingBean
  */
  private boolean     billable                = false;
  private boolean     clientGenerated         = false;
  private boolean     isCloseCall             = false;
  
  //Scheduling Call
  public static final String SCHEDULING_CALL_SCHEDULED          = "Scheduled";
  public static final String SCHEDULING_CALL_LEFT_MESSAGE       = "Left Message";
  public static final String SCHEDULING_CALL_TOLD_TO_CALL_BACK  = "Told to Call Back";
  public static final String SCHEDULING_CALL_ALREADY_DEPOSITING = "Already Depositing";
  public static final String SCHEDULING_CALL_REFUSED_ACCOUNT    = "Refused Account";
  public static final String SCHEDULING_CALL_REFER_TO_REP       = "Refer to Rep";

  //Training Call
  public static final String TRAINING_CALL_COMPLETED                = "Completed";
  public static final String TRAINING_CALL_ALREADY_DEPOSITING       = "Already Depositing";
  public static final String TRAINING_CALL_RESCHEDULE               = "Reschedule";
  public static final String TRAINING_CALL_NOT_AVAILABLE_RESCHEDULE = "Not Available/Reschedule";
  public static final String TRAINING_CALL_MAINTENANCE_REQUIRED     = "Maintenance Required";
  
  public Vector activationStatuses            = new Vector();
  
  public boolean  showCaseNumber              = false;
  
  protected static final String[][] yesNoRadioList = 
  {
    { "No",  "N" },
    { "Yes", "Y" }
  };
  
  protected static final String[][] activationStatusList =
  {
    { SCHEDULING_CALL_SCHEDULED,              "0"  },
    { SCHEDULING_CALL_LEFT_MESSAGE,           "1"  },
    { SCHEDULING_CALL_TOLD_TO_CALL_BACK,      "2"  },
    { SCHEDULING_CALL_ALREADY_DEPOSITING,     "3"  },
    { SCHEDULING_CALL_REFUSED_ACCOUNT,        "4"  },
    { SCHEDULING_CALL_REFER_TO_REP,           "5"  },
    { TRAINING_CALL_COMPLETED,                "6"  },
    { TRAINING_CALL_ALREADY_DEPOSITING,       "7"  },
    { TRAINING_CALL_RESCHEDULE,               "8"  },
    { TRAINING_CALL_NOT_AVAILABLE_RESCHEDULE, "9"  },
    { TRAINING_CALL_MAINTENANCE_REQUIRED,     "10" },
  };
  
  protected static final String[][] ConversionTypeList = 
  {
    { "Trident",  "0" },
    { "Client",   "1" },
  };
  
  
  protected class CallTypeTable extends DropDownTable
  {
    public CallTypeTable()
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      try
      {
        connect();
        
        addElement("0", "--Select--");

        // retrieve call types from database
        // use special logic for users at top level
        if(user.getHierarchyNode() == 9999999999L)
        {
          /*@lineinfo:generated-code*//*@lineinfo:136^11*/

//  ************************************************************
//  #sql [Ctx] it = { select    sct.type            type,
//                        sct.description     description
//              from      service_call_types  sct
//              where     sct.type > 0 and
//                        sct.hierarchy_node in (9999999999, 394100000)
//              order by  display_order
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    sct.type            type,\n                      sct.description     description\n            from      service_call_types  sct\n            where     sct.type > 0 and\n                      sct.hierarchy_node in (9999999999, 394100000)\n            order by  display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.ServiceCallBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.ServiceCallBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:144^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:148^11*/

//  ************************************************************
//  #sql [Ctx] it = { select    sct.                type,
//                        sct.description     description
//              from      service_call_types  sct,
//                        t_hierarchy         th
//              where     sct.type > 0 and
//                        sct.hierarchy_node = th.ancestor and
//                        th.descendent = :user.getHierarchyNode()
//              order by  display_order
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_922 = user.getHierarchyNode();
  try {
   String theSqlTS = "select    sct.                type,\n                      sct.description     description\n            from      service_call_types  sct,\n                      t_hierarchy         th\n            where     sct.type > 0 and\n                      sct.hierarchy_node = th.ancestor and\n                      th.descendent =  :1 \n            order by  display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.ServiceCallBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_922);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.ServiceCallBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:158^11*/
        }

        rs = it.getResultSet();

        while(rs.next())
        {
          addElement(rs.getString("type"), rs.getString("description"));
        }
      }
      catch(Exception e)
      {
        logEntry("ServiceCalBean::CallTypeTable()", e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }

  public ServiceCallBean()
  {
    setAutoCommit(true);
  }
  
  public boolean isSubmission()
  {
    boolean result  = false;
    
    try
    {
      if(getData("submitServiceCall").equals("Submit") ||
         getData("closeCall").equals("Submit"))
      {
        result = true;
      }
    }
    catch(Exception e)
    {
    }
    return result;
  }

  public void setProperties(HttpServletRequest request, UserBean user)
  {
    try
    {
      connect();
      
      init();

      this.user = user;
      this.request = request;
      
      register(user.getLoginName(), request);
      
      if(request.getRequestURI().indexOf("close_call") != -1)
      {
        isCloseCall = true;
      }

      addFields();

      setFields(request);
      
      if(getData("submitServiceCall").equals("Submit"))
      {
        if(isValid())
        {
          // submit new call
          submitNewCall();
          fields.resetDefaults();
        }
      }
      else if(getData("closeCall").equals("Close Call"))
      {
        if(isValid())
        {
          closeCall();
        }
      }
      else if(isCloseCall)
      {
        loadData();
      }
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void submitNewCall()
  {
    int     callType    = getInt("callType");
    try
    {
      switch( callType )
      {
        case mesConstants.CALL_TYPE_INVALID:
        case mesConstants.CALL_TYPE_OTHER:
          callType = mesConstants.CALL_TYPE_OTHER;  // force to other
          fields.setData("callType", Integer.toString(callType));
          if(fields.getData("otherDescription").equals(""))
          {
            fields.setData("otherDescription", "Other");
          }
          break;
          
        default:
          break;
      }

      if(user.hasRight(MesUsers.RIGHT_MODIFY_BANK_NOTES))
      {
        clientGenerated = true;
        billable = false;
      }
      else
      {
        clientGenerated = false;
        billable = true;
      }
      
      // determine if call type is non-billable (exempt)
      if(billable)
      {
        int exemptCount;
        /*@lineinfo:generated-code*//*@lineinfo:293^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(sce.call_type)
//            
//            from    service_call_exemptions sce
//            where   sce.hierarchy_node = get_service_call_node(:fields.getData("merchant")) and
//                    sce.call_type = :callType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_923 = fields.getData("merchant");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(sce.call_type)\n           \n          from    service_call_exemptions sce\n          where   sce.hierarchy_node = get_service_call_node( :1 ) and\n                  sce.call_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.ServiceCallBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_923);
   __sJT_st.setInt(2,callType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   exemptCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:300^9*/
      
        if(exemptCount > 0)
        {
          billable = false;
        }
      }
      
      int newStatus = fields.getData("callBack").equals("Y") ? mesConstants.CALL_STATUS_OPEN : mesConstants.CALL_STATUS_CLOSED;
      
      // get sequence which will be used for activation purposes later
      int callSeq;
      
      /*@lineinfo:generated-code*//*@lineinfo:313^7*/

//  ************************************************************
//  #sql [Ctx] { select  service_call_sequence.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  service_call_sequence.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.ServiceCallBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   callSeq = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:318^7*/

      fields.setData("sequence", Integer.toString(callSeq));
      
      // setup the conversion type for storage
      // if the call type is not conversion, do not save the value
      // of the radio buttons
      String conversionType = null; 
      if ( callType == mesConstants.CALL_TYPE_CONVERSION )
      {
        conversionType = getData("conversionType");
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:331^7*/

//  ************************************************************
//  #sql [Ctx] { insert into service_calls
//          (
//            sequence,
//            merchant_number,
//            call_date,
//            type,
//            other_description,
//            login_name,
//            call_back,
//            status,
//            notes,
//            billable,
//            client_generated,
//            call_back_date,
//            conversion_type
//          )
//          values
//          (
//            :fields.getData("sequence"),
//            :fields.getData("merchant"),
//            sysdate,
//            :fields.getData("callType"),
//            :fields.getData("otherDescription"),
//            :user.getLoginName(),
//            :fields.getData("callBack"),
//            :newStatus,
//            :fields.getData("callNotes"),
//            :billable ? "Y" : "N",
//            :clientGenerated ? "Y" : "N",
//            to_date(:fields.getData("followUp"), 'MM/DD/YYYY'),
//            :conversionType
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_924 = fields.getData("sequence");
 String __sJT_925 = fields.getData("merchant");
 String __sJT_926 = fields.getData("callType");
 String __sJT_927 = fields.getData("otherDescription");
 String __sJT_928 = user.getLoginName();
 String __sJT_929 = fields.getData("callBack");
 String __sJT_930 = fields.getData("callNotes");
 String __sJT_931 = billable ? "Y" : "N";
 String __sJT_932 = clientGenerated ? "Y" : "N";
 String __sJT_933 = fields.getData("followUp");
   String theSqlTS = "insert into service_calls\n        (\n          sequence,\n          merchant_number,\n          call_date,\n          type,\n          other_description,\n          login_name,\n          call_back,\n          status,\n          notes,\n          billable,\n          client_generated,\n          call_back_date,\n          conversion_type\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n          sysdate,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n          to_date( :11 , 'MM/DD/YYYY'),\n           :12 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.maintenance.ServiceCallBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_924);
   __sJT_st.setString(2,__sJT_925);
   __sJT_st.setString(3,__sJT_926);
   __sJT_st.setString(4,__sJT_927);
   __sJT_st.setString(5,__sJT_928);
   __sJT_st.setString(6,__sJT_929);
   __sJT_st.setInt(7,newStatus);
   __sJT_st.setString(8,__sJT_930);
   __sJT_st.setString(9,__sJT_931);
   __sJT_st.setString(10,__sJT_932);
   __sJT_st.setString(11,__sJT_933);
   __sJT_st.setString(12,conversionType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:365^7*/
      
      if(!getData("caseNumber").equals(""))
      {
        /*@lineinfo:generated-code*//*@lineinfo:369^9*/

//  ************************************************************
//  #sql [Ctx] { insert into service_calls_case_numbers
//            (
//              sequence,
//              case_number
//            )
//            values
//            (
//              :fields.getData("sequence"),
//              :fields.getData("caseNumber")
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_934 = fields.getData("sequence");
 String __sJT_935 = fields.getData("caseNumber");
   String theSqlTS = "insert into service_calls_case_numbers\n          (\n            sequence,\n            case_number\n          )\n          values\n          (\n             :1 ,\n             :2 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.maintenance.ServiceCallBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_934);
   __sJT_st.setString(2,__sJT_935);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:381^9*/
      }

      if( callType == mesConstants.CALL_TYPE_ACTIVATION && 
          !fields.getData("activationStatusReq").equals("") )
      {
        // submit activation status
        submitActivationStatus();
      }
    }
    catch(Exception e)
    {
      logEntry("submitNewCall()", e.toString());
    }
  }
  
  private void submitActivationStatus()
  {
    try
    {
      int activationIdx = Integer.parseInt(fields.getData("activationStatus"));
    
      // retrieve status details for this sequence
      ActivationStatusDetails statusDetails = (ActivationStatusDetails)(activationStatuses.elementAt(activationIdx));
      
      // add to service_call_activation
      /*@lineinfo:generated-code*//*@lineinfo:407^7*/

//  ************************************************************
//  #sql [Ctx] { insert into service_call_activation
//          (
//            sequence,
//            merchant_number,
//            call_date,
//            login_name,
//            activation_status,
//            activation_status_type
//          )
//          values
//          (
//            :fields.getData("sequence"),
//            :fields.getData("merchant"),
//            sysdate,
//            :user.getLoginName(),
//            :activationStatusList[activationIdx][0],
//            :statusDetails.statusType
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_936 = fields.getData("sequence");
 String __sJT_937 = fields.getData("merchant");
 String __sJT_938 = user.getLoginName();
 String __sJT_939 = activationStatusList[activationIdx][0];
   String theSqlTS = "insert into service_call_activation\n        (\n          sequence,\n          merchant_number,\n          call_date,\n          login_name,\n          activation_status,\n          activation_status_type\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n          sysdate,\n           :3 ,\n           :4 ,\n           :5 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.maintenance.ServiceCallBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_936);
   __sJT_st.setString(2,__sJT_937);
   __sJT_st.setString(3,__sJT_938);
   __sJT_st.setString(4,__sJT_939);
   __sJT_st.setInt(5,statusDetails.statusType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:427^7*/
      
      // move item in queue
      long appSeqNum;
      
      /*@lineinfo:generated-code*//*@lineinfo:432^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_seq_num
//          
//          from    merchant
//          where   merch_number = :fields.getData("merchant")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_940 = fields.getData("merchant");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_seq_num\n         \n        from    merchant\n        where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.maintenance.ServiceCallBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_940);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:438^7*/

      // move item in new queue      
      QueueTools.moveQueueItem(appSeqNum, QueueTools.getCurrentQueueType(appSeqNum, MesQueues.Q_ITEM_TYPE_ACTIVATION), statusDetails.queueType, user, "");
      ActivationQueueBean.changeStage(QueueConstants.QUEUE_ACTIVATION, statusDetails.queueStage, fields.getData("merchant"), -1L);
    }
    catch(Exception e)
    {
      logEntry("submitActivationStatus()", e.toString());
    }
  }

  private void closeCall()
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:454^7*/

//  ************************************************************
//  #sql [Ctx] { update  service_calls
//          set     close_notes       = :fields.getData("closeNotes"),
//                  close_login_name  = :user.getLoginName(),
//                  status            = :mesConstants.CALL_STATUS_CLOSED,
//                  close_date        = sysdate
//          where   sequence          = :fields.getData("sequence")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_941 = fields.getData("closeNotes");
 String __sJT_942 = user.getLoginName();
 String __sJT_943 = fields.getData("sequence");
   String theSqlTS = "update  service_calls\n        set     close_notes       =  :1 ,\n                close_login_name  =  :2 ,\n                status            =  :3 ,\n                close_date        = sysdate\n        where   sequence          =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.maintenance.ServiceCallBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_941);
   __sJT_st.setString(2,__sJT_942);
   __sJT_st.setInt(3,mesConstants.CALL_STATUS_CLOSED);
   __sJT_st.setString(4,__sJT_943);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:462^7*/
    }
    catch(Exception e)
    {
      logEntry("closeCall()", e.toString());
    }
  }
  
  private boolean showCaseNumber(String merchantNumber)
  {
    boolean result = false;
    
    try
    {
      connect();
      
      if(merchantNumber != null)
      {
        int recCount = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:482^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(scc.hierarchy_node)
//            
//            from    service_call_case_num_config scc,
//                    t_hierarchy th,
//                    mif m
//            where   m.merchant_number = :merchantNumber and
//                    (
//                      (
//                        m.merchant_number = scc.hierarchy_node and
//                        m.association_node = th.descendent and
//                        th.descendent = th.ancestor
//                      )
//                      or
//                      (
//                        m.association_node = th.descendent and
//                        th.ancestor = scc.hierarchy_node
//                      )
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(scc.hierarchy_node)\n           \n          from    service_call_case_num_config scc,\n                  t_hierarchy th,\n                  mif m\n          where   m.merchant_number =  :1  and\n                  (\n                    (\n                      m.merchant_number = scc.hierarchy_node and\n                      m.association_node = th.descendent and\n                      th.descendent = th.ancestor\n                    )\n                    or\n                    (\n                      m.association_node = th.descendent and\n                      th.ancestor = scc.hierarchy_node\n                    )\n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.maintenance.ServiceCallBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:502^9*/
        
        result = (recCount > 0);
      }
    }
    catch(Exception e)
    {
      logEntry("showCaseNumber(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }

  private void addFields()
  {
    try
    {
      if(isCloseCall)
      {
        fields.add(new HiddenField("sequence"));
        fields.add(new HiddenField("returnURL"));
        fields.add(new HiddenField("merchantName"));
        fields.add(new HiddenField("merchantNumber"));
        fields.add(new HiddenField("callDate"));
        fields.add(new HiddenField("callTime"));
        fields.add(new HiddenField("callStatus"));
        fields.add(new HiddenField("callType"));
        fields.add(new HiddenField("callNotes"));
        
        fields.add(new TextareaField("closeNotes", 1500, 4, 80, true));
        fields.add(new ButtonField("closeCall", "Close Call"));
        
        fields.getField("closeNotes").addValidation(new RequiredNotesValidation(fields.getField("closeCall")));
      }
      else
      {
        fields.add(new HiddenField("sequence"));
        fields.add(new HiddenField("merchant"));
        fields.add(new DropDownField("callType", new CallTypeTable(), true));
        fields.add(new Field("otherDescription", 30, 20, true));
        fields.add(new TextareaField("callNotes", 1500, 3, 75, true));
        fields.add(new RadioButtonField("callBack", yesNoRadioList,0,false,"Required"));
        fields.add(new RadioButtonField("activationStatus", activationStatusList,-1,true,""));
        fields.add(new RadioButtonField("conversionType",ConversionTypeList,-1,true,""));
        fields.add(new DateStringField("followUp", "Call Back Date", true, false));
        fields.add(new HiddenField("activationStatusReq", "Y"));
        fields.add(new ButtonField("submitServiceCall", "Submit"));
        fields.add(new Field("displayBold", 1, 1, true));
        
        // add case # field if required
        if(showCaseNumber(request.getParameter("merchant")))
        {
          showCaseNumber = true;
          fields.add(new NumberField("caseNumber", "Case #", 12, 12, true, 0));
          fields.getField("caseNumber").setMinLength(12);
        }
        else
        {
          showCaseNumber = false;
        }
        
        // this validation ensures that the call type is set to "Other" if the other description is used, and vice versa.
        fields.getField("otherDescription").addValidation(new OtherCallTypeValidation(fields.getField("callType")));
      
        // this validation ensures that an activation status is selected if the call type is Activation, and vice versa
        fields.getField("callType").addValidation(new CallTypeValidation(fields.getField("activationStatus")));
      
        // this validation ensure that the note field has been filled
        fields.getField("callNotes").addValidation(new RequiredNotesValidation(fields.getField("submitServiceCall")));
        
        // make sure call back is no if date field is submitted
        fields.getField("followUp").addValidation(new RequiredCallBackValidation(fields.getField("callBack")));
      }
      
      fields.addHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      logEntry("addFields()", e.toString());
    }
  }

  public void loadData()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:594^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sc.merchant_number                  merchant_number,
//                  m.dba_name                          merchant_name,
//                  to_char(sc.call_date, 'mm/dd/yy')   call_date,
//                  to_char(sc.call_date, 'hh24:mi:ss') call_time,
//                  scs.description                     call_status,
//                  decode(sc.type, 
//                    99, sc.other_description,
//                    sct.description)                  call_type,
//                  sc.notes                            call_notes,
//                  sc.conversion_type                  conversion_type,
//                  nvl(sc.display_bold,'N')            display_bold
//          from    service_calls           sc,
//                  service_call_types      sct,
//                  service_call_statuses   scs,
//                  mif                     m
//          where   sc.sequence = :fields.getData("sequence") and
//                  sc.type = sct.type and
//                  sc.status = scs.status and
//                  m.merchant_number = sc.merchant_number      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_944 = fields.getData("sequence");
  try {
   String theSqlTS = "select  sc.merchant_number                  merchant_number,\n                m.dba_name                          merchant_name,\n                to_char(sc.call_date, 'mm/dd/yy')   call_date,\n                to_char(sc.call_date, 'hh24:mi:ss') call_time,\n                scs.description                     call_status,\n                decode(sc.type, \n                  99, sc.other_description,\n                  sct.description)                  call_type,\n                sc.notes                            call_notes,\n                sc.conversion_type                  conversion_type,\n                nvl(sc.display_bold,'N')            display_bold\n        from    service_calls           sc,\n                service_call_types      sct,\n                service_call_statuses   scs,\n                mif                     m\n        where   sc.sequence =  :1  and\n                sc.type = sct.type and\n                sc.status = scs.status and\n                m.merchant_number = sc.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.maintenance.ServiceCallBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_944);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.maintenance.ServiceCallBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:615^7*/

      rs = it.getResultSet();

      setFields(rs);
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  public void init()
  {
    try
    {
      // set up activation status details
      // note this has to match the order of the ActivationStatusList which sets up the radio buttons
      activationStatuses.add(new ActivationStatusDetails(mesConstants.CALL_STATUS_CLOSED, MesQueues.Q_ACTIVATION_SCHEDULED, QueueConstants.Q_ACTIVATION_SCHEDULED));
      activationStatuses.add(new ActivationStatusDetails(mesConstants.CALL_STATUS_OPEN, MesQueues.Q_ACTIVATION_PENDING, QueueConstants.Q_ACTIVATION_PENDING));
      activationStatuses.add(new ActivationStatusDetails(mesConstants.CALL_STATUS_OPEN, MesQueues.Q_ACTIVATION_PENDING, QueueConstants.Q_ACTIVATION_PENDING));
      activationStatuses.add(new ActivationStatusDetails(mesConstants.CALL_STATUS_CLOSED, MesQueues.Q_ACTIVATION_COMPLETED, QueueConstants.Q_ACTIVATION_COMPLETE));
      activationStatuses.add(new ActivationStatusDetails(mesConstants.CALL_STATUS_CLOSED, MesQueues.Q_ACTIVATION_COMPLETED, QueueConstants.Q_ACTIVATION_COMPLETE));
      activationStatuses.add(new ActivationStatusDetails(mesConstants.CALL_STATUS_CLOSED, MesQueues.Q_ACTIVATION_REFERRED, QueueConstants.Q_ACTIVATION_COMPLETE));
      activationStatuses.add(new ActivationStatusDetails(mesConstants.CALL_STATUS_CLOSED, MesQueues.Q_ACTIVATION_COMPLETED, QueueConstants.Q_ACTIVATION_COMPLETE));
      activationStatuses.add(new ActivationStatusDetails(mesConstants.CALL_STATUS_CLOSED, MesQueues.Q_ACTIVATION_COMPLETED, QueueConstants.Q_ACTIVATION_COMPLETE));
      activationStatuses.add(new ActivationStatusDetails(mesConstants.CALL_STATUS_OPEN, MesQueues.Q_ACTIVATION_PENDING, QueueConstants.Q_ACTIVATION_PENDING));
      activationStatuses.add(new ActivationStatusDetails(mesConstants.CALL_STATUS_OPEN, MesQueues.Q_ACTIVATION_PENDING, QueueConstants.Q_ACTIVATION_PENDING));
      activationStatuses.add(new ActivationStatusDetails(mesConstants.CALL_STATUS_OPEN, MesQueues.Q_ACTIVATION_PENDING, QueueConstants.Q_ACTIVATION_PENDING));
    }
    catch(Exception e)
    {
      logEntry("init()", e.toString());
    }
  }
  
  public void _hideCall(long sequence)
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:665^7*/

//  ************************************************************
//  #sql [Ctx] { update  service_calls
//          set     hidden = 'Y'
//          where   sequence = :sequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  service_calls\n        set     hidden = 'Y'\n        where   sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.maintenance.ServiceCallBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,sequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:670^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("hideCall( " + sequence + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public class OtherCallTypeValidation
    implements Validation
  {
    Field callType;
    String errorText;
    
    public OtherCallTypeValidation(Field _callType)
    {
      callType = _callType;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean result = true;
      /***  BB&T doesn't want this validation because they use the service call types in a weird way
      if(fdata != null && !fdata.equals(""))
      {
        // make sure call type is set to "CALL_TYPE_OTHER"
        if(Integer.parseInt(callType.getData()) != mesConstants.CALL_TYPE_OTHER)
        {
          errorText = "Call type must be 'Other' to use this field";
          result = false;
        }
      }
      else
      {
        // make sure call type is something other than 'OTHER'
        if(Integer.parseInt(callType.getData()) == mesConstants.CALL_TYPE_OTHER)
        {
          errorText = "Required if call type is set to 'Other'";
          result = false;
        }
      }
      */
      
      return result;
    }
  }
  
  public class CallTypeValidation
    implements Validation
  {
    Field   activationStatus;
    String  errorText;
    
    public CallTypeValidation(Field _activationStatus)
    {
      activationStatus = _activationStatus;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    private boolean requiresConversionType(String merchantNumber)
    {
      boolean result = false;
      try
      {
        connect();
        
        int recCount = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:753^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//            
//            from    mif
//            where   merchant_number = :merchantNumber and
//                    bank_number in (3941, 3858)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n           \n          from    mif\n          where   merchant_number =  :1  and\n                  bank_number in (3941, 3858)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.maintenance.ServiceCallBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:760^9*/
        
        result = (recCount > 0);
      }
      catch(Exception e)
      {
        logEntry("requiresConversionType(" + merchantNumber + ")", e.toString()); 
      }
      finally
      {
        cleanUp();
      }
      
      return( result );
    }
    
    public boolean validate(String fdata)
    {
      int     callType  = Integer.parseInt(fdata);
      boolean result    = true;
      
      try
      {
        if(callType == mesConstants.CALL_TYPE_INVALID)
        {
          errorText = "You must select a call type";
          result = false;
        }
        else if(callType == mesConstants.CALL_TYPE_ACTIVATION && 
          (user.hasRight(MesUsers.RIGHT_ACCOUNT_SERVICING) || user.hasRight(MesUsers.RIGHT_ACTIVATION_QUEUE_USERS)))
        {
          if(activationStatus.getData().equals(""))
          {
            errorText = "You must select an Activation Status (below) for this call type";
            result = false;
          }
        }
        else if (callType == mesConstants.CALL_TYPE_CONVERSION && requiresConversionType(getData("merchant")))
        {
          if( getData("conversionType").equals("") )
          {
            errorText = "You must select a conversion type (below) for this call type";
            result = false;
          }
        }
      }
      catch(Exception e)
      {
        errorText = e.toString();
        result = false;
      }
      return result;
    }
  }
  
  public class RequiredNotesValidation
    implements Validation
  {
    Field       submitField;
    String      errorText;
    
    public RequiredNotesValidation(Field _submitField)
    {
      submitField = _submitField;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean result = true;
      
      if((fdata == null || fdata.equals("")) &&
          submitField.getData().equals("Submit"))
      {
        errorText = "Field Required";
        result = false;
      }
      
      return result;
    }
  }
  
  public class RequiredCallBackValidation
    implements Validation
  {
    Field   callBack;
    String  errorText;
    
    public RequiredCallBackValidation(Field _callBack)
    {
      callBack = _callBack;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean result = true;
      
      if(fdata != null && !fdata.equals("") &&
          callBack.getData().equals("N"))
      {
        errorText = "Follow up date not allowed if not a Call Back";
        result = false;
      }
      
      return result;
    }
  }

  public class ActivationStatusDetails
  {
    public int statusType;
    public int queueType;
    public int queueStage;

    public ActivationStatusDetails(int _statusType, int _queueType, int _queueStage)
    {
      statusType = _statusType;
      queueType = _queueType;
      queueStage = _queueStage;
    }
  }
  
  public static void hideCall(long sequence)
  {
    try
    {
      ServiceCallBean scb = new ServiceCallBean();
      
      scb._hideCall(sequence);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("ServiceCallBean::hideCall(" + sequence + ")", e.toString());
    }
  }
}/*@lineinfo:generated-code*/