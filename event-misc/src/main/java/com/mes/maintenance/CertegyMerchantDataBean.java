/*@lineinfo:filename=CertegyMerchantDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/CertegyMerchantDataBean.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/05/03 3:24p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import sqlj.runtime.ResultSetIterator;

public class CertegyMerchantDataBean extends MerchantDataBean
{
  // certegy-specific merchant data
  protected String          corporate;
  protected String          region;
  protected String          principal;
  protected String          associate;
  protected String          chain;
  protected String          merchantName;
  protected String          openDate;
  protected String          dateLastActivity;
  protected String          emailAddress;
  protected String          salesRep;
  protected String          salesRep2;
  protected String          specComm;
  protected String          signedNetVol;
  protected String          signedAvgTicket;
  protected String          mtdNetVolume;
  protected String          mtdDepositCount;
  protected String          mtdSalesVol;
  protected String          mtdSalesCount;
  protected String          mtdCreditVol;
  protected String          mtdCreditCount;
  protected String          mtdChargebackVol;
  protected String          mtdChargebackCount;
  protected String          mtdAvgTicket;
  protected String          attention;
  protected String          address1;
  protected String          address2;
  protected String          city;
  protected String          state;
  protected String          zipCode;
  protected String          country;
  protected String          merchantContact;
  protected String          sicCode;
  protected String          pmNetVol;
  protected String          pmDepositCount;
  protected String          pmSalesVol;
  protected String          pmSalesCount;
  protected String          pmCreditVol;
  protected String          pmCreditCount;
  protected String          pmChargebackVol;
  protected String          pmChargebackCount;
  protected String          pmAvgTicket;

  public void loadData()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:86^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    mc.corporate                        corporate,
//                    mc.region                           region,
//                    mc.principal                        principal,
//                    mc.associate                        associate,
//                    mc.chain                            chain,
//                    mc.merchant_number                  merchant_number,
//                    mc.company_name                     merchant_name,
//                    to_char(mc.date_opened,
//                      'MM/DD/YYYY')                     date_open,
//                    to_char(mc.date_closed,
//                      'MM/DD/YYYY')                     date_closed,
//                    to_char(mc.date_last_activity,
//                      'MM/DD/YYYY')                     date_last_activity,
//                    mc.merchant_status                  merchant_status,
//                    mc.electronic_address               email_address,
//                    mci.service_rep                     sales_rep,
//                    mci.marketing_rep                   sales_rep_2,
//                    mci.freq_of_calls                   spec_comm,
//                    to_char(mci.annual_deposit_amount,
//                      '$99,999,990.00')                 signed_net_vol,
//                    to_char(mci.annual_sales_amount,
//                      '$99,999,990.00')                 signed_avg_tkt,
//                    to_char(mci.mtd_deposit_amount,
//                      '$99,999,990.00')                 mtd_net_vol,
//                    to_char(mci.mtd_deposit_count,
//                      '999,990')                        mtd_dep_count,
//                    to_char(mci.mtd_sales_amount,
//                      '$99,999,990.00')                 mtd_sales_vol,
//                    to_char(mci.mtd_sales_count,
//                      '999,990')                        mtd_sales_count,
//                    to_char(mci.mtd_credit_amount,
//                      '$99,999,990.00')                 mtd_credit_vol,
//                    to_char(mci.mtd_credit_count,
//                      '999,990')                        mtd_credit_count,
//                    to_char(mci.mtd_chargeback_amount,
//                      '$99,999,990.00')                 mtd_cb_vol,
//                    to_char(mci.mtd_chargeback_count,
//                      '999,990')                        mtd_cb_count,
//                    to_char(decode(mci.mtd_sales_count,
//                      null, 0,
//                      0, 0,
//                      round((mci.mtd_sales_amount /
//                      mci.mtd_sales_count), 2)),
//                      '$999,990.00')                    mtd_avg_tkt,
//                    mci.dba_name                        dba_name,
//                    mci.dba_attn                        attention,
//                    mci.dba_address_1                   address_1,
//                    mci.dba_address_2                   address_2,
//                    mci.dba_city                        city,
//                    mci.dba_state                       state,
//                    mci.dba_zip                         zip_code,
//                    mci.dba_country                     country,
//                    mci.merchant_contact                merchant_contact,
//                    mci.phone_1                         phone,
//                    mci.sic_code                        mcc,
//                    to_char(mci.last_deposit_amount,
//                      '$99,999,990.00')                 pm_net_vol,
//                    to_char(mci.last_deposit_count,
//                      '999,990')                        pm_dep_count,
//                    to_char(mci.last_sales_amount,
//                      '$99,999,990.00')                 pm_sales_vol,
//                    to_char(mci.last_sales_count,
//                      '999,990')                        pm_sales_count,
//                    to_char(mci.last_credit_amount,
//                      '$99,999,990.00')                 pm_credit_amount,
//                    to_char(mci.last_credit_count,
//                      '999,990')                        pm_credit_count,
//                    to_char(mci.last_chargeback_amount,
//                      '$99,999,990.00')                 pm_cb_vol,
//                    to_char(mci.last_chargeback_count,
//                      '999,990')                        pm_cb_count,
//                    to_char(decode(mci.last_sales_amount,
//                      0, 0,
//                      null, 0,
//                      round((mci.last_sales_amount /
//                      mci.last_sales_count),2)),
//                      '$999,990.00')                    pm_avg_tkt
//          from      mif_certegy       mc,
//                    mif_certegy_index mci
//          where     mc.merchant_number = :getData("merchant") and
//                    mc.merchant_number = mci.merchant_number(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_819 = getData("merchant");
  try {
   String theSqlTS = "select    mc.corporate                        corporate,\n                  mc.region                           region,\n                  mc.principal                        principal,\n                  mc.associate                        associate,\n                  mc.chain                            chain,\n                  mc.merchant_number                  merchant_number,\n                  mc.company_name                     merchant_name,\n                  to_char(mc.date_opened,\n                    'MM/DD/YYYY')                     date_open,\n                  to_char(mc.date_closed,\n                    'MM/DD/YYYY')                     date_closed,\n                  to_char(mc.date_last_activity,\n                    'MM/DD/YYYY')                     date_last_activity,\n                  mc.merchant_status                  merchant_status,\n                  mc.electronic_address               email_address,\n                  mci.service_rep                     sales_rep,\n                  mci.marketing_rep                   sales_rep_2,\n                  mci.freq_of_calls                   spec_comm,\n                  to_char(mci.annual_deposit_amount,\n                    '$99,999,990.00')                 signed_net_vol,\n                  to_char(mci.annual_sales_amount,\n                    '$99,999,990.00')                 signed_avg_tkt,\n                  to_char(mci.mtd_deposit_amount,\n                    '$99,999,990.00')                 mtd_net_vol,\n                  to_char(mci.mtd_deposit_count,\n                    '999,990')                        mtd_dep_count,\n                  to_char(mci.mtd_sales_amount,\n                    '$99,999,990.00')                 mtd_sales_vol,\n                  to_char(mci.mtd_sales_count,\n                    '999,990')                        mtd_sales_count,\n                  to_char(mci.mtd_credit_amount,\n                    '$99,999,990.00')                 mtd_credit_vol,\n                  to_char(mci.mtd_credit_count,\n                    '999,990')                        mtd_credit_count,\n                  to_char(mci.mtd_chargeback_amount,\n                    '$99,999,990.00')                 mtd_cb_vol,\n                  to_char(mci.mtd_chargeback_count,\n                    '999,990')                        mtd_cb_count,\n                  to_char(decode(mci.mtd_sales_count,\n                    null, 0,\n                    0, 0,\n                    round((mci.mtd_sales_amount /\n                    mci.mtd_sales_count), 2)),\n                    '$999,990.00')                    mtd_avg_tkt,\n                  mci.dba_name                        dba_name,\n                  mci.dba_attn                        attention,\n                  mci.dba_address_1                   address_1,\n                  mci.dba_address_2                   address_2,\n                  mci.dba_city                        city,\n                  mci.dba_state                       state,\n                  mci.dba_zip                         zip_code,\n                  mci.dba_country                     country,\n                  mci.merchant_contact                merchant_contact,\n                  mci.phone_1                         phone,\n                  mci.sic_code                        mcc,\n                  to_char(mci.last_deposit_amount,\n                    '$99,999,990.00')                 pm_net_vol,\n                  to_char(mci.last_deposit_count,\n                    '999,990')                        pm_dep_count,\n                  to_char(mci.last_sales_amount,\n                    '$99,999,990.00')                 pm_sales_vol,\n                  to_char(mci.last_sales_count,\n                    '999,990')                        pm_sales_count,\n                  to_char(mci.last_credit_amount,\n                    '$99,999,990.00')                 pm_credit_amount,\n                  to_char(mci.last_credit_count,\n                    '999,990')                        pm_credit_count,\n                  to_char(mci.last_chargeback_amount,\n                    '$99,999,990.00')                 pm_cb_vol,\n                  to_char(mci.last_chargeback_count,\n                    '999,990')                        pm_cb_count,\n                  to_char(decode(mci.last_sales_amount,\n                    0, 0,\n                    null, 0,\n                    round((mci.last_sales_amount /\n                    mci.last_sales_count),2)),\n                    '$999,990.00')                    pm_avg_tkt\n        from      mif_certegy       mc,\n                  mif_certegy_index mci\n        where     mc.merchant_number =  :1  and\n                  mc.merchant_number = mci.merchant_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.CertegyMerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_819);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.CertegyMerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:169^7*/

      rs = it.getResultSet();
      
      if(rs.next())
      {
        // set base class stuff
        setFields(rs, false);

        // set certegy-specific fields
        setOpenDate(processString(rs.getString("date_open")));
        setCorporate(processString(rs.getString("corporate")));
        setRegion(processString(rs.getString("region")));
        setPrincipal(processString(rs.getString("principal")));
        setAssociate(processString(rs.getString("associate")));
        setChain(processString(rs.getString("chain")));
        setMerchantName(processString(rs.getString("merchant_name")));
        setDateLastActivity(processString(rs.getString("date_last_activity")));
        setEmailAddress(processString(rs.getString("email_address")));
        setSalesRep(processString(rs.getString("sales_rep")));
        setSalesRep2(processString(rs.getString("sales_rep_2")));
        setSpecComm(processString(rs.getString("spec_comm")));
        setSignedNetVol(processString(rs.getString("signed_net_vol")));
        setSignedAvgTicket(processString(rs.getString("signed_avg_tkt")));
        setMtdNetVolume(processString(rs.getString("mtd_net_vol")));
        setMtdDepositCount(processString(rs.getString("mtd_dep_count")));
        setMtdSalesVol(processString(rs.getString("mtd_sales_vol")));
        setMtdSalesCount(processString(rs.getString("mtd_sales_count")));
        setMtdCreditVol(processString(rs.getString("mtd_credit_vol")));
        setMtdCreditCount(processString(rs.getString("mtd_credit_count")));
        setMtdChargebackVol(processString(rs.getString("mtd_cb_vol")));
        setMtdChargebackCount(processString(rs.getString("mtd_cb_count")));
        setMtdAvgTicket(processString(rs.getString("mtd_avg_tkt")));
        setAttention(processString(rs.getString("attention")));
        setAddress1(processString(rs.getString("address_1")));
        setAddress2(processString(rs.getString("address_2")));
        setCity(processString(rs.getString("city")));
        setState(processString(rs.getString("state")));
        setZipCode(processString(rs.getString("zip_code")));
        setCountry(processString(rs.getString("country")));
        setMerchantContact(processString(rs.getString("merchant_contact")));
        setSicCode(processString(rs.getString("mcc")));
        setPmNetVol(processString(rs.getString("pm_net_vol")));
        setPmDepositCount(processString(rs.getString("pm_dep_count")));
        setPmSalesVol(processString(rs.getString("pm_sales_vol")));
        setPmSalesCount(processString(rs.getString("pm_sales_count")));
        setPmCreditVol(processString(rs.getString("pm_credit_amount")));
        setPmCreditCount(processString(rs.getString("pm_credit_count")));
        setPmChargebackVol(processString(rs.getString("pm_cb_vol")));
        setPmChargebackCount(processString(rs.getString("pm_cb_count")));
        setPmAvgTicket(processString(rs.getString("pm_avg_tkt")));
      }
    }
    catch(Exception e)
    {
      logEntry("loadData(" + getData("merchant") + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public void setCorporate(String data)
  {
    corporate = data;
  }
  public String getCorporate()
  {
    return corporate;
  }

  public void setRegion(String data)
  {
    region = data;
  }
  public String getRegion()
  {
    return region;
  }

  public void setPrincipal(String data)
  {
    principal = data;
  }
  public String getPrincipal()
  {
    return principal;
  }

  public void setAssociate(String data)
  {
    associate = data;
  }
  public String getAssociate()
  {
    return associate;
  }

  public void setChain(String data)
  {
    chain = data;
  }
  public String getChain()
  {
    return chain;
  }

  public void setMerchantName(String data)
  {
    merchantName = data;
  }
  public String getMerchantName()
  {
    return merchantName;
  }

  public void setDateLastActivity(String data)
  {
    dateLastActivity = data;
  }
  public String getDateLastActivity()
  {
    return dateLastActivity;
  }

  public void setEmailAddress(String data)
  {
    emailAddress = data;
  }
  public String getEmailAddress()
  {
    return emailAddress;
  }

  public void setSalesRep(String data)
  {
    salesRep = data;
  }
  public String getSalesRep()
  {
    return salesRep;
  }
  
  public void setSalesRep2(String data)
  {
    salesRep2 = data;
  }
  public String getSalesRep2()
  {
    return salesRep2;
  }
  
  public void setSpecComm(String data)
  {
    specComm = data;
  }
  public String getSpecComm()
  {
    return specComm;
  }

  public void setSignedNetVol(String data)
  {
    signedNetVol = data;
  }
  public String getSignedNetVol()
  {
    return signedNetVol;
  }

  public void setSignedAvgTicket(String data)
  {
    signedAvgTicket = data;
  }
  public String getSignedAvgTicket()
  {
    return signedAvgTicket;
  }

  public void setMtdNetVolume(String data)
  {
    mtdNetVolume = data;
  }
  public String getMtdNetVolume()
  {
    return mtdNetVolume;
  }

  public void setMtdDepositCount(String data)
  {
    mtdDepositCount = data;
  }
  public String getMtdDepositCount()
  {
    return mtdDepositCount;
  }

  public void setMtdSalesVol(String data)
  {
    mtdSalesVol = data;
  }
  public String getMtdSalesVol()
  {
    return mtdSalesVol;
  }

  public void setMtdSalesCount(String data)
  {
    mtdSalesCount = data;
  }
  public String getMtdSalesCount()
  {
    return mtdSalesCount;
  }

  public void setMtdCreditVol(String data)
  {
    mtdCreditVol = data;
  }
  public String getMtdCreditVol()
  {
    return mtdCreditVol;
  }

  public void setMtdCreditCount(String data)
  {
    mtdCreditCount = data;
  }
  public String getMtdCreditCount()
  {
    return mtdCreditCount;
  }

  public void setMtdChargebackVol(String data)
  {
    mtdChargebackVol = data;
  }
  public String getMtdChargebackVol()
  {
    return mtdChargebackVol;
  }

  public void setMtdChargebackCount(String data)
  {
    mtdChargebackCount = data;
  }
  public String getMtdChargebackCount()
  {
    return mtdChargebackCount;
  }

  public void setMtdAvgTicket(String data)
  {
    mtdAvgTicket = data;
  }
  public String getMtdAvgTicket()
  {
    return mtdAvgTicket;
  }

  public void setAttention(String data)
  {
    attention = data;
  }
  public String getAttention()
  {
    return attention;
  }

  public void setAddress1(String data)
  {
    address1 = data;
  }
  public String getAddress1()
  {
    return address1;
  }

  public void setAddress2(String data)
  {
    address2 = data;
  }
  public String getAddress2()
  {
    return address2;
  }

  public void setCity(String data)
  {
    city = data;
  }
  public String getCity()
  {
    return city;
  }

  public void setState(String data)
  {
    state = data;
  }
  public String getState()
  {
    return state;
  }

  public void setZipCode(String data)
  {
    zipCode = data;
  }
  public String getZipCode()
  {
    return zipCode;
  }

  public void setCountry(String data)
  {
    country = data;
  }
  public String getCountry()
  {
    return country;
  }

  public void setMerchantContact(String data)
  {
    merchantContact = data;
  }
  public String getMerchantContact()
  {
    return merchantContact;
  }

  public void setSicCode(String data)
  {
    sicCode = data;
  }
  public String getSicCode()
  {
    return sicCode;
  }

  public void setPmNetVol(String data)
  {
    pmNetVol = data;
  }
  public String getPmNetVol()
  {
    return pmNetVol;
  }

  public void setPmDepositCount(String data)
  {
    pmDepositCount = data;
  }
  public String getPmDepositCount()
  {
    return pmDepositCount;
  }

  public void setPmSalesVol(String data)
  {
    pmSalesVol = data;
  }
  public String getPmSalesVol()
  {
    return pmSalesVol;
  }

  public void setPmSalesCount(String data)
  {
    pmSalesCount = data;
  }
  public String getPmSalesCount()
  {
    return pmSalesCount;
  }

  public void setPmCreditVol(String data)
  {
    pmCreditVol = data;
  }
  public String getPmCreditVol()
  {
    return pmCreditVol;
  }

  public void setPmCreditCount(String data)
  {
    pmCreditCount = data;
  }
  public String getPmCreditCount()
  {
    return pmCreditCount;
  }

  public void setPmChargebackVol(String data)
  {
    pmChargebackVol = data;
  }
  public String getPmChargebackVol()
  {
    return pmChargebackVol;
  }

  public void setPmChargebackCount(String data)
  {
    pmChargebackCount = data;
  }
  public String getPmChargebackCount()
  {
    return pmChargebackCount;
  }

  public void setPmAvgTicket(String data)
  {
    pmAvgTicket = data;
  }
  public String getPmAvgTicket()
  {
    return pmAvgTicket;
  }
  
  public void setOpenDate(String data)
  {
    openDate = data;
  }
  public String getOpenDate()
  {
    return openDate;
  }
}/*@lineinfo:generated-code*/