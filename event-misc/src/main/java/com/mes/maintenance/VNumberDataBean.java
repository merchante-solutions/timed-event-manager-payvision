/*@lineinfo:filename=VNumberDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/VNumberDataBean.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jkirton $
  Last Modified Date : $Date: 9/24/03 6:29p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import java.util.Locale;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;


public class VNumberDataBean extends SQLJConnectionBase
{
  private   long    appSeqNum         = 0L;
  private   Vector  vnumbers          = new Vector();
  public    String  controlNumber     = "";
  public    String  dbaName           = "";
  public    String  merchantNumber    = "";
  
  public void setAppSeqNum(String appSeqNum)
  {
    try
    {
      this.appSeqNum = Long.parseLong(appSeqNum);
    }
    catch(Exception e)
    {
      logEntry("setAppSeqNum(" + appSeqNum + ")", e.toString());
    }
  }
  
  public void getData()
  {
    ResultSetIterator     it      = null;
    try
    {
      connect();
      
      // get the app details
      /*@lineinfo:generated-code*//*@lineinfo:66^7*/

//  ************************************************************
//  #sql [Ctx] { select  merc_cntrl_number,
//                  merch_business_name,
//                  merch_number
//          
//          from    merchant
//          where   app_seq_num = :this.appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merc_cntrl_number,\n                merch_business_name,\n                merch_number\n         \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.VNumberDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,this.appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   this.controlNumber = (String)__sJT_rs.getString(1);
   this.dbaName = (String)__sJT_rs.getString(2);
   this.merchantNumber = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:76^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:78^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num,
//                  terminal_number,
//                  vnumber,
//                  tid,
//                  store_number,
//                  time_zone,
//                  location_number,
//                  app_code,
//                  equip_model,
//                  nvl(epg.name,'MMS')  pgn,
//                  mv.profile_completed_date pcd
//          from    merch_vnumber mv
//                  ,equip_profile_generators epg
//          where   app_seq_num = :this.appSeqNum
//                  and mv.profile_generator_code = epg.code(+)
//          order by store_number, terminal_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num,\n                terminal_number,\n                vnumber,\n                tid,\n                store_number,\n                time_zone,\n                location_number,\n                app_code,\n                equip_model,\n                nvl(epg.name,'MMS')  pgn,\n                mv.profile_completed_date pcd\n        from    merch_vnumber mv\n                ,equip_profile_generators epg\n        where   app_seq_num =  :1 \n                and mv.profile_generator_code = epg.code(+)\n        order by store_number, terminal_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.VNumberDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,this.appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.VNumberDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:96^7*/
      
      ResultSet rs = it.getResultSet();
      
      while(rs.next())
      {
        String pcd = ((rs.getString("pcd") == null)?  
                      "---":java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("pcd")));
        
        vnumbers.add(new VNumberData( rs.getString("app_seq_num"),
                                      rs.getString("terminal_number"),
                                      rs.getString("vnumber"),
                                      rs.getString("tid"),
                                      rs.getString("store_number"),
                                      rs.getString("time_zone"),
                                      rs.getString("location_number"),
                                      rs.getString("app_code"),
                                      rs.getString("equip_model"),
                                      rs.getString("pgn"),
                                      pcd
                                     ));
      }
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public Vector getVNumbers()
  {
    return this.vnumbers;
  }
  
  /*
  ** NESTED CLASS VNumberData
  */
  public class VNumberData
  {
    public String   appSeqNum         = "";
    public String   terminalNumber    = "";
    public String   vNumber           = "";
    public String   tid               = "";
    public String   storeNumber       = "";
    public String   timeZone          = "";
    public String   locationNumber    = "";
    public String   appCode           = "";
    public String   equipModel        = "";
    public String   profGenName       = "";
    public String   profCmpltnDate    = "";
    
    public VNumberData()
    {
    }
    
    public VNumberData( String  appSeqNum,
                        String  terminalNumber,
                        String  vNumber,
                        String  tid,
                        String  storeNumber,
                        String  timeZone,
                        String  locationNumber,
                        String  appCode,
                        String  equipModel,
                        String  profGenName,
                        String  profCmpltnDate
                       )
    {
      this.appSeqNum      = processStringField(appSeqNum);
      this.terminalNumber = processStringField(terminalNumber);
      this.vNumber        = processStringField(vNumber);
      this.tid            = processStringField(tid);
      this.storeNumber    = processStringField(storeNumber);
      this.timeZone       = processStringField(timeZone);
      this.locationNumber = processStringField(locationNumber);
      this.appCode        = processStringField(appCode);
      this.equipModel     = processStringField(equipModel);
      this.profGenName    = processStringField(profGenName);
      this.profCmpltnDate = processStringField(profCmpltnDate);
    }
                        
    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }
  }
}/*@lineinfo:generated-code*/