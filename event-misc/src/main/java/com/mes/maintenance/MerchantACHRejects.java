/*@lineinfo:filename=MerchantACHRejects*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/MerchantACHRejects.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 4/30/03 11:43a $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.maintenance;

import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesQueues;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;


public class MerchantACHRejects extends SQLJConnectionBase
{
  public String   merchantNumber;
  public long     numericMerchantNumber;
  public Vector   achRejects;
  
  public MerchantACHRejects()
  {
    achRejects            = new Vector();
    merchantNumber        = "";
    numericMerchantNumber = 0L;
  }
  
  public void setProperties(HttpServletRequest request)
  {
    // get merchant number
    try
    {
      merchantNumber        = HttpHelper.getString(request, "merchant");
      numericMerchantNumber = Long.parseLong(merchantNumber);
    }
    catch(Exception e)
    {
      logEntry("setProperties(" + merchantNumber + ")", e.toString());
    }
  }
  
  public void loadData()
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:77^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                   id,
//                  qd.type                 type,
//                  qd.item_type            item_type,
//                  ar.settled_date         date_created,
//                  qd.last_changed         last_changed,
//                  qd.last_user            last_user,
//                  ar.reject_seq_num       control,
//                  qd.locked_by            locked_by,
//                  get_queue_note_count(ar.reject_seq_num,qd.type)   note_count,
//                  qt.description          type_desc,
//                  ar.merchant_number      merchant_number,
//                  ar.reason_code          reason_code,
//                  arrc.description        code_description,
//                  decode( ar.amount,
//                    0, '$0.00',
//                    decode( ar.amount/abs(ar.amount),
//                      -1, ( '(' || ltrim(rtrim(to_char(abs(ar.amount),'$999999990.00'))) || ')' ),
//                      ltrim(rtrim(to_char(ar.amount,'$999999990.00'))) ) )       amount,
//                  decode(ars.reject_status, null, 'NEW', ars.reject_status) reject_status,
//                  ar.transit_routing      transit_routing_num,
//                  ar.dda                  dda_num
//          from    q_data                  qd,
//                  q_types                 qt,
//                  q_group_to_types        qtt,
//                  ach_rejects             ar,
//                  ach_reject_reason_codes arrc,
//                  ach_reject_status       ars
//          where   qd.id                 = ar.reject_seq_num       and
//                  ar.reject_seq_num     = ars.reject_seq_num(+)   and
//                  ar.reason_code        = arrc.reason_code(+)     and
//                  qd.type               = qt.type                 and
//                  qt.type               = qtt.queue_type          and
//                  qtt.group_type        = :MesQueues.QG_ACH_REJECT_QUEUES and
//                  ar.merchant_number    = :numericMerchantNumber
//          group by qd.id,
//                   qd.type,
//                   qd.item_type,
//                   ar.settled_date,
//                   qd.last_changed,
//                   qd.last_user,
//                   ar.reject_seq_num,
//                   qd.locked_by,
//                   qt.description,
//                   ar.merchant_number,
//                   ar.reason_code,
//                   arrc.description,
//                   ar.amount,
//                   ars.reject_status,
//                   ar.transit_routing,
//                   ar.dda
//          order by ar.settled_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id                   id,\n                qd.type                 type,\n                qd.item_type            item_type,\n                ar.settled_date         date_created,\n                qd.last_changed         last_changed,\n                qd.last_user            last_user,\n                ar.reject_seq_num       control,\n                qd.locked_by            locked_by,\n                get_queue_note_count(ar.reject_seq_num,qd.type)   note_count,\n                qt.description          type_desc,\n                ar.merchant_number      merchant_number,\n                ar.reason_code          reason_code,\n                arrc.description        code_description,\n                decode( ar.amount,\n                  0, '$0.00',\n                  decode( ar.amount/abs(ar.amount),\n                    -1, ( '(' || ltrim(rtrim(to_char(abs(ar.amount),'$999999990.00'))) || ')' ),\n                    ltrim(rtrim(to_char(ar.amount,'$999999990.00'))) ) )       amount,\n                decode(ars.reject_status, null, 'NEW', ars.reject_status) reject_status,\n                ar.transit_routing      transit_routing_num,\n                ar.dda                  dda_num\n        from    q_data                  qd,\n                q_types                 qt,\n                q_group_to_types        qtt,\n                ach_rejects             ar,\n                ach_reject_reason_codes arrc,\n                ach_reject_status       ars\n        where   qd.id                 = ar.reject_seq_num       and\n                ar.reject_seq_num     = ars.reject_seq_num(+)   and\n                ar.reason_code        = arrc.reason_code(+)     and\n                qd.type               = qt.type                 and\n                qt.type               = qtt.queue_type          and\n                qtt.group_type        =  :1  and\n                ar.merchant_number    =  :2 \n        group by qd.id,\n                 qd.type,\n                 qd.item_type,\n                 ar.settled_date,\n                 qd.last_changed,\n                 qd.last_user,\n                 ar.reject_seq_num,\n                 qd.locked_by,\n                 qt.description,\n                 ar.merchant_number,\n                 ar.reason_code,\n                 arrc.description,\n                 ar.amount,\n                 ars.reject_status,\n                 ar.transit_routing,\n                 ar.dda\n        order by ar.settled_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.MerchantACHRejects",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.QG_ACH_REJECT_QUEUES);
   __sJT_st.setLong(2,numericMerchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.MerchantACHRejects",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:130^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        achRejects.add(new ACHRejectDetail(rs));
      }
    }
    catch(Exception e)
    {
      logEntry("loadData(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public Vector getACHRejects()
  {
    return achRejects;
  }
  
  public class ACHRejectDetail
  {
    public String   merchantNumber;
    public String   rejectDate;
    public String   control;
    public String   lastWorkedDate;
    public String   lastWorkedBy;
    public String   lockedBy;
    public String   notes;
    public String   transitRouting;
    public String   dda;
    public String   rejectCode;
    public String   rejectCodeDesc;
    public String   rejectAmount;
    public String   rejectStatus;
    public String   queue;
    public String   queueType;
    public String   itemType;
    
    public ACHRejectDetail(ResultSet rs)
    {
      try
      {
        merchantNumber  = rs.getString("merchant_number");
        rejectDate      = DateTimeFormatter.getFormattedDate(rs.getDate("date_created"), "MM/dd/yy");
        control         = rs.getString("id");
        lastWorkedDate  = (rs.getDate("last_changed") == null ? rejectDate : DateTimeFormatter.getFormattedDate(rs.getDate("last_changed"), "MM/dd/yy"));
        lastWorkedBy    = rs.getString("last_user");
        lockedBy        = blankIfNull(rs.getString("locked_by"));
        notes           = rs.getString("note_count");
        transitRouting  = rs.getString("transit_routing_num");
        dda             = rs.getString("dda_num");
        rejectCode      = rs.getString("reason_code");
        rejectCodeDesc  = rs.getString("code_description");
        rejectAmount    = rs.getString("amount");
        rejectStatus    = rs.getString("reject_status");
        queue           = rs.getString("type_desc");
        queueType       = rs.getString("type");
        itemType        = rs.getString("item_type");
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::constructor()", e.toString());
      }
    }
    
  }
}/*@lineinfo:generated-code*/