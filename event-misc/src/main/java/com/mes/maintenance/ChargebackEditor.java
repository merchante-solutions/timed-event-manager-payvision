/*@lineinfo:filename=ChargebackEditor*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/ChargebackEditor.sqlj $

  Description:  
  
    Support bean for viewing/editing risk scoring criteria
    
  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 8/04/04 3:29p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import javax.servlet.http.HttpServletRequest;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.forms.Field;
import com.mes.forms.Validation;
import com.mes.tools.ChargebackTools;

public class ChargebackEditor extends GenericEditor
{
  
  /**
   * Log4J Logger
   */
  static Logger log = Logger.getLogger(ChargebackEditor.class);
  
  public class MerchantOnlyValidation implements Validation
  {
    String    ErrorMessage  = null;
    
    public MerchantOnlyValidation( )
    {
    }
    
    public String getErrorText()
    {
      return( (ErrorMessage == null) ? "" : ErrorMessage );
    }
    
    public boolean validate(String fieldData)
    {
      int         recCount      = 0;
      
      try
      {
        connect();
        
        ErrorMessage = null;
        
        /*@lineinfo:generated-code*//*@lineinfo:93^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(mf.merchant_number) 
//            from    mif                   mf 
//            where   mf.merchant_number = :fieldData
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mf.merchant_number)  \n          from    mif                   mf \n          where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.ChargebackEditor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fieldData);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:98^9*/
        if ( recCount == 0 )
        {
          ErrorMessage = "Invalid merchant account number";  
        }
      }
      catch( Exception e )
      {
        // ingore and consider this to be failed validation
        ErrorMessage = "Error: " + e.toString();
      }
      finally
      {
        cleanUp();
      }
      return( ErrorMessage == null );
    }
  }

  public ChargebackEditor()
  {
  }
  
  public void initEditorFields( HttpServletRequest request )
  {
    super.initEditorFields(request);
    
    NameMap map     = getFieldNameMap();
    
    // add extra validation to insure that this is a
    // merchant number (not just a hierarchy node)
    EditorFields.getField(map.getFieldName("merchant_number")).addValidation( new MerchantOnlyValidation() );
  }
  
  public void store( )
  {
    int                     bankNumber  = 0;
    String                  dbaName     = null;
    Field                   field       = null;
    NameMap                 map         = null;
    long                    merchantId  = 0L;
    int                     queueType   = 0;
    
    try
    {
      if ( isValid() )
      {
        // the field name map allows use of the
        // database column name instead of the 
        // generic ("field" + display id) name.
        map = getFieldNameMap();
      
        field = EditorFields.getField(map.getFieldName("cb_load_sec"));
      
        // if this is a new chargeback being added
        // then go to FTPDB and get the next primary
        // key value and set the load filename to system
        if ( field != null && field.asLong() == 0L )
        {
          field.setData(Long.toString(ChargebackTools.getNewLoadSec()));     // set cb_load_sec
        
          // fill in the other necessary hidden fields (all 9xx)
          EditorFields.setData(map.getFieldName("load_filename"),"system");    // set load_filename
        
          // 902, bank number
          // 903, dba name
          merchantId = EditorFields.getField(map.getFieldName("merchant_number")).asLong();
          /*@lineinfo:generated-code*//*@lineinfo:165^11*/

//  ************************************************************
//  #sql [Ctx] { select  mf.bank_number,mf.dba_name 
//              from    mif     mf
//              where   mf.merchant_number = :merchantId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mf.bank_number,mf.dba_name  \n            from    mif     mf\n            where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.ChargebackEditor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   dbaName = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:170^11*/
          EditorFields.setData(map.getFieldName("bank_number"),Integer.toString(bankNumber));
          EditorFields.setData(map.getFieldName("merchant_name"),dbaName);
        
          // 904, debit credit indicator
          EditorFields.setData(map.getFieldName("debit_credit_ind"),
                         ((EditorFields.getField(map.getFieldName("tran_amount")).asDouble() < 0.0) ? "C" : "D"));
        }
        super.store();
        
        // item should be added now, so add it to the queue
        
        //First Time status can be C, N, P, Y, A
        int status = decodeStatus(EditorFields.getData(map.getFieldName("first_time_chargeback")));
        
        if(status==1) // 1 lookup value, using reason code
        {
          /*@lineinfo:generated-code*//*@lineinfo:187^11*/

//  ************************************************************
//  #sql [Ctx] { select get_cb_queue_assignment( :EditorFields.getData(map.getFieldName("bank_number")),
//                                              :EditorFields.getData(map.getFieldName("merchant_number")),
//                                              :EditorFields.getData(map.getFieldName("card_number")),
//                                              :EditorFields.getData(map.getFieldName("reason_code")),
//                                              1,
//                                              null  -- always incoming
//                                            )
//              
//              from dual                                        
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_821 = EditorFields.getData(map.getFieldName("bank_number"));
 String __sJT_822 = EditorFields.getData(map.getFieldName("merchant_number"));
 String __sJT_823 = EditorFields.getData(map.getFieldName("card_number"));
 String __sJT_824 = EditorFields.getData(map.getFieldName("reason_code"));
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select get_cb_queue_assignment(  :1 ,\n                                             :2 ,\n                                             :3 ,\n                                             :4 ,\n                                            1,\n                                            null  -- always incoming\n                                          )\n             \n            from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.ChargebackEditor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_821);
   __sJT_st.setString(2,__sJT_822);
   __sJT_st.setString(3,__sJT_823);
   __sJT_st.setString(4,__sJT_824);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   queueType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:198^11*/
        }
        else if(status==2) //2 lookup value - reason code doesn't matter
        {
          //this assumes that if a user selects 'N' for first-time field, it means '2nd time'
          /*@lineinfo:generated-code*//*@lineinfo:203^11*/

//  ************************************************************
//  #sql [Ctx] { select get_cb_queue_assignment( :EditorFields.getData(map.getFieldName("bank_number")),
//                                              :EditorFields.getData(map.getFieldName("merchant_number")),
//                                              :EditorFields.getData(map.getFieldName("card_number")),
//                                              null,
//                                              2,
//                                              null  -- always incoming
//                                            )
//              
//              from dual                                        
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_825 = EditorFields.getData(map.getFieldName("bank_number"));
 String __sJT_826 = EditorFields.getData(map.getFieldName("merchant_number"));
 String __sJT_827 = EditorFields.getData(map.getFieldName("card_number"));
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select get_cb_queue_assignment(  :1 ,\n                                             :2 ,\n                                             :3 ,\n                                            null,\n                                            2,\n                                            null  -- always incoming\n                                          )\n             \n            from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.ChargebackEditor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_825);
   __sJT_st.setString(2,__sJT_826);
   __sJT_st.setString(3,__sJT_827);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   queueType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:214^11*/
        }
        else if(status==3) //Compliance - auto into compliance queue
        {
          queueType = MesQueues.Q_CHARGEBACKS_PRE_COMPLIANCE;
        }
        else if(status==4) //Amex - auto into amex queue
        {
          queueType = MesQueues.Q_CHARGEBACKS_AMEX;
        }

        
        if ( queueType != 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:228^11*/

//  ************************************************************
//  #sql [Ctx] { insert into q_data
//              (
//                type,
//                id,
//                item_type,
//                owner,
//                date_created,
//                source,
//                affiliate   
//              )
//              values
//              (
//                :queueType,
//                :EditorFields.getData(map.getFieldName("cb_load_sec")),
//                18,
//                1,
//                sysdate,
//                :User.getLoginName(),
//                :EditorFields.getData(map.getFieldName("bank_number"))
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_828 = EditorFields.getData(map.getFieldName("cb_load_sec"));
 String __sJT_829 = User.getLoginName();
 String __sJT_830 = EditorFields.getData(map.getFieldName("bank_number"));
   String theSqlTS = "insert into q_data\n            (\n              type,\n              id,\n              item_type,\n              owner,\n              date_created,\n              source,\n              affiliate   \n            )\n            values\n            (\n               :1 ,\n               :2 ,\n              18,\n              1,\n              sysdate,\n               :3 ,\n               :4 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.maintenance.ChargebackEditor",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,queueType);
   __sJT_st.setString(2,__sJT_828);
   __sJT_st.setString(3,__sJT_829);
   __sJT_st.setString(4,__sJT_830);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:250^11*/
        }
      }
    }
    catch( Exception e )
    {
      logEntry("store()",e.toString());
    }
    finally
    {
    }
  }
  
  private int decodeStatus(String status){
    
    int statusInt = 1;
    if(status!=null)
    {
      if(status.equalsIgnoreCase("N")||status.equalsIgnoreCase("P"))
      {
        statusInt=2;
      }
      else if (status.equalsIgnoreCase("C"))
      {
        statusInt=3;
      }
      else if (status.equalsIgnoreCase("A"))
      {
        statusInt=4;
      }
    }
    return statusInt;
  }

}/*@lineinfo:generated-code*/