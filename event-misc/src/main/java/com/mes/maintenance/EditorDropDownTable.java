/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/EditorDropDownTable.java $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 9/23/03 3:45p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.mes.tools.DropDownTable;

public class EditorDropDownTable extends DropDownTable
{
  public EditorDropDownTable( ResultSet rowData )
  {
    ResultSet           resultSet     = null;
    StringBuffer        sqlBuffer     = new StringBuffer();
    PreparedStatement   statement     = null;
  
    try
    {
      connect();
      
      // build the statement
      sqlBuffer.append( "select " );
      sqlBuffer.append( rowData.getString("lookup_col") );
      sqlBuffer.append( ",\n       " );
      sqlBuffer.append( rowData.getString("lookup_desc_col") );
      sqlBuffer.append( "\n" );
      sqlBuffer.append( "from " );
      sqlBuffer.append( rowData.getString("lookup_table") );
      sqlBuffer.append( "\n" );
      sqlBuffer.append( "order by " );
      sqlBuffer.append( rowData.getString("lookup_desc_col") );
  
      statement = getPreparedStatement( sqlBuffer.toString() );
      resultSet = statement.executeQuery();
      
      while( resultSet.next() )
      {
        addElement( resultSet );
      }
      resultSet.close();
      statement.close();
    }      
    catch( Exception e )
    {
      logEntry("EditorDropDownTable()",e.toString());
    }
    finally
    {
      try{ statement.close(); }catch(Exception ee){}
      cleanUp();
    }
  }
}
