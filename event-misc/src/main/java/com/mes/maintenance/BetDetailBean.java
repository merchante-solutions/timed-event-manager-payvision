/*@lineinfo:filename=BetDetailBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/BetDetailBean.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/14/02 12:59p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.NumberFormatter;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;


public class BetDetailBean extends SQLJConnectionBase
{
  public final static int   BET_INDIVIDUAL_PLAN     = 1;
  public final static int   BET_INTERCHANGE         = 2;
  public final static int   BET_DEBIT               = 3;
  public final static int   BET_DATA_CAPTURE        = 4;
  public final static int   BET_AUTHORIZATION       = 5;
  public final static int   BET_SYSTEM_GENERATED    = 6;

  public int    bankNumber      = 0;
  public String betDescription  = "";
  public int    betType         = 0;
  public int    betSubType      = 0;
  public int    betNumber       = 0;

  public  Vector  betDetails    = new Vector();

  public BetDetailBean()
  {
  }

  public BetDetailBean(String connectionString)
  {
    super(connectionString);
  }

  public void setProperties(HttpServletRequest request, UserBean user)
  {
    try
    {
      // fill local variables
      bankNumber  = HttpHelper.getInt(request, "bank");
      betType     = HttpHelper.getInt(request, "type");
      betSubType  = HttpHelper.getInt(request, "subType");
      betNumber   = HttpHelper.getInt(request, "bet");

      // register this bean with MesSystem
      register(user.getLoginName(), request);

      // perform the necessary queries based on passed data
      loadData();
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
  }

  private void loadData()
  {
    ResultSetIterator     it              = null;
    ResultSet             rs              = null;
    String                description     = "";
    StringBuffer          betDesc         = new StringBuffer("");

    try
    {
      connect();

      if(betSubType != 0)
      {
        try
        {
          // get the sub type first
          /*@lineinfo:generated-code*//*@lineinfo:108^11*/

//  ************************************************************
//  #sql [Ctx] { select  description
//              
//              from    tsys_bet_subtypes
//              where   bet_subtype = :betSubType
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  description\n             \n            from    tsys_bet_subtypes\n            where   bet_subtype =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.BetDetailBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,betSubType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   description = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:114^11*/
        }
        catch(Exception e)
        {
        }

        betDesc.append(description);
        betDesc.append(" ");
      }

      // get the bet table generic description
      /*@lineinfo:generated-code*//*@lineinfo:125^7*/

//  ************************************************************
//  #sql [Ctx] { select  description
//          
//          from    tsys_bet_types
//          where   bet_type = :betType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  description\n         \n        from    tsys_bet_types\n        where   bet_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.BetDetailBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,betType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   description = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:131^7*/

      betDesc.append(description);

      betDescription = betDesc.toString();

      switch(betType)
      {
        case BET_INDIVIDUAL_PLAN:
          /*@lineinfo:generated-code*//*@lineinfo:140^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  tb.bet_type             bet_type,
//                      tbt.description         bet_description,
//                      tb.bet_number           bet_number,
//                      tb.last_maint_date      last_maint_date,
//                      tb.enabled              active,
//                      tb.segment_id           segment_id,
//                      tbsd.segment_desc       segment_desc,
//                      tb.expense_rate         expense_rate,
//                      tb.expense_per_item     expense_per_item,
//                      tb.expense_fixed        expense_fixed,
//                      tb.expense_freq_mask    expense_freq_mask,
//                      tb.expense_last_maint   expense_last_maint,
//                      tb.expense_expiration   expense_expiration,
//                      tb.income_rate          income_rate,
//                      tb.income_per_item      income_per_item,
//                      tb.income_fixed         income_fixed,
//                      tb.income_freq_mask     income_freq_mask,
//                      tb.income_last_maint    income_last_maint,
//                      tb.income_expiration    income_expiration,
//                      tb.statement_message    statement_message,
//                      tb.client_message       client_message,
//                      tb.print_option         statement_indicator,
//                      tb.print_group          combination_indicator,
//                      tbseg.description       segment_description,
//                      tb.ip_plan_type         plan_type
//              from    tsys_bet_file           tb,
//                      tsys_bet_types          tbt,
//                      tsys_bet_segment_desc   tbsd,
//                      tsys_bet_segment_types  tbseg
//              where   tb.active_date  = trunc((trunc(sysdate-2, 'month') - 1),'month') and
//                      tb.bank_number  = :bankNumber and
//                      tb.bet_type     = :betType and
//                      tb.bet_number   = :betNumber and
//                      tb.bet_type     = tbt.bet_type and
//                      tb.bet_type     = tbsd.bet_type(+) and
//                      tb.segment_id   = tbsd.segment_id(+) and
//                      tb.segment_type = tbseg.segment_type(+)
//                      
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tb.bet_type             bet_type,\n                    tbt.description         bet_description,\n                    tb.bet_number           bet_number,\n                    tb.last_maint_date      last_maint_date,\n                    tb.enabled              active,\n                    tb.segment_id           segment_id,\n                    tbsd.segment_desc       segment_desc,\n                    tb.expense_rate         expense_rate,\n                    tb.expense_per_item     expense_per_item,\n                    tb.expense_fixed        expense_fixed,\n                    tb.expense_freq_mask    expense_freq_mask,\n                    tb.expense_last_maint   expense_last_maint,\n                    tb.expense_expiration   expense_expiration,\n                    tb.income_rate          income_rate,\n                    tb.income_per_item      income_per_item,\n                    tb.income_fixed         income_fixed,\n                    tb.income_freq_mask     income_freq_mask,\n                    tb.income_last_maint    income_last_maint,\n                    tb.income_expiration    income_expiration,\n                    tb.statement_message    statement_message,\n                    tb.client_message       client_message,\n                    tb.print_option         statement_indicator,\n                    tb.print_group          combination_indicator,\n                    tbseg.description       segment_description,\n                    tb.ip_plan_type         plan_type\n            from    tsys_bet_file           tb,\n                    tsys_bet_types          tbt,\n                    tsys_bet_segment_desc   tbsd,\n                    tsys_bet_segment_types  tbseg\n            where   tb.active_date  = trunc((trunc(sysdate-2, 'month') - 1),'month') and\n                    tb.bank_number  =  :1  and\n                    tb.bet_type     =  :2  and\n                    tb.bet_number   =  :3  and\n                    tb.bet_type     = tbt.bet_type and\n                    tb.bet_type     = tbsd.bet_type(+) and\n                    tb.segment_id   = tbsd.segment_id(+) and\n                    tb.segment_type = tbseg.segment_type(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.BetDetailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,betType);
   __sJT_st.setInt(3,betNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.maintenance.BetDetailBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:180^11*/
          break;

        case BET_INTERCHANGE:
          /*@lineinfo:generated-code*//*@lineinfo:184^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  tb.bet_type             bet_type,
//                      tbt.description         bet_description,
//                      tb.bet_number           bet_number,
//                      tb.last_maint_date      last_maint_date,
//                      tb.enabled              active,
//                      tb.segment_id           segment_id,
//                      tbsd.segment_desc       segment_desc,
//                      tb.expense_rate         expense_rate,
//                      tb.expense_per_item     expense_per_item,
//                      tb.expense_fixed        expense_fixed,
//                      tb.expense_freq_mask    expense_freq_mask,
//                      tb.expense_last_maint   expense_last_maint,
//                      tb.expense_expiration   expense_expiration,
//                      tb.income_rate          income_rate,
//                      tb.income_per_item      income_per_item,
//                      tb.income_fixed         income_fixed,
//                      tb.income_freq_mask     income_freq_mask,
//                      tb.income_last_maint    income_last_maint,
//                      tb.income_expiration    income_expiration,
//                      tb.statement_message    statement_message,
//                      tb.client_message       client_message,
//                      tb.print_option         statement_indicator,
//                      tb.print_group          combination_indicator,
//                      tbseg.description       segment_description,
//                      tbst.description        subtype_description
//              from    tsys_bet_file           tb,
//                      tsys_bet_types          tbt,
//                      tsys_bet_segment_desc   tbsd,
//                      tsys_bet_segment_types  tbseg,
//                      tsys_bet_subtypes       tbst
//              where   tb.active_date    = trunc((trunc(sysdate-2, 'month') - 1),'month') and
//                      tb.bank_number    = :bankNumber and
//                      tb.bet_type       = :betType and
//                      tb.bet_number     = :betNumber and
//                      tb.bet_type       = tbt.bet_type and
//                      tbst.bet_subtype  = :betSubType and
//                      tb.bet_type       = tbsd.bet_type(+) and
//                      tb.segment_id     = tbsd.segment_id(+) and
//                      tbsd.bet_subtype(+)  = :betSubType and
//                      tb.segment_type   = tbseg.segment_type(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tb.bet_type             bet_type,\n                    tbt.description         bet_description,\n                    tb.bet_number           bet_number,\n                    tb.last_maint_date      last_maint_date,\n                    tb.enabled              active,\n                    tb.segment_id           segment_id,\n                    tbsd.segment_desc       segment_desc,\n                    tb.expense_rate         expense_rate,\n                    tb.expense_per_item     expense_per_item,\n                    tb.expense_fixed        expense_fixed,\n                    tb.expense_freq_mask    expense_freq_mask,\n                    tb.expense_last_maint   expense_last_maint,\n                    tb.expense_expiration   expense_expiration,\n                    tb.income_rate          income_rate,\n                    tb.income_per_item      income_per_item,\n                    tb.income_fixed         income_fixed,\n                    tb.income_freq_mask     income_freq_mask,\n                    tb.income_last_maint    income_last_maint,\n                    tb.income_expiration    income_expiration,\n                    tb.statement_message    statement_message,\n                    tb.client_message       client_message,\n                    tb.print_option         statement_indicator,\n                    tb.print_group          combination_indicator,\n                    tbseg.description       segment_description,\n                    tbst.description        subtype_description\n            from    tsys_bet_file           tb,\n                    tsys_bet_types          tbt,\n                    tsys_bet_segment_desc   tbsd,\n                    tsys_bet_segment_types  tbseg,\n                    tsys_bet_subtypes       tbst\n            where   tb.active_date    = trunc((trunc(sysdate-2, 'month') - 1),'month') and\n                    tb.bank_number    =  :1  and\n                    tb.bet_type       =  :2  and\n                    tb.bet_number     =  :3  and\n                    tb.bet_type       = tbt.bet_type and\n                    tbst.bet_subtype  =  :4  and\n                    tb.bet_type       = tbsd.bet_type(+) and\n                    tb.segment_id     = tbsd.segment_id(+) and\n                    tbsd.bet_subtype(+)  =  :5  and\n                    tb.segment_type   = tbseg.segment_type(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.BetDetailBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,betType);
   __sJT_st.setInt(3,betNumber);
   __sJT_st.setInt(4,betSubType);
   __sJT_st.setInt(5,betSubType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.maintenance.BetDetailBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:226^11*/
          break;

        case BET_DEBIT:
          break;

        case BET_DATA_CAPTURE:
          break;

        case BET_AUTHORIZATION:
          break;

        case BET_SYSTEM_GENERATED:
          break;
      }

      rs = it.getResultSet();

      while(rs.next())
      {
        switch(betType)
        {
          case BET_INDIVIDUAL_PLAN:
            betDetails.add(new IndividualPlanBet(rs));
            break;

          case BET_INTERCHANGE:
            betDetails.add(new InterchangeBet(rs));
            break;

          case BET_DEBIT:
            break;

          case BET_DATA_CAPTURE:
            break;

          case BET_AUTHORIZATION:
            break;

          case BET_SYSTEM_GENERATED:
            break;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadData()", e.toString());
    }
    finally
    {

      cleanUp();
    }
  }

  /*
  ** NESTED CLASS BetDataBase
  */
  public class BetDataBase
  {
    public    String    betType;
    public    String    betNumber;
    public    String    lastMaintDate;
    public    String    active;
    public    String    segmentId;
    public    String    expenseRate;
    public    String    expensePerItem;
    public    String    expenseFixed;
    public    String    expenseFreqMask;
    public    String    expenseLastMaintDate;
    public    String    expenseExpirationDate;
    public    String    incomeRate;
    public    String    incomePerItem;
    public    String    incomeFixed;
    public    String    incomeFreqMask;
    public    String    incomeLastMaintDate;
    public    String    incomeExpirationDate;
    public    String    statementMessage;
    public    String    clientMessage;
    public    String    statementIndicator;
    public    String    combinationIndicator;
    public    String    segmentDescription;

    public BetDataBase()
    {
    }

    protected String processString(String input)
    {
      String result = "";

      // just make sure that the result is not a null string
      if(input == null || input.equals(""))
      {
        result = "&nbsp";
      }
      else
      {
        result = input;
      }

      return result;
    }

    protected String getRateString(double input)
    {
      StringBuffer result = new StringBuffer("");

      result.append(NumberFormatter.getPercentString(input/100, 4));

      return result.toString();
    }

    protected String getPerItemString(double input)
    {
      String result = "";

      result = NumberFormatter.getDoubleString(input, "$##0.000");

      return result;
    }

    protected String getExpenseString(double input)
    {
      String result = "";

      result = NumberFormatter.getDoubleString(input, NumberFormatter.CURRENCY_FORMAT);

      return result;
    }

    protected String getSegmentString(String description, String segment)
    {
      StringBuffer result = new StringBuffer("");
      
      int seg;
      
      try
      {
        seg = Integer.parseInt(segment);
      }
      catch(Exception e)
      {
        seg = 0;
      }

      result.append(processString(description));
      result.append(" (");
      result.append(NumberFormatter.getPaddedInt(seg, 3));
      result.append(")");

      return result.toString();
    }

    public BetDataBase(ResultSet rs)
    {
      try
      {
        // extract base data items
        betType               = processString(rs.getString("bet_description"));
        betNumber             = NumberFormatter.getPaddedInt(rs.getInt("bet_number"), 4);
        lastMaintDate         = DateTimeFormatter.getFormattedDate(rs.getDate("last_maint_date"), DateTimeFormatter.DEFAULT_DATE_FORMAT);
        active                = processString(rs.getString("active"));
        segmentId             = getSegmentString(rs.getString("segment_desc"), rs.getString("segment_id"));
        expenseRate           = getRateString(rs.getDouble("expense_rate"));
        expensePerItem        = getPerItemString(rs.getDouble("expense_per_item"));
        expenseFixed          = getExpenseString(rs.getDouble("expense_fixed"));
        expenseFreqMask       = processString(rs.getString("expense_freq_mask"));
        expenseLastMaintDate  = DateTimeFormatter.getFormattedDate(rs.getDate("expense_last_maint"), DateTimeFormatter.DEFAULT_DATE_FORMAT);
        expenseExpirationDate = DateTimeFormatter.getFormattedDate(rs.getDate("expense_expiration"), DateTimeFormatter.DEFAULT_DATE_FORMAT);
        incomeRate            = getRateString(rs.getDouble("income_rate"));
        incomePerItem         = getPerItemString(rs.getDouble("income_per_item"));
        incomeFixed           = getExpenseString(rs.getDouble("income_fixed"));
        incomeFreqMask        = processString(rs.getString("income_freq_mask"));
        incomeLastMaintDate   = DateTimeFormatter.getFormattedDate(rs.getDate("income_last_maint"), DateTimeFormatter.DEFAULT_DATE_FORMAT);
        incomeExpirationDate  = DateTimeFormatter.getFormattedDate(rs.getDate("income_expiration"), DateTimeFormatter.DEFAULT_DATE_FORMAT);
        statementMessage      = processString(rs.getString("statement_message"));
        clientMessage         = processString(rs.getString("client_message"));
        statementIndicator    = processString(rs.getString("statement_indicator"));
        combinationIndicator  = processString(rs.getString("combination_indicator"));
        segmentDescription    = processString(rs.getString("segment_description"));
      }
      catch(Exception e)
      {
        logEntry("BetDataBase constructor", e.toString());
      }
    }
  }

  public class IndividualPlanBet extends BetDataBase
  {
    public String   planType;

    public IndividualPlanBet()
    {
    }

    public IndividualPlanBet(ResultSet rs)
    {
      super(rs);

      try
      {
        this.planType = processString(rs.getString("plan_type"));
      }
      catch(Exception e)
      {
        logEntry("IndividualPlanBet constructor", e.toString());
      }
    }
  }

  public class InterchangeBet extends BetDataBase
  {
    public InterchangeBet()
    {
    }

    public InterchangeBet(ResultSet rs)
    {
      super(rs);

      try
      {
        // set the betType to include the subtype information
        this.betType          = processString(rs.getString("subtype_description")) +
                                " " +
                                processString(rs.getString("bet_description"));
      }
      catch(Exception e)
      {
        logEntry("InterchangeBet constructor", e.toString());
      }
    }
  }
}/*@lineinfo:generated-code*/