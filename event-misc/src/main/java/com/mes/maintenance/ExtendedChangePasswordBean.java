/*@lineinfo:filename=ExtendedChangePasswordBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/ExtendedChangePasswordBean.sqlj $

  Description:

    ExtendedChangePasswordBean

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/20/03 6:08p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.CityStateZipField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.PhoneField;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class ExtendedChangePasswordBean extends UserChangePasswordBean
{
  public ExtendedChangePasswordBean()
  {
  }
  
  public void setProperties(HttpServletRequest request, UserBean user)
  {
    try
    {
      // do the default
      super.setProperties(request,user);
      
      connect();
      
      if(!getData("Submit").equals("Submit"))
      {
        loadData();
      }
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void loadData()
  {
    ResultSetIterator   it = null;
    ResultSet           rs = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:76^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  u.name      name,
//                  u.address1  address1,
//                  u.address2  address2,
//                  u.city      user_csz_city,
//                  u.state     user_csz_state,
//                  u.zip       user_csz_zip,
//                  u.email     email,
//                  u.phone     phone,
//                  u.fax       fax,
//                  u.mobile    mobile,
//                  u.pager     pager,
//                  u.pager_pin pager_pin
//          from    users u
//          where   u.login_name = :user.getLoginName()  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_834 = user.getLoginName();
  try {
   String theSqlTS = "select  u.name      name,\n                u.address1  address1,\n                u.address2  address2,\n                u.city      user_csz_city,\n                u.state     user_csz_state,\n                u.zip       user_csz_zip,\n                u.email     email,\n                u.phone     phone,\n                u.fax       fax,\n                u.mobile    mobile,\n                u.pager     pager,\n                u.pager_pin pager_pin\n        from    users u\n        where   u.login_name =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.ExtendedChangePasswordBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_834);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.ExtendedChangePasswordBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:92^7*/
      
      rs = it.getResultSet();
      
      setFields(rs);
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::loadData(): " + e.toString());
      logEntry("loadData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  protected void addFields()
  {
    try
    {
      super.addFields();
      
      // set up form fields
      fields.add(new Field("name", "Name", 100, 40, false));
      fields.add(new Field("address1", 40, 40, false));
      fields.add(new Field("address2", 40, 40, true));
      fields.add(new CityStateZipField("userCsz", "City, State, Zip", 30, false));
      fields.add(new EmailField("email", "Email", 100, 40, true));
      fields.add(new PhoneField("phone", "Phone", false));
      fields.add(new PhoneField("fax", "Fax", true));
      fields.add(new PhoneField("mobile", "Mobile", true));
      fields.add(new PhoneField("pager", "Pager", true));
      fields.add(new Field("pagerPin", "Pager PIN", 10, 10, true));
      
      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      logEntry("addFields()", e.toString());
    }
  }
  
  protected void submitData()
  {
    try
    {
      super.submitData();
      
      // update user record
      /*@lineinfo:generated-code*//*@lineinfo:147^7*/

//  ************************************************************
//  #sql [Ctx] { update  users
//          set     name = :fields.getData("name"),
//                  address1 = :fields.getData("address1"),
//                  address2 = :fields.getData("address2"),
//                  city = :fields.getData("userCszCity"),
//                  state = :fields.getData("userCszState"),
//                  zip = :fields.getData("userCszZip"),
//                  email = :fields.getData("email"),
//                  phone = :fields.getData("phone"),
//                  fax = :fields.getData("fax"),
//                  mobile = :fields.getData("mobile"),
//                  pager = :fields.getData("pager"),
//                  pager_pin = :fields.getData("pagerPin")
//          where   login_name = :user.getLoginName()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_835 = fields.getData("name");
 String __sJT_836 = fields.getData("address1");
 String __sJT_837 = fields.getData("address2");
 String __sJT_838 = fields.getData("userCszCity");
 String __sJT_839 = fields.getData("userCszState");
 String __sJT_840 = fields.getData("userCszZip");
 String __sJT_841 = fields.getData("email");
 String __sJT_842 = fields.getData("phone");
 String __sJT_843 = fields.getData("fax");
 String __sJT_844 = fields.getData("mobile");
 String __sJT_845 = fields.getData("pager");
 String __sJT_846 = fields.getData("pagerPin");
 String __sJT_847 = user.getLoginName();
   String theSqlTS = "update  users\n        set     name =  :1 ,\n                address1 =  :2 ,\n                address2 =  :3 ,\n                city =  :4 ,\n                state =  :5 ,\n                zip =  :6 ,\n                email =  :7 ,\n                phone =  :8 ,\n                fax =  :9 ,\n                mobile =  :10 ,\n                pager =  :11 ,\n                pager_pin =  :12 \n        where   login_name =  :13";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.maintenance.ExtendedChangePasswordBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_835);
   __sJT_st.setString(2,__sJT_836);
   __sJT_st.setString(3,__sJT_837);
   __sJT_st.setString(4,__sJT_838);
   __sJT_st.setString(5,__sJT_839);
   __sJT_st.setString(6,__sJT_840);
   __sJT_st.setString(7,__sJT_841);
   __sJT_st.setString(8,__sJT_842);
   __sJT_st.setString(9,__sJT_843);
   __sJT_st.setString(10,__sJT_844);
   __sJT_st.setString(11,__sJT_845);
   __sJT_st.setString(12,__sJT_846);
   __sJT_st.setString(13,__sJT_847);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:163^7*/
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::submitData(): " + e.toString());
      logEntry("submitData()", e.toString());
    }
  }
}/*@lineinfo:generated-code*/