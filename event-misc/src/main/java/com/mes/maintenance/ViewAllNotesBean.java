/*@lineinfo:filename=ViewAllNotesBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/ViewAllNotesBean.sqlj $

  Description:
    Bean that allows viewing of all notes generated in the history of
    an application/account.  Includes service calls, queue notes, and
    risk/credit notes.  Default sorting is chronological, but can be
    changed to sort by category if necessary.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 11/05/03 12:04p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesQueues;
import com.mes.constants.MesUsers;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;


public class ViewAllNotesBean extends SQLJConnectionBase
{
  private long            appSeqNum       = -1;
  private long            merchantNumber  = -1;
  private UserBean        user            = null;
  private NoteComparator  nc              = new NoteComparator();
  private Vector          notes           = new Vector();
  private TreeSet         sortedNotes     = null;

  public void setProperties(HttpServletRequest request, UserBean user)
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      connect();
      
      this.user = user;
      
      // load merchant number if present
      merchantNumber = Long.parseLong(HttpHelper.getString(request, "merchant", "-1"));
      
      if(merchantNumber > 0)
      {
        // try to load an appseqnum
        /*@lineinfo:generated-code*//*@lineinfo:73^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num
//            from    merchant
//            where   merch_number = :merchantNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num\n          from    merchant\n          where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.ViewAllNotesBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.ViewAllNotesBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:78^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          appSeqNum = rs.getLong("app_seq_num");
        }
        
        rs.close();
        it.close();
      }
      
      // try to load an app seq num if no merchant number or app seq num was found
      if(merchantNumber == -1 && appSeqNum == -1)
      {
        appSeqNum = Long.parseLong(HttpHelper.getString(request, "appSeqNum", "-1"));
        
        if(appSeqNum > 0)
        {
          // load merchant number from app seq num
          /*@lineinfo:generated-code*//*@lineinfo:99^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(merch_number, -1) merchant_number
//              from    merchant
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(merch_number, -1) merchant_number\n            from    merchant\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.ViewAllNotesBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.ViewAllNotesBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:104^11*/
          
          rs = it.getResultSet();
          
          if(rs.next())
          {
            merchantNumber = rs.getLong("merchant_number");
          }
          
          rs.close();
          it.close();
        }
      }
      
      // set sortBy if present in request
      nc.setSortBy(HttpHelper.getInt(request, "sortBy", NoteComparator.SB_NOTE_DATE));
      
      // now we have the best data we can get.  proceed to loading notes
      loadData();
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public Iterator getSortedNotes()
  {
    Iterator  result = null;
    
    if(sortedNotes != null)
    {
      result = sortedNotes.iterator();
    }
    
    return result;
  }
  
  private void loadServiceCalls()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      if(merchantNumber > 0)
      {
        if(user.hasRight(MesUsers.RIGHT_REPORT_RISK))
        {
          /*@lineinfo:generated-code*//*@lineinfo:159^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  sc.call_date            note_date,
//                      decode(sc.type,
//                             99, sc.other_description,
//                             sct.description) type,
//                      sc.notes                note,
//                      sc.login_name           note_user,
//                      sc.close_date           close_note_date,
//                      sc.close_notes          close_note,
//                      sc.close_login_name     close_user
//              from    service_calls       sc,
//                      service_call_types  sct
//              where   sc.merchant_number = :merchantNumber and
//                      nvl(sc.hidden, 'N') != 'Y' and
//                      sc.type = sct.type(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sc.call_date            note_date,\n                    decode(sc.type,\n                           99, sc.other_description,\n                           sct.description) type,\n                    sc.notes                note,\n                    sc.login_name           note_user,\n                    sc.close_date           close_note_date,\n                    sc.close_notes          close_note,\n                    sc.close_login_name     close_user\n            from    service_calls       sc,\n                    service_call_types  sct\n            where   sc.merchant_number =  :1  and\n                    nvl(sc.hidden, 'N') != 'Y' and\n                    sc.type = sct.type(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.ViewAllNotesBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.maintenance.ViewAllNotesBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:175^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:179^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  sc.call_date            note_date,
//                      decode(sc.type,
//                             99, sc.other_description,
//                             sct.description) type,
//                      sc.notes                note,
//                      sc.login_name           note_user,
//                      sc.close_date           close_note_date,
//                      sc.close_notes          close_note,
//                      sc.close_login_name     close_user
//              from    service_calls       sc,
//                      service_call_types  sct
//              where   sc.merchant_number = :merchantNumber and
//                      nvl(sc.hidden, 'N') != 'Y' and
//                      sc.type = sct.type(+) and
//                      sc.type != 13
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sc.call_date            note_date,\n                    decode(sc.type,\n                           99, sc.other_description,\n                           sct.description) type,\n                    sc.notes                note,\n                    sc.login_name           note_user,\n                    sc.close_date           close_note_date,\n                    sc.close_notes          close_note,\n                    sc.close_login_name     close_user\n            from    service_calls       sc,\n                    service_call_types  sct\n            where   sc.merchant_number =  :1  and\n                    nvl(sc.hidden, 'N') != 'Y' and\n                    sc.type = sct.type(+) and\n                    sc.type != 13";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.ViewAllNotesBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.maintenance.ViewAllNotesBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:196^11*/
        }
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          notes.add(new Note(Note.CAT_SERVICE_CALL, rs));
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadServiceCalls(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void loadActivationCalls()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      if(merchantNumber > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:227^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ac.call_date          note_date,
//                    decode(ac.activation_status_type,
//                           1, 'Pending',
//                           2, 'Completed',
//                           'Unknown')     type,
//                    ac.activation_status  note,
//                    ac.login_name         note_user,
//                    null                  close_note_date,
//                    null                  close_note,
//                    null                  close_user
//            from    service_call_activation ac
//            where   ac.merchant_number = :merchantNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ac.call_date          note_date,\n                  decode(ac.activation_status_type,\n                         1, 'Pending',\n                         2, 'Completed',\n                         'Unknown')     type,\n                  ac.activation_status  note,\n                  ac.login_name         note_user,\n                  null                  close_note_date,\n                  null                  close_note,\n                  null                  close_user\n          from    service_call_activation ac\n          where   ac.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.maintenance.ViewAllNotesBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.maintenance.ViewAllNotesBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:241^9*/
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          notes.add(new Note(Note.CAT_ACTIVATION, rs));
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadActivationCalls(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void loadAppNotes()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      if(user.hasRight(MesUsers.RIGHT_REPORT_RISK))
      {
        /*@lineinfo:generated-code*//*@lineinfo:271^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  an.app_note_date      note_date,
//                    dpt.dept_desc         type,
//                    an.app_note_message   note,
//                    an.app_note_user      note_user,
//                    null                  close_note_date,
//                    null                  close_note,
//                    null                  close_user
//            from    app_notes             an,
//                    departments           dpt
//            where   an.app_seq_num = :appSeqNum and
//                    an.app_department = dpt.dept_code(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  an.app_note_date      note_date,\n                  dpt.dept_desc         type,\n                  an.app_note_message   note,\n                  an.app_note_user      note_user,\n                  null                  close_note_date,\n                  null                  close_note,\n                  null                  close_user\n          from    app_notes             an,\n                  departments           dpt\n          where   an.app_seq_num =  :1  and\n                  an.app_department = dpt.dept_code(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.maintenance.ViewAllNotesBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.maintenance.ViewAllNotesBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:284^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:288^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  an.app_note_date      note_date,
//                    dpt.dept_desc         type,
//                    an.app_note_message   note,
//                    an.app_note_user      note_user,
//                    null                  close_note_date,
//                    null                  close_note,
//                    null                  close_user
//            from    app_notes             an,
//                    departments           dpt
//            where   an.app_seq_num = :appSeqNum and
//                    an.app_department not in (100, 150) and
//                    an.app_department = dpt.dept_code(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  an.app_note_date      note_date,\n                  dpt.dept_desc         type,\n                  an.app_note_message   note,\n                  an.app_note_user      note_user,\n                  null                  close_note_date,\n                  null                  close_note,\n                  null                  close_user\n          from    app_notes             an,\n                  departments           dpt\n          where   an.app_seq_num =  :1  and\n                  an.app_department not in (100, 150) and\n                  an.app_department = dpt.dept_code(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.maintenance.ViewAllNotesBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.maintenance.ViewAllNotesBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:302^9*/
      }
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        notes.add(new Note(Note.CAT_APP_QUEUE, rs));
      }
    }
    catch(Exception e)
    {
      logEntry("loadAppNotes(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void loadQNotes()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:330^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qn.note_date          note_date,
//                  qt.description        type,
//                  qn.note               note,
//                  qn.note_source        note_user,
//                  null                  close_note_date,
//                  null                  close_note,
//                  null                  close_user
//          from    q_notes               qn,
//                  q_types               qt,
//                  q_group_to_types      qgt
//          where   qn.id = :appSeqNum and
//                  qn.type = qt.type and
//                  qn.type = qgt.queue_type and
//                  qgt.group_type = :MesQueues.QG_ALL_APP_QUEUES   
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qn.note_date          note_date,\n                qt.description        type,\n                qn.note               note,\n                qn.note_source        note_user,\n                null                  close_note_date,\n                null                  close_note,\n                null                  close_user\n        from    q_notes               qn,\n                q_types               qt,\n                q_group_to_types      qgt\n        where   qn.id =  :1  and\n                qn.type = qt.type and\n                qn.type = qgt.queue_type and\n                qgt.group_type =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.maintenance.ViewAllNotesBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,MesQueues.QG_ALL_APP_QUEUES);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.maintenance.ViewAllNotesBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:346^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        notes.add(new Note(Note.CAT_APP_QUEUE, rs));
      }
    }
    catch(Exception e)
    {
      logEntry("loadQNotes(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void loadRiskNotes()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      if(user.hasRight(MesUsers.RIGHT_REPORT_RISK))
      {
        /*@lineinfo:generated-code*//*@lineinfo:375^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  rn.note_date          note_date,
//                    rnt.description       type,
//                    rn.notes              note,
//                    u.login_name          note_user,   
//                    null                  close_note_date,
//                    null                  close_note,
//                    null                  close_user
//            from    risk_notes            rn,
//                    risk_note_types       rnt,
//                    users                 u
//            where   rn.merchant_number = :merchantNumber and
//                    rn.type = rnt.type and
//                    rn.user_id = u.user_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rn.note_date          note_date,\n                  rnt.description       type,\n                  rn.notes              note,\n                  u.login_name          note_user,   \n                  null                  close_note_date,\n                  null                  close_note,\n                  null                  close_user\n          from    risk_notes            rn,\n                  risk_note_types       rnt,\n                  users                 u\n          where   rn.merchant_number =  :1  and\n                  rn.type = rnt.type and\n                  rn.user_id = u.user_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.maintenance.ViewAllNotesBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.maintenance.ViewAllNotesBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:390^9*/
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          notes.add(new Note(Note.CAT_RISK, rs));
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadRiskNotes(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void loadData()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      notes.clear();
      
      // service calls
      loadServiceCalls();
      
      // activation calls
      loadActivationCalls();
      
      // application queue notes
      loadAppNotes();
      
      // new queue notes
      loadQNotes();
      
      // risk notes
      loadRiskNotes();
      
      // sort the results
      sortedNotes = new TreeSet(nc);
      sortedNotes.addAll(notes);
    }
    catch(Exception e)
    {
      logEntry("loadData(" + merchantNumber + ", " + appSeqNum + ")", e.toString());
    }
    finally
    {
    }
  }
  
  public class Note
  {
    public static final int     CAT_SERVICE_CALL      = 0;
    public static final int     CAT_ACTIVATION        = 1;
    public static final int     CAT_APP_QUEUE         = 2;
    public static final int     CAT_RISK              = 3;
    
    public int      category;
    public String   noteDate;
    public String   noteTime;
    public String   type;
    public String   note;
    public String   user;
    public String   closeNote;
    public String   closeUser;
    public String   closeDate;
    public String   closeTime;
    public String   noteDateString;
    
    public Note(int cat, ResultSet rs)
    {
      try
      {
        category = cat;
      
        noteDate  = DateTimeFormatter.getFormattedDate(rs.getTimestamp("note_date"), "MM/dd/yyyy");
        noteTime  = DateTimeFormatter.getFormattedDate(rs.getTimestamp("note_date"), "HH:mm:ss");
        noteDateString = DateTimeFormatter.getFormattedDate(rs.getTimestamp("note_date"), "yyyyMMddHHmmss");
        type      = rs.getString("type");
        note      = rs.getString("note");
        user      = rs.getString("note_user");
      
        if(rs.getTimestamp("close_note_date") != null)
        {
          closeNote = rs.getString("close_note");
          closeUser = rs.getString("close_user");
          closeDate = DateTimeFormatter.getFormattedDate(rs.getTimestamp("close_note_date"), "MM/dd/yyyy");
          closeTime = DateTimeFormatter.getFormattedDate(rs.getTimestamp("close_note_date"), "HH:mm:ss");
        }
        else
        {
          closeNote = "&nbsp;";
          closeUser = "&nbsp;";
          closeDate = "&nbsp;";
          closeTime = "&nbsp;";
        }
      }
      catch(Exception e)
      {
        logEntry("Note()", e.toString());
      }
    }
    
    public String getCategoryString()
    {
      String result = "";
      
      switch(category)
      {
        case CAT_SERVICE_CALL:
          result = "Service Calls";
          break;
          
        case CAT_ACTIVATION:
          result = "Activations";
          break;
          
        case CAT_APP_QUEUE:
          result = "Queues";
          break;
        
        case CAT_RISK:
          result = "Risk";
          break;
      }
      
      return result;
    }
  }
  
  public class NoteComparator implements Comparator
  {
    public final static int     SB_NOTE_DATE    = 1;
    
    private int       sortBy;
    private boolean   sortAscending = true;
    
    public NoteComparator()
    {
      sortBy = SB_NOTE_DATE;
    }
    
    public void setSortBy(int newSort)
    {
      if(newSort == sortBy)
      {
        sortAscending = ! sortAscending;
      }
      else
      {
        sortBy = newSort;
        sortAscending = true;
      }
    }
    
    String getCompareString(Note note)
    {
      String result = "";
      try
      {
        switch(sortBy)
        {
          case SB_NOTE_DATE:
            result = note.noteDateString;
            break;
        }
      }
      catch(Exception e)
      {
        logEntry("getCompareString()", e.toString());
      }
      
      return result;
    }
    
    int compare(Note o1, Note o2)
    {
      int result = 0;
      
      try
      {
        if(sortAscending)
        {
          result = getCompareString(o1).compareTo(getCompareString(o2));
        }
        else
        {
          result = getCompareString(o2).compareTo(getCompareString(o1));
        }
      }
      catch(Exception e)
      {
        logEntry("compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(Note o1, Note o2)
    {
      return getCompareString(o1).equals(getCompareString(o2));
    }
    
    public int compare(Object o1, Object o2)
    {
      int result = 0;
      
      try
      {
        result = compare((Note)o1, (Note)o2);
      }
      catch(Exception e)
      {
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result = false;
      
      try
      {
        result = equals((Note)o1, (Note)o2);
      }
      catch(Exception e)
      {
      }
      
      return result;
    }
    
  }
}/*@lineinfo:generated-code*/