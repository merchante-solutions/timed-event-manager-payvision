/*@lineinfo:filename=NonBankCardDetails*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/NonBankCardDetails.sqlj $

  Description:

    NonBankCardDetails

    Pulls Non BankCard (Discover, American Express, etc) details for display
    on the CIF screen

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-05-02 10:38:58 -0700 (Fri, 02 May 2008) $
  Version            : $Revision: 14819 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class NonBankCardDetails extends SQLJConnectionBase
{
  /**
   * Log4J Logger
   */
  static Logger log = Logger.getLogger(NonBankCardDetails.class);

  public  String  discoverMerchantNumber  = "N/A";
  public  String  amexMerchantNumber      = "N/A";
  public  String  dinersMerchantNumber    = "N/A";
  public  String  jcbMerchantNumber       = "N/A";
  public  String  debitMerchantNumber     = "N/A";
  public  String  ebtMerchantNumber       = "N/A";

  // payvision/foreign exchange data
  public  String  payvisionMemberIds      = "N/A";
  public  String  fxIndicator             = "&nbsp;";
  
  protected long merchantNumber     = 0L;

  public void setProperties(String merchantNum)
  {
    try
    {
      merchantNumber = Long.parseLong(merchantNum);
      loadData();
    }
    catch(Exception e)
    {
      logEntry("setProperties(String)", e.toString());
    }
  }

  public void setProperties(HttpServletRequest request)
  {
    setProperties(HttpHelper.getString(request, "merchant"));
  }
  
  protected String setIfBlank(String field, String data)
  {
    String result = field;
    
    if(field == null || field.equals("N/A"))
    {
      result = data;
    }
    
    return result;
  }

  protected void loadData()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;

    try
    {
      connect();

      // first see if merchant numbers are in MIF
      /*@lineinfo:generated-code*//*@lineinfo:104^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(to_char(dmdsnum),
//                    null, 'N/A',
//                    to_char(dmdsnum)||' (mif)') discover_merchant_number,
//                  decode(to_char(damexse),
//                    null, 'N/A',
//                    to_char(damexse)||' (mif)') amex_merchant_number,
//                  decode(debit_plan,
//                    null, 'N/A',
//                    'YES')                      debit_acceptance
//          from    mif
//          where   merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(to_char(dmdsnum),\n                  null, 'N/A',\n                  to_char(dmdsnum)||' (mif)') discover_merchant_number,\n                decode(to_char(damexse),\n                  null, 'N/A',\n                  to_char(damexse)||' (mif)') amex_merchant_number,\n                decode(debit_plan,\n                  null, 'N/A',\n                  'YES')                      debit_acceptance\n        from    mif\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.NonBankCardDetails",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.NonBankCardDetails",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:117^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        discoverMerchantNumber = rs.getString("discover_merchant_number");
        amexMerchantNumber = rs.getString("amex_merchant_number");
        debitMerchantNumber = rs.getString("debit_acceptance");
      }

      rs.close();
      it.close();

      // now look in application database
      /*@lineinfo:generated-code*//*@lineinfo:132^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mpo.cardtype_code                                   card_type,
//                  decode(mpo.cardtype_code,
//                    :mesConstants.APP_CT_DEBIT, 'YES',
//                  nvl(to_char(mpo.merchpo_card_merch_number), 'N/A')) merchant_number
//          from    merchpayoption  mpo,
//                  merchant        m
//          where   m.merch_number = :merchantNumber and
//                  m.app_seq_num = mpo.app_seq_num and
//                  mpo.cardtype_code in 
//                  (
//                    :mesConstants.APP_CT_DISCOVER, 
//                    :mesConstants.APP_CT_AMEX,
//                    :mesConstants.APP_CT_DINERS_CLUB,
//                    :mesConstants.APP_CT_JCB,
//                    :mesConstants.APP_CT_DEBIT,
//                    :mesConstants.APP_CT_EBT
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mpo.cardtype_code                                   card_type,\n                decode(mpo.cardtype_code,\n                   :1 , 'YES',\n                nvl(to_char(mpo.merchpo_card_merch_number), 'N/A')) merchant_number\n        from    merchpayoption  mpo,\n                merchant        m\n        where   m.merch_number =  :2  and\n                m.app_seq_num = mpo.app_seq_num and\n                mpo.cardtype_code in \n                (\n                   :3 , \n                   :4 ,\n                   :5 ,\n                   :6 ,\n                   :7 ,\n                   :8 \n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.NonBankCardDetails",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.APP_CT_DEBIT);
   __sJT_st.setLong(2,merchantNumber);
   __sJT_st.setInt(3,mesConstants.APP_CT_DISCOVER);
   __sJT_st.setInt(4,mesConstants.APP_CT_AMEX);
   __sJT_st.setInt(5,mesConstants.APP_CT_DINERS_CLUB);
   __sJT_st.setInt(6,mesConstants.APP_CT_JCB);
   __sJT_st.setInt(7,mesConstants.APP_CT_DEBIT);
   __sJT_st.setInt(8,mesConstants.APP_CT_EBT);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.NonBankCardDetails",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:151^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        int cardType = rs.getInt("card_type");

        switch(cardType)
        {
          case mesConstants.APP_CT_DISCOVER:
            discoverMerchantNumber = setIfBlank(discoverMerchantNumber, rs.getString("merchant_Number"));
            break;

          case mesConstants.APP_CT_AMEX:
            amexMerchantNumber = setIfBlank(amexMerchantNumber, rs.getString("merchant_number"));
            break;
            
          case mesConstants.APP_CT_DINERS_CLUB:
            dinersMerchantNumber = setIfBlank(dinersMerchantNumber, rs.getString("merchant_number"));
            break;
            
          case mesConstants.APP_CT_JCB:
            jcbMerchantNumber = setIfBlank(jcbMerchantNumber, rs.getString("merchant_number"));
            break;
          
          case mesConstants.APP_CT_DEBIT:
            debitMerchantNumber = setIfBlank(debitMerchantNumber, rs.getString("merchant_number"));
            break;
            
          case mesConstants.APP_CT_EBT:
            ebtMerchantNumber = setIfBlank(ebtMerchantNumber, rs.getString("merchant_number"));
            break;
        }
      }

      rs.close();
      it.close();

      // check mif_non_bank_cards
      /*@lineinfo:generated-code*//*@lineinfo:191^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(discover, 'N/A')  discover,
//                  nvl(amex,     'N/A')  amex,
//                  nvl(diners,   'N/A')  diners,
//                  nvl(jcb,      'N/A')  jcb
//          from    mif_non_bank_cards
//          where   merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(discover, 'N/A')  discover,\n                nvl(amex,     'N/A')  amex,\n                nvl(diners,   'N/A')  diners,\n                nvl(jcb,      'N/A')  jcb\n        from    mif_non_bank_cards\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.NonBankCardDetails",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.maintenance.NonBankCardDetails",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:199^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        discoverMerchantNumber = setIfBlank(discoverMerchantNumber, rs.getString("discover"));
        amexMerchantNumber = setIfBlank(amexMerchantNumber, rs.getString("amex"));
        dinersMerchantNumber = setIfBlank(dinersMerchantNumber, rs.getString("diners"));
        jcbMerchantNumber = setIfBlank(jcbMerchantNumber, rs.getString("jcb"));
      }

      rs.close();
      it.close();
      
      // finally, check current trident profile
      /*@lineinfo:generated-code*//*@lineinfo:215^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct
//                  nvl(disc_caid, 'N/A')  as discover,
//                  nvl(amex_caid, 'N/A')  as amex,
//                  nvl(jcb_caid,  'N/A')  as jcb
//          from    trident_profile   tp
//          where   tp.merchant_number = :merchantNumber and
//                  tp.profile_status = 'A' -- active
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct\n                nvl(disc_caid, 'N/A')  as discover,\n                nvl(amex_caid, 'N/A')  as amex,\n                nvl(jcb_caid,  'N/A')  as jcb\n        from    trident_profile   tp\n        where   tp.merchant_number =  :1  and\n                tp.profile_status = 'A' -- active";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.NonBankCardDetails",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.maintenance.NonBankCardDetails",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:224^7*/
      rs = it.getResultSet();

      if( rs.next() )
      {
        discoverMerchantNumber  = setIfBlank(discoverMerchantNumber, rs.getString("discover"));
        amexMerchantNumber      = setIfBlank(amexMerchantNumber, rs.getString("amex"));
        jcbMerchantNumber       = setIfBlank(jcbMerchantNumber, rs.getString("jcb"));
      }
      rs.close();
      it.close();

      // get payvision member id's
      /*@lineinfo:generated-code*//*@lineinfo:237^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct
//                  tpa.payvision_member_id
//          from    trident_profile     tp,
//                  trident_profile_api tpa
//          where   tp.merchant_number = :merchantNumber and
//                  tp.terminal_id = tpa.terminal_id and
//                  tpa.payvision_member_id is not null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct\n                tpa.payvision_member_id\n        from    trident_profile     tp,\n                trident_profile_api tpa\n        where   tp.merchant_number =  :1  and\n                tp.terminal_id = tpa.terminal_id and\n                tpa.payvision_member_id is not null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.maintenance.NonBankCardDetails",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.maintenance.NonBankCardDetails",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:246^7*/
      rs = it.getResultSet();

      StringBuffer temp = new StringBuffer();
      while (rs.next())
      {
        if (temp.length() > 0)
        {
          temp.append(", ");
        }
        temp.append(rs.getString("payvision_member_id"));
      }
      rs.close();
      it.close();

      payvisionMemberIds = temp.toString();

      // determine foreign exchange indicator
      int fxCount;
      /*@lineinfo:generated-code*//*@lineinfo:265^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(tpa.fx_client_id) 
//          from    trident_profile tp,
//                  trident_profile_api tpa
//          where   tp.merchant_number = :merchantNumber and
//                  tp.terminal_id = tpa.terminal_id and
//                  tpa.fx_client_id is not null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(tpa.fx_client_id)  \n        from    trident_profile tp,\n                trident_profile_api tpa\n        where   tp.merchant_number =  :1  and\n                tp.terminal_id = tpa.terminal_id and\n                tpa.fx_client_id is not null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.maintenance.NonBankCardDetails",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   fxCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:273^7*/
      fxIndicator = (fxCount > 0 ? "Y" : "N");

      // determine foreign exchange product types
      /*
      if (fxIndicator.equals("Y"))
      {
        #sql [Ctx] it=
        {
          select  distinct
                  tpa.fx_product_type
          from    trident_profile     tp,
                  trident_profile_api tpa
          where   tp.merchant_number = :merchantNumber and
                  tp.terminal_id = tpa.terminal_id and
                  tpa.fx_product_type is not null
        };
        rs = it.getResultSet();

        temp = new StringBuffer();
        while (rs.next())
        {
          if (temp.length() > 0)
          {
            temp.append(", ");
          }
          int productType = rs.getInt("fx_product_type");
          String descriptor = null;
          switch (productType)
          {
            case 201:
              descriptor = "Physical Goods";
              break;

            case 202:
              descriptor = "Digital Download";
              break;

            case 203:
              descriptor = "Reoccuring";
              break;

            case 204:
              descriptor = "One Time Submission";
              break;

            case 205:
              descriptor = "Services";
              break;
          }
          temp.append("" + productTyep + "-" + descriptor);
        }
        rs.close();
        it.close();

        fxIndicator = fxIndicator + " (" + temp + ")";
      }
      */
    }
    catch(Exception e)
    {
      logEntry("loadData(" + merchantNumber + ")", e.toString());

    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/