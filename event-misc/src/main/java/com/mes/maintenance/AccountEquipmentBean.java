/*@lineinfo:filename=AccountEquipmentBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  Description:

    AccountEquipmentBean

    Support bean for viewing equipment requested by and deployed to a merchant

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class AccountEquipmentBean extends SQLJConnectionBase
{
  public Vector   requestedEquipment    = new Vector();
  public Vector   deployedEquipment     = new Vector();
  
  public AccountEquipmentBean()
  {
  }
  
  public AccountEquipmentBean(String merchantNumber)
  {
    loadData(merchantNumber);
  }
  
  private void loadData(String merchantNumber)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    try
    {
      connect();
      
      // load requested equipment
      /*@lineinfo:generated-code*//*@lineinfo:53^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  me.merchequip_equip_quantity  equip_quantity,
//                  me.equip_model                equip_model,
//                  tpt.description               equip_descriptor,
//                  etq.part_number               qrg_part_number,
//                  tpf.feat_name                 equip_type,
//                  tpf2.feat_name                mfgr_name,        
//                  elt.equiplendtype_code        lendtype_code,
//                  elt.equiplendtype_description lendtype
//          from    merchant mr,
//                  merchequipment  me,
//                  tmg_part_types  tpt,
//                  tmg_part_feature_maps tpfm,
//                  tmg_part_features tpf,
//                  tmg_part_feature_maps tpfm2,
//                  tmg_part_features tpf2,
//                  equiplendtype   elt,
//                  (
//                    select  msi.equip_model,
//                            msi.term_application_profgen,
//                            msi.front_end
//                    from    mms_stage_info  msi,
//                            merchant        mr
//                    where   mr.merch_number = :merchantNumber and
//                            mr.app_seq_num = msi.app_seq_num
//                  ) msi,
//                  equip_termapp_qrg etq
//          where   mr.merch_number       = :merchantNumber and
//                  mr.app_seq_num        = me.app_seq_num and
//                  me.equiptype_code     != 9 and
//                  me.equip_model        = tpt.model_code and
//                  tpt.pt_id = tpfm.pt_id and
//                  tpfm.ft_code = 'CATEGORY' and
//                  tpfm.feat_id = tpf.feat_id and
//                  tpt.pt_id = tpfm2.pt_id and
//                  tpfm2.ft_code = 'MANUFACTURER' and
//                  tpfm2.feat_id = tpf2.feat_id and 
//                  me.equiplendtype_code = elt.equiplendtype_code and
//                  me.equip_model = msi.equip_model(+) and
//                  me.app_seq_num = mr.app_seq_num and
//                  msi.term_application_profgen = etq.terminal_application(+) and
//                  (
//                    etq.termapp_type is null or 
//                    (
//                      etq.termapp_type = 'any' and
//                      (etq.model_code = 'na' or etq.model_code = me.equip_model) 
//                    ) or 
//                    (
//                      etq.termapp_type = mr.terminal_application and 
//                      (etq.model_code = 'na' or etq.model_code = me.equip_model)
//                    )
//                  ) and
//                  msi.front_end = etq.front_end(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  me.merchequip_equip_quantity  equip_quantity,\n                me.equip_model                equip_model,\n                tpt.description               equip_descriptor,\n                etq.part_number               qrg_part_number,\n                tpf.feat_name                 equip_type,\n                tpf2.feat_name                mfgr_name,        \n                elt.equiplendtype_code        lendtype_code,\n                elt.equiplendtype_description lendtype\n        from    merchant mr,\n                merchequipment  me,\n                tmg_part_types  tpt,\n                tmg_part_feature_maps tpfm,\n                tmg_part_features tpf,\n                tmg_part_feature_maps tpfm2,\n                tmg_part_features tpf2,\n                equiplendtype   elt,\n                (\n                  select  msi.equip_model,\n                          msi.term_application_profgen,\n                          msi.front_end\n                  from    mms_stage_info  msi,\n                          merchant        mr\n                  where   mr.merch_number =  :1  and\n                          mr.app_seq_num = msi.app_seq_num\n                ) msi,\n                equip_termapp_qrg etq\n        where   mr.merch_number       =  :2  and\n                mr.app_seq_num        = me.app_seq_num and\n                me.equiptype_code     != 9 and\n                me.equip_model        = tpt.model_code and\n                tpt.pt_id = tpfm.pt_id and\n                tpfm.ft_code = 'CATEGORY' and\n                tpfm.feat_id = tpf.feat_id and\n                tpt.pt_id = tpfm2.pt_id and\n                tpfm2.ft_code = 'MANUFACTURER' and\n                tpfm2.feat_id = tpf2.feat_id and \n                me.equiplendtype_code = elt.equiplendtype_code and\n                me.equip_model = msi.equip_model(+) and\n                me.app_seq_num = mr.app_seq_num and\n                msi.term_application_profgen = etq.terminal_application(+) and\n                (\n                  etq.termapp_type is null or \n                  (\n                    etq.termapp_type = 'any' and\n                    (etq.model_code = 'na' or etq.model_code = me.equip_model) \n                  ) or \n                  (\n                    etq.termapp_type = mr.terminal_application and \n                    (etq.model_code = 'na' or etq.model_code = me.equip_model)\n                  )\n                ) and\n                msi.front_end = etq.front_end(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.AccountEquipmentBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   __sJT_st.setString(2,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.AccountEquipmentBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:107^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        for(int i=0; i < rs.getInt("equip_quantity"); ++i)
        {
          requestedEquipment.add(new RequestedEquipmentDetail(rs));
        }
      }
      
      rs.close();
      it.close();
      
      // load deployed equipment
      /*@lineinfo:generated-code*//*@lineinfo:123^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_char(td.start_ts, 'mm/dd/yyyy')  deployed_date,
//                  decode(td.end_ts,
//                    null, '---',
//                    to_char(td.end_ts, 'mm/dd/yyyy')) returned_date,
//                  tps.serial_num                      serial_number,
//                  tpt.pt_name                         equipment_type,
//                  decode(td.end_ts,
//                    null, 'Deployed',
//                    'Returned')                       equipment_status,
//                  decode(td.end_ts,
//                    null, tpc.pc_name,
//                    '---')                            equipment_class,
//                  decode(td.end_ts,
//                    null, tpd.disp_name,
//                    '---')                            equipment_disposition,
//                  tps.state_id                        current_state
//          from    tmg_deployment td,
//                  tmg_part_states tps,
//                  tmg_part_types tpt,
//                  tmg_part_classes tpc,
//                  tmg_part_dispositions tpd,
//                  tmg_op_log tol
//          where   td.merch_num = :merchantNumber and
//                  td.deploy_id = tps.deploy_id and
//                  tps.pt_id = tpt.pt_id and
//                  tps.pc_code = tpc.pc_code and
//                  tps.disp_code = tpd.disp_code and
//                  tps.op_id = tol.op_id and
//                  tol.op_code in ( 'PART_DEPLOY', 'PART_DISP_CHANGE', 'PART_IMPORT_ADD' )
//          order by td.end_ts desc, td.start_ts desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  to_char(td.start_ts, 'mm/dd/yyyy')  deployed_date,\n                decode(td.end_ts,\n                  null, '---',\n                  to_char(td.end_ts, 'mm/dd/yyyy')) returned_date,\n                tps.serial_num                      serial_number,\n                tpt.pt_name                         equipment_type,\n                decode(td.end_ts,\n                  null, 'Deployed',\n                  'Returned')                       equipment_status,\n                decode(td.end_ts,\n                  null, tpc.pc_name,\n                  '---')                            equipment_class,\n                decode(td.end_ts,\n                  null, tpd.disp_name,\n                  '---')                            equipment_disposition,\n                tps.state_id                        current_state\n        from    tmg_deployment td,\n                tmg_part_states tps,\n                tmg_part_types tpt,\n                tmg_part_classes tpc,\n                tmg_part_dispositions tpd,\n                tmg_op_log tol\n        where   td.merch_num =  :1  and\n                td.deploy_id = tps.deploy_id and\n                tps.pt_id = tpt.pt_id and\n                tps.pc_code = tpc.pc_code and\n                tps.disp_code = tpd.disp_code and\n                tps.op_id = tol.op_id and\n                tol.op_code in ( 'PART_DEPLOY', 'PART_DISP_CHANGE', 'PART_IMPORT_ADD' )\n        order by td.end_ts desc, td.start_ts desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.AccountEquipmentBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.AccountEquipmentBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:155^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        deployedEquipment.add(new DeployedEquipmentDetail(rs));
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadData(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public class RequestedEquipmentDetail
  {
    public String   equipModel      = "";
    public String   equipDescriptor = "";
    public String   qrgPartNumber   = "";
    public String   equipType       = "";
    public String   mfgrName        = "";
    public String   lendTypeCode    = "";
    public String   lendType        = "";
    
    public RequestedEquipmentDetail (ResultSet rs)
    {
      try
      {
        equipModel      = rs.getString("equip_model");
        equipDescriptor = rs.getString("equip_descriptor");
        qrgPartNumber   = blankIfNull(rs.getString("qrg_part_number"));
        equipType       = rs.getString("equip_type");
        mfgrName        = rs.getString("mfgr_name");
        lendTypeCode    = rs.getString("lendtype_code");
        lendType        = rs.getString("lendtype");
      }
      catch(Exception e)
      {
        logEntry("RequestedEquipmentDetail()", e.toString());
      }
    }
  }
  
  public class DeployedEquipmentDetail
  {
    public String dateDeployed          = "";
    public String dateReturned          = "";
    public String serialNumber          = "";
    public String equipmentType         = "";
    public String equipmentStatus       = "";
    public String equipmentClass        = "";
    public String equipmentDisposition  = "";
    public String currentState          = "";
    
    public DeployedEquipmentDetail( ResultSet rs )
    {
      try
      {
        dateDeployed = rs.getString("deployed_date");
        dateReturned = rs.getString("returned_date");
        serialNumber = rs.getString("serial_number");
        equipmentType = rs.getString("equipment_type");
        equipmentStatus = rs.getString("equipment_status");
        equipmentClass = rs.getString("equipment_class");
        equipmentDisposition = rs.getString("equipment_disposition");
        currentState = rs.getString("current_state");
      }
      catch(Exception e)
      {
        logEntry("DeployedEquipmentDetail()", e.toString());
      }
    }
  }
}/*@lineinfo:generated-code*/