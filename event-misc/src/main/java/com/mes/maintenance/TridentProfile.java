/*@lineinfo:filename=TridentProfile*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/TridentProfile.sqlj $

  Description:  
  
  TridentProfile
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-01-15 15:06:44 -0800 (Tue, 15 Jan 2013) $
  Version            : $Revision: 20841 $

  Change History:
     See VSS database

  Copyright (C) 2004,2005,2006 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.StringReader;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.constants.MesHierarchy;
import com.mes.constants.MesUsers;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.AmexSENumber;
import com.mes.forms.ButtonField;
import com.mes.forms.ButtonImageField;
import com.mes.forms.CheckboxField;
import com.mes.forms.CityStateZipField;
import com.mes.forms.DateStringField;
import com.mes.forms.DiscoverMerchantNumber;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.JCBMerchantNumber;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.StaticTextField;
import com.mes.forms.TridentTidField;
import com.mes.forms.Validation;
import com.mes.net.GetClosestParent;
import com.mes.ops.TimezoneTable;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import com.mes.tools.ResultSetRow;
import sqlj.runtime.ResultSetIterator;

public class TridentProfile extends FieldBean
{
  static Logger log = Logger.getLogger(TridentProfile.class);
  
  public static final int         ET_ADD_REC      = 0;
  public static final int         ET_UPD_REC      = 1;
  
  public static final String      ACTION_CREATE   = "create";
  public static final String      ACTION_EDIT     = "edit";
  public static final String      ACTION_VIEW     = "view";
  
  // key codes
  public static final int         KC_INVALID      = -1;
  public static final int         KC_TID          = 1;
  public static final int         KC_REQUEST_ID   = 2;
  
  // Trident Profile Post Edit actions
  public static final int         TPPE_PROGRAMMING_QUEUE  = 1;
  public static final int         TPPE_MMS_NEW_QUEUE      = 2;
  
  /*************************************************************************
  **
  **   TridentProfile
  **
  **************************************************************************/
  protected Vector                    SearchRows      = new Vector();
  protected Vector                    ChangeHistory   = new Vector();

  private Hashtable KeyTypes        = new Hashtable();
  public  HashSet   appTypeSet      = new HashSet();
  private String    errorString     = "";
  private boolean   success         = false;
  private boolean   validated       = true;
  public  Vector    nextLinks       = new Vector();
  
  public TridentProfile( )
  {
    // build key types
    KeyTypes.put("tid", KC_TID);
    KeyTypes.put("requestId", KC_REQUEST_ID);
  }
  
  public boolean mustRedirect()
  {
    boolean result = false;
    
    if(!getData("sendToURL").equals(""))
    {
      result = true;
    }
    
    return (result);
  }
  
  public boolean shouldDisplayUpdateButtons()
  {
    boolean result = false;
    
    if(isSubmitted())
    {
      // if the form was submitted show update buttons UNLESS it was a
      // new profile creation and the validation failed
      if(getData("action").equals(ACTION_CREATE) && !validated)
      {
        result = false;
      }
      else
      {
        result = true;
      }
    }
    else if(getData("action").equals(ACTION_EDIT))
    {
      // if already editing then definitely show update buttons
      result = true;
    }
    else
    {
      result = false;
    }
    
    return (result);
  }
  
  public String getStatusString()
  {
    StringBuffer result = new StringBuffer("&nbsp;");
    
    if(fields.isSubmitted())
    {
      if(validated)
      {
        if(success)
        {
          result.append("<span class=\"formTextNoticeBlue\">");
          if(!getData("updateAll").equals(""))
          {
            result.append("All Profiles modified successfully for this merchant");
          }
          else if(!getData("update").equals(""))
          {
            result.append("Profile Modified Successfully");
          }
          else if(!getData("create").equals(""))
          {
            result.append("Profile Created Successfully");
          }
          result.append("</span>");
        }
        else
        {
          result.append("<span class=\"formTextNoticeRed\">");
          result.append(errorString);
          result.append("</span>");
        }
      }
      else
      {
        result.append("<span class=\"formTextNoticeRed\">");
        result.append("Validation Failed.  See errors below.");
        result.append("</span>");
      }
    }
    
    return (result.toString());
  }
  
  private void addNextLink(String label, String url, boolean newWindow)
  {
    StringBuffer nextLink = new StringBuffer("");
    
    try
    {
      nextLink.append("<a href=\"");
      nextLink.append(url);
      if(newWindow)
      {
        nextLink.append("\" target=\"_blank");
      }
      nextLink.append("\">");
      nextLink.append(label);
      nextLink.append("</a>");
    
      nextLinks.add(nextLink.toString());
    }
    catch(Exception e)
    {
      logEntry("addNextLink(" + label + ", " + url + ")", e.toString());
      log.error("addNextLink(" + label + ", " + url + "): " + e.toString());
    }
  }
  
  private void addNextLink(String label, String url)
  {
    addNextLink(label, url, false);
  }

  private boolean achProfileExists()
  {
    boolean           achExists   = false;
    ResultSetIterator it          = null;
    ResultSet         rs          = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:232^7*/

//  ************************************************************
//  #sql [Ctx] it = { select profile_id from achp_profiles where profile_id = :getData("tid")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_945 = getData("tid");
  try {
   String theSqlTS = "select profile_id from achp_profiles where profile_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_945);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:235^7*/
      rs = it.getResultSet();
      achExists = rs.next();
    }
    catch(Exception e)
    {
      logEntry("achProfileExists()", e.toString());
      log.error("achProfileExists(): " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
    
    return achExists;
  }
  
  public void setProperties(HttpServletRequest request)
  {
    try
    {
      connect();
      
      setFields(request);
      
      register(user.getLoginName(), request);
      
      // establish base url
      StringBuffer baseUrl = new StringBuffer(request.getServletPath());
      baseUrl.append("?action=");
      baseUrl.append(HttpHelper.getString(request, "action"));
      baseUrl.append("&keyType=");
      baseUrl.append(HttpHelper.getString(request, "keyType"));
      baseUrl.append("&key=");
      baseUrl.append(HttpHelper.getString(request, "key"));
      
      if(!getData("referredFrom").equals(""))
      {
        baseUrl.append("&referredFrom=");
        baseUrl.append(getData("referredFrom"));
      }
      
      setData("baseUrl", baseUrl.toString());
      
      // check for submission
      if(!getData("create").equals(""))
      {
        if(isValid())
        {
          // submit new or edit
          success = doInsert();
        }
        else
        {
          validated = false;
        }
        
        addNextLink("View VAR Form", "/jsp/maintenance/tridentvarform.jsp?catid="+getData("catid"), true);
      }
      else if(!getData("update").equals(""))
      {
        if(isValid())
        {
          success = doUpdate();
        }
        else
        {
          validated = false;
        }
        
        addNextLink("Back", getData("referredFrom"));
        addNextLink("View VAR Form", "/jsp/maintenance/tridentvarform.jsp?catid="+getData("catid"), true);
      }
      else if(!getData("updateAll").equals(""))
      {
        if(isValid())
        {
          success = doUpdateAll();
        }
        else
        {
          validated = false;
        }
        
        addNextLink("Back", getData("referredFrom"));
        addNextLink("View VAR Form", "/jsp/maintenance/tridentvarform.jsp?catid="+getData("catid"), true);
      }
      else if(!getData("getAmex").equals(""))
      {
        // fill amex fields from app
        success = getAmexFromApp();
      }
      else if(!getData("cancel").equals(""))
      {
        // cancel submission, take user back to where they came from
        setData("sendToURL", getData("referredFrom"));
      }
      else
      {
        // handle pre-submission tasks
        if(getData("action").equals(ACTION_CREATE))
        {
          // generate data for a new profile based on passed information
          if(generateProfile())
          {
            // success
            success = true;
          }
        }
        else if(getData("action").equals(ACTION_EDIT) || getData("action").equals(ACTION_VIEW))
        {
          // retrieve profile data from database
          if(loadProfile())
          {
            // success
            success = true;
          }
          
          if( user.hasRight(MesUsers.RIGHT_PG_PROFILE_ADMIN) )
          {
            addNextLink("Back", getData("referredFrom"));
            addNextLink("View VAR Form", "/jsp/maintenance/tridentvarform.jsp?catid="+getData("catid"), true);
          }
        }
        else
        {
          // invalid or no action type passed -- send them to main menu
          setData("sendToURL", "/jsp/secure/navigate.jsp");
          
          addNextLink("Back", getData("referredFrom"));
          addNextLink("View VAR Form", "/jsp/maintenance/tridentvarform.jsp?catid="+getData("catid"), true);
        }
      }

      // links to add or edit ach profiles 
      // achp profile will be view only for users without achp edit right
      //  6042 - view achp profile 
      //  6041 - edit achp profile
      if (achProfileExists() && (user.hasRight(6042) || user.hasRight(6041))) 
      {
        addNextLink("Edit ACH Profile","/srv/mes/pachEditProfileFromTri?triAction=" 
          + getData("action") + "&profileId=" + getData("tid"),true);
      }
      else if (user.hasRight(6041))
      {
        addNextLink("Add ACH Profile", "/srv/mes/pachAddProfileFromTri?triAction=" 
          + getData("action") + "&profileId=" + getData("tid"),true);
      }
      
      // retrieve change history
      getChangeHistory();
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  protected void postHandleRequest(HttpServletRequest request)
  {
    try
    {
      // set referred field from request if not already set
      if(getData("referredFrom").equals(""))
      {
        if(request.getHeader("Referer") != null && !request.getHeader("Referer").equals(""))
        {
          setData("referredFrom", request.getHeader("Referer"));
        }
      }
    }
    catch(Exception e)
    {
      logEntry("preHandleRequest()", e.toString());
    }
  }
  
  protected String buildIntlCredentials()
  {
    Field       f           = getField("intlCredentials-0");
    int         fldIdx      = 0;
    String      retVal      = "";
    
    while( f != null )
    {
      if( f.isBlank() ) break;
      retVal += ((fldIdx == 0) ? "" : ",") + f.getData();
      f = getField("intlCredentials-" + ++fldIdx);
    }
    return( retVal );
  }
  
  protected void createFields(HttpServletRequest request)
  {
    CheckboxField       acceptField   = null;
    String              fname         = null;
    
    super.createFields(request);
    
    try
    {
      // action is the field that tells us what the user is trying to do
      // initialized to ACTION_CREATE
      fields.add(new HiddenField("action"));
      
      // keyType and key provide necessary information to link to the
      // merchant 
      fields.add(new HiddenField("keyType"));
      fields.add(new HiddenField("key"));
      
      fields.add(new HiddenField("requestId"));
      
      // referredFrom is initialized to allow sending the user back to where
      // they came from
      fields.add(new HiddenField("referredFrom"));
      fields.add(new HiddenField("sendToURL"));
        
      // fields to support the search function
      fields.add(new DateStringField  ("fromDate", "From", true, false));
      fields.add(new DateStringField  ("toDate", "To", true, false));
      fields.add(new DropDownField    ("portfolio", "Portfolio", new PortfolioListTable(), true));
      fields.add(new DropDownField    ("profileGen", "Profile Generator", new ProfileGenTable(), true));
      fields.add(new DropDownField    ("appType", "Profile App Type", new AppTypeTable(), true));
      fields.add(new Field            ("searchValue","Search Criteria:",25,0,true));
      fields.add(new ButtonImageField ("searchButton", "Search", "/images/search.gif", fields));
      
      // profile account info
      FieldGroup gAccountInfo = new FieldGroup("gAcctInfo");
      
      gAccountInfo.add  (new TridentTidField  ("tid",           "Profile ID",20,false));
      gAccountInfo.add  (new Field            ("merchantId",    "Merchant Number",16,30,false));
      gAccountInfo.add  (new Field            ("merchantName",  "Location Name (DBA)",25,30,true));
      gAccountInfo.add  (new DropDownField    ("dynamicDbaAllowed", "Dynamic DBA Allowed",new DynamicDbaAllowedTable(),false));
      gAccountInfo.add  (new Field            ("dynamicDbaPrefix", "Dynamic DBA Name Prefix",12,14,true));
      gAccountInfo.add  (new DropDownField    ("binNumber",     "BIN Number",new BinNumberTable(),false));
      gAccountInfo.add  (new Field            ("sicCode",       "SIC",4,6,true));
      gAccountInfo.add  (new DropDownField    ("timeZone",      "Time Zone",new TimezoneTable(Ctx),false));
      gAccountInfo.add  (new Field            ("locAddr1",      "Location Address Line 1",32,40,true));
      gAccountInfo.add  (new CityStateZipField("locCSZ",        "Location City, State, Zip",13,16,true));
      gAccountInfo.add  (new PhoneField       ("locPhone",      "Location Phone Number",true));
      gAccountInfo.add  (new Field            ("dmContactInfo", "Direct Marketing Contact Info",13,15,true));
      gAccountInfo.add  (new DropDownField    ("profileStatus", "Profile Status",new ProfileStatusTable(),true));
      gAccountInfo.add  (new DropDownField    ("riskStatus",    "Risk Status",new RiskStatusTable(),true));
      gAccountInfo.add  (new DropDownField    ("productCode",   "Product Code",new ProductCodeTable(),false));
      gAccountInfo.add  (new Field            ("merchantKey",   "Merchant Key",32,32,true));
      gAccountInfo.add  (new Field            ("secondaryKey",  "Secondary Key",32,32,true));
      gAccountInfo.add  (new CheckboxField    ("merchantKeyRequired", "Merchant Key Required", false));
      gAccountInfo.add  (new CheckboxField    ("secondaryKeyEnabled", "Secondary Key Enabled", false));
      gAccountInfo.add  (new CheckboxField    ("apiEnabled",    "Trident Payment Gateway Enabled", false));
      gAccountInfo.add  (new DropDownField    ("fraudMode",     "Fraud Monitoring Mode", new FraudModeTable(), false));
      gAccountInfo.add  (new CheckboxField    ("valSettleAmt",  "Validate Settlement Amount", true));
      gAccountInfo.add  (new DropDownField    ("cardStoreLevel","Payment Gateway Card Store Level",new CardStoreLevelTable(),false));
      gAccountInfo.add  (new CheckboxField    ("multiCapture",  "Payment Gateway Multi-Capture Enabled", false));
      gAccountInfo.add  (new CheckboxField    ("forceOnDecline","Force Declined Multi-Capture", false));
      gAccountInfo.add  (new CheckboxField    ("batchProcessing", "Batch Processing Enabled", false));
      gAccountInfo.add  (new Field            ("batchProcessingEmail",  "Batch Processing Email Address",100,45,true));
      //gAccountInfo.add  (new CheckboxField    ("amexCid",       "Amex CID Response Enabled", false));
      
      fields.add(gAccountInfo);
      
      fields.add  (new CheckboxField    ("apiCreditsAllowed", "Trident Payment Gateway Credits Enabled", false));
      fields.add  (new NumberField      ("authExpDays",   "Auth Expiration Days", 6, 3, false, 0));
      fields.add  (new HiddenField      ("catidType"));
      
      // add field validations
      fields.getField("authExpDays").addValidation( new AuthExpirationDaysValidation() );
      
      // fields necessary to complete conversions
      fields.add  (new HiddenField      ("equipType"));  // for conversions
      fields.add  (new HiddenField      ("oldVnum"));
      fields.add  (new HiddenField      ("appSeqNum"));
      fields.add  (new HiddenField      ("appCode"));
      
      FieldGroup gCardsAccepted = new FieldGroup("gCardsAccepted");
      gCardsAccepted.add  (new Field          ("catid",         "Card Acceptor Terminal ID",8,8,false));
      
      // add the card accepted section
      for( int i = 0; i < CardTypes.length; ++i )
      {
        acceptField = new CheckboxField(  CardTypes[i].AcceptFieldName,
                                          CardTypes[i].AcceptLabel,
                                          CardTypes[i].AcceptDefault );
        fname = CardTypes[i].CaidFieldName;
        gCardsAccepted.add  ( acceptField );
        
        String acceptName = CardTypes[i].AcceptFieldName;
        String acceptLabel = CardTypes[i].AcceptLabel + " Card Acceptor ID";
        
        if(acceptName.equals("amexAccept"))
        {
          gCardsAccepted.add(new AmexSENumber(fname, acceptLabel, 25, true, acceptField));
        }
        else if(acceptName.equals("discAccept"))
        {
          gCardsAccepted.add(new DiscoverMerchantNumber(fname, acceptLabel, 25, true, acceptField));
          
          // add use Discover MAP checkbox
          gCardsAccepted.add(new CheckboxField("useDiscoverAid", "MAP Participant?", false));
        }
        else if(acceptName.equals("jcbAccept"))
        {
          gCardsAccepted.add(new JCBMerchantNumber(fname, acceptLabel, 25, true, acceptField));
        }
        else if( acceptName.equals("vmcAccept") || acceptName.equals("debitAccept") )
        {
          gCardsAccepted.add( new StaticTextField(fname, acceptLabel) );
        }
        else if ( fname != null && !fname.equals("") )    // default
        {
          gCardsAccepted.add  (new NumberField (fname,acceptLabel,15,25,true,0));
          gCardsAccepted.getField(fname).addValidation( new CardAcceptedValidation( acceptField ) );
        }
        
        if(acceptName.equals("amexAccept"))
        {
          // add pcid and charge description
          gCardsAccepted.add(new DropDownField("amexPcid",  "American Express PCID", new PCIDTable(),true));
          gCardsAccepted.add(new Field("amexChargeDesc",    "American Express Charge Description", 23, 23, true));
          fields.add(new Field("appChargeDesc", "Business Description from App", 200, 32, true));
          
          gCardsAccepted.getField("amexPcid").addValidation(
            new AmexRequiredValidation(gCardsAccepted.getField("amexAccept")));
          gCardsAccepted.getField("amexChargeDesc").addValidation(
            new AmexRequiredValidation(gCardsAccepted.getField("amexAccept")));
        }
      }

      // add validations
      fields.getField("productCode").addValidation(
        new ProductCodeProductValidation(getField("catidType")));
        
      fields.getField("merchantKeyRequired").addValidation(
        new APIValidation(getField("apiEnabled")));
        
      fields.getField("secondaryKeyEnabled").addValidation(
        new SecondaryKeyValidation(getField("secondaryKey")));
        
      fields.getField("dynamicDbaPrefix").addValidation(
        new DynamicDbaPrefixValidation(getField("dynamicDbaAllowed")));
        
      /* removed restriction on this checkbox per BUG-1056
      fields.getField("amexCid").addValidation(
        new AmexCIDValidation(getField("apiEnabled")));
      */
      
      fields.add(gCardsAccepted);

      // other payment types field group
      FieldGroup gOtherPayTypes = new FieldGroup("gOtherPayTypes");

      
      // international processing
      Field[] intlCredentials = new Field[]
      {
        new Field("intlCredentials-0","International Merchant ID"    ,40,40,true),
        new Field("intlCredentials-1","International Merchant Key"   ,40,40,true),
        new Field("intlCredentials-2","International Company ID"     ,40,40,true),
        new Field("intlCredentials-3","International 3DS Acquirer ID",40,40,true),
        new Field("intlCredentials-4","International 3DS Merchant ID",40,40,true),
      };
      
      DropDownField intlProcDropDown = new DropDownField("intlProcessor", "International Processor",new IntlProcessorTable(),false);
      gOtherPayTypes.add(intlProcDropDown);
      for( int i = 0; i < intlCredentials.length; ++i )
      {
        gOtherPayTypes.add(intlCredentials[i]);
      }
      gOtherPayTypes.add(new CheckboxField("intlUSD","USD to International Processor",false));
      gOtherPayTypes.getField("intlUSD").addValidation(new IntlUSDValidation(intlProcDropDown));
      
      // validate that only one international vendor is selected
      intlProcDropDown.addValidation(new IntlProcessorValidation(intlCredentials));
      
      // fx
      Field f = new NumberField("fxClientId","FX Client ID",10,10,true,0);
      gOtherPayTypes.add(f);
      AllOrNoneValidation v = new AllOrNoneValidation("FX data missing");
      f.addValidation(new FxAllowedValidation(intlProcDropDown));
      v.addField(f);
      f.addValidation(v);
      f = new DropDownField("fxProductType", "FX Product Type", new FxProductTypeTable(),true);
      gOtherPayTypes.add(f);
      f.addValidation(new FxAllowedValidation(intlProcDropDown));
      v.addField(f);
      f.addValidation(v);
      
      // ach
      gOtherPayTypes.add(new CheckboxField("achpCcdAccept","ACH CCD Enabled",false));
      gOtherPayTypes.add(new CheckboxField("achpPpdAccept","ACH PPD Enabled",false));
      gOtherPayTypes.add(new CheckboxField("achpWebAccept","ACH WEB Enabled",false));
      gOtherPayTypes.add(new CheckboxField("achpTelAccept","ACH TEL Enabled",false));

      // bill me later
      gOtherPayTypes.add(new CheckboxField("bmlAccept","BillMeLater",false));
      gOtherPayTypes.add(new HiddenField("bmlPromoCode1","Default"));
      gOtherPayTypes.add(new Field("bmlMerchId1","BillMeLater Core Merchant ID",32,20,true));
      gOtherPayTypes.add(new DropDownField("bmlPromoCode2","BillMeLater Promo Code 2",new com.mes.api.bml.test.PromoCodeTable("select one"),true));
      gOtherPayTypes.add(new Field("bmlMerchId2","BillMeLater Merchant ID 2",32,20,true));
      gOtherPayTypes.add(new DropDownField("bmlPromoCode3","BillMeLater Promo Code 3",new com.mes.api.bml.test.PromoCodeTable("select one"),true));
      gOtherPayTypes.add(new Field("bmlMerchId3","BillMeLater Merchant ID 3",32,20,true));
      gOtherPayTypes.add(new DropDownField("bmlPromoCode4","BillMeLater Promo Code 4",new com.mes.api.bml.test.PromoCodeTable("select one"),true));
      gOtherPayTypes.add(new Field("bmlMerchId4","BillMeLater Merchant ID 4",32,20,true));
      gOtherPayTypes.add(new DropDownField("bmlPromoCode5","BillMeLater Promo Code 5",new com.mes.api.bml.test.PromoCodeTable("select one"),true));
      gOtherPayTypes.add(new Field("bmlMerchId5","BillMeLater Merchant ID 5",32,20,true));

      CheckboxField fBmlAccept = (CheckboxField)gOtherPayTypes.getField("bmlAccept");
      gOtherPayTypes.getField("bmlMerchId1").addValidation(new BmlCoreMerchIdValidation(fBmlAccept));
      Field fPromoCode = gOtherPayTypes.getField("bmlPromoCode2");
      fPromoCode.addValidation(new BmlPromoCodeValidation(fBmlAccept));
      gOtherPayTypes.getField("bmlMerchId2").addValidation(new BmlMerchIdValidation(fBmlAccept,fPromoCode));
      fPromoCode = gOtherPayTypes.getField("bmlPromoCode3");
      fPromoCode.addValidation(new BmlPromoCodeValidation(fBmlAccept));
      gOtherPayTypes.getField("bmlMerchId3").addValidation(new BmlMerchIdValidation(fBmlAccept,fPromoCode));
      fPromoCode = gOtherPayTypes.getField("bmlPromoCode4");
      fPromoCode.addValidation(new BmlPromoCodeValidation(fBmlAccept));
      gOtherPayTypes.getField("bmlMerchId4").addValidation(new BmlMerchIdValidation(fBmlAccept,fPromoCode));
      fPromoCode = gOtherPayTypes.getField("bmlPromoCode5");
      fPromoCode.addValidation(new BmlPromoCodeValidation(fBmlAccept));
      gOtherPayTypes.getField("bmlMerchId5").addValidation(new BmlMerchIdValidation(fBmlAccept,fPromoCode));

      fields.add(gOtherPayTypes);

      // make fields read-only
      fields.getField("merchantId").makeReadOnly();
      fields.getField("catid").makeReadOnly();
      fields.getField("appChargeDesc").makeReadOnly();
      fields.getField("merchantKey").makeReadOnly();
      fields.getField("secondaryKey").makeReadOnly();
      
      // add submit buttons
      fields.add(new ButtonField("create", "Create This Profile")); // used only for creating new profiles
      fields.add(new ButtonField("update", "Update This Profile")); // used only for updating existing profiles
      fields.add(new ButtonField("updateAll", "** Update All Profiles for this Merchant **"));  // used only for updating existing profiles
      fields.add(new ButtonField("cancel", "Cancel"));
      
      // special button for defaulting amex info from the app
      fields.add(new ButtonField("getAmex", "Default from OLA"));
      
      // fields used in Profile History
      fields.add(new Field("baseUrl", "Base URL", 500, 500, true));
      fields.add(new Field("expand", "Expand Index", 3, 3, true));
      
      // set html extra
      fields.setHtmlExtra("class=\"formText\"");
      
      gOtherPayTypes.getField("achpCcdAccept").addHtmlExtra("disabled");
      gOtherPayTypes.getField("achpPpdAccept").addHtmlExtra("disabled");
      gOtherPayTypes.getField("achpWebAccept").addHtmlExtra("disabled");
      gOtherPayTypes.getField("achpTelAccept").addHtmlExtra("disabled");

      getField("updateAll").setHtmlExtra("class=\"formTextRed\"");
    }
    catch( Exception e )
    {
      logEntry("createFields()",e.toString());
    }
  }
  
  public void convertAccount( long merchantId, String vnum, String downloadId, 
                              int storeNum, int termNum, String productCode )
  {
    ResultSetIterator   it        = null;
    ResultSet           resultSet = null;
    
    try
    {
      System.out.println("convertAccount(" + merchantId + "," + vnum + "," + downloadId + "," + storeNum + "," + termNum + "," + productCode + ")");
    
      // extract the conversion data from Oracle for this merchant
      /*@lineinfo:generated-code*//*@lineinfo:709^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(mf.bank_number,
//                    3858, '0',
//                    '')||to_char(mf.merchant_number)  as merchant_id,
//                  ( decode(mf.bank_number,
//                           3858, rpad(('0' || to_char(mf.merchant_number)),12,'0'),
//                           lpad(mf.merchant_number,12,'0')) || 
//                    lpad(:storeNum,4,'0') ||
//                    lpad(:termNum,4,'0') )            as tid,
//                  mf.dmaddr                           as loc_addr_1,
//                  mf.dmcity                           as loc_csz_city,
//                  mf.dmstate                          as loc_csz_state,
//                  substr(mf.dmzip,1,5)                as loc_csz_zip,
//                  :downloadId                         as catid,
//                  'Y'                                 as vmc_accept,
//                  decode(mf.bank_number,
//                    3858, '0',
//                    '')||to_char(mf.merchant_number)  as vmc_caid,
//                  decode(mf.bank_number,
//                    3858, '0',
//                    '')||to_char(mf.merchant_number)  as debit_caid,
//                  decode(mf.debit_plan,
//                    null, 'N',
//                    'Y')                              as debit_accept,
//                  mf.dba_name                         as merchant_name,
//                  decode(mpo_amex.app_seq_num,
//                         null,'N','Y')                as amex_accept,
//                  mpo_amex.merchpo_card_merch_number  as amex_caid,
//                  decode(mpo_amex.app_seq_num,
//                         null,null,
//                         '097224' )                   as amex_pcid,
//                  decode(mpo_amex.app_seq_num,
//                         null,null,
//                         'RETAIL' )                   as amex_charge_desc,
//                  decode(mpo_disc.app_seq_num,
//                         null,'N','Y')                as disc_accept,
//                  mpo_disc.merchpo_card_merch_number  as disc_caid,
//                  decode(mpo_jcb.app_seq_num,
//                         null,'N','Y')                as jcb_accept,
//                  mpo_jcb.merchpo_card_merch_number   as jcb_caid,
//                  nvl( :productCode,
//                       substr(mf.user_data_3,3) )     as product_code,
//                  lower(nvl(tpc.trident_payment_gateway,'n')) as api_enabled,
//                  lower(nvl(tpc.trident_payment_gateway,'n')) as merchantKeyRequired,
//                  'N'                                 as secondary_key_enabled,
//                  'A'                                 as profile_status,
//                  null                                as risk_status,
//                  mf.sic_code                         as sic_code,
//                  nvl(mv.time_zone,nvl(zc.timezone,708))  as time_zone,
//                  mf.phone_1                          as loc_phone,
//                  mf.merchant_key                     as merchant_key,
//                  nvl(mbn.bin_number, 
//                      decode( mf.bank_number,
//                              3858,'413747',
//                              3942,'430193',
//                              '433239')   -- default to MES bin
//                      )                               as bin_number,
//                  mv.vnumber                          as old_vnum,
//                  mv.equip_model                      as equip_type,
//                  mv.app_code                         as app_code,
//                  mr.app_seq_num                      as app_seq_num,
//                  28                                  as auth_exp_days
//          from    mif             mf,
//                  merchant        mr,
//                  merchpayoption  mpo_amex,
//                  merchpayoption  mpo_disc,
//                  merchpayoption  mpo_jcb,
//                  application     app,
//                  mms_bin_numbers mbn,
//                  merch_vnumber   mv,
//                  trident_product_codes tpc,
//                  zip_codes       zc
//          where   mf.merchant_number = :merchantId and 
//                  mr.merch_number(+) = mf.merchant_number and
//                  mpo_amex.app_seq_num(+) = mr.app_seq_num and
//                  mpo_amex.cardtype_code(+) = :mesConstants.APP_CT_AMEX and -- 16 and
//                  mpo_disc.app_seq_num(+) = mr.app_seq_num and
//                  mpo_disc.cardtype_code(+) = :mesConstants.APP_CT_DISCOVER and -- 14 and
//                  mpo_jcb.app_seq_num(+) = mr.app_seq_num and
//                  mpo_jcb.cardtype_code(+) = :mesConstants.APP_CT_JCB and -- 15 and
//                  mr.app_seq_num = app.app_seq_num(+) and
//                  app.app_type = mbn.app_type(+) and
//                  mr.app_seq_num = mv.app_seq_num(+) and
//                  mv.vnumber(+) = :vnum and
//                  mf.dmzip = zc.zip_code(+) and
//                  :productCode = tpc.product_code(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(mf.bank_number,\n                  3858, '0',\n                  '')||to_char(mf.merchant_number)  as merchant_id,\n                ( decode(mf.bank_number,\n                         3858, rpad(('0' || to_char(mf.merchant_number)),12,'0'),\n                         lpad(mf.merchant_number,12,'0')) || \n                  lpad( :1 ,4,'0') ||\n                  lpad( :2 ,4,'0') )            as tid,\n                mf.dmaddr                           as loc_addr_1,\n                mf.dmcity                           as loc_csz_city,\n                mf.dmstate                          as loc_csz_state,\n                substr(mf.dmzip,1,5)                as loc_csz_zip,\n                 :3                          as catid,\n                'Y'                                 as vmc_accept,\n                decode(mf.bank_number,\n                  3858, '0',\n                  '')||to_char(mf.merchant_number)  as vmc_caid,\n                decode(mf.bank_number,\n                  3858, '0',\n                  '')||to_char(mf.merchant_number)  as debit_caid,\n                decode(mf.debit_plan,\n                  null, 'N',\n                  'Y')                              as debit_accept,\n                mf.dba_name                         as merchant_name,\n                decode(mpo_amex.app_seq_num,\n                       null,'N','Y')                as amex_accept,\n                mpo_amex.merchpo_card_merch_number  as amex_caid,\n                decode(mpo_amex.app_seq_num,\n                       null,null,\n                       '097224' )                   as amex_pcid,\n                decode(mpo_amex.app_seq_num,\n                       null,null,\n                       'RETAIL' )                   as amex_charge_desc,\n                decode(mpo_disc.app_seq_num,\n                       null,'N','Y')                as disc_accept,\n                mpo_disc.merchpo_card_merch_number  as disc_caid,\n                decode(mpo_jcb.app_seq_num,\n                       null,'N','Y')                as jcb_accept,\n                mpo_jcb.merchpo_card_merch_number   as jcb_caid,\n                nvl(  :4 ,\n                     substr(mf.user_data_3,3) )     as product_code,\n                lower(nvl(tpc.trident_payment_gateway,'n')) as api_enabled,\n                lower(nvl(tpc.trident_payment_gateway,'n')) as merchantKeyRequired,\n                'N'                                 as secondary_key_enabled,\n                'A'                                 as profile_status,\n                null                                as risk_status,\n                mf.sic_code                         as sic_code,\n                nvl(mv.time_zone,nvl(zc.timezone,708))  as time_zone,\n                mf.phone_1                          as loc_phone,\n                mf.merchant_key                     as merchant_key,\n                nvl(mbn.bin_number, \n                    decode( mf.bank_number,\n                            3858,'413747',\n                            3942,'430193',\n                            '433239')   -- default to MES bin\n                    )                               as bin_number,\n                mv.vnumber                          as old_vnum,\n                mv.equip_model                      as equip_type,\n                mv.app_code                         as app_code,\n                mr.app_seq_num                      as app_seq_num,\n                28                                  as auth_exp_days\n        from    mif             mf,\n                merchant        mr,\n                merchpayoption  mpo_amex,\n                merchpayoption  mpo_disc,\n                merchpayoption  mpo_jcb,\n                application     app,\n                mms_bin_numbers mbn,\n                merch_vnumber   mv,\n                trident_product_codes tpc,\n                zip_codes       zc\n        where   mf.merchant_number =  :5  and \n                mr.merch_number(+) = mf.merchant_number and\n                mpo_amex.app_seq_num(+) = mr.app_seq_num and\n                mpo_amex.cardtype_code(+) =  :6  and -- 16 and\n                mpo_disc.app_seq_num(+) = mr.app_seq_num and\n                mpo_disc.cardtype_code(+) =  :7  and -- 14 and\n                mpo_jcb.app_seq_num(+) = mr.app_seq_num and\n                mpo_jcb.cardtype_code(+) =  :8  and -- 15 and\n                mr.app_seq_num = app.app_seq_num(+) and\n                app.app_type = mbn.app_type(+) and\n                mr.app_seq_num = mv.app_seq_num(+) and\n                mv.vnumber(+) =  :9  and\n                mf.dmzip = zc.zip_code(+) and\n                 :10  = tpc.product_code(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,storeNum);
   __sJT_st.setInt(2,termNum);
   __sJT_st.setString(3,downloadId);
   __sJT_st.setString(4,productCode);
   __sJT_st.setLong(5,merchantId);
   __sJT_st.setInt(6,mesConstants.APP_CT_AMEX);
   __sJT_st.setInt(7,mesConstants.APP_CT_DISCOVER);
   __sJT_st.setInt(8,mesConstants.APP_CT_JCB);
   __sJT_st.setString(9,vnum);
   __sJT_st.setString(10,productCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:796^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        fields.resetDefaults(true);   // return fields to default values
        setFields(resultSet,false);

        int     recCount = 0;
        String  termId   = getData("tid");
        /*@lineinfo:generated-code*//*@lineinfo:806^9*/

//  ************************************************************
//  #sql [Ctx] { select count(terminal_id) 
//            from    trident_profile   tp
//            where   tp.terminal_id = :termId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(terminal_id)  \n          from    trident_profile   tp\n          where   tp.terminal_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,termId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:811^9*/
        
        if ( recCount == 0 )
        {
          doInsert(true);   // this is a conversion
        }          
      }        
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("convertAccount(" + merchantId + "," + vnum + "," + downloadId + "," + storeNum + "," + termNum + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  private boolean getAmexFromApp()
  {
    boolean           result  = false;
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:838^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(tapd.pcid, '097224')  amex_pcid,
//                  substr(mr.merch_busgoodserv_descr, 1, 23) amex_charge_desc
//          from    merchant mr,
//                  trident_amex_pcid_default tapd
//          where   mr.merch_number = :getData("merchantId") and
//                  mr.sic_code = tapd.sic_code(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_946 = getData("merchantId");
  try {
   String theSqlTS = "select  nvl(tapd.pcid, '097224')  amex_pcid,\n                substr(mr.merch_busgoodserv_descr, 1, 23) amex_charge_desc\n        from    merchant mr,\n                trident_amex_pcid_default tapd\n        where   mr.merch_number =  :1  and\n                mr.sic_code = tapd.sic_code(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_946);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:846^7*/
      
      rs = it.getResultSet();
      setFields(rs);
      
      rs.close();
      it.close();
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("getAmexFromApp()", e.toString());
      log.error("getAmexFromApp(): " + e.toString());
      result = false;
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
    
    return (result);
  }
  
  private void setPGProfileDefaults()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:877^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tppd.credits_enabled api_credits_allowed
//          from    merchant mr,
//                  mif mf,
//                  t_hierarchy th,
//                  trident_profile_pg_defaults tppd
//          where   mr.merch_number = :getData("merchantId")
//                  and mr.merch_number = mf.merchant_number(+)
//                  and nvl(mf.association_node,  (mr.merch_bank_number*1000000)+mr.asso_number) = th.descendent
//                  and th.hier_type = 1
//                  and th.ancestor = tppd.hierarchy_node
//          order by th.relation
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_947 = getData("merchantId");
  try {
   String theSqlTS = "select  tppd.credits_enabled api_credits_allowed\n        from    merchant mr,\n                mif mf,\n                t_hierarchy th,\n                trident_profile_pg_defaults tppd\n        where   mr.merch_number =  :1 \n                and mr.merch_number = mf.merchant_number(+)\n                and nvl(mf.association_node,  (mr.merch_bank_number*1000000)+mr.asso_number) = th.descendent\n                and th.hier_type = 1\n                and th.ancestor = tppd.hierarchy_node\n        order by th.relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_947);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:890^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        // set fields but don't scan the resultset because we already have.
        // we only want the first entry from this resultset if it exists
        setFields(rs, false);
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("setPGPRofileDefaults()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private boolean generateProfileRequestId(String requestId)
  {
    boolean result = false;
    
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      setData("requestId", requestId);
      
      log.debug("running mms_stage_info query");
      /*@lineinfo:generated-code*//*@lineinfo:927^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(length(mr.merch_number),
//                    8, '0', 
//                    '')||to_char(mr.merch_number) merchant_id,
//                  msi.business_name             merchant_name,
//                  msi.mcc                       sic_code,
//                  msi.time_zone                 time_zone,
//                  msi.business_address          loc_addr_1,
//                  msi.business_city             loc_csz_city,
//                  msi.business_state            loc_csz_state,
//                  substr(msi.business_zip,1,5)  loc_csz_zip,
//                  case
//                    when mr.customer_service_phone is null or mr.customer_service_phone = 0
//                    then msi.merch_phone
//                    else to_char(mr.customer_service_phone)
//                  end                           loc_phone,
//                  decode(msi.term_application_profgen,
//                    'TRI-API',     'y',
//                    'TRI-GATEWAY', 'y',
//                    'VT-LIMITED',  'y',
//                    'TELE-PAY',    'y',
//                    'PAY-HERE',    'y',
//                    'n')                        api_enabled,
//                  nvl(msi.merchant_key_required, 'n') merchant_key_required,
//                  nvl(mf.merchant_key, generate_merchant_key(mr.merch_number)) merchant_key,
//                  'A'                           profile_status,
//                  ''                            risk_status,
//                  'y'                           vmc_accept,
//                  decode(length(mr.merch_number),
//                    8, '0',
//                    '')||to_char(mr.merch_number) vmc_caid,
//                  decode(length(mr.merch_number),
//                    8, '0',
//                    '')||to_char(mr.merch_number) debit_caid,
//                  decode(mpo.app_seq_num,
//                           null,'n',
//                           'y')                   debit_accept,
//                  decode(msi.amex_ac,
//                    1, 'y', 'n')                amex_accept,
//                  nvl(msi.amex, '')             amex_caid,
//                  decode(msi.disc_ac,
//                    1, 'y', 'n')                disc_accept,
//                  nvl(msi.disc, '')             disc_caid,
//                  decode(msi.jcb_ac,
//                    1, 'y', 'n')                jcb_accept,
//                  nvl(msi.jcb, '')              jcb_caid,
//                  mr.merch_busgoodserv_descr    app_charge_desc,
//  --                decode(msi.term_application_profgen,
//  --                  'TRI-API',      'y',
//  --                  'TRI-GATEWAY',  'y',
//  --                  'VT-LIMITED',   'y',
//  --                  'PAYANY',       'y',
//  --                  'TELE-PAY',     'n',
//  --                  'PAY-HERE',     'n',
//  --                  'n')                        api_credits_allowed,
//                  decode(eq.equipmfgr_mfr_code,
//                    20, 'y',
//                    'n')                        api_credits_allowed,
//                  decode(msi.amex_ac,
//                    1, nvl(tapd.pcid, '097224'),
//                    '')                         amex_pcid,
//                  decode(msi.amex_ac,
//                    1, substr(mr.merch_busgoodserv_descr, 1, 23),
//                    '')                         amex_charge_desc,
//                  decode(eq.equipmfgr_mfr_code,
//                    20, 'GS',
//                    nvl(mta.product_code,''))   product_code,
//                  etp.catid_type                catid_type,
//                  msi.bin                       bin_number,
//                  decode(mf.discover_map,
//                    'N', 'n',
//                    'Y', 'y',
//                    'n')                        use_discover_aid,
//                  28                            auth_exp_days
//          from    mms_stage_info                msi,
//                  merchant                      mr,
//                  trident_amex_pcid_default     tapd,
//                  equip_termapp_profgen         etp,
//                  mms_terminal_applications     mta,
//                  merch_pos                     mp,
//                  merchpayoption                mpo,
//                  mif                           mf,
//                  equipment                     eq
//          where   msi.request_id = :requestId and
//                  msi.app_seq_num = mr.app_seq_num and
//                  mr.merch_number = mf.merchant_number(+) and
//                  msi.term_application_profgen = etp.terminal_application(+) and
//                  msi.mcc = tapd.sic_code(+) and
//                  msi.app_seq_num = mp.app_seq_num(+) and
//                  msi.term_application_profgen = mta.terminal_application(+)
//                  and mpo.app_seq_num(+) = msi.app_seq_num and
//                  mpo.cardtype_code(+) = 17
//                  and mpo.cardtype_code(+) = 17 and
//                  msi.equip_model = eq.equip_model(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(length(mr.merch_number),\n                  8, '0', \n                  '')||to_char(mr.merch_number) merchant_id,\n                msi.business_name             merchant_name,\n                msi.mcc                       sic_code,\n                msi.time_zone                 time_zone,\n                msi.business_address          loc_addr_1,\n                msi.business_city             loc_csz_city,\n                msi.business_state            loc_csz_state,\n                substr(msi.business_zip,1,5)  loc_csz_zip,\n                case\n                  when mr.customer_service_phone is null or mr.customer_service_phone = 0\n                  then msi.merch_phone\n                  else to_char(mr.customer_service_phone)\n                end                           loc_phone,\n                decode(msi.term_application_profgen,\n                  'TRI-API',     'y',\n                  'TRI-GATEWAY', 'y',\n                  'VT-LIMITED',  'y',\n                  'TELE-PAY',    'y',\n                  'PAY-HERE',    'y',\n                  'n')                        api_enabled,\n                nvl(msi.merchant_key_required, 'n') merchant_key_required,\n                nvl(mf.merchant_key, generate_merchant_key(mr.merch_number)) merchant_key,\n                'A'                           profile_status,\n                ''                            risk_status,\n                'y'                           vmc_accept,\n                decode(length(mr.merch_number),\n                  8, '0',\n                  '')||to_char(mr.merch_number) vmc_caid,\n                decode(length(mr.merch_number),\n                  8, '0',\n                  '')||to_char(mr.merch_number) debit_caid,\n                decode(mpo.app_seq_num,\n                         null,'n',\n                         'y')                   debit_accept,\n                decode(msi.amex_ac,\n                  1, 'y', 'n')                amex_accept,\n                nvl(msi.amex, '')             amex_caid,\n                decode(msi.disc_ac,\n                  1, 'y', 'n')                disc_accept,\n                nvl(msi.disc, '')             disc_caid,\n                decode(msi.jcb_ac,\n                  1, 'y', 'n')                jcb_accept,\n                nvl(msi.jcb, '')              jcb_caid,\n                mr.merch_busgoodserv_descr    app_charge_desc,\n--                decode(msi.term_application_profgen,\n--                  'TRI-API',      'y',\n--                  'TRI-GATEWAY',  'y',\n--                  'VT-LIMITED',   'y',\n--                  'PAYANY',       'y',\n--                  'TELE-PAY',     'n',\n--                  'PAY-HERE',     'n',\n--                  'n')                        api_credits_allowed,\n                decode(eq.equipmfgr_mfr_code,\n                  20, 'y',\n                  'n')                        api_credits_allowed,\n                decode(msi.amex_ac,\n                  1, nvl(tapd.pcid, '097224'),\n                  '')                         amex_pcid,\n                decode(msi.amex_ac,\n                  1, substr(mr.merch_busgoodserv_descr, 1, 23),\n                  '')                         amex_charge_desc,\n                decode(eq.equipmfgr_mfr_code,\n                  20, 'GS',\n                  nvl(mta.product_code,''))   product_code,\n                etp.catid_type                catid_type,\n                msi.bin                       bin_number,\n                decode(mf.discover_map,\n                  'N', 'n',\n                  'Y', 'y',\n                  'n')                        use_discover_aid,\n                28                            auth_exp_days\n        from    mms_stage_info                msi,\n                merchant                      mr,\n                trident_amex_pcid_default     tapd,\n                equip_termapp_profgen         etp,\n                mms_terminal_applications     mta,\n                merch_pos                     mp,\n                merchpayoption                mpo,\n                mif                           mf,\n                equipment                     eq\n        where   msi.request_id =  :1  and\n                msi.app_seq_num = mr.app_seq_num and\n                mr.merch_number = mf.merchant_number(+) and\n                msi.term_application_profgen = etp.terminal_application(+) and\n                msi.mcc = tapd.sic_code(+) and\n                msi.app_seq_num = mp.app_seq_num(+) and\n                msi.term_application_profgen = mta.terminal_application(+)\n                and mpo.app_seq_num(+) = msi.app_seq_num and\n                mpo.cardtype_code(+) = 17\n                and mpo.cardtype_code(+) = 17 and\n                msi.equip_model = eq.equip_model(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,requestId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1022^7*/
      
      rs = it.getResultSet();
      setFields(rs);
      
      rs.close();
      it.close();
      
      // establish new catid based on catid type
      setData("catid", getNewTridentCatid(getField("catidType").asInteger()));
      
      // get terminal number
      String terminalNumber = "1";
      /*@lineinfo:generated-code*//*@lineinfo:1035^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(max(to_number(substr(tp.terminal_id, 17, 4))), 0)+1 terminal_number
//          from    trident_profile tp
//          where   tp.merchant_number = :getData("merchantId")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_948 = getData("merchantId");
  try {
   String theSqlTS = "select  nvl(max(to_number(substr(tp.terminal_id, 17, 4))), 0)+1 terminal_number\n        from    trident_profile tp\n        where   tp.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_948);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1040^7*/
      
      rs = it.getResultSet();
      if(rs.next())
      {
        terminalNumber = rs.getString("terminal_number");
      }
      
      rs.close();
      it.close();
      
      // create full tid from merchant number and terminal number
      ((TridentTidField)(fields.getField("tid"))).createTid(getData("merchantId"), terminalNumber);
      
      // set profile defaults for payment gateway profiles
      // DISABLED -- the only config param this sets is apiCreditsAllowed and that has already been set by the previous query
      if( false && ((CheckboxField)(fields.getField("apiEnabled"))).isChecked() ) //("y").equals(getData("apiEnabled")) )
      {
        setPGProfileDefaults();
      }
      
      if( getData("productCode").equals(mesConstants.TRIDENT_PC_TELE_PAY) )
      {
        // set some defaults
        setData("merchantKeyRequired", "y");
        setData("apiEnabled", "y");
        setData("apiCreditsAllowed", "y");
      }
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("generateProfileRequestId(" + requestId + ")", e.toString());
      log.error("generateProfileRequestId(" + requestId + "): " + e.toString());
      errorString = "generateProfileRequestId("+requestId+"): " + e.toString();
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
    
    return (result);
  }
  
  private boolean generateProfile()
  {
    boolean             result      = false;
    int                 keyCode     = KC_INVALID;
    
    try
    {
      // get key type
      if(KeyTypes.containsKey(getData("keyType")))
      {
        keyCode = ((Integer)(KeyTypes.get(getData("keyType")))).intValue();
      }
      
      switch(keyCode)
      {
        case KC_REQUEST_ID:
          result = generateProfileRequestId(getData("key"));
          break;
        case KC_INVALID:
        default:
          result = false;
          errorString = "Invalid key type for profile creation (" + getData("keyType") + ")";
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("generateProfile()", e.toString());
    }
    
    return (result);
  }
  
  public int getFieldGroupCount()
  {
    return( ProfileFieldGroups.length );
  }
  
  public String getFieldGroupLabel( int fg )
  {
    return( ProfileFieldGroups[fg].Label );
  }
  
  public String getFieldGroupName( int fg )
  {
    return( ProfileFieldGroups[fg].FieldName );
  }
  
  public Vector getSearchRows( )
  {
    return( SearchRows );
  }
  
  private boolean loadProfileTid(String tid)
  {
    boolean           result    = false;
    ResultSetIterator it        = null;
    ResultSet         rs        = null;  
    
    try
    {
      // load profile from trident_profile table
      /*@lineinfo:generated-code*//*@lineinfo:1148^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tp.terminal_id                      as tid,
//                  tp.addr_city                        as loc_csz_city, 
//                  tp.addr_line_1                      as loc_addr_1, 
//                  tp.addr_state                       as loc_csz_state, 
//                  tp.addr_zip                         as loc_csz_zip, 
//                  tp.phone_number                     as loc_phone,
//                  tp.dm_contact_info                  as dm_contact_info,
//                  tp.catid                            as catid,
//                  lower(tp.vmc_accept)                as vmc_accept, 
//                  tp.vmc_caid                         as vmc_caid,
//                  tp.vmc_caid                         as debit_caid,
//                  lower(tp.debit_accept)              as debit_accept, 
//                  lower(tp.amex_accept)               as amex_accept, 
//                  tp.amex_caid                        as amex_caid, 
//                  --tp.creation_date                  as creation_date, 
//                  tp.merchant_name                    as merchant_name, 
//                  lower(tp.disc_accept)               as disc_accept, 
//                  tp.disc_caid                        as disc_caid, 
//                  lower(tp.jcb_accept)                as jcb_accept, 
//                  tp.jcb_caid                         as jcb_caid, 
//                  --tp.last_modified_date             as last_modified_date, 
//                  --tp.last_modified_user             as last_modified_user, 
//                  decode(mf.bank_number, 
//                    3858, '0',
//                    '')||to_char(tp.merchant_number)  as merchant_id, 
//                  tp.product_code                     as product_code, 
//                  nvl(tp.profile_status,'A')          as profile_status, 
//                  tp.risk_status                      as risk_status, 
//                  tp.sic_code                         as sic_code, 
//                  tp.time_zone                        as time_zone,
//                  tp.amex_pcid                        as amex_pcid,
//                  tp.amex_charge_desc                 as amex_charge_desc,
//                  mr.merch_busgoodserv_descr          as app_charge_desc,
//                  nvl(tp.merchant_key, generate_merchant_key(tp.merchant_number)) as merchant_key,
//                  lower(tp.merchant_key_required)     as merchant_key_required,
//                  tp.secondary_key                    as secondary_key,
//                  lower(tp.secondary_key_enabled)     as secondary_key_enabled,
//                  lower(nvl(tp.use_discover_aid,'n')) as use_discover_aid,
//                  lower(nvl(tp.use_amex_gateway, 'n')) as use_amex_gateway,
//                  lower(tp.api_enabled)               as api_enabled,
//                  lower(tp.api_credits_allowed)       as api_credits_allowed,
//                  upper(nvl(tp.dynamic_dba_allowed,'N')) as dynamic_dba_allowed,
//                  tp.dynamic_dba_name_prefix          as dynamic_dba_prefix,
//                  tp.bin_number                       as bin_number,
//                  tp.intl_processor                   as intl_processor,
//                  tpa.intl_credentials                as intl_credentials,
//                  lower(tpa.intl_usd)                 as intl_usd,
//                  nvl(tpa.card_store_level,0)         as card_store_level,
//                  tpa.fx_client_id                    as fx_client_id,
//                  tpa.fx_product_type                 as fx_product_type,
//                  --tpa.amex_cid_response_enabled       as amex_cid,
//                  tpa.achp_ccd_accept                 as achp_ccd_accept,
//                  tpa.achp_ppd_accept                 as achp_ppd_accept,
//                  tpa.achp_web_accept                 as achp_web_accept,
//                  tpa.achp_tel_accept                 as achp_tel_accept,
//                  tpa.bml_accept                      as bml_accept,
//                  tpa.bml_promo_code_1                as bml_promo_code_1,
//                  tpa.bml_merch_id_1                  as bml_merch_id_1,
//                  tpa.bml_promo_code_2                as bml_promo_code_2,
//                  tpa.bml_merch_id_2                  as bml_merch_id_2,
//                  tpa.bml_promo_code_3                as bml_promo_code_3,
//                  tpa.bml_merch_id_3                  as bml_merch_id_3,
//                  tpa.bml_promo_code_4                as bml_promo_code_4,
//                  tpa.bml_merch_id_4                  as bml_merch_id_4,
//                  tpa.bml_promo_code_5                as bml_promo_code_5,
//                  tpa.bml_merch_id_5                  as bml_merch_id_5,
//                  tpa.multi_capture_allowed           as multi_capture,
//                  nvl(tpa.fraud_mode,0)               as fraud_mode,
//                  nvl(tpa.validate_settle_amount,'Y') as val_settle_amt,
//                  nvl(tpa.auth_expiration_days,28)    as auth_exp_days,
//                  nvl(tpa.force_declined_multi_cap,'N') 
//                                                      as force_on_decline,
//                  nvl(tpa.batch_processing_allowed,'N') 
//                                                      as batch_processing,
//                  tpa.batch_processing_email_address  as batch_processing_email
//          from    trident_profile               tp,
//                  trident_profile_api           tpa,
//                  merchant                      mr,
//                  mif                           mf
//          where   tp.terminal_id = :tid and
//                  tp.terminal_id = tpa.terminal_id(+) and
//                  tp.merchant_number = mr.merch_number(+) and
//                  tp.merchant_number = mf.merchant_number(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tp.terminal_id                      as tid,\n                tp.addr_city                        as loc_csz_city, \n                tp.addr_line_1                      as loc_addr_1, \n                tp.addr_state                       as loc_csz_state, \n                tp.addr_zip                         as loc_csz_zip, \n                tp.phone_number                     as loc_phone,\n                tp.dm_contact_info                  as dm_contact_info,\n                tp.catid                            as catid,\n                lower(tp.vmc_accept)                as vmc_accept, \n                tp.vmc_caid                         as vmc_caid,\n                tp.vmc_caid                         as debit_caid,\n                lower(tp.debit_accept)              as debit_accept, \n                lower(tp.amex_accept)               as amex_accept, \n                tp.amex_caid                        as amex_caid, \n                --tp.creation_date                  as creation_date, \n                tp.merchant_name                    as merchant_name, \n                lower(tp.disc_accept)               as disc_accept, \n                tp.disc_caid                        as disc_caid, \n                lower(tp.jcb_accept)                as jcb_accept, \n                tp.jcb_caid                         as jcb_caid, \n                --tp.last_modified_date             as last_modified_date, \n                --tp.last_modified_user             as last_modified_user, \n                decode(mf.bank_number, \n                  3858, '0',\n                  '')||to_char(tp.merchant_number)  as merchant_id, \n                tp.product_code                     as product_code, \n                nvl(tp.profile_status,'A')          as profile_status, \n                tp.risk_status                      as risk_status, \n                tp.sic_code                         as sic_code, \n                tp.time_zone                        as time_zone,\n                tp.amex_pcid                        as amex_pcid,\n                tp.amex_charge_desc                 as amex_charge_desc,\n                mr.merch_busgoodserv_descr          as app_charge_desc,\n                nvl(tp.merchant_key, generate_merchant_key(tp.merchant_number)) as merchant_key,\n                lower(tp.merchant_key_required)     as merchant_key_required,\n                tp.secondary_key                    as secondary_key,\n                lower(tp.secondary_key_enabled)     as secondary_key_enabled,\n                lower(nvl(tp.use_discover_aid,'n')) as use_discover_aid,\n                lower(nvl(tp.use_amex_gateway, 'n')) as use_amex_gateway,\n                lower(tp.api_enabled)               as api_enabled,\n                lower(tp.api_credits_allowed)       as api_credits_allowed,\n                upper(nvl(tp.dynamic_dba_allowed,'N')) as dynamic_dba_allowed,\n                tp.dynamic_dba_name_prefix          as dynamic_dba_prefix,\n                tp.bin_number                       as bin_number,\n                tp.intl_processor                   as intl_processor,\n                tpa.intl_credentials                as intl_credentials,\n                lower(tpa.intl_usd)                 as intl_usd,\n                nvl(tpa.card_store_level,0)         as card_store_level,\n                tpa.fx_client_id                    as fx_client_id,\n                tpa.fx_product_type                 as fx_product_type,\n                --tpa.amex_cid_response_enabled       as amex_cid,\n                tpa.achp_ccd_accept                 as achp_ccd_accept,\n                tpa.achp_ppd_accept                 as achp_ppd_accept,\n                tpa.achp_web_accept                 as achp_web_accept,\n                tpa.achp_tel_accept                 as achp_tel_accept,\n                tpa.bml_accept                      as bml_accept,\n                tpa.bml_promo_code_1                as bml_promo_code_1,\n                tpa.bml_merch_id_1                  as bml_merch_id_1,\n                tpa.bml_promo_code_2                as bml_promo_code_2,\n                tpa.bml_merch_id_2                  as bml_merch_id_2,\n                tpa.bml_promo_code_3                as bml_promo_code_3,\n                tpa.bml_merch_id_3                  as bml_merch_id_3,\n                tpa.bml_promo_code_4                as bml_promo_code_4,\n                tpa.bml_merch_id_4                  as bml_merch_id_4,\n                tpa.bml_promo_code_5                as bml_promo_code_5,\n                tpa.bml_merch_id_5                  as bml_merch_id_5,\n                tpa.multi_capture_allowed           as multi_capture,\n                nvl(tpa.fraud_mode,0)               as fraud_mode,\n                nvl(tpa.validate_settle_amount,'Y') as val_settle_amt,\n                nvl(tpa.auth_expiration_days,28)    as auth_exp_days,\n                nvl(tpa.force_declined_multi_cap,'N') \n                                                    as force_on_decline,\n                nvl(tpa.batch_processing_allowed,'N') \n                                                    as batch_processing,\n                tpa.batch_processing_email_address  as batch_processing_email\n        from    trident_profile               tp,\n                trident_profile_api           tpa,\n                merchant                      mr,\n                mif                           mf\n        where   tp.terminal_id =  :1  and\n                tp.terminal_id = tpa.terminal_id(+) and\n                tp.merchant_number = mr.merch_number(+) and\n                tp.merchant_number = mf.merchant_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,tid);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1233^7*/
      
      rs = it.getResultSet();
      
      if ( rs.next() )
      {
        // set the field data (do not advance result set)
        setFields(rs,false);
        setIntlCredentials(rs);
        result = true;
      }
      else
      {
        result = false;
        errorString = "No Profile found for " + tid;
      }
      
      rs.close();
      it.close();
      
      result = true;
    }
    catch (Exception e)
    {
      logEntry("loadProfile("+tid+")",e.toString());
      log.error("loadProfile("+tid+"): " + e.toString());
      result = false;
      errorString = "loadProfile("+tid+"): " + e.toString();
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception ee){}
    }
    
    return (result);
  }
  
  private boolean loadProfile() 
  {
    boolean result  = false;
    int     keyCode = KC_INVALID;
    
    try
    {
      // get key type
      if(KeyTypes.containsKey(getData("keyType")))
      {
        keyCode = ((Integer)(KeyTypes.get(getData("keyType")))).intValue();
      }
      
      switch(keyCode)
      {
        case KC_TID:
          result = loadProfileTid(getData("key"));
          break;
        case KC_INVALID:
        default:
          result = false;
          errorString = "Invalid key type for profile loading (" + getData("keyType") + ")";
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("loadProfile()", e.toString());
      log.error("loadProfile(): " + e.toString());
    }
    
    return (result);
  }
  
  public void loadSearchData()
  {
    int               appType       = -1;
    double            doubleVal     = 0.0;
    ResultSetIterator it            = null;
    long              longVal       = -1L;
    ResultSet         resultSet     = null;  
    String            searchValue   = getData("searchValue");
    java.sql.Date     fromDate      = null;
    java.sql.Date     toDate        = null;
    long              searchNode    = 9999999999L;
    int               profGen       = -1;
    
    try
    {
      connect();
      
      // clear the results
      SearchRows.removeAllElements();
      
      // get the value as a long and double if possible
      longVal   = getLong("searchValue",-1L);
      doubleVal = getDouble("searchValue",0.0);
      
      if(!getData("fromDate").equals(""))
      {
        fromDate = new java.sql.Date(((DateStringField)(getField("fromDate"))).getUtilDate().getTime());
      }
      else
      {
        // set to earliest possible create/modify date (11/8/2004)
        Calendar cal = Calendar.getInstance();
        cal.set(2004, Calendar.NOVEMBER, 8);
        fromDate = new java.sql.Date(cal.getTime().getTime());
      }
      
      if(!getData("toDate").equals(""))
      {
        toDate = new java.sql.Date(((DateStringField)(getField("toDate"))).getUtilDate().getTime());
      }
      else
      {
        // use current calendar time
        toDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
      }
      
      // extract the remaining query params
      appType     = getInt("appType",-1);
      profGen     = getInt("profileGen",-1);
      searchNode  = getLong("portfolio",getUser().getHierarchyNode());
      
      // load data from merchant
      /*@lineinfo:generated-code*//*@lineinfo:1357^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tp.terminal_id              tid,
//                  tp.merchant_number          merchant_number, 
//                  tp.merchant_name            merchant_name, 
//                  mf.association_node         assoc_number,
//                  tp.addr_city                loc_csz_city, 
//                  tp.addr_line_1              loc_addr_1, 
//                  tp.addr_state               loc_csz_state, 
//                  tp.addr_zip                 loc_csz_zip, 
//                  tp.phone_number             loc_phone,
//                  tp.dm_contact_info          dm_contact_info,
//                  tp.sic_code                 sic_code, 
//                  tp.time_zone                time_zone,
//                  tp.product_code             product_code, 
//                  nvl(tp.profile_status,'A')  profile_status, 
//                  tp.risk_status              risk_status, 
//                  tp.catid                    catid,
//                  lower(tp.vmc_accept)        vmc_accept, 
//                  tp.vmc_caid                 vmc_caid,
//                  tp.vmc_caid                 debit_caid,
//                  lower(tp.amex_accept)       amex_accept, 
//                  tp.amex_caid                amex_caid, 
//                  lower(tp.disc_accept)       disc_accept, 
//                  tp.disc_caid                disc_caid, 
//                  lower(tp.jcb_accept)        jcb_accept, 
//                  tp.jcb_caid                 jcb_caid, 
//                  tp.creation_date            creation_date,
//                  tp.last_modified_date       last_modified_date, 
//                  tp.last_modified_user       last_modified_user,
//                  u.name                      last_modified_name,
//                  tp.creation_date            creation_date,
//                  nvl(epg.name, 'Unknown')    prof_gen_code
//          from    trident_profile             tp,
//                  mif                         mf,
//                  t_hierarchy                 th,
//                  merchant                    mr,
//                  application                 app,
//                  mms_stage_info              msi,
//                  users                       u,
//                  equip_profile_generators    epg
//          where   (
//                    ( tp.terminal_id     = :searchValue ) or
//                    ( tp.merchant_number = :longVal ) or
//                    ( lower(tp.merchant_name) like lower('%' || :searchValue || '%') ) or
//                    ( mf.association_node = :longVal ) or
//                    ( mf.dmagent = :searchValue ) or
//                    ( tp.catid = :searchValue ) or
//                    ( lower(tp.last_modified_user) like lower('%'||:searchValue||'%') )
//                  ) and
//                  (
//                    trunc(tp.creation_date) between :fromDate and :toDate or
//                    trunc(tp.last_modified_date) between :fromDate and :toDate
//                  ) and
//                  th.ancestor = :searchNode and
//                  (
//                    (:searchNode = 99999999 and th.descendent = :searchNode) or
//                    th.descendent = mf.association_node
//                  ) and
//                  (
//                    :appType = -1 or 
//                    app.app_type = :appType
//                  ) and
//                  (
//                    :profGen = -1 or
//                    epg.code = :profGen
//                  ) and
//                  tp.last_modified_user = u.login_name(+) and
//                  tp.merchant_number = mf.merchant_number(+) and
//                  tp.merchant_number = mr.merch_number(+) and
//                  mr.app_seq_num = app.app_seq_num(+) and
//                  tp.catid = msi.vnum(+) and
//                  msi.profile_generator_code = epg.code(+)
//          order by tp.terminal_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tp.terminal_id              tid,\n                tp.merchant_number          merchant_number, \n                tp.merchant_name            merchant_name, \n                mf.association_node         assoc_number,\n                tp.addr_city                loc_csz_city, \n                tp.addr_line_1              loc_addr_1, \n                tp.addr_state               loc_csz_state, \n                tp.addr_zip                 loc_csz_zip, \n                tp.phone_number             loc_phone,\n                tp.dm_contact_info          dm_contact_info,\n                tp.sic_code                 sic_code, \n                tp.time_zone                time_zone,\n                tp.product_code             product_code, \n                nvl(tp.profile_status,'A')  profile_status, \n                tp.risk_status              risk_status, \n                tp.catid                    catid,\n                lower(tp.vmc_accept)        vmc_accept, \n                tp.vmc_caid                 vmc_caid,\n                tp.vmc_caid                 debit_caid,\n                lower(tp.amex_accept)       amex_accept, \n                tp.amex_caid                amex_caid, \n                lower(tp.disc_accept)       disc_accept, \n                tp.disc_caid                disc_caid, \n                lower(tp.jcb_accept)        jcb_accept, \n                tp.jcb_caid                 jcb_caid, \n                tp.creation_date            creation_date,\n                tp.last_modified_date       last_modified_date, \n                tp.last_modified_user       last_modified_user,\n                u.name                      last_modified_name,\n                tp.creation_date            creation_date,\n                nvl(epg.name, 'Unknown')    prof_gen_code\n        from    trident_profile             tp,\n                mif                         mf,\n                t_hierarchy                 th,\n                merchant                    mr,\n                application                 app,\n                mms_stage_info              msi,\n                users                       u,\n                equip_profile_generators    epg\n        where   (\n                  ( tp.terminal_id     =  :1  ) or\n                  ( tp.merchant_number =  :2  ) or\n                  ( lower(tp.merchant_name) like lower('%' ||  :3  || '%') ) or\n                  ( mf.association_node =  :4  ) or\n                  ( mf.dmagent =  :5  ) or\n                  ( tp.catid =  :6  ) or\n                  ( lower(tp.last_modified_user) like lower('%'|| :7 ||'%') )\n                ) and\n                (\n                  trunc(tp.creation_date) between  :8  and  :9  or\n                  trunc(tp.last_modified_date) between  :10  and  :11 \n                ) and\n                th.ancestor =  :12  and\n                (\n                  ( :13  = 99999999 and th.descendent =  :14 ) or\n                  th.descendent = mf.association_node\n                ) and\n                (\n                   :15  = -1 or \n                  app.app_type =  :16 \n                ) and\n                (\n                   :17  = -1 or\n                  epg.code =  :18 \n                ) and\n                tp.last_modified_user = u.login_name(+) and\n                tp.merchant_number = mf.merchant_number(+) and\n                tp.merchant_number = mr.merch_number(+) and\n                mr.app_seq_num = app.app_seq_num(+) and\n                tp.catid = msi.vnum(+) and\n                msi.profile_generator_code = epg.code(+)\n        order by tp.terminal_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,searchValue);
   __sJT_st.setLong(2,longVal);
   __sJT_st.setString(3,searchValue);
   __sJT_st.setLong(4,longVal);
   __sJT_st.setString(5,searchValue);
   __sJT_st.setString(6,searchValue);
   __sJT_st.setString(7,searchValue);
   __sJT_st.setDate(8,fromDate);
   __sJT_st.setDate(9,toDate);
   __sJT_st.setDate(10,fromDate);
   __sJT_st.setDate(11,toDate);
   __sJT_st.setLong(12,searchNode);
   __sJT_st.setLong(13,searchNode);
   __sJT_st.setLong(14,searchNode);
   __sJT_st.setInt(15,appType);
   __sJT_st.setInt(16,appType);
   __sJT_st.setInt(17,profGen);
   __sJT_st.setInt(18,profGen);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1431^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        SearchRows.add( new SearchRowData( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry("loadSearchData()",e.toString());
    }
    finally
    {
      try{ resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception ee) {}
      cleanUp();
    }
  }
  
  public boolean searchFieldsSet()
  {
    boolean result = false;
    
    if(!getData("searchButton").equals(""))
    {
      if(!getData("fromDate").equals("") ||
         !getData("toDate").equals("") ||
         !getData("portfolio").equals("") || 
         !getData("profileGen").equals("-1") ||
         !getData("appType").equals("") ||
         !getData("searchValue").equals(""))
      {
        result = true;
     }
   }
    
    return (result);
  }
  
  public void setFields( HttpServletRequest request )
  {
    String            deleteTid     = null;
    
    super.setFields(request);
    
    if(searchFieldsSet())
    {
      loadSearchData();
    }
  }

  protected void setIntlCredentials( ResultSet rs )
    throws java.sql.SQLException
  {
    String    value   = rs.getString("intl_credentials");
    
    if ( !isBlank(value) )
    {
      String creds[] = value.split(",");
      for( int i = 0; i < creds.length; ++i )
      {
        setData(("intlCredentials-" + i),creds[i]);
      }
    }
  }
    
  private boolean doInsert( )
  {
    return( doInsert(false) );    // standard insert
  }
  
  private boolean doInsert( boolean conversionAccount )
  {
    String    modifyUser  = null;
    boolean   result      = false;
    
    try
    {
      try
      {
        modifyUser = user.getLoginName();
      }
      catch( Exception noUser_e )
      {
        modifyUser = "system";
      }

      // update api extension data before the main profile record
      ApiExtensionData apiData  = new ApiExtensionData();
      apiData.tid               = getData("tid");
      apiData.intlCredentials   = buildIntlCredentials();
      apiData.intlUSD           = getData("intlUSD");
      apiData.fxClientId        = getData("fxClientId");
      apiData.fxProductType     = getData("fxProductType");
      apiData.achpCcdAccept     = getData("achpCcdAccept");
      apiData.achpPpdAccept     = getData("achpPpdAccept");
      apiData.achpWebAccept     = getData("achpWebAccept");
      apiData.achpTelAccept     = getData("achpTelAccept");
      apiData.bmlAccept         = getData("bmlAccept");
      apiData.bmlPromoCode1     = getData("bmlPromoCode1");
      apiData.bmlMerchId1       = getData("bmlMerchId1");
      apiData.bmlPromoCode2     = getData("bmlPromoCode2");
      apiData.bmlMerchId2       = getData("bmlMerchId2");
      apiData.bmlPromoCode3     = getData("bmlPromoCode3");
      apiData.bmlMerchId3       = getData("bmlMerchId3");
      apiData.bmlPromoCode4     = getData("bmlPromoCode4");
      apiData.bmlMerchId4       = getData("bmlMerchId4");
      apiData.bmlPromoCode5     = getData("bmlPromoCode5");
      apiData.bmlMerchId5       = getData("bmlMerchId5");
      //apiData.amexCid           = getData("amexCid");
      apiData.cardStoreLevel    = getData("cardStoreLevel");
      apiData.fraudMode         = getData("fraudMode");
      apiData.valSettleAmt      = getData("valSettleAmt");
      apiData.authExpDays       = getData("authExpDays");
      apiData.multiCapture      = getData("multiCapture");
      apiData.forceOnDecline    = getData("forceOnDecline");
      apiData.batchProcessing   = getData("batchProcessing");
      apiData.batchProcessingEmail = getData("batchProcessingEmail");
      updateApiExtensionData(apiData);

      /*@lineinfo:generated-code*//*@lineinfo:1554^7*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_profile
//          (
//            terminal_id,
//            addr_city,           
//            addr_line_1,           
//            addr_state,           
//            addr_zip,           
//            catid,
//            vmc_accept,
//            vmc_caid,          
//            debit_accept,
//            amex_accept,           
//            amex_caid,           
//            merchant_name,           
//            disc_accept,           
//            disc_caid,           
//            jcb_accept,           
//            jcb_caid,           
//            creation_date,
//            last_modified_date,           
//            last_modified_user,           
//            merchant_number,           
//            product_code,           
//            profile_status,           
//            risk_status,           
//            sic_code,           
//            time_zone,
//            phone_number,
//            dm_contact_info,
//            amex_pcid,
//            amex_charge_desc,
//            merchant_key,
//            merchant_key_required,
//            api_enabled,
//            api_credits_allowed,
//            bin_number,
//            secondary_key_enabled,
//            dynamic_dba_allowed,
//            dynamic_dba_name_prefix,
//            use_discover_aid,
//            intl_processor
//          )
//          values
//          (
//            :getData("tid"),
//            :getData("locCSZCity"),
//            :getData("locAddr1"),
//            :getData("locCSZState"),
//            :getData("locCSZZip"),
//            :getData("catid"),
//            :getData("vmcAccept").toUpperCase(),
//            :getData("vmcCaid"),
//            :getData("debitAccept").toUpperCase(),
//            :getData("amexAccept").toUpperCase(),
//            :getData("amexCaid"),
//            :getData("merchantName"),
//            :getData("discAccept").toUpperCase(),
//            :getData("discCaid"),
//            :getData("jcbAccept").toUpperCase(),
//            :getData("jcbCaid"),
//            sysdate,
//            sysdate,
//            :modifyUser,
//            :getData("merchantId"),
//            :getData("productCode"),
//            :getData("profileStatus"),
//            :getData("riskStatus"),
//            :getData("sicCode"),
//            :getData("timeZone"),
//            :getData("locPhone"),
//            :getData("dmContactInfo"),
//            :getData("amexPcid"),
//            :getData("amexChargeDesc"),
//            :getData("merchantKey"),
//            upper(:getData("merchantKeyRequired")),
//            upper(:getData("apiEnabled")),
//            upper(:getData("apiCreditsAllowed")),
//            :getData("binNumber"),
//            upper(:getData("secondaryKeyEnabled")),
//            :getData("dynamicDbaAllowed"),
//            upper(:getData("dynamicDbaPrefix")),
//            upper(:getData("useDiscoverAid")),
//            :getData("intlProcessor")
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_949 = getData("tid");
 String __sJT_950 = getData("locCSZCity");
 String __sJT_951 = getData("locAddr1");
 String __sJT_952 = getData("locCSZState");
 String __sJT_953 = getData("locCSZZip");
 String __sJT_954 = getData("catid");
 String __sJT_955 = getData("vmcAccept").toUpperCase();
 String __sJT_956 = getData("vmcCaid");
 String __sJT_957 = getData("debitAccept").toUpperCase();
 String __sJT_958 = getData("amexAccept").toUpperCase();
 String __sJT_959 = getData("amexCaid");
 String __sJT_960 = getData("merchantName");
 String __sJT_961 = getData("discAccept").toUpperCase();
 String __sJT_962 = getData("discCaid");
 String __sJT_963 = getData("jcbAccept").toUpperCase();
 String __sJT_964 = getData("jcbCaid");
 String __sJT_965 = getData("merchantId");
 String __sJT_966 = getData("productCode");
 String __sJT_967 = getData("profileStatus");
 String __sJT_968 = getData("riskStatus");
 String __sJT_969 = getData("sicCode");
 String __sJT_970 = getData("timeZone");
 String __sJT_971 = getData("locPhone");
 String __sJT_972 = getData("dmContactInfo");
 String __sJT_973 = getData("amexPcid");
 String __sJT_974 = getData("amexChargeDesc");
 String __sJT_975 = getData("merchantKey");
 String __sJT_976 = getData("merchantKeyRequired");
 String __sJT_977 = getData("apiEnabled");
 String __sJT_978 = getData("apiCreditsAllowed");
 String __sJT_979 = getData("binNumber");
 String __sJT_980 = getData("secondaryKeyEnabled");
 String __sJT_981 = getData("dynamicDbaAllowed");
 String __sJT_982 = getData("dynamicDbaPrefix");
 String __sJT_983 = getData("useDiscoverAid");
 String __sJT_984 = getData("intlProcessor");
   String theSqlTS = "insert into trident_profile\n        (\n          terminal_id,\n          addr_city,           \n          addr_line_1,           \n          addr_state,           \n          addr_zip,           \n          catid,\n          vmc_accept,\n          vmc_caid,          \n          debit_accept,\n          amex_accept,           \n          amex_caid,           \n          merchant_name,           \n          disc_accept,           \n          disc_caid,           \n          jcb_accept,           \n          jcb_caid,           \n          creation_date,\n          last_modified_date,           \n          last_modified_user,           \n          merchant_number,           \n          product_code,           \n          profile_status,           \n          risk_status,           \n          sic_code,           \n          time_zone,\n          phone_number,\n          dm_contact_info,\n          amex_pcid,\n          amex_charge_desc,\n          merchant_key,\n          merchant_key_required,\n          api_enabled,\n          api_credits_allowed,\n          bin_number,\n          secondary_key_enabled,\n          dynamic_dba_allowed,\n          dynamic_dba_name_prefix,\n          use_discover_aid,\n          intl_processor\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n           :16 ,\n          sysdate,\n          sysdate,\n           :17 ,\n           :18 ,\n           :19 ,\n           :20 ,\n           :21 ,\n           :22 ,\n           :23 ,\n           :24 ,\n           :25 ,\n           :26 ,\n           :27 ,\n           :28 ,\n          upper( :29 ),\n          upper( :30 ),\n          upper( :31 ),\n           :32 ,\n          upper( :33 ),\n           :34 ,\n          upper( :35 ),\n          upper( :36 ),\n           :37 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_949);
   __sJT_st.setString(2,__sJT_950);
   __sJT_st.setString(3,__sJT_951);
   __sJT_st.setString(4,__sJT_952);
   __sJT_st.setString(5,__sJT_953);
   __sJT_st.setString(6,__sJT_954);
   __sJT_st.setString(7,__sJT_955);
   __sJT_st.setString(8,__sJT_956);
   __sJT_st.setString(9,__sJT_957);
   __sJT_st.setString(10,__sJT_958);
   __sJT_st.setString(11,__sJT_959);
   __sJT_st.setString(12,__sJT_960);
   __sJT_st.setString(13,__sJT_961);
   __sJT_st.setString(14,__sJT_962);
   __sJT_st.setString(15,__sJT_963);
   __sJT_st.setString(16,__sJT_964);
   __sJT_st.setString(17,modifyUser);
   __sJT_st.setString(18,__sJT_965);
   __sJT_st.setString(19,__sJT_966);
   __sJT_st.setString(20,__sJT_967);
   __sJT_st.setString(21,__sJT_968);
   __sJT_st.setString(22,__sJT_969);
   __sJT_st.setString(23,__sJT_970);
   __sJT_st.setString(24,__sJT_971);
   __sJT_st.setString(25,__sJT_972);
   __sJT_st.setString(26,__sJT_973);
   __sJT_st.setString(27,__sJT_974);
   __sJT_st.setString(28,__sJT_975);
   __sJT_st.setString(29,__sJT_976);
   __sJT_st.setString(30,__sJT_977);
   __sJT_st.setString(31,__sJT_978);
   __sJT_st.setString(32,__sJT_979);
   __sJT_st.setString(33,__sJT_980);
   __sJT_st.setString(34,__sJT_981);
   __sJT_st.setString(35,__sJT_982);
   __sJT_st.setString(36,__sJT_983);
   __sJT_st.setString(37,__sJT_984);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1640^7*/
      
      if ( conversionAccount )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1644^9*/

//  ************************************************************
//  #sql [Ctx] { update merchant 
//            set     trident_conversion = 'Y'
//            where   merch_number = :getData("merchantId")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_985 = getData("merchantId");
   String theSqlTS = "update merchant \n          set     trident_conversion = 'Y'\n          where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_985);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1649^9*/
        
        // add entry to merch_vnumber and v_numbers
        /*
        #sql [Ctx]
        {
          insert into merch_vnumber
          (
            app_seq_num,
            terminal_number,
            vnumber,
            store_number,
            time_zone,
            location_number,
            app_code,
            equip_model,
            request_id,
            profile_generator_code,
            profile_completed_date,
            tid,
            front_end,
            term_comm_type
          )
          values
          (
            :(getData("appSeqNum")),
            substr(:(getData("tid"), length(:getData("tid")) - 4, 4),
            :(getData("catid")),
            0,
            :(getData("timeZone")),
            1,
            :(getData("appCode")),
            
          )
        };
        */
      }
      else    // not a conversion account
      {
        String sno = ((TridentTidField)(fields.getField("tid"))).getStoreNumber();
        String tno = ((TridentTidField)(fields.getField("tid"))).getTerminalNumber();
      
        // update v number tables to show generated load id
        // ** THIS WILL CAUSE ITEM TO APPEAR IN PROPER PROGRAMMING QUEUE **
        /*@lineinfo:generated-code*//*@lineinfo:1693^9*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info
//            set     vnum = :getData("catid"),
//                    store_number = :sno,
//                    terminal_number = :tno
//            where   request_id = :getData("requestId")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_986 = getData("catid");
 String __sJT_987 = getData("requestId");
   String theSqlTS = "update  mms_stage_info\n          set     vnum =  :1 ,\n                  store_number =  :2 ,\n                  terminal_number =  :3 \n          where   request_id =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_986);
   __sJT_st.setString(2,sno);
   __sJT_st.setString(3,tno);
   __sJT_st.setString(4,__sJT_987);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1700^9*/

        // get programming type to filter through to servlet that will send user
        // to proper programming queue      
        int progType = 0;
      
        /*@lineinfo:generated-code*//*@lineinfo:1706^9*/

//  ************************************************************
//  #sql [Ctx] { select  profile_generator_code
//            
//            from    mms_stage_info
//            where   request_id = :getData("requestId")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_988 = getData("requestId");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  profile_generator_code\n           \n          from    mms_stage_info\n          where   request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_988);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   progType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1712^9*/
      
        // get appSeqNum
        int appSeqNum = 0;
      
        /*@lineinfo:generated-code*//*@lineinfo:1717^9*/

//  ************************************************************
//  #sql [Ctx] { select  app_seq_num
//            
//            from    merchant
//            where   merch_number = :getData("merchantId")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_989 = getData("merchantId");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_seq_num\n           \n          from    merchant\n          where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_989);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1723^9*/
      
        addNextLink("Create Another Profile for this Merchant", "/jsp/credit/mms_screen.jsp?action=add&merchant="+getData("merchantId"));
        addNextLink("Go to Programming Queue", "/srv/tppe?tppeact=" + TPPE_PROGRAMMING_QUEUE +
                                                "&actionId=" + progType + 
                                                "&requestId=" + getData("requestId"));
        addNextLink("Return to POS ID New Queue", "/srv/tppe?tppeact=" + TPPE_MMS_NEW_QUEUE +
                                                "&actionId=" + appSeqNum + 
                                                "&requestId=" + getData("requestId"));
      }

      // update mif
      if( ! ("").equals(getData("discCaid")) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1737^9*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//            set     dmdsnum = :getData("discCaid")
//            where   merchant_number = :getData("merchantId")                
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_990 = getData("discCaid");
 String __sJT_991 = getData("merchantId");
   String theSqlTS = "update  mif\n          set     dmdsnum =  :1 \n          where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_990);
   __sJT_st.setString(2,__sJT_991);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1742^9*/
      }
      
      if( ! ("").equals(getData("amexCaid")) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1747^9*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//            set     damexse = :getData("amexCaid")
//            where   merchant_number = :getData("merchantId")                
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_992 = getData("amexCaid");
 String __sJT_993 = getData("merchantId");
   String theSqlTS = "update  mif\n          set     damexse =  :1 \n          where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_992);
   __sJT_st.setString(2,__sJT_993);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1752^9*/
      }
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("doInsert()", e.toString());
      log.error("doInsert(): " + e.toString());
      result = false;
      errorString = "doInsert(): " + e.toString();
    }
    
    return( result );
  }
  
  public boolean submitUpload( HttpServletRequest request )
  {
    ServletFileUpload  fileUpload      = null;
    boolean     retVal          = false;
    
    try
    {
      createFields(request);    // inialize the data fields
    
      fileUpload = new ServletFileUpload(new DiskFileItemFactory(1048576, new File("./")));
      ArrayList items = (ArrayList)(fileUpload.parseRequest(request));
      
      for( int i = 0; i < items.size(); ++i )
      {
        DiskFileItem fi = (DiskFileItem)items.get(i);
        
        if( fi.getFieldName().equals("uploadFile") )
        {
          // process the data file to generate a work file
          processUploadFile( fi.getString() );
        }
      }
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("submitUpload()", e.toString());
    }
    return( retVal );
  }
  
  private void processUploadFile( String fileContent )
  {
    try
    {
      BufferedReader  in      = new BufferedReader( new StringReader(fileContent) );
      String          line    = null;
      StringTokenizer tokens  = null;

      while( (line = in.readLine()) != null )
      {
        tokens = new StringTokenizer(line,",");
        convertAccount(Long.parseLong(tokens.nextToken()),    // mid
                       tokens.nextToken(),     // vnum
                       tokens.nextToken(),     // dl id
                       Integer.parseInt(tokens.nextToken()),  // store num
                       Integer.parseInt(tokens.nextToken()),  // term num
                       tokens.nextToken());   // product code
      }
      in.close();
    }
    catch( Exception e )
    {
      logEntry("processUploadFile()",e.toString());
    }      
  }    
  
  // NOTE: this should always be called before updates to the main
  // trident_profile record due to how the trigger on that table
  // generates the change log (it needs changes to api extension
  // data to be completed in order to properly log changes to the
  // extension data).
  private boolean updateApiExtensionData(ApiExtensionData data)
    throws Exception
  {
    try
    {
      connect();

      // update payvision data in api table
      /*@lineinfo:generated-code*//*@lineinfo:1838^7*/

//  ************************************************************
//  #sql [Ctx] { update  trident_profile_api
//          set     intl_credentials      = :data.intlCredentials,
//                  intl_usd              = :data.intlUSD,
//                  fx_client_id          = :data.fxClientId,
//                  fx_product_type       = :data.fxProductType,
//                  --achp_ccd_accept       = :(data.achpCcdAccept),
//                  --achp_ppd_accept       = :(data.achpPpdAccept),
//                  --achp_web_accept       = :(data.achpWebAccept),
//                  --achp_tel_accept       = :(data.achpTelAccept),
//                  bml_accept            = :data.bmlAccept,
//                  bml_promo_code_1      = :data.bmlPromoCode1,
//                  bml_merch_id_1        = :data.bmlMerchId1,
//                  bml_promo_code_2      = :data.bmlPromoCode2,
//                  bml_merch_id_2        = :data.bmlMerchId2,
//                  bml_promo_code_3      = :data.bmlPromoCode3,
//                  bml_merch_id_3        = :data.bmlMerchId3,
//                  bml_promo_code_4      = :data.bmlPromoCode4,
//                  bml_merch_id_4        = :data.bmlMerchId4,
//                  bml_promo_code_5      = :data.bmlPromoCode5,
//                  bml_merch_id_5        = :data.bmlMerchId5,
//                  --amex_cid_response_enabled = :(data.amexCid),
//                  card_store_level = :data.cardStoreLevel,
//                  fraud_mode = :data.fraudMode,
//                  validate_settle_amount = :data.valSettleAmt,
//                  auth_expiration_days = :data.authExpDays,
//                  multi_capture_allowed = :data.multiCapture,
//                  force_declined_multi_cap = :data.forceOnDecline,
//                  batch_processing_allowed = :data.batchProcessing,
//                  batch_processing_email_address = :data.batchProcessingEmail
//          where   terminal_id = :data.tid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_profile_api\n        set     intl_credentials      =  :1 ,\n                intl_usd              =  :2 ,\n                fx_client_id          =  :3 ,\n                fx_product_type       =  :4 ,\n                --achp_ccd_accept       = :(data.achpCcdAccept),\n                --achp_ppd_accept       = :(data.achpPpdAccept),\n                --achp_web_accept       = :(data.achpWebAccept),\n                --achp_tel_accept       = :(data.achpTelAccept),\n                bml_accept            =  :5 ,\n                bml_promo_code_1      =  :6 ,\n                bml_merch_id_1        =  :7 ,\n                bml_promo_code_2      =  :8 ,\n                bml_merch_id_2        =  :9 ,\n                bml_promo_code_3      =  :10 ,\n                bml_merch_id_3        =  :11 ,\n                bml_promo_code_4      =  :12 ,\n                bml_merch_id_4        =  :13 ,\n                bml_promo_code_5      =  :14 ,\n                bml_merch_id_5        =  :15 ,\n                --amex_cid_response_enabled = :(data.amexCid),\n                card_store_level =  :16 ,\n                fraud_mode =  :17 ,\n                validate_settle_amount =  :18 ,\n                auth_expiration_days =  :19 ,\n                multi_capture_allowed =  :20 ,\n                force_declined_multi_cap =  :21 ,\n                batch_processing_allowed =  :22 ,\n                batch_processing_email_address =  :23 \n        where   terminal_id =  :24";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,data.intlCredentials);
   __sJT_st.setString(2,data.intlUSD);
   __sJT_st.setString(3,data.fxClientId);
   __sJT_st.setString(4,data.fxProductType);
   __sJT_st.setString(5,data.bmlAccept);
   __sJT_st.setString(6,data.bmlPromoCode1);
   __sJT_st.setString(7,data.bmlMerchId1);
   __sJT_st.setString(8,data.bmlPromoCode2);
   __sJT_st.setString(9,data.bmlMerchId2);
   __sJT_st.setString(10,data.bmlPromoCode3);
   __sJT_st.setString(11,data.bmlMerchId3);
   __sJT_st.setString(12,data.bmlPromoCode4);
   __sJT_st.setString(13,data.bmlMerchId4);
   __sJT_st.setString(14,data.bmlPromoCode5);
   __sJT_st.setString(15,data.bmlMerchId5);
   __sJT_st.setString(16,data.cardStoreLevel);
   __sJT_st.setString(17,data.fraudMode);
   __sJT_st.setString(18,data.valSettleAmt);
   __sJT_st.setString(19,data.authExpDays);
   __sJT_st.setString(20,data.multiCapture);
   __sJT_st.setString(21,data.forceOnDecline);
   __sJT_st.setString(22,data.batchProcessing);
   __sJT_st.setString(23,data.batchProcessingEmail);
   __sJT_st.setString(24,data.tid);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1870^7*/
    
      // no records updated and no exception, try insert
      if ( Ctx.getExecutionContext().getUpdateCount() == 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1875^9*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_profile_api
//            ( 
//              terminal_id, 
//              intl_credentials,
//              intl_usd,
//              fx_client_id,
//              fx_product_type,
//              --achp_ccd_accept,
//              --achp_ppd_accept,
//              --achp_web_accept,
//              --achp_tel_accept,
//              bml_accept,
//              bml_promo_code_1,
//              bml_merch_id_1,
//              bml_promo_code_2,
//              bml_merch_id_2,
//              bml_promo_code_3,
//              bml_merch_id_3,
//              bml_promo_code_4,
//              bml_merch_id_4,
//              bml_promo_code_5,
//              bml_merch_id_5,
//              --amex_cid_response_enabled,
//              card_store_level,
//              fraud_mode,
//              validate_settle_amount,
//              auth_expiration_days,
//              multi_capture_allowed,
//              force_declined_multi_cap,
//              batch_processing_allowed,
//              batch_processing_email_address
//            )
//            values
//            ( 
//              :data.tid, 
//              :data.intlCredentials,
//              :data.intlUSD,
//              :data.fxClientId,
//              :data.fxProductType,
//              --:(data.achpCcdAccept),
//              --:(data.achpPpdAccept),
//              --:(data.achpWebAccept),
//              --:(data.achpTelAccept),
//              :data.bmlAccept,
//              :data.bmlPromoCode1,
//              :data.bmlMerchId1,
//              :data.bmlPromoCode2,
//              :data.bmlMerchId2,
//              :data.bmlPromoCode3,
//              :data.bmlMerchId3,
//              :data.bmlPromoCode4,
//              :data.bmlMerchId4,
//              :data.bmlPromoCode5,
//              :data.bmlMerchId5,
//              --:(data.amexCid),
//              :data.cardStoreLevel,
//              :data.fraudMode,
//              :data.valSettleAmt,
//              :data.authExpDays,
//              :data.multiCapture,
//              :data.forceOnDecline,
//              :data.batchProcessing,
//              :data.batchProcessingEmail
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into trident_profile_api\n          ( \n            terminal_id, \n            intl_credentials,\n            intl_usd,\n            fx_client_id,\n            fx_product_type,\n            --achp_ccd_accept,\n            --achp_ppd_accept,\n            --achp_web_accept,\n            --achp_tel_accept,\n            bml_accept,\n            bml_promo_code_1,\n            bml_merch_id_1,\n            bml_promo_code_2,\n            bml_merch_id_2,\n            bml_promo_code_3,\n            bml_merch_id_3,\n            bml_promo_code_4,\n            bml_merch_id_4,\n            bml_promo_code_5,\n            bml_merch_id_5,\n            --amex_cid_response_enabled,\n            card_store_level,\n            fraud_mode,\n            validate_settle_amount,\n            auth_expiration_days,\n            multi_capture_allowed,\n            force_declined_multi_cap,\n            batch_processing_allowed,\n            batch_processing_email_address\n          )\n          values\n          ( \n             :1 , \n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n            --:(data.achpCcdAccept),\n            --:(data.achpPpdAccept),\n            --:(data.achpWebAccept),\n            --:(data.achpTelAccept),\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n            --:(data.amexCid),\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,data.tid);
   __sJT_st.setString(2,data.intlCredentials);
   __sJT_st.setString(3,data.intlUSD);
   __sJT_st.setString(4,data.fxClientId);
   __sJT_st.setString(5,data.fxProductType);
   __sJT_st.setString(6,data.bmlAccept);
   __sJT_st.setString(7,data.bmlPromoCode1);
   __sJT_st.setString(8,data.bmlMerchId1);
   __sJT_st.setString(9,data.bmlPromoCode2);
   __sJT_st.setString(10,data.bmlMerchId2);
   __sJT_st.setString(11,data.bmlPromoCode3);
   __sJT_st.setString(12,data.bmlMerchId3);
   __sJT_st.setString(13,data.bmlPromoCode4);
   __sJT_st.setString(14,data.bmlMerchId4);
   __sJT_st.setString(15,data.bmlPromoCode5);
   __sJT_st.setString(16,data.bmlMerchId5);
   __sJT_st.setString(17,data.cardStoreLevel);
   __sJT_st.setString(18,data.fraudMode);
   __sJT_st.setString(19,data.valSettleAmt);
   __sJT_st.setString(20,data.authExpDays);
   __sJT_st.setString(21,data.multiCapture);
   __sJT_st.setString(22,data.forceOnDecline);
   __sJT_st.setString(23,data.batchProcessing);
   __sJT_st.setString(24,data.batchProcessingEmail);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1941^9*/
      }

      return true;
    }
    catch (Exception e)
    {
      logEntry("updateApiExtensionData(" + data + ")",e.toString());
      errorString = "updateApiExtensionData(" + data + "): " + e.toString();
      log.error(errorString);
      throw e;
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void updateMerchPayOption(int appSeqNum, int merchantNumber, String caid, int cardType)
  {
    int recCount = 0;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1964^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchpayoption
//          where   app_seq_num = :appSeqNum and
//                  cardtype_code = :cardType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchpayoption\n        where   app_seq_num =  :1  and\n                cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1971^7*/
      
      // remove entry from application table so
      // the CIF screen will drop/update the TID
      if ( caid == null || caid.equals("") )
      {
        // remove entry for this card type
        /*@lineinfo:generated-code*//*@lineinfo:1978^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    merchpayoption mpo
//            where   mpo.app_seq_num = :appSeqNum and
//                    mpo.cardtype_code = :cardType
//                    
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    merchpayoption mpo\n          where   mpo.app_seq_num =  :1  and\n                  mpo.cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1985^9*/
      }
      else if(recCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:1989^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchpayoption
//            set     merchpo_card_merch_number = :caid,
//                    merchpo_rate = null
//            where   app_seq_num = :appSeqNum and
//                    cardtype_code = :cardType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchpayoption\n          set     merchpo_card_merch_number =  :1 ,\n                  merchpo_rate = null\n          where   app_seq_num =  :2  and\n                  cardtype_code =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,caid);
   __sJT_st.setInt(2,appSeqNum);
   __sJT_st.setInt(3,cardType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1996^9*/
      }
      else
      {
        // get max serial number -- this is stupid
        int srNum = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:2003^9*/

//  ************************************************************
//  #sql [Ctx] { select  max(card_sr_number)+1
//            
//            from    merchpayoption
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max(card_sr_number)+1\n           \n          from    merchpayoption\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   srNum = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2009^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:2011^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchpayoption
//            (
//              app_seq_num,
//              card_sr_number,
//              cardtype_code,
//              merchpo_card_merch_number
//            )
//            values
//            (
//              :appSeqNum,
//              :srNum,
//              :cardType,
//              :caid
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchpayoption\n          (\n            app_seq_num,\n            card_sr_number,\n            cardtype_code,\n            merchpo_card_merch_number\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appSeqNum);
   __sJT_st.setInt(2,srNum);
   __sJT_st.setInt(3,cardType);
   __sJT_st.setString(4,caid);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2027^9*/
      }
      
      // update mif and mif_non_bank_cards tables
      if( caid != null && ! ("").equals(caid) )
      {
        switch(cardType)
        {
          case mesConstants.APP_CT_DISCOVER:
            /*@lineinfo:generated-code*//*@lineinfo:2036^13*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//                set     dmdsnum = :caid
//                where   merchant_number = :merchantNumber
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif\n              set     dmdsnum =  :1 \n              where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,caid);
   __sJT_st.setInt(2,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2041^13*/
          
            /*@lineinfo:generated-code*//*@lineinfo:2043^13*/

//  ************************************************************
//  #sql [Ctx] { update  mif_non_bank_cards
//                set     discover = :caid
//                where   merchant_number = :merchantNumber
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif_non_bank_cards\n              set     discover =  :1 \n              where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,caid);
   __sJT_st.setInt(2,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2048^13*/
            break;
          
          case mesConstants.APP_CT_AMEX:
            /*@lineinfo:generated-code*//*@lineinfo:2052^13*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//                set     damexse = :caid
//                where   merchant_number = :merchantNumber
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif\n              set     damexse =  :1 \n              where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,caid);
   __sJT_st.setInt(2,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2057^13*/
          
            /*@lineinfo:generated-code*//*@lineinfo:2059^13*/

//  ************************************************************
//  #sql [Ctx] { update  mif_non_bank_cards
//                set     amex = :caid
//                where   merchant_number = :merchantNumber
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif_non_bank_cards\n              set     amex =  :1 \n              where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,caid);
   __sJT_st.setInt(2,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2064^13*/
            break;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("updateMerchPayOption(" + appSeqNum + ", " + caid + ", " + cardType + ")", e.toString());
    }
  }
  
  private boolean updateNonBankCards()
  {
    boolean   result    = false;
    int       appSeqNum = 0;
    
    String    discCaid  = getData("discCaid");
    String    amexCaid  = getData("amexCaid");
    String    jcbCaid   = getData("jcbCaid");
    
    try
    {
      // update mif_non_bank_cards
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:2088^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//          
//          from    mif_non_bank_cards
//          where   merchant_number = :getData("merchantId")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_994 = getData("merchantId");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n         \n        from    mif_non_bank_cards\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_994);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2094^7*/
    
      if(recCount > 0)
      {
        // update table
        /*@lineinfo:generated-code*//*@lineinfo:2099^9*/

//  ************************************************************
//  #sql [Ctx] { update  mif_non_bank_cards
//            set     discover  = :discCaid,
//                    amex      = :amexCaid,
//                    jcb       = :jcbCaid
//            where   merchant_number = :getData("merchantId")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_995 = getData("merchantId");
   String theSqlTS = "update  mif_non_bank_cards\n          set     discover  =  :1 ,\n                  amex      =  :2 ,\n                  jcb       =  :3 \n          where   merchant_number =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,discCaid);
   __sJT_st.setString(2,amexCaid);
   __sJT_st.setString(3,jcbCaid);
   __sJT_st.setString(4,__sJT_995);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2106^9*/
      }
      else
      {
        // insert new record
        /*@lineinfo:generated-code*//*@lineinfo:2111^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mif_non_bank_cards
//            (
//              merchant_number,
//              discover,
//              amex,
//              jcb
//            )
//            values
//            (
//              :getData("merchantId"),
//              :discCaid,
//              :amexCaid,
//              :jcbCaid
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_996 = getData("merchantId");
   String theSqlTS = "insert into mif_non_bank_cards\n          (\n            merchant_number,\n            discover,\n            amex,\n            jcb\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"29com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_996);
   __sJT_st.setString(2,discCaid);
   __sJT_st.setString(3,amexCaid);
   __sJT_st.setString(4,jcbCaid);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2127^9*/
      }
      
      // update merchpayoption
      /*@lineinfo:generated-code*//*@lineinfo:2131^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant
//          where   merch_number = :getData("merchantId")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_997 = getData("merchantId");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant\n        where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_997);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2137^7*/
      
      if(recCount > 0)
      {
        // get app seq num
        /*@lineinfo:generated-code*//*@lineinfo:2142^9*/

//  ************************************************************
//  #sql [Ctx] { select  app_seq_num
//            
//            from    merchant
//            where   merch_number = :getData("merchantId")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_998 = getData("merchantId");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_seq_num\n           \n          from    merchant\n          where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_998);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2148^9*/
        
        // update the merch pay option table entries
        updateMerchPayOption(appSeqNum, getField("merchantId").asInteger(), discCaid, mesConstants.APP_CT_DISCOVER);
        updateMerchPayOption(appSeqNum, getField("merchantId").asInteger(), amexCaid, mesConstants.APP_CT_AMEX);
        updateMerchPayOption(appSeqNum, getField("merchantId").asInteger(), jcbCaid, mesConstants.APP_CT_JCB);
        
        // update the app_status_summary table (used by app status)
        /*@lineinfo:generated-code*//*@lineinfo:2156^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_status_summary
//            set     discover_status = :discCaid,
//                    disc_merch_num  = :discCaid,
//                    amex_status     = :amexCaid,
//                    amex_merch_num  = :amexCaid
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_status_summary\n          set     discover_status =  :1 ,\n                  disc_merch_num  =  :2 ,\n                  amex_status     =  :3 ,\n                  amex_merch_num  =  :4 \n          where   app_seq_num =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"32com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,discCaid);
   __sJT_st.setString(2,discCaid);
   __sJT_st.setString(3,amexCaid);
   __sJT_st.setString(4,amexCaid);
   __sJT_st.setInt(5,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2164^9*/
      }
      
      // update mif
      if( discCaid != null && ! ("").equals(discCaid) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:2170^9*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//            set     dmdsnum = :discCaid
//            where   merchant_number = :getData("merchantId")                
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_999 = getData("merchantId");
   String theSqlTS = "update  mif\n          set     dmdsnum =  :1 \n          where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"33com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,discCaid);
   __sJT_st.setString(2,__sJT_999);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2175^9*/
      }
      
      
      if( amexCaid != null && ! ("").equals(amexCaid) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:2181^9*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//            set     dmdsnum = :discCaid,
//                    damexse = :amexCaid
//            where   merchant_number = :getData("merchantId")                
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1000 = getData("merchantId");
   String theSqlTS = "update  mif\n          set     dmdsnum =  :1 ,\n                  damexse =  :2 \n          where   merchant_number =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"34com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,discCaid);
   __sJT_st.setString(2,amexCaid);
   __sJT_st.setString(3,__sJT_1000);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2187^9*/
      }
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("updateNonBankCards()", e.toString());
      result = false;
      errorString = "updateNonBankCards(): " + e.toString();
    }
    
    return result;
  }
  
  private boolean doUpdate()
  {
    boolean result = false;
    
    try
    {
      // update api extension data before the main profile record
      ApiExtensionData apiData  = new ApiExtensionData();
      apiData.tid               = getData("tid");
      apiData.intlCredentials   = buildIntlCredentials();
      apiData.intlUSD           = getData("intlUSD");
      apiData.fxClientId        = getData("fxClientId");
      apiData.fxProductType     = getData("fxProductType");
      apiData.achpCcdAccept     = getData("achpCcdAccept");
      apiData.achpPpdAccept     = getData("achpPpdAccept");
      apiData.achpWebAccept     = getData("achpWebAccept");
      apiData.achpTelAccept     = getData("achpTelAccept");
      apiData.bmlAccept         = getData("bmlAccept");
      apiData.bmlPromoCode1     = getData("bmlPromoCode1");
      apiData.bmlMerchId1       = getData("bmlMerchId1");
      apiData.bmlPromoCode2     = getData("bmlPromoCode2");
      apiData.bmlMerchId2       = getData("bmlMerchId2");
      apiData.bmlPromoCode3     = getData("bmlPromoCode3");
      apiData.bmlMerchId3       = getData("bmlMerchId3");
      apiData.bmlPromoCode4     = getData("bmlPromoCode4");
      apiData.bmlMerchId4       = getData("bmlMerchId4");
      apiData.bmlPromoCode5     = getData("bmlPromoCode5");
      apiData.bmlMerchId5       = getData("bmlMerchId5");
      //apiData.amexCid           = getData("amexCid");
      apiData.cardStoreLevel    = getData("cardStoreLevel");
      apiData.fraudMode         = getData("fraudMode");
      apiData.valSettleAmt      = getData("valSettleAmt");
      apiData.authExpDays       = getData("authExpDays");
      apiData.multiCapture      = getData("multiCapture");
      apiData.forceOnDecline    = getData("forceOnDecline");
      apiData.batchProcessing   = getData("batchProcessing");
      apiData.batchProcessingEmail  = getData("batchProcessingEmail");
      updateApiExtensionData(apiData);

      /*@lineinfo:generated-code*//*@lineinfo:2241^7*/

//  ************************************************************
//  #sql [Ctx] { update trident_profile
//          set addr_city                 = :getData("locCSZCity"),
//              addr_line_1               = :getData("locAddr1"),  
//              addr_state                = :getData("locCSZState"), 
//              addr_zip                  = :getData("locCSZZip"),
//              vmc_accept                = :getData("vmcAccept").toUpperCase(),  
//              vmc_caid                  = :getData("vmcCaid"),
//              debit_accept              = :getData("debitAccept").toUpperCase(),  
//              amex_accept               = :getData("amexAccept").toUpperCase(),  
//              amex_caid                 = :getData("amexCaid"),
//              merchant_name             = :getData("merchantName"),
//              disc_accept               = :getData("discAccept").toUpperCase(),  
//              disc_caid                 = :getData("discCaid"),
//              jcb_accept                = :getData("jcbAccept").toUpperCase(), 
//              jcb_caid                  = :getData("jcbCaid"),
//              last_modified_date        = sysdate,
//              last_modified_user        = :user.getLoginName(),
//              product_code              = :getData("productCode"),   
//              profile_status            = :getData("profileStatus"),     
//              risk_status               = :getData("riskStatus"),  
//              sic_code                  = :getData("sicCode"),
//              time_zone                 = :getData("timeZone"),
//              phone_number              = :getData("locPhone"),
//              dm_contact_info           = :getData("dmContactInfo"),
//              amex_pcid                 = :getData("amexPcid"),
//              amex_charge_desc          = :getData("amexChargeDesc"),
//              merchant_key_required     = upper(:getData("merchantKeyRequired")),
//              secondary_key_enabled     = upper(:getData("secondaryKeyEnabled")),
//              api_enabled               = upper(:getData("apiEnabled")),
//              api_credits_allowed       = upper(:getData("apiCreditsAllowed")),
//              bin_number                = :getData("binNumber"),
//              dynamic_dba_allowed       = upper(:getData("dynamicDbaAllowed")),
//              dynamic_dba_name_prefix   = upper(:getData("dynamicDbaPrefix")),
//              use_discover_aid          = upper(:getData("useDiscoverAid")),
//              intl_processor            = :getData("intlProcessor")
//          where terminal_id             = :getData("tid")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1001 = getData("locCSZCity");
 String __sJT_1002 = getData("locAddr1");
 String __sJT_1003 = getData("locCSZState");
 String __sJT_1004 = getData("locCSZZip");
 String __sJT_1005 = getData("vmcAccept").toUpperCase();
 String __sJT_1006 = getData("vmcCaid");
 String __sJT_1007 = getData("debitAccept").toUpperCase();
 String __sJT_1008 = getData("amexAccept").toUpperCase();
 String __sJT_1009 = getData("amexCaid");
 String __sJT_1010 = getData("merchantName");
 String __sJT_1011 = getData("discAccept").toUpperCase();
 String __sJT_1012 = getData("discCaid");
 String __sJT_1013 = getData("jcbAccept").toUpperCase();
 String __sJT_1014 = getData("jcbCaid");
 String __sJT_1015 = user.getLoginName();
 String __sJT_1016 = getData("productCode");
 String __sJT_1017 = getData("profileStatus");
 String __sJT_1018 = getData("riskStatus");
 String __sJT_1019 = getData("sicCode");
 String __sJT_1020 = getData("timeZone");
 String __sJT_1021 = getData("locPhone");
 String __sJT_1022 = getData("dmContactInfo");
 String __sJT_1023 = getData("amexPcid");
 String __sJT_1024 = getData("amexChargeDesc");
 String __sJT_1025 = getData("merchantKeyRequired");
 String __sJT_1026 = getData("secondaryKeyEnabled");
 String __sJT_1027 = getData("apiEnabled");
 String __sJT_1028 = getData("apiCreditsAllowed");
 String __sJT_1029 = getData("binNumber");
 String __sJT_1030 = getData("dynamicDbaAllowed");
 String __sJT_1031 = getData("dynamicDbaPrefix");
 String __sJT_1032 = getData("useDiscoverAid");
 String __sJT_1033 = getData("intlProcessor");
 String __sJT_1034 = getData("tid");
   String theSqlTS = "update trident_profile\n        set addr_city                 =  :1 ,\n            addr_line_1               =  :2 ,  \n            addr_state                =  :3 , \n            addr_zip                  =  :4 ,\n            vmc_accept                =  :5 ,  \n            vmc_caid                  =  :6 ,\n            debit_accept              =  :7 ,  \n            amex_accept               =  :8 ,  \n            amex_caid                 =  :9 ,\n            merchant_name             =  :10 ,\n            disc_accept               =  :11 ,  \n            disc_caid                 =  :12 ,\n            jcb_accept                =  :13 , \n            jcb_caid                  =  :14 ,\n            last_modified_date        = sysdate,\n            last_modified_user        =  :15 ,\n            product_code              =  :16 ,   \n            profile_status            =  :17 ,     \n            risk_status               =  :18 ,  \n            sic_code                  =  :19 ,\n            time_zone                 =  :20 ,\n            phone_number              =  :21 ,\n            dm_contact_info           =  :22 ,\n            amex_pcid                 =  :23 ,\n            amex_charge_desc          =  :24 ,\n            merchant_key_required     = upper( :25 ),\n            secondary_key_enabled     = upper( :26 ),\n            api_enabled               = upper( :27 ),\n            api_credits_allowed       = upper( :28 ),\n            bin_number                =  :29 ,\n            dynamic_dba_allowed       = upper( :30 ),\n            dynamic_dba_name_prefix   = upper( :31 ),\n            use_discover_aid          = upper( :32 ),\n            intl_processor            =  :33 \n        where terminal_id             =  :34";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"35com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1001);
   __sJT_st.setString(2,__sJT_1002);
   __sJT_st.setString(3,__sJT_1003);
   __sJT_st.setString(4,__sJT_1004);
   __sJT_st.setString(5,__sJT_1005);
   __sJT_st.setString(6,__sJT_1006);
   __sJT_st.setString(7,__sJT_1007);
   __sJT_st.setString(8,__sJT_1008);
   __sJT_st.setString(9,__sJT_1009);
   __sJT_st.setString(10,__sJT_1010);
   __sJT_st.setString(11,__sJT_1011);
   __sJT_st.setString(12,__sJT_1012);
   __sJT_st.setString(13,__sJT_1013);
   __sJT_st.setString(14,__sJT_1014);
   __sJT_st.setString(15,__sJT_1015);
   __sJT_st.setString(16,__sJT_1016);
   __sJT_st.setString(17,__sJT_1017);
   __sJT_st.setString(18,__sJT_1018);
   __sJT_st.setString(19,__sJT_1019);
   __sJT_st.setString(20,__sJT_1020);
   __sJT_st.setString(21,__sJT_1021);
   __sJT_st.setString(22,__sJT_1022);
   __sJT_st.setString(23,__sJT_1023);
   __sJT_st.setString(24,__sJT_1024);
   __sJT_st.setString(25,__sJT_1025);
   __sJT_st.setString(26,__sJT_1026);
   __sJT_st.setString(27,__sJT_1027);
   __sJT_st.setString(28,__sJT_1028);
   __sJT_st.setString(29,__sJT_1029);
   __sJT_st.setString(30,__sJT_1030);
   __sJT_st.setString(31,__sJT_1031);
   __sJT_st.setString(32,__sJT_1032);
   __sJT_st.setString(33,__sJT_1033);
   __sJT_st.setString(34,__sJT_1034);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2279^7*/

      result = updateNonBankCards();
    }
    catch(Exception e)
    {
      logEntry("doUpdate()", e.toString());
      log.error("doUpdate(): " + e.toString());
      result = false;
      errorString = "doUpdate(): " + e.toString();
    }
    
    return (result);
  }

  private boolean doUpdateAll()
  {
    boolean result = false;
    ResultSetIterator it = null;
    try
    {
      // update all related api recs payvision data
      String merchNum = getData("merchantId");
      /*@lineinfo:generated-code*//*@lineinfo:2302^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  terminal_id
//          from    trident_profile
//          where   merchant_number = :merchNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  terminal_id\n        from    trident_profile\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"36com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"36com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2307^7*/

      // update all related profiles' api extension data
      ApiExtensionData apiData  = new ApiExtensionData();
      apiData.intlCredentials   = buildIntlCredentials();
      apiData.intlUSD           = getData("intlUSD");
      apiData.fxClientId        = getData("fxClientId");
      apiData.fxProductType     = getData("fxProductType");
      apiData.achpCcdAccept     = getData("achpCcdAccept");
      apiData.achpPpdAccept     = getData("achpPpdAccept");
      apiData.achpWebAccept     = getData("achpWebAccept");
      apiData.achpTelAccept     = getData("achpTelAccept");
      apiData.bmlAccept         = getData("bmlAccept");
      apiData.bmlPromoCode1     = getData("bmlPromoCode1");
      apiData.bmlMerchId1       = getData("bmlMerchId1");
      apiData.bmlPromoCode2     = getData("bmlPromoCode2");
      apiData.bmlMerchId2       = getData("bmlMerchId2");
      apiData.bmlPromoCode3     = getData("bmlPromoCode3");
      apiData.bmlMerchId3       = getData("bmlMerchId3");
      apiData.bmlPromoCode4     = getData("bmlPromoCode4");
      apiData.bmlMerchId4       = getData("bmlMerchId4");
      apiData.bmlPromoCode5     = getData("bmlPromoCode5");
      apiData.bmlMerchId5       = getData("bmlMerchId5");
      //apiData.amexCid           = getData("amexCid");
      apiData.cardStoreLevel    = getData("cardStoreLevel");
      apiData.fraudMode         = getData("fraudMode");
      apiData.valSettleAmt      = getData("valSettleAmt");
      apiData.authExpDays       = getData("authExpDays");
      apiData.multiCapture      = getData("multiCapture");
      apiData.forceOnDecline    = getData("forceOnDecline");
      ResultSet rs              = it.getResultSet();
      while(rs.next())
      {
        apiData.tid = rs.getString("terminal_id");
        updateApiExtensionData(apiData);
      }
      rs.close();

      // update the this profile's api extension data
      apiData.tid = getData("tid");
      updateApiExtensionData(apiData);

      // update main profile record
      /*@lineinfo:generated-code*//*@lineinfo:2350^7*/

//  ************************************************************
//  #sql [Ctx] { update trident_profile
//          set addr_city                 = :getData("locCSZCity"),
//              addr_line_1               = :getData("locAddr1"),  
//              addr_state                = :getData("locCSZState"), 
//              addr_zip                  = :getData("locCSZZip"),
//              vmc_accept                = :getData("vmcAccept").toUpperCase(),  
//              vmc_caid                  = :getData("vmcCaid"),
//              debit_accept              = :getData("debitAccept").toUpperCase(),  
//              amex_accept               = :getData("amexAccept").toUpperCase(),  
//              amex_caid                 = :getData("amexCaid"),
//              merchant_name             = :getData("merchantName"),
//              disc_accept               = :getData("discAccept").toUpperCase(),  
//              disc_caid                 = :getData("discCaid"),
//              jcb_accept                = :getData("jcbAccept").toUpperCase(), 
//              jcb_caid                  = :getData("jcbCaid"),
//              last_modified_date        = sysdate,
//              last_modified_user        = :user.getLoginName(),
//              product_code              = :getData("productCode"),   
//              profile_status            = :getData("profileStatus"),     
//              risk_status               = :getData("riskStatus"),  
//              sic_code                  = :getData("sicCode"),
//              time_zone                 = :getData("timeZone"),
//              phone_number              = :getData("locPhone"),
//              dm_contact_info           = :getData("dmContactInfo"),
//              amex_pcid                 = :getData("amexPcid"),
//              amex_charge_desc          = :getData("amexChargeDesc"),
//              merchant_key_required     = upper(:getData("merchantKeyRequired")),
//              secondary_key_enabled     = upper(:getData("secondaryKeyEnabled")),
//              api_enabled               = upper(:getData("apiEnabled")),
//              api_credits_allowed       = upper(:getData("apiCreditsAllowed")),
//              bin_number                = :getData("binNumber"),
//              dynamic_dba_allowed       = upper(:getData("dynamicDbaAllowed")),
//              dynamic_dba_name_prefix   = upper(:getData("dynamicDbaPrefix")),
//              use_discover_aid          = upper(:getData("useDiscoverAid")),
//              intl_processor            = :getData("intl_processor")
//          where terminal_id       = :getData("tid")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1035 = getData("locCSZCity");
 String __sJT_1036 = getData("locAddr1");
 String __sJT_1037 = getData("locCSZState");
 String __sJT_1038 = getData("locCSZZip");
 String __sJT_1039 = getData("vmcAccept").toUpperCase();
 String __sJT_1040 = getData("vmcCaid");
 String __sJT_1041 = getData("debitAccept").toUpperCase();
 String __sJT_1042 = getData("amexAccept").toUpperCase();
 String __sJT_1043 = getData("amexCaid");
 String __sJT_1044 = getData("merchantName");
 String __sJT_1045 = getData("discAccept").toUpperCase();
 String __sJT_1046 = getData("discCaid");
 String __sJT_1047 = getData("jcbAccept").toUpperCase();
 String __sJT_1048 = getData("jcbCaid");
 String __sJT_1049 = user.getLoginName();
 String __sJT_1050 = getData("productCode");
 String __sJT_1051 = getData("profileStatus");
 String __sJT_1052 = getData("riskStatus");
 String __sJT_1053 = getData("sicCode");
 String __sJT_1054 = getData("timeZone");
 String __sJT_1055 = getData("locPhone");
 String __sJT_1056 = getData("dmContactInfo");
 String __sJT_1057 = getData("amexPcid");
 String __sJT_1058 = getData("amexChargeDesc");
 String __sJT_1059 = getData("merchantKeyRequired");
 String __sJT_1060 = getData("secondaryKeyEnabled");
 String __sJT_1061 = getData("apiEnabled");
 String __sJT_1062 = getData("apiCreditsAllowed");
 String __sJT_1063 = getData("binNumber");
 String __sJT_1064 = getData("dynamicDbaAllowed");
 String __sJT_1065 = getData("dynamicDbaPrefix");
 String __sJT_1066 = getData("useDiscoverAid");
 String __sJT_1067 = getData("intl_processor");
 String __sJT_1068 = getData("tid");
   String theSqlTS = "update trident_profile\n        set addr_city                 =  :1 ,\n            addr_line_1               =  :2 ,  \n            addr_state                =  :3 , \n            addr_zip                  =  :4 ,\n            vmc_accept                =  :5 ,  \n            vmc_caid                  =  :6 ,\n            debit_accept              =  :7 ,  \n            amex_accept               =  :8 ,  \n            amex_caid                 =  :9 ,\n            merchant_name             =  :10 ,\n            disc_accept               =  :11 ,  \n            disc_caid                 =  :12 ,\n            jcb_accept                =  :13 , \n            jcb_caid                  =  :14 ,\n            last_modified_date        = sysdate,\n            last_modified_user        =  :15 ,\n            product_code              =  :16 ,   \n            profile_status            =  :17 ,     \n            risk_status               =  :18 ,  \n            sic_code                  =  :19 ,\n            time_zone                 =  :20 ,\n            phone_number              =  :21 ,\n            dm_contact_info           =  :22 ,\n            amex_pcid                 =  :23 ,\n            amex_charge_desc          =  :24 ,\n            merchant_key_required     = upper( :25 ),\n            secondary_key_enabled     = upper( :26 ),\n            api_enabled               = upper( :27 ),\n            api_credits_allowed       = upper( :28 ),\n            bin_number                =  :29 ,\n            dynamic_dba_allowed       = upper( :30 ),\n            dynamic_dba_name_prefix   = upper( :31 ),\n            use_discover_aid          = upper( :32 ),\n            intl_processor            =  :33 \n        where terminal_id       =  :34";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"37com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1035);
   __sJT_st.setString(2,__sJT_1036);
   __sJT_st.setString(3,__sJT_1037);
   __sJT_st.setString(4,__sJT_1038);
   __sJT_st.setString(5,__sJT_1039);
   __sJT_st.setString(6,__sJT_1040);
   __sJT_st.setString(7,__sJT_1041);
   __sJT_st.setString(8,__sJT_1042);
   __sJT_st.setString(9,__sJT_1043);
   __sJT_st.setString(10,__sJT_1044);
   __sJT_st.setString(11,__sJT_1045);
   __sJT_st.setString(12,__sJT_1046);
   __sJT_st.setString(13,__sJT_1047);
   __sJT_st.setString(14,__sJT_1048);
   __sJT_st.setString(15,__sJT_1049);
   __sJT_st.setString(16,__sJT_1050);
   __sJT_st.setString(17,__sJT_1051);
   __sJT_st.setString(18,__sJT_1052);
   __sJT_st.setString(19,__sJT_1053);
   __sJT_st.setString(20,__sJT_1054);
   __sJT_st.setString(21,__sJT_1055);
   __sJT_st.setString(22,__sJT_1056);
   __sJT_st.setString(23,__sJT_1057);
   __sJT_st.setString(24,__sJT_1058);
   __sJT_st.setString(25,__sJT_1059);
   __sJT_st.setString(26,__sJT_1060);
   __sJT_st.setString(27,__sJT_1061);
   __sJT_st.setString(28,__sJT_1062);
   __sJT_st.setString(29,__sJT_1063);
   __sJT_st.setString(30,__sJT_1064);
   __sJT_st.setString(31,__sJT_1065);
   __sJT_st.setString(32,__sJT_1066);
   __sJT_st.setString(33,__sJT_1067);
   __sJT_st.setString(34,__sJT_1068);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2388^7*/
      
      // then do a limited update of all other profiles for this merchant
      //    NOT product code
      //    NOT Merchant Key checkbox
      //    NOT TPG Enabled checkbox
      //    NOT Dynamic DBA checkbox/prefix
      //    NOT International Processor
      /*@lineinfo:generated-code*//*@lineinfo:2396^7*/

//  ************************************************************
//  #sql [Ctx] { update trident_profile
//          set addr_city           = :getData("locCSZCity"),
//              addr_line_1         = :getData("locAddr1"),  
//              addr_state          = :getData("locCSZState"), 
//              addr_zip            = :getData("locCSZZip"),
//              vmc_accept          = :getData("vmcAccept").toUpperCase(),  
//              vmc_caid            = :getData("vmcCaid"),
//              debit_accept        = :getData("debitAccept").toUpperCase(),  
//              amex_accept         = :getData("amexAccept").toUpperCase(),  
//              amex_caid           = :getData("amexCaid"),
//              merchant_name       = :getData("merchantName"),
//              disc_accept         = :getData("discAccept").toUpperCase(),  
//              disc_caid           = :getData("discCaid"),
//              jcb_accept          = :getData("jcbAccept").toUpperCase(), 
//              jcb_caid            = :getData("jcbCaid"),
//              last_modified_date  = sysdate,
//              last_modified_user  = :user.getLoginName(),
//              --product_code        = :(getData("productCode")),   
//              profile_status      = :getData("profileStatus"),     
//              risk_status         = :getData("riskStatus"),  
//              sic_code            = :getData("sicCode"),
//              time_zone           = :getData("timeZone"),
//              phone_number        = :getData("locPhone"),
//              dm_contact_info     = :getData("dmContactInfo"),
//              amex_pcid           = :getData("amexPcid"),
//              amex_charge_desc    = :getData("amexChargeDesc"),
//              --merchant_key_required = upper(:(getData("merchantKeyRequired"))),
//              --api_enabled         = upper(:(getData("apiEnabled"))),
//              bin_number          = :getData("binNumber")
//          where merchant_number   = :getData("merchantId")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1069 = getData("locCSZCity");
 String __sJT_1070 = getData("locAddr1");
 String __sJT_1071 = getData("locCSZState");
 String __sJT_1072 = getData("locCSZZip");
 String __sJT_1073 = getData("vmcAccept").toUpperCase();
 String __sJT_1074 = getData("vmcCaid");
 String __sJT_1075 = getData("debitAccept").toUpperCase();
 String __sJT_1076 = getData("amexAccept").toUpperCase();
 String __sJT_1077 = getData("amexCaid");
 String __sJT_1078 = getData("merchantName");
 String __sJT_1079 = getData("discAccept").toUpperCase();
 String __sJT_1080 = getData("discCaid");
 String __sJT_1081 = getData("jcbAccept").toUpperCase();
 String __sJT_1082 = getData("jcbCaid");
 String __sJT_1083 = user.getLoginName();
 String __sJT_1084 = getData("profileStatus");
 String __sJT_1085 = getData("riskStatus");
 String __sJT_1086 = getData("sicCode");
 String __sJT_1087 = getData("timeZone");
 String __sJT_1088 = getData("locPhone");
 String __sJT_1089 = getData("dmContactInfo");
 String __sJT_1090 = getData("amexPcid");
 String __sJT_1091 = getData("amexChargeDesc");
 String __sJT_1092 = getData("binNumber");
 String __sJT_1093 = getData("merchantId");
   String theSqlTS = "update trident_profile\n        set addr_city           =  :1 ,\n            addr_line_1         =  :2 ,  \n            addr_state          =  :3 , \n            addr_zip            =  :4 ,\n            vmc_accept          =  :5 ,  \n            vmc_caid            =  :6 ,\n            debit_accept        =  :7 ,  \n            amex_accept         =  :8 ,  \n            amex_caid           =  :9 ,\n            merchant_name       =  :10 ,\n            disc_accept         =  :11 ,  \n            disc_caid           =  :12 ,\n            jcb_accept          =  :13 , \n            jcb_caid            =  :14 ,\n            last_modified_date  = sysdate,\n            last_modified_user  =  :15 ,\n            --product_code        = :(getData(\"productCode\")),   \n            profile_status      =  :16 ,     \n            risk_status         =  :17 ,  \n            sic_code            =  :18 ,\n            time_zone           =  :19 ,\n            phone_number        =  :20 ,\n            dm_contact_info     =  :21 ,\n            amex_pcid           =  :22 ,\n            amex_charge_desc    =  :23 ,\n            --merchant_key_required = upper(:(getData(\"merchantKeyRequired\"))),\n            --api_enabled         = upper(:(getData(\"apiEnabled\"))),\n            bin_number          =  :24 \n        where merchant_number   =  :25";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"38com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1069);
   __sJT_st.setString(2,__sJT_1070);
   __sJT_st.setString(3,__sJT_1071);
   __sJT_st.setString(4,__sJT_1072);
   __sJT_st.setString(5,__sJT_1073);
   __sJT_st.setString(6,__sJT_1074);
   __sJT_st.setString(7,__sJT_1075);
   __sJT_st.setString(8,__sJT_1076);
   __sJT_st.setString(9,__sJT_1077);
   __sJT_st.setString(10,__sJT_1078);
   __sJT_st.setString(11,__sJT_1079);
   __sJT_st.setString(12,__sJT_1080);
   __sJT_st.setString(13,__sJT_1081);
   __sJT_st.setString(14,__sJT_1082);
   __sJT_st.setString(15,__sJT_1083);
   __sJT_st.setString(16,__sJT_1084);
   __sJT_st.setString(17,__sJT_1085);
   __sJT_st.setString(18,__sJT_1086);
   __sJT_st.setString(19,__sJT_1087);
   __sJT_st.setString(20,__sJT_1088);
   __sJT_st.setString(21,__sJT_1089);
   __sJT_st.setString(22,__sJT_1090);
   __sJT_st.setString(23,__sJT_1091);
   __sJT_st.setString(24,__sJT_1092);
   __sJT_st.setString(25,__sJT_1093);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2428^7*/
      
      result = updateNonBankCards();
    }
    catch(Exception e)
    {
      logEntry("doUpdateAll()", e.toString());
      log.error("doUpdateAll(): " + e.toString());
      result = false;
      errorString = "doUpdateAll(): " + e.toString();
    }
    finally
    {
      try { it.close(); } catch (Exception e) { }
    }
    return (result);
  }
  
  public Vector changeSummary = new Vector();
  public Vector changeDetails = new Vector();
  private void getChangeHistory()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    try
    {
      connect();
      
      // see if there are any records at all in the change log
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:2458^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(terminal_id)
//          
//          from    trident_profile_changes
//          where   terminal_id = :getData("tid")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1094 = getData("tid");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(terminal_id)\n         \n        from    trident_profile_changes\n        where   terminal_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_1094);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2464^7*/
      
      if(recCount > 0)
      {
        // see if there is an identity record in the change log
        int identityCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:2470^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(terminal_id)
//            
//            from    trident_profile_changes
//            where   terminal_id = :getData("tid") and
//                    last_modified_date = creation_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1095 = getData("tid");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(terminal_id)\n           \n          from    trident_profile_changes\n          where   terminal_id =  :1  and\n                  last_modified_date = creation_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"40com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_1095);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   identityCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2477^9*/
      
        if(identityCount == 0)
        {
          // get sequence id of first item
          int   profSequence = 0;
          /*@lineinfo:generated-code*//*@lineinfo:2483^11*/

//  ************************************************************
//  #sql [Ctx] { select  min(profile_sequence)
//              
//              from    trident_profile_changes
//              where   terminal_id = :getData("tid")
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1096 = getData("tid");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  min(profile_sequence)\n             \n            from    trident_profile_changes\n            where   terminal_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"41com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_1096);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   profSequence = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2489^11*/
        
          // get summary data for all but the identity element
          /*@lineinfo:generated-code*//*@lineinfo:2492^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_char(last_modified_date, 'mm/dd/yyyy hh24:mi:ss')  last_modified,
//                      last_modified_user                                    last_modified_user
//              from    trident_profile_changes
//              where   terminal_id = :getData("tid") and
//                      profile_sequence != :profSequence
//              order by last_modified_date
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1097 = getData("tid");
  try {
   String theSqlTS = "select  to_char(last_modified_date, 'mm/dd/yyyy hh24:mi:ss')  last_modified,\n                    last_modified_user                                    last_modified_user\n            from    trident_profile_changes\n            where   terminal_id =  :1  and\n                    profile_sequence !=  :2 \n            order by last_modified_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"42com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1097);
   __sJT_st.setInt(2,profSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"42com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2500^11*/
        }
        else
        {
          // get summary data (who changed the profile and when)
          /*@lineinfo:generated-code*//*@lineinfo:2505^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_char(last_modified_date, 'mm/dd/yyyy hh24:mi:ss')  last_modified,
//                      last_modified_user                                    last_modified_user
//              from    trident_profile_changes
//              where   terminal_id = :getData("tid") and
//                      last_modified_date != creation_date
//              order by last_modified_date
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1098 = getData("tid");
  try {
   String theSqlTS = "select  to_char(last_modified_date, 'mm/dd/yyyy hh24:mi:ss')  last_modified,\n                    last_modified_user                                    last_modified_user\n            from    trident_profile_changes\n            where   terminal_id =  :1  and\n                    last_modified_date != creation_date\n            order by last_modified_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"43com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1098);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"43com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2513^11*/
        }
      
        rs = it.getResultSet();
    
        while(rs.next())
        {
          changeSummary.add(new ChangeSummary(rs));
        }
    
        rs.close();
        it.close();
    
        if(changeSummary.size() > 0)
        {
          // now get details
          /*@lineinfo:generated-code*//*@lineinfo:2529^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  tpc.terminal_id, 
//                      tpc.merchant_number, 
//                      tpc.merchant_name, 
//                      tpc.sic_code, 
//                      tpc.creation_date, 
//                      tpc.profile_status, 
//                      tpc.risk_status, 
//                      tpc.vmc_accept, 
//                      tpc.vmc_caid, 
//                      tpc.amex_accept, 
//                      tpc.amex_caid, 
//                      tpc.disc_accept, 
//                      tpc.disc_caid, 
//                      tpc.diners_accept, 
//                      tpc.diners_caid, 
//                      tpc.jcb_accept, 
//                      tpc.jcb_caid, 
//                      tpc.addr_line_1, 
//                      tpc.addr_city, 
//                      tpc.addr_state, 
//                      tpc.addr_zip, 
//                      tpc.phone_number, 
//                      tpc.dm_contact_info,
//                      tpc.time_zone, 
//                      tpc.product_code, 
//                      tpc.catid, 
//                      tpc.amex_pcid, 
//                      tpc.amex_charge_desc,
//                      tpc.merchant_key_required,
//                      tpc.secondary_key_enabled,
//                      dynamic_dba_allowed,
//                      dynamic_dba_name_prefix,
//                      --tpc.use_discover_aid,
//                      tpc.use_amex_gateway,
//                      tpc.ic_intervention,
//                      tpc.api_enabled,
//                      tpc.bin_number,
//                      tpc.intl_processor,
//                      tpc.intl_credentials,
//                      tpc.intl_usd,
//                      tpc.api_credits_allowed,
//                      tpc.telepay_enabled,
//                      tpac.change_details as pg_change_details
//              from    trident_profile_changes tpc,
//                      trident_profile_api_changes tpac 
//              where   tpc.terminal_id = :getData("tid")
//                      and tpac.terminal_id(+) = tpc.terminal_id
//                      and tpac.date_changed(+) = tpc.last_modified_date
//              order by last_modified_date
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1099 = getData("tid");
  try {
   String theSqlTS = "select  tpc.terminal_id, \n                    tpc.merchant_number, \n                    tpc.merchant_name, \n                    tpc.sic_code, \n                    tpc.creation_date, \n                    tpc.profile_status, \n                    tpc.risk_status, \n                    tpc.vmc_accept, \n                    tpc.vmc_caid, \n                    tpc.amex_accept, \n                    tpc.amex_caid, \n                    tpc.disc_accept, \n                    tpc.disc_caid, \n                    tpc.diners_accept, \n                    tpc.diners_caid, \n                    tpc.jcb_accept, \n                    tpc.jcb_caid, \n                    tpc.addr_line_1, \n                    tpc.addr_city, \n                    tpc.addr_state, \n                    tpc.addr_zip, \n                    tpc.phone_number, \n                    tpc.dm_contact_info,\n                    tpc.time_zone, \n                    tpc.product_code, \n                    tpc.catid, \n                    tpc.amex_pcid, \n                    tpc.amex_charge_desc,\n                    tpc.merchant_key_required,\n                    tpc.secondary_key_enabled,\n                    dynamic_dba_allowed,\n                    dynamic_dba_name_prefix,\n                    --tpc.use_discover_aid,\n                    tpc.use_amex_gateway,\n                    tpc.ic_intervention,\n                    tpc.api_enabled,\n                    tpc.bin_number,\n                    tpc.intl_processor,\n                    tpc.intl_credentials,\n                    tpc.intl_usd,\n                    tpc.api_credits_allowed,\n                    tpc.telepay_enabled,\n                    tpac.change_details as pg_change_details\n            from    trident_profile_changes tpc,\n                    trident_profile_api_changes tpac \n            where   tpc.terminal_id =  :1 \n                    and tpac.terminal_id(+) = tpc.terminal_id\n                    and tpac.date_changed(+) = tpc.last_modified_date\n            order by last_modified_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"44com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1099);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"44com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2580^11*/
    
          rs = it.getResultSet();
    
          while(rs.next())
          {
            changeDetails.add(new ResultSetRow(rs));
          }
        
          rs.close();
          it.close();
        }
      }
    }
    catch(Exception e)
    {
      logEntry("getChangeHistory(" + getData("tid") + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public String getChangeDetails(int rowIndex)
  {
    String result = "";

    try
    {
      ResultSetRow oldRow = (ResultSetRow)(changeDetails.elementAt(rowIndex));
      ResultSetRow newRow = (ResultSetRow)(changeDetails.elementAt(rowIndex+1));
      
      if( newRow.pgSettingsChanged() )
      {
        result = newRow.pgChangeDetails();
      }
      else
      {
        result = newRow.diff(oldRow);
      }
    }
    catch(Exception e)
    {
      logEntry("getChangeDetails(" + rowIndex + ")", e.toString());
      result = e.toString();
    }
    
    return (result);
  }
  
  private String getNewTridentCatid(int catidType)
  {
    String catid = "";
    try
    {
      connect();
      
      switch(catidType)
      {
        case mesConstants.TRIDENT_CATID_TERM_MASTER:
          /*@lineinfo:generated-code*//*@lineinfo:2643^11*/

//  ************************************************************
//  #sql [Ctx] { select  ltrim(to_char(trident_catid_tm_sequence.nextval, '00000000'))
//              
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ltrim(to_char(trident_catid_tm_sequence.nextval, '00000000'))\n             \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"45com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   catid = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2648^11*/
          break;
        case mesConstants.TRIDENT_CATID_VERICENTRE:
          /*@lineinfo:generated-code*//*@lineinfo:2651^11*/

//  ************************************************************
//  #sql [Ctx] { select  ltrim(to_char(trident_catid_vc_sequence.nextval, '00000000'))
//              
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ltrim(to_char(trident_catid_vc_sequence.nextval, '00000000'))\n             \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"46com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   catid = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2656^11*/
          break;
        case mesConstants.TRIDENT_CATID_VIRTUAL_TERMINAL:
          /*@lineinfo:generated-code*//*@lineinfo:2659^11*/

//  ************************************************************
//  #sql [Ctx] { select  ltrim(to_char(trident_catid_vt_sequence.nextval, '00000000'))
//              
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ltrim(to_char(trident_catid_vt_sequence.nextval, '00000000'))\n             \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"47com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   catid = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2664^11*/
          break;
        case mesConstants.TRIDENT_CATID_NURIT:
          /*@lineinfo:generated-code*//*@lineinfo:2667^11*/

//  ************************************************************
//  #sql [Ctx] { select  ltrim(to_char(trident_catid_nr_sequence.nextval, '00000000'))
//              
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ltrim(to_char(trident_catid_nr_sequence.nextval, '00000000'))\n             \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"48com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   catid = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2672^11*/
          break;
        case mesConstants.TRIDENT_CATID_VAR:
        default:
          /*@lineinfo:generated-code*//*@lineinfo:2676^11*/

//  ************************************************************
//  #sql [Ctx] { select  ltrim(to_char(trident_catid_td_sequence.nextval, '00000000'))
//              
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ltrim(to_char(trident_catid_td_sequence.nextval, '00000000'))\n             \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"49com.mes.maintenance.TridentProfile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   catid = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2681^11*/
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("getNewTridentCatid(" + catidType + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( catid );
  }
  
  public class ChangeSummary
  {
    public String lastModified      = "";
    public String lastModifiedUser  = "";
    
    public ChangeSummary(ResultSet rs)
    {
      try
      {
        lastModified = rs.getString("last_modified");
        lastModifiedUser = rs.getString("last_modified_user");
      }
      catch(Exception e)
      {
        logEntry("ChangeSummary() constructor", e.toString());
      }
    }
  }
  
  /*************************************************************************
  **
  **   Drop Down Tables
  **
  **************************************************************************/
  protected class ProfileStatusTable extends DropDownTable
  {
    public ProfileStatusTable()
    {
      addElement("A","Active");
      addElement("S","Suspended");
      addElement("C","Closed");
    }
  }
  
  protected class PCIDTable extends DropDownTable
  {
    public PCIDTable()
    {
      ResultSetIterator   it      = null;
      ResultSet           rs      = null;
      
      try
      {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:2741^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  pcid,
//                    pcid||' - '||control_name
//            from    amex_settlement_control
//            where   nvl(enabled,'N') = 'Y' and
//                    pcid != '999999'
//            order by control_name
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pcid,\n                  pcid||' - '||control_name\n          from    amex_settlement_control\n          where   nvl(enabled,'N') = 'Y' and\n                  pcid != '999999'\n          order by control_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"50com.mes.maintenance.TridentProfile",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"50com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2749^9*/
        
        rs = it.getResultSet();

        addElement("", "select");        
        while(rs.next())
        {
          addElement(rs);
        }
        
        rs.close();
        it.close();
      }
      catch(Exception e)
      {
        logEntry("TridentProfile::PCIDTable()", e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }
  
  protected class CardStoreLevelTable extends DropDownTable
  {
    public CardStoreLevelTable()
    {
      addElement("0","Merchant");
      addElement("1","Association");
      addElement("2","Association Parent");
    }
  }
  
  protected class FraudModeTable extends DropDownTable
  {
    public FraudModeTable()
    {
      addElement(String.valueOf(TridentApiConstants.FM_NONE)    ,"None"     );
      addElement(String.valueOf(TridentApiConstants.FM_OBSERVE) ,"Observe"  );
      addElement(String.valueOf(TridentApiConstants.FM_ACTIVE)  ,"Active"   );
      addElement(String.valueOf(TridentApiConstants.FM_STRICT)  ,"Strict"   );
    }
  }

  protected class ProductCodeTable extends DropDownTable
  {
    public ProductCodeTable()
    {
      ResultSetIterator   it          = null;
      ResultSet           resultSet   = null;
      try
      {
        connect();
          
        /*@lineinfo:generated-code*//*@lineinfo:2806^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  product_code, product_desc
//            from    trident_product_codes 
//            order by display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  product_code, product_desc\n          from    trident_product_codes \n          order by display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"51com.mes.maintenance.TridentProfile",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"51com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2811^9*/
        resultSet = it.getResultSet();
        
        addElement("","select one");
        
        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      catch( Exception e )
      {
        logEntry("ProductCodeTable()",e.toString());
      }
      finally
      {
        try{ resultSet.close(); } catch(Exception e) {}
        try{ it.close(); } catch( Exception e ){}
        cleanUp();
      }
    }
  }
  
  protected class BinNumberTable extends DropDownTable
  {
    public BinNumberTable()
    {
      ResultSetIterator   it          = null;
      ResultSet           resultSet   = null;
      try
      {
        connect();
          
        /*@lineinfo:generated-code*//*@lineinfo:2846^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  bin_number, to_char(bin_number)||' ('||bank_name||')'
//            from    mms_bin_numbers
//            order by sort_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bin_number, to_char(bin_number)||' ('||bank_name||')'\n          from    mms_bin_numbers\n          order by sort_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"52com.mes.maintenance.TridentProfile",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"52com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2851^9*/
        resultSet = it.getResultSet();
        
        addElement("","select one");
        
        while( resultSet.next() )
        {
          addElement(resultSet);
        }
        resultSet.close();
        it.close();
      }
      catch( Exception e )
      {
        logEntry("ProductCodeTable()",e.toString());
      }
      finally
      {
        try{ resultSet.close(); } catch(Exception e) {}
        try{ it.close(); } catch( Exception e ){}
        cleanUp();
      }
    }
  }
  
  protected class DynamicDbaAllowedTable extends DropDownTable
  {
    public DynamicDbaAllowedTable()
    {
      addElement("N","No");
      addElement("Y","Yes");
      addElement("X","Yes (Prefix Edit Disabled)");
    }
  }
  
  protected class IntlProcessorTable extends DropDownTable
  {
    public IntlProcessorTable()
    {
      addElement("0","None");                         // INTL_PID_NONE
      addElement("1","Dynamic Currency Conversion");  // INTL_PID_DCC
      addElement("2","Reserved");                     // INTL_PID_MES
      addElement("3","PayVision");                    // INTL_PID_PAYVISION
      addElement("4","Adyen");                        // INTL_PID_ADYEN
    }
  }
  
  protected class RiskStatusTable extends DropDownTable
  {
    public RiskStatusTable()
    {
      addElement("","None");
      addElement("WB","Warning");
      addElement("RM","Actively Monitor");
    }
  }
  
  protected class PortfolioListTable extends DropDownTable
  {
    public PortfolioListTable()
    {
      ResultSetIterator it    = null;
      ResultSet         rs    = null;
      
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:2919^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  th.descendent                                     hierarchy_node,
//                    (to_char(gn.group_number)||' - '||gn.group_name)  portfolio
//            from    t_hierarchy th,
//                    group_names gn,
//                    portfolio_nodes pn
//            where   th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                    th.ancestor = :user.getHierarchyNode() and
//                    th.descendent = pn.hierarchy_node and
//                    th.descendent = gn.group_number
//            order by th.descendent        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1100 = user.getHierarchyNode();
  try {
   String theSqlTS = "select  th.descendent                                     hierarchy_node,\n                  (to_char(gn.group_number)||' - '||gn.group_name)  portfolio\n          from    t_hierarchy th,\n                  group_names gn,\n                  portfolio_nodes pn\n          where   th.hier_type =  :1  and\n                  th.ancestor =  :2  and\n                  th.descendent = pn.hierarchy_node and\n                  th.descendent = gn.group_number\n          order by th.descendent";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"53com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(2,__sJT_1100);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"53com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2931^9*/
        
        rs = it.getResultSet();
        
        addElement("", "select");
        while(rs.next())
        {
          addElement(rs);
        }
        rs.close();
        it.close();
      }
      catch(Exception e)
      {
        logEntry("PortfolioListTable constructor", e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }
  
  protected class ProfileGenTable extends DropDownTable
  {
    public ProfileGenTable()
    {
      ResultSetIterator it    = null;
      ResultSet         rs    = null;
      
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:2967^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  epg.code  code,
//                    epg.name  name
//            from    equip_profile_generators epg
//            where   is_on = 1
//            order by epg.code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  epg.code  code,\n                  epg.name  name\n          from    equip_profile_generators epg\n          where   is_on = 1\n          order by epg.code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"54com.mes.maintenance.TridentProfile",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"54com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2974^9*/
        
        addElement("", "select");
        
        rs = it.getResultSet();
        while(rs.next())
        {
          addElement(rs);
        }
        rs.close();
        it.close();
      }
      catch(Exception e)
      {
        logEntry("ProfileGenTable constructor", e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }
    
  protected class AppTypeTable extends DropDownTable
  {
    public AppTypeTable()
    {
      ResultSetIterator it    = null;
      ResultSet         rs    = null;
      
      try
      {
        connect();
      
        appTypeSet.clear();
          
        addElement("-1", "All App Types");
        
        if(user.getHierarchyNode() == 9999999999L)
        {
          // get all app types (except those specifically excluded)
          /*@lineinfo:generated-code*//*@lineinfo:3017^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct  oa.app_type  app_type,
//                      oa.app_name            description
//              from    org_app oa,
//                      app_type apt
//              where   oa.app_type = apt.app_type_code and
//                      nvl(apt.restrict_mes, 'N') = 'N'
//              order by oa.app_name asc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct  oa.app_type  app_type,\n                    oa.app_name            description\n            from    org_app oa,\n                    app_type apt\n            where   oa.app_type = apt.app_type_code and\n                    nvl(apt.restrict_mes, 'N') = 'N'\n            order by oa.app_name asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"55com.mes.maintenance.TridentProfile",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"55com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3026^11*/
        }
        else
        {
          long appNode = 0L;
          appNode = GetClosestParent.get("app_status_app_types", user.getHierarchyNode(), Ctx);
          
          /*@lineinfo:generated-code*//*@lineinfo:3033^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct  oa.app_type app_type,
//                      oa.app_name           description
//              from    org_app   oa,
//                      app_status_app_types  asat
//              where   asat.hierarchy_node = :appNode and
//                      asat.app_type = oa.app_type
//              order by oa.app_type asc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct  oa.app_type app_type,\n                    oa.app_name           description\n            from    org_app   oa,\n                    app_status_app_types  asat\n            where   asat.hierarchy_node =  :1  and\n                    asat.app_type = oa.app_type\n            order by oa.app_type asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"56com.mes.maintenance.TridentProfile",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"56com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3042^11*/
        }
        
        rs = it.getResultSet();
        while(rs.next())
        {
          appTypeSet.add(rs.getString("app_type"));
          addElement(rs);
        }
        
        rs.close();
        it.close();
      }
      catch(Exception e)
      {
        logEntry("AppTypeTable constructor", e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }

  protected class FxProductTypeTable extends DropDownTable
  {
    public FxProductTypeTable()
    {
      ResultSetIterator it    = null;
      ResultSet         rs    = null;
      
      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:3079^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  product_type,
//                    product_desc
//            from    fx_product_types
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  product_type,\n                  product_desc\n          from    fx_product_types";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"57com.mes.maintenance.TridentProfile",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"57com.mes.maintenance.TridentProfile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3084^9*/

        addElement("","select");

        rs = it.getResultSet();
        while (rs.next()) 
        {
          addElement(rs);
        }
      }
      catch (Exception e)
      {
        logEntry("FxProductTypeTable constructor", e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }
  
  /*************************************************************************
  **
  **   Validations
  **
  **************************************************************************/
  public class CardAcceptedValidation 
    implements Validation
  {
    private CheckboxField     Accepted      = null;
    private String            ErrorMessage  = null;
    
    public CardAcceptedValidation( CheckboxField accepted )
    {
      Accepted = accepted;
    }
    
    public String getErrorText()
    {
      return( ErrorMessage );
    }
    
    public boolean validate(String fdata)
    {
      ErrorMessage = null;
      
      if ( Accepted.isChecked() )
      {
        if ( (fdata == null) || fdata.equals("") )
        {
          ErrorMessage = "Card Acceptor ID required";
        }
      }
      else    // card not accepted 
      {
        if ( (fdata != null) && !fdata.equals("") )
        {
          ErrorMessage = "Card not accepted.  Acceptor ID must be blank.";
        }
      }
      return( (ErrorMessage == null) );
    }
  }

  public class BmlCoreMerchIdValidation implements Validation
  {
    private CheckboxField bmlAccepted;
    private String        errorText;

    public BmlCoreMerchIdValidation(CheckboxField bmlAccepted)
    {
      this.bmlAccepted = bmlAccepted;
    }

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String fd)
    {
      if (!bmlAccepted.isChecked())
      {
        if (fd != null && !fd.equals(""))
        {
          errorText = "BillMeLater not accepted.  Core merchant ID must be blank.";
          return false;
        }
      }
      else
      {
        if (fd == null || fd.equals(""))
        {
          errorText = "BillMeLater core merchant ID required if BillMeLater accepted.";
          return false;
        }
      }
      return true;
    }
  }

  public class BmlPromoCodeValidation implements Validation
  {
    private CheckboxField bmlAccepted;

    public BmlPromoCodeValidation(CheckboxField bmlAccepted)
    {
      this.bmlAccepted = bmlAccepted;
    }

    public String getErrorText()
    {
      return "BillMeLater not accepted.  BML promo code must be blank.";
    }

    public boolean validate(String fd)
    {
      if (!bmlAccepted.isChecked())
      {
        if (fd != null && !fd.equals(""))
        {
          return false;
        }
      }
      return true;
    }
  }

  public class BmlMerchIdValidation implements Validation
  {
    private CheckboxField bmlAccepted;
    private Field         promoCode;
    private String        errorText;

    public BmlMerchIdValidation(CheckboxField bmlAccepted, Field promoCode)
    {
      this.bmlAccepted = bmlAccepted;
      this.promoCode = promoCode;
    }

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String fd)
    {
      if (!bmlAccepted.isChecked())
      {
        if (fd != null && !fd.equals(""))
        {
          errorText 
            = "BillMeLater not accepted.  BML merchant ID must be blank.";
          return false;
        }
      }
      if (promoCode.isBlank())
      {
        if (fd != null && !fd.equals(""))
        {
          errorText = "BillMeLater promo code must be selected or merchant"
            + " ID must be blank.";
          return false;
        }
      }
      else
      {
        if (fd == null || fd.equals(""))
        {
          errorText = "BillMeLater merchant ID required if promo code is"
            + " selected.";
          return false;
        }
      }
      return true;
    }
  }
  
  public class IntlUSDValidation implements Validation
  {
    private String        ErrorMessage        = null;
    private DropDownField IntlAccepted        = null;

    public IntlUSDValidation(DropDownField intlAccepted)
    {
      IntlAccepted = intlAccepted;
    }

    public boolean validate(String fdata)
    {
      ErrorMessage = null;
      
      if ( IntlAccepted.asInt() <= 2 )
      {
        if (fdata != null && !fdata.equals("") && !fdata.equals("n"))
        {
          ErrorMessage = "International vendor required to send USD";
        }
      }
      return( ErrorMessage == null );
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }
  }
  
  public class IntlProcessorValidation implements Validation
  {
    private String        ErrorMessage            = null;
    private Field[]       IntlCredentials         = null;

    public IntlProcessorValidation( Field[] intlCredentials )
    {
      IntlCredentials = intlCredentials;
    }
    
    public boolean validate(String fdata)
    {
      int     intlProcessor   = Integer.parseInt(fdata);
      
      ErrorMessage = null;    // reset the error message
      
      switch( intlProcessor )
      {
        case mesConstants.INTL_PID_NONE:
        case mesConstants.INTL_PID_DCC:
        case mesConstants.INTL_PID_MES:
          for( int i = 0; i < IntlCredentials.length; ++i )
          {
            if ( !IntlCredentials[i].isBlank() )
            {
              ErrorMessage = "International Credentials need to be blank";
              break;
            }
          }
          break;
          
        case mesConstants.INTL_PID_PAYVISION:
          if ( IntlCredentials[0].isBlank() || IntlCredentials[1].isBlank() )
          {
            ErrorMessage = "PayVision requires the International Merchant ID and Merchant Key";
          }
          break;
          
        case mesConstants.INTL_PID_ADYEN:
          if ( IntlCredentials[0].isBlank() || IntlCredentials[1].isBlank() || IntlCredentials[2].isBlank() )
          {
            ErrorMessage = "Adyen requires the International Merchant ID, Merchant Key, and Company ID";
          }
          else if ( !IntlCredentials[3].isBlank() && IntlCredentials[4].isBlank() )
          {
            ErrorMessage = "Adyen requires the 3DS Merchant ID when the 3DS Acquirer ID is set";
          }
          else if ( IntlCredentials[3].isBlank() && !IntlCredentials[4].isBlank() )
          {
            ErrorMessage = "Adyen requires the 3DS Acquirer ID when the 3DS Merchant ID is set";
          }
          break;
      }
      return( ErrorMessage == null );
    }

    public String getErrorText()
    {
      return( ErrorMessage );
    }
  }
  
  public class  FxAllowedValidation implements Validation
  {
    private DropDownField intlProcessor;

    public FxAllowedValidation(DropDownField intlProcessor)
    {
      this.intlProcessor = intlProcessor;
    }

    public boolean validate(String fdata)
    {
      if ( intlProcessor.asInt() != mesConstants.INTL_PID_PAYVISION )
      {
        if (fdata != null && !fdata.equals(""))
        {
          return false;
        }
      }
      return true;
    }

    public String getErrorText()
    {
      return "Payvision required for FX; enable Payvision or clear FX fields";
    }
  }

  public class AmexRequiredValidation
    implements Validation
  {
    private Field         amexAccepted;
    private String        errorText;
    
    public AmexRequiredValidation(Field _amexAccepted)
    {
      amexAccepted = _amexAccepted;
    }
    
    public String getErrorText()
    {
      return (errorText);
    }
    
    public boolean validate(String fdata)
    {
      boolean valid = true;
      
      if(amexAccepted.getData().equals("y"))
      {
        if(fdata == null || fdata.equals(""))
        {
          valid = false;
          errorText = "Required if American Express acceptance is checked";
        }
      }
      else
      {
        if(fdata != null && !fdata.equals(""))
        {
          valid = false;
          errorText = "Card not accepted.  Acceptor ID must be blank.";
        }
      }
      
      return( valid );
    }
  }
  
  public class ProductCodeProductValidation
    implements Validation
  {
    private Field   catidType;
    private String  errorText;
    
    public ProductCodeProductValidation(Field _catidType)
    {
      catidType = _catidType;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean valid = true;
      
      if(catidType.getData().equals(Integer.toString(mesConstants.TRIDENT_CATID_VIRTUAL_TERMINAL)))
      {
        if(! fdata.equals("DC"))
        {
          valid = false;
          errorText = "Virtual Terminal profile requires Direct Connection";
        }
      }
      
      return( valid );
    }
  }
  
  public class APIValidation
    implements Validation
  {
    private Field   apiEnabled;
    private String  errorText;
    
    public APIValidation(Field _apiEnabled)
    {
      apiEnabled = _apiEnabled;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String fdata)
    {
      boolean valid = true;
      
      // Merchant Key Required checkbox is never actually REQUIRED
      //if( fdata.equals("n") && apiEnabled.getData().equals("y") && ! mesConstants.TRIDENT_PC_DIRECT_CONNECT.equals(getData("productCode")) )
      //{
      //  valid = false;
      //  errorText = "Merchant Key required for this profile";
      //}
      
      return( valid );
    }
  }
  
  public class AmexCIDValidation
    implements Validation
  {
    private Field apiEnabled;
    
    public AmexCIDValidation(Field _apiEnabled)
    {
      apiEnabled = _apiEnabled;
    }
    
    public String getErrorText()
    {
      return( "Amex CID response is only available for API profiles" );
    }
    
    public boolean validate(String fdata)
    {
      boolean valid = true;
      
      if( fdata.equals("y") && apiEnabled.getData().equals("n") )
      {
        valid = false;
      }
      
      return( valid );
    }
  }
  
  public class SecondaryKeyValidation
    implements Validation
  {
    private String  ErrorText           = null;
    private Field   SecondaryKeyField   = null;
    
    public SecondaryKeyValidation(Field secondaryKey)
    {
      SecondaryKeyField = secondaryKey;
    }
    
    public String getErrorText()
    {
      return( ErrorText );
    }
    
    public boolean validate(String fdata)
    {
      ErrorText = null;   // reset the error text
      
      if ( "y".equals(fdata) && (SecondaryKeyField.isBlank() || SecondaryKeyField.getData().length() != 32) )
      {
        ErrorText = "To be enabled, the secondary key must be a valid 32-character profile key";
      }
      return( ErrorText == null );
    }
  }
  
  public class AuthExpirationDaysValidation 
    implements Validation
  {
    private String            ErrorMessage  = null;
    
    public AuthExpirationDaysValidation()
    {
    }
    
    public String getErrorText()
    {
      return( ErrorMessage );
    }
    
    public boolean validate(String fdata)
    {
      ErrorMessage = null;
      
      try
      {
        int value = Integer.parseInt(fdata);
        
        if ( value > 180 )
        {
          ErrorMessage = "Maximum number of days is 180";
        }
        else if ( value < 7 )
        {
          ErrorMessage = "Minimum number of days is 7";
        }
      }
      catch( Exception e )
      {
        ErrorMessage = fdata + " is an invalid value";
      }
      
      return( (ErrorMessage == null) );
    }
  }
  
  public class DynamicDbaPrefixValidation
    implements Validation
  {
    private String  ErrorText           = null;
    private Field   DynamicDbaAllowed   = null;
    
    public DynamicDbaPrefixValidation(Field enabled)
    {
      DynamicDbaAllowed = enabled;
    }
    
    public String getErrorText()
    {
      return( ErrorText );
    }
    
    public boolean validate(String fdata)
    {
      int       dataLen       = (fdata == null ? 0 : fdata.length());
      
      ErrorText = null;   // reset the error text
      
      if ( "Y".equals(DynamicDbaAllowed.getData()) )
      {
        if ( dataLen != 3 && dataLen != 7 && dataLen != 12 )
        {
          ErrorText = "Prefix must be 3,7 or 12 characters";
        }
        else
        {
          String  upperVal = fdata.toUpperCase();
          for ( int i = 0; i < dataLen; ++i )
          {
            if ( "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890*.".indexOf(upperVal.charAt(i)) < 0 )
            {
              ErrorText = "Invalid character found in prefix.  Must be A-Z, 0-9, or a period";
            }
          }
        }
      }
      else if ( dataLen != 0 )
      {
        ErrorText = "Dynamic DBA prefix must be blank when not enabled";
      }
      return( ErrorText == null );
    }
  }
  
  /*************************************************************************
  **
  **   Subclasses and static classes
  **
  **************************************************************************/
  public class SearchRowData
  {
    public long             AssocNumber         = 0L;
    public Timestamp        CreationDate        = null;
    public Timestamp        LastModifiedDate    = null;
    public String           LastModifiedUser    = null;
    public String           LastModifiedName    = null;
    public long             MerchantId          = 0L;
    public String           MerchantName        = null;
    public TridentTidField  Tid                 = new TridentTidField();
    public String           Catid               = null;
    public String           ProfGenCode         = null;
    
    
    public SearchRowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AssocNumber       = resultSet.getLong("assoc_number");
      CreationDate      = resultSet.getTimestamp("creation_date");
      LastModifiedDate  = resultSet.getTimestamp("last_modified_date");
      LastModifiedUser  = resultSet.getString("last_modified_user");
      LastModifiedName  = resultSet.getString("last_modified_name");
      MerchantName      = resultSet.getString("merchant_name");
      MerchantId        = resultSet.getLong("merchant_number");
      Tid.setData(resultSet.getString("tid"));
      Catid             = resultSet.getString("catid");
      ProfGenCode       = resultSet.getString("prof_gen_code");
    }
  }

  public static class FieldGroupEntry
  {
    public String       FieldName   = null;
    public String       Label       = null;
    
    public FieldGroupEntry( String fname, String label )
    {
      FieldName = fname;
      Label     = label;
    }
  }

  public class ApiExtensionData
  {
    public String tid;
    public String intlCredentials;
    public String intlUSD;
    public String fxClientId;
    public String fxProductType;
    public String achpCcdAccept;
    public String achpPpdAccept;
    public String achpWebAccept;
    public String achpTelAccept;
    public String bmlAccept;
    public String bmlPromoCode1;
    public String bmlPromoCode2;
    public String bmlPromoCode3;
    public String bmlPromoCode4;
    public String bmlPromoCode5;
    public String bmlMerchId1;
    public String bmlMerchId2;
    public String bmlMerchId3;
    public String bmlMerchId4;
    public String bmlMerchId5;
    //public String amexCid;
    public String cardStoreLevel;
    public String fraudMode;
    public String valSettleAmt;
    public String authExpDays;
    public String multiCapture;
    public String forceOnDecline;
    public String batchProcessing;
    public String batchProcessingEmail;

    public String toString()
    {
      return "ApiExtensionData [ "
        + " tid: " + tid
        + ", intlCredentials: "   + intlCredentials
        + ", intlUSD: "           + intlUSD
        + ", fxClientId: "        + fxClientId
        + ", fxProductType: "     + fxProductType
        + ", achpCcdAccept: "     + achpCcdAccept
        + ", achpPpdAccept: "     + achpPpdAccept
        + ", achpWebAccept: "     + achpWebAccept
        + ", achpTelAccept: "     + achpTelAccept
        + ", bmlAccept: "         + bmlAccept
        + ", bmlPromoCode1: "     + bmlPromoCode1
        + ", bmlMerchId1: "       + bmlMerchId1
        + ", bmlPromoCode2: "     + bmlPromoCode2
        + ", bmlMerchId2: "       + bmlMerchId2
        + ", bmlPromoCode3: "     + bmlPromoCode3
        + ", bmlMerchId3: "       + bmlMerchId3
        + ", bmlPromoCode4: "     + bmlPromoCode4
        + ", bmlMerchId4: "       + bmlMerchId4
        + ", bmlPromoCode5: "     + bmlPromoCode5
        + ", bmlMerchId5: "       + bmlMerchId5
        //+ ", amexCid: "           + amexCid
        + ", cardStoreLevel: "    + cardStoreLevel
        + ", fraudMode: "         + fraudMode
        + ", valSettleAmt: "      + valSettleAmt
        + ", authExpDays: "       + authExpDays
        + ", multiCapture: "      + multiCapture
        + ", forceOnDecline: "    + forceOnDecline
        + ", batchProcessing: "   + batchProcessing
        + ", batchProcessingEmail: " + batchProcessingEmail
        + " ]";
    }
  }
  
  public static final FieldGroupEntry[] ProfileFieldGroups =
  {
    new FieldGroupEntry( "gAcctInfo",       "Account Information" ),
    new FieldGroupEntry( "gCardsAccepted",  "Cards Accepted" ),
    new FieldGroupEntry( "gOtherPayTypes",  "Other Payment Types" ),
  };
  
  public static class CardTypeAcceptedEntry
  {
    public  boolean       AcceptDefault       = false;
    public  String        AcceptFieldName     = null;
    public  String        AcceptLabel         = null;
    public  String        CaidFieldName       = null;
    public  boolean       CaidEditable        = true;
    
    public CardTypeAcceptedEntry( String aname, String alabel, boolean adefault,
                                  String cname, boolean editable )
    {
      AcceptFieldName = aname;
      AcceptLabel     = alabel;
      AcceptDefault   = adefault;
      CaidFieldName   = cname;
      CaidEditable    = editable;
    }
  }
  
  public static final CardTypeAcceptedEntry[] CardTypes =
  {
    new CardTypeAcceptedEntry("vmcAccept",      "Visa/MC",         true,
                              "vmcCaid", false),
    new CardTypeAcceptedEntry("debitAccept",    "Debit",           false,
                              "debitCaid", false),
    new CardTypeAcceptedEntry("amexAccept",     "American Express",false,
                              "amexCaid", true),
    new CardTypeAcceptedEntry("discAccept",     "Discover",        false,
                              "discCaid", true),
    new CardTypeAcceptedEntry("jcbAccept",      "JCB",             false,
                              "jcbCaid", true)
  };
  
  public static void main( String[] args )
  {
    TridentProfile        tp    = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      if( args.length > 0 )
      {
        tp = new TridentProfile();
        tp.connect();
        tp.createFields(null);
        
        if ( args[0].equals("convert") )
        {
          BufferedReader  in      = new BufferedReader( new FileReader(args[1]) );
          String          line    = null;
          StringTokenizer tokens  = null;
  
          while( (line = in.readLine()) != null )
          {
            tokens = new StringTokenizer(line,",");
            tp.convertAccount(Long.parseLong(tokens.nextToken()),     // mid
                              tokens.nextToken(),     // vnum
                              tokens.nextToken(),     // dl id
                              Integer.parseInt(tokens.nextToken()),  // store num
                              Integer.parseInt(tokens.nextToken()),  // term num
                              tokens.nextToken());    // product code
          }
          in.close();
        }
      }
    }
    catch( Exception e )
    {
    }
    finally
    {
      try{ tp.cleanUp(); } catch( Exception ee ) {}
    }
  }
}/*@lineinfo:generated-code*/