/*@lineinfo:filename=TerminalProfileBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  Description:

    TerminalProfileBean

    Support bean for viewing and maintaining terminal profiles

  Copyright (C) 2000-2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.HttpHelper;
import sqlj.runtime.ResultSetIterator;

public class TerminalProfileBean extends SQLJConnectionBase
{
  public static final int           DT_SUMMARY              = 1;
  public static final int           DT_DETAIL               = 2;

  public Vector   profiles = null;

  protected static String[] frontEndDescriptions =
  {
    "Vital",
    "Trident"
  };

  public TerminalProfileBean()
  {
  }

  public TerminalProfileBean(String key, int dataType)
  {
    try
    {
      switch(dataType)
      {
        case DT_SUMMARY:
          loadSummaryData(key);
          break;

        case DT_DETAIL:
          loadDetailData(key);
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("TerminalProfileBean(" + key + ", " + dataType + ")", e.toString());
    }
  }

  private void loadSummaryData(String merchantNumber)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:77^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mv.vnumber                          vnumber,
//                  nvl(mv.tid, '---')                  tid,
//                  decode(mv.profile_completed_date,
//                    null, '---',
//                    to_char(mv.profile_completed_date, 'mm/dd/yyyy')) profile_completed_date,
//  --                to_char(mv.profile_completed_date,
//  --                  'mm/dd/yyyy hh24:mi:ss')          profile_completed_date,
//                  nvl(mv.profile_generator_code,
//                    :mesConstants.DEFAULT_PROFGEN)  profile_generator_code,
//                  mv.app_code                         app_code,
//                  mv.equip_model                      equip_model,
//                  mv.terminal_number                  terminal_number,
//                  mv.store_number                     store_number,
//                  mv.location_number                  location_number,
//                  mv.request_id                       request_id,
//                  msi.process_method                  process_method,
//                  msi.process_status                  process_status,
//                  msi.process_response                process_response,
//                  to_char(msi.process_start_date,
//                    'mm/dd/yyyy hh24:mi:ss')          process_start_date,
//                  to_char(msi.process_end_date,
//                    'mm/dd/yyyy hh24:mi_ss')          process_end_date,
//                  nvl(msi.term_application,'STAGE')   mms_app_code,
//                  epg.name                            profile_generator_name,
//                  tz.description                      time_zone,
//                  nvl(mv.front_end, 0)                front_end,
//                  me.equiplendtype_code               lendtype_code,
//                  m.dba_name                          dba_name,
//                  decode(mv.term_comm_type,
//                    :mesConstants.TERM_COMM_TYPE_DIAL,    'Dial',
//                    :mesConstants.TERM_COMM_TYPE_IP,      'IP',
//                    :mesConstants.TERM_COMM_TYPE_WIRELESS,'WLess',
//                    'Dial')                           term_comm_type,
//                  nvl(mp.pos_param,'--')              pos_product,
//                  mp.pos_code                         pos_code,
//                  pc.pos_desc                         pos_desc
//          from    merch_vnumber                       mv,
//                  (
//                    select  mi.process_method,
//                            mi.process_status,
//                            mi.process_response,
//                            mi.process_start_date,
//                            mi.process_end_date,
//                            mi.term_application,
//                            mi.vnum
//                    from    mms_stage_info mi,
//                            merchant mr
//                    where   mr.merch_number = :merchantNumber and
//                            mr.app_seq_num = mi.app_seq_num
//                  ) msi,
//                  equip_profile_generators            epg,
//                  time_zones                          tz,
//                  merchant                            mr,
//                  merchequipment                      me,
//                  mif                                 m,
//                  merch_pos                           mp,
//                  pos_category                        pc
//          where   mr.merch_number = :merchantNumber and
//                  mr.app_seq_num = mv.app_seq_num and
//                  mv.vnumber = msi.vnum(+) and
//                  nvl(mv.profile_generator_code,:mesConstants.DEFAULT_PROFGEN) = epg.code(+) and
//                  mv.time_zone = tz.code(+) and
//                  mv.app_seq_num = me.app_seq_num(+) and
//                  mv.equip_model = me.equip_model(+) and
//                  mr.merch_number = m.merchant_number and
//                  mr.app_seq_num = mp.app_seq_num(+) and
//                  mp.pos_code = pc.pos_code(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mv.vnumber                          vnumber,\n                nvl(mv.tid, '---')                  tid,\n                decode(mv.profile_completed_date,\n                  null, '---',\n                  to_char(mv.profile_completed_date, 'mm/dd/yyyy')) profile_completed_date,\n--                to_char(mv.profile_completed_date,\n--                  'mm/dd/yyyy hh24:mi:ss')          profile_completed_date,\n                nvl(mv.profile_generator_code,\n                   :1 )  profile_generator_code,\n                mv.app_code                         app_code,\n                mv.equip_model                      equip_model,\n                mv.terminal_number                  terminal_number,\n                mv.store_number                     store_number,\n                mv.location_number                  location_number,\n                mv.request_id                       request_id,\n                msi.process_method                  process_method,\n                msi.process_status                  process_status,\n                msi.process_response                process_response,\n                to_char(msi.process_start_date,\n                  'mm/dd/yyyy hh24:mi:ss')          process_start_date,\n                to_char(msi.process_end_date,\n                  'mm/dd/yyyy hh24:mi_ss')          process_end_date,\n                nvl(msi.term_application,'STAGE')   mms_app_code,\n                epg.name                            profile_generator_name,\n                tz.description                      time_zone,\n                nvl(mv.front_end, 0)                front_end,\n                me.equiplendtype_code               lendtype_code,\n                m.dba_name                          dba_name,\n                decode(mv.term_comm_type,\n                   :2 ,    'Dial',\n                   :3 ,      'IP',\n                   :4 ,'WLess',\n                  'Dial')                           term_comm_type,\n                nvl(mp.pos_param,'--')              pos_product,\n                mp.pos_code                         pos_code,\n                pc.pos_desc                         pos_desc\n        from    merch_vnumber                       mv,\n                (\n                  select  mi.process_method,\n                          mi.process_status,\n                          mi.process_response,\n                          mi.process_start_date,\n                          mi.process_end_date,\n                          mi.term_application,\n                          mi.vnum\n                  from    mms_stage_info mi,\n                          merchant mr\n                  where   mr.merch_number =  :5  and\n                          mr.app_seq_num = mi.app_seq_num\n                ) msi,\n                equip_profile_generators            epg,\n                time_zones                          tz,\n                merchant                            mr,\n                merchequipment                      me,\n                mif                                 m,\n                merch_pos                           mp,\n                pos_category                        pc\n        where   mr.merch_number =  :6  and\n                mr.app_seq_num = mv.app_seq_num and\n                mv.vnumber = msi.vnum(+) and\n                nvl(mv.profile_generator_code, :7 ) = epg.code(+) and\n                mv.time_zone = tz.code(+) and\n                mv.app_seq_num = me.app_seq_num(+) and\n                mv.equip_model = me.equip_model(+) and\n                mr.merch_number = m.merchant_number and\n                mr.app_seq_num = mp.app_seq_num(+) and\n                mp.pos_code = pc.pos_code(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.TerminalProfileBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.DEFAULT_PROFGEN);
   __sJT_st.setInt(2,mesConstants.TERM_COMM_TYPE_DIAL);
   __sJT_st.setInt(3,mesConstants.TERM_COMM_TYPE_IP);
   __sJT_st.setInt(4,mesConstants.TERM_COMM_TYPE_WIRELESS);
   __sJT_st.setString(5,merchantNumber);
   __sJT_st.setString(6,merchantNumber);
   __sJT_st.setInt(7,mesConstants.DEFAULT_PROFGEN);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.TerminalProfileBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:146^7*/

      rs = it.getResultSet();

      profiles = new Vector();
      while(rs.next())
      {
        profiles.add(new TerminalProfileSummary(rs));
      }

      //call this to toggle product names once all are in
      updatePOSProductName();

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadSummaryData("+ merchantNumber + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void loadDetailData(String vnumber)
  {
  }

  /**
   * specifically designed to look at vector of TerminalProfileSummary
   * and determine appropriate POS Product name... will never be 100%
   */
  private void updatePOSProductName()
  {
    int tridentCount = 0;
    int vitalCount = 0;
    TerminalProfileSummary tps;
    Iterator i;

    try
    {
      for (i = profiles.iterator(); i.hasNext();)
      {
        tps = (TerminalProfileSummary)i.next();

        //check to see if is vital or trident
        if(tps.frontEndDescription.equals(frontEndDescriptions[0]))
        {
          vitalCount++;
        }
        else if(tps.frontEndDescription.equals(frontEndDescriptions[1]))
        {
          tridentCount++;
        }

      }

      for (i = profiles.iterator(); i.hasNext();)
      {
        tps = (TerminalProfileSummary)i.next();

        //check to see if is Vital - may need to update if it is
        if(tps.frontEndDescription.equals(frontEndDescriptions[0]))
        {
          //no trident? trident exists, but not the first STAGE terminal?
          //Reset the vital to the non-"voice-auth" product name
          if(tridentCount == 0 || !tps.terminalNumber.equals("1"))
          {
            tps.updatePOSProduct();
          }

        }
      }
    }
    catch(Exception e)
    {
    }
  }

  public class TerminalProfileSummary
  {
    // data members
    public String               vnum                    = "";
    public String               tid                     = "";
    public String               termApp                 = "";
    public String               terminalNumber          = "";
    public String               storeNumber             = "";
    public String               locationNumber          = "";
    public String               timeZone                = "";
    public String               equipModel              = "";
    public int                  posCode                 = 0;
    public String               posProductTemp          = "";
    public String               posProduct              = "";
    public String               posDesc                 = "";
    public String               termAppString           = "";
    public String               commType                = "";

    public long                 requestId               = 0L;
    public String               processMethod           = "";
    public String               processStatus           = "";
    public String               processResponse         = "";
    public String               processStartDate        = "";
    public String               processEndDate          = "";
    public String               mmsTermApp              = "";
    public String               profileCompletedDate    = "";
    public int                  profGenCode             = mesConstants.PROFGEN_UNDEFINED;
    public String               profGenName             = "";
    public int                  frontEnd                = mesConstants.HOST_VITAL;
    public String               frontEndDescription     = "";
    public int                  lendtypeCode            = 0;
    public String               dbaName                 = "";
    public String               dbaNameEncoded          = "";

    public TerminalProfileSummary(ResultSet rs)
    {
      try
      {
        if(rs != null)
        vnum                    = rs.getString("vnumber");
        tid                     = rs.getString("tid");
        termApp                 = rs.getString("app_code");
        equipModel              = rs.getString("equip_model");
        terminalNumber          = rs.getString("terminal_number");
        storeNumber             = rs.getString("store_number");
        locationNumber          = rs.getString("location_number");
        timeZone                = rs.getString("time_zone");
        requestId               = rs.getString("request_id")==null? 0L:rs.getLong("request_id");
        processMethod           = rs.getString("process_method");
        processStatus           = rs.getString("process_status");
        processResponse         = rs.getString("process_response");
        processStartDate        = rs.getString("process_start_date");
        processEndDate          = rs.getString("process_end_date");
        mmsTermApp              = rs.getString("mms_app_code");
        profileCompletedDate    = rs.getString("profile_completed_date");
        profGenCode             = rs.getInt("profile_generator_code");
        profGenName             = rs.getString("profile_generator_name");
        frontEnd                = rs.getInt("front_end");
        frontEndDescription     = frontEndDescriptions[frontEnd];
        lendtypeCode            = rs.getInt("lendtype_code");
        dbaName                 = rs.getString("dba_name");
        dbaNameEncoded          = HttpHelper.urlEncode(dbaName);
        commType                = rs.getString("term_comm_type");
        posCode                 = rs.getInt("pos_code");
        posProductTemp          = rs.getString("pos_product");
        posDesc                 = rs.getString("pos_desc");

        switch(profGenCode)
        {
          case -1: // Le Ann doesn't want this complicated string any more
            termAppString = "MMS: '"+mmsTermApp+"' - "+profGenName+": '"+termApp+"'";
            break;

          case mesConstants.PROFGEN_VERICENTRE:
          case mesConstants.PROFGEN_TERMMASTER:
          case mesConstants.PROFGEN_VCTM:
          case mesConstants.PROFGEN_MMS:
          default:
            termAppString = termApp;
            break;

        }

        //we want the autogenerated Vital Stage Only to reflect Voice Auth
        //otherwise, we want to use various other product descriptions
        /*
        //Removed due to general POS confusion 4/1/08
        if(termApp.equals("STAGE") && rs.getInt("terminal_number")==1)
        {
           posProduct = "Voice Auth";
        }
        else
        {
           updatePOSProduct();
        }
        */
        updatePOSProduct();

      }
      catch(Exception e)
      {
        logEntry("TerminalProfileSummary()", e.toString());
      }
    }


/*
As provided, PRF-990...

There are two tables used for creating these rules, the equipment description and
the application name both of which can be selected in the POS ID set up section of
account set up and appear in the POS/TID section of the CIF screen.

1. If the equipment description is anything other than 'na' then the Product column should be blank.
2. The only exception is the current logic built in to populate the voice auth description.
3. The following additional logic should be applied depending on the applications selected:

A. Application name: PayFlow Link, Product Name=PayPal Payflow Link
B. Application name: PayFlow Pro, Product Name=PayPal Payflow Pro
C. Application name: VIR-TERM, Product Name=MeS Virtual Terminal
D. Application name: TRI-STAGE, Product Name=pull from OLA, if none listed leave blank, Host=Trident
E. Application name: TRI-GATEWAY, Product Name=pull from OLA, if none listed leave blank, Host=Trident
F. Application name: STAGE, Product Name=pull from OLA, if none listed leave blank, Host=Vital
  (the only exception to this is voice auth rule mentioned above).

*/
    public void updatePOSProduct()
    {
      try
      {
        //DEFAULT
        posProduct = "--";

        if(equipModel.equals("na"))
        {
          if(termAppString.equals("PAYFLOWLINK"))
          {
            posProduct = "PayPal Payflow Link";
          }
          else if(termAppString.equals("PAYFLOWPRO"))
          {
            posProduct = "PayPal Payflow Pro";
          }
          else if(termAppString.equals("VIR-TERM"))
          {
            posProduct = "MeS Virtual Terminal";
          }
          else if(termAppString.equals("TRI-GATEWAY") ||
                  termAppString.equals("TRI-STAGE")   ||
                  termAppString.equals("STAGE") )
          {
            //Vital display
            if(frontEnd == mesConstants.HOST_VITAL)
            {
              if( termAppString.equals("STAGE") &&
                  posCode == mesConstants.APP_MPOS_OTHER_VITAL_PRODUCT)
              {
                posProduct = posProductTemp;
              }
            }
            else if(frontEnd == mesConstants.HOST_TRIDENT)
            {
              if( (termAppString.equals("TRI-GATEWAY") || termAppString.equals("TRI-STAGE")) &&
                  (posCode == mesConstants.APP_MPOS_PAYMENT_GATEWAY_CNP || posCode == mesConstants.APP_MPOS_PAYMENT_GATEWAY_CP)
                )
              {
                posProduct = posProductTemp;
              }
            }
          }//end termAppString if

        }//end if na
      }
      catch(Exception e)
      {
      }
    }
  }
          /*

          HOST_VITAL                          = 0
          HOST_TRIDENT                        = 1

          //posProduct = posProductTemp;
          switch(posCode)
          {

            //change to:
            case mesConstants.APP_MPOS_VIRTUAL_TERMINAL:

              posProduct = "MeS Virtual Terminal";
              break;

            //use posProductTemp as defined
            case mesConstants.APP_MPOS_OTHER_VITAL_PRODUCT:
            case mesConstants.APP_MPOS_PAYMENT_GATEWAY_CNP:
            case mesConstants.APP_MPOS_PAYMENT_GATEWAY_CP:

              posProduct = posProductTemp;
              break;

            //reassign these to the pos_category description
            case mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK:
            case mesConstants.APP_MPOS_VERISIGN_PAYFLOW_PRO:
            case mesConstants.APP_MPOS_POS_PARTNER:
            case mesConstants.APP_MPOS_POS_PARTNER_2000:
            case mesConstants.APP_MPOS_PC_CHARGE_EXPRESS:
            case mesConstants.APP_MPOS_PC_CHARGE_PRO:
            case mesConstants.APP_MPOS_VIRTUAL_NET:
            case mesConstants.APP_MPOS_CYBERSOURCE:
            case mesConstants.APP_MPOS_AUTHORIZE_NET:

              posProduct = posDesc;
              break;

            default:

              posProduct = "--";
              break;

           */

}/*@lineinfo:generated-code*/