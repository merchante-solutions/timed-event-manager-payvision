/*@lineinfo:filename=CallTagLookup*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.maintenance;

import java.io.Serializable;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.MesHierarchy;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DateSQLJBean;
import sqlj.runtime.ResultSetIterator;

public class CallTagLookup extends DateSQLJBean
  implements Serializable
{
  private String            lookupValue         = "";
  private String            lastLookupValue     = "";

  private int               action              = mesConstants.LU_ACTION_INVALID;
  private String            actionDescription   = "Invalid Action";

  private CallTagDataComparator cdc             = new CallTagDataComparator();
  private Vector            lookupResults       = new Vector();
  private TreeSet           sortedResults       = null;

  private boolean           submitted           = false;
  private boolean           refresh             = false;

  private String            accountType         = "-1";
  private String            lastAccountType     = "-1";
  private int               residentQueue        = -1;
  private int               lastResidentQueue    = -1;

  private int               lastFromMonth       = -1;
  private int               lastFromDay         = -1;
  private int               lastFromYear        = -1;
  private int               lastToMonth         = -1;
  private int               lastToDay           = -1;
  private int               lastToYear          = -1;

  private String            stringLookup        = "";
  private long              longLookup          = -1;
  private Date              fromDate            = null;
  private Date              toDate              = null;

  public  Vector            residentQueues      = new Vector();
  public  Vector            residentQueueValues = new Vector();
  public  Vector            accountTypes        = new Vector();
  public  Vector            accountTypeValues   = new Vector();

  public void CallTagLookup()
  {
    fillDropDowns();
  }

  public synchronized void forceRefresh(boolean refresh)
  {
    this.refresh = refresh;
  }

  public synchronized boolean hasLookupChanged()
  {
    boolean result = false;

    if( refresh     ||
        (accountType != null && !accountType.equals(lastAccountType)) ||
        residentQueue  != lastResidentQueue ||
        fromMonth     != lastFromMonth    ||
        fromDay       != lastFromDay      ||
        fromYear      != lastFromYear     ||
        toMonth       != lastToMonth      ||
        toDay         != lastToDay        ||
        toYear        != lastToYear       ||
        ! lookupValue.equals(lastLookupValue)
      )
    {
      result = true;
    }

    return result;
  }

  /*
  ** METHOD fillDropDowns
  **
  */
  public synchronized void fillDropDowns()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      connect();
      
      if(residentQueues.size() == 0)
      {
        residentQueues.clear();
        residentQueueValues.clear();
        accountTypes.clear();
        accountTypeValues.clear();

        // resident queues
        residentQueues.add("");
        residentQueueValues.add("-1");
        /*@lineinfo:generated-code*//*@lineinfo:114^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  type,
//                    description
//            from    q_types
//            where   item_type = :MesQueues.Q_ITEM_TYPE_CALLTAG
//            order by type asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  type,\n                  description\n          from    q_types\n          where   item_type =  :1 \n          order by type asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.CallTagLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_ITEM_TYPE_CALLTAG);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.CallTagLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:121^9*/

        rs = it.getResultSet();
        while(rs.next())
        {
          residentQueues.add(rs.getString("description"));
          residentQueueValues.add(rs.getString("type"));
        }

        rs.close();
        it.close();

        // account types
        accountTypes.add("All Account Types");
        accountTypeValues.add("-1");
        /*@lineinfo:generated-code*//*@lineinfo:136^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct
//                    appsrctype_code type,
//                    app_name        description
//            from    org_app
//            order by app_name asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct\n                  appsrctype_code type,\n                  app_name        description\n          from    org_app\n          order by app_name asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.CallTagLookup",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.CallTagLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:143^9*/

        rs = it.getResultSet();

        while(rs.next())
        {
          accountTypes.add(rs.getString("description"));
          accountTypeValues.add(rs.getString("type"));
        }

        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("fillDropDowns()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  /*
  ** METHOD setDefaultAction
  **
  ** Sets up a default action type if one has not already been assigned
  */
  public synchronized void setDefaultAction()
  {
    if(action == mesConstants.LU_ACTION_INVALID)
    {
      setAction(mesConstants.LU_ACTION_CALLTAG);
    }
  }

  /*
  ** METHOD getCallTags
  **
  ** Fills the lookupResults vector
  */
  private void getCallTags(long userNode)
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:194^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                       control_number,
//                  qd.date_created             calltag_date,
//                  qd.source                   originator,
//                  nvl(oa.app_name, 'Unknown') type,
//                  m.dba_name                  dba_name,
//                  ct.merch_number             merchant_number,
//                  count(qn.id)                note_count,
//                  qt.short_desc               status,
//                  qd.type                     queue_type,
//                  qd.last_changed             last_changed,
//                  qd.item_type                item_type
//          from    q_data                      qd,
//                  q_notes                     qn,
//                  q_types                     qt,
//                  q_group_to_types            qtt,
//                  equip_calltag               ct,
//                  equip_calltag_notification  ctn,
//                  mif                         m,
//                  org_app                     oa,
//                  users                       u
//          where   trunc(ct.date_created) between :fromDate and :toDate and
//                  ct.ct_seq_num = qd.id and
//                  ct.merch_number = m.merchant_number and
//                  (-1 = :residentQueue or qd.type = :residentQueue) and
//                  ('-1' = :accountType or oa.appsrctype_code = :accountType) and
//                  qd.affiliate = oa.appsrctype_code(+) and
//                  qd.type = qtt.queue_type and
//                  qtt.group_type = :MesQueues.QG_CALLTAG_QUEUES and
//                  qd.type = qt.type and
//                  qd.id = qn.id(+) and
//                  qd.id = ctn.ctid(+) and
//                  qd.source = u.login_name and
//                  (
//                    ct.merch_number       = :longLookup or
//                    qd.id                 = :longLookup or
//                    upper(m.dba_name) like :stringLookup or
//                    upper(qd.source) like :stringLookup or
//                    upper(ctn.pkg_trk_num) like :stringLookup or
//                    'passall' = :stringLookup
//                  )
//                  and
//                  u.hierarchy_node in
//                  (
//                    select  descendent
//                    from    t_hierarchy
//                    where   hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                            ancestor  = :userNode
//                  )
//          group by qd.date_created,
//                   qd.id,
//                   qd.source,
//                   oa.app_name,
//                   m.dba_name,
//                   ct.merch_number,
//                   qt.short_desc,
//                   qd.type,
//                   qd.last_changed,
//                   qd.item_type
//          };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id                       control_number,\n                qd.date_created             calltag_date,\n                qd.source                   originator,\n                nvl(oa.app_name, 'Unknown') type,\n                m.dba_name                  dba_name,\n                ct.merch_number             merchant_number,\n                count(qn.id)                note_count,\n                qt.short_desc               status,\n                qd.type                     queue_type,\n                qd.last_changed             last_changed,\n                qd.item_type                item_type\n        from    q_data                      qd,\n                q_notes                     qn,\n                q_types                     qt,\n                q_group_to_types            qtt,\n                equip_calltag               ct,\n                equip_calltag_notification  ctn,\n                mif                         m,\n                org_app                     oa,\n                users                       u\n        where   trunc(ct.date_created) between  :1  and  :2  and\n                ct.ct_seq_num = qd.id and\n                ct.merch_number = m.merchant_number and\n                (-1 =  :3  or qd.type =  :4 ) and\n                ('-1' =  :5  or oa.appsrctype_code =  :6 ) and\n                qd.affiliate = oa.appsrctype_code(+) and\n                qd.type = qtt.queue_type and\n                qtt.group_type =  :7  and\n                qd.type = qt.type and\n                qd.id = qn.id(+) and\n                qd.id = ctn.ctid(+) and\n                qd.source = u.login_name and\n                (\n                  ct.merch_number       =  :8  or\n                  qd.id                 =  :9  or\n                  upper(m.dba_name) like  :10  or\n                  upper(qd.source) like  :11  or\n                  upper(ctn.pkg_trk_num) like  :12  or\n                  'passall' =  :13 \n                )\n                and\n                u.hierarchy_node in\n                (\n                  select  descendent\n                  from    t_hierarchy\n                  where   hier_type =  :14  and\n                          ancestor  =  :15 \n                )\n        group by qd.date_created,\n                 qd.id,\n                 qd.source,\n                 oa.app_name,\n                 m.dba_name,\n                 ct.merch_number,\n                 qt.short_desc,\n                 qd.type,\n                 qd.last_changed,\n                 qd.item_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.CallTagLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromDate);
   __sJT_st.setDate(2,toDate);
   __sJT_st.setInt(3,residentQueue);
   __sJT_st.setInt(4,residentQueue);
   __sJT_st.setString(5,accountType);
   __sJT_st.setString(6,accountType);
   __sJT_st.setInt(7,MesQueues.QG_CALLTAG_QUEUES);
   __sJT_st.setLong(8,longLookup);
   __sJT_st.setLong(9,longLookup);
   __sJT_st.setString(10,stringLookup);
   __sJT_st.setString(11,stringLookup);
   __sJT_st.setString(12,stringLookup);
   __sJT_st.setString(13,stringLookup);
   __sJT_st.setInt(14,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(15,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.maintenance.CallTagLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:254^8*/

      rs = it.getResultSet();

      while(rs.next())
      {
        lookupResults.add(new CallTagData( rs.getTimestamp("calltag_date"),
                                          DateTimeFormatter.getFormattedDate(rs.getTimestamp("calltag_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT),
                                          rs.getString("dba_name"),
                                          rs.getString("merchant_number"),
                                          rs.getString("control_number"),
                                          rs.getString("status"),
                                          rs.getString("type"),
                                          rs.getString("originator"),
                                          DateTimeFormatter.getFormattedDate(rs.getTimestamp("last_changed"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT),
                                          rs.getInt("note_count"),
                                          rs.getInt("queue_type"),
                                          rs.getInt("item_type")));
      }
    }
    catch(Exception e)
    {
      logEntry("getCallTags()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** METHOD public void getData()
  **
  */
  public synchronized void getData(long userNode)
  {
    try
    {
      connect();
      
      if (hasLookupChanged())
      {
        // set up longLookup value (numeric version of lookup value)
        try
        {
          longLookup = Long.parseLong(lookupValue.trim());
        }
        catch (Exception e)
        {
          longLookup = -1;
        }

        // add wildcards to stringLookup so we can match partial strings
        if(lookupValue.trim().equals(""))
        {
          stringLookup = "passall";
        }
        else
        {
          stringLookup = "%" + lookupValue.toUpperCase() + "%";
        }

        fromDate = getSqlFromDate();
        toDate   = getSqlToDate();

        // clear out any existing results
        lookupResults.clear();

        // get new statuses
        getCallTags(userNode);

        lastAccountType    = accountType;
        lastResidentQueue  = residentQueue;
        lastFromMonth     = fromMonth;
        lastFromDay       = fromDay;
        lastFromYear      = fromYear;
        lastToMonth       = toMonth;
        lastToDay         = toDay;
        lastToYear        = toYear;
        lastLookupValue   = lookupValue;
      }

      // now create the sorted tree
      if(sortedResults != null)
      {
        sortedResults.clear();
      }

      sortedResults = null;

      sortedResults = new TreeSet(cdc);
      sortedResults.addAll(lookupResults);
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public synchronized Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }

    return result;
  }

  /*
  ** METHOD getActionDescription
  **
  ** Returns the description of the current action
  */
  public synchronized String getActionDescription()
  {
    return this.actionDescription;
  }

  /*
  ** ACCESSORS
  */
  public synchronized String getLookupValue()
  {
    return lookupValue;
  }
  public synchronized void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }

  public synchronized int getAction()
  {
    return this.action;
  }
  public synchronized void setAction(int action)
  {
    this.actionDescription = "Call Tag";
  }
  public void setAction(String action)
  {
    int       actionNum;
    try
    {
      actionNum = Integer.parseInt(action);
    }
    catch(Exception e)
    {
      actionNum = mesConstants.LU_ACTION_INVALID;
    }

    setAction(actionNum);
  }

  public synchronized void setSortBy(String sortBy)
  {
    try
    {
      cdc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }

  public synchronized void setAccountType(String accountType)
  {
    this.accountType = accountType;
  }
  public synchronized String getAccountType()
  {
    return this.accountType;
  }
  public synchronized void setResidentQueue(String residentQueue)
  {
    try
    {
      this.residentQueue = Integer.parseInt(residentQueue);
    }
    catch(Exception e)
    {
    }
  }
  public synchronized String getResidentQueue()
  {
    return Integer.toString(this.residentQueue);
  }
  public synchronized void setSubmitted(String submitted)
  {
    this.submitted = true;
  }
  public synchronized boolean isSubmitted()
  {
    return this.submitted;
  }

  /*
  ** NESTED CLASSES
  */
  public class CallTagData
  {
    private Timestamp   calltagDateSorted;
    private String      calltagDate;
    private String      dbaName;
    private String      merchantNumber;
    private String      controlNumber;
    private String      status;
    private String      type;
    private String      originator;
    private String      lastChanged;
    private int         noteCount;
    private int         queueType;
    private int         itemType;

    public CallTagData()
    {
      calltagDateSorted  = null;
      calltagDate        = "";
      dbaName           = "";
      controlNumber     = "";
      status            = "";
      type              = "";
      originator        = "";
      lastChanged       = "";
      noteCount         = 0;
      queueType         = 0;
      itemType          = 0;
    }

    public CallTagData(Timestamp calltagDateSorted,
                      String    calltagDate,
                      String    dbaName,
                      String    merchantNumber,
                      String    controlNumber,
                      String    status,
                      String    type,
                      String    originator,
                      String    lastChanged,
                      int       noteCount,
                      int       queueType,
                      int       itemType)
    {
      setCallTagDateSorted(calltagDateSorted);
      setCallTagDate(calltagDate);
      setDbaName(dbaName);
      setMerchantNumber(merchantNumber);
      setControlNumber(controlNumber);
      setStatus(status);
      setType(type);
      setOriginator(originator);
      setLastChanged(lastChanged);
      setNoteCount(noteCount);
      setQueueType(queueType);
      setItemType(itemType);
    }

    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }
    
    public void setCallTagDateSorted(Timestamp calltagDateSorted)
    {
      this.calltagDateSorted = calltagDateSorted;
    }
    public String getCallTagDateSorted()
    {
      return DateTimeFormatter.getFormattedDate(calltagDateSorted, "yyyyMMddHHmmss");
    }
    public void setCallTagDate(String calltagDate)
    {
      this.calltagDate = processStringField(calltagDate);
    }
    public String getCallTagDate()
    {
      return this.calltagDate;
    }
    public void setDbaName(String dbaName)
    {
      this.dbaName = processStringField(dbaName);
    }
    public String getDbaName()
    {
      return this.dbaName;
    }
    public void setMerchantNumber(String merchantNumber)
    {
      this.merchantNumber = processStringField(merchantNumber);
    }
    public String getMerchantNumber()
    {
      return this.merchantNumber;
    }
    public void setControlNumber(String controlNumber)
    {
      this.controlNumber = processStringField(controlNumber);
    }
    public String getControlNumber()
    {
      return this.controlNumber;
    }
    public void setStatus(String status)
    {
      this.status = processStringField(status);
    }
    public String getStatus()
    {
      return this.status;
    }
    public void setType(String type)
    {
      this.type = processStringField(type);
    }
    public String getType()
    {
      return this.type;
    }
    public void setOriginator(String originator)
    {
      this.originator = processStringField(originator);
    }
    public String getOriginator()
    {
      return this.originator;
    }
    public void setLastChanged(String lastChanged)
    {
      this.lastChanged = processStringField(lastChanged);
    }
    public String getLastChanged()
    {
      return this.lastChanged;
    }
    public void setNoteCount(int noteCount)
    {
      this.noteCount = noteCount;
    }
    public String getNoteCount()
    {
      String result = "0";
      try
      {
        result = Integer.toString(this.noteCount);
      }
      catch(Exception e)
      {
      }

      return result;
    }
    public void setQueueType(int queueType)
    {
      this.queueType = queueType;
    }
    public int getQueueType()
    {
      return this.queueType;
    }
    public void setItemType(int itemType)
    {
      this.itemType = itemType;
    }
    public int getItemType()
    {
      return this.itemType;
    }
  }

  public class CallTagDataComparator
    implements Comparator, Serializable
  {
    public final static int   SB_CALLTAG_DATE     = 0;
    public final static int   SB_DBA_NAME         = 1;
    public final static int   SB_MERCHANT_NUMBER  = 2;
    public final static int   SB_CONTROL_NUMBER   = 3;
    public final static int   SB_STATUS           = 4;
    public final static int   SB_TYPE             = 5;
    public final static int   SB_ORIGINATOR       = 6;

    private int               sortBy;

    private boolean           sortAscending       = false;

    public CallTagDataComparator()
    {
      this.sortBy = SB_CALLTAG_DATE;
    }

    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }

    public void setSortBy(int sortBy)
    {
      if(sortBy >= SB_CALLTAG_DATE && sortBy <= SB_ORIGINATOR)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }

        this.sortBy = sortBy;
      }
    }

    private String getCompareString(CallTagData obj)
    {
      String result = "";

      try
      {
        switch(sortBy)
        {
          case SB_CALLTAG_DATE:
            result = obj.getCallTagDateSorted() + obj.getDbaName();
            break;

          case SB_DBA_NAME:
            result = obj.getDbaName() + obj.getCallTagDateSorted();
            break;

          case SB_MERCHANT_NUMBER:
            result = obj.getMerchantNumber() + obj.getCallTagDateSorted();
            break;

          case SB_CONTROL_NUMBER:
            result = obj.getControlNumber();
            break;

          case SB_STATUS:
            result = obj.getStatus() + obj.getCallTagDateSorted();
            break;

          case SB_TYPE:
            result = obj.getType() + obj.getCallTagDateSorted();
            break;
            
          case SB_ORIGINATOR:
            result = obj.getOriginator() + obj.getCallTagDateSorted();
            break;

          default:
            break;
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::getCompareString()", e.toString());
      }

      return result;
    }

    int compare(CallTagData o1, CallTagData o2)
    {
      int result    = 0;

      String compareString1 = getCompareString(o1);
      String compareString2 = getCompareString(o2);

      try
      {
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }

      return result;
    }

    boolean equals(CallTagData o1, CallTagData o2)
    {
      boolean result    = false;

      String compareString1 = getCompareString(o1);
      String compareString2 = getCompareString(o2);

      try
      {
        result = compareString1.equals(compareString2);
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }

      return result;
    }

    public int compare(Object o1, Object o2)
    {
      int result;

      try
      {
        result = compare((CallTagData)o1, (CallTagData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }

      return result;
    }

    public boolean equals(Object o1, Object o2)
    {
      boolean result;

      try
      {
        result = equals((CallTagData)o1, (CallTagData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }

      return result;
    }
  }
}/*@lineinfo:generated-code*/