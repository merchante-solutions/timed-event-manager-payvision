/*@lineinfo:filename=QueueMaintenance*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/QueueMaintenance.sqlj $

  Description:

    GroupRecord.

    Contains a user_groups record.  Allows insertion, modification and
    deletion of a record.

  Last Modified By   : $
  Last Modified Date : $
  Version            : $

  Change History:
     See VSS database

  Copygroup (C) 2000,2001 by Merchant e-Solutions Inc.
  All groups reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import com.mes.ops.QueueConstants;
import sqlj.runtime.ResultSetIterator;

public class QueueMaintenance extends SQLJConnectionBase
{
  public static final int     Q_CREDIT      = 1;
  public static final int     Q_APPROVE     = 2;
  public static final int     Q_PEND        = 3;
  public static final int     Q_DECLINE     = 4;
  public static final int     Q_CANCEL      = 5;

  private long      appSeqNum       = 0L;
  private int       curQueue        = 0;
  private String    curQueueDesc    = "";
  private int       newQueue        = 0;
  private String    merchantName    = "";
  private boolean   submitted       = false;
  
  private String[]  queueNames      =
  {
    "Unknown",
    "New (Credit)",
    "Approved",
    "Pending",
    "Declined",
    "Cancelled"
  };
  
  public Vector    queueVals       = new Vector();
  public Vector    queueDescs      = new Vector();
  
  public QueueMaintenance()
  {
    init();
  }
  
  private void init()
  {
    // fill drop-down vectors
    queueVals.clear();
    queueDescs.clear();
    
    for(int i=1; i<6; ++i)
    {
      if(i != Q_APPROVE)
      {
        queueVals.add(Integer.toString(i));
        queueDescs.add(queueNames[i]);
      }
    }
  }
  
  public void submitData(String userName)
  {
    Timestamp           startDate   = null;
    int                 appUser     = 0;
    String              appSource   = "";
    
    try
    {
      connect();
      
      // get the current state of the app from the app_queue_table
      /*@lineinfo:generated-code*//*@lineinfo:98^7*/

//  ************************************************************
//  #sql [Ctx] { select  app_start_date,
//                  app_user,
//                  app_source
//          
//          from    app_queue
//          where   app_seq_num = :appSeqNum and
//                  app_queue_type in
//                  (
//                    :QueueConstants.QUEUE_CREDIT,
//                    :QueueConstants.QUEUE_SETUP,
//                    :QueueConstants.QUEUE_CANCEL
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_start_date,\n                app_user,\n                app_source\n         \n        from    app_queue\n        where   app_seq_num =  :1  and\n                app_queue_type in\n                (\n                   :2 ,\n                   :3 ,\n                   :4 \n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.QueueMaintenance",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,QueueConstants.QUEUE_CREDIT);
   __sJT_st.setInt(3,QueueConstants.QUEUE_SETUP);
   __sJT_st.setInt(4,QueueConstants.QUEUE_CANCEL);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   startDate = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   appUser = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   appSource = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:114^7*/
      
      // remove all entries from the app_queue table for this application
      /*@lineinfo:generated-code*//*@lineinfo:117^7*/

//  ************************************************************
//  #sql [Ctx] { delete from app_queue
//          where       app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from app_queue\n        where       app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.maintenance.QueueMaintenance",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:121^7*/
      
      // add back the one entry we need
      int     newType         = 0;
      int     newStage        = 0;
      int     newStatus       = 0;
      String  newReason       = "";
      int     newCreditStatus = 0;
      
      switch(newQueue)
      {
        case Q_CREDIT:
        default:
          newType         = QueueConstants.QUEUE_CREDIT;
          newStage        = QueueConstants.Q_CREDIT_NEW;
          newStatus       = QueueConstants.Q_STATUS_NEW;
          newReason       = "";
          newCreditStatus = QueueConstants.CREDIT_NEW;
          break;
          
        case Q_PEND:
          newType         = QueueConstants.QUEUE_CREDIT;
          newStage        = QueueConstants.Q_CREDIT_PEND;
          newStatus       = QueueConstants.Q_STATUS_PENDING;
          newReason       = "OT";
          newCreditStatus = QueueConstants.CREDIT_PEND;
          break;
          
        case Q_DECLINE:
          newType         = QueueConstants.QUEUE_CREDIT;
          newStage        = QueueConstants.Q_CREDIT_DECLINE;
          newStatus       = QueueConstants.Q_STATUS_DECLINED;
          newReason       = "OT";
          newCreditStatus = QueueConstants.CREDIT_DECLINE;
          break;
          
        case Q_CANCEL:
          newType         = QueueConstants.QUEUE_CANCEL;
          newStage        = QueueConstants.Q_CANCEL_NEW;
          newStatus       = QueueConstants.Q_STATUS_CANCELLED;
          newReason       = "OT";
          newCreditStatus = QueueConstants.CREDIT_CANCEL;
          break;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:166^7*/

//  ************************************************************
//  #sql [Ctx] { insert into app_queue
//          (
//            app_seq_num,
//            app_queue_type,
//            app_queue_stage,
//            app_start_date,
//            app_status,
//            app_status_reason,
//            app_user,
//            app_source,
//            app_last_date,
//            app_last_user
//          )
//          values
//          (
//            :appSeqNum,
//            :newType,
//            :newStage,
//            :startDate,
//            :newStatus,
//            :newReason,
//            :appUser,
//            :appSource,
//            sysdate,
//            :userName
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_queue\n        (\n          app_seq_num,\n          app_queue_type,\n          app_queue_stage,\n          app_start_date,\n          app_status,\n          app_status_reason,\n          app_user,\n          app_source,\n          app_last_date,\n          app_last_user\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n          sysdate,\n           :9 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.maintenance.QueueMaintenance",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,newType);
   __sJT_st.setInt(3,newStage);
   __sJT_st.setTimestamp(4,startDate);
   __sJT_st.setInt(5,newStatus);
   __sJT_st.setString(6,newReason);
   __sJT_st.setInt(7,appUser);
   __sJT_st.setString(8,appSource);
   __sJT_st.setString(9,userName);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:194^7*/
      
      // update the merchant credit status
      /*@lineinfo:generated-code*//*@lineinfo:197^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//          set     merch_credit_status = :newCreditStatus
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n        set     merch_credit_status =  :1 \n        where   app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.maintenance.QueueMaintenance",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,newCreditStatus);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:202^7*/
      
      // reset internal variables so the page will display correctly
      setAppSeqNum(Long.toString(appSeqNum));
    }
    catch(Exception e)
    {
      logEntry("submitData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void setAppSeqNum(String appSeqNum)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      this.appSeqNum = Long.parseLong(appSeqNum);

      // determine the current queue for this app
      /*@lineinfo:generated-code*//*@lineinfo:229^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  aq.app_queue_type,
//                  aq.app_queue_stage,
//                  m.merch_business_name
//          from    app_queue aq,
//                  merchant m
//          where   aq.app_seq_num = :appSeqNum and
//                  m.app_seq_num = aq.app_seq_num and
//                  (
//                    aq.app_queue_type = :QueueConstants.QUEUE_CREDIT or
//                    aq.app_queue_type = :QueueConstants.QUEUE_SETUP or
//                    aq.app_queue_type = :QueueConstants.QUEUE_CANCEL
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  aq.app_queue_type,\n                aq.app_queue_stage,\n                m.merch_business_name\n        from    app_queue aq,\n                merchant m\n        where   aq.app_seq_num =  :1  and\n                m.app_seq_num = aq.app_seq_num and\n                (\n                  aq.app_queue_type =  :2  or\n                  aq.app_queue_type =  :3  or\n                  aq.app_queue_type =  :4 \n                )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.maintenance.QueueMaintenance",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,appSeqNum);
   __sJT_st.setInt(2,QueueConstants.QUEUE_CREDIT);
   __sJT_st.setInt(3,QueueConstants.QUEUE_SETUP);
   __sJT_st.setInt(4,QueueConstants.QUEUE_CANCEL);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.maintenance.QueueMaintenance",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:243^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        int qType         = rs.getInt("app_queue_type");
        int qStage        = rs.getInt("app_queue_stage");
        this.merchantName = rs.getString("merch_business_name");

        switch(qType)
        {
          case QueueConstants.QUEUE_CREDIT:
            switch(qStage)
            {
              case QueueConstants.Q_CREDIT_NEW:
                setCurQueue(Q_CREDIT);
                break;

              case QueueConstants.Q_CREDIT_DECLINE:
                setCurQueue(Q_DECLINE);
                break;

              case QueueConstants.Q_CREDIT_PEND:
                setCurQueue(Q_PEND);
                break;
            }
            break;

          case QueueConstants.QUEUE_CANCEL:
          case QueueConstants.QUEUE_SETUP:
            setCurQueue(qType);
            break;
        }
      }

      it.close();
    }
    catch(Exception e)
    {
      logEntry("setAppSeqNum(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void setCurApp(String appSeqNum)
  {
    try
    {
      this.appSeqNum = Long.parseLong(appSeqNum);
    }
    catch(Exception e)
    {
    }
  }
  
  public void setSubmitted(String submitted)
  {
    this.submitted = true;
  }
  
  public boolean isSubmitted()
  {
    return this.submitted;
  }
  
  public long getAppSeqNum()
  {
    return this.appSeqNum;
  }

  public void setNewQueue(String newQueue)
  {
    try
    {
      setNewQueue(Integer.parseInt(newQueue));
    }
    catch(Exception e)
    {
    }
  }
  public void setNewQueue(int newQueue)
  {
    this.newQueue = newQueue;
  }
  
  public void setCurQueue(String curQueue)
  {
    try
    {
      setCurQueue(Integer.parseInt(curQueue));
    }
    catch(Exception e)
    {
      logEntry("setCurQueue(" + curQueue + ")", e.toString());
    }
  }

  public void setCurQueue(int curQueue)
  {
    try
    {
      this.curQueue     = curQueue;
      this.curQueueDesc = queueNames[curQueue];
    }
    catch(Exception e)
    {
      logEntry("SetCurQueue(" + curQueue + ")", e.toString());
    }
  }
  
  public String getCurQueueDesc()
  {
    return this.curQueueDesc;
  }
  
  public String getMerchantName()
  {
    return this.merchantName;
  }
}/*@lineinfo:generated-code*/