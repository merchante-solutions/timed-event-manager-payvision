/*@lineinfo:filename=MerchantDataBeanBBT*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/MerchantDataBean.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/26/04 12:38p $
  Version            : $Revision: 46 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.BBTBusinessTypeTable;
import com.mes.forms.ButtonField;
import com.mes.forms.CurrencyPrettyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import sqlj.runtime.ResultSetIterator;

public class MerchantDataBeanBBT extends MerchantDataBean
{
  public void createFields(HttpServletRequest request)
  {
    try
    {
      super.createFields(request);
      
      // add some additional fields
      fields.add(new Field("merchAgreementTerm","Term",           16, 16, true));
      fields.add(new DropDownField("businessType","",new BBTBusinessTypeTable(),true));
      fields.add(new Field("productDesc",      "Product Desc",    80, 25, true));
      fields.add(new Field("termServiceLevel", "Service Level",   30, 25, true));
      fields.add(new CurrencyPrettyField("signedAnnualVolume","Signed Annual Volume", 12, 12, true));
      fields.add(new CurrencyPrettyField("signedAverageTicket", "Signed Average Ticekt", 12, 12, true));
      
      fields.add(new ButtonField("submitChanges", "Submit"));
      
      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      logEntry("createFields()", e.toString());
    }
  }
  
  protected void updateData()
  {
    System.out.println("Updating data from MerchantDataBeanBBT");
  }
  
  protected void getSupplementalData()
  {
    super.getSupplementalData();
    
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:75^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_agreement_term    merch_agreement_term,
//                  product_desc            product_desc,
//                  equipsply_servicetype   term_service_level,
//                  annual_volume           signed_annual_volume,
//                  average_ticket          signed_average_ticket
//          from    app_merch_bbt
//          where   app_seq_num = :getData("appSeqNum") 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_890 = getData("appSeqNum");
  try {
   String theSqlTS = "select  merch_agreement_term    merch_agreement_term,\n                product_desc            product_desc,\n                equipsply_servicetype   term_service_level,\n                annual_volume           signed_annual_volume,\n                average_ticket          signed_average_ticket\n        from    app_merch_bbt\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.MerchantDataBeanBBT",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_890);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.MerchantDataBeanBBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:84^7*/

      rs = it.getResultSet();
      
      setFields(rs);

      rs.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:93^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  babn.description    business_nature,
//                  mr.bustype_code     business_type
//          from    merchant mr,
//                  bbt_app_business_nature babn
//          where   mr.app_seq_num = :getData("appSeqNum") and
//                  mr.business_nature = babn.value(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_891 = getData("appSeqNum");
  try {
   String theSqlTS = "select  babn.description    business_nature,\n                mr.bustype_code     business_type\n        from    merchant mr,\n                bbt_app_business_nature babn\n        where   mr.app_seq_num =  :1  and\n                mr.business_nature = babn.value(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.MerchantDataBeanBBT",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_891);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.MerchantDataBeanBBT",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:101^7*/
      
      rs = it.getResultSet();
      setFields(rs);
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getBbtSupplementalData(" + getData("merchant") + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
}/*@lineinfo:generated-code*/