/*@lineinfo:filename=ChangeLookup*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/ChangeLookup.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-04-09 10:58:27 -0700 (Wed, 09 Apr 2008) $
  Version            : $Revision: 14728 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.io.Serializable;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesHierarchy;
import com.mes.constants.MesQueues;
import com.mes.constants.MesUsers;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.DateSQLJBean;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class ChangeLookup extends DateSQLJBean
  implements Serializable
{
  // create class log category
  static Logger log = Logger.getLogger(ChangeLookup.class);

  private String            lookupValue         = "";
  private String            lastLookupValue     = "";

  private int               action              = mesConstants.LU_ACTION_INVALID;
  private String            actionDescription   = "Invalid Action";

  private ChangeDataComparator cdc              = new ChangeDataComparator();
  private Vector            lookupResults       = new Vector();
  private TreeSet           sortedResults       = null;

  private boolean           submitted           = false;
  private boolean           refresh             = false;

  private String            accountType             = "-1";
  private String            lastAccountType         = "-1";
  private int               acrResidentQueue        = -1;
  private int               lastAcrResidentQueue    = -1;
  private String            acrType                 = "-1";
  private String            lastAcrType             = "-1";

  private int               lastFromMonth       = -1;
  private int               lastFromDay         = -1;
  private int               lastFromYear        = -1;
  private int               lastToMonth         = -1;
  private int               lastToDay           = -1;
  private int               lastToYear          = -1;

  private String            stringLookup        = "";
  private long              longLookup          = -1;
  private Date              fromDate            = null;
  private Date              toDate              = null;

  public  Vector            acrResidentQueues         = new Vector();
  public  Vector            acrResidentQueueValues    = new Vector();
  public  Vector            acrResidentQueueRights    = new Vector();
  public  Vector            accountTypes              = new Vector();
  public  Vector            accountTypeValues         = new Vector();
  public  Vector            acrTypes                  = new Vector();
  public  Vector            acrTypeValues             = new Vector();

  public void ChangeLookup()
  {
  }

  public synchronized void forceRefresh(boolean refresh)
  {
    this.refresh = refresh;
  }

  public synchronized boolean hasLookupChanged()
  {
    boolean result = false;

    if( refresh     ||
        (accountType != null && !accountType.equals(lastAccountType)) ||
        acrResidentQueue  != lastAcrResidentQueue ||
        (acrType != null && !acrType.equals(lastAcrType)) ||
        fromMonth     != lastFromMonth    ||
        fromDay       != lastFromDay      ||
        fromYear      != lastFromYear     ||
        toMonth       != lastToMonth      ||
        toDay         != lastToDay        ||
        toYear        != lastToYear       ||
        ! lookupValue.equals(lastLookupValue)
      )
    {
      result = true;
    }

    return result;
  }

  /*
  ** METHOD fillDropDowns
  **
  ** Fills the "Change Status" and "Change Type" drop down boxes
  */
  public synchronized void fillDropDowns(UserBean user)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      connect();

      if(acrResidentQueues.size() == 0)
      {
        acrResidentQueues.clear();
        acrResidentQueueValues.clear();
        acrResidentQueueRights.clear();

        accountTypes.clear();
        accountTypeValues.clear();

        acrTypes.clear();
        acrTypeValues.clear();

        // ACR RESIDENT QUEUES
        acrResidentQueues.add("ACR - All Queue Types");
        acrResidentQueueValues.add("-1");
        acrResidentQueueRights.add(""+MesUsers.RIGHT_ACR_SYSTEM);
        /*@lineinfo:generated-code*//*@lineinfo:160^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  type,
//                    description,
//                    required_right
//            from    q_types
//            where   item_type = :MesQueues.Q_ITEM_TYPE_CHANGE_REQUEST
//            order by type asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  type,\n                  description,\n                  required_right\n          from    q_types\n          where   item_type =  :1 \n          order by type asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.ChangeLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_ITEM_TYPE_CHANGE_REQUEST);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.ChangeLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:168^9*/

        rs = it.getResultSet();
        while(rs.next())
        {
          acrResidentQueues.add(rs.getString("description"));
          acrResidentQueueValues.add(rs.getString("type"));
          acrResidentQueueRights.add(rs.getString("required_right"));
        }

        rs.close();
        it.close();

        // ACCOUNT TYPES
        accountTypes.add("All Account Types");
        accountTypeValues.add("-1");
        /*@lineinfo:generated-code*//*@lineinfo:184^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct
//                    appsrctype_code type,
//                    app_name        description
//            from    org_app
//            order by app_name asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct\n                  appsrctype_code type,\n                  app_name        description\n          from    org_app\n          order by app_name asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.ChangeLookup",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.ChangeLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:191^9*/

        rs = it.getResultSet();

        while(rs.next())
        {
          //need to remove BBT from general MES public view
          if(!rs.getString("type").equals("BBT") ||
              user.hasRight(MesUsers.RIGHT_ACR_BBT_STATUS_VIEW))
          {
            accountTypes.add(rs.getString("description"));
            accountTypeValues.add(rs.getString("type"));
          }
        }

        rs.close();
        it.close();

        // ACR TYPES
        acrTypes.add("All ACR Types");
        acrTypeValues.add("-1");

        if(user.isMember(MesUsers.GROUP_ACCOUNT_SERVICES)
        //||
        //   user.hasRight(MesUsers.RIGHT_APPLICATION_SUPER)
        )
        {
          log.debug("building FULL ACR list");
          /*@lineinfo:generated-code*//*@lineinfo:219^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  short_name,
//                      name
//              from    acrdef
//              where   is_on=1
//              order by name asc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  short_name,\n                    name\n            from    acrdef\n            where   is_on=1\n            order by name asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.ChangeLookup",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.maintenance.ChangeLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:226^11*/
        }
        else
        {
          log.debug("building restricted ACR list");

          /*@lineinfo:generated-code*//*@lineinfo:232^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  ad.short_name,
//                      ad.name
//              from    acrdef ad, acr_acct_type aat
//              where   ad.is_on=1
//              and     ad.acrdef_seq_num = aat.def_id
//              and     aat.hierarchy_node = :user.getHierarchyNode()
//              order by name asc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_820 = user.getHierarchyNode();
  try {
   String theSqlTS = "select  ad.short_name,\n                    ad.name\n            from    acrdef ad, acr_acct_type aat\n            where   ad.is_on=1\n            and     ad.acrdef_seq_num = aat.def_id\n            and     aat.hierarchy_node =  :1 \n            order by name asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.ChangeLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_820);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.maintenance.ChangeLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:241^11*/
        }

        rs = it.getResultSet();

        while(rs.next())
        {
          acrTypes.add(rs.getString("name"));
          acrTypeValues.add(rs.getString("short_name"));
        }

        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("fillDropDowns()", e.toString());
      cleanUp();
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** METHOD setDefaultAction
  **
  ** Sets up a default action type if one has not already been assigned
  */
  public synchronized void setDefaultAction()
  {
    if(action == mesConstants.LU_ACTION_INVALID)
    {
      setAction(mesConstants.LU_ACTION_CHANGE_REQUEST);
    }
  }

  /*
  ** METHOD getOldChangeRequests
  **
  ** Fills the lookupResults vector with old change requests that match criteria
  */
  private void getOldChangeRequests(long userNode)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:293^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                   control_number,
//                  qd.date_created         change_date,
//                  qd.source               originator,
//                  oa.app_name             type,
//                  acr.merchant_name       dba_name,
//                  acr.merchant_number     merchant_number,
//                  count(qn.id)            note_count,
//                  qt.short_desc           status,
//                  qd.type                 queue_type,
//                  qd.last_changed         last_changed,
//                  qd.item_type            item_type,
//                  'N/A'                   form_type,
//                  nvl(sud.tracking_number,'')  track_num
//          from    q_data                  qd,
//                  q_notes                 qn,
//                  q_types                 qt,
//                  q_group_to_types        qtt,
//                  account_change_request  acr,
//                  org_app                 oa,
//                  users                   u,
//                  ship_upload_detail      sud
//          where   trunc(qd.date_created) between :fromDate and :toDate and
//                  (-1 = :acrResidentQueue or qd.type = :acrResidentQueue) and
//                  ('-1' = :accountType or oa.appsrctype_code = :accountType) and
//                  oa.appsrctype_code = qd.affiliate and
//                  qd.id = acr.change_sequence_id and
//                  qd.type = qtt.queue_type and
//                  qtt.group_type = :MesQueues.QG_OLD_ACR_QUEUES and
//                  qd.type = qt.type and
//                  qd.id = qn.id(+) and
//                  u.login_name = qd.source
//                  and
//                  (
//                    acr.merchant_number   = :longLookup or
//                    qd.id                 = :longLookup or
//                    upper(acr.merchant_name) like :stringLookup or
//                    upper(qd.source) like :stringLookup or
//                    'passall' = :stringLookup
//                  )
//                  and
//                  u.hierarchy_node in
//                  (
//                    select  descendent
//                    from    t_hierarchy
//                    where   hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                            ancestor  = :userNode
//                  )
//                  and
//                  acr.change_sequence_id = sud.process_id(+)
//          group by qd.date_created,
//                   qd.id,
//                   qd.source,
//                   oa.app_name,
//                   acr.merchant_name,
//                   acr.merchant_number,
//                   qt.short_desc,
//                   qd.type,
//                   qd.last_changed,
//                   qd.item_type,
//                   'N/A',
//                   sud.tracking_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id                   control_number,\n                qd.date_created         change_date,\n                qd.source               originator,\n                oa.app_name             type,\n                acr.merchant_name       dba_name,\n                acr.merchant_number     merchant_number,\n                count(qn.id)            note_count,\n                qt.short_desc           status,\n                qd.type                 queue_type,\n                qd.last_changed         last_changed,\n                qd.item_type            item_type,\n                'N/A'                   form_type,\n                nvl(sud.tracking_number,'')  track_num\n        from    q_data                  qd,\n                q_notes                 qn,\n                q_types                 qt,\n                q_group_to_types        qtt,\n                account_change_request  acr,\n                org_app                 oa,\n                users                   u,\n                ship_upload_detail      sud\n        where   trunc(qd.date_created) between  :1  and  :2  and\n                (-1 =  :3  or qd.type =  :4 ) and\n                ('-1' =  :5  or oa.appsrctype_code =  :6 ) and\n                oa.appsrctype_code = qd.affiliate and\n                qd.id = acr.change_sequence_id and\n                qd.type = qtt.queue_type and\n                qtt.group_type =  :7  and\n                qd.type = qt.type and\n                qd.id = qn.id(+) and\n                u.login_name = qd.source\n                and\n                (\n                  acr.merchant_number   =  :8  or\n                  qd.id                 =  :9  or\n                  upper(acr.merchant_name) like  :10  or\n                  upper(qd.source) like  :11  or\n                  'passall' =  :12 \n                )\n                and\n                u.hierarchy_node in\n                (\n                  select  descendent\n                  from    t_hierarchy\n                  where   hier_type =  :13  and\n                          ancestor  =  :14 \n                )\n                and\n                acr.change_sequence_id = sud.process_id(+)\n        group by qd.date_created,\n                 qd.id,\n                 qd.source,\n                 oa.app_name,\n                 acr.merchant_name,\n                 acr.merchant_number,\n                 qt.short_desc,\n                 qd.type,\n                 qd.last_changed,\n                 qd.item_type,\n                 'N/A',\n                 sud.tracking_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.maintenance.ChangeLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromDate);
   __sJT_st.setDate(2,toDate);
   __sJT_st.setInt(3,acrResidentQueue);
   __sJT_st.setInt(4,acrResidentQueue);
   __sJT_st.setString(5,accountType);
   __sJT_st.setString(6,accountType);
   __sJT_st.setInt(7,MesQueues.QG_OLD_ACR_QUEUES);
   __sJT_st.setLong(8,longLookup);
   __sJT_st.setLong(9,longLookup);
   __sJT_st.setString(10,stringLookup);
   __sJT_st.setString(11,stringLookup);
   __sJT_st.setString(12,stringLookup);
   __sJT_st.setInt(13,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(14,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.maintenance.ChangeLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:356^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        lookupResults.add(new ChangeData( rs.getTimestamp("change_date"),
                                          DateTimeFormatter.getFormattedDate(rs.getTimestamp("change_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT),
                                          rs.getString("dba_name"),
                                          rs.getString("merchant_number"),
                                          rs.getString("control_number"),
                                          rs.getString("status"),
                                          rs.getString("type"),
                                          rs.getString("originator"),
                                          DateTimeFormatter.getFormattedDate(rs.getTimestamp("last_changed"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT),
                                          rs.getInt("note_count"),
                                          rs.getInt("queue_type"),
                                          rs.getInt("item_type"),
                                          rs.getString("form_type"),
                                          rs.getString("track_num")));
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      //e.printStackTrace();
      logEntry("getOldChangeRequests()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** METHOD getNewChangeRequests
  **
  ** Fills the lookupResults vector with new change requests that match criteria
  */
  private void getNewChangeRequests(long userNode)
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:405^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  qd.id                       control_number,
//                  qd.date_created             change_date,
//                  qd.source                   originator,
//                  nvl(oa.app_name, 'Unknown') type,
//                  m.dba_name                  dba_name,
//                  acr.merch_number            merchant_number,
//                  count(qn.id)                note_count,
//                  qt.short_desc               status,
//                  qd.type                     queue_type,
//                  qd.last_changed             last_changed,
//                  qd.item_type                item_type,
//                  ad.short_display_name       form_type,
//                  nvl(sud.tracking_number,'') track_num
//          from    q_data                      qd,
//                  q_notes                     qn,
//                  q_types                     qt,
//                  q_group_to_types            qtt,
//                  acr                         acr,
//                  acrdef                      ad,
//                  mif                         m,
//                  org_app                     oa,
//                  users                       u,
//                  t_hierarchy                 th,
//                  ship_upload_detail          sud
//          where   trunc(acr.date_created) between :fromDate and :toDate and
//                  acr.acrdefid = ad.acrdef_seq_num and
//                  acr.acr_seq_num = qd.id and
//                  acr.merch_number = m.merchant_number and
//                  (-1 = :acrResidentQueue or qd.type = :acrResidentQueue) and
//                  ('-1' = :accountType or oa.appsrctype_code = :accountType) and
//                  ('-1' = :acrType or ad.short_name = :acrType) and
//                  qd.affiliate = oa.appsrctype_code(+) and
//                  qd.type = qtt.queue_type and
//                  qtt.group_type = :MesQueues.QG_NEW_ACR_QUEUES and
//                  qd.type = qt.type and
//                  qd.id = qn.id(+) and
//                  qd.source = u.login_name and
//                  (
//                    acr.merch_number      = :longLookup or
//                    qd.id                 = :longLookup or
//                    upper(m.dba_name) like :stringLookup or
//                    upper(qd.source) like :stringLookup or
//                    'passall' = :stringLookup
//                  ) and
//                  th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                  th.ancestor = :userNode and
//                  th.descendent = m.association_node
//                  and
//                  acr.acr_seq_num = sud.process_id(+)
//          group by qd.date_created,
//                   qd.id,
//                   qd.source,
//                   oa.app_name,
//                   m.dba_name,
//                   acr.merch_number,
//                   qt.short_desc,
//                   qd.type,
//                   qd.last_changed,
//                   qd.item_type,
//                   ad.short_display_name,
//                   sud.tracking_number
//          };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  qd.id                       control_number,\n                qd.date_created             change_date,\n                qd.source                   originator,\n                nvl(oa.app_name, 'Unknown') type,\n                m.dba_name                  dba_name,\n                acr.merch_number            merchant_number,\n                count(qn.id)                note_count,\n                qt.short_desc               status,\n                qd.type                     queue_type,\n                qd.last_changed             last_changed,\n                qd.item_type                item_type,\n                ad.short_display_name       form_type,\n                nvl(sud.tracking_number,'') track_num\n        from    q_data                      qd,\n                q_notes                     qn,\n                q_types                     qt,\n                q_group_to_types            qtt,\n                acr                         acr,\n                acrdef                      ad,\n                mif                         m,\n                org_app                     oa,\n                users                       u,\n                t_hierarchy                 th,\n                ship_upload_detail          sud\n        where   trunc(acr.date_created) between  :1  and  :2  and\n                acr.acrdefid = ad.acrdef_seq_num and\n                acr.acr_seq_num = qd.id and\n                acr.merch_number = m.merchant_number and\n                (-1 =  :3  or qd.type =  :4 ) and\n                ('-1' =  :5  or oa.appsrctype_code =  :6 ) and\n                ('-1' =  :7  or ad.short_name =  :8 ) and\n                qd.affiliate = oa.appsrctype_code(+) and\n                qd.type = qtt.queue_type and\n                qtt.group_type =  :9  and\n                qd.type = qt.type and\n                qd.id = qn.id(+) and\n                qd.source = u.login_name and\n                (\n                  acr.merch_number      =  :10  or\n                  qd.id                 =  :11  or\n                  upper(m.dba_name) like  :12  or\n                  upper(qd.source) like  :13  or\n                  'passall' =  :14 \n                ) and\n                th.hier_type =  :15  and\n                th.ancestor =  :16  and\n                th.descendent = m.association_node\n                and\n                acr.acr_seq_num = sud.process_id(+)\n        group by qd.date_created,\n                 qd.id,\n                 qd.source,\n                 oa.app_name,\n                 m.dba_name,\n                 acr.merch_number,\n                 qt.short_desc,\n                 qd.type,\n                 qd.last_changed,\n                 qd.item_type,\n                 ad.short_display_name,\n                 sud.tracking_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.maintenance.ChangeLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fromDate);
   __sJT_st.setDate(2,toDate);
   __sJT_st.setInt(3,acrResidentQueue);
   __sJT_st.setInt(4,acrResidentQueue);
   __sJT_st.setString(5,accountType);
   __sJT_st.setString(6,accountType);
   __sJT_st.setString(7,acrType);
   __sJT_st.setString(8,acrType);
   __sJT_st.setInt(9,MesQueues.QG_NEW_ACR_QUEUES);
   __sJT_st.setLong(10,longLookup);
   __sJT_st.setLong(11,longLookup);
   __sJT_st.setString(12,stringLookup);
   __sJT_st.setString(13,stringLookup);
   __sJT_st.setString(14,stringLookup);
   __sJT_st.setInt(15,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(16,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.maintenance.ChangeLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:468^8*/

      rs = it.getResultSet();

      while(rs.next())
      {
        lookupResults.add(new ChangeData( rs.getTimestamp("change_date"),
                                          DateTimeFormatter.getFormattedDate(rs.getTimestamp("change_date"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT),
                                          rs.getString("dba_name"),
                                          rs.getString("merchant_number"),
                                          rs.getString("control_number"),
                                          rs.getString("status"),
                                          rs.getString("type"),
                                          rs.getString("originator"),
                                          DateTimeFormatter.getFormattedDate(rs.getTimestamp("last_changed"), DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT),
                                          rs.getInt("note_count"),
                                          rs.getInt("queue_type"),
                                          rs.getInt("item_type"),
                                          rs.getString("form_type"),
                                          rs.getString("track_num")));
      }

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      //e.printStackTrace();
      logEntry("getNewChangeRequests()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  /*
  ** METHOD public void getData()
  **
  */
  public synchronized void getData(long userNode)
  {
    try
    {
      connect();

      if (hasLookupChanged())
      {
        // set up longLookup value (numeric version of lookup value)
        try
        {
          longLookup = Long.parseLong(lookupValue.trim());
        }
        catch (Exception e)
        {
          longLookup = -1;
        }

        // add wildcards to stringLookup so we can match partial strings
        if(lookupValue.trim().equals(""))
        {
          stringLookup = "passall";
        }
        else
        {
          stringLookup = "%" + lookupValue.toUpperCase() + "%";
        }

        fromDate = getSqlFromDate();
        toDate   = getSqlToDate();

        // clear out any existing results
        lookupResults.clear();

        // get old acr statuses only if looking for anything/everything
        if(acrType.equals("-1"))
        {
          getOldChangeRequests(userNode);
        }

        // get new acr statuses
        getNewChangeRequests(userNode);

        lastAccountType    = accountType;
        lastAcrResidentQueue  = acrResidentQueue;
        lastFromMonth     = fromMonth;
        lastFromDay       = fromDay;
        lastFromYear      = fromYear;
        lastToMonth       = toMonth;
        lastToDay         = toDay;
        lastToYear        = toYear;
        lastLookupValue   = lookupValue;
      }

      // now create the sorted tree
      if(sortedResults != null)
      {
        sortedResults.clear();
      }

      sortedResults = null;

      sortedResults = new TreeSet(cdc);
      sortedResults.addAll(lookupResults);
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public synchronized Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }

    return result;
  }

  /*
  ** METHOD getActionDescription
  **
  ** Returns the description of the current action
  */
  public synchronized String getActionDescription()
  {
    return this.actionDescription;
  }

  /*
  ** ACCESSORS
  */
  public synchronized String getLookupValue()
  {
    return lookupValue;
  }
  public synchronized void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }

  public synchronized int getAction()
  {
    return this.action;
  }
  public synchronized void setAction(int action)
  {
    this.actionDescription = "Account Change Request";
  }
  public void setAction(String action)
  {
    int       actionNum;
    try
    {
      actionNum = Integer.parseInt(action);
    }
    catch(Exception e)
    {
      actionNum = mesConstants.LU_ACTION_INVALID;
    }

    setAction(actionNum);
  }

  public synchronized void setSortBy(String sortBy)
  {
    try
    {
      cdc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }

  public synchronized void setaccountType(String accountType)
  {
    this.accountType = accountType;
  }
  public synchronized String getaccountType()
  {
    return this.accountType;
  }
  public synchronized void setAcrResidentQueue(String acrResidentQueue)
  {
    try
    {
      this.acrResidentQueue = Integer.parseInt(acrResidentQueue);
    }
    catch(Exception e)
    {
    }
  }
  public synchronized String getAcrResidentQueue()
  {
    return Integer.toString(this.acrResidentQueue);
  }
  public synchronized void setAcrType(String acrType)
  {
    this.acrType = acrType;
  }
  public synchronized String getAcrType()
  {
    return this.acrType;
  }
  public synchronized void setSubmitted(String submitted)
  {
    this.submitted = true;
  }
  public synchronized boolean isSubmitted()
  {
    return this.submitted;
  }

  /*
  ** NESTED CLASSES
  */
  public class ChangeData
  {
    private Timestamp   changeDateSorted;
    private String      changeDate;
    private String      dbaName;
    private String      merchantNumber;
    private String      controlNumber;
    private String      status;
    private String      type;
    private String      originator;
    private String      lastChanged;
    private int         noteCount;
    private int         queueType;
    private int         itemType;
    private String      formType;
    private String      trackNum;

    public ChangeData()
    {
      changeDateSorted  = null;
      changeDate        = "";
      dbaName           = "";
      controlNumber     = "";
      status            = "";
      type              = "";
      originator        = "";
      lastChanged       = "";
      noteCount         = 0;
      queueType         = 0;
      itemType          = 0;
      formType          = "";
      trackNum          = "";
    }

    public ChangeData(Timestamp changeDateSorted,
                      String    changeDate,
                      String    dbaName,
                      String    merchantNumber,
                      String    controlNumber,
                      String    status,
                      String    type,
                      String    originator,
                      String    lastChanged,
                      int       noteCount,
                      int       queueType,
                      int       itemType,
                      String    formType,
                      String    trackNum)
    {
      setChangeDateSorted(changeDateSorted);
      setChangeDate(changeDate);
      setDbaName(dbaName);
      setMerchantNumber(merchantNumber);
      setControlNumber(controlNumber);
      setStatus(status);
      setType(type);
      setOriginator(originator);
      setLastChanged(lastChanged);
      setNoteCount(noteCount);
      setQueueType(queueType);
      setItemType(itemType);
      setFormType(formType);
      setTrackNum(trackNum);
    }

    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }

    public void setFormType(String formType)
    {
      this.formType = processStringField(formType);
    }
    public String getFormType()
    {
      return this.formType;
    }

    public void setChangeDateSorted(Timestamp changeDateSorted)
    {
      this.changeDateSorted = changeDateSorted;
    }
    public String getChangeDateSorted()
    {
      return DateTimeFormatter.getFormattedDate(changeDateSorted, "yyyyMMddHHmmss");
    }
    public void setChangeDate(String changeDate)
    {
      this.changeDate = processStringField(changeDate);
    }
    public String getChangeDate()
    {
      return this.changeDate;
    }
    public void setDbaName(String dbaName)
    {
      this.dbaName = processStringField(dbaName);
    }
    public String getDbaName()
    {
      return this.dbaName;
    }
    public void setMerchantNumber(String merchantNumber)
    {
      this.merchantNumber = processStringField(merchantNumber);
    }
    public String getMerchantNumber()
    {
      return this.merchantNumber;
    }
    public void setControlNumber(String controlNumber)
    {
      this.controlNumber = processStringField(controlNumber);
    }
    public String getControlNumber()
    {
      return this.controlNumber;
    }
    public void setStatus(String status)
    {
      this.status = processStringField(status);
    }
    public String getStatus()
    {
      return this.status;
    }
    public void setType(String type)
    {
      this.type = processStringField(type);
    }
    public String getType()
    {
      return this.type;
    }
    public void setOriginator(String originator)
    {
      this.originator = processStringField(originator);
    }
    public String getOriginator()
    {
      return this.originator;
    }
    public void setLastChanged(String lastChanged)
    {
      this.lastChanged = processStringField(lastChanged);
    }
    public String getLastChanged()
    {
      return this.lastChanged;
    }
    public void setNoteCount(int noteCount)
    {
      this.noteCount = noteCount;
    }
    public String getNoteCount()
    {
      String result = "0";
      try
      {
        result = Integer.toString(this.noteCount);
      }
      catch(Exception e)
      {
      }

      return result;
    }
    public void setQueueType(int queueType)
    {
      this.queueType = queueType;
    }
    public int getQueueType()
    {
      return this.queueType;
    }
    public void setItemType(int itemType)
    {
      this.itemType = itemType;
    }
    public int getItemType()
    {
      return this.itemType;
    }
    public String getTrackNum()
    {
      return trackNum;
    }
    public void setTrackNum(String trackNum)
    {
      if(trackNum==null)
      {
        this.trackNum = "";
      }
      else
      {
        this.trackNum = trackNum;
      }
    }
  }

  public class ChangeDataComparator
    implements Comparator, Serializable
  {
    public final static int   SB_CHANGE_DATE      = 0;
    public final static int   SB_DBA_NAME         = 1;
    public final static int   SB_MERCHANT_NUMBER  = 2;
    public final static int   SB_CONTROL_NUMBER   = 3;
    public final static int   SB_STATUS           = 4;
    public final static int   SB_TYPE             = 5;
    public final static int   SB_ORIGINATOR       = 6;
    public final static int   SB_FORM_TYPE        = 7;

    private int               sortBy;

    private boolean           sortAscending       = false;

    public ChangeDataComparator()
    {
      this.sortBy = SB_CHANGE_DATE;
    }

    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }

    public void setSortBy(int sortBy)
    {
      if(sortBy >= SB_CHANGE_DATE && sortBy <= SB_FORM_TYPE)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }

        this.sortBy = sortBy;
      }
    }

    private String getCompareString(ChangeData obj)
    {
      String result = "";

      try
      {
        switch(sortBy)
        {
          case SB_CHANGE_DATE:
            result = obj.getChangeDateSorted() + obj.getControlNumber();
            break;

          case SB_DBA_NAME:
            result = obj.getDbaName() + obj.getChangeDateSorted();
            break;

          case SB_MERCHANT_NUMBER:
            result = obj.getMerchantNumber() + obj.getChangeDateSorted();
            break;

          case SB_CONTROL_NUMBER:
            result = obj.getControlNumber();
            break;

          case SB_STATUS:
            result = obj.getStatus() + obj.getChangeDateSorted();
            break;

          case SB_TYPE:
            result = obj.getType() + obj.getChangeDateSorted();
            break;

          case SB_ORIGINATOR:
            result = obj.getOriginator() + obj.getChangeDateSorted();
            break;

          case SB_FORM_TYPE:
            result = obj.getFormType() + obj.getChangeDateSorted();
            break;

          default:
            break;
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::getCompareString()", e.toString());
      }

      return result;
    }

    int compare(ChangeData o1, ChangeData o2)
    {
      int result    = 0;

      String compareString1 = getCompareString(o1);
      String compareString2 = getCompareString(o2);

      try
      {
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }

      return result;
    }

    boolean equals(ChangeData o1, ChangeData o2)
    {
      boolean result    = false;

      String compareString1 = getCompareString(o1);
      String compareString2 = getCompareString(o2);

      try
      {
        result = compareString1.equals(compareString2);
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }

      return result;
    }

    public int compare(Object o1, Object o2)
    {
      int result;

      try
      {
        result = compare((ChangeData)o1, (ChangeData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }

      return result;
    }

    public boolean equals(Object o1, Object o2)
    {
      boolean result;

      try
      {
        result = equals((ChangeData)o1, (ChangeData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }

      return result;
    }
  }
}/*@lineinfo:generated-code*/