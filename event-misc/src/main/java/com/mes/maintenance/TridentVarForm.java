/*@lineinfo:filename=TridentVarForm*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/TridentProfile.sqlj $

  Description:  
  
  TridentProfile
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-02-03 10:39:36 -0800 (Tue, 03 Feb 2009) $
  Version            : $Revision: 15771 $

  Change History:
     See VSS database

  Copyright (C) 2004,2005,2006 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesEmails;
import com.mes.constants.mesConstants;
import com.mes.forms.AmexSENumber;
import com.mes.forms.ButtonField;
import com.mes.forms.CityStateZipField;
import com.mes.forms.DiscoverMerchantNumber;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.JCBMerchantNumber;
import com.mes.forms.PhoneField;
import com.mes.forms.TextareaField;
import com.mes.forms.TridentTidField;
import com.mes.net.MailMessage;
import com.mes.ops.TimezoneTable;
import sqlj.runtime.ResultSetIterator;

public class TridentVarForm extends FieldBean
{
  public Vector nextLinks   = new Vector();
  public boolean success    = false;
  
  public TridentVarForm()
  {
  }
  
  public void setProperties(HttpServletRequest request)
  {
    try
    {
      connect();
      
      // setFields calls createFields()
      setFields(request);
      
      register(user.getLoginName(), request);
      
      // check for submission
      if(!getData("sendEmail").equals(""))
      {
        // user wants to send this form via email
        MailMessage msg = new MailMessage();
        
        msg.setFrom(getData("emailFrom"));
        msg.addTo(getData("emailTo"));
        msg.setSubject(getData("emailSubject"));
        msg.setText(getData("emailBody"));
        msg.addStringAsTextFile("varform.txt", buildAttachment());
        
        msg.send();
        
        success = true;
      }
      else
      {
        // just load data
        loadData();
      }
      
      addNextLink("Close", "javascript:window.close();");
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // hidden fields
      fields.add(new HiddenField("referredFrom"));
      fields.add(new HiddenField("productSelection"));
      fields.add(new HiddenField("productCode"));
      fields.add(new HiddenField("tridentPaymentGateway"));
      
      // regular fields
      fields.add(new Field("catid",           "Terminal Id", 8, 25, false));
      fields.add(new TridentTidField("tid",   "Profile ID", 20, true));
      fields.add(new Field("merchantName",    "Merchant Name", 25, 25, true));
      fields.add(new Field("merchantNumber",  "Merchant Number", 16, 25, false));
      fields.add(new CityStateZipField("merchantCSZ", "Merchant City, State, Zip", 25, true));
      fields.add(new Field("merchantBin",     "BIN Number", 6, 25, false));
      fields.add(new Field("merchantAgent",   "Agent", 6, 25, false));
      fields.add(new Field("merchantChain",   "Chain", 6, 25, false));
      fields.add(new Field("sicCode",         "MCC/SIC", 4, 25, true));
      fields.add(new Field("countryCode",     "Country Code", 3, 25, true));
      fields.add(new Field("locationNumber",  "Location Number", 5, 25, true));
      fields.add(new DropDownField("timeZone",  "Time Zone", new TimezoneTable(),false));
      fields.add(new Field("merchantKey",     "Merchant Key", 32, 32, true));
      fields.add(new PhoneField("primaryAuth",  "Primary Authorization", false));
      fields.add(new PhoneField("secondaryAuth","Secondary Authorization", false));
      fields.add(new PhoneField("primarySettlement", "Primary Settlement", false));
      fields.add(new PhoneField("secondarySettlement", "Secondary Settlement", false));
      fields.add(new Field("ipUrl", "IP Address", 25, 25, true));
      fields.add(new Field("ipPort", "Port", 4, 25, true));
      fields.add(new Field("apiUrl", "API URL", 60, 60, true));
      fields.add(new AmexSENumber("amexCaid", "American Express", 25, true, null));
      fields.add(new DiscoverMerchantNumber("discoverCaid", "Discover", 25, true, null));
      fields.add(new JCBMerchantNumber("jcbCaid", "JCB", 25, true, null));
      fields.add(new PhoneField("merchantPhone", "Merchant Phone", false));
      
      // email fields
      fields.add(new EmailField("emailTo", "To:", 75, 75, false));
      fields.add(new EmailField("emailFrom", "From: ", 75, 75, false));
      fields.add(new Field("emailSubject", "Subject:", 75, 75, false));
      fields.add(new TextareaField("emailBody", "Message:", 750, 10, 75, false));
      
      fields.add(new ButtonField("sendEmail", "Email VAR Form"));
      
      fields.setHtmlExtra("class=\"formText\"");
    }
    catch(Exception e)
    {
      logEntry("createFields()", e.toString());
    }
    finally
    {
    }
  }
  
  private void loadData()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:158^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tp.terminal_id                  tid,
//                  tp.merchant_name                merchant_name,
//                  decode(mr.merch_bank_number,
//                    3858, '0'||to_char(tp.merchant_number)||'000',
//                    to_char(tp.merchant_number))  merchant_number, 
//                  tp.addr_city                    merchant_csz_city,
//                  tp.addr_state                   merchant_csz_state,
//                  tp.addr_zip                     merchant_csz_zip,
//                  msi.bin                         merchant_bin,
//                  msi.agent                       merchant_agent,
//                  msi.chain                       merchant_chain,
//                  tp.sic_code                     sic_code,
//                  '840'                           country_code,
//                  msi.location_number             location_number,
//                  tp.time_zone                    time_zone,
//                  decode(tp.merchant_key_required,
//                    'Y', tp.merchant_key,
//                    'not required')               merchant_key,
//                  '8668407916'                    primary_auth,
//                  '8668407916'                    secondary_auth,
//                  '8668407916'                    primary_settlement,
//                  '8668407916'                    secondary_settlement,
//                  'pos.tnsi.com'                  ip_url,
//                  '5080'                          ip_port,
//                  'https://api.merchante-solutions.com/mes-api/tridentApi' api_url,
//                  decode(msi.term_application_profgen,
//                    :mesConstants.TRIDENT_STAGE_BUILD_TERM_APP, -- 'TRI_STAGE',
//                      decode(msi.term_comm_type, 
//                              :mesConstants.TERM_COMM_TYPE_IP, 'TRISTAGEIP', -- 2
//                                  'TRISTAGEDIAL'),
//                    :mesConstants.TRIDENT_API_TERM_APP, 'TRIAPI', -- 'TRI-GATEWAY'
//                    :mesConstants.TRIDENT_VIRTUAL_TERMINAL_LIMITED, 'VTLIMITED', -- 'VT-LIMITED'
//                    'TERMINAL')                   product_selection,
//                  tp.product_code                 product_code,
//                  nvl(tpc.trident_payment_gateway,'N')  trident_payment_gateway,
//                  tp.amex_caid                    amex_caid,
//                  tp.disc_caid                    discover_caid,
//                  tp.jcb_caid                     jcb_caid,
//                  mr.merch_email_address          email_to,
//                  et.from_addr                    email_from,
//                  et.subject                      email_subject,
//                  et.body                         email_body,
//                  addr.address_phone              merchant_phone
//          from    trident_profile tp,
//                  merchant mr,
//                  mms_stage_info msi,
//                  email_templates et,
//                  trident_product_codes tpc,
//                  address addr
//          where   tp.catid = :getData("catid") and
//                  tp.merchant_number = mr.merch_number(+) and
//                  tp.catid = msi.vnum(+) and
//                  et.id = decode(msi.term_application_profgen,
//                    --'VT-LIMITED', 2,
//                    --1) and
//                    :mesConstants.TRIDENT_VIRTUAL_TERMINAL_LIMITED, :MesEmails.ET_TRIDENT_VT_LIMITED,
//                    :MesEmails.ET_TRIDENT_VAR_FORM) and
//                  tp.product_code = tpc.product_code(+) and
//                  mr.app_seq_num = addr.app_seq_num(+) and
//                  addr.addresstype_code(+) = :mesConstants.ADDR_TYPE_BUSINESS -- 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1102 = getData("catid");
  try {
   String theSqlTS = "select  tp.terminal_id                  tid,\n                tp.merchant_name                merchant_name,\n                decode(mr.merch_bank_number,\n                  3858, '0'||to_char(tp.merchant_number)||'000',\n                  to_char(tp.merchant_number))  merchant_number, \n                tp.addr_city                    merchant_csz_city,\n                tp.addr_state                   merchant_csz_state,\n                tp.addr_zip                     merchant_csz_zip,\n                msi.bin                         merchant_bin,\n                msi.agent                       merchant_agent,\n                msi.chain                       merchant_chain,\n                tp.sic_code                     sic_code,\n                '840'                           country_code,\n                msi.location_number             location_number,\n                tp.time_zone                    time_zone,\n                decode(tp.merchant_key_required,\n                  'Y', tp.merchant_key,\n                  'not required')               merchant_key,\n                '8668407916'                    primary_auth,\n                '8668407916'                    secondary_auth,\n                '8668407916'                    primary_settlement,\n                '8668407916'                    secondary_settlement,\n                'pos.tnsi.com'                  ip_url,\n                '5080'                          ip_port,\n                'https://api.merchante-solutions.com/mes-api/tridentApi' api_url,\n                decode(msi.term_application_profgen,\n                   :1 , -- 'TRI_STAGE',\n                    decode(msi.term_comm_type, \n                             :2 , 'TRISTAGEIP', -- 2\n                                'TRISTAGEDIAL'),\n                   :3 , 'TRIAPI', -- 'TRI-GATEWAY'\n                   :4 , 'VTLIMITED', -- 'VT-LIMITED'\n                  'TERMINAL')                   product_selection,\n                tp.product_code                 product_code,\n                nvl(tpc.trident_payment_gateway,'N')  trident_payment_gateway,\n                tp.amex_caid                    amex_caid,\n                tp.disc_caid                    discover_caid,\n                tp.jcb_caid                     jcb_caid,\n                mr.merch_email_address          email_to,\n                et.from_addr                    email_from,\n                et.subject                      email_subject,\n                et.body                         email_body,\n                addr.address_phone              merchant_phone\n        from    trident_profile tp,\n                merchant mr,\n                mms_stage_info msi,\n                email_templates et,\n                trident_product_codes tpc,\n                address addr\n        where   tp.catid =  :5  and\n                tp.merchant_number = mr.merch_number(+) and\n                tp.catid = msi.vnum(+) and\n                et.id = decode(msi.term_application_profgen,\n                  --'VT-LIMITED', 2,\n                  --1) and\n                   :6 ,  :7 ,\n                   :8 ) and\n                tp.product_code = tpc.product_code(+) and\n                mr.app_seq_num = addr.app_seq_num(+) and\n                addr.addresstype_code(+) =  :9  -- 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.TridentVarForm",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,mesConstants.TRIDENT_STAGE_BUILD_TERM_APP);
   __sJT_st.setInt(2,mesConstants.TERM_COMM_TYPE_IP);
   __sJT_st.setString(3,mesConstants.TRIDENT_API_TERM_APP);
   __sJT_st.setString(4,mesConstants.TRIDENT_VIRTUAL_TERMINAL_LIMITED);
   __sJT_st.setString(5,__sJT_1102);
   __sJT_st.setString(6,mesConstants.TRIDENT_VIRTUAL_TERMINAL_LIMITED);
   __sJT_st.setInt(7,MesEmails.ET_TRIDENT_VT_LIMITED);
   __sJT_st.setInt(8,MesEmails.ET_TRIDENT_VAR_FORM);
   __sJT_st.setInt(9,mesConstants.ADDR_TYPE_BUSINESS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.TridentVarForm",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:220^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        System.out.println("merchant number: " + rs.getString("merchant_number"));
        setFields(rs, false);
        System.out.println("merchant number field: " + getData("merchantNumber"));
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  protected void postHandleRequest(HttpServletRequest request)
  {
    try
    {
      // set referred field from request if not already set
      if(getData("referredFrom").equals(""))
      {
        if(request.getHeader("Referer") != null && !request.getHeader("Referer").equals(""))
        {
          setData("referredFrom", request.getHeader("Referer"));
        }
      }
    }
    catch(Exception e)
    {
      logEntry("preHandleRequest()", e.toString());
    }
  }
  
  private void addNextLink(String label, String url)
  {
    StringBuffer nextLink = new StringBuffer("");
    
    try
    {
      nextLink.append("<a href=\"");
      nextLink.append(url);
      nextLink.append("\">");
      nextLink.append(label);
      nextLink.append("</a>");
    
      nextLinks.add(nextLink.toString());
    }
    catch(Exception e)
    {
      logEntry("addNextLink(" + label + ", " + url + ")", e.toString());
    }
  }
  
  private void addConfigLine(StringBuffer sb, String label, String value)
  {
    sb.append(label);
    
    for(int i=0; i<(25-label.length()); ++i)
    {
      sb.append(" ");
    }
    
    sb.append(value);
    sb.append("\n");
  }
  
  private String buildAttachment()
  {
    StringBuffer result = new StringBuffer("");
    
    if( isPayFlow() )
    {
      result.append("\nMerchant e-Solutions\n920 N. Argonne Rd. Suite 200 Spokane WA, 99212\n\n\n");
      result.append("PayPal Payflow Gateway Merchant Account Setup Form\n");
      result.append("PLEASE CHOOSE MERCHANT E-SOLUTIONS AS YOUR PROCESSOR\n\n");
      
      addConfigLine(result, "Merchant Name", getData("merchantName"));
      addConfigLine(result, "Acquirer", "Merchant e-Solutions");
      addConfigLine(result, "Acquirer Contact", "CLIENT SUPPORT");
      addConfigLine(result, "Acquirer Phone", "1-888-288-2692");
      addConfigLine(result, "Merchant Account #", getData("merchantNumber"));
      result.append("\nAdditional Card Types: Choose Merchant e-Solutions as your processor\n");
      addConfigLine(result, "American Express", getData("amexCaid"));
      addConfigLine(result, "Discover", getData("discoverCaid"));
      addConfigLine(result, "JCB", getData("jcbCaid"));
      addConfigLine(result, "Visa/Mastercard", "Choose Merchant e-Solutions as your processor");
      result.append("\nPayflow Specific Information:\n");
      addConfigLine(result, "BIN", getData("merchantBin"));    
      addConfigLine(result, "Merchant Number", getData("merchantNumber"));
      addConfigLine(result, "Store Number", ((TridentTidField)(getField("tid"))).getStoreNumber());
      addConfigLine(result, "Terminal Number", ((TridentTidField)(getField("tid"))).getTerminalNumber());
      addConfigLine(result, "Zip Code", getData("merchantCSZZip"));
      addConfigLine(result, "Time Zone", getData("timeZone"));
      addConfigLine(result, "MCC/SIC", getData("sicCode"));
      addConfigLine(result, "Merchant Name", getData("merchantName"));
      addConfigLine(result, "Merchant Phone", getData("merchantPhone"));
      addConfigLine(result, "Merchant State", getData("merchantCSZState"));
      addConfigLine(result, "Agent BIN", "000000");
      addConfigLine(result, "Agent Chain", getData("merchantChain"));
      addConfigLine(result, "Merchant Location #", "000001");
      addConfigLine(result, "Terminal ID", getData("catid"));
    }
    else
    {
      result.append("\nMerchant e-Solutions\n920 N. Argonne Rd. Suite 200 Spokane WA, 99212\n\n\n");
      result.append("PROPRIETARY SOFTWARE MERCHANT\n     CONFIGURATION INFORMATION\n\n");
      
      if(getData("productSelection").equals("VTLIMITED"))
      {
        System.out.println("adding merchant number to attachment (" + getData("merchantNumber") + ")");
        addConfigLine(result, "Merchant Number", getData("merchantNumber"));
        addConfigLine(result, "Business Name", getData("merchantName"));
      }
      else
      {
        if(getData("tridentPaymentGateway").equals("Y"))
        {
          addConfigLine(result, "Profile ID", getField("tid").getHtmlData());
        }
        else
        {
          addConfigLine(result, "Terminal ID", getData("catid"));
          addConfigLine(result, "Merchant Name", getData("merchantName"));
          addConfigLine(result, "Merchant Number", getData("merchantNumber"));
          addConfigLine(result, "City", getData("merchantCSZCity"));
          addConfigLine(result, "State", getData("merchantCSZState"));
          addConfigLine(result, "Zip", getData("merchantCSZZip"));
          addConfigLine(result, "BIN Number", getData("merchantBin"));
          addConfigLine(result, "Agent", getData("merchantAgent"));
          addConfigLine(result, "Chain", getData("merchantChain"));
          addConfigLine(result, "Store Number", ((TridentTidField)(getField("tid"))).getStoreNumber());
          addConfigLine(result, "Terminal Number", ((TridentTidField)(getField("tid"))).getTerminalNumber());
          addConfigLine(result, "MCC/SIC", getData("sicCode"));
          addConfigLine(result, "Country Code", getData("countryCode"));
          addConfigLine(result, "Location Number", getData("locationNumber"));
          addConfigLine(result, "Time Zone", getData("timeZone"));
        }
    
        // merchantKey goes for all profiles
        addConfigLine(result, "Merchant Key", getData("merchantKey"));
    
        if(getData("productSelection").equals("TRISTAGEDIAL") || getData("productSelection").equals("TERMINAL"))
        {
          addConfigLine(result, "Primary Authorization", getData("primaryAuth"));
          addConfigLine(result, "Secondary Authorization", getData("secondaryAuth"));
          addConfigLine(result, "Primary Settlement", getData("primarySettlement"));
          addConfigLine(result, "Secondary Settlement", getData("secondarySettlement"));
        }
    
        if(getData("productSelection").equals("TRISTAGEIP"))
        {
          addConfigLine(result, "IP Address", getData("ipUrl"));
          addConfigLine(result, "Port", getData("ipPort"));
        }
    
        if(getData("productSelection").equals("TRIAPI"))
        {
          addConfigLine(result, "API URL", getData("apiUrl"));
          result.append("\n");
          addConfigLine(result, "Merchant Name", getData("merchantName"));
        }
    
        addConfigLine(result, "American Express", getData("amexCaid"));
        addConfigLine(result, "Discover", getData("discoverCaid"));
        addConfigLine(result, "JCB", getData("jcbCaid"));
      }
    }
    
    return (result.toString());
  }
  
  public boolean isPayFlow()
  {
    boolean result = false;
    
    if(getData("productCode").equals(mesConstants.VITAL_ME_VERISIGN_PAYFLOW_LINK) ||
       getData("productCode").equals(mesConstants.VITAL_ME_VERISIGN_PAYFLOW_PRO) )
    {
      result = true;
    }
    
    return( result );
  }
}/*@lineinfo:generated-code*/