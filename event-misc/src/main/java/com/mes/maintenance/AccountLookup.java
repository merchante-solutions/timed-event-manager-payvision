/*@lineinfo:filename=AccountLookup*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/AccountLookup.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-02-01 09:43:39 -0800 (Sun, 01 Feb 2009) $
  Version            : $Revision: 15764 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import com.mes.constants.MesHierarchy;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class AccountLookup extends SQLJConnectionBase
  implements Serializable
{
  public class MerchantData
    implements Serializable
  {
    public final static int     REC_TYPE_MERCHANT   = 1;
    public final static int     REC_TYPE_GROUP      = 2;
    
    private String      merchantNumber;
    private String      dbaName;
    private String      assocNumber;
    private String      bankNumber;
    private String      controlNumber;
    private String      appSeqNum;
    private int         recordType;
    private String      status;
    private String      activation;
    private String      lastActivity;
    private String      accountStatus;
    private String      accountStatusCompare;
    private String      dateOpened;
    private String      dateOpenedCompare;
    
    public MerchantData()
    {
      merchantNumber        = "";
      dbaName               = "";
      assocNumber           = "";
      bankNumber            = "";
      controlNumber         = "";
      appSeqNum             = "";
      status                = "";
      activation            = "";
      lastActivity          = "";
      accountStatus         = "";
      dateOpened            = "";
      accountStatusCompare  = "";
      dateOpenedCompare     = "";
      
      recordType            = 0;
    }
    
    public MerchantData(ResultSet rs, int recordType)
    {
      try
      {
        setMerchantNumber(rs.getString("merchant_number"));
        setDbaName(rs.getString("dba_name"));
        setAssocNumber(rs.getString("dmagent"));
        setBankNumber(rs.getString("bank_number"));
        setControlNumber(rs.getString("merc_cntrl_number"));
        setAppSeqNum(rs.getString("app_seq_num"));
        setStatus(rs.getString("status"));
        setActivation(rs.getString("activation_date"));
        setLastActivity(rs.getString("last_activity"));
        setDateOpened(rs.getString("date_opened"));
        setAccountStatus(rs.getString("account_status"));
        setDateOpenedCompare(rs.getString("date_opened_compare"));
        setAccountStatusCompare(rs.getString("account_status_compare"));
      }
      catch(Exception e)
      {
        logEntry("MerchantData()", e.toString());
      }
      
      setRecordType(recordType);
    }
    
    public String processStringField(String fieldData)
    {
      return((fieldData == null || fieldData.length() == 0) ? "---" : fieldData);
    }
    
    public void setDateOpened(String dateOpened)
    {
      this.dateOpened = processStringField(dateOpened);
    }
    public String getDateOpened()
    {
      return dateOpened;
    }
    public void setDateOpenedCompare(String dateOpenedCompare)
    {
      this.dateOpenedCompare = processStringField(dateOpenedCompare);
    }
    public String getDateOpenedCompare()
    {
      return dateOpenedCompare;
    }
    public void setAccountStatus(String accountStatus)
    {
      this.accountStatus = processStringField(accountStatus);
    }
    public String getAccountStatus()
    {
      return accountStatus;
    }
    public void setAccountStatusCompare(String accountStatusCompare)
    {
      this.accountStatusCompare = processStringField(accountStatusCompare);
    }
    public String getAccountStatusCompare()
    {
      return accountStatusCompare;
    }
    public void setStatus(String status)
    {
      this.status = processStringField(status);
    }
    public String getStatus()
    {
      return status;
    }
    public void setActivation(String activation)
    {
      this.activation = processStringField(activation);
    }
    public String getActivation()
    {
      return activation;
    }
    public void setLastActivity(String lastActivity)
    {
      this.lastActivity = processStringField(lastActivity);
    }
    public String getLastActivity()
    {
      return lastActivity;
    }
    public void setMerchantNumber(String merchantNumber)
    {
      this.merchantNumber = processStringField(merchantNumber);
    }
    public String getMerchantNumber()
    {
      return this.merchantNumber;
    }
    public void setDbaName(String dbaName)
    {
      this.dbaName = processStringField(dbaName);
    }
    public String getDbaName()
    {
      return this.dbaName;
    }
    public void setAssocNumber(String assocNumber)
    {
      this.assocNumber = processStringField(assocNumber);
    }
    public String getAssocNumber()
    {
      return this.assocNumber;
    }
    public void setBankNumber(String bankNumber)
    {
      this.bankNumber = processStringField(bankNumber);
    }
    public String getBankNumber()
    {
      return this.bankNumber;
    }
    public void setControlNumber(String controlNumber)
    {
      this.controlNumber = processStringField(controlNumber);
    }
    public String getControlNumber()
    {
      return this.controlNumber;
    }
    public void setAppSeqNum(String appSeqNum)
    {
      this.appSeqNum = processStringField(appSeqNum);
    }
    public String getAppSeqNum()
    {
      return this.appSeqNum;
    }
    public void setRecordType(int recordType)
    {
      this.recordType = recordType;
    }
    public int getRecordType()
    {
      return this.recordType;
    }
    
  }
  
  public class MerchantDataComparator
    implements Comparator, Serializable
  {
    public final static int   SB_DBA_NAME         = 0;
    public final static int   SB_MERCHANT_NUMBER  = 1;
    public final static int   SB_ASSOC_NUMBER     = 2;
    public final static int   SB_BANK_NUMBER      = 3;
    public final static int   SB_CONTROL_NUMBER   = 4;
    public final static int   SB_APP_SEQ_NUM      = 5;
    public final static int   SB_ACCOUNT_STATUS   = 6;
    public final static int   SB_DATE_OPENED      = 7;
    
    private int               sortBy;
    
    private boolean           sortAscending       = true;
    
    public MerchantDataComparator()
    {
      this.sortBy = SB_DBA_NAME;
    }
    
    public void setSortBy(String sortBy)
    {
      try
      {
        setSortBy(Integer.parseInt(sortBy));
      }
      catch(Exception e)
      {
        logEntry("setSortBy(" + sortBy + ")", e.toString());
      }
    }
    
    public void setSortBy(int sortBy)
    {
      if(sortBy >= 0 && sortBy <= SB_DATE_OPENED)
      {
        if(sortBy == this.sortBy)
        {
          sortAscending = ! sortAscending;
        }
        else
        {
          sortAscending = true;
        }
        
        this.sortBy = sortBy;
      }
    }
  
    int compare(MerchantData o1, MerchantData o2)
    {
      int result    = 0;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_DBA_NAME:
            // do some special logic for groups/assns to put them at the top
            if((o1.getRecordType() == MerchantData.REC_TYPE_GROUP &&
                o2.getRecordType() == MerchantData.REC_TYPE_GROUP) ||
               (o1.getRecordType() != MerchantData.REC_TYPE_GROUP &&
                o2.getRecordType() != MerchantData.REC_TYPE_GROUP))
            {
              // normal conditions
              compareString1 = o1.getDbaName() + o1.getMerchantNumber();
              compareString2 = o2.getDbaName() + o2.getMerchantNumber();
            }
            else if(o1.getRecordType() == MerchantData.REC_TYPE_GROUP)
            {
              // o1 gets the top
              compareString1  = "aaa";
              compareString2  = "zzz";
            }
            else
            {
              // o2 gets the top
              compareString1  = "zzz";
              compareString2  = "aaa";
            }
            break;
          
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.getMerchantNumber();
            compareString2 = o2.getMerchantNumber();
            break;
          
          case SB_ASSOC_NUMBER:
            compareString1 = o1.getAssocNumber() + o1.getMerchantNumber();
            compareString2 = o2.getAssocNumber() + o2.getMerchantNumber();
            break;
          
          case SB_BANK_NUMBER:
            compareString1 = o1.getBankNumber() + o1.getMerchantNumber();
            compareString2 = o2.getBankNumber() + o2.getBankNumber();
            break;
          
          case SB_CONTROL_NUMBER:
            compareString1 = o1.getControlNumber() + o1.getMerchantNumber();
            compareString2 = o2.getControlNumber() + o2.getMerchantNumber();
            break;
          
          case SB_APP_SEQ_NUM:
            compareString1 = o1.getAppSeqNum() + o1.getMerchantNumber();
            compareString2 = o2.getAppSeqNum() + o2.getMerchantNumber();
            break;

          case SB_ACCOUNT_STATUS:
            compareString1 = o1.getAccountStatusCompare() + o1.getMerchantNumber();
            compareString2 = o2.getAccountStatusCompare() + o2.getMerchantNumber();
            break;
            
          case SB_DATE_OPENED:
            compareString1 = o1.getDateOpenedCompare() + o1.getMerchantNumber();
            compareString2 = o2.getDateOpenedCompare() + o2.getMerchantNumber();
            break;
          
          default:
            break;
        }
        
        if(sortAscending)
        {
          result = compareString1.compareTo(compareString2);
        }
        else
        {
          result = compareString2.compareTo(compareString1);
        }
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::compare()", e.toString());
      }
      
      return result;
    }
    
    boolean equals(MerchantData o1, MerchantData o2)
    {
      boolean result    = false;
      
      String compareString1 = "";
      String compareString2 = "";
      
      try
      {
        switch(sortBy)
        {
          case SB_DBA_NAME:
            compareString1 = o1.getDbaName() + o1.getMerchantNumber();
            compareString2 = o2.getDbaName() + o2.getMerchantNumber();
            break;
          
          case SB_MERCHANT_NUMBER:
            compareString1 = o1.getMerchantNumber();
            compareString2 = o2.getMerchantNumber();
            break;
          
          case SB_ASSOC_NUMBER:
            compareString1 = o1.getAssocNumber() + o1.getMerchantNumber();
            compareString2 = o2.getAssocNumber() + o2.getMerchantNumber();
            break;
          
          case SB_BANK_NUMBER:
            compareString1 = o1.getBankNumber() + o1.getMerchantNumber();
            compareString2 = o2.getBankNumber() + o2.getBankNumber();
            break;
          
          case SB_CONTROL_NUMBER:
            compareString1 = o1.getControlNumber() + o1.getMerchantNumber();
            compareString2 = o2.getControlNumber() + o2.getMerchantNumber();
            break;
          
          case SB_APP_SEQ_NUM:
            compareString1 = o1.getAppSeqNum() + o1.getMerchantNumber();
            compareString2 = o2.getAppSeqNum() + o2.getMerchantNumber();
            break;

          case SB_ACCOUNT_STATUS:
            compareString1 = o1.getAccountStatusCompare() + o1.getMerchantNumber();
            compareString2 = o2.getAccountStatusCompare() + o2.getMerchantNumber();
            break;
            
          case SB_DATE_OPENED:
            compareString1 = o1.getDateOpenedCompare() + o1.getMerchantNumber();
            compareString2 = o2.getDateOpenedCompare() + o2.getMerchantNumber();
            break;
          
          default:
            break;
        }

        result = compareString1.equals(compareString2);        
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::equals()", e.toString());
      }
      
      return result;
    }
    
    public int compare(Object o1, Object o2)
    {
      int result;
      
      try
      {
        result = compare((MerchantData)o1, (MerchantData)o2);
      }
      catch(Exception e)
      {
        result = 0;
      }
      
      return result;
    }
    
    public boolean equals(Object o1, Object o2)
    {
      boolean result;
      
      try
      {
        result = equals((MerchantData)o1, (MerchantData)o2);
      }
      catch(Exception e)
      {
        result = false;
      }
      
      return result;
    }
  }
  
  private String                  lookupValue         = null;
  private String                  lastLookupValue     = null;
  
  private int                     action              = mesConstants.LU_ACTION_ACCOUNT_LOOKUP;
  private String                  actionDescription   = "Merchant Account";
  private String                  actionURL           = "/jsp/maintenance/view_account.jsp";
  
  private MerchantDataComparator  mdc                 = new MerchantDataComparator();
  private Vector                  lookupResults       = new Vector();
  private TreeSet                 sortedResults       = null;
  
  private boolean                 useAppFilter        = false;
  private long                    userId              = 0L;
  
  public void AccountLookup()
  {
  }
    
  /*
  ** METHOD setDefaultAction
  **
  ** Sets up a default action type if one has not already been assigned
  */
  public void setDefaultAction()
  {
    if(action == mesConstants.LU_ACTION_INVALID)
    {
      setAction(mesConstants.LU_ACTION_CALL_TRACKING);
    }
  }
  
  /*
  ** METHOD getRecsFromSuperNode
  **
  ** Performs index-enhanced queries from the super node (9999999999)
  */
  private void getRecsFromSuperNode(String stringLookup, long longLookup, String numberString, long userNode)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    try
    {
      if(longLookup > -1)  // user is searching for a number
      {
        // first check merchant number
        if(useAppFilter)
        {
          /*@lineinfo:generated-code*//*@lineinfo:521^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif)
//                          index (mr idx_merchant_merch_number)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')') account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif                 m,
//                        merchant            mr,
//                        group_rep_merchant  grm
//              where     m.merchant_number = :longLookup and
//                        m.merchant_number = grm.merchant_number and
//                        grm.user_id = :userId and
//                        m.merchant_number = mr.merch_number(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif)\n                        index (mr idx_merchant_merch_number)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')') account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif                 m,\n                      merchant            mr,\n                      group_rep_merchant  grm\n            where     m.merchant_number =  :1  and\n                      m.merchant_number = grm.merchant_number and\n                      grm.user_id =  :2  and\n                      m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setLong(2,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:559^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:563^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif)
//                          index (mr idx_merchant_merch_number)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif m,
//                        merchant mr,
//                        mes_allowed_bank_numbers mabn
//              where     m.bank_number = mabn.bank_number and  
//                        m.merchant_number = :longLookup and
//                        m.merchant_number = mr.merch_number(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif)\n                        index (mr idx_merchant_merch_number)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif m,\n                      merchant mr,\n                      mes_allowed_bank_numbers mabn\n            where     m.bank_number = mabn.bank_number and  \n                      m.merchant_number =  :1  and\n                      m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:600^11*/
        }
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
        }
        
        rs.close();
        it.close();
        
        // next check association
        if(useAppFilter)
        {
          /*@lineinfo:generated-code*//*@lineinfo:616^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif)
//                          index (mr idx_merchant_merch_number)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif                 m,
//                        merchant            mr,
//                        group_rep_merchant  grm
//              where     m.dmagent = :numberString and
//                        m.merchant_number = grm.merchant_number and
//                        grm.user_id = :userId and
//                        m.merchant_number = mr.merch_number(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif)\n                        index (mr idx_merchant_merch_number)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif                 m,\n                      merchant            mr,\n                      group_rep_merchant  grm\n            where     m.dmagent =  :1  and\n                      m.merchant_number = grm.merchant_number and\n                      grm.user_id =  :2  and\n                      m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,numberString);
   __sJT_st.setLong(2,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:654^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:658^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m idx_mif_dmagent))
//                          index (mr idx_merchant_merch_number)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif m,
//                        merchant mr,
//                        mes_allowed_bank_numbers mabn
//              where     m.bank_number = mabn.bank_number and
//                        m.dmagent = :numberString and
//                        m.merchant_number = mr.merch_number(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m idx_mif_dmagent))\n                        index (mr idx_merchant_merch_number)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif m,\n                      merchant mr,\n                      mes_allowed_bank_numbers mabn\n            where     m.bank_number = mabn.bank_number and\n                      m.dmagent =  :1  and\n                      m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,numberString);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:695^11*/
        }
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
        }
        
        rs.close();
        it.close();
        
        // next check phone number, transit routing number, and dda
        if(useAppFilter)
        {
          /*@lineinfo:generated-code*//*@lineinfo:711^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif)
//                          index (mr idx_merchant_merch_number)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif                 m,
//                        merchant            mr,
//                        group_rep_merchant  grm
//              where     (
//                          m.phone_1 = :numberString or
//                          m.transit_routng_num = :longLookup or
//                          m.dda_num = :numberString.trim()
//                        ) and
//                        m.merchant_number = grm.merchant_number and
//                        grm.user_id = :userId and
//                        m.merchant_number = mr.merch_number(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_814 = numberString.trim();
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif)\n                        index (mr idx_merchant_merch_number)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif                 m,\n                      merchant            mr,\n                      group_rep_merchant  grm\n            where     (\n                        m.phone_1 =  :1  or\n                        m.transit_routng_num =  :2  or\n                        m.dda_num =  :3 \n                      ) and\n                      m.merchant_number = grm.merchant_number and\n                      grm.user_id =  :4  and\n                      m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,numberString);
   __sJT_st.setLong(2,longLookup);
   __sJT_st.setString(3,__sJT_814);
   __sJT_st.setLong(4,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:753^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:757^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m idx_mif_phone1) 
//                          index (mr idx_merchant_merch_number) 
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif m,
//                        merchant mr,
//                        mes_allowed_bank_numbers mabn
//              where     m.bank_number = mabn.bank_number and
//                        (
//                          m.phone_1 = :numberString or
//                          m.transit_routng_num = :longLookup or
//                          m.dda_num = :numberString.trim()
//                        ) and
//                        m.merchant_number = mr.merch_number(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_815 = numberString.trim();
  try {
   String theSqlTS = "select  /*+\n                        index (m idx_mif_phone1) \n                        index (mr idx_merchant_merch_number) \n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif m,\n                      merchant mr,\n                      mes_allowed_bank_numbers mabn\n            where     m.bank_number = mabn.bank_number and\n                      (\n                        m.phone_1 =  :1  or\n                        m.transit_routng_num =  :2  or\n                        m.dda_num =  :3 \n                      ) and\n                      m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,numberString);
   __sJT_st.setLong(2,longLookup);
   __sJT_st.setString(3,__sJT_815);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:798^11*/
        }
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
        }
        
        rs.close();
        it.close();

        // next check control number
        if(useAppFilter)
        {
          /*@lineinfo:generated-code*//*@lineinfo:814^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif) 
//                          index (mr idx_merchant_control_number)  
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif                 m,
//                        merchant            mr,
//                        group_rep_merchant  grm
//              where     mr.merc_cntrl_number = :longLookup and
//                        mr.merch_number = m.merchant_number and
//                        mr.merch_number = grm.merchant_number and
//                        grm.user_id = :userId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif) \n                        index (mr idx_merchant_control_number)  \n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif                 m,\n                      merchant            mr,\n                      group_rep_merchant  grm\n            where     mr.merc_cntrl_number =  :1  and\n                      mr.merch_number = m.merchant_number and\n                      mr.merch_number = grm.merchant_number and\n                      grm.user_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setLong(2,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:852^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:856^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif) 
//                          index (mr idx_merchant_control_number)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif m,
//                        merchant mr,
//                        mes_allowed_bank_numbers mabn
//              where     mr.merc_cntrl_number = :longLookup and
//                        mr.merch_number = m.merchant_number and
//                        m.bank_number = mabn.bank_number
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif) \n                        index (mr idx_merchant_control_number)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif m,\n                      merchant mr,\n                      mes_allowed_bank_numbers mabn\n            where     mr.merc_cntrl_number =  :1  and\n                      mr.merch_number = m.merchant_number and\n                      m.bank_number = mabn.bank_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:893^11*/
        }
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
        }
        
        rs.close();
        it.close();
        
        // next check App Seq Num
        if(useAppFilter)
        {
          /*@lineinfo:generated-code*//*@lineinfo:909^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif) 
//                          index (mr idx_merchant_control_number)  
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif                 m,
//                        merchant            mr,
//                        group_rep_merchant  grm
//              where     mr.app_seq_num = :longLookup and
//                        mr.merch_number = m.merchant_number and
//                        mr.merch_number = grm.merchant_number and
//                        grm.user_id = :userId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif) \n                        index (mr idx_merchant_control_number)  \n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif                 m,\n                      merchant            mr,\n                      group_rep_merchant  grm\n            where     mr.app_seq_num =  :1  and\n                      mr.merch_number = m.merchant_number and\n                      mr.merch_number = grm.merchant_number and\n                      grm.user_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setLong(2,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:947^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:951^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif) 
//                          index (mr xpkmerchant)  
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif m,
//                        merchant mr,
//                        mes_allowed_bank_numbers mabn
//              where     mr.app_seq_num = :longLookup and
//                        mr.merch_number = m.merchant_number and
//                        m.bank_number = mabn.bank_number
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif) \n                        index (mr xpkmerchant)  \n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif m,\n                      merchant mr,\n                      mes_allowed_bank_numbers mabn\n            where     mr.app_seq_num =  :1  and\n                      mr.merch_number = m.merchant_number and\n                      m.bank_number = mabn.bank_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:988^11*/
        }
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
        }
        
        rs.close();
        it.close();
      }
      
      // always do the search string
      // user is looking for a string -- check dba name
      if(useAppFilter)
      {
        /*@lineinfo:generated-code*//*@lineinfo:1006^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                        index (m pkmif)
//                        index (mr idx_merchant_merch_number)
//                     */
//                      m.merchant_number     merchant_number,
//                      m.dba_name            dba_name,
//                      m.dmagent             dmagent,
//                      m.bank_number         bank_number,
//                      mr.merc_cntrl_number  merc_cntrl_number,
//                      nvl(m.dmacctst,' ')   status,
//                      decode(m.activation_date,
//                        null, '---',
//                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                      decode(m.last_deposit_date,
//                        null, '---',
//                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                      mr.app_seq_num        app_seq_num
//            from      mif                 m,
//                      merchant            mr,
//                      group_rep_merchant  grm
//            where     m.dba_name like :stringLookup and
//                      m.merchant_number = grm.merchant_number and
//                      grm.user_id = :userId and
//                      m.merchant_number = mr.merch_number(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                      index (m pkmif)\n                      index (mr idx_merchant_merch_number)\n                   */\n                    m.merchant_number     merchant_number,\n                    m.dba_name            dba_name,\n                    m.dmagent             dmagent,\n                    m.bank_number         bank_number,\n                    mr.merc_cntrl_number  merc_cntrl_number,\n                    nvl(m.dmacctst,' ')   status,\n                    decode(m.activation_date,\n                      null, '---',\n                      to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                    decode(m.last_deposit_date,\n                      null, '---',\n                      to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                    mr.app_seq_num        app_seq_num\n          from      mif                 m,\n                    merchant            mr,\n                    group_rep_merchant  grm\n          where     m.dba_name like  :1  and\n                    m.merchant_number = grm.merchant_number and\n                    grm.user_id =  :2  and\n                    m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,stringLookup);
   __sJT_st.setLong(2,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1044^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1048^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                        index (m idx_mif_dba_name) 
//                        index (mr idx_merchant_merch_number) 
//                     */
//                      m.merchant_number     merchant_number,
//                      m.dba_name            dba_name,
//                      m.dmagent             dmagent,
//                      m.bank_number         bank_number,
//                      mr.merc_cntrl_number  merc_cntrl_number,
//                      nvl(m.dmacctst,' ')   status,
//                      decode(m.activation_date,
//                        null, '---',
//                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                      decode(m.last_deposit_date,
//                        null, '---',
//                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                      mr.app_seq_num        app_seq_num
//            from      mif m,
//                      merchant mr,
//                      mes_allowed_bank_numbers mabn
//            where     m.bank_number = mabn.bank_number and
//                      m.dba_name like :stringLookup and
//                      m.merchant_number = mr.merch_number(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                      index (m idx_mif_dba_name) \n                      index (mr idx_merchant_merch_number) \n                   */\n                    m.merchant_number     merchant_number,\n                    m.dba_name            dba_name,\n                    m.dmagent             dmagent,\n                    m.bank_number         bank_number,\n                    mr.merc_cntrl_number  merc_cntrl_number,\n                    nvl(m.dmacctst,' ')   status,\n                    decode(m.activation_date,\n                      null, '---',\n                      to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                    decode(m.last_deposit_date,\n                      null, '---',\n                      to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                    mr.app_seq_num        app_seq_num\n          from      mif m,\n                    merchant mr,\n                    mes_allowed_bank_numbers mabn\n          where     m.bank_number = mabn.bank_number and\n                    m.dba_name like  :1  and\n                    m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,stringLookup);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1085^9*/
      }
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getRecsFromSuperNode(" + stringLookup + ", " + longLookup + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  /*
  ** METHOD getRecsFromBankNode
  **
  ** Performs index-enhanced queries from a bank node (xxxx00000)
  */
  private void getRecsFromBankNode(String stringLookup, long longLookup, long bankNumber, String numberString)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    try
    {
      if(longLookup > -1)  // user is searching for a number
      {
        // first check merchant number
        /*@lineinfo:generated-code*//*@lineinfo:1124^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                        index (m pkmif)
//                        index (mr idx_merchant_merch_number)
//                     */
//                      m.merchant_number     merchant_number,
//                      m.dba_name            dba_name,
//                      m.dmagent             dmagent,
//                      m.bank_number         bank_number,
//                      mr.merc_cntrl_number  merc_cntrl_number,
//                      nvl(m.dmacctst,' ')   status,
//                      decode(m.activation_date,
//                        null, '---',
//                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                      decode(m.last_deposit_date,
//                        null, '---',
//                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                      mr.app_seq_num        app_seq_num
//            from      mif m,
//                      merchant mr
//            where     m.merchant_number = :longLookup and
//                      m.bank_number     = :bankNumber and
//                      m.merchant_number = mr.merch_number(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                      index (m pkmif)\n                      index (mr idx_merchant_merch_number)\n                   */\n                    m.merchant_number     merchant_number,\n                    m.dba_name            dba_name,\n                    m.dmagent             dmagent,\n                    m.bank_number         bank_number,\n                    mr.merc_cntrl_number  merc_cntrl_number,\n                    nvl(m.dmacctst,' ')   status,\n                    decode(m.activation_date,\n                      null, '---',\n                      to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                    decode(m.last_deposit_date,\n                      null, '---',\n                      to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                    mr.app_seq_num        app_seq_num\n          from      mif m,\n                    merchant mr\n          where     m.merchant_number =  :1  and\n                    m.bank_number     =  :2  and\n                    m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setLong(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1160^9*/
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
        }
        
        rs.close();
        it.close();
        
        // next check association
        /*@lineinfo:generated-code*//*@lineinfo:1173^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                        index (m idx_mif_dmagent))
//                        index (mr idx_merchant_merch_number)
//                     */
//                      m.merchant_number     merchant_number,
//                      m.dba_name            dba_name,
//                      m.dmagent             dmagent,
//                      m.bank_number         bank_number,
//                      mr.merc_cntrl_number  merc_cntrl_number,
//                      nvl(m.dmacctst,' ')   status,
//                      decode(m.activation_date,
//                        null, '---',
//                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                      decode(m.last_deposit_date,
//                        null, '---',
//                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                      mr.app_seq_num        app_seq_num
//            from      mif m,
//                      merchant mr
//            where     m.dmagent         = :numberString and
//                      m.bank_number     = :bankNumber and
//                      m.merchant_number = mr.merch_number(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                      index (m idx_mif_dmagent))\n                      index (mr idx_merchant_merch_number)\n                   */\n                    m.merchant_number     merchant_number,\n                    m.dba_name            dba_name,\n                    m.dmagent             dmagent,\n                    m.bank_number         bank_number,\n                    mr.merc_cntrl_number  merc_cntrl_number,\n                    nvl(m.dmacctst,' ')   status,\n                    decode(m.activation_date,\n                      null, '---',\n                      to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                    decode(m.last_deposit_date,\n                      null, '---',\n                      to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                    mr.app_seq_num        app_seq_num\n          from      mif m,\n                    merchant mr\n          where     m.dmagent         =  :1  and\n                    m.bank_number     =  :2  and\n                    m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,numberString);
   __sJT_st.setLong(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1209^9*/
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
        }
        
        rs.close();
        it.close();
        
        // next check phone number, transit routing number, and dda
        /*@lineinfo:generated-code*//*@lineinfo:1222^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                        index (m idx_mif_phone1) 
//                        index (mr idx_merchant_merch_number) 
//                     */
//                      m.merchant_number     merchant_number,
//                      m.dba_name            dba_name,
//                      m.dmagent             dmagent,
//                      m.bank_number         bank_number,
//                      mr.merc_cntrl_number  merc_cntrl_number,
//                      nvl(m.dmacctst,' ')   status,
//                      decode(m.activation_date,
//                        null, '---',
//                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                      decode(m.last_deposit_date,
//                        null, '---',
//                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                      mr.app_seq_num        app_seq_num
//            from      mif m,
//                      merchant mr
//            where     (
//                        m.phone_1 = :numberString or
//                        m.transit_routng_num = :longLookup or
//                        m.dda_num = :numberString.trim()
//                      ) and
//                      m.bank_number     = :bankNumber and
//                      m.merchant_number = mr.merch_number(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_816 = numberString.trim();
  try {
   String theSqlTS = "select  /*+\n                      index (m idx_mif_phone1) \n                      index (mr idx_merchant_merch_number) \n                   */\n                    m.merchant_number     merchant_number,\n                    m.dba_name            dba_name,\n                    m.dmagent             dmagent,\n                    m.bank_number         bank_number,\n                    mr.merc_cntrl_number  merc_cntrl_number,\n                    nvl(m.dmacctst,' ')   status,\n                    decode(m.activation_date,\n                      null, '---',\n                      to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                    decode(m.last_deposit_date,\n                      null, '---',\n                      to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                    mr.app_seq_num        app_seq_num\n          from      mif m,\n                    merchant mr\n          where     (\n                      m.phone_1 =  :1  or\n                      m.transit_routng_num =  :2  or\n                      m.dda_num =  :3 \n                    ) and\n                    m.bank_number     =  :4  and\n                    m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,numberString);
   __sJT_st.setLong(2,longLookup);
   __sJT_st.setString(3,__sJT_816);
   __sJT_st.setLong(4,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1262^9*/
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
        }
        
        rs.close();
        it.close();

        // next check control number
        /*@lineinfo:generated-code*//*@lineinfo:1275^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                        index (m pkmif) 
//                        index (mr idx_merchant_control_number)  
//                     */
//                      m.merchant_number     merchant_number,
//                      m.dba_name            dba_name,
//                      m.dmagent             dmagent,
//                      m.bank_number         bank_number,
//                      mr.merc_cntrl_number  merc_cntrl_number,
//                      nvl(m.dmacctst,' ')   status,
//                      decode(m.activation_date,
//                        null, '---',
//                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                      decode(m.last_deposit_date,
//                        null, '---',
//                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                      mr.app_seq_num        app_seq_num
//            from      mif m,
//                      merchant mr
//            where     mr.merc_cntrl_number = :longLookup and
//                      mr.merch_number = m.merchant_number and
//                      m.bank_number     = :bankNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                      index (m pkmif) \n                      index (mr idx_merchant_control_number)  \n                   */\n                    m.merchant_number     merchant_number,\n                    m.dba_name            dba_name,\n                    m.dmagent             dmagent,\n                    m.bank_number         bank_number,\n                    mr.merc_cntrl_number  merc_cntrl_number,\n                    nvl(m.dmacctst,' ')   status,\n                    decode(m.activation_date,\n                      null, '---',\n                      to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                    decode(m.last_deposit_date,\n                      null, '---',\n                      to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                    mr.app_seq_num        app_seq_num\n          from      mif m,\n                    merchant mr\n          where     mr.merc_cntrl_number =  :1  and\n                    mr.merch_number = m.merchant_number and\n                    m.bank_number     =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setLong(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1311^9*/
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
        }
        
        rs.close();
        it.close();
        
        // next check App Seq Num
        /*@lineinfo:generated-code*//*@lineinfo:1324^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                        index (m pkmif) 
//                        index (mr xpkmerchant)  
//                     */
//                      m.merchant_number     merchant_number,
//                      m.dba_name            dba_name,
//                      m.dmagent             dmagent,
//                      m.bank_number         bank_number,
//                      mr.merc_cntrl_number  merc_cntrl_number,
//                      nvl(m.dmacctst,' ')   status,
//                      decode(m.activation_date,
//                        null, '---',
//                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                      decode(m.last_deposit_date,
//                        null, '---',
//                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                      mr.app_seq_num        app_seq_num
//            from      mif m,
//                      merchant mr
//            where     mr.app_seq_num = :longLookup and
//                      mr.merch_number = m.merchant_number and
//                      m.bank_number = :bankNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                      index (m pkmif) \n                      index (mr xpkmerchant)  \n                   */\n                    m.merchant_number     merchant_number,\n                    m.dba_name            dba_name,\n                    m.dmagent             dmagent,\n                    m.bank_number         bank_number,\n                    mr.merc_cntrl_number  merc_cntrl_number,\n                    nvl(m.dmacctst,' ')   status,\n                    decode(m.activation_date,\n                      null, '---',\n                      to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                    decode(m.last_deposit_date,\n                      null, '---',\n                      to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                    mr.app_seq_num        app_seq_num\n          from      mif m,\n                    merchant mr\n          where     mr.app_seq_num =  :1  and\n                    mr.merch_number = m.merchant_number and\n                    m.bank_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setLong(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1360^9*/
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
        }
        
        rs.close();
        it.close();
      }
      
      // user is looking for a string -- check dba name
      /*@lineinfo:generated-code*//*@lineinfo:1374^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                      index (m idx_mif_dba_name) 
//                      index (mr idx_merchant_merch_number) 
//                   */
//                    m.merchant_number     merchant_number,
//                    m.dba_name            dba_name,
//                    m.dmagent             dmagent,
//                    m.bank_number         bank_number,
//                    mr.merc_cntrl_number  merc_cntrl_number,
//                    nvl(m.dmacctst,' ')   status,
//                    decode(m.activation_date,
//                      null, '---',
//                      to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                    decode(m.last_deposit_date,
//                      null, '---',
//                      to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                    decode(m.date_opened,
//                      null, '---',
//                      to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                    decode(m.dmacctst,
//                      null, '---',
//                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                    decode(m.date_opened,
//                      null, '---',
//                      to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                    decode(m.dmacctst,
//                      null, '---',
//                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                    mr.app_seq_num        app_seq_num
//          from      mif m,
//                    merchant mr
//          where     m.dba_name like :stringLookup and
//                    m.bank_number     = :bankNumber and
//                    m.merchant_number = mr.merch_number(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                    index (m idx_mif_dba_name) \n                    index (mr idx_merchant_merch_number) \n                 */\n                  m.merchant_number     merchant_number,\n                  m.dba_name            dba_name,\n                  m.dmagent             dmagent,\n                  m.bank_number         bank_number,\n                  mr.merc_cntrl_number  merc_cntrl_number,\n                  nvl(m.dmacctst,' ')   status,\n                  decode(m.activation_date,\n                    null, '---',\n                    to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                  decode(m.last_deposit_date,\n                    null, '---',\n                    to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                  decode(m.date_opened,\n                    null, '---',\n                    to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                  decode(m.dmacctst,\n                    null, '---',\n                    m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                  decode(m.date_opened,\n                    null, '---',\n                    to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                  decode(m.dmacctst,\n                    null, '---',\n                    m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                  mr.app_seq_num        app_seq_num\n        from      mif m,\n                  merchant mr\n        where     m.dba_name like  :1  and\n                  m.bank_number     =  :2  and\n                  m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,stringLookup);
   __sJT_st.setLong(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1410^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getRecsFromBankNode(" + stringLookup + ", " + longLookup + ", " + bankNumber + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  /*
  ** METHOD getRecsFromUserNode
  **
  ** Performs index-enhanced queries from a user node perspective
  */
  private void getRecsFromUserNode(String stringLookup, long longLookup, long userNode, String numberString)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    try
    {
      if(longLookup > -1)  // user is searching for a number
      {
        // first check merchant number
        if(useAppFilter)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1450^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif)
//                          index (mr idx_merchant_merch_number)
//                          index (th idx_ancestor)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif                 m,
//                        merchant            mr,
//                        t_hierarchy         th,
//                        group_rep_merchant  grm
//              where     m.merchant_number = :longLookup and
//                        m.merchant_number = grm.merchant_number and
//                        grm.user_id = :userId and
//                        th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                        th.ancestor = :userNode and
//                        th.descendent = to_number(m.bank_number||m.dmagent) and
//                        m.merchant_number = mr.merch_number(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif)\n                        index (mr idx_merchant_merch_number)\n                        index (th idx_ancestor)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif                 m,\n                      merchant            mr,\n                      t_hierarchy         th,\n                      group_rep_merchant  grm\n            where     m.merchant_number =  :1  and\n                      m.merchant_number = grm.merchant_number and\n                      grm.user_id =  :2  and\n                      th.hier_type =  :3  and\n                      th.ancestor =  :4  and\n                      th.descendent = to_number(m.bank_number||m.dmagent) and\n                      m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setLong(2,userId);
   __sJT_st.setInt(3,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(4,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1493^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:1497^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif)
//                          index (mr idx_merchant_merch_number)
//                          index (th idx_ancestor)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif m,
//                        merchant mr,
//                        t_hierarchy th
//              where     m.merchant_number = :longLookup and
//                        th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                        th.ancestor = :userNode and
//                        th.descendent = to_number(m.bank_number||m.dmagent) and
//                        m.merchant_number = mr.merch_number(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif)\n                        index (mr idx_merchant_merch_number)\n                        index (th idx_ancestor)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif m,\n                      merchant mr,\n                      t_hierarchy th\n            where     m.merchant_number =  :1  and\n                      th.hier_type =  :2  and\n                      th.ancestor =  :3  and\n                      th.descendent = to_number(m.bank_number||m.dmagent) and\n                      m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(3,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1537^11*/
        }
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
        }
        
        rs.close();
        it.close();
        
        // next check association
        if(useAppFilter)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1553^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif)
//                          index (mr idx_merchant_merch_number)
//                          index (th idx_ancestor)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif                 m,
//                        merchant            mr,
//                        t_hierarchy         th,
//                        group_rep_merchant  grm
//              where     m.dmagent = :numberString and
//                        m.merchant_number = grm.merchant_number and
//                        grm.user_id = :userId and
//                        th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                        th.ancestor = :userNode and
//                        th.descendent = to_number(m.bank_number||m.dmagent) and
//                        m.merchant_number = mr.merch_number(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif)\n                        index (mr idx_merchant_merch_number)\n                        index (th idx_ancestor)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif                 m,\n                      merchant            mr,\n                      t_hierarchy         th,\n                      group_rep_merchant  grm\n            where     m.dmagent =  :1  and\n                      m.merchant_number = grm.merchant_number and\n                      grm.user_id =  :2  and\n                      th.hier_type =  :3  and\n                      th.ancestor =  :4  and\n                      th.descendent = to_number(m.bank_number||m.dmagent) and\n                      m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,numberString);
   __sJT_st.setLong(2,userId);
   __sJT_st.setInt(3,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(4,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1596^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:1600^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m idx_mif_dmagent))
//                          index (mr idx_merchant_merch_number)
//                          index (th idx_ancestor)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif m,
//                        merchant mr,
//                        t_hierarchy th
//              where     m.dmagent = :numberString and
//                        th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                        th.ancestor = :userNode and
//                        th.descendent = to_number(m.bank_number||m.dmagent) and
//                        m.merchant_number = mr.merch_number(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m idx_mif_dmagent))\n                        index (mr idx_merchant_merch_number)\n                        index (th idx_ancestor)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif m,\n                      merchant mr,\n                      t_hierarchy th\n            where     m.dmagent =  :1  and\n                      th.hier_type =  :2  and\n                      th.ancestor =  :3  and\n                      th.descendent = to_number(m.bank_number||m.dmagent) and\n                      m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,numberString);
   __sJT_st.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(3,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1640^11*/
        }
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
        }
        
        rs.close();
        it.close();
        
        // next check phone number 
        if(useAppFilter)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1656^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif)
//                          index (mr idx_merchant_merch_number)
//                          index (th idx_ancestor)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif                 m,
//                        merchant            mr,
//                        t_hierarchy         th,
//                        group_rep_merchant  grm
//              where     m.phone_1 = :numberString and
//                        m.merchant_number = grm.merchant_number and
//                        grm.user_id = :userId and
//                        th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                        th.ancestor = :userNode and
//                        th.descendent = to_number(m.bank_number||m.dmagent) and
//                        m.merchant_number = mr.merch_number(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif)\n                        index (mr idx_merchant_merch_number)\n                        index (th idx_ancestor)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif                 m,\n                      merchant            mr,\n                      t_hierarchy         th,\n                      group_rep_merchant  grm\n            where     m.phone_1 =  :1  and\n                      m.merchant_number = grm.merchant_number and\n                      grm.user_id =  :2  and\n                      th.hier_type =  :3  and\n                      th.ancestor =  :4  and\n                      th.descendent = to_number(m.bank_number||m.dmagent) and\n                      m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,numberString);
   __sJT_st.setLong(2,userId);
   __sJT_st.setInt(3,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(4,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1699^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:1703^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m idx_mif_phone1) 
//                          index (mr idx_merchant_merch_number) 
//                          index (th idx_ancestor)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif m,
//                        merchant mr,
//                        t_hierarchy th
//              where     m.phone_1 = :numberString and
//                        th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                        th.ancestor = :userNode and
//                        th.descendent = to_number(m.bank_number||m.dmagent) and
//                        m.merchant_number = mr.merch_number(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m idx_mif_phone1) \n                        index (mr idx_merchant_merch_number) \n                        index (th idx_ancestor)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif m,\n                      merchant mr,\n                      t_hierarchy th\n            where     m.phone_1 =  :1  and\n                      th.hier_type =  :2  and\n                      th.ancestor =  :3  and\n                      th.descendent = to_number(m.bank_number||m.dmagent) and\n                      m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,numberString);
   __sJT_st.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(3,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"23com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1743^11*/
        }
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
        }
        
        rs.close();
        it.close();

        // next check control number
        if(useAppFilter)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1759^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif) 
//                          index (mr idx_merchant_control_number)  
//                          index (th idx_ancestor)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif                 m,
//                        merchant            mr,
//                        t_hierarchy         th,
//                        group_rep_merchant  grm
//              where     mr.merc_cntrl_number = :longLookup and
//                        mr.merch_number = m.merchant_number and
//                        mr.merch_number = grm.merchant_number and
//                        grm.user_id = :userId and
//                        th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                        th.ancestor = :userNode and
//                        th.descendent = to_number(m.bank_number||m.dmagent)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif) \n                        index (mr idx_merchant_control_number)  \n                        index (th idx_ancestor)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif                 m,\n                      merchant            mr,\n                      t_hierarchy         th,\n                      group_rep_merchant  grm\n            where     mr.merc_cntrl_number =  :1  and\n                      mr.merch_number = m.merchant_number and\n                      mr.merch_number = grm.merchant_number and\n                      grm.user_id =  :2  and\n                      th.hier_type =  :3  and\n                      th.ancestor =  :4  and\n                      th.descendent = to_number(m.bank_number||m.dmagent)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setLong(2,userId);
   __sJT_st.setInt(3,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(4,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"24com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1802^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:1806^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif) 
//                          index (mr idx_merchant_control_number)  
//                          index (th idx_ancestor)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif m,
//                        merchant mr,
//                        t_hierarchy th
//              where     mr.merc_cntrl_number = :longLookup and
//                        mr.merch_number = m.merchant_number and
//                        th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                        th.ancestor = :userNode and
//                        th.descendent = to_number(m.bank_number||m.dmagent)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif) \n                        index (mr idx_merchant_control_number)  \n                        index (th idx_ancestor)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif m,\n                      merchant mr,\n                      t_hierarchy th\n            where     mr.merc_cntrl_number =  :1  and\n                      mr.merch_number = m.merchant_number and\n                      th.hier_type =  :2  and\n                      th.ancestor =  :3  and\n                      th.descendent = to_number(m.bank_number||m.dmagent)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(3,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"25com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1846^11*/
        }
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
        }
        
        rs.close();
        it.close();
        
        // next check App Seq Num
        if(useAppFilter)
        {
          /*@lineinfo:generated-code*//*@lineinfo:1862^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif) 
//                          index (mr xpkmerchant)  
//                          index (th idx_ancestor)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif                 m,
//                        merchant            mr,
//                        t_hierarchy         th,
//                        group_rep_merchant  grm
//              where     mr.app_seq_num = :longLookup and
//                        mr.merch_number = m.merchant_number and
//                        mr.merch_number = grm.merchant_number and
//                        grm.user_id = :userId and
//                        th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                        th.ancestor = :userNode and
//                        th.descendent = to_number(m.bank_number||m.dmagent)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif) \n                        index (mr xpkmerchant)  \n                        index (th idx_ancestor)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif                 m,\n                      merchant            mr,\n                      t_hierarchy         th,\n                      group_rep_merchant  grm\n            where     mr.app_seq_num =  :1  and\n                      mr.merch_number = m.merchant_number and\n                      mr.merch_number = grm.merchant_number and\n                      grm.user_id =  :2  and\n                      th.hier_type =  :3  and\n                      th.ancestor =  :4  and\n                      th.descendent = to_number(m.bank_number||m.dmagent)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setLong(2,userId);
   __sJT_st.setInt(3,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(4,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"26com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1905^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:1909^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                          index (m pkmif) 
//                          index (mr xpkmerchant)  
//                          index (th idx_ancestor)
//                       */
//                        m.merchant_number     merchant_number,
//                        m.dba_name            dba_name,
//                        m.dmagent             dmagent,
//                        m.bank_number         bank_number,
//                        mr.merc_cntrl_number  merc_cntrl_number,
//                        nvl(m.dmacctst,' ')   status,
//                        decode(m.activation_date,
//                          null, '---',
//                          to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                        decode(m.last_deposit_date,
//                          null, '---',
//                          to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                        decode(m.date_opened,
//                          null, '---',
//                          to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                        decode(m.dmacctst,
//                          null, '---',
//                          m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                        mr.app_seq_num        app_seq_num
//              from      mif m,
//                        merchant mr,
//                        t_hierarchy th
//              where     mr.app_seq_num = :longLookup and
//                        mr.merch_number = m.merchant_number and
//                        th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                        th.ancestor = :userNode and
//                        th.descendent = to_number(m.bank_number||m.dmagent)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                        index (m pkmif) \n                        index (mr xpkmerchant)  \n                        index (th idx_ancestor)\n                     */\n                      m.merchant_number     merchant_number,\n                      m.dba_name            dba_name,\n                      m.dmagent             dmagent,\n                      m.bank_number         bank_number,\n                      mr.merc_cntrl_number  merc_cntrl_number,\n                      nvl(m.dmacctst,' ')   status,\n                      decode(m.activation_date,\n                        null, '---',\n                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                      decode(m.last_deposit_date,\n                        null, '---',\n                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                      decode(m.date_opened,\n                        null, '---',\n                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                      decode(m.dmacctst,\n                        null, '---',\n                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                      mr.app_seq_num        app_seq_num\n            from      mif m,\n                      merchant mr,\n                      t_hierarchy th\n            where     mr.app_seq_num =  :1  and\n                      mr.merch_number = m.merchant_number and\n                      th.hier_type =  :2  and\n                      th.ancestor =  :3  and\n                      th.descendent = to_number(m.bank_number||m.dmagent)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(3,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"27com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1949^11*/
        }
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
        }
        
        rs.close();
        it.close();
      }
      
      // user is looking for a string -- check dba name
      if(useAppFilter)
      {
        /*@lineinfo:generated-code*//*@lineinfo:1966^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                        index (m idx_mif_dba_name) 
//                        index (mr idx_merchant_merch_number) 
//                        index (th idx_ancestor)
//                     */
//                      m.merchant_number     merchant_number,
//                      m.dba_name            dba_name,
//                      m.dmagent             dmagent,
//                      m.bank_number         bank_number,
//                      mr.merc_cntrl_number  merc_cntrl_number,
//                      nvl(m.dmacctst,' ')   status,
//                      decode(m.activation_date,
//                        null, '---',
//                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                      decode(m.last_deposit_date,
//                        null, '---',
//                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                      mr.app_seq_num        app_seq_num
//            from      mif                 m,
//                      merchant            mr,
//                      t_hierarchy         th,
//                      group_rep_merchant  grm
//            where     m.dba_name like :stringLookup and
//                      m.merchant_number = grm.merchant_number and
//                      grm.user_id = :userId and
//                      th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                      th.ancestor = :userNode and
//                      th.descendent = to_number(m.bank_number||m.dmagent) and
//                      m.merchant_number = mr.merch_number(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                      index (m idx_mif_dba_name) \n                      index (mr idx_merchant_merch_number) \n                      index (th idx_ancestor)\n                   */\n                    m.merchant_number     merchant_number,\n                    m.dba_name            dba_name,\n                    m.dmagent             dmagent,\n                    m.bank_number         bank_number,\n                    mr.merc_cntrl_number  merc_cntrl_number,\n                    nvl(m.dmacctst,' ')   status,\n                    decode(m.activation_date,\n                      null, '---',\n                      to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                    decode(m.last_deposit_date,\n                      null, '---',\n                      to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                    mr.app_seq_num        app_seq_num\n          from      mif                 m,\n                    merchant            mr,\n                    t_hierarchy         th,\n                    group_rep_merchant  grm\n          where     m.dba_name like  :1  and\n                    m.merchant_number = grm.merchant_number and\n                    grm.user_id =  :2  and\n                    th.hier_type =  :3  and\n                    th.ancestor =  :4  and\n                    th.descendent = to_number(m.bank_number||m.dmagent) and\n                    m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,stringLookup);
   __sJT_st.setLong(2,userId);
   __sJT_st.setInt(3,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(4,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"28com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2009^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:2013^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                        index (m idx_mif_dba_name) 
//                        index (mr idx_merchant_merch_number) 
//                        index (th idx_ancestor)
//                     */
//                      m.merchant_number     merchant_number,
//                      m.dba_name            dba_name,
//                      m.dmagent             dmagent,
//                      m.bank_number         bank_number,
//                      mr.merc_cntrl_number  merc_cntrl_number,
//                      nvl(m.dmacctst,' ')   status,
//                      decode(m.activation_date,
//                        null, '---',
//                        to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,
//                      decode(m.last_deposit_date,
//                        null, '---',
//                        to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,
//                      decode(m.date_opened,
//                        null, '---',
//                        to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,
//                      decode(m.dmacctst,
//                        null, '---',
//                        m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,
//                      mr.app_seq_num        app_seq_num
//            from      mif m,
//                      merchant mr,
//                      t_hierarchy th
//            where     m.dba_name like :stringLookup and
//                      th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                      th.ancestor = :userNode and
//                      th.descendent = to_number(m.bank_number||m.dmagent) and
//                      m.merchant_number = mr.merch_number(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                      index (m idx_mif_dba_name) \n                      index (mr idx_merchant_merch_number) \n                      index (th idx_ancestor)\n                   */\n                    m.merchant_number     merchant_number,\n                    m.dba_name            dba_name,\n                    m.dmagent             dmagent,\n                    m.bank_number         bank_number,\n                    mr.merc_cntrl_number  merc_cntrl_number,\n                    nvl(m.dmacctst,' ')   status,\n                    decode(m.activation_date,\n                      null, '---',\n                      to_char(m.activation_date, 'mm/dd/yyyy'))   activation_date,\n                    decode(m.last_deposit_date,\n                      null, '---',\n                      to_char(m.last_deposit_date, 'mm/dd/yyyy')) last_activity,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'mm/dd/yyyy')) date_opened,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'mm/dd/yyyy')||')')  account_status,\n                    decode(m.date_opened,\n                      null, '---',\n                      to_char(mif_date_opened(m.date_opened), 'yyyymmdd')) date_opened_compare,\n                    decode(m.dmacctst,\n                      null, '---',\n                      m.dmacctst||' ('||to_char(m.date_stat_chgd_to_dcb,'yyyymmdd')||')') account_status_compare,\n                    mr.app_seq_num        app_seq_num\n          from      mif m,\n                    merchant mr,\n                    t_hierarchy th\n          where     m.dba_name like  :1  and\n                    th.hier_type =  :2  and\n                    th.ancestor =  :3  and\n                    th.descendent = to_number(m.bank_number||m.dmagent) and\n                    m.merchant_number = mr.merch_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,stringLookup);
   __sJT_st.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(3,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"29com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2053^9*/
      }
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_MERCHANT));
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getRecsFromUserNode(" + stringLookup + ", " + longLookup + ", " + userNode + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
    
  /*
  ** METHOD getMerchantData
  **
  ** Gets all merchants that satisfy the criteria and places them in the
  ** lookupResults Vector
  */
  private void getMerchantData(String stringLookup, long longLookup, long userNode, String numberString)
  {
    try
    {
      // determine if this is a bank-level node
      int entityType      = 0;
      long bankNumber     = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:2091^7*/

//  ************************************************************
//  #sql [Ctx] { select  entity_type 
//          
//          from    t_hierarchy
//          where   hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                  ancestor = :userNode and
//                  descendent = :userNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  entity_type \n         \n        from    t_hierarchy\n        where   hier_type =  :1  and\n                ancestor =  :2  and\n                descendent =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.maintenance.AccountLookup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(2,userNode);
   __sJT_st.setLong(3,userNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   entityType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2099^7*/
      
      if(entityType == MesHierarchy.ET_BANK_NODE)
      {
        // get the bank number from the user node
        bankNumber = userNode / 100000L;
      }
      else 
      {
        bankNumber = userNode / 1000000L;
      }
      
      if(entityType == MesHierarchy.ET_SUPER_NODE)
      {
        getRecsFromSuperNode(stringLookup, longLookup, numberString, userNode);
      }
      else if(entityType == MesHierarchy.ET_BANK_NODE)
      {
        getRecsFromBankNode(stringLookup, longLookup, bankNumber, numberString);
      }
      else
      {
        getRecsFromUserNode(stringLookup, longLookup, userNode, numberString);
      }
    }
    catch(Exception e)
    {
      logEntry("getMerchantData(" + stringLookup + ", " + longLookup + ", " + userNode + ")", e.toString());
    }
  }
  
  /*
  ** METHOD getGroupData
  **
  ** Gets all groups that satisfy the criteria and places them in the 
  ** lookupResults Vector
  */
  private void getGroupData(String stringLookup, long longLookup, long userNode, String numberString)
  {
    ResultSetIterator it    = null;
    ResultSet         rs    = null;
    
    try
    {
      if(userNode == 9999999999L)
      {
        if(useAppFilter)
        {
          // only show allow orgs that include merchants this user has rights to
          /*@lineinfo:generated-code*//*@lineinfo:2148^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_group                   as merchant_number,
//                      o.org_name                    as dba_name,
//                      o.org_num                     as org_num,
//                      trunc(o.org_group / 1000000)  as bank_number,
//                      mod(o.org_group, 1000000)     as dmagent,
//                      'GROUP/ASSN'                  as merc_cntrl_number,
//                      ''                            as app_seq_num,
//                      ''                            as status,
//                      ''                            as activation_date,
//                      ''                            as last_activity,
//                      ''                            as account_status,
//                      ''                            as date_opened,
//                      '---'                         as account_status_compare,
//                      '---'                         as date_opened_compare
//              from    organization        o,
//                      group_merchant      gm,
//                      group_rep_merchant  grm
//              where   o.org_type_code <> 'm' and
//                      (
//                        o.org_group           = :longLookup or
//                        upper(o.org_name) like  :stringLookup or
//                        mod(o.org_group, 1000000) = :longLookup or
//                        trunc(o.org_group/1000000) = :longLookup
//                      ) and
//                      o.org_num = gm.org_num and
//                      gm.merchant_number = grm.merchant_number and
//                      grm.user_id = :userId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_group                   as merchant_number,\n                    o.org_name                    as dba_name,\n                    o.org_num                     as org_num,\n                    trunc(o.org_group / 1000000)  as bank_number,\n                    mod(o.org_group, 1000000)     as dmagent,\n                    'GROUP/ASSN'                  as merc_cntrl_number,\n                    ''                            as app_seq_num,\n                    ''                            as status,\n                    ''                            as activation_date,\n                    ''                            as last_activity,\n                    ''                            as account_status,\n                    ''                            as date_opened,\n                    '---'                         as account_status_compare,\n                    '---'                         as date_opened_compare\n            from    organization        o,\n                    group_merchant      gm,\n                    group_rep_merchant  grm\n            where   o.org_type_code <> 'm' and\n                    (\n                      o.org_group           =  :1  or\n                      upper(o.org_name) like   :2  or\n                      mod(o.org_group, 1000000) =  :3  or\n                      trunc(o.org_group/1000000) =  :4 \n                    ) and\n                    o.org_num = gm.org_num and\n                    gm.merchant_number = grm.merchant_number and\n                    grm.user_id =  :5";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setString(2,stringLookup);
   __sJT_st.setLong(3,longLookup);
   __sJT_st.setLong(4,longLookup);
   __sJT_st.setLong(5,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"31com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2177^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:2181^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_group                   as merchant_number,
//                      o.org_name                    as dba_name,
//                      o.org_num                     as org_num,
//                      trunc(o.org_group / 1000000)  as bank_number,
//                      mod(o.org_group, 1000000)     as dmagent,
//                      'GROUP/ASSN'                  as merc_cntrl_number,
//                      ''                            as app_seq_num,
//                      ''                            as status,
//                      ''                            as activation_date,
//                      ''                            as last_activity,
//                      ''                            as account_status,
//                      ''                            as date_opened,
//                      '---'                         as account_status_compare,
//                      '---'                         as date_opened_compare
//              from    organization  o,
//                      t_hierarchy th,
//                      mes_allowed_bank_numbers mabn
//              where   o.org_type_code <> 'm' and
//                      (
//                        o.org_group           = :longLookup or
//                        upper(o.org_name) like  :stringLookup or
//                        mod(o.org_group, 1000000) = :longLookup or
//                        trunc(o.org_group/1000000) = :longLookup
//                      ) and
//                      trunc(o.org_group/1000000) = mabn.bank_number and
//                      o.org_group = th.descendent and
//                      th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                      th.ancestor = :userNode          
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_group                   as merchant_number,\n                    o.org_name                    as dba_name,\n                    o.org_num                     as org_num,\n                    trunc(o.org_group / 1000000)  as bank_number,\n                    mod(o.org_group, 1000000)     as dmagent,\n                    'GROUP/ASSN'                  as merc_cntrl_number,\n                    ''                            as app_seq_num,\n                    ''                            as status,\n                    ''                            as activation_date,\n                    ''                            as last_activity,\n                    ''                            as account_status,\n                    ''                            as date_opened,\n                    '---'                         as account_status_compare,\n                    '---'                         as date_opened_compare\n            from    organization  o,\n                    t_hierarchy th,\n                    mes_allowed_bank_numbers mabn\n            where   o.org_type_code <> 'm' and\n                    (\n                      o.org_group           =  :1  or\n                      upper(o.org_name) like   :2  or\n                      mod(o.org_group, 1000000) =  :3  or\n                      trunc(o.org_group/1000000) =  :4 \n                    ) and\n                    trunc(o.org_group/1000000) = mabn.bank_number and\n                    o.org_group = th.descendent and\n                    th.hier_type =  :5  and\n                    th.ancestor =  :6";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setString(2,stringLookup);
   __sJT_st.setLong(3,longLookup);
   __sJT_st.setLong(4,longLookup);
   __sJT_st.setInt(5,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(6,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"32com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2211^11*/
        }
      }
      else
      {
        if(useAppFilter)
        {
          /*@lineinfo:generated-code*//*@lineinfo:2218^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_group                   as merchant_number,
//                      o.org_name                    as dba_name,
//                      o.org_num                     as org_num,
//                      trunc(o.org_group / 1000000)  as bank_number,
//                      mod(o.org_group, 1000000)     as dmagent,
//                      'GROUP/ASSN'                  as merc_cntrl_number,
//                      ''                            as app_seq_num,
//                      ''                            as status,
//                      ''                            as activation_date,
//                      ''                            as last_activity,
//                      ''                            as account_status,
//                      ''                            as date_opened,
//                      '---'                         as account_status_compare,
//                      '---'                         as date_opened_compare
//              from    organization        o,
//                      t_hierarchy         th,
//                      group_merchant      gm,
//                      group_rep_merchant  grm
//              where   o.org_type_code <> 'm' and
//                      (
//                        o.org_group           = :longLookup or
//                        upper(o.org_name) like  :stringLookup or
//                        mod(o.org_group, 1000000) = :longLookup or
//                        trunc(o.org_group/1000000) = :longLookup
//                      ) and
//                      th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                      th.ancestor = :userNode and
//                      th.descendent = o.org_group and
//                      o.org_num = gm.org_num and
//                      gm.merchant_number = grm.merchant_number and
//                      grm.user_id = :userId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_group                   as merchant_number,\n                    o.org_name                    as dba_name,\n                    o.org_num                     as org_num,\n                    trunc(o.org_group / 1000000)  as bank_number,\n                    mod(o.org_group, 1000000)     as dmagent,\n                    'GROUP/ASSN'                  as merc_cntrl_number,\n                    ''                            as app_seq_num,\n                    ''                            as status,\n                    ''                            as activation_date,\n                    ''                            as last_activity,\n                    ''                            as account_status,\n                    ''                            as date_opened,\n                    '---'                         as account_status_compare,\n                    '---'                         as date_opened_compare\n            from    organization        o,\n                    t_hierarchy         th,\n                    group_merchant      gm,\n                    group_rep_merchant  grm\n            where   o.org_type_code <> 'm' and\n                    (\n                      o.org_group           =  :1  or\n                      upper(o.org_name) like   :2  or\n                      mod(o.org_group, 1000000) =  :3  or\n                      trunc(o.org_group/1000000) =  :4 \n                    ) and\n                    th.hier_type =  :5  and\n                    th.ancestor =  :6  and\n                    th.descendent = o.org_group and\n                    o.org_num = gm.org_num and\n                    gm.merchant_number = grm.merchant_number and\n                    grm.user_id =  :7";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setString(2,stringLookup);
   __sJT_st.setLong(3,longLookup);
   __sJT_st.setLong(4,longLookup);
   __sJT_st.setInt(5,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(6,userNode);
   __sJT_st.setLong(7,userId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"33com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2251^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:2255^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_group                   as merchant_number,
//                      o.org_name                    as dba_name,
//                      o.org_num                     as org_num,
//                      trunc(o.org_group / 1000000)  as bank_number,
//                      mod(o.org_group, 1000000)     as dmagent,
//                      'GROUP/ASSN'                  as merc_cntrl_number,
//                      ''                            as app_seq_num,
//                      ''                            as status,
//                      ''                            as activation_date,
//                      ''                            as last_activity,
//                      ''                            as account_status,
//                      ''                            as date_opened,
//                      '---'                         as account_status_compare,
//                      '---'                         as date_opened_compare
//              from    organization        o,
//                      t_hierarchy         th
//              where   o.org_type_code <> 'm' and
//                      (
//                        o.org_group           = :longLookup or
//                        upper(o.org_name) like  :stringLookup or
//                        mod(o.org_group, 1000000) = :longLookup or
//                        trunc(o.org_group/1000000) = :longLookup
//                      ) and
//                      th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                      th.ancestor = :userNode and
//                      th.descendent = o.org_group
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_group                   as merchant_number,\n                    o.org_name                    as dba_name,\n                    o.org_num                     as org_num,\n                    trunc(o.org_group / 1000000)  as bank_number,\n                    mod(o.org_group, 1000000)     as dmagent,\n                    'GROUP/ASSN'                  as merc_cntrl_number,\n                    ''                            as app_seq_num,\n                    ''                            as status,\n                    ''                            as activation_date,\n                    ''                            as last_activity,\n                    ''                            as account_status,\n                    ''                            as date_opened,\n                    '---'                         as account_status_compare,\n                    '---'                         as date_opened_compare\n            from    organization        o,\n                    t_hierarchy         th\n            where   o.org_type_code <> 'm' and\n                    (\n                      o.org_group           =  :1  or\n                      upper(o.org_name) like   :2  or\n                      mod(o.org_group, 1000000) =  :3  or\n                      trunc(o.org_group/1000000) =  :4 \n                    ) and\n                    th.hier_type =  :5  and\n                    th.ancestor =  :6  and\n                    th.descendent = o.org_group";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.maintenance.AccountLookup",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,longLookup);
   __sJT_st.setString(2,stringLookup);
   __sJT_st.setLong(3,longLookup);
   __sJT_st.setLong(4,longLookup);
   __sJT_st.setInt(5,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(6,userNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"34com.mes.maintenance.AccountLookup",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2283^11*/
        }
      }
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        lookupResults.add(new MerchantData(rs, MerchantData.REC_TYPE_GROUP));
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getGroupData(" + stringLookup + ", " + longLookup + ", " + userNode + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  /*
  ** METHOD public void getData()
  **
  ** Gets a result set iterator containing all merchant account records that
  ** match the lookup value.  The lookup value is matched against control 
  ** numbers, merchant numbers, sequence numbers and dba names.  Anything that
  ** matches is included.  
  */
  public synchronized void getData(UserBean  ub)
  {
    long    userNode  = 0L;
    
    try
    {
      connect();
      
      // set up parameters for App Filter search if the user has a special
      // report hierarchy node
      if(ub.getReportHierarchyNode() > 0)
      {
        userNode      = ub.getReportHierarchyNode();
        useAppFilter  = true;
        userId        = ub.getUserId();
      }
      else
      {
        userNode      = ub.getHierarchyNode();
        useAppFilter  = false;
        userId        = 0L;
      }
      
      if (lookupValue != null)
      {
        if(! lookupValue.equals(lastLookupValue))
        {
          long longLookup;
          try
          {
            longLookup = Long.parseLong(lookupValue.trim());
          }
          catch (Exception e) 
          {
            longLookup = -1;
          }
        
          String stringLookup = "%" + lookupValue.toUpperCase() + "%";
          
          lookupResults.clear();
          
          // get merchants
          getMerchantData(stringLookup, longLookup, userNode, lookupValue);
          
          // get groups/associations
          getGroupData(stringLookup, longLookup, userNode, lookupValue);
          
          lastLookupValue = lookupValue;
        }
      
        // now create the sorted tree
        if(sortedResults != null)
        {
          sortedResults.clear();
        }
        
        sortedResults = null;
      
        sortedResults = new TreeSet(mdc);
        sortedResults.addAll(lookupResults);
      }
    }
    catch(Exception e)
    {
      logEntry("getData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*
  ** METHOD getSortedResults
  **
  ** Returns an iterator containing the sorted results of the query
  */
  public synchronized Iterator getSortedResults()
  {
    Iterator result = null;
    if(sortedResults != null)
    {
      result = sortedResults.iterator();
    }
    
    return result;
  }
  
  /*
  ** METHOD getActionDescription
  **
  ** Returns the description of the current action
  */
  public synchronized String getActionDescription()
  {
    return this.actionDescription;
  }
  
  /*
  ** METHOD getActionURL
  **
  ** Returns the URL for the current action
  */
  public synchronized String getActionURL()
  {
    return this.actionURL;
  }
  
  /*
  ** ACCESSORS
  */
  public synchronized String getLookupValue()
  {
    return lookupValue;
  }
  public synchronized void setLookupValue(String newLookupValue)
  {
    lookupValue = newLookupValue;
  }
  
  public synchronized int getAction()
  {
    return this.action;
  }
  public synchronized void setAction(int action)
  {
    if(this.action != action)
    {
      // assign instance variables related to the action type
      this.action = action;
    
      try
      {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:2450^9*/

//  ************************************************************
//  #sql [Ctx] { select  description, 
//                    url         
//            
//            from    lookup_actions
//            where   action = :action
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  description, \n                  url         \n           \n          from    lookup_actions\n          where   action =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"35com.mes.maintenance.AccountLookup",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,action);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   this.actionDescription = (String)__sJT_rs.getString(1);
   this.actionURL = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2458^9*/
      }
      catch(Exception e)
      {
        this.action = mesConstants.LU_ACTION_INVALID;
        this.actionDescription = "Invalid Action: " + action;
        this.actionURL = "/jsp/maintenance/lookup_account.jsp";
        logEntry("setAction(" + action + ")", e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
  }
  public synchronized void setAction(String action)
  {
    int       actionNum;
    try
    {
      actionNum = Integer.parseInt(action);
    }
    catch(Exception e)
    {
      actionNum = mesConstants.LU_ACTION_INVALID;
    }
    
    setAction(actionNum);
  }
  
  public synchronized void setSortBy(String sortBy)
  {
    try
    {
      mdc.setSortBy(Integer.parseInt(sortBy));
    }
    catch(Exception e)
    {
      logEntry("setSortBy(" + sortBy + ")", e.toString());
    }
  }
}/*@lineinfo:generated-code*/