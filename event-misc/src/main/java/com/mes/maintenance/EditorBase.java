/*@lineinfo:filename=EditorBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/EditorBase.sqlj $

  Description:  
  
    Support bean for viewing/editing risk scoring criteria
    
  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 6/09/04 4:44p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.HierarchyNodeField;
import com.mes.forms.NumberField;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import com.mes.user.Permissions;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

abstract public class EditorBase extends SQLJConnectionBase
{
  public static final int               REC_NEW             = 0;
  
  public static final int               EID_NONE            = 0;
  public static final int               EID_RISK_SCORE      = 0;
  
  public static class NameMap extends HashMap
  {
    public String getFieldName( String colName )
    {
      return((String)this.get((Object)colName));
    }
  }
  
  public static class Router extends SQLJConnectionBase
  {
    private   String        BeanName          = null;
    private   String        EditorDesc        = null;
    private   String        Rights            = null;
    
    public Router(HttpServletRequest request)
    {
      int     editId      = HttpHelper.getInt(request,"editorId",EID_NONE);
      try
      {
        if ( editId != EID_NONE )
        {
          connect();
          /*@lineinfo:generated-code*//*@lineinfo:83^11*/

//  ************************************************************
//  #sql [Ctx] { select  ed.bean_name,
//                      ed.rights,
//                      ed.editor_desc     
//              from    editors       ed
//              where   ed.editor_id = :editId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ed.bean_name,\n                    ed.rights,\n                    ed.editor_desc      \n            from    editors       ed\n            where   ed.editor_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.EditorBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,editId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   BeanName = (String)__sJT_rs.getString(1);
   Rights = (String)__sJT_rs.getString(2);
   EditorDesc = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:93^11*/
        }
      }
      catch( java.sql.SQLException e )
      {
        logEntry( "Router()", e.toString() );
      }
      finally
      {
        cleanUp();
      }
    }
    
    public String getBeanName()
    {
      return(BeanName);
    }
    
    public String getEditorDesc()
    {
      return(EditorDesc);
    }
    
    public void setPermissions( Permissions perms )
    {
      long                  rightId   = 0L;
      StringTokenizer       tokenizer = null;

      tokenizer = new StringTokenizer( Rights, "," );

      while( tokenizer.hasMoreTokens() )
      {
        try
        {
          perms.allowRight( Long.parseLong(tokenizer.nextToken()) );
        }
        catch(Exception ee)
        {
          continue;
        }
      }
    }
  }
  
  protected FieldGroup          EditorFields            = new FieldGroup("editorFields");
  protected int                 EditorId                = EID_NONE;
  protected long                HierarchyNodeDefault    = 0L;
  protected UserBean            User                    = null;
  
  public EditorBase()
  {
  }
  
  public Vector getEditorFieldsVector( )
  {
    return( EditorFields.getFieldsVector() );
  }
  
  public Field getField( String fieldName )
  {
    return( getField( fieldName, false ) );
  }
  
  public Field getField( String fieldName, boolean ignoreCase )
  {
    return( EditorFields.getField(fieldName,ignoreCase) );
  }
  
  public NameMap getFieldNameMap( )
  {
    ResultSetIterator     it          = null;
    NameMap               map         = null;
    ResultSet             resultSet   = null;
    
    try
    {
      map = new NameMap();
      
      /*@lineinfo:generated-code*//*@lineinfo:171^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  efd.column_name                 as col_name,
//                  ('field' || efd.display_order)  as field_name
//          from    editor_field_defs   efd
//          where   efd.editor_id = 9
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  efd.column_name                 as col_name,\n                ('field' || efd.display_order)  as field_name\n        from    editor_field_defs   efd\n        where   efd.editor_id = 9";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.EditorBase",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.EditorBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:177^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        map.put(resultSet.getString("col_name"),
                resultSet.getString("field_name"));
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getFieldNameMap()",e.toString());
    }        
    finally
    {
      try{it.close();}catch(Exception ee){}
    }
    return( map );
  }
  
  public String getSummaryUrl()
  {
    String      retVal        = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:205^7*/

//  ************************************************************
//  #sql [Ctx] { select  e.summary_jsp 
//          from    editors     e
//          where   e.editor_id = :EditorId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  e.summary_jsp  \n        from    editors     e\n        where   e.editor_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.EditorBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,EditorId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:210^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "getSummaryUrl()",e.toString() );
    }
    return( retVal );
  }
  
  public void initEditorFields( HttpServletRequest request )
  {
    String              className     = null;
    Field               field         = null;
    String              fieldName     = null;
    ResultSetIterator   it            = null;
    String              label         = null;
    String              mask          = null;
    int                 maxLen        = 0;
    boolean             nullAllowed   = false;
    ResultSet           resultSet     = null;
    
    try
    { 
      EditorFields.removeAllFields();
      
      /*@lineinfo:generated-code*//*@lineinfo:235^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  efd.display_order                       as display_order,
//                  efd.table_name                          as table_name,
//                  efd.column_name                         as col_name,
//                  efd.label                               as label,
//                  efd.field_type                          as type,
//                  efd.data_mask                           as data_mask,
//                  efd.lookup_table_name                   as lookup_table,
//                  efd.lookup_column_name                  as lookup_col,
//                  efd.lookup_desc_column_name             as lookup_desc_col,
//                  efd.field_max_size                      as maxlength,
//                  efd.field_decimal_count                 as decimal_count,
//                  decode(efd.field_null_allowed,'Y',1,0)  as null_allowed,
//                  decode(efd.rec_id_column,'Y',1,0)       as rec_id_col,
//                  efd.drop_down_data_class                as drop_down_class                       
//          from    editor_field_defs     efd
//          where   efd.editor_id = :EditorId
//          order by efd.display_order
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  efd.display_order                       as display_order,\n                efd.table_name                          as table_name,\n                efd.column_name                         as col_name,\n                efd.label                               as label,\n                efd.field_type                          as type,\n                efd.data_mask                           as data_mask,\n                efd.lookup_table_name                   as lookup_table,\n                efd.lookup_column_name                  as lookup_col,\n                efd.lookup_desc_column_name             as lookup_desc_col,\n                efd.field_max_size                      as maxlength,\n                efd.field_decimal_count                 as decimal_count,\n                decode(efd.field_null_allowed,'Y',1,0)  as null_allowed,\n                decode(efd.rec_id_column,'Y',1,0)       as rec_id_col,\n                efd.drop_down_data_class                as drop_down_class                       \n        from    editor_field_defs     efd\n        where   efd.editor_id =  :1 \n        order by efd.display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.EditorBase",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,EditorId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.maintenance.EditorBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:254^7*/
      resultSet = it.getResultSet();

      // required field 
      EditorFields.add( new HiddenField("editorId") );
      
      while( resultSet.next() )
      {
        fieldName   = ("field" + resultSet.getInt("display_order"));
        label       = resultSet.getString("label");
        mask        = resultSet.getString("data_mask");
        maxLen      = resultSet.getInt("maxlength");
        nullAllowed = (resultSet.getInt("null_allowed") != 0);
        
        switch( resultSet.getInt("type") )
        {
          case mesConstants.PT_STRING:
            field = new Field(fieldName,label,maxLen,25,nullAllowed);
            break;
            
          case mesConstants.PT_NUMBER:
            field = new NumberField(fieldName,label,maxLen,maxLen,nullAllowed,resultSet.getInt("decimal_count"));
            if ( mask != null )
            {
              ((NumberField)field).setNumberFormat(mask);
            }
            break;
            
          case mesConstants.PT_DATE:
            field = new DateField(fieldName,label,nullAllowed);
            if ( mask != null )
            {
              ((DateField)field).setDateFormat(mask);
            }
            break;
            
          case mesConstants.PT_CURRENCY:
            field = new CurrencyField(fieldName,label,10,10,nullAllowed);
            break;
            
          case mesConstants.PT_HIERARCHY_NODE:
            field = new HierarchyNodeField(Ctx, HierarchyNodeDefault, fieldName, label, nullAllowed);
            break;
            
          case mesConstants.PT_DROP_DOWN_TABLE:
            if ( (className = resultSet.getString("drop_down_class")) == null )
            {
              field = new DropDownField( fieldName, label, new EditorDropDownTable(resultSet), nullAllowed );
            }
            else
            {
              field = new DropDownField( fieldName, label,(DropDownTable)(Class.forName(className).newInstance()), nullAllowed );
            }              
            break;
            
          case mesConstants.PT_HIDDEN:
            field = new HiddenField(fieldName);
            break;
            
          default:
            continue;
        }
        // preset the rec id columns with any valid incoming data
        // this allows for load() to access fields in the field
        // group for data to be used in the where clause
        //
        // See GenericEditor.load() for an example.
        //
        if( resultSet.getInt("rec_id_col") == 1 )
        {
          field.setData( HttpHelper.getString(request,fieldName,"") );
        }
        field.addHtmlExtra("class=\"formFields\"");
        EditorFields.add( field );
      }
      resultSet.close();
      it.close();
      
      // load any necessary record
      // data from the database
      load();
    }
    catch( Exception e )
    {
      logEntry( "initEditorFields()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }
  
  public boolean isValid( )
  {
    return( EditorFields.isValid() );
  }
  
  abstract public void load( );
  
  public void setProperties(HttpServletRequest request, UserBean user)
  {
    Field       field         = null;
    String      name          = null;
  
    try
    {
      // store the user
      User = user;
      
      // register bean with MesSystem
      register(user.getLoginName(), request);
      
      EditorId = HttpHelper.getInt(request,"editorId",EID_NONE);
      
      // store the users default hierarchy node
      /*@lineinfo:generated-code*//*@lineinfo:369^7*/

//  ************************************************************
//  #sql [Ctx] { select  o.org_group 
//          from    organization      o
//          where   o.org_num = :User.getOrgId()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_833 = User.getOrgId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  o.org_group  \n        from    organization      o\n        where   o.org_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.maintenance.EditorBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,__sJT_833);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   HierarchyNodeDefault = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:374^7*/
      
      // instantiate all the editor fields
      initEditorFields(request);
      
      // apply request parameters to the fields
      for (Enumeration names = request.getParameterNames(); 
           names.hasMoreElements(); )
      {
        name  = (String)names.nextElement();
  
        // if the edit field exists, store the value
        field = getField(name);
      
        if ( field != null )
        {
          field.setData(request.getParameter(name));
        }
      }   
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
    }
  }
  
  abstract public void store( );
}/*@lineinfo:generated-code*/