/*@lineinfo:filename=UserChangePasswordBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/UserChangePasswordBean.sqlj $

  Description:

    BankServACHBean

    Facilitates addition of ACH data to the outgoing BankServ ACH File

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-08-06 11:02:28 -0700 (Mon, 06 Aug 2007) $
  Version            : $Revision: 13944 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import com.mes.crypt.MD5;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.ButtonField;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.PasswordField;
import com.mes.forms.Validation;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class UserChangePasswordBean extends FieldBean
{
  private   String  description       = "Change Password";
  private   boolean PasswordChanged   = false;
  
  public UserChangePasswordBean()
  {
  }
  
  public void setProperties(HttpServletRequest request, UserBean user)
  {
    try
    {
      connect();
      
      this.user = user;
      
      register(user.getLoginName(), request);
      
      addFields();
      
      // set reason description
      setReasonDescription(HttpHelper.getInt(request, "reason", 0));
      
      setFields(request);
      
      if(getData("Submit").equals("Submit"))
      {
        if(isValid())
        {
          submitData();
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public String getLoginName()
  {
    return user.getLoginName();
  }
  
  public String getUserName()
  {
    return user.getUserName();
  }
  
  protected void addFields()
  {
    try
    {
      // set up form fields
      PasswordField     fPassword         = new PasswordField("password", 32, 20, false);
      PasswordField     fConfirmPassword  = new PasswordField("passwordConfirm", 32, 20, false);
      
      fConfirmPassword.addValidation(new FieldEqualsFieldValidation(fPassword, "Password"));
      fPassword.addValidation(new MesPasswordValidation(user.getPasswordEnc(),fPassword.getNullAllowed()));
      
      fields.add(fPassword);
      fields.add(fConfirmPassword);
      fields.add(new HiddenField("reason"));
      fields.add(new ButtonField("Submit"));
    }
    catch(Exception e)
    {
      logEntry("addFields()", e.toString());
    }
  }
  
  protected void replaceKeywords( StringBuffer buffer, HashMap keywordList )
  {
    int           offsetBegin       = 0;
    int           offsetEnd         = 0;
    String        keyword           = null;
    
    for ( Iterator it = keywordList.keySet().iterator(); it.hasNext(); )
    {
      keyword = (String)it.next();
      
      while ( ( offsetBegin = buffer.toString().indexOf(keyword) ) != -1 )
      {
        offsetEnd = (offsetBegin + keyword.length());
        buffer.replace(offsetBegin,offsetEnd,(String)keywordList.get(keyword));
      }
    }      
  }
  
  protected void submitData()
  {
    try
    {
      // try to change password on TriCipher appliance
      
        
      // update user record
      /*@lineinfo:generated-code*//*@lineinfo:150^7*/

//  ************************************************************
//  #sql [Ctx] { update  users
//          set     password_enc = encrypt_password(:getData("password"))
//          where   login_name = :user.getLoginName()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1103 = getData("password");
 String __sJT_1104 = user.getLoginName();
   String theSqlTS = "update  users\n        set     password_enc = encrypt_password( :1 )\n        where   login_name =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.maintenance.UserChangePasswordBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1103);
   __sJT_st.setString(2,__sJT_1104);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:155^7*/
      
      PasswordChanged = true;
    }
    catch(Exception e)
    {
      logEntry("submitData()", e.toString());
    }
  }
  
  protected void setReasonDescription(int reason)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    StringBuffer        descBuf     = new StringBuffer("");
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:173^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  description
//          from    users_password_reasons
//          where   reason = :reason
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  description\n        from    users_password_reasons\n        where   reason =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.UserChangePasswordBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,reason);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.UserChangePasswordBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:178^7*/
      rs = it.getResultSet();
      
      if(rs.next())
      {
        descBuf.append(rs.getString("description"));
      }
      rs.close();
      it.close();
      
      // get expiration days
      /*@lineinfo:generated-code*//*@lineinfo:189^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  upe.expiration_days                   expire_days
//          from    users_password_expire upe,
//                  t_hierarchy           th,
//                  users                 u,
//                  mif                   m
//          where   th.hier_type = 1 and
//                  th.ancestor = upe.hierarchy_node and
//                  upe.enabled = 'Y' and
//                  upe.reason = :reason and
//                  u.hierarchy_node = m.merchant_number(+) and
//                  (
//                    (
//                      m.merchant_number is null and
//                      th.descendent = u.hierarchy_node
//                    ) or
//                    (
//                      th.descendent = m.association_node and
//                      m.merchant_number = u.hierarchy_node
//                    ) 
//                  ) and
//                  u.login_name = :user.getLoginName()
//          order by th.relation asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1105 = user.getLoginName();
  try {
   String theSqlTS = "select  upe.expiration_days                   expire_days\n        from    users_password_expire upe,\n                t_hierarchy           th,\n                users                 u,\n                mif                   m\n        where   th.hier_type = 1 and\n                th.ancestor = upe.hierarchy_node and\n                upe.enabled = 'Y' and\n                upe.reason =  :1  and\n                u.hierarchy_node = m.merchant_number(+) and\n                (\n                  (\n                    m.merchant_number is null and\n                    th.descendent = u.hierarchy_node\n                  ) or\n                  (\n                    th.descendent = m.association_node and\n                    m.merchant_number = u.hierarchy_node\n                  ) \n                ) and\n                u.login_name =  :2 \n        order by th.relation asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.UserChangePasswordBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,reason);
   __sJT_st.setString(2,__sJT_1105);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.maintenance.UserChangePasswordBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:213^7*/
      rs = it.getResultSet();
      
      String expireDays  = "";
      if(rs.next())
      {
        expireDays = rs.getString("expire_days");
      }
      rs.close();
      it.close();
      
      // build a lookup table of key words to replace
      HashMap     keywords = new HashMap();
      
      keywords.put("#days#", expireDays );
      
      replaceKeywords(descBuf,keywords);
      
      description = descBuf.toString();
    }
    catch(Exception e)
    {
      logEntry("setReasonDescription(" + reason + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  public String getReasonDescription()
  {
    return description;
  }
  
  public boolean passwordChanged()
  {
    return(PasswordChanged);
  }
  
  public class MesPasswordValidation extends SQLJConnectionBase
    implements Validation
  {
    String  errorText     = "";
    String  oldPassword   = null;
    boolean nullAllowed   = false;
    
    public MesPasswordValidation(String currentPassword, boolean nullAllowed)
    {
      super();
      this.nullAllowed  = nullAllowed;
      this.oldPassword  = currentPassword;
    }
    
    // passwords must be at least 6 characters.  May add more validation later
    public boolean validate(String password)
    {              
      MD5     md5         = null;
      int     recCount    = 0;
      
      try
      {
        errorText = null;   // reset the error
        
        // if field is optional and the password is blank then don't bother
        if( !nullAllowed )
        {
          if( password == null || password.length() < 6 )
          {
            errorText = "Password length must be at least 6 characters";
          }
          
          // no error yet
          if ( errorText == null )
          {
            // get hash of password
            md5 = new MD5();
            md5.Init();
            md5.Update(password);
            String newPass = md5.asHex();
            
            if ( oldPassword.equals(newPass) )
            {
              errorText = "New password cannot match the old password";
            }
          }
        }
      }
      catch(Exception e)
      {
        logEntry("validate(" + password + ")", e.toString());
      }
      finally
      {
        cleanUp();
      }
      
      return( errorText == null );
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }
  
}/*@lineinfo:generated-code*/