/*@lineinfo:filename=MerchantIPS*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/MerchantIPS.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;

public class MerchantIPS extends SQLJConnectionBase
{
  private long    merchantNumber    = 0L;
  private Vector  rows              = new Vector();
  
  public void loadData(String merchantNumber)
  {
    ResultSetIterator     it      = null;
    try
    {
      connect();
      
      // convert merchantNumber to internal long
      setMerchantNumber(Long.parseLong(merchantNumber));
      
      /*@lineinfo:generated-code*//*@lineinfo:55^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ips.serial_number   application_id,
//                  ips.terminal_type   equip_type,
//                  ips.install_date    install_date,
//                  ips.cost / 100      cost,
//                  ips.disposition     disposition,
//                  ips.rec_num         rec_num
//          from    ips_file  ips
//          where   ips.merchant_number = :this.merchantNumber
//          order by rec_num asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ips.serial_number   application_id,\n                ips.terminal_type   equip_type,\n                ips.install_date    install_date,\n                ips.cost / 100      cost,\n                ips.disposition     disposition,\n                ips.rec_num         rec_num\n        from    ips_file  ips\n        where   ips.merchant_number =  :1 \n        order by rec_num asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.MerchantIPS",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,this.merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.MerchantIPS",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:66^7*/
      
      ResultSet rs = it.getResultSet();
      
      while(rs.next())
      {
        rows.add(new IPSData( rs.getString("application_id"),
                              rs.getString("equip_type"),
                              rs.getDate("install_date"),
                              rs.getDouble("cost"),
                              rs.getString("disposition")));
      }
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadData(" + merchantNumber + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void setMerchant(String merchantNumber)
  {
    loadData(merchantNumber);
  }
  
  public void setMerchantNumber(String merchantNumber)
  {
    try
    {
      setMerchantNumber(Long.parseLong(merchantNumber));
    }
    catch(Exception e)
    {
      logEntry("setMerchantNumber(" + merchantNumber + ")", e.toString());
    }
  }
  
  public void setMerchantNumber(long merchantNumber)
  {
    this.merchantNumber = merchantNumber;
  }
  public long getMerchantNumber()
  {
    return this.merchantNumber;
  }
  
  public Vector getRows()
  {
    return this.rows;
  }
  
  public class IPSData
  {
    public String   applicationId     = "";
    public String   equipType         = "";
    public String   installDate       = "";
    public String   cost              = "";
    public String   disposition       = "";
  
    public IPSData( String  applicationId, 
                    String  equipType,
                    Date    installDate,
                    double  cost,
                    String  disposition)
    {
      setApplicationId(applicationId);
      setEquipType(equipType);
      setInstallDate(installDate);
      setCost(cost);
      setDisposition(disposition);
    }
    
    public String processString(String data)
    {
      String result = "&nbsp";
      
      if(data != null && ! data.equals(""))
      {
        result = data;
      }
      
      return result;
    }
    
    public void setApplicationId(String applicationId)
    {
      this.applicationId = processString(applicationId);
    }
    
    public void setEquipType(String equipType)
    {
      this.equipType = processString(equipType);
    }
    
    public void setInstallDate(Date installDate)
    {
      this.installDate = processString(DateTimeFormatter.getFormattedDate(installDate, "MM/dd/yy"));
    }
    
    public void setCost(double cost)
    {
      this.cost = processString(NumberFormatter.getDoubleString(cost, "$#####0.00"));
    }
    
    public void setDisposition(String disposition)
    {
      this.disposition = processString(disposition);
    }
  }
}/*@lineinfo:generated-code*/