/*@lineinfo:filename=MerchantDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/maintenance/MerchantDataBean.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2010-08-09 14:57:56 -0700 (Mon, 09 Aug 2010) $
  Version            : $Revision: 17687 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.constants.MesHierarchy;
import com.mes.constants.MesUsers;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.CurrencyField;
import com.mes.forms.CurrencyPrettyField;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.NameField;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.RadioButtonField;
import com.mes.support.NumberFormatter;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class MerchantDataBean extends FieldBean
{
  static Logger log = Logger.getLogger(MerchantDataBean.class);
  
  public static final int   CBT_IDX_AUTH_TERMINAL   = 0;
  public static final int   CBT_IDX_AUTH_VOICE      = 1;
  public static final int   CBT_IDX_AUTH_ARU        = 2;
  public static final int   CBT_IDX_AUTH_ECR        = 3;
  public static final int   CBT_IDX_AUTH_BATCH      = 4;
  public static final int   CBT_IDX_AUTH_FIXED      = 5;
  
  /*
  ** BEGINNING OF CLASS MerchantDataBean
  */
  // bank procedures link
  public boolean      showBankProcedures    = false;
  
  // bet table numbers
  public Vector     authBets              = new Vector();
  public Vector     dataCaptureBets       = new Vector();
  public Vector     planBets              = new Vector();
  public Vector     debitBets             = new Vector();
  public String     visaInterchangeBet    = "";
  public String     mcInterchangeBet      = "";
  public String     dinersInterchangeBet  = "";
  public String     systemGeneratedBet    = "";
  
  // CB&T-specific bet descriptions
  public String       visaInterchangeDesc = "";
  public String       mcInterchangeDesc   = "";
  public CBTAuthBet[] cbtAuthBets         = new CBTAuthBet[6];
  
  private Hashtable   planLookup          = new Hashtable();

  public MerchantDataBean()
  {
    cbtAuthBets[0]  = new CBTAuthBet();
    cbtAuthBets[1]  = new CBTAuthBet();
    cbtAuthBets[2]  = new CBTAuthBet();
    cbtAuthBets[3]  = new CBTAuthBet();
    cbtAuthBets[4]  = new CBTAuthBet();
    cbtAuthBets[5]  = new CBTAuthBet();
  }
  
  protected static final String[][] yesNoRadioList = 
  {
    { "No",  "N" },
    { "Yes", "Y" }
  };
  
  //********************************************************************
  //
  // These methods are here to support legacy code that may use them
  //
  public String getMerchantNumber()
  {
    return( getData("merchantNumber") );
  }
  public String getDba()
  {
    return( getData("dbaName") );
  }
  public long getAppSeqNum()
  {
    long result = 0L;
    if(!getData("appSeqNum").equals(""))
    {
      result = Long.parseLong(getData("appSeqNum"));
    }
    return( result );
  }
  public String getBankNumber()
  {
    return( getData("bankNumber") );
  }
  public String getBankProceduresUrl()
  {
    return( getData("bankProceduresUrl") );
  }
  
  public String bankProceduresUrl = null;
  //
  //
  //********************************************************************
  
  protected void updateData()
  {
    try
    {
      connect();
      
      if(user.hasRight(MesUsers.RIGHT_CIF_SCREEN_EDIT))
      {
        // get app_seq_num from merchant
        long appSeqNum = 0L;
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:156^11*/

//  ************************************************************
//  #sql [Ctx] { select  app_seq_num
//              
//              from    merchant
//              where   merch_number = :getData("merchant")
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_853 = getData("merchant");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app_seq_num\n             \n            from    merchant\n            where   merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.MerchantDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_853);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:162^11*/
        }
        catch(Exception e)
        {
          appSeqNum = 0L;
        }
      
        if(appSeqNum > 0L)
        {
          // update contact name
          int recCount = 0;
          /*@lineinfo:generated-code*//*@lineinfo:173^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//              
//              from    merchcontact
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n             \n            from    merchcontact\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.MerchantDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:179^11*/
      
          if(recCount > 0)
          {
            /*@lineinfo:generated-code*//*@lineinfo:183^13*/

//  ************************************************************
//  #sql [Ctx] { update  merchcontact
//                set     merchcont_prim_first_name = :getData("contactNameFirst"),
//                        merchcont_prim_last_name  = :getData("contactNameLast"),
//                        merchcont_prim_phone      = :getData("contactPhone")
//                where   app_seq_num = :appSeqNum
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_854 = getData("contactNameFirst");
 String __sJT_855 = getData("contactNameLast");
 String __sJT_856 = getData("contactPhone");
   String theSqlTS = "update  merchcontact\n              set     merchcont_prim_first_name =  :1 ,\n                      merchcont_prim_last_name  =  :2 ,\n                      merchcont_prim_phone      =  :3 \n              where   app_seq_num =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_854);
   __sJT_st.setString(2,__sJT_855);
   __sJT_st.setString(3,__sJT_856);
   __sJT_st.setLong(4,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:190^13*/
          }
          else
          {
            // insert new record
            /*@lineinfo:generated-code*//*@lineinfo:195^13*/

//  ************************************************************
//  #sql [Ctx] { insert into merchcontact
//                (
//                  app_seq_num,
//                  merchcont_prim_first_name,
//                  merchcont_prim_last_name,
//                  merchcont_prim_phone
//                )
//                values
//                (
//                  :appSeqNum,
//                  :getData("contactNameFirst"),
//                  :getData("contactNameLast"),
//                  :getData("contactPhone")
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_857 = getData("contactNameFirst");
 String __sJT_858 = getData("contactNameLast");
 String __sJT_859 = getData("contactPhone");
   String theSqlTS = "insert into merchcontact\n              (\n                app_seq_num,\n                merchcont_prim_first_name,\n                merchcont_prim_last_name,\n                merchcont_prim_phone\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_857);
   __sJT_st.setString(3,__sJT_858);
   __sJT_st.setString(4,__sJT_859);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:211^13*/
          }
        
          // update web address and app email address
          /*@lineinfo:generated-code*//*@lineinfo:215^11*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//              set     merch_web_url = :getData("appWebAddressDisplay"),
//                      merch_email_address = :getData("appEmailAddress")
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_860 = getData("appWebAddressDisplay");
 String __sJT_861 = getData("appEmailAddress");
   String theSqlTS = "update  merchant\n            set     merch_web_url =  :1 ,\n                    merch_email_address =  :2 \n            where   app_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_860);
   __sJT_st.setString(2,__sJT_861);
   __sJT_st.setLong(3,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:221^11*/
        
          // update user email address
          /*@lineinfo:generated-code*//*@lineinfo:224^11*/

//  ************************************************************
//  #sql [Ctx] { update  users
//              set     email = :getData("userEmailAddressDisplay")
//              where   hierarchy_node = :getData("merchant")
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_862 = getData("userEmailAddressDisplay");
 String __sJT_863 = getData("merchant");
   String theSqlTS = "update  users\n            set     email =  :1 \n            where   hierarchy_node =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_862);
   __sJT_st.setString(2,__sJT_863);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:229^11*/
        }
      }
      
      if(user.hasRight(MesUsers.RIGHT_BANKSERV_EDIT))
      {
        int recCount = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:237^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//            
//            from    bankserv_account_types
//            where   merchant_number = :getData("merchant")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_864 = getData("merchant");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n           \n          from    bankserv_account_types\n          where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.maintenance.MerchantDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_864);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:243^9*/
        
        if(recCount > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:247^11*/

//  ************************************************************
//  #sql [Ctx] { update  bankserv_account_types
//              set     account_type = :getData("bankservAcctType"),
//                      enabled = 'Y'
//              where   merchant_number = :getData("merchant")
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_865 = getData("bankservAcctType");
 String __sJT_866 = getData("merchant");
   String theSqlTS = "update  bankserv_account_types\n            set     account_type =  :1 ,\n                    enabled = 'Y'\n            where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_865);
   __sJT_st.setString(2,__sJT_866);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:253^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:257^11*/

//  ************************************************************
//  #sql [Ctx] { insert into bankserv_account_types
//              (
//                merchant_number,
//                account_type,
//                enabled
//              )
//              values
//              (
//                :getData("merchant"),
//                :getData("bankservAcctType"),
//                'Y'
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_867 = getData("merchant");
 String __sJT_868 = getData("bankservAcctType");
   String theSqlTS = "insert into bankserv_account_types\n            (\n              merchant_number,\n              account_type,\n              enabled\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n              'Y'\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_867);
   __sJT_st.setString(2,__sJT_868);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:271^11*/
        }
      }
      
      if( user.hasRight(MesUsers.RIGHT_MERCHANT_ACH_EDIT) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:277^9*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//            set     daily_discount_interchange = :getData("dailyDiscount"),
//                    minimum_monthly_discount = :getData("monthlyMinimum"),
//                    daily_ach = :getData("dailyAch"),
//                    suspended_days = :getData("suspendedDays")
//            where   merchant_number = :getData("merchant")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_869 = getData("dailyDiscount");
 String __sJT_870 = getData("monthlyMinimum");
 String __sJT_871 = getData("dailyAch");
 String __sJT_872 = getData("suspendedDays");
 String __sJT_873 = getData("merchant");
   String theSqlTS = "update  mif\n          set     daily_discount_interchange =  :1 ,\n                  minimum_monthly_discount =  :2 ,\n                  daily_ach =  :3 ,\n                  suspended_days =  :4 \n          where   merchant_number =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_869);
   __sJT_st.setString(2,__sJT_870);
   __sJT_st.setString(3,__sJT_871);
   __sJT_st.setString(4,__sJT_872);
   __sJT_st.setString(5,__sJT_873);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:285^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("updateData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void setProperties(HttpServletRequest request, UserBean user)
  {
    try
    {
      connect();
      
      this.user     = user;
      this.request  = request;
      
      register(user.getLoginName(), request);
      
      setFields(request);
      
      if(!getData("submitChanges").equals(""))
      {
        // submit data to database
        if(isValid())
        {
          updateData();
        }
      }
      
      loadData();
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void createPlanLookup()
  {
    planLookup.put("VISA", "vs");
    planLookup.put("VSDB", "vd");
    planLookup.put("VSBS", "vb");
    planLookup.put("MC",   "mc");
    planLookup.put("MCDB", "mcd");
    planLookup.put("MCBS", "mcb");
  }
  
  public void createFields(HttpServletRequest request)
  {
    try
    {
      fields.add(new Field("merchant",        "Merchant Number",  16, 16, true));
      fields.add(new Field("merchantNumber",  "Merchant Number",  16, 16, true));
      fields.add(new Field("merchantNumberEnhanced", "Merchant Number", 40, 40, true));
      fields.add(new Field("dbaName",         "DBA Name",         32, 32, true));
      fields.add(new Field("dmagent",         "Association",      6,  6,  true));
      fields.add(new Field("corporateName",   "Corporate Name",   25, 25, true));
      fields.add(new Field("assocDescription","Assoc Description",25, 25, true));
      fields.add(new Field("bankNumber",      "Bank Number",      4,  4,  true));
      fields.add(new Field("bankNameSyf",     "Bank Name",        10, 10, true));
      fields.add(new Field("sicCode",         "Sic Code",         4,  4,  true));
      fields.add(new Field("sicDescription",  "Sic Description",  60, 60, true));
      fields.add(new Field("group1",          "Group 1",          6,  6,  true));
      fields.add(new Field("group1Description","Group 1 Desc",    25, 25, true));
      fields.add(new Field("group2",          "Group 2",          6,  6,  true));
      fields.add(new Field("group2Description","Group 2 Desc",    25, 25, true));
      fields.add(new Field("group3",          "Group 3",          6,  6,  true));
      fields.add(new Field("group3Description","Group 3 Desc",    25, 25, true));
      fields.add(new Field("group4",          "Group 4",          6,  6,  true));
      fields.add(new Field("group4Description","Group 4 Desc",    25, 25, true));
      fields.add(new Field("group5",          "Group 5",          6,  6,  true));
      fields.add(new Field("group5Description","Group 5 Desc",    25, 25, true));
      fields.add(new Field("group6",          "Group 6",          6,  6,  true));
      fields.add(new Field("group6Description","Group 6 Desc",    25, 25, true));
      fields.add(new Field("phone1",          "Ph 1",             10, 10, true));
      fields.add(new Field("phone2Fax",       "Ph 2",             10, 10, true));
      fields.add(new Field("ddaNum",          "DDA",              17, 17, true));
      fields.add(new Field("transitRouting",  "TR #",             9,  9,  true));
      fields.add(new Field("merchantStatus",  "Merchant Status",  1,  1,  true));
      fields.add(new Field("accountStatus",   "Account Status",   20, 20, true));
      fields.add(new Field("userData1",       "User 1",           4,  4,  true));
      fields.add(new Field("userData2",       "User 2",           4,  4,  true));
      fields.add(new Field("userData3",       "User 3",           4,  4,  true));
      fields.add(new Field("userData4",       "User 4",           16, 16, true));
      fields.add(new Field("userData5",       "User 5",           16, 16, true));
      fields.add(new Field("userAcct1",       "User Acct 1",      16, 16, true));
      fields.add(new Field("userAcct2",       "User Acct 2",      16, 16, true));
      fields.add(new Field("addr100",         "Addr 1 00",        32, 32, true));
      fields.add(new Field("addr200",         "Addr 2 00",        32, 32, true));
      fields.add(new Field("city00",          "City 00",          24, 24, true));
      fields.add(new Field("state00",         "State 00",         2,  2,  true));
      fields.add(new Field("zip00",           "Zip 00",           9,  9,  true));
      fields.add(new Field("name01",          "Name 01",          32, 32, true));
      fields.add(new Field("addr101",         "Addr 1 01",        32, 32, true));
      fields.add(new Field("addr201",         "Addr 2 01",        32, 32, true));
      fields.add(new Field("city01",          "City 01",          24, 24, true));
      fields.add(new Field("state01",         "State 01",         2,  2,  true));
      fields.add(new Field("zip01",           "Zip 01",           9,  9,  true));
      fields.add(new Field("name02",          "Name 02",          32, 32, true));
      fields.add(new Field("addr102",         "Addr 1 02",        32, 32, true));
      fields.add(new Field("addr202",         "Addr 2 02",        32, 32, true));
      fields.add(new Field("city02",          "City 02",          24, 24, true));
      fields.add(new Field("state02",         "State 02",         2,  2,  true));
      fields.add(new Field("zip02",           "Zip 02",           9,  9,  true));
      fields.add(new Field("vsPlan",          "VS Plan",          2,  2,  true));
      fields.add(new Field("mcPlan",          "MC Plan",          2,  2,  true));
      fields.add(new Field("dcPlan",          "DC Plan",          2,  2,  true));
      fields.add(new Field("jcPlan",          "JC Plan",          2,  2,  true));
      fields.add(new Field("amPlan",          "AM Plan",          2,  2,  true));
      fields.add(new Field("dsPlan",          "DS Plan",          2,  2,  true));
      fields.add(new Field("dbPlan",          "DB Plan",          2,  2,  true));
      fields.add(new Field("v$Plan",          "V$ Plan",          2,  2,  true));
      fields.add(new Field("m$Plan",          "M$ Plan",          2,  2,  true));
      fields.add(new Field("emailDesc00",     "Email Desc 00",    25, 25, true));
      fields.add(new Field("emailAddr00",     "Email Addr 00",    75, 75, true));
      fields.add(new Field("emailDesc01",     "Email Desc 01",    25, 25, true));
      fields.add(new Field("emailAddr01",     "Email Addr 01",    75, 75, true));
      fields.add(new Field("emailDesc02",     "Email Desc 02",    25, 25, true));
      fields.add(new Field("emailAddr02",     "Email Addr 02",    75, 75, true));
      fields.add(new Field("emailDesc03",     "Email Desc 03",    25, 25, true));
      fields.add(new Field("emailAddr03",     "Email Addr 03",    75, 75, true));
      fields.add(new Field("emailDesc04",     "Email Desc 04",    25, 25, true));
      fields.add(new Field("emailAddr04",     "Email Addr 04",    75, 75, true));
      fields.add(new Field("emailDesc05",     "Email Desc 05",    25, 25, true));
      fields.add(new Field("emailAddr05",     "Email Addr 05",    75, 75, true));
      fields.add(new Field("emailDesc06",     "Email Desc 06",    25, 25, true));
      fields.add(new Field("emailAddr06",     "Email Addr 06",    75, 75, true));
      fields.add(new Field("emailDesc07",     "Email Desc 07",    25, 25, true));
      fields.add(new Field("emailAddr07",     "Email Addr 07",    75, 75, true));
      fields.add(new Field("emailDesc08",     "Email Desc 08",    25, 25, true));
      fields.add(new Field("emailAddr08",     "Email Addr 08",    75, 75, true));
      fields.add(new Field("emailDesc09",     "Email Desc 09",    25, 25, true));
      fields.add(new Field("emailAddr09",     "Email Addr 09",    75, 75, true));
      fields.add(new Field("metTable",        "Met Table",        4,  4,  true));
      fields.add(new Field("metDescription",  "Met Descr",        30, 30, true));
      fields.add(new Field("investigatorCode","Investigator",     4,  4,  true));
      fields.add(new Field("repCode",         "Rep Code",         4,  4,  true));
      fields.add(new Field("vsDiscTable",     "VS Table #",       4,  4,  true));
      fields.add(new Field("vsDiscMethod",    "VS Disc Method",   1,  1,  true));
      fields.add(new Field("vsDiscType",      "VS Disc Type",     1,  1,  true));
      fields.add(new Field("mcDiscTable",     "MC Disc Table",    4,  4,  true));
      fields.add(new Field("mcDiscMethod",    "MC Disc Method",   1,  1,  true));
      fields.add(new Field("mcDiscType",      "MC DIsc Type",     1,  1,  true));
      fields.add(new Field("confirmationCode","Confirmation Code",20, 20, true));
      fields.add(new Field("enrollEmail",     "Enroll Email",     75, 75, true));
      fields.add(new Field("processor",       "Processor",        40, 40, true));
      fields.add(new Field("appSeqNum",       "App Seq Num",      10, 10, true));
      fields.add(new Field("valutecTid",      "Valutec Tid",      25, 25, true));
      fields.add(new Field("checkTid",        "Check Tid",        25, 25, true));
      fields.add(new Field("checkProvider",   "Check Provider",   50, 50, true));
      fields.add(new Field("appUserLogin",    "Sales Rep",        75, 75, true));
      fields.add(new NameField("contactName",     "Contact Name",     25, true, true));
      fields.add(new PhoneField("contactPhone",    "Contact Phone", true));
      fields.add(new Field("controlNumber",   "Control Number",   11, 11, true));
      fields.add(new Field("appEmailAddress", "App Email Address",75, 40, true));
      fields.add(new Field("appWebAddress",   "App Web Address",  100, 40, true));
      fields.add(new Field("appWebAddressDisplay","App Web Address",  100, 40, true));
      fields.add(new Field("appWebAddress2",  "App Web Address 2",512, 512, true));
      fields.add(new Field("bankProceduresUrl","Bank Proceduree", 100, 100, true));
      fields.add(new Field("federalTaxId",     "Federal Tax Id",  9,  9,  true));
      fields.add(new Field("socialSecurityNumber", "SSN",         9,  9,  true));
      fields.add(new Field("ownerName",        "Owner Name",      51, 25, true));
      fields.add(new Field("referringBank",    "Referring Bank",  40, 40, true));
      fields.add(new Field("businessNature",   "Nature of Bus",   30, 25, true));
      fields.add(new RadioButtonField("seasonalFlag", yesNoRadioList,-1,true,"Required"));
      //fields.add(new Field("seasonalFlag",     "Seasonal Flag",   1,  1,  true));
      fields.add(new Field("merchLegalName",   "Legal Name",      50, 50, true));
      
      fields.add(new Field("mifEmailAddresses", "MIF Email Addrs",500, 500, true));
      fields.add(new Field("mifWebAddresses",   "MIF Web Addrs",  500, 500, true));
      fields.add(new Field("appEmailAddresses", "APP Email Addrs",500, 40, true));
      fields.add(new Field("userEmailAddresses","USR Email Addrs",500, 40, true));
      fields.add(new Field("userEmailAddressDisplay", "USR Email Address", 75, 40, true));
      fields.add(new Field("xpressCheckoutLogin",   "Express Checkout Login Name",  100, 40, true));
      
      fields.add(new NumberField("vsDiscRate",  "VS Disc Rate", 7,  7,  true, 3));
      fields.add(new NumberField("vdDiscRate",  "VS Check Disc Rate", 7,  7,  true, 3));
      fields.add(new NumberField("vbDiscRate",  "VS Business Disc Rate", 7,  7,  true, 3));
      fields.add(new NumberField("vsPerItem",   "VS Per Item",  7,  7,  true, 3));
      fields.add(new NumberField("vdPerItem",   "VS Check Per Item",  7,  7,  true, 3));
      fields.add(new NumberField("vbPerItem",   "VS Business Per Item",  7,  7,  true, 3));
      fields.add(new NumberField("mcDiscRate",  "MC Disc Rate", 7,  7,  true, 3));
      fields.add(new NumberField("mcdDiscRate",  "MC Check Disc Rate", 7,  7,  true, 3));
      fields.add(new NumberField("mcbDiscRate",  "MC Business Disc Rate", 7,  7,  true, 3));
      fields.add(new NumberField("mcPerItem",   "MC Per Item",  7,  7,  true, 3));
      fields.add(new NumberField("mcdPerItem",   "MC Check Per Item",  7,  7,  true, 3));
      fields.add(new NumberField("mcbPerItem",   "MC Business Per Item",  7,  7,  true, 3));
      
      fields.add(new DateField("dateOpened",      "Date Opened",        true));
      ((DateField)(fields.getField("dateOpened"))).setDateFormat("MM/dd/yy");
      fields.add(new DateField("activationDate",  "Activation Date",    true));
      ((DateField)(fields.getField("activationDate"))).setDateFormat("MM/dd/yy");
      fields.add(new DateField("lastDepositDate", "Last Deposit Date",  true));
      ((DateField)(fields.getField("lastDepositDate"))).setDateFormat("MM/dd/yy");
      fields.add(new DateField("statusDate",      "Status Date",        true));
      ((DateField)(fields.getField("statusDate"))).setDateFormat("MM/dd/yy");
      fields.add(new DateField("enrollDate",      "Enroll Date",        true));
      ((DateField)(fields.getField("enrollDate"))).setDateFormat("MM/dd/yy");
      fields.add(new DateField("dateApproved",    "Date Approved",      true));
      ((DateField)(fields.getField("dateApproved"))).setDateFormat("MM/dd/yy");
      
      fields.add(new Field ("conversionDate", "Conversion Date", 10, 10, true));
      
      fields.add(new CurrencyPrettyField("lastDepositAmount", "Last Deposit Amount",  12, 12, true));
      fields.add(new CurrencyPrettyField("monthlySales",      "Monthly Sales",        12, 12, true));
      fields.add(new CurrencyPrettyField("avgTicket",         "Average Ticket",       12, 12, true));
      
      // pricing fields
      fields.add(new CurrencyPrettyField("debitPerItem",    "Debit Per Item",   5, 5, true));
      fields.add(new CurrencyPrettyField("ebtPerItem",      "EBT Per Item",     5, 5, true));

      // BankServ account type (drop-down)      
      fields.add(new DropDownField("bankservAcctType", "BankServ Acct Type", new BankServAcctTypeTable(), true));
      fields.add(new ButtonField("submitChanges", "Submit Changes"));
      
      // processor id 
      fields.add(new NumberField("processorId", "Processor ID", 4, 4, true, 0));
      
      // amex processing
      fields.add(new Field("mesAmexProcessing", "MES Amex Processing", 1, 1, true));
      fields.add(new Field("amexConversionDate", "Amex Conversion Date", 8, 8, true));
      
      // ACH-related data
      fields.add(new DropDownField("dailyDiscount", "Daily Discount/IC", new DailyDiscountICTable(), true));
      fields.add(new CurrencyField("monthlyMinimum", "Minimum Monthly Discount", 7, 7, true));
      fields.add(new Field("dailyAch", "Daily ACH", 1, 1, true));
      fields.add(new NumberField("suspendedDays", "Days Suspense", 2, 2, true, 0));

      // ACH Payment data
      fields.add(new Field("achpAccepted","ACH Payments Accepted",1,1,true));
      
      fields.setHtmlExtra("class=\"formFields\"");
      
      // create plan type lookup hash
      createPlanLookup();
    }
    catch(Exception e)
    {
      logEntry("createFields()", e.toString());
    }
  }

  public boolean shouldSeeBetLinks()
  {
    return (getData("bankNumber").equals("3941") || 
            getData("bankNumber").equals("3858") || 
            getData("bankNumber").equals("3860"));
  }
  
  protected void getMerchantData()
  {
    ResultSetIterator     it        = null;
    ResultSet             rs        = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:551^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.merchant_number                           merchant_number,
//                  m.merchant_number||
//                    decode(ms.premier_customer,
//                          null, '',
//                          'N',  '',
//                          ' (PREMIER)')                       merchant_number_enhanced,
//                  m.dba_name                                  dba_name,
//                  mif_date_opened(m.date_opened)              date_opened,
//                  to_char(m.conversion_date, 'mm/dd/rr')      conversion_date,
//                  m.activation_date                           activation_date,
//                  m.last_deposit_date                         last_deposit_date,
//                  m.last_deposit_amount                       last_deposit_amount,
//                  m.dmagent                                   dmagent,
//                  m.fdr_corp_name                             corporate_name,
//                  gn7.group_name                              assoc_description,
//                  m.bank_number                               bank_number,
//                  m.bank_name_syf                             bank_name_syf,
//                  m.sic_code                                  sic_code,
//                  substr(sc.merchant_type, 1, 33)             sic_description,
//                  m.group_1_association                       group_1,
//                  substr(gn1.group_name, 1, 25)               group_1_description,
//                  m.group_2_association                       group_2,
//                  substr(gn2.group_name, 1, 25)               group_2_description,
//                  m.group_3_association                       group_3,
//                  substr(gn3.group_name, 1, 25)               group_3_description,
//                  m.group_4_association                       group_4,
//                  substr(gn4.group_name, 1, 25)               group_4_description,
//                  m.group_5_association                       group_5,
//                  substr(gn5.group_name, 1, 25)               group_5_description,
//                  m.group_6_association                       group_6,
//                  substr(gn6.group_name, 1, 25)               group_6_description,
//                  m.phone_1                                   phone_1,
//                  m.phone_2_fax                               phone_2_fax,
//                  m.dda_num                                   dda_num,
//                  lpad(m.transit_routng_num, 9, '0')          transit_routing,
//                  m.date_stat_chgd_to_dcb                     status_date,
//                  m.dmacctst                                  merchant_status,
//                  m.user_data_1                               user_data_1,
//                  m.user_data_2                               user_data_2,
//                  m.user_data_3                               user_data_3,
//                  m.user_data_4                               user_data_4,
//                  m.user_data_5                               user_data_5,
//                  m.user_acount_1                             user_acct_1,
//                  m.user_account_2                            user_acct_2,
//                  m.dmaddr                                    addr1_00,
//                  m.address_line_3                            addr2_00,
//                  m.dmcity                                    city_00,
//                  m.dmstate                                   state_00,
//                  m.dmzip                                     zip_00,
//                  m.name1_line_1                              name_01,
//                  m.addr1_line_1                              addr1_01,
//                  m.addr1_line_2                              addr2_01,
//                  m.city1_line_4                              city_01,
//                  m.state1_line_4                             state_01,
//                  m.zip1_line_4                               zip_01,
//                  m.name2_line_1                              name_02,
//                  m.addr2_line_1                              addr1_02,
//                  m.addr2_line_2                              addr2_02,
//                  m.city2_line_4                              city_02,
//                  m.state2_line_4                             state_02,
//                  m.zip2_line_4                               zip_02,
//                  m.visa_plan                                 vs_plan,
//                  m.mastcd_plan                               mc_plan,
//                  m.diners_plan                               dc_plan,
//                  m.jcb_plan                                  jc_plan,
//                  m.amex_plan                                 am_plan,
//                  m.discover_plan                             ds_plan,
//                  m.debit_plan                                db_plan,
//                  m.visa_cash_plan                            v$_plan,
//                  m.mastcd_cash_plan                          m$_plan,
//                  m.email_desc_00                             email_desc_00,
//                  m.email_addr_00                             email_addr_00,
//                  m.email_desc_01                             email_desc_01,
//                  m.email_addr_01                             email_addr_01,
//                  m.email_desc_02                             email_desc_02,
//                  m.email_addr_02                             email_addr_02,
//                  m.email_desc_03                             email_desc_03,
//                  m.email_addr_03                             email_addr_03,
//                  m.email_desc_04                             email_desc_04,
//                  m.email_addr_04                             email_addr_04,
//                  m.email_desc_05                             email_desc_05,
//                  m.email_addr_05                             email_addr_05,
//                  m.email_desc_06                             email_desc_06,
//                  m.email_addr_06                             email_addr_06,
//                  m.email_desc_07                             email_desc_07,
//                  m.email_addr_07                             email_addr_07,
//                  m.email_desc_08                             email_desc_08,
//                  m.email_addr_08                             email_addr_08,
//                  m.email_desc_09                             email_desc_09,
//                  m.email_addr_09                             email_addr_09,
//                  m.met_table                                 met_table,
//                  nvl(substr(mtn.met_type||' '||
//                    mtn.ave_ticket||' ave tick, '||
//                    nvl(to_char(mtn.monthly_sales), '--')||' sales, '||
//                    dup_card_num||' dup card, '||
//                    nvl(dup_amount,0)||' dup amt, '||
//                    percent_keyed||'% keyed',
//                    1, 25), '---')                            met_description,
//                  m.inv_code_investigate                      investigator_code,
//                  nvl(m.rep_code,'n/a')                       rep_code,
//                  -- pricing                                  
//                  m.visa_disc_table                           vs_disc_table,
//                  m.visa_disc_method                          vs_disc_method,
//                  m.visa_disc_type                            vs_disc_type,
//                  m.visa_disc_rate / 1000                     vs_disc_rate,
//                  m.visa_per_item / 1000                      vs_per_item,
//                  m.mastcd_disc_table                         mc_disc_table,
//                  m.mastcd_disc_method                        mc_disc_method,
//                  m.mastcd_disc_type                          mc_disc_type,
//                  m.mastcd_disc_rate / 1000                   mc_disc_rate,
//                  m.mastcd_per_item / 1000                    mc_per_item,
//                  m.federal_tax_id                            federal_tax_id,
//                  me.confirmation_code                        confirmation_code,
//                  me.email_address                            enroll_email,
//                  me.complete_date                            enroll_date,
//                  merch.app_seq_num                           app_seq_num,
//                  nvl(mpo_vtec.merchpo_tid,'N/A')             valutec_tid,
//                  nvl(mpo_check.merchpo_tid,'N/A')            check_tid,
//                  nvl(mpo_check.merchpo_provider_name,'N/A')  check_provider,
//                  upper(merch.merch_legal_name)               merch_legal_name,
//                  nvl(bat.account_type, 'CK')                 bankserv_acct_type,
//                  nvl(m.daily_discount_interchange, 'N')      daily_discount,
//                  nvl(m.minimum_monthly_discount, 0)          monthly_minimum,
//                  nvl(m.daily_ach, 'Y')                       daily_ach,
//                  nvl(m.suspended_days, 0)                    suspended_days,
//                  nvl(m.processor_id, :mesConstants.PROCESSOR_TSYS) processor_id,
//                  upper(nvl(m.mes_amex_processing, 'N'))      mes_amex_processing,
//                  to_char(m.amex_conversion_date,'mm/dd/yyyy') amex_conversion_date,
//                  decode(m.processor_id,
//                    :mesConstants.PROCESSOR_MES, '<font color="red">MES</font>',
//                    'Vital')                                  processor,
//                  decode( nvl(ppc.merchant_number,-1),
//                          -1, 'NONE',
//                          (ppc.payflow_login_name || '/' || ppc.pp_account_email_addr) )
//                                                              xpress_checkout_login,
//                  decode(nvl(achp.enabled_count,0),0,'N','Y') achp_accepted
//          from    mif                           m,
//                  merchant_enrollment           me,
//                  merchant                      merch,
//                  merchpayoption                mpo_vtec,
//                  merchpayoption                mpo_check,
//                  sic_codes                     sc,
//                  met_tables_new                mtn,
//                  group_names                   gn1,
//                  group_names                   gn2,
//                  group_names                   gn3,
//                  group_names                   gn4,
//                  group_names                   gn5,
//                  group_names                   gn6,
//                  group_names                   gn7,
//                  merchant_sabre                ms,
//                  bankserv_account_types        bat,
//                  merchant_paypal_checkout      ppc,
//                  ( select  tp.merchant_number,
//                            count(tpa.terminal_id) enabled_count
//                    from    trident_profile     tp,
//                            trident_profile_api tpa
//                    where   tp.merchant_number = :getData("merchant")
//                            and tp.terminal_id = tpa.terminal_id
//                            and ( lower(tpa.achp_ccd_accept) = 'y' or
//                                  lower(tpa.achp_ppd_accept) = 'y' or
//                                  lower(tpa.achp_web_accept) = 'y' or
//                                  lower(tpa.achp_tel_accept) = 'y' )
//                    group by tp.merchant_number ) achp
//          where   m.merchant_number = :getData("merchant") and
//                  m.merchant_number = me.merchant_number(+) and
//                  m.merchant_number = merch.merch_number(+) and
//                  m.sic_code = sc.sic_code(+) and
//                  m.met_table = mtn.met_table_num(+) and
//                  mpo_vtec.app_seq_num(+) = merch.app_seq_num and
//                  mpo_vtec.cardtype_code(+) = :mesConstants.APP_CT_VALUTEC_GIFT_CARD and -- 22
//                  mpo_check.app_seq_num(+) = merch.app_seq_num and
//                  mpo_check.cardtype_code(+) = :mesConstants.APP_CT_CHECK_AUTH and -- 18
//                  m.bank_number||m.group_1_association = gn1.group_number(+) and
//                  m.bank_number||m.group_2_association = gn2.group_number(+) and
//                  m.bank_number||m.group_3_association = gn3.group_number(+) and
//                  m.bank_number||m.group_4_association = gn4.group_number(+) and
//                  m.bank_number||m.group_5_association = gn5.group_number(+) and
//                  m.bank_number||m.group_6_association = gn6.group_number(+) and
//                  m.bank_number||m.dmagent             = gn7.group_number(+) and
//                  merch.app_seq_num = ms.app_seq_num(+) and
//                  m.merchant_number = bat.merchant_number(+) and
//                  ppc.merchant_number(+) = m.merchant_number and
//                  achp.merchant_number(+) = m.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_874 = getData("merchant");
 String __sJT_875 = getData("merchant");
  try {
   String theSqlTS = "select  m.merchant_number                           merchant_number,\n                m.merchant_number||\n                  decode(ms.premier_customer,\n                        null, '',\n                        'N',  '',\n                        ' (PREMIER)')                       merchant_number_enhanced,\n                m.dba_name                                  dba_name,\n                mif_date_opened(m.date_opened)              date_opened,\n                to_char(m.conversion_date, 'mm/dd/rr')      conversion_date,\n                m.activation_date                           activation_date,\n                m.last_deposit_date                         last_deposit_date,\n                m.last_deposit_amount                       last_deposit_amount,\n                m.dmagent                                   dmagent,\n                m.fdr_corp_name                             corporate_name,\n                gn7.group_name                              assoc_description,\n                m.bank_number                               bank_number,\n                m.bank_name_syf                             bank_name_syf,\n                m.sic_code                                  sic_code,\n                substr(sc.merchant_type, 1, 33)             sic_description,\n                m.group_1_association                       group_1,\n                substr(gn1.group_name, 1, 25)               group_1_description,\n                m.group_2_association                       group_2,\n                substr(gn2.group_name, 1, 25)               group_2_description,\n                m.group_3_association                       group_3,\n                substr(gn3.group_name, 1, 25)               group_3_description,\n                m.group_4_association                       group_4,\n                substr(gn4.group_name, 1, 25)               group_4_description,\n                m.group_5_association                       group_5,\n                substr(gn5.group_name, 1, 25)               group_5_description,\n                m.group_6_association                       group_6,\n                substr(gn6.group_name, 1, 25)               group_6_description,\n                m.phone_1                                   phone_1,\n                m.phone_2_fax                               phone_2_fax,\n                m.dda_num                                   dda_num,\n                lpad(m.transit_routng_num, 9, '0')          transit_routing,\n                m.date_stat_chgd_to_dcb                     status_date,\n                m.dmacctst                                  merchant_status,\n                m.user_data_1                               user_data_1,\n                m.user_data_2                               user_data_2,\n                m.user_data_3                               user_data_3,\n                m.user_data_4                               user_data_4,\n                m.user_data_5                               user_data_5,\n                m.user_acount_1                             user_acct_1,\n                m.user_account_2                            user_acct_2,\n                m.dmaddr                                    addr1_00,\n                m.address_line_3                            addr2_00,\n                m.dmcity                                    city_00,\n                m.dmstate                                   state_00,\n                m.dmzip                                     zip_00,\n                m.name1_line_1                              name_01,\n                m.addr1_line_1                              addr1_01,\n                m.addr1_line_2                              addr2_01,\n                m.city1_line_4                              city_01,\n                m.state1_line_4                             state_01,\n                m.zip1_line_4                               zip_01,\n                m.name2_line_1                              name_02,\n                m.addr2_line_1                              addr1_02,\n                m.addr2_line_2                              addr2_02,\n                m.city2_line_4                              city_02,\n                m.state2_line_4                             state_02,\n                m.zip2_line_4                               zip_02,\n                m.visa_plan                                 vs_plan,\n                m.mastcd_plan                               mc_plan,\n                m.diners_plan                               dc_plan,\n                m.jcb_plan                                  jc_plan,\n                m.amex_plan                                 am_plan,\n                m.discover_plan                             ds_plan,\n                m.debit_plan                                db_plan,\n                m.visa_cash_plan                            v$_plan,\n                m.mastcd_cash_plan                          m$_plan,\n                m.email_desc_00                             email_desc_00,\n                m.email_addr_00                             email_addr_00,\n                m.email_desc_01                             email_desc_01,\n                m.email_addr_01                             email_addr_01,\n                m.email_desc_02                             email_desc_02,\n                m.email_addr_02                             email_addr_02,\n                m.email_desc_03                             email_desc_03,\n                m.email_addr_03                             email_addr_03,\n                m.email_desc_04                             email_desc_04,\n                m.email_addr_04                             email_addr_04,\n                m.email_desc_05                             email_desc_05,\n                m.email_addr_05                             email_addr_05,\n                m.email_desc_06                             email_desc_06,\n                m.email_addr_06                             email_addr_06,\n                m.email_desc_07                             email_desc_07,\n                m.email_addr_07                             email_addr_07,\n                m.email_desc_08                             email_desc_08,\n                m.email_addr_08                             email_addr_08,\n                m.email_desc_09                             email_desc_09,\n                m.email_addr_09                             email_addr_09,\n                m.met_table                                 met_table,\n                nvl(substr(mtn.met_type||' '||\n                  mtn.ave_ticket||' ave tick, '||\n                  nvl(to_char(mtn.monthly_sales), '--')||' sales, '||\n                  dup_card_num||' dup card, '||\n                  nvl(dup_amount,0)||' dup amt, '||\n                  percent_keyed||'% keyed',\n                  1, 25), '---')                            met_description,\n                m.inv_code_investigate                      investigator_code,\n                nvl(m.rep_code,'n/a')                       rep_code,\n                -- pricing                                  \n                m.visa_disc_table                           vs_disc_table,\n                m.visa_disc_method                          vs_disc_method,\n                m.visa_disc_type                            vs_disc_type,\n                m.visa_disc_rate / 1000                     vs_disc_rate,\n                m.visa_per_item / 1000                      vs_per_item,\n                m.mastcd_disc_table                         mc_disc_table,\n                m.mastcd_disc_method                        mc_disc_method,\n                m.mastcd_disc_type                          mc_disc_type,\n                m.mastcd_disc_rate / 1000                   mc_disc_rate,\n                m.mastcd_per_item / 1000                    mc_per_item,\n                m.federal_tax_id                            federal_tax_id,\n                me.confirmation_code                        confirmation_code,\n                me.email_address                            enroll_email,\n                me.complete_date                            enroll_date,\n                merch.app_seq_num                           app_seq_num,\n                nvl(mpo_vtec.merchpo_tid,'N/A')             valutec_tid,\n                nvl(mpo_check.merchpo_tid,'N/A')            check_tid,\n                nvl(mpo_check.merchpo_provider_name,'N/A')  check_provider,\n                upper(merch.merch_legal_name)               merch_legal_name,\n                nvl(bat.account_type, 'CK')                 bankserv_acct_type,\n                nvl(m.daily_discount_interchange, 'N')      daily_discount,\n                nvl(m.minimum_monthly_discount, 0)          monthly_minimum,\n                nvl(m.daily_ach, 'Y')                       daily_ach,\n                nvl(m.suspended_days, 0)                    suspended_days,\n                nvl(m.processor_id,  :1 ) processor_id,\n                upper(nvl(m.mes_amex_processing, 'N'))      mes_amex_processing,\n                to_char(m.amex_conversion_date,'mm/dd/yyyy') amex_conversion_date,\n                decode(m.processor_id,\n                   :2 , '<font color=\"red\">MES</font>',\n                  'Vital')                                  processor,\n                decode( nvl(ppc.merchant_number,-1),\n                        -1, 'NONE',\n                        (ppc.payflow_login_name || '/' || ppc.pp_account_email_addr) )\n                                                            xpress_checkout_login,\n                decode(nvl(achp.enabled_count,0),0,'N','Y') achp_accepted\n        from    mif                           m,\n                merchant_enrollment           me,\n                merchant                      merch,\n                merchpayoption                mpo_vtec,\n                merchpayoption                mpo_check,\n                sic_codes                     sc,\n                met_tables_new                mtn,\n                group_names                   gn1,\n                group_names                   gn2,\n                group_names                   gn3,\n                group_names                   gn4,\n                group_names                   gn5,\n                group_names                   gn6,\n                group_names                   gn7,\n                merchant_sabre                ms,\n                bankserv_account_types        bat,\n                merchant_paypal_checkout      ppc,\n                ( select  tp.merchant_number,\n                          count(tpa.terminal_id) enabled_count\n                  from    trident_profile     tp,\n                          trident_profile_api tpa\n                  where   tp.merchant_number =  :3 \n                          and tp.terminal_id = tpa.terminal_id\n                          and ( lower(tpa.achp_ccd_accept) = 'y' or\n                                lower(tpa.achp_ppd_accept) = 'y' or\n                                lower(tpa.achp_web_accept) = 'y' or\n                                lower(tpa.achp_tel_accept) = 'y' )\n                  group by tp.merchant_number ) achp\n        where   m.merchant_number =  :4  and\n                m.merchant_number = me.merchant_number(+) and\n                m.merchant_number = merch.merch_number(+) and\n                m.sic_code = sc.sic_code(+) and\n                m.met_table = mtn.met_table_num(+) and\n                mpo_vtec.app_seq_num(+) = merch.app_seq_num and\n                mpo_vtec.cardtype_code(+) =  :5  and -- 22\n                mpo_check.app_seq_num(+) = merch.app_seq_num and\n                mpo_check.cardtype_code(+) =  :6  and -- 18\n                m.bank_number||m.group_1_association = gn1.group_number(+) and\n                m.bank_number||m.group_2_association = gn2.group_number(+) and\n                m.bank_number||m.group_3_association = gn3.group_number(+) and\n                m.bank_number||m.group_4_association = gn4.group_number(+) and\n                m.bank_number||m.group_5_association = gn5.group_number(+) and\n                m.bank_number||m.group_6_association = gn6.group_number(+) and\n                m.bank_number||m.dmagent             = gn7.group_number(+) and\n                merch.app_seq_num = ms.app_seq_num(+) and\n                m.merchant_number = bat.merchant_number(+) and\n                ppc.merchant_number(+) = m.merchant_number and\n                achp.merchant_number(+) = m.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.PROCESSOR_TSYS);
   __sJT_st.setInt(2,mesConstants.PROCESSOR_MES);
   __sJT_st.setString(3,__sJT_874);
   __sJT_st.setString(4,__sJT_875);
   __sJT_st.setInt(5,mesConstants.APP_CT_VALUTEC_GIFT_CARD);
   __sJT_st.setInt(6,mesConstants.APP_CT_CHECK_AUTH);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.maintenance.MerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:737^7*/

      rs = it.getResultSet();

      setFields(rs);
      
      // special descriptions and stuff
      setAccountStatus();
      
      setEnrollEmail();
      setEnrollDate();

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getMerchantData(" + getData("merchant") + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  protected void getSplitPricing()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      connect();
      
      // retrieve split pricing from monthly extract tables if present,
      // otherwise use MIF data (which isn't split but is at least accurate
      // for the consumer card rates
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:776^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(distinct pl.pl_disc_rate)
//          
//          from    monthly_extract_gn gn,
//                  monthly_extract_pl pl
//          where   gn.hh_merchant_number = :getData("merchant") and
//                  gn.hh_active_date = trunc((trunc(sysdate-2, 'month') - 1),'month') and
//                  gn.hh_load_sec = pl.hh_load_sec and
//                  pl.pl_plan_type in ('VISA', 'VSDB', 'VSBS', 'MC', 'MCDB', 'MCBS')      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_876 = getData("merchant");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(distinct pl.pl_disc_rate)\n         \n        from    monthly_extract_gn gn,\n                monthly_extract_pl pl\n        where   gn.hh_merchant_number =  :1  and\n                gn.hh_active_date = trunc((trunc(sysdate-2, 'month') - 1),'month') and\n                gn.hh_load_sec = pl.hh_load_sec and\n                pl.pl_plan_type in ('VISA', 'VSDB', 'VSBS', 'MC', 'MCDB', 'MCBS')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.maintenance.MerchantDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_876);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:786^7*/
      
      if(recCount > 1)
      {
        /*@lineinfo:generated-code*//*@lineinfo:790^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  pl.pl_plan_type           plan_type,
//                    pl.pl_disc_rate           disc_rate,
//                    pl.pl_disc_rate_per_item  per_item
//            from    monthly_extract_gn gn,
//                    monthly_extract_pl pl
//            where   gn.hh_merchant_number =  :getData("merchant") and
//                    gn.hh_active_date = trunc((trunc(sysdate-2, 'month') - 1),'month') and
//                    gn.hh_load_sec = pl.hh_load_sec and
//                    pl.pl_plan_type in ('VISA', 'VSDB', 'VSBS', 'MC', 'MCDB', 'MCBS')
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_877 = getData("merchant");
  try {
   String theSqlTS = "select  pl.pl_plan_type           plan_type,\n                  pl.pl_disc_rate           disc_rate,\n                  pl.pl_disc_rate_per_item  per_item\n          from    monthly_extract_gn gn,\n                  monthly_extract_pl pl\n          where   gn.hh_merchant_number =   :1  and\n                  gn.hh_active_date = trunc((trunc(sysdate-2, 'month') - 1),'month') and\n                  gn.hh_load_sec = pl.hh_load_sec and\n                  pl.pl_plan_type in ('VISA', 'VSDB', 'VSBS', 'MC', 'MCDB', 'MCBS')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_877);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.maintenance.MerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:801^9*/
        
        rs = it.getResultSet();
        
        String discRateField = "";
        String perItemField = "";
        while(rs.next())
        {
          discRateField = planLookup.get(rs.getString("plan_type")) + "DiscRate";
          perItemField  = planLookup.get(rs.getString("plan_type")) + "PerItem";
          
          setData(discRateField, rs.getString("disc_rate"));
          setData(perItemField,  rs.getString("per_item"));
        }
        
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("getSplitPricing()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  protected void getSupplementalData()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:839^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app.app_user_login                  app_user_login,
//                  decode(mc.merchcont_prim_first_name,
//                    null, upper(mf.other_name),
//                    upper(mc.merchcont_prim_first_name)||' '||
//                    upper(mc.merchcont_prim_last_name)) contact_name,
//                  mc.merchcont_prim_first_name        contact_first_name,
//                  mc.merchcont_prim_last_name         contact_last_name,
//                  mc.merchcont_prim_phone             contact_phone,
//                  merch.merc_cntrl_number             control_number,
//                  merch.merch_email_address           app_email_address,
//                  merch.merch_web_url                 app_web_address,
//                  merch.merch_web_url                 app_web_address_display,
//                  merch.merch_month_visa_mc_sales     monthly_sales,
//                  merch.merch_average_cc_tran         avg_ticket,
//                  merch.seasonal_flag                 seasonal_flag,
//                  mp.pos_param                        app_web_address_2,
//                  trunc(track.date_completed)         date_approved,
//                  lpad(bo.busowner_ssn,9,'0')         social_security_number,
//                  upper(bo.busowner_first_name)||' '||
//                    upper(bo.busowner_last_name)      owner_name
//          from    application                   app,
//                  merchcontact                  mc,
//                  merchant                      merch,
//                  app_tracking                  track,
//                  merch_pos                     mp,
//                  businessowner                 bo,
//                  mif                           mf
//          where   app.app_seq_num       = :getData("appSeqNum") and
//                  mc.app_seq_num(+)     = app.app_seq_num    and
//                  merch.app_seq_num     = app.app_seq_num and
//                  merch.merch_number    = mf.merchant_number and
//                  track.app_seq_num(+)  = app.app_seq_num and
//                  track.dept_code(+)    = 100 and
//                  app.app_seq_num       = mp.app_seq_num(+) and
//                  mp.pos_code(+) < :mesConstants.APP_MPOS_POS_PARTNER and
//                  app.app_seq_num       = bo.app_seq_num(+) and
//                  bo.busowner_num(+)    = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_878 = getData("appSeqNum");
  try {
   String theSqlTS = "select  app.app_user_login                  app_user_login,\n                decode(mc.merchcont_prim_first_name,\n                  null, upper(mf.other_name),\n                  upper(mc.merchcont_prim_first_name)||' '||\n                  upper(mc.merchcont_prim_last_name)) contact_name,\n                mc.merchcont_prim_first_name        contact_first_name,\n                mc.merchcont_prim_last_name         contact_last_name,\n                mc.merchcont_prim_phone             contact_phone,\n                merch.merc_cntrl_number             control_number,\n                merch.merch_email_address           app_email_address,\n                merch.merch_web_url                 app_web_address,\n                merch.merch_web_url                 app_web_address_display,\n                merch.merch_month_visa_mc_sales     monthly_sales,\n                merch.merch_average_cc_tran         avg_ticket,\n                merch.seasonal_flag                 seasonal_flag,\n                mp.pos_param                        app_web_address_2,\n                trunc(track.date_completed)         date_approved,\n                lpad(bo.busowner_ssn,9,'0')         social_security_number,\n                upper(bo.busowner_first_name)||' '||\n                  upper(bo.busowner_last_name)      owner_name\n        from    application                   app,\n                merchcontact                  mc,\n                merchant                      merch,\n                app_tracking                  track,\n                merch_pos                     mp,\n                businessowner                 bo,\n                mif                           mf\n        where   app.app_seq_num       =  :1  and\n                mc.app_seq_num(+)     = app.app_seq_num    and\n                merch.app_seq_num     = app.app_seq_num and\n                merch.merch_number    = mf.merchant_number and\n                track.app_seq_num(+)  = app.app_seq_num and\n                track.dept_code(+)    = 100 and\n                app.app_seq_num       = mp.app_seq_num(+) and\n                mp.pos_code(+) <  :2  and\n                app.app_seq_num       = bo.app_seq_num(+) and\n                bo.busowner_num(+)    = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_878);
   __sJT_st.setInt(2,mesConstants.APP_MPOS_POS_PARTNER);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.maintenance.MerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:878^7*/

      rs = it.getResultSet();

      setFields(rs);

      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getSupplementalData(" + getData("merchant") + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void addEmailAddress(StringBuffer dest, String addr)
  {
    try
    {
      if(addr != null && !addr.equals("") && addr.indexOf('@') != -1)
      {
        dest.append("<a href=\"mailto:");
        dest.append(addr.toLowerCase());
        dest.append("\">");
        dest.append(addr.toLowerCase());
        dest.append("</a>&nbsp;&nbsp;");
      }
    }
    catch(Exception e)
    {
      logEntry("addEmailAddress(" + addr + ")", e.toString());
    }
  }
  
  private void addWebAddress(StringBuffer dest, String addr)
  {
    try
    {
      if(!addr.equals("") && addr.indexOf('@') == -1)
      {
        dest.append("<a href=\"");
        if(addr.toLowerCase().indexOf("http") == -1)
        {
          dest.append("http://");
        }
        dest.append(addr.toLowerCase());
        dest.append("\">");
        dest.append(addr.toLowerCase());
        dest.append("</a>");
        dest.append("&nbsp;&nbsp;&nbsp;");
      }
    }
    catch(Exception e)
    {
      logEntry("addWebAddress("+ addr + ")", e.toString());
    }
  }
  
  protected String processBetString(String val)
  {
    String retVal = "";
    
    try
    {
      if(val == null)
      {
        retVal = "NONE";
      }
      else
      {
        retVal = NumberFormatter.getPaddedInt(Integer.parseInt(val), 4);
      }
    }
    catch(Exception e)
    {
      logEntry("processBetString(" + val + ")", e.toString());
    }
    
    return retVal;
  }
  
  protected void getBetTableData()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      // get auth plan bets
      if(getData("bankNumber").equals("3858"))
      {
        // set auth bet descriptions
        cbtAuthBets[CBT_IDX_AUTH_TERMINAL].description  = "Terminal";
        cbtAuthBets[CBT_IDX_AUTH_VOICE].description     = "Voice";
        cbtAuthBets[CBT_IDX_AUTH_ARU].description       = "ARU";
        cbtAuthBets[CBT_IDX_AUTH_ECR].description       = "ECR";
        cbtAuthBets[CBT_IDX_AUTH_BATCH].description     = "Batch";
        cbtAuthBets[CBT_IDX_AUTH_FIXED].description     = "Fixed";
        
        // CB&T has special authorization stuff they want to see
        /*@lineinfo:generated-code*//*@lineinfo:983^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(ap.a1_media_type, 
//                            'TE', 0, 
//                            'VO', 1, 
//                            'AR', 2, 
//                            'EC', 3,
//                            'BA', 4,
//                            'FX', 5, -1)                    media_type,
//                    mod(to_number(ap.a1_bet_table_id), 100) bet_table,        
//                    ap.a1_plan_type                         plan_type
//            from    monthly_extract_summary sm,
//                    monthly_extract_ap      ap
//            where   sm.merchant_number = :getData("merchant") and
//                    sm.active_date = trunc((trunc(sysdate-2, 'month') - 1), 'month') and
//                    sm.hh_load_sec = ap.hh_load_sec and
//                    ap.a1_vendor_code = 'VI'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_879 = getData("merchant");
  try {
   String theSqlTS = "select  decode(ap.a1_media_type, \n                          'TE', 0, \n                          'VO', 1, \n                          'AR', 2, \n                          'EC', 3,\n                          'BA', 4,\n                          'FX', 5, -1)                    media_type,\n                  mod(to_number(ap.a1_bet_table_id), 100) bet_table,        \n                  ap.a1_plan_type                         plan_type\n          from    monthly_extract_summary sm,\n                  monthly_extract_ap      ap\n          where   sm.merchant_number =  :1  and\n                  sm.active_date = trunc((trunc(sysdate-2, 'month') - 1), 'month') and\n                  sm.hh_load_sec = ap.hh_load_sec and\n                  ap.a1_vendor_code = 'VI'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_879);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.maintenance.MerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1000^9*/
        
        rs = it.getResultSet();
        
        int curIdx    = 0;
        while(rs.next())
        {
          curIdx = rs.getInt("media_type");
          
          if(curIdx >= 0)
          {
            if(rs.getString("plan_type").equals("VS"))
            {
              cbtAuthBets[curIdx].visaAuth = "$." + rs.getString("bet_table");
            }
            else if(rs.getString("plan_type").equals("MC"))
            {
              cbtAuthBets[curIdx].mcAuth = "$." + rs.getString("bet_table");
            }
            else if(rs.getString("plan_type").equals("AM"))
            {
              cbtAuthBets[curIdx].amexAuth = "$." + rs.getString("bet_table");
            }
            else if(rs.getString("plan_type").equals("DS"))
            {
              cbtAuthBets[curIdx].discoverAuth = "$." + rs.getString("bet_table");
            }
            else if(rs.getString("plan_type").equals("DC"))
            {
              cbtAuthBets[curIdx].dinersAuth = "$." + rs.getString("bet_table");
            }
            else if(rs.getString("plan_type").equals("DB"))
            {
              cbtAuthBets[curIdx].debitAuth = "$." + rs.getString("bet_table");
            }
          }
        }
        
        rs.close();
        it.close();
        
        // get debit peritem (from debit bet number)
        /*@lineinfo:generated-code*//*@lineinfo:1042^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mod(to_number(dn.n1_bet_table_number), 100)/100 bet_table
//            from    monthly_extract_dn      dn,
//                    monthly_extract_summary sm
//            where   sm.merchant_number = :getData("merchant") and
//                    sm.active_date = trunc((trunc(sysdate-2, 'month') - 1), 'month') and
//                    sm.hh_load_sec = dn.hh_load_sec
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_880 = getData("merchant");
  try {
   String theSqlTS = "select  mod(to_number(dn.n1_bet_table_number), 100)/100 bet_table\n          from    monthly_extract_dn      dn,\n                  monthly_extract_summary sm\n          where   sm.merchant_number =  :1  and\n                  sm.active_date = trunc((trunc(sysdate-2, 'month') - 1), 'month') and\n                  sm.hh_load_sec = dn.hh_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_880);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.maintenance.MerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1050^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          setData("debitPerItem", rs.getString("bet_table"));
        }
        
        rs.close();
        it.close();
        
        // get ebt peritem
        /*@lineinfo:generated-code*//*@lineinfo:1063^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  pl.pl_disc_rate_per_item    per_item
//            from    monthly_extract_pl          pl,
//                    monthly_extract_summary     sm
//            where   sm.merchant_number = :getData("merchant") and
//                    sm.active_date = trunc((trunc(sysdate-2, 'month') - 1), 'month') and
//                    sm.hh_load_sec = pl.hh_load_sec and
//                    pl.pl_plan_type = 'EBT'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_881 = getData("merchant");
  try {
   String theSqlTS = "select  pl.pl_disc_rate_per_item    per_item\n          from    monthly_extract_pl          pl,\n                  monthly_extract_summary     sm\n          where   sm.merchant_number =  :1  and\n                  sm.active_date = trunc((trunc(sysdate-2, 'month') - 1), 'month') and\n                  sm.hh_load_sec = pl.hh_load_sec and\n                  pl.pl_plan_type = 'EBT'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_881);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.maintenance.MerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1072^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          setData("ebtPerItem", rs.getString("per_item"));
        }
        
        rs.close();
        it.close();
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1086^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct(ap.a1_bet_table_id)      auth_bet_number
//            from    monthly_extract_summary   sm,
//                    monthly_extract_ap        ap
//            where   sm.merchant_number = :getData("merchant") and
//                    sm.active_date = trunc((trunc(sysdate-2, 'month') - 1),'month') and
//                    sm.hh_load_sec = ap.hh_load_sec
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_882 = getData("merchant");
  try {
   String theSqlTS = "select  distinct(ap.a1_bet_table_id)      auth_bet_number\n          from    monthly_extract_summary   sm,\n                  monthly_extract_ap        ap\n          where   sm.merchant_number =  :1  and\n                  sm.active_date = trunc((trunc(sysdate-2, 'month') - 1),'month') and\n                  sm.hh_load_sec = ap.hh_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_882);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.maintenance.MerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1094^9*/
      
        rs = it.getResultSet();
      
        if(rs.next())
        {
          do
          {
            authBets.add(processBetString(rs.getString("auth_bet_number")));
          } while (rs.next());
        }
        else
        {
          authBets.add("NONE");
        }
      
        rs.close();
        it.close();
      }
      
      // get data capture plan bets
      /*@lineinfo:generated-code*//*@lineinfo:1115^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cp.c1_bet_table_id              capture_bet_number
//          from    monthly_extract_summary   sm,
//                  monthly_extract_c1        cp        
//          where   sm.merchant_number = :getData("merchant") and
//                  sm.active_date = trunc((trunc(sysdate-2, 'month') - 1),'month') and
//                  sm.hh_load_sec = cp.hh_load_sec        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_883 = getData("merchant");
  try {
   String theSqlTS = "select  cp.c1_bet_table_id              capture_bet_number\n        from    monthly_extract_summary   sm,\n                monthly_extract_c1        cp        \n        where   sm.merchant_number =  :1  and\n                sm.active_date = trunc((trunc(sysdate-2, 'month') - 1),'month') and\n                sm.hh_load_sec = cp.hh_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_883);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.maintenance.MerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1123^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        do
        {
          dataCaptureBets.add(processBetString(rs.getString("capture_bet_number")));
        } while (rs.next());
      }
      else
      {
        dataCaptureBets.add("NONE");
      }
      
      rs.close();
      it.close();
      
      
      // get individual plan bets
      /*@lineinfo:generated-code*//*@lineinfo:1144^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct(pl.pl_bet_table_number)  individual_plan_bet_number
//          from    monthly_extract_summary   sm,
//                  monthly_extract_pl        pl
//          where   sm.merchant_number = :getData("merchant") and
//                  sm.active_date = trunc((trunc(sysdate-2, 'month') - 1),'month') and
//                  sm.hh_load_sec = pl.hh_load_sec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_884 = getData("merchant");
  try {
   String theSqlTS = "select  distinct(pl.pl_bet_table_number)  individual_plan_bet_number\n        from    monthly_extract_summary   sm,\n                monthly_extract_pl        pl\n        where   sm.merchant_number =  :1  and\n                sm.active_date = trunc((trunc(sysdate-2, 'month') - 1),'month') and\n                sm.hh_load_sec = pl.hh_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_884);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.maintenance.MerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1152^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        do
        {
          planBets.add(processBetString(rs.getString("individual_plan_bet_number")));
        } while (rs.next());
      }
      else
      {
        planBets.add("NONE");
      }
      
      rs.close();
      it.close();
      
      // get interchange and system generated bets
      /*@lineinfo:generated-code*//*@lineinfo:1172^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  gn.s1_bet_table_number          as system_generated_bet,
//                  mf.ic_bet_visa                  as visa_ic_bet,
//                  mf.ic_bet_mc                    as mc_ic_bet,
//                  dc.d1_bet_table_number          as diners_ic_bet
//          from    mif                       mf,
//                  monthly_extract_gn        gn,
//                  monthly_extract_dc        dc        
//          where   mf.merchant_number = :getData("merchant") 
//                  and mf.merchant_number = gn.hh_merchant_number(+)
//                  and gn.hh_active_date(+) = trunc((trunc(sysdate-2, 'month') - 1),'month')
//                  and gn.hh_load_sec = dc.hh_load_sec(+)   
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_885 = getData("merchant");
  try {
   String theSqlTS = "select  gn.s1_bet_table_number          as system_generated_bet,\n                mf.ic_bet_visa                  as visa_ic_bet,\n                mf.ic_bet_mc                    as mc_ic_bet,\n                dc.d1_bet_table_number          as diners_ic_bet\n        from    mif                       mf,\n                monthly_extract_gn        gn,\n                monthly_extract_dc        dc        \n        where   mf.merchant_number =  :1  \n                and mf.merchant_number = gn.hh_merchant_number(+)\n                and gn.hh_active_date(+) = trunc((trunc(sysdate-2, 'month') - 1),'month')\n                and gn.hh_load_sec = dc.hh_load_sec(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_885);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.maintenance.MerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1185^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        systemGeneratedBet  = processBetString(rs.getString("system_generated_bet"));
        visaInterchangeBet  = processBetString(rs.getString("visa_ic_bet"));
        mcInterchangeBet    = processBetString(rs.getString("mc_ic_bet"));
        dinersInterchangeBet= processBetString(rs.getString("diners_ic_bet"));
      }
      else
      {
        systemGeneratedBet  = "NONE";
        visaInterchangeBet  = "NONE";
        mcInterchangeBet    = "NONE";
        dinersInterchangeBet= "NONE";
      }
      
      rs.close();
      it.close();
      
      // if this is a cb&t merchant, get the interchange bet descriptions
      if(getData("bankNumber").equals("3858"))
      {
        try
        {
          // visa interchange bet description
          /*@lineinfo:generated-code*//*@lineinfo:1213^11*/

//  ************************************************************
//  #sql [Ctx] { select 'Visa ' || label
//              
//              from    appo_ic_bets
//              where   app_type = 31 and
//                      vs_bet = :visaInterchangeBet
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select 'Visa ' || label\n             \n            from    appo_ic_bets\n            where   app_type = 31 and\n                    vs_bet =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.maintenance.MerchantDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,visaInterchangeBet);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   visaInterchangeDesc = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1220^11*/
        }
        catch(Exception e)
        {
          visaInterchangeDesc = "Description Not Found (" + visaInterchangeBet + ")";
        }
        
        try
        {
          // mc interchange bet description
          /*@lineinfo:generated-code*//*@lineinfo:1230^11*/

//  ************************************************************
//  #sql [Ctx] { select 'MC ' || label
//              
//              from    appo_ic_bets
//              where   app_type = 31 and
//                      mc_bet = :mcInterchangeBet
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select 'MC ' || label\n             \n            from    appo_ic_bets\n            where   app_type = 31 and\n                    mc_bet =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.maintenance.MerchantDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,mcInterchangeBet);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mcInterchangeDesc = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1237^11*/
        }
        catch(Exception e)
        {
          mcInterchangeDesc = "Description Not Found (" + mcInterchangeBet + ")";
        }
      }
      
      // get debit network bets
      /*@lineinfo:generated-code*//*@lineinfo:1246^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct(dn.n1_bet_table_number)  debit_bet_number
//          from    monthly_extract_summary   sm,
//                  monthly_extract_dn        dn
//          where   sm.merchant_number = :getData("merchant") and
//                  sm.active_date = trunc((trunc(sysdate-2, 'month') - 1),'month') and
//                  sm.hh_load_sec = dn.hh_load_sec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_886 = getData("merchant");
  try {
   String theSqlTS = "select  distinct(dn.n1_bet_table_number)  debit_bet_number\n        from    monthly_extract_summary   sm,\n                monthly_extract_dn        dn\n        where   sm.merchant_number =  :1  and\n                sm.active_date = trunc((trunc(sysdate-2, 'month') - 1),'month') and\n                sm.hh_load_sec = dn.hh_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_886);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"23com.mes.maintenance.MerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1254^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        do
        {
          debitBets.add(processBetString(rs.getString("debit_bet_number")));
        } while (rs.next());
      }
      else
      {
        debitBets.add("NONE");
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getBetTableData(" + getData("merchant") + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void getBankProcedures()
  {
    ResultSetIterator     it = null;
    ResultSet             rs = null;
    
    try
    {
      // load bank procedures if present
      /*@lineinfo:generated-code*//*@lineinfo:1292^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bpu.url             bank_procedures_url
//          from    bank_procedures_url bpu,
//                  t_hierarchy         th,
//                  mif                 m
//          where   m.merchant_number = :getData("merchant") and
//                  m.association_node = th.descendent and
//                  th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                  th.ancestor = bpu.hierarchy_node
//          order by relation asc      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_887 = getData("merchant");
  try {
   String theSqlTS = "select  bpu.url             bank_procedures_url\n        from    bank_procedures_url bpu,\n                t_hierarchy         th,\n                mif                 m\n        where   m.merchant_number =  :1  and\n                m.association_node = th.descendent and\n                th.hier_type =  :2  and\n                th.ancestor = bpu.hierarchy_node\n        order by relation asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_887);
   __sJT_st.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"24com.mes.maintenance.MerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1303^7*/
      
      rs = it.getResultSet();
      if ( rs.next() )
      {
        setFields(rs,false);
      }
              
      bankProceduresUrl = getData("bankProceduresUrl");

      rs.close();
      it.close();      
    }
    catch(Exception e)
    {
      logEntry("getBankProcedures(" + getData("merchant") + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

  public static boolean __userNotAllowedToSeeMerchant(UserBean user, String merchant)
  {
    MerchantDataBean worker = new MerchantDataBean();
    
    return( worker.userNotAllowedToSeeMerchant(user, merchant) );
  }
  public boolean userNotAllowedToSeeMerchant(UserBean user, String merchant)
  {
    boolean   retVal        = false;
    long      userNode      = 0L;
    boolean   useAppFilter  = false;

    try
    {
      connect();

      int merchantCount = 0;

      // if this merchant isn't even in the mif yet, don't bother checking
      /*@lineinfo:generated-code*//*@lineinfo:1346^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//          
//          from    mif
//          where   merchant_number = :merchant
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n         \n        from    mif\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.maintenance.MerchantDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchant);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1352^7*/

      if(merchantCount > 0)
      {
        if(user.getReportHierarchyNode() > 0)
        {
          userNode      = user.getReportHierarchyNode();
          useAppFilter  = true;
        }
        else
        {
          userNode      = user.getHierarchyNode();
          useAppFilter  = false;
        }
        
        if(useAppFilter)
        {
          // very simple - they can see anyone in the group_rep_merchant table
          // that matches their user id
          /*@lineinfo:generated-code*//*@lineinfo:1371^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//              
//              from    group_rep_merchant
//              where   user_id = :user.getUserId()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_888 = user.getUserId();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n             \n            from    group_rep_merchant\n            where   user_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.maintenance.MerchantDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_888);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1377^11*/
        }
        else
        {
          // get the entity type to help determine whether this account should be
          // viewable
          int   entityType  =   0;
          long  bankNumber  =   0L;

          /*@lineinfo:generated-code*//*@lineinfo:1386^11*/

//  ************************************************************
//  #sql [Ctx] { select  entity_type
//              
//              from    t_hierarchy
//              where   hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                      ancestor = :userNode and
//                      descendent = :userNode
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  entity_type\n             \n            from    t_hierarchy\n            where   hier_type =  :1  and\n                    ancestor =  :2  and\n                    descendent =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.maintenance.MerchantDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(2,userNode);
   __sJT_st.setLong(3,userNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   entityType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1394^11*/

          if(entityType == MesHierarchy.ET_BANK_NODE)
          {
            // get the bank number from the user node
            bankNumber = userNode / 100000L;
          }

          switch(entityType)
          {
            case MesHierarchy.ET_SUPER_NODE:
              merchantCount = 1;
              break;

            case MesHierarchy.ET_BANK_NODE:
              // they can see any merchant under their bank number
              /*@lineinfo:generated-code*//*@lineinfo:1410^15*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//                  
//                  from    mif
//                  where   merchant_number = :merchant and
//                          bank_number = :bankNumber
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n                 \n                from    mif\n                where   merchant_number =  :1  and\n                        bank_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.maintenance.MerchantDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchant);
   __sJT_st.setLong(2,bankNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1417^15*/
              break;

            default:
              // user can only see merchants under them in the hierarchy
              /*@lineinfo:generated-code*//*@lineinfo:1422^15*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//                  
//                  from    mif
//                  where   merchant_number = :merchant and
//                          dmagent in
//                          (
//                            select  m.dmagent
//                            from    t_hierarchy th,
//                                    mif         m
//                            where   th.hier_type = :MesHierarchy.HT_BANK_PORTFOLIOS and
//                                    th.descendent = ((m.bank_number * 1000000) + m.dmagent) and
//                                    th.ancestor = :userNode
//                          )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n                 \n                from    mif\n                where   merchant_number =  :1  and\n                        dmagent in\n                        (\n                          select  m.dmagent\n                          from    t_hierarchy th,\n                                  mif         m\n                          where   th.hier_type =  :2  and\n                                  th.descendent = ((m.bank_number * 1000000) + m.dmagent) and\n                                  th.ancestor =  :3 \n                        )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.maintenance.MerchantDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchant);
   __sJT_st.setInt(2,MesHierarchy.HT_BANK_PORTFOLIOS);
   __sJT_st.setLong(3,userNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1437^15*/
              break;
          }
        }

        if(merchantCount == 0)
        {
          retVal = true;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("userNotAllowedToSeeMerchant()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return retVal;
  }
  
  private void addEmailAddresses()
  {
    ResultSetIterator it        = null;
    ResultSet         rs        = null;
    StringBuffer      addrs     = new StringBuffer("");
    StringBuffer      webAddrs  = new StringBuffer("");
    
    try
    {
      connect();
      
      // mif
      for(int i=0; i<10; ++i)
      {
        addEmailAddress(addrs, getData("emailAddr0"+Integer.toString(i)));
      }
      setData("mifEmailAddresses", addrs.toString());
    
      // merchant
      addrs.setLength(0);
      addEmailAddress(addrs, getData("appEmailAddress"));
      setData("appEmailAddresses", addrs.toString());
      
      addrs.setLength(0);
      
      if(hasData("appWebAddress"))
      {
        addWebAddress(addrs, getData("appWebAddress"));
      }
      else
      {
        addWebAddress(addrs, getData("appWebAddress2"));
      }
      
      // overwrite appWebAddress with the true web address
      setData("appWebAddress", addrs.toString());
    
      // user
      addrs.setLength(0);
      StringBuffer displayAddrs = new StringBuffer("");
      /*@lineinfo:generated-code*//*@lineinfo:1500^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct email
//          from    users
//          where   hierarchy_node = :getData("merchant") and
//                  email is not null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_889 = getData("merchant");
  try {
   String theSqlTS = "select  distinct email\n        from    users\n        where   hierarchy_node =  :1  and\n                email is not null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.maintenance.MerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_889);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"30com.mes.maintenance.MerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1506^7*/
      
      rs = it.getResultSet();
      
      int  count = 0;
      while(rs.next())
      {
        ++count;
        addEmailAddress(addrs, rs.getString("email"));
        
        if(count > 1)
        {
          displayAddrs.append(";");
        }
        
        displayAddrs.append(rs.getString("email"));
      }
      
      setData("userEmailAddresses", addrs.toString());
      setData("userEmailAddressDisplay", displayAddrs.toString());
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("addEmailAddresses()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public void loadData()
  {
    try
    {
      connect();

      getMerchantData();
      
      // only get supplemental (application) data if it is known to exist
      if(!getData("appSeqNum").equals(""))
      {
        getSupplementalData();
      }
      
      // load email addresses into single stringbuffers
      addEmailAddresses();

      // get bet table information
      getBetTableData();
      
      // get bank procedures
      getBankProcedures();
      
      // get full pricing details (for banks that use split pricing)
      if( ! ("1").equals( getData("processorId") ) )
      {
        getSplitPricing();
      }
    }
    catch(Exception e)
    {
      logEntry("loadData(" + getData("merchant") + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void setAccountStatus()
  {
    StringBuffer  buf = new StringBuffer("");

    if(!getData("merchantStatus").equals(""))
    {
      buf.append("<font color=\"red\"> ");
      buf.append(getField("merchantStatus").getRenderData());
      buf.append(" ");
      if(!getData("statusDate").equals(""))
      {
        buf.append(" (");
        buf.append(getField("statusDate").getRenderData());
        buf.append(")");
      }
      buf.append("</font>");
    }
    else if(!getData("statusDate").equals(""))
    {
      buf.append("<font color=\"red\">STATUSED (");
      buf.append(getField("statusDate").getRenderData());
      buf.append(")</font>");
    }
    else if(! getData("activationDate").equals(""))
    {
      buf.append("<font color=\"green\">ACTIVATED (");
      buf.append(getField("activationDate").getRenderData());
      buf.append(")</font>");
    }
    else
    {
      buf.append("<font color=\"red\">NOT ACTIVATED</font>");
    }

    setData("accountStatus", buf.toString());
  }
  
  public void setEnrollEmail()
  {
    if(getData("enrollEmail").equals(""))
    {
      setData("enrollEmail", "<font color=\"red\">NOT ENROLLED</font>");
    }
    else
    {
      setData("enrollEmail", "<a href=\"mailto:" + getData("enrollEmail") + "\">" + getData("enrollEmail") + "</a>");
    }
  }
  public void setEnrollDate()
  {
    if(getData("enrollDate").equals(""))
    {
      if(!getData("enrollEmail").equals(""))
      {
        setData("enrollDate", "<font color=\"red\">ENROLLMENT INCOMPLETE</font>");
      }
      else
      {
        setData("enrollDate", "&nbsp;");
      }
    }
  }

  public String getCardPlans()
  {
    StringBuffer      result      = new StringBuffer("");
    String            preColor    = "<font color=\"red\">";
    String            postColor   = "</font>";

    // visa plan
    if(! getData("vsPlan").equals(""))
    {
      result.append(preColor);
      result.append("VS:");
      result.append(postColor);
      result.append(getData("vsPlan"));
    }

    if(! getData("v$Plan").equals(""))
    {
      result.append(preColor);
      result.append(" V$:");
      result.append(postColor);
      result.append(getData("v$Plan"));
    }

    if(! getData("mcPlan").equals(""))
    {
      result.append(preColor);
      result.append(" MC:");
      result.append(postColor);
      result.append(getData("mcPlan"));
    }

    if(! getData("m$Plan").equals(""))
    {
      result.append(preColor);
      result.append(" M$:" );
      result.append(postColor);
      result.append(getData("m$Plan"));
    }

    if(! getData("amPlan").equals(""))
    {
      result.append(preColor);
      result.append(" AM:");
      result.append(postColor);
      result.append(getData("amPlan"));
    }

    if(! getData("dsPlan").equals(""))
    {
      result.append(preColor);
      result.append(" DS:");
      result.append(postColor);
      result.append(getData("dsPlan"));
    }

    if(! getData("dcPlan").equals(""))
    {
      result.append(preColor);
      result.append(" DC:");
      result.append(postColor);
      result.append(getData("dcPlan"));
    }

    if(! getData("dbPlan").equals(""))
    {
      result.append(preColor);
      result.append(" DB:");
      result.append(postColor);
      result.append(getData("dbPlan"));
    }

    if(! getData("jcPlan").equals(""))
    {
      result.append(preColor);
      result.append(" JC:");
      result.append(postColor);
      result.append(getData("jcPlan"));
    }

    return result.toString();
  }
  
  public class CBTAuthBet
  {
    public String   description   = "";
    public String   visaAuth      = "--";
    public String   mcAuth        = "--";
    public String   amexAuth      = "--";
    public String   discoverAuth  = "--";
    public String   dinersAuth    = "--";
    public String   debitAuth     = "--";
    
    public CBTAuthBet()
    {
    }
  }
  
  protected class BankServAcctTypeTable extends DropDownTable
  {
    public BankServAcctTypeTable()
    {
      addElement("CK", "CK");
      addElement("GL", "GL");
      addElement("SV", "SV");
    }
  }
  
  protected class DailyDiscountICTable extends DropDownTable
  {
    public DailyDiscountICTable()
    {
      addElement("N", "N");
      addElement("D", "D");
      addElement("I", "I");
      addElement("B", "B");
    }
  }
}/*@lineinfo:generated-code*/