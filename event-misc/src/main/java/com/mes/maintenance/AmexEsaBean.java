/*@lineinfo:filename=AmexEsaBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/AmexEsaBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/27/02 2:37p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class AmexEsaBean extends SQLJConnectionBase
{
  public long     appSeqNum         = 0;
  public String   merchantNumber    = "";
  public String   spid              = "";
  public String   esa               = "7";
  public String   businessName      = "";
  public String   dba               = "";
  public String   address           = "";
  public String   city              = "";
  public String   state             = "";
  public String   zip               = "";
  public String   phone             = "";
  public String   contact           = "";
  public String   sicCode           = "";
  public String   signerName        = "";
  public String   signerAddress     = "";
  public String   signerCity        = "";
  public String   signerState       = "";
  public String   signerZip         = "";
  public String   signerSSN         = "";
  public String   taxId             = "";
  public String   signerPhone       = "";
  public String   rate              = "";
  public String   payFreq           = "3";
  public String   amexVolume        = "";
  public String   avgTicket         = "";
  public String   cutoff            = "30";
  public String   achType           = "C";
  public String   aba               = "";
  public String   dda               = "";
  public String   accountName       = "";
  public String   bankName          = "";
  public String   bankAddress       = "";
  public String   bankCity          = "";
  public String   bankState         = "";
  public String   bankZip           = "";
  public String   siteInspection    = "X";
  public String   ownership         = "";
  public String   timeWithOwner     = "0";
  
  public boolean  submitted         = false;
  
  public boolean  formSubmitted     = false;
  public String   submitDate        = null;
  
  public AmexEsaBean()
  {
  }
  
  public void loadData()
  {
    ResultSetIterator     it      = null;
    try
    {
      connect();
      
      // set the date submitted if user clicked the form
      if(submitted)
      {
        int recCount;
        
        /*@lineinfo:generated-code*//*@lineinfo:95^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//            from    amex_esa_submissions
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)  \n          from    amex_esa_submissions\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.AmexEsaBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:100^9*/
        
        // insert the record if it isn't already there
        if(recCount == 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:105^11*/

//  ************************************************************
//  #sql [Ctx] { insert into amex_esa_submissions
//              (
//                app_seq_num,
//                submit_date
//              )
//              values
//              (
//                :appSeqNum,
//                sysdate
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into amex_esa_submissions\n            (\n              app_seq_num,\n              submit_date\n            )\n            values\n            (\n               :1 ,\n              sysdate\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.maintenance.AmexEsaBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:117^11*/
        }
      }
      
      // get all the ESA data
      /*@lineinfo:generated-code*//*@lineinfo:122^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.app_seq_num                             app_seq_num,
//                  m.merch_number                            merchant_number,
//                  a.appsrctype_code||'/'||a.app_user_login  spid,
//                  m.merch_legal_name                        business_name,
//                  m.merch_business_name                     dba_name,
//                  a1.address_line1||' '||a1.address_line2   business_address,
//                  a1.address_city                           business_city,
//                  a1.countrystate_code                      business_state,
//                  a1.address_zip                            business_zip,
//                  a1.address_phone                          business_phone,
//                  mc.merchcont_prim_first_name||' '||mc.merchcont_prim_last_name    contact_name,
//                  m.sic_code                                sic_code,
//                  bo.busowner_first_name||' '||bo.busowner_last_name                signer_name,
//                  a2.address_line1||' '||a2.address_line2   signer_address,
//                  a2.address_city                           signer_city,
//                  a2.countrystate_code                      signer_state,
//                  a2.address_zip                            signer_zip,
//                  bo.busowner_ssn                           signer_ssn,
//                  m.merch_federal_tax_id                    tax_id,
//                  a2.address_phone                          signer_phone,
//                  mpo.merchpo_rate                          amex_rate,
//                  m.merch_month_visa_mc_sales * 12          annual_volume,
//                  m.merch_average_cc_tran                   average_ticket,
//                  mb.merchbank_transit_route_num            bank_aba,
//                  mb.merchbank_acct_num                     bank_dda,
//                  m.merch_legal_name                        account_name,
//                  mb.merchbank_name                         bank_name,
//                  a3.address_line1                          bank_address,
//                  a3.address_city                           bank_city,
//                  a3.countrystate_code                      bank_state,
//                  a3.address_zip                            bank_zip,
//                  decode( m.bustype_code,
//                          1, 's',
//                          2, 'c',
//                          3, 'P',
//                          'Other')                          ownership                            
//          from    merchant        m,
//                  application     a,
//                  address         a1,
//                  merchcontact    mc,
//                  businessowner   bo,
//                  address         a2,
//                  merchpayoption  mpo,
//                  merchbank       mb,
//                  address         a3
//          where   m.app_seq_num = :appSeqNum and
//                  m.app_seq_num = a.app_seq_num and
//                  m.app_seq_num = a1.app_seq_num(+) and
//                  a1.addresstype_code = 1 and
//                  m.app_seq_num = mc.app_seq_num(+) and
//                  m.app_seq_num = bo.app_seq_num(+) and
//                  bo.busowner_num = 1 and
//                  m.app_seq_num = a2.app_seq_num(+) and
//                  a2.addresstype_code = 4 and
//                  m.app_seq_num = mpo.app_seq_num(+) and
//                  mpo.cardtype_code = 16 and
//                  m.app_seq_num = mb.app_seq_num(+) and
//                  m.app_seq_num = a3.app_seq_num(+) and
//                  a3.addresstype_code = 9      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.app_seq_num                             app_seq_num,\n                m.merch_number                            merchant_number,\n                a.appsrctype_code||'/'||a.app_user_login  spid,\n                m.merch_legal_name                        business_name,\n                m.merch_business_name                     dba_name,\n                a1.address_line1||' '||a1.address_line2   business_address,\n                a1.address_city                           business_city,\n                a1.countrystate_code                      business_state,\n                a1.address_zip                            business_zip,\n                a1.address_phone                          business_phone,\n                mc.merchcont_prim_first_name||' '||mc.merchcont_prim_last_name    contact_name,\n                m.sic_code                                sic_code,\n                bo.busowner_first_name||' '||bo.busowner_last_name                signer_name,\n                a2.address_line1||' '||a2.address_line2   signer_address,\n                a2.address_city                           signer_city,\n                a2.countrystate_code                      signer_state,\n                a2.address_zip                            signer_zip,\n                bo.busowner_ssn                           signer_ssn,\n                m.merch_federal_tax_id                    tax_id,\n                a2.address_phone                          signer_phone,\n                mpo.merchpo_rate                          amex_rate,\n                m.merch_month_visa_mc_sales * 12          annual_volume,\n                m.merch_average_cc_tran                   average_ticket,\n                mb.merchbank_transit_route_num            bank_aba,\n                mb.merchbank_acct_num                     bank_dda,\n                m.merch_legal_name                        account_name,\n                mb.merchbank_name                         bank_name,\n                a3.address_line1                          bank_address,\n                a3.address_city                           bank_city,\n                a3.countrystate_code                      bank_state,\n                a3.address_zip                            bank_zip,\n                decode( m.bustype_code,\n                        1, 's',\n                        2, 'c',\n                        3, 'P',\n                        'Other')                          ownership                            \n        from    merchant        m,\n                application     a,\n                address         a1,\n                merchcontact    mc,\n                businessowner   bo,\n                address         a2,\n                merchpayoption  mpo,\n                merchbank       mb,\n                address         a3\n        where   m.app_seq_num =  :1  and\n                m.app_seq_num = a.app_seq_num and\n                m.app_seq_num = a1.app_seq_num(+) and\n                a1.addresstype_code = 1 and\n                m.app_seq_num = mc.app_seq_num(+) and\n                m.app_seq_num = bo.app_seq_num(+) and\n                bo.busowner_num = 1 and\n                m.app_seq_num = a2.app_seq_num(+) and\n                a2.addresstype_code = 4 and\n                m.app_seq_num = mpo.app_seq_num(+) and\n                mpo.cardtype_code = 16 and\n                m.app_seq_num = mb.app_seq_num(+) and\n                m.app_seq_num = a3.app_seq_num(+) and\n                a3.addresstype_code = 9";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.AmexEsaBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.maintenance.AmexEsaBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:183^7*/
      
      ResultSet rs = it.getResultSet();
      
      if(rs.next())
      {
        merchantNumber  = rs.getString("merchant_number");
        spid            = rs.getString("spid");
        businessName    = rs.getString("business_name");
        dba             = rs.getString("dba_name");
        address         = rs.getString("business_address");
        city            = rs.getString("business_city");
        state           = rs.getString("business_state");
        zip             = rs.getString("business_zip");
        phone           = rs.getString("business_phone");
        contact         = rs.getString("contact_name");
        sicCode         = rs.getString("sic_code");
        signerName      = rs.getString("signer_name");
        signerAddress   = rs.getString("signer_address");
        signerCity      = rs.getString("signer_city");
        signerState     = rs.getString("signer_state");
        signerZip       = rs.getString("signer_zip");
        signerSSN       = rs.getString("signer_ssn");
        taxId           = rs.getString("tax_id");
        signerPhone     = rs.getString("signer_phone");
        rate            = rs.getString("amex_rate");
        amexVolume      = rs.getString("annual_volume");
        avgTicket       = rs.getString("average_ticket");
        aba             = rs.getString("bank_aba");
        dda             = rs.getString("bank_dda");
        accountName     = rs.getString("account_name");
        bankName        = rs.getString("bank_name");
        bankAddress     = rs.getString("bank_address");
        
        if(bankAddress == null || bankAddress.trim().equals(""))
        {
          bankAddress = "123 Main St.";
        }
        
        bankCity        = rs.getString("bank_city");
        bankState       = rs.getString("bank_state");
        bankZip         = rs.getString("bank_zip");
        ownership       = rs.getString("ownership");
      }
      
      rs.close();
      it.close();
      
      // determine if this information has already been established
      /*@lineinfo:generated-code*//*@lineinfo:232^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num,
//                  submit_date
//          from    amex_esa_submissions
//          where   app_seq_num = :this.appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num,\n                submit_date\n        from    amex_esa_submissions\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.AmexEsaBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,this.appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.maintenance.AmexEsaBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:238^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        // someone already submitted this one
        formSubmitted = true;
        submitDate = DateTimeFormatter.getFormattedDate(rs.getDate("submit_date"), "MM/dd/yy");
      }
      
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadData(" + this.appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void setAppSeqNum(String appSeqNum)
  {
    try
    {
      this.appSeqNum = Long.parseLong(appSeqNum);
    }
    catch(Exception e)
    {
      logEntry("setAppSeqNum(" + appSeqNum + ")", e.toString());
    }
  }
  
  public void setSubmit(String submit)
  {
    this.submitted = true;
  }
  public boolean isSubmitted()
  {
    return this.submitted;
  }
}/*@lineinfo:generated-code*/