/*@lineinfo:filename=GenericEditor*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/GenericEditor.sqlj $

  Description:  
  
    Support bean for viewing/editing risk scoring criteria
    
  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 10/12/04 4:59p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.mes.forms.DateField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import sqlj.runtime.ResultSetIterator;

public class GenericEditor extends EditorBase
{

  /**
   * Log4J Logger
   */
  static Logger log = Logger.getLogger(GenericEditor.class);
  
  public class QueryParam
  {
    public String        ColName     = null;
    public String        ColValue    = null;
    
    public QueryParam( String colName, String colValue )
    {
      ColName   = colName;
      ColValue  = colValue;
    }
  }

  public class SelectColumn
  {
    public String        ColName     = null;
    public String        FieldName   = null;
    
    public SelectColumn( String colName, String fname )
    {
      ColName   = colName;
      FieldName = fname;
    }
  }
  
  public GenericEditor()
  {
  }
  
  public void load( ) 
  {
    Field               field         = null;
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
    Vector              queryParams   = new Vector();
    Vector              selectCols    = new Vector();
    String              tableName     = null;
    StringBuffer        tempBuffer    = new StringBuffer();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:105^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ('field' || efd.display_order)       as field_name,
//                  efd.column_name                      as col_name,
//                  efd.table_name                       as table_name,
//                  decode(efd.rec_id_column,'Y',1,0)    as rec_id_col
//          from    editor_field_defs   efd
//          where   efd.editor_id = :EditorId      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ('field' || efd.display_order)       as field_name,\n                efd.column_name                      as col_name,\n                efd.table_name                       as table_name,\n                decode(efd.rec_id_column,'Y',1,0)    as rec_id_col\n        from    editor_field_defs   efd\n        where   efd.editor_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.GenericEditor",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,EditorId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.GenericEditor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:113^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        if ( tableName != null &&
             !tableName.equals(resultSet.getString("table_name")) )
        {
          selectRow( tableName, selectCols, queryParams );
          
          // reset columns
          selectCols.removeAllElements();
          queryParams.removeAllElements();
        }
        
        // add this column name to the list of column names
        selectCols.addElement( new SelectColumn( resultSet.getString("col_name"),
                                                 resultSet.getString("field_name") ) );
        
        if (  resultSet.getInt("rec_id_col") == 1 )
        {
          // reset the temp buffer, the load it with the 
          // properly encoded editor field value.
          tempBuffer.setLength(0);
          
          field = getField( resultSet.getString("field_name") );
          
          if( field instanceof DateField )
          {
            // dates require special treatment when performing text substitution in SQL
            tempBuffer.append("to_date('");
            tempBuffer.append(((DateField)field).getSqlDateString());
            tempBuffer.append("','dd-mon-yyyy')");
          }
          else
          {
            // store the current offset in case
            // it is necessary to add string markers
            tempBuffer.append( field.getData() );
    
            try
            {
              Double.parseDouble(field.getData());
            }
            catch( Exception nfe )
            {
              // set and ' character to '' in the buffer
              for( int i = 0; i < tempBuffer.length(); ++i )
              {
                if ( tempBuffer.charAt(i) == '\'' )
                {
                  tempBuffer.insert(i,'\'');
                }
              }
              
              // add quotes to the beginning and end
              tempBuffer.insert(0,"'");
              tempBuffer.append("'");
            }
          }
          // add the where clause params
          queryParams.addElement( new QueryParam( resultSet.getString("col_name"),
                                                  tempBuffer.toString() ) );
        }
        
        // store the current table name
        tableName = resultSet.getString("table_name");
      }
      
      if ( tableName != null ) 
      {
        selectRow( tableName, selectCols, queryParams );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry( "load(" + EditorId + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }
  
  protected void selectRow( String tableName, Vector selectCols, Vector queryParams )
  {
    QueryParam            queryParam  = null;
    ResultSet             resultSet   = null;
    StringBuffer          sqlBuffer   = new StringBuffer();
    PreparedStatement     statement   = null;
    SelectColumn          selCol      = null;

    try
    {    
      sqlBuffer.append(" select ");
      for( int i = 0; i < selectCols.size(); ++i )
      {
        selCol = (SelectColumn)selectCols.elementAt(i);
        
        sqlBuffer.append( (i==0) ? "" : ",\n        " );
        sqlBuffer.append( selCol.ColName );
        for( int l = selCol.ColName.length(); l < 50; ++l )
        {
          sqlBuffer.append(" ");
        }
        sqlBuffer.append(" as ");
        sqlBuffer.append( selCol.FieldName );
      }
      sqlBuffer.append("\n from ");
      sqlBuffer.append( tableName );
      
      // add any where clause portions
      if ( queryParams.size() > 0 )
      {
        sqlBuffer.append("\n where ");
      
        for( int i = 0; i < queryParams.size(); ++i )
        {
          queryParam = (QueryParam)queryParams.elementAt(i);
          if ( i > 0 )
          {
            sqlBuffer.append(" and\n        ");
          }
          sqlBuffer.append( queryParam.ColName );
          sqlBuffer.append( " = " );
          sqlBuffer.append( queryParam.ColValue );
        }
      }        
      statement = getPreparedStatement(sqlBuffer.toString());
      resultSet = statement.executeQuery();
      
      if( resultSet.next() )
      {
        // set the editor fields with the first row 
        // false indicates that the result set should not be advanced
        FieldBean.setFields(EditorFields, resultSet, false);
      }
      resultSet.close();
      statement.close();
    }
    catch( Exception e )
    {
      System.out.println("sql: " + sqlBuffer.toString());//@
      logEntry( "selectRow()",e.toString() );
    }            
    finally
    {
      try{ statement.close(); } catch(Exception e) {}
    }
  }
  
  public void store( )
  {
    ResultSetIterator   it            = null;
    Field               field         = null;
    Vector              queryParams   = new Vector();
    ResultSet           resultSet     = null;
    String              tableName     = null;
    StringBuffer        tempBuffer    = new StringBuffer();
    Vector              updateParams  = new Vector();
    
    try
    {
      if( isValid() )
      {
        /*@lineinfo:generated-code*//*@lineinfo:280^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  efd.table_name                    as table_name,
//                    efd.column_name                   as col_name,
//                    ('field' || efd.display_order)    as field_name,
//                    decode(efd.rec_id_column,'Y',1,0) as rec_id_col
//            from    editor_field_defs     efd
//            where   efd.editor_id = :EditorId
//            order by efd.table_name, efd.display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  efd.table_name                    as table_name,\n                  efd.column_name                   as col_name,\n                  ('field' || efd.display_order)    as field_name,\n                  decode(efd.rec_id_column,'Y',1,0) as rec_id_col\n          from    editor_field_defs     efd\n          where   efd.editor_id =  :1 \n          order by efd.table_name, efd.display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.GenericEditor",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,EditorId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.GenericEditor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:289^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          if ( tableName != null &&
               !tableName.equals(resultSet.getString("table_name")) )
          {
            storeRow( tableName, updateParams, queryParams );
            
            // reset columns
            updateParams.removeAllElements();
            queryParams.removeAllElements();
          }
          
          // set the field reference and reset the text buffer
          field = getField( resultSet.getString("field_name") );
          tempBuffer.setLength(0);
          
          // do the necessary SQL encoding of the field data
          if( field instanceof DateField )
          {
            // dates require special treatment when performing text substitution in SQL
            tempBuffer.append("to_date('");
            tempBuffer.append(((DateField)field).getSqlDateString());
            tempBuffer.append("','dd-mon-yyyy')");
          }
          else
          {             
            // store the current offset in case
            // it is necessary to add string markers
            StringBuffer temp2 = new StringBuffer( field.getData() );
            tempBuffer.append("'");
    
            // format the data field.  treat all non-date like a string
            // so that the SQL parser will not remove leading zeros
            log.debug("BEFORE: DATA = " +  field.getData());
            for( int i = 0; i < temp2.length(); ++i )
            {
              if ( temp2.charAt(i) == '\'' )
              {
                tempBuffer.append("'");
              }
              tempBuffer.append(temp2.charAt(i));
            }
            // add quotes to the end
            tempBuffer.append("'");
          }
          log.debug("AFTER: DATA  = " +  tempBuffer.toString());
          if( tempBuffer.length() == 0 )
          {
            tempBuffer.append("null");
          }
          
          // this record is a key value.  currently
          // key values are read only and therefore
          // only appear in the queryParams vector 
          if ( resultSet.getInt("rec_id_col") == 1 && !field.isBlank() )
          {
            queryParams.addElement( new QueryParam( resultSet.getString("col_name"),
                                                    tempBuffer.toString() ) );
                                                  
          }
          else    // update column name/value pair
          {
            // add this column name to the list of column names
            updateParams.addElement( new QueryParam( resultSet.getString("col_name"), 
                                                     tempBuffer.toString() ) );
          }
          
          // store the current table name
          tableName = resultSet.getString("table_name");
        }
        
        if ( tableName != null ) 
        {
          storeRow( tableName, updateParams, queryParams );
        }
        resultSet.close();
        it.close();
      }
    }
    catch( Exception e )
    {
      logEntry("store()",e.toString());
      //@addError( "Failed to apply changes: " + e.toString() );
    }
    finally
    {
      try{ it.close(); } catch(Exception ee){}
    }
  }
  
  protected void storeRow( String tableName, Vector updateParams, Vector queryParams )
  {
    QueryParam            param         = null;
    int                   recCount      = 0;
    boolean               recordExists  = false;
    StringBuffer          sqlBuffer     = new StringBuffer();
    PreparedStatement     statement     = null;

    try
    {    
      if ( queryParams.size() > 0 )
      {
        for( int i = 0; i < queryParams.size(); ++i )
        {
          param = (QueryParam) queryParams.elementAt(i);
      
          if ( i != 0 )
          {
            sqlBuffer.append(" and \n ");
          }
          sqlBuffer.append(param.ColName);
          sqlBuffer.append(" = ");
          sqlBuffer.append(param.ColValue);
        }
        
        System.out.println("where clause: " + sqlBuffer.toString());//@
        /*@lineinfo:generated-code*//*@lineinfo:408^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(*) 
//            from    :tableName
//            where   :sqlBuffer.toString()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_848 = sqlBuffer.toString();
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  count(*)  \n          from     ");
   __sjT_sb.append(tableName);
   __sjT_sb.append(" \n          where    ");
   __sjT_sb.append(__sJT_848);
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "2com.mes.maintenance.GenericEditor:" + __sjT_sql;
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:413^9*/
        if ( recCount > 0 )
        {
          recordExists = true;
        }
      }        
      
      // reset the buffer      
      sqlBuffer.setLength(0);
      
      if ( recordExists == false )      // new record
      {
        String    names     = null;
        String    values    = null;
        
        for( int i = 0; i < updateParams.size(); ++i )
        {
          param = (QueryParam) updateParams.elementAt(i);
          if ( sqlBuffer.length() > 0 )
          {
            sqlBuffer.append(",\n  ");
          }
          sqlBuffer.append( param.ColName );
        }
        
        for( int i = 0; i < queryParams.size(); ++i )
        {
          param = (QueryParam) queryParams.elementAt(i);
          if ( sqlBuffer.length() > 0 )
          {
            sqlBuffer.append(",\n  ");
          }
          sqlBuffer.append( param.ColName );
        }
        names = sqlBuffer.toString();
        
        sqlBuffer.setLength(0);
        for( int i = 0; i < updateParams.size(); ++i )
        {
          param = (QueryParam) updateParams.elementAt(i);
          if ( sqlBuffer.length() > 0 )
          {
            sqlBuffer.append(",\n  ");
          }
          sqlBuffer.append( param.ColValue );
        }
        for( int i = 0; i < queryParams.size(); ++i )
        {
          param = (QueryParam) queryParams.elementAt(i);
          if ( sqlBuffer.length() > 0 )
          {
            sqlBuffer.append(",\n  ");
          }
          if ( param.ColValue == null || param.ColValue.equals("") )
          {
            sqlBuffer.append( "null" );
          }
          else
          {
            sqlBuffer.append( param.ColValue );
          }            
        }
        values = sqlBuffer.toString();
        
        /*@lineinfo:generated-code*//*@lineinfo:477^9*/

//  ************************************************************
//  #sql [Ctx] { insert into :tableName
//            (
//              :names
//            )            
//            values
//            (
//              :values
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("insert into  ");
   __sjT_sb.append(tableName);
   __sjT_sb.append(" \n          (\n             ");
   __sjT_sb.append(names);
   __sjT_sb.append(" \n          )            \n          values\n          (\n             ");
   __sjT_sb.append(values);
   __sjT_sb.append(" \n          )");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "3com.mes.maintenance.GenericEditor:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:487^9*/
      }
      else  // updating an existing record
      {
        String    values    = null;
        
        for( int i = 0; i < updateParams.size(); ++i )
        {
          param = (QueryParam) updateParams.elementAt(i);
          if ( i > 0 )
          {
            sqlBuffer.append(",\n     ");
          }
          sqlBuffer.append( param.ColName );
          sqlBuffer.append( " = " );
          sqlBuffer.append( param.ColValue );
        }
        values = sqlBuffer.toString();
        
        sqlBuffer.setLength(0);
        for( int i = 0; i < queryParams.size(); ++i )
        {
          param = (QueryParam) queryParams.elementAt(i);
      
          if ( i > 0 )
          {
            sqlBuffer.append(" and\n      ");
          }
          sqlBuffer.append(param.ColName);
          sqlBuffer.append(" = ");
          sqlBuffer.append(param.ColValue);
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:520^9*/

//  ************************************************************
//  #sql [Ctx] { update :tableName
//            set :values
//            where :sqlBuffer.toString()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_849 = sqlBuffer.toString();
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update  ");
   __sjT_sb.append(tableName);
   __sjT_sb.append(" \n          set  ");
   __sjT_sb.append(values);
   __sjT_sb.append(" \n          where  ");
   __sjT_sb.append(__sJT_849);
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "4com.mes.maintenance.GenericEditor:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:525^9*/
      }
    }
    catch( Exception e )
    {
      logEntry( "storeRow()",e.toString() );
      //@addError( "record insert/update failed: " + e.toString() );
    }            
    finally
    {
      try{ statement.close(); } catch(Exception e) {}
    }
  }
}/*@lineinfo:generated-code*/