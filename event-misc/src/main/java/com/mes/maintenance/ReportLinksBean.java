/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/ReportLinksBean.java $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 1/21/03 11:19a $
  Version            : $Revision: 9 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.util.Vector;
import com.mes.constants.MesMenus;
import com.mes.database.SQLJConnectionBase;
import com.mes.menus.MenuDataBean;
import com.mes.menus.MenuItem;
import com.mes.user.UserBean;

public class ReportLinksBean extends SQLJConnectionBase
{
  private String        node                      = "";
  private String        merchant                  = "";
  private UserBean      user                      = null;
  
  public  Vector        reportLinks               = new Vector();
  
  private boolean       isMerchant                = false;
  private boolean       nodeMerchantInit          = false;
  private boolean       userInit                  = false;
  
  public void loadReportLinks( int menuId, int sortBy )
  {
    MenuDataBean  menuBean                  = new MenuDataBean();
    MenuItem      menuItem                  = null;
    Vector        menuItems                 = null;
    
    try
    {
      // clear the existing buffer
      reportLinks.removeAllElements();
      
      menuItems   = menuBean.loadMenu(menuId, sortBy);
      
      for( int i = 0; i < menuItems.size(); ++i )
      {
        menuItem = (MenuItem) menuItems.elementAt(i);
        
        if( menuItem.isItemDisplayed(user) )
        {
          // add this menu item to the reportLinks vector
          reportLinks.add(menuItem);
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadReportLinks(" + menuId + ")", e.toString());
    }
  }
  
  public void loadReportLinks(int menuId )
  {
    loadReportLinks(menuId, MenuDataBean.SB_MENU_ORDER);
  }

  private void createLinks(int sortBy)
  {
    if(nodeMerchantInit && userInit)
    {
      if(isMerchant)
      {
        loadReportLinks(MesMenus.MENU_ID_MERCHANT, sortBy);
      }
      else
      {
        loadReportLinks(MesMenus.MENU_ID_SUMMARY, sortBy);
      }
    }
  }
  
  private void createLinks()
  {
    createLinks(MenuDataBean.SB_MENU_ORDER);
  }

  public void setNode(String node)
  {
    this.node = node;
    nodeMerchantInit = true;
    createLinks();
  }
  public void setMerchant(String merchant, int sortBy)
  {
    this.merchant = merchant;
    this.isMerchant = true;
    nodeMerchantInit = true;
    
    createLinks(sortBy);
  }
  
  public void setMerchant(String merchant)
  {
    setMerchant(merchant, MenuDataBean.SB_MENU_ORDER);
  }

  public void setUser(UserBean user)
  {
    this.user = user;

    userInit = true;
  }
}
