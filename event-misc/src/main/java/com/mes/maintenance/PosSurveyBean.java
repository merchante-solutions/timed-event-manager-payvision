/*@lineinfo:filename=PosSurveyBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: $

  Description:

  PosSurveyBean

  Bean to store and retrieve POS information about a merchant.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2006-12-26 11:20:46 -0800 (Tue, 26 Dec 2006) $
  Version            : $Revision: 13241 $

  Change History:
     See SVN database

  Copyright (C) 2000,2001,2002,2003,2004,2005,2006,2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.maintenance;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.PhoneField;
import com.mes.forms.Validation;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class PosSurveyBean extends FieldBean
{
  /*  DropDown Tables */
  protected class POSTypeTable extends DropDownTable
  {
    public POSTypeTable(DefaultContext Ctx)
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;

      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:51^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  pos_type,
//                    pos_desc
//            from    pos_survey_pos_types
//            order by display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pos_type,\n                  pos_desc\n          from    pos_survey_pos_types\n          order by display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.PosSurveyBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.maintenance.PosSurveyBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:57^9*/

        rs = it.getResultSet();

        addElement("", "select one");
        while(rs.next())
        {
          addElement(rs);
        }
      }
      catch(Exception e)
      {
        logEntry("POSTypeTable()", e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
      }
    }
  }

  protected class ChangeSourceTable extends DropDownTable
  {
    public ChangeSourceTable(DefaultContext Ctx)
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;

      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:88^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  source_id,
//                    description
//            from    pos_survey_change_sources
//            order by display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  source_id,\n                  description\n          from    pos_survey_change_sources\n          order by display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.PosSurveyBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.PosSurveyBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:94^9*/

        rs = it.getResultSet();

        addElement("", "select one");
        while(rs.next())
        {
          addElement(rs);
        }
      }
      catch(Exception e)
      {
        logEntry("ChangeSourceTable()", e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
      }
    }
  }

  protected class TranTypeTable extends DropDownTable
  {
    public TranTypeTable(DefaultContext Ctx)
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;

      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:125^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tran_type,
//                    description
//            from    pos_survey_tran_types
//            order by display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tran_type,\n                  description\n          from    pos_survey_tran_types\n          order by display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.PosSurveyBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.maintenance.PosSurveyBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:131^9*/

        rs = it.getResultSet();

        addElement("", "select one");
        while(rs.next())
        {
          addElement(rs);
        }
      }
      catch(Exception e)
      {
        logEntry("TranTypeTable()", e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { rs.close(); } catch(Exception e) {}
      }
    }
  }

  protected class CommTypeTable extends DropDownTable
  {
    public CommTypeTable(DefaultContext Ctx)
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;

      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:162^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  comm_type,
//                    description
//            from    pos_survey_comm_types
//            order by display_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  comm_type,\n                  description\n          from    pos_survey_comm_types\n          order by display_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.PosSurveyBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.maintenance.PosSurveyBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:168^9*/

        rs = it.getResultSet();

        addElement("", "select one");
        while(rs.next())
        {
          addElement(rs);
        }
      }
      catch(Exception e)
      {
        logEntry("CommTypeTable()", e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
      }
    }
  }

  public class OtherRequiredValidation
    implements Validation
  {
    private Field   dropField = null;
    private String  errorText = "";

    public OtherRequiredValidation(Field dropField)
    {
      this.dropField = dropField;
    }

    public String getErrorText()
    {
      return( errorText );
    }

    public boolean validate(String fdata)
    {
      boolean valid = true;

      try
      {
        if( fdata == null || fdata.equals("") )
        {
          // field is empty, call type must be something other than "Other"
          if(dropField.getData().equals("-1"))
          {
            valid = false;
            errorText = "Required if drop-down value is set to \"Other\"";
          }
        }
        else
        {
          // field is not empty, call type must be "Other"
          if(! dropField.getData().equals("-1"))
          {
            valid = false;
            errorText = "drop-down value must be \"Other\" or this field must be empty";
          }
        }
      }
      catch(Exception e)
      {
        errorText = e.toString();
        valid = false;
      }

      return( valid );
    }
  }

  public void createFields(HttpServletRequest request)
  {
    try
    {
      connect();
      //added for PCI version
      fields.add(new Field          ("pciName",        "Contact Name",  40, 20, true));
      fields.add(new EmailField     ("pciEmail",       "Contact Email", true));
      fields.add(new PhoneField     ("pciPhone",       "Contact Phone", true));

      fields.add(new Field          ("merchant",        "Merchant Number",  16, 16, true));
      fields.add(new DropDownField  ("posType",         "POS Type", new POSTypeTable(Ctx), false));
      fields.add(new Field          ("serviceProvider", "Service Provider", 40, 20, false));
      fields.add(new Field          ("paymentProduct",  "Payment Product", 40, 20, false));
      fields.add(new Field          ("productVersion",  "Product Version", 20, 20, true));
      fields.add(new DropDownField  ("changeSource",    "Change Source", new ChangeSourceTable(Ctx), false));
      fields.add(new Field          ("changeSourceOther", "Other Change Source", 40, 20, true));
      fields.add(new PhoneField     ("vendorPhone",     "Vendor Phone", true));
      fields.add(new DropDownField  ("tranType",        "Transaction Type", new TranTypeTable(Ctx), false));
      fields.add(new DropDownField  ("commType",        "Comm Type", new CommTypeTable(Ctx), false));
      fields.add(new Field          ("commTypeOther",   "Other Comm Type", 20, 20, true));

      getField("changeSourceOther").addValidation(new OtherRequiredValidation(getField("changeSource")));
      getField("commTypeOther").addValidation(new OtherRequiredValidation(getField("commType")));

      fields.add(new ButtonField("submitSurvey", "Submit Survey"));

      fields.setHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      logEntry("createFields()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void setProperties(HttpServletRequest request, UserBean user)
  {
    try
    {
      connect();

      this.user     = user;
      this.request  = request;

      register(user.getLoginName(), request);

      setFields(request);

      if( ! getData("submitSurvey").equals(""))
      {
        System.out.println("should be processing");

        // submit data to database
        if(isValid())
        {
          updateData();
        }
      }
      else
      {
        System.out.println("should be loading");
        System.out.println("merch number = "+ getData("merchant"));
        loadData();
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
      logEntry("setProperties()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  private void updateData()
  {
    // update database with results of survey
    try
    {
      int recCount = 0;

      /*@lineinfo:generated-code*//*@lineinfo:328^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//          
//          from    pos_surveys
//          where   merchant_number = :getData("merchant")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_892 = getData("merchant");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n         \n        from    pos_surveys\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.maintenance.PosSurveyBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_892);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:334^7*/

      if(recCount > 0)
      {
        // update
        /*@lineinfo:generated-code*//*@lineinfo:339^9*/

//  ************************************************************
//  #sql [Ctx] { update  pos_surveys
//            set
//                    pci_name          = :getData("pciName"),
//                    pci_email         = :getData("pciEmail"),
//                    pci_phone         = :getData("pciPhone"),
//                    pos_type          = :getData("posType"),
//                    service_provider  = :getData("serviceProvider"),
//                    payment_product   = :getData("paymentProduct"),
//                    product_version   = :getData("productVersion"),
//                    change_source     = :getData("changeSource"),
//                    change_source_other = :getData("changeSourceOther"),
//                    vendor_phone      = :getData("vendorPhone"),
//                    transaction_type  = :getData("tranType"),
//                    comm_type         = :getData("commType"),
//                    comm_type_other   = :getData("commTypeOther")
//            where   merchant_number   = :getData("merchant")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_893 = getData("pciName");
 String __sJT_894 = getData("pciEmail");
 String __sJT_895 = getData("pciPhone");
 String __sJT_896 = getData("posType");
 String __sJT_897 = getData("serviceProvider");
 String __sJT_898 = getData("paymentProduct");
 String __sJT_899 = getData("productVersion");
 String __sJT_900 = getData("changeSource");
 String __sJT_901 = getData("changeSourceOther");
 String __sJT_902 = getData("vendorPhone");
 String __sJT_903 = getData("tranType");
 String __sJT_904 = getData("commType");
 String __sJT_905 = getData("commTypeOther");
 String __sJT_906 = getData("merchant");
   String theSqlTS = "update  pos_surveys\n          set\n                  pci_name          =  :1 ,\n                  pci_email         =  :2 ,\n                  pci_phone         =  :3 ,\n                  pos_type          =  :4 ,\n                  service_provider  =  :5 ,\n                  payment_product   =  :6 ,\n                  product_version   =  :7 ,\n                  change_source     =  :8 ,\n                  change_source_other =  :9 ,\n                  vendor_phone      =  :10 ,\n                  transaction_type  =  :11 ,\n                  comm_type         =  :12 ,\n                  comm_type_other   =  :13 \n          where   merchant_number   =  :14";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.maintenance.PosSurveyBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_893);
   __sJT_st.setString(2,__sJT_894);
   __sJT_st.setString(3,__sJT_895);
   __sJT_st.setString(4,__sJT_896);
   __sJT_st.setString(5,__sJT_897);
   __sJT_st.setString(6,__sJT_898);
   __sJT_st.setString(7,__sJT_899);
   __sJT_st.setString(8,__sJT_900);
   __sJT_st.setString(9,__sJT_901);
   __sJT_st.setString(10,__sJT_902);
   __sJT_st.setString(11,__sJT_903);
   __sJT_st.setString(12,__sJT_904);
   __sJT_st.setString(13,__sJT_905);
   __sJT_st.setString(14,__sJT_906);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:357^9*/
      }
      else
      {
        // insert
        /*@lineinfo:generated-code*//*@lineinfo:362^9*/

//  ************************************************************
//  #sql [Ctx] { insert into pos_surveys
//            (
//              pci_name,
//              pci_email,
//              pci_phone,
//              merchant_number,
//              pos_type,
//              service_provider,
//              payment_product,
//              product_version,
//              change_source,
//              change_source_other,
//              vendor_phone,
//              transaction_type,
//              comm_type,
//              comm_type_other
//            )
//            values
//            (
//              :getData("pciName"),
//              :getData("pciEmail"),
//              :getData("pciPhone"),
//              :getData("merchant"),
//              :getData("posType"),
//              :getData("serviceProvider"),
//              :getData("paymentProduct"),
//              :getData("productVersion"),
//              :getData("changeSource"),
//              :getData("changeSourceOther"),
//              :getData("vendorPhone"),
//              :getData("tranType"),
//              :getData("commType"),
//              :getData("commTypeOther")
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_907 = getData("pciName");
 String __sJT_908 = getData("pciEmail");
 String __sJT_909 = getData("pciPhone");
 String __sJT_910 = getData("merchant");
 String __sJT_911 = getData("posType");
 String __sJT_912 = getData("serviceProvider");
 String __sJT_913 = getData("paymentProduct");
 String __sJT_914 = getData("productVersion");
 String __sJT_915 = getData("changeSource");
 String __sJT_916 = getData("changeSourceOther");
 String __sJT_917 = getData("vendorPhone");
 String __sJT_918 = getData("tranType");
 String __sJT_919 = getData("commType");
 String __sJT_920 = getData("commTypeOther");
   String theSqlTS = "insert into pos_surveys\n          (\n            pci_name,\n            pci_email,\n            pci_phone,\n            merchant_number,\n            pos_type,\n            service_provider,\n            payment_product,\n            product_version,\n            change_source,\n            change_source_other,\n            vendor_phone,\n            transaction_type,\n            comm_type,\n            comm_type_other\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.maintenance.PosSurveyBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_907);
   __sJT_st.setString(2,__sJT_908);
   __sJT_st.setString(3,__sJT_909);
   __sJT_st.setString(4,__sJT_910);
   __sJT_st.setString(5,__sJT_911);
   __sJT_st.setString(6,__sJT_912);
   __sJT_st.setString(7,__sJT_913);
   __sJT_st.setString(8,__sJT_914);
   __sJT_st.setString(9,__sJT_915);
   __sJT_st.setString(10,__sJT_916);
   __sJT_st.setString(11,__sJT_917);
   __sJT_st.setString(12,__sJT_918);
   __sJT_st.setString(13,__sJT_919);
   __sJT_st.setString(14,__sJT_920);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:398^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("updateData()", e.toString());
    }
  }

  private void loadData()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:414^7*/

//  ************************************************************
//  #sql [Ctx] it = { select
//                  nvl(pci_name, '')   pci_name,
//                  nvl(pci_email, '')  pci_email,
//                  nvl(pci_phone, '')  pci_phone,
//                  pos_type            pos_type,
//                  service_provider    service_provider,
//                  payment_product     payment_product,
//                  product_version     product_version,
//                  change_source       change_source,
//                  change_source_other change_source_other,
//                  vendor_phone        vendor_phone,
//                  transaction_type    tran_type,
//                  comm_type           comm_type,
//                  comm_type_other     comm_type_other
//          from    pos_surveys
//          where   merchant_number = :getData("merchant")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_921 = getData("merchant");
  try {
   String theSqlTS = "select\n                nvl(pci_name, '')   pci_name,\n                nvl(pci_email, '')  pci_email,\n                nvl(pci_phone, '')  pci_phone,\n                pos_type            pos_type,\n                service_provider    service_provider,\n                payment_product     payment_product,\n                product_version     product_version,\n                change_source       change_source,\n                change_source_other change_source_other,\n                vendor_phone        vendor_phone,\n                transaction_type    tran_type,\n                comm_type           comm_type,\n                comm_type_other     comm_type_other\n        from    pos_surveys\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.maintenance.PosSurveyBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_921);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.maintenance.PosSurveyBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:432^7*/

      rs = it.getResultSet();

      setFields(rs);
    }
    catch(Exception e)
    {
      logEntry("loadData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }

}/*@lineinfo:generated-code*/