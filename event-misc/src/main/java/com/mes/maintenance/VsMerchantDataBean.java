/*@lineinfo:filename=VsMerchantDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/VsMerchantDataBean.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 5/22/03 2:38p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.maintenance;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import com.mes.database.SQLJConnectionBase;
import com.mes.ops.QueueConstants;
import sqlj.runtime.ResultSetIterator;

public class VsMerchantDataBean extends SQLJConnectionBase
{
  
  /*
  ** BEGINNING OF CLASS MerchantDataBean
  */
  private String    merchantNumber        = "";
  private String    appStatus             = "";
  private int       appStatusCode         = -1;
  private String    appDate               = "";
  private String    appFinishDate         = "";
  private String    dba                   = "";
  private String    addr0Line1            = "";
  private String    addr0Line2            = "";
  private String    addr0City             = "";
  private String    addr0State            = "";
  private String    addr0Zip              = "";
  private String    businessPhone         = "";
  private long      appSeqNum             = 0L;
  private String    contactName           = "";
  private String    contactPhone          = "";
  private String    contactEmail          = "";
  private String    controlNumber         = "";
  private String    webUrl                = "";
  private String    vsPassword            = "";
  private String    vsLogin               = "";

  private Timestamp today                 = null;
    
  public VsMerchantDataBean()
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:82^7*/

//  ************************************************************
//  #sql [Ctx] { select    sysdate 
//          from      dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    sysdate  \n        from      dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.VsMerchantDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   today = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:86^7*/
    }
    catch(Exception e)
    {
      logEntry("VsMerchantDataBean(" + this.appSeqNum + ")", e.toString());
    } 
    finally
    {
      cleanUp();
    }
  }
  
  public void loadData()
  {
    loadData(this.appSeqNum);
  }
  
  //public void setMerchant(long appSeq)
  //{
    //loadData(appSeq);
  //}
  
  public boolean isBlank(String field)
  {
    boolean result = false;
    
    if(field == null || field.length() == 0)
    {
      result = true;
    }
    
    return result;
  }
  
  public String formatPhone(String phone)
  {
    if(isBlank(phone))
    {
      return "";
    }
    if(phone.length() != 10)
    {
      return phone;
    }
    String areaCode   = phone.substring(0,3);
    String firstThree = phone.substring(3,6);
    String lastFour   = phone.substring(6);
    return ("(" + areaCode + ")" + " " + firstThree + "-" + lastFour);
  }

  
  public String formatEmail(String email)
  {
    StringBuffer    result = new StringBuffer("");
    
    try
    {
      if(isBlank(email))
      {
        return "NONE";
      }
      if(email.indexOf('@') >= 0)
      {
        result.append("<a href=\"mailto:");
        result.append(email.toLowerCase());
        result.append("\">");
      }

      result.append(email.toLowerCase());
      
      result.append("</a>");
    }
    catch(Exception e)
    {
      logEntry("formatEmail()", e.toString());
    }
    
    return result.toString();
  }
  public String formatUrl(String url)
  {
    StringBuffer    result = new StringBuffer("");
    
    try
    {
        if(isBlank(url))
        {
          return "NONE";
        }
        else
        {
          result.append("<a href=\"");
          if(url.toLowerCase().indexOf("http:") == -1)
          {
            result.append("http://");
          }
          result.append(url.toLowerCase());
          result.append("\">");
        }

        result.append(url.toLowerCase());
        
        result.append("</a>");
    }
    catch(Exception e)
    {
      logEntry("formatUrl()", e.toString());
    }
    
    return result.toString();
  }

  
  private void getMerchantData(long appSeqNum)
  {
    ResultSetIterator     it      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:205^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app.app_created_date           app_date,
//                  app.app_seq_num                app_seq_num,
//                  mc.merchcont_prim_first_name   contact_first_name,
//                  mc.merchcont_prim_last_name    contact_last_name,
//                  mc.merchcont_prim_phone        contact_phone,
//                  merch.merc_cntrl_number        control_number,
//                  merch.merch_number             merchant_number,
//                  merch.merch_business_name      dba_name,
//                  merch.merch_email_address      contact_email,
//                  merch.merch_password           vspass,
//                  merch.merch_login              vslogin,
//                  merch.merch_credit_status      status,
//                  pos.pos_param                  weburl
//  
//          from    application                   app,
//                  merchcontact                  mc,
//                  merchant                      merch,
//                  merch_pos                     pos
//  
//          where   app.app_seq_num = :appSeqNum and
//                  app.app_seq_num = merch.app_seq_num   and
//                  app.app_seq_num = pos.app_seq_num(+)  and
//                  app.app_seq_num = mc.app_seq_num(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app.app_created_date           app_date,\n                app.app_seq_num                app_seq_num,\n                mc.merchcont_prim_first_name   contact_first_name,\n                mc.merchcont_prim_last_name    contact_last_name,\n                mc.merchcont_prim_phone        contact_phone,\n                merch.merc_cntrl_number        control_number,\n                merch.merch_number             merchant_number,\n                merch.merch_business_name      dba_name,\n                merch.merch_email_address      contact_email,\n                merch.merch_password           vspass,\n                merch.merch_login              vslogin,\n                merch.merch_credit_status      status,\n                pos.pos_param                  weburl\n\n        from    application                   app,\n                merchcontact                  mc,\n                merchant                      merch,\n                merch_pos                     pos\n\n        where   app.app_seq_num =  :1  and\n                app.app_seq_num = merch.app_seq_num   and\n                app.app_seq_num = pos.app_seq_num(+)  and\n                app.app_seq_num = mc.app_seq_num(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.maintenance.VsMerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.maintenance.VsMerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:230^7*/
    
      ResultSet rs = it.getResultSet();

      if(rs.next())
      {
        setAppStatus(processString(rs.getString("status")));
        setAppDate(rs.getDate("app_date"));
        setContactEmail(processString(rs.getString("contact_email")));
        setWebUrl(processString(rs.getString("weburl")));
        setVsPassword(processString(rs.getString("vspass")));
        setVsLogin(processString(rs.getString("vslogin")));
        setContactName(rs.getString("contact_first_name"), rs.getString("contact_last_name"));
        setContactPhone(processString(rs.getString("contact_phone")));
        setControlNumber(processString(rs.getString("control_number")));
        setMerchantNumber(processString(rs.getString("merchant_number")));
        setDba(processString(rs.getString("dba_name")));
        setAppSeqNum(rs.getLong("app_seq_num"));
      }
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getMerchantData(" + this.appSeqNum + ")", e.toString());
    }
    finally
    {
      // DON'T KILL THE CONNECTION HERE -- IT WILL BE DONE IN loadData()
    }
  }

  private void getFinishDate(long appSeqNum)
  {
    ResultSetIterator     it      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:268^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  apt.date_received             app_finish_date
//  
//          from    app_tracking                  apt
//  
//          where   apt.app_seq_num = :appSeqNum and
//                  apt.dept_code = 100
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  apt.date_received             app_finish_date\n\n        from    app_tracking                  apt\n\n        where   apt.app_seq_num =  :1  and\n                apt.dept_code = 100";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.maintenance.VsMerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.maintenance.VsMerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:276^7*/
    
      ResultSet rs = it.getResultSet();

      if(rs.next())
      {
        setAppFinishDate(rs.getDate("app_finish_date"));
      }
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getFinishDate(" + this.appSeqNum + ")", e.toString());
    }
    finally
    {
      // DON'T KILL THE CONNECTION HERE -- IT WILL BE DONE IN loadData()
    }
  }


  private void getAddressData(long appSeqNum)
  {
    ResultSetIterator     it      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:304^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  addr.address_line1             add_line1,
//                  addr.address_line2             add_line2,
//                  addr.address_city              add_city,
//                  addr.address_zip               add_zip,
//                  addr.countrystate_code         add_state,
//                  addr.address_phone             business_phone
//  
//          from    address                       addr
//  
//          where   addr.app_seq_num = :appSeqNum 
//                  and addr.addresstype_code = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  addr.address_line1             add_line1,\n                addr.address_line2             add_line2,\n                addr.address_city              add_city,\n                addr.address_zip               add_zip,\n                addr.countrystate_code         add_state,\n                addr.address_phone             business_phone\n\n        from    address                       addr\n\n        where   addr.app_seq_num =  :1  \n                and addr.addresstype_code = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.maintenance.VsMerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.maintenance.VsMerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:317^7*/
    
      ResultSet rs = it.getResultSet();

      if(rs.next())
      {
        setAddr0Line1(processString(rs.getString("add_line1")));
        setAddr0Line2(processString(rs.getString("add_line2")));
        setAddr0City(processString(rs.getString("add_city")));
        setAddr0State(processString(rs.getString("add_state")));
        setAddr0Zip(processString(rs.getString("add_zip")));
        setBusinessPhone(processString(rs.getString("business_phone")));
      }
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getAddressData(" + this.appSeqNum + ")", e.toString());
    }
    finally
    {
      // DON'T KILL THE CONNECTION HERE -- IT WILL BE DONE IN loadData()
    }
  }

  private void setAppTracking(long appSeqNum, int status)
  {
    ResultSetIterator     it      = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:349^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    vs_app_tracking
//          where   app_seq_num = :appSeqNum 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    vs_app_tracking\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.maintenance.VsMerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.maintenance.VsMerchantDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:354^7*/
    
      ResultSet rs = it.getResultSet();

      if(rs.next())
      {
        //if a record set exsists dont do a damn thing... if it dont exsist.. put an entry into the database
      }
      else
      {
        if(status != QueueConstants.CREDIT_APPROVE)
        {
          /*@lineinfo:generated-code*//*@lineinfo:366^11*/

//  ************************************************************
//  #sql [Ctx] { insert into vs_app_tracking
//              ( 
//                APP_SEQ_NUM,
//                start_status,
//                start_date
//              )
//              values
//              ( 
//                :appSeqNum,
//                :status,
//                :today
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into vs_app_tracking\n            ( \n              APP_SEQ_NUM,\n              start_status,\n              start_date\n            )\n            values\n            ( \n               :1 ,\n               :2 ,\n               :3 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.maintenance.VsMerchantDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,status);
   __sJT_st.setTimestamp(3,today);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:380^11*/
        }
      }
      
      it.close();
    }
    catch(Exception e)
    {
      logEntry("setAppTracking(" + this.appSeqNum + ")", e.toString());
    }
    finally
    {
      // DON'T KILL THE CONNECTION HERE -- IT WILL BE DONE IN loadData()
    }

  }

  public void loadData(long appSeqNum)
  {
    try
    {
      connect();
      getMerchantData(appSeqNum);
      getAddressData(appSeqNum);
      getFinishDate(appSeqNum);
      setAppTracking(appSeqNum, appStatusCode);
    }
    catch(Exception e)
    {
      logEntry("loadData(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public void setMerchantNumber(String merchantNumber)
  {
    this.merchantNumber = merchantNumber;
  }
  public String getMerchantNumber()
  {
    return this.merchantNumber;
  }
  public void setDba(String dba)
  {
    this.dba = dba;
  }
  public String getDba()
  {
    return this.dba;
  }
        
  public void setAppStatus(String appStatusCode)
  {
    try
    {
      int status = Integer.parseInt(appStatusCode);
      switch(status)
      {
        case QueueConstants.APP_NOT_FINISHED:
          this.appStatus = "Not Finished";
          this.appStatusCode = status;
        break;
        case QueueConstants.CREDIT_APPROVE:
          this.appStatus = "Approved";
          this.appStatusCode = status;
        break;
        case QueueConstants.CREDIT_DECLINE:
          this.appStatus = "Declined";
          this.appStatusCode = status;
        break;
        case QueueConstants.CREDIT_PEND:
          this.appStatus = "Pending";
          this.appStatusCode = status;
        break;
        case QueueConstants.CREDIT_CANCEL:
          this.appStatus = "Cancelled";
          this.appStatusCode = status;
        break;
        case QueueConstants.CREDIT_NEW:
          this.appStatus = "New Application";
          this.appStatusCode = status;
        break;
        default:
          this.appStatus = "UNKNOWN";
          this.appStatusCode = status;
        break;
      }
    }
    catch(Exception e)
    {
      this.appStatus = "UNKNOWN";
    }
  }

  public String getAppStatus()
  {
    return this.appStatus;
  }        
        
  public void setBusinessPhone(String businessPhone)
  {
    this.businessPhone = businessPhone;
  }
        
  public String getBusinessPhone()
  {
    return this.businessPhone;
  }

  public void  setContactEmail(String contactEmail)
  {
    this.contactEmail = contactEmail;
  }
  public String getContactEmail()
  {
    return this.contactEmail;
  }

  public void setWebUrl(String webUrl)
  {
    this.webUrl = webUrl;
  }
  public String getWebUrl()
  {
    return this.webUrl;
  }

  public void setVsPassword(String vsPassword)
  {
    this.vsPassword = vsPassword;
  }
  public String getVsPassword()
  {
    return this.vsPassword;
  }

  public void setVsLogin(String vsLogin)
  {
    this.vsLogin = vsLogin;
  }
  public String getVsLogin()
  {
    return this.vsLogin;
  }

  public void setAppDate(Date appDate)
  {
    DateFormat  indf  = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat  outdf = new SimpleDateFormat("MM/dd/yy");

    try
    {
      if(appDate == null)
      {
        this.appDate = "";
        return;
      }
      else
      {
        this.appDate = appDate.toString();
      }
      this.appDate = outdf.format(indf.parse(this.appDate, new ParsePosition(0)));
    }
    catch(Exception e)
    {
      logEntry("setAppDate(" + appDate + ")", e.toString());
      this.appDate = "";
    }
  }
  
  public String getAppDate()
  {
    return this.appDate;
  }

  public void setAppFinishDate(Date appFinishDate)
  {
    DateFormat  indf  = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat  outdf = new SimpleDateFormat("MM/dd/yy");

    try
    {
      if(appFinishDate == null)
      {
        this.appFinishDate = "";
        return;
      }
      else
      {
        this.appFinishDate = appFinishDate.toString();
      }
      this.appFinishDate = outdf.format(indf.parse(this.appFinishDate, new ParsePosition(0)));
    }
    catch(Exception e)
    {
      logEntry("setAppFinishDate(" + appFinishDate + ")", e.toString());
      this.appFinishDate = "";
    }
  }
  
  public String getAppFinishDate()
  {
    return this.appFinishDate;
  }
  
  
  public void setAddr0Line1(String addr0Line1)
  {
    this.addr0Line1 = addr0Line1;
  }
  public String getAddr0Line1()
  {
    return this.addr0Line1;
  }
  public void setAddr0Line2(String addr0Line2)
  {
    this.addr0Line2 = addr0Line2;
  }
  public String getAddr0Line2()
  {
    return this.addr0Line2;
  }
  public void setAddr0City(String addr0City)
  {
    this.addr0City = addr0City;
  }
  public String getAddr0City()
  {
    return this.addr0City;
  }
  public void setAddr0State(String addr0State)
  {
    this.addr0State = addr0State;
  }
  public String getAddr0State()
  {
    return this.addr0State;
  }
  public void setAddr0Zip(String addr0Zip)
  {
    this.addr0Zip = addr0Zip;
  }
  public String getAddr0Zip()
  {
    return this.addr0Zip;
  }
  
  public long getAppSeqNum()
  {
    return this.appSeqNum;
  }
  public void setAppSeqNum(String appSeqNum)
  {
    try
    {
      setAppSeqNum(Long.parseLong(appSeqNum));
    }
    catch(Exception e)
    {
      logEntry("setAppSeqNum(" + appSeqNum + ")", e.toString());
    }
  }
  public void setAppSeqNum(long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
  }
  
  public void setContactName(String contactFirstName, String contactLastName)
  {
    this.contactName = contactFirstName + " " + contactLastName;
  }
  public String getContactName()
  {
    return this.contactName;
  }
  
  public void setContactPhone(String contactPhone)
  {
    this.contactPhone = contactPhone;
  }
  public String getContactPhone()
  {
    return this.contactPhone;
  }
  
  public void setControlNumber(String controlNumber)
  {
    this.controlNumber = controlNumber;
  }
  public String getControlNumber()
  {
    return this.controlNumber;
  }
  
}/*@lineinfo:generated-code*/