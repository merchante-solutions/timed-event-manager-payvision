/*@lineinfo:filename=BankNumberBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/maintenance/BankNumberBean.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/05/03 10:21a $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.maintenance;

import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import com.mes.support.HttpHelper;


public class BankNumberBean extends com.mes.database.SQLJConnectionBase
{
  public static final int       BANK_NUMBER_CERTEGY   = 1000;
  public static final int       BANK_NUMBER_COMDATA   = 1010;
  public static final int       BANK_NUMBER_CBT       = 3858;
  public static final int       BANK_NUMBER_NBSC      = 3860;
  public static final int       BANK_NUMBER_BBT       = 3867;
  public static final int       BANK_NUMBER_UBOC      = 3870;
  public static final int       BANK_NUMBER_MES       = 3941;
  
  public BankNumberBean()
  {
  }
  
  public int getMerchantBankNumber(String merchant)
  {
    int result = 0;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:57^7*/

//  ************************************************************
//  #sql [Ctx] { select  bank_number
//          
//          from    mif
//          where   merchant_number = :merchant
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  bank_number\n         \n        from    mif\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.maintenance.BankNumberBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchant);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:63^7*/
    }
    catch(SQLException se)
    {
    }
    catch(Exception e)
    {
      logEntry("getMerchantBankNumber(" + merchant + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  public static int getBankNumber(HttpServletRequest request)
  {
    int result = 0;
    try
    {
      String merchant = HttpHelper.getString(request, "merchant", "");
      
      if(!merchant.equals(""))
      {
        BankNumberBean  bnb = new BankNumberBean();
        
        result = bnb.getMerchantBankNumber(merchant);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("com.mes.maintenance.BankNumberBeean::getBankNumber()", e.toString());
    }
    
    return result;
  }
}/*@lineinfo:generated-code*/