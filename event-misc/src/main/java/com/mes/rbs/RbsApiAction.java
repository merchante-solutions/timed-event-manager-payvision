package com.mes.rbs;

import java.math.BigDecimal;
import java.util.Date;
import java.util.regex.Matcher;
import org.apache.log4j.Logger;
import com.mes.ads.AccountType;
import com.mes.ads.AchpDfiType;
import com.mes.ads.AdsDb;
import com.mes.ads.PmgStoredCard;
import com.mes.api.ach.AchTranRequest;
import com.mes.mvc.ApiException;
import com.mes.mvc.mes.MesApiAction;
import com.mes.mvc.mes.MesApiInvalidParmException;
import com.mes.mvc.mes.MesApiMissingParmException;
import com.mes.pach.SecCode;
import com.mes.support.HttpHelper;
import com.mes.support.TridentTools;

public class RbsApiAction extends MesApiAction
{
  static Logger log = Logger.getLogger(RbsApiAction.class);

  public static final String AN_CREATE            = "rbsCreate";
  public static final String AN_UPDATE            = "rbsUpdate";
  public static final String AN_DELETE            = "rbsDelete";
  public static final String AN_SUSPEND           = "rbsSuspend";
  public static final String AN_ACTIVATE          = "rbsActivate";

  public static final String RC_REC_DUPLICATE     = "200";
  public static final String RC_REC_NOT_FOUND     = "201";

  /**
   * API parameter names used to load model from API request data
   */
   
  // required billing record data
  public static final String PARM_PROFILE_ID      = "profileId";
  public static final String PARM_CUSTOMER_ID     = "customer_id";
  public static final String PARM_AMOUNT          = "recur_amount";
  public static final String PARM_START_DATE      = "recur_start_date";
  public static final String PARM_PAY_TOTAL       = "recur_num_pmt";
  public static final String PARM_FREQUENCY       = "recur_frequency";
  
  // optional billing record data
  public static final String PARM_CURRENCY_CODE   = "currency_code";
  public static final String PARM_TAX_AMOUNT      = "tax_amount";
  public static final String PARM_CLIENT_REF_NUM  = "client_reference_number";
  
  // dynamic dba
  public static final String PARM_MERCH_NAME      = "business_name";
  public static final String PARM_MERCH_PHONE     = "phone_number";
  
  // account type indicator: CC or ACHP
  public static final String PARM_PAY_TYPE        = "payment_type";
  
  // credit card account data
  public static final String PARM_STORE_ID        = "card_id";
  public static final String PARM_CARD_NUM        = "card_number";
  public static final String PARM_CARD_TYPE       = "card_type";
  public static final String PARM_EXP_DATE        = "card_exp_date";
  public static final String PARM_CARD_NAME       = "cardholder_name";
  public static final String PARM_AVS_STREET      = "cardholder_street_address";
  public static final String PARM_AVS_ZIP         = "cardholder_zip";

  // achp data
  public static final String PARM_ACH_ACCT_NUM    = "account_num";
  public static final String PARM_ACH_ACCT_TYPE   = "account_type";
  public static final String PARM_TRANSIT_NUM     = "transit_num";
  public static final String PARM_ACH_CUST_NAME   = "cust_name";
  public static final String PARM_ACH_CUST_ID     = "cust_id";
  public static final String PARM_ACH_AUTH_TYPE   = "auth_type";
  public static final String PARM_IP_ADDRESS      = "ip_address";

  // update request data
  public static final String PARM_NEXT_DATE       = "nextDate";
  
  private RbsDb db = new RbsDb();
  
  private boolean hasParm(String parmName)
  {
    return apiRequest.hasParameter(parmName);
  }

  /**
   * Fetches profile id from api request, verifies it is linked to user.
   */
  private String getProfileIdParm()
  {
    String profileId = apiRequest.getParameter(PARM_PROFILE_ID);
    if (!db.profileLinkEnabled(user.getLoginName(),profileId))
    {
      throw new MesApiInvalidParmException(PARM_PROFILE_ID);
    }
    return profileId;
  }

  /**
   * Get customer id from request, validate based on api request name.
   * Should be usable by any api request type...
   */
  private String getCustomerIdParm()
  {
    String customerId = apiRequest.getParameter(PARM_CUSTOMER_ID);

    // if creating must not exist, if updating/deleting/etc. must exist
    boolean isCreate = AN_CREATE.equals(name);
    String profileId = apiRequest.getParameter(PARM_PROFILE_ID);
    if (db.customerIdExists(profileId,customerId) == isCreate)
    {
      throw new ApiException(isCreate ? RC_REC_DUPLICATE : RC_REC_NOT_FOUND);
    }

    // customer ID can't be larger than 17 (stored in invoice num column)
    if (isCreate && customerId.length() > 17)
    {
      throw new MesApiInvalidParmException(PARM_CUSTOMER_ID);
    }
    return customerId;
  }

  /**
   * Validate credit card account number.
   */
  private void validateCardNum(String cardNum)
  { 
    // sanity check on length, mod 10 check
    if (cardNum.length() < 5 || cardNum.length() > 22 ||
        !TridentTools.mod10Check(cardNum))
    {
      throw new MesApiInvalidParmException(PARM_CARD_NUM);
    }
  }
  
  private String getCardNumParm()
  {
    String cardNum = apiRequest.getParameter(PARM_CARD_NUM);
    if (cardNum == null)
    {
      throw new MesApiMissingParmException(PARM_CARD_NUM);
    }
    validateCardNum(cardNum);
    return cardNum;
  }
  
  private String getStoreIdParm()
  {
    return apiRequest.getParameter(PARM_STORE_ID);
  }
    
  
  /**
   * HACK: If stored card id provided fetch account info to store
   * with billing record initially.  Going forward billing record
   * will need to fetch stored card data from PMG table due to
   * potential AUS auto updating.
   */
  private void setStoredCardData(BillingRecord rec)
  {
    String storeId = getStoreIdParm();
    rec.setStoreId(storeId);
    AdsDb ads = new AdsDb();
    PmgStoredCard psc = ads.getPmgStoredCard(rec.getProfileId(),storeId);
    // NOTE: change of plans, we are not going to validate card id's here...
    //if (psc == null)
    //{
    //  throw new MesApiInvalidParmException(PARM_STORE_ID);
    //}
    if (psc != null)
    {
      rec.setAccountNum(psc.getCardNum());
      rec.setExpDate(psc.getExpDate());
      rec.setCardName(psc.getCardName());
    }
  }
  
  /**
   * Acquire credit card specific data from either stored card
   * record or from provided parameters.
   */
  private void resolveCardData(BillingRecord rec)
  {
    if (hasParm(PARM_STORE_ID))
    {
      setStoredCardData(rec);
    }
    else
    {
      rec.setAccountNum(getCardNumParm());
      rec.setExpDate(getExpDateParm());
      if (hasParm(PARM_CARD_NAME))
      {
        rec.setCardName(getCardNameParm());
      }
      if (hasParm(PARM_AVS_STREET))
      {
        rec.setAvsStreet(getAvsStreetParm());
      }
      if (hasParm(PARM_AVS_ZIP))
      {
        rec.setAvsZip(getAvsZipParm());
      }
    }
  }

  /**
   * Validate expiration date.
   *
   * TODO: determine year validation logic
   */
  private void validateExpDate(String expDate)
  {
    try
    {
      int mm = Integer.parseInt(expDate.substring(0,2));
      int yy = Integer.parseInt(expDate.substring(2,4));
      if (mm < 1 || mm > 12 /*|| yy < 11 || yy > 30*/)
      {
        throw new MesApiInvalidParmException(PARM_EXP_DATE);
      }
      if (expDate.length() != 4)
      {
        throw new MesApiInvalidParmException(PARM_EXP_DATE);
      }
    }
    catch (NumberFormatException nfe)
    {
      throw new MesApiInvalidParmException(PARM_EXP_DATE);
    }
  }

  /**
   * Exp date only fetcher, useful for updates.
   */
  private String getExpDateParm()
  {
    String expDate = apiRequest.getParameter(PARM_EXP_DATE);
    if (expDate == null)
    {
      throw new MesApiMissingParmException(PARM_EXP_DATE);
    }
    validateExpDate(expDate);
    return expDate;
  }

  /**
   * Convert a request parm string to a BigDecimal amount.  Returns
   * default if data not found.
   *
   * TODO: support currency code other than 840 - USD
   *       needed: decimals, min amt by currency code
   */
  private BigDecimal getAmountParm(String parmName, AccountType at,
    int currencyCode, BigDecimal defaultAmount)
  {
    String amountStr = apiRequest.getParameter(parmName);
    BigDecimal amount = defaultAmount;
    if (amountStr != null)
    {
      try
      {
        amount = new BigDecimal(amountStr).setScale(2);
        if (amount.compareTo(new BigDecimal("0.01")) < 0)
        {
          throw new Exception("Amount to small: " + amount);
        }
        if (AccountType.CC.equals(at))
        {
          if (amount.compareTo(new BigDecimal("9999999999.99")) > 0)
          {
            throw new Exception("CC amount to large: " + amount);
          }
        }
        else if (AccountType.ACHP.equals(at))
        {
          if (amount.compareTo(new BigDecimal("999999999.99")) > 0)
          {
            throw new Exception("ACHP amount to large: " + amount);
          }
        }
      }
      catch (Exception e)
      {
        throw new MesApiInvalidParmException(parmName);
      }
    }
    return amount;
  }
  private BigDecimal getAmountParm(String parmName, AccountType at,
    BigDecimal defaultAmount)
  {
    return getAmountParm(parmName,at,840,defaultAmount);
  }
  private BigDecimal getAmountParm(String parmName,AccountType at)
  {
    return getAmountParm(parmName,at,null);
  }

  /**
   * Fetch payment amount.
   */
  private BigDecimal getPaymentAmountParm(AccountType at)
  {
    return getAmountParm(PARM_AMOUNT,at);
  }

  /**
   * Fetch tax amount.
   */
  private BigDecimal getTaxAmountParm(AccountType at)
  {
    return getAmountParm(PARM_TAX_AMOUNT,at);
  }

  /**
   * Parse start date parm string to java.util.Date.
   */
  private Date getStartDateParm()
  {
    try
    {
      Date startDate = apiRequest.getParameterAsDate(PARM_START_DATE);
      //if (!Calendar.getInstance().getTime().before(startDate))
      //{
      //  throw new Exception("Start date can't be before current date: " 
      //    + startDate);
      //}
      
      // TODO: may want to revise how date parameters get handled
      // in MesApiRequest...
      if (startDate == null)
      {
        throw new MesApiInvalidParmException(PARM_START_DATE);
      }
      return startDate;
    }
    catch (Exception e)
    {
      throw new MesApiInvalidParmException(PARM_START_DATE);
    }
  }

  /**
   * Parse next date parm string to java.util.Date.
   */
  private Date getNextDateParm()
  {
    try
    {
      Date nextDate = apiRequest.getParameterAsDate(PARM_NEXT_DATE);
      //if (!Calendar.getInstance().getTime().before(nextDate))
      //{
      //  throw new Exception("Next date must be after current date: " 
      //    + nextDate);
      //}
      return nextDate;
    }
    catch (Exception e)
    {
      throw new MesApiInvalidParmException(PARM_NEXT_DATE);
    }
  }

  /**
   * Get payTotal from api request, validate.
   *
   * Zero is used to indicate indefinite payment plan.
   */
  private int getPayTotalParm()
  {
    int payTotal = apiRequest.getParameterAsInt(PARM_PAY_TOTAL);
    // six digit max
    if (payTotal < 0 || payTotal > 999999)
    {
      throw new MesApiInvalidParmException(PARM_PAY_TOTAL);
    }
    return payTotal;
  }

  /**
   * Get frequency from api request, validate it using BillingRecord.
   */
  private String getFrequencyParm()
  {
    String frequency = apiRequest.getParameter(PARM_FREQUENCY);
    try
    {
      BillingRecord.validateFrequency(frequency);
    }
    catch (Exception e)
    {
      throw new MesApiInvalidParmException(PARM_FREQUENCY);
    }
    return frequency;
  }

  /**
   * Get currency code, default to 840 (USD) if not present.
   */
  private int getCurrencyCodeParm()
  {
    int cc = apiRequest.getParameterAsInt(PARM_CURRENCY_CODE);
    if (cc == -1)
    {
      cc = 840;
    }
    if (cc <= 0 || cc > 999)
    {
      throw new MesApiInvalidParmException(PARM_CURRENCY_CODE);
    }
    return cc;
  }

  /**
   * Get dynamic DBA merch name.  Can't exceed length 25.
   * Digit 4, 8 or 13 must be a *.
   * Leading digits before * should always be the same. Not checking this...
   */
  private String getMerchNameParm()
  {
    String nameStr = apiRequest.getParameter(PARM_MERCH_NAME);
    if (nameStr != null)
    {
      if (nameStr.length() > 25 || nameStr.length() < 5)
      {
        throw new MesApiInvalidParmException(PARM_MERCH_NAME);
      }
      try
      {
        if (nameStr.charAt(3) != '*' &&
            nameStr.charAt(7) != '*' &&
            nameStr.charAt(12) != '*')
        {
          throw new MesApiInvalidParmException(PARM_MERCH_NAME);
        }
      }
      catch (Exception e)
      {
        throw new MesApiInvalidParmException(PARM_MERCH_NAME);
      }

    }
    return nameStr;
  }

  /**
   * Get dynamic DBA merch phone.  10 numeric characters allowed.
   */
  private String getMerchPhoneParm()
  {
    String phoneStr = apiRequest.getParameter(PARM_MERCH_PHONE);
    if (phoneStr != null)
    {
      if (phoneStr.length() > 10 || phoneStr.length() <= 0)
      {
        throw new MesApiInvalidParmException(PARM_MERCH_PHONE);
      }
      for (int i = 0; i < phoneStr.length(); ++i)
      {
        if (!Character.isDigit(phoneStr.charAt(i)))
        {
          throw new MesApiInvalidParmException(PARM_MERCH_PHONE);
        }
      }
    }
    return phoneStr;
  }

  /**
   * Get address verification street address. 
   *
   * Truncates at 19 if longer.
   */
  private String getAvsStreetParm()
  {
    String street = apiRequest.getParameter(PARM_AVS_STREET);
    if (street != null && street.length() > 19)
    {
      street = street.substring(0,19);
    }
    return street;
  }

  /**
   * Get address verification zip code. 
   *
   * Must be 5 or 9 digits, must be numeric.
   */
  private String getAvsZipParm()
  {
    String zipStr = apiRequest.getParameter(PARM_AVS_ZIP);
    if (zipStr != null)
    {
      if (zipStr.length() <= 0 || zipStr.length() > 9)
      {
        throw new MesApiInvalidParmException(PARM_AVS_ZIP);
      }
      //if (zipStr.length() != 5 && zipStr.length() != 9)
      //{
      //  throw new MesApiInvalidParmException(PARM_AVS_ZIP);
      //}
      //for (int i = 0; i < zipStr.length(); ++i)
      //{
      //  if (!Character.isDigit(zipStr.charAt(i)))
      //  {
      //    throw new MesApiInvalidParmException(PARM_AVS_ZIP);
      //  }
      //}
    }
    return zipStr;
  }

  /**
   * Get client reference number.  Can't exceed 96 characters.
   */
  private String getClientRefNumParm()
  {
    String refNum = apiRequest.getParameter(PARM_CLIENT_REF_NUM);
    if (refNum != null && refNum.length() > 96)
    {
      throw new MesApiInvalidParmException(PARM_CLIENT_REF_NUM);
    }
    return refNum;
  }
  
  /**
   * Get account type: CC or ACHP
   */
  private AccountType getAccountTypeParm()
  {
    String typeStr = apiRequest.getParameter(PARM_PAY_TYPE);
    AccountType at = AccountType.forType(typeStr,null);
    if (at == null)
    {
      throw new MesApiInvalidParmException(PARM_PAY_TYPE);
    }
    return at;
  }
  
  /**
   * Credit cardholder name, 64 chars allowed.
   */
  private String getCardNameParm()
  {
    String cardName = apiRequest.getParameter(PARM_CARD_NAME);
    if (cardName != null && cardName.length() > 64)
    {
      throw new MesApiInvalidParmException(PARM_CARD_NAME);
    }
    return cardName;
  }
  
  /**
   * ACHP account number
   */
  private String getAchAccountNumParm()
  {
    String accountNum = apiRequest.getParameter(PARM_ACH_ACCT_NUM);
    if (accountNum == null)
    {
      throw new MesApiMissingParmException(PARM_ACH_ACCT_NUM);
    }
    return accountNum;
  }
  
  /**
   * ACHP Account Type (DFI type)
   */
  private AchpDfiType getDfiTypeParm()
  {
    String typeStr = apiRequest.getParameter(PARM_ACH_ACCT_TYPE);
    if (typeStr == null)
    {
      throw new MesApiMissingParmException(PARM_ACH_ACCT_TYPE);
    }
    AchpDfiType dt = AchpDfiType.forType(typeStr,null);
    if (dt == null)
    {
      throw new MesApiInvalidParmException(PARM_ACH_ACCT_TYPE);
    }
    return dt;
  }
  
  /**
   * ACHP transit routing number (9 characters numeric)
   */
  private String getTransitNumParm()
  {
    String transitNum = apiRequest.getParameter(PARM_TRANSIT_NUM);
    if (transitNum == null)
    {
      throw new MesApiMissingParmException(PARM_TRANSIT_NUM);
    }
    if (transitNum.length() != 9)
    {
      throw new MesApiInvalidParmException(PARM_TRANSIT_NUM);
    }
    for (int i = 0; i < transitNum.length(); ++i)
    {
      if (!Character.isDigit(transitNum.charAt(i)))
      {
        throw new MesApiInvalidParmException(PARM_TRANSIT_NUM);
      }
    }
    return transitNum;
  }
  
  /**
   * ACHP customer name, 22 chars
   */
  private String getAchCustomerNameParm()
  {
    String custName = apiRequest.getParameter(PARM_ACH_CUST_NAME);
    if (custName != null && custName.length() > 22)
    {
      throw new MesApiInvalidParmException(PARM_ACH_CUST_NAME);
    }
    return custName;
  }
  
  /**
   * ACHP customer id, 15 chars
   */
  private String getAchCustomerIdParm()
  {
    String custId = apiRequest.getParameter(PARM_ACH_CUST_ID);
    if (custId != null && custId.length() > 15)
    {
      throw new MesApiInvalidParmException(PARM_ACH_CUST_ID);
    }
    return custId;
  }
  
  /**
   * ACHP sec code (auth type): WEB, PPD, CCD allowed
   */
  private SecCode getAchAuthTypeParm()
  {
    String codeStr = apiRequest.getParameter(PARM_ACH_AUTH_TYPE);
    if (codeStr == null)
    {
      throw new MesApiMissingParmException(PARM_ACH_AUTH_TYPE);
    }
    SecCode sc = SecCode.codeFor(codeStr);
    if (!SecCode.WEB.equals(sc) && 
        !SecCode.PPD.equals(sc) && 
        !SecCode.CCD.equals(sc))
    {
      throw new MesApiInvalidParmException(PARM_ACH_AUTH_TYPE);
    }
    return sc;
  }
  
  /**
   * ACHP IP address, uses AchTranRequest to validate value.
   */
  private String getIpAddressParm()
  {
    String ipAddress = apiRequest.getParameter(PARM_IP_ADDRESS);
    if (ipAddress == null)
    {
      throw new MesApiMissingParmException(PARM_IP_ADDRESS);
    }
    Matcher matcher = AchTranRequest.ipPattern.matcher(ipAddress);
    if (!matcher.matches())
    {
      throw new MesApiInvalidParmException(PARM_IP_ADDRESS);
    }
    return ipAddress;
  }
  
  /**
   * Create a new billing record.
   *
   * TODO: stored card ID is not handled currently (ignored)
   */
  public void doRbsCreate()
  {
    BillingRecord rec = new BillingRecord();

    // billing record id
    rec.setProfileId(getProfileIdParm());
    rec.setUserName(user.getLoginName());
    rec.setCustomerId(getCustomerIdParm());

    // payment type
    AccountType at = getAccountTypeParm();
    rec.setAccountType(at);
    
    // recurring billing/transaction data
    rec.setAmount(getPaymentAmountParm(at));
    rec.setStartDate(getStartDateParm());
    rec.setPayTotal(getPayTotalParm());
    rec.setFrequency(getFrequencyParm());

    // optional billing record items
    rec.setCurrencyCode(getCurrencyCodeParm());
    if (hasParm(PARM_TAX_AMOUNT))
    {
      rec.setTaxAmount(getTaxAmountParm(at));
    }
    if (hasParm(PARM_CLIENT_REF_NUM))
    {
      rec.setClientRefNum(getClientRefNumParm());
    }

    // dynamic dba only supported by cc, but allow for achp for now?
    if (hasParm(PARM_MERCH_NAME))
    {
      rec.setMerchName(getMerchNameParm());
    }
    if (hasParm(PARM_MERCH_PHONE))
    {
      rec.setMerchPhone(getMerchPhoneParm());
    }
      
    // credit cards
    if (AccountType.CC.equals(at))
    {
      resolveCardData(rec);
    }
    // achp
    else if (AccountType.ACHP.equals(at))
    {
      rec.setAccountNum(getAchAccountNumParm());
      rec.setDfiType(getDfiTypeParm());
      rec.setTransitNum(getTransitNumParm());
      if (hasParm(PARM_ACH_CUST_NAME))
      {
        rec.setAchCustomerName(getAchCustomerNameParm());
      }
      if (hasParm(PARM_ACH_CUST_ID))
      {
        rec.setAchCustomerId(getAchCustomerIdParm());
      }
      rec.setAchAuthType(getAchAuthTypeParm());
      if (hasParm(PARM_IP_ADDRESS))
      {
        rec.setIpAddress(getIpAddressParm());
      }
      else if (SecCode.WEB.equals(rec.getAchAuthType()))
      {
        throw new MesApiMissingParmException(PARM_IP_ADDRESS);
      }
    }
    else
    {
      throw new MesApiInvalidParmException(PARM_PAY_TYPE);
    }

    rec.setTestFlag(HttpHelper.isProdServer(null) ? "N" : "Y");

    rec = db.insertBillingRecord(rec);
    
    if (user.getLoginName().equals("tbaker"))
    {
      apiResponse.setParameter("rbsId",""+rec.getRbsId());
    }
  }
  
  private void checkParmAllowed(AccountType allowedAt, 
    AccountType actualAt, String parmName)
  {
    if (!allowedAt.equals(actualAt))
    {
      throw new MesApiInvalidParmException(parmName);
    }
  }

  /**
   * Make changes to existing billing record.
   */
  public void doRbsUpdate()
  {
    String profileId = getProfileIdParm();
    String customerId = getCustomerIdParm();
    BillingRecord rec = db.getBillingRecord(profileId,customerId);
    AccountType at = rec.getAccountType();
    
    log.debug("fetched record: " + rec);

    // makes sure the record is loaded
    if (rec == null)
    {
      throw new ApiException(RC_REC_NOT_FOUND);
    }

    boolean hasChange = false;
    
    // payment amount
    if (hasParm(PARM_AMOUNT))
    {
      rec.setAmount(getPaymentAmountParm(at));
      hasChange = true;
    }

    // tax amount
    if (hasParm(PARM_TAX_AMOUNT))
    {
      rec.setTaxAmount(getTaxAmountParm(at));
      hasChange = true;
    }

    // next date
    if (hasParm(PARM_NEXT_DATE))
    {
      rec.setNextDate(getNextDateParm());
      hasChange = true;
    }

    // pay count
    if (hasParm(PARM_PAY_TOTAL))
    {
      rec.setPayTotal(getPayTotalParm());
      hasChange = true;
    }

    // currency code
    if (hasParm(PARM_CURRENCY_CODE))
    {
      rec.setCurrencyCode(getCurrencyCodeParm());
      hasChange = true;
    }

    // client ref num
    if (hasParm(PARM_CLIENT_REF_NUM))
    {
      rec.setClientRefNum(getClientRefNumParm());
      hasChange = true;
    }

    // merch name
    if (hasParm(PARM_MERCH_NAME))
    {
      rec.setMerchName(getMerchNameParm());
      hasChange = true;
    }

    // merch phone
    if (hasParm(PARM_MERCH_PHONE))
    {
      rec.setMerchPhone(getMerchPhoneParm());
      hasChange = true;
    }

    /**
     * Credit card account data
     */
     
    // stored card id
    if (hasParm(PARM_STORE_ID))
    {
      if (!rec.hasStoreId())
      {
        throw new MesApiInvalidParmException(PARM_STORE_ID);
      }
      rec.setStoreId(getStoreIdParm());
      hasChange = true;
    }
    
    // card num 
    if (hasParm(PARM_CARD_NUM))
    {
      checkParmAllowed(AccountType.CC,at,PARM_CARD_NUM);
      if (rec.hasStoreId())
      {
        throw new MesApiInvalidParmException(PARM_CARD_NUM);
      }
      rec.setAccountNum(getCardNumParm());
      hasChange = true;
    }

    // exp date
    if (hasParm(PARM_EXP_DATE))
    {
      checkParmAllowed(AccountType.CC,at,PARM_EXP_DATE);
      if (rec.hasStoreId())
      {
        throw new MesApiInvalidParmException(PARM_CARD_NUM);
      }
      rec.setExpDate(getExpDateParm());
      hasChange = true;
    }

    // avs street
    if (hasParm(PARM_AVS_STREET))
    {
      checkParmAllowed(AccountType.CC,at,PARM_AVS_STREET);
      rec.setAvsStreet(getAvsStreetParm());
      hasChange = true;
    }

    // avs zip
    if (hasParm(PARM_AVS_ZIP))
    {
      checkParmAllowed(AccountType.CC,at,PARM_AVS_ZIP);
      rec.setAvsZip(getAvsZipParm());
      hasChange = true;
    }
    
    /**
     * ACHP account data
     */

    // achp account num
    if (hasParm(PARM_ACH_ACCT_NUM))
    {
      checkParmAllowed(AccountType.ACHP,at,PARM_ACH_ACCT_NUM);
      rec.setAccountNum(getAchAccountNumParm());
      hasChange = true;
    }
    
    // achp checking/saving account type
    if (hasParm(PARM_ACH_ACCT_TYPE))
    {
      checkParmAllowed(AccountType.ACHP,at,PARM_ACH_ACCT_TYPE);
      rec.setDfiType(getDfiTypeParm());
      hasChange = true;
    }

    // achp transit num
    if (hasParm(PARM_TRANSIT_NUM))
    {
      checkParmAllowed(AccountType.ACHP,at,PARM_TRANSIT_NUM);
      rec.setTransitNum(getTransitNumParm());
      hasChange = true;
    }

    // achp customer name
    if (hasParm(PARM_ACH_CUST_NAME))
    {
      checkParmAllowed(AccountType.ACHP,at,PARM_ACH_CUST_NAME);
      rec.setAchCustomerName(getAchCustomerNameParm());
      hasChange = true;
    }

    // achp customer id
    if (hasParm(PARM_ACH_CUST_ID))
    {
      checkParmAllowed(AccountType.ACHP,at,PARM_ACH_CUST_ID);
      rec.setAchCustomerId(getAchCustomerIdParm());
      hasChange = true;
    }
    
    // achp sec code
    if (hasParm(PARM_ACH_AUTH_TYPE))
    {
      checkParmAllowed(AccountType.ACHP,at,PARM_ACH_AUTH_TYPE);
      rec.setAchAuthType(getAchAuthTypeParm());
      hasChange = true;
    }
    
    // ip address
    if (hasParm(PARM_IP_ADDRESS))
    {
      checkParmAllowed(AccountType.ACHP,at,PARM_IP_ADDRESS);
      rec.setIpAddress(getIpAddressParm());
      hasChange = true;
    }
      
    // update db record if a change was found
    if (hasChange)
    {
      db.updateBillingRecord(rec);
    }
  }

  /**
   * Delete billing record.
   *
   * NOTE: this is most likely a stupid idea.
   */
  public void doRbsDelete()
  {
    String profileId = getProfileIdParm();
    String customerId = getCustomerIdParm();
    db.deleteBillingRecord(profileId,customerId);
  }
  
  /**
   * Fetch billing record details.
   */
  public void doRbsInquiry()
  {
    String profileId = getProfileIdParm();
    String customerId = getCustomerIdParm();
    BillingRecord rec = db.getBillingRecord(profileId,customerId);
        
    // makes sure the record is loaded
    if (rec == null)
    {
      throw new ApiException(RC_REC_NOT_FOUND);
    }
    
    int failCount = db.getFailedPaymentCount(rec);
    
    apiResponse.setParameter("num_completed_payments",""+rec.getPayCount());
    apiResponse.setParameter("num_failed_payments",""+failCount);
    apiResponse.setParameter("next_payment",rec.formatDate(rec.getNextDate(),"MM/dd/yyyy"));
    
    log.debug("inquiry record found: " + rec);
  }
}
