package com.mes.rbs;

import org.apache.log4j.Logger;
import com.mes.mvc.mes.MesAction;
import com.mes.mvc.mes.MesViewBean;

public class RbsViewBean extends MesViewBean
{
  static Logger log = Logger.getLogger(RbsViewBean.class);

  private RbsDb db;
  
  public RbsViewBean(MesAction action)
  {
    super(action);
  }

  protected RbsDb db()
  {
    if (db == null)
    {
      db = new RbsDb();
    }
    return db;
  }
}
