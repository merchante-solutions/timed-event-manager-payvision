package com.mes.rbs;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.ads.AccountType;
import com.mes.ads.AchpDfiType;
import com.mes.ads.AdsDb;
import com.mes.ads.PmgStoredCard;
import com.mes.api.ach.AchTranRequest;
import com.mes.forms.ButtonField;
import com.mes.forms.Condition;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateStringField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.RadioField;
import com.mes.forms.Validation;
import com.mes.mvc.mes.MesAction;
import com.mes.pach.SecCode;
import com.mes.tools.DropDownTable;

public class EditBillingRecordBean extends RbsViewBean
{
  static Logger log = Logger.getLogger(EditBillingRecordBean.class);

  public static final String FN_RBS_ID          = "rbsId";
  public static final String FN_CUSTOMER_ID     = "customerId";
  public static final String FN_PAYMENT_TYPE    = "paymentType";
  public static final String FN_AMOUNT          = "amount";
  public static final String FN_TAX_AMOUNT      = "taxAmount";
  public static final String FN_NEXT_DATE       = "nextDate";
  public static final String FN_PAY_TOTAL       = "payTotal";
  public static final String FN_FREQUENCY       = "frequency";
  public static final String FN_CURRENCY_CODE   = "currencyCode";
  public static final String FN_CLIENT_REF_NUM  = "clientRefNum";

  public static final String FN_STORE_ID        = "storeId";
  public static final String FN_CARD_NUM        = "cardNum";
  public static final String FN_EXP_DATE        = "expDate";
  public static final String FN_AVS_STREET      = "avsStreet";
  public static final String FN_AVS_ZIP         = "avsZip";
  public static final String FN_MERCH_NAME      = "merchName";
  public static final String FN_MERCH_PHONE     = "merchPhone";
  
  public static final String FN_ACCOUNT_NUM     = "accountNum";
  public static final String FN_DFI_TYPE        = "dfiType";
  public static final String FN_TRANSIT_NUM     = "transitNum";
  public static final String FN_SEC_CODE        = "secCode";
  public static final String FN_CUST_NAME       = "custName";
  public static final String FN_CUST_ID         = "custId";
  public static final String FN_IP_ADDR         = "ipAddress";
  
  public static final String FN_TEST_FLAG       = "testFlag";
  public static final String FN_SUBMIT_BTN      = "submitBtn";

  private BillingRecord rec;
  private PmgStoredCard psc;

  public EditBillingRecordBean(MesAction action)
  {
    super(action);
  }

  public class FrequencyTable extends DropDownTable
  {
    public FrequencyTable()
    {
      addElement(BillingRecord.FREQ_MONTHLY,"Monthly");
      addElement(BillingRecord.FREQ_QUARTERLY,"Quarterly");
      addElement(BillingRecord.FREQ_ANNUALLY,"Annually");
    }
  }

  public class AccountTypeCondition implements Condition
  {
    private Field atField;
    private AccountType acctType;
    
    public AccountTypeCondition(Field atField, AccountType acctType)
    {
      this.atField = atField;
      this.acctType = acctType;
    }
    
    public boolean isMet()
    {
      if (!atField.isBlank())
      {
        return acctType.equals(AccountType.forType(atField.getData()));
      }
      return false;
    }
  }
  
  /**
   * NOTE: no longer doing card_id validation in RBS
   */
  public class StoreIdValidation implements Validation
  {
    private String profileId;
    
    public StoreIdValidation(String profileId)
    {
      this.profileId = profileId;
    }
    
    public String getErrorText()
    {
      return "Invalid stored card ID";
    }
    public boolean validate(String data)
    {
      if (data != null && data.length() != 0)
      {
        AdsDb ads = new AdsDb();
        if (ads.getPmgStoredCard(profileId,data) == null)
        {
          return false;
        }
      }
      return true;
    }
  }
  
  public class IpAddressValidation implements Validation
  {
    private AccountTypeCondition condition;
    private String errorText;
    
    public IpAddressValidation(AccountTypeCondition condition)
    {
      this.condition = condition;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
    
    public boolean validate(String data)
    {
      if (condition.isMet() && data != null && data.length() != 0)
      {
        Matcher matcher = AchTranRequest.ipPattern.matcher(data);
        if (!matcher.matches())
        {
          errorText = "Invalid IP Address";
          return false;
        }
      }
      return true;
    }
  }
  
  public class DfiTypeRadioField extends RadioField
  {
    public DfiTypeRadioField(String name, String label)
    {
      super(name,label);
      addButton(""+AchpDfiType.CHECKING,"Checking");
      addButton(""+AchpDfiType.SAVINGS,"Savings");
      setRenderStyle(RS_HORIZONTAL);
    }
  }

  public class SecCodeRadioField extends RadioField
  {
    public SecCodeRadioField(String name, String label)
    {
      super(name,label);
      addButton(""+SecCode.WEB,""+SecCode.WEB);
      addButton(""+SecCode.CCD,""+SecCode.CCD);
      addButton(""+SecCode.PPD,""+SecCode.PPD);
      setRenderStyle(RS_HORIZONTAL);
    }
  }

  public BillingRecord getBillingRecord()
  {
    return rec;
  }
  
  public PmgStoredCard getPmgStoredCard()
  {
    return psc;      
  }

  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      // billing details
      fields.add(new HiddenField(FN_RBS_ID));
      fields.add(new HiddenField(FN_TEST_FLAG));
      fields.add(new HiddenField(FN_CUSTOMER_ID));
      fields.add(new HiddenField(FN_FREQUENCY));
      fields.add(new HiddenField(FN_PAYMENT_TYPE));
      fields.add(new DateStringField(FN_NEXT_DATE,"Next Date",false,false));
      fields.add(new NumberField(FN_PAY_TOTAL,"Total Payments",6,6,true,0));
      fields.add(new NumberField(FN_CURRENCY_CODE,"Currency Code",3,3,true,0));
      fields.add(new CurrencyField(FN_AMOUNT,"Amount",10,10,false));
      fields.add(new CurrencyField(FN_TAX_AMOUNT,"Tax Amount",10,10,true));
      fields.add(new NumberField(FN_CURRENCY_CODE,"Currency Code",3,3,true,0));
      fields.add(new Field(FN_CLIENT_REF_NUM,"Client Reference Number",96,16,true));

      Field fPayType = fields.getField(FN_PAYMENT_TYPE);
      
      // credit card account data
      AccountTypeCondition ccAtc = new AccountTypeCondition(fPayType,AccountType.CC);
      fields.add(new Field(FN_CARD_NUM,"New Card Number",32,16,true));
      fields.add(new Field(FN_STORE_ID,"New Stored Card ID",32,32,true));
      fields.add(new Field(FN_EXP_DATE,"Expiration Date",4,4,false));
      fields.getField(FN_EXP_DATE).setOptionalCondition(ccAtc);
      fields.add(new Field(FN_AVS_STREET,"AVS Street Address",19,16,true));
      fields.add(new Field(FN_AVS_ZIP,"AVS Zip Code",9,9,true));
      fields.add(new Field(FN_MERCH_NAME,"Dynamic DBA",25,16,true));
      fields.add(new Field(FN_MERCH_PHONE,"Dynamic DBA Phone",10,10,true));
      
      // achp account data
      AccountTypeCondition achpAtc = new AccountTypeCondition(fPayType,AccountType.ACHP);
      fields.add(new Field(FN_ACCOUNT_NUM,"New Account Number",32,16,true));
      fields.add(new Field(FN_TRANSIT_NUM,"Transit Routing Number",9,16,false));
      fields.getField(FN_TRANSIT_NUM).setOptionalCondition(achpAtc);
      fields.add(new DfiTypeRadioField(FN_DFI_TYPE,"Account Type"));
      fields.add(new Field(FN_CUST_NAME,"Customer Name",22,16,true));
      fields.add(new Field(FN_CUST_ID,"Customer ID",15,16,true));
      fields.add(new SecCodeRadioField(FN_SEC_CODE,"Authorization Type"));
      fields.add(new Field(FN_IP_ADDR,"IP Address",15,16,true));
      fields.getField(FN_IP_ADDR).addValidation(new IpAddressValidation(achpAtc));
      
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }
  
  /**
   * Use the post set field hook to load the billing record.
   */
  protected void postHandleRequest(HttpServletRequest request)
  {
    long rbsId = getField(FN_RBS_ID).asLong();
    if (rbsId != 0L)
    {
      rec = db().getBillingRecord(rbsId);
      if (rec.hasStoreId()) {
        // retrieve stored card data via card_id only now...
        psc = new AdsDb().getPmgStoredCard(rec.getStoreId());
        // unset the exp date required validation
        fields.getField(FN_EXP_DATE).makeOptional();
      }
    }
    else
    {
      log.warn("Unable to load billing record, rbsId not found in request");
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }

  private void loadFieldsFromRecord(BillingRecord rec)
  {
    setData(FN_RBS_ID,""+rec.getRbsId());
    setData(FN_TEST_FLAG,rec.getTestFlag());
    ((DateStringField)getField(FN_NEXT_DATE)).setUtilDate(rec.getNextDate());
    setData(FN_CUSTOMER_ID,rec.getCustomerId());
    setData(FN_FREQUENCY,rec.getFrequency());
    setData(FN_PAYMENT_TYPE,""+rec.getAccountType());
    setData(FN_PAY_TOTAL,""+rec.getPayTotal());
    setData(FN_CURRENCY_CODE,""+rec.getCurrencyCode());
    if (!rec.getAmount().equals(new BigDecimal("0")))
    {
      setData(FN_AMOUNT,""+rec.getAmount());
    }
    if (!rec.getTaxAmount().equals(new BigDecimal("0")))
    {
      setData(FN_TAX_AMOUNT,""+rec.getTaxAmount());
    }
    setData(FN_CLIENT_REF_NUM,rec.getClientRefNum());

    if (rec.isCc())
    {
      if (!rec.hasStoreId())
      {
        setData(FN_EXP_DATE,rec.getExpDate());
      }
      setData(FN_AVS_STREET,rec.getAvsStreet());
      setData(FN_AVS_ZIP,rec.getAvsZip());
      setData(FN_MERCH_NAME,rec.getMerchName());
      setData(FN_MERCH_PHONE,rec.getMerchPhone());
    }
    else if (rec.isAchp())
    {
      setData(FN_DFI_TYPE,""+rec.getDfiType());
      setData(FN_SEC_CODE,""+rec.getAchAuthType());
      setData(FN_TRANSIT_NUM,rec.getTransitNum());
      setData(FN_CUST_NAME,rec.getAchCustomerName());
      setData(FN_CUST_ID,rec.getAchCustomerId());
      setData(FN_IP_ADDR,rec.getIpAddress());
    }
  }

  protected boolean autoLoad()
  {
    try
    {
      loadFieldsFromRecord(rec);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }

  protected boolean autoSubmit()
  {
    try
    {
      long rbsId = getField(FN_RBS_ID).asLong();
      rec = db().getBillingRecord(rbsId);
      rec.setPayTotal(Integer.parseInt(getData(FN_PAY_TOTAL)));
      rec.setCurrencyCode(Integer.parseInt(getData(FN_CURRENCY_CODE)));
      rec.setAmount(new BigDecimal(getData(FN_AMOUNT)).setScale(2));
      rec.setTaxAmount(new BigDecimal(getData(FN_TAX_AMOUNT)).setScale(2));
      rec.setClientRefNum(getData(FN_CLIENT_REF_NUM));
      rec.setNextDate(
        ((DateStringField)getField(FN_NEXT_DATE)).getUtilDate());

      if (rec.isCc())
      {
        // no stored id means credit card and exp date stored with record
        if (!rec.hasStoreId())
        {
          // update cc account num if it is given
          if (!isFieldBlank(FN_CARD_NUM))
          {
            rec.setAccountNum(getData(FN_CARD_NUM));
          }
          rec.setExpDate(getData(FN_EXP_DATE));
        }
        else
        {
          // update stored card id if it is given
          if (!isFieldBlank(FN_STORE_ID))
          {
            rec.setStoreId(getData(FN_STORE_ID));
          }
        }
        rec.setAvsStreet(getData(FN_AVS_STREET));
        rec.setAvsZip(getData(FN_AVS_ZIP));
        rec.setMerchName(getData(FN_MERCH_NAME));
        rec.setMerchPhone(getData(FN_MERCH_PHONE));
      }
      else if (rec.isAchp())
      {
        // update achp account num if it is given
        if (!isFieldBlank(FN_ACCOUNT_NUM))
        {
          rec.setAccountNum(getData(FN_ACCOUNT_NUM));
        }
        rec.setDfiType(AchpDfiType.forType(getData(FN_DFI_TYPE)));
        rec.setAchAuthType(SecCode.codeFor(getData(FN_SEC_CODE)));
        rec.setTransitNum(getData(FN_TRANSIT_NUM));
        rec.setAchCustomerName(getData(FN_CUST_NAME));
        rec.setAchCustomerId(getData(FN_CUST_ID));
        rec.setIpAddress(getData(FN_IP_ADDR));
      }
      
      rec = db().updateBillingRecord(rec);
      loadFieldsFromRecord(rec);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }
}