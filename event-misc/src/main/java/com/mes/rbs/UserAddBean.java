package com.mes.rbs;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.Mvc;
import com.mes.mvc.mes.MesAction;

public class UserAddBean extends RbsViewBean
{
  static Logger log = Logger.getLogger(UserAddBean.class);

  public static final String FN_LOGIN_NAME    = "loginName";
  public static final String FN_SUBMIT_BTN    = "submitBtn";

  public UserAddBean(MesAction action)
  {
    super(action);
  }
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new Field(FN_LOGIN_NAME,"User",64,32,false));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));
      fields.add(new HiddenField(Mvc.DISCARD_ARGS_FLAG)); 
      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  protected boolean showFeedback()
  {
    return false;
  }

  protected boolean autoSubmit()
  {
    try
    {
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }
}