package com.mes.rbs;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.mvc.Downloadable;
import com.mes.support.MesCalendar;

public class BillingRecordDownloader implements Downloadable
{
  static Logger log = Logger.getLogger(BillingRecordDownloader.class);
  
  private List recs;
  private String profileId;
  private RbsDb db = new RbsDb();
  
  public void setBillingRecords(List recs)
  {
    this.recs = recs;
  }
  
  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  
  private Iterator dli;
  
  public void startDownload()
  {
    dli = recs.iterator();
  }
  
  public String getDlHeader()
  {
    return "\"RBS ID\""
      + ",\"Customer ID\""
      + ",\"Start Date\""
      + ",\"Next Date\""
      + ",Frequency"
      + ",\"Account Number\""
      + ",Amount"
      + ",Currency"
      + ",\"Payments So Far\""
      + ",\"Total Payments\""
      + ",Test?"
      + ",\"Client Reference Number\"";
  }
  
  public String getDlNext()
  {
    if (dli == null || !dli.hasNext())
    {
      return null;
    }
    
    BillingRecord rec = (BillingRecord)dli.next();
    String startDateStr = MesCalendar.formatDate(rec.getStartDate(),MesCalendar.FMT_DATE2);
    String nextDateStr = MesCalendar.formatDate(rec.getNextDate(),MesCalendar.FMT_DATE2);

    return "\"" + rec.getRbsId() + "\","
      + "\"" + rec.getCustomerId() + "\","
      + "\"" + startDateStr + "\","
      + "\"" + nextDateStr + "\","
      + "\"" + rec.getFrequency() + "\","
      + "\"" + rec.getAccountNumTrunc() + "\","
      + "\"" + rec.getAmount() + "\","
      + "\"" + rec.getCurrencyCode() + "\","
      + "\"" + rec.getPayCount() + "\","
      + "\"" + rec.getPayTotal() + "\","
      + "\"" + (rec.isTest() ? "Y" : "N") + "\","
      + "\"" + rec.getClientRefNum() + "\",";
  }
  
  public void finishDownload()
  {
    dli = null;
  }
  
  public String getDlFilename()
  {
    return "billing_records.csv";
  }
}