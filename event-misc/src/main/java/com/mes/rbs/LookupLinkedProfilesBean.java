package com.mes.rbs;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.HiddenField;
import com.mes.mvc.Mvc;
import com.mes.mvc.mes.MesAction;

/**
 * Used to lookup linked RBS profiles.  Similar to LookupProfilesBean,
 * differs in that the search term is optional and the lookup method
 * called.
 */
public class LookupLinkedProfilesBean extends RbsViewBean
{
  static Logger log = Logger.getLogger(LookupLinkedProfilesBean.class);

  public static final String FN_SEARCH_TERM   = "searchTerm";
  public static final String FN_SUBMIT_BTN    = "submitBtn";

  private List rows;

  public LookupLinkedProfilesBean(MesAction action)
  {
    super(action);
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);

    try
    {
      fields.add(new Field(FN_SEARCH_TERM,"Search Term",64,32,true));
      fields.add(new ButtonField(FN_SUBMIT_BTN,"submit"));

      // bean will be session stored, so have 
      // back links discard form field parameters
      fields.add(new HiddenField(Mvc.DISCARD_ARGS_FLAG)); 

      fields.setShowErrorText(true);
    }
    catch( Exception e )
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }

  public List getRows()
  {
    if (rows == null)
    {
      rows = new ArrayList();
    }
    return rows;
  }

  protected boolean showFeedback()
  {
    return false;
  }

  protected boolean autoSubmit()
  {
    try
    {
      String searchTerm = getData(FN_SEARCH_TERM);
      // pass in a login name if user does not have right to view all profiles
      // this will cause lookup to be filtered by the user's linked profiles
      String loginName = !user.hasRight(6052) ? user.getLoginName() : null;
      rows = db().lookupLinkedProfileRefs(searchTerm,loginName);
      return true;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    return false;
  }
}