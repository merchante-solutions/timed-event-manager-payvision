package com.mes.rbs;

import org.apache.log4j.Logger;
import com.mes.support.TridentTools;
import com.mes.support.dc.MesEncryptedDataContainer;

public class RbsAccount extends MesEncryptedDataContainer
{
  static Logger log = Logger.getLogger(RbsAccount.class);

  private String _truncated;
  private String expDate;

  public RbsAccount()
  {
  }
  public RbsAccount(String accountNum, String expDate)
  {
    setData(accountNum);
    setExpDate(expDate);
  }

  public String clearText()
  {
    return empty() ? null : new String(getData());
  }

  public String truncated()
  {
    if (!empty() && _truncated == null)
    {
      try
      {
        _truncated = 
          TridentTools.encodeCardNumber(clearText());
      }
      catch (Exception e)
      {
        log.error("Truncation error: " + e);
        e.printStackTrace();
      }
    }
    return _truncated;
  }

  public void setEncodedData(byte[] data)
  {
    super.setEncodedData(data);
    _truncated = null;
  }

  public void setAccountNum(String accountNum)
  {
    setData(accountNum.getBytes());
  }
  public String getAccountNum()
  {
    return clearText();
  }

  public void setExpDate(String expDate)
  {
    this.expDate = expDate;
  }
  public String getExpDate()
  {
    return expDate;
  }

  public String toString()
  {
    return "RbsAccount [ "
      + ", acct num: " + truncated()
      + ", exp date: " + expDate
      + " ]";
  }
}

