package com.mes.rbs;

import java.util.List;
import org.apache.log4j.Logger;
import com.mes.mvc.mes.MesAction;
import com.mes.support.HttpHelper;

public class RbsAction extends MesAction
{
  static Logger log = Logger.getLogger(RbsAction.class);

  protected RbsDb db = new RbsDb();

  public static final String SN_LOOKUP_PROFILES_BEAN  = "lookupProfilesBean";
  public static final String SN_LOOKUP_LINKED_PROFILES_BEAN 
                                                = "lookupLinkedProfilesBean";

  /**
   * Look for profile selection bean in the session, create on if needed.
   */
  private void setLookupProfilesBean()
  {
    LookupProfilesBean bean = (LookupProfilesBean)
      getSessionAttr(SN_LOOKUP_PROFILES_BEAN,null);
    if (bean == null)
    {
      bean = new LookupProfilesBean(this);
      setSessionAttr(SN_LOOKUP_PROFILES_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
  }

  /**
   * Lookup all profiles matching a search term.
   * Do the admin version of profile selection.
   */
  public void doRbsAdminProSelect()
  {
    setLookupProfilesBean();
    doView("rbsAdminProSelect");
  }

  public static final String SN_PROFILE_REF = "profileRef";

  private ProfileRef setProfileRef(String profileId)
  {
    // see if profile ref is stored in session
    ProfileRef ref = (ProfileRef)getSessionAttr(SN_PROFILE_REF,null);

    // try to fetch profile id from request if not provided by caller
    if (profileId == null)
    {
      profileId = getParm("profileId",null);
    }

    // reload using profile id parm if available
    if (profileId != null)
    {
      ref = db.getProfileRef(profileId);
    }
    // reload using session stored ref if available
    else if (ref != null)
    {
      ref = db.getProfileRef(ref.getProfileId());
    }

    // store ref in session and request scopes
    setSessionAttr(SN_PROFILE_REF,ref);
    setRequestAttr("ref",ref);

    return ref;
  }

  /** 
   * Used to clear a selected profile ref from the session.
   */
  private void reloadProfileRef()
  { 
    setProfileRef(null);
  }

  /**
   * For a selected profile provides administration functionality.
   * (add/delete user profile links, enable/disable profile).
   */
  public void doRbsAdminProEdit()
  {
    ProfileRef ref = setProfileRef(getParm("profileId",null));
    String profileId = ref.getProfileId();
    UserAddBean bean = new UserAddBean(this);
    if (bean.isAutoSubmitOk())
    {
      String loginName = bean.getData(bean.FN_LOGIN_NAME);
      if (!db.tridentProfileExists(profileId))
      {
        addFeedback("Invalid profile ID: '" + profileId + "'");
      }
      else if (!db.loginNameExists(loginName))
      {
        addFeedback("Invalid login name: '" + loginName + "'");
      }
      else if (db.getUserProfileLink(loginName,profileId) != null)
      {
        addFeedback("User already linked to profile");
      }
      else
      {
        if (!db.profileExists(profileId))
        {
          RbsProfile profile = new RbsProfile();
          profile.setProfileId(profileId);
          profile.setEnableFlag(profile.FLAG_YES);
          db.insertRbsProfile(profile);
        }
        UserProfileLink link = new UserProfileLink();
        link.setProfileId(profileId);
        link.setLoginName(loginName);
        link.setEnableFlag(link.FLAG_YES);
        link = db.insertUserProfileLink(link);
        if (link == null)
        {
          addFeedback("Error, unable to link user");
        }
        else
        {
          addFeedback("User '" + link.getUserName() + "' linked to profile");
          bean.setData(bean.FN_LOGIN_NAME,""); // clear login name
          reloadProfileRef();
        }
      }
    }
    doView("rbsAdminProEdit");
  }
  
  /**
   * Toggle the enabled flag on an RBS profile.
   */
  public void doRbsAdminProToggle()
  {
    String profileId = getParm("profileId",null);
    boolean wasEnabled = db.profileEnabled(profileId);
    if (profileId == null)
    {
      redirectWithFeedback(getBackLink(),"Profile ID not found");
    }
    else if (isConfirmed())
    {
      db.toggleProfileEnable(profileId);
      redirectWithFeedback(getBackLink(),"Profile " 
        + (wasEnabled ? "disabled" : "enabled"));
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Profile not " + (wasEnabled ? "disabled" : "enabled"));
    }
    else
    {
      confirm((wasEnabled ? "Disable" : "Enabled") + " profile?");
    }
  }
    

  /**
   * Unlink a user from a profile in RBS.
   */
  public void doRbsAdminUserDelete()
  {
    long uplId = getParmLong("uplId");
    UserProfileLink link = db.getUserProfileLink(uplId);
    if (link == null)
    {
      redirectWithFeedback(getBackLink(),"User profile link not found");
    }
    else if (isConfirmed())
    {
      db.deleteUserProfileLink(uplId);
      redirectWithFeedback(getBackLink(),"User '" + link.getLoginName()
        + "' removed from profile");
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Delete operation canceled");
    }
    else
    {
      confirm("Remove user '" + link.getLoginName() + "' from profile?");
    }
  }
  
  /**
   * Permanently delete all billing records associated with a profile.
   */
  public void doRbsAdminBillRecPurge()
  {
    String profileId = getParm("profileId");
    String recType = HttpHelper.isProdServer(null) ? "production" : "test";
    if (isConfirmed())
    {
      db.purgeBillingRecords(profileId);
      redirectWithFeedback(getBackLink(),"All " + recType + " billing records "
        + " deleted for profile " + profileId);
    }
    else if (isCanceled())
    {
      redirectWithFeedback(getBackLink(),"Delete operation canceled");
    }
    else
    {
      confirm("Permanently delete all " + recType + " billing records "
        + " for profile " + profileId + "?");
    }
  }

  /**
   * Look for LINKED profile selection bean in the session, create one
   * if needed.
   */
  private void setLookupLinkedProfilesBean()
  {
    LookupLinkedProfilesBean bean = (LookupLinkedProfilesBean)
      getSessionAttr(SN_LOOKUP_LINKED_PROFILES_BEAN,null);
    if (bean == null)
    {
      bean = new LookupLinkedProfilesBean(this);
      setSessionAttr(SN_LOOKUP_LINKED_PROFILES_BEAN,bean);
    }
    else
    {
      bean.setAction(this);
    }
  }

  /**
   * Do billing record maintenance version of profile select.
   */
  public void doRbsMaintProSelect()
  {
    setLookupLinkedProfilesBean();
    doView("rbsMaintProSelect");
  }

  /**
   * Search and edit functions for maintaining billing records associated
   * with a specific profile.
   */
  public void doRbsMaintBillRecs()
  {
    ProfileRef ref = setProfileRef(getParm("profileId",null));
    String profileId = ref.getProfileId();
    List recs = db.getBillingRecords(profileId);
    setRequestAttr("recs",recs);
    doView("rbsMaintBillRecs");
  }
  
  /**
   * Returns a csv file containing billing records associated with the
   * given profile ID.
   */
  public void doRbsMaintBillRecsDl()
  {
    BillingRecordDownloader downloader = new BillingRecordDownloader();
    String profileId = getParm("profileId",null);
    downloader.setProfileId(profileId);
    downloader.setBillingRecords(db.getBillingRecords(profileId));
    doCsvDownload(downloader);
  }

  /**
   * Edit an existing billing record.
   */
  public void doRbsMaintBillRecEdit()
  {
    // set profile ref in request
    setProfileRef(null);
    
    EditBillingRecordBean bean = new EditBillingRecordBean(this);
    request.setAttribute("rec",bean.getBillingRecord());
    // stored card may or may not be present regardless of store id presence...
    if (bean.getBillingRecord().hasStoreId() && bean.getPmgStoredCard() != null)
    {
      request.setAttribute("psc",bean.getPmgStoredCard());
    }
    
    if (bean.isAutoSubmitOk())
    {
      addFeedback("Billing record updated");
    }
    doView("rbsMaintBillRecEdit");
  }
}
