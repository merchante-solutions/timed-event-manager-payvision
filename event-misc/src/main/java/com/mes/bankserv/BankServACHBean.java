/*@lineinfo:filename=BankServACHBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/bankserv/BankServACHBean.sqlj $

  Description:

    BankServACHBean

    Facilitates addition of ACH data to the outgoing BankServ ACH File

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 11/19/03 3:50p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.bankserv;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.NumberField;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class BankServACHBean extends FieldBean
{
  protected DefaultContext    myCtx     = null;
  
  public boolean              success   = false;
  
  
  public boolean connect()
  {
    boolean result = super.connect();
    
    myCtx = Ctx;
    return result;
  }
  
  public void setProperties(HttpServletRequest request, UserBean user)
  {
    try
    {
      connect();
      
      this.user = user;
      
      register(user.getLoginName(), request);
      
      addFields();
      
      // set merchant number for loading
      setData("merchantNumber", HttpHelper.getString(request, "merchant"));
      
      // load data from request
      setFields(request);
      
      // load supplemental data (transit routing, dda, bank number)
      loadData();
                 
      // if submitted, process the data
      if(getData("Submit").equals("Submit"))
      {
        // submit only if the data is valid
        if(isValid())
        {
          submitData(user.getLoginName());
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void loadData()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      // load bank number, transit routing, and dda from mif
      /*@lineinfo:generated-code*//*@lineinfo:113^7*/

//  ************************************************************
//  #sql [myCtx] it = { select  bank_number,
//                  dba_name,
//                  dda_num,
//                  transit_routng_num
//          from    mif
//          where   merchant_number = :getData("merchantNumber")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = myCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3590 = getData("merchantNumber");
  try {
   String theSqlTS = "select  bank_number,\n                dba_name,\n                dda_num,\n                transit_routng_num\n        from    mif\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.bankserv.BankServACHBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3590);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.bankserv.BankServACHBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:121^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        setData("bankNumber", rs.getString("bank_number"));
        setData("merchantName", rs.getString("dba_name"));
        setData("dda", rs.getString("dda_num"));
        setData("transitRouting", rs.getString("transit_routng_num"));
      }
    }
    catch(Exception e)
    {
      logEntry("loadData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { it.close(); } catch (Exception e) {}
    }
  }
  
  private void submitData(String loginName)
  {
    try
    {
      // get credit/debit indicator for this description
      String    creditDebitInd = "";
      
      /*@lineinfo:generated-code*//*@lineinfo:151^7*/

//  ************************************************************
//  #sql [myCtx] { select  transaction_type
//          
//          from    bankserv_ach_descriptions
//          where   description = :getData("description")
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = myCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3591 = getData("description");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  transaction_type\n         \n        from    bankserv_ach_descriptions\n        where   description =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.bankserv.BankServACHBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3591);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   creditDebitInd = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:157^7*/
      
      // insert record into bankserv_ach_detail
      /*@lineinfo:generated-code*//*@lineinfo:160^7*/

//  ************************************************************
//  #sql [myCtx] { insert into bankserv_ach_detail
//          (
//            merchant_number,
//            amount,
//            entry_description,
//            credit_debit_ind,
//            created_by,
//            source_type,
//            source_id
//          )
//          values
//          (
//            :getData("merchantNumber"),
//            :getData("amount"),
//            :getData("description"),
//            :creditDebitInd,
//            :loginName,
//            :mesConstants.BANKSERV_SOURCE_MANUAL,
//            -1
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = myCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3592 = getData("merchantNumber");
 String __sJT_3593 = getData("amount");
 String __sJT_3594 = getData("description");
   String theSqlTS = "insert into bankserv_ach_detail\n        (\n          merchant_number,\n          amount,\n          entry_description,\n          credit_debit_ind,\n          created_by,\n          source_type,\n          source_id\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n          -1\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.bankserv.BankServACHBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3592);
   __sJT_st.setString(2,__sJT_3593);
   __sJT_st.setString(3,__sJT_3594);
   __sJT_st.setString(4,creditDebitInd);
   __sJT_st.setString(5,loginName);
   __sJT_st.setInt(6,mesConstants.BANKSERV_SOURCE_MANUAL);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:182^7*/
    }
    catch(Exception e)
    {
      logEntry("submitData()", e.toString());
    }
  }
  
  private void addFields()
  {
    try
    {
      // set up form fields
      Field                       fMerchantNumber     = new NumberField("merchantNumber", 16, 16, false, 0);
      Field                       fMerchantName       = new Field("merchantName", 32, 32, false);
      Field                       fBankNumber         = new NumberField("bankNumber", 4, 4, false, 0);
      Field                       fDDA                = new Field("dda", 17, 17, false);
      Field                       fTransitRouting     = new Field("transitRouting", 9, 9, false);
      ACHDescriptionDropDownField fDescription        = new ACHDescriptionDropDownField("description");
      Field                       fAmount             = new CurrencyField("amount", 10, 10, false, 0, 100000);
      
      fields.add(fMerchantNumber);
      fields.add(fMerchantName);
      fields.add(fBankNumber);
      fields.add(fDDA);
      fields.add(fTransitRouting);
      fields.add(fDescription);
      fields.add(fAmount);
      
      fields.add(new ButtonField("Submit"));
      
      fields.setGroupHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      logEntry("addFields()", e.toString());
    }
  }
  
  public class ACHDescriptionDropDownField extends DropDownField
  {
    public ACHDescriptionDropDownField(String fname)
    {
      super(fname, null, false);
      dropDown = new ACHDescriptionTable();
    }
    
    public class ACHDescriptionTable extends DropDownTable
    {
      public ACHDescriptionTable()
      {
        getData();
      }
      
      private void getData()
      {
        ResultSetIterator   it      = null;
        ResultSet           rs      = null;
        
        try
        {
          addElement("", "-- Select --");
          
          /*@lineinfo:generated-code*//*@lineinfo:245^11*/

//  ************************************************************
//  #sql [myCtx] it = { select  description,
//                      long_description,
//                      decode(transaction_type, 'D', 'style="color:Red"', null)
//              from    bankserv_ach_descriptions
//              where   enabled = 'Y'
//              order by display_order asc
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = myCtx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  description,\n                    long_description,\n                    decode(transaction_type, 'D', 'style=\"color:Red\"', null)\n            from    bankserv_ach_descriptions\n            where   enabled = 'Y'\n            order by display_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.bankserv.BankServACHBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.bankserv.BankServACHBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:253^11*/
          
          rs = it.getResultSet();
          
          while(rs.next())
          {
            addElement(rs);
          }
          
          rs.close();
          it.close();
        }
        catch(Exception e)
        {
          logEntry("getData()", e.toString());
        }
        finally
        {
          try { rs.close(); } catch(Exception e) {}
          try { it.close(); } catch(Exception e) {}
        }
      }
    }
  }
}/*@lineinfo:generated-code*/