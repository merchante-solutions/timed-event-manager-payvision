/*@lineinfo:filename=BankServUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/bankserv/BankServUpload.sqlj $

  Description:

    BankServACHBean

    Facilitates addition of ACH data to the outgoing BankServ ACH File

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 8/13/04 1:53p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.bankserv;

import java.io.File;
import java.util.ArrayList;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.FieldBean;
import com.mes.forms.FileUploadField;
import com.mes.forms.RadioButtonField;
import com.mes.support.CSVUploadParser;
import com.mes.user.UserBean;

public class BankServUpload extends FieldBean
{
  public static final int   FILE_TYPE_COMMISSION    = 1;
  public static final int   FILE_TYPE_AGENT_BANK    = 2;
  
  private Vector             transactions  = new Vector();
  private ServletFileUpload  fileUpload    = null;
  public  String             fileContents  = "";
  public  boolean         success       = false;
  
  protected String[][] fileTypeList =
  {
    { "Commission Payments", "1" },
    { "Agent Bank Payments", "2" }
  };
  
  public void setProperties(HttpServletRequest request, UserBean user)
  {
    try
    {
      connect();
      this.user = user;
      
      register(user.getLoginName(), request);
      
      // establish FileUpload if submitting
      setFields(request);
      if(request.getContentType() != null)
      {
        fileUpload = new ServletFileUpload(new DiskFileItemFactory(1048576, new File("./")));
      
        // process request
        submitData(request);
        
        if(!hasErrors())
        {
          success = true;
        }
      }
    }
    catch(Exception e)
    {
      logEntry("setProperties()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void submitData(HttpServletRequest request)
  {
    try
    {
      connect();
      
      ArrayList items = (ArrayList)(fileUpload.parseRequest(request));
      
      for(int i=0; i<items.size(); ++i)
      {
        DiskFileItem fi = (DiskFileItem)items.get(i);
        
        if(fi.isFormField())
        {
          // set field data from field data
          fields.getField(fi.getFieldName()).setData(fi.getString());
        }
        else if(fi.getFieldName().equals("uploadFile"))
        {
          processFile(fi);
        }
      }
    }
    catch(Exception e)
    {
      addError(e.toString());
      logEntry("submitData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void processFile(FileItem fi)
  {
    try
    {
      fileContents = fi.getString();
      
      // create CSVUploadParser
      CSVUploadParser parser = new CSVUploadParser(fi);
      
      int curLine = 0;
      while(parser.nextLine())
      {
        transactions.add(new BankServItem(parser, ++curLine));
      }
      
      if(!hasErrors())
      {
        // submit items to database
        for(int i=0; i<transactions.size(); ++i)
        {
          ((BankServItem)(transactions.elementAt(i))).submit(fields.getField("fileType").asInteger(), user.getLoginName());
        }
      }
    }
    catch(Exception e)
    {
      System.out.println("processFile: " + e.toString());
      logEntry("processFile()", e.toString());
    }
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // set up form fields
      fields.add(new FileUploadField("uploadFile", 100, 100, false));
      fields.add(new RadioButtonField("fileType", fileTypeList, -1, false, "Please specify a file type"));
  
      fields.add(new ButtonField("Submit"));
  
      fields.setGroupHtmlExtra("class=\"formFields\"");
    }
    catch(Exception e)
    {
      addError("Error Creating Fields: " + e.toString());
      logEntry("createFields()", e.toString());
    }
  }
  
  public class BankServItem
  {
    long    merchantNumber;
    double  amount;
    
    public BankServItem(CSVUploadParser p, int line)
    {
      try
      {
        merchantNumber  = p.nextTokenAsLong();
        amount          = p.nextTokenAsDouble();
      }
      catch(Exception e)
      {
        addError("BankServItem Constructor (line " + line + "): " + e.toString());
        logEntry("BankServItem(): ", e.toString());
      }
    }
    
    public void submit(int fileType, String userName)
    {
      String  entryDescription  = "";
      String  creditDebitInd    = "";
      
      try
      {
        // set up entry description
        switch(fileType)
        {
          case FILE_TYPE_COMMISSION:
            if(amount < 0)
            {
              entryDescription  = "COMM-DEBIT";
              creditDebitInd    = "D";
            }
            else
            {
              entryDescription  = "COMM-CREDT";
              creditDebitInd    = "C";
            }
            break;
            
          case FILE_TYPE_AGENT_BANK:
            if(amount < 0)
            {
              entryDescription  = "TRS-DEBIT";
              creditDebitInd    = "D";
            }
            else
            {
              entryDescription  = "TRS-CREDIT";
              creditDebitInd    = "C";
            }
            break;
        }
        
        // insert record
        /*@lineinfo:generated-code*//*@lineinfo:245^9*/

//  ************************************************************
//  #sql [Ctx] { insert into bankserv_ach_detail
//            (
//              merchant_number,
//              amount,
//              entry_description,
//              credit_debit_ind,
//              created_by,
//              source_type,
//              source_id
//            )
//            values
//            (
//              :merchantNumber,
//              abs(:amount),
//              :entryDescription,
//              :creditDebitInd,
//              :userName,
//              :mesConstants.BANKSERV_SOURCE_UPLOAD,
//              -1
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into bankserv_ach_detail\n          (\n            merchant_number,\n            amount,\n            entry_description,\n            credit_debit_ind,\n            created_by,\n            source_type,\n            source_id\n          )\n          values\n          (\n             :1 ,\n            abs( :2 ),\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n            -1\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.bankserv.BankServUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDouble(2,amount);
   __sJT_st.setString(3,entryDescription);
   __sJT_st.setString(4,creditDebitInd);
   __sJT_st.setString(5,userName);
   __sJT_st.setInt(6,mesConstants.BANKSERV_SOURCE_UPLOAD);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:267^9*/
      }
      catch(Exception e)
      {
        logEntry("submit()", e.toString());
      }
    }
  }
}/*@lineinfo:generated-code*/