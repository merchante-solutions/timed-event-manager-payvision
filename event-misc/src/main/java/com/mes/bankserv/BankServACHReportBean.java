/*@lineinfo:filename=BankServACHReportBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/bankserv/BankServACHReportBean.sqlj $

  Description:

    BankServACHBean

    Facilitates addition of ACH data to the outgoing BankServ ACH File

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2010-08-09 14:57:56 -0700 (Mon, 09 Aug 2010) $
  Version            : $Revision: 17687 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.bankserv;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.ComboDateField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.NumberFormatter;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class BankServACHReportBean extends FieldBean
{
  public static final int       REPORT_TYPE_AUTHORIZE         = 1;
  public static final int       REPORT_TYPE_PENDING_TRANSMIT  = 2;
  public static final int       REPORT_TYPE_TRANSMISSIONS     = 3;
  
  private int       reportType;
  private String    reportTitle;
  
  private Vector    debitReportRows     = new Vector();
  private Vector    creditReportRows    = new Vector();
  private Vector    downloadReportRows  = new Vector();
  
  public void setProperties(HttpServletRequest request, Object user)
  {
    try
    {
      connect();
      
      this.user = (UserBean)user;
      
      register(this.user.getLoginName(), request);
      
      // establish report type which will determine what fields to add (if any)
      reportType = HttpHelper.getInt(request, "type");
      
      setReportType(request);
      
      // Un-authorize item if requested
      if(HttpHelper.getInt(request, "unauthItem", -1) != 1)
      {
        unauthorizeItem(HttpHelper.getInt(request, "unauthItem"));
      }
      
      addFields();
      
      fields.setGroupHtmlExtra("class=\"formFields\"");
      
      // load data from request
      setFields(request);
      
      // load transmission data if this is a transmission report
      if(reportType == REPORT_TYPE_TRANSMISSIONS)
      {
        loadTransmissionData();
      }
      
      // if submitted, post data to database
      if(getData("Submit").equals("Authorize Marked Items"))
      {
        // modify authorization data
        submitAuthorizeData(this.user.getLoginName());
      }
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("setProperties(" + this.user.getLoginName()+ ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void unauthorizeItem(int achSeqNum)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:118^7*/

//  ************************************************************
//  #sql [Ctx] { update  bankserv_ach_detail
//          set     date_authorized = null,
//                  authorized_by = null
//          where   ach_seq_num = :achSeqNum  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  bankserv_ach_detail\n        set     date_authorized = null,\n                authorized_by = null\n        where   ach_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.bankserv.BankServACHReportBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,achSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:124^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("unAuthorizeItem(" + achSeqNum + ")", e.toString());
    }
  }
  
  private void addAuthorizeFields()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      debitReportRows.clear();
      creditReportRows.clear();
      fields.removeAllFields();
      
      // determine how many items are available for authorization
      /*@lineinfo:generated-code*//*@lineinfo:146^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bad.ach_seq_num       ach_seq_num,
//                  bad.date_created      date_created,
//                  bad.created_by        created_by,
//                  bad.merchant_number   merchant_number,
//                  m.dba_name            dba_name,
//                  dsc.long_description  reason,
//                  bad.amount *
//                    (decode(bad.credit_debit_ind, 'C', -1, 1))  amount,
//                  to_char(bad.target_routing_number, '000000000') transit_routing,
//                  bad.target_account_number dda
//          from    bankserv_ach_detail       bad,
//                  bankserv_ach_descriptions dsc,
//                  mif                       m
//          where   bad.process_sequence is null and
//                  bad.date_authorized is null and
//                  bad.date_declined is null and
//                  bad.merchant_number = m.merchant_number and
//                  bad.entry_description = dsc.description
//          order by bad.date_created desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bad.ach_seq_num       ach_seq_num,\n                bad.date_created      date_created,\n                bad.created_by        created_by,\n                bad.merchant_number   merchant_number,\n                m.dba_name            dba_name,\n                dsc.long_description  reason,\n                bad.amount *\n                  (decode(bad.credit_debit_ind, 'C', -1, 1))  amount,\n                to_char(bad.target_routing_number, '000000000') transit_routing,\n                bad.target_account_number dda\n        from    bankserv_ach_detail       bad,\n                bankserv_ach_descriptions dsc,\n                mif                       m\n        where   bad.process_sequence is null and\n                bad.date_authorized is null and\n                bad.date_declined is null and\n                bad.merchant_number = m.merchant_number and\n                bad.entry_description = dsc.description\n        order by bad.date_created desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.bankserv.BankServACHReportBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.bankserv.BankServACHReportBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:167^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        // add to reporting vector
        if(rs.getDouble("amount") < 0.0)
        {
          creditReportRows.add(new ACHAuthItem(rs));
        }
        else
        {
          debitReportRows.add(new ACHAuthItem(rs));
        }
        
        // add field for marking as authorized
        fields.add(new CheckboxField("auth" + rs.getString("ach_seq_num"), false));
        fields.add(new CheckboxField("decl" + rs.getString("ach_seq_num"), false));
      }
      
      fields.add(new ButtonField("Submit", "Authorize Marked Items"));
    }
    catch(Exception e)
    {
      logEntry("addAuthorizeFields()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void loadTransmissionData()
  {
    ResultSetIterator  it     = null;
    ResultSet         rs      = null;
    
    try
    {
      creditReportRows.clear();
      debitReportRows.clear();
      downloadReportRows.clear();
      
      ComboDateField fromDate = (ComboDateField)fields.getField("fromDate");
      ComboDateField toDate   = (ComboDateField)fields.getField("toDate");
      
      // get data with current date setup
      /*@lineinfo:generated-code*//*@lineinfo:216^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bad.ach_seq_num       ach_seq_num,
//                  bad.date_created      date_created,
//                  bad.date_transmitted  date_transmitted,
//                  bad.created_by        created_by,
//                  bad.merchant_number   merchant_number,
//                  m.dba_name            dba_name,
//                  dsc.long_description  reason,
//                  bad.amount *
//                    (decode(bad.credit_debit_ind, 'C', -1, 1))  amount,
//                  bad.date_authorized   date_authorized,
//                  bad.authorized_by     authorized_by,
//                  to_char(bad.target_routing_number, '000000000') transit_routing,
//                  bad.target_account_number dda,
//                  th.ancestor               portfolio_node,
//                  gn.group_name             portfolio_name
//          from    bankserv_ach_detail       bad,
//                  bankserv_ach_descriptions dsc,
//                  mif                       m,
//                  t_hierarchy               th,
//                  group_names               gn
//          where   trunc(bad.date_transmitted) between
//                    :fromDate.getSqlDate() and
//                    :toDate.getSqlDate() and
//                  bad.merchant_number = m.merchant_number and
//                  bad.entry_description = dsc.description and
//                  m.association_node = th.descendent(+) and
//                  th.relation(+) = 4 and
//                  th.ancestor = gn.group_number(+)
//          order by  bad.credit_debit_ind desc, 
//                    bad.date_transmitted asc, 
//                    bad.date_created asc, 
//                    bad.merchant_number asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_3595 = fromDate.getSqlDate();
 java.sql.Date __sJT_3596 = toDate.getSqlDate();
  try {
   String theSqlTS = "select  bad.ach_seq_num       ach_seq_num,\n                bad.date_created      date_created,\n                bad.date_transmitted  date_transmitted,\n                bad.created_by        created_by,\n                bad.merchant_number   merchant_number,\n                m.dba_name            dba_name,\n                dsc.long_description  reason,\n                bad.amount *\n                  (decode(bad.credit_debit_ind, 'C', -1, 1))  amount,\n                bad.date_authorized   date_authorized,\n                bad.authorized_by     authorized_by,\n                to_char(bad.target_routing_number, '000000000') transit_routing,\n                bad.target_account_number dda,\n                th.ancestor               portfolio_node,\n                gn.group_name             portfolio_name\n        from    bankserv_ach_detail       bad,\n                bankserv_ach_descriptions dsc,\n                mif                       m,\n                t_hierarchy               th,\n                group_names               gn\n        where   trunc(bad.date_transmitted) between\n                   :1  and\n                   :2  and\n                bad.merchant_number = m.merchant_number and\n                bad.entry_description = dsc.description and\n                m.association_node = th.descendent(+) and\n                th.relation(+) = 4 and\n                th.ancestor = gn.group_number(+)\n        order by  bad.credit_debit_ind desc, \n                  bad.date_transmitted asc, \n                  bad.date_created asc, \n                  bad.merchant_number asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.bankserv.BankServACHReportBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_3595);
   __sJT_st.setDate(2,__sJT_3596);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.bankserv.BankServACHReportBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:250^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        if(rs.getDouble("amount") < 0.0)
        {
          creditReportRows.add(new ACHTransmissionItem(rs));
        }
        else
        {
          debitReportRows.add(new ACHTransmissionItem(rs));
        }
        
        downloadReportRows.add(new ACHTransmissionItem(rs));
        ++downloadRowCount;
      }
      
    }
    catch(Exception e)
    {
      logEntry("loadTransmissionData()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void addTransmissionFields()
  {
    try
    {
      creditReportRows.clear();
      debitReportRows.clear();
      fields.removeAllFields();
      
      // add date fields
      ComboDateField fromDate = new ComboDateField("fromDate", "From: ", true, false);
      ComboDateField toDate   = new ComboDateField("toDate", "To: ", true, false);
      
      //fromDate.addDays(-1);
      //toDate.addDays(-1);
      
      fields.add(fromDate);
      fields.add(toDate);
      fields.add(new ButtonField("Submit", "Change Date"));
    }
    catch(Exception e)
    {
      logEntry("addTransmissionFields()", e.toString());
    }
  }
  
  private void addPendingFields()
  {
    ResultSetIterator     it      = null;
    ResultSet             rs      = null;
    
    try
    {
      creditReportRows.clear();
      debitReportRows.clear();
      fields.removeAllFields();
      
      // retrieve authorized items
      /*@lineinfo:generated-code*//*@lineinfo:318^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bad.ach_seq_num       ach_seq_num,
//                  bad.date_created      date_created,
//                  bad.created_by        created_by,
//                  bad.merchant_number   merchant_number,
//                  m.dba_name            dba_name,
//                  dsc.long_description  reason,
//                  bad.amount *
//                    (decode(bad.credit_debit_ind, 'C', -1, 1))  amount,
//                  bad.date_authorized   date_authorized,
//                  bad.authorized_by     authorized_by,
//                  to_char(bad.target_routing_number, '000000000') transit_routing,
//                  bad.target_account_number dda
//          from    bankserv_ach_detail       bad,
//                  bankserv_ach_descriptions dsc,
//                  mif                       m
//          where   bad.process_sequence is null and
//                  bad.date_authorized is not null and
//                  bad.merchant_number = m.merchant_number and
//                  bad.entry_description = dsc.description
//          order by  bad.credit_debit_ind desc, 
//                    bad.date_created desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bad.ach_seq_num       ach_seq_num,\n                bad.date_created      date_created,\n                bad.created_by        created_by,\n                bad.merchant_number   merchant_number,\n                m.dba_name            dba_name,\n                dsc.long_description  reason,\n                bad.amount *\n                  (decode(bad.credit_debit_ind, 'C', -1, 1))  amount,\n                bad.date_authorized   date_authorized,\n                bad.authorized_by     authorized_by,\n                to_char(bad.target_routing_number, '000000000') transit_routing,\n                bad.target_account_number dda\n        from    bankserv_ach_detail       bad,\n                bankserv_ach_descriptions dsc,\n                mif                       m\n        where   bad.process_sequence is null and\n                bad.date_authorized is not null and\n                bad.merchant_number = m.merchant_number and\n                bad.entry_description = dsc.description\n        order by  bad.credit_debit_ind desc, \n                  bad.date_created desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.bankserv.BankServACHReportBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.bankserv.BankServACHReportBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:341^7*/
      
      rs = it.getResultSet();
      
      while(rs.next())
      {
        if(rs.getDouble("amount") < 0.0)
        {
          creditReportRows.add(new ACHPendingItem(rs));
        }
        else
        {
          debitReportRows.add(new ACHPendingItem(rs));
        }
      }
    }
    catch(Exception e)
    {
      logEntry("addPendingFields()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
    }
  }
  
  private void submitAuthorizeData(String loginName)
  {
    try
    {
      // for each checkbox checked, update database with login name and date/time
      Iterator it = fields.iterator();
      
      while(it.hasNext())
      {
        Field f = (Field)it.next();
        
        if(f.getClass().getName().equals("com.mes.forms.CheckboxField"))
        {
          int achSeqNum = Integer.parseInt(f.getName().substring(4, f.getName().length()));
          
          if (f.getName().substring(0, 4).equals("auth") && f.getData().equals("y"))
          {
            // mark authorized item
            /*@lineinfo:generated-code*//*@lineinfo:386^13*/

//  ************************************************************
//  #sql [Ctx] { update  bankserv_ach_detail
//                set     date_authorized = sysdate,
//                        authorized_by = :loginName
//                where   ach_seq_num = :achSeqNum
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  bankserv_ach_detail\n              set     date_authorized = sysdate,\n                      authorized_by =  :1 \n              where   ach_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.bankserv.BankServACHReportBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setInt(2,achSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:392^13*/
          }
          
          // mark declined items
          if(f.getName().substring(0, 4).equals("decl") && f.getData().equals("y"))
          {
            // mark declined items
            /*@lineinfo:generated-code*//*@lineinfo:399^13*/

//  ************************************************************
//  #sql [Ctx] { update  bankserv_ach_detail
//                set     date_declined = sysdate,
//                        declined_by = :loginName
//                where   ach_seq_num = :achSeqNum
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  bankserv_ach_detail\n              set     date_declined = sysdate,\n                      declined_by =  :1 \n              where   ach_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.bankserv.BankServACHReportBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setInt(2,achSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:405^13*/
          }
        }
      }
      
      // re-add all fields so that worked items are removed
      addFields();
    }
    catch(Exception e)
    {
      logEntry("submitAuthorizeData(" + loginName + ")", e.toString());
    }
  }
  
  private void addFields()
  {
    switch(reportType)
    {
      case REPORT_TYPE_AUTHORIZE:
        addAuthorizeFields();
        break;
        
      case REPORT_TYPE_PENDING_TRANSMIT:
        addPendingFields();
        break;
        
      case REPORT_TYPE_TRANSMISSIONS:
        addTransmissionFields();
        break;
        
      default:
        // no fields needed
        break;
    }
  }
  
  public int getReportType()
  {
    return reportType;
  }
  
  public String getReportTitle()
  {
    return reportTitle;
  }
  
  public Vector getCreditReportRows()
  {
    return creditReportRows;
  }
  
  public Vector getDebitReportRows()
  {
    return debitReportRows;
  }
  
  public String getFromDate()
  {
    return ((ComboDateField)(fields.getField("fromDate"))).getHtmlDate();
  }
  
  public String getToDate()
  {
    return ((ComboDateField)(fields.getField("toDate"))).getHtmlDate();
  }
  
  private void setReportType(HttpServletRequest request)
  {
    try
    {
      reportType  = HttpHelper.getInt(request, "type");
      
      switch(reportType)
      {
        case REPORT_TYPE_AUTHORIZE:
          reportTitle = "Authorize Transmission of Outgoing ACH Transactions";
          break;
        case REPORT_TYPE_PENDING_TRANSMIT:
          reportTitle = "ACH Items Pending Transmission";
          break;
        case REPORT_TYPE_TRANSMISSIONS:
          reportTitle = "Historical ACH Transmissions";
          break;
        default:
          reportTitle = "Unknown";
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("setReportType()", e.toString());
    }
  }
  
  protected String getDownloadFilenameBase()
  {
    String filenameBase = "";
    switch(reportType)
    {
      case REPORT_TYPE_AUTHORIZE:
        filenameBase = "ach_auth";
        break;
        
      case REPORT_TYPE_PENDING_TRANSMIT:
        filenameBase = "ach_pending";
        break;
        
      case REPORT_TYPE_TRANSMISSIONS:
        filenameBase = "ach_transmit";
        break;
        
      default:
        filenameBase = "unknown";
        break;
    }
    
    return filenameBase;
  }
  
  protected void setDownloadHeader(int encoding, StringBuffer line)
  {
    try
    {
      switch(encoding)
      {
        case mesConstants.FF_CSV:
          switch(reportType)
          {
            case REPORT_TYPE_TRANSMISSIONS:
              line.append("\"Date Transmitted\",");
              line.append("\"Date Created\",");
              line.append("\"Created By\",");
              line.append("\"Date Authorized\",");
              line.append("\"Authorized By\",");
              line.append("\"Portfolio Node\",");
              line.append("\"Portfolio Name\",");
              line.append("\"Merchant Number\",");
              line.append("\"Transit Routing\",");
              line.append("\"DDA\",");
              line.append("\"Merchant Name\",");
              line.append("\"Description\",");
              line.append("\"Amount\"");
              break;

            case REPORT_TYPE_AUTHORIZE:
              break;
              
            case REPORT_TYPE_PENDING_TRANSMIT:
              break;
          }
          break;
          
        default:
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("setDownloadHeader(" + encoding + ")", e.toString());
    }
  }
  
  protected void setDownloadRow(int i, int encoding, StringBuffer line)
  {
    try
    {
      switch(encoding)
      {
        case mesConstants.FF_CSV:
          switch(reportType)
          {
            case REPORT_TYPE_TRANSMISSIONS:
              ACHTransmissionItem item = (ACHTransmissionItem)(downloadReportRows.elementAt(i));
              line.append("\"");
              line.append(item.dateTransmitted);
              line.append("\",");
              line.append("\"");
              line.append(item.dateCreated);
              line.append("\",");
              line.append("\"");
              line.append(item.createdBy);
              line.append("\",");
              line.append("\"");
              line.append(item.dateAuthorized);
              line.append("\",");
              line.append("\"");
              line.append(item.authorizedBy);
              line.append("\",");
              line.append("\"");
              line.append(item.portfolioNode);
              line.append("\",");
              line.append("\"");
              line.append(item.portfolioName);
              line.append("\",");
              line.append("\"");
              line.append(item.merchantNumber);
              line.append("\",");
              line.append("\"");
              line.append(item.transitRouting);
              line.append("\",");
              line.append("\"");
              line.append(item.dda);
              line.append("\",");
              line.append("\"");
              line.append(item.dbaName);
              line.append("\",");
              line.append("\"");
              line.append(item.reasonCode);
              line.append("\",");
              line.append(item.amountNum);
              break;

            case REPORT_TYPE_AUTHORIZE:
              break;
              
            case REPORT_TYPE_PENDING_TRANSMIT:
              break;
          }
          break;
          
        default:
          break;
      }
    }
    catch(Exception e)
    {
      logEntry("setDownloadRow(" + i + ", " + encoding + ")", e.toString());
    }
  }
  
  
  public class ACHAuthItem
  {
    public String seqNum          = "";
    public String dateCreated     = "";
    public String createdBy       = "";
    public String merchantNumber  = "";
    public String dbaName         = "";
    public String reasonCode      = "";
    public String amount          = "";
    public String checkBoxName    = "";
    public String declineName     = "";
    public double amountNum       = 0.0;
    public String transitRouting  = "";
    public String dda             = "";
    
    public ACHAuthItem(ResultSet rs)
    {
      try
      {
        seqNum = rs.getString("ach_seq_num");
        dateCreated = DateTimeFormatter.getFormattedDate(rs.getDate("date_created"), "MM/dd/yyyy");
        createdBy = rs.getString("created_by");
        merchantNumber = rs.getString("merchant_number");
        dbaName = rs.getString("dba_name");
        reasonCode = rs.getString("reason");
        amountNum = rs.getDouble("amount");
        amount = NumberFormatter.getDoubleString(amountNum, NumberFormatter.CURRENCY_FORMAT);
        checkBoxName = "auth" + seqNum;
        declineName = "decl" + seqNum;
        transitRouting = rs.getString("transit_routing");
        dda = rs.getString("dda");
      }
      catch(Exception e)
      {
        logEntry("constructor", e.toString());
      }
    }
  }
  
  public class ACHPendingItem
  {
    public String seqNum          = "";
    public String dateCreated     = "";
    public String createdBy       = "";
    public String merchantNumber  = "";
    public String dbaName         = "";
    public String reasonCode      = "";
    public String amount          = "";
    public double amountNum       = 0.0;
    public String dateAuthorized  = "";
    public String authorizedBy    = "";
    public String transitRouting  = "";
    public String dda             = "";
    
    public ACHPendingItem(ResultSet rs)
    {
      try
      {
        seqNum = rs.getString("ach_seq_num");
        dateCreated = DateTimeFormatter.getFormattedDate(rs.getDate("date_created"), "MM/dd/yyyy");
        createdBy = rs.getString("created_by");
        merchantNumber = rs.getString("merchant_number");
        dbaName = rs.getString("dba_name");
        reasonCode = rs.getString("reason");
        amountNum = rs.getDouble("amount");
        amount = NumberFormatter.getDoubleString(amountNum, NumberFormatter.CURRENCY_FORMAT);
        dateAuthorized = DateTimeFormatter.getFormattedDate(rs.getDate("date_authorized"), "MM/dd/yy hh:mm");
        authorizedBy = rs.getString("authorized_by");
        transitRouting = rs.getString("transit_routing");
        dda = rs.getString("dda");
      }
      catch(Exception e)
      {
        logEntry("constructor", e.toString());
      }
    }
  }
  
  public class ACHTransmissionItem
  {
    public String seqNum          = "";
    public String dateCreated     = "";
    public String dateTransmitted = "";
    public String createdBy       = "";
    public String merchantNumber  = "";
    public String dbaName         = "";
    public String reasonCode      = "";
    public String amount          = "";
    public double amountNum       = 0.0;
    public String dateAuthorized  = "";
    public String authorizedBy    = "";
    public String transitRouting  = "";
    public String dda             = "";
    public String portfolioNode   = "";
    public String portfolioName   = "";
    
    public ACHTransmissionItem(ResultSet rs)
    {
      try
      {
        seqNum = rs.getString("ach_seq_num");
        dateCreated = DateTimeFormatter.getFormattedDate(rs.getDate("date_created"), "MM/dd/yyyy");
        dateTransmitted = DateTimeFormatter.getFormattedDate(rs.getDate("date_transmitted"), "MM/dd/yyyy");
        createdBy = rs.getString("created_by");
        merchantNumber = rs.getString("merchant_number");
        dbaName = rs.getString("dba_name");
        reasonCode = rs.getString("reason");
        amountNum = rs.getDouble("amount");
        amount = NumberFormatter.getDoubleString(amountNum, NumberFormatter.CURRENCY_FORMAT);
        dateAuthorized = DateTimeFormatter.getFormattedDate(rs.getTimestamp("date_authorized"), "MM/dd/yy hh:mm");
        authorizedBy = rs.getString("authorized_by");
        transitRouting = rs.getString("transit_routing");
        dda = rs.getString("dda");
        
        portfolioNode = rs.getString("portfolio_node");
        portfolioName = rs.getString("portfolio_name");
      }
      catch(Exception e)
      {
        logEntry("constructor", e.toString());
      }
    }
  }
}/*@lineinfo:generated-code*/