/*@lineinfo:filename=RefreshComdataHierarchy*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/RefreshCertegyCheckHierarchy.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-07-20 10:07:58 -0700 (Mon, 20 Jul 2009) $
  Version            : $Revision: 16324 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class RefreshComdataHierarchy extends EventBase
{
  private Vector  merchants = null;
  
  private String  corpCode  = null;
  private String  chainCode = null;
  
  public final static int COMDATA_BANK_NUMBER       = 1010;
  public final static int DEFAULT_CORP_GROUP        = 700000;
  public final static int DEFAULT_CORP_CHAIN_GROUP  = 700001;
  
  private int getCorporateCodeGroup()
  {
    int corpCodeGroup = 0;
    
    try
    {
      // if both corp and chain are null, use default
      if(corpCode.equals("NULL") && chainCode.equals("NULL"))
      {
        corpCodeGroup = DEFAULT_CORP_GROUP;
      }
      else
      // if this is a null corporate with a non-null chain then use default 
      if(corpCode.equals("NULL") && !chainCode.equals("NULL"))
      {
        corpCodeGroup = DEFAULT_CORP_CHAIN_GROUP;
      }
      else
      {
        // see if this chain code already has a group
        int corpCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:67^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(corporate_code)
//            
//            from    mif_comdata_hierarchy
//            where   corporate_code = :corpCode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(corporate_code)\n           \n          from    mif_comdata_hierarchy\n          where   corporate_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,corpCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   corpCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:73^9*/
        
        if(corpCount > 0)
        {
          // get group from hierarchy mapper
          /*@lineinfo:generated-code*//*@lineinfo:78^11*/

//  ************************************************************
//  #sql [Ctx] { select  distinct group_1
//              
//              from    mif_comdata_hierarchy
//              where   corporate_code = :corpCode
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct group_1\n             \n            from    mif_comdata_hierarchy\n            where   corporate_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,corpCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   corpCodeGroup = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:84^11*/
        }
        else
        {
          // get from sequence
          /*@lineinfo:generated-code*//*@lineinfo:89^11*/

//  ************************************************************
//  #sql [Ctx] { select  comdata_corp_groups.nextval
//              
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  comdata_corp_groups.nextval\n             \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   corpCodeGroup = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:94^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("getCorporateCodeGroup(" + corpCode + ", " + chainCode + ")", e.toString());
    }
    
    return corpCodeGroup;
  }
  
  private int getChainCodeGroup()
  {
    int chainCodeGroup = 0;
    
    try
    {
      // just get from sequence
      /*@lineinfo:generated-code*//*@lineinfo:113^7*/

//  ************************************************************
//  #sql [Ctx] { select  comdata_chain_groups.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  comdata_chain_groups.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   chainCodeGroup = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:118^7*/
    }
    catch(Exception e)
    {
      logEntry("getChainCodeGroup(" + corpCode + ", " + chainCode + ")", e.toString());
    }
    
    return chainCodeGroup;
  }
  
  public boolean execute()
  {
    boolean             result  = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      // see if there are items to fix
      int procCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:141^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//          
//          from    mif_comdata_process
//          where   process_sequence is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n         \n        from    mif_comdata_process\n        where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:147^7*/
      
      if(procCount > 0)
      {
        long procSequence = 0L;
        /*@lineinfo:generated-code*//*@lineinfo:152^9*/

//  ************************************************************
//  #sql [Ctx] { select  hierarchy_refresh_sequence.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  hierarchy_refresh_sequence.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procSequence = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:157^9*/
        
        // mark items to process
        /*@lineinfo:generated-code*//*@lineinfo:160^9*/

//  ************************************************************
//  #sql [Ctx] { update  mif_comdata_process
//            set     process_sequence = :procSequence
//            where   process_sequence is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif_comdata_process\n          set     process_sequence =  :1 \n          where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:165^9*/
        
        // build vector of items
        /*@lineinfo:generated-code*//*@lineinfo:168^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchant_number
//            from    mif_comdata_process
//            where   process_sequence = :procSequence
//            order by merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merchant_number\n          from    mif_comdata_process\n          where   process_sequence =  :1 \n          order by merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,procSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.RefreshComdataHierarchy",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:174^9*/
        
        rs = it.getResultSet();
        merchants = new Vector();
        while(rs.next())
        {
          merchants.add(rs.getString("merchant_number"));
        }
        
        rs.close();
        it.close();
        
        for(int i=0; i < merchants.size(); ++i)
        {
          long merchantNumber = Long.parseLong((String)(merchants.elementAt(i)));
          
          // mark this item as processing
          /*@lineinfo:generated-code*//*@lineinfo:191^11*/

//  ************************************************************
//  #sql [Ctx] { update  mif_comdata_process
//              set     date_started = sysdate
//              where   merchant_number = :merchantNumber and
//                      process_sequence = :procSequence
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif_comdata_process\n            set     date_started = sysdate\n            where   merchant_number =  :1  and\n                    process_sequence =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setLong(2,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:197^11*/
          commit();
          
          // process
          updateHierarchy(merchantNumber);
          
          // mark record as completed
          /*@lineinfo:generated-code*//*@lineinfo:204^11*/

//  ************************************************************
//  #sql [Ctx] { update  mif_comdata_process
//              set     date_completed = sysdate
//              where   merchant_number = :merchantNumber and
//                      process_sequence = :procSequence
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif_comdata_process\n            set     date_completed = sysdate\n            where   merchant_number =  :1  and\n                    process_sequence =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setLong(2,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:210^11*/
          commit();
        }
      }
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }
  
  private void updateHierarchy(long merchantNumber)
  {
    int association;
    int group1;
    int rowCount;
    
    try
    {
      // get hierarchy details
      /*@lineinfo:generated-code*//*@lineinfo:240^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(corporate_code, 'NULL'),
//                  nvl(chain_code, 'NULL')
//          
//          from    mif_comdata
//          where   merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(corporate_code, 'NULL'),\n                nvl(chain_code, 'NULL')\n         \n        from    mif_comdata\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   corpCode = (String)__sJT_rs.getString(1);
   chainCode = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:248^7*/
      
      // see if hierarchy exists already
      /*@lineinfo:generated-code*//*@lineinfo:251^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(corporate_code)
//          
//          from    mif_comdata_hierarchy
//          where   corporate_code = :corpCode and
//                  chain_code = :chainCode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(corporate_code)\n         \n        from    mif_comdata_hierarchy\n        where   corporate_code =  :1  and\n                chain_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,corpCode);
   __sJT_st.setString(2,chainCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:258^7*/
      
      if(rowCount == 0)
      {
        // need to create a new hierarchy
        group1      = getCorporateCodeGroup();
        association = getChainCodeGroup();
        
        // insert into mif_comdata_hierarchy
        /*@lineinfo:generated-code*//*@lineinfo:267^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mif_comdata_hierarchy
//            (
//              corporate_code,
//              chain_code,
//              group_1,
//              association
//            )
//            values
//            (
//              :corpCode,
//              :chainCode,
//              :group1,
//              :association
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mif_comdata_hierarchy\n          (\n            corporate_code,\n            chain_code,\n            group_1,\n            association\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,corpCode);
   __sJT_st.setString(2,chainCode);
   __sJT_st.setInt(3,group1);
   __sJT_st.setInt(4,association);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:283^9*/
      }
      else
      {
        // get group numbers for this branch
        /*@lineinfo:generated-code*//*@lineinfo:288^9*/

//  ************************************************************
//  #sql [Ctx] { select  group_1,
//                    association
//            
//            from    mif_comdata_hierarchy
//            where   corporate_code = :corpCode and
//                    chain_code = :chainCode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  group_1,\n                  association\n           \n          from    mif_comdata_hierarchy\n          where   corporate_code =  :1  and\n                  chain_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,corpCode);
   __sJT_st.setString(2,chainCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   group1 = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   association = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:297^9*/
      }
      
      // insert or update mif data
      int mifCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:303^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//          
//          from    mif
//          where   merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n         \n        from    mif\n        where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mifCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:309^7*/
      
      // get merchant data
      String  orgName;
      String  address;
      String  city;
      String  state;
      String  zip;
      String  loadFilename;
      
      /*@lineinfo:generated-code*//*@lineinfo:319^7*/

//  ************************************************************
//  #sql [Ctx] { select  mc.location_name,
//                  mc.address,
//                  substr(mc.city, 1, 24),
//                  substr(mc.state, 1, 2),
//                  substr(mc.zip, 1, 9),
//                  mc.load_filename
//          
//          from    mif_comdata mc
//          where   mc.merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mc.location_name,\n                mc.address,\n                substr(mc.city, 1, 24),\n                substr(mc.state, 1, 2),\n                substr(mc.zip, 1, 9),\n                mc.load_filename\n         \n        from    mif_comdata mc\n        where   mc.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 6) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(6,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   orgName = (String)__sJT_rs.getString(1);
   address = (String)__sJT_rs.getString(2);
   city = (String)__sJT_rs.getString(3);
   state = (String)__sJT_rs.getString(4);
   zip = (String)__sJT_rs.getString(5);
   loadFilename = (String)__sJT_rs.getString(6);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:335^7*/
      
      if(mifCount > 0)
      {
        // update
        /*@lineinfo:generated-code*//*@lineinfo:340^9*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//            set     dba_name = :orgName,
//                    name1_line_1 = :orgName,
//                    addr1_line_1 = :address,
//                    city1_line_4 = :city,
//                    state1_line_4 = :state,
//                    zip1_line_4 = :zip,
//                    load_filename = :loadFilename,
//                    group_1_association = :group1,
//                    dmagent = :association
//            where   merchant_number = :merchantNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif\n          set     dba_name =  :1 ,\n                  name1_line_1 =  :2 ,\n                  addr1_line_1 =  :3 ,\n                  city1_line_4 =  :4 ,\n                  state1_line_4 =  :5 ,\n                  zip1_line_4 =  :6 ,\n                  load_filename =  :7 ,\n                  group_1_association =  :8 ,\n                  dmagent =  :9 \n          where   merchant_number =  :10";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,orgName);
   __sJT_st.setString(2,orgName);
   __sJT_st.setString(3,address);
   __sJT_st.setString(4,city);
   __sJT_st.setString(5,state);
   __sJT_st.setString(6,zip);
   __sJT_st.setString(7,loadFilename);
   __sJT_st.setInt(8,group1);
   __sJT_st.setInt(9,association);
   __sJT_st.setLong(10,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:353^9*/
      }
      else
      {
        // insert
        /*@lineinfo:generated-code*//*@lineinfo:358^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mif
//            (
//              date_opened,
//              merchant_status,
//              dmacctst,
//              dba_name,
//              name1_line_1,
//              addr1_line_1,
//              city1_line_4,
//              state1_line_4,
//              zip1_line_4,
//              bank_number,
//              load_filename,
//              group_1_association,
//              dmagent,
//              merchant_number
//            )
//            values
//            (
//              to_char(sysdate, 'MMDDYY'),
//              'O',
//              null,
//              :orgName,
//              :orgName,
//              :address,
//              :city,
//              :state,
//              :zip,
//              :COMDATA_BANK_NUMBER,
//              :loadFilename,
//              :group1,
//              :association,
//              :merchantNumber
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mif\n          (\n            date_opened,\n            merchant_status,\n            dmacctst,\n            dba_name,\n            name1_line_1,\n            addr1_line_1,\n            city1_line_4,\n            state1_line_4,\n            zip1_line_4,\n            bank_number,\n            load_filename,\n            group_1_association,\n            dmagent,\n            merchant_number\n          )\n          values\n          (\n            to_char(sysdate, 'MMDDYY'),\n            'O',\n            null,\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.startup.RefreshComdataHierarchy",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,orgName);
   __sJT_st.setString(2,orgName);
   __sJT_st.setString(3,address);
   __sJT_st.setString(4,city);
   __sJT_st.setString(5,state);
   __sJT_st.setString(6,zip);
   __sJT_st.setInt(7,COMDATA_BANK_NUMBER);
   __sJT_st.setString(8,loadFilename);
   __sJT_st.setInt(9,group1);
   __sJT_st.setInt(10,association);
   __sJT_st.setLong(11,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:394^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("updateHierarchy(" + merchantNumber + ")", e.toString());
    }
  }
  
  public static void main (String[] args)
  {
    SQLJConnectionBase.initStandalone("DEBUG");
    RefreshComdataHierarchy rch = new RefreshComdataHierarchy();
    
    rch.execute();
  }
}/*@lineinfo:generated-code*/