/*@lineinfo:filename=TranscomACH*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/TranscomACH.sqlj $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-07-20 10:07:58 -0700 (Mon, 20 Jul 2009) $
  Version            : $Revision: 16324 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class TranscomACH extends TranscomFileProcess
{
  static Logger log = Logger.getLogger(TranscomACH.class);

  public TranscomACH()
  {
    super(FILE_TYPE_ACH);
  }
  
  public TranscomACH(String connectionString)
  {
    super(FILE_TYPE_ACH, connectionString);
  }
  
  protected void loadCSVFile(Vector loadNames)
  {
    ResultSet             rs  = null;
    ResultSetIterator     it  = null;
    
    try
    {
      csvFile.clear();
      
      for(int i=0; i < loadNames.size(); ++i)
      {
        String loadName = (String)loadNames.elementAt(i);
      
        log.debug("  *   processing " + loadName);
        
        /*@lineinfo:generated-code*//*@lineinfo:66^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_char(to_number(a.internal_account_number)) id,
//                    replace(m.dba_name,',',' ')                   name,
//                    a.post_date_option                            adate,
//                    (a.amount_of_transaction * 
//                     decode(a.transaction_code, 22, 1, 23, 1, 32, 1, 33, 1, -1)) amount
//            from    ach_detail  a,
//                    application app,
//                    merchant    mr,
//                    mif         m
//            where   a.load_filename = :loadName and
//                    m.merchant_number = to_number(decode(a.internal_account_number, 'PRIMARY ACCOUNT', 0, a.internal_account_number)) and
//                    m.merchant_number = mr.merch_number and
//                    mr.app_seq_num = app.app_seq_num and
//                    app.app_type in (0, 8, 28)        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  to_char(to_number(a.internal_account_number)) id,\n                  replace(m.dba_name,',',' ')                   name,\n                  a.post_date_option                            adate,\n                  (a.amount_of_transaction * \n                   decode(a.transaction_code, 22, 1, 23, 1, 32, 1, 33, 1, -1)) amount\n          from    ach_detail  a,\n                  application app,\n                  merchant    mr,\n                  mif         m\n          where   a.load_filename =  :1  and\n                  m.merchant_number = to_number(decode(a.internal_account_number, 'PRIMARY ACCOUNT', 0, a.internal_account_number)) and\n                  m.merchant_number = mr.merch_number and\n                  mr.app_seq_num = app.app_seq_num and\n                  app.app_type in (0, 8, 28)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TranscomACH",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadName);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.TranscomACH",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:82^9*/
        
        rs = it.getResultSet();
        
        if(i == 0)
        {
          // set the header
          csvFile.setHeader(rs);
        }
        
        // add the rows
        csvFile.addRows(rs);
        
        rs.close();
        it.close();
        
        // get 
      }
    }
    catch(Exception e)
    {
      logEntry("loadCSVFile()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { it.close(); } catch (Exception e) {}
    }
  }
  
  public static void main(String[] args)
  {
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      
      TranscomACH worker = new TranscomACH();
      
      worker.execute();
    }
    catch(Exception e)
    {
      log.error("ERROR: " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/