/*@lineinfo:filename=BatchMaintExport*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/BatchMaintExport.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.net.MailMessage;
import com.mes.support.CSVFileDisk;
import sqlj.runtime.ResultSetIterator;

public class BatchMaintExport extends EventBase
{
  static Logger log = Logger.getLogger(BatchMaintExport.class);
  
  public static int     BILLING_DAY_BEGIN                 = 25;
  public static int     BILLING_DAY_END                   = 24;
  
  public  static final int          ZIP_BUFFER_LEN      = 4096;
  
  // add bank numbers to this list to enable processing
  // their records for the TSYS batch maint file.
  public static final int[] EligibleBanks =
  {
    mesConstants.BANK_ID_MES,
    mesConstants.BANK_ID_STERLING,
    mesConstants.BANK_ID_CBT,
    mesConstants.BANK_ID_MES_WF,
    mesConstants.BANK_ID_SVB
  };
  
  private     int                 HeaderCount     = 0;

  public BatchMaintExport()
  {
    PropertiesFilename = "batchmaint.properties";
  }
  
  protected void encodeHeader( StringBuffer buffer, long merchantId, String action )
  {
    FlatFileRecord            ffd         = new FlatFileRecord(MesFlatFiles.DEF_TYPE_BM_HEADER);
    ResultSetIterator         it          = null;
    ResultSet                 resultSet   = null;
    try
    {
      buffer.setLength(0);
      
      ++HeaderCount;   // increment packet counter
  
      /*@lineinfo:generated-code*//*@lineinfo:83^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_char(sysdate,'mmddyyyy')     as file_date,
//                  mf.bank_number                  as bank_number,
//                  decode(mf.bank_number,
//                         3858,'0','') ||
//                  to_char(mf.merchant_number)     as merchant_number,
//                  'MCG'                           as packet_type,
//                  :HeaderCount                    as packet_id,
//                  :action                         as action_ind
//          from    mif                   mf
//          where   mf.merchant_number = :merchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  to_char(sysdate,'mmddyyyy')     as file_date,\n                mf.bank_number                  as bank_number,\n                decode(mf.bank_number,\n                       3858,'0','') ||\n                to_char(mf.merchant_number)     as merchant_number,\n                'MCG'                           as packet_type,\n                 :1                     as packet_id,\n                 :2                          as action_ind\n        from    mif                   mf\n        where   mf.merchant_number =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.BatchMaintExport",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,HeaderCount);
   __sJT_st.setString(2,action);
   __sJT_st.setLong(3,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.BatchMaintExport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:95^7*/
      resultSet = it.getResultSet();
  
      if( resultSet.next() )
      {
        ffd.setAllFieldData(resultSet);
      }
      resultSet.close();
      it.close();
      
      buffer.append( ffd.spew() );
    }
    catch(Exception e)
    {
      logEntry("encodeHeader(" + merchantId + ")",e.toString());
    }
    finally
    {
      try{it.close();}catch(Exception ee){}
    }
  }
  
  public boolean execute()
  {
    int                 bankNumber  = 0;
    Date                beginDate   = null;
    Calendar            cal         = Calendar.getInstance();
    String              dateString  = null;
    Date                endDate     = null;
    String              filename    = null;
    File                file        = null;
    int                 recCount    = 0;
    boolean             result      = false;
    
    try
    {
      connect();
      
      for( int i = 0; i < EligibleBanks.length; ++i )
      {
        cal         = Calendar.getInstance();   // set to today
        bankNumber  = EligibleBanks[i];
        
        // build the load filename daily records
        filename = generateFilename("bm" + bankNumber);
        
        // start of a new monthly cycle, clear the current cycle
        if ( cal.get(Calendar.DAY_OF_MONTH) > BILLING_DAY_END )
        {
          // setup the query dates
          cal.set(Calendar.DAY_OF_MONTH,BILLING_DAY_END);
          endDate = new java.sql.Date( cal.getTime().getTime() );
        
          // move back one month
          cal.set(Calendar.DAY_OF_MONTH,BILLING_DAY_BEGIN);
          cal.add(Calendar.MONTH,-1);
          beginDate = new java.sql.Date( cal.getTime().getTime() );
        
          if ( monthProcessed( bankNumber, endDate ) == false )
          {
            // add monthly records to billing_auto_cg table
            exportMonthlyChargeRecs(bankNumber,beginDate,endDate);
                                     
            // mark the month as processed
            /*@lineinfo:generated-code*//*@lineinfo:159^13*/

//  ************************************************************
//  #sql [Ctx] { insert into billing_auto_cg_month_end
//                  ( load_filename, active_date, file_creation_date, bank_number )
//                values
//                  ( :filename, trunc(:endDate,'month'), sysdate, :bankNumber )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into billing_auto_cg_month_end\n                ( load_filename, active_date, file_creation_date, bank_number )\n              values\n                (  :1 , trunc( :2 ,'month'), sysdate,  :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.BatchMaintExport",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,filename);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setInt(3,bankNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:165^13*/
          }                                     
        }                                   
        
        HeaderCount = 0;    // reset the packet id
        
        // TSYS merchants first
        /*@lineinfo:generated-code*//*@lineinfo:172^9*/

//  ************************************************************
//  #sql [Ctx] { select count(cg.rec_id) 
//            from    mif               mf,
//                    billing_auto_cg   cg
//            where   mf.bank_number = :bankNumber 
//                    and not nvl(mf.dmacctst,' ') in ( 'D', 'C', 'B', 'Z' )
//                    and nvl(mf.processor_id, 0) = 0
//                    and cg.merchant_number = mf.merchant_number
//                    and trunc(cg.date_start,'month') >= trunc(sysdate,'month')
//                    and cg.load_filename is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(cg.rec_id)  \n          from    mif               mf,\n                  billing_auto_cg   cg\n          where   mf.bank_number =  :1  \n                  and not nvl(mf.dmacctst,' ') in ( 'D', 'C', 'B', 'Z' )\n                  and nvl(mf.processor_id, 0) = 0\n                  and cg.merchant_number = mf.merchant_number\n                  and trunc(cg.date_start,'month') >= trunc(sysdate,'month')\n                  and cg.load_filename is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.BatchMaintExport",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:183^9*/
        
        if ( recCount > 0  && bankNumber != mesConstants.BANK_ID_SVB )
        {
          exportAutoBillChargeRecs( bankNumber, filename );
        }
        
        if ( HeaderCount == 0 )
        {
          // delete the empty file
          file = new File(filename);
          file.delete();
        }
        else
        {
          // send the file to tsys
          sendFile(filename);
          sendReceipt(bankNumber,filename);
        }
        
        //.now look for MBS charge records that need uploading
        /*@lineinfo:generated-code*//*@lineinfo:204^9*/

//  ************************************************************
//  #sql [Ctx] { select count(cg.rec_id) 
//            from    mif               mf,
//                    billing_auto_cg   cg
//            where   mf.bank_number = :bankNumber 
//                    and not nvl(mf.dmacctst,' ') in ( 'D', 'C', 'B', 'Z' )
//                    and nvl(mf.processor_id, 0) = 1
//                    and cg.merchant_number = mf.merchant_number
//                    and trunc(cg.date_start,'month') >= trunc(sysdate,'month')
//                    and cg.load_filename is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(cg.rec_id)  \n          from    mif               mf,\n                  billing_auto_cg   cg\n          where   mf.bank_number =  :1  \n                  and not nvl(mf.dmacctst,' ') in ( 'D', 'C', 'B', 'Z' )\n                  and nvl(mf.processor_id, 0) = 1\n                  and cg.merchant_number = mf.merchant_number\n                  and trunc(cg.date_start,'month') >= trunc(sysdate,'month')\n                  and cg.load_filename is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.BatchMaintExport",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:215^9*/
        
        if ( recCount > 0 )
        {
          exportMBSChargeRecs( bankNumber, filename );
        }
      }        
      result = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( result );
  }
  
  public void exportAutoBillChargeRecs( int bankNumber, String filename )
  {
    String              action            = null;
    Calendar            cal               = Calendar.getInstance();
    CSVFileDisk         csvFile           = null;
    String              csvFilename       = null;
    int                 defType           = 0;
    BufferedWriter      exportFile        = null;
    FlatFileRecord      ffd               = new FlatFileRecord();
    ResultSetIterator   it                = null;
    String              lastAction        = "";
    long                lastMerchantId    = 0L;
    StringBuffer        line              = new StringBuffer();
    long                merchantId        = 0L;
    ResultSet           resultSet         = null;
    
    try
    {
      exportFile  = new BufferedWriter( new FileWriter( filename, true ) );
      csvFilename = getCsvFilename( filename ); 
      csvFile     = new CSVFileDisk( csvFilename );
    
      /*@lineinfo:generated-code*//*@lineinfo:257^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(cg.charge_type,'MIS')               as charge_type,
//                  nvl(cg.action,'A')                      as action,
//                  mf.merchant_number                      as merchant_number,
//                  cg.dda_number                           as dda_number,
//                  cg.transit_routing                      as transit_routing,
//                  cg.statement_message                    as statement_message,
//                  'CG0001'                                as field_id_01,
//                  1                                       as field_length_01,
//                  'D'                                     as debit_remit_flag,
//                  'CG0002'                                as field_id_02,
//                  12                                      as field_length_02,
//                  cg.billing_months                       as billing_months,
//                  'CG0003'                                as field_id_03,
//                  4                                       as field_length_03,
//                  to_char(cg.date_start,'mmyy')           as start_month,
//                  'CG0004'                                as field_id_04,
//                  6                                       as field_length_04,
//                  decode( cg.date_expiration,
//                          to_date('12/31/9999','mm/dd/yyyy'), '999999',
//                          to_char(cg.date_expiration,'mmddyy')
//                        )                                 as expiration_date,                
//                  'CG0005'                                as field_id_05,
//                  8                                       as field_length_05,
//                  ( 
//                    decode( cg.charge_amount,
//                            0,'0',
//                            decode( (abs(cg.charge_amount)/cg.charge_amount),
//                                    1,'0',
//                                    '-' 
//                                  ) 
//                          ) ||
//                    lpad((abs(cg.charge_amount)*100),7,'0') 
//                  )                                       as charge_amount,              
//                  'CG0006'                                as field_id_06,
//                  17                                      as field_length_06,
//                  cg.dda_number                           as charge_dda_number,
//                  'CG0007'                                as field_id_07,
//                  9                                       as field_length_07,
//                  lpad(cg.transit_routing,9,'0')          as charge_transit_routing,
//                  'CG0008'                                as field_id_08,
//                  4                                       as field_length_08,
//                  '0000'                                  as officer_code,
//                  'CG0009'                                as field_id_09,
//                  2                                       as field_length_09,
//                  lpad(nvl(cg.item_count,0),2,'0')        as number_to_charge,
//                  'CG0010'                                as field_id_10,
//                  1                                       as field_length_10,
//                  'N'                                     as sales_tax_indicator,
//                  'CG0011'                                as field_id_11,
//                  74                                      as field_length_11,
//                  cg.statement_message                    as charge_message,
//                  cg.rec_id                               as rec_id
//          from    mif                         mf,
//                  billing_auto_cg             cg
//          where   mf.bank_number = :bankNumber
//                  --and mf.dmacctst is null and   -- not statused
//                  and not nvl(mf.dmacctst,' ') in ( 'D', 'C', 'B' )
//                  and nvl(mf.processor_id, 0) = 0 -- TSYS
//                  and cg.merchant_number = mf.merchant_number
//                  --and cg.active_date between :beginDate and :endDate and
//                  and trunc(cg.date_start,'month') >= trunc(sysdate,'month')
//                  and cg.load_filename is null
//          order by mf.merchant_number, nvl(cg.action,'A')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(cg.charge_type,'MIS')               as charge_type,\n                nvl(cg.action,'A')                      as action,\n                mf.merchant_number                      as merchant_number,\n                cg.dda_number                           as dda_number,\n                cg.transit_routing                      as transit_routing,\n                cg.statement_message                    as statement_message,\n                'CG0001'                                as field_id_01,\n                1                                       as field_length_01,\n                'D'                                     as debit_remit_flag,\n                'CG0002'                                as field_id_02,\n                12                                      as field_length_02,\n                cg.billing_months                       as billing_months,\n                'CG0003'                                as field_id_03,\n                4                                       as field_length_03,\n                to_char(cg.date_start,'mmyy')           as start_month,\n                'CG0004'                                as field_id_04,\n                6                                       as field_length_04,\n                decode( cg.date_expiration,\n                        to_date('12/31/9999','mm/dd/yyyy'), '999999',\n                        to_char(cg.date_expiration,'mmddyy')\n                      )                                 as expiration_date,                \n                'CG0005'                                as field_id_05,\n                8                                       as field_length_05,\n                ( \n                  decode( cg.charge_amount,\n                          0,'0',\n                          decode( (abs(cg.charge_amount)/cg.charge_amount),\n                                  1,'0',\n                                  '-' \n                                ) \n                        ) ||\n                  lpad((abs(cg.charge_amount)*100),7,'0') \n                )                                       as charge_amount,              \n                'CG0006'                                as field_id_06,\n                17                                      as field_length_06,\n                cg.dda_number                           as charge_dda_number,\n                'CG0007'                                as field_id_07,\n                9                                       as field_length_07,\n                lpad(cg.transit_routing,9,'0')          as charge_transit_routing,\n                'CG0008'                                as field_id_08,\n                4                                       as field_length_08,\n                '0000'                                  as officer_code,\n                'CG0009'                                as field_id_09,\n                2                                       as field_length_09,\n                lpad(nvl(cg.item_count,0),2,'0')        as number_to_charge,\n                'CG0010'                                as field_id_10,\n                1                                       as field_length_10,\n                'N'                                     as sales_tax_indicator,\n                'CG0011'                                as field_id_11,\n                74                                      as field_length_11,\n                cg.statement_message                    as charge_message,\n                cg.rec_id                               as rec_id\n        from    mif                         mf,\n                billing_auto_cg             cg\n        where   mf.bank_number =  :1 \n                --and mf.dmacctst is null and   -- not statused\n                and not nvl(mf.dmacctst,' ') in ( 'D', 'C', 'B' )\n                and nvl(mf.processor_id, 0) = 0 -- TSYS\n                and cg.merchant_number = mf.merchant_number\n                --and cg.active_date between :beginDate and :endDate and\n                and trunc(cg.date_start,'month') >= trunc(sysdate,'month')\n                and cg.load_filename is null\n        order by mf.merchant_number, nvl(cg.action,'A')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.BatchMaintExport",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.BatchMaintExport",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:322^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        if ( lastMerchantId == 0L && csvFile != null )   // first time in loop
        {
          File tempFile = new File(csvFilename);
        
          // if it does not exists, the set the header
          if ( ! tempFile.exists() )
          {
            csvFile.setHeader(resultSet);
          }          
        }          
      
        action          = resultSet.getString("action");
        merchantId      = resultSet.getLong("merchant_number");
        
        // if the action or the merchant id has changed, create new header
        if ( merchantId != lastMerchantId || !action.equals(lastAction) )
        {
          encodeHeader(line,merchantId,action);
          exportFile.write( line.toString() );
          exportFile.newLine();
          
          if ( action.equals("D") )
          {
            defType = MesFlatFiles.DEF_TYPE_BM_DEL_CHARGE_REC;
          }
          else  // add/update
          {            
            defType = MesFlatFiles.DEF_TYPE_BM_CHARGE_REC_DT;
          }            
          
          // reset the type of the details record
          ffd.setDefType(defType);
        }
        lastMerchantId = merchantId;
        lastAction     = action;

        // set the field data from the acr results        
        ffd.setAllFieldData(resultSet);
          
        // write the spew to the file and add newline
        exportFile.write( ffd.spew() );
        exportFile.newLine();
        
        if ( csvFile != null )
        {
          csvFile.addRow(resultSet);
        }
        
        // mark this record as exported
        /*@lineinfo:generated-code*//*@lineinfo:376^9*/

//  ************************************************************
//  #sql [Ctx] { update  billing_auto_cg
//            set     load_filename = :filename
//            where   rec_id = :resultSet.getLong("rec_id")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_3793 = resultSet.getLong("rec_id");
   String theSqlTS = "update  billing_auto_cg\n          set     load_filename =  :1 \n          where   rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.BatchMaintExport",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,filename);
   __sJT_st.setLong(2,__sJT_3793);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:381^9*/
      }
    }
    catch(Exception e)
    {
      logEntry( "exportAutoBillChargeRecs()", e.toString() );
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      try { exportFile.close(); } catch( Exception e ) {}
    }
  }
  
  protected void exportMBSChargeRecs( int bankNumber, String filename )
  {
    try
    {
      // insert records directly into mbs_charge_records
      /*@lineinfo:generated-code*//*@lineinfo:400^7*/

//  ************************************************************
//  #sql [Ctx] { insert into mbs_charge_records
//          (
//            charge_type,
//            merchant_number,
//            billing_frequency,
//            item_count,
//            charge_amount,
//            transit_routing,
//            dda,
//            start_date,
//            end_date,
//            statement_message,
//            charge_acct_type,
//            enabled,
//            created_by
//          )
//          select  nvl(cg.charge_type, 'MIS')  as charge_type,
//                  cg.merchant_number          as merchant_number,
//                  cg.billing_months           as billing_frequency,
//                  nvl(cg.item_count,0)        as item_count,
//                  cg.charge_amount            as charge_amount,
//                  lpad(cg.transit_routing,9,'0')  as transit_routing,
//                  cg.dda_number               as dda,
//                  cg.date_start               as start_date,
//                  cg.date_expiration          as end_date,
//                  cg.statement_message        as statement_message,
//                  'CK'                        as charge_acct_type,
//                  'Y'                         as enabled,
//                  'BATCH MAINT'               as created_by
//          from    mif mf,
//                  billing_auto_cg cg
//          where   mf.bank_number = :bankNumber
//                  and not nvl(mf.dmacctst,' ') in ( 'D', 'C', 'B' )
//                  and nvl(mf.processor_id, 0) = 1 -- MBS
//                  and mf.merchant_number = cg.merchant_number
//                  and trunc(cg.date_start,'month') >= trunc(sysdate, 'month')
//                  and cg.load_filename is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mbs_charge_records\n        (\n          charge_type,\n          merchant_number,\n          billing_frequency,\n          item_count,\n          charge_amount,\n          transit_routing,\n          dda,\n          start_date,\n          end_date,\n          statement_message,\n          charge_acct_type,\n          enabled,\n          created_by\n        )\n        select  nvl(cg.charge_type, 'MIS')  as charge_type,\n                cg.merchant_number          as merchant_number,\n                cg.billing_months           as billing_frequency,\n                nvl(cg.item_count,0)        as item_count,\n                cg.charge_amount            as charge_amount,\n                lpad(cg.transit_routing,9,'0')  as transit_routing,\n                cg.dda_number               as dda,\n                cg.date_start               as start_date,\n                cg.date_expiration          as end_date,\n                cg.statement_message        as statement_message,\n                'CK'                        as charge_acct_type,\n                'Y'                         as enabled,\n                'BATCH MAINT'               as created_by\n        from    mif mf,\n                billing_auto_cg cg\n        where   mf.bank_number =  :1 \n                and not nvl(mf.dmacctst,' ') in ( 'D', 'C', 'B' )\n                and nvl(mf.processor_id, 0) = 1 -- MBS\n                and mf.merchant_number = cg.merchant_number\n                and trunc(cg.date_start,'month') >= trunc(sysdate, 'month')\n                and cg.load_filename is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.BatchMaintExport",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:439^7*/
      
      
      // update records with filename to show they were processed
      /*@lineinfo:generated-code*//*@lineinfo:443^7*/

//  ************************************************************
//  #sql [Ctx] { update  billing_auto_cg
//          set     load_filename = :filename||' - MBS'
//          where   rec_id in
//                  (
//                    select  cg.rec_id
//                    from    mif mf,
//                            billing_auto_cg cg
//                    where   mf.bank_number = :bankNumber
//                            and not nvl(mf.dmacctst,' ') in ( 'D', 'C', 'B' )
//                            and nvl(mf.processor_id, 0) = 1 -- MBS
//                            and mf.merchant_number = cg.merchant_number
//                            and trunc(cg.date_start,'month') >= trunc(sysdate, 'month')
//                            and cg.load_filename is null
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  billing_auto_cg\n        set     load_filename =  :1 ||' - MBS'\n        where   rec_id in\n                (\n                  select  cg.rec_id\n                  from    mif mf,\n                          billing_auto_cg cg\n                  where   mf.bank_number =  :2 \n                          and not nvl(mf.dmacctst,' ') in ( 'D', 'C', 'B' )\n                          and nvl(mf.processor_id, 0) = 1 -- MBS\n                          and mf.merchant_number = cg.merchant_number\n                          and trunc(cg.date_start,'month') >= trunc(sysdate, 'month')\n                          and cg.load_filename is null\n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.BatchMaintExport",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,filename);
   __sJT_st.setInt(2,bankNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:459^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("exportMBSChargeRecs(" + bankNumber + ")", e.toString());
      rollback();
    } 
  }
  
  public void exportMonthlyChargeRecs( int bankNumber, Date beginDate, Date endDate )
  {
    Date                billingDate       = null;
    Calendar            cal               = Calendar.getInstance();
    StringBuffer        months            = new StringBuffer("NNNNNNNNNNNN");
    
    try
    {
      cal.setTime(endDate);
      cal.set(Calendar.DAY_OF_MONTH,1);
      months.setCharAt(cal.get(Calendar.MONTH), 'Y');
      billingDate = new java.sql.Date(cal.getTime().getTime());
      
      if ( bankNumber != mesConstants.BANK_ID_SVB ) {
	      /*@lineinfo:generated-code*//*@lineinfo:484^8*/

//  ************************************************************
//  #sql [Ctx] { insert into billing_auto_cg
//  	        (
//  	          merchant_number,
//  	          charge_amount,
//  	          statement_message,
//  	          billing_months,
//  	          date_start,
//  	          date_expiration,
//  	          active_date
//  	        )
//  	        select  mf.merchant_number                as merchant_number,
//  	                (acg.charge_amount * rt.retrieval_count)
//  	                                                  as charge_amount,
//  	                ( rt.retrieval_count || 
//  	                  ' RETRIEVAL FEES @ $' ||
//  	                  to_char(acg.charge_amount,'999.99') || 
//  	                  '/PER' )                        as statement_message,
//  	                :months.toString()              as billing_months,
//  	                trunc(:endDate,'month')           as start_month,
//  	                trunc(last_day(last_day(sysdate)+1))
//  	                                                  as expiration_date,
//  	                trunc(sysdate)                    as active_date
//  	        from    network_retrievals_auto_cg  acg,
//  	                t_hierarchy                 th,
//  	                mif                         mf,
//  	                (
//  	                  select  merchant_number         as merchant_number,
//  	                          count(merchant_number)  as retrieval_count
//  	                  from    network_retrievals
//  	                  where   incoming_date between :beginDate and :endDate
//  	                  group by merchant_number                
//  	                )                           rt
//  	        where   :billingDate between acg.valid_date_begin and acg.valid_date_end and
//  	                th.hier_type = 1 and
//  	                th.ancestor = acg.hierarchy_node and
//  	                th.entity_type = 4 and
//  	                mf.association_node = th.descendent and
//  	                mf.bank_number = :bankNumber and
//  	                nvl(mf.dmacctst,' ') not in ( 'D', 'C', 'B' ) and
//  	                rt.merchant_number = mf.merchant_number      
//  	       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3794 = months.toString();
   String theSqlTS = "insert into billing_auto_cg\n\t        (\n\t          merchant_number,\n\t          charge_amount,\n\t          statement_message,\n\t          billing_months,\n\t          date_start,\n\t          date_expiration,\n\t          active_date\n\t        )\n\t        select  mf.merchant_number                as merchant_number,\n\t                (acg.charge_amount * rt.retrieval_count)\n\t                                                  as charge_amount,\n\t                ( rt.retrieval_count || \n\t                  ' RETRIEVAL FEES @ $' ||\n\t                  to_char(acg.charge_amount,'999.99') || \n\t                  '/PER' )                        as statement_message,\n\t                 :1               as billing_months,\n\t                trunc( :2 ,'month')           as start_month,\n\t                trunc(last_day(last_day(sysdate)+1))\n\t                                                  as expiration_date,\n\t                trunc(sysdate)                    as active_date\n\t        from    network_retrievals_auto_cg  acg,\n\t                t_hierarchy                 th,\n\t                mif                         mf,\n\t                (\n\t                  select  merchant_number         as merchant_number,\n\t                          count(merchant_number)  as retrieval_count\n\t                  from    network_retrievals\n\t                  where   incoming_date between  :3  and  :4 \n\t                  group by merchant_number                \n\t                )                           rt\n\t        where    :5  between acg.valid_date_begin and acg.valid_date_end and\n\t                th.hier_type = 1 and\n\t                th.ancestor = acg.hierarchy_node and\n\t                th.entity_type = 4 and\n\t                mf.association_node = th.descendent and\n\t                mf.bank_number =  :6  and\n\t                nvl(mf.dmacctst,' ') not in ( 'D', 'C', 'B' ) and\n\t                rt.merchant_number = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.BatchMaintExport",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3794);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   __sJT_st.setDate(5,billingDate);
   __sJT_st.setInt(6,bankNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:526^8*/
	        
	          
	      /*@lineinfo:generated-code*//*@lineinfo:529^8*/

//  ************************************************************
//  #sql [Ctx] { insert into billing_auto_cg
//  	        (
//  	          merchant_number,
//  	          charge_amount,
//  	          statement_message,
//  	          billing_months,
//  	          date_start,
//  	          date_expiration,
//  	          active_date
//  	        )
//  	        select  mf.merchant_number                    as merchant_number,
//  	                (acg.charge_amount * sc.call_count)   as charge_amount,
//  	                ( sc.call_count || 
//  	                  ' CUSTOMER SERVICE CALLS @ $' ||
//  	                  to_char(acg.charge_amount,'999.99') || 
//  	                  '/PER' )                            as statement_message,
//  	                :months.toString()                  as billing_months,
//  	                trunc(:endDate,'month')               as start_month,
//  	                trunc(last_day(last_day(sysdate)+1))  as expiration_date,
//  	                trunc(sysdate)                        as active_date
//  	        from    service_calls_auto_cg       acg,
//  	                t_hierarchy                 th,
//  	                mif                         mf,
//  	                (
//  	                  select  sc.merchant_number         as merchant_number,
//  	                          count(sc.merchant_number)  as call_count
//  	                  from    mif                     mf,
//  	                          service_calls           sc,
//  	                          service_call_exemptions sce
//  	                  where   mf.bank_number = :bankNumber and
//  	                          sc.MERCHANT_NUMBER = mf.merchant_number and
//  	                          trunc(sc.call_date) between :beginDate and :endDate and
//  	                          (
//  	                            delay_service_calls(sc.merchant_number) = 0 or
//  	                            trunc(sc.call_date) > (mf.activation_date + delay_service_calls(sc.merchant_number))
//  	                          ) and
//  	                          nvl(sc.client_generated,'N') = 'N' and
//  	                          nvl(sc.billable,'Y') = 'Y' and
//  	                          sce.hierarchy_node(+) = get_service_call_node( sc.merchant_number ) and
//  	                          sce.call_type(+) = sc.type and
//  	                          sce.call_type is null
//  	                  group by sc.merchant_number                
//  	                )                           sc
//  	        where   :billingDate between acg.valid_date_begin and acg.valid_date_end and
//  	                th.hier_type = 1 and
//  	                th.ancestor = acg.hierarchy_node and
//  	                th.entity_type = 4 and
//  	                mf.association_node = th.descendent and
//  	                mf.bank_number = :bankNumber and
//  	                nvl(mf.dmacctst,' ') not in ( 'D', 'C', 'B' ) and
//  	                sc.merchant_number = mf.merchant_number      
//  	       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3795 = months.toString();
   String theSqlTS = "insert into billing_auto_cg\n\t        (\n\t          merchant_number,\n\t          charge_amount,\n\t          statement_message,\n\t          billing_months,\n\t          date_start,\n\t          date_expiration,\n\t          active_date\n\t        )\n\t        select  mf.merchant_number                    as merchant_number,\n\t                (acg.charge_amount * sc.call_count)   as charge_amount,\n\t                ( sc.call_count || \n\t                  ' CUSTOMER SERVICE CALLS @ $' ||\n\t                  to_char(acg.charge_amount,'999.99') || \n\t                  '/PER' )                            as statement_message,\n\t                 :1                   as billing_months,\n\t                trunc( :2 ,'month')               as start_month,\n\t                trunc(last_day(last_day(sysdate)+1))  as expiration_date,\n\t                trunc(sysdate)                        as active_date\n\t        from    service_calls_auto_cg       acg,\n\t                t_hierarchy                 th,\n\t                mif                         mf,\n\t                (\n\t                  select  sc.merchant_number         as merchant_number,\n\t                          count(sc.merchant_number)  as call_count\n\t                  from    mif                     mf,\n\t                          service_calls           sc,\n\t                          service_call_exemptions sce\n\t                  where   mf.bank_number =  :3  and\n\t                          sc.MERCHANT_NUMBER = mf.merchant_number and\n\t                          trunc(sc.call_date) between  :4  and  :5  and\n\t                          (\n\t                            delay_service_calls(sc.merchant_number) = 0 or\n\t                            trunc(sc.call_date) > (mf.activation_date + delay_service_calls(sc.merchant_number))\n\t                          ) and\n\t                          nvl(sc.client_generated,'N') = 'N' and\n\t                          nvl(sc.billable,'Y') = 'Y' and\n\t                          sce.hierarchy_node(+) = get_service_call_node( sc.merchant_number ) and\n\t                          sce.call_type(+) = sc.type and\n\t                          sce.call_type is null\n\t                  group by sc.merchant_number                \n\t                )                           sc\n\t        where    :6  between acg.valid_date_begin and acg.valid_date_end and\n\t                th.hier_type = 1 and\n\t                th.ancestor = acg.hierarchy_node and\n\t                th.entity_type = 4 and\n\t                mf.association_node = th.descendent and\n\t                mf.bank_number =  :7  and\n\t                nvl(mf.dmacctst,' ') not in ( 'D', 'C', 'B' ) and\n\t                sc.merchant_number = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.BatchMaintExport",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3795);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setInt(3,bankNumber);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setDate(6,billingDate);
   __sJT_st.setInt(7,bankNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:582^8*/
	      
	      // interchange correction billing
	      /*@lineinfo:generated-code*//*@lineinfo:585^8*/

//  ************************************************************
//  #sql [Ctx] { insert into billing_auto_cg
//  	        (
//  	          merchant_number,
//  	          charge_amount,
//  	          statement_message,
//  	          billing_months,
//  	          date_start,
//  	          date_expiration,
//  	          active_date
//  	        )
//  	        select  mf.merchant_number                    as merchant_number,
//  	                (acg.charge_amount * icc.corr_count)  as charge_amount,
//  	                ( icc.corr_count || 
//  	                  ' TRANSACTION REPAIR' || decode(icc.corr_count,1,' ','S ') || 
//  	                  '@ $' ||
//  	                  to_char(acg.charge_amount,'990.999') || 
//  	                  '/PER' )                            as statement_message,
//  	                :months.toString()                  as billing_months,
//  	                trunc(:endDate,'month')               as start_month,
//  	                trunc(last_day(last_day(sysdate)+1))  as expiration_date,
//  	                trunc(sysdate)                        as active_date
//  	        from    trident_ic_corr_auto_cg     acg,
//  	                organization                o,
//  	                group_merchant              gm,
//  	                mif                         mf,
//  	                (
//  	                  select  icc.merchant_number         as merchant_number,
//  	                          count(icc.merchant_number)  as corr_count
//  	                  from    mif                             mf,
//  	                          trident_capture_ic_corrections  icc
//  	                  where   mf.bank_number = :bankNumber and
//  	                          icc.MERCHANT_NUMBER = mf.merchant_number and
//  	                          trunc(icc.batch_date) between :beginDate and :endDate
//  	                  group by icc.merchant_number 
//  	                )                           icc
//  	        where   :billingDate between acg.valid_date_begin and acg.valid_date_end and
//  	                o.org_group = acg.hierarchy_node and
//  	                gm.org_num = o.org_num and
//  	                mf.merchant_number = gm.merchant_number and
//  	                mf.bank_number = :bankNumber and
//  	                nvl(mf.dmacctst,' ') not in ( 'D', 'C', 'B' ) and
//  	                icc.merchant_number = mf.merchant_number      
//  	       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3796 = months.toString();
   String theSqlTS = "insert into billing_auto_cg\n\t        (\n\t          merchant_number,\n\t          charge_amount,\n\t          statement_message,\n\t          billing_months,\n\t          date_start,\n\t          date_expiration,\n\t          active_date\n\t        )\n\t        select  mf.merchant_number                    as merchant_number,\n\t                (acg.charge_amount * icc.corr_count)  as charge_amount,\n\t                ( icc.corr_count || \n\t                  ' TRANSACTION REPAIR' || decode(icc.corr_count,1,' ','S ') || \n\t                  '@ $' ||\n\t                  to_char(acg.charge_amount,'990.999') || \n\t                  '/PER' )                            as statement_message,\n\t                 :1                   as billing_months,\n\t                trunc( :2 ,'month')               as start_month,\n\t                trunc(last_day(last_day(sysdate)+1))  as expiration_date,\n\t                trunc(sysdate)                        as active_date\n\t        from    trident_ic_corr_auto_cg     acg,\n\t                organization                o,\n\t                group_merchant              gm,\n\t                mif                         mf,\n\t                (\n\t                  select  icc.merchant_number         as merchant_number,\n\t                          count(icc.merchant_number)  as corr_count\n\t                  from    mif                             mf,\n\t                          trident_capture_ic_corrections  icc\n\t                  where   mf.bank_number =  :3  and\n\t                          icc.MERCHANT_NUMBER = mf.merchant_number and\n\t                          trunc(icc.batch_date) between  :4  and  :5 \n\t                  group by icc.merchant_number \n\t                )                           icc\n\t        where    :6  between acg.valid_date_begin and acg.valid_date_end and\n\t                o.org_group = acg.hierarchy_node and\n\t                gm.org_num = o.org_num and\n\t                mf.merchant_number = gm.merchant_number and\n\t                mf.bank_number =  :7  and\n\t                nvl(mf.dmacctst,' ') not in ( 'D', 'C', 'B' ) and\n\t                icc.merchant_number = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.startup.BatchMaintExport",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3796);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setInt(3,bankNumber);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setDate(6,billingDate);
   __sJT_st.setInt(7,bankNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:629^8*/
      }
      
      // account updater service (AUS) billing 
      /*@lineinfo:generated-code*//*@lineinfo:633^7*/

//  ************************************************************
//  #sql [Ctx] { insert into billing_auto_cg
//          (
//            merchant_number,
//            charge_amount,
//            statement_message,
//            billing_months,
//            date_start,
//            date_expiration,
//            active_date
//          )
//          select  mf.merchant_number                    as merchant_number,  
//                  (count(rsp.merch_num) * nvl(mp.per_item,0.12))
//                                                        as charge_amount,
//                  (
//                    count(rsp.merch_num) 
//                      || ' UPDATER MATCHES  @ $' 
//                      || ltrim(rtrim(decode( substr(to_char(nvl(mp.per_item,0.12),'0.999'),-1),
//                                              '0',to_char(nvl(mp.per_item,0.12),'0.99'),
//                                              to_char(nvl(mp.per_item,0.12),'0.999') )
//                                    )
//                              )  
//                      || ' ' || to_char(sysdate,'MON yyyy')
//                  )                                     as statement_message,
//                  :months.toString()                  as billing_months,
//                  trunc(:endDate,'month')               as start_month,
//                  trunc(last_day(last_day(sysdate)+1))  as expiration_date,
//                  trunc(sysdate)                        as active_date
//          from    aus_responses       rsp, 
//                  aus_request_files   reqf, 
//                  mif                 mf,
//                  mbs_pricing         mp
//          where   rsp.rsp_code in ('CALL','CLOSED','NEWACCT','NEWEXP') 
//                  and rsp.reqf_id = reqf.reqf_id 
//                  and reqf.test_flag <> 'y' 
//                  and rsp.create_ts between :beginDate and :endDate
//                  and is_number(reqf.merch_num) != 0
//                  and mf.merchant_number = reqf.merch_num
//                  and mf.bank_number = :bankNumber
//                  and mp.merchant_number(+) = rsp.merch_num                
//                  and mp.item_type(+) = 204   -- account updater
//                  and rsp.create_ts between mp.valid_date_begin(+) and mp.valid_date_end(+)
//          group by mf.merchant_number,nvl(mp.per_item,0.12)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3797 = months.toString();
   String theSqlTS = "insert into billing_auto_cg\n        (\n          merchant_number,\n          charge_amount,\n          statement_message,\n          billing_months,\n          date_start,\n          date_expiration,\n          active_date\n        )\n        select  mf.merchant_number                    as merchant_number,  \n                (count(rsp.merch_num) * nvl(mp.per_item,0.12))\n                                                      as charge_amount,\n                (\n                  count(rsp.merch_num) \n                    || ' UPDATER MATCHES  @ $' \n                    || ltrim(rtrim(decode( substr(to_char(nvl(mp.per_item,0.12),'0.999'),-1),\n                                            '0',to_char(nvl(mp.per_item,0.12),'0.99'),\n                                            to_char(nvl(mp.per_item,0.12),'0.999') )\n                                  )\n                            )  \n                    || ' ' || to_char(sysdate,'MON yyyy')\n                )                                     as statement_message,\n                 :1                   as billing_months,\n                trunc( :2 ,'month')               as start_month,\n                trunc(last_day(last_day(sysdate)+1))  as expiration_date,\n                trunc(sysdate)                        as active_date\n        from    aus_responses       rsp, \n                aus_request_files   reqf, \n                mif                 mf,\n                mbs_pricing         mp\n        where   rsp.rsp_code in ('CALL','CLOSED','NEWACCT','NEWEXP') \n                and rsp.reqf_id = reqf.reqf_id \n                and reqf.test_flag <> 'y' \n                and rsp.create_ts between  :3  and  :4 \n                and is_number(reqf.merch_num) != 0\n                and mf.merchant_number = reqf.merch_num\n                and mf.bank_number =  :5 \n                and mp.merchant_number(+) = rsp.merch_num                \n                and mp.item_type(+) = 204   -- account updater\n                and rsp.create_ts between mp.valid_date_begin(+) and mp.valid_date_end(+)\n        group by mf.merchant_number,nvl(mp.per_item,0.12)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.BatchMaintExport",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3797);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   __sJT_st.setInt(5,bankNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:677^7*/
      
      //******************************************************************
      // Bank Specific Exports
      //******************************************************************
      
      // Sterling Bank (3942) Chargebacks
      if ( bankNumber == mesConstants.BANK_ID_STERLING )
      {
        /*@lineinfo:generated-code*//*@lineinfo:686^9*/

//  ************************************************************
//  #sql [Ctx] { insert into billing_auto_cg
//            (
//              merchant_number,
//              charge_amount,
//              statement_message,
//              billing_months,
//              date_start,
//              date_expiration,
//              active_date
//            )
//            select  cb.merchant_number                    as merchant_number,  
//                    (count(cba.cb_load_sec) * 20.00)      as charge_amount,
//                    (
//                      decode(cb.card_type,
//                             'VS', 'VISA',
//                             'MC', 'MASTERCARD',
//                             'DB', 'DEBIT',      
//                             'AM', 'AMEX',
//                             'DS', 'DISCOVER',
//                             cb.card_type) 
//                      || ' CHARGEBACK FEE '
//                      || count(cba.cb_load_sec) 
//                      || ' @ $20.00/PER'
//                    )                                     as statement_message,
//                    :months.toString()                  as billing_months,
//                    trunc(:endDate,'month')               as start_month,
//                    trunc(last_day(last_day(sysdate)+1))  as expiration_date,
//                    trunc(sysdate)                        as active_date
//            from    network_chargebacks           cb,
//                    network_chargeback_activity   cba,
//                    chargeback_action_codes       ac
//            where   cb.bank_number = :bankNumber
//                    and cb.incoming_date between :beginDate-180 and :endDate
//                    and cba.cb_load_sec = cb.cb_load_sec 
//                    and cba.action_date between :beginDate and :endDate
//                    and ac.short_action_code = cba.action_code
//                    and ac.adj_tran_code = 9071
//            group by cb.merchant_number,cb.card_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3798 = months.toString();
   String theSqlTS = "insert into billing_auto_cg\n          (\n            merchant_number,\n            charge_amount,\n            statement_message,\n            billing_months,\n            date_start,\n            date_expiration,\n            active_date\n          )\n          select  cb.merchant_number                    as merchant_number,  \n                  (count(cba.cb_load_sec) * 20.00)      as charge_amount,\n                  (\n                    decode(cb.card_type,\n                           'VS', 'VISA',\n                           'MC', 'MASTERCARD',\n                           'DB', 'DEBIT',      \n                           'AM', 'AMEX',\n                           'DS', 'DISCOVER',\n                           cb.card_type) \n                    || ' CHARGEBACK FEE '\n                    || count(cba.cb_load_sec) \n                    || ' @ $20.00/PER'\n                  )                                     as statement_message,\n                   :1                   as billing_months,\n                  trunc( :2 ,'month')               as start_month,\n                  trunc(last_day(last_day(sysdate)+1))  as expiration_date,\n                  trunc(sysdate)                        as active_date\n          from    network_chargebacks           cb,\n                  network_chargeback_activity   cba,\n                  chargeback_action_codes       ac\n          where   cb.bank_number =  :3 \n                  and cb.incoming_date between  :4 -180 and  :5 \n                  and cba.cb_load_sec = cb.cb_load_sec \n                  and cba.action_date between  :6  and  :7 \n                  and ac.short_action_code = cba.action_code\n                  and ac.adj_tran_code = 9071\n          group by cb.merchant_number,cb.card_type";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.startup.BatchMaintExport",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3798);
   __sJT_st.setDate(2,endDate);
   __sJT_st.setInt(3,bankNumber);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:726^9*/
      }
      
      // 3941/3943/3858 VMA, ZFL
      if (    bankNumber == mesConstants.BANK_ID_MES 
           || bankNumber == mesConstants.BANK_ID_MES_WF
           || bankNumber == mesConstants.BANK_ID_CBT )
      {
        /*@lineinfo:generated-code*//*@lineinfo:734^9*/

//  ************************************************************
//  #sql [Ctx] { insert into billing_auto_cg
//            (
//              merchant_number,
//              charge_amount,
//              statement_message,
//              billing_months,
//              date_start,
//              date_expiration,
//              active_date
//            )
//            select  sm.merchant_number                    as merchant_number,  
//                    (sum(sm.item_count) * mp.per_item)    as charge_amount,
//                    (
//                      'VISA '
//                      || upper(me.statement_msg_default)
//                      || ' ' || sum(sm.item_count)
//                      || ' @ $' 
//                      || ltrim(rtrim(decode( substr(to_char(mp.per_item,'0.999'),-1),
//                                              '0',to_char(mp.per_item,'0.99'),
//                                              to_char(mp.per_item,'0.999') )
//                                    )
//                              )  
//                      || '/EA ' || to_char(trunc(:beginDate,'month'),'MON yyyy')
//                    )                                     as statement_message,
//                    :months.toString()                  as billing_months,
//                    trunc(:endDate,'month')               as start_month,
//                    trunc(last_day(last_day(sysdate)+1))  as expiration_date,
//                    trunc(sysdate)                        as active_date
//            from    mif                           mf,
//                    visa_auth_misuse_summary      sm,
//                    mbs_pricing                   mp,
//                    mbs_elements                  me
//            where   mf.bank_number = :bankNumber
//                    and sm.merchant_number = mf.merchant_number
//                    and sm.active_date = trunc(:beginDate,'month')
//                    and mp.merchant_number = sm.merchant_number
//                    and mp.item_type = decode(sm.report_identifier,'FEE100S',11,'FEE200S',10,-1)
//                    and sm.active_date between mp.valid_date_begin and mp.valid_date_end
//                    and me.item_type = mp.item_type
//            group by sm.merchant_number, mp.per_item, me.statement_msg_default          
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3799 = months.toString();
   String theSqlTS = "insert into billing_auto_cg\n          (\n            merchant_number,\n            charge_amount,\n            statement_message,\n            billing_months,\n            date_start,\n            date_expiration,\n            active_date\n          )\n          select  sm.merchant_number                    as merchant_number,  \n                  (sum(sm.item_count) * mp.per_item)    as charge_amount,\n                  (\n                    'VISA '\n                    || upper(me.statement_msg_default)\n                    || ' ' || sum(sm.item_count)\n                    || ' @ $' \n                    || ltrim(rtrim(decode( substr(to_char(mp.per_item,'0.999'),-1),\n                                            '0',to_char(mp.per_item,'0.99'),\n                                            to_char(mp.per_item,'0.999') )\n                                  )\n                            )  \n                    || '/EA ' || to_char(trunc( :1 ,'month'),'MON yyyy')\n                  )                                     as statement_message,\n                   :2                   as billing_months,\n                  trunc( :3 ,'month')               as start_month,\n                  trunc(last_day(last_day(sysdate)+1))  as expiration_date,\n                  trunc(sysdate)                        as active_date\n          from    mif                           mf,\n                  visa_auth_misuse_summary      sm,\n                  mbs_pricing                   mp,\n                  mbs_elements                  me\n          where   mf.bank_number =  :4 \n                  and sm.merchant_number = mf.merchant_number\n                  and sm.active_date = trunc( :5 ,'month')\n                  and mp.merchant_number = sm.merchant_number\n                  and mp.item_type = decode(sm.report_identifier,'FEE100S',11,'FEE200S',10,-1)\n                  and sm.active_date between mp.valid_date_begin and mp.valid_date_end\n                  and me.item_type = mp.item_type\n          group by sm.merchant_number, mp.per_item, me.statement_msg_default";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.startup.BatchMaintExport",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setString(2,__sJT_3799);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setDate(5,beginDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:776^9*/
      }
    }
    catch(Exception e)
    {
      logEntry( "exportMonthlyChargeRecs()", e.toString() );
    }
    finally
    {
    }
  }
  
  protected String getCsvFilename( String dataFilename )
  {
    String      csvFilename     = null;
    int         offset          = 0;
    
    offset = dataFilename.lastIndexOf('.');
    if ( offset < 0 )
    {
      csvFilename  = dataFilename + ".csv";
    }
    else
    {
      csvFilename  = dataFilename.substring(0,offset) + ".csv";
    }
    return( csvFilename );
  }
  
  protected boolean monthProcessed( int bankNumber, Date endDate )
  {
    int     recCount    = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:811^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(bank_number) 
//          from    billing_auto_cg_month_end cgme
//          where   bank_number = :bankNumber and
//                  active_date = trunc(:endDate,'month')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(bank_number)  \n        from    billing_auto_cg_month_end cgme\n        where   bank_number =  :1  and\n                active_date = trunc( :2 ,'month')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.BatchMaintExport",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:817^7*/
    }
    catch(Exception e)
    {
      logEntry("fileProcessed(" + bankNumber + "," + endDate + ")", e.toString());
    }
    return( (recCount != 0) );
  }
  
  protected boolean sendFile( String dataFilename )
  {
    boolean       success         = false;
    StringBuffer  status          = new StringBuffer();
    
    try
    {    
      success = sendDataFile( dataFilename,
                    MesDefaults.getString(MesDefaults.DK_OUTGOING_CB_HOST),
                    MesDefaults.getString(MesDefaults.DK_OUTGOING_BM_USER),
                    MesDefaults.getString(MesDefaults.DK_OUTGOING_BM_PASSWORD));
                    
      if ( success )
      {
        archiveDailyFile(dataFilename);
      }                    
      
      // send a status email
      sendStatusEmail(success, dataFilename, status.toString() );
    }      
    catch( Exception e )
    {
      logEntry("sendFile(" + dataFilename + ")", e.toString());
    }
    finally
    {
    }
    return( success );
  }    
  
  private void sendReceipt( int bankNumber, String loadFilename )
  {
    MailMessage         msg         = null;
    StringBuffer        body        = new StringBuffer();
  
    try
    {
      msg = new MailMessage();

      body.setLength(0);
      body.append(bankNumber);
      body.append(" Batch Maintenance File Generated\n\n");

      msg.setAddresses(MesEmails.MSG_ADDRS_BATCH_MAINT);
      msg.setSubject( "Batch Maint File " + bankNumber);
      msg.setText( body.toString() );
      msg.addFile( getCsvFilename( loadFilename ) );
      msg.send();
      
      // remove the CSV file
      new File( getCsvFilename( loadFilename ) ).delete();
    }
    catch( Exception e )
    {
      logEntry("sendReceipt(" + bankNumber +","+ loadFilename + ")", e.toString());
    }
  }
  
  private void sendStatusEmail(boolean success, String fileName, String message)
  {
    StringBuffer  subject         = new StringBuffer("");
    StringBuffer  body            = new StringBuffer("");
    
    try
    {
      if(success)
      {
        subject.append("Outgoing BatchMaint Success");
      }
      else
      {
        subject.append("Outgoing BatchMaint Error");
        
        // make sure the filename is in 
        // the message text for error research
        body.append("Filename: ");
        body.append(fileName);
        body.append("\n\n");
      }
      body.append(message);
      
      MailMessage msg = new MailMessage();
      
      if(success)
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_OUTGOING_BM_NOTIFY);
      }
      else
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_OUTGOING_BM_FAILURE);
      }
      
      msg.setSubject(subject.toString());
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "sendStatusEmail()", e.toString());
      logEntry("sendStatusEmail()", e.toString());
    }
  }
  
  public static void main( String[] args )
  {
    BatchMaintExport      test   = null;
    

        try {
            if (args.length > 0 && args[0].equals("testproperties")) {
                EventBase.printKeyListStatus(new String[]{
                        MesDefaults.DK_OUTGOING_CB_HOST,
                        MesDefaults.DK_OUTGOING_BM_USER,
                        MesDefaults.DK_OUTGOING_BM_PASSWORD
                });
            }
        } catch (Exception e) {
            System.out.println("main(): " + e.toString());
            log.error(e.getMessage());
        }

    try
    {
      SQLJConnectionBase.initStandalone();
      
      test = new BatchMaintExport();
      
      if ( args.length > 0 ) 
      {
        test.connect();
        if ( args[0].equals("export") )
        {
          test.exportAutoBillChargeRecs( Integer.parseInt(args[1]), args[2] );
        }
        else
        {
          log.debug("Command " + args[0] + " not recognized.");
        }
      }
      else
      {
        test.execute();
      }        
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception e ) {}
    }
  }
}/*@lineinfo:generated-code*/