/*@lineinfo:filename=ComdataTranSummary*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/ComdataTranSummary.sqlj $

  Description:  
    Summarization methods for comdata daily transaction files


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SubVersion database

  Copyright (C) 2004,2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class ComdataTranSummary extends EventBase
{
  public static final int     PT_TRAN_SUMMARY                 = 1;
  public static final int     PT_VOL_SUMMARY                  = 2;
  public static final int     PT_ACCT_SUMMARY                 = 3;
  
  public ComdataTranSummary()
  {
  }
  
  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    boolean               fileProcessed     = false;
    ResultSetIterator     it                = null;
    int                   itemCount         = 0;
    String                loadFilename      = null;
    int                   procType          = -1;
    long                  recId             = 0L;
    ResultSet             resultSet         = null;
    int                   sequenceId        = 0;
    
    try
    {
      // get an automated timeout exempt connection
      connect(true);
      
      /*@lineinfo:generated-code*//*@lineinfo:68^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(load_filename)  
//          from    comdata_summary_process
//          where   process_sequence is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(load_filename)   \n        from    comdata_summary_process\n        where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.ComdataTranSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:73^7*/
      
      if ( itemCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:77^9*/

//  ************************************************************
//  #sql [Ctx] { select  comdata_summary_proc_sequence.nextval 
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  comdata_summary_proc_sequence.nextval  \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.ComdataTranSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sequenceId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:81^9*/
      
        /*@lineinfo:generated-code*//*@lineinfo:83^9*/

//  ************************************************************
//  #sql [Ctx] { update  comdata_summary_process
//            set     process_sequence = :sequenceId
//            where   process_sequence is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  comdata_summary_process\n          set     process_sequence =  :1 \n          where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.ComdataTranSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sequenceId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:88^9*/
      
        /*@lineinfo:generated-code*//*@lineinfo:90^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  rec_id                      as rec_id,
//                    load_filename               as load_filename,
//                    process_type                as process_type
//            from    comdata_summary_process
//            where   process_sequence = :sequenceId
//            order by process_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rec_id                      as rec_id,\n                  load_filename               as load_filename,\n                  process_type                as process_type\n          from    comdata_summary_process\n          where   process_sequence =  :1 \n          order by process_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.ComdataTranSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sequenceId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.ComdataTranSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:98^9*/
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          loadFilename  = resultSet.getString("load_filename");
          procType      = resultSet.getInt("process_type");
          recId         = resultSet.getLong("rec_id");
        
          recordTimestampBegin( recId );
          
          fileProcessed = true;     // default 
        
          switch( procType )
          {
            case PT_TRAN_SUMMARY:
              fileProcessed = loadTranSummary(loadFilename);
              break;
              
            case PT_VOL_SUMMARY:
              fileProcessed = loadVolSummary(loadFilename);
              break;
              
            case PT_ACCT_SUMMARY:
              fileProcessed = loadAccountSummary(loadFilename);
              break;
          
            default:      // skip
              continue;
          }
          if ( fileProcessed == true )
          {
            recordTimestampEnd( recId );
          }            
        }
        resultSet.close();
        it.close();        
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return( true );
  }
  
  protected boolean loadAccountSummary( String loadFilename )
  {
    boolean         fileProcessed   = false;
    long            loadFileId      = 0L;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:157^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init( :loadFilename )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init(  :1  )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.ComdataTranSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:160^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:162^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:loadFilename) 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1 )  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.ComdataTranSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:166^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:168^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from    comdata_detail_acct_summary   sm
//          where   sm.load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from    comdata_detail_acct_summary   sm\n        where   sm.load_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.ComdataTranSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:173^7*/
  
      /*@lineinfo:generated-code*//*@lineinfo:175^7*/

//  ************************************************************
//  #sql [Ctx] { insert into comdata_detail_acct_summary
//          (
//            location_code, 
//            chain_code, 
//            corp_code, 
//            batch_date, 
//            fleet_code, 
//            account_code,
//            account_name, 
//            data_type, 
//            tran_count, 
//            fuel_quantity, 
//            fuel_cost, 
//            oil_quantity,
//            oil_cost, 
//            cash_advance_amount, 
//            other_amount, 
//            fee_amount, 
//            pmt_amount, 
//            adj_amount, 
//            select_amount, 
//            focus_amount, 
//            load_file_id, 
//            load_filename
//          )
//          select  dt.location_code                        as location_code, 
//                  nvl(dt.chain_code,'nochain')            as chain_code,
//                  nvl(dt.corp_code,'nocorp')              as corp_code,
//                  dt.batch_date                           as batch_date,
//                  nvl(dt.company_corp_code,'nofleet')     as fleet_code,
//                  dt.customer_account_code                as account_code,                       
//                  dt.company_name                         as account_name,
//                  'ALLT'                                  as data_type,
//                  count(dt.customer_account_code)         as tran_count, 
//                  sum( nvl(dt.diesel_1_gallons,0) + 
//                       nvl(dt.diesel_2_gallons,0) + 
//                       nvl(dt.reefer_gallons,0) + 
//                       nvl(dt.other_gallons,0) 
//                     )                                    as fuel_quantity, 
//                  sum( nvl(dt.diesel_1_total_cost,0) + 
//                       nvl(dt.diesel_2_total_cost,0) + 
//                       nvl(dt.reefer_total_cost,0) + 
//                       nvl(dt.other_total_cost,0) 
//                     )                                    as fuel_cost, 
//                  sum( nvl(dt.oil_purchased,0) )          as oil_quantity, 
//                  sum( nvl(dt.oil_total_cost,0) )         as oil_cost, 
//                  sum( nvl(dt.cash_advance_amount,0) )    as cash_amount, 
//                  sum( nvl(dt.oil_total_cost,0) +
//                       nvl(dt.product_code_1_amount,0) + 
//                       nvl(dt.product_code_2_amount,0) + 
//                       nvl(dt.product_code_3_amount,0) )  as other_amount, 
//                  sum( nvl(dt.fee_amount,0) )             as fee_amount, 
//                  sum( nvl(dt.payment_amount,0) )         as pmt_amount, 
//                  sum( nvl(dt.adjustment_amount,0) )      as adj_amount,
//                  sum( decode(dt.discount_type,
//                              'S',nvl(dt.discount_amount,0),
//                              0 ) )                       as select_amount,
//                  sum( decode(dt.discount_type,                                
//                              'F',nvl(dt.discount_amount,0),
//                              0) )                        as focus_amount,
//                  :loadFileId                             as load_file_id,
//                  :loadFilename                           as load_filename
//          from    load_file_index         lfi,
//                  comdata_detail_file_dt  dt
//          where   lfi.load_filename = :loadFilename and
//                  dt.load_file_id = lfi.load_file_id and
//                  -- ignore non-account activity (i.e. fees)
//                  not dt.customer_account_code is null and 
//                  dt.data_type = 'ALLT' and
//                  not exists
//                  (
//                    select  evi.invoice_number
//                    from    comdata_excl_vol_invoice evi
//                    where   upper(evi.invoice_number) = upper(dt.invoice_number)
//                  ) and
//                  not dt.tran_type in ( 'CNL' )
//          group by  nvl(dt.corp_code,'nocorp'),
//                    nvl(dt.chain_code,'nochain'),
//                    dt.location_code,
//                    dt.batch_date,
//                    nvl(dt.company_corp_code,'nofleet'),
//                    dt.customer_account_code,
//                    dt.company_name
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into comdata_detail_acct_summary\n        (\n          location_code, \n          chain_code, \n          corp_code, \n          batch_date, \n          fleet_code, \n          account_code,\n          account_name, \n          data_type, \n          tran_count, \n          fuel_quantity, \n          fuel_cost, \n          oil_quantity,\n          oil_cost, \n          cash_advance_amount, \n          other_amount, \n          fee_amount, \n          pmt_amount, \n          adj_amount, \n          select_amount, \n          focus_amount, \n          load_file_id, \n          load_filename\n        )\n        select  dt.location_code                        as location_code, \n                nvl(dt.chain_code,'nochain')            as chain_code,\n                nvl(dt.corp_code,'nocorp')              as corp_code,\n                dt.batch_date                           as batch_date,\n                nvl(dt.company_corp_code,'nofleet')     as fleet_code,\n                dt.customer_account_code                as account_code,                       \n                dt.company_name                         as account_name,\n                'ALLT'                                  as data_type,\n                count(dt.customer_account_code)         as tran_count, \n                sum( nvl(dt.diesel_1_gallons,0) + \n                     nvl(dt.diesel_2_gallons,0) + \n                     nvl(dt.reefer_gallons,0) + \n                     nvl(dt.other_gallons,0) \n                   )                                    as fuel_quantity, \n                sum( nvl(dt.diesel_1_total_cost,0) + \n                     nvl(dt.diesel_2_total_cost,0) + \n                     nvl(dt.reefer_total_cost,0) + \n                     nvl(dt.other_total_cost,0) \n                   )                                    as fuel_cost, \n                sum( nvl(dt.oil_purchased,0) )          as oil_quantity, \n                sum( nvl(dt.oil_total_cost,0) )         as oil_cost, \n                sum( nvl(dt.cash_advance_amount,0) )    as cash_amount, \n                sum( nvl(dt.oil_total_cost,0) +\n                     nvl(dt.product_code_1_amount,0) + \n                     nvl(dt.product_code_2_amount,0) + \n                     nvl(dt.product_code_3_amount,0) )  as other_amount, \n                sum( nvl(dt.fee_amount,0) )             as fee_amount, \n                sum( nvl(dt.payment_amount,0) )         as pmt_amount, \n                sum( nvl(dt.adjustment_amount,0) )      as adj_amount,\n                sum( decode(dt.discount_type,\n                            'S',nvl(dt.discount_amount,0),\n                            0 ) )                       as select_amount,\n                sum( decode(dt.discount_type,                                \n                            'F',nvl(dt.discount_amount,0),\n                            0) )                        as focus_amount,\n                 :1                              as load_file_id,\n                 :2                            as load_filename\n        from    load_file_index         lfi,\n                comdata_detail_file_dt  dt\n        where   lfi.load_filename =  :3  and\n                dt.load_file_id = lfi.load_file_id and\n                -- ignore non-account activity (i.e. fees)\n                not dt.customer_account_code is null and \n                dt.data_type = 'ALLT' and\n                not exists\n                (\n                  select  evi.invoice_number\n                  from    comdata_excl_vol_invoice evi\n                  where   upper(evi.invoice_number) = upper(dt.invoice_number)\n                ) and\n                not dt.tran_type in ( 'CNL' )\n        group by  nvl(dt.corp_code,'nocorp'),\n                  nvl(dt.chain_code,'nochain'),\n                  dt.location_code,\n                  dt.batch_date,\n                  nvl(dt.company_corp_code,'nofleet'),\n                  dt.customer_account_code,\n                  dt.company_name";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.ComdataTranSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setString(2,loadFilename);
   __sJT_st.setString(3,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:260^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:262^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:265^7*/
      
      fileProcessed = true;
    }
    catch( Exception e )
    {
      logEntry("loadAccountSummary(" + loadFilename + ")", e.toString());
    }
    finally
    {
    }
    return( fileProcessed );
  }

  private boolean loadTranSummary( String loadFilename )
  {
    boolean         fileProcessed   = false;
    long            loadFileId      = 0L;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:286^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init( :loadFilename )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init(  :1  )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.ComdataTranSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:289^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:291^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:loadFilename) 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1 )  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.ComdataTranSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:295^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:297^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from    comdata_detail_file_summary   sm
//          where   sm.load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from    comdata_detail_file_summary   sm\n        where   sm.load_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.startup.ComdataTranSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:302^7*/
    
      /*@lineinfo:generated-code*//*@lineinfo:304^7*/

//  ************************************************************
//  #sql [Ctx] { insert into comdata_detail_file_summary sm
//          (
//            location_code,
//            chain_code,
//            corp_code,
//            batch_date,
//            batch_number,
//            data_type,  
//            diesel_1_gallons,
//            diesel_1_cost_per_gallon,
//            diesel_1_total_cost,
//            diesel_2_gallons,
//            diesel_2_cost_per_gallon,
//            diesel_2_total_cost,
//            reefer_gallons,
//            reefer_cost_per_gallon,
//            reefer_total_cost,
//            other_gallons,
//            other_cost_per_gallon,
//            other_total_cost,
//            oil_purchased,
//            oil_cost_per_quart,
//            oil_total_cost,
//            cash_advance_amount,
//            product_code_1_amount,
//            product_code_2_amount,
//            product_code_3_amount,
//            discount_amount,
//            fee_amount,
//            payment_amount,
//            adjustment_amount,
//            load_file_id,
//            load_filename
//          )
//          select  dt.location_code                  as location_code,
//                  dt.chain_code                     as chain_code,
//                  dt.corp_code                      as corp_code,
//                  dt.batch_date                     as batch_date,  
//                  dt.batch_number                   as batch_number,
//                  dt.data_type                      as data_type,
//                  sum(dt.diesel_1_gallons)          as diesel_1_gallons,
//                  avg(dt.diesel_1_cost_per_gallon)  as diesel_1_cost_per_gallon,
//                  sum(dt.diesel_1_total_cost)       as diesel_1_total_cost,
//                  sum(dt.diesel_2_gallons)          as diesel_2_gallons,
//                  avg(dt.diesel_2_cost_per_gallon)  as diesel_2_cost_per_gallon,
//                  sum(dt.diesel_2_total_cost)       as diesel_2_total_cost,
//                  sum(dt.reefer_gallons)            as reefer_gallons,
//                  avg(dt.reefer_cost_per_gallon)    as reefer_cost_per_gallon,
//                  sum(dt.reefer_total_cost)         as reefer_total_cost,
//                  sum(dt.other_gallons)             as other_gallons,
//                  avg(dt.other_cost_per_gallon)     as other_cost_per_gallon,
//                  sum(dt.other_total_cost)          as other_total_cost,
//                  sum(dt.oil_purchased)             as oil_purchased,
//                  avg(dt.oil_cost_per_quart)        as oil_cost_per_quart,
//                  sum(dt.oil_total_cost)            as oil_total_cost,
//                  sum(dt.cash_advance_amount)       as cash_advance_amount,
//                  sum(dt.product_code_1_amount)     as product_code_1_amount,
//                  sum(dt.product_code_2_amount)     as product_code_2_amount,
//                  sum(dt.product_code_3_amount)     as product_code_3_amount,
//                  sum(dt.discount_amount)           as discount_amount,
//                  sum(dt.fee_amount)                as fee_amount,
//                  sum(dt.payment_amount)            as payment_amount,
//                  sum(dt.adjustment_amount)         as adjustment_amount,
//                  :loadFileId                       as load_file_id,
//                  :loadFilename                     as load_filename
//          from    comdata_detail_file_dt  dt
//          where   dt.load_filename = :loadFilename
//          group by dt.location_code, chain_code, corp_code,
//                    dt.batch_date, dt.batch_number, dt.data_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into comdata_detail_file_summary sm\n        (\n          location_code,\n          chain_code,\n          corp_code,\n          batch_date,\n          batch_number,\n          data_type,  \n          diesel_1_gallons,\n          diesel_1_cost_per_gallon,\n          diesel_1_total_cost,\n          diesel_2_gallons,\n          diesel_2_cost_per_gallon,\n          diesel_2_total_cost,\n          reefer_gallons,\n          reefer_cost_per_gallon,\n          reefer_total_cost,\n          other_gallons,\n          other_cost_per_gallon,\n          other_total_cost,\n          oil_purchased,\n          oil_cost_per_quart,\n          oil_total_cost,\n          cash_advance_amount,\n          product_code_1_amount,\n          product_code_2_amount,\n          product_code_3_amount,\n          discount_amount,\n          fee_amount,\n          payment_amount,\n          adjustment_amount,\n          load_file_id,\n          load_filename\n        )\n        select  dt.location_code                  as location_code,\n                dt.chain_code                     as chain_code,\n                dt.corp_code                      as corp_code,\n                dt.batch_date                     as batch_date,  \n                dt.batch_number                   as batch_number,\n                dt.data_type                      as data_type,\n                sum(dt.diesel_1_gallons)          as diesel_1_gallons,\n                avg(dt.diesel_1_cost_per_gallon)  as diesel_1_cost_per_gallon,\n                sum(dt.diesel_1_total_cost)       as diesel_1_total_cost,\n                sum(dt.diesel_2_gallons)          as diesel_2_gallons,\n                avg(dt.diesel_2_cost_per_gallon)  as diesel_2_cost_per_gallon,\n                sum(dt.diesel_2_total_cost)       as diesel_2_total_cost,\n                sum(dt.reefer_gallons)            as reefer_gallons,\n                avg(dt.reefer_cost_per_gallon)    as reefer_cost_per_gallon,\n                sum(dt.reefer_total_cost)         as reefer_total_cost,\n                sum(dt.other_gallons)             as other_gallons,\n                avg(dt.other_cost_per_gallon)     as other_cost_per_gallon,\n                sum(dt.other_total_cost)          as other_total_cost,\n                sum(dt.oil_purchased)             as oil_purchased,\n                avg(dt.oil_cost_per_quart)        as oil_cost_per_quart,\n                sum(dt.oil_total_cost)            as oil_total_cost,\n                sum(dt.cash_advance_amount)       as cash_advance_amount,\n                sum(dt.product_code_1_amount)     as product_code_1_amount,\n                sum(dt.product_code_2_amount)     as product_code_2_amount,\n                sum(dt.product_code_3_amount)     as product_code_3_amount,\n                sum(dt.discount_amount)           as discount_amount,\n                sum(dt.fee_amount)                as fee_amount,\n                sum(dt.payment_amount)            as payment_amount,\n                sum(dt.adjustment_amount)         as adjustment_amount,\n                 :1                        as load_file_id,\n                 :2                      as load_filename\n        from    comdata_detail_file_dt  dt\n        where   dt.load_filename =  :3 \n        group by dt.location_code, chain_code, corp_code,\n                  dt.batch_date, dt.batch_number, dt.data_type";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.ComdataTranSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setString(2,loadFilename);
   __sJT_st.setString(3,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:375^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:377^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:380^7*/
      
      fileProcessed = true;
    }
    catch( Exception e )
    {
      logEntry("loadTranSummary()",e.toString());
    }
    finally
    {
    }
    return( fileProcessed );
  }
  
  protected boolean loadVolSummary( String loadFilename )
  {
    boolean         fileProcessed   = false;
    long            loadFileId      = 0L;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:401^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init( :loadFilename )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init(  :1  )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.ComdataTranSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:404^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:406^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:loadFilename) 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1 )  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.ComdataTranSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:410^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:412^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from    comdata_detail_vol_summary   sm
//          where   sm.load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from    comdata_detail_vol_summary   sm\n        where   sm.load_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.startup.ComdataTranSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:417^7*/
  
      /*@lineinfo:generated-code*//*@lineinfo:419^7*/

//  ************************************************************
//  #sql [Ctx] { insert into comdata_detail_vol_summary
//          (
//            location_code, 
//            chain_code, 
//            corp_code, 
//            batch_date, 
//            fleet_code, 
//            fleet_name, 
//            data_type, 
//            tran_count, 
//            fuel_quantity, 
//            fuel_cost, 
//            oil_quantity,
//            oil_cost, 
//            cash_advance_amount, 
//            other_amount, 
//            fee_amount, 
//            pmt_amount, 
//            adj_amount, 
//            select_amount, 
//            focus_amount, 
//            load_file_id, 
//            load_filename
//          )
//          select  dt.location_code                        as location_code, 
//                  nvl(dt.chain_code,'nochain')            as chain_code,
//                  nvl(dt.corp_code,'nocorp')              as corp_code,
//                  dt.batch_date                           as batch_date,
//                  nvl(dt.company_corp_code,'nofleet')     as fleet_code,
//                  decode(dt.company_corp_code,
//                         null,null,
//                         dt.company_name)                 as fleet_name,
//                  'ALLT'                                  as data_type,
//                  count(dt.customer_account_code)         as tran_count, 
//                  sum( nvl(dt.diesel_1_gallons,0) + 
//                       nvl(dt.diesel_2_gallons,0) + 
//                       nvl(dt.reefer_gallons,0) + 
//                       nvl(dt.other_gallons,0) 
//                     )                                    as fuel_quantity, 
//                  sum( nvl(dt.diesel_1_total_cost,0) + 
//                       nvl(dt.diesel_2_total_cost,0) + 
//                       nvl(dt.reefer_total_cost,0) + 
//                       nvl(dt.other_total_cost,0) 
//                     )                                    as fuel_cost, 
//                  sum( nvl(dt.oil_purchased,0) )          as oil_quantity, 
//                  sum( nvl(dt.oil_total_cost,0) )         as oil_cost, 
//                  sum( nvl(dt.cash_advance_amount,0) )    as cash_amount, 
//                  sum( nvl(dt.oil_total_cost,0) +
//                       nvl(dt.product_code_1_amount,0) + 
//                       nvl(dt.product_code_2_amount,0) + 
//                       nvl(dt.product_code_3_amount,0) )  as other_amount, 
//                  sum( nvl(dt.fee_amount,0) )             as fee_amount, 
//                  sum( nvl(dt.payment_amount,0) )         as pmt_amount, 
//                  sum( nvl(dt.adjustment_amount,0) )      as adj_amount,
//                  sum( decode(dt.discount_type,
//                              'S',nvl(dt.discount_amount,0),
//                              0 ) )                       as select_amount,
//                  sum( decode(dt.discount_type,                                
//                              'F',nvl(dt.discount_amount,0),
//                              0) )                        as focus_amount,
//                  :loadFileId                             as load_file_id,
//                  :loadFilename                           as load_filename
//          from    load_file_index         lfi,
//                  comdata_detail_file_dt  dt
//          where   lfi.load_filename = :loadFilename and
//                  dt.load_file_id = lfi.load_file_id and
//                  -- ignore non-account activity (i.e. fees)
//                  not dt.customer_account_code is null and 
//                  dt.data_type = 'ALLT' and
//                  not exists
//                  (
//                    select  evi.invoice_number
//                    from    comdata_excl_vol_invoice evi
//                    where   upper(evi.invoice_number) = upper(dt.invoice_number)
//                  ) and
//                  not dt.tran_type in ( 'CNL' )
//          group by  nvl(dt.corp_code,'nocorp'),
//                    nvl(dt.chain_code,'nochain'),
//                    dt.location_code,
//                    dt.batch_date,
//                    nvl(dt.company_corp_code,'nofleet'),
//                    decode(dt.company_corp_code,
//                           null,null,
//                           dt.company_name)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into comdata_detail_vol_summary\n        (\n          location_code, \n          chain_code, \n          corp_code, \n          batch_date, \n          fleet_code, \n          fleet_name, \n          data_type, \n          tran_count, \n          fuel_quantity, \n          fuel_cost, \n          oil_quantity,\n          oil_cost, \n          cash_advance_amount, \n          other_amount, \n          fee_amount, \n          pmt_amount, \n          adj_amount, \n          select_amount, \n          focus_amount, \n          load_file_id, \n          load_filename\n        )\n        select  dt.location_code                        as location_code, \n                nvl(dt.chain_code,'nochain')            as chain_code,\n                nvl(dt.corp_code,'nocorp')              as corp_code,\n                dt.batch_date                           as batch_date,\n                nvl(dt.company_corp_code,'nofleet')     as fleet_code,\n                decode(dt.company_corp_code,\n                       null,null,\n                       dt.company_name)                 as fleet_name,\n                'ALLT'                                  as data_type,\n                count(dt.customer_account_code)         as tran_count, \n                sum( nvl(dt.diesel_1_gallons,0) + \n                     nvl(dt.diesel_2_gallons,0) + \n                     nvl(dt.reefer_gallons,0) + \n                     nvl(dt.other_gallons,0) \n                   )                                    as fuel_quantity, \n                sum( nvl(dt.diesel_1_total_cost,0) + \n                     nvl(dt.diesel_2_total_cost,0) + \n                     nvl(dt.reefer_total_cost,0) + \n                     nvl(dt.other_total_cost,0) \n                   )                                    as fuel_cost, \n                sum( nvl(dt.oil_purchased,0) )          as oil_quantity, \n                sum( nvl(dt.oil_total_cost,0) )         as oil_cost, \n                sum( nvl(dt.cash_advance_amount,0) )    as cash_amount, \n                sum( nvl(dt.oil_total_cost,0) +\n                     nvl(dt.product_code_1_amount,0) + \n                     nvl(dt.product_code_2_amount,0) + \n                     nvl(dt.product_code_3_amount,0) )  as other_amount, \n                sum( nvl(dt.fee_amount,0) )             as fee_amount, \n                sum( nvl(dt.payment_amount,0) )         as pmt_amount, \n                sum( nvl(dt.adjustment_amount,0) )      as adj_amount,\n                sum( decode(dt.discount_type,\n                            'S',nvl(dt.discount_amount,0),\n                            0 ) )                       as select_amount,\n                sum( decode(dt.discount_type,                                \n                            'F',nvl(dt.discount_amount,0),\n                            0) )                        as focus_amount,\n                 :1                              as load_file_id,\n                 :2                            as load_filename\n        from    load_file_index         lfi,\n                comdata_detail_file_dt  dt\n        where   lfi.load_filename =  :3  and\n                dt.load_file_id = lfi.load_file_id and\n                -- ignore non-account activity (i.e. fees)\n                not dt.customer_account_code is null and \n                dt.data_type = 'ALLT' and\n                not exists\n                (\n                  select  evi.invoice_number\n                  from    comdata_excl_vol_invoice evi\n                  where   upper(evi.invoice_number) = upper(dt.invoice_number)\n                ) and\n                not dt.tran_type in ( 'CNL' )\n        group by  nvl(dt.corp_code,'nocorp'),\n                  nvl(dt.chain_code,'nochain'),\n                  dt.location_code,\n                  dt.batch_date,\n                  nvl(dt.company_corp_code,'nofleet'),\n                  decode(dt.company_corp_code,\n                         null,null,\n                         dt.company_name)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.startup.ComdataTranSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setString(2,loadFilename);
   __sJT_st.setString(3,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:505^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:507^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:510^7*/
      
      fileProcessed = true;
    }
    catch( Exception e )
    {
      logEntry("loadVolSummary(" + loadFilename + ")", e.toString());
    }
    finally
    {
    }
    return( fileProcessed );
  }

  protected void recordTimestampBegin( long recId )
  {
    Timestamp     beginTime   = null;
    
    try
    {
      beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
    
      /*@lineinfo:generated-code*//*@lineinfo:532^7*/

//  ************************************************************
//  #sql [Ctx] { update  comdata_summary_process
//          set     process_begin_date = :beginTime
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  comdata_summary_process\n        set     process_begin_date =  :1 \n        where   rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.startup.ComdataTranSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,beginTime);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:537^7*/
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "recordTimestampBegin()", e.toString());
      setError("recordTimestampBegin(): " + e.toString());
      logEntry("recordTimestampBegin()", e.toString());
    }
  }
  
  protected void recordTimestampEnd( long recId )
  {
    Timestamp     beginTime   = null;
    long          elapsed     = 0L; 
    Timestamp     endTime     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:555^7*/

//  ************************************************************
//  #sql [Ctx] { select  process_begin_date 
//          from    comdata_summary_process 
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  process_begin_date  \n        from    comdata_summary_process \n        where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.ComdataTranSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:560^7*/
      endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      elapsed = (endTime.getTime() - beginTime.getTime());
    
      /*@lineinfo:generated-code*//*@lineinfo:564^7*/

//  ************************************************************
//  #sql [Ctx] { update  comdata_summary_process
//          set     process_end_date = :endTime,
//                  process_elapsed = :DateTimeFormatter.getFormattedTimestamp(elapsed)
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4045 = DateTimeFormatter.getFormattedTimestamp(elapsed);
   String theSqlTS = "update  comdata_summary_process\n        set     process_end_date =  :1 ,\n                process_elapsed =  :2 \n        where   rec_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.startup.ComdataTranSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,endTime);
   __sJT_st.setString(2,__sJT_4045);
   __sJT_st.setLong(3,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:570^7*/
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "recordTimestampEnd()", e.toString());
      setError("recordTimestampEnd(): " + e.toString());
      logEntry("recordTimestampEnd()", e.toString());
    }
  }
  
  public static void main( String[] args )
  {
    ComdataTranSummary    test          = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      test = new ComdataTranSummary();
      test.execute();
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/