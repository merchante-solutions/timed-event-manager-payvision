/*@lineinfo:filename=MerchantRebuttal*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/queues/MerchantRebuttal.sqlj $

  Description:  convenience object to handle chargeback rebuttal processes

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2010-03-09 12:33:32 -0800 (Tue, 09 Mar 2010) $
  Version            : $Revision: 17035 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.queues;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.database.SQLJConnectionBase;
import com.mes.tools.DocumentTool;
import com.mes.tools.FileUtils;
import com.mes.user.UserBean;
import com.mes.utils.MultipartFormReader;
import sqlj.runtime.ResultSetIterator;

public class MerchantRebuttal extends SQLJConnectionBase
{
  // create class log category
  static Logger log = Logger.getLogger(MerchantRebuttal.class);

  private long cbNum          = 0L;
  private StringBuffer notes  = null;
  private String docStatus    = DOC_STATUS_COMING;
  private String status       = "";
  private Date dateInitiated  = null;
  private String fileName     = "";

  private String newNotes     = "";
  private String newStatus    = "";
  private String newDocStatus = "";


  //for doc received status
  private Date dateDocsReceived = null;

  //for queues
  private UserBean user = null;

  public static String DOC_STATUS_COMING      = "W";
  public static String DOC_STATUS_NOT_COMING  = "N";
  public static String DOC_STATUS_RECEIVED    = "R";

  public static String STATUS_ACTIVE    = "V";
  public static String STATUS_DECLINED  = "N";
  public static String STATUS_ACCEPTED  = "A";

  public static String CBNUM_TAG      = "cbNum";
  public static String DOC_TAG        = "docs";
  public static String NOTE_TAG       = "notes";
  public static String FILENAME_TAG   = "uploadFile";

  public MerchantRebuttal(long cbNum)
  {
    this(cbNum, null);
  }

  public MerchantRebuttal(long cbNum, UserBean user)
  {
    this.cbNum = cbNum;
    this.user = user;
    initialize();
  }

  private void initialize()
  {

    this.notes = new StringBuffer();

    if(exists())
    {
      refresh();
    }
  }

  private boolean exists()
  {
    int count = 0;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:120^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT count(date_initiated)
//          
//          FROM   chargeback_rebuttal
//          WHERE  cb_load_sec = :getCBNum()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_500 = getCBNum();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT count(date_initiated)\n         \n        FROM   chargeback_rebuttal\n        WHERE  cb_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.queues.MerchantRebuttal",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_500);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:126^7*/
    }
    catch (Exception e)
    {
    }
    finally
    {
      cleanUp();
    }

    if(count > 0)
    {
      return true;
    }
    return false;

  }

  private void refresh()
  {

    ResultSetIterator it = null;
    ResultSet rs = null;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:154^7*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT
//            cbr.date_initiated,
//            cbr.doc_status,
//            cbr.cbr_status,
//            nvl(cbr.notes,''),
//            cbd.date_submitted,
//            d.doc_name
//          FROM
//            chargeback_rebuttal cbr,
//            chargeback_docs cbd,
//            document d
//          WHERE
//            cbr.cb_load_sec = :getCBNum() and
//            cbd.cb_load_sec(+) = cbr.cb_load_sec and
//            d.doc_id(+) = cbd.doc_id
//          ORDER BY
//            cbd.date_submitted DESC
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_501 = getCBNum();
  try {
   String theSqlTS = "SELECT\n          cbr.date_initiated,\n          cbr.doc_status,\n          cbr.cbr_status,\n          nvl(cbr.notes,''),\n          cbd.date_submitted,\n          d.doc_name\n        FROM\n          chargeback_rebuttal cbr,\n          chargeback_docs cbd,\n          document d\n        WHERE\n          cbr.cb_load_sec =  :1  and\n          cbd.cb_load_sec(+) = cbr.cb_load_sec and\n          d.doc_id(+) = cbd.doc_id\n        ORDER BY\n          cbd.date_submitted DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.queues.MerchantRebuttal",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_501);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.queues.MerchantRebuttal",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:173^7*/

      rs = it.getResultSet();

      //just need the first version of recent info, if available
      if(rs.next())
      {
        this.dateInitiated = rs.getDate("date_initiated");
        this.docStatus = rs.getString("doc_status");
        this.status = rs.getString("cbr_status");
        this.notes.append(rs.getString("notes"));
      }

    }
    catch (Exception e)
    {
    }
    finally
    {
      try{rs.close();}catch(Exception e){}
      try{it.close();}catch(Exception e){}
      cleanUp();
    }
  }


  public Map processSubmit(HttpServletRequest request, Map map)
  throws Exception
  {
    log.debug("in processSubmit");

    if(map == null)
    {
      //build the map of elements
      map  = MultipartFormReader.read(request);
    }

    //set the elements
    this.newDocStatus  = (String)map.get(DOC_TAG);

    if(newDocStatus == null)
    {
      throw new Exception("Please select one of the documentation options below.");
    }

    this.newNotes   = (String)map.get(NOTE_TAG);


    log.debug("newDocStatus = " + newDocStatus);
    log.debug("newNotes = " + newNotes);

    //handle the rebuttal itself
    //REMOVE FOR TESTING ONLY
    try
    {
      processRebuttal(false);

      return map;
    }
    catch(Exception e)
    {
      //log actual response
      logEntry("processSubmit()", e.getMessage());

      //change the message for the user
      e = new Exception("Unable to successfully process rebuttal.");

      throw e;
    }

  }

  //PDF was turned back on via PRF 1457 on Aug 13th, 2009
  //comment added for JIRA commit only
  public void attachDocument(Map map)
  throws Exception
  {
    //processed in the processSubmit call above
    //if they've checked off the docs incoming button...
    if( DOC_STATUS_RECEIVED.equals(newDocStatus) )
    {
      try
      {
        String[] extTypes = {"tif", "tiff", "pdf"};
        //String[] extTypes = {"tif", "tiff"};

        //multipart processor assigns the filename to param 'filename'
        fileName = (String)map.get("filename");

        log.debug("Attempting to attach " + fileName);

        if(FileUtils.validateExtension(fileName, extTypes))
        {
          //multipart processor assigns the byte[] to the FILENAME_TAG param
          byte[] body     = (byte[])map.get(FILENAME_TAG);

          if (body != null)
          {

            DocumentTool dTool = new DocumentTool();

            // merch doc type == 1 , leave fileName as is
            // and leave PDF conversion false,
            dTool.attachCBDocument(getCBNum(), 1, fileName, null, body, false);
          }
          else
          {
            throw new Exception("Unable to find data for " + fileName);
          }

        }
        else
        {
          //throw an exception if it's a file type not accepted
          throw new Exception("Unable to attach file - invalid file type.");
        }
      }
      catch (Exception e)
      {
        //log it and throw it
        logEntry("attachDocument()", e.getMessage());
        throw e;
      }
    }

  }

  public void processSubmit(HttpServletRequest request)
  throws Exception
  {
    processSubmit(request, null);
  }


  public void processRebuttal() throws Exception
  {
    processRebuttal(true);
  }

  public void processRebuttal(boolean rebuild) throws Exception
  {
    if(isValid())
    {
      log.debug("isValid");
      //does this already exist?
      //if so, update it, otherwise add it
      if(exists())
      {
        updateRebuttal();
      }
      else
      {
        setStatus(STATUS_ACTIVE);
        //log.debug(toString());
        addRebuttal();
      }

      if(rebuild)
      {
        //rebuild the rebuttal
        initialize();
      }

    }
    else
    {
      // this is never really an exception situation so just ignore
      //throw new Exception("Not in a valid MCHB state.");
    }
  }


  public void moveQueue()
  {

    if(isValid())
    {
      int type = MesQueues.Q_CHARGEBACKS_MCHB;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:356^9*/

//  ************************************************************
//  #sql [Ctx] { select type 
//            from q_data
//            where item_type = :MesQueues.Q_ITEM_TYPE_CHARGEBACK
//            and id = :getCBNum()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_502 = getCBNum();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select type  \n          from q_data\n          where item_type =  :1 \n          and id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.queues.MerchantRebuttal",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_ITEM_TYPE_CHARGEBACK);
   __sJT_st.setLong(2,__sJT_502);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   type = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:362^9*/
      }
      catch(Exception e)
      {
      }
      finally
      {
        cleanUp();
      }


      //move queue/add notes -
      //destination queue is always Q_CHARGEBACKS_MERCH_REB
      //and only add the new notes, if any...
      QueueTools.moveQueueItem( getCBNum(),
                                type,
                                this.determineQueue(),
                                user,
                                getNewNotes(),
                                false);
    }
  }

  public int determineQueue()
  {

     double amt = 0.0d;
     int qType  = MesQueues.Q_CHARGEBACKS_MERCH_REB;


     try
     {
       connect();

       /*@lineinfo:generated-code*//*@lineinfo:396^8*/

//  ************************************************************
//  #sql [Ctx] { select
//            tran_amount 
//           from
//            network_chargebacks
//           where
//            cb_load_sec = :this.getCBNum()
//          };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_503 = this.getCBNum();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select\n          tran_amount  \n         from\n          network_chargebacks\n         where\n          cb_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.queues.MerchantRebuttal",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_503);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amt = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:404^8*/

       if (amt > 999.99d)
       {
         qType = MesQueues.Q_CHARGEBACKS_MERCH_REB_HIGH;
       }

     }
     catch (Exception e)
     {
       //leave as Q_CHARGEBACKS_MERCH_REB
     }
     finally
     {
       cleanUp();
       return qType;
     }
  }


  public static int determineQueue(long cbNum)
  {
    MerchantRebuttal mRub = new MerchantRebuttal(cbNum);
    return mRub.determineQueue();
  }


  public void acceptChargeback()
  throws Exception
  {
    if(isValid())
    {

      setStatus(STATUS_ACCEPTED);

      if(exists())
      {
        updateRebuttal();
      }
      else
      {
        addRebuttal();
      }
    }
    else
    {
      throw new Exception("Cannot accept chargeback.");
    }

  }


  public boolean isValid()
  {

    boolean isValid = false;

    if(cbNum==0L)
    {
      return isValid;
    }

    ResultSetIterator it = null;
    ResultSet rs = null;

    try{

      connect();

      //1. Establish that this can be processed as a Rebuttal...
      String actionCode = "";

      /*@lineinfo:generated-code*//*@lineinfo:476^7*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT
//            action_code
//          FROM
//            network_chargeback_activity
//          WHERE
//            cb_load_sec = :cbNum
//          ORDER BY action_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n          action_code\n        FROM\n          network_chargeback_activity\n        WHERE\n          cb_load_sec =  :1 \n        ORDER BY action_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.queues.MerchantRebuttal",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,cbNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.queues.MerchantRebuttal",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:485^7*/

      rs = it.getResultSet();
      if(rs.next())
      {
        actionCode = rs.getString("action_code");
      }

      //...must be in MCHB stage (= 'D' or 'B')
      if (actionCode.charAt(0)=='D' ||
          actionCode.charAt(0)=='B'
          )
      {
        isValid = true;
      }
    }
    catch (Exception e)
    {
    }
    finally
    {
      try{rs.close();}catch(Exception e){}
      try{it.close();}catch(Exception e){}
      cleanUp();
    }

    return isValid;
  }

  private void addRebuttal()
  throws Exception
  {

    String _notes   = getNotes();

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:524^7*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO chargeback_rebuttal
//            (
//            cb_load_sec,
//            date_initiated,
//            doc_status,
//            cbr_status,
//            notes
//            )
//          VALUES
//            (
//            :this.getCBNum(),
//            sysdate,
//            :this.getDocStatus(),
//            :this.getStatus(),
//            :_notes
//            )
//          };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_504 = this.getCBNum();
 String __sJT_505 = this.getDocStatus();
 String __sJT_506 = this.getStatus();
   String theSqlTS = "INSERT INTO chargeback_rebuttal\n          (\n          cb_load_sec,\n          date_initiated,\n          doc_status,\n          cbr_status,\n          notes\n          )\n        VALUES\n          (\n           :1 ,\n          sysdate,\n           :2 ,\n           :3 ,\n           :4 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.queues.MerchantRebuttal",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_504);
   __sJT_st.setString(2,__sJT_505);
   __sJT_st.setString(3,__sJT_506);
   __sJT_st.setString(4,_notes);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:542^8*/
     }
     catch(SQLException sqle)
     {
       updateRebuttal();
     }
     finally
     {
       cleanUp();
     }

  }

  private void updateRebuttal()
  throws Exception
  {

    String _notes = getNotes();

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:565^7*/

//  ************************************************************
//  #sql [Ctx] { UPDATE
//            chargeback_rebuttal
//          SET
//            doc_status = :this.getDocStatus(),
//            notes = :_notes,
//            cbr_status = :this.getStatus()
//          WHERE
//            cb_load_sec = :this.getCBNum()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_507 = this.getDocStatus();
 String __sJT_508 = this.getStatus();
 long __sJT_509 = this.getCBNum();
  try {
   String theSqlTS = "UPDATE\n          chargeback_rebuttal\n        SET\n          doc_status =  :1 ,\n          notes =  :2 ,\n          cbr_status =  :3 \n        WHERE\n          cb_load_sec =  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.queues.MerchantRebuttal",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_507);
   __sJT_st.setString(2,_notes);
   __sJT_st.setString(3,__sJT_508);
   __sJT_st.setLong(4,__sJT_509);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:575^7*/
    }
    catch(Exception e)
    {
      throw e;
    }
    finally
    {
      cleanUp();
    }
  }

  public long getCBNum()
  {
    return cbNum;
  }

  public String getDocStatus()
  {
    if(newDocStatus.length() > 0)
    {
      return newDocStatus;
    }
    return docStatus;
  }

  public void setDocStatus(String docStatus)
  {
    this.docStatus = docStatus;
  }

  public void setStatus(String status)
  {
    this.status = status;
  }

  public String getStatus()
  {
    if(newStatus.length() > 0)
    {
      return newStatus;
    }
    return status;
  }

  public Date getDateDocsReceived()
  {
    return dateDocsReceived;
  }

  public Date getDateInitiated()
  {
    return dateInitiated;
  }

  public String getNotes()
  {

    //check notes
    rebuildNotes();

    if(notes.toString().length() > 0 && newNotes.length()>0 )
    {
      //this one exists, so ensure that the new notes (if any)
      //are added after the existing notes.
      notes.append("\n\n");
    }

    notes.append(newNotes);

    return notes.toString();

  }

  private void rebuildNotes()
  {

    String _notes = "";

    try
    {

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:659^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT
//            nvl(cbr.notes,'')
//          
//          FROM
//            chargeback_rebuttal cbr
//          WHERE
//            cbr.cb_load_sec = :getCBNum()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_510 = getCBNum();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT\n          nvl(cbr.notes,'')\n         \n        FROM\n          chargeback_rebuttal cbr\n        WHERE\n          cbr.cb_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.queues.MerchantRebuttal",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_510);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   _notes = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:669^7*/

    }
    catch (Exception e)
    {
    }
    finally
    {
      cleanUp();
    }

    notes.setLength(0);
    notes.append(_notes);

  }

  public String getNewNotes()
  {
    return newNotes;
  }

  public boolean isActive()
  {
    return (  getStatus()!=null &&
              getStatus().equals(STATUS_ACTIVE));
  }

  public String getFileName()
  {
    return fileName;
  }

  public String toString()
  {
    StringBuffer temp = new StringBuffer("\n");
    temp.append("isActive(): ").append(isActive()).append("\n");
    temp.append("cbNum: ").append(getCBNum()).append("\n");
    temp.append("docStatus: ").append(getDocStatus()).append("\n");
    temp.append("status: ").append(getStatus()).append("\n");
    temp.append("dateInitiated: ").append(getDateInitiated()).append("\n");
    temp.append("newNotes: ").append(getNewNotes()).append("\n");
    temp.append("notes: ").append(getNotes()).append("\n");
    return temp.toString();
  }

}/*@lineinfo:generated-code*/