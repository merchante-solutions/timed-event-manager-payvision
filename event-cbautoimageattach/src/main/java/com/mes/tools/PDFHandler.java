/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/PDFHandler.java $

  Description:  PDFHandler.java

  Designed to handle the conversion of various file types into PDF documents
  NOTE: this class uses the latest version of iText open source PDF generation
  itext-paulo-132.jar and is, therefore, under restrictions as provided by its
  authors, Bruno Lowagie and Paulo Soares. More information can be found at
  www.lowagie.com/iText/ and http://itextpdf.sourceforge.net/. Of course, PDF is
  a format created by Adobe... so all rights covered on that front too.

  //Copyright 2001 by Paulo Soares (iText)

  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 10/13/04 10:54a $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
// log4j
import org.apache.log4j.Category;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.codec.TiffImage;
import com.mes.constants.mesConstants;

public class PDFHandler
{

  // create class log category
  static Category log = Category.getInstance(PDFHandler.class.getName());

  public static void convertToPDF(String incomingFile, OutputStream oStream)
  throws Exception
  {
    //get type of file top convert
    int fileType = FileUtils.getFileType(incomingFile);
    if(fileType==0)
    {
      throw new Exception("Invalid file type.");
    }
    else
    {
      //generate byte[] from file
      File pdfFile = new File(incomingFile);
      byte[] fileBytes = FileUtils.getBytesFromFile(pdfFile);

      //convert doc
      convertToPDF(fileType, fileBytes, oStream);
    }
  }

  /**
   * convertToPDF - accepts any fully realized file name and
   * attempts to convert that file into a PDF, which it relays into
   * the provided OutputStream
   * @param int - type of file to be converted
   * @param String - name of file to be converted - must be fully qualified
   * @param OutputStream - allows calling object to handle data as needed
   * @returns boolean - indicates success of translation
   **/
  public static void convertToPDF(int fileType, byte[] incomingFile, OutputStream oStream)
  throws Exception
  {

    try
    {
      //switch on int
      switch(fileType)
      {

        case mesConstants.FILETYPE_PDF:
          //hmmm.. what to do if already a PDF?
          pdfToPdf(incomingFile, oStream);
          break;

        case mesConstants.FILETYPE_TIF:
          tifToPdf(incomingFile, oStream);
          break;

        case mesConstants.FILETYPE_DOC:
        case mesConstants.FILETYPE_XLS:
          //these MS-based files can't be converted using iText...
          throw new Exception("MS-based docs cannot be converted to PDF at this time.");

        case mesConstants.FILETYPE_TXT:
        case mesConstants.FILETYPE_CSV:
        case mesConstants.FILETYPE_XML:
        case mesConstants.FILETYPE_GIF:
        case mesConstants.FILETYPE_JPG:
        case mesConstants.FILETYPE_BMP:
          //not implemented yet (ever?)
          //probably just want to BLOB these directly anyway...
          throw new Exception("Current File Format is not supported.");

        default:
          //do nothing - not recognized

      }//switch
    }
    catch(Exception e)
    {
      //TODO
      log.debug(""+e.getMessage());
      throw e;
    }
  }

  /**
   * tifToPdf - use iText experimental to read TIF file and convert to PDF
   * @param byte[] - byte body from which image will be created
   * @param OutputStream - allows calling object to handle data as needed
   **/
  public static void tifToPdf(byte[] tifImage, OutputStream oStream)
  throws Exception{

    com.lowagie.text.Document document = new com.lowagie.text.Document(PageSize.A4, 25, 25, 25, 25);
    RandomAccessFileOrArray ra = null;
    try
    {
      PdfWriter writer = PdfWriter.getInstance(document, oStream);
      int pages = 0;
      document.open();
      PdfContentByte cb = writer.getDirectContent();
      int comp = 0;

      try
      {
        ra = new RandomAccessFileOrArray(tifImage);
        comp = TiffImage.getNumberOfPages(ra);
      }
      catch (Exception e)
      {
        log.debug("Exception in tifToPdf " + e.getMessage());
        throw e;
      }

      float docInnerWidth = document.getPageSize().width() - (document.leftMargin()+document.rightMargin()) ;
      float docInnerHeight = document.getPageSize().height() - (document.topMargin()+document.bottomMargin());
      float scaleWidth = 100;
      float scaleHeight = 100;

      Image img = null;

      for (int c = 0; c < comp; ++c)
      {
          try
          {

            try
            {
              img = TiffImage.getTiffImage(ra, c + 1);
            }
            catch(Exception e)
            {
              log.debug("Exception tifToPdf: page " + (c + 1) + " " + e.getMessage());
              log.debug("Trying to flip compression...");
              throw e;
              /*
              try
              {
                //TODO
              }
              catch(Exception e2)
              {
                log.debug("Compression flip didn't work...");
                log.debug("Exception tifToPdf: page " + (c + 1) + " " + e2.getMessage());
              }
              */
            }

            if (img != null)
            {
                log.debug("OK Image: page " + (c + 1));

                if (img.scaledWidth() > docInnerWidth)
                {
                  //reduce width
                  scaleWidth = (float)(docInnerWidth/img.scaledWidth()*100);
                }

                if( img.scaledHeight() > docInnerHeight)
                {
                  //reduce height
                  scaleHeight = (float)(docInnerHeight/img.scaledHeight()*100);
                }

                img.scalePercent(scaleWidth,scaleHeight);
                //have to set position
                img.setAbsolutePosition(20, 20);
                cb.addImage(img);
                document.newPage();
                ++pages;
            }

          }
          catch (Exception e)
          {
              throw new Exception("corrupt TIF image type... please try conversion technique.");
          }
      }

      if (pages == 0)
      {
        //document.add(new Paragraph("No pages."));
        throw new Exception("Unable to convert image.");
      }

    }
    catch(Exception e)
    {
      log.debug("tifToPdf Exception = "+ e.getMessage());
      throw e;
    }
    finally
    {
      try
      {
        ra.close();
      }
      catch(IOException e){}

      document.close();
    }

  }


  /**
   * pdfToPdf - pretty much assumes that the calling object doesn't realize
   * it already has a PDF... so we basically just break it down and give it back
   * @param byte[] - file body in byte[]
   * @param OutputStream - allows calling object to handle data as needed
   **/
  public static void pdfToPdf(byte[] pdfFileName, OutputStream oStream)
  throws Exception{

    try{

      oStream.write(pdfFileName,0,pdfFileName.length);

    }
    catch (Exception e)
    {
      log.debug("pdfToPdf Exception: "+e.getMessage());
      throw e;
    }

  }

}

