/*@lineinfo:filename=DocumentTool*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/DocumentTool.sqlj $

  Description:  DocumentTool.sqlj

  Designed for the manipulation of documents within the parameters set forward
  by MES. Not really a true Handler, but more of a MES_defined set of tools to gather
  and build documents using PDFHandler, BlobHandler etc.


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-03-22 17:05:24 -0700 (Tue, 22 Mar 2011) $
  Version            : $Revision: 18599 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.io.ByteArrayOutputStream;
import java.sql.ResultSet;
import java.util.ArrayList;
// log4j
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class DocumentTool extends SQLJConnectionBase
{

  // create class log category
  static Logger log = Logger.getLogger(DocumentTool.class);

  public class _DocData extends DocData
  {

    public String submitDate;
    public String type;

    public _DocData()
    {
     super(0L,"","","");
     this.submitDate  = "";
     this.type        = "";
    }

    public _DocData (long id, String ext, String name, String mimeType, String submitDate, String type)
    {
     super(id, ext, name, mimeType);
     this.submitDate  = submitDate;
     this.type        = type;
    }

    public _DocData (long id, String ext, String name, String mimeType)
    {
      super(id, ext, name, mimeType);
      this.submitDate  = "";
      this.type        = "";
    }

    public byte[] getBody()
    {
      return null;
    }
  }

  //touching this for Bug-1116
  public static ArrayList getCurrentDocs(long cbNum)
  {
    DocumentTool tool = new DocumentTool();
    return tool.buildSimpleDocList(cbNum);
  }

  public ArrayList buildSimpleDocList(long refId)
  {

    ArrayList list = null;
    ResultSetIterator it = null;
    ResultSet rs = null;

    try
    {

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:104^7*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT
//            d.doc_id
//            ,dt.extension
//            ,dt.mimetype
//            ,nvl(to_char(cbd.date_submitted,'Mon dd, yyyy'),'')          as submitDate
//            ,d.doc_name                                                  as name
//            ,decode(cbd.cb_doc_type,1,'Merchant',2,'Issuer','Undefined') as type
//          FROM
//            chargeback_docs cbd
//            ,document_type dt
//            ,document d
//          WHERE
//            cbd.cb_load_sec = :refId
//          AND
//            cbd.doc_id = d.doc_id
//          AND
//            d.doc_type_id = dt.doc_type_id(+)
//          AND
//            d.can_remove is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n          d.doc_id\n          ,dt.extension\n          ,dt.mimetype\n          ,nvl(to_char(cbd.date_submitted,'Mon dd, yyyy'),'')          as submitDate\n          ,d.doc_name                                                  as name\n          ,decode(cbd.cb_doc_type,1,'Merchant',2,'Issuer','Undefined') as type\n        FROM\n          chargeback_docs cbd\n          ,document_type dt\n          ,document d\n        WHERE\n          cbd.cb_load_sec =  :1 \n        AND\n          cbd.doc_id = d.doc_id\n        AND\n          d.doc_type_id = dt.doc_type_id(+)\n        AND\n          d.can_remove is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.DocumentTool",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,refId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.DocumentTool",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:125^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        if (list == null)
        {
          list  = new ArrayList();
        }

        list.add(new _DocData( rs.getLong("doc_id"),
                                  rs.getString("extension"),
                                  rs.getString("name"),
                                  rs.getString("mimetype"),
                                  rs.getString("submitDate"),
                                  rs.getString("type")
                                ));

      }
    }
    catch (Exception e)
    {
      log.debug("buildDocumentList() EXCEPTION: "+e.getMessage());
    }
    finally
    {
      cleanUp();
    }

    return list;
  }

  public void attachCBDocument( long cbId,
                                int cbDocType,
                                String fileName,
                                String newFileName,
                                byte[] body)
  throws Exception
  {
    attachCBDocument(cbId,cbDocType,fileName,newFileName,body,true);
  }

  public void attachCBDocument( long cbId,
                                int cbDocType,
                                String fileName,
                                String newFileName,
                                byte[] body,
                                boolean convertToPDF)
  throws Exception
  {

    //get the reference name to save
    boolean newDocNameExists = true;

    if( newFileName==null || newFileName.trim().equals(""))
    {
      newDocNameExists = false;
    }

    //get name for DB - must ensure that if new name is given without
    //an extenstion, we will match it to the original filename
    String saveName = (
      newDocNameExists?
      FileUtils.revertExtension(newFileName,fileName):
      FileUtils.getNameFromFullPath(fileName)
    );

    //after all that, ensure that this is an accepted format
    if(!FileUtils.validateExtension(saveName))
    {
      throw new Exception("Unable to validate file type chosen");
    }

    int fileType = FileUtils.getFileType(fileName);


    if(convertToPDF)
    {
      //convert bytearray into PDF - if this fails
      //we'll save it as is
      try
      {
        if( fileType == mesConstants.FILETYPE_TIF)
        {
          //create outputstream to handle conversion
          ByteArrayOutputStream baos = new ByteArrayOutputStream();

          //convert - if it doesn't work it'll throw an exception
          PDFHandler.convertToPDF(fileType, body, baos);

          //change extension
          saveName = FileUtils.removeExtension(saveName)+".pdf";

          //convert the stream back to the byte array
          body = baos.toByteArray();

        }
      }
      catch(Exception e)
      {
        log.warn("Unable convert TIF to PDF.");
      }
    }

    //encrypt the byte[], add enc extension
    //EncryptionClient client = new masthead.encrypt.EncryptionClient("10.1.4.12", 9000);
    //body = client.encrypt(body);
    //saveName = saveName + ".enc";

    //insert as BLOB in db
    BlobHandler bh = new BlobHandler();

    if( bh.insert(body, saveName) )
    {
      //file was successfully saved into DB
      //now map to cbId
      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:246^9*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO chargeback_docs
//              (cb_load_sec
//              ,doc_id
//              ,date_submitted
//              ,cb_doc_type)
//            VALUES
//              (:cbId
//              ,:bh.getGeneratedId()
//              ,sysdate
//              ,:cbDocType)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_63 = bh.getGeneratedId();
   String theSqlTS = "INSERT INTO chargeback_docs\n            (cb_load_sec\n            ,doc_id\n            ,date_submitted\n            ,cb_doc_type)\n          VALUES\n            ( :1 \n            , :2 \n            ,sysdate\n            , :3 )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.tools.DocumentTool",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,cbId);
   __sJT_st.setLong(2,__sJT_63);
   __sJT_st.setInt(3,cbDocType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:258^9*/

      }
      catch (Exception e)
      {
        log.debug("Unable to map docId to cbId.");
        throw new Exception("Unable to map document ID to control number.");
      }
      finally
      {
        cleanUp();
      }
    }
    else
    {
      throw new Exception("Unable to insert document into the database");
    }
  }


  protected void deleteDocument(long docId)
  {
    boolean closeAfter = false;
    try
    {
      if(isConnectionStale())
      {
        connect();
        closeAfter = true;
      }

     //don't actually delete for now - just flag to
     //indicate that removal is possible
      /*@lineinfo:generated-code*//*@lineinfo:291^7*/

//  ************************************************************
//  #sql [Ctx] { UPDATE
//            document
//          SET
//            can_remove = 'y'
//          WHERE
//            doc_id = :docId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "UPDATE\n          document\n        SET\n          can_remove = 'y'\n        WHERE\n          doc_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.DocumentTool",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,docId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:299^7*/
    }
    catch (Exception e)
    {
      e.printStackTrace();
      log.debug("Unable to delete document");
    }
    finally
    {
      if(closeAfter)
        cleanUp();
    }
  }

  public boolean removeCBDocument(long refId, long docId)
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:319^7*/

//  ************************************************************
//  #sql [Ctx] { DELETE FROM
//            chargeback_docs
//          WHERE
//            cb_load_sec = :refId
//          AND
//            doc_id = :docId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "DELETE FROM\n          chargeback_docs\n        WHERE\n          cb_load_sec =  :1 \n        AND\n          doc_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.tools.DocumentTool",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,refId);
   __sJT_st.setLong(2,docId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:327^7*/

      //if multiple mappings to same doc,
      //we'll want to query other tables etc.
      //before deleting...TBD

      deleteDocument(docId);

      return true;
    }
    catch (Exception e)
    {
      log.debug("Unable to remove document mapping");
      return false;
    }
    finally
    {
      cleanUp();
    }
  }



}/*@lineinfo:generated-code*/