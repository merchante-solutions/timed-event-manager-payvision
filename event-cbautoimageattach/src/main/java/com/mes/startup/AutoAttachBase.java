/*@lineinfo:filename=AutoAttachBase*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.io.ByteArrayOutputStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
// log4j
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.net.MailMessage;
import com.mes.support.StringUtilities;
import com.mes.tools.DocumentTool;
import com.mes.tools.FileUtils;
import sqlj.runtime.ResultSetIterator;

public abstract class AutoAttachBase extends EventBase
{
  // create class log category
  static Logger log = Logger.getLogger(AutoAttachBase.class);

  protected ArrayList sftpList    = null;

  protected Sftp      SftpSec     = null;
  protected SFTPObj   SftpObj     = null;
  
  //all values below are defaults -
  //values will be retrieved from properties file
  protected String FTPHOST          = "shadowcat";
  protected String FTPUSER          = "mesdom\\mallorygold";
  protected String FTPPASSWORD      = "38ddN&ty";
  protected String IMAGE_DIRECTORY  = "vautoupload";
  protected String ADDRESS_REF      = "-1";

  protected String MERCHANT         = "1";
  protected String ISSUER           = "2";
  //default here is 1 for merchant...
  //2 would indicate issuer docs
  protected String SOURCE           = MERCHANT;

  protected String REGEX            = ",";

  protected ArrayList fileList    = null;
  protected ArrayList errorList   = null;
  protected ArrayList successList = null;
  protected String msgSubject     = "";

  public AutoAttachBase()
  {
    try
    {

      readProps();

      buildSFTPList();

    }
    catch(Exception e)
    {
      //logEntry("imageattach.properties", e.toString());
    }

  }


  /**
   * process()
   * main method whereby each file (image) is attached
   * and rebuttals are created/items moved in queue where
   * applicable
   **/
  protected abstract void process() throws Exception;
  protected abstract void readProps() throws Exception;
  
  private void buildSFTPList()
  {
    String[] hosts  = FTPHOST.split(REGEX);
    String[] users  = FTPUSER.split(REGEX);
    String[] pWords = FTPPASSWORD.split(REGEX);
    String[] dirs   = IMAGE_DIRECTORY.split(REGEX);
    String[] addys  = ADDRESS_REF.split(REGEX);

    try
    {
      for (int i = 0; i < hosts.length; i++)
      {
        if (sftpList == null)
        {
          sftpList = new ArrayList();
        }
        //build list of SFTP Objects that will control the
        //original process
        try
        {
          sftpList.add( new SFTPObj( hosts[i],
                                     users[i],
                                     pWords[i],
                                     dirs[i],
                                     addys[i] )
                      );
        }
        catch(Exception e)
        {
          logEntry("buildSFTPList::entry " + i, e.toString());
          continue;
        }
      }
    }
    catch (Exception e1)
    {
      logEntry("buildSFTPList::NO HOSTS FOUND ", e1.toString());
    }
  }
  

 /**
   * METHOD execute()
   *
   * generates/processes auto image attachment for queue items
   * that have been rebutted - and moves item to proper queue
   */
  public boolean execute()
  {
    boolean result = false;

    //works through list of items, each FTPObj representing a separate
    //image search and attach process
    for (int i = 0; sftpList != null && i < sftpList.size(); i++ )
    {
      //use class level object here
      SftpObj = (SFTPObj)sftpList.get(i);

      //build new lists for each entity
      fileList    = new ArrayList();
      errorList   = new ArrayList();
      successList = new ArrayList();

      log.debug(SftpObj.toString());

      try
      {
        //via SFTP
        initSFTP();

        //build file list
        if( getFileNames() )
        {
          //only process if images exist
          process();
        }

        //drop SFTP
        closeSFTP();

        result = true;
      }
      catch(Exception e)
      {
        logEntry("execute()", e.toString());
      }
      finally
      {
        try
        {
          sendMessage();
        }
        catch(Exception e)
        {
          logEntry("sendErrors()", e.toString());
        }
      }
    }

    return result;
  }

  /**
   * getFileNames() - complies the remote list of files.
   **/
  private boolean getFileNames() throws Exception
  {
    Vector        filenames   = new Vector();
    String        filename    = null;
    FileObj       obj         = null;
    ArrayList     refList     = null;
    boolean       result      = false;

    log.debug("START getFileNames()");

    try
    {
      // get directory listing
      EnumerationIterator enumi = new EnumerationIterator();
      Iterator it = enumi.iterator(SftpSec.getNameListing());
    
      while( it.hasNext() )
      {
        filename  = (String)it.next();
        filenames.addElement(filename);
        log.debug("Adding to filename list..." + filename);
      }
    }
    catch( Exception ftpe )
    {
      log.error("get directory listing " + ftpe.toString());
    }

    for( int i = 0; i < filenames.size(); ++i )
    {
      filename = (String)filenames.elementAt(i);
      if ( !isFileTypeSupported(filename) )
      {
        continue;   // skip directories and non-image files
      }

      obj = new FileObj(filename);

      log.debug("File:: "+ obj.getFilenameBase());

      if(obj.isValid())
      {
        fileList.add(obj);
      }
      else
      {
        if(obj.otherRef != null)
        {
          //lazy instantiation
          if(refList==null)
          {
            refList = new ArrayList();
          }

          refList.add(obj);
        }
        else
        {
          log.debug("Adding to error list...");
          errorList.add(obj);
        }
      }
      //counter++;
    }

    if(refList!=null)
    {
      processRefList(refList);
    }

    if(fileList!=null && fileList.size() > 0)
    {
      result = true;
    }

    return result;
  }

  /**
   * processRefList()
   * trys to calibrate images named with the 23 numbered ARN
   * to its corresponding control number
   **/
  private void processRefList(ArrayList refList)
  {

    log.debug("START processRefList()");

    ResultSetIterator rsItr = null;
    ResultSet rs = null;
    FileObj obj = null;

    try
    {

      connect();

      int result;

      for(int i=0;i<refList.size();i++)
      {
        log.debug("processing #:"+i);


        obj = (FileObj)refList.get(i);

        /*@lineinfo:generated-code*//*@lineinfo:301^9*/

//  ************************************************************
//  #sql [Ctx] rsItr = { select cb_load_sec as controlNum
//            from network_chargebacks
//            where reference_number = :obj.otherRef
//            order by cb_load_sec desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select cb_load_sec as controlNum\n          from network_chargebacks\n          where reference_number =  :1 \n          order by cb_load_sec desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.AutoAttachBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,obj.otherRef);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.AutoAttachBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:307^9*/

        rs = rsItr.getResultSet();

        //get first control number
        if(rs.next())
        {
          obj.controlNum = rs.getLong("controlNum");
          obj.setValid(true);
          fileList.add(obj);
        }
        else
        {
          obj.message = "Unable to find ref # in chargeback file.";
          errorList.add(obj);
        }

        rs.close();
        rsItr.close();
      }

    }
    catch(Exception e)
    {
      //what happens here?
      //leave unhandled files until the next go around?
    }
    finally
    {
      try
      {
        cleanUp();
      }
      catch(Exception e)
      {}
    }
    log.debug("END processRefList()");
  }


  /**
   * attachFile()
   * attaches image to corresponding chargeback item
   * and removes from host FTP when successful
   **/
  protected void attachFile (FileObj obj, String prefix)
  throws Exception
  {
    byte[] b = null;
    try
    {
      //attach image
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      SftpSec.download(baos,obj.fileName);
      b = baos.toByteArray();
      
      DocumentTool docTool = new DocumentTool();
      
      // TODO: fix this hack, add an indicator to the 5 parm set in the props file
      boolean convertToPDF = SftpObj.getHost().equals("ca2pldr1")?false:true;

      docTool.attachCBDocument( obj.controlNum,
                                obj.imageOrigin,
                                obj.fileName,
                                prefix + obj.getFilenameBase(),
                                b,
                                convertToPDF);

      removeHostFile(obj);

      obj.setAttachedFlag(true);
    }
    catch(Exception e)
    {
      throw e;
    }
    finally
    {
      if(b!=null)
      {
        b = null;
      }
    }

  }

  private boolean isFileTypeSupported( String fileName )
  {
    int       fileType  = -1;
    boolean   retVal    = false;

    if ( fileName != null )
    {
      switch( FileUtils.getFileType(fileName) )
      {
        case mesConstants.FILETYPE_TIF:
        case mesConstants.FILETYPE_PDF:
        //case mesConstants.FILETYPE_DOC:
        //case mesConstants.FILETYPE_XLS:
        //case mesConstants.FILETYPE_TXT:
        //case mesConstants.FILETYPE_CSV:
        //case mesConstants.FILETYPE_XML:
        //case mesConstants.FILETYPE_TIF:
        //case mesConstants.FILETYPE_GIF:
        //case mesConstants.FILETYPE_JPG:
        //case mesConstants.FILETYPE_BMP:
          retVal = true;
          break;
      }
    }
    return( retVal );
  }

  /**
   * removeHostFile()
   * cleans up the remote FTP host, so files are duplicated
   * (attached more than once)
   **/
  private void removeHostFile (FileObj obj)
  throws Exception
  {
    log.debug("START removeHostFile()...");

    try
    {
      SftpSec.deleteFile(obj.fileName);
    }
    catch (Exception e)
    {
      // timed event log entry will cause email to go to sys admin
      logEntry("removeHostFile(" + String.valueOf(obj.fileName) + ")",e.toString());
      log.error("Exception removeHostFile(): "+ e.getMessage());
    }

    log.debug("END removeHostFile()...");

  }


  private void sendMessage()
  throws Exception
  {
    if(SftpObj.addressInt > 0)
    {
      StringBuffer message  = new StringBuffer("");

      //generate error message
      message.append("The following control numbers had problems processing:\n\n");
      generateMessage(errorList, message);

      //generate spacer
      message.append("\n");
      message.append( StringUtilities.leftJustify("", 80,'=') );
      message.append("\n");

      //generate success message
      message.append("The following control numbers were successfully processed:\n\n");
      generateMessage(successList, message);

      //email message
      MailMessage msg = new MailMessage();
      msg.setAddresses(SftpObj.addressInt);
      msg.setSubject("Auto Image Attach Notification");
      msg.setText(message.toString());
      msg.send();
    }

  }

  private void generateMessage(ArrayList list, StringBuffer message)
  {
    if( list != null && list.size() > 0 )
    {

      message.append( StringUtilities.leftJustify("Ctl #", 10,' ') );
      message.append( StringUtilities.leftJustify("Filename", 16,' ') );
      message.append( StringUtilities.leftJustify("Image", 8,' ') );
      message.append( StringUtilities.leftJustify("Status", 46,' ') );
      message.append("\n");

      message.append( StringUtilities.leftJustify("", 80,'=') );
      message.append("\n");


      FileObj obj;
      for(int i = 0; i< list.size();i++)
      {
        obj = (FileObj)list.get(i);

        message.append( StringUtilities.leftJustify(obj.getCNString(),10,' ') );
        message.append( StringUtilities.leftJustify(obj.fileName, 16, ' ') );
        message.append( StringUtilities.leftJustify((obj.isAttached() ? "Loaded" : "Failed"), 8, ' ') );
        message.append( StringUtilities.leftJustify(obj.message,46,' ') );
        message.append("\n");
      }
    }
    else
    {
      message.append( StringUtilities.leftJustify("No files found", 80,' ') );
      message.append("\n");
    }
  }


  /**
   * general FTP commands
   **/
  protected void initSFTP()
  {
    try
    {
      log.debug("Connecting to " + SftpObj.getHost() + " as " + SftpObj.getUser());
      SftpSec = getSftp( SftpObj.getHost(),         // host
                         SftpObj.getUser(),         // user
                         SftpObj.getPassword(),     // password
                         SftpObj.getPath(),         // path
                         true                       // binary
                       );
    }
    catch(Exception e)
    {
      log.error("initSFTP(): "+ e.getMessage());
    }
  }

  protected void closeSFTP()
  {
    if( SftpSec != null )
    {
      try
      {
        SftpSec.disconnect();
      }
      catch(Exception e)
      {
      }
      finally
      {
        SftpSec = null;
      }
    }
  }
  
  protected void refreshSFTP()
  throws Exception
  {
    closeSFTP();
    initSFTP();
  }

  public static boolean isNumeric(String s)
  {
    boolean result = true;
    try
    {
      s.trim();
      char[] chars = s.toCharArray();
      for(int i=0;i<chars.length;i++)
      {
        if((chars[i]>=48 && chars[i]<=57))
        {
         continue;
        }
        else
        {
         result = false;
         break;
        }
      }
    }
    catch(Exception e)
    {
      result = false;
    }

    return result;
  }


  protected class FileObj
  {

    private int         REF_NUM_LENGTH    = 23;
    private int         TYPE_UNDEF        = 0;

    protected String    fileName          = null;
    protected String    message           = null;
    //Merchant = 1
    //Issuer = 2
    protected int       imageOrigin       = 1;
    private boolean     attachedFlag      = false;
    protected long      controlNum;
    private String      otherRef          = null;
    private boolean     isValidObj        = true;
    protected int       iType             = TYPE_UNDEF;



    public FileObj (String fileName)
    {
      if( fileName == null )
      {
        fileName = "";
      }

      this.fileName = fileName.trim();

      setControlNum();

      //if this is an issuer doc, change the origin
      if(fileName.charAt(0) == 'i')
      {
        imageOrigin = 2;
      }
    }

    public long getControlNum()
    {
      return controlNum;
    }

    public String getCNString()
    {
      return "" + controlNum;
    }

    public String getFilenameBase()
    {
      return( FileUtils.removeExtension( this.fileName ) );
    }

    public int getImageOrigin()
    {
      return imageOrigin;
    }

    public void setAttachedFlag( boolean attached )
    {
      this.attachedFlag = attached;
    }

    public int getType()
    {
      return iType;
    }

    public boolean isType(int type)
    {
      if (this.iType == type)
      {
        return true;
      }

      return false;
    }

    private void setControlNum()
    {
      String cn     = null;
      int    idx    = 0;
      int    start  = 0;

      /*
        check for prefix
        i = issuer doc
        r and c = used by Verifi
        if found, increase  start by 1
      */
      if( fileName.charAt(0) == 'c' ||
          fileName.charAt(0) == 'r' ||
          fileName.charAt(0) == 'i'
        )
      {
        start++;
      }

      try
      {
        // allow for nnnnnn.tif or nnnnn-x.tif formatted filenames
        idx = fileName.indexOf("-");
        if ( idx < 0 )
        {
          idx = fileName.indexOf(".");
        }
        cn = (fileName.substring(start,idx)).trim();
        controlNum = Long.parseLong(cn);
        setValid(isValidCN(controlNum));
      }
      catch (NumberFormatException nfe)
      {
        controlNum = -1;
        isValidObj = false;

        //might be a ref num
        if( cn.length() == REF_NUM_LENGTH &&
            AutoAttachBase.isNumeric(cn))
        {
          otherRef = cn;
        }
      }
      catch (Exception e)
      {
        isValidObj = false;
        //e.printStackTrace();
      }
    }

    private boolean isValidCN(long controlNum)
    {
      boolean isValid = false;
      int result = 0;
      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:722^9*/

//  ************************************************************
//  #sql [Ctx] { select count(cb_load_sec) 
//            from network_chargebacks
//            where cb_load_sec = :controlNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(cb_load_sec)  \n          from network_chargebacks\n          where cb_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.AutoAttachBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,controlNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:727^9*/

        if(result > 0)
        {
          isValid = true;
          iType    = MesQueues.Q_ITEM_TYPE_CHARGEBACK;
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:736^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(retr_load_sec) 
//              from    network_retrievals  rt
//              where   retr_load_sec = :controlNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(retr_load_sec)  \n            from    network_retrievals  rt\n            where   retr_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.AutoAttachBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,controlNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:741^11*/

          if ( result == 0 )
          {
            message = "Invalid Ctl #.  Must be ctl # or ref #.";
          }
          else
          {
            iType = MesQueues.Q_ITEM_TYPE_RETRIEVAL;
            isValid = true;
            /*
            //check this retr - if a supported amex retr we can attach the image
            #sql[Ctx]
            {
              select  count(rt.retr_load_sec) into :result
              from    network_retrievals  rt,
                      q_data qd
              where   rt.retr_load_sec = :controlNum
              and     rt.retr_load_Sec = qd.id(+)
              and     qd.type = :(MesQueues.Q_RETRIEVALS_AMEX_PENDING)
            };

            if(result == 1)
            {
              isValid = true;
            }
            else
            {
              message = "Ctl # is an unsupported retrieval request.";
            }
            */
          }
        }
      }
      catch(Exception e)
      {
        logEntry("isValidCN(" + controlNum + ")",e.toString());
      }
      finally
      {
        try {cleanUp();}catch(Exception e){}
      }

      return isValid;

    }

    public boolean isAttached()
    {
      return( attachedFlag );
    }

    public boolean isValid()
    {
      return isValidObj;
    }

    public void setValid(boolean isValid)
    {
      this.isValidObj = isValid;
    }

  }

  //contains the information for making the FTP connection
  //then performs the necessary actions to process the image attachment etc.
  private class SFTPObj
  {

    public String sftpHost;
    public String sftpUser;
    public String sftpPassword;
    public String sftpPath;
    public int    addressInt;

    public SFTPObj(String sftpHost,String sftpUser,String sftpPassword, String sftpPath, String addressInt )
    {
      this.sftpHost        =  sftpHost;
      this.sftpUser        =  sftpUser;
      this.sftpPassword    =  sftpPassword;
      this.sftpPath        =  sftpPath;
      
      try
      {
        this.addressInt     =  Integer.parseInt(addressInt);
      }
      catch (Exception e)
      {
        this.addressInt = -1;
      }
    }

    public SFTPObj(String sftpHost,String sftpUser,String sftpPassword, String sftpPath)
    {
      this.sftpHost        =  sftpHost;
      this.sftpUser        =  sftpUser;
      this.sftpPassword    =  sftpPassword;
      this.sftpPath        =  sftpPath;
      this.addressInt     =  -1;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer();
      sb.append("sftpHost = ").append(sftpHost).append("\n");
      sb.append("sftpUser = ").append(sftpUser).append("\n");
      sb.append("sftpPassword = ").append(sftpPassword).append("\n");
      sb.append("sftpPath = ").append(sftpPath).append("\n");
      sb.append("addressInt = ").append(addressInt).append("\n");
      return sb.toString();
    }
    
    public String getHost()
    {
      return sftpHost;
    }

    public String getUser()
    {
      return sftpUser;
    }
    
    public String getPassword()
    {
      return sftpPassword;
    }
    
    public String getPath()
    {
      return sftpPath;
    }
    
    public int getAddressInt()
    {
      return addressInt;
    }
  }

}/*@lineinfo:generated-code*/