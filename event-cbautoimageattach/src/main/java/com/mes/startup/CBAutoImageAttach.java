/*@lineinfo:filename=CBAutoImageAttach*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

// log4j
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.database.SQLJConnectionBase;
import com.mes.queues.MerchantRebuttal;
import com.mes.queues.QueueTools;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;


public class CBAutoImageAttach extends AutoAttachBase
{

  // create class log category
  static Logger log = Logger.getLogger(CBAutoImageAttach.class);

  private MerchantRebuttal reb;
  private QueueTools tools    = null;

  public CBAutoImageAttach()
  {
    super();
    msgSubject = "Auto Merchant Doc Attachment Notification";
  }


  protected void readProps() throws Exception
  {

    PropertiesFile props = new PropertiesFile("imageattach.properties");

    //above Strings will be retrieved from props
    FTPHOST         = props.getString("M_FTPHOST",FTPHOST);
    FTPUSER         = props.getString("M_FTPUSER",FTPUSER);
    FTPPASSWORD     = props.getString("M_FTPPASSWORD",FTPPASSWORD);
    IMAGE_DIRECTORY = props.getString("M_IMAGE_DIRECTORY",IMAGE_DIRECTORY);
    ADDRESS_REF     = props.getString("M_ADDRESS_REF",ADDRESS_REF);
    SOURCE          = MERCHANT;

    REGEX           = props.getString("REGEX",REGEX);

  }

  /**
   * process()
   * main method whereby each file (image) is attached
   * and rebuttals are created/items moved in queue where
   * applicable
   **/
  protected void process()
  throws Exception{

    log.debug("START process()");

    AutoAttachBase.FileObj obj;
    String MESSAGE = "Auto-process: images";
    

    //ensure FTP connection is live
    try
    {
      if( !SftpSec.isConnected() )
      {
        refreshSFTP();
      }
    }
    catch(Exception fail)
    {
      refreshSFTP();
    }
    
    //add timestamp to MESSAGE
    String timeStamp    = " (action timestamp: "+DateTimeFormatter.getCurDateTimeString()+")";
    MESSAGE = MESSAGE + timeStamp;

    for(int i = 0; i < fileList.size(); i++)
    {
      obj = (AutoAttachBase.FileObj)fileList.get(i);
      try
      {
        log.debug("obj CN: "+obj.getControlNum()+" is valid");

        attachFile(obj, "au_");
        obj.message = "Doc successfully attached.";

        //only create rebuttals and move for CB obj
        if(obj.isType(MesQueues.Q_ITEM_TYPE_CHARGEBACK))
        {
          createRebuttal(obj);
          obj.message = "CB successfully processed.";
        }
        else
        {
          obj.message = "RETR successfully processed.";
        }

        moveQueue(obj, MESSAGE);

        successList.add(obj);
      }
      catch (Exception e)
      {
        obj.message = "Unable to process: " + e.getMessage();
        errorList.add(obj);

        //add this, in case something happens to the sendError()
        //kinda redundant
        //logEntry("process(" + String.valueOf(obj.fileName) + ")",e.toString());
      }
    }

    log.debug("\nValid files: " + fileList.size());
    log.debug("END process()");
  }

  protected void createRebuttal(AutoAttachBase.FileObj obj)
  throws Exception
  {
    log.debug("START createRebuttal()...");

    try
    {
      reb = new MerchantRebuttal(obj.getControlNum());

      reb.setDocStatus(MerchantRebuttal.DOC_STATUS_RECEIVED);

      //process, without rebuilding
      reb.processRebuttal(false);
    }
    catch(Exception e)
    {
      //exception thrown, null out reb and throw it on
      reb = null;
      throw e;
    }

    log.debug("END createRebuttal()...");
  }


  protected void moveQueue (AutoAttachBase.FileObj obj, String note)
  throws Exception
  {
    log.debug("START moveQueue()...");
    //update the queue
    //only update queue id item is not already in rebuttal queue

    if(tools==null)
    {
      tools = new QueueTools();
    }

    int currentQueue = tools.getCurrentType(obj.controlNum, obj.getType());
    int toQ;

    switch(obj.getType())
    {
      case MesQueues.Q_ITEM_TYPE_CHARGEBACK:
        if( tools.validRebuttalMove(obj.controlNum, currentQueue) )
        {
          log.debug("moving CN..." + obj.controlNum);

          //places into proper rebuttal queue
          toQ = MerchantRebuttal.determineQueue(obj.controlNum);
        }
        else
        {
          //throw new Exception("Found in REMC or REPR status - No Q move initiated.");
          toQ = 0;
        }
        break;

      case MesQueues.Q_ITEM_TYPE_RETRIEVAL:

        //use card_type to find applicable pend queue: DS, AM, MC, VS
        toQ = 0;
        try
        {
          connect();

          /*@lineinfo:generated-code*//*@lineinfo:202^11*/

//  ************************************************************
//  #sql [Ctx] { select  decode(rt.card_type,
//                              'AM',:MesQueues.Q_RETRIEVALS_AMEX_PENDING,
//                              'VS',:MesQueues.Q_RETRIEVALS_VISA_PENDING,
//                              'MC',:MesQueues.Q_RETRIEVALS_MC_PENDING,
//                              'DS',:MesQueues.Q_RETRIEVALS_DS_PENDING,
//                              0)
//                
//                from    network_retrievals  rt,
//                        q_data qd
//                where   rt.retr_load_sec = :obj.controlNum
//                and     rt.retr_load_Sec = qd.id(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode(rt.card_type,\n                            'AM', :1 ,\n                            'VS', :2 ,\n                            'MC', :3 ,\n                            'DS', :4 ,\n                            0)\n               \n              from    network_retrievals  rt,\n                      q_data qd\n              where   rt.retr_load_sec =  :5 \n              and     rt.retr_load_Sec = qd.id(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.CBAutoImageAttach",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_RETRIEVALS_AMEX_PENDING);
   __sJT_st.setInt(2,MesQueues.Q_RETRIEVALS_VISA_PENDING);
   __sJT_st.setInt(3,MesQueues.Q_RETRIEVALS_MC_PENDING);
   __sJT_st.setInt(4,MesQueues.Q_RETRIEVALS_DS_PENDING);
   __sJT_st.setLong(5,obj.controlNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   toQ = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:215^11*/
        }catch (Exception e){
        }finally {cleanUp();}

        if(toQ == currentQueue)
        {

          //stop the queue move - already there
          toQ = 0;
        }

        break;

      default:
        throw new Exception("Unknown item type - No Q move initiated.");

    }

    if(toQ > 0)
    {
      //move the item
      tools.moveQueue(  obj.controlNum,
                        currentQueue,
                        toQ,
                        null,
                        note,
                        false);
    }

    log.debug("END moveQueue()...");
  }

  public static void main( String[] args )
  {
    CBAutoImageAttach     test      = null;

    try
    {
      SQLJConnectionBase.initStandalone();

      test = new CBAutoImageAttach();
      test.execute();
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/