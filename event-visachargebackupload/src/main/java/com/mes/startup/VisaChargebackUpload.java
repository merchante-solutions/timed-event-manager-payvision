/*@lineinfo:filename=VisaChargebackUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/VisaChargebackUpload.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.chargeback.utils.ChargeBackConstants;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.settlement.SettlementRecord;
import sqlj.runtime.ResultSetIterator;

public class VisaChargebackUpload extends VisaFileBase
{
  public VisaChargebackUpload( )
  {
    PropertiesFilename          = "visa-wcb.properties";

    CardTypeTable               = "network_chargeback_visa";
    CardTypeTableActivity       = "network_chargeback_activity";
    CardTypePostTotals          = "vs_wcb";
    CardTypeFileDesc            = "Visa Chargeback";
    DkOutgoingHost              = MesDefaults.DK_VISA_EP_HOST;
    DkOutgoingUser              = MesDefaults.DK_VISA_EP_USER;
    DkOutgoingPassword          = MesDefaults.DK_VISA_EP_PASSWORD;
    DkOutgoingPath              = MesDefaults.DK_VISA_EP_OUTGOING_PATH;
    DkOutgoingUseBinary         = false;
    DkOutgoingSendFlagFile      = true;
    SettlementAddrsNotify       = MesEmails.MSG_ADDRS_OUTGOING_CB_NOTIFY;
    SettlementAddrsFailure      = MesEmails.MSG_ADDRS_OUTGOING_CB_FAILURE;
    SettlementRecMT             = SettlementRecord.MT_SECOND_PRESENTMENT;
  }
  
  protected boolean processTransactions( )
  {
    Date                      cpd               = null;
    Date                      fileDate          = null;
    ResultSetIterator         it                = null;
    int                       recCount          = 0;
    String                    workFilename      = null;
    
    try
    {
      if ( TestFilename == null )
      {
        FileTimestamp   = new Timestamp(Calendar.getInstance().getTime().getTime());
        fileDate        = new Date( FileTimestamp.getTime() );
        cpd             = getSettlementDate();

        workFilename    = generateFilename("vs_wcb"                                                     // base name
                                          + String.valueOf(Integer.parseInt( getEventArg(0) )),         // bank number
                                          ".ctf");                                                      // file extension


        /*@lineinfo:generated-code*//*@lineinfo:77^9*/

//  ************************************************************
//  #sql [Ctx] { update  network_chargeback_activity cba
//            set     cba.load_filename = :workFilename,
//                    cba.settlement_date = :cpd
//            where   cba.cb_load_sec in
//                    (
//                      select  cb.cb_load_sec 
//                      from    network_chargebacks           cb
//                      where   cb.bank_number in (select bank_number from mbs_banks where processor_id = 1)
//                              and cb.incoming_date between (:fileDate-180) and :fileDate 
//                              and cb.merchant_number != 0 
//                              and not cb.card_number_enc is null
//                              and cb.card_type = 'VS'
//                    ) 
//                    and cba.load_filename is null
//                    and cba.action_code = nvl(:ActionCode,cba.action_code)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  network_chargeback_activity cba\n          set     cba.load_filename =  :1  ,\n                  cba.settlement_date =  :2  \n          where   cba.cb_load_sec in\n                  (\n                    select  cb.cb_load_sec \n                    from    network_chargebacks           cb\n                    where   cb.bank_number in (select bank_number from mbs_banks where processor_id = 1)\n                            and cb.incoming_date between ( :3  -180) and  :4   \n                            and cb.merchant_number != 0 \n                            and not cb.card_number_enc is null\n                            and cb.card_type = 'VS'\n                  ) \n                  and cba.load_filename is null\n                  and cba.action_code = nvl( :5  ,cba.action_code)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.VisaChargebackUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setDate(3,fileDate);
   __sJT_st.setDate(4,fileDate);
   __sJT_st.setString(5,ActionCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:94^9*/
        // only process if there were records updated 
        recCount = Ctx.getExecutionContext().getUpdateCount();
      }
      else    // re-build a previous file
      {
        FileTimestamp   = new Timestamp( getFileDate(TestFilename).getTime() );
        fileDate        = new Date( FileTimestamp.getTime() );
        workFilename    = TestFilename;
        recCount        = 1;   // always re-build
      }
      
      if ( recCount > 0 )
      {
        // select the transactions to process
        /*@lineinfo:generated-code*//*@lineinfo:109^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  0                                           as batch_id,          -- not used in chargeback tables
//                    cb.cb_load_sec                              as batch_record_id,   -- really "cb_load_sec"
//                    cb.card_number                              as card_number,
//                    dukpt_decrypt_wrapper(cb.card_number_enc)   as card_number_full,
//                    cb.card_number_enc                          as card_number_enc,
//                    cb.tran_date                                as transaction_date,  
//                    cbv.transaction_date                        as transaction_time,  
//                    cb.reference_number                         as reference_number,                              
//                    cbv.acquirer_member_id                      as acquirer_business_id,
//                    cb.bin_number                               as bin_number,
//                    abs( cb.tran_amount )                       as transaction_amount,
//                    nvl( cbv.currency_code,'840' )              as currency_code,
//                    cbv.original_amt                            as original_amount,
//                    cbv.original_currency_code                  as original_currency_code,
//                    cbv.debit_credit_ind                        as debit_credit_indicator,
//                    cbv.authorization_number                    as auth_code,
//                    cbv.trans_id                                as auth_tran_id,
//                    substr(cbv.reason_code,-2)                  as reason_code,
//                    cbv.class_code                              as sic_code,
//                    cb.merchant_number                          as merchant_number,
//                    nvl(cbv.merchant_name,mf.dba_name)          as dba_name,
//                    nvl(cbv.merchant_city,mf.dmcity)            as dba_city,
//                    nvl(cbv.merchant_state,mf.dmstate)          as dba_state,
//                    substr(mf.dmzip,1,5)                        as dba_zip,
//                    cbv.merchant_country                        as country_code,
//                    cbv.chargeback_ref_num                      as cb_ref_num,                
//                    cba.document_indicator                      as documentation_ind,                
//                    substr(cba.user_message,1,50)               as message_text,
//                    decode(cbv.usage_code,'9', cbv.usage_code, least((cbv.usage_code+1),3))  as usage_code,
//                    cba.action_code                             as action_code,
//                    cbv.request_payment_service                 as requested_payment_service,
//                    cbv.authorization_source                    as auth_source_code,
//                    cbv.pos_terminal_capablities                as pos_term_cap,
//                    cbv.terminal_entry_mode                     as pos_entry_mode,
//                    cbv.cardholder_id_method                    as cardholder_id_method,
//                    cbv.multiple_clearing_seq_num               as multiple_clearing_seq_num,
//                    cbv.cardholder_act_term_ind                 as cat_indicator,
//                    cbv.mail_phone_order_ind                    as moto_ecommerce_ind,
//                    cbv.reimbursement_attr_code                 as reimbursement_attribute,
//                    cbv.merchant_volume_ind                     as merchant_vol_indicator,
//                    cbv.cashback_amount                         as cashback_amount,
//                    cbv.special_condition_ind                   as special_conditions_ind,
//                    cbv.fee_program_ind                         as fee_program_indicator,
//                    cbv.central_processing_date                 as batch_date,        -- really "cpd”
//                    cbv.special_chargeback_ind                  as special_chargeback_ind,
//                    cbv.business_format_code                    as business_format_code,
//                    cbv.dispute_condition                       as dispute_condition,
//                    cbv.vrol_case_number                        as vrol_case_number,
//                    cbv.dispute_status                          as dispute_status
//            from    network_chargebacks           cb,
//                    network_chargeback_activity   cba,                
//                    chargeback_action_codes       ac,
//                    network_chargeback_visa       cbv,
//                    mif                           mf
//            where   cb.bank_number in (select bank_number from mbs_banks where processor_id = 1)
//                    and cb.incoming_date between (:fileDate-180) and :fileDate 
//                    and cb.merchant_number != 0 
//                    and not cb.card_number_enc is null 
//                    and cba.cb_load_sec = cb.cb_load_sec 
//                    and cba.load_filename = :workFilename 
//                    and ac.short_action_code = cba.action_code 
//                    and nvl(ac.represent,'N') = 'Y'   -- REPR, REMC, REVR only
//                    and cbv.load_sec = cb.cb_load_sec
//                    and mf.merchant_number(+) = cb.merchant_number
//            order by cb.cb_load_sec      
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  0                                           as batch_id,          -- not used in chargeback tables\n                  cb.cb_load_sec                              as batch_record_id,   -- really \"cb_load_sec\"\n                  cb.card_number                              as card_number,\n                  dukpt_decrypt_wrapper(cb.card_number_enc)   as card_number_full,\n                  cb.card_number_enc                          as card_number_enc,\n                  cb.tran_date                                as transaction_date,  \n                  cbv.transaction_date                        as transaction_time,  \n                  cb.reference_number                         as reference_number,                              \n                  cbv.acquirer_member_id                      as acquirer_business_id,\n                  cb.bin_number                               as bin_number,\n                  abs( cb.tran_amount )                       as transaction_amount,\n                  nvl( cbv.currency_code,'840' )              as currency_code,\n                  cbv.original_amt                            as original_amount,\n                  cbv.original_currency_code                  as original_currency_code,\n                  cbv.debit_credit_ind                        as debit_credit_indicator,\n                  cbv.authorization_number                    as auth_code,\n                  cbv.trans_id                                as auth_tran_id,\n                  substr(cbv.reason_code,-2)                  as reason_code,\n                  cbv.class_code                              as sic_code,\n                  cb.merchant_number                          as merchant_number,\n                  nvl(cbv.merchant_name,mf.dba_name)          as dba_name,\n                  nvl(cbv.merchant_city,mf.dmcity)            as dba_city,\n                  nvl(cbv.merchant_state,mf.dmstate)          as dba_state,\n                  substr(mf.dmzip,1,5)                        as dba_zip,\n                  cbv.merchant_country                        as country_code,\n                  cbv.chargeback_ref_num                      as cb_ref_num,                \n                  cba.document_indicator                      as documentation_ind,                \n                  substr(cba.user_message,1,50)               as message_text,\n decode(cbv.usage_code,'9', cbv.usage_code, least((cbv.usage_code+1),3))  as usage_code,\n                  cba.action_code                             as action_code,\n                  cbv.request_payment_service                 as requested_payment_service,\n                  cbv.authorization_source                    as auth_source_code,\n                  cbv.pos_terminal_capablities                as pos_term_cap,\n                  cbv.terminal_entry_mode                     as pos_entry_mode,\n                  cbv.cardholder_id_method                    as cardholder_id_method,\n                  cbv.multiple_clearing_seq_num               as multiple_clearing_seq_num,\n                  cbv.cardholder_act_term_ind                 as cat_indicator,\n                  cbv.mail_phone_order_ind                    as moto_ecommerce_ind,\n                  cbv.reimbursement_attr_code                 as reimbursement_attribute,\n                  cbv.merchant_volume_ind                     as merchant_vol_indicator,\n                  cbv.cashback_amount                         as cashback_amount,\n                  cbv.special_condition_ind                   as special_conditions_ind,\n                  cbv.fee_program_ind                         as fee_program_indicator,\n                  cbv.central_processing_date                 as batch_date,        -- really \"cpd”\n                  cbv.special_chargeback_ind                  as special_chargeback_ind,\n                  cbv.business_format_code                    as business_format_code,\n                  cbv.dispute_condition                       as dispute_condition,\n                  cbv.vrol_case_number                        as vrol_case_number,\n                  cbv.dispute_status                          as dispute_status\n          from    network_chargebacks           cb,\n                  network_chargeback_activity   cba,                \n                  chargeback_action_codes       ac,\n                  network_chargeback_visa       cbv,\n                  mif                           mf\n          where   cb.bank_number in (select bank_number from mbs_banks where processor_id = 1)\n                  and cb.incoming_date between ( :1  -180) and  :2   \n                  and cb.merchant_number != 0 \n                  and not cb.card_number_enc is null \n                  and cba.cb_load_sec = cb.cb_load_sec \n                  and cba.load_filename =  :3   \n                  and ac.short_action_code = cba.action_code \n                  and nvl(ac.represent,'N') = 'Y'   -- REPR, REMC, REVR only\n                  and cbv.load_sec = cb.cb_load_sec\n                  and mf.merchant_number(+) = cb.merchant_number\n          order by cb.cb_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.VisaChargebackUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fileDate);
   __sJT_st.setDate(2,fileDate);
   __sJT_st.setString(3,workFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.VisaChargebackUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:176^9*/

        handleSelectedRecords( it, workFilename );
        addChargebackAdjustmentProcessEntry( workFilename );
        addMbsProcessEntry( ChargeBackConstants.PROC_TYPE_CB_FILE.getIntValue(), workFilename );
      }        
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "processTransactions()", e.toString());
      logEntry("processTransactions(" + workFilename + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return( true );
  }
  
  public static void main( String[] args )
  {
      try
      {
          if (args.length > 0 && args[0].equals("testproperties")) {
              EventBase.printKeyListStatus(new String[]{
                      MesDefaults.DK_VISA_EP_HOST,
                      MesDefaults.DK_VISA_EP_USER,
                      MesDefaults.DK_VISA_EP_PASSWORD,
                      MesDefaults.DK_VISA_EP_OUTGOING_PATH
              });
          }
      } catch (Exception e) {
          System.out.println(e.toString());
      }



    VisaChargebackUpload              test          = null;
    
    try
    { 
      test = new VisaChargebackUpload();
      test.executeCommandLine( args );
    }
    catch( Exception e )
    {
      log.debug(e.toString());
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}/*@lineinfo:generated-code*/