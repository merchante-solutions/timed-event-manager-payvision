/*@lineinfo:filename=DynamicDescriptorSummarizer*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

public class DynamicDescriptorSummarizer extends EventBase
{
  public boolean execute()
  {
    boolean retVal = false;
    
    try
    {
      connect();
      
      // summarize descriptors from previous day
      /*@lineinfo:generated-code*//*@lineinfo:14^7*/

//  ************************************************************
//  #sql [Ctx] { insert into dynamic_descriptors
//          (
//            merchant_number,
//            transaction_date,
//            card_type,
//            descriptor,
//            usage_count
//          )
//          select  /*+ index (tca idx_trident_capture_api) */
//                  tca.merchant_number,
//                  tca.transaction_date,
//                  tca.card_type,
//                  tca.dba_name,
//                  count(1)
//          from    trident_capture_api tca
//          where   tca.merchant_number = 941000108061 -- Digital River is the only PSP
//                  and tca.transaction_date = trunc(sysdate-1)
//          group by tca.merchant_number, tca.dba_name, tca.transaction_date, tca.card_type        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into dynamic_descriptors\n        (\n          merchant_number,\n          transaction_date,\n          card_type,\n          descriptor,\n          usage_count\n        )\n        select  /*+ index (tca idx_trident_capture_api) */\n                tca.merchant_number,\n                tca.transaction_date,\n                tca.card_type,\n                tca.dba_name,\n                count(1)\n        from    trident_capture_api tca\n        where   tca.merchant_number = 941000108061 -- Digital River is the only PSP\n                and tca.transaction_date = trunc(sysdate-1)\n        group by tca.merchant_number, tca.dba_name, tca.transaction_date, tca.card_type";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.DynamicDescriptorSummarizer",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:34^7*/
      
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( retVal );
  }
  
  public static void main(String[] args)
  {
    com.mes.database.SQLJConnectionBase.initStandalone();
    
    try
    {
      DynamicDescriptorSummarizer dds = new DynamicDescriptorSummarizer();
    
      System.out.println("summarizing yesterdays dynamic descriptors");
      dds.execute();
      System.out.println("done");
    }
    catch(Exception e)
    {
      System.out.println("main(): " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/