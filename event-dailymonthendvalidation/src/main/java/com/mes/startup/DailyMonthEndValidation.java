package com.mes.startup;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;

public class DailyMonthEndValidation extends EventBase	{
	
	private static final long serialVersionUID = 1L;
	
	private static final int IC_ACCEPTABLE_VARIANCE = 10000;
	static Logger log = Logger.getLogger(DailyMonthEndValidation.class);
	
	public DailyMonthEndValidation() {		
	}
	
	int[] bankNumbers = new int[] {3941, 3942, 3943, 3003, 3858};
	
	public boolean execute() {	
		
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		
		StringBuffer msgBody = new StringBuffer();
		
		boolean isVarianceGood = true;
		String ic_type = null;
		double clearing_visa_ic = 0.0, clearing_mc_ic = 0.0, clearing_disc_ic = 0.0;
		double summary_visa_ic = 0.0, summary_mc_ic = 0.0, summary_disc_ic = 0.0;
		double visa_ic_diff = 0.0, mc_ic_diff = 0.0, disc_ic_diff = 0.0;
		
		Calendar cal = Calendar.getInstance();
	    cal.set( Calendar.DAY_OF_MONTH, 1 );
	    java.sql.Date  activeDate  = new java.sql.Date( cal.getTime().getTime() );
		
		try {
			connect(true);
			
			pStmt = getPreparedStatement(IC_DIFF_SQL);
			
			for(int i = 0; i < bankNumbers.length; ++i) {
				
        int bankNumber = bankNumbers[i];
				isVarianceGood = true;
				ic_type = null;
				clearing_visa_ic = 0.0; clearing_mc_ic = 0.0; clearing_disc_ic = 0.0;
				summary_visa_ic = 0.0; summary_mc_ic = 0.0; summary_disc_ic = 0.0;
				visa_ic_diff = 0.0; mc_ic_diff = 0.0; disc_ic_diff = 0.0;
				int paramIdx = 1;
				
				pStmt.setInt(paramIdx++, bankNumber); 
				pStmt.setDate(paramIdx++, activeDate);
				pStmt.setDate(paramIdx++, activeDate);
				pStmt.setInt(paramIdx++, bankNumber); 
				pStmt.setDate(paramIdx++, activeDate);
				pStmt.setDate(paramIdx++, activeDate);
				
				rs = pStmt.executeQuery();
				
				while(rs.next()) {
					ic_type = rs.getString("ic_type");
					
					if(ic_type.equals("clearing")) {
						clearing_visa_ic = rs.getDouble("visa_ic"); 
						clearing_mc_ic = rs.getDouble("mc_ic");
						clearing_disc_ic = rs.getDouble("disc_ic");
					} else if(ic_type.equals("dailysummary")) {
						summary_visa_ic = rs.getDouble("visa_ic");
						summary_mc_ic = rs.getDouble("mc_ic");
						summary_disc_ic = rs.getDouble("disc_ic");
					}
				}
				
				visa_ic_diff = clearing_visa_ic + summary_visa_ic;
				mc_ic_diff = clearing_mc_ic + summary_mc_ic;
				disc_ic_diff = clearing_disc_ic + summary_disc_ic;
					
				isVarianceGood = visa_ic_diff < IC_ACCEPTABLE_VARIANCE 
							&& visa_ic_diff * -1 < IC_ACCEPTABLE_VARIANCE 
							&& mc_ic_diff < IC_ACCEPTABLE_VARIANCE
							&& mc_ic_diff * -1 < IC_ACCEPTABLE_VARIANCE
							&& disc_ic_diff < IC_ACCEPTABLE_VARIANCE
							&& disc_ic_diff * -1 < IC_ACCEPTABLE_VARIANCE;
					
				msgBody.append("IC differences for " + bankNumber + " are ");
				//+ "IC differences between Daily Clearing report and Daily Summarization ");
				msgBody.append(isVarianceGood ? "Okay \n\n" : "NOT Okay \n\n");				
			    msgBody.append("MONTHLY_VISA_IC   : " + summary_visa_ic + "\n");
				msgBody.append("CLEARING_VISA_IC  : " + clearing_visa_ic + "\n");
				msgBody.append("VISA_IC_DIFF              : " + visa_ic_diff + "\n\n");
				msgBody.append("MONTHLY_MC_IC     : " + summary_mc_ic + "\n");
				msgBody.append("CLEARING_MC_IC    : " + clearing_mc_ic + "\n");
				msgBody.append("MC_IC_DIFF                : " + mc_ic_diff + "\n\n");
				msgBody.append("MONTHLY_DISC_IC   : " + summary_disc_ic + "\n");
				msgBody.append("CLEARING_DISC_IC  : " + clearing_disc_ic + "\n");
				msgBody.append("DISC_IC_DIFF              : " + disc_ic_diff + "\n\n\n");
			}	
				
			sendEmail(msgBody, activeDate);
			
		} catch(SQLException sqe) {
			log.error("execute()", sqe);
			System.out.println(sqe);
		} finally {
			try { 
				if(pStmt != null) 
					pStmt.close(); 
			} catch(Exception ignore) {}
			
		    try { 
		    	if(rs != null) 
					rs.close(); 
		    } catch(Exception ignore) {}
		}
		
		return isVarianceGood;
	}
	
	
	private void sendEmail(StringBuffer msgBody, Date activeDate) {
	    
		try {
			log.debug(msgBody);
			System.out.println(msgBody);
			
			MailMessage msg = new MailMessage();
			msg.setAddresses(MesEmails.MSG_ADDRS_DAILY_MONTH_END_VALIDATION_NOTIFY); // 177
			msg.setSubject("Daily Month End Validation - " + activeDate);
			msg.setText(msgBody.toString());
			msg.send();
	    } catch(Exception e) {
	    	log.error("sendEmail()", e);
	    }
	}
	
	public static void main(String args[]) {		
		if(args.length > 0 && "execute".equals(args[0]) ) {
				DailyMonthEndValidation dmev = new DailyMonthEndValidation();
				dmev.execute();
		}
	}
	
	private final static String NL = "\n";
	
	private String IC_DIFF_SQL = "select trunc(sysdate), " + NL + 
			"        bank_number, " + NL + 
			"		 'clearing'				 as ic_type, " + NL +
			"        sum(visa_ic)            as visa_ic, " + NL + 
			"        sum(mc_ic)              as mc_ic, " + NL + 
			"        sum(disc_ic)            as disc_ic " + NL + 
			"from ( " + NL + 
			"    select  bank_number, " + NL + 
			"            case when card_type = 'VS' " + NL + 
			"                    then sum(reimbursement_fees) " + NL + 
			"                    else 0   end                        as visa_ic, " + NL + 
			"            case when card_type = 'MC' " + NL + 
			"                    then sum(reimbursement_fees) " + NL + 
			"                    else 0   end                        as mc_ic, " + NL + 
			"            case when card_type = 'DS' " + NL + 
			"                    then sum(reimbursement_fees) " + NL + 
			"                    else 0   end                        as disc_ic " + NL + 
			"    from    daily_clearing_summary " + NL + 
			"    where   bank_number = ? " + NL + 
			"            and clearing_date between ? and last_day(?) " + NL + 
			"    group by bank_number, card_type) " + NL + 
			" group by bank_number " + NL + 
			" union " + NL + 
			" select   trunc(sysdate), " + NL + 
			"          mf.bank_number, " + NL +
			"		  'dailysummary'				 						  as ic_type, " + NL +
			"          nvl(sum( decode(ds.item_type,111,1,0)  " + NL + 
			"                   * decode(ds.item_subclass,'VS',1,0) " + NL + 
			"                   * ds.expense_actual " + NL + 
			"                 ),0)                                             as visa_ic, " + NL + 
			"          nvl(sum( decode(ds.item_type,111,1,0)  " + NL + 
			"                   * decode(ds.item_subclass,'MC',1,0) " + NL + 
			"                   * ds.expense_actual " + NL + 
			"                 ),0)                                             as mc_ic, " + NL + 
			"          nvl(sum( decode(ds.item_type,111,1,0)  " + NL + 
			"                   * decode(ds.item_subclass,'DS',1,0) " + NL + 
			"                   * ds.expense_actual " + NL + 
			"                 ),0)                                             as disc_ic " + NL + 
			"  from    mbs_daily_summary   ds, mif mf " + NL + 
			"  where   ds.me_load_file_id = 0  " + NL + 
			"          and ds.item_type in ( 7, 14, 15, 111, 113, 117 ) " + NL + 
			"          and mf.merchant_number = ds.merchant_number   " + NL + 
			"          and mf.bank_number = ? " + NL + 
			"          and ds.activity_date  between ? and last_day(?) " + NL +
			"  group by  mf.bank_number" + NL; 
}