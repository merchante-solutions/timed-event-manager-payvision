/*@lineinfo:filename=WFAutoBoardResponse*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/trunk/src/main/com/mes/startup/WfAutoBoardResponse.sqlj $

  Description:

  Last Modified By   : $Author: mjanbay $
  Last Modified Date : $Date: 2015-02-10 17:34:41 -0700 (Wed, 10 Apr 2013) $
  Version            : $Revision: 21033 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;

public class WFAutoBoardResponse extends TsysFileBase
{
  static Logger log = Logger.getLogger(WFAutoBoardResponse.class);
  
  private String manualRequiredDate = null;
  private static final String newLine = System.getProperty("line.separator").toString();
  private static final String delimiter = ",";
  private int EmailGroup = 0;
  
  public WFAutoBoardResponse( ) {
    EmailGroup = MesEmails.MSG_ADDRS_INCORRECT_WF_MERCHANTS_TO_MES;
  }
  
  private boolean retrieveBoardingResponses() {
    log.debug("retrieving profile response file(s)");  
    
    try {
      String host = MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_HOST);     
      String user = MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_USER);     
      String pw   = MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_PASSWORD); 
      String path = MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_PATH);  
      
      Iterator i = getSftpDirList(host, user, pw, path, false);
      
      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
      String dateStr = dateFormat.format(new java.util.Date()); 
      
      if (getManualRequiredDate() != null) {
        // use user provided date
        dateStr = manualRequiredDate;
      }
      
      String fileName = null;
      int numberOfFiles = 0;
      while( i.hasNext() )
      {
        fileName = (String)i.next();
        if (fileName.indexOf("rack_"+dateStr) != -1) {
        
        log.debug("found file: " + fileName);
        
        log.debug("retrieving file");
        //test start
//        byte respFile[] = null;
//        File file = new File("C:/sql/1186_rack_20141001_test.xml"); 
//        FileInputStream fin = null; 
//        try { 
//            fin = new FileInputStream(file); 
//            respFile = new byte[(int)file.length()]; 
//            fin.read(respFile);  
//        } 
//        catch (FileNotFoundException e) { 
//        } 
//        catch (IOException ioe) { 
//        } 
//        finally { 
//            try { 
//                if (fin != null) { 
//                    fin.close(); 
//                } 
//            } 
//            catch (IOException ioe) { 
//            } 
//        }
        //test end
        // retrieve this file
        byte[] respFile = ftpGet(fileName, null, host, user, pw, path, false);
        numberOfFiles++;
        String outFilename = "wf_resp_"+dateStr+"_"+numberOfFiles+".dat";
        
        log.debug("writing response file: " + outFilename);
        
        FileWriter out = null;
        try {
          out = new FileWriter(outFilename);
        
          String data = new String(respFile);
          
          out.write( data );
          
          out.flush();
          out.close();
          //start
          StringBuffer details = new StringBuffer();
          
          //append the header
          details.append("Agent ID").append(delimiter)
                 .append("Registration ID").append(delimiter)
                 .append("RSP Codes").append(newLine);

          try {
              DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
              DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
              Document doc = docBuilder.parse(new File(outFilename));

              // normalize text representation
              doc.getDocumentElement().normalize();
              System.out.println("Root element of the doc is :\" "+ doc.getDocumentElement().getNodeName() + "\"");
              NodeList listOfAck = doc.getElementsByTagName("Ack");
              int totalAck = listOfAck.getLength();
              System.out.println("Total no of Ack : " + totalAck);
              String agentId = "", registrationId = "";
              for (int a = 0; a < listOfAck.getLength(); a++) {
                  Node firstAckNode = listOfAck.item(a);
                  if (firstAckNode.getNodeType() == Node.ELEMENT_NODE) {
                      Element firstAckElement = (Element) firstAckNode;
                      NodeList agentIdList = firstAckElement.getElementsByTagName("AgentID");
                      Element agentIdElement = (Element) agentIdList.item(0);
                      NodeList textAgentIdList = agentIdElement.getChildNodes();
                      System.out.println("Agent ID : "+ ((Node) textAgentIdList.item(0)).getNodeValue().trim());
                      agentId = ((Node) textAgentIdList.item(0)).getNodeValue().trim();
                      
                      NodeList registrationIdList = firstAckElement.getElementsByTagName("RegistrationID");
                      Element registrationIdElement = (Element) registrationIdList.item(0);
                      NodeList textRegistrationIdList = registrationIdElement.getChildNodes();
                      System.out.println("Registration ID : "+ ((Node) textRegistrationIdList.item(0)).getNodeValue().trim());
                      registrationId = ((Node) textRegistrationIdList.item(0)).getNodeValue().trim();
                      
                      StringBuffer responseCodes = new StringBuffer();
                      NodeList registrationFaultList = firstAckElement.getElementsByTagName("RegistrationFaultList");
                      Element registrationFaultElement = (Element) registrationFaultList.item(0);
                      if (registrationFaultElement != null) {
                        NodeList listOfRegistrationFault = registrationFaultElement.getChildNodes();
                        int totalRegistrationFault = listOfRegistrationFault.getLength();
                        String rspCode = "", faultField = "", faultDescription = "", faultAdvice = "";
                        for (int r = 0; r < listOfRegistrationFault.getLength(); r++)  {
                             Node firstRegistrationFault = listOfRegistrationFault.item(r);
                             if (firstRegistrationFault.getNodeType() == Node.ELEMENT_NODE) {  
                                 if (r > 0) {
                                   responseCodes.append(delimiter);
                                 }
                                Element firstRegistrationElement = (Element) firstRegistrationFault;
                                NodeList rspCodeList = firstRegistrationElement.getElementsByTagName("RSPCode");
                                Element rspCodeElement = (Element) rspCodeList.item(0);
                                NodeList textRspCodeList = rspCodeElement.getChildNodes();
                                System.out.println("RSP Code : "+ ((Node) textRspCodeList.item(0)).getNodeValue().trim());
                                rspCode = ((Node) textRspCodeList.item(0)).getNodeValue().trim();
                                responseCodes.append(rspCode).append(delimiter);
                                
                                NodeList faultFieldList = firstRegistrationElement.getElementsByTagName("FaultField");
                                Element faultFieldElement = (Element) faultFieldList.item(0);
                                NodeList textFaultFieldList = faultFieldElement.getChildNodes();
                                System.out.println("FaultField : "+ ((Node) textFaultFieldList.item(0)).getNodeValue().trim());
                                faultField = ((Node) textFaultFieldList.item(0)).getNodeValue().trim();
                                responseCodes.append(faultField).append(delimiter);
                                
                                NodeList faultDescriptionList = firstRegistrationElement.getElementsByTagName("FaultDescription");
                                Element faultDescriptionElement = (Element) faultDescriptionList.item(0);
                                NodeList textFaultDescriptionList = faultDescriptionElement.getChildNodes();
                                System.out.println("FaultDescription : "+ ((Node) textFaultDescriptionList.item(0)).getNodeValue().trim());
                                faultDescription = ((Node) textFaultDescriptionList.item(0)).getNodeValue().trim();
                                responseCodes.append(faultDescription);
                                
                                NodeList faultAdviceList = firstRegistrationElement.getElementsByTagName("FaultAdvice");
                                if (faultAdviceList != null && faultAdviceList.getLength()>0) {
                                  Element faultAdviceElement = (Element) faultAdviceList.item(0);
                                  NodeList textFaultAdviceList = faultAdviceElement.getChildNodes();
                                  System.out.println("FaultAdvice : "+ ((Node) textFaultAdviceList.item(0)).getNodeValue().trim());
                                  faultAdvice = ((Node) textFaultAdviceList.item(0)).getNodeValue().trim();
                                  responseCodes.append(delimiter).append(faultAdvice);
                                }
                              }
                          }
                      }
                      if (responseCodes.length() > 0) {
                        details.append(agentId).append(delimiter).append(registrationId).append(responseCodes.toString()).append(newLine);
                        insertRejectedMerchant(Long.parseLong(registrationId));
                      }

                  }// end of if clause

              }// end of for loop with a var
          } 
          catch (SAXParseException err) {
              System.out.println();
              System.out.println(" " + err.getMessage());
              logEntry("retrieveBoardingResponses()", "** Parsing error" + ", line "+ err.getLineNumber() + ", uri " + err.getSystemId());
              throw( err );
          } 
          catch (SAXException e) {
              Exception x = e.getException();
              logEntry("retrieveBoardingResponses()", "** SAX Exception: " + ((x == null) ? e : x).getMessage());
              throw( e );
          } 
          catch (Throwable t) {
              logEntry("retrieveBoardingResponses()", "** SAX Exception: " + t.getMessage());
              throw( t );
          }
        
          // send file in email
          //System.out.println(details.toString());
          MailMessage msg = new MailMessage();
          msg.setAddresses( EmailGroup);
          msg.setSubject("Wells Fargo Response File - " + outFilename);
          msg.setText("Response file retrieved from Wells Fargo\n\n" + outFilename);
          msg.addStringAsTextFile("wf_resp_"+dateStr+"_"+numberOfFiles+".csv", details.toString());
          msg.send();
        }
        catch(Exception fe) {
          logEntry("retrieveBoardingResponses() - writing file", fe.toString());
        }
        finally {
          try{ out.flush(); out.close(); } catch(Exception se) {}
        }
        
        } //
      } //while
    }
    catch(Exception e) {
      logEntry("retrieveBoardingRepsonse()", e.toString());
    }
    
    return( true );
  }
  
  /**
   * 
   */
  public void insertRejectedMerchant(long merchantNumber) {
    
    try {
      connect(true);
      int recCount = 0;
      try {

        /*@lineinfo:generated-code*//*@lineinfo:293^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//            
//            from    auto_boarding_reject
//            where   merchant_number = :merchantNumber and 
//                    corrected_ind = 'N'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n           \n          from    auto_boarding_reject\n          where   merchant_number =  :1  and \n                  corrected_ind = 'N'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.WFAutoBoardResponse",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:300^9*/

      }
      catch(Exception ex) {
        //nothing to do
      }

      if (recCount == 0) {
 
        /*@lineinfo:generated-code*//*@lineinfo:309^9*/

//  ************************************************************
//  #sql [Ctx] { insert into auto_boarding_reject
//             (
//               merchant_number,
//               rejected_date,
//               corrected_ind
//             )
//             values
//             (
//               :merchantNumber,
//               sysdate,
//               'N'
//             )
//           
//            };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into auto_boarding_reject\n           (\n             merchant_number,\n             rejected_date,\n             corrected_ind\n           )\n           values\n           (\n              :1 ,\n             sysdate,\n             'N'\n           )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.WFAutoBoardResponse",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:324^10*/
        //  commit();
      }
    }
    catch(Exception e) {
      logEntry("insertRejectedMerchant()", e.toString());
    }
    
  }
  
  public boolean execute( ) {
    return retrieveBoardingResponses();
  }
  
  
  public String getManualRequiredDate() {
    return manualRequiredDate;
  }

  public void setManualRequiredDate(String manualRequiredDate) {
    this.manualRequiredDate = manualRequiredDate;
  }
  
  public static void main(String[] args) {
    WFAutoBoardResponse worker = null;
    //  arg[0] = "execute"
    //  arg[1] = "20140731" format is yyyyMMdd
    try {
      if (args.length > 1 && "execute".equals(args[0])) {
        com.mes.database.SQLJConnectionBase.initStandalonePool("DEBUG");
        
        worker = new WFAutoBoardResponse();
        worker.setManualRequiredDate(args[1].trim());
        worker.execute();
        System.exit(0);
      } else {
        System.out.println("Invalid Command");
        Runtime.getRuntime().exit(1);
      }
    }
    catch(Exception e) {
      System.out.println("main(): " + e.toString());
      Runtime.getRuntime().exit(1);
    }
    
    //Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/