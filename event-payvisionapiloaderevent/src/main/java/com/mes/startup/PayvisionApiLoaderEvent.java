package com.mes.startup;

import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.config.DbProperties;
import com.mes.config.MesDefaults;
import com.mes.net.MesNetTools;
import com.mes.payvision.ApiLoader;
import com.mes.support.DateTimeFormatter;
import com.mes.support.LoggingConfigurator;

public final class PayvisionApiLoaderEvent extends EventBase
{
  static { LoggingConfigurator.requireConfiguration(); } // configure log4j
  static Logger log = Logger.getLogger(PayvisionApiLoaderEvent.class);
  
  static { DbProperties.requireConfiguration(); } // configure db connections
  
  private   String    EventUserName   = "timed_event";

  public boolean execute()
  {
    Calendar  cal           = Calendar.getInstance();
    Date      activityDate  = null;
    int       retryCount    = 0;
    boolean   retVal        = false;
  
    try
    {
      // requires load type event parameter, see ApiLoader for valid args
	  log.info("Enabling TLS 1.2");
	  MesNetTools.setupSecureSocketFactory();
      String  loadType    = getEventArg(0);
      int     daysToLoad  = MesDefaults.getInt(MesDefaults.DK_PV_API_LOADER_DAYS);

      // clear the time portion of the calendar
      cal.set(Calendar.HOUR_OF_DAY,0);
      cal.set(Calendar.MINUTE,0);
      cal.set(Calendar.SECOND,0);
      cal.set(Calendar.MILLISECOND,0);
      
      for( int i = 0; i < daysToLoad; ++i )
      {
        cal.add(Calendar.DAY_OF_MONTH,-1);
        activityDate = cal.getTime();
        retryCount = 0;
        
        log.debug("Loading " + com.mes.support.DateTimeFormatter.getFormattedDate(activityDate,"MM/dd/yyyy"));
        
        while( true )
        {
          try
          {
            ApiLoader.load(EventUserName,loadType,activityDate);
            break;    // exit loop
          }
          catch( Exception ee )
          {
            if ( ++retryCount > 4 )
            {
              throw(ee);
            }
          }
        }
      }
      retVal = true;
    }
    catch (Exception e)
    {
      log.error("Payvision API loader event error: " + e);
      e.printStackTrace();
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    return( retVal );
  }
  
  private void setEventUserName( String userName )
  {
    EventUserName = userName;
  }
  
  public static void main(String[] args)
  {
    try {
      if (args.length > 0 && args[0].equals("testproperties")) {
        EventBase.printKeyListStatus(new String[]{
                MesDefaults.DK_PV_API_LOADER_DAYS
        });
      }
    } catch (Exception e) {
      log.error(e.toString());
    }

    PayvisionApiLoaderEvent   evt  = new PayvisionApiLoaderEvent();
    String loadType = null;
    String customDateStr = null;
    Date customDate = null;
    
    try
    { 
      if( args.length > 0 && "reload".equals(args[0]) )
      {
        log.debug("Loading Payvision API data for loadType " + args[1] + "  and date " + args[2]);
        evt.setEventArgs(args[1]);
        evt.setEventUserName("te_cmd_line");
        
        loadType = args[1];
        customDateStr = args[2];
        customDate = new java.sql.Date(DateTimeFormatter.parseDate(customDateStr, "MM/dd/yyyy").getTime());
        evt.reloadDataForCustomDate(loadType, customDate );
      }
      else    // default is to execute the TE 
      {     
        if(evt.execute())
        {
          log.debug("Success!");
        }
        else
        {
          log.debug("Failure!");
        }
      }
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ com.mes.database.OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
  
  private boolean reloadDataForCustomDate(String loadType, Date customDate) {
    Calendar  cal           = Calendar.getInstance();
    Date      activityDate  = null;
    int       retryCount    = 0;
    boolean   retVal        = false;
  
    try
    {
      // requires load type event parameter, see ApiLoader for valid args
      com.mes.net.MesNetTools.disableSSLCertificateValidation();          
      int     daysToLoad  =  0; //MesDefaults.getInt(MesDefaults.DK_PV_API_LOADER_DAYS,2);

      // set custom date and clear the time portion of the calendar
      cal.setTime(customDate);
      cal.set(Calendar.HOUR_OF_DAY,0);
      cal.set(Calendar.MINUTE,0);
      cal.set(Calendar.SECOND,0);
      cal.set(Calendar.MILLISECOND,0);
      
      cal.add(Calendar.DAY_OF_MONTH,-1);
      activityDate = cal.getTime();
      retryCount = 0;
      
      log.debug("Reloading data for date - " + com.mes.support.DateTimeFormatter.getFormattedDate(activityDate,"MM/dd/yyyy"));
        
      while( true )
      {
        try
        {
          ApiLoader.load(EventUserName,loadType,activityDate);
          break;    // exit loop
        }
        catch( Exception ee )
        {
          if ( ++retryCount > 4 )
          {
            throw(ee);
          }
        }
      }         
      retVal = true;
    }
    catch (Exception e)
    {
      log.error("Payvision API loader event error: " + e);
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    
    return retVal;
  }
}