package com.mes.payvision;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class DateRange
{
  private Calendar start  = Calendar.getInstance();
  private Calendar end    = Calendar.getInstance();
  
  /**
   * Midnight to midnight on current date
   */
  public DateRange()
  {
    this(Calendar.getInstance().getTime());
  }

  /**
   * Midnight to midnight on a date
   */
  public DateRange(Date date)
  {
    setOneDay(date);
  }

  /**
   * Date + offset to the present
   */
  public DateRange(Date date, int startOff)
  {
    setStartDate(date);
    start.add(Calendar.DATE,startOff);
    setEndDate(Calendar.getInstance().getTime());
  }

  /** 
   * Start date to end date
   */
  public DateRange(Date startDate, Date endDate)
  {
    setStartDate(startDate);
    setEndDate(endDate);
  }


  public DateRange(Date startDate, Date endDate, boolean doPartDays)
  {
    if (doPartDays)
    {
      start.setTime(startDate);
      end.setTime(endDate);
    }
    else
    {
      setStartDate(startDate);
      setEndDate(endDate);
    }
  }

  /**
   * Start date + offset to end date
   */
  public DateRange(Date startDate, Date endDate, int startOff)
  {
    this(startDate,endDate);
    start.add(Calendar.DATE,startOff);
  }

  /**
   * Sets date range from midnight of date to 23:59:59.999 of date
   */
  public void setOneDay(Date date)
  {
    start = startOfDay(date);
    end = endOfDay(date);
  }
  
  /**
   * Returns calendar set to 0:00:00.000 of date.
   */
  private Calendar startOfDay(Date date)
  {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(cal.HOUR_OF_DAY,0);
    cal.set(cal.MINUTE,0);
    cal.set(cal.SECOND,0);
    cal.set(cal.MILLISECOND,0);
    return cal;
  }

  /**
   * Returns calendar set to 23:59:59.999 of date.
   */
  private Calendar endOfDay(Date date)
  {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.set(cal.HOUR_OF_DAY,23);
    cal.set(cal.MINUTE,59);
    cal.set(cal.SECOND,59);
    cal.set(cal.MILLISECOND,999);
    return cal;
  }

  /**
   * Start date accessors
   */
  
  public void setStartDate(Date startDate)
  {
    start = startOfDay(startDate);
  }
  public Date startDate()
  {
    return start.getTime();
  }
  public Timestamp startTs()
  {
    return new Timestamp(start.getTime().getTime());
  }
  public Calendar getStart()
  {
    return start;
  }

  /**
   * End date accessors
   */
  
  public void setEndDate(Date endDate)
  {
    end = endOfDay(endDate);
  }
  public Date endDate()
  {
    return end.getTime();
  }
  public Timestamp endTs()
  {
    return new Timestamp(end.getTime().getTime());
  }
  public Calendar getEnd()
  {
    return end;
  }
}

