/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/payvision/ApiLoader.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-10-22 16:21:37 -0700 (Tue, 22 Oct 2013) $
  Version            : $Revision: 21660 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.payvision;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;

public class ApiLoader extends SQLJConnectionBase
{
  static Logger log = Logger.getLogger(ApiLoader.class);

  public class LastResponse
  {
    public int memberId;
    public Date lastDate;

    public LastResponse(String memberId, Timestamp lastTs)
    {
      try
      {
        this.memberId = Integer.parseInt(memberId);
      }
      catch(Exception e)
      {
        // invalid member id - set to zero
        this.memberId = 0;
      }
      lastDate = new Date();
      lastDate.setTime(lastTs.getTime());
    }
  }

  /**
   * Loads the last response for active member id's for the given request type.
   * Currently this only works for response records with a date range specified.
   */
  private List getLastResponses(String requestType)
  {
    PreparedStatement ps = null;
    ResultSet rs = null;

    try
    {
      connect();
      ps = con.prepareStatement(
        " select                                    " +
        "  am.member_id,                            " +
        "  nvl(lr.latest,'24-may-2008') last_date    " +
        " from                                      " +
        "  ( select                                 " +
        "     to_char(member_id) as member_id,      " +
        "     max(end_ts) latest                    " +
        "    from                                   " +
        "     payvision_api_responses               " +
        "    where                                  " +
        "     request_type = ?                      " +
        "     and end_ts is not null                " +
        "    group by member_id) lr,                " +
        "  ( select                                 " +
        "     unique(substr(intl_credentials,1,     " +
        "       instr(intl_credentials,',') - 1))   " +
        "        member_id                          " +
        "    from                                   " +
        "     trident_profile_api                   " +
        "    where                                  " +
        "     payvision_accept = 'y'                " +
        "     and intl_credentials is not null ) am " +
        " where                                     " +
        "  am.member_id = lr.member_id(+)           ");
      ps.setString(1,requestType);
      rs = ps.executeQuery();
      List list = new ArrayList();
      while (rs.next())
      {
        list.add(
          new LastResponse(rs.getString(1),rs.getTimestamp(2)));
      }
      return list;
    }
    catch (Exception e)
    {
      log.error("Error loading last responses: " + e);
      e.printStackTrace();
    }
    finally
    {
      try { rs.close(); } catch (Exception ie) { }
      try { ps.close(); } catch (Exception ie) { }
      cleanUp();
    }
    return null;
  }

  /**
   * Transaction loading
 * @throws Exception 
   */
  private boolean loadTransactions(String userName, int memberId, DateRange range) throws Exception
  {
    List members = getLastResponses("getReconciliation");
    PayvisionDb db = new PayvisionDb();
    for (Iterator i = members.iterator(); i.hasNext();)
    {
      LastResponse lr = (LastResponse)i.next();
      
      if (memberId == ALL_MEMBER_IDS || memberId == lr.memberId)
      {
        DateRange dr = 
          (range != null ?  range : new DateRange(lr.lastDate,Calendar.getInstance().getTime()));
        ReconciliationRequest request =
          new ReconciliationRequest(userName,lr.memberId,dr.startDate(),dr.endDate());
        
        request.setEndpoint(MesDefaults.PAYVISION_TRANSACTIONS_PATH);
        log.debug("sending request " + request);
        request.sendRequest();
        ReconciliationResponse response = request.getReconciliationResponse();
        log.debug("received response " + response);
        db.saveReconciliationData(response);
      }        
    }
    return true;
  }
  
  private boolean loadTransactions(String userName) throws Exception
  {
    return loadTransactions(userName,ALL_MEMBER_IDS,null);
  }
  
  // load a member for a specific date range
  public static boolean loadTransactionDates(String userName, int memberId, Date startDate, Date endDate) throws Exception
  {
    ApiLoader loader = new ApiLoader();
    DateRange dr = new DateRange(startDate,endDate);
    return loader.loadTransactions(userName,memberId,dr);
  }

  public static boolean loadTransactionDates(String userName, int memberId, DateRange range) throws Exception
  {
    ApiLoader loader = new ApiLoader();
    return loader.loadTransactions(userName,memberId,range);
  }
  

  /**
   * Chargeback loading
   */

  public static int ALL_MEMBER_IDS = -1;

  // load a member in a date range
  private boolean loadChargebacks(String userName, int memberId, 
    DateRange range) throws Exception
  {
    List members = getLastResponses("getChargebacks");
    for (Iterator i = members.iterator(); i.hasNext();)
    {
      LastResponse lr = (LastResponse)i.next();

      // load chargebacks if loading all members, or if memb
      if (memberId == ALL_MEMBER_IDS || memberId == lr.memberId)
      {
        // if no range given, default to 
        // last response date - 14 days to present
        DateRange dr = 
          (range != null ?  range : new DateRange(lr.lastDate,-14));

        ChargebackRequest request = new ChargebackRequest(
          userName,lr.memberId,dr.startDate(),dr.endDate());
        request.setEndpoint(MesDefaults.PAYVISION_CHARGEBACKS_PATH);
        
        log.debug("sending request " + request);
        request.sendRequest();
        ChargebackResponse response = request.getChargebackResponse();
        log.debug("received response " + response);
        PayvisionDb db = new PayvisionDb();
        db.saveChargebackRecords(response);
      }
    }
    return true;
  }

  // load all members in the date range
  private boolean loadChargebacks(String userName, DateRange dr) throws Exception
  {
    return loadChargebacks(userName,ALL_MEMBER_IDS,dr);
  }

  // load a member from last load -14 days to present
  private boolean loadChargebacks(String userName, int memberId) throws Exception
  {
    return loadChargebacks(userName,memberId,null);
  }
    
  // load all members from last load - 14 days to present
  private boolean loadChargebacks(String userName) throws Exception
  {
    return loadChargebacks(userName,ALL_MEMBER_IDS,null);
  }

  // load a day of chargebacks for a member
  public static boolean loadChargebackDate(String userName, int memberId, 
    Date date) throws Exception
  {
    ApiLoader loader = new ApiLoader();
    DateRange dr = new DateRange(date);
    return loader.loadChargebacks(userName,memberId,dr);
  }

  // load a member for a specific date range
  public static boolean loadChargebackDates(String userName, int memberId, 
    Date startDate, Date endDate) throws Exception
  {
    ApiLoader loader = new ApiLoader();
    DateRange dr = new DateRange(startDate,endDate);
    return loader.loadChargebacks(userName,memberId,dr);
  }
  
  // load a member for a specific date range
  public static boolean loadChargebackDates(String userName, int memberId, DateRange range ) throws Exception
  {
    ApiLoader loader = new ApiLoader();
    return loader.loadChargebacks(userName,memberId,range);
  }
  
    
  /**
   * Decline loading
 * @throws Exception 
   */

  private boolean loadDeclines(String userName, int memberId, DateRange range) throws Exception
  {
    List members = getLastResponses("getDeclines");
    for (Iterator i = members.iterator(); i.hasNext();)
    {
      LastResponse lr = (LastResponse)i.next();

      // load chargebacks if loading all members, or if memb
      if (memberId == ALL_MEMBER_IDS || memberId == lr.memberId)
      {
        DateRange dr = 
          (range != null ?  range : new DateRange(lr.lastDate,Calendar.getInstance().getTime()));
      
        PayvisionDb db = new PayvisionDb();

        DeclineRequest request = new DeclineRequest(userName,
                                                    lr.memberId,
                                                    dr.startDate(),
                                                    dr.endDate());
        
        request.setEndpoint(MesDefaults.PAYVISION_DECLINES_PATH);
        log.debug("sending request " + request);
        request.sendRequest();
        DeclineResponse response = request.getDeclineResponse();
        log.debug("received response " + response);
        db.saveDeclineRecords(response);
      }
    }
    return true;
  }

  // load all members from last load - 14 days to present
  private boolean loadDeclines(String userName) throws Exception
  {
    return loadDeclines(userName,ALL_MEMBER_IDS,null);
  }

  // load a member for a specific date range
  public static boolean loadDeclineDates(String userName, int memberId, 
    Date startDate, Date endDate) throws Exception
  {
    ApiLoader loader = new ApiLoader();
    DateRange dr = new DateRange(startDate,endDate);
    return loader.loadDeclines(userName,memberId,dr);
  }

  public static boolean loadDeclineDates(String userName, int memberId,
    DateRange range) throws Exception
  {
    ApiLoader loader = new ApiLoader();
    return loader.loadDeclines(userName,memberId,range);
  }
    
  /** 
   * Timed event entry point.
 * @throws Exception 
   */

  // used by timed event, takes a load type parameter that determines type to load
  public static boolean load(String userName, String loadType, Date activityDate) throws Exception
  {
    ApiLoader loader      = new ApiLoader();

    if (loadType.equals("chargebacks"))
    {
      return ApiLoader.loadChargebackDates(userName,ALL_MEMBER_IDS,activityDate,activityDate);
    }
    else if (loadType.equals("transactions"))
    {
      return ApiLoader.loadTransactionDates(userName,ALL_MEMBER_IDS,activityDate,activityDate);
    }
    else if (loadType.equals("declines"))
    {
      return ApiLoader.loadDeclineDates(userName,ALL_MEMBER_IDS,activityDate,activityDate);
    }
    throw new RuntimeException("Unknown load type '" + loadType + "'");
  }
  
  // used by timed event, takes a load type parameter that determines type to load
  public static boolean reload(Date beginDate, Date endDate) throws Exception
  {
    ApiLoader.loadTransactionDates("reload",ALL_MEMBER_IDS,beginDate,endDate);
    ApiLoader.loadChargebackDates ("reload",ALL_MEMBER_IDS,beginDate,endDate);
    ApiLoader.loadDeclineDates    ("reload",ALL_MEMBER_IDS,beginDate,endDate);
    return(true);
  }
}