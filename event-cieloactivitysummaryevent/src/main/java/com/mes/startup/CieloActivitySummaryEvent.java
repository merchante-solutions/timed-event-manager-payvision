/*@lineinfo:filename=CieloActivitySummaryEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/CieloActivitySummaryEvent.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2012,2013 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Vector;
import com.mes.config.MesDefaults;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.mbs.BillingDb;
import com.mes.mbs.IcBillingData;
import com.mes.mbs.MerchantBilling;
import com.mes.mbs.MerchantBillingElement;
import com.mes.support.DateTimeFormatter;
import com.mes.support.ThreadPool;
import sqlj.runtime.ResultSetIterator;

public class CieloActivitySummaryEvent extends EventBase
{
  public class RevenueWorker 
    extends SQLJConnectionBase
    implements Runnable
  {
    protected   Date      ActivityDate  = null;
    protected   int       BankNumber    = 0;
    protected   long      MerchantId    = 0L;
    
    public RevenueWorker( int bankNumber, long merchantId, Date activityDate )
      throws Exception
    {
      ActivityDate  = activityDate;
      BankNumber    = bankNumber;
      MerchantId    = merchantId;
    }
    
    public void run()
    {
      double                  amount            = 0.0;
      HashMap                 betData           = null;
      IcBillingData           betItem           = null;
      int                     betNumber         = 0;
      MerchantBilling         billingData       = null;
      int                     count             = 0;
      MerchantBillingElement  el                = null;
      double                  feesDue           = 0.0;
      ResultSetIterator       it                = null;
      String                  itemSubclass      = null;
      int                     itemType          = -1;
      int                     lastDay           = 0;
      ResultSet               resultSet         = null;
      
      try
      {
        connect(true);
        
        /*@lineinfo:generated-code*//*@lineinfo:77^9*/

//  ************************************************************
//  #sql [Ctx] { select  decode(:ActivityDate,last_day(:ActivityDate),1,0) as last_day
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode( :1 ,last_day( :2 ),1,0) as last_day\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,ActivityDate);
   __sJT_st.setDate(2,ActivityDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   lastDay = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:82^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:84^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.ic_bet_visa                        as bet_number,
//                    nvl(mbe.data_item_type,mbe.item_type) as item_type,
//                    ds.item_subclass                      as item_subclass,
//                    decode(nvl(ds.item_count,0),0,ds.sales_count,ds.item_count)
//                                                          as sales_count,
//                    decode(nvl(ds.item_amount,0),0,ds.sales_amount,ds.item_amount)
//                                                          as sales_amount,                  
//                    ds.credits_count                      as credits_count,
//                    ds.credits_amount                     as credits_amount,
//                    case
//                      when ds.item_type = 111 then ( decode(substr(ds.item_subclass,1,1),'V','VS','M','MC',ds.item_subclass) || lpad(ds.ic_cat,5,'0') )
//                      else null                      
//                    end                                   as ic_label,
//                    mf.ic_bet_visa                        as bet_number,
//                    (ds.sales_count + ds.credits_count)   as net_count,
//                    (ds.sales_amount - ds.credits_amount) as net_amount
//            from    mbs_daily_summary   ds,
//                    mbs_elements        mbe,
//                    mif                 mf
//            where   ds.merchant_number = :MerchantId
//                    and ds.activity_date = :ActivityDate
//                    and nvl(mbe.data_item_type,mbe.item_type) = ds.item_type
//                    and mf.merchant_number = ds.merchant_number
//            order by ds.item_type        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.ic_bet_visa                        as bet_number,\n                  nvl(mbe.data_item_type,mbe.item_type) as item_type,\n                  ds.item_subclass                      as item_subclass,\n                  decode(nvl(ds.item_count,0),0,ds.sales_count,ds.item_count)\n                                                        as sales_count,\n                  decode(nvl(ds.item_amount,0),0,ds.sales_amount,ds.item_amount)\n                                                        as sales_amount,                  \n                  ds.credits_count                      as credits_count,\n                  ds.credits_amount                     as credits_amount,\n                  case\n                    when ds.item_type = 111 then ( decode(substr(ds.item_subclass,1,1),'V','VS','M','MC',ds.item_subclass) || lpad(ds.ic_cat,5,'0') )\n                    else null                      \n                  end                                   as ic_label,\n                  mf.ic_bet_visa                        as bet_number,\n                  (ds.sales_count + ds.credits_count)   as net_count,\n                  (ds.sales_amount - ds.credits_amount) as net_amount\n          from    mbs_daily_summary   ds,\n                  mbs_elements        mbe,\n                  mif                 mf\n          where   ds.merchant_number =  :1 \n                  and ds.activity_date =  :2 \n                  and nvl(mbe.data_item_type,mbe.item_type) = ds.item_type\n                  and mf.merchant_number = ds.merchant_number\n          order by ds.item_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   __sJT_st.setDate(2,ActivityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.CieloActivitySummaryEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:110^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          betNumber     = resultSet.getInt("bet_number");
          itemType      = resultSet.getInt("item_type");
          itemSubclass  = resultSet.getString("item_subclass"); 
          
          if ( itemType == 111 )    // interchange
          {
            if ( betData == null )    // load the BET
            {
              betData = BillingDb.loadIcBillingData(BankNumber,betNumber,ActivityDate);
            }
            betItem = (betData == null) ? null : (IcBillingData)betData.get(resultSet.getString("ic_label"));
        
            if ( betItem == null ) continue;    // skip entries with no billing data
        
            if ( betItem.ApplyToReturns == true )
            {
              count   = resultSet.getInt("net_count");
              amount  = resultSet.getDouble("net_amount");
            }          
            else  // sales only
            {
              count   = resultSet.getInt("sales_count");
              amount  = resultSet.getDouble("sales_amount");
            }
            double rate    = betItem.getRate();
            double perItem = betItem.getPerItem();
        
            if ( rate != 0.0 || perItem != 0.0 )
            {
              feesDue += ((amount * rate * 0.01) + (count * perItem));
            }          
          }
          else    // all other types
          {
            if ( billingData == null )
            {
              billingData = BillingDb.loadMerchantBilling(MerchantId,ActivityDate);
            }
            boolean useDefault  = !("DB".equals(itemSubclass) || "EB".equals(itemSubclass));
            
            el = billingData.findItemByType(itemType,itemSubclass,useDefault);
            if ( el != null )
            {
              switch( el.getVolumeType() )
              {
                case MerchantBillingElement.VT_CREDITS_ONLY:
                  amount  = resultSet.getDouble("credits_amount");
                  count   = resultSet.getInt   ("credits_count");
                  break;
              
                case MerchantBillingElement.VT_SALES_PLUS_CREDITS:
                  amount  = resultSet.getDouble("sales_amount") + resultSet.getDouble("credits_amount");
                  count   = resultSet.getInt   ("sales_count")  + resultSet.getInt   ("credits_count");
                  break;
              
                case MerchantBillingElement.VT_SALES_MINUS_CREDITS:
                  amount  = resultSet.getDouble("sales_amount") - resultSet.getDouble("credits_amount");
                  count   = resultSet.getInt   ("sales_count")  - resultSet.getInt   ("credits_count");
                  break;
              
                // case MerchantBillingElement.VT_SALES_ONLY:
                default:    
                  amount  = resultSet.getDouble("sales_amount");
                  count   = resultSet.getInt   ("sales_count");
                  break;
              }
              feesDue += (count * el.getPerItem());
              feesDue += (amount * el.getRate() * 0.01);
            }
          }
        }   // end while( resultSet.next() )
        resultSet.close();
        it.close();
        
        if ( lastDay == 1 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:191^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  case
//                        when last_day(:ActivityDate) between mcr.start_date and mcr.end_date then 1
//                        else 0
//                      end                         as armed,
//                      case
//                        when substr(mcr.billing_frequency,to_char(:ActivityDate,'mm'),1) = 'Y' then 1
//                        else 0
//                      end                         as active,
//                      (
//                        decode( nvl(mcr.item_count,0),
//                                0, 1, -- unused, default to 1 item
//                                mcr.item_count) *
//                        mcr.charge_amount
//                      )                           as fees_due
//              from    mbs_charge_records    mcr
//              where   mcr.merchant_number = :MerchantId
//                      and mcr.end_date >= trunc(:ActivityDate,'month')
//                      and upper(nvl(mcr.enabled,'Y')) = 'Y'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  case\n                      when last_day( :1 ) between mcr.start_date and mcr.end_date then 1\n                      else 0\n                    end                         as armed,\n                    case\n                      when substr(mcr.billing_frequency,to_char( :2 ,'mm'),1) = 'Y' then 1\n                      else 0\n                    end                         as active,\n                    (\n                      decode( nvl(mcr.item_count,0),\n                              0, 1, -- unused, default to 1 item\n                              mcr.item_count) *\n                      mcr.charge_amount\n                    )                           as fees_due\n            from    mbs_charge_records    mcr\n            where   mcr.merchant_number =  :3 \n                    and mcr.end_date >= trunc( :4 ,'month')\n                    and upper(nvl(mcr.enabled,'Y')) = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,ActivityDate);
   __sJT_st.setDate(2,ActivityDate);
   __sJT_st.setLong(3,MerchantId);
   __sJT_st.setDate(4,ActivityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.CieloActivitySummaryEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:211^11*/
          resultSet = it.getResultSet();
          
          while( resultSet.next() )
          {
            if ( resultSet.getInt("armed") == 1 && resultSet.getInt("active") == 1 )
            {
              feesDue += resultSet.getDouble("fees_due");
            }
          }
          resultSet.close();
          it.close();
        }
        
        addNetRevenue(feesDue);
      }
      catch( Exception e )
      {
        System.out.println(itemType + "," + itemSubclass + "," + feesDue);//@
        logEntry("run(" + MerchantId + "," + ActivityDate + ")",e.toString());
      }
      finally
      {
        try{ it.close(); } catch( Exception ee ) {}
        cleanUp();
      }
    }
  }
  
  public class SimpleRevenueWorker 
    extends SQLJConnectionBase
    implements Runnable
  {
    protected   Date      ActivityDate  = null;
    protected   int       BankNumber    = 0;
    protected   long      MerchantId    = 0L;
    
    public SimpleRevenueWorker( int bankNumber, long merchantId, Date activityDate )
      throws Exception
    {
      ActivityDate  = activityDate;
      BankNumber    = bankNumber;
      MerchantId    = merchantId;
    }
    
    public void run()
    {
      double                  bp                = 0.0;
      double                  netRevenue        = 0.0;
      
      try
      {
        connect(true);
        
        /*@lineinfo:generated-code*//*@lineinfo:265^9*/

//  ************************************************************
//  #sql [Ctx] { --select  round(((gn.t1_tot_income-gn.t1_tot_exp_interchange)/gn.t1_tot_amount_of_sales),5) as bp
//            select  (sum(gn.t1_tot_income-gn.t1_tot_exp_interchange)/sum(gn.t1_tot_amount_of_sales)) as bp
//            
//            from    monthly_extract_gn    gn
//            where   gn.hh_bank_number = :BankNumber
//                    and gn.hh_active_date = trunc(trunc(:ActivityDate,'month')-1,'month')
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "--select  round(((gn.t1_tot_income-gn.t1_tot_exp_interchange)/gn.t1_tot_amount_of_sales),5) as bp\n          select  (sum(gn.t1_tot_income-gn.t1_tot_exp_interchange)/sum(gn.t1_tot_amount_of_sales)) as bp\n           \n          from    monthly_extract_gn    gn\n          where   gn.hh_bank_number =  :1 \n                  and gn.hh_active_date = trunc(trunc( :2 ,'month')-1,'month')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,BankNumber);
   __sJT_st.setDate(2,ActivityDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bp = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:273^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:275^9*/

//  ************************************************************
//  #sql [Ctx] { select  round((sum(sm.bank_sales_amount) * :bp),2) as net_revenue
//            
//            from    daily_detail_file_summary sm
//            where   sm.merchant_number = :MerchantId
//                    and sm.batch_date = :ActivityDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  round((sum(sm.bank_sales_amount) *  :1 ),2) as net_revenue\n           \n          from    daily_detail_file_summary sm\n          where   sm.merchant_number =  :2 \n                  and sm.batch_date =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDouble(1,bp);
   __sJT_st.setLong(2,MerchantId);
   __sJT_st.setDate(3,ActivityDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   netRevenue = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:282^9*/
        addNetRevenue(netRevenue);
      }
      catch( Exception e )
      {
        logEntry("run(" + MerchantId + "," + ActivityDate + ")",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
  }
  
  public static class ActivityData
  {
  
    double        AchAmount           = 0.0;
    int           AchCount            = 0;
    Date          ActivityDate        = null;
    int           BankNumber          = 0;
    double        CbIncomingAmount    = 0.0;
    int           CbIncomingCount     = 0;
    double        CbWorkedAmount      = 0.0;
    int           CbWorkedCount       = 0;
    int           ClosedCount         = 0;
    double        FxRevenue           = 0.0;
    double        FxProcRevenue       = 0.0;
    int           OpenedCount         = 0;
    double        ProcRevenue         = 0.0;
    double        SalesAmount         = 0.0;
    int           SalesCount          = 0;
  
    public ActivityData( int bankNumber, Date activityDate )
    {
      BankNumber      = bankNumber;
      ActivityDate    = activityDate; 
    }
    
    public void setAch( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AchCount  = resultSet.getInt("ach_count");
      AchAmount = resultSet.getDouble("ach_amount");
    }
    
    public void setFxRevenue( ResultSet resultSet )
      throws java.sql.SQLException
    {
      FxRevenue     = resultSet.getDouble("fx_revenue");
      FxProcRevenue = resultSet.getDouble("fx_proc_revenue");
    }
    
    public void setIncomingChargebacks( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CbIncomingCount  = resultSet.getInt("incoming_count");
      CbIncomingAmount = resultSet.getDouble("incoming_amount");
    }
    
    public void setOpenClosed( ResultSet resultSet )
      throws java.sql.SQLException
    {
      OpenedCount  = resultSet.getInt("opened_count");
      ClosedCount  = resultSet.getInt("closed_count");
    }
    
    public void setProcessingRevenue( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ProcRevenue  = resultSet.getDouble("proc_revenue");
    }
    
    public void setSales( ResultSet resultSet )
      throws java.sql.SQLException
    {
      SalesCount  = resultSet.getInt("sales_count");
      SalesAmount = resultSet.getDouble("sales_amount");
    }
    
    public void setWorkedChargebacks( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CbWorkedCount  = resultSet.getInt("mchb_count");
      CbWorkedAmount = resultSet.getDouble("mchb_amount");
    }
  }
  
  private double    FxBp            = 0.03900;
  private double    FxProcBp        = 0.01296;
  private double    NetRevenue      = 0.0;
  private double    ProcBp          = 0.00205;
  
  protected synchronized void addNetRevenue( double revenue )
  {
    NetRevenue += revenue;
  }
  
  public boolean execute( )
  {
    boolean     retVal        = false;
  
    try
    {
      connect(true);
    
      Date  activityDate    = null;
      int   bankNumber      = Integer.parseInt(getEventArg(0));
      
      /*@lineinfo:generated-code*//*@lineinfo:391^7*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(sysdate-1)  
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc(sysdate-1)  \n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:396^7*/
      
      loadData( bankNumber, activityDate );
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  public double getNetRevenue()
  {
    return( NetRevenue );
  }
    
  public void loadData( int bankNumber, Date activityDate)
  {
    ResultSetIterator   it              = null;
    ResultSet           resultSet       = null;
    ActivityData        summaryData     = null;
    
    try
    {
      NetRevenue  = 0.0;    // reset the net revenue
      summaryData = new ActivityData(9999,activityDate);
      
      /*@lineinfo:generated-code*//*@lineinfo:428^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum(sm.bank_sales_count
//                      + sm.nonbank_sales_count
//                      - sm.bank_reject_sales_count
//                      - sm.nonbank_reject_sales_count)  as sales_count,
//                  sum(sm.bank_sales_amount
//                      + sm.nonbank_sales_amount
//                      - sm.bank_reject_sales_amount
//                      - sm.nonbank_reject_sales_amount) as sales_amount
//          from    daily_detail_file_summary   sm
//          where   sm.merchant_number in 
//                  (
//                    select  merchant_number 
//                    from    mif 
//                    where   bank_number in (select bank_number from mbs_banks where owner = 1)
//                            and sic_code not in ('6010','6011') 
//                  )
//                  and sm.batch_date = :activityDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum(sm.bank_sales_count\n                    + sm.nonbank_sales_count\n                    - sm.bank_reject_sales_count\n                    - sm.nonbank_reject_sales_count)  as sales_count,\n                sum(sm.bank_sales_amount\n                    + sm.nonbank_sales_amount\n                    - sm.bank_reject_sales_amount\n                    - sm.nonbank_reject_sales_amount) as sales_amount\n        from    daily_detail_file_summary   sm\n        where   sm.merchant_number in \n                (\n                  select  merchant_number \n                  from    mif \n                  where   bank_number in (select bank_number from mbs_banks where owner = 1)\n                          and sic_code not in ('6010','6011') \n                )\n                and sm.batch_date =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.startup.CieloActivitySummaryEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:447^7*/
      resultSet = it.getResultSet();
      resultSet.next();
      summaryData.setSales(resultSet);
      resultSet.close();
      it.close();
              
      /*@lineinfo:generated-code*//*@lineinfo:454^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum(case when mmddyy_to_date(mf.date_opened) = :activityDate then 1 else 0 end) as opened_count,
//                  sum(case when mf.date_stat_chgd_to_dcb = :activityDate then 1 else 0 end)       as closed_count
//          from    mif     mf
//          where   mf.bank_number in (select bank_number from mbs_banks where owner = 1)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum(case when mmddyy_to_date(mf.date_opened) =  :1  then 1 else 0 end) as opened_count,\n                sum(case when mf.date_stat_chgd_to_dcb =  :2  then 1 else 0 end)       as closed_count\n        from    mif     mf\n        where   mf.bank_number in (select bank_number from mbs_banks where owner = 1)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activityDate);
   __sJT_st.setDate(2,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.CieloActivitySummaryEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:460^7*/
      resultSet = it.getResultSet();
      resultSet.next();
      summaryData.setOpenClosed(resultSet);
      resultSet.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:467^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(1)                as incoming_count,
//                  sum(cb.tran_amount)     as incoming_amount
//          from    network_chargebacks   cb
//          where   cb.bank_number in (select bank_number from mbs_banks where owner = 1)
//                  and cb.incoming_date = :activityDate
//                  and cb.first_time_chargeback = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(1)                as incoming_count,\n                sum(cb.tran_amount)     as incoming_amount\n        from    network_chargebacks   cb\n        where   cb.bank_number in (select bank_number from mbs_banks where owner = 1)\n                and cb.incoming_date =  :1 \n                and cb.first_time_chargeback = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.startup.CieloActivitySummaryEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:475^7*/
      resultSet = it.getResultSet();
      resultSet.next();
      summaryData.setIncomingChargebacks(resultSet);
      resultSet.close();
      it.close();
        
      /*@lineinfo:generated-code*//*@lineinfo:482^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(1)                as mchb_count,
//                  sum(cb.tran_amount)     as mchb_amount
//          from    network_chargebacks           cb,
//                  network_chargeback_activity   cba
//          where   cb.bank_number in (select bank_number from mbs_banks where owner = 1)
//                  and cb.incoming_date between :activityDate-180 and :activityDate
//                  and cba.cb_load_sec = cb.cb_load_sec
//                  and cba.action_date = :activityDate
//                  and cba.action_code = 'D'        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(1)                as mchb_count,\n                sum(cb.tran_amount)     as mchb_amount\n        from    network_chargebacks           cb,\n                network_chargeback_activity   cba\n        where   cb.bank_number in (select bank_number from mbs_banks where owner = 1)\n                and cb.incoming_date between  :1 -180 and  :2 \n                and cba.cb_load_sec = cb.cb_load_sec\n                and cba.action_date =  :3 \n                and cba.action_code = 'D'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activityDate);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setDate(3,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.startup.CieloActivitySummaryEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:493^7*/
      resultSet = it.getResultSet();
      resultSet.next();
      summaryData.setWorkedChargebacks(resultSet);
      resultSet.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:500^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(1)                    as ach_count,
//                  nvl(sum(ach.ach_amount),0)  as ach_amount
//          from    ach_trident_detail      ach
//          where   ach.merchant_number in (select merchant_number from mif where bank_number in (select bank_number from mbs_banks where owner = 1))
//                  and ach.post_date = :activityDate
//                  and ach.entry_description = 'MERCH DEP'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(1)                    as ach_count,\n                nvl(sum(ach.ach_amount),0)  as ach_amount\n        from    ach_trident_detail      ach\n        where   ach.merchant_number in (select merchant_number from mif where bank_number in (select bank_number from mbs_banks where owner = 1))\n                and ach.post_date =  :1 \n                and ach.entry_description = 'MERCH DEP'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.startup.CieloActivitySummaryEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:508^7*/
      resultSet = it.getResultSet();
      resultSet.next();
      summaryData.setAch(resultSet);
      resultSet.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:515^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (tapi idx_tapi_merch_batch_date) */
//                  nvl(round((sum(tapi.fx_amount_base) * :FxBp),2),0)     as fx_revenue,
//                  nvl(round((sum(tapi.fx_amount_base) * :FxProcBp),2),0) as fx_proc_revenue
//          from    trident_capture_api   tapi
//          where   tapi.merchant_number in 
//                  (
//                    select  mf.merchant_number 
//                    from    mif   mf 
//                    where   mf.bank_number in (select bank_number from mbs_banks where owner = 1)
//                            and mf.association_node = 3941650500  -- 2CO
//                  )
//                  and tapi.batch_date = :activityDate
//                  and tapi.mesdb_timestamp between :activityDate-30 and :activityDate+1
//                  and tapi.debit_credit_indicator = 'D'
//                  and nvl(tapi.fx_amount_base,0) != 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (tapi idx_tapi_merch_batch_date) */\n                nvl(round((sum(tapi.fx_amount_base) *  :1 ),2),0)     as fx_revenue,\n                nvl(round((sum(tapi.fx_amount_base) *  :2 ),2),0) as fx_proc_revenue\n        from    trident_capture_api   tapi\n        where   tapi.merchant_number in \n                (\n                  select  mf.merchant_number \n                  from    mif   mf \n                  where   mf.bank_number in (select bank_number from mbs_banks where owner = 1)\n                          and mf.association_node = 3941650500  -- 2CO\n                )\n                and tapi.batch_date =  :3 \n                and tapi.mesdb_timestamp between  :4 -30 and  :5 +1\n                and tapi.debit_credit_indicator = 'D'\n                and nvl(tapi.fx_amount_base,0) != 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,FxBp);
   __sJT_st.setDouble(2,FxProcBp);
   __sJT_st.setDate(3,activityDate);
   __sJT_st.setDate(4,activityDate);
   __sJT_st.setDate(5,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.startup.CieloActivitySummaryEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:532^7*/
      resultSet = it.getResultSet();
      resultSet.next();
      summaryData.setFxRevenue(resultSet);
      resultSet.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:539^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(round((sum(sm.bank_sales_amount
//                                 + sm.nonbank_sales_amount
//                                 - sm.bank_reject_sales_amount
//                                 - sm.nonbank_reject_sales_amount) * :ProcBp),2),0) as proc_revenue
//          from    daily_detail_file_summary   sm
//          where   sm.merchant_number in 
//                  (
//                    select  mf.merchant_number 
//                    from    mif   mf 
//                    where   mf.bank_number in (select bank_number from mbs_banks where owner = 2)
//                            and mf.sic_code not in ('6010','6011')
//                  )
//                  and sm.batch_date = :activityDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  nvl(round((sum(sm.bank_sales_amount\n                               + sm.nonbank_sales_amount\n                               - sm.bank_reject_sales_amount\n                               - sm.nonbank_reject_sales_amount) *  :1 ),2),0) as proc_revenue\n        from    daily_detail_file_summary   sm\n        where   sm.merchant_number in \n                (\n                  select  mf.merchant_number \n                  from    mif   mf \n                  where   mf.bank_number in (select bank_number from mbs_banks where owner = 2)\n                          and mf.sic_code not in ('6010','6011')\n                )\n                and sm.batch_date =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,ProcBp);
   __sJT_st.setDate(2,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.startup.CieloActivitySummaryEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:554^7*/
      resultSet = it.getResultSet();
      resultSet.next();
      summaryData.setProcessingRevenue(resultSet);
      resultSet.close();
      it.close();
      
      loadNetRevenue(bankNumber,activityDate);
      
      /*@lineinfo:generated-code*//*@lineinfo:563^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    cielo_activity_summary
//          where   bank_number = :bankNumber
//                  and activity_date = :activityDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    cielo_activity_summary\n        where   bank_number =  :1 \n                and activity_date =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,activityDate);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:569^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:571^7*/

//  ************************************************************
//  #sql [Ctx] { insert into cielo_activity_summary
//          (
//            bank_number,
//            activity_date,
//            sales_count,
//            sales_amount,
//            new_accounts,
//            closed_accounts,
//            incoming_chargeback_count,
//            incoming_chargeback_amount,
//            adjusted_chargeback_count,
//            adjusted_chargeback_amount,
//            ach_count,
//            ach_amount,
//            net_revenue,
//            fx_revenue,
//            fx_proc_revenue,
//            proc_revenue
//          )
//          values
//          (
//            :summaryData.BankNumber,
//            :summaryData.ActivityDate,
//            :summaryData.SalesCount,
//            :summaryData.SalesAmount,
//            :summaryData.OpenedCount,
//            :summaryData.ClosedCount,
//            :summaryData.CbIncomingCount,
//            :summaryData.CbIncomingAmount,
//            :summaryData.CbWorkedCount,
//            :summaryData.CbWorkedAmount,
//            :summaryData.AchCount,
//            :summaryData.AchAmount,
//            :getNetRevenue(),
//            :summaryData.FxRevenue,
//            :summaryData.FxProcRevenue,
//            :summaryData.ProcRevenue
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_3835 = getNetRevenue();
   String theSqlTS = "insert into cielo_activity_summary\n        (\n          bank_number,\n          activity_date,\n          sales_count,\n          sales_amount,\n          new_accounts,\n          closed_accounts,\n          incoming_chargeback_count,\n          incoming_chargeback_amount,\n          adjusted_chargeback_count,\n          adjusted_chargeback_amount,\n          ach_count,\n          ach_amount,\n          net_revenue,\n          fx_revenue,\n          fx_proc_revenue,\n          proc_revenue\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,\n           :12 ,\n           :13 ,\n           :14 ,\n           :15 ,\n           :16 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,summaryData.BankNumber);
   __sJT_st.setDate(2,summaryData.ActivityDate);
   __sJT_st.setInt(3,summaryData.SalesCount);
   __sJT_st.setDouble(4,summaryData.SalesAmount);
   __sJT_st.setInt(5,summaryData.OpenedCount);
   __sJT_st.setInt(6,summaryData.ClosedCount);
   __sJT_st.setInt(7,summaryData.CbIncomingCount);
   __sJT_st.setDouble(8,summaryData.CbIncomingAmount);
   __sJT_st.setInt(9,summaryData.CbWorkedCount);
   __sJT_st.setDouble(10,summaryData.CbWorkedAmount);
   __sJT_st.setInt(11,summaryData.AchCount);
   __sJT_st.setDouble(12,summaryData.AchAmount);
   __sJT_st.setDouble(13,__sJT_3835);
   __sJT_st.setDouble(14,summaryData.FxRevenue);
   __sJT_st.setDouble(15,summaryData.FxProcRevenue);
   __sJT_st.setDouble(16,summaryData.ProcRevenue);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:611^7*/                               
    }
    catch(Exception e)
    {
      logEntry("loadData(" + bankNumber + ", " + activityDate.toString() + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  protected void loadNetRevenue( int bankNumber, Date activityDate )
    throws Exception
  {
    Date          activeDate      = null;
    double        bp              = 0.0;
    int           lastDay         = 0;
    double        netRevenue      = 0.0;
    double        totalFees       = 0.0;
      
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:634^7*/

//  ************************************************************
//  #sql [Ctx] { select  decode(count(1),0,trunc(:activityDate,'month'),trunc(trunc(:activityDate,'month')-1,'month')) as active_date,
//                  case when last_day(:activityDate) = :activityDate then 1 else 0 end                           as last_day
//          
//          from    monthly_extract_gn  gn
//          where   gn.hh_bank_number in (select bank_number from mbs_banks where owner = 1)
//                  and gn.hh_active_date = trunc(trunc(:activityDate,'month')-1,'month')
//                  and gn.t1_tot_income != 0      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode(count(1),0,trunc( :1 ,'month'),trunc(trunc( :2 ,'month')-1,'month')) as active_date,\n                case when last_day( :3 ) =  :4  then 1 else 0 end                           as last_day\n         \n        from    monthly_extract_gn  gn\n        where   gn.hh_bank_number in (select bank_number from mbs_banks where owner = 1)\n                and gn.hh_active_date = trunc(trunc( :5 ,'month')-1,'month')\n                and gn.t1_tot_income != 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,activityDate);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setDate(3,activityDate);
   __sJT_st.setDate(4,activityDate);
   __sJT_st.setDate(5,activityDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activeDate = (java.sql.Date)__sJT_rs.getDate(1);
   lastDay = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:643^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:645^7*/

//  ************************************************************
//  #sql [Ctx] { select  sum(st.st_fee_amount)   as total_fees
//          
//          from    monthly_extract_gn    gn,
//                  monthly_extract_st    st
//          where   gn.hh_bank_number in (select bank_number from mbs_banks where owner = 1)
//                  and gn.hh_active_date = :activeDate
//                  and st.hh_load_sec = gn.hh_load_sec
//                  and ( st.st_statement_desc like '%PCI%'
//                        or st.st_statement_desc like '%IRS%'
//                        or st.st_statement_desc like '%ADMINISTRATIVE%'
//                        or st.st_statement_desc like '%COMPLIANCE%'
//                        or st.st_statement_desc like '%1099 %' )                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum(st.st_fee_amount)   as total_fees\n         \n        from    monthly_extract_gn    gn,\n                monthly_extract_st    st\n        where   gn.hh_bank_number in (select bank_number from mbs_banks where owner = 1)\n                and gn.hh_active_date =  :1 \n                and st.hh_load_sec = gn.hh_load_sec\n                and ( st.st_statement_desc like '%PCI%'\n                      or st.st_statement_desc like '%IRS%'\n                      or st.st_statement_desc like '%ADMINISTRATIVE%'\n                      or st.st_statement_desc like '%COMPLIANCE%'\n                      or st.st_statement_desc like '%1099 %' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   totalFees = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:659^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:661^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(((sum(gn.t1_tot_income-(sm.interchange_expense_enhanced+sm.vmc_assessment_expense-sm.vmc_fees))-:totalFees)
//                      /sum(gn.t1_tot_amount_of_sales)),0) as bp
//          
//          from    monthly_extract_gn        gn,
//                  monthly_extract_summary   sm
//          where   gn.hh_bank_number in (select bank_number from mbs_banks where owner = 1)
//                  and gn.hh_active_date = :activeDate
//                  and sm.merchant_number = gn.hh_merchant_number
//                  and sm.active_date = gn.hh_active_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(((sum(gn.t1_tot_income-(sm.interchange_expense_enhanced+sm.vmc_assessment_expense-sm.vmc_fees))- :1 )\n                    /sum(gn.t1_tot_amount_of_sales)),0) as bp\n         \n        from    monthly_extract_gn        gn,\n                monthly_extract_summary   sm\n        where   gn.hh_bank_number in (select bank_number from mbs_banks where owner = 1)\n                and gn.hh_active_date =  :2 \n                and sm.merchant_number = gn.hh_merchant_number\n                and sm.active_date = gn.hh_active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDouble(1,totalFees);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bp = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:672^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:674^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(round((sum(sm.bank_sales_amount
//                                 + sm.nonbank_sales_amount
//                                 - sm.bank_reject_sales_amount
//                                 - sm.nonbank_reject_sales_amount) * :bp),2),0) as net_revenue
//          
//          from    daily_detail_file_summary sm
//          where   sm.merchant_number in 
//                  (
//                    select  mf.merchant_number 
//                    from    mif   mf 
//                    where   mf.bank_number in (select bank_number from mbs_banks where owner = 1)
//                            and mf.sic_code not in ('6010','6011')
//                  )
//                  and sm.batch_date = :activityDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(round((sum(sm.bank_sales_amount\n                               + sm.nonbank_sales_amount\n                               - sm.bank_reject_sales_amount\n                               - sm.nonbank_reject_sales_amount) *  :1 ),2),0) as net_revenue\n         \n        from    daily_detail_file_summary sm\n        where   sm.merchant_number in \n                (\n                  select  mf.merchant_number \n                  from    mif   mf \n                  where   mf.bank_number in (select bank_number from mbs_banks where owner = 1)\n                          and mf.sic_code not in ('6010','6011')\n                )\n                and sm.batch_date =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDouble(1,bp);
   __sJT_st.setDate(2,activityDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   netRevenue = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:690^7*/
      addNetRevenue(netRevenue);
      
      if ( lastDay == 1 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:695^9*/

//  ************************************************************
//  #sql [Ctx] { select  sum(charge_amount)    as net_fees
//            
//            from    mbs_charge_records    mcr,
//                    mif                   mf
//            where   substr(mcr.billing_frequency,to_char(:activityDate,'mm'),1) = 'Y'
//                    and :activityDate between mcr.start_date and mcr.end_date
//                    and ( mcr.statement_message like '%PCI%'
//                          or mcr.statement_message like '%IRS%'
//                          or mcr.statement_message like '%COMPLIANCE%'
//                          or mcr.statement_message like '%1099 %' )                
//                    and mf.merchant_number = mcr.merchant_number
//                    and mf.dmacctst is null                    
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum(charge_amount)    as net_fees\n           \n          from    mbs_charge_records    mcr,\n                  mif                   mf\n          where   substr(mcr.billing_frequency,to_char( :1 ,'mm'),1) = 'Y'\n                  and  :2  between mcr.start_date and mcr.end_date\n                  and ( mcr.statement_message like '%PCI%'\n                        or mcr.statement_message like '%IRS%'\n                        or mcr.statement_message like '%COMPLIANCE%'\n                        or mcr.statement_message like '%1099 %' )                \n                  and mf.merchant_number = mcr.merchant_number\n                  and mf.dmacctst is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,activityDate);
   __sJT_st.setDate(2,activityDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   netRevenue = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:709^9*/
        addNetRevenue(netRevenue);
      }
    }
    catch( Exception e )
    {
      logEntry("loadNetRevenue(" + bankNumber + "," + activityDate + ")",e.toString());
      throw(e);
    }
    finally
    {
    }
  }
  
  protected void loadNetRevenue_fancy( int bankNumber, Date activityDate )
    throws Exception
  {
    ResultSetIterator       it                = null;
    Vector                  mids              = new Vector();
    ThreadPool              pool              = null;
    ResultSet               resultSet         = null;
      
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:733^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mf.merchant_number
//          from    mif         mf
//          where   mf.bank_number = :bankNumber
//                  and exists
//                  (
//                    select  ds.merchant_number
//                    from    mbs_daily_summary ds
//                    where   ds.merchant_number = mf.merchant_number
//                            and ds.activity_date = :activityDate
//                  )
//                  --and mf.merchant_number = 941000108061 -- dr
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mf.merchant_number\n        from    mif         mf\n        where   mf.bank_number =  :1 \n                and exists\n                (\n                  select  ds.merchant_number\n                  from    mbs_daily_summary ds\n                  where   ds.merchant_number = mf.merchant_number\n                          and ds.activity_date =  :2 \n                )\n                --and mf.merchant_number = 941000108061 -- dr";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.startup.CieloActivitySummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.startup.CieloActivitySummaryEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:746^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        mids.addElement(resultSet.getString("merchant_number"));
      }
      resultSet.close();
      it.close();
      
      pool = new ThreadPool(10);
      for( int i = 0; i < mids.size(); ++i )
      {
        Thread thread = pool.getThread( new SimpleRevenueWorker(bankNumber,Long.parseLong((String)mids.elementAt(i)),activityDate) );
        thread.start(); 
      }
      pool.waitForAllThreads();
    }
    catch( Exception e )
    {
      logEntry("loadNetRevenue(" + bankNumber + "," + activityDate + ")",e.toString());
      throw(e);
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  public static void main(String[] args)
  {
    CieloActivitySummaryEvent         testEvent   = null;

    try
    {
        if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{
                    MesDefaults.BILLING_DB_FTPDB
            });
        }

      SQLJConnectionBase.initStandalonePool();
      
      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      
      testEvent = new CieloActivitySummaryEvent();
      testEvent.connect(true);

      if ( "execute".equals(args[0]) )
      {
        testEvent.setEventArgs(args[1]);
        testEvent.execute();
      }
      else if ( "backfill".equals(args[0]) )
      {
        Calendar        cal           = Calendar.getInstance();
        int             bankNumber    = Integer.parseInt(args[1]);
        java.util.Date  beginDate     = DateTimeFormatter.parseDate(args[2],"MM/dd/yyyy");
        
        cal.setTime(DateTimeFormatter.parseDate(args[3],"MM/dd/yyyy"));
        
        while( !cal.getTime().before(beginDate) )
        {
          System.out.print("Loading " + DateTimeFormatter.getFormattedDate(cal.getTime(),"MM/dd/yyyy") + "           \r");
          testEvent.loadData(bankNumber,new java.sql.Date(cal.getTime().getTime()));
          cal.add(Calendar.DAY_OF_MONTH,-1);
        }
        System.out.println();
      }
      else if ( "loadNetRevenue".equals(args[0]) )
      {
        Calendar        cal           = Calendar.getInstance();
        int             bankNumber    = Integer.parseInt(args[1]);
        java.util.Date  beginDate     = DateTimeFormatter.parseDate(args[2],"MM/dd/yyyy");
        
        cal.setTime(DateTimeFormatter.parseDate(args[3],"MM/dd/yyyy"));
        
        while( !cal.getTime().before(beginDate) )
        {
          System.out.print("Loading " + DateTimeFormatter.getFormattedDate(cal.getTime(),"MM/dd/yyyy") + "           \r");
          testEvent.loadNetRevenue(bankNumber,new java.sql.Date(cal.getTime().getTime()));
          cal.add(Calendar.DAY_OF_MONTH,-1);
        }
        System.out.println();
        System.out.println("Net Revenue : " + testEvent.getNetRevenue());
      }
      
      Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      long elapsed = (endTime.getTime() - beginTime.getTime());
      
      System.out.println("Begin Time: " + String.valueOf(beginTime));
      System.out.println("End Time  : " + String.valueOf(endTime));
      System.out.println("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));
    }
    catch(Exception e)
    {
    }
    finally
    {
      try{ testEvent.cleanUp(); } catch(Exception e){}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/