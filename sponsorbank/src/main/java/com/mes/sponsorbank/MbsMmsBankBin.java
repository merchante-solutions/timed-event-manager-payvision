package com.mes.sponsorbank;

public class MbsMmsBankBin {
	private String appSource;
	private Integer bankNumber;
	private String mmsBankBin;

	public String getAppSource() {
		return appSource;
	}

	public void setAppSource(String appSource) {
		this.appSource = appSource;
	}

	public Integer getBankNumber() {
		return bankNumber;
	}

	public void setBankNumber(Integer bankNumber) {
		this.bankNumber = bankNumber;
	}

	public String getMmsBankBin() {
		return mmsBankBin;
	}

	public void setMmsBankBin(String mmsBankBin) {
		this.mmsBankBin = mmsBankBin;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MbsMmsBankBin [appSource=").append(appSource).append(", bankNumber=").append(bankNumber).append(", mmsBankBin=").append(mmsBankBin)
				.append("]");
		return builder.toString();
	}

}
