package com.mes.sponsorbank;

public class MbsBankContact {
	private String addressLine1;
	private String addressLine2;
	private Integer bankNumber;
	private String city;
	private String emailAddress;
	private String helpInfo;
	private String helpName;
	private String name;
	private String phoneNumber;
	private String postalCode;
	private String state;

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public Integer getBankNumber() {
		return bankNumber;
	}

	public void setBankNumber(Integer bankNumber) {
		this.bankNumber = bankNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getHelpInfo() {
		return helpInfo;
	}

	public void setHelpInfo(String helpInfo) {
		this.helpInfo = helpInfo;
	}

	public String getHelpName() {
		return helpName;
	}

	public void setHelpName(String helpName) {
		this.helpName = helpName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public int hashCode() {
		final int prime = 127;
		return prime * ((bankNumber == null) ? 0 : bankNumber.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MbsBankContact other = (MbsBankContact) obj;

		if (bankNumber == null) {
			if (other.bankNumber != null)
				return false;
		}
		else if (!bankNumber.equals(other.bankNumber))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MbsBankContact [addressLine1=").append(addressLine1).append(", addressLine2=").append(addressLine2).append(", bankNumber=")
				.append(bankNumber).append(", city=").append(city).append(", helpInfo=").append(helpInfo).append(", helpName=").append(helpName)
				.append(", postalCode=").append(postalCode).append(", state=").append(state).append("]");
		return builder.toString();
	}

}
