package com.mes.sponsorbank;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

public class MbsBanksCache {

	private static MbsBanksCache instance = null;
	private MbsBanksDomainInterface sponsorBankDomain;
	static Logger log = Logger.getLogger(MbsBanksCache.class);

	private Map<String, List<Integer>> enabledBankMap = new ConcurrentHashMap<>();
	private Map<Integer, List<MbsBankContact>> bankContactMap = new ConcurrentHashMap<>();
	private Map<String, List<MbsBankGroupAttributes>> bankGroupAttrMap = new ConcurrentHashMap<>();
	private Map<Integer, List<MbsMmsBankBin>> bankBinMap = new ConcurrentHashMap<>();
	private Map<Integer, MbsBank> sponsorBankMap = new ConcurrentHashMap<>();

	private List<MbsBank> activeBanks = null;

	private MbsBanksCache() {
		// private no-arg constructor for singleton pattern
	}

	public static MbsBanksCache getInstance() {
		if (instance == null) {
			instance = new MbsBanksCache();
			instance.sponsorBankDomain = instance.getDomainService();
		}
		return instance;
	}

	private MbsBanksDomainInterface getDomainService() {
		String classname = System.getProperty("sponsor.bank.domain.datasource", "com.mes.sponsorbank.impl.MbsBanksDAO");
		try {
			return (MbsBanksDomainInterface) Class.forName(classname).newInstance();
		}
		catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			log.error("[getDomainService()] - Unexpected exception when attempting to create instance of MbsBanksDomainInterface", e);
			throw new NoDomainInterfaceException(classname);
		}
	}

	public List<MbsBank> getActiveBanks() {
		if (CollectionUtils.isNotEmpty(activeBanks)) {
			return activeBanks;
		}
		activeBanks = sponsorBankDomain.getActiveBanks();
		return activeBanks;
	}

	public List<MbsBankContact> getBankContact(Integer bankNumber) {
		if (log.isDebugEnabled()) {
			log.debug("[getBankContact()] - bankNumber = " + bankNumber);
		}
		if (bankContactMap.containsKey(bankNumber)) {
			return bankContactMap.get(bankNumber);
		}
		List<MbsBankContact> contacts = sponsorBankDomain.getBankContact(bankNumber);
		if (CollectionUtils.isNotEmpty(contacts)) {
			bankContactMap.put(bankNumber, contacts);
		}
		return contacts;
	}

	public List<Integer> getEnabledBanks(String eventClassname) {
		if (log.isDebugEnabled()) {
			log.debug("[getEnabledBanks()] - eventClassname=" + eventClassname);
		}
		if (enabledBankMap.containsKey(eventClassname)) {
			return enabledBankMap.get(eventClassname);
		}
		List<Integer> enabledBanks = sponsorBankDomain.getEnabledBanks(eventClassname);
		if (CollectionUtils.isNotEmpty(enabledBanks)) {
			enabledBankMap.put(eventClassname, enabledBanks);
		}
		return enabledBanks;
	}

	public List<MbsBankGroupAttributes> getBankGroupAssnAttributes(Integer bankNumber, String eventClassname) {
		String key = String.format("%s.%s", bankNumber, eventClassname);
		if (log.isDebugEnabled()) {
			log.debug(String.format("[getEnabledBanks()] - bankNumber=%s, eventClassname=%s", bankNumber, eventClassname));
		}
		if (bankGroupAttrMap.containsKey(key)) {
			return bankGroupAttrMap.get(key);
		}
		List<MbsBankGroupAttributes> attributes = sponsorBankDomain.getBankGrpAssnAttributes(bankNumber, eventClassname);
		if (CollectionUtils.isNotEmpty(attributes)) {
			bankGroupAttrMap.put(key, attributes);
		}
		return attributes;
	}

	public List<MbsMmsBankBin> getMMSActiveBanks(Integer bankNumber) {
		if (log.isDebugEnabled()) {
			log.debug(String.format("[getMMSActiveBanks()] - bankNumber=%s", bankNumber));
		}
		if (bankBinMap.containsKey(bankNumber)) {
			return bankBinMap.get(bankNumber);
		}
		List<MbsMmsBankBin> mmsBankBins = sponsorBankDomain.getMMSActiveBanks(bankNumber);
		if (CollectionUtils.isNotEmpty(mmsBankBins)) {
			bankBinMap.put(bankNumber, mmsBankBins);
		}
		return mmsBankBins;
	}

	public MbsBank getSponsorBank(Integer bankNumber) {
		if (log.isDebugEnabled()) {
			log.debug(String.format("[getSponsorBank()] - bankNumber=%s", bankNumber));
		}
		if (sponsorBankMap.containsKey(bankNumber)) {
			return sponsorBankMap.get(bankNumber);
		}
		MbsBank bank = sponsorBankDomain.getSponsorBank(bankNumber);
		if (bank != null) {
			sponsorBankMap.put(bankNumber, bank);
		}
		return bank;
	}
}
