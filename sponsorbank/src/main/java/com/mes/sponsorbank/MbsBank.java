package com.mes.sponsorbank;

import java.util.ArrayList;
import java.util.List;

public class MbsBank {
	private Boolean achHolidayFlag;
	private Boolean active;
	private Boolean altAssn;
	private Boolean altAddress;
	private Boolean altMerchantLegalName;
	private Boolean altMerchantName;
	private String amexAggregators;
	private Boolean amexInterchangeOverride;
	private String authBillingVendorId;
	private Integer authPaymentGatewayBillingBank;
	private Integer bankNumber;
	private String defaultCieloChannelCategory;
	private Integer discoverAcquirerId;
	private String discoverMailboxId;
	private Integer masterCardBinInet;
	private Integer masterCardIcaNumber;
	private Integer masterCardPinDebit;
	private String merchantPrefix;
	private Boolean nonBankCards;
	private Integer owner;
	private Integer processorId;
	private List<Integer> traits = new ArrayList<>();
	private Integer visaBinBaseII;
	private Integer visaBinPinDebit;
	private Integer visaAquirerId;
	private Integer visaSreNumber;

	public Boolean getAchHolidayFlag() {
		return achHolidayFlag;
	}

	public void setAchHolidayFlag(Boolean achHolidayFlag) {
		this.achHolidayFlag = achHolidayFlag;
	}

	public void setAchHolidayFlag(String achHolidayFlag) {
		this.achHolidayFlag = "Y".equalsIgnoreCase(achHolidayFlag);
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public void setActive(String active) {
		this.active = "Y".equalsIgnoreCase(active);
	}

	public Boolean getAltAssocication() {
		return altAssn;
	}

	public void setAltAssocication(Boolean altAssocication) {
		this.altAssn = altAssocication;
	}

	public void setAltAssocication(String altAssocication) {
		this.altAssn = "Y".equalsIgnoreCase(altAssocication);
	}

	public Boolean getAltAddress() {
		return altAddress;
	}

	public void setAltAddress(Boolean altAddress) {
		this.altAddress = altAddress;
	}

	public void setAltAddress(String altAddress) {
		this.altAddress = "Y".equalsIgnoreCase(altAddress);
	}

	public Boolean getAltMerchantLegalName() {
		return altMerchantLegalName;
	}

	public void setAltMerchantLegalName(Boolean altMerchantLegalName) {
		this.altMerchantLegalName = altMerchantLegalName;
	}

	public void setAltMerchantLegalName(String altMerchantLegalName) {
		this.altMerchantLegalName = "Y".equalsIgnoreCase(altMerchantLegalName);
	}

	public Boolean getAltMerchantName() {
		return altMerchantName;
	}

	public void setAltMerchantName(Boolean altMerchantName) {
		this.altMerchantName = altMerchantName;
	}

	public void setAltMerchantName(String altMerchantName) {
		this.altMerchantName = "Y".equalsIgnoreCase(altMerchantName);
	}

	public String getAmexAggregators() {
		return amexAggregators;
	}

	public void setAmexAggregators(String amexAggregators) {
		this.amexAggregators = amexAggregators;
	}

	public Boolean getAmexInterchangeOverride() {
		return amexInterchangeOverride;
	}

	public void setAmexInterchangeOverride(Boolean amexInterchangeOverride) {
		this.amexInterchangeOverride = amexInterchangeOverride;
	}

	public void setAmexInterchangeOverride(String amexInterchangeOverride) {
		this.amexInterchangeOverride = "Y".equalsIgnoreCase(amexInterchangeOverride);
	}

	public String getAuthBillingVendorId() {
		return authBillingVendorId;
	}

	public void setAuthBillingVendorId(String authBillingVendorId) {
		this.authBillingVendorId = authBillingVendorId;
	}

	public Integer getAuthPaymentGatewayBillingBank() {
		return authPaymentGatewayBillingBank;
	}

	public void setAuthPaymentGatewayBillingBank(Integer authPaymentGatewayBillingBank) {
		this.authPaymentGatewayBillingBank = authPaymentGatewayBillingBank;
	}

	public Integer getBankNumber() {
		return bankNumber;
	}

	public void setBankNumber(Integer bankNumber) {
		this.bankNumber = bankNumber;
	}

	public String getDefaultCieloChannelCategory() {
		return defaultCieloChannelCategory;
	}

	public void setDefaultCieloChannelCategory(String defaultCieloChannelCategory) {
		this.defaultCieloChannelCategory = defaultCieloChannelCategory;
	}

	public Integer getDiscoverAcquirerId() {
		return discoverAcquirerId;
	}

	public void setDiscoverAcquirerId(Integer discoverAcquirerId) {
		this.discoverAcquirerId = discoverAcquirerId;
	}

	public String getDiscoverMailboxId() {
		return discoverMailboxId;
	}

	public void setDiscoverMailboxId(String discoverMailboxId) {
		this.discoverMailboxId = discoverMailboxId;
	}

	public Integer getMasterCardBinInet() {
		return masterCardBinInet;
	}

	public void setMasterCardBinInet(Integer masterCardBinInet) {
		this.masterCardBinInet = masterCardBinInet;
	}

	public Integer getMasterCardIcaNumber() {
		return masterCardIcaNumber;
	}

	public void setMasterCardIcaNumber(Integer masterCardIcaNumber) {
		this.masterCardIcaNumber = masterCardIcaNumber;
	}

	public Integer getMasterCardPinDebit() {
		return masterCardPinDebit;
	}

	public void setMasterCardPinDebit(Integer masterCardPinDebit) {
		this.masterCardPinDebit = masterCardPinDebit;
	}

	public String getMerchantPrefix() {
		return merchantPrefix;
	}

	public void setMerchantPrefix(String merchantPrefix) {
		this.merchantPrefix = merchantPrefix;
	}

	public Boolean getNonBankCards() {
		return nonBankCards;
	}

	public void setNonBankCards(Boolean nonBankCards) {
		this.nonBankCards = nonBankCards;
	}

	public void setNonBankCards(String nonBankCards) {
		this.nonBankCards = "Y".equalsIgnoreCase(nonBankCards);
	}

	public Integer getOwner() {
		return owner;
	}

	public void setOwner(Integer owner) {
		this.owner = owner;
	}

	public Integer getProcessorId() {
		return processorId;
	}

	public void setProcessorId(Integer processorId) {
		this.processorId = processorId;
	}

	public void addTrait(Integer trait, Integer index) {
		if (traits != null) {
			traits = new ArrayList<>(11);
			for (int i = 0; i < 11; i++) {
				traits.add(null);
			}
		}
		traits.set(index, trait);
	}

	public List<Integer> getTraits() {
		return traits;
	}

	public Integer getTrait(Integer traitNumber) {
		return traits.get(traitNumber);
	}

	public void setTraits(List<Integer> traits) {
		this.traits = traits;
	}

	public Integer getVisaAquirerId() {
		return visaAquirerId;
	}

	public void setVisaAquirerId(Integer visaAquirerId) {
		this.visaAquirerId = visaAquirerId;
	}

	public Integer getVisaBinBaseII() {
		return visaBinBaseII;
	}

	public void setVisaBinBaseII(Integer visaBinBaseII) {
		this.visaBinBaseII = visaBinBaseII;
	}

	public Integer getVisaBinPinDebit() {
		return visaBinPinDebit;
	}

	public void setVisaBinPinDebit(Integer visaBinPinDebit) {
		this.visaBinPinDebit = visaBinPinDebit;
	}

	public Integer getVisaSreNumber() {
		return visaSreNumber;
	}

	public void setVisaSreNumber(Integer visaSreNumber) {
		this.visaSreNumber = visaSreNumber;
	}

	@Override
	public int hashCode() {
		return 167 *  ((bankNumber == null) ? 0 : bankNumber.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MbsBank other = (MbsBank) obj;
		if (bankNumber == null) {
			if (other.bankNumber != null)
				return false;
		}
		else if (!bankNumber.equals(other.bankNumber))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MbsBank [achHolidayFlag=").append(", bankNumber=").append(bankNumber).append("]");
		return builder.toString();
	}

}
