package com.mes.sponsorbank;

public class NoDomainInterfaceException extends RuntimeException{
	public NoDomainInterfaceException(String classname) {
		super("The implmentation of MbsBanksDomainInterface is missing, impl name="+classname);
	}
}
