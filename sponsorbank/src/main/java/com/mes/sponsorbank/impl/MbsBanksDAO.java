package com.mes.sponsorbank.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import com.mes.database.MesQueryHandlerResultSet;
import com.mes.database.MesResultSet;
import com.mes.sponsorbank.MbsBank;
import com.mes.sponsorbank.MbsBankContact;
import com.mes.sponsorbank.MbsBankGroupAttributes;
import com.mes.sponsorbank.MbsBanksDomainInterface;
import com.mes.sponsorbank.MbsMmsBankBin;

public class MbsBanksDAO implements MbsBanksDomainInterface {

	static Logger log = Logger.getLogger(MbsBanksDAO.class);

	//@formatter:off
	private static String MBS_BANKS_SQL ="SELECT"
								+"     bank_number,     processor_id,                vs_bin_base_ii,     vs_bin_pin_debit, vs_acquirer_id," 
								+"     mc_bin_inet,     mc_bin_pin_debit,            mc_ica_number,      am_aggregators,   ds_acquirer_id, "
								+"     owner,           trait_01,                    trait_02,           trait_03,         trait_04, " 
								+"     trait_05,        trait_06,                    trait_07,           trait_08,         trait_09, " 
								+"     trait_10,        vs_sre_number,               active,             amex_interchange_override,   merchant_prefix, " 
								+"     ds_mailbox_id,   dflt_cielo_channel_category, ach_holiday_flag,   non_bank_cards,   auth_billing_vendor_id, " 
								+"     auth_pg_billing_bank, use_alt_association,    use_alt_addr,       use_alt_merch_legal_name, use_alt_merch_name " 
								+" FROM " 
								+"     mes.mbs_banks ";
	//@formatter:on

	public MesQueryHandlerResultSet getQueryHandler() {
		return new MesQueryHandlerResultSet();
	}

	public MbsBank getSponsorBank(Integer bankNumber) {
		if (log.isDebugEnabled()) {
			log.debug("[getSponsorBank()] - bankNumber=" + bankNumber);
		}
		if (bankNumber == null) {
			throw new IllegalArgumentException("getSponsorBank:bankNumber is null");
		}
		String preparedSql = MBS_BANKS_SQL + " WHERE bank_number=?";
		try {
			MesResultSet resultSet = getQueryHandler().executePreparedStatement("0com.mes.sponsorbank.impl.MbsBanksDAO", preparedSql, bankNumber);
			if (resultSet.next()) {
				return extractResultSet(resultSet);
			}
		}
		catch (SQLException e) {
			log.error("[getActiveBanks()] - Unexpected Exception retreiving active sponsor banks", e);

		}
		return null;
	}

	@Override
	public List<MbsBank> getActiveBanks() {
		if (log.isDebugEnabled()) {
			log.debug("[getActiveBanks()]");
		}

		String preparedSql = MBS_BANKS_SQL + " WHERE active='Y'";

		List<MbsBank> banks = new ArrayList<>();
		try {
			MesResultSet resultSet = getQueryHandler().executePreparedStatement("1com.mes.sponsorbank.impl.MbsBanksDAO", preparedSql, new Object[0]);
			while (resultSet.next()) {
				banks.add(extractResultSet(resultSet));
			}

		}
		catch (SQLException e) {
			log.error("[getActiveBanks()] - Unexpected Exception retreiving active sponsor banks", e);

		}
		return banks;
	}

	@Override
	public List<MbsBankContact> getBankContact(Integer bankNumber) {
		if (log.isDebugEnabled()) {
			log.debug(String.format("[getBankContact()] - bankNumber - %s", bankNumber));
		}
		if (bankNumber == null) {
			throw new IllegalArgumentException("getBankContact:bankNumber is null");
		}
		List<MbsBankContact> contacts = new ArrayList<>();
		//@formatter:off
		String preparedSql = "SELECT "
						   + "   bank_number,   name,  	   addr_line_1, addr_line_2, "
						   + "   city,          state,     postal_code, phone_number, "
						   + "   email_address, help_info, help_name "
						   + "FROM "
						   + "   mes.mbs_bank_contact "
						   + "WHERE "
						   + "   bank_number=?";
		//@formatter:on
		try {
			MesResultSet resultSet = getQueryHandler().executePreparedStatement("2com.mes.sponsorbank.impl.MbsBanksDAO", preparedSql, bankNumber);
			while (resultSet.next()) {
				MbsBankContact contact = new MbsBankContact();
				contact.setBankNumber(resultSet.getInt("bank_number"));
				contact.setName(resultSet.getString("name"));
				contact.setAddressLine1(resultSet.getString("addr_line_1"));
				contact.setAddressLine2(resultSet.getString("addr_line_2"));
				contact.setCity(resultSet.getString("city"));
				contact.setState(resultSet.getString("state"));
				contact.setCity(resultSet.getString("city"));
				contact.setPostalCode(resultSet.getString("postal_code"));
				contact.setPhoneNumber(resultSet.getString("phone_number"));
				contact.setEmailAddress(resultSet.getString("email_address"));
				contact.setHelpInfo(resultSet.getString("help_info"));
				contact.setHelpName(resultSet.getString("help_name"));
				contacts.add(contact);
			}
		}
		catch (SQLException e) {
			log.error("[getBankContact()] - Unexpected Exception sponsor bank contact data", e);
		}
		return contacts;
	}

	@Override
	public List<MbsBankGroupAttributes> getBankGrpAssnAttributes(Integer bankNumber, String eventClassname) {
		if (log.isDebugEnabled()) {
			log.debug(String.format("[getBankGrpAssnAttributes()] - bankNumber - %s, eventClassname=%s", bankNumber, eventClassname));
		}
		if (bankNumber == null) {
			throw new IllegalArgumentException("getBankGrpAssnAttributes:bankNumber is null");
		}
		if (StringUtils.isBlank(eventClassname)) {
			throw new IllegalArgumentException("getBankGrpAssnAttributes:eventClassname is null");
		}
		List<MbsBankGroupAttributes> attributes = new ArrayList<>();
		//@formatter:off
		String preparedSql = "SELECT " 
				+ "    bank_number, " 
				+ "    association, " 
				+ "    event_classname, " 
				+ "    type, " 
				+ "    alternate_bank, "
				+ "    interchange_override_rate, " 
				+ "    risk_score_cc_email_addr, " 
				+ "    cb_auto_action_trait, " 
				+ "    netsuit_diner_uatp_override " 
				+ " FROM "
				+ "    mes.mbs_bank_assn_attributes " 
				+ " WHERE " 
				+ "    bank_number=? and event_classname=?";
		//@formatter:on
		try {
			MesResultSet resultSet =
					getQueryHandler().executePreparedStatement("3com.mes.sponsorbank.impl.MbsBanksDAO", preparedSql, bankNumber, eventClassname);
			while (resultSet.next()) {
				MbsBankGroupAttributes attribute = new MbsBankGroupAttributes();
				attribute.setBankNumber(resultSet.getInt("bank_number"));
				attribute.setAssociation(resultSet.getString("association"));
				attribute.setEventClassname(resultSet.getString("event_classname"));
				attribute.setType(resultSet.getString("type"));
				attribute.setAlternateBank(resultSet.getInt("alternate_bank"));
				attribute.setInterchangeOverrideRate(resultSet.getDouble("interchange_override_rate"));
				attribute.setRiskScoreCCEmailAddress(resultSet.getString("risk_score_cc_email_addr"));
				attribute.setCbAutoActionTrait(resultSet.getString("cb_auto_action_trait"));
				attribute.setNetsuiteDinerUatpOverride(resultSet.getString("netsuit_diner_uatp_override"));
				attributes.add(attribute);
			}
		}
		catch (SQLException e) {
			log.error("[getBankGrpAssnAttributes()] - Unexpected Exception retreiving attributes", e);
		}
		return attributes;
	}

	@Override
	public List<Integer> getEnabledBanks(String eventClassname) {
		if (log.isDebugEnabled()) {
			log.debug(String.format("[getEnabledBanks()] - eventClassname=%s", eventClassname));
		}
		if (StringUtils.isBlank(eventClassname)) {
			throw new IllegalArgumentException("getEnabledBanks:eventClassname is null");
		}
		List<Integer> sponsorBanks = new ArrayList<>();
		//@formatter:off
		String preparedSql =  "select" 
							+ "  evt.bank_number, " 
							+ "  evt.event_classname, " 
							+ "  bnk.active " 
							+ "from " 
							+ "  mbs_banks bnk "
							+ "  inner join mbs_bank_event evt on evt.bank_number = bnk.bank_number " 
							+ "where " 
							+ "   bnk.active='Y' "
							+ "   and evt.event_classname=?";
		//@formatter:on
		try {
			MesResultSet resultSet = getQueryHandler().executePreparedStatement("4com.mes.sponsorbank.impl.MbsBanksDAO", preparedSql, eventClassname);
			while (resultSet.next()) {
				sponsorBanks.add(resultSet.getInt("bank_number"));
			}
		}
		catch (SQLException e) {
			log.error("[getEnabledBanks()] - Unexpected Exception retreiving enabled banks", e);
		}
		return sponsorBanks;
	}

	@Override
	public List<MbsMmsBankBin> getMMSActiveBanks(Integer bankNumber) {
		if (log.isDebugEnabled()) {
			log.debug(String.format("[getMMSActiveBanks()] - bankNumber - %s", bankNumber));
		}
		if (bankNumber == null) {
			throw new IllegalArgumentException("getMMSActiveBanks:bankNumber is null");
		}
		List<MbsMmsBankBin> mmsBanks = new ArrayList<>();
		//@formatter:off
		String preparedSql =  "SELECT bank_number, app_source, mms_bank_bin FROM mes.mms_active_banks WHERE bank_number=?";
		//@formatter:on
		try {
			MesResultSet resultSet = getQueryHandler().executePreparedStatement("5com.mes.sponsorbank.impl.MbsBanksDAO", preparedSql, bankNumber);
			while (resultSet.next()) {
				MbsMmsBankBin mmsBank = new MbsMmsBankBin();
				mmsBank.setBankNumber(resultSet.getInt("bank_number"));
				mmsBank.setAppSource(resultSet.getString("app_source"));
				mmsBank.setMmsBankBin(resultSet.getString("mms_bank_bin"));
				mmsBanks.add(mmsBank);
			}
		}
		catch (SQLException e) {
			log.error("[getEnabledBanks()] - Unexpected Exception retreiving enabled banks", e);
		}
		return mmsBanks;
	}

	private MbsBank extractResultSet(MesResultSet resultSet) throws SQLException {
		MbsBank bank = new MbsBank();
		bank.setBankNumber(resultSet.getInt("bank_number"));
		bank.setProcessorId(resultSet.getInt("processor_id"));
		bank.setVisaBinBaseII(resultSet.getInt("vs_bin_base_ii"));
		bank.setVisaBinPinDebit(resultSet.getInt("vs_bin_pin_debit"));
		bank.setVisaAquirerId(resultSet.getInt("vs_acquirer_id"));
		bank.setMasterCardBinInet(resultSet.getInt("mc_bin_inet"));
		bank.setMasterCardPinDebit(resultSet.getInt("mc_bin_pin_debit"));
		bank.setMasterCardIcaNumber(resultSet.getInt("mc_ica_number"));
		bank.setAmexAggregators(resultSet.getString("am_aggregators"));
		bank.setDiscoverAcquirerId(resultSet.getInt("ds_acquirer_id"));
		bank.setOwner(resultSet.getInt("owner"));
		bank.addTrait(resultSet.getInt("trait_01"), 1);
		bank.addTrait(resultSet.getInt("trait_02"), 2);
		bank.addTrait(resultSet.getInt("trait_03"), 3);
		bank.addTrait(resultSet.getInt("trait_04"), 4);
		bank.addTrait(resultSet.getInt("trait_05"), 5);
		bank.addTrait(resultSet.getInt("trait_06"), 6);
		bank.addTrait(resultSet.getInt("trait_07"), 7);
		bank.addTrait(resultSet.getInt("trait_08"), 8);
		bank.addTrait(resultSet.getInt("trait_09"), 9);
		bank.addTrait(resultSet.getInt("trait_10"), 10);
		bank.setVisaSreNumber(resultSet.getInt("vs_sre_number"));
		bank.setActive(resultSet.getString("active"));
		bank.setAmexInterchangeOverride(resultSet.getString("amex_interchange_override"));
		bank.setMerchantPrefix(resultSet.getString("merchant_prefix"));
		bank.setDiscoverMailboxId(resultSet.getString("ds_mailbox_id"));
		bank.setDefaultCieloChannelCategory(resultSet.getString("dflt_cielo_channel_category"));
		bank.setAchHolidayFlag(resultSet.getString("ach_holiday_flag"));
		bank.setNonBankCards(resultSet.getString("non_bank_cards"));
		bank.setAuthBillingVendorId(resultSet.getString("auth_billing_vendor_id"));
		bank.setAuthPaymentGatewayBillingBank(resultSet.getInt("auth_pg_billing_bank"));
		bank.setAltAssocication(resultSet.getString("use_alt_association"));
		bank.setAltAddress(resultSet.getString("use_alt_addr"));
		bank.setAltMerchantLegalName(resultSet.getString("use_alt_merch_legal_name"));
		bank.setAltMerchantName(resultSet.getString("use_alt_merch_name"));
		return bank;
	}

}
