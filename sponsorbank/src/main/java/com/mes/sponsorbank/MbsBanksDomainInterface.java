package com.mes.sponsorbank;

import java.util.List;

public interface MbsBanksDomainInterface {

	/**
	 * Retrieves a specific sponsor bank
	 * @param bankNumber
	 * @return
	 */
	public MbsBank getSponsorBank(Integer bankNumber);

	/**
	 * Retrieves all the currently active sponsor banks
	 * @return <code>List<MbsBank></code>
	 */
	public List<MbsBank> getActiveBanks();

	/**
	 * Retreives the bank contact (address information) for the requested sponsor bank
	 * @param bankNumber
	 * @return List<MbsBankContact>
	 */
	public List<MbsBankContact> getBankContact(Integer bankNumber);

	/**
	 * Returns the bank+group association and attributes for the requested bank number and event
	 * @param bankNumber
	 * @param eventClassname
	 * @return
	 */
	public List<MbsBankGroupAttributes> getBankGrpAssnAttributes(Integer bankNumber, String eventClassname);

	/**
	 * Returns the list of enabled (and active) banks for the given event. The bank must be both active (MBS_BANKS.ACTIVE=Y) and enabled
	 * @param eventClassname
	 * @return A list of Integers (bank numbers)
	 */
	public List<Integer> getEnabledBanks(String eventClassname);

	/**
	 * Returns the list of bins configured in MMS_ACTIVE_BANKS configured for the bankNumber
	 * @param eventClassname
	 * @return
	 */
	public List<MbsMmsBankBin> getMMSActiveBanks(Integer bankNumber);

}
