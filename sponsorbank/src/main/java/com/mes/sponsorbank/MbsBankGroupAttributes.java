package com.mes.sponsorbank;

public class MbsBankGroupAttributes {
	private Integer alternateBank;
	private String association;
	private Integer bankNumber;
	private Boolean cbAutoActionTrait;
	private String eventClassname;
	private Double interchangeOverrideRate;
	private Boolean netsuiteDinerUatpOverride;
	private String riskScoreCCEmailAddress;
	private String type;

	public Integer getAlternateBank() {
		return alternateBank;
	}

	public void setAlternateBank(Integer alternateBank) {
		this.alternateBank = alternateBank;
	}

	public String getAssociation() {
		return association;
	}

	public void setAssociation(String association) {
		this.association = association;
	}

	public Integer getBankNumber() {
		return bankNumber;
	}

	public void setBankNumber(Integer bankNumber) {
		this.bankNumber = bankNumber;
	}

	public Boolean getCbAutoActionTrait() {
		return cbAutoActionTrait;
	}

	public void setCbAutoActionTrait(Boolean cbAutoActionTrait) {
		this.cbAutoActionTrait = cbAutoActionTrait;
	}

	public void setCbAutoActionTrait(String cbAutoActionTrait) {
		this.cbAutoActionTrait = "Y".equalsIgnoreCase(cbAutoActionTrait);
	}

	public String getEventClassname() {
		return eventClassname;
	}

	public void setEventClassname(String eventClassname) {
		this.eventClassname = eventClassname;
	}

	public Double getInterchangeOverrideRate() {
		return interchangeOverrideRate;
	}

	public void setInterchangeOverrideRate(Double interchangeOverrideRate) {
		this.interchangeOverrideRate = interchangeOverrideRate;
	}

	public Boolean getNetsuiteDinerUatpOverride() {
		return netsuiteDinerUatpOverride;
	}

	public void setNetsuiteDinerUatpOverride(Boolean netsuiteDinerUatpOverride) {
		this.netsuiteDinerUatpOverride = netsuiteDinerUatpOverride;
	}

	public void setNetsuiteDinerUatpOverride(String netsuiteDinerUatpOverride) {
		this.netsuiteDinerUatpOverride = "Y".equalsIgnoreCase(netsuiteDinerUatpOverride);
	}

	public String getRiskScoreCCEmailAddress() {
		return riskScoreCCEmailAddress;
	}

	public void setRiskScoreCCEmailAddress(String riskScoreCCEmailAddress) {
		this.riskScoreCCEmailAddress = riskScoreCCEmailAddress;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MbsBankGroupAttributes [alternateBank=").append(alternateBank).append(", association=").append(association).append(", bankNumber=")
				.append(bankNumber).append(", cbAutoActionTrait=").append(cbAutoActionTrait).append(", eventClassname=").append(eventClassname)
				.append(", interchangeOverrideRate=").append(interchangeOverrideRate).append(", netsuiteDinerUatpOverride=").append(netsuiteDinerUatpOverride)
				.append(", riskScoreCCEmailAddress=").append(riskScoreCCEmailAddress).append(", type=").append(type).append("]");
		return builder.toString();
	}

}
