package com.me.sponsorbank.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.mes.database.MesQueryHandlerResultSet;
import com.mes.database.MesResultSet;
import com.mes.sponsorbank.MbsBank;
import com.mes.sponsorbank.MbsBankContact;
import com.mes.sponsorbank.MbsBankGroupAttributes;
import com.mes.sponsorbank.MbsMmsBankBin;
import com.mes.sponsorbank.impl.MbsBanksDAO;

public class MbsBanksDAOTest {
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	MesQueryHandlerResultSet queryHandlerMock = null;
	String preparedSql = null;
	
	@Captor
	ArgumentCaptor<Object[]> objCaptor;

	@Mock
	MesResultSet resultSetMock;
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);

		System.setProperty("log4j.configuration", "Development/log4j.properties");
		System.setProperty("db.configuration", "Development/db.properties");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetSponsorBank_NullBank() throws SQLException {
		Integer bankNumber = null;
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("0com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		try {
			 daoMock.getSponsorBank(bankNumber);
		}
		catch(NullPointerException e){
			assertEquals("getSponsorBank:bankNumber is null",e.getMessage());
			throw e;
		}
	}
	
	@Test
	public void testGetSponsorBank_Success() throws SQLException {
		Integer bankNumber = 3941;
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		Map<String, Object> bankMock = new HashMap<>();
		bankMock.put("bank_number", 3941);
		bankMock.put("processor_id", 1);
		bankMock.put("vs_bin_base_ii", 433239);
		bankMock.put("vs_bin_pin_debit", 412072);
		bankMock.put("vs_acquirer_id", 10049494);
		bankMock.put("mc_bin_inet", 514061);
		bankMock.put("mc_bin_pin_debit", null);
		bankMock.put("mc_ica_number", 5185);
		bankMock.put("am_aggregators", "1046000048,1048175723,1048175749,1048175756,1048175764,1048175772,1048593214,5049892912,1101618072,1101618148,1101618130,1101618064,1101618098,1101618122,1101618106,1101618163,1101618114,1101618155,1104346473,1104346465,2100687672,2100687664");
		bankMock.put("ds_acquirer_id", 650436);
		bankMock.put("owner", 1);
		bankMock.put("trait_01", null);
		bankMock.put("trait_02", 3941);
		bankMock.put("trait_03", null);
		bankMock.put("trait_04", null);
		bankMock.put("trait_05", null);
		bankMock.put("trait_06", null);
		bankMock.put("trait_07", null);
		bankMock.put("trait_08", null);
		bankMock.put("trait_09", null);
		bankMock.put("trait_10", null);
		bankMock.put("vs_sre_number", 1000066937);
		bankMock.put("active", "Y");
		bankMock.put("amex_interchange_override", "N");
		bankMock.put("merchant_prefix", "");
		bankMock.put("ds_mailbox_id", "");
		bankMock.put("dflt_cielo_channel_category", "Uncategorized");
		bankMock.put("ach_holiday_flag", "N");
		bankMock.put("non_bank_cards", "Y");
		bankMock.put("auth_billing_vendor_id", "");
		bankMock.put("auth_pg_billing_bank", null);
		bankMock.put("use_alt_association", "N");
		bankMock.put("use_alt_addr", "N");
		bankMock.put("use_alt_merch_legal_name", "N");
		bankMock.put("use_alt_merch_name", "N");
		mockResultSet.add(bankMock);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("0com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		MbsBank sponsorBank = daoMock.getSponsorBank(bankNumber);
		
		List argumentList = objCaptor.getAllValues();
		assertEquals(1,argumentList.size());
		assertEquals(Integer.valueOf(bankNumber),argumentList.get(0));
		
		assertNotNull(sponsorBank);
		assertEquals(3941,sponsorBank.getBankNumber().intValue());
		assertTrue(sponsorBank.getNonBankCards());
		assertFalse(sponsorBank.getAltAddress());
	}

	@Test
	public void testGetActive_One_AltFlagFalse() throws SQLException {

		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		Map<String, Object> bankMock = new HashMap<>();
		bankMock.put("bank_number", 3941);
		bankMock.put("processor_id", 1);
		bankMock.put("vs_bin_base_ii", 433239);
		bankMock.put("vs_bin_pin_debit", 412072);
		bankMock.put("vs_acquirer_id", 10049494);
		bankMock.put("mc_bin_inet", 514061);
		bankMock.put("mc_bin_pin_debit", null);
		bankMock.put("mc_ica_number", 5185);
		bankMock.put("am_aggregators", "1046000048,1048175723,1048175749,1048175756,1048175764,1048175772,1048593214,5049892912,1101618072,1101618148,1101618130,1101618064,1101618098,1101618122,1101618106,1101618163,1101618114,1101618155,1104346473,1104346465,2100687672,2100687664");
		bankMock.put("ds_acquirer_id", 650436);
		bankMock.put("owner", 1);
		bankMock.put("trait_01", null);
		bankMock.put("trait_02", 3941);
		bankMock.put("trait_03", null);
		bankMock.put("trait_04", null);
		bankMock.put("trait_05", null);
		bankMock.put("trait_06", null);
		bankMock.put("trait_07", null);
		bankMock.put("trait_08", null);
		bankMock.put("trait_09", null);
		bankMock.put("trait_10", null);
		bankMock.put("vs_sre_number", 1000066937);
		bankMock.put("active", "Y");
		bankMock.put("amex_interchange_override", "N");
		bankMock.put("merchant_prefix", "");
		bankMock.put("ds_mailbox_id", "");
		bankMock.put("dflt_cielo_channel_category", "Uncategorized");
		bankMock.put("ach_holiday_flag", "N");
		bankMock.put("non_bank_cards", "Y");
		bankMock.put("auth_billing_vendor_id", "");
		bankMock.put("auth_pg_billing_bank", null);
		bankMock.put("use_alt_association", "N");
		bankMock.put("use_alt_addr", "N");
		bankMock.put("use_alt_merch_legal_name", "N");
		bankMock.put("use_alt_merch_name", "N");
		mockResultSet.add(bankMock);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("1com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		List<MbsBank> banks = daoMock.getActiveBanks();
		
		assertTrue(objCaptor.getAllValues().isEmpty());
		
		assertNotNull(banks);
		assertFalse(banks.isEmpty());
		assertEquals(3941,banks.get(0).getBankNumber().intValue());
		assertTrue(banks.get(0).getNonBankCards());
		assertFalse(banks.get(0).getAltAddress());
	}
	
	@Test
	public void testGetActive_One_AltFlagTrue() throws SQLException {

		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		Map<String, Object> bankMock = new HashMap<>();
		bankMock = new HashMap<>();
		bankMock.put("bank_number", 3858);
		bankMock.put("processor_id", 1);
		bankMock.put("vs_bin_base_ii", 413747);
		bankMock.put("vs_bin_pin_debit", 423184);
		bankMock.put("vs_acquirer_id", 10000238);
		bankMock.put("mc_bin_inet", 545733);
		bankMock.put("mc_bin_pin_debit", null);
		bankMock.put("mc_ica_number", 6739);
		bankMock.put("am_aggregators", "1046000048,1048175723,1048175749,1048175756,1048175764,1048175772,1048593214,5049892912,1101618072,1101618148,1101618130,1101618064,1101618098,1101618122,1101618106,1101618163,1101618114,1101618155,1104346473,1104346465,2100687672,2100687664");
		bankMock.put("ds_acquirer_id", 650495);
		bankMock.put("owner", 1);
		bankMock.put("trait_01", null);
		bankMock.put("trait_02", 3941);
		bankMock.put("trait_03", null);
		bankMock.put("trait_04", null);
		bankMock.put("trait_05", null);
		bankMock.put("trait_06", null);
		bankMock.put("trait_07", null);
		bankMock.put("trait_08", null);
		bankMock.put("trait_09", null);
		bankMock.put("trait_10", null);
		bankMock.put("vs_sre_number", 1000049010);
		bankMock.put("active", "Y");
		bankMock.put("amex_interchange_override", "N");
		bankMock.put("merchant_prefix", "0");
		bankMock.put("ds_mailbox_id", "");
		bankMock.put("dflt_cielo_channel_category", "SE Sales");
		bankMock.put("ach_holiday_flag", "N");
		bankMock.put("non_bank_cards", "N");
		bankMock.put("auth_billing_vendor_id", "");
		bankMock.put("auth_pg_billing_bank", null);
		bankMock.put("use_alt_association", "Y");
		bankMock.put("use_alt_addr", "Y");
		bankMock.put("use_alt_merch_legal_name", "Y");
		bankMock.put("use_alt_merch_name", "Y");
		mockResultSet.add(bankMock);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("1com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		List<MbsBank> banks = daoMock.getActiveBanks();
		
		assertTrue(objCaptor.getAllValues().isEmpty());
		
		assertNotNull(banks);
		assertFalse(banks.isEmpty());
		assertEquals(3858,banks.get(0).getBankNumber().intValue());
		assertFalse(banks.get(0).getNonBankCards());
		assertTrue(banks.get(0).getAltAddress());
	}
	
	@Test
	public void testGetActive_Two() throws SQLException {

		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		Map<String, Object> bankMock = new HashMap<>();
		bankMock.put("bank_number", 3941);
		bankMock.put("processor_id", 1);
		bankMock.put("vs_bin_base_ii", 433239);
		bankMock.put("vs_bin_pin_debit", 412072);
		bankMock.put("vs_acquirer_id", 10049494);
		bankMock.put("mc_bin_inet", 514061);
		bankMock.put("mc_bin_pin_debit", null);
		bankMock.put("mc_ica_number", 5185);
		bankMock.put("am_aggregators", "1046000048,1048175723,1048175749,1048175756,1048175764,1048175772,1048593214,5049892912,1101618072,1101618148,1101618130,1101618064,1101618098,1101618122,1101618106,1101618163,1101618114,1101618155,1104346473,1104346465,2100687672,2100687664");
		bankMock.put("ds_acquirer_id", 650436);
		bankMock.put("owner", 1);
		bankMock.put("trait_01", null);
		bankMock.put("trait_02", 3941);
		bankMock.put("trait_03", null);
		bankMock.put("trait_04", null);
		bankMock.put("trait_05", null);
		bankMock.put("trait_06", null);
		bankMock.put("trait_07", null);
		bankMock.put("trait_08", null);
		bankMock.put("trait_09", null);
		bankMock.put("trait_10", null);
		bankMock.put("vs_sre_number", 1000066937);
		bankMock.put("active", "Y");
		bankMock.put("amex_interchange_override", "N");
		bankMock.put("merchant_prefix", "");
		bankMock.put("ds_mailbox_id", "");
		bankMock.put("dflt_cielo_channel_category", "Uncategorized");
		bankMock.put("ach_holiday_flag", "N");
		bankMock.put("non_bank_cards", "Y");
		bankMock.put("auth_billing_vendor_id", "");
		bankMock.put("auth_pg_billing_bank", null);
		bankMock.put("use_alt_association", "N");
		bankMock.put("use_alt_addr", "N");
		bankMock.put("use_alt_merch_legal_name", "N");
		bankMock.put("use_alt_merch_name", "N");
		mockResultSet.add(bankMock);
		
		bankMock = new HashMap<>();
		bankMock.put("bank_number", 3858);
		bankMock.put("processor_id", 1);
		bankMock.put("vs_bin_base_ii", 413747);
		bankMock.put("vs_bin_pin_debit", 423184);
		bankMock.put("vs_acquirer_id", 10000238);
		bankMock.put("mc_bin_inet", 545733);
		bankMock.put("mc_bin_pin_debit", null);
		bankMock.put("mc_ica_number", 6739);
		bankMock.put("am_aggregators", "1046000048,1048175723,1048175749,1048175756,1048175764,1048175772,1048593214,5049892912,1101618072,1101618148,1101618130,1101618064,1101618098,1101618122,1101618106,1101618163,1101618114,1101618155,1104346473,1104346465,2100687672,2100687664");
		bankMock.put("ds_acquirer_id", 650495);
		bankMock.put("owner", 1);
		bankMock.put("trait_01", null);
		bankMock.put("trait_02", 3941);
		bankMock.put("trait_03", null);
		bankMock.put("trait_04", null);
		bankMock.put("trait_05", null);
		bankMock.put("trait_06", null);
		bankMock.put("trait_07", null);
		bankMock.put("trait_08", null);
		bankMock.put("trait_09", null);
		bankMock.put("trait_10", null);
		bankMock.put("vs_sre_number", 1000049010);
		bankMock.put("active", "Y");
		bankMock.put("amex_interchange_override", "N");
		bankMock.put("merchant_prefix", "0");
		bankMock.put("ds_mailbox_id", "");
		bankMock.put("dflt_cielo_channel_category", "SE Sales");
		bankMock.put("ach_holiday_flag", "N");
		bankMock.put("non_bank_cards", "N");
		bankMock.put("auth_billing_vendor_id", "");
		bankMock.put("auth_pg_billing_bank", null);
		bankMock.put("use_alt_association", "Y");
		bankMock.put("use_alt_addr", "Y");
		bankMock.put("use_alt_merch_legal_name", "Y");
		bankMock.put("use_alt_merch_name", "Y");
		mockResultSet.add(bankMock);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("1com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		List<MbsBank> banks = daoMock.getActiveBanks();
		
		assertTrue(objCaptor.getAllValues().isEmpty());
		
		assertNotNull(banks);
		assertFalse(banks.isEmpty());
		assertEquals(2,banks.size());
		
		assertEquals(3941,banks.get(0).getBankNumber().intValue());
		assertTrue(banks.get(0).getNonBankCards());
		assertFalse(banks.get(0).getAltAddress());
		
		assertEquals(3858,banks.get(1).getBankNumber().intValue());
		assertFalse(banks.get(1).getNonBankCards());
		assertTrue(banks.get(1).getAltAddress());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetBankContact_NullBankNumber() throws SQLException {
		Integer bankNumber = null;
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("2com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		try {
			List<MbsBankContact> contacts = daoMock.getBankContact(bankNumber);
		}
		catch(IllegalArgumentException e){
			assertEquals("getBankContact:bankNumber is null",e.getMessage());
			throw e;
		}
	}
	
	@Test
	public void testGetBankContact_NotFound() throws SQLException {
		Integer bankNumber = 3003;
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		Map<String, Object> rowMock = new HashMap<>();
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("2com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		List<MbsBankContact> contacts = daoMock.getBankContact(bankNumber);
		
		List argumentList = objCaptor.getAllValues();
		assertEquals(1,argumentList.size());
		assertEquals(Integer.valueOf(bankNumber),argumentList.get(0));
		
		assertNotNull(contacts);
		assertTrue(contacts.isEmpty());
		
	}
	
	@Test
	public void testGetBankContact_OneFound() throws SQLException {
		Integer bankNumber = 3941;
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		Map<String, Object> rowMock = new HashMap<>();
		rowMock.put("bank_number",3941);
		rowMock.put("name","MERCHANT E-SOLUTIONS");
		rowMock.put("addr_line_1","P.O. BOX 13305");
		rowMock.put("city","SPOKANE");
		rowMock.put("state","WA");
		rowMock.put("postal_code","99213-3305");
		rowMock.put("phone_number","(888)288-2692");
		rowMock.put("help_info","24 HR HELP DESK");
		rowMock.put("help_name","MERCHANT E-SOLUTIONS");
		mockResultSet.add(rowMock);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("2com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		List<MbsBankContact> contacts = daoMock.getBankContact(bankNumber);
		
		List argumentList = objCaptor.getAllValues();
		assertEquals(1,argumentList.size());
		assertEquals(Integer.valueOf(bankNumber),argumentList.get(0));
		
		assertNotNull(contacts);
		assertFalse(contacts.isEmpty());
		assertEquals(1,contacts.size());
		
		MbsBankContact contact = contacts.get(0);
		assertEquals(Integer.valueOf(3941), contact.getBankNumber());
		assertEquals("MERCHANT E-SOLUTIONS", contact.getName());
		assertEquals("P.O. BOX 13305", contact.getAddressLine1());
		assertEquals("SPOKANE", contact.getCity());
		assertEquals("WA", contact.getState());
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetBankGrpAssnAttributes_NullBank() throws SQLException {
		Integer bankNumber = null;
		String eventClassname = "com.mes.startup.EventClassname";
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("3com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		try {
			 daoMock.getBankGrpAssnAttributes(bankNumber, eventClassname);
		}
		catch(IllegalArgumentException e){
			assertEquals("getBankGrpAssnAttributes:bankNumber is null",e.getMessage());
			throw e;
		}
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetBankGrpAssnAttributes_NullEvent() throws SQLException {
		Integer bankNumber = 3941;
		String eventClassname = null;
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("3com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		try {
			 daoMock.getBankGrpAssnAttributes(bankNumber, eventClassname);
		}
		catch(IllegalArgumentException e){
			assertEquals("getBankGrpAssnAttributes:eventClassname is null",e.getMessage());
			throw e;
		}
	}
	
	@Test
	public void testGetBankGrpAssnAttributes_OverrideRate() throws SQLException {
		Integer bankNumber = 3941;
		String eventClassname = "com.mes.startup.MojaveSummaryEvent";
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		Map<String, Object> rowMock = new HashMap<>();
		rowMock.put("bank_number",3941);
		rowMock.put("association","400059");
		rowMock.put("event_classname","com.mes.startup.MojaveSummaryEvent");
		rowMock.put("type","MojaveOverride");
		rowMock.put("interchange_override_rate",0.0235);
		mockResultSet.add(rowMock);

		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("3com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		List<MbsBankGroupAttributes> attributes = daoMock.getBankGrpAssnAttributes(bankNumber, eventClassname);
		
		List argumentList = objCaptor.getAllValues();
		assertEquals(2,argumentList.size());
		assertEquals(Integer.valueOf(bankNumber),argumentList.get(0));
		assertEquals(eventClassname,argumentList.get(1));
		
		assertNotNull(attributes);
		assertEquals(1,attributes.size());
		
		MbsBankGroupAttributes attribute = attributes.get(0);
		assertEquals(Integer.valueOf(bankNumber), attribute.getBankNumber());
		assertEquals("400059", attribute.getAssociation());
		assertEquals(eventClassname, attribute.getEventClassname());
		assertEquals(Double.valueOf(0.0235), attribute.getInterchangeOverrideRate());
		
	}
	
	@Test
	public void testGetBankGrpAssnAttributes_TwoRows() throws SQLException {
		Integer bankNumber = 3941;
		String eventClassname = "com.mes.startup.RiskScoringLoader";
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		Map<String, Object> rowMock = new HashMap<>();
		rowMock.put("bank_number",3941);
		rowMock.put("association","816031");
		rowMock.put("event_classname","com.mes.startup.RiskScoringLoader");
		rowMock.put("type","RiskCCEmail");
		rowMock.put("risk_score_cc_email_addr","chargeback@paymate.com");
		mockResultSet.add(rowMock);
		
		rowMock = new HashMap<>();
		rowMock.put("bank_number",3941);
		rowMock.put("association","000000");
		rowMock.put("event_classname","com.mes.startup.RiskScoringLoader");
		rowMock.put("type","CBAutoAction");
		rowMock.put("cb_auto_action_trait","Y");
		mockResultSet.add(rowMock);

		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("3com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		List<MbsBankGroupAttributes> attributes = daoMock.getBankGrpAssnAttributes(bankNumber, eventClassname);
		
		List argumentList = objCaptor.getAllValues();
		assertEquals(2,argumentList.size());
		assertEquals(Integer.valueOf(bankNumber),argumentList.get(0));
		assertEquals(eventClassname,argumentList.get(1));
		
		assertNotNull(attributes);
		assertEquals(2,attributes.size());
		
		MbsBankGroupAttributes attribute = attributes.get(0);
		assertEquals(Integer.valueOf(bankNumber), attribute.getBankNumber());
		assertEquals("816031", attribute.getAssociation());
		assertEquals(eventClassname, attribute.getEventClassname());
		assertEquals(Double.valueOf(0.0),attribute.getInterchangeOverrideRate());
		assertEquals("chargeback@paymate.com", attribute.getRiskScoreCCEmailAddress());
		
		attribute = attributes.get(1);
		assertEquals(Integer.valueOf(bankNumber), attribute.getBankNumber());
		assertEquals("000000", attribute.getAssociation());
		assertEquals(eventClassname, attribute.getEventClassname());
		assertEquals(Double.valueOf(0.0),attribute.getInterchangeOverrideRate());
		assertNull(attribute.getRiskScoreCCEmailAddress());
		assertTrue(attribute.getCbAutoActionTrait());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetEnabledBanks_NullEventName() throws SQLException {
		String eventClassname = null;
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("4com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		try {
			 daoMock.getEnabledBanks(eventClassname);
		}
		catch(IllegalArgumentException e){
			assertEquals("getEnabledBanks:eventClassname is null",e.getMessage());
			throw e;
		}
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetEnabledBanks_EmptyEventName() throws SQLException {
		String eventClassname = "";
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("4com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		try {
			 daoMock.getEnabledBanks(eventClassname);
		}
		catch(IllegalArgumentException e){
			assertEquals("getEnabledBanks:eventClassname is null",e.getMessage());
			throw e;
		}
	}
	
	@Test
	public void testGetEnabledBanks_Success() throws SQLException {
		
		String eventClassname = "com.mes.startup.CBTQuarterlyData";
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		Map<String, Object> rowMock = new HashMap<>();
		rowMock.put("bank_number",3941);
		rowMock.put("event_classname","com.mes.startup.CBTQuarterlyData");
		rowMock.put("active","Y");
		mockResultSet.add(rowMock);
		
		rowMock = new HashMap<>();
		rowMock.put("bank_number",3858);
		rowMock.put("event_classname","com.mes.startup.CBTQuarterlyData");
		rowMock.put("active","Y");
		mockResultSet.add(rowMock);

		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("4com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		List<Integer> sponsorBanks = daoMock.getEnabledBanks(eventClassname);
		
		List argumentList = objCaptor.getAllValues();
		assertEquals(1,argumentList.size());
		assertEquals(eventClassname,argumentList.get(0));
		
		assertNotNull(sponsorBanks);
		assertEquals(2,sponsorBanks.size());
		
		assertEquals(Integer.valueOf(3941), sponsorBanks.get(0));
		assertEquals(Integer.valueOf(3858), sponsorBanks.get(1));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetMMSActiveBanks_NullBank() throws SQLException {
		Integer bankNumber = null;
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("5com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		try {
			 daoMock.getMMSActiveBanks(bankNumber);
		}
		catch(IllegalArgumentException e){
			assertEquals("getMMSActiveBanks:bankNumber is null",e.getMessage());
			throw e;
		}
	}
	
	@Test
	public void testGetMMSActiveBanks_Success() throws SQLException {
		Integer bankNumber = 3941;
		
		MesResultSet mockResultSet = Mockito.spy(MesResultSet.class);
		MesQueryHandlerResultSet mockHandler = Mockito.mock(MesQueryHandlerResultSet.class);
		
		Map<String, Object> rowMock = new HashMap<>();
		rowMock.put("bank_number",3941);
		rowMock.put("app_source","XXYY");
		rowMock.put("mms_bank_bin","1234567");
		mockResultSet.add(rowMock);
		
		MbsBanksDAO daoMock = Mockito.spy(MbsBanksDAO.class);
		
		objCaptor.getAllValues().clear();
		doReturn(mockHandler).when(daoMock).getQueryHandler();
		doReturn(mockResultSet).when(mockHandler).executePreparedStatement(Mockito.eq("5com.mes.sponsorbank.impl.MbsBanksDAO"), Mockito.any(), objCaptor.capture());
		
		List<MbsMmsBankBin> mmsBanks = daoMock.getMMSActiveBanks(bankNumber);
		
		List argumentList = objCaptor.getAllValues();
		assertEquals(1,argumentList.size());
		assertEquals(Integer.valueOf(bankNumber),argumentList.get(0));
		
		assertNotNull(mmsBanks);
		assertFalse(mmsBanks.isEmpty());
		assertEquals(Integer.valueOf(3941), mmsBanks.get(0).getBankNumber());
		assertEquals("1234567", mmsBanks.get(0).getMmsBankBin());
	}
}