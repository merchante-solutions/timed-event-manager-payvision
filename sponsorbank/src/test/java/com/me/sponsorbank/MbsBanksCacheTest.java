package com.me.sponsorbank;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;
import com.mes.database.MesResultSet;
import com.mes.sponsorbank.MbsBank;
import com.mes.sponsorbank.MbsBankContact;
import com.mes.sponsorbank.MbsBankGroupAttributes;
import com.mes.sponsorbank.MbsBanksCache;
import com.mes.sponsorbank.MbsBanksDomainInterface;
import com.mes.sponsorbank.MbsMmsBankBin;

public class MbsBanksCacheTest {
	
	@Mock
	MesResultSet resultSetMock;
	
	MbsBanksCache instance = MbsBanksCache.getInstance();
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);

		System.setProperty("log4j.configuration", "Development/log4j.properties");
		System.setProperty("db.configuration", "Development/db.properties");
		
		Whitebox.setInternalState(instance, "enabledBankMap", new ConcurrentHashMap<>());
		Whitebox.setInternalState(instance, "bankContactMap", new ConcurrentHashMap<>());
		Whitebox.setInternalState(instance, "bankGroupAttrMap", new ConcurrentHashMap<>());
		Whitebox.setInternalState(instance, "bankBinMap", new ConcurrentHashMap<>());
	}
	
	@Test
	public void testGetSponsorBank_NotFound() {
		Integer bankNumber = 3943;
		
		MbsBanksDomainInterface domainMock = Mockito.mock(MbsBanksDomainInterface.class);
		Whitebox.setInternalState(instance, "sponsorBankDomain", domainMock);
		
		when(domainMock.getBankContact(eq(3943))).thenReturn(null);
		
		MbsBank actualBank = instance.getSponsorBank(bankNumber);
		
		assertNull(actualBank);
		
		//return empty array here, the mock should not be called as the data is cached
		when(domainMock.getBankContact(eq(3943))).thenReturn(new ArrayList<>());
		
		//this call should return the previously cached data
		actualBank = instance.getSponsorBank(bankNumber);
		
		assertNull(actualBank);
	}
	
	@Test
	public void testGetSponsorBank_Success() {
		Integer bankNumber = 3941;
		
		MbsBank mockBank = new MbsBank();
		mockBank.setBankNumber(3941);
		mockBank.setProcessorId(1);
		mockBank.setVisaBinBaseII(433239);
		mockBank.setVisaBinPinDebit(412072);
		mockBank.setVisaAquirerId(10049494);
		mockBank.setMasterCardBinInet(514061);
		mockBank.setMasterCardPinDebit(null);
		mockBank.setMasterCardIcaNumber(5185);
		mockBank.setAmexAggregators("1046000048,1048175723");
		mockBank.setDiscoverAcquirerId(650436);
		mockBank.setOwner(1);
		mockBank.addTrait(null, 1);
		mockBank.addTrait(3941, 2);
		mockBank.addTrait(null, 3);
		mockBank.addTrait(null, 4);
		mockBank.addTrait(null, 5);
		mockBank.addTrait(null, 6);
		mockBank.addTrait(null, 7);
		mockBank.addTrait(null, 8);
		mockBank.addTrait(null, 9);
		mockBank.addTrait(null, 10);
		mockBank.setVisaSreNumber(1000066937);
		mockBank.setActive("Y");
		mockBank.setAmexInterchangeOverride("N");
		mockBank.setMerchantPrefix("");
		mockBank.setDiscoverMailboxId(null);
		mockBank.setDefaultCieloChannelCategory("Uncategorized");
		mockBank.setAchHolidayFlag("N");
		mockBank.setNonBankCards("Y");
		mockBank.setAuthBillingVendorId(null);
		mockBank.setAuthPaymentGatewayBillingBank(null);
		mockBank.setAltAssocication("N");
		mockBank.setAltAddress("N");
		mockBank.setAltMerchantLegalName("N");
		mockBank.setAltMerchantName("N");
	
		
		MbsBanksDomainInterface domainMock = Mockito.mock(MbsBanksDomainInterface.class);
		Whitebox.setInternalState(instance, "sponsorBankDomain", domainMock);
		
		when(domainMock.getSponsorBank(eq(3941))).thenReturn(mockBank);
		
		MbsBank actualBank = instance.getSponsorBank(bankNumber);
		
		assertNotNull(actualBank);
		assertEquals(mockBank, actualBank);
		//return empty array here, the mock should not be called as the data is cached
		when(domainMock.getSponsorBank(eq(3941))).thenReturn(null);
		
		//this call should return the previously cached data
		actualBank = instance.getSponsorBank(bankNumber);
		
		assertNotNull(actualBank);
		assertEquals(mockBank, actualBank);
	}
	
	@Test
	public void testGetBankContact_NotFound() {
		Integer bankNumber = 3943;
		List<MbsBankContact> contacts = new ArrayList<>();
		
		
		MbsBanksDomainInterface domainMock = Mockito.mock(MbsBanksDomainInterface.class);
		Whitebox.setInternalState(instance, "sponsorBankDomain", domainMock);
		
		when(domainMock.getBankContact(eq(3943))).thenReturn(contacts);
		
		List<MbsBankContact> actualContacts = instance.getBankContact(bankNumber);
		
		assertNotNull(actualContacts);
		assertTrue(actualContacts.isEmpty());
		
		//return empty array here, the mock should not be called as the data is cached
		when(domainMock.getBankContact(eq(3943))).thenReturn(new ArrayList<>());
		
		//this call should return the previously cached data
		actualContacts = instance.getBankContact(bankNumber);
		
		assertNotNull(actualContacts);
		assertTrue(actualContacts.isEmpty());
	}
	
	@Test
	public void testGetBankContact_Success() {
		Integer bankNumber = 3941;
		List<MbsBankContact> contacts = new ArrayList<>();
		MbsBankContact contact = new MbsBankContact();
		contact.setBankNumber(3941);
		contact.setName("MERCHANT E-SOLUTIONS");
		contact.setAddressLine1("P.O. BOX 13305");
		contact.setCity("SPOKANE");
		contact.setState("WA");
		contact.setPostalCode("99213-3305");
		contact.setPhoneNumber("(888)288-2692");
		contact.setHelpInfo("24 HR HELP DESK");
		contact.setHelpName("MERCHANT E-SOLUTIONS");
		contacts.add(contact);
		
		MbsBanksDomainInterface domainMock = Mockito.mock(MbsBanksDomainInterface.class);
		Whitebox.setInternalState(instance, "sponsorBankDomain", domainMock);
		
		when(domainMock.getBankContact(eq(3941))).thenReturn(contacts);
		
		List<MbsBankContact> actualContacts = instance.getBankContact(bankNumber);
		
		assertNotNull(actualContacts);
		assertFalse(actualContacts.isEmpty());
		assertEquals(1, actualContacts.size());
		assertEquals(contact, actualContacts.get(0));
		
		//return empty array here, the mock should not be called as the data is cached
		when(domainMock.getBankContact(eq(3941))).thenReturn(new ArrayList<>());
		
		//this call should return the previously cached data
		actualContacts = instance.getBankContact(bankNumber);
		
		assertNotNull(actualContacts);
		assertFalse(actualContacts.isEmpty());
		assertEquals(1, actualContacts.size());
		assertEquals(contact, actualContacts.get(0));
	}
	
	@Test
	public void testGetBankGrpAssnAttributes_NotFound() {
		Integer bankNumber = 3941;
		String eventClassname = "com.mes.startup.RiskScoringLoader";
		
		List<MbsBankGroupAttributes> attributes = new ArrayList<>();
		
		MbsBanksDomainInterface domainMock = Mockito.mock(MbsBanksDomainInterface.class);
		Whitebox.setInternalState(instance, "sponsorBankDomain", domainMock);
		
		when(domainMock.getBankGrpAssnAttributes(eq(3941),eq("com.mes.startup.RiskScoringLoader"))).thenReturn(attributes);
		
		List<MbsBankGroupAttributes> actualAttributes = instance.getBankGroupAssnAttributes(bankNumber,eventClassname);
		
		assertNotNull(actualAttributes);
		assertTrue(actualAttributes.isEmpty());
		
		//return empty array here, the mock should not be called as the data is cached
		when(domainMock.getBankGrpAssnAttributes(eq(3941),eq("com.mes.startup.RiskScoringLoader"))).thenReturn(new ArrayList<>());
		
		//this call should return the previously cached data
		actualAttributes = instance.getBankGroupAssnAttributes(bankNumber,eventClassname);
		
		assertNotNull(actualAttributes);
		assertTrue(actualAttributes.isEmpty());
	}
	
	@Test
	public void testGetBankGrpAssnAttributes_Success() {
		Integer bankNumber = 3941;
		String eventClassname = "com.mes.startup.RiskScoringLoader";
		
		List<MbsBankGroupAttributes> attributes = new ArrayList<>();
		MbsBankGroupAttributes attr = new MbsBankGroupAttributes();
		attr.setBankNumber(3941);
		attr.setEventClassname(eventClassname);
		attr.setType("RiskCCEmail");
		attr.setRiskScoreCCEmailAddress("chargeback@paymate.com");
		attributes.add(attr);
		attr = new MbsBankGroupAttributes();
		attr.setBankNumber(3941);
		attr.setEventClassname(eventClassname);
		attr.setType("CBAutoAction");
		attr.setCbAutoActionTrait(true);
		attributes.add(attr);
		
		MbsBanksDomainInterface domainMock = Mockito.mock(MbsBanksDomainInterface.class);
		Whitebox.setInternalState(instance, "sponsorBankDomain", domainMock);
		
		when(domainMock.getBankGrpAssnAttributes(eq(3941),eq("com.mes.startup.RiskScoringLoader"))).thenReturn(attributes);
		
		List<MbsBankGroupAttributes> actualAttributes = instance.getBankGroupAssnAttributes(bankNumber,eventClassname);
		
		assertNotNull(actualAttributes);
		assertFalse(actualAttributes.isEmpty());
		assertEquals(2,actualAttributes.size());
		assertEquals(Integer.valueOf(3941), actualAttributes.get(0).getBankNumber());
		assertEquals("com.mes.startup.RiskScoringLoader", actualAttributes.get(0).getEventClassname());
		assertEquals("RiskCCEmail", actualAttributes.get(0).getType());
		
		assertEquals(Integer.valueOf(3941), actualAttributes.get(1).getBankNumber());
		assertEquals("com.mes.startup.RiskScoringLoader", actualAttributes.get(0).getEventClassname());
		assertEquals("CBAutoAction", actualAttributes.get(1).getType());
		
		//return empty array here, the mock should not be called as the data is cached
		when(domainMock.getBankGrpAssnAttributes(eq(3941),eq("com.mes.startup.RiskScoringLoader"))).thenReturn(new ArrayList<>());
		
		//this call should return the previously cached data
		actualAttributes = instance.getBankGroupAssnAttributes(bankNumber,eventClassname);
		
		assertNotNull(actualAttributes);
		assertFalse(actualAttributes.isEmpty());
		assertEquals(2,actualAttributes.size());
		assertEquals(Integer.valueOf(3941), actualAttributes.get(0).getBankNumber());
		assertEquals("com.mes.startup.RiskScoringLoader", actualAttributes.get(0).getEventClassname());
		assertEquals("RiskCCEmail", actualAttributes.get(0).getType());
		
		assertEquals(Integer.valueOf(3941), actualAttributes.get(1).getBankNumber());
		assertEquals("com.mes.startup.RiskScoringLoader", actualAttributes.get(0).getEventClassname());
		assertEquals("CBAutoAction", actualAttributes.get(1).getType());
	}
	
	@Test
	public void testGetEnabledBanks_Success() {
		String eventClassname = "com.mes.startup.CBTQuarterlyData";
		List<Integer> banks = new ArrayList<>();
		banks.add(3941);
		banks.add(3858);
		
		MbsBanksDomainInterface domainMock = Mockito.mock(MbsBanksDomainInterface.class);
		Whitebox.setInternalState(instance, "sponsorBankDomain", domainMock);
		
		when(domainMock.getEnabledBanks(eq("com.mes.startup.CBTQuarterlyData"))).thenReturn(banks);
		
		List<Integer> actualBanks = instance.getEnabledBanks(eventClassname);
		
		assertNotNull(actualBanks);
		assertFalse(actualBanks.isEmpty());
		assertEquals(Integer.valueOf(3941), actualBanks.get(0));
		assertEquals(Integer.valueOf(3858), actualBanks.get(1));
		
		//return empty array here, the mock should not be called as the data is cached
		when(domainMock.getEnabledBanks(eq("com.mes.startup.CBTQuarterlyData"))).thenReturn(new ArrayList<>());
		
		//this call should return the previously cached data
		actualBanks = instance.getEnabledBanks(eventClassname);
		
		assertNotNull(actualBanks);
		assertFalse(actualBanks.isEmpty());
		assertEquals(Integer.valueOf(3941), actualBanks.get(0));
		assertEquals(Integer.valueOf(3858), actualBanks.get(1));
	}
	
	@Test
	public void testGetEnabledBanks_Not_Found() {
		String eventClassname = "com.mes.startup.CBTQuarterlyData";
		List<Integer> banks = new ArrayList<>();
		banks.add(3941);
		banks.add(3858);
		
		MbsBanksDomainInterface domainMock = Mockito.mock(MbsBanksDomainInterface.class);
		Whitebox.setInternalState(instance, "sponsorBankDomain", domainMock);
		
		when(domainMock.getEnabledBanks(eq("com.mes.startup.CBTQuarterlyData"))).thenReturn(new ArrayList<>());
		
		List<Integer> actualBanks = instance.getEnabledBanks(eventClassname);
		
		assertNotNull(actualBanks);
		assertTrue(actualBanks.isEmpty());
		
		//return empty array here, the mock should not be called as the data is cached
		when(domainMock.getEnabledBanks(eq("com.mes.startup.CBTQuarterlyData"))).thenReturn(new ArrayList<>());
		
		//this call should return the previously cached data
		actualBanks = instance.getEnabledBanks(eventClassname);
		
		assertNotNull(actualBanks);
		assertTrue(actualBanks.isEmpty());
	}
	
	@Test
	public void testGetMMSActiveBanks_NotFound() {
		Integer bankNumber = 3941;
		
		List<MbsMmsBankBin> mmsBankBins = new ArrayList<>();
		
		MbsBanksDomainInterface domainMock = Mockito.mock(MbsBanksDomainInterface.class);
		Whitebox.setInternalState(instance, "sponsorBankDomain", domainMock);
		
		when(domainMock.getMMSActiveBanks(eq(3941))).thenReturn(mmsBankBins);
		
		List<MbsMmsBankBin> actualBins = instance.getMMSActiveBanks(bankNumber);
		
		assertNotNull(actualBins);
		assertTrue(actualBins.isEmpty());
		
		//return empty array here, the mock should not be called as the data is cached
		when(domainMock.getMMSActiveBanks(eq(3941))).thenReturn(new ArrayList<>());
		
		//this call should return the previously cached data
		actualBins = instance.getMMSActiveBanks(bankNumber);
		
		assertNotNull(actualBins);
		assertTrue(actualBins.isEmpty());
	}
	
	@Test
	public void testGetMMSActiveBanks_Success() {
		Integer bankNumber = 3941;
		
		List<MbsMmsBankBin> mmsBankBins = new ArrayList<>();
		MbsMmsBankBin bankBin = new MbsMmsBankBin();
		bankBin.setBankNumber(3941);
		bankBin.setAppSource("MESR");
		bankBin.setMmsBankBin("433239");
		mmsBankBins.add(bankBin);
		
		MbsBanksDomainInterface domainMock = Mockito.mock(MbsBanksDomainInterface.class);
		Whitebox.setInternalState(instance, "sponsorBankDomain", domainMock);
		
		when(domainMock.getMMSActiveBanks(eq(3941))).thenReturn(mmsBankBins);
		
		List<MbsMmsBankBin> actualBins = instance.getMMSActiveBanks(bankNumber);
		
		assertNotNull(actualBins);
		assertFalse(actualBins.isEmpty());
		assertEquals(1,actualBins.size());
		assertEquals(Integer.valueOf(3941), actualBins.get(0).getBankNumber());
		assertEquals("MESR", actualBins.get(0).getAppSource());
		assertEquals("433239", actualBins.get(0).getMmsBankBin());
		
		//return empty array here, the mock should not be called as the data is cached
		when(domainMock.getMMSActiveBanks(eq(3941))).thenReturn(new ArrayList<>());
		
		//this call should return the previously cached data
		actualBins = instance.getMMSActiveBanks(bankNumber);
		
		assertNotNull(actualBins);
		assertFalse(actualBins.isEmpty());
		assertEquals(1,actualBins.size());
		assertEquals(Integer.valueOf(3941), actualBins.get(0).getBankNumber());
		assertEquals("MESR", actualBins.get(0).getAppSource());
		assertEquals("433239", actualBins.get(0).getMmsBankBin());
	}
}
