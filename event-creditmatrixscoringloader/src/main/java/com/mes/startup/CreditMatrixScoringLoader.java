/*@lineinfo:filename=CreditMatrixScoringLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.creditmatrix.CreditMatrixScore;
import com.mes.creditmatrix.CreditMatrixScoreCalculator;
import com.mes.creditmatrix.CreditMatrixScoreDAO;
import com.mes.creditmatrix.DAOFactory;
import com.mes.creditmatrix.DAOFactoryBuilder;
import sqlj.runtime.ResultSetIterator;


/**
 * CreditMatrixScoringLoader class.
 * 
 * Encapsulates the logic to auto-calculate the internal credit score for a given app.
 */
public final class CreditMatrixScoringLoader extends EventBase
{
  // constants
  public static final int             MAX_WAIT_MINUTES            = 30;
  
  // data members
  private int                         procSequence                = 0;
  private CreditMatrixScoreCalculator cmsc;
  private CreditMatrixScore           cms;
  
  
  // construction
  public CreditMatrixScoringLoader()
  {
    cmsc = new CreditMatrixScoreCalculator();
    cms=null;
  }
  
  private void getProcSequence()
  {
    try
    {
      // get the processSequence
      /*@lineinfo:generated-code*//*@lineinfo:46^7*/

//  ************************************************************
//  #sql [Ctx] { select  cm_sequence.nextval 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  cm_sequence.nextval  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.CreditMatrixScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procSequence = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:50^7*/
      
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "getProcSequence()", e.toString());
      setError("getProcSequence(): " + e.toString());
      logEntry("getProcSequence()", e.toString());
    }
  }

  private CreditMatrixScoreDAO getCreditMatrixScoreDAO()
  {
    return DAOFactoryBuilder.getDAOFactory(DAOFactory.DAO_MESDB).getCreditMatrixScoreDAO();
  }

  private int getNumElidgableRequests()
  {
    // NOTE:  a wait time is imposed for request elidgability 
    //        due to the dependency on the FICA credit rating 
    //        which involves a wait time itself.
    
    // Elidgable requests are:
    //  Those requests with acquired FICA credit info
    //  OR those requests older than 30 minutes
    
    int count = 0;
    
    try {

      /*@lineinfo:generated-code*//*@lineinfo:80^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(rec_id)
//          
//          from    credit_matrix_process
//  				        ,credit_scores
//          where   credit_matrix_process.app_seq_num=credit_scores.app_seq_num(+) 
//  				        and credit_matrix_process.process_sequence is null
//  				        and (credit_scores.score >0 or to_number(to_char(to_date('00','MI') + (sysdate - date_created), 'MI')) > :MAX_WAIT_MINUTES)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(rec_id)\n         \n        from    credit_matrix_process\n\t\t\t\t        ,credit_scores\n        where   credit_matrix_process.app_seq_num=credit_scores.app_seq_num(+) \n\t\t\t\t        and credit_matrix_process.process_sequence is null\n\t\t\t\t        and (credit_scores.score >0 or to_number(to_char(to_date('00','MI') + (sysdate - date_created), 'MI')) >  :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.CreditMatrixScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,MAX_WAIT_MINUTES);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:89^7*/
    }
    catch(Exception e) {
      logEvent(this.getClass().getName(), "getNumEligableRequests()", e.toString());
      logEntry("getNumEligableRequests()", e.toString());
    }

    return count;
  }

  public boolean execute()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             result  = false;
    
    try
    {
      connect();
      
      // see if there are elidgable requests
      if(getNumElidgableRequests()<1)
        return true;    // no penalty

      // get process sequence number
      getProcSequence();
      
      // mark interesting rows with the process sequence
      /*@lineinfo:generated-code*//*@lineinfo:117^7*/

//  ************************************************************
//  #sql [Ctx] { update  credit_matrix_process
//          set     process_sequence = :procSequence
//          where   app_seq_num in 
//                  (
//                    select  credit_matrix_process.app_seq_num
//                    from    credit_matrix_process
//  	        			          ,credit_scores
//                    where   credit_matrix_process.app_seq_num=credit_scores.app_seq_num(+) 
//  				          and     credit_matrix_process.process_sequence is null
//  				          and     (credit_scores.score >0 or to_number(to_char(to_date('00','MI') + (sysdate - date_created), 'MI')) > :MAX_WAIT_MINUTES)
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  credit_matrix_process\n        set     process_sequence =  :1 \n        where   app_seq_num in \n                (\n                  select  credit_matrix_process.app_seq_num\n                  from    credit_matrix_process\n\t        \t\t\t          ,credit_scores\n                  where   credit_matrix_process.app_seq_num=credit_scores.app_seq_num(+) \n\t\t\t\t          and     credit_matrix_process.process_sequence is null\n\t\t\t\t          and     (credit_scores.score >0 or to_number(to_char(to_date('00','MI') + (sysdate - date_created), 'MI')) >  :2 )\n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.CreditMatrixScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   __sJT_st.setInt(2,MAX_WAIT_MINUTES);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:130^7*/
      
      // make sure database shows that we are processing
      commit();
      
      // get all the interesting rows into a vector
      /*@lineinfo:generated-code*//*@lineinfo:136^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    rec_id,app_seq_num,credit_matrix_name
//          from      credit_matrix_process
//          where     process_sequence = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    rec_id,app_seq_num,credit_matrix_name\n        from      credit_matrix_process\n        where     process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.CreditMatrixScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.CreditMatrixScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:141^7*/
      
      rs = it.getResultSet();

      String s;
      long recId;
      
      while(rs.next())
      {
        recId=rs.getLong(1);
        
        recordTimestampBegin(recId);

        // calculate the score
        try {
          cms=cmsc.calculateScore(rs.getLong(2),rs.getString(3));
        }
        catch(Exception e) {
          s=e.getMessage();
          //@logEntry("execute()", s);
          continue;
        }
        
        // persist the score
        if(!getCreditMatrixScoreDAO().persistCreditMatrixScore(cms)) {
          s="Unable to persist the Underwriting App Matrix Score for app_seq_num: "+rs.getLong(2)+" (CreditMatrix ID: '"+cms.getID()+"', Name: '"+rs.getString(3)+"').";
          logEvent(this.getClass().getName(), "execute()", s);
          setError("execute(): " + s);
          logEntry("execute()", s);
          continue;
        }

        recordTimestampEnd(recId);
      }
      
      rs.close();
      it.close();
      
      commit();

      result=true;
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  protected void recordTimestampBegin( long recId )
  {
    Timestamp     beginTime   = null;
    
    try
    {
      beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
    
      /*@lineinfo:generated-code*//*@lineinfo:204^7*/

//  ************************************************************
//  #sql [Ctx] { update  credit_matrix_process
//          set     date_started = :beginTime
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  credit_matrix_process\n        set     date_started =  :1 \n        where   rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.CreditMatrixScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,beginTime);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:209^7*/
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "recordTimestampBegin()", e.toString());
      setError("recordTimestampBegin(): " + e.toString());
      logEntry("recordTimestampBegin()", e.toString());
    }
  }
  
  protected void recordTimestampEnd( long recId )
  {
    Timestamp     beginTime   = null;
    long          elapsed     = 0L; 
    Timestamp     endTime     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:227^7*/

//  ************************************************************
//  #sql [Ctx] { select  date_started 
//          from    credit_matrix_process 
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  date_started  \n        from    credit_matrix_process \n        where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.CreditMatrixScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:232^7*/
      endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      elapsed = (endTime.getTime() - beginTime.getTime());
    
      /*@lineinfo:generated-code*//*@lineinfo:236^7*/

//  ************************************************************
//  #sql [Ctx] { update  credit_matrix_process
//          set     date_completed = :endTime
//                  ,credit_matrix_name = :cms.getCreditMatrix().getName()
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4048 = cms.getCreditMatrix().getName();
   String theSqlTS = "update  credit_matrix_process\n        set     date_completed =  :1 \n                ,credit_matrix_name =  :2 \n        where   rec_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.CreditMatrixScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,endTime);
   __sJT_st.setString(2,__sJT_4048);
   __sJT_st.setLong(3,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:242^7*/

    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "recordTimestampEnd()", e.toString());
      setError("recordTimestampEnd(): " + e.toString());
      logEntry("recordTimestampEnd()", e.toString());
    }
  }

}/*@lineinfo:generated-code*/