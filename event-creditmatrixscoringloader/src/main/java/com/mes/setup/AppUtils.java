/*@lineinfo:filename=AppUtils*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/setup/AppUtils.java $

  Description:  
    Base class for application data beans
    
    This class should maintain any data that is common to all of the 
    possible application data beans (application source, submission date/time,
    etc.).  The inheritors of this class should maintain any page-specific
    application data.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-07 15:59:41 -0700 (Mon, 07 Jul 2008) $
  Version            : $Revision: 15046 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.setup;

import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class AppUtils extends SQLJConnectionBase
{
  public static final int VALUE_TYPE_INDUSTRY      = 1;
  public static final int VALUE_TYPE_BUSINESS      = 2;
  public static final int VALUE_TYPE_LOCATION      = 3;

  public static final int TIMESTAMP_COMPLETED      = 1;
  public static final int TIMESTAMP_RECEIVED       = 2;

  public static final int INVALID_APP_TYPE         = -1;
  
  private String getFirstPage(long screenSequenceId)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "";
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:58^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  screen_jsp
//          from    screen_sequence
//          where   screen_sequence_id = :screenSequenceId
//          order by screen_order
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  screen_jsp\n        from    screen_sequence\n        where   screen_sequence_id =  :1 \n        order by screen_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.setup.AppUtils",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,screenSequenceId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.setup.AppUtils",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:64^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        result = rs.getString("screen_jsp");
      }
      else
      {
        result = "/jsp/secure/RouteErr.jsp";
        logEntry("getFirstPage(" + screenSequenceId + ")", "getFirstPage: first page not found");
      }
    }
    catch(Exception e)
    {
      logEntry("getFirstPage(" + screenSequenceId + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( result );
  }

  private long getSequenceType(long primaryKey)
  {
    long              result  = -1L;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:100^7*/

//  ************************************************************
//  #sql [Ctx] { select  app.screen_sequence_id
//          
//          from    application app
//          where   app.app_seq_num = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app.screen_sequence_id\n         \n        from    application app\n        where   app.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.setup.AppUtils",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:106^7*/
      
      if( result > 9000 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:110^9*/

//  ************************************************************
//  #sql [Ctx] { select  app.screen_sequence_id-decode(nvl(vcp.submit_partial,'N'),'Y',8000,0) screen_sequence_id
//            
//            from    application app,
//                    vapp_channel_pricing vcp
//            where   app.app_seq_num = :primaryKey and
//                    app.app_type = vcp.app_type(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  app.screen_sequence_id-decode(nvl(vcp.submit_partial,'N'),'Y',8000,0) screen_sequence_id\n           \n          from    application app,\n                  vapp_channel_pricing vcp\n          where   app.app_seq_num =  :1  and\n                  app.app_type = vcp.app_type(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.setup.AppUtils",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:118^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("getSequenceType(" + primaryKey + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( result );
  }

  private String decodeIndustryType(int indusType, int appType)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "Unknown";
    
    try
    {
      connect();
      
      switch(appType)
      {
        case mesConstants.APP_TYPE_VERISIGN:
        case mesConstants.APP_TYPE_NSI:
        case mesConstants.APP_TYPE_VISA:
          /*@lineinfo:generated-code*//*@lineinfo:148^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  company_type_desc description
//              from    visa_company_types
//              where   company_type_code = :indusType
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  company_type_desc description\n            from    visa_company_types\n            where   company_type_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.setup.AppUtils",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,indusType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.setup.AppUtils",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:153^11*/
        break;

        default:
          /*@lineinfo:generated-code*//*@lineinfo:157^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  industype_descr description
//              from    industype
//              where   industype_code = :indusType
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  industype_descr description\n            from    industype\n            where   industype_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.setup.AppUtils",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,indusType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.setup.AppUtils",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:162^11*/
        break;
      }

      rs = it.getResultSet();
      
      if(rs.next())
      {
        result = rs.getString("description");
      }
    }
    catch(Exception e)
    {
      logEntry("decodeIndustryType(" + indusType + ", " + appType + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( result );
  }


  private String decodeBusinessType(int busType, int appType)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "Unknown";
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:198^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bustype_desc
//          from    bustype
//          where   bustype_code = :busType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bustype_desc\n        from    bustype\n        where   bustype_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.setup.AppUtils",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,busType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.setup.AppUtils",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:203^7*/

      rs = it.getResultSet();
      
      if(rs.next())
      {
        result = rs.getString("bustype_desc");
      }
    }
    catch(Exception e)
    {
      logEntry("decodeBusinessType(" + busType + ", " + appType + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( result );
  }

  private String decodeLocationType(int locType, int appType)
  {
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    String            result  = "Unknown";
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:236^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  loctype_desc
//          from    loctype
//          where   loctype_code = :locType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  loctype_desc\n        from    loctype\n        where   loctype_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.setup.AppUtils",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,locType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.setup.AppUtils",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:241^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        result = rs.getString("loctype_desc");
      }
    }
    catch(Exception e)
    {
      logEntry("decodeLocationType(" + locType + ", " + appType + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( result );
  }

  public boolean setTimeStamp(long primaryKey, int departmentCode, int statusCode, int stamp)
  {
    boolean           result  = false;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:272^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//          set     date_completed = sysdate,
//                  status_code = :statusCode
//          where   app_seq_num = :primaryKey and
//                  dept_code = :departmentCode and
//                  date_completed is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n        set     date_completed = sysdate,\n                status_code =  :1 \n        where   app_seq_num =  :2  and\n                dept_code =  :3  and\n                date_completed is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.setup.AppUtils",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,statusCode);
   __sJT_st.setLong(2,primaryKey);
   __sJT_st.setInt(3,departmentCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:280^7*/
    }
    catch(Exception e)
    {
      logEntry("setTimeStamp(" + primaryKey + ", " + departmentCode + ", " + statusCode + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }


  public static String decodeValue(int valueType, int value, int appType)
  {
    String result = "Unknown";
    AppUtils au = new AppUtils();
    
    switch(valueType)
    {
      case VALUE_TYPE_INDUSTRY:
        result = au.decodeIndustryType(value, appType);
        break;

      case VALUE_TYPE_BUSINESS:
        result = au.decodeBusinessType(value, appType);
        break;
        
      case VALUE_TYPE_LOCATION:
        result = au.decodeLocationType(value, appType);
        break;
      
      default:
        break;
    }
    
    return( result );
  }

  public static String decodeValue(int valueType, int value)
  {
    return decodeValue(valueType, value, INVALID_APP_TYPE);
  }

  public static String getApplicationFirstPage(long screenSequenceId)
  {
    AppUtils au = new AppUtils();
    
    return( au.getFirstPage(screenSequenceId) );
  }

  public static long getScreenSequenceType(long primaryKey)
  {
    AppUtils au = new AppUtils();
    
    return( au.getSequenceType(primaryKey) );
  }

  public static boolean setCompletedTimeStamp(long primaryKey, int departmentCode, int statusCode)
  {
    AppUtils au = new AppUtils();
    
    return( au.setTimeStamp(primaryKey, departmentCode, statusCode, TIMESTAMP_COMPLETED) );
  }
}/*@lineinfo:generated-code*/