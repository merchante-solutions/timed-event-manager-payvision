package com.mes.creditmatrix;

import java.util.StringTokenizer;
// log4j classes.
import org.apache.log4j.Category;

public final class CreditMatrixElement_AppStringMatch extends CreditMatrixElement_App
{
  // create class log category
  static Category log = Category.getInstance(CreditMatrixElement_AppStringMatch.class.getName());
  
  // constants
  
  // data members
  protected String    matchstring;
  private float       minScore;
  private float       maxScore;
  
  // class functions
  
  // object functions

  // construction
  public CreditMatrixElement_AppStringMatch()
  {
  }
  
  public void clear()
  {
    super.clear();
    
    matchstring="";
    minScore=0f;
    maxScore=0f;
  }
  
  // accessors
  public String getMatchstring() {  return matchstring; }
  
  // mutators
  public void setMatchstring(String v)
  {
    if(v==null || matchstring.equals(v))
      return;
    matchstring=v.trim().toLowerCase();
    is_dirty=true;
  }
  
  public int  getType()
  {
    return CMETYPE_APPSTRINGMATCH;
  }
    
  public String    getTitle()
  {
    StringBuffer sb = new StringBuffer(256);
    
    sb.append("App Field: \"");
    sb.append(label);
    sb.append("\" MATCH");
    
    return sb.toString();
  }
  
  public String  getDescription()
  {
    return "Requires the application's field value match with the specified string and if so adds a specified offset to the credit matrix score.";
  }
  
  private void calcMinMaxScores()
  {
    minScore=maxScore=0f;
    
    StringTokenizer st = new StringTokenizer(matchstring,",:\n\r");
    String matchstr,escore;
    float scr=0f;
    
    while(st.hasMoreElements()) {
      st.nextElement(); // skip matchstring
      if((escore=(String)st.nextElement())==null) {
        log.error("Match String parsing error (score).  Continuing.");
        continue;
      }
      try {
        scr=Float.valueOf(escore).floatValue();
        if(scr<minScore)
          minScore=scr;
        if(scr>maxScore)
          maxScore=scr;
      }
      catch(Exception e) {
        log.error(e.getMessage());
      }
    }
      
    log.debug("cme_appstringmatch minscore: "+minScore+", maxscore: "+maxScore);
  }
  
  public float calcScore(long app_seq_num)
  {
    // reset
    score=0f;
    
    try {
      app_val = ApplicationProxy.getInstance().getAppVal(app_seq_num,mib_accessor).toLowerCase();
    }
    catch(Exception e) {
      log.error(e.getMessage());
      return 0f;
    }
    
    StringTokenizer st = new StringTokenizer(matchstring,",:\n\r");
    String matchstr,escore;
    
    while(st.hasMoreElements()) {
      if((matchstr=((String)st.nextElement()).trim().toLowerCase())==null) {
        log.error("Match String parsing error (0matchstr).  Continuing.");
        continue;
      }
      if((escore=(String)st.nextElement())==null) {
        log.error("Match String parsing error (score).  Continuing.");
        continue;
      }
      
      try {
        if(matchstr.equals(app_val)) {
          score=Float.valueOf(escore).floatValue();
          break;
        }
      }
      catch(Exception e) {
        log.error("Unable to convert match string score: '"+escore+"' for matching string: '"+matchstr+"'.");
      }
      
    }

    return score;
  }
  
  public String toString()
  {
    final String nl = System.getProperty("line.separator");
    
    StringBuffer sb = new StringBuffer(256);
    
    sb.append("id="+id);
    sb.append("cm_id="+cm_id);
    sb.append(",name="+name);
    sb.append(",label="+label);
    sb.append(",mib_accessor="+mib_accessor);
    sb.append(",matchstring="+matchstring);
    sb.append(" - "+(is_dirty? "DIRTY":"CLEAN"));
    
    sb.append(nl);
    
    return sb.toString();
  }

  public boolean isBounded()
  {
    return true;
  }
  
  public float getMinScore()
  {
    if(minScore==0f && maxScore==0f)
      calcMinMaxScores();
    
    return minScore;
  }
  
  public float getMaxScore()
  {
    if(minScore==0f && maxScore==0f)
      calcMinMaxScores();
    
    return maxScore;
  }

} // class CreditMatrixElement_AppStringMatch
