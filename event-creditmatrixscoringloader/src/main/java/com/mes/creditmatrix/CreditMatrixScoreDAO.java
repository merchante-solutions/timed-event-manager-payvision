package com.mes.creditmatrix;

import java.util.Vector;

/**
 * CreditMatrixScoreDAO interface
 * 
 * DAO definition for CreditMatrix class
 */
public interface CreditMatrixScoreDAO
{
  // constants
  // (none)
  
  // methods
  
  public Vector             getCreditMatrixScoresByApplication(long app_seq_num);

  public CreditMatrixScore  createCreditMatrixScore();
  public long               insertCreditMatrixScore(CreditMatrixScore cms);
  public boolean            deleteCreditMatrixScore(CreditMatrixScore cms);
  public CreditMatrixScore  findCreditMatrixScore(long pkid);
  public boolean            updateCreditMatrixScore(CreditMatrixScore cms);
  public boolean            persistCreditMatrixScore(CreditMatrixScore cms);
  
} // interface CreditMatrixScoreDAO
