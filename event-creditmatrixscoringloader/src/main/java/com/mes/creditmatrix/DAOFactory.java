package com.mes.creditmatrix;

public abstract class DAOFactory
{
  // constants
  // dao factory types
  public static final int       DAO_MESDB         = 1;
  
  // class functions
  // (none)
  
  // object functions
  
  public abstract CreditMatrixDAO         getCreditMatrixDAO();
  public abstract CreditMatrixElementDAO  getCreditMatrixElementDAO();
  public abstract CreditMatrixScoreDAO    getCreditMatrixScoreDAO();
  
} // class DAOFactory
