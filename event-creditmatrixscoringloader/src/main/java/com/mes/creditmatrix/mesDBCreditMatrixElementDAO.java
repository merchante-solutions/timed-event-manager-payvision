package com.mes.creditmatrix;

// log4j classes.
import org.apache.log4j.Category;

/**
 * mesDBCreditMatrixElementDAO class.
 * 
 * CreditMatrixElementDAO implementation for the MES db.
 */
class mesDBCreditMatrixElementDAO implements CreditMatrixElementDAO
{
  // create class log category
  static Category log = Category.getInstance(mesDBCreditMatrixElementDAO.class.getName());
  
  // constants
  
  // data members
  protected CreditMatrixMESDB mesdb;
  
  // construction
  public mesDBCreditMatrixElementDAO()
  {
    mesdb = CreditMatrixMESDB.getInstance();
  }
  
  // class methods
  // (none)
  
  // object methods
  
  public CreditMatrixElement createCreditMatrixElement(int cme_type)
  {
    return mesdb.createCreditMatrixElement(cme_type);
  }
  
  public long insertCreditMatrixElement(CreditMatrixElement cme)
  {
    return mesdb.insert(cme);
  }
  
  public boolean deleteCreditMatrixElement(long pkid)
  {
    return mesdb.deleteCreditMatrixElement(pkid);
  }
  
  public CreditMatrixElement findCreditMatrixElement(long pkid)
  {
    CreditMatrixElement cme=null;
    
    try {
      cme=mesdb.load(pkid);
    }
    catch(Exception e) {
      log.error("mesDBCreditMatrixElementDAO.findCreditMatrixElement() EXCEPTION: '"+e.getMessage()+"'.");
      cme=null;
    }
    
    return cme;
  }
  
  public boolean updateCreditMatrixElement(CreditMatrixElement cme)
  {
    if(cme.getID()<0) {
      log.error("Unable to update CreditMatrixElement: No ID specified.");
      return false;
    }
    
    return persistCreditMatrixElement(cme);
  }
  
  public boolean persistCreditMatrixElement(CreditMatrixElement cme)
  {
    return (mesdb.persist(cme)>0);
  }
  
} // class mesDBCreditMatrixElementDAO
