/*@lineinfo:filename=CreditMatrixMESDB*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.creditmatrix;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.support.StringUtilities;
import sqlj.runtime.ResultSetIterator;

/**
 * CreditMatrixMESDB class.
 * 
 * Performs all interfacing to the MES db for CreditMatrix related objects.
 * SINGLETON.
 * 
 * DB structure:
 * -------------
 *  CREDIT_MATRIX     <1(CM_ID)--(CME_ID)M>    CREDIT_MATRIX_ELEMENTS
 * 
 *  CREDIT_MATRIX               Credit scoring matrices by app type
 *  CREDIT_MATRIX_ELEMENTS      Individual elements in a given credit matrix
 *  CREDIT_MATRIX_SCORE         Credit score for a given application
 */
public class CreditMatrixMESDB extends com.mes.database.SQLJConnectionBase
{
  // create class log category
  static Category log = Category.getInstance(CreditMatrixMESDB.class.getName());
  
  // Singleton
  public static CreditMatrixMESDB getInstance()
  {
    if(_instance==null)
      _instance = new CreditMatrixMESDB();
    
    return _instance;
  }
  private static CreditMatrixMESDB _instance = null;
  ///
  
  // constants
  // (NONE)
  
  // data members
  private int dbTransCount;
  

  // class methods
  // (none)
  

  // object methods
  
  // construction
  private CreditMatrixMESDB()
  {
    // - disable auto commit
    super(false);
    
    dbTransCount=0;
  }
  
  /**
   * getNextAvailID()
   *
   * Retreives the next available Primary key for the specified.
   */
  public long getNextAvailID()
  {
    try {
    
      dbTransStart();
      
      long id=-1L;
      
      /*@lineinfo:generated-code*//*@lineinfo:86^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT  CM_SEQUENCE.nextval
//          
//          FROM    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  CM_SEQUENCE.nextval\n         \n        FROM    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   id = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:91^7*/
      
      return id;
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.getNextAvailID() EXCEPTION: '"+e.getMessage()+"'.");
      return -1L;
    }
    finally {
      dbTransEnd();
    }
  }
  
  public Vector getAppIDsByTypeAndDateRange(long appType,Date startDate,Date endDate)
  {
    Vector rval = new Vector(0,10);
    
    ResultSet rs;
    ResultSetIterator rsItr;
    
    try {
    
      dbTransStart();
      
      Timestamp start_time = new java.sql.Timestamp(startDate.getTime());
      Timestamp end_time = new java.sql.Timestamp(endDate.getTime());
      
      /*@lineinfo:generated-code*//*@lineinfo:118^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT 
//                  APP_SEQ_NUM
//          FROM    
//                  APPLICATION
//          WHERE
//                  APP_CREATED_DATE >= :start_time AND APP_CREATED_DATE <= :end_time
//                  AND APP_TYPE = :appType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT \n                APP_SEQ_NUM\n        FROM    \n                APPLICATION\n        WHERE\n                APP_CREATED_DATE >=  :1  AND APP_CREATED_DATE <=  :2 \n                AND APP_TYPE =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,start_time);
   __sJT_st.setTimestamp(2,end_time);
   __sJT_st.setLong(3,appType);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.creditmatrix.CreditMatrixMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:127^7*/
        
      rs = rsItr.getResultSet();

      while(rs.next())
        rval.addElement(rs.getLong(1));
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.getApplicationTypes() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      dbTransEnd();
    }

    return rval;
  }
  
  public String[][] getApplicationTypes()
  {
    String[][] rval=null;
    
    ResultSet rs;
    ResultSetIterator rsItr;
    
    try {
    
      dbTransStart();
      
      int count=0;
      
      /*@lineinfo:generated-code*//*@lineinfo:157^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT 
//                  COUNT(APP_TYPE_CODE)
//          
//          FROM    
//                  APP_TYPE
//          ORDER BY
//                  APP_DESCRIPTION ASC
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT \n                COUNT(APP_TYPE_CODE)\n         \n        FROM    \n                APP_TYPE\n        ORDER BY\n                APP_DESCRIPTION ASC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:167^7*/
      
      rval = new String[count][2];
      
      /*@lineinfo:generated-code*//*@lineinfo:171^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT 
//                  APP_TYPE_CODE
//                  ,APP_DESCRIPTION
//          FROM    
//                  APP_TYPE
//          ORDER BY
//                  APP_DESCRIPTION ASC
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT \n                APP_TYPE_CODE\n                ,APP_DESCRIPTION\n        FROM    \n                APP_TYPE\n        ORDER BY\n                APP_DESCRIPTION ASC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.creditmatrix.CreditMatrixMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:180^7*/
        
      rs = rsItr.getResultSet();

      int i=0;
      
      while(rs.next()) {
        rval[i][0]=rs.getString(1);
        rval[i][1]=rs.getString(2);
        i++;
      }
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.getApplicationTypes() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      dbTransEnd();
    }

    return rval;
  }

  public Vector getCreditMatricesByAppType(long app_type)
  {
    Vector v = new Vector(0,1);
    CreditMatrix cm;
    CreditMatrixElement cme;
    
    ResultSet rs,rsC;
    ResultSetIterator rsItr,rsItrC;
    
    try {
    
      dbTransStart();
      
      /*@lineinfo:generated-code*//*@lineinfo:215^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT 
//                  CM_ID
//                  ,APP_TYPE
//                  ,NAME
//                  ,AUTOAPPROVE_THRESHOLD
//                  ,AUTODECLINE_THRESHOLD
//          FROM    
//                  CREDIT_MATRIX
//          WHERE   
//                  APP_TYPE=:app_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT \n                CM_ID\n                ,APP_TYPE\n                ,NAME\n                ,AUTOAPPROVE_THRESHOLD\n                ,AUTODECLINE_THRESHOLD\n        FROM    \n                CREDIT_MATRIX\n        WHERE   \n                APP_TYPE= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,app_type);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.creditmatrix.CreditMatrixMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:227^7*/
        
      rs = rsItr.getResultSet();
      
      while(rs.next()) {
        cm = new CreditMatrix();
        if(!dbRecToObject(rs,cm))
          continue;
          
        // load the child elements
        /*@lineinfo:generated-code*//*@lineinfo:237^9*/

//  ************************************************************
//  #sql [Ctx] rsItrC = { SELECT 
//                    CME_ID
//                    ,CM_ID
//                    ,NAME
//                    ,LABEL
//                    ,WEIGHT
//                    ,OFFSET
//                    ,MATCHSTRING
//                    ,MIB_ACCESSOR
//                    ,TYPE
//                    ,NUMRANGE
//                    ,IS_BOUNDED
//            FROM    
//                    CREDIT_MATRIX_ELEMENTS
//            WHERE   
//                    CM_ID=:cm.getID()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1560 = cm.getID();
  try {
   String theSqlTS = "SELECT \n                  CME_ID\n                  ,CM_ID\n                  ,NAME\n                  ,LABEL\n                  ,WEIGHT\n                  ,OFFSET\n                  ,MATCHSTRING\n                  ,MIB_ACCESSOR\n                  ,TYPE\n                  ,NUMRANGE\n                  ,IS_BOUNDED\n          FROM    \n                  CREDIT_MATRIX_ELEMENTS\n          WHERE   \n                  CM_ID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1560);
   // execute query
   rsItrC = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.creditmatrix.CreditMatrixMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:255^9*/
          
        rsC = rsItrC.getResultSet();
        
        while(rsC.next()) {
          if((cme=createCreditMatrixElement(Integer.parseInt(rsC.getString(9).trim())))==null) {
            log.error("createCreditMatrixElement FAILED.  Continuing.");
            continue;
          }
          if(dbRecToObject(rsC,cme)) {
            cm.addCreditMatrixElement(cme);
          }
        }
        
        rsC.close();
        rsItrC.close();

        v.addElement(cm);
      }

      rs.close();
      rsItr.close();
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.getCreditMatricesByAppType() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      dbTransEnd();
    }
    
    return v;
  }
  
  public Vector getCreditMatrixScoresByApplication(long app_seq_num)
  {
    Vector v = new Vector(0,1);
    CreditMatrixScore cms;
    CreditMatrixScoreElement cmse;
    
    ResultSet rs,rsC;
    ResultSetIterator rsItr,rsItrC;
    
    long pkid;
    
    try {
    
      dbTransStart();
      
      /*@lineinfo:generated-code*//*@lineinfo:303^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT 
//                  CMS_ID
//                  ,APP_SEQ_NUM
//                  ,CM_ID
//                  ,DATE_CREATED
//          FROM    
//                  CREDIT_MATRIX_SCORE
//          WHERE   
//                  APP_SEQ_NUM=:app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT \n                CMS_ID\n                ,APP_SEQ_NUM\n                ,CM_ID\n                ,DATE_CREATED\n        FROM    \n                CREDIT_MATRIX_SCORE\n        WHERE   \n                APP_SEQ_NUM= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,app_seq_num);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.creditmatrix.CreditMatrixMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:314^7*/
      
      rs = rsItr.getResultSet();
      
      while(rs.next()) {
        cms = new CreditMatrixScore();
        if(!dbRecToObject(rs,cms))
          continue;
        
        pkid=rs.getLong("CMS_ID");
        
        // load the child elements
        /*@lineinfo:generated-code*//*@lineinfo:326^9*/

//  ************************************************************
//  #sql [Ctx] rsItrC = { SELECT 
//                  CMSE_ID
//                  ,CMS_ID
//                  ,ELEMENT_NAME
//                  ,ELEMENT_TYPE
//                  ,SCORE
//                  ,FLAG_SET
//                  ,MIN_SCORE
//                  ,MAX_SCORE
//                  ,IS_BOUNDED
//                  ,VALUE
//                  ,DESCRIPTOR
//          FROM    
//                  CREDIT_MATRIX_SELEMENTS
//          WHERE   
//                  CMS_ID=:pkid
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT \n                CMSE_ID\n                ,CMS_ID\n                ,ELEMENT_NAME\n                ,ELEMENT_TYPE\n                ,SCORE\n                ,FLAG_SET\n                ,MIN_SCORE\n                ,MAX_SCORE\n                ,IS_BOUNDED\n                ,VALUE\n                ,DESCRIPTOR\n        FROM    \n                CREDIT_MATRIX_SELEMENTS\n        WHERE   \n                CMS_ID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
   // execute query
   rsItrC = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.creditmatrix.CreditMatrixMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:344^9*/
          
        rsC = rsItrC.getResultSet();

        while(rsC.next()) {
          cmse = new CreditMatrixScoreElement();
          if(dbRecToObject(rsC,cmse))
            cms.addScoreElement(cmse);
        }
        
        rsC.close();
        rsItrC.close();

        v.addElement(cms);
      }
      
      rs.close();
      rsItr.close();
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.getCreditMatrixScoresByApplication() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      dbTransEnd();
    }
    
    return v;
  }
  
  public Vector getCreditMatricesByApplication(long app_seq_num)
  {
    log.debug("getCreditMatricesByApplication(app_seq_num: "+app_seq_num+") - START");
    
    Vector v = new Vector(0,1);
    CreditMatrix cm;
    CreditMatrixElement cme;
    
    ResultSet rs,rsC;
    ResultSetIterator rsItr,rsItrC;
    
    try {
    
      dbTransStart();
      
      /*@lineinfo:generated-code*//*@lineinfo:388^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT 
//                  CM_ID
//                  ,APP_TYPE
//                  ,NAME
//                  ,AUTOAPPROVE_THRESHOLD
//                  ,AUTODECLINE_THRESHOLD
//          FROM    
//                  CREDIT_MATRIX
//          WHERE   
//          				APP_TYPE=(SELECT APP_TYPE FROM APPLICATION WHERE APP_SEQ_NUM=:app_seq_num)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT \n                CM_ID\n                ,APP_TYPE\n                ,NAME\n                ,AUTOAPPROVE_THRESHOLD\n                ,AUTODECLINE_THRESHOLD\n        FROM    \n                CREDIT_MATRIX\n        WHERE   \n        \t\t\t\tAPP_TYPE=(SELECT APP_TYPE FROM APPLICATION WHERE APP_SEQ_NUM= :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,app_seq_num);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.creditmatrix.CreditMatrixMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:400^7*/
      
      rs = rsItr.getResultSet();
      
      while(rs.next()) {
        cm = new CreditMatrix();
        if(!dbRecToObject(rs,cm))
          continue;
          
        // load the child elements
        /*@lineinfo:generated-code*//*@lineinfo:410^9*/

//  ************************************************************
//  #sql [Ctx] rsItrC = { SELECT 
//                    CME_ID
//                    ,CM_ID
//                    ,NAME
//                    ,LABEL
//                    ,WEIGHT
//                    ,OFFSET
//                    ,MATCHSTRING
//                    ,MIB_ACCESSOR
//                    ,TYPE
//                    ,NUMRANGE
//                    ,IS_BOUNDED
//            FROM    
//                    CREDIT_MATRIX_ELEMENTS
//            WHERE   
//                    CM_ID=:cm.getID()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1561 = cm.getID();
  try {
   String theSqlTS = "SELECT \n                  CME_ID\n                  ,CM_ID\n                  ,NAME\n                  ,LABEL\n                  ,WEIGHT\n                  ,OFFSET\n                  ,MATCHSTRING\n                  ,MIB_ACCESSOR\n                  ,TYPE\n                  ,NUMRANGE\n                  ,IS_BOUNDED\n          FROM    \n                  CREDIT_MATRIX_ELEMENTS\n          WHERE   \n                  CM_ID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1561);
   // execute query
   rsItrC = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.creditmatrix.CreditMatrixMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:428^9*/
          
        rsC = rsItrC.getResultSet();

        while(rsC.next()) {
          if((cme=createCreditMatrixElement(Integer.parseInt(rsC.getString(9).trim())))==null) {
            log.error("Unrecognized CreditMatrixElement type: '"+rs.getString(9)+"'.  Skipping.");
            continue;
          }
          if(dbRecToObject(rsC,cme)) {
            cm.addCreditMatrixElement(cme);
          }
        }
        
        rsC.close();
        rsItrC.close();

        v.addElement(cm);
      }

      rs.close();
      rsItr.close();
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.getCreditMatricesByApplication() EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      dbTransEnd();
    }
    
    return v;
  }
  
  // INSERT
  
  public long insert(CreditMatrix cm)
  {
    try {
    
      dbTransStart();
      
      long id;
      
      // auto-set id if not already specified
      if((id=cm.getID())<1)
        id=getNextAvailID();

      /*@lineinfo:generated-code*//*@lineinfo:475^7*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO 
//            CREDIT_MATRIX
//          (
//            CM_ID
//            ,APP_TYPE
//            ,NAME
//            ,AUTOAPPROVE_THRESHOLD
//            ,AUTODECLINE_THRESHOLD
//          )
//          VALUES
//          (
//            :id
//            ,:cm.getAppType()
//            ,:cm.getName()
//            ,:cm.getAutoApproveThreshold()
//            ,:cm.getAutoDeclineThreshold()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1562 = cm.getAppType();
 String __sJT_1563 = cm.getName();
 long __sJT_1564 = cm.getAutoApproveThreshold();
 long __sJT_1565 = cm.getAutoDeclineThreshold();
   String theSqlTS = "INSERT INTO \n          CREDIT_MATRIX\n        (\n          CM_ID\n          ,APP_TYPE\n          ,NAME\n          ,AUTOAPPROVE_THRESHOLD\n          ,AUTODECLINE_THRESHOLD\n        )\n        VALUES\n        (\n           :1 \n          , :2 \n          , :3 \n          , :4 \n          , :5 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setInt(2,__sJT_1562);
   __sJT_st.setString(3,__sJT_1563);
   __sJT_st.setLong(4,__sJT_1564);
   __sJT_st.setLong(5,__sJT_1565);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:494^7*/
      
      cm.setID(id);
      
      // add elements
      
      CreditMatrixElement cme;
      
      for(Enumeration e=cm.getCreditMatrixElementsEnumeration();e.hasMoreElements();) {
        cme=(CreditMatrixElement)e.nextElement();
        if((id=insert(cme))<1)
          throw new Exception("Unable to insert Credit Matrix: Error adding constituent element.  Aborting.");
      }
      
      return id;
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.insert(CreditMatrix) EXCEPTION: '"+e.getMessage()+"'.");
      return -1L;
    }
    finally {
      dbTransEnd();
    }
  }
  
  public long insert(CreditMatrixElement cme)
  {
    try {
    
      dbTransStart();
      
      long id=-1L;
      
      // auto-set id if not already specified
      if((id=cme.getID())<1)
        id=getNextAvailID();
        
      if(cme.getType()==CreditMatrixElement.CMETYPE_APPSTRINGMATCH) {
        /*@lineinfo:generated-code*//*@lineinfo:532^9*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO 
//              CREDIT_MATRIX_ELEMENTS
//            (
//              CME_ID
//              ,CM_ID
//              ,NAME
//              ,LABEL
//              ,MIB_ACCESSOR
//              ,MATCHSTRING
//              ,TYPE
//            )
//            VALUES
//            (
//              :id
//              ,:cme.getCreditMatrixID()
//              ,:cme.getName()
//              ,:cme.getLabel()
//              ,:((CreditMatrixElement_AppStringMatch)cme).getMibAccessor()
//              ,:((CreditMatrixElement_AppStringMatch)cme).getMatchstring()
//              ,:cme.getType()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1566 = cme.getCreditMatrixID();
 String __sJT_1567 = cme.getName();
 String __sJT_1568 = cme.getLabel();
 String __sJT_1569 = ((CreditMatrixElement_AppStringMatch)cme).getMibAccessor();
 String __sJT_1570 = ((CreditMatrixElement_AppStringMatch)cme).getMatchstring();
 int __sJT_1571 = cme.getType();
   String theSqlTS = "INSERT INTO \n            CREDIT_MATRIX_ELEMENTS\n          (\n            CME_ID\n            ,CM_ID\n            ,NAME\n            ,LABEL\n            ,MIB_ACCESSOR\n            ,MATCHSTRING\n            ,TYPE\n          )\n          VALUES\n          (\n             :1 \n            , :2 \n            , :3 \n            , :4 \n            , :5 \n            , :6 \n            , :7 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setLong(2,__sJT_1566);
   __sJT_st.setString(3,__sJT_1567);
   __sJT_st.setString(4,__sJT_1568);
   __sJT_st.setString(5,__sJT_1569);
   __sJT_st.setString(6,__sJT_1570);
   __sJT_st.setInt(7,__sJT_1571);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:555^9*/
      
      } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPOFFSET) {
        /*@lineinfo:generated-code*//*@lineinfo:558^9*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO 
//              CREDIT_MATRIX_ELEMENTS
//            (
//              CME_ID
//              ,CM_ID
//              ,NAME
//              ,LABEL
//              ,MIB_ACCESSOR
//              ,OFFSET
//              ,TYPE
//            )
//            VALUES
//            (
//              :id
//              ,:cme.getCreditMatrixID()
//              ,:cme.getName()
//              ,:cme.getLabel()
//              ,:((CreditMatrixElement_AppOffset)cme).getMibAccessor()
//              ,:((CreditMatrixElement_AppOffset)cme).getOffset()
//              ,:cme.getType()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1572 = cme.getCreditMatrixID();
 String __sJT_1573 = cme.getName();
 String __sJT_1574 = cme.getLabel();
 String __sJT_1575 = ((CreditMatrixElement_AppOffset)cme).getMibAccessor();
 float __sJT_1576 = ((CreditMatrixElement_AppOffset)cme).getOffset();
 int __sJT_1577 = cme.getType();
   String theSqlTS = "INSERT INTO \n            CREDIT_MATRIX_ELEMENTS\n          (\n            CME_ID\n            ,CM_ID\n            ,NAME\n            ,LABEL\n            ,MIB_ACCESSOR\n            ,OFFSET\n            ,TYPE\n          )\n          VALUES\n          (\n             :1 \n            , :2 \n            , :3 \n            , :4 \n            , :5 \n            , :6 \n            , :7 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setLong(2,__sJT_1572);
   __sJT_st.setString(3,__sJT_1573);
   __sJT_st.setString(4,__sJT_1574);
   __sJT_st.setString(5,__sJT_1575);
   __sJT_st.setFloat(6,__sJT_1576);
   __sJT_st.setInt(7,__sJT_1577);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:581^9*/
      
      } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPWEIGHT) {
        /*@lineinfo:generated-code*//*@lineinfo:584^9*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO 
//              CREDIT_MATRIX_ELEMENTS
//            (
//              CME_ID
//              ,CM_ID
//              ,NAME
//              ,LABEL
//              ,MIB_ACCESSOR
//              ,WEIGHT
//              ,TYPE
//            )
//            VALUES
//            (
//              :id
//              ,:cme.getCreditMatrixID()
//              ,:cme.getName()
//              ,:cme.getLabel()
//              ,:((CreditMatrixElement_AppWeight)cme).getMibAccessor()
//              ,:((CreditMatrixElement_AppWeight)cme).getWeight()
//              ,:cme.getType()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1578 = cme.getCreditMatrixID();
 String __sJT_1579 = cme.getName();
 String __sJT_1580 = cme.getLabel();
 String __sJT_1581 = ((CreditMatrixElement_AppWeight)cme).getMibAccessor();
 float __sJT_1582 = ((CreditMatrixElement_AppWeight)cme).getWeight();
 int __sJT_1583 = cme.getType();
   String theSqlTS = "INSERT INTO \n            CREDIT_MATRIX_ELEMENTS\n          (\n            CME_ID\n            ,CM_ID\n            ,NAME\n            ,LABEL\n            ,MIB_ACCESSOR\n            ,WEIGHT\n            ,TYPE\n          )\n          VALUES\n          (\n             :1 \n            , :2 \n            , :3 \n            , :4 \n            , :5 \n            , :6 \n            , :7 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setLong(2,__sJT_1578);
   __sJT_st.setString(3,__sJT_1579);
   __sJT_st.setString(4,__sJT_1580);
   __sJT_st.setString(5,__sJT_1581);
   __sJT_st.setFloat(6,__sJT_1582);
   __sJT_st.setInt(7,__sJT_1583);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:607^9*/
      
      } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPNUMRANGE) {
        /*@lineinfo:generated-code*//*@lineinfo:610^9*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO 
//              CREDIT_MATRIX_ELEMENTS
//            (
//              CME_ID
//              ,CM_ID
//              ,NAME
//              ,LABEL
//              ,MIB_ACCESSOR
//              ,NUMRANGE
//              ,TYPE
//              ,IS_BOUNDED
//            )
//            VALUES
//            (
//              :id
//              ,:cme.getCreditMatrixID()
//              ,:cme.getName()
//              ,:cme.getLabel()
//              ,:((CreditMatrixElement_App)cme).getMibAccessor()
//              ,:((CreditMatrixElement_AppNumRange)cme).getNumRange()
//              ,:cme.getType()
//              ,:((CreditMatrixElement_AppNumRange)cme).isNumRangeBounded()? "1":"0"
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1584 = cme.getCreditMatrixID();
 String __sJT_1585 = cme.getName();
 String __sJT_1586 = cme.getLabel();
 String __sJT_1587 = ((CreditMatrixElement_App)cme).getMibAccessor();
 String __sJT_1588 = ((CreditMatrixElement_AppNumRange)cme).getNumRange();
 int __sJT_1589 = cme.getType();
 String __sJT_1590 = ((CreditMatrixElement_AppNumRange)cme).isNumRangeBounded()? "1":"0";
   String theSqlTS = "INSERT INTO \n            CREDIT_MATRIX_ELEMENTS\n          (\n            CME_ID\n            ,CM_ID\n            ,NAME\n            ,LABEL\n            ,MIB_ACCESSOR\n            ,NUMRANGE\n            ,TYPE\n            ,IS_BOUNDED\n          )\n          VALUES\n          (\n             :1 \n            , :2 \n            , :3 \n            , :4 \n            , :5 \n            , :6 \n            , :7 \n            , :8 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setLong(2,__sJT_1584);
   __sJT_st.setString(3,__sJT_1585);
   __sJT_st.setString(4,__sJT_1586);
   __sJT_st.setString(5,__sJT_1587);
   __sJT_st.setString(6,__sJT_1588);
   __sJT_st.setInt(7,__sJT_1589);
   __sJT_st.setString(8,__sJT_1590);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:635^9*/
      
      } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPCREDITSCORE) {
        /*@lineinfo:generated-code*//*@lineinfo:638^9*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO 
//              CREDIT_MATRIX_ELEMENTS
//            (
//              CME_ID
//              ,CM_ID
//              ,NAME
//              ,LABEL
//              ,TYPE
//              ,NUMRANGE
//              ,OFFSET
//              ,IS_BOUNDED
//            )
//            VALUES
//            (
//              :id
//              ,:cme.getCreditMatrixID()
//              ,:cme.getName()
//              ,:cme.getLabel()
//              ,:cme.getType()
//              ,:((CreditMatrixElement_CreditScore)cme).getNumRange()
//              ,:((CreditMatrixElement_CreditScore)cme).getRedFlagThreshold()
//              ,:((CreditMatrixElement_CreditScore)cme).isNumRangeBounded()? "1":"0"
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1591 = cme.getCreditMatrixID();
 String __sJT_1592 = cme.getName();
 String __sJT_1593 = cme.getLabel();
 int __sJT_1594 = cme.getType();
 String __sJT_1595 = ((CreditMatrixElement_CreditScore)cme).getNumRange();
 int __sJT_1596 = ((CreditMatrixElement_CreditScore)cme).getRedFlagThreshold();
 String __sJT_1597 = ((CreditMatrixElement_CreditScore)cme).isNumRangeBounded()? "1":"0";
   String theSqlTS = "INSERT INTO \n            CREDIT_MATRIX_ELEMENTS\n          (\n            CME_ID\n            ,CM_ID\n            ,NAME\n            ,LABEL\n            ,TYPE\n            ,NUMRANGE\n            ,OFFSET\n            ,IS_BOUNDED\n          )\n          VALUES\n          (\n             :1 \n            , :2 \n            , :3 \n            , :4 \n            , :5 \n            , :6 \n            , :7 \n            , :8 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setLong(2,__sJT_1591);
   __sJT_st.setString(3,__sJT_1592);
   __sJT_st.setString(4,__sJT_1593);
   __sJT_st.setInt(5,__sJT_1594);
   __sJT_st.setString(6,__sJT_1595);
   __sJT_st.setInt(7,__sJT_1596);
   __sJT_st.setString(8,__sJT_1597);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:663^9*/
      
      } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPADRSMATCH
                || cme.getType()==CreditMatrixElement.CMETYPE_APPTRMNLPROC) {
        /*@lineinfo:generated-code*//*@lineinfo:667^9*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO 
//              CREDIT_MATRIX_ELEMENTS
//            (
//              CME_ID
//              ,CM_ID
//              ,NAME
//              ,LABEL
//              ,TYPE
//            )
//            VALUES
//            (
//              :id
//              ,:cme.getCreditMatrixID()
//              ,:cme.getName()
//              ,:cme.getLabel()
//              ,:cme.getType()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1598 = cme.getCreditMatrixID();
 String __sJT_1599 = cme.getName();
 String __sJT_1600 = cme.getLabel();
 int __sJT_1601 = cme.getType();
   String theSqlTS = "INSERT INTO \n            CREDIT_MATRIX_ELEMENTS\n          (\n            CME_ID\n            ,CM_ID\n            ,NAME\n            ,LABEL\n            ,TYPE\n          )\n          VALUES\n          (\n             :1 \n            , :2 \n            , :3 \n            , :4 \n            , :5 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setLong(2,__sJT_1598);
   __sJT_st.setString(3,__sJT_1599);
   __sJT_st.setString(4,__sJT_1600);
   __sJT_st.setInt(5,__sJT_1601);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:686^9*/
      
      } else {
        log.error("Unknown Credit Matrix Element type.  Add failed.");
        return -1L;
      }
      
      cme.setID(id);
      
      return id;
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.insert(CreditMatrixElement) EXCEPTION: '"+e.getMessage()+"'.");
      return -1L;
    }
    finally {
      dbTransEnd();
    }
  }
  
  public long insert(CreditMatrixScore cms)
  {
    try {
    
      dbTransStart();
      
      long id=-1L;
      
      // auto-set id if not already specified
      if((id=cms.getID())<1)
        id=getNextAvailID();

      java.sql.Timestamp sqldc = new java.sql.Timestamp(cms.getDateCreated().getTime());
      
      /*@lineinfo:generated-code*//*@lineinfo:720^7*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO 
//            CREDIT_MATRIX_SCORE
//          (
//            CMS_ID
//            ,APP_SEQ_NUM
//            ,CM_ID
//            ,DATE_CREATED
//          )
//          VALUES
//          (
//            :id
//            ,:cms.getAppSeqNum()
//            ,:cms.getCreditMatrixID()
//            ,:sqldc
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1602 = cms.getAppSeqNum();
 long __sJT_1603 = cms.getCreditMatrixID();
   String theSqlTS = "INSERT INTO \n          CREDIT_MATRIX_SCORE\n        (\n          CMS_ID\n          ,APP_SEQ_NUM\n          ,CM_ID\n          ,DATE_CREATED\n        )\n        VALUES\n        (\n           :1 \n          , :2 \n          , :3 \n          , :4 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setLong(2,__sJT_1602);
   __sJT_st.setLong(3,__sJT_1603);
   __sJT_st.setTimestamp(4,sqldc);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:737^7*/
      
      cms.setID(id);
      
      // add elements
      
      CreditMatrixScoreElement cmse;
      
      for(Enumeration e=cms.getScoreElementsEnumeration();e.hasMoreElements();) {
        cmse=(CreditMatrixScoreElement)e.nextElement();
        if((id=insert(cmse))<1)
          throw new Exception("Unable to insert Credit Matrix Score: Error adding constituent element.  Aborting.");
      }
      
      return id;
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.insert(CreditMatrixScore) EXCEPTION: '"+e.getMessage()+"'.");
      return -1L;
    }
    finally {
      dbTransEnd();
    }
  }
  
  public long insert(CreditMatrixScoreElement cmse)
  {
    try {
    
      dbTransStart();
      
      long id=-1L;
      
      // auto-set id if not already specified
      if((id=cmse.getID())<1)
        id=getNextAvailID();

      /*@lineinfo:generated-code*//*@lineinfo:774^7*/

//  ************************************************************
//  #sql [Ctx] { INSERT INTO 
//            CREDIT_MATRIX_SELEMENTS
//          (
//            CMSE_ID
//            ,CMS_ID
//            ,ELEMENT_NAME
//            ,ELEMENT_TYPE
//            ,SCORE
//            ,FLAG_SET
//            ,MIN_SCORE
//            ,MAX_SCORE
//            ,IS_BOUNDED
//            ,VALUE
//            ,DESCRIPTOR
//          )
//          VALUES
//          (
//            :id
//            ,:cmse.getCreditMatrixScoreID()
//            ,:cmse.getElementName()
//            ,:cmse.getElementType()
//            ,:cmse.getScore()
//            ,:cmse.isFlagSet()
//            ,:cmse.getMinScore()
//            ,:cmse.getMaxScore()
//            ,:cmse.isBoundedScore()
//            ,:cmse.getValue()
//            ,:cmse.getDescriptor()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1604 = cmse.getCreditMatrixScoreID();
 String __sJT_1605 = cmse.getElementName();
 int __sJT_1606 = cmse.getElementType();
 float __sJT_1607 = cmse.getScore();
 boolean __sJT_1608 = cmse.isFlagSet();
 float __sJT_1609 = cmse.getMinScore();
 float __sJT_1610 = cmse.getMaxScore();
 boolean __sJT_1611 = cmse.isBoundedScore();
 String __sJT_1612 = cmse.getValue();
 String __sJT_1613 = cmse.getDescriptor();
   String theSqlTS = "INSERT INTO \n          CREDIT_MATRIX_SELEMENTS\n        (\n          CMSE_ID\n          ,CMS_ID\n          ,ELEMENT_NAME\n          ,ELEMENT_TYPE\n          ,SCORE\n          ,FLAG_SET\n          ,MIN_SCORE\n          ,MAX_SCORE\n          ,IS_BOUNDED\n          ,VALUE\n          ,DESCRIPTOR\n        )\n        VALUES\n        (\n           :1 \n          , :2 \n          , :3 \n          , :4 \n          , :5 \n          , :6 \n          , :7 \n          , :8 \n          , :9 \n          , :10 \n          , :11 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setLong(2,__sJT_1604);
   __sJT_st.setString(3,__sJT_1605);
   __sJT_st.setInt(4,__sJT_1606);
   __sJT_st.setFloat(5,__sJT_1607);
   __sJT_st.setBoolean(6,__sJT_1608);
   __sJT_st.setFloat(7,__sJT_1609);
   __sJT_st.setFloat(8,__sJT_1610);
   __sJT_st.setBoolean(9,__sJT_1611);
   __sJT_st.setString(10,__sJT_1612);
   __sJT_st.setString(11,__sJT_1613);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:805^7*/
      
      cmse.setID(id);
      
      return id;
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.insert(CreditMatrixScoreElement) EXCEPTION: '"+e.getMessage()+"'.");
      return -1L;
    }
    finally {
      dbTransEnd();
    }
  }
  
  
  // DELETE
  
  public boolean deleteCreditMatrix(long pkid)
  {
    try {
    
      dbTransStart();
      
      /*@lineinfo:generated-code*//*@lineinfo:829^7*/

//  ************************************************************
//  #sql [Ctx] { DELETE
//              
//          FROM
//                  CREDIT_MATRIX
//          WHERE
//                  CM_ID=:pkid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "DELETE\n            \n        FROM\n                CREDIT_MATRIX\n        WHERE\n                CM_ID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:837^7*/

      // commit the db trans
      commit();
      
      return true;
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.delete(CreditMatrix) EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
    finally {
      dbTransEnd();
    }
  }
  
  public boolean deleteCreditMatrixElement(long pkid)
  {
    try {
    
      dbTransStart();
      
      /*@lineinfo:generated-code*//*@lineinfo:859^7*/

//  ************************************************************
//  #sql [Ctx] { DELETE
//              
//          FROM
//                  CREDIT_MATRIX_ELEMENTS
//          WHERE
//                  CME_ID=:pkid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "DELETE\n            \n        FROM\n                CREDIT_MATRIX_ELEMENTS\n        WHERE\n                CME_ID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:867^7*/

      // commit the db trans
      commit();
      
      return true;
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.delete(CreditMatrixElement) EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
    finally {
      dbTransEnd();
    }
  }
  
  public boolean deleteCreditMatrixScore(long pkid)
  {
    try {
    
      dbTransStart();
      
      /*@lineinfo:generated-code*//*@lineinfo:889^7*/

//  ************************************************************
//  #sql [Ctx] { DELETE
//              
//          FROM
//                  CREDIT_MATRIX_SCORE
//          WHERE
//                  CMS_ID=:pkid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "DELETE\n            \n        FROM\n                CREDIT_MATRIX_SCORE\n        WHERE\n                CMS_ID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:897^7*/

      // commit the db trans
      commit();
      
      return true;
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.delete(CreditMatrixScore) EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
    finally {
      dbTransEnd();
    }
  }
  
  // SELECT
  
  public boolean load(long pkid,CreditMatrix cm)
  {
    ResultSet rs=null;
    ResultSetIterator rsItr=null;
    boolean rval=false;
    
    try {
    
      dbTransStart();
      
      /*@lineinfo:generated-code*//*@lineinfo:925^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT 
//                  :pkid CM_ID
//                  ,APP_TYPE
//                  ,NAME
//                  ,AUTOAPPROVE_THRESHOLD
//                  ,AUTODECLINE_THRESHOLD
//          FROM    
//                  CREDIT_MATRIX
//          WHERE   
//                  CM_ID=:pkid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT \n                 :1  CM_ID\n                ,APP_TYPE\n                ,NAME\n                ,AUTOAPPROVE_THRESHOLD\n                ,AUTODECLINE_THRESHOLD\n        FROM    \n                CREDIT_MATRIX\n        WHERE   \n                CM_ID= :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
   __sJT_st.setLong(2,pkid);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.creditmatrix.CreditMatrixMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:937^7*/
        
      rs = rsItr.getResultSet();
      
      if(!rs.next())
        throw new Exception("CreditMatrix record of id '"+pkid+"' not found.");
      else if(!dbRecToObject(rs,cm))
        throw new Exception("Load CreditMatrix '"+pkid+"' FAILED.");
        
      rs.close();
      rsItr.close();
        
      // load the child elements
      /*@lineinfo:generated-code*//*@lineinfo:950^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT 
//                  CME_ID
//                  ,CM_ID
//                  ,NAME
//                  ,LABEL
//                  ,WEIGHT
//                  ,OFFSET
//                  ,MATCHSTRING
//                  ,MIB_ACCESSOR
//                  ,TYPE
//                  ,NUMRANGE
//                  ,IS_BOUNDED
//          FROM    
//                  CREDIT_MATRIX_ELEMENTS
//          WHERE   
//                  CM_ID=:pkid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT \n                CME_ID\n                ,CM_ID\n                ,NAME\n                ,LABEL\n                ,WEIGHT\n                ,OFFSET\n                ,MATCHSTRING\n                ,MIB_ACCESSOR\n                ,TYPE\n                ,NUMRANGE\n                ,IS_BOUNDED\n        FROM    \n                CREDIT_MATRIX_ELEMENTS\n        WHERE   \n                CM_ID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"23com.mes.creditmatrix.CreditMatrixMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:968^7*/
        
      rs = rsItr.getResultSet();
      
      CreditMatrixElement cme=null;
      int cme_type;
        
      while(rs.next()) {
        cme_type=Integer.parseInt(rs.getString(9).trim());
        if((cme=createCreditMatrixElement(cme_type))==null) {
          log.error("Unrecognized CreditMatrixElement type: '"+cme_type+"'.  Skipping.");
          continue;
        }
        if(!dbRecToObject(rs,cme))
          throw new Exception("Load CreditMatrixElement of id '"+rs.getString(1)+"' FAILED.");
        cm.addCreditMatrixElement(cme);
      }

      rs.close();
      rsItr.close();
      
      rval=true;
        
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.load(CreditMatrix) EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
    finally {
      dbTransEnd();
    }
    
    return rval;
  }
  
  public CreditMatrixElement load(long pkid)
  {
    ResultSet rs=null;
    ResultSetIterator rsItr=null;
    CreditMatrixElement cme=null;
    
    try {
    
      dbTransStart();
      
      /*@lineinfo:generated-code*//*@lineinfo:1013^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT 
//                  :pkid CME_ID
//                  ,CM_ID
//                  ,CME_ID
//                  ,NAME
//                  ,LABEL
//                  ,MATCHSTRING
//                  ,OFFSET
//                  ,MIB_ACCESSOR
//                  ,TYPE
//                  ,NUMRANGE
//                  ,IS_BOUNDED
//          FROM    
//                  CREDIT_MATRIX_ELEMENTS
//          WHERE   
//                  CME_ID=:pkid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT \n                 :1  CME_ID\n                ,CM_ID\n                ,CME_ID\n                ,NAME\n                ,LABEL\n                ,MATCHSTRING\n                ,OFFSET\n                ,MIB_ACCESSOR\n                ,TYPE\n                ,NUMRANGE\n                ,IS_BOUNDED\n        FROM    \n                CREDIT_MATRIX_ELEMENTS\n        WHERE   \n                CME_ID= :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
   __sJT_st.setLong(2,pkid);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"24com.mes.creditmatrix.CreditMatrixMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1031^7*/
      
        
      rs = rsItr.getResultSet();
      
      if(!rs.next())
        throw new Exception("CreditMatrixElement record of id '"+pkid+"' not found.");
        
      if((cme=createCreditMatrixElement(Integer.parseInt(rs.getString(9).trim())))==null)
        throw new Exception("Unrecognized CreditMatrixElement type: '"+rs.getString(9)+"'.  Aborted.");
      
      if(!dbRecToObject(rs,cme))
        throw new Exception("Load CreditMatrixElement '"+pkid+"' FAILED.");
        
      rs.close();
      rsItr.close();
        
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.load(CreditMatrixElement) EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      dbTransEnd();
    }
    
    return cme;
  }
  
  public boolean load(long pkid,CreditMatrixScore cms)
  {
    ResultSet rs=null;
    ResultSetIterator rsItr=null;
    boolean rval=false;
    
    try {
    
      dbTransStart();
      
      /*@lineinfo:generated-code*//*@lineinfo:1069^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT 
//                  :pkid CMS_ID
//                  ,APP_SEQ_NUM
//                  ,CM_ID
//                  ,DATE_CREATED
//          FROM    
//                  CREDIT_MATRIX_SCORE
//          WHERE   
//                  CMS_ID=:pkid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT \n                 :1  CMS_ID\n                ,APP_SEQ_NUM\n                ,CM_ID\n                ,DATE_CREATED\n        FROM    \n                CREDIT_MATRIX_SCORE\n        WHERE   \n                CMS_ID= :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
   __sJT_st.setLong(2,pkid);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"25com.mes.creditmatrix.CreditMatrixMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1080^7*/
        
      rs = rsItr.getResultSet();
      
      if(!rs.next())
        throw new Exception("CreditMatrixScore record of id '"+pkid+"' not found.");
      else if(!dbRecToObject(rs,cms))
        throw new Exception("Load CreditMatrixScore '"+pkid+"' FAILED.");
        
      rs.close();
      rsItr.close();
        
      // load the child elements
      /*@lineinfo:generated-code*//*@lineinfo:1093^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT 
//                  CMSE_ID
//                  ,CMS_ID
//                  ,ELEMENT_NAME
//                  ,ELEMENT_TYPE
//                  ,SCORE
//                  ,FLAG_SET
//                  ,MIN_SCORE
//                  ,MAX_SCORE
//                  ,IS_BOUNDED
//                  ,VALUE
//                  ,DESCRIPTOR
//          FROM    
//                  CREDIT_MATRIX_SELEMENTS
//          WHERE   
//                  CMS_ID=:pkid
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT \n                CMSE_ID\n                ,CMS_ID\n                ,ELEMENT_NAME\n                ,ELEMENT_TYPE\n                ,SCORE\n                ,FLAG_SET\n                ,MIN_SCORE\n                ,MAX_SCORE\n                ,IS_BOUNDED\n                ,VALUE\n                ,DESCRIPTOR\n        FROM    \n                CREDIT_MATRIX_SELEMENTS\n        WHERE   \n                CMS_ID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pkid);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"26com.mes.creditmatrix.CreditMatrixMESDB",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1111^7*/
        
      rs = rsItr.getResultSet();
      
      CreditMatrixScoreElement cmse=null;
        
      while(rs.next()) {
        cmse = new CreditMatrixScoreElement();
        if(!dbRecToObject(rs,cmse))
          throw new Exception("Load CreditMatrixScoreElement of id '"+rs.getString(1)+"' FAILED.");
        cms.addScoreElement(cmse);
      }
      
      rval=true;
        
    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.load(CreditMatrixScore) EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
    finally {
      dbTransEnd();
    }
    
    return rval;
  }
  
  // UPDATE
  
  public long persist(CreditMatrix cm)
  {
    try {
    
      dbTransStart();
      
      int prntCount;
          
      // determine if need to create parent ACR record
      if(cm.getID()>0) {
        /*@lineinfo:generated-code*//*@lineinfo:1150^9*/

//  ************************************************************
//  #sql [Ctx] { SELECT  COUNT(*)
//            
//            FROM    CREDIT_MATRIX
//            WHERE   CM_ID=:cm.getID()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1614 = cm.getID();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  COUNT(*)\n           \n          FROM    CREDIT_MATRIX\n          WHERE   CM_ID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1614);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   prntCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1156^9*/
      } else
        prntCount=0;

      // insert if non-existant
      if(prntCount==0)
        return insert(cm);

      boolean bAltered=false;
      StringBuffer sb = new StringBuffer();
      CreditMatrixElement cme;
    
      for(Enumeration e=cm.getCreditMatrixElementsEnumeration();e.hasMoreElements();) {
        cme=(CreditMatrixElement)e.nextElement();
        
        persist(cme);
        
        // append id to IN clause for later use
        sb.append(',');
        sb.append(cme.getID());
      }
      
      // IMPT: delete those recs not contained in items collection but in db
      // (dynamic query so have to use JDBC)
      //log.debug("Cleansing related child response records for CreditMatrix ID '"+cm.getID()+"'...");
      if(sb.length()>1) {
        final String inclause = sb.substring(1);  // remove preceeding ','
        final String dynmcSQL = 
            "DELETE"
          +" FROM"
                  +" CREDIT_MATRIX_ELEMENTS"
          +" WHERE "
                  +" CM_ID="+cm.getID()
                  +" AND NOT CME_ID IN ("+inclause+")";
            
        PreparedStatement ps = getPreparedStatement(dynmcSQL);
        ps.executeQuery();
        ps.close();
      
      } else {  // i.e. no cm items assoc. w/ cm
        // delete all child recs
        /*@lineinfo:generated-code*//*@lineinfo:1197^9*/

//  ************************************************************
//  #sql [Ctx] { DELETE
//            FROM
//                    CREDIT_MATRIX_ELEMENTS
//            WHERE
//                    CME_ID=:cm.getID()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1615 = cm.getID();
  try {
   String theSqlTS = "DELETE\n          FROM\n                  CREDIT_MATRIX_ELEMENTS\n          WHERE\n                  CME_ID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1615);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1204^9*/
      }
      
      if(cm.isDirty()) {
        /*@lineinfo:generated-code*//*@lineinfo:1208^9*/

//  ************************************************************
//  #sql [Ctx] { UPDATE  
//                    CREDIT_MATRIX
//            SET     
//                    APP_TYPE = :cm.getAppType()
//                    ,NAME = :cm.getName()
//                    ,AUTOAPPROVE_THRESHOLD = :cm.getAutoApproveThreshold()
//                    ,AUTODECLINE_THRESHOLD = :cm.getAutoDeclineThreshold()
//            WHERE   
//                    CM_ID = :cm.getID()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1616 = cm.getAppType();
 String __sJT_1617 = cm.getName();
 long __sJT_1618 = cm.getAutoApproveThreshold();
 long __sJT_1619 = cm.getAutoDeclineThreshold();
 long __sJT_1620 = cm.getID();
   String theSqlTS = "UPDATE  \n                  CREDIT_MATRIX\n          SET     \n                  APP_TYPE =  :1 \n                  ,NAME =  :2 \n                  ,AUTOAPPROVE_THRESHOLD =  :3 \n                  ,AUTODECLINE_THRESHOLD =  :4 \n          WHERE   \n                  CM_ID =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"29com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_1616);
   __sJT_st.setString(2,__sJT_1617);
   __sJT_st.setLong(3,__sJT_1618);
   __sJT_st.setLong(4,__sJT_1619);
   __sJT_st.setLong(5,__sJT_1620);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1219^9*/
        log.debug("CreditMatrix '"+cm.getID()+"' UPDATED.");
        cm.clean();
        bAltered=true;
      }
      
      // commit the db trans
      if(bAltered)
        commit();

    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.persist(CreditMatrix) EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      dbTransEnd();
    }
    
    return cm.getID();
  }
  
  public long persist(CreditMatrixElement cme)
  {
    try {
    
      dbTransStart();
      
      int prntCount;
          
      // determine if need to create parent ACR record
      if(cme.getID()>0) {
        /*@lineinfo:generated-code*//*@lineinfo:1250^9*/

//  ************************************************************
//  #sql [Ctx] { SELECT  COUNT(*)
//            
//            FROM    CREDIT_MATRIX_ELEMENTS
//            WHERE   CME_ID=:cme.getID()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1621 = cme.getID();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  COUNT(*)\n           \n          FROM    CREDIT_MATRIX_ELEMENTS\n          WHERE   CME_ID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1621);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   prntCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1256^9*/
      } else
        prntCount=0;

      // insert if non-existant
      if(prntCount==0)
        return insert(cme);

      boolean bAltered=false;
      StringBuffer sb = new StringBuffer();
      
      if(cme.isDirty()) {
        if(cme.getType()==CreditMatrixElement.CMETYPE_APPSTRINGMATCH) {
          /*@lineinfo:generated-code*//*@lineinfo:1269^11*/

//  ************************************************************
//  #sql [Ctx] { UPDATE  
//                      CREDIT_MATRIX_ELEMENTS
//              SET     
//                      CM_ID = :cme.getCreditMatrixID()
//                      ,NAME = :cme.getName()
//                      ,LABEL = :cme.getLabel()
//                      ,TYPE = :cme.getType()
//                      ,MIB_ACCESSOR = :((CreditMatrixElement_App)cme).getMibAccessor()
//                      ,MATCHSTRING = :((CreditMatrixElement_AppStringMatch)cme).getMatchstring()
//              WHERE   
//                      CME_ID = :cme.getID()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1622 = cme.getCreditMatrixID();
 String __sJT_1623 = cme.getName();
 String __sJT_1624 = cme.getLabel();
 int __sJT_1625 = cme.getType();
 String __sJT_1626 = ((CreditMatrixElement_App)cme).getMibAccessor();
 String __sJT_1627 = ((CreditMatrixElement_AppStringMatch)cme).getMatchstring();
 long __sJT_1628 = cme.getID();
   String theSqlTS = "UPDATE  \n                    CREDIT_MATRIX_ELEMENTS\n            SET     \n                    CM_ID =  :1 \n                    ,NAME =  :2 \n                    ,LABEL =  :3 \n                    ,TYPE =  :4 \n                    ,MIB_ACCESSOR =  :5 \n                    ,MATCHSTRING =  :6 \n            WHERE   \n                    CME_ID =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"31com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1622);
   __sJT_st.setString(2,__sJT_1623);
   __sJT_st.setString(3,__sJT_1624);
   __sJT_st.setInt(4,__sJT_1625);
   __sJT_st.setString(5,__sJT_1626);
   __sJT_st.setString(6,__sJT_1627);
   __sJT_st.setLong(7,__sJT_1628);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1282^11*/
        } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPWEIGHT) {
          /*@lineinfo:generated-code*//*@lineinfo:1284^11*/

//  ************************************************************
//  #sql [Ctx] { UPDATE  
//                      CREDIT_MATRIX_ELEMENTS
//              SET     
//                      CM_ID = :cme.getCreditMatrixID()
//                      ,NAME = :cme.getName()
//                      ,LABEL = :cme.getLabel()
//                      ,TYPE = :cme.getType()
//                      ,MIB_ACCESSOR = :((CreditMatrixElement_App)cme).getMibAccessor()
//                      ,WEIGHT = :((CreditMatrixElement_AppWeight)cme).getWeight()
//              WHERE   
//                      CME_ID = :cme.getID()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1629 = cme.getCreditMatrixID();
 String __sJT_1630 = cme.getName();
 String __sJT_1631 = cme.getLabel();
 int __sJT_1632 = cme.getType();
 String __sJT_1633 = ((CreditMatrixElement_App)cme).getMibAccessor();
 float __sJT_1634 = ((CreditMatrixElement_AppWeight)cme).getWeight();
 long __sJT_1635 = cme.getID();
   String theSqlTS = "UPDATE  \n                    CREDIT_MATRIX_ELEMENTS\n            SET     \n                    CM_ID =  :1 \n                    ,NAME =  :2 \n                    ,LABEL =  :3 \n                    ,TYPE =  :4 \n                    ,MIB_ACCESSOR =  :5 \n                    ,WEIGHT =  :6 \n            WHERE   \n                    CME_ID =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"32com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1629);
   __sJT_st.setString(2,__sJT_1630);
   __sJT_st.setString(3,__sJT_1631);
   __sJT_st.setInt(4,__sJT_1632);
   __sJT_st.setString(5,__sJT_1633);
   __sJT_st.setFloat(6,__sJT_1634);
   __sJT_st.setLong(7,__sJT_1635);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1297^11*/
        } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPOFFSET) {
          /*@lineinfo:generated-code*//*@lineinfo:1299^11*/

//  ************************************************************
//  #sql [Ctx] { UPDATE  
//                      CREDIT_MATRIX_ELEMENTS
//              SET     
//                      CM_ID = :cme.getCreditMatrixID()
//                      ,NAME = :cme.getName()
//                      ,LABEL = :cme.getLabel()
//                      ,TYPE = :cme.getType()
//                      ,MIB_ACCESSOR = :((CreditMatrixElement_App)cme).getMibAccessor()
//                      ,OFFSET = :((CreditMatrixElement_AppOffset)cme).getOffset()
//              WHERE   
//                      CME_ID = :cme.getID()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1636 = cme.getCreditMatrixID();
 String __sJT_1637 = cme.getName();
 String __sJT_1638 = cme.getLabel();
 int __sJT_1639 = cme.getType();
 String __sJT_1640 = ((CreditMatrixElement_App)cme).getMibAccessor();
 float __sJT_1641 = ((CreditMatrixElement_AppOffset)cme).getOffset();
 long __sJT_1642 = cme.getID();
   String theSqlTS = "UPDATE  \n                    CREDIT_MATRIX_ELEMENTS\n            SET     \n                    CM_ID =  :1 \n                    ,NAME =  :2 \n                    ,LABEL =  :3 \n                    ,TYPE =  :4 \n                    ,MIB_ACCESSOR =  :5 \n                    ,OFFSET =  :6 \n            WHERE   \n                    CME_ID =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"33com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1636);
   __sJT_st.setString(2,__sJT_1637);
   __sJT_st.setString(3,__sJT_1638);
   __sJT_st.setInt(4,__sJT_1639);
   __sJT_st.setString(5,__sJT_1640);
   __sJT_st.setFloat(6,__sJT_1641);
   __sJT_st.setLong(7,__sJT_1642);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1312^11*/
        } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPNUMRANGE) {
          
          log.debug("CreditMatrixMESDB.persist(cme) - cme.isNumRangeBounded()="+((CreditMatrixElement_AppNumRange)cme).isNumRangeBounded());
          
          /*@lineinfo:generated-code*//*@lineinfo:1317^11*/

//  ************************************************************
//  #sql [Ctx] { UPDATE  
//                      CREDIT_MATRIX_ELEMENTS
//              SET     
//                      CM_ID = :cme.getCreditMatrixID()
//                      ,NAME = :cme.getName()
//                      ,LABEL = :cme.getLabel()
//                      ,TYPE = :cme.getType()
//                      ,MIB_ACCESSOR = :((CreditMatrixElement_App)cme).getMibAccessor()
//                      ,NUMRANGE = :((CreditMatrixElement_AppNumRange)cme).getNumRange()
//                      ,IS_BOUNDED = :((CreditMatrixElement_AppNumRange)cme).isNumRangeBounded()? "1":"0"
//              WHERE   
//                      CME_ID = :cme.getID()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1643 = cme.getCreditMatrixID();
 String __sJT_1644 = cme.getName();
 String __sJT_1645 = cme.getLabel();
 int __sJT_1646 = cme.getType();
 String __sJT_1647 = ((CreditMatrixElement_App)cme).getMibAccessor();
 String __sJT_1648 = ((CreditMatrixElement_AppNumRange)cme).getNumRange();
 String __sJT_1649 = ((CreditMatrixElement_AppNumRange)cme).isNumRangeBounded()? "1":"0";
 long __sJT_1650 = cme.getID();
   String theSqlTS = "UPDATE  \n                    CREDIT_MATRIX_ELEMENTS\n            SET     \n                    CM_ID =  :1 \n                    ,NAME =  :2 \n                    ,LABEL =  :3 \n                    ,TYPE =  :4 \n                    ,MIB_ACCESSOR =  :5 \n                    ,NUMRANGE =  :6 \n                    ,IS_BOUNDED =  :7 \n            WHERE   \n                    CME_ID =  :8";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"34com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1643);
   __sJT_st.setString(2,__sJT_1644);
   __sJT_st.setString(3,__sJT_1645);
   __sJT_st.setInt(4,__sJT_1646);
   __sJT_st.setString(5,__sJT_1647);
   __sJT_st.setString(6,__sJT_1648);
   __sJT_st.setString(7,__sJT_1649);
   __sJT_st.setLong(8,__sJT_1650);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1331^11*/
        } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPCREDITSCORE) {
          /*@lineinfo:generated-code*//*@lineinfo:1333^11*/

//  ************************************************************
//  #sql [Ctx] { UPDATE  
//                      CREDIT_MATRIX_ELEMENTS
//              SET     
//                      CM_ID = :cme.getCreditMatrixID()
//                      ,NAME = :cme.getName()
//                      ,LABEL = :cme.getLabel()
//                      ,TYPE = :cme.getType()
//                      ,NUMRANGE = :((CreditMatrixElement_CreditScore)cme).getNumRange()
//                      ,OFFSET = :((CreditMatrixElement_CreditScore)cme).getRedFlagThreshold()
//                      ,IS_BOUNDED = :((CreditMatrixElement_CreditScore)cme).isNumRangeBounded()? "1":"0"
//              WHERE   
//                      CME_ID = :cme.getID()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1651 = cme.getCreditMatrixID();
 String __sJT_1652 = cme.getName();
 String __sJT_1653 = cme.getLabel();
 int __sJT_1654 = cme.getType();
 String __sJT_1655 = ((CreditMatrixElement_CreditScore)cme).getNumRange();
 int __sJT_1656 = ((CreditMatrixElement_CreditScore)cme).getRedFlagThreshold();
 String __sJT_1657 = ((CreditMatrixElement_CreditScore)cme).isNumRangeBounded()? "1":"0";
 long __sJT_1658 = cme.getID();
   String theSqlTS = "UPDATE  \n                    CREDIT_MATRIX_ELEMENTS\n            SET     \n                    CM_ID =  :1 \n                    ,NAME =  :2 \n                    ,LABEL =  :3 \n                    ,TYPE =  :4 \n                    ,NUMRANGE =  :5 \n                    ,OFFSET =  :6 \n                    ,IS_BOUNDED =  :7 \n            WHERE   \n                    CME_ID =  :8";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"35com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1651);
   __sJT_st.setString(2,__sJT_1652);
   __sJT_st.setString(3,__sJT_1653);
   __sJT_st.setInt(4,__sJT_1654);
   __sJT_st.setString(5,__sJT_1655);
   __sJT_st.setInt(6,__sJT_1656);
   __sJT_st.setString(7,__sJT_1657);
   __sJT_st.setLong(8,__sJT_1658);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1347^11*/
        
        } else if(cme.getType()==CreditMatrixElement.CMETYPE_APPADRSMATCH) {
          /*@lineinfo:generated-code*//*@lineinfo:1350^11*/

//  ************************************************************
//  #sql [Ctx] { UPDATE  
//                      CREDIT_MATRIX_ELEMENTS
//              SET     
//                      CM_ID = :cme.getCreditMatrixID()
//                      ,NAME = :cme.getName()
//                      ,LABEL = :cme.getLabel()
//                      ,TYPE = :cme.getType()
//              WHERE   
//                      CME_ID = :cme.getID()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1659 = cme.getCreditMatrixID();
 String __sJT_1660 = cme.getName();
 String __sJT_1661 = cme.getLabel();
 int __sJT_1662 = cme.getType();
 long __sJT_1663 = cme.getID();
   String theSqlTS = "UPDATE  \n                    CREDIT_MATRIX_ELEMENTS\n            SET     \n                    CM_ID =  :1 \n                    ,NAME =  :2 \n                    ,LABEL =  :3 \n                    ,TYPE =  :4 \n            WHERE   \n                    CME_ID =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"36com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1659);
   __sJT_st.setString(2,__sJT_1660);
   __sJT_st.setString(3,__sJT_1661);
   __sJT_st.setInt(4,__sJT_1662);
   __sJT_st.setLong(5,__sJT_1663);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1361^11*/
        
        } else {
          log.error("Unable to persist Credit Matrix Element: Unknown type.");
          return -1L;
        }
        log.debug("CreditMatrixElement '"+cme.getID()+"' UPDATED.");
        cme.clean();
        bAltered=true;
      }
      
      // commit the db trans
      if(bAltered)
        commit();

    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.persist(CreditMatrixElement) EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      dbTransEnd();
    }
    
    return cme.getID();
  }
  
  public long persist(CreditMatrixScoreElement cmse)
  {
    try {
    
      dbTransStart();
      
      int prntCount;
          
      // determine if need to create parent ACR record
      if(cmse.getID()>0) {
        /*@lineinfo:generated-code*//*@lineinfo:1397^9*/

//  ************************************************************
//  #sql [Ctx] { SELECT  COUNT(*)
//            
//            FROM    CREDIT_MATRIX_SELEMENTS
//            WHERE   CMSE_ID=:cmse.getID()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1664 = cmse.getID();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  COUNT(*)\n           \n          FROM    CREDIT_MATRIX_SELEMENTS\n          WHERE   CMSE_ID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"37com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1664);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   prntCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1403^9*/
      } else
        prntCount=0;

      // insert if non-existant
      if(prntCount==0)
        return insert(cmse);

      boolean bAltered=false;
      StringBuffer sb = new StringBuffer();
      
      if(cmse.isDirty()) {
        
        /*@lineinfo:generated-code*//*@lineinfo:1416^9*/

//  ************************************************************
//  #sql [Ctx] { UPDATE  
//                    CREDIT_MATRIX_SELEMENTS
//            SET     
//                    CMS_ID = :cmse.getCreditMatrixScoreID()
//                    ,ELEMENT_NAME = :cmse.getElementName()
//                    ,ELEMENT_TYPE = :cmse.getElementType()
//                    ,SCORE = :cmse.getScore()
//                    ,FLAG_SET = :cmse.isFlagSet()
//                    ,MIN_SCORE = :cmse.getMinScore()
//                    ,MAX_SCORE = :cmse.getMaxScore()
//                    ,IS_BOUNDED = :cmse.isBoundedScore()
//                    ,VALUE = :cmse.getValue()
//                    ,DESCRIPTOR = :cmse.getDescriptor()
//            WHERE   
//                    CMSE_ID = :cmse.getID()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1665 = cmse.getCreditMatrixScoreID();
 String __sJT_1666 = cmse.getElementName();
 int __sJT_1667 = cmse.getElementType();
 float __sJT_1668 = cmse.getScore();
 boolean __sJT_1669 = cmse.isFlagSet();
 float __sJT_1670 = cmse.getMinScore();
 float __sJT_1671 = cmse.getMaxScore();
 boolean __sJT_1672 = cmse.isBoundedScore();
 String __sJT_1673 = cmse.getValue();
 String __sJT_1674 = cmse.getDescriptor();
 long __sJT_1675 = cmse.getID();
   String theSqlTS = "UPDATE  \n                  CREDIT_MATRIX_SELEMENTS\n          SET     \n                  CMS_ID =  :1 \n                  ,ELEMENT_NAME =  :2 \n                  ,ELEMENT_TYPE =  :3 \n                  ,SCORE =  :4 \n                  ,FLAG_SET =  :5 \n                  ,MIN_SCORE =  :6 \n                  ,MAX_SCORE =  :7 \n                  ,IS_BOUNDED =  :8 \n                  ,VALUE =  :9 \n                  ,DESCRIPTOR =  :10 \n          WHERE   \n                  CMSE_ID =  :11";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"38com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1665);
   __sJT_st.setString(2,__sJT_1666);
   __sJT_st.setInt(3,__sJT_1667);
   __sJT_st.setFloat(4,__sJT_1668);
   __sJT_st.setBoolean(5,__sJT_1669);
   __sJT_st.setFloat(6,__sJT_1670);
   __sJT_st.setFloat(7,__sJT_1671);
   __sJT_st.setBoolean(8,__sJT_1672);
   __sJT_st.setString(9,__sJT_1673);
   __sJT_st.setString(10,__sJT_1674);
   __sJT_st.setLong(11,__sJT_1675);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1433^9*/
        
        log.debug("CreditMatrixScoreElement '"+cmse.getID()+"' UPDATED.");
        cmse.clean();
        bAltered=true;
      }
      
      // commit the db trans
      if(bAltered)
        commit();

    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.persist(CreditMatrixScoreElement) EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      dbTransEnd();
    }
    
    return cmse.getID();
  }
  
  public long persist(CreditMatrixScore cms)
  {
    try {
    
      dbTransStart();
      
      int prntCount;
          
      // determine if need to create parent ACR record
      if(cms.getID()>0) {
        /*@lineinfo:generated-code*//*@lineinfo:1465^9*/

//  ************************************************************
//  #sql [Ctx] { SELECT  COUNT(*)
//            
//            FROM    CREDIT_MATRIX_SCORE
//            WHERE   CMS_ID=:cms.getID()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1676 = cms.getID();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT  COUNT(*)\n           \n          FROM    CREDIT_MATRIX_SCORE\n          WHERE   CMS_ID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1676);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   prntCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1471^9*/
      } else
        prntCount=0;

      // insert if non-existant
      if(prntCount==0)
        return insert(cms);

      boolean bAltered=false;
      StringBuffer sb = new StringBuffer();
      CreditMatrixScoreElement cmse;
    
      for(Enumeration e=cms.getScoreElementsEnumeration();e.hasMoreElements();) {
        cmse=(CreditMatrixScoreElement)e.nextElement();
        
        persist(cmse);
        
        // append id to IN clause for later use
        sb.append(',');
        sb.append(cmse.getID());
      }
      
      // IMPT: delete those recs not contained in items collection but in db
      // (dynamic query so have to use JDBC)
      //log.debug("Cleansing related child response records for CreditMatrix ID '"+cm.getID()+"'...");
      if(sb.length()>1) {
        final String inclause = sb.substring(1);  // remove preceeding ','
        final String dynmcSQL = 
            "DELETE"
          +" FROM"
                  +" CREDIT_MATRIX_SELEMENTS"
          +" WHERE "
                  +" CMS_ID="+cms.getID()
                  +" AND NOT CMSE_ID IN ("+inclause+")";
            
        PreparedStatement ps = getPreparedStatement(dynmcSQL);
        ps.executeQuery();
        ps.close();
      
      } else {  // i.e. no cm items assoc. w/ cm
        // delete all child recs
        /*@lineinfo:generated-code*//*@lineinfo:1512^9*/

//  ************************************************************
//  #sql [Ctx] { DELETE
//            FROM
//                    CREDIT_MATRIX_SELEMENTS
//            WHERE
//                    CMS_ID=:cms.getID()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1677 = cms.getID();
  try {
   String theSqlTS = "DELETE\n          FROM\n                  CREDIT_MATRIX_SELEMENTS\n          WHERE\n                  CMS_ID= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"40com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1677);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1519^9*/
      }
      
      if(cms.isDirty()) {
        /*@lineinfo:generated-code*//*@lineinfo:1523^9*/

//  ************************************************************
//  #sql [Ctx] { UPDATE  
//                    CREDIT_MATRIX_SCORE
//            SET     
//                    APP_SEQ_NUM = :cms.getAppSeqNum()
//                    ,CM_ID = :cms.getCreditMatrixID()
//            WHERE   
//                    CMS_ID = :cms.getID()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1678 = cms.getAppSeqNum();
 long __sJT_1679 = cms.getCreditMatrixID();
 long __sJT_1680 = cms.getID();
   String theSqlTS = "UPDATE  \n                  CREDIT_MATRIX_SCORE\n          SET     \n                  APP_SEQ_NUM =  :1 \n                  ,CM_ID =  :2 \n          WHERE   \n                  CMS_ID =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"41com.mes.creditmatrix.CreditMatrixMESDB",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1678);
   __sJT_st.setLong(2,__sJT_1679);
   __sJT_st.setLong(3,__sJT_1680);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1532^9*/
        log.debug("CreditMatrixScore '"+cms.getID()+"' UPDATED.");
        cms.clean();
        bAltered=true;
      }
      
      // commit the db trans
      if(bAltered)
        commit();

    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.persist(CreditMatrixScore) EXCEPTION: '"+e.getMessage()+"'.");
    }
    finally {
      dbTransEnd();
    }
    
    return cms.getID();
  }
  
  ////////////////////////
  
  protected CreditMatrixElement createCreditMatrixElement(int cme_type)
  {
    log.debug("mesdb.createCreditMatrixElement() cme_type="+cme_type);
    
    CreditMatrixElement cme;
    
    if(cme_type==CreditMatrixElement.CMETYPE_APPWEIGHT) {
      cme = new CreditMatrixElement_AppWeight();
    } else if(cme_type==CreditMatrixElement.CMETYPE_APPOFFSET) {
      cme = new CreditMatrixElement_AppOffset();
    } else if(cme_type==CreditMatrixElement.CMETYPE_APPSTRINGMATCH) {
      cme = new CreditMatrixElement_AppStringMatch();
    } else if(cme_type==CreditMatrixElement.CMETYPE_APPNUMRANGE) {
      cme = new CreditMatrixElement_AppNumRange();
    } else if(cme_type==CreditMatrixElement.CMETYPE_APPCREDITSCORE) {
      cme = new CreditMatrixElement_CreditScore();
    } else if(cme_type==CreditMatrixElement.CMETYPE_APPADRSMATCH) {
      cme = new CreditMatrixElement_AdrsMatch();
    } else if(cme_type==CreditMatrixElement.CMETYPE_APPTRMNLPROC) {
      cme = new CreditMatrixElement_TrmnlProc();
    } else
      cme=null;
      
    return cme;
  }
  
  ////////////////////////

  protected boolean dbRecToObject(ResultSet rs, CreditMatrix cm)
  {
    try {
      
      String sDta;
      final ResultSetMetaData rsMtaDta = rs.getMetaData();
      
      for(int i=1; i<=rsMtaDta.getColumnCount(); i++) {
      
        sDta = rs.getString(i);
        if(null == sDta)
          sDta = "";
          
        //log.debug("dbRecToObject(ACRDefinition) - "+rsMtaDta.getColumnName(i)+" = '"+sDta+"'");
        
        if(rsMtaDta.getColumnName(i).equals("CM_ID")) {
          if(sDta.length()>0)
            cm.setID(Long.parseLong(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("APP_TYPE")) {
          if(sDta.length()>0)
            cm.setAppType(Integer.parseInt(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("NAME")) {
          cm.setName(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("AUTOAPPROVE_THRESHOLD")) {
          if(sDta.length()>0)
            cm.setAutoApproveThreshold(Long.parseLong(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("AUTODECLINE_THRESHOLD")) {
          if(sDta.length()>0)
            cm.setAutoDeclineThreshold(Long.parseLong(sDta));
        }

      }

    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.dbRecToObject(CreditMatrix) - EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
    
    // object loaded successfully so mark as clean
    cm.clean();
    
    return true;
  }
  
  protected boolean dbRecToObject(ResultSet rs, CreditMatrixElement cme)
  {
    try {
      
      String sDta;
      final ResultSetMetaData rsMtaDta = rs.getMetaData();
      
      for(int i=1; i<=rsMtaDta.getColumnCount(); i++) {
      
        sDta = rs.getString(i);
        if(null == sDta)
          sDta = "";
          
        if(rsMtaDta.getColumnName(i).equals("CME_ID")) {
          if(sDta.length()>0)
            cme.setID(Long.parseLong(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("CM_ID")) {
          if(sDta.length()>0)
            cme.setCreditMatrixID(Long.parseLong(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("NAME")) {
          cme.setName(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("LABEL")) {
          cme.setLabel(sDta);
        }
        
        switch(cme.getType()) {
          case CreditMatrixElement.CMETYPE_APPWEIGHT:
            if(rsMtaDta.getColumnName(i).equals("MIB_ACCESSOR")) {
              ((CreditMatrixElement_App)cme).setMibAccessor(sDta);
            } else if(rsMtaDta.getColumnName(i).equals("WEIGHT")) {
              ((CreditMatrixElement_AppWeight)cme).setWeight(Float.valueOf(sDta).floatValue());
            }
            break;
          case CreditMatrixElement.CMETYPE_APPOFFSET:
            if(rsMtaDta.getColumnName(i).equals("MIB_ACCESSOR")) {
              ((CreditMatrixElement_App)cme).setMibAccessor(sDta);
            } else if(rsMtaDta.getColumnName(i).equals("OFFSET")) {
              ((CreditMatrixElement_AppOffset)cme).setOffset(Float.valueOf(sDta).floatValue());
            }
            break;
          case CreditMatrixElement.CMETYPE_APPSTRINGMATCH:
            if(rsMtaDta.getColumnName(i).equals("MIB_ACCESSOR")) {
              ((CreditMatrixElement_App)cme).setMibAccessor(sDta);
            } else if(rsMtaDta.getColumnName(i).equals("MATCHSTRING")) {
              ((CreditMatrixElement_AppStringMatch)cme).setMatchstring(sDta);
            }
            break;
          case CreditMatrixElement.CMETYPE_APPNUMRANGE:
            if(rsMtaDta.getColumnName(i).equals("MIB_ACCESSOR")) {
              ((CreditMatrixElement_App)cme).setMibAccessor(sDta);
            } else if(rsMtaDta.getColumnName(i).equals("NUMRANGE")) {
              ((CreditMatrixElement_AppNumRange)cme).setNumRange(sDta);
            } else if(rsMtaDta.getColumnName(i).equals("IS_BOUNDED")) {
              ((CreditMatrixElement_AppNumRange)cme).setIsRangeBound(sDta.equals("1"));
            }
            break;
          case CreditMatrixElement.CMETYPE_APPCREDITSCORE:
            if(rsMtaDta.getColumnName(i).equals("NUMRANGE")) {
              ((CreditMatrixElement_CreditScore)cme).setNumRange(sDta);
            } else if(rsMtaDta.getColumnName(i).equals("OFFSET")) {
              ((CreditMatrixElement_CreditScore)cme).setRedFlagThreshold(StringUtilities.stringToInt(sDta));
            } else if(rsMtaDta.getColumnName(i).equals("IS_BOUNDED")) {
              ((CreditMatrixElement_CreditScore)cme).setIsRangeBound(sDta.equals("1"));
            }
            break;
        }

      }

    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.dbRecToObject(CreditMatrixElement) - EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
    
    // object loaded successfully so mark as clean
    cme.clean();
    
    return true;
  }
  
  protected boolean dbRecToObject(ResultSet rs, CreditMatrixScore cms)
  {
    try {
      
      String sDta;
      final ResultSetMetaData rsMtaDta = rs.getMetaData();
      
      for(int i=1; i<=rsMtaDta.getColumnCount(); i++) {
      
        sDta = rs.getString(i);
        if(null == sDta)
          sDta = "";
          
        if(rsMtaDta.getColumnName(i).equals("CMS_ID")) {
          if(sDta.length()>0)
            cms.setID(Long.parseLong(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("APP_SEQ_NUM")) {
          if(sDta.length()>0)
            cms.setAppSeqNum(Long.parseLong(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("CM_ID")) {
          if(sDta.length()>0)
            cms.setCreditMatrixID(Long.parseLong(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("DATE_CREATED")) {
          if(rs.getDate(i) != null)
            cms.setDateCreated(rs.getTimestamp(i));
        }
      }

    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.dbRecToObject(CreditMatrixScore) - EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
    
    // object loaded successfully so mark as clean
    cms.clean();
    
    return true;
  }
  
  protected boolean dbRecToObject(ResultSet rs, CreditMatrixScoreElement cmse)
  {
    try {
      
      String sDta;
      final ResultSetMetaData rsMtaDta = rs.getMetaData();
      
      for(int i=1; i<=rsMtaDta.getColumnCount(); i++) {
      
        sDta = rs.getString(i);
        if(null == sDta)
          sDta = "";
        
        if(rsMtaDta.getColumnName(i).equals("CMSE_ID")) {
          if(sDta.length()>0)
            cmse.setID(Long.parseLong(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("CMS_ID")) {
          if(sDta.length()>0)
            cmse.setCreditMatrixScoreID(Long.parseLong(sDta));
        } else if(rsMtaDta.getColumnName(i).equals("ELEMENT_NAME")) {
          if(sDta.length()>0)
            cmse.setElementName(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("ELEMENT_TYPE")) {
          if(sDta.length()>0)
            cmse.setElementType(Integer.parseInt(sDta.trim()));
        } else if(rsMtaDta.getColumnName(i).equals("VALUE")) {
          if(sDta.length()>0)
            cmse.setValue(sDta);
        } else if(rsMtaDta.getColumnName(i).equals("SCORE")) {
          if(sDta.length()>0)
            cmse.setScore(Float.valueOf(sDta).floatValue());
        } else if(rsMtaDta.getColumnName(i).equals("FLAG_SET")) {
          if(sDta.equals("1"))
            cmse.turnFlagOn();
           else
            cmse.turnFlagOff();
        } else if(rsMtaDta.getColumnName(i).equals("IS_BOUNDED")) {
          cmse.setBoundedScore(sDta.equals("1"));
        } else if(rsMtaDta.getColumnName(i).equals("MIN_SCORE")) {
          if(sDta.length()>0)
            cmse.setMinScore(Float.valueOf(sDta).floatValue());
        } else if(rsMtaDta.getColumnName(i).equals("MAX_SCORE")) {
          if(sDta.length()>0)
            cmse.setMaxScore(Float.valueOf(sDta).floatValue());
        } else if(rsMtaDta.getColumnName(i).equals("DESCRIPTOR")) {
          if(sDta.length()>0)
            cmse.setDescriptor(sDta);
        }

      }

    }
    catch(Exception e) {
      log.error("CreditMatrixMESDB.dbRecToObject(CreditMatrixScoreElement) - EXCEPTION: '"+e.getMessage()+"'.");
      return false;
    }
    
    // object loaded successfully so mark as clean
    cmse.clean();
    
    return true;
  }

  /////////////////////////
  
  public void dbTransStart()
  {
		try {
      
		  if(dbTransCount<1 || isConnectionStale()) {
		    log.debug("Connecting to db... (dbTransCount="+dbTransCount+")");
		    connect();
		    if(con==null)
		      log.error("dbTransStart(): NULL db connection returned!");
		    if(Ctx==null)
		      log.error("dbTransStart(): NULL db Ctx returned!");
		  }
		
		}
		catch(Exception e) {
			log.error(e.getMessage());
		}
		finally {
      dbTransCount++;
	    //log.debug("dbTransStart(): dbTransCount="+dbTransCount);
		}
  }
  
  public void dbTransEnd()
  {
		try {
	    
	    dbTransCount--;
	    //log.debug("dbTransEnd(): dbTransCount="+dbTransCount);
      
      if(dbTransCount<1) {
        log.debug("Cleaning up db connection...");
		    cleanUp();
		  }
	    
		}
		catch(Exception e) {
			log.error(e.getMessage());
		}
  }
  

}/*@lineinfo:generated-code*/