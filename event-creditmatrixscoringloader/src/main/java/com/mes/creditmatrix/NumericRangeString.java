package com.mes.creditmatrix;

import java.util.StringTokenizer;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;

/**
 * NumericRangeString class
 * 
 * Helper class for parsing numeric range strings for use by credit matrix element types that require numeric ranges.
 */
public final class NumericRangeString
{
  // create class log category
  static Category log = Category.getInstance(NumericRangeString.class.getName());
  
  // constants
  
  // data members
  private String      numRange;
  private float       minScore;
  private float       maxScore;
  private boolean     _isRangeBound;
  
  // numRange FORMAT:
  //    "{lbound 1} - {ubound 1} : {score 1}
  //    ...
  //    {lbound N} - {ubound N} : {score N}"
  
  // _isRangeBound - true: all declered ranges cover the entire possible range the target value may have.
  
  
  // class functions
  // (NONE)
  
  
  // object functions

  // construction
  public NumericRangeString()
  {
    clear();
  }
  
  public void clear()
  {
    numRange="";
    minScore=0f;
    maxScore=0f;
    _isRangeBound=false;
  }
  
  // accessors
  
  public String getNumRange()
  {
    return numRange;
  }
  
  public float getMinScore()
  {
    if(minScore==0f && maxScore==0f)
      calcMinMaxScores();
    
    return minScore;
  }
  
  public float getMaxScore()
  {
    if(minScore==0f && maxScore==0f)
      calcMinMaxScores();
    
    return maxScore;
  }
  
  public boolean isRangeBound()
  {
    return _isRangeBound;
  }
  
  // mutators

  public void setNumRange(String v)
  {
    if(v!=null)
      numRange=v;
  }
  
  public void setMinScore(float v)
  {
    minScore=v;
  }
  
  public void setMaxScore(float v)
  {
    maxScore=v;
  }
  
  public void setIsRangeBound(boolean v)
  {
    _isRangeBound=v;
  }
  
  private void calcMinMaxScores()
  {
    minScore=maxScore=0f;
    
    try {
      float[] range;
      StringTokenizer st = new StringTokenizer(numRange,"-{},:\n\r");
      int loopCnt=0;
      
      while(st.hasMoreElements()) {
        loopCnt++;
        
        range = new float[3];
        range[0]=Float.valueOf((String)st.nextElement()).floatValue();
        range[1]=Float.valueOf((String)st.nextElement()).floatValue();
        range[2]=Float.valueOf((String)st.nextElement()).floatValue();
        
        // bounded range   - the minimum is the smallest lbound value contained w/in the numeric range string
        // unbounded range - 0 is the minimum
        
        if(_isRangeBound && loopCnt==1) {
          minScore=range[2];
          maxScore=range[2];
        } else {
          if(range[2]<minScore)
            minScore=range[2];
          if(range[2]>maxScore)
            maxScore=range[2];
        }
      }
    }
    catch(Exception e) {
      log.error("NumericRangeString.calcMinMaxScores() EXCEPTION: '"+e.getMessage()+"'");
    }
  }
  
  /**
   * getBoundScore()
   * 
   * Returns the number associated with the range in the numeric range string that
   * boundifies the 'fval' parameter.
   */
  public float getBoundScore(float fval)
  {
    float score=0f;
    
    try {
      
      Vector matrix = new Vector(0,1);
      float[] range;
      StringTokenizer st = new StringTokenizer(numRange,"-{},:\n\r");
      
      while(st.hasMoreElements()) {
        range = new float[3];
        range[0]=Float.valueOf((String)st.nextElement()).floatValue();
        range[1]=Float.valueOf((String)st.nextElement()).floatValue();
        range[2]=Float.valueOf((String)st.nextElement()).floatValue();
        matrix.addElement(range);
      }
      
      for(int i=0;i<matrix.size();i++) {
        range=(float[])matrix.elementAt(i);
        if(range[0]<=fval && range[1]>=fval) {
          score=range[2];
          break;
        }
      }
    }
    catch(Exception e) {
      log.error("NumericRangeString.calcScore() EXCEPTION: '"+e.getMessage()+"'");
    }
    
    return score;
  }
  
  public String toString()
  {
    final String nl = System.getProperty("line.separator");
    
    StringBuffer sb = new StringBuffer(256);
    
    sb.append(",numRange="+numRange);
    sb.append(",minScore="+minScore);
    sb.append(",maxScore="+maxScore);
    sb.append(",_isRangeBound="+_isRangeBound);
    
    sb.append(nl);
    
    return sb.toString();
  }

} // class NumericRangeString
