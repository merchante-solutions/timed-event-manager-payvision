package com.mes.creditmatrix;

// log4j classes.
import org.apache.log4j.Category;

public final class CreditMatrixElement_AppNumRange extends CreditMatrixElement_App
{
  // create class log category
  static Category log = Category.getInstance(CreditMatrixElement_AppNumRange.class.getName());
  
  // constants
  
  // data members
  private NumericRangeString    nrs;
  
  // class functions
  
  // object functions

  // construction
  public CreditMatrixElement_AppNumRange()
  {
  }
  
  public void clear()
  {
    super.clear();
    
    if(nrs==null)
      nrs = new NumericRangeString();
    else
      nrs.clear();
  }
  
  // accessors
  
  public String getNumRange()
  {
    return nrs.getNumRange();
  }
  
  public boolean isBounded()
  {
    return true;
  }
  
  public boolean isNumRangeBounded()
  {
    return nrs.isRangeBound();
  }
  
  public float getMinScore()
  {
    return nrs.getMinScore();
  }
  
  public float getMaxScore()
  {
    return nrs.getMaxScore();
  }
  
  public int  getType()
  {
    return CMETYPE_APPNUMRANGE;
  }
    
  public String    getTitle()
  {
    StringBuffer sb = new StringBuffer(256);
    
    sb.append("App Field: \"");
    sb.append(label);
    sb.append("\" - RANGE");
    
    return sb.toString();
  }
  
  public String  getDescription()
  {
    return "Scoring based on numeric ranges where each range corresponds to a particular score.";
  }
  
  // mutators
  
  public void setNumRange(String v)
  {
    if(v==null || nrs.getNumRange().equals(v))
      return;
    nrs.setNumRange(v);
    is_dirty=true;
  }
  
  public void setIsRangeBound(boolean v)
  {
    nrs.setIsRangeBound(v);
  }
  
  public float calcScore(long app_seq_num)
  {
    // reset
    score=0f;
    
    try {
      app_val = ApplicationProxy.getInstance().getAppVal(app_seq_num,mib_accessor);
      float fappval=Float.valueOf(app_val).floatValue();
      score=nrs.getBoundScore(fappval);
    }
    catch(Exception e) {
      log.error("calcScore("+this.mib_accessor+") EXCEPTION: '"+e.getMessage()+"'");
      return 0f;
    }
    

    return score;
  }
  
  public String toString()
  {
    final String nl = System.getProperty("line.separator");
    
    StringBuffer sb = new StringBuffer(256);
    
    sb.append("id="+id);
    sb.append("cm_id="+cm_id);
    sb.append(",name="+name);
    sb.append(",label="+label);
    sb.append(",mib_accessor="+mib_accessor);
    sb.append(nrs.toString());
    sb.append(" - "+(is_dirty? "DIRTY":"CLEAN"));
    
    sb.append(nl);
    
    return sb.toString();
  }

} // class CreditMatrixElement_AppNumRange
