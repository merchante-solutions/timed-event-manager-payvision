package com.mes.creditmatrix;

import java.util.Enumeration;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.database.ValueObjectBase;

/**
 * CreditMatrix class
 * 
 * Represents a single Credit Matrix for a given app type.
 */
public class CreditMatrix extends ValueObjectBase
  implements Cloneable
{
  // create class log category
  static Category log = Category.getInstance(CreditMatrix.class.getName());
  
  // constants
  // (NONE)
  
  
  // data members
  protected long            id;
  protected int             app_type;       // application type
  protected String          name;           // name
  protected long            autoapprove_threshold;
  protected long            autodecline_threshold;
  protected Vector          cmelements;     // key: CreditMatrixElement.name, val: {CreditMatrixElement}
  
  // class methods
  // (NONE)
  
  // object methods
  
  // construction
  public CreditMatrix()
  {
    super(true);
    
    cmelements=null;
    
    clear();
  }
  
  public boolean equals(Object obj)
  {
    if(!(obj instanceof CreditMatrix))
      return false;
    
    return (((CreditMatrix)obj).getID()==this.id);
  }
  
  public void clear()
  {
    id=-1L;
    app_type=-1;
    name="";
    autoapprove_threshold=-1L;
    autodecline_threshold=-1L;
    
    if(cmelements!=null)
      cmelements.clear();
    
    is_dirty=true;
  }
  
  public CreditMatrix copy()
  {
    CreditMatrix cm = null;
    
    try {
       cm=(CreditMatrix)this.clone();
       
       if(cm==null)
         return null;
      
      // reset all ids to avoid duplicate key db errors
      cm.id=-1L;
      for(Enumeration e=cm.cmelements.elements();e.hasMoreElements();)
        ((CreditMatrixElement)e.nextElement()).id=-1L;
      
    }
    catch(Exception e) {
      return null;
    }
    
    return cm;
  }
  
  protected Object clone()
  {
    try {
      return super.clone();
    }
    catch(Exception e) {
      return null;
    }
  }
  
  
  // accessors

  public long           getID() { return id; }
  public int            getAppType() { return app_type; }
  public String         getName() { return name; }
  public long           getAutoApproveThreshold() { return autoapprove_threshold; }
  public long           getAutoDeclineThreshold() { return autodecline_threshold; }
  //public Vector      getCreditMatrixElements() { return cmelements; }
  
  public boolean isBounded()
  {
    // i.e. at least one element and all elements are themselves bounded
    
    if(cmelements==null || cmelements.size()==0)
      return false;
    
    for(Enumeration e=cmelements.elements();e.hasMoreElements();) {
      if(!((CreditMatrixElement)e.nextElement()).isBounded())
        return false;
    }
    
    return true;
  }
  
  public float getMinScore()
  {
    if(!isBounded())
      return 0f;
    
    float min=0f;
    
    for(Enumeration e=cmelements.elements();e.hasMoreElements();)
      min += ((CreditMatrixElement)e.nextElement()).getMinScore();
    
    return min;
  }
  
  public float getMaxScore()
  {
    if(!isBounded())
      return 0f;
    
    float max=0f;
    
    for(Enumeration e=cmelements.elements();e.hasMoreElements();)
      max += ((CreditMatrixElement)e.nextElement()).getMaxScore();
    
    return max;
  }
  
  public int getNumCreditMatrixElements()
  {
    return cmelements==null? 0:cmelements.size();
  }
  
  public Enumeration getCreditMatrixElementsEnumeration()
  {
    if(cmelements==null)
      cmelements = new Vector(0,1);
    
    return cmelements.elements();
  }
  
  // mutators
  
  public void setID(long v)
  {
    if(v==id)
      return;
    id=v;
    is_dirty=true;
    
    // propagate to child elements
    if(cmelements==null)
      return;
    for(int i=0;i<cmelements.size();i++)
      ((CreditMatrixElement)cmelements.elementAt(i)).setCreditMatrixID(v);
  }
  
  public void setAppType(int v)
  {
    if(v==app_type)
      return;
    app_type=v;
    is_dirty=true;
  }

  public void setName(String v)
  {
    if(v==null || name.equals(v))
      return;
    name=v;
    is_dirty=true;
  }
  
  public void setAutoApproveThreshold(long v)
  {
    if(v==autoapprove_threshold)
      return;
    autoapprove_threshold=v;
    is_dirty=true;
  }
  
  public void setAutoDeclineThreshold(long v)
  {
    if(v==autodecline_threshold)
      return;
    autodecline_threshold=v;
    is_dirty=true;
  }

  public void addCreditMatrixElement(CreditMatrixElement cme)
  {
    if(cmelements==null)
      cmelements = new Vector(0,1);
    
    if(cme==null)
      return;
    cmelements.addElement(cme);
    is_dirty=true;
    cme.setParent(this);
    cme.setCreditMatrixID(this.id);
  }

  public void removeCreditMatrixElement(CreditMatrixElement cme)
  {
    if(cme==null)
      return;
    cmelements.removeElement(cme);
    is_dirty=true;
  }
  
  public String toString()
  {
    final String nl = System.getProperty("line.separator");
    
    StringBuffer sb = new StringBuffer(256);
    
    sb.append(nl);
    sb.append("CreditMatrix: ");
    
    sb.append("id="+id);
    sb.append(", app_type="+app_type);
    sb.append(", name="+name);
    sb.append(", autoapprove_threshold="+autoapprove_threshold);
    sb.append(", autodecline_threshold="+autodecline_threshold);
    sb.append(" - "+(is_dirty? "DIRTY":"CLEAN"));
    
    sb.append(nl);
    if(cmelements!=null && cmelements.size()>0) {
      sb.append("ELEMENTS:");
      CreditMatrixElement cme=null;
      for(int i=0;i<cmelements.size();i++) {
        sb.append(nl+"  - ");
        sb.append(((CreditMatrixElement)cmelements.elementAt(i)).toString());
      }
    } else {
      sb.append(" *NO Elements.*");
    }
    
    return sb.toString();
  }

} // class CreditMatrix
