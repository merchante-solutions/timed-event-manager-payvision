package com.mes.creditmatrix;

// log4j classes.
import org.apache.log4j.Category;
import com.mes.database.ValueObjectBase;

/**
 * CreditMatrixElement class
 * 
 * Represents a single Credit Matrix Element.
 */
public abstract class CreditMatrixElement extends ValueObjectBase
  implements Cloneable 
{
  // create class log category
  static Category log = Category.getInstance(CreditMatrixElement.class.getName());
  
  // constants (each type corresponds to a single concrete sub-class)
  public static final int           CMETYPE_UNDEFINED                 = 0;
  public static final int           CMETYPE_START                     = 1;
  public static final int           CMETYPE_APPWEIGHT                 = CMETYPE_START;
  public static final int           CMETYPE_APPOFFSET                 = 2;
  public static final int           CMETYPE_APPSTRINGMATCH            = 3;
  public static final int           CMETYPE_APPNUMRANGE               = 4;
  public static final int           CMETYPE_APPCREDITSCORE            = 5;
  public static final int           CMETYPE_APPADRSMATCH              = 6;
  public static final int           CMETYPE_APPTRMNLPROC              = 7;
  public static final int           CMETYPE_END                       = CMETYPE_APPTRMNLPROC;
  
  // data members
  protected long          id;
  protected long          cm_id;   // CreditMatrix.id
  protected String        name;
  protected String        label;
  protected float         score;
  protected boolean       flagSet;
  protected Object        parent; // CreditMatrix
  
  
  // class functions
  
  public static String getTypeDescriptor(int cme_type)
  {
    switch(cme_type) {
      case CMETYPE_APPWEIGHT:
        return "Application Weight";
      case CMETYPE_APPOFFSET:
        return "Application Offset";
      case CMETYPE_APPSTRINGMATCH:
        return "Application String Match";
      case CMETYPE_APPNUMRANGE:
        return "Application Numeric Range";
      case CMETYPE_APPCREDITSCORE:
        return "Credit Score";
      case CMETYPE_APPADRSMATCH:
        return "Application Address Match";
      case CMETYPE_APPTRMNLPROC:
        return "Application Processed/Owns Equipment Flag";
    }
    
    return "";
  }
  
  // object functions

  // construction
  public CreditMatrixElement()
  {
    super(true);
    
    parent=null;
    
    clear();
  }
  
  public boolean equals(Object obj)
  {
    if(!(obj instanceof CreditMatrixElement))
      return false;
    
    return (((CreditMatrixElement)obj).getID()==this.id);
  }
  
  public void clear()
  {
    id=-1L;
    cm_id=-1L;
    name="";
    label="";
    score=0f;
    flagSet=false;
  }
  
  public CreditMatrixElement copy()
  {
    CreditMatrixElement cme = null;
    
    try {
       cme=(CreditMatrixElement)this.clone();
       
       if(cme==null)
         return null;
      
      // reset all ids to avoid duplicate key db errors
      cme.id=-1L;
      
    }
    catch(Exception e) {
      return null;
    }
    
    return cme;
  }
  
  protected Object clone()
  {
    try {
      return super.clone();
    }
    catch(Exception e) {
      return null;
    }
  }
  
  public boolean isFlagSet() { return flagSet; }
  
  public void turnFlagOn()
  {
    flagSet=true;
  }
  public void turnFlagOff()
  {
    flagSet=false;
  }

  // accessors
  public long           getID() { return id; }
  public long           getCreditMatrixID() { return cm_id; }
  public String         getName() { return name; }
  public String         getLabel() { return label; }
  public float          getScore() { return score; }
  public Object         getParent() { return parent; }
  
  public String getDescriptor()
  {
    return getTypeDescriptor(this.getType());
  }
  
  // mutators
  public void setID(long v)
  {
    if(v==id)
      return;
    id=v;
    is_dirty=true;
  }
  public void setCreditMatrixID(long v)
  {
    if(v==cm_id)
      return;
    cm_id=v;
    is_dirty=true;
  }
  public void setName(String v)
  {
    if(v==null)
      return;
    name=v;
    is_dirty=true;
  }
  public void setLabel(String v)
  {
    if(v==null)
      return;
    label=v;
    is_dirty=true;
  }
  public void setParent(Object v)
  {
    parent=v;
  }
  
  public String toString()
  {
    final String nl = System.getProperty("line.separator");
    
    StringBuffer sb = new StringBuffer(256);
    
    sb.append("id="+id);
    sb.append("cm_id="+cm_id);
    sb.append(",name="+name);
    sb.append(",label="+label);
    sb.append(",score="+score);
    sb.append(",flagSet="+flagSet);
    sb.append(" - "+(is_dirty? "DIRTY":"CLEAN"));
    
    sb.append(nl);
    
    return sb.toString();
  }
  
  public abstract int       getType();
  public abstract String    getTitle();
  public abstract String    getDescription();
  public abstract float     calcScore(long app_seq_num) throws Exception;
  public abstract boolean   isBounded();
  public abstract float     getMinScore();
  public abstract float     getMaxScore();
  public abstract String    getValue();
  public abstract String    getScoreDescriptor();
  
} // class CreditMatrixElement
