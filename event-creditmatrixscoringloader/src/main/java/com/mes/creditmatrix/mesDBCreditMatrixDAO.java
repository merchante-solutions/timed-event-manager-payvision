package com.mes.creditmatrix;

import java.util.Date;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;

/**
 * mesDBCreditMatrixDAO class.
 * 
 * CreditMatrixDAO implementation for the MES db.
 */
class mesDBCreditMatrixDAO implements CreditMatrixDAO
{
  // create class log category
  static Category log = Category.getInstance(mesDBCreditMatrixDAO.class.getName());
  
  // constants
  
  // data members
  protected CreditMatrixMESDB mesdb;
  
  // construction
  public mesDBCreditMatrixDAO()
  {
    mesdb = CreditMatrixMESDB.getInstance();
  }
  
  // class methods
  // (none)
  
  // object methods
  
  public long getUniqueID()
  {
    return mesdb.getNextAvailID();
  }

  public Vector getAppIDsByTypeAndDateRange(long appType,Date startDate,Date endDate)
  {
    return mesdb.getAppIDsByTypeAndDateRange(appType,startDate,endDate);
  }
  
  public String[][] getApplicationTypes()
  {
    return mesdb.getApplicationTypes();
  }
  
  public Vector getCreditMatricesByAppType(long app_type)
  {
    return mesdb.getCreditMatricesByAppType(app_type);
  }
  
  public Vector getCreditMatricesByApplication(long app_seq_num)
  {
    return mesdb.getCreditMatricesByApplication(app_seq_num);
  }
  
  public CreditMatrix createCreditMatrix()
  {
    return new CreditMatrix();
  }
  
  public long insertCreditMatrix(CreditMatrix cm)
  {
    return mesdb.insert(cm);
  }
  
  public boolean deleteCreditMatrix(CreditMatrix cm)
  {
    if(cm.getID()<0) {
      log.error("Unable to delete CreditMatrix: No ID specified.");
      return false;
    }
    
    return mesdb.deleteCreditMatrix(cm.getID());
  }
  
  public CreditMatrix findCreditMatrix(long pkid)
  {
    CreditMatrix cm = new CreditMatrix();
    
    try {
      if(!mesdb.load(pkid,cm))
        return null;
    }
    catch(Exception e) {
      log.error("mesDBCreditMatrixDAO.findCreditMatrix() EXCEPTION: '"+e.getMessage()+"'.");
      cm=null;
    }
    
    return cm;
  }
  
  public boolean updateCreditMatrix(CreditMatrix cm)
  {
    if(cm.getID()<0) {
      log.error("Unable to update CreditMatrix: No ID specified.");
      return false;
    }
    
    return persistCreditMatrix(cm);
  }
  
  public boolean persistCreditMatrix(CreditMatrix cm)
  {
    return (mesdb.persist(cm)>0);
  }
  
} // class mesDBCreditMatrixDAO
