package com.mes.creditmatrix;

// log4j classes.
import org.apache.log4j.Category;
import com.mes.constants.mesConstants;
import com.mes.support.StringUtilities;

/**
 * CreditMatrixElement_TrmnlProc
 * 
 * Red flag only.  Raised when application reports merchant never processed credit cards
 * BUT reports having a terminal.
 * 
 */
public final class CreditMatrixElement_TrmnlProc extends CreditMatrixElement
  implements Cloneable 
{
  // create class log category
  static Category log = Category.getInstance(CreditMatrixElement_TrmnlProc.class.getName());
  
  // constants
  // (NONE)
  
  // data members
  // (NONE)
  
  // class functions
  
  // object functions

  // construction
  public CreditMatrixElement_TrmnlProc()
  {
    super();
    
    clear();
  }

  public void clear()
  {
    super.clear();
    name=label="App Trmnl Process";
  }
  
  // accessors
  
  public String getValue()
  {
    return "";  // no value for this type of element
  }
  
  // mutators
  
  public int       getType()
  {
    return CMETYPE_APPTRMNLPROC;
  }
    
  public String    getTitle()
  {
    return "App Terminal Credit Card Check";
  }
  
  public String    getDescription()
  {
    return "Application reports applicant has a terminal but has never before processed credit cards.";
  }
  
  public float     calcScore(long app_seq_num) 
    throws Exception
  {
    // reset
    flagSet=false;
    
    ApplicationProxy app = ApplicationProxy.getInstance();
    
    String sHaveProcessed = app.getAppVal(app_seq_num,"HaveProcessed");
    int    equipCode =      StringUtilities.stringToInt(app.getAppVal(app_seq_num,"EquipCode"),0);
    
    // has processed CCs?
    boolean hasProcessed  = (sHaveProcessed.toLowerCase().equals("y"));
    boolean ownsEquipment = (equipCode == mesConstants.APP_EQUIP_OWNED);
    
    flagSet=(!hasProcessed && ownsEquipment);
    
    return 0f;
  }
  
  public boolean isBounded()
  {
    return true;
  }
  
  public float getMinScore()
  {
    return 0f;
  }
  
  public float getMaxScore()
  {
    return 0f;
  }
  
  public String getScoreDescriptor()
  {
    StringBuffer sb = new StringBuffer(256);

    sb.append("Application reports applicant has never before processed BUT reports owning equipment?  ");
    
    sb.append(flagSet? "Yes (FAIL).":"No (OK).");
    sb.append("\n");
    
    return sb.toString();
  }
  
} // class CreditMatrixElement_TrmnlProc
