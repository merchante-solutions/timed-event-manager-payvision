package com.mes.creditmatrix;

import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

/**
 * CreditMatrixScoreCalculator class.
 * 
 * Calculates the credit matrix score for a given application (app_seq_num).
 */
public final class CreditMatrixScoreCalculator
{
  // constants
  // (NONE)
  
  // data members
  // (NONE)
  
  
  // construction
  public CreditMatrixScoreCalculator()
  {
  }
  
  public CreditMatrixScore calculateScore(long app_seq_num)
    throws Exception
  {
    return calculateScore(app_seq_num,null);
  }
  
  public CreditMatrixScore calculateScore(long app_seq_num, String cmName)
    throws Exception
  {
    Vector v = getDAOFactory().getCreditMatrixDAO().getCreditMatricesByApplication(app_seq_num);
    
    CreditMatrix cm=null,cmitr;

    if(v.size()>1 && cmName!=null) {
      for(int i=0;i<v.size();i++) {
        cmitr=(CreditMatrix)v.elementAt(i);
        if(cmitr.getName().equals(cmName)) {
          cm=cmitr;
          break;
        }
      }
    } else if(v.size()==1)
        cm=(CreditMatrix)v.elementAt(0);
        
    if(cm==null) {
      if(v.size()>1) {
        throw new Exception("Ambiguous Underwriting Matrix specified for app_seq_num: "+app_seq_num);
      } else {
        throw new Exception("No Underwriting Matrix exists for the Application Type specified for app_seq_num: "+app_seq_num);
      }
    }
    
    CreditMatrixScore cms = getDAOFactory().getCreditMatrixScoreDAO().createCreditMatrixScore();
    cms.setAppSeqNum(app_seq_num);
    cms.setCreditMatrix(cm);    // auto-sets cm_id as well
    cms.setDateCreated(new Date());
    
    CreditMatrixElement cme;
    CreditMatrixScoreElement cmse;

    // get the score for each matrix element
    for(Enumeration e=cm.getCreditMatrixElementsEnumeration();e.hasMoreElements();) {
      cme=(CreditMatrixElement)e.nextElement();
      
      // calc the score
      cme.calcScore(app_seq_num);
      
      // create the score element (adding it to the score)
      cmse=new CreditMatrixScoreElement(cme);
      cmse.setDescriptor(cme.getScoreDescriptor());
      cms.addScoreElement(cmse);
    }
    
    return cms;
  }
  
  private DAOFactory getDAOFactory()
  {
    // hook onto the mes db for this bean
    return DAOFactoryBuilder.getDAOFactory(DAOFactory.DAO_MESDB);
  }

} // CreditMatrixScoreCalculator
