package com.mes.creditmatrix;

// log4j classes.
import org.apache.log4j.Category;
import com.mes.database.ValueObjectBase;

/**
 * CreditMatrixScoreElement class
 * 
 * Represents a single Credit Matrix Element.
 */
public class CreditMatrixScoreElement extends ValueObjectBase
  implements Cloneable 
{
  // create class log category
  static Category log = Category.getInstance(CreditMatrixScoreElement.class.getName());
  
  // constants
  
  // data members
  protected long          id;
  protected long          cms_id;         // CreditMatrixScore.id
  protected String        element_name;
  protected int           element_type;
  protected String        value;          // the value the element score is based upon
  protected float         score;          // element score
  protected boolean       flagSet;        // red flag?
  protected boolean       is_BoundedScore;
  protected float         minScore;
  protected float         maxScore;
  protected String        descriptor;
  protected Object        parent;         // CreditMatrixScore
  
  
  // class functions
  
  // object functions

  // construction
  public CreditMatrixScoreElement()
  {
    super(true);
    
    parent=null;
    
    clear();
  }
  public CreditMatrixScoreElement(CreditMatrixElement cme)
  {
    clear();
    
    this.element_name=cme.getName();
    this.element_type=cme.getType();
    this.value=cme.getValue();
    this.score=cme.getScore();
    this.flagSet=cme.isFlagSet();
    this.is_BoundedScore=cme.isBounded();
    this.minScore=cme.getMinScore();
    this.maxScore=cme.getMaxScore();
    this.descriptor=cme.getScoreDescriptor();
  }
  
  public boolean equals(Object obj)
  {
    if(!(obj instanceof CreditMatrixScoreElement))
      return false;
    
    return (((CreditMatrixScoreElement)obj).getID()==this.id);
  }
  
  public void clear()
  {
    id=-1L;
    cms_id=-1L;
    element_name="";
    element_type=CreditMatrixElement.CMETYPE_UNDEFINED;
    value="";
    score=0f;
    flagSet=false;
    is_BoundedScore=false;
    minScore=0f;
    maxScore=0f;
    descriptor="";
  }
  
  public CreditMatrixScoreElement copy()
  {
    CreditMatrixScoreElement cmse = null;
    
    try {
       cmse=(CreditMatrixScoreElement)this.clone();
       
       if(cmse==null)
         return null;
      
      // reset all ids to avoid duplicate key db errors
      cmse.id=-1L;
      
    }
    catch(Exception e) {
      return null;
    }
    
    return cmse;
  }
  
  protected Object clone()
  {
    try {
      return super.clone();
    }
    catch(Exception e) {
      return null;
    }
  }
  
  // accessors
  public long           getID() { return id; }
  public long           getCreditMatrixScoreID() { return cms_id; }
  public String         getElementName() { return element_name; }
  public int            getElementType() { return element_type; }
  public String         getValue() { return value; }
  public float          getScore() { return score; }
  public boolean        isFlagSet() { return flagSet; }
  public boolean        isBoundedScore() { return is_BoundedScore; }
  public float          getMinScore() { return minScore; }
  public float          getMaxScore() { return maxScore; }
  public Object         getParent() { return parent; }
  
  public String         getDescriptor()
  {
    if(descriptor!=null && descriptor.length()>0)
      return descriptor;
    
    // default calculation
    StringBuffer sb = new StringBuffer(256);
    
    sb.append(element_name);
    if(value.length()>0) {
      sb.append(" ('");
      sb.append(value);
      sb.append("')");
    }
    sb.append(" Score: ");
    sb.append(score);
    
    if(flagSet)
      sb.append("  ***RED FLAG***");
    
    sb.append("\n");
    
    descriptor=sb.toString();
    
    return descriptor;
  }
  
  // mutators
  public void setID(long v)
  {
    if(v==id)
      return;
    id=v;
    is_dirty=true;
  }
  public void setCreditMatrixScoreID(long v)
  {
    if(v==cms_id)
      return;
    cms_id=v;
    is_dirty=true;
  }
  public void setElementName(String v)
  {
    if(v==null || v.equals(element_name))
      return;
    element_name=v;
    is_dirty=true;
  }
  public void setElementType(int v)
  {
    if(v==element_type)
      return;
    element_type=v;
    is_dirty=true;
  }
  public void setValue(String v)
  {
    if(v==null || v.equals(value))
      return;
    value=v;
    is_dirty=true;
  }
  public void setScore(float v)
  {
    if(v==score)
      return;
    score=v;
    is_dirty=true;
  }
  public void turnFlagOn()
  {
    flagSet=true;
  }
  public void turnFlagOff()
  {
    flagSet=false;
  }
  
  public void setBoundedScore(boolean v)
  {
    if(v==is_BoundedScore)
      return;
    is_BoundedScore=v;
    is_dirty=true;
  }    
  
  public void setMinScore(float v)
  {
    if(v==minScore)
      return;
    minScore=v;
    is_dirty=true;
  }

  public void setMaxScore(float v)
  {
    if(v==maxScore)
      return;
    maxScore=v;
    is_dirty=true;
  }

  public void setDescriptor(String v)
  {
    if(v==null || v.equals(descriptor))
      return;
    descriptor=v;
    is_dirty=true;
  }
  
  public void setParent(Object v)
  {
    parent=v;
  }
  
  public String toString()
  {
    final String nl = System.getProperty("line.separator");
    
    StringBuffer sb = new StringBuffer(256);
    
    sb.append("id="+id);
    sb.append("cms_id="+cms_id);
    sb.append(",element_name="+element_name);
    sb.append(",element_type="+element_type);
    sb.append(",value="+value);
    sb.append(",score="+score);
    sb.append(",flagSet="+flagSet);
    sb.append(",is_BoundedScore="+is_BoundedScore);
    sb.append(",minScore="+minScore);
    sb.append(",maxScore="+maxScore);
    sb.append(",descriptor="+descriptor);
    sb.append(" - "+(is_dirty? "DIRTY":"CLEAN"));
    
    sb.append(nl);
    
    return sb.toString();
  }
  
} // class CreditMatrixScoreElement
