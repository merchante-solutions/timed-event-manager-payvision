package com.mes.creditmatrix;

import java.util.Date;
import java.util.Vector;

/**
 * CreditMatrixDAO interface
 * 
 * DAO definition for CreditMatrix class
 */
public interface CreditMatrixDAO
{
  public long             getUniqueID();  // i.e. new unique id suited for a credit matrix pk
                                      
  public Vector           getAppIDsByTypeAndDateRange(long appType,Date startDate,Date endDate);
  
  public String[][]       getApplicationTypes();
    //  FORMAT: [app type][description]
  
  public Vector           getCreditMatricesByAppType(long app_type);
  public Vector           getCreditMatricesByApplication(long app_seq_num);
  
  public CreditMatrix     createCreditMatrix();
  public long             insertCreditMatrix(CreditMatrix cm);
  public boolean          deleteCreditMatrix(CreditMatrix cm);
  public CreditMatrix     findCreditMatrix(long pkid);
  public boolean          updateCreditMatrix(CreditMatrix cm);
  public boolean          persistCreditMatrix(CreditMatrix cm);

} // interface CreditMatrixDAO
