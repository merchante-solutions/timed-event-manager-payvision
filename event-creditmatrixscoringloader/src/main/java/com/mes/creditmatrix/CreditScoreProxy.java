/*@lineinfo:filename=CreditScoreProxy*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.creditmatrix;

import java.sql.ResultSet;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.StringUtilities;
import sqlj.runtime.ResultSetIterator;

/**
 * CreditScoreProxy class
 * 
 * Provides the relevant application field names for use by the credit matrix module 
 * and provides the value from the applciation for a given app field name.
 */
public final class CreditScoreProxy extends SQLJConnectionBase
{
  // create class log category
  static Category log = Category.getInstance(CreditScoreProxy.class.getName());

  // Singleton
  public static CreditScoreProxy getInstance()
  {
    if(_instance==null)
      _instance = new CreditScoreProxy();
    
    return _instance;
  }
  private static CreditScoreProxy _instance = null;
  ///
  
  // constants
  // (NONE)
  
  // data members
  protected long               app_seq_num;
  protected int                credit_score;
  protected int                bk_count;
  protected int                mort_count;
  protected int                derog_count;

  // class functions
  // (none)
  
  
  // object functions

  // construction
  private CreditScoreProxy()
  {
    app_seq_num=-1L;
    credit_score=0;
    bk_count=0;
    mort_count=0;
    derog_count=0;
  }
  
  public void load(long app_seq_num)
  {
    if(app_seq_num==this.app_seq_num)
      return;
      
    this.app_seq_num=app_seq_num;
    this.credit_score=0;
    this.bk_count=0;
    this.mort_count=0;
    this.derog_count=0;

    ResultSetIterator rsItr;
    ResultSet rs;

    int sum=0, count=0;
      
    try {

      connect();
    
      /*@lineinfo:generated-code*//*@lineinfo:82^7*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT  score,BK_COUNT,MORT_COUNT,DEROG_COUNT
//          FROM    credit_scores
//          WHERE   app_seq_num=:app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT  score,BK_COUNT,MORT_COUNT,DEROG_COUNT\n        FROM    credit_scores\n        WHERE   app_seq_num= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.creditmatrix.CreditScoreProxy",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,app_seq_num);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.creditmatrix.CreditScoreProxy",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:87^7*/

      rs=rsItr.getResultSet();

      while(rs.next()) {
        sum+=StringUtilities.stringToInt(rs.getString(1));
        count++;
        
        // use the highest of the count fields if more than one rec exist for an app_seq_num
        
        if(rs.getInt(2) > bk_count)
          bk_count=rs.getInt(2);
        
        if(rs.getInt(3) > mort_count)
          mort_count=rs.getInt(3);
        
        if(rs.getInt(4) > derog_count)
          derog_count=rs.getInt(4);
          
      }

      rs.close();
      rsItr.close();

      cleanUp();

      credit_score = sum/count;
      log.debug("CrditScoreProxy() credit_score="+credit_score);
    }
    catch(Exception e) {
      log.error("CreditScoreProxy.getCreditScore() EXCEPTION: '"+e.getMessage()+"'.");
    }
    
  }
  
  public int getCreditScore()
  {
    return credit_score;
  }
  public int getBankruptcyCount()
  {
    return bk_count;
  }
  public int getMortgageCount()
  {
    return mort_count;
  }
  public int getDerogCount()
  {
    return derog_count;
  }

}/*@lineinfo:generated-code*/