package com.mes.creditmatrix;

// log4j classes.
import org.apache.log4j.Category;

public abstract class CreditMatrixElement_App extends CreditMatrixElement
{
  // create class log category
  static Category log = Category.getInstance(CreditMatrixElement_App.class.getName());
  
  // constants
  
  // data members
  protected String                    mib_accessor; // function name of MerchInfoBean function that provides the app value this element is tied to
  protected String                    app_val;      // target value from the application (non-state related)
  
  // class functions
  
  // object functions

  // construction
  public CreditMatrixElement_App()
  {
    super();
    
    clear();
  }
  
  public void clear()
  {
    super.clear();
    
    mib_accessor="";
    app_val="";
  }
  
  // accessors
  public String getMibAccessor()
  {
    return mib_accessor;
  }
  public String getAppVal()
  {
    return app_val;
  }
  
  public String getScoreDescriptor()
  {
    StringBuffer sb = new StringBuffer(256);
    
    sb.append("App field - ");
    sb.append(mib_accessor);
    
    if(app_val.length()>0) {
      sb.append(" ('");
      sb.append(app_val);
      sb.append("')");
    }
    sb.append(" Score: ");
    sb.append(score);
    
    if(flagSet)
      sb.append("  ***RED FLAG***");
    
    sb.append("\n");
    
    return sb.toString();
  }
  
  public String getValue()
  {
    return app_val;
  }
  
  // mutators
  public void setMibAccessor(String v)
  {
    if(v==null || v.equals(mib_accessor))
      return;
    mib_accessor=v;
    is_dirty=true;
  }
  
  // NOTE: No mutator for app_val as it is set via ApplicationProxy class.

} // class CreditMatrixElement_App
