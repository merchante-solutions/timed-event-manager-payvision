/*@lineinfo:filename=ApplicationProxy*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.creditmatrix;

import java.lang.reflect.Field;
import java.sql.ResultSet;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.database.SQLJConnectionBase;
import com.mes.setup.AppUtils;
import sqlj.runtime.ResultSetIterator;



/**
 * ApplicationProxy class
 * 
 * Provides the relevant application field names for use by the credit matrix module 
 * and provides the value from the applciation for a given app field name.
 */
public final class ApplicationProxy extends SQLJConnectionBase
{
  // create class log category
  static Category log = Category.getInstance(ApplicationProxy.class.getName());

  // Singleton
  public static ApplicationProxy getInstance()
  {
    if(_instance==null)
      _instance = new ApplicationProxy();
    
    return _instance;
  }
  private static ApplicationProxy _instance = null;
  ///
  
  // constants
  // (NONE)
  
  // data members
  protected long               app_seq_num         = 0L;
  protected int                app_type            = 0;     // i.e.: mesdb.APP_TYPE
  protected AppData            appData             = new AppData();

  // class functions
  // (none)
  
  
  // object functions

  // construction
  private ApplicationProxy()
  {
  }

  public void clear()
  {
    app_seq_num = 0L;
    app_type = 0;
    appData.clear();
  }
  
  protected class AppData
  {
    public String MotoPercentage;
    public String BusinessCity;
    public String BusinessState;
    public String BankCity;
    public String BankState;
    public String BusinessType;
    public String LocationYears;
    public String YearsOpen;
    public String AverageTicket;
    public String MonthlySales;
    public String LocationType;
    public String IndustryType;
    public String HaveProcessed;
    public String EquipCode;

    public void clear()
    {
      MotoPercentage = null;
      BusinessCity = null;
      BusinessState = null;
      BankCity = null;
      BankState = null;
      BusinessType = null;
      LocationYears = null;
      YearsOpen = null;
      AverageTicket = null;
      MonthlySales = null;
      LocationType = null;
      IndustryType = null;
      HaveProcessed = null;
      EquipCode = null;
    }

  }
  
  public String[] getAppFieldNames()
  {
    Field[] fields = ApplicationProxy.AppData.class.getDeclaredFields();
    
    String[] appFldNmes = new String[fields.length-1];  // exclude the implicit this ref

    for(int i=0;i<fields.length-1;i++) {
      appFldNmes[i] = fields[i].getName();
    }

    return appFldNmes;
  }

  protected void loadApp(long app_seq_num)
    throws Exception
  {
    if(this.app_seq_num==app_seq_num)
      return;

    clear();

    ResultSetIterator  it                  = null;
    ResultSet          rs                  = null;
    
    try {
      
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:127^7*/

//  ************************************************************
//  #sql [Ctx] it = { select     m.*
//  				          ,busadr.ADDRESS_CITY 			  bus_city
//  				          ,busadr.COUNTRYSTATE_CODE		bus_state
//  				          ,bnkadr.ADDRESS_CITY 			  bnk_city
//  				          ,bnkadr.COUNTRYSTATE_CODE 	bnk_state
//                    ,me.equiptype_code
//                    ,mb.merchbank_num_years_open
//                    ,a.app_type                 app_type
//          
//          from      merchant          m
//                    ,address          busadr
//                    ,address          bnkadr
//                    ,merchequipment   me
//                    ,merchbank        mb
//                    ,application      a
//          
//          where     m.app_seq_num=busadr.app_seq_num(+) and busadr.addresstype_code(+)=1
//                    and m.app_seq_num=bnkadr.app_seq_num(+) and bnkadr.addresstype_code(+)=9
//                    and m.app_seq_num=me.app_seq_num(+)
//                    and m.app_seq_num=mb.app_seq_num(+)
//                    and m.app_seq_num=a.app_seq_num
//                    and m.app_seq_num=:app_seq_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select     m.*\n\t\t\t\t          ,busadr.ADDRESS_CITY \t\t\t  bus_city\n\t\t\t\t          ,busadr.COUNTRYSTATE_CODE\t\tbus_state\n\t\t\t\t          ,bnkadr.ADDRESS_CITY \t\t\t  bnk_city\n\t\t\t\t          ,bnkadr.COUNTRYSTATE_CODE \tbnk_state\n                  ,me.equiptype_code\n                  ,mb.merchbank_num_years_open\n                  ,a.app_type                 app_type\n        \n        from      merchant          m\n                  ,address          busadr\n                  ,address          bnkadr\n                  ,merchequipment   me\n                  ,merchbank        mb\n                  ,application      a\n        \n        where     m.app_seq_num=busadr.app_seq_num(+) and busadr.addresstype_code(+)=1\n                  and m.app_seq_num=bnkadr.app_seq_num(+) and bnkadr.addresstype_code(+)=9\n                  and m.app_seq_num=me.app_seq_num(+)\n                  and m.app_seq_num=mb.app_seq_num(+)\n                  and m.app_seq_num=a.app_seq_num\n                  and m.app_seq_num= :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.creditmatrix.ApplicationProxy",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,app_seq_num);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.creditmatrix.ApplicationProxy",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:151^7*/

      rs = it.getResultSet();

      if(!rs.next()) {
        clear();
        throw new Exception("No application data found for App seq num '"+app_seq_num+"'.");
      }
      
      this.app_seq_num=app_seq_num;
      this.app_type=rs.getInt("app_type");

      // fill app data structure
      appData.MotoPercentage    = rs.getString("merch_mail_phone_sales");
      appData.BusinessCity      = rs.getString("bus_city");
      appData.BusinessState     = rs.getString("bus_state");
      appData.BankCity          = rs.getString("bnk_city");
      appData.BankState         = rs.getString("bnk_state");
      appData.BusinessType      = rs.getString("bustype_code");
      appData.LocationYears     = rs.getString("merch_years_at_loc");
      appData.YearsOpen         = rs.getString("merchbank_num_years_open");
      appData.AverageTicket     = rs.getString("merch_average_cc_tran");
      appData.MonthlySales      = rs.getString("merch_month_tot_proj_sales");
      appData.LocationType      = rs.getString("loctype_code");
      appData.IndustryType      = rs.getString("industype_code");
      appData.HaveProcessed     = rs.getString("merch_prior_cc_accp_flag");
      appData.EquipCode         = rs.getString("equiptype_code");

      log.debug("EquipCode="+appData.EquipCode);

      it.close();
      rs.close();

      log.debug("App '"+app_seq_num+"' loaded.");
    }
    catch(Exception e) {
      log.error("loadApp() EXCEPTION: '"+e.toString()+"'");
      throw e;
    }
    finally {
      try { it.close(); } catch(Exception e) {}
      try { rs.close(); } catch(Exception e) {}
      cleanUp();
    }
    
  }
  
  public String getAppVal(long app_seq_num,String fieldName) 
    throws Exception
  {
    loadApp(app_seq_num);

    String appVal;
    Field[] fields = ApplicationProxy.AppData.class.getDeclaredFields();

    for(int i=0;i<fields.length;i++) {
      if(fields[i].getName().equals(fieldName)) {
        try {
          appVal = fields[i].get(this.appData).toString();
        }
        catch(Exception e) {
          appVal = "";
        }
        return xfrmAppVal(fieldName,appVal);
      }
    }

    throw new Exception("Unsupported application field name: '"+fieldName+"'.");
  }
  
  private String xfrmAppVal(String fieldName,String appVal)
  {
    if(fieldName==null || fieldName.length()<1)
      return "";
    if(appVal==null)
      appVal = "";
    
    String xfrm;
    int vt;
    
    // BusinessType
    if(fieldName.equals("BusinessType")) {
      vt=AppUtils.VALUE_TYPE_BUSINESS;
    
    // LocationType
    } else if(fieldName.equals("LocationType")) {
      vt=AppUtils.VALUE_TYPE_LOCATION;
    
    // IndustryType
    } else if(fieldName.equals("IndustryType")) {
      vt=AppUtils.VALUE_TYPE_INDUSTRY;
    
    // NO TRANSFORM
    } else
      return appVal;
  
    // delegate transform to AppUtils
    try {
      xfrm=AppUtils.decodeValue(vt,Integer.parseInt(appVal),app_type);
    }
    catch(Exception e) {
      log.error("ApplicationProxy.xfrmAppVal() EXCEPTION: '"+e.getMessage()+"'.");
      xfrm=appVal;  // default to non-transform
    }
    
    log.debug("xfrmAppVal - orig: "+fieldName+", xfrm: "+xfrm);

    return xfrm;
  }

  

}/*@lineinfo:generated-code*/