package com.mes.creditmatrix;

/**
 * CreditMatrixElementDAO interface
 * 
 * DAO definition for CreditMatrix class
 */
public interface CreditMatrixElementDAO
{
  // constants
  // (none)
  
  // methods
  
  public CreditMatrixElement      createCreditMatrixElement(int cme_type);
  public long                     insertCreditMatrixElement(CreditMatrixElement cme);
  public boolean                  deleteCreditMatrixElement(long pkid);
  public CreditMatrixElement      findCreditMatrixElement(long pkid);
  public boolean                  updateCreditMatrixElement(CreditMatrixElement cme);
  public boolean                  persistCreditMatrixElement(CreditMatrixElement cme);
  
} // interface CreditMatrixElementDAO
