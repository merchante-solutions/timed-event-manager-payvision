package com.mes.creditmatrix;

// log4j classes.
import org.apache.log4j.Category;

/**
 * CreditMatrixElement_AdrsMatch
 * 
 * Serves as both a red flag as well as an element score.
 */
public final class CreditMatrixElement_AdrsMatch extends CreditMatrixElement
  implements Cloneable 
{
  // create class log category
  static Category log = Category.getInstance(CreditMatrixElement_AdrsMatch.class.getName());
  
  // constants
  // (NONE)
  
  // data members
  // (NONE)
  
  // class functions
  
  // object functions

  // construction
  public CreditMatrixElement_AdrsMatch()
  {
    super();
    
    clear();
  }

  public void clear()
  {
    super.clear();
    name=label="App Adrs Match";
  }
  
  // accessors
  
  public String getValue()
  {
    return "";  // no value for this type of element
  }
  
  // mutators
  
  public int       getType()
  {
    return CMETYPE_APPADRSMATCH;
  }
    
  public String    getTitle()
  {
    return "App Adrs Match Check";
  }
  
  public String    getDescription()
  {
    return "Application Business Address matches with Bank address?";
  }
  
  public float     calcScore(long app_seq_num) 
    throws Exception
  {
    // reset
    score=0f;
    flagSet=false;
    
    ApplicationProxy app = ApplicationProxy.getInstance();
    
    final String busCity = app.getAppVal(app_seq_num,"BusinessCity");
    final String busState = app.getAppVal(app_seq_num,"BusinessState");
    final String bankCity = app.getAppVal(app_seq_num,"BankCity");
    final String bankState = app.getAppVal(app_seq_num,"BankState");
    
    if(busCity.equals(bankCity) && busState.equals(bankState)) {
      flagSet=false;
      score=3f;
    } else {
      // set a red-flag (disallow auto-approval)
      flagSet=true;
    }
    
    return score;
  }
  
  public boolean isBounded()
  {
    return true;  // i.e. NOT UNBOUNDED
  }
  
  public float getMinScore()
  {
    return 0f;
  }
  
  public float getMaxScore()
  {
    return 3f;
  }
  
  public String getScoreDescriptor()
  {
    StringBuffer sb = new StringBuffer(256);
    
    sb.append("App Address Match: ");
    sb.append(!flagSet? "OK: City and State Business and Bank addresses match.":"FAIL: City and State Business and Bank addresses DO NOT match.");
    sb.append(" Score: ");
    sb.append(score);
    sb.append("\n");
    
    return sb.toString();
  }
  
} // class CreditMatrixElement_AdrsMatch
