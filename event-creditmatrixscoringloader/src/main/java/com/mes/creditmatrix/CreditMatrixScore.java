package com.mes.creditmatrix;

import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;
import com.mes.database.ValueObjectBase;

/**
 * CreditMatrixScore class
 * 
 * Represents a single Credit Matrix Score for a given app type.
 */
public class CreditMatrixScore extends ValueObjectBase
{
  // create class log category
  static Category log = Category.getInstance(CreditMatrixScore.class.getName());
  
  // constants
  // (NONE)
  
  
  // data members
  protected long            id;
  protected long            app_seq_num;
  protected long            cm_id;    // CreditMatrix.id
  protected Date            date_created;
  
  protected CreditMatrix    cm;
  
  protected Vector          selements;
  
  // non-state related - calculated by iterating the child score elements
  protected boolean         isCalculated;         // score is already calculated?
  protected float           score;
  protected int             numRedFlags;
  protected String          score_detailtext;
  protected boolean         is_BoundedScore;
  protected float           minScore;
  protected float           maxScore;
  ///
  
  // class methods
  // (NONE)
  
  // object methods
  
  // construction
  public CreditMatrixScore()
  {
    super(true);
    
    date_created=new Date();
    selements=null;
    
    clear();
  }
  
  public void clear()
  {
    id=-1L;
    app_seq_num=-1L;
    cm_id=-1L;
    date_created.setTime(0L);
    
    isCalculated=false;
    score=0L;
    numRedFlags=0;
    score_detailtext="";
    is_BoundedScore=false;
    minScore=0f;
    maxScore=0f;
    
    if(cm!=null)
      cm.clear();
    
    if(selements!=null)
      selements.clear();
    
    is_dirty=true;
  }
  
  protected void calcScore()
  {
    if(selements==null || selements.size()==0) {
      isCalculated=false;
      return;
    }
    
    isCalculated=false;
    score=0f;
    numRedFlags=0;
    score_detailtext="";
    is_BoundedScore=true;
    minScore=0f;
    maxScore=0f;
    
    StringBuffer sdt = new StringBuffer(64*selements.size());  // score detail text
    CreditMatrixScoreElement cmse;
    
    for(int i=0;i<selements.size();i++) {
      cmse=(CreditMatrixScoreElement)selements.elementAt(i);
      
      score += cmse.getScore();
      
      if(cmse.isFlagSet())
        numRedFlags++;
    
      if(is_BoundedScore && !cmse.isBoundedScore()) {
        is_BoundedScore=false;  // require ALL elements be bounded for the score to be bounded
        minScore=0f;
        maxScore=0f;
      }
      if(is_BoundedScore) {
          minScore += cmse.getMinScore();
          maxScore += cmse.getMaxScore();
      }
      
      // append score detail text
      sdt.append(cmse.getDescriptor());
    }
    
    score_detailtext=sdt.toString();
    
    isCalculated=true;
  }
  
  // accessors

  public long           getID() { return id; }
  public long           getAppSeqNum() { return app_seq_num; }
  public long           getCreditMatrixID() { return cm_id; }
  public Date           getDateCreated() { return date_created; }
  public CreditMatrix   getCreditMatrix() { return cm; }
  
  public float          getScore()
  {
    if(!isCalculated)
      calcScore();
    return score;
  }
  
  public int            getNumRedFlags()
  { 
    if(!isCalculated)
      calcScore();
    return numRedFlags; 
  }
  
  public String         getScoreDetailText() 
  { 
    if(!isCalculated)
      calcScore();
    return score_detailtext; 
  }
  
  public boolean        isBoundedScore() 
  { 
    if(!isCalculated)
      calcScore();
    return is_BoundedScore; 
  }
  
  public float          getMinScore() 
  { 
    if(!isCalculated)
      calcScore();
    return minScore; 
  }
  public float          getMaxScore() 
  { 
    if(!isCalculated)
      calcScore();
    return maxScore; 
  }
  
  public float          getScoreAsPercentage()  // 0 - 100
  {
    if(!isCalculated)
      calcScore();
    
    if(!is_BoundedScore)
      return 0f;
    
    // (score - min)/(max - min) * 100 = score as percentage
    
    float pscore = (score-minScore)/(maxScore-minScore)*100;
    log.debug("getScoreAsPercentage() pscore="+pscore);
    
    return pscore;
  }
  
  public boolean        isAutoApproved()
    throws Exception
  {
    if(cm==null)
      throw new Exception("Unable to determine if Credit Matrix Score is auto-approved: No associated Credit Matrix specified.");
    
    if(!isCalculated)
      calcScore();
    
    return (score >= (float)cm.getAutoApproveThreshold() && numRedFlags==0);
  }
  
  public boolean        isAutoDeclined()
    throws Exception
  {
    if(cm==null)
      throw new Exception("Unable to determine if Credit Matrix Score is auto-declined: No associated Credit Matrix specified.");
    
    if(!isCalculated)
      calcScore();
    
    return (score <= (float)cm.getAutoDeclineThreshold());
  }
  
  // mutators
  
  public void setID(long v)
  {
    if(v==id)
      return;
    id=v;
    is_dirty=true;
    
    // propagate to child elements
    if(selements==null)
      return;
    for(int i=0;i<selements.size();i++)
      ((CreditMatrixScoreElement)selements.elementAt(i)).setCreditMatrixScoreID(v);
  }
  
  public void setAppSeqNum(long v)
  {
    if(v==app_seq_num)
      return;
    app_seq_num=v;
    is_dirty=true;
  }
  
  public void setCreditMatrixID(long v)
  {
    if(v==cm_id)
      return;
    cm_id=v;
    is_dirty=true;
    
    if(cm!=null && id!=cm.getID())
      cm=null;
  }

  public void setDateCreated(Date v)
  {
    if(v==null || v==date_created)
      return;
    date_created=v;
    is_dirty=true;
  }
  
  public void setCreditMatrix(CreditMatrix cm)
  {
    if(cm!=null)
      this.cm=cm;
    
    cm_id=cm.getID();
  }
  
  public void setScoreDetailText(String v)
  {
    if(v==null || v.equals(score_detailtext))
      return;
    score_detailtext=v;
  }
  
  public void addScoreElement(CreditMatrixScoreElement cmse)
  {
    if(selements==null)
      selements = new Vector(0,1);
    
    if(cmse==null)
      return;
    selements.addElement(cmse);
    is_dirty=true;
    cmse.setParent(this);
    cmse.setCreditMatrixScoreID(this.id);
  }

  public void removeCreditMatrixElement(CreditMatrixScoreElement cmse)
  {
    if(cmse==null)
      return;
    selements.removeElement(cmse);
    is_dirty=true;
  }
  
  public String toString()
  {
    final String nl = System.getProperty("line.separator");
    
    StringBuffer sb = new StringBuffer(256);
    
    sb.append(nl);
    sb.append("CreditMatrixScore: ");
    
    sb.append("id="+id);
    sb.append(", app_seq_num="+app_seq_num);
    sb.append(", cm_id="+cm_id);
    sb.append(", date_created="+date_created);
    sb.append(", score="+score);
    sb.append(", numRedFlags="+numRedFlags);
    sb.append(", score_detailtext="+score_detailtext);
    sb.append(", is_BoundedScore="+is_BoundedScore);
    sb.append(", minScore="+minScore);
    sb.append(", maxScore="+maxScore);
    sb.append(" - "+(is_dirty? "DIRTY":"CLEAN"));
    
    return sb.toString();
  }

  public int getNumScoreElements()
  {
    return selements==null? 0:selements.size();
  }
  
  public Enumeration getScoreElementsEnumeration()
  {
    if(selements==null)
      selements = new Vector(0,1);
    
    return selements.elements();
  }

} // class CreditMatrixScore
