package com.mes.creditmatrix;

// log4j classes.
import org.apache.log4j.Category;

public final class CreditMatrixElement_CreditScore extends CreditMatrixElement
{
  // create class log category
  static Category log = Category.getInstance(CreditMatrixElement_CreditScore.class.getName());
  
  // constants
  
  // data members
  private NumericRangeString  nrs;
  private int                 redflagThreshold; // NOTE: MES DB: CREDIT_MATRIX_ELEMENTS.OFFSET field is used to store this value
  private int                 cscore;
  private int                 numBankruptcies;
  private int                 numMortgages;
  private int                 numDerogs;
  
  // class functions
  
  // object functions

  // construction
  public CreditMatrixElement_CreditScore()
  {
  }
  
  public void clear()
  {
    super.clear();
    
    name=label="Credit Score";

    redflagThreshold=0;
    cscore=0;
    numBankruptcies=0;
    numMortgages=0;
    numDerogs=0;
    
    if(nrs==null)
      nrs = new NumericRangeString();
    else
      nrs.clear();
  }
  
  // accessors
  
  public String getValue()
  {
    return Integer.toString(cscore);
  }
  
  public int  getType()
  {
    return CMETYPE_APPCREDITSCORE;
  }
    
  public String    getTitle()
  {
    return "Credit Score";
  }
  
  public String  getDescription()
  {
    return "The average of all personal credit scores associated with a given merchant application.";
  }
  
  public int getRedFlagThreshold()
  {
    return redflagThreshold;
  }
  
  public String getNumRange()
  {
    return nrs.getNumRange();
  }
  
  public boolean isBounded()
  {
    return true;
  }
  
  public boolean isNumRangeBounded()
  {
    return nrs.isRangeBound();
  }
  
  public float getMinScore()
  {
    return nrs.getMinScore();
  }
  
  public float getMaxScore()
  {
    return nrs.getMaxScore();
  }
  
  // mutators
  
  public void setNumRange(String v)
  {
    if(v==null || nrs.getNumRange().equals(v))
      return;
    nrs.setNumRange(v);
    is_dirty=true;
  }
  
  public void setIsRangeBound(boolean v)
  {
    nrs.setIsRangeBound(v);
  }
  
  public void setRedFlagThreshold(int v)
  {
    redflagThreshold=v;
  }
  
  
  public float calcScore(long app_seq_num)
  {
    // reset
    score=0f;
    flagSet=false;
    
    CreditScoreProxy csp = CreditScoreProxy.getInstance();
    csp.load(app_seq_num);

    // get the raw credit score
    cscore=csp.getCreditScore();
    log.debug("CME_CreditScore.calcScore() cscore="+cscore);
    
    // get bankruptcy count
    numBankruptcies = csp.getBankruptcyCount();
    log.debug("CME_CreditScore.calcScore() numBankruptcies="+numBankruptcies);
    if(numBankruptcies>0)
        flagSet=true;
    
    // get mortgages count
    numMortgages = csp.getMortgageCount();
    log.debug("CME_CreditScore.calcScore() numMortgages="+numMortgages);
    if(numMortgages>0)
        flagSet=true;
    
    // get bankruptcy count
    numDerogs = csp.getDerogCount();
    log.debug("CME_CreditScore.calcScore() numDerogs="+numDerogs);
    if(numDerogs>0)
        flagSet=true;
    
    /*
    // set a red flag if the raw credit score is above a threshold
    if(cscore >= redflagThreshold || cscore<=0)
      flagSet=true;
    */
    
    log.debug("CME_CreditScore.calcScore() flagSet="+flagSet);

    // transform the raw credit score via a numeric range table
    score=nrs.getBoundScore(cscore);
    log.debug("CME_CreditScore.calcScore() score="+score);

    return score;
  }

  public String getScoreDescriptor()
  {
    StringBuffer sb = new StringBuffer(256);
    
    sb.append("Credit Rating (avg.): ");
    sb.append(cscore);
    sb.append(" Score: ");
    sb.append(score);
    
    if(flagSet) {
      /*
      if(cscore >= redflagThreshold) {
        sb.append(" ***RED FLAG: High Credit Rating *** ");
      } else if(cscore<=0) {
        sb.append(" ***RED FLAG: No Credit Rating Available *** ");
      }
      */
      if(numBankruptcies > 0)
        sb.append(" ***RED FLAG: "+numBankruptcies+" BANKRUPTCY counts *** ");
      if(numMortgages > 0)
        sb.append(" ***RED FLAG: "+numMortgages+" MORTGAGE counts *** ");
      if(numDerogs > 0)
        sb.append(" ***RED FLAG: "+numDerogs+" DEROG counts *** ");
    }
    
    sb.append("\n");
    
    return sb.toString();
  }
  
  public String toString()
  {
    final String nl = System.getProperty("line.separator");
    
    StringBuffer sb = new StringBuffer(256);
    
    sb.append("id="+id);
    sb.append("cm_id="+cm_id);
    sb.append(",name="+name);
    sb.append(",label="+label);
    sb.append(nrs.toString());
    sb.append(" - "+(is_dirty? "DIRTY":"CLEAN"));
    
    sb.append(nl);
    
    return sb.toString();
  }

} // class CreditMatrixElement_CreditScore
