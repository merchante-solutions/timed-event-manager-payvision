package com.mes.creditmatrix;

import java.util.Vector;
// log4j classes.
import org.apache.log4j.Category;

/**
 * mesDBCreditMatrixScoreDAO class.
 * 
 * CreditMatrixScoreDAO implementation for the MES db.
 */
class mesDBCreditMatrixScoreDAO implements CreditMatrixScoreDAO
{
  // create class log category
  static Category log = Category.getInstance(mesDBCreditMatrixScoreDAO.class.getName());
  
  // constants
  
  // data members
  protected CreditMatrixMESDB mesdb;
  
  // construction
  public mesDBCreditMatrixScoreDAO()
  {
    mesdb = CreditMatrixMESDB.getInstance();
  }
  
  // class methods
  // (none)
  
  // object methods
  
  public Vector getCreditMatrixScoresByApplication(long app_seq_num)
  {
    Vector v = mesdb.getCreditMatrixScoresByApplication(app_seq_num);
    
    //for(int i=0;i<v.size();i++)
    //  loadAssociatedCreditMatrix((CreditMatrixScore)v.elementAt(i));
    
    return v;
  }
  
  public CreditMatrixScore createCreditMatrixScore()
  {
    return new CreditMatrixScore();
  }
  
  public long insertCreditMatrixScore(CreditMatrixScore cms)
  {
    return mesdb.insert(cms);
  }
  
  public boolean deleteCreditMatrixScore(CreditMatrixScore cms)
  {
    if(cms.getID()<0) {
      log.error("Unable to delete CreditMatrixScore: No ID specified.");
      return false;
    }
    
    return mesdb.deleteCreditMatrixScore(cms.getID());
  }
  
  public CreditMatrixScore findCreditMatrixScore(long pkid)
  {
    CreditMatrixScore cms = new CreditMatrixScore();
    
    try {
      if(!mesdb.load(pkid,cms))
        return null;
      //loadAssociatedCreditMatrix(cms);
    }
    catch(Exception e) {
      log.error("mesDBCreditMatrixScoreDAO.findCreditMatrixScore() EXCEPTION: '"+e.getMessage()+"'.");
      cms=null;
    }
    
    return cms;
  }
  
  public boolean updateCreditMatrixScore(CreditMatrixScore cms)
  {
    if(cms.getID()<0) {
      log.error("Unable to update CreditMatrixScore: No ID specified.");
      return false;
    }
    
    return persistCreditMatrixScore(cms);
  }
  
  public boolean persistCreditMatrixScore(CreditMatrixScore cms)
  {
    return (mesdb.persist(cms)>0);
  }
  
  private boolean loadAssociatedCreditMatrix(CreditMatrixScore cms)
  {
    CreditMatrix cm = new CreditMatrix();
    if(!mesdb.load(cms.getCreditMatrixID(),cm))
      return false;
    cms.setCreditMatrix(cm);
    return true;
  }
  
} // class mesDBCreditMatrixScoreDAO
