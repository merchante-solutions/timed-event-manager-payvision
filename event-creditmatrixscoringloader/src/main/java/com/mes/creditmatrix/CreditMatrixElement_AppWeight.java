package com.mes.creditmatrix;

// log4j classes.
import org.apache.log4j.Category;

public final class CreditMatrixElement_AppWeight extends CreditMatrixElement_App
{
  // create class log category
  static Category log = Category.getInstance(CreditMatrixElement_AppWeight.class.getName());
  
  // constants
  
  // data members
  protected float                   weight;
  
  // class functions
  
  // object functions

  // construction
  public CreditMatrixElement_AppWeight()
  {
  }
  
  public void clear()
  {
    super.clear();
    
    weight=0.0f;
  }
  
  // accessors
  public float getWeight() {  return weight; }
  
  // mutators
  public void setWeight(float v)
  {
    if(v==weight)
      return;
    weight=v;
    is_dirty=true;
  }
  
  public int  getType()
  {
    return CMETYPE_APPWEIGHT;
  }
    
  public String    getTitle()
  {
    StringBuffer sb = new StringBuffer(256);
    
    sb.append("App Field: \"");
    sb.append(label);
    sb.append("\" - WEIGHT: ");
    sb.append(weight);
    sb.append(" (UNBOUNDED)");
    
    return sb.toString();
  }
  
  public String  getDescription()
  {
    return "Multiplies a numeric weight to a numeric application field value.";
  }
  
  public float calcScore(long app_seq_num)
  {
    score=0f; // reset
    
    try {
      app_val = ApplicationProxy.getInstance().getAppVal(app_seq_num,mib_accessor);
    }
    catch(Exception e) {
      log.error(e.getMessage());
      return 0f;
    }
      
    float valf = Float.valueOf(app_val).floatValue();
    
    score = weight*valf;
      
    return score;
  }
  
  public String toString()
  {
    final String nl = System.getProperty("line.separator");
    
    StringBuffer sb = new StringBuffer(256);
    
    sb.append("id="+id);
    sb.append("cm_id="+cm_id);
    sb.append(",name="+name);
    sb.append(",label="+label);
    sb.append(",mib_accessor="+mib_accessor);
    sb.append(",weight="+weight);
    sb.append(" - "+(is_dirty? "DIRTY":"CLEAN"));
    
    sb.append(nl);
    
    return sb.toString();
  }

  public boolean isBounded()
  {
    return false;
  }
  
  public float getMinScore()
  {
    return 0f;
  }
  
  public float getMaxScore()
  {
    return 0f;
  }

} // class CreditMatrixElement_AppWeight
