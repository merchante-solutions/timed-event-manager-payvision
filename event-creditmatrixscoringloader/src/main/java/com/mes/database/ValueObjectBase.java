package com.mes.database;

/**
 * ValueObjectBase class
 * 
 * Base class for value objects.
 */
public abstract class ValueObjectBase extends Object implements Persistable
{
  // constants
  // (none)
  
  // data members
  protected boolean is_dirty;
  
  // construction
  protected ValueObjectBase()
  {
    is_dirty=false;
  }
  protected ValueObjectBase(boolean is_dirty)
  {
    this.is_dirty=is_dirty;
  }
  
  // methods
  
  public final boolean    isDirty()
  {
    return is_dirty;
  }
  
  public final void       dirty()
  {
    is_dirty=true;
  }
  
  public final void       clean()
  {
    is_dirty=false;
  }
  
  /**
   * clear()
   * 
   * Alters the state of the object such that its state 
   *  equals the state of a newly created object via the default constructor.
   */
  public abstract void clear();
  
} // ValueObjectBase
