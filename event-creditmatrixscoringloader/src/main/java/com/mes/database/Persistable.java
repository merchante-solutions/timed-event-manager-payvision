package com.mes.database;

/**
 * Persistable interface
 * 
 * Implement this interface when intending to persist objects of the implementing class.
 * Used in conjunction w/ db logic that is aware of this interface.
 */
public interface Persistable
{
  // constants
  // (none)
  
  // methods
  
  /**
   * isDirty()
   * 
   * State of object has changed?
   * Used to determine whether or not the object should be persisted based on altered state.
   */
  public boolean    isDirty();
  /**
   * dirty()
   * 
   * Sets dirty flag.
   */
  public void       dirty();
  /**
   * clean()
   * 
   * Clears dirty flag.
   */
  public void       clean();
  
} // interface Persistable
