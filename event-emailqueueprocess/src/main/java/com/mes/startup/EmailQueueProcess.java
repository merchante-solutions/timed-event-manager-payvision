/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/BankServTransmit.sqlj $

  Description:

    EmailQueueProcess

    Timed event to process the outgoing email queue

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-31 11:13:04 -0800 (Mon, 31 Jan 2011) $
  Version            : $Revision: 18354 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import com.mes.database.SQLJConnectionBase;
import com.mes.net.EmailQueue;

public class EmailQueueProcess extends EventBase
{
  public EmailQueueProcess()
  {
  }
  
  public boolean execute()
  {
    boolean result = false;
    
    try
    {
      EmailQueue.send(true);
      result = true;
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
      result = false;
    }
    
    return result;
  }
  
  public static void main(String[] args)
  {
    SQLJConnectionBase.initStandalone();
    
    EmailQueueProcess worker = new EmailQueueProcess();
    
    worker.execute();
  }
}
