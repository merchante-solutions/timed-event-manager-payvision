/*@lineinfo:filename=AmexChargebackUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/startup/AmexChargebackUpload.sqlj $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2011-08-15 11:43:18 -0700 (Mon, 15 Aug 2011) $
  Version            : $Revision: 19132 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.chargeback.utils.ChargeBackConstants;
import com.mes.constants.MesEmails;
import com.mes.settlement.SettlementRecord;
import sqlj.runtime.ResultSetIterator;

public class AmexChargebackUpload extends AmexFileBase
{
  public AmexChargebackUpload( )
  {
    PropertiesFilename          = "amex-wcb.properties";

    CardTypeTable               = "network_chargeback_amex";
    CardTypeTableActivity       = "network_chargeback_activity";
    CardTypePostTotals          = "am_wcb";
    CardTypeFileDesc            = "Amex Chargeback";
    DkOutgoingHost              = null;
    DkOutgoingUser              = null;
    DkOutgoingPassword          = null;
    DkOutgoingPath              = null;
    DkOutgoingUseBinary         = false;
    DkOutgoingSendFlagFile      = true;
    SettlementAddrsNotify       = MesEmails.MSG_ADDRS_OUTGOING_CB_NOTIFY;
    SettlementAddrsFailure      = MesEmails.MSG_ADDRS_OUTGOING_CB_FAILURE;
    SettlementRecMT             = SettlementRecord.MT_SECOND_PRESENTMENT;
  }
  
  protected boolean processTransactions( )
  {
    Date                      cpd               = null;
    Date                      fileDate          = null;
    ResultSetIterator         it                = null;
    int                       recCount          = 0;
    String                    workFilename      = null;
    
    try
    {
      if ( TestFilename == null )
      {
        FileTimestamp   = new Timestamp(Calendar.getInstance().getTime().getTime());
        fileDate        = new Date( FileTimestamp.getTime() );
        cpd             = getSettlementDate();

        workFilename    = generateFilename("am_wcb"                                                     // base name
                                          + String.valueOf(Integer.parseInt( getEventArg(0) )),         // bank number
                                          ".dat");                                                      // file extension

        /*@lineinfo:generated-code*//*@lineinfo:75^9*/

//  ************************************************************
//  #sql [Ctx] { update  network_chargeback_activity cba
//            set     cba.load_filename = :workFilename,
//                    cba.settlement_date = :cpd
//            where   cba.cb_load_sec in
//                    (
//                      select  cb.cb_load_sec 
//                      from    network_chargebacks           cb
//                      where   cb.bank_number in (select bank_number from mbs_banks where processor_id = 1)
//                              and cb.incoming_date between (:fileDate-180) and :fileDate 
//                              and cb.merchant_number != 0 
//                              and not cb.card_number_enc is null
//                              and cb.card_type = 'AM'
//                    ) 
//                    and cba.load_filename is null
//                    and cba.action_code = nvl(:ActionCode,cba.action_code)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  network_chargeback_activity cba\n          set     cba.load_filename =  :1 ,\n                  cba.settlement_date =  :2 \n          where   cba.cb_load_sec in\n                  (\n                    select  cb.cb_load_sec \n                    from    network_chargebacks           cb\n                    where   cb.bank_number in (select bank_number from mbs_banks where processor_id = 1)\n                            and cb.incoming_date between ( :3 -180) and  :4  \n                            and cb.merchant_number != 0 \n                            and not cb.card_number_enc is null\n                            and cb.card_type = 'AM'\n                  ) \n                  and cba.load_filename is null\n                  and cba.action_code = nvl( :5 ,cba.action_code)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.AmexChargebackUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setDate(3,fileDate);
   __sJT_st.setDate(4,fileDate);
   __sJT_st.setString(5,ActionCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:92^9*/
        // only process if there were records updated 
        recCount = Ctx.getExecutionContext().getUpdateCount();
      }
      else    // re-build a previous file
      {
        FileTimestamp   = new Timestamp( getFileDate(TestFilename).getTime() );
        fileDate        = new Date( FileTimestamp.getTime() );
        workFilename    = TestFilename;
        recCount        = 1;   // always re-build
      }
      
      if ( recCount > 0 )
      {
        // NOTE: Second presentments are issued via the Amex website.
        //       Only merchant adjustments are handled by this timed event.
        addChargebackAdjustmentProcessEntry( workFilename );
        addMbsProcessEntry( ChargeBackConstants.PROC_TYPE_CB_FILE.getIntValue(), workFilename );
      }        
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "processTransactions()", e.toString());
      logEntry("processTransactions(" + workFilename + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return( true );
  }
  
  public static void main( String[] args )
  {
    AmexChargebackUpload              test          = null;
    
    try
    { 
      test = new AmexChargebackUpload();
      test.executeCommandLine( args );
    }
    catch( Exception e )
    {
      log.debug(e.toString());
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}/*@lineinfo:generated-code*/