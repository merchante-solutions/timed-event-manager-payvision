/*@lineinfo:filename=TridentVexAdjustmentUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/TridentVexAdjustmentUpload.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-05-31 14:24:22 -0700 (Thu, 31 May 2012) $
  Version            : $Revision: 20222 $

  Change History:
     See VSS database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.constants.MesEmails;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;
import com.mes.support.StringUtilities;
import com.mes.support.TridentTools;
import masthead.client.NetConnectAuthClient;
import masthead.formats.visad.DMessageUtil;
import masthead.formats.visad.RequestDMessage;
import masthead.formats.visad.ResponseDMessage;
import sqlj.runtime.ResultSetIterator;

public class TridentVexAdjustmentUpload extends EventBase
{
  static Logger log = Logger.getLogger(TridentVexAdjustmentUpload.class);

  private final static int[] ActiveBanks = 
  {
    mesConstants.BANK_ID_MES,
    mesConstants.BANK_ID_STERLING,
    mesConstants.BANK_ID_CBT,
  };

  public static class QRowData
  {
    private   long        RecId               = 0L;
    private   long        ActionRecId         = 0L;
    private   String      ProfileId           = null;
    private   String      MerchantId          = null;
    private   String      TranId              = null;
    private   double      TranAmount          = 0.0;
    private   Timestamp   TranTime            = null;
    private   String      CardNumberFull      = null;
    private   String      CardExpDate         = null;
    private   String      MerchantName        = null;
    private   String      MerchantCity        = null;
    private   String      MerchantState       = null;
    private   String      MerchantZip         = null;
    private   String      TerminalId          = null;
    private   String      CategoryCode        = null;
    private   String      NetworkId           = null;
    private   String      TraceNumber         = null;
    private   String      RetrievalRefNumber  = null;
    private   String      AcquirerBinNumber   = null;
    private   String      TimeZone            = null;
    private   String      TranCode            = null;
      
    public QRowData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      RecId           = resultSet.getLong("rec_id");
      ActionRecId     = resultSet.getLong("action_rec_id");
      ProfileId       = resultSet.getString( "profile_id");
      MerchantId      = resultSet.getString( "merchant_number" );
      TranId          = resultSet.getString( "transaction_id" );
      TranAmount      = resultSet.getDouble( "transaction_amount" );
      TranTime        = resultSet.getTimestamp( "transaction_time" );
      CardExpDate     = resultSet.getString( "card_exp_date" );
      MerchantName    = resultSet.getString( "merchant_name" );
      MerchantCity    = resultSet.getString( "merchant_city" );
      MerchantState   = resultSet.getString( "merchant_state" );
      MerchantZip     = resultSet.getString( "merchant_zip" );
      CategoryCode    = resultSet.getString( "merchant_category_code" );
      CardNumberFull  = resultSet.getString( "card_number_full" );
      NetworkId       = resultSet.getString( "network_id" );
      TraceNumber     = resultSet.getString( "trace_number" );
      RetrievalRefNumber  = resultSet.getString( "retrieval_ref_number");
      AcquirerBinNumber   = resultSet.getString( "acquirer_bin_number");
      TimeZone        = resultSet.getString( "time_zone" );
      TranCode        = resultSet.getString("tran_code");
    }
    
    public String getAcquirerBinNumber()  { return( AcquirerBinNumber ); }
    public long   getActionRecId()        { return( ActionRecId ); }
    public String getCardExpDate()        { return( CardExpDate ); }
    public String getCardNumberFull()     { return( CardNumberFull ); }
    public String getCategoryCode()       { return( CategoryCode ); }
    public String getMerchantCity()       { return( MerchantCity ); }
    public String getMerchantId()         { return( MerchantId ); }
    public String getMerchantName()       { return( MerchantName ); }
    public String getMerchantState()      { return( MerchantState ); }
    public String getMerchantZip()        { return( MerchantZip ); }
    public String getNetworkId()          { return( NetworkId ); }
    public String getProfileId()          { return( ProfileId ); }
    public long   getRecId()              { return( RecId ); }
    public String getRetrievalRefNumber() { return( RetrievalRefNumber ); }
    public String getTimeZone()           { return( TimeZone ); }
    public String getTraceNumber()        { return( TraceNumber ); }
    public double getTranAmount()         { return( TranAmount ); }
    public String getTranCode()           { return( TranCode ); }
    public String getTranId()             { return( TranId ); }
    public Timestamp getTranTime()        { return( TranTime ); }
  }
  
  private int       FileBankNumber    = 0;
  private String    TestFilename      = null;
  private String    TridentHost       = null;
  private boolean   Verbose           = false;
    
  public TridentVexAdjustmentUpload()
  {
    PropertiesFile pf   = null;
    
    PropertiesFilename  = "trident-vex.properties";
    
    // load the host and port from the properties file
    try
    {
      pf          = new PropertiesFile( PropertiesFilename );
    } catch( Exception e) {}
    TridentHost = pf.getString("trident.host",TridentApiConstants.DESKTOP_HOST_SIMULATOR);
    
    log.debug("Trident Host: " + TridentHost);
  }
    
  public boolean execute()
  {
    Date                activityDate  = null;
    StringBuffer        errorMsg      = new StringBuffer("");
    ResultSetIterator   it            = null;
    RequestDMessage     request       = DMessageUtil.getBlankAdjustment();
    ResponseDMessage    response      = null;
    ResultSet           resultSet     = null;
    QRowData            row           = null;
    Vector              rows          = new Vector();
    boolean             retVal        = false;
    String              workFilename  = null;
    
    try
    {
      connect(true);
      
      for( int bankIdx = 0; bankIdx < ActiveBanks.length; ++bankIdx )
      {
        FileBankNumber = ActiveBanks[bankIdx];
      
        if ( TestFilename == null )
        {
          /*@lineinfo:generated-code*//*@lineinfo:174^11*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(sysdate) 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc(sysdate)  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TridentVexAdjustmentUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:178^11*/
          workFilename = buildFilename( ("vex" + String.valueOf(FileBankNumber)) );
  
          /*@lineinfo:generated-code*//*@lineinfo:181^11*/

//  ************************************************************
//  #sql [Ctx] { update  trident_debit_activity    tda
//              set     tda.load_filename = :workFilename
//              where   tda.rec_id in
//                      (
//                        select  dt.rec_id
//                        from    trident_debit_financial   dt
//                        where   dt.bank_number = :FileBankNumber and
//                                dt.settlement_date between (:activityDate-180) and :activityDate
//                      ) 
//                      and tda.load_filename is null 
//                      and exists
//                      ( 
//                        select  ac.short_action_code
//                        from    trident_debit_action_codes    ac
//                        where   ac.short_action_code = tda.action_code and
//                                nvl(ac.vex_adjustment,'N') = 'Y'
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_debit_activity    tda\n            set     tda.load_filename =  :1 \n            where   tda.rec_id in\n                    (\n                      select  dt.rec_id\n                      from    trident_debit_financial   dt\n                      where   dt.bank_number =  :2  and\n                              dt.settlement_date between ( :3 -180) and  :4 \n                    ) \n                    and tda.load_filename is null \n                    and exists\n                    ( \n                      select  ac.short_action_code\n                      from    trident_debit_action_codes    ac\n                      where   ac.short_action_code = tda.action_code and\n                              nvl(ac.vex_adjustment,'N') = 'Y'\n                    )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.TridentVexAdjustmentUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setInt(2,FileBankNumber);
   __sJT_st.setDate(3,activityDate);
   __sJT_st.setDate(4,activityDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:200^11*/
        }
        else    // allow test files to be used
        {
          workFilename    = TestFilename;
          FileBankNumber  = getFileBankNumber(TestFilename);
    
          /*@lineinfo:generated-code*//*@lineinfo:207^11*/

//  ************************************************************
//  #sql [Ctx] { select  max(tda.action_date) 
//              from    trident_debit_activity      tda
//              where   tda.load_filename = :workFilename
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max(tda.action_date)  \n            from    trident_debit_activity      tda\n            where   tda.load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.TridentVexAdjustmentUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:212^11*/
        }
            
        /*@lineinfo:generated-code*//*@lineinfo:215^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tp.terminal_id                  as profile_id,
//                    lpad(ltrim(rtrim(tp.sic_code)),4,'0')         
//                                                    as merchant_category_code,
//                    tdn.pos_network_id              as network_id,
//                    decode(tda.action_code,
//                           'V',decode(tdf.debit_credit_indicator,'C','AC','AD'),
//                           'R',decode(tdf.debit_credit_indicator,'C','AD','AC'),
//                           'XX')                    as tran_code,
//                    tda.rec_id                      as rec_id,
//                    tda.action_rec_id               as action_rec_id,
//                    tdf.merchant_number             as merchant_number,
//                    lpad(tdf.trace_number,6,'0')    as trace_number,
//                    tdf.retrieval_reference_number  as retrieval_ref_number,
//                    tp.bin_number                   as acquirer_bin_number,
//                    tdf.transaction_identifier      as transaction_id,
//                    tdf.transaction_amount          as transaction_amount,
//                    tdf.local_transaction_time      as transaction_time,
//                    tdf.card_acceptor_name          as merchant_name,
//                    tdf.card_acceptor_city          as merchant_city,
//                    tdf.geo_zip_five                as merchant_zip,
//                    tp.addr_state                   as merchant_state,
//                    tp.time_zone                    as time_zone,
//                    tdf.card_expiration_date        as card_exp_date,
//                    dukpt_decrypt_wrapper(tdf.card_number_enc)  as card_number_full
//           from     trident_debit_financial     tdf,
//                    trident_debit_activity      tda,
//                    trident_profile             tp,
//                    trident_debit_networks      tdn
//            where   tdf.bank_number = :FileBankNumber
//                    and tdf.settlement_date between (:activityDate-180) and :activityDate
//                    and tda.rec_id = tdf.rec_id 
//                    and tda.load_filename = :workFilename
//                    and tp.catid = tdf.card_acceptor_terminal_id
//                    and tdn.visa_network_id = tdf.network_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tp.terminal_id                  as profile_id,\n                  lpad(ltrim(rtrim(tp.sic_code)),4,'0')         \n                                                  as merchant_category_code,\n                  tdn.pos_network_id              as network_id,\n                  decode(tda.action_code,\n                         'V',decode(tdf.debit_credit_indicator,'C','AC','AD'),\n                         'R',decode(tdf.debit_credit_indicator,'C','AD','AC'),\n                         'XX')                    as tran_code,\n                  tda.rec_id                      as rec_id,\n                  tda.action_rec_id               as action_rec_id,\n                  tdf.merchant_number             as merchant_number,\n                  lpad(tdf.trace_number,6,'0')    as trace_number,\n                  tdf.retrieval_reference_number  as retrieval_ref_number,\n                  tp.bin_number                   as acquirer_bin_number,\n                  tdf.transaction_identifier      as transaction_id,\n                  tdf.transaction_amount          as transaction_amount,\n                  tdf.local_transaction_time      as transaction_time,\n                  tdf.card_acceptor_name          as merchant_name,\n                  tdf.card_acceptor_city          as merchant_city,\n                  tdf.geo_zip_five                as merchant_zip,\n                  tp.addr_state                   as merchant_state,\n                  tp.time_zone                    as time_zone,\n                  tdf.card_expiration_date        as card_exp_date,\n                  dukpt_decrypt_wrapper(tdf.card_number_enc)  as card_number_full\n         from     trident_debit_financial     tdf,\n                  trident_debit_activity      tda,\n                  trident_profile             tp,\n                  trident_debit_networks      tdn\n          where   tdf.bank_number =  :1 \n                  and tdf.settlement_date between ( :2 -180) and  :3 \n                  and tda.rec_id = tdf.rec_id \n                  and tda.load_filename =  :4 \n                  and tp.catid = tdf.card_acceptor_terminal_id\n                  and tdn.visa_network_id = tdf.network_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.TridentVexAdjustmentUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,FileBankNumber);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setDate(3,activityDate);
   __sJT_st.setString(4,workFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.TridentVexAdjustmentUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:251^9*/
        resultSet = it.getResultSet();
      
        rows.removeAllElements();   // remove any existing entries
        
        while( resultSet.next() )
        {
          rows.addElement( new QRowData(resultSet) );
        }
        resultSet.close();
        it.close();
      
        for( int i = 0; i < rows.size(); ++i )
        {
          row = (QRowData)rows.elementAt(i);
        
          if ( "XX".equals(row.getTranCode()) )
          {
            continue;   // invalid tran code, skip?
          }
        
          request.setTransactionCode( row.getTranCode() );    
          request.setTransactionAmount( TridentTools.encodeAmount(row.getTranAmount(),1) );
            // Merchant info
          request.setProfileId( row.getProfileId() );
          request.setAcquirerBin( row.getAcquirerBinNumber() );
          request.setCityCode( row.getMerchantZip() );
          request.setTimeZoneDifferential( row.getTimeZone() );
          request.setMerchantCategoryCode( row.getCategoryCode() );
          request.setCardAcceptorData( row.getMerchantName(), row.getMerchantCity(), row.getMerchantState() );
                 
          // Original transaction info
          String year = row.getCardExpDate().substring(0, 2);
          String month = row.getCardExpDate().substring(2);
          request.setCustomerDataField( row.getCardNumberFull(), month + year  );
          request.setReversalandIncrementalTransactionID( row.getTranId() );

          request.setReversalAndCancelDataI( "000000" + DateTimeFormatter.getFormattedDate( row.getTranTime(), "MMddyyHHmmss" ) + row.getRetrievalRefNumber() ); 
          request.setReversalAndCancelDataII( row.getTraceNumber() + row.getNetworkId());

          NetConnectAuthClient client = new NetConnectAuthClient();
          client.setUrl(TridentHost);
          response = client.sendRequest(request);

          /*@lineinfo:generated-code*//*@lineinfo:295^11*/

//  ************************************************************
//  #sql [Ctx] { update  trident_debit_activity  tda
//              set     tda.trident_tran_id = :response.getUuid()
//              where   tda.rec_id = :row.getRecId()
//                      and tda.action_rec_id = :row.getActionRecId()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4885 = response.getUuid();
 long __sJT_4886 = row.getRecId();
 long __sJT_4887 = row.getActionRecId();
   String theSqlTS = "update  trident_debit_activity  tda\n            set     tda.trident_tran_id =  :1 \n            where   tda.rec_id =  :2 \n                    and tda.action_rec_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.TridentVexAdjustmentUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_4885);
   __sJT_st.setLong(2,__sJT_4886);
   __sJT_st.setLong(3,__sJT_4887);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:301^11*/
        
          if( Verbose )
          {
            log.debug("request " + request.toXml());
            log.debug("response " + response.toXml());
          }

          if( response == null || !"00".equals(response.getResponseCode()) )
          {
            if( errorMsg.length() == 0 )
            {
              errorMsg.append( "\n\nThe following control numbers had problems processing:\n\n" );

              errorMsg.append( StringUtilities.leftJustify( "Ctl #"           , 11  , ' ' ) );
              errorMsg.append( StringUtilities.leftJustify( "Merchant ID"     , 17  , ' ' ) );
              errorMsg.append( StringUtilities.leftJustify( "Trident Tran ID" , 34  , ' ' ) );
              errorMsg.append( StringUtilities.leftJustify( "Response"        , 18  , ' ' ) );
              errorMsg.append( "\n");
              errorMsg.append( StringUtilities.leftJustify( ""                , 80  , '=' ) );
              errorMsg.append( "\n");
            }

            errorMsg.append( StringUtilities.leftJustify( String.valueOf(row.getRecId())          , 11  , ' ' ) );
            errorMsg.append( StringUtilities.leftJustify( row.getMerchantId()                     , 17  , ' ' ) );
            if( response != null )
            {
              errorMsg.append( StringUtilities.leftJustify( response.getUuid()                      , 34  , ' ' ) );
              errorMsg.append( StringUtilities.leftJustify( response.getAuthorizationResponseText() , 18  , ' ' ) );
            }
            errorMsg.append( "\n");
          }
        }
        
        if ( TestFilename != null )
        {
          break;    // test file, do not loop through bank list
        }
      }
      if( errorMsg.length() != 0 )
      {
        errorMsg.append("\n\n");
        sendStatusEmail(errorMsg);
      }
      retVal = true; 
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return(retVal);
  }
  
  private void sendStatusEmail(StringBuffer errorMsg)
  {
    MailMessage   msg             = null;

    try
    {
      msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_DEBIT_ADJ_ERROR);
      msg.setSubject("Debit Adjustment Failure(s) [" + 
                      DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy HH:mm:ss") + "]");
      msg.setText(errorMsg.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "sendStatusEmail()", e.toString());
      logEntry("sendStatusEmail()", e.toString());
    }
  }

  public void setTestFilename( String testFilename )
  {
    TestFilename = testFilename;
  }

  public void setVerboseFlag( boolean newValue )
  {
    Verbose = newValue;
  }
    
  public static void main( String[] args )
  {
    TridentVexAdjustmentUpload       tc  = null;
    
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      tc = new TridentVexAdjustmentUpload();
      tc.setVerboseFlag(true);
      if ( args.length > 0 )
      {
        tc.setTestFilename(args[0]);
      }        
      tc.execute();
    }
    catch( Exception e )
    {
      log.debug(e.toString());
    }
    finally
    {
      try{ tc.cleanUp(); } catch( Exception ee ) {}
    }
  }
}/*@lineinfo:generated-code*/