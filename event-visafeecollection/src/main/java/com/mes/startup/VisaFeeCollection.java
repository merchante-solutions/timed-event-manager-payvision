/*@lineinfo:filename=VisaFeeCollection*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/VisaFeeCollection.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.settlement.SettlementRecord;
import sqlj.runtime.ResultSetIterator;

public class VisaFeeCollection extends VisaFileBase
{
  public VisaFeeCollection( )
  {
    PropertiesFilename          = "visa-fee-collection.properties";

    CardTypeTable               = "visa_settlement_fee_collect";
    CardTypeTableActivity       = null;
    CardTypePostTotals          = "vs_fee";
    CardTypeFileDesc            = "Visa Fee Collect";
    DkOutgoingHost              = MesDefaults.DK_VISA_EP_HOST;
    DkOutgoingUser              = MesDefaults.DK_VISA_EP_USER;
    DkOutgoingPassword          = MesDefaults.DK_VISA_EP_PASSWORD;
    DkOutgoingPath              = MesDefaults.DK_VISA_EP_OUTGOING_PATH;
    DkOutgoingUseBinary         = false;
    DkOutgoingSendFlagFile      = true;
    SettlementAddrsNotify       = MesEmails.MSG_ADDRS_OUTGOING_VISA_NOTIFY;
    SettlementAddrsFailure      = MesEmails.MSG_ADDRS_OUTGOING_VISA_FAILURE;
    SettlementRecMT             = SettlementRecord.MT_FEE_COLLECTION;
  }
  
  protected boolean processTransactions()
  {
    Date                      cpd               = null;
    ResultSetIterator         it                = null;
    long                      workFileId        = 0L;
    String                    workFilename      = null;
    
    try
    {
      if ( TestFilename == null )
      {
        FileTimestamp   = new Timestamp(Calendar.getInstance().getTime().getTime());

        cpd             = getSettlementDate();

        workFilename    = generateFilename("vs_fee"                                                     // base name
                                          + String.valueOf(Integer.parseInt( getEventArg(0) )),         // bank number
                                          ".ctf");                                                      // file extension
        workFileId      = loadFilenameToLoadFileId(workFilename);

        /*@lineinfo:generated-code*//*@lineinfo:76^9*/

//  ************************************************************
//  #sql [Ctx] { update  visa_settlement_fee_collect fc
//            set     fc.load_filename  = :workFilename,
//                    fc.load_file_id   = :workFileId,
//                    fc.settlement_date= :cpd
//            where   fc.load_file_id = 0
//                    and fc.tran_type = 1    -- outgoing
//                    and nvl(fc.test_flag,'Y') = 'N'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  visa_settlement_fee_collect fc\n          set     fc.load_filename  =  :1 ,\n                  fc.load_file_id   =  :2 ,\n                  fc.settlement_date=  :3 \n          where   fc.load_file_id = 0\n                  and fc.tran_type = 1    -- outgoing\n                  and nvl(fc.test_flag,'Y') = 'N'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.VisaFeeCollection",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setLong(2,workFileId);
   __sJT_st.setDate(3,cpd);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:85^9*/
      }
      else    // re-build a previous file
      {
        FileTimestamp   = new Timestamp( getFileDate(TestFilename).getTime() );
        workFilename    = TestFilename;
        workFileId      = loadFilenameToLoadFileId(workFilename);
      }

      // select the transactions to process
      /*@lineinfo:generated-code*//*@lineinfo:95^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  null                          as action_code,
//                  0                             as batch_id,
//                  nvl(fc.settle_rec_id,0)       as batch_record_id,
//                  fc.settlement_date            as batch_date,
//                  fc.transaction_date           as transaction_date,
//                  fc.card_number_enc            as card_number_enc,
//                  nvl(fc.auth_tran_id,'000000000000000')
//                                                as auth_tran_id,
//                  dukpt_decrypt_wrapper(fc.card_number_enc)
//                                                as card_number_full,
//                  fc.dest_bin                   as issuer_bin,
//                  fc.source_bin                 as bin_number,
//                  fc.reason_code                as reason_code,
//                  fc.message_text               as message_text,
//                  fc.debit_credit_indicator     as debit_credit_indicator,
//                  nvl(fc.reimbursement_attribute,0) 
//                                                as reimbursement_attribute,
//                  fc.currency_code              as currency_code,
//                  fc.transaction_amount         as transaction_amount
//          from    visa_settlement_fee_collect   fc
//          where   fc.load_file_id = :workFileId
//                  and fc.tran_type = 1    -- outgoing only
//                  and nvl(fc.test_flag,'Y') = 'N'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  null                          as action_code,\n                0                             as batch_id,\n                nvl(fc.settle_rec_id,0)       as batch_record_id,\n                fc.settlement_date            as batch_date,\n                fc.transaction_date           as transaction_date,\n                fc.card_number_enc            as card_number_enc,\n                nvl(fc.auth_tran_id,'000000000000000')\n                                              as auth_tran_id,\n                dukpt_decrypt_wrapper(fc.card_number_enc)\n                                              as card_number_full,\n                fc.dest_bin                   as issuer_bin,\n                fc.source_bin                 as bin_number,\n                fc.reason_code                as reason_code,\n                fc.message_text               as message_text,\n                fc.debit_credit_indicator     as debit_credit_indicator,\n                nvl(fc.reimbursement_attribute,0) \n                                              as reimbursement_attribute,\n                fc.currency_code              as currency_code,\n                fc.transaction_amount         as transaction_amount\n        from    visa_settlement_fee_collect   fc\n        where   fc.load_file_id =  :1 \n                and fc.tran_type = 1    -- outgoing only\n                and nvl(fc.test_flag,'Y') = 'N'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.VisaFeeCollection",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,workFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.VisaFeeCollection",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:120^7*/
      handleSelectedRecords( it, workFilename );
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "processTransactions()", e.toString());
      logEntry("processTransactions()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return( true );
  }
  
  public static void main( String[] args )
  {
      try
      {
          if (args.length > 0 && args[0].equals("testproperties")) {
              EventBase.printKeyListStatus(new String[]{
                      MesDefaults.DK_VISA_EP_HOST,
                      MesDefaults.DK_VISA_EP_USER,
                      MesDefaults.DK_VISA_EP_PASSWORD,
                      MesDefaults.DK_VISA_EP_OUTGOING_PATH
              });
          }
      } catch (Exception e) {
          System.out.println(e.toString());
      }

    VisaFeeCollection                 test          = null;
    
    try
    { 
      test = new VisaFeeCollection();
      test.executeCommandLine( args );
    }
    catch( Exception e )
    {
      log.debug(e.toString());
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}/*@lineinfo:generated-code*/