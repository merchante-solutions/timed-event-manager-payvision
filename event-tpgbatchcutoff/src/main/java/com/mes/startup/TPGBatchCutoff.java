/*@lineinfo:filename=TPGBatchCutoff*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/TPGBatchCutoff.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.api.ApiDb;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.ThreadPool;
import sqlj.runtime.ResultSetIterator;

public class TPGBatchCutoff extends EventBase
{
  private class BatchCloseInfo
  {
    public    Date          BatchDate     = null;
    public    int           BatchNumber   = 1;
    public    Date          LastCloseDate = null;
    public    long          MerchantId    = 0L;
    public    String        TerminalId    = null;

    BatchCloseInfo( ResultSet resultSet )
      throws java.sql.SQLException
    {
      TerminalId    = resultSet.getString("terminal_id");
      BatchDate     = resultSet.getDate("batch_date");
      LastCloseDate = resultSet.getDate("last_auto_close_date");
      MerchantId    = resultSet.getLong("merchant_number");

      // establish a new batch number for this batch
      BatchNumber   = ((resultSet.getInt("last_batch_number") + 1)%1000);
      if ( BatchNumber == 0 )
      {
        BatchNumber = 1;    // do not allow 0 for a batch number
      }
    }
  }
  
  private static class BatchCloseWorker
    extends SQLJConnectionBase
    implements Runnable
  {
    BatchCloseInfo      CloseInfo     = null;
    Date                CurrentDate   = null;
    
    public BatchCloseWorker( BatchCloseInfo bci, Date currentDate )
    {
      CloseInfo     = bci;
      CurrentDate   = currentDate;
    }
    
    public void run()
    {
      try
      {
        connect(true);
        
        ApiDb.closeBatch(CloseInfo.TerminalId,CloseInfo.BatchDate,CloseInfo.BatchNumber,CloseInfo.LastCloseDate);
      }
      catch( Exception e )
      {
        try{ /*@lineinfo:generated-code*//*@lineinfo:88^14*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:88^36*/ }catch( Exception ee ) {}
        logEntry("run(" + CloseInfo.TerminalId + "," + CloseInfo.BatchNumber + ")",e.toString());
        try{ /*@lineinfo:generated-code*//*@lineinfo:90^14*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:90^34*/ }catch( Exception ee ) {}
      }
      finally
      {
        cleanUp();
      }
    }
  }

  static Logger log = Logger.getLogger(TPGBatchCutoff.class);

  private String        TestTerminalId    = null;

  public boolean execute()
  {
    int                 batchCloseDefault   = 2100;
    Vector              batchesToClose      = new Vector();
    Date                currentDate         = null;
    int                 currentTime         = 0;
    ResultSetIterator   it                  = null;
    ThreadPool          pool                = null;
    ResultSet           resultSet           = null;
    boolean             retVal              = false;

    try
    {
      connect(true);

      batchCloseDefault = MesDefaults.getInt(MesDefaults.DK_TPG_DEFAULT_BATCH_CLOSE_TIME);

      /*@lineinfo:generated-code*//*@lineinfo:120^7*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(sysdate),
//                  to_number(to_char(sysdate,'hh24mi'))
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc(sysdate),\n                to_number(to_char(sysdate,'hh24mi'))\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TPGBatchCutoff",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   currentDate = (java.sql.Date)__sJT_rs.getDate(1);
   currentTime = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:126^7*/

      // select the terminal ids that are ready to close their batch
      /*@lineinfo:generated-code*//*@lineinfo:129^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tp.terminal_id                  as terminal_id,
//                  tp.merchant_number              as merchant_number,
//                  trunc(sysdate)                  as batch_date,
//                  nvl(tpapi.last_batch_number,0)  as last_batch_number,
//                  case
//                    when nvl(tpapi.last_auto_close_date,trunc(sysdate)) < :currentDate-1 then :currentDate-1
//                    else :currentDate
//                  end                             as last_auto_close_date
//          from    trident_profile         tp,
//                  trident_profile_api     tpapi
//          where   ( nvl(tp.api_enabled,'N') = 'Y' or
//                    exists
//                    (
//                      select  tpc.product_code
//                      from    trident_product_codes tpc
//                      where   tpc.product_code = nvl(tp.product_code,'XX')
//                              and nvl(tpc.trident_payment_gateway,'N') = 'Y'
//                    )
//                  ) 
//                  and tpapi.batch_close_time != '9999'
//                  and tpapi.terminal_id(+) = tp.terminal_id 
//                  and
//                  (
//                    ( nvl(tpapi.last_auto_close_date,'01-JAN-2000') < :currentDate 
//                      and nvl(tpapi.batch_close_time,:batchCloseDefault) <= :currentTime )
//                     or( nvl(tpapi.last_auto_close_date,trunc(sysdate))  < :currentDate-1 )
//                  )
//                  and ( :TestTerminalId is null or tp.terminal_id = :TestTerminalId )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tp.terminal_id                  as terminal_id,\n                tp.merchant_number              as merchant_number,\n                trunc(sysdate)                  as batch_date,\n                nvl(tpapi.last_batch_number,0)  as last_batch_number,\n                case\n                  when nvl(tpapi.last_auto_close_date,trunc(sysdate)) <  :1 -1 then  :2 -1\n                  else  :3 \n                end                             as last_auto_close_date\n        from    trident_profile         tp,\n                trident_profile_api     tpapi\n        where   ( nvl(tp.api_enabled,'N') = 'Y' or\n                  exists\n                  (\n                    select  tpc.product_code\n                    from    trident_product_codes tpc\n                    where   tpc.product_code = nvl(tp.product_code,'XX')\n                            and nvl(tpc.trident_payment_gateway,'N') = 'Y'\n                  )\n                ) \n                and tpapi.batch_close_time != '9999'\n                and tpapi.terminal_id(+) = tp.terminal_id \n                and\n                (\n                  ( nvl(tpapi.last_auto_close_date,'01-JAN-2000') <  :4  \n                    and nvl(tpapi.batch_close_time, :5 ) <=  :6  )\n                   or( nvl(tpapi.last_auto_close_date,trunc(sysdate))  <  :7 -1 )\n                )\n                and (  :8  is null or tp.terminal_id =  :9  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.TPGBatchCutoff",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,currentDate);
   __sJT_st.setDate(2,currentDate);
   __sJT_st.setDate(3,currentDate);
   __sJT_st.setDate(4,currentDate);
   __sJT_st.setInt(5,batchCloseDefault);
   __sJT_st.setInt(6,currentTime);
   __sJT_st.setDate(7,currentDate);
   __sJT_st.setString(8,TestTerminalId);
   __sJT_st.setString(9,TestTerminalId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.TPGBatchCutoff",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:159^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        batchesToClose.add( new BatchCloseInfo(resultSet) );
      }
      resultSet.close();
      it.close();
      
      pool = new ThreadPool(5);

      for( int i = 0; i < batchesToClose.size(); ++i )
      {
        Thread worker = pool.getThread( new BatchCloseWorker((BatchCloseInfo)batchesToClose.elementAt(i),currentDate) );
        worker.start();
      }
      pool.waitForAllThreads();

      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }

  protected void setTestTerminalId( String tid )
  {
    TestTerminalId = tid;
  }

  public static void main( String[] args )
  {
      try {

          if (args.length > 0 && args[0].equals("testproperties")) {
              EventBase.printKeyListStatus(new String[]{
                      MesDefaults.DK_TPG_DEFAULT_BATCH_CLOSE_TIME
              });
          }
      } catch (Exception e) {
          System.out.println(e.toString());
      }

    TPGBatchCutoff      bc  = null;

    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      bc = new TPGBatchCutoff();
      bc.connect();

      if ( "execute".equals(args[0]) )
      {
        bc.setTestTerminalId((args.length > 1) ? args[1] : null);
        bc.execute();
      }
    }
    catch( Exception e )
    {
      log.error(e.toString());
    }
    finally
    {
      try{ bc.cleanUp(); } catch( Exception ee ) {}
    }
  }
}/*@lineinfo:generated-code*/