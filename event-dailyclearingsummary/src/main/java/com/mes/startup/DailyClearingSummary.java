/*@lineinfo:filename=DailyClearingSummary*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

 FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/startup/DailyClearingSummary.sqlj $

 Description:

 Last Modified By   : $Author: elee $
 Last Modified Date : $Date: 2013-08-08 $
 Version            : $Revision:  $

 Change History:
 See SVN database

 Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
 All rights reserved, Unauthorized distribution prohibited.

 This document contains information which is the proprietary
 property of Merchant e-Solutions, Inc.  This document is received in
 confidence and its contents may not be disclosed without the
 prior written consent of Merchant e-Solutions, Inc.

 **************************************************************************/
package com.mes.startup;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import org.apache.log4j.BasicConfigurator;
import com.mes.database.OracleConnectionPool;
import com.mes.reports.MbsReconcileDataBean;

public class DailyClearingSummary extends EventBase {

  private MbsReconcileDataBean dataBean = new MbsReconcileDataBean();
  private int[] bankNumbers = { 3941, 3858, 3943, 3942, 3003 };
  private java.sql.Date beginDate = null;
  private int dayOfWeek = 0;

  public void storeMCClearingSummary() {
    if(dayOfWeek != Calendar.SUNDAY ) {
      //for(int i : bankNumbers) {
      for(int i=0; i<bankNumbers.length; i++) {
        //dataBean.loadClearingSummaryMasterCard(i, beginDate);
        dataBean.loadClearingSummaryMasterCard(bankNumbers[i], beginDate);
        storeClearingSummary(bankNumbers[i], "MC");
      }
    }
  }

  public void storeVSClearingSummary() {
    //for(int i : bankNumbers) {
    for(int i=0; i<bankNumbers.length; i++) {
      //dataBean.loadClearingSummaryVisa(i, beginDate);
      dataBean.loadClearingSummaryVisa(bankNumbers[i], beginDate);
      storeClearingSummary(bankNumbers[i], "VS");
    }
  }

  public void storeAMClearingSummary() {
    if(dayOfWeek != Calendar.SATURDAY ) {
      //for(int i : bankNumbers) {
      for(int i=0; i<bankNumbers.length; i++) {
        //dataBean.loadClearingSummaryAmex(i, beginDate);
        dataBean.loadClearingSummaryAmex(bankNumbers[i], beginDate);
        dataBean.loadClearingSummaryAmexOptblue(bankNumbers[i], beginDate);
        storeClearingSummary(bankNumbers[i], "AM");
      }
    }
  }

  public void storeDSClearingSummary() {
    if(dayOfWeek != Calendar.SUNDAY ) {
      //for(int i : bankNumbers) {
      for(int i=0; i<bankNumbers.length; i++) {
        //dataBean.loadClearingSummaryDiscover(i, beginDate);
        dataBean.loadClearingSummaryDiscover(bankNumbers[i], beginDate);
        storeClearingSummary(bankNumbers[i], "DS");
      }
    }
  }

  private void storeClearingSummary(int bankNumber, String cardType) {
    Vector rows = dataBean.getReportRows();
    MbsReconcileDataBean.ClearingSummary entry = (MbsReconcileDataBean.ClearingSummary)rows.elementAt(0);
    HashMap map = entry.getBinData();

    Set keys =  map.keySet();
    MbsReconcileDataBean.BinSummary binSummary = null;
    String binNum = "";
    //for(Object key : keys) {
    for( Iterator it = keys.iterator(); it.hasNext(); ) {
      //binNum = (String)key;
      binNum = (String)it.next();
      binSummary = (MbsReconcileDataBean.BinSummary)map.get(binNum);

      try {
        connect(true);
        // delete any existing records to avoid primary key violations
        System.out.println("Removing data from daily_clearing_summary (" + 
          com.mes.support.DateTimeFormatter.getFormattedDate(beginDate, "MM/dd/yy") + ", " +
          cardType + ", " +
          bankNumber + ", " +
          binNum + ")");
        /*@lineinfo:generated-code*//*@lineinfo:120^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    daily_clearing_summary
//            where   clearing_date = :beginDate
//                    and card_type = :cardType
//                    and bank_number = :bankNumber
//                    and bin_number = :binNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    daily_clearing_summary\n          where   clearing_date =  :1 \n                  and card_type =  :2 \n                  and bank_number =  :3 \n                  and bin_number =  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.DailyClearingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setString(2,cardType);
   __sJT_st.setInt(3,bankNumber);
   __sJT_st.setString(4,binNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:127^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:129^9*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_clearing_summary
//            (
//              actual_amount,
//              assoc_fees,
//              bank_number,
//              bin_number,
//              card_type,
//              cash_advance_amount,
//              clearing_date,
//              credits_amount,
//              fee_collection_amount,
//              funding_amount,
//              fx_adjustment_amount,
//              incoming_chargebacks_amount,
//              incoming_reversals_amount,
//              reimbursement_fees,
//              rejects_amount,
//              reprocessed_rejects_amount,
//              reversals_amount,
//              sales_amount,
//              second_presentments_amount,
//              second_presentment_rejects_amt
//            )
//            values
//            (
//              :binSummary.getActualAmount(),
//              :binSummary.getAssocFees(),
//              :bankNumber,
//              :binNum,
//              :cardType,
//              :binSummary.getCashAdvanceAmount(),
//              :beginDate,
//              :binSummary.getCreditsAmount(),
//              :binSummary.getFeeCollectionAmount(),
//              :binSummary.getFundingAmount(),
//              :binSummary.getFxAdjustmentAmount(),
//              :binSummary.getIncomingChargebacksAmount(),
//              :binSummary.getIncomingReversalsAmount(),
//              :binSummary.getReimbursementFees(),
//              :binSummary.getRejectsAmount(),
//              :binSummary.getReprocessedRejectsAmount(),
//              :binSummary.getReversalsAmount(),
//              :binSummary.getSalesAmount(),
//              :binSummary.getSecondPresentmentsAmount(),
//              :binSummary.getSecondPresentmentRejectsAmount()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4049 = binSummary.getActualAmount();
 double __sJT_4050 = binSummary.getAssocFees();
 double __sJT_4051 = binSummary.getCashAdvanceAmount();
 double __sJT_4052 = binSummary.getCreditsAmount();
 double __sJT_4053 = binSummary.getFeeCollectionAmount();
 double __sJT_4054 = binSummary.getFundingAmount();
 double __sJT_4055 = binSummary.getFxAdjustmentAmount();
 double __sJT_4056 = binSummary.getIncomingChargebacksAmount();
 double __sJT_4057 = binSummary.getIncomingReversalsAmount();
 double __sJT_4058 = binSummary.getReimbursementFees();
 double __sJT_4059 = binSummary.getRejectsAmount();
 double __sJT_4060 = binSummary.getReprocessedRejectsAmount();
 double __sJT_4061 = binSummary.getReversalsAmount();
 double __sJT_4062 = binSummary.getSalesAmount();
 double __sJT_4063 = binSummary.getSecondPresentmentsAmount();
 double __sJT_4064 = binSummary.getSecondPresentmentRejectsAmount();
   String theSqlTS = "insert into daily_clearing_summary\n          (\n            actual_amount,\n            assoc_fees,\n            bank_number,\n            bin_number,\n            card_type,\n            cash_advance_amount,\n            clearing_date,\n            credits_amount,\n            fee_collection_amount,\n            funding_amount,\n            fx_adjustment_amount,\n            incoming_chargebacks_amount,\n            incoming_reversals_amount,\n            reimbursement_fees,\n            rejects_amount,\n            reprocessed_rejects_amount,\n            reversals_amount,\n            sales_amount,\n            second_presentments_amount,\n            second_presentment_rejects_amt\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.DailyClearingSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_4049);
   __sJT_st.setDouble(2,__sJT_4050);
   __sJT_st.setInt(3,bankNumber);
   __sJT_st.setString(4,binNum);
   __sJT_st.setString(5,cardType);
   __sJT_st.setDouble(6,__sJT_4051);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDouble(8,__sJT_4052);
   __sJT_st.setDouble(9,__sJT_4053);
   __sJT_st.setDouble(10,__sJT_4054);
   __sJT_st.setDouble(11,__sJT_4055);
   __sJT_st.setDouble(12,__sJT_4056);
   __sJT_st.setDouble(13,__sJT_4057);
   __sJT_st.setDouble(14,__sJT_4058);
   __sJT_st.setDouble(15,__sJT_4059);
   __sJT_st.setDouble(16,__sJT_4060);
   __sJT_st.setDouble(17,__sJT_4061);
   __sJT_st.setDouble(18,__sJT_4062);
   __sJT_st.setDouble(19,__sJT_4063);
   __sJT_st.setDouble(20,__sJT_4064);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:176^9*/

        int updateCount = Ctx.getExecutionContext().getUpdateCount();
        if(updateCount==0) {
          logEntry( "storeClearingSummary()", "Error storing clearing summary for card type " + cardType + ", bank number " + bankNumber + ", and report date " + beginDate.toString());
      }
      } catch( Exception e ) {
        logEntry( "storeClearingSummary(" +bankNumber + ", " + cardType +")", e.toString() );
      }
      finally {
        cleanUp();
      }
    }
  }

  public void storeAll(String cardType, Calendar date) {
    dataBean.connect(true);
    dataBean.setReportType(dataBean.RT_CLEARING_SUMMARY);

    beginDate = (date==null) ? dataBean.getReportDateBegin() : new java.sql.Date(date.getTime().getTime());

    Calendar cal = Calendar.getInstance();
    cal.setTime(beginDate);
    dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

    System.out.println("Storing all card types for date: " + com.mes.support.DateTimeFormatter.getFormattedDate(beginDate, "MM/dd/yy"));
    if("mc".equalsIgnoreCase(cardType)) {
      System.out.println("storing MC Clearing Summary");
      storeMCClearingSummary();
    } else if("vs".equalsIgnoreCase(cardType)) {
      System.out.println("storing VS Clearing Summary");
      storeVSClearingSummary();
    } else if("am".equalsIgnoreCase(cardType)) {
      System.out.println("storing AM Clearing Summary");
      storeAMClearingSummary();
    } else if("ds".equalsIgnoreCase(cardType)) {
      System.out.println("storing DS Clearing Summary");
      storeDSClearingSummary();
    } else {
      System.out.println("storing MC Clearing Summary");
      storeMCClearingSummary();
      System.out.println("storing AM Clearing Summary");
      storeAMClearingSummary();
      System.out.println("storing DS Clearing Summary");
      storeDSClearingSummary();
      System.out.println("storing VS Clearing Summary");
      storeVSClearingSummary();
    }
    System.out.println("Done");

    dataBean.disconnect();
  }

  public boolean execute() {
    System.out.println("Entering com.mes.startup.DailyClearingSummary.execute()");
    storeAll("all", null);
    System.out.println("DailyClearingSummary Complete");
    return( true );
  }

  public static void main(String[] args) {
    try {
      com.mes.database.SQLJConnectionBase.initStandalonePool("DEBUG");
      BasicConfigurator.configure();

      DailyClearingSummary dcs = new DailyClearingSummary();
      SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
      String cardType= args.length==0 ? "all" : args[0];
      Calendar start = Calendar.getInstance();

      dcs.connect();
      if(args.length==3) {
        start.setTime(sdf.parse(args[1]));
        Calendar end = Calendar.getInstance();
        end.setTime(sdf.parse(args[2]));

        while(!start.after(end)) {
          System.out.println(start.getTime().toString());
          dcs.storeAll(cardType, start);
          start.add(Calendar.DATE, 1);
        }
      } else if(args.length==2) {
        start.setTime(sdf.parse(args[1]));
        dcs.storeAll(cardType, start);
      } else {
        dcs.execute();
      }
    } catch(Exception e) {
      System.err.println("main(): " + e.toString());
      e.printStackTrace();
    } finally {
      OracleConnectionPool.getInstance().cleanUp();
      Runtime.getRuntime().exit(0);
    }
  }

}/*@lineinfo:generated-code*/