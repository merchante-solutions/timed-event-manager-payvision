package com.mes.reports;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.database.MesQueryHandlerResultSet;
import com.mes.reports.MbsReconcileDataBean.ClearingSummary;
import com.mes.reports.mc.MasterCardClearingSummary;

public abstract class DailyClearingSummaryReport {

	protected static final String DEBUG_LOG_FORMAT = "[%1s()] - loading for bankNumber=%2s, reportBeginDate=%3$tm-%3$td-%3$tY ";
	protected static final String ERROR_LOG_FORMAT = "[%1s()] - Unexpected Exception : %2s";

	public enum ReportType {
		MASTER_CARD("MC","com.mes.reports.mc.MasterCardClearingSummary");
		private final String cardType;
		private final String className;

		private ReportType(String cardType,String value) {
			this.cardType = cardType;
			this.className = value;
		}
		public static ReportType getReportType(String cardbrand) {
			if ( MASTER_CARD.cardType.equals(cardbrand)) {
				return MASTER_CARD;
			}
			return null;
		}
	}

	public static DailyClearingSummaryReport getInstance(String cardType) throws ClearingSummaryException {
		Logger log = Logger.getLogger(DailyClearingSummaryReport.class);
		if ( log.isDebugEnabled()) {
			log.debug("[getInstance()] -  create instance for cardType="+cardType);
		}
		try {
			ReportType reportType = ReportType.getReportType(cardType);
			if (reportType != null ) {
				return (DailyClearingSummaryReport)Class.forName(reportType.className).newInstance();
			}
			throw new IllegalArgumentException("CardType/Brand "+cardType+" is not supported");
		}
		catch (Exception e) {
			log.error("[getInstance()] - Unexpected exception instance for cardType=" + cardType, e);
			throw new ClearingSummaryException("Exception getting report for "+cardType,e);
		}
	}

	protected MesQueryHandlerResultSet getQueryHandler() {
		return new MesQueryHandlerResultSet();
	}

	/**
	 * Load the clearing summary
	 * @param bankNumber
	 * @param beginDate - report begin date
	 * @param cutOff - based on configuration in TE properties it will be 1 if the begin date is on Monday, otherwise 0
	 */
	public ClearingSummary loadClearingSummary(Date reportBeginDate, int cutOff, int bankNumber) {
		Logger log = Logger.getLogger(getClass().getName());
		ClearingSummary clearingSummary = new ClearingSummary();
		if (log.isDebugEnabled()) {
			log.debug("[loadClearingSummary()] - reportBeginDate=" + reportBeginDate + "  for bankNumber" + bankNumber);
		}
		long startTime = System.currentTimeMillis();
		try {
			// 1. outgoing transactions
			loadFirstPresentment(clearingSummary, reportBeginDate, cutOff, bankNumber);
			// 2. reprocessed rejects and reversals
			loadReprocessedRejects(clearingSummary, reportBeginDate, cutOff, bankNumber);
			// 3. outgoing FP rejects and incoming returns
			loadRejects(clearingSummary, reportBeginDate, cutOff, bankNumber);
			// 4. incoming chargebacks
			loadIncomingChargebacks(clearingSummary, reportBeginDate, cutOff, bankNumber);
			// 5. incoming chargeback reversals
			loadIncomingChargebackReversals(clearingSummary, reportBeginDate, cutOff, bankNumber);
			// 6. second presentments
			loadSecondPresentments(clearingSummary, reportBeginDate, cutOff, bankNumber);
			// 7. second presentment rejects
			loadSecondPresentmentRejects(clearingSummary, reportBeginDate, cutOff, bankNumber);
			// 8. fee collection
			loadFeeCollection(clearingSummary, reportBeginDate, cutOff, bankNumber);
			// reimbursement fees, Association fees
			loadAssocData(clearingSummary, reportBeginDate, cutOff, bankNumber);

		}
		catch (Exception e) {
			log.error(String.format(ERROR_LOG_FORMAT, "loadClearingSummary", e.getMessage()), e);
		}
		finally {
			long stopTime = System.currentTimeMillis();
			log.debug("[loadClearingSummary()] - completed, elapsed time = " + (stopTime - startTime) + " milliseconds");
		}
		return clearingSummary;
	}

	protected abstract void loadFirstPresentment(ClearingSummary clearingSummary, Date reportBeginDate, int cutOff, int bankNumber);

	protected abstract void loadReprocessedRejects(ClearingSummary clearingSummary, Date reportBeginDate, int cutOff, int bankNumber);

	protected abstract void loadRejects(ClearingSummary clearingSummary, Date reportBeginDate, int cutOff, int bankNumber);

	protected abstract void loadIncomingChargebacks(ClearingSummary clearingSummary, Date reportBeginDate, int cutOff, int bankNumber);

	protected abstract void loadIncomingChargebackReversals(ClearingSummary clearingSummary, Date reportBeginDate, int cutOff, int bankNumber);

	protected abstract void loadSecondPresentments(ClearingSummary clearingSummary, Date reportBeginDate, int cutOff, int bankNumber);

	protected abstract void loadSecondPresentmentRejects(ClearingSummary clearingSummary, Date reportBeginDate, int cutOff, int bankNumber);

	protected abstract void loadFeeCollection(ClearingSummary clearingSummary, Date reportBeginDate, int cutOff, int bankNumber);

	protected abstract void loadAssocData(ClearingSummary clearingSummary, Date reportBeginDate, int cutOff, int bankNumber);

	public static void main(String[] args) {
		Logger log = Logger.getLogger(DailyClearingSummaryReport.class);
		try {
			java.sql.Date beginDate = null;
			if (args.length > 0) {
				int bankNumber = Integer.parseInt(args[0]);
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				Calendar start = Calendar.getInstance();
				int cutOff = ((start.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) ? 1 : 0);
				start.setTime(sdf.parse(args[1]));
				beginDate = new java.sql.Date(start.getTime().getTime());
				DailyClearingSummaryReport aMasterCardClearingSummary = new MasterCardClearingSummary();
				aMasterCardClearingSummary.loadClearingSummary(beginDate, cutOff, bankNumber);

			}
			System.exit(0);
		}
		catch (Exception e) {
			log.error(String.format(ERROR_LOG_FORMAT, "DailyClearingSummaryReport.main", e.getMessage()), e);
		}

	}

}
