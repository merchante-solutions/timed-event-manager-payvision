package com.mes.reports;

public class ClearingSummaryException extends Exception {
	ClearingSummaryException(String cardType, Throwable cause) {
		super(String.format("Exception getting report for %s", cardType), cause);
	}
}
