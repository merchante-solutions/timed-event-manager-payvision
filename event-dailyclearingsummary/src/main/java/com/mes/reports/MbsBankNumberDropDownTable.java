/*@lineinfo:filename=MbsBankNumberDropDownTable*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/reports/MbsBankNumberDropDownTable.sqlj $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2011-08-18 12:47:04 -0700 (Thu, 18 Aug 2011) $
  Version            : $Revision: 19154 $

  Change History:
     See SVN database

  Copyright (C) 2009-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.ResultSet;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class MbsBankNumberDropDownTable extends DropDownTable
{
  public MbsBankNumberDropDownTable( long nodeId )
  {
    ResultSetIterator     it          = null;
    ResultSet             resultSet   = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:43^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mb.bank_number        as bank_number,
//                  mb.bank_number || ' - ' || nvl(o.org_name,mb.bank_number)   
//                                        as bank_desc
//          from    t_hierarchy     th,
//                  mbs_banks       mb,
//                  organization    o
//          where   th.hier_type = 1
//                  and th.ancestor = :nodeId
//                  and nvl(mb.processor_id,0) = 1
//                  and (mb.bank_number || '00000') = th.descendent
//                  and o.org_group = th.descendent
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mb.bank_number        as bank_number,\n                mb.bank_number || ' - ' || nvl(o.org_name,mb.bank_number)   \n                                      as bank_desc\n        from    t_hierarchy     th,\n                mbs_banks       mb,\n                organization    o\n        where   th.hier_type = 1\n                and th.ancestor =  :1 \n                and nvl(mb.processor_id,0) = 1\n                and (mb.bank_number || '00000') = th.descendent\n                and o.org_group = th.descendent";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MbsBankNumberDropDownTable",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MbsBankNumberDropDownTable",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:56^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        addElement(resultSet.getString("bank_number"),resultSet.getString("bank_desc"));
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("MbsBankNumberDropDownTable()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/