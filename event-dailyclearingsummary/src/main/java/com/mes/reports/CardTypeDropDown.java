package com.mes.reports;

import com.mes.tools.DropDownTable;

public class CardTypeDropDown extends DropDownTable
{
  public CardTypeDropDown()
  {
    this(true);

  }

  public CardTypeDropDown(boolean addAll) {
    if(addAll) {
      addElement("ALL", "All Card Types");
    }

    addElements();
  }

  private void addElements() {
    addElement("VS", "Visa");
    addElement("MC", "MasterCard");
    addElement("DS", "Discover");
    addElement("AM", "American Express");
  }
}