/*@lineinfo:filename=MbsReconcileDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/reports/MbsReconcileDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2014-03-04 16:04:02 -0800 (Tue, 04 Mar 2014) $
  Version            : $Revision: 22226 $

  Change History:
     See SVN database

  Copyright (C) 2009-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import com.mes.forms.ComboDateField;
import com.mes.forms.DropDownField;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;

public class MbsReconcileDataBean extends ReportSQLJBean
{
  public static final int           RT_FUNDING_SUMMARY    = (RT_USER + 0);
  public static final int           RT_CLEARING_SUMMARY   = (RT_USER + 1);
  public static final int           RT_CARRYOVER_SUMMARY  = (RT_USER + 2);
  
  public static class CarryoverSummary
  {
    protected   Date      BatchDate           = null;
    protected   String    BinNumber           = null;
    protected   String    CardType            = null;
    protected   double    CarryoverAmount     = 0.0;
    protected   long      LoadFileId          = 0L;
    protected   String    LoadFilename        = null;
    protected   Date      SettlementDate      = null;
  
    public CarryoverSummary( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      BinNumber       = resultSet.getString("bin_number");
      CardType        = resultSet.getString("card_type");
      LoadFileId      = resultSet.getLong("load_file_id");
      LoadFilename    = resultSet.getString("load_filename");
      BatchDate       = resultSet.getDate("batch_date");
      SettlementDate  = resultSet.getDate("settlement_date");
      CarryoverAmount = resultSet.getDouble("carryover_amount");
    }
    
    public Date      getBatchDate()         { return( BatchDate );          }
    public String    getBinNumber()         { return( BinNumber );          }
    public String    getCardType()          { return( CardType );           }
    public double    getCarryoverAmount()   { return( CarryoverAmount );    }
    public long      getLoadFileId()        { return( LoadFileId );         }
    public String    getLoadFilename()      { return( LoadFilename );       }
    public Date      getSettlementDate()    { return( SettlementDate );     }
  }
  
  public static class ClearingSummary
  {
    protected   HashMap     BinData           = new HashMap();
    
    public ClearingSummary()
    {
    }

    public void addBinSummary(ResultSet rs) throws SQLException {
      BinSummary bs = getBinSummary(rs.getString("bin_number"));

      bs.setActualAmount(rs.getDouble("actual_amount"));
      bs.setAssocFees(rs.getDouble("assoc_fees"));
      bs.setCashAdvanceAmount(rs.getDouble("cash_advance_amount"));
      bs.setCreditsAmount(rs.getDouble("credits_amount"));
      bs.setFeeCollectionAmount(rs.getDouble("fee_collection_amount"));
      bs.setFundingAmount(rs.getDouble("funding_amount"));
      bs.setFxAdjustmentAmount(rs.getDouble("fx_adjustment_amount"));
      bs.setIncomingChargebacksAmount(rs.getDouble("incoming_chargebacks_amount"));
      bs.setIncomingReversalsAmount(rs.getDouble("incoming_reversals_amount"));
      bs.setReimbursementFees(rs.getDouble("reimbursement_fees"));
      bs.setRejectsAmount(rs.getDouble("rejects_amount"));
      bs.setReprocessedRejectsAmount(rs.getDouble("reprocessed_rejects_amount"));
      bs.setReversalsAmount(rs.getDouble("reversals_amount"));
      bs.setSalesAmount(rs.getDouble("sales_amount"));
      bs.setSecondPresentmentsAmount(rs.getDouble("second_presentments_amount"));
      bs.setSecondPresentmentRejectsAmount(rs.getDouble("second_presentment_rejects_amt"));
    }
    
    public void addIncomingReversals( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      BinSummary    bs    = getBinSummary(resultSet.getString("bin_number"));
      
      bs.setIncomingReversalsAmount ( resultSet.getDouble("reversal_amount")  );
      bs.setFxAdjustmentAmount      ( resultSet.getDouble("fx_adj_amount")    );
    }
    
    public void addFirstPresentments( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      BinSummary    bs    = getBinSummary(resultSet.getString("bin_number"));
      
      bs.setSalesAmount      ( resultSet.getDouble("sales_amount")   );
      bs.setCreditsAmount    ( resultSet.getDouble("credits_amount") );
      bs.setCashAdvanceAmount( resultSet.getDouble("ca_amount")      );
      bs.setFundingAmount    ( resultSet.getDouble("funding_amount") );
    }      
    
    public void addReprocessedRejects( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      BinSummary    bs    = getBinSummary(resultSet.getString("bin_number"));
      
      bs.setReprocessedRejectsAmount( resultSet.getDouble("reject_amount")   );
      bs.setReversalsAmount         ( resultSet.getDouble("reversal_amount") );
    }
    
    public void addRejects( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      BinSummary    bs    = getBinSummary(resultSet.getString("bin_number"));
      
      bs.setRejectsAmount( resultSet.getDouble("reject_amount") );
    }
    
    public void addIncomingChargebacks( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      BinSummary    bs    = getBinSummary(resultSet.getString("bin_number"));
      
      bs.setIncomingChargebacksAmount( resultSet.getDouble("cb_amount") );
    }
    
    public void addSecondPresentments( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      BinSummary    bs    = getBinSummary(resultSet.getString("bin_number"));
      
      bs.setSecondPresentmentsAmount( resultSet.getDouble("sp_amount") );
    }
    
    public void addSecondPresentmentRejects( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      BinSummary    bs    = getBinSummary(resultSet.getString("bin_number"));
      
      bs.setSecondPresentmentRejectsAmount( resultSet.getDouble("reject_amount") );
    }
    
    public void addFeeCollection( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      BinSummary    bs    = getBinSummary(resultSet.getString("bin_number"));
      
      bs.setFeeCollectionAmount( resultSet.getDouble("fees_amount") );
    }
    
    public void addAssocData( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      BinSummary    bs    = getBinSummary(resultSet.getString("bin_number"));
      
      bs.setReimbursementFees( resultSet.getDouble("reimbursement_fees") );
      bs.setAssocFees        ( resultSet.getDouble("assoc_fees")         );
      bs.setActualAmount     ( resultSet.getDouble("total_amount")       );
    }
    
    public HashMap getBinData()
    {
      return( BinData );
    }
    
    public BinSummary getBinSummary( String binNumber )
    {
      BinSummary      bs        = (BinSummary)BinData.get(binNumber);
      
      if ( bs == null )
      {
        bs = new BinSummary();
        BinData.put(binNumber,bs);
      }
      return( bs );
    }
    
    public boolean hasFxFundingVariance()
    {
      BinSummary    binSummary    = null;
      HashMap       map           = getBinData();
      boolean       retVal        = false;
      double        variance      = 0.0;
      
      for( Iterator it = map.keySet().iterator(); it.hasNext(); )
      {
        String key  = (String)it.next();
        binSummary  = (BinSummary)map.get(key);
        
        variance    =   binSummary.getSalesAmount()
                      - binSummary.getCreditsAmount()
                      + binSummary.getCashAdvanceAmount()
                      - binSummary.getFundingAmount();
                      
        if ( !"$0.00".equals(MesMath.toCurrency(variance)) )
        {
          retVal = true;
          break;
        }
      }
      return( retVal );
    }
  }
  
  public static class BinSummary
  {
    protected   String      BinNumber                       = null;
    protected   double      SalesAmount                     = 0.0;
    protected   double      CreditsAmount                   = 0.0;
    protected   double      CashAdvanceAmount               = 0.0;
    protected   double      ReprocessedRejectsAmount        = 0.0;
    protected   double      ReversalsAmount                 = 0.0;
    protected   double      RejectsAmount                   = 0.0;
    protected   double      IncomingReversalsAmount         = 0.0;
    protected   double      FundingAmount                   = 0.0;
    protected   double      FxAdjustmentAmount              = 0.0;
    protected   double      IncomingChargebacksAmount       = 0.0;
    protected   double      SecondPresentmentsAmount        = 0.0;
    protected   double      SecondPresentmentRejectsAmount  = 0.0;
    protected   double      ReimbursementFees               = 0.0;
    protected   double      AssocFees                       = 0.0;
    protected   double      FeeCollectionAmount             = 0.0;
    protected   double      ActualAmount                    = 0.0;
  
    public BinSummary()
    {
    }
    
    public  double getSalesAmount()                             { return( SalesAmount );                }
    public  void   setSalesAmount( double value )               { SalesAmount = value;                  }
    
    public  double getCreditsAmount()                           { return( CreditsAmount );              }
    public  void   setCreditsAmount( double value )             { CreditsAmount = value;                }
    
    public  double getCashAdvanceAmount()                       { return( CashAdvanceAmount );          }
    public  void   setCashAdvanceAmount( double value )         { CashAdvanceAmount = value;            }
    
    public  double getFundingAmount()                           { return( FundingAmount );              }
    public  void   setFundingAmount( double value )             { FundingAmount = value;                }
    
    public  double getReprocessedRejectsAmount()                { return( ReprocessedRejectsAmount );   }
    public  void   setReprocessedRejectsAmount( double value )  { ReprocessedRejectsAmount = value;     }
    
    public  double getReversalsAmount()                         { return( ReversalsAmount );            }
    public  void   setReversalsAmount( double value )           { ReversalsAmount = value;              }
    
    public  double getRejectsAmount()                           { return( RejectsAmount );              }
    public  void   setRejectsAmount( double value )             { RejectsAmount = value;                }
    
    public  double getIncomingChargebacksAmount()               { return( IncomingChargebacksAmount );  }
    public  void   setIncomingChargebacksAmount( double value ) { IncomingChargebacksAmount = value;    }
    
    public  double getIncomingReversalsAmount()                 { return( IncomingReversalsAmount );    }
    public  void   setIncomingReversalsAmount( double value )   { IncomingReversalsAmount = value;      }
    
    public  double getFxAdjustmentAmount()                      { return( FxAdjustmentAmount );         }
    public  void   setFxAdjustmentAmount( double value )        { FxAdjustmentAmount = value;           }
    
    public  double getSecondPresentmentsAmount()                { return( SecondPresentmentsAmount );   }
    public  void   setSecondPresentmentsAmount( double value )  { SecondPresentmentsAmount = value;     }
    
    public  double getSecondPresentmentRejectsAmount()          { return( SecondPresentmentRejectsAmount );     }
    public  void   setSecondPresentmentRejectsAmount( double value ) { SecondPresentmentRejectsAmount = value;  }
    
    public  double getReimbursementFees()                       { return( ReimbursementFees );          }
    public  void   setReimbursementFees( double value )         { ReimbursementFees = value;            }
    
    public  double getAssocFees()                               { return( AssocFees );                  }
    public  void   setAssocFees( double value )                 { AssocFees = value;                    }
    
    public  double getFeeCollectionAmount()                     { return( FeeCollectionAmount );        }
    public  void   setFeeCollectionAmount( double value )       { FeeCollectionAmount = value;          }
    
    public  double getActualAmount()                            { return( ActualAmount );               }
    public  void   setActualAmount( double value )              { ActualAmount = value;                 }
  }
  
  public static class AtdData 
  {
    public  double        AmexDepAmount     = 0.0;
    public  double        MerchDepAmount    = 0.0;
    public  double[]      ReleasedAmount    = new double[2];
    public  double        TotalAmount       = 0.0;
    public  double        TotalAmountATS    = 0.0;
    
    public AtdData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      TotalAmountATS    = resultSet.getDouble("total_amount");
      
      String[] prefixes = new String[] { "std","amex" };
      for ( int i = 0; i < prefixes.length; ++i )
      {
        ReleasedAmount[i] = resultSet.getDouble(prefixes[i] + "_released_amount");
      }        
    }
    
    public void addFileTotals( ResultSet resultSet )
      throws java.sql.SQLException
    {
      TotalAmount     = resultSet.getDouble("total_amount");
      AmexDepAmount   = resultSet.getDouble("amex_dep_amount");
      MerchDepAmount  = resultSet.getDouble("merch_dep_amount");
    }
  }
  
  public static class AtsData 
  {
    public double[]   SuspendedAmount   = new double[2];
    public double[]   GrossAmount       = new double[2];
    public double[]   DailyDiscIcAmount = new double[2];
    public double[]   DisqualAmount     = new double[2];
    public double[]   NetAmount         = new double[2];
    
    public AtsData( ResultSet resultSet )
      throws java.sql.SQLException
    {
    
      String[] prefixes = new String[] { "std","amex" };
      for ( int i = 0; i < prefixes.length; ++i )
      {
        GrossAmount[i]        = resultSet.getDouble(prefixes[i] + "_gross_amount");
        DailyDiscIcAmount[i]  = resultSet.getDouble(prefixes[i] + "_disc_ic_amount");
        SuspendedAmount[i]    = resultSet.getDouble(prefixes[i] + "_suspended_amount");
        DisqualAmount[i]      = resultSet.getDouble(prefixes[i] + "_disqual_amount");
        NetAmount[i]          = resultSet.getDouble(prefixes[i] + "_net_amount");
      }
    }
  }

  public static class DdfData 
  {
    public double     AmexAmount                = 0.0;
    public double     AmexAmountSabre           = 0.0;
    public double     BmlAmount                 = 0.0;
    public double     DebitAmount               = 0.0;
    public double     DinersAmount              = 0.0;
    public double     DiscoverAmount            = 0.0;
    public double     FxMarkupAmount            = 0.0;
    public double     FxMarkupAmountAmex        = 0.0;
    public double     JcbAmount                 = 0.0;
    public double     MasterCardAmount          = 0.0;
    public double     OtherAmount               = 0.0;
    public double     RejectsAmount             = 0.0;
    public double     RejectsAmountAmex         = 0.0;
    public double     TotalAmount               = 0.0;
    public double     TotalAmountAmex           = 0.0;
    public double     UatpAmount                = 0.0;
    public double     VisaAmount                = 0.0;
    
    public DdfData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      TotalAmount               = resultSet.getDouble("total_amount");
      VisaAmount                = resultSet.getDouble("vs_amount");
      MasterCardAmount          = resultSet.getDouble("mc_amount");
      DebitAmount               = resultSet.getDouble("db_amount");
      AmexAmount                = resultSet.getDouble("am_amount");
      AmexAmountSabre           = resultSet.getDouble("am_amount_sabre");
      DiscoverAmount            = resultSet.getDouble("ds_amount");
      DinersAmount              = resultSet.getDouble("dn_amount");
      JcbAmount                 = resultSet.getDouble("jc_amount");
      UatpAmount                = resultSet.getDouble("ot_amount_sabre");
      OtherAmount               = resultSet.getDouble("ot_amount");
      BmlAmount                 = resultSet.getDouble("bml_amount");
      RejectsAmount             = resultSet.getDouble("rejects_amount");
      FxMarkupAmount            = resultSet.getDouble("fx_markup_amount");
      FxMarkupAmountAmex        = resultSet.getDouble("fx_markup_amount_amex");
    }
    
    public void setRejectsAmountAmex( double amexRejectsAmount )
      throws java.sql.SQLException
    {
      RejectsAmountAmex = amexRejectsAmount;
      TotalAmountAmex   = (AmexAmount - RejectsAmountAmex);
    }
  }
  
  public static class FundingSummary
  {
    private   AtdData       AtdTotals     = null;
    private   AtsData       AtsTotals     = null;
    private   DdfData       DdfTotals     = null;
    
    public FundingSummary()
    {
    }
    public void addAtdData( AtdData data ) { AtdTotals = data; } 
    public void addAtsData( AtsData data ) { AtsTotals = data; } 
    public void addDdfData( DdfData data ) { DdfTotals = data; } 
    
    public AtdData  getAtdData()            { return( AtdTotals ); } 
    
    public double   getVisaAmount()           { return( DdfTotals.VisaAmount );                       }
    public double   getMasterCardAmount()     { return( DdfTotals.MasterCardAmount );                 }
    public double   getDebitAmount()          { return( DdfTotals.DebitAmount );                      }
    public double   getAmexAmount()           { return( DdfTotals.AmexAmount );                       }
    public double   getAmexAmountSabre()      { return( DdfTotals.AmexAmountSabre );                  }
    public double   getDiscoverAmount()       { return( DdfTotals.DiscoverAmount );                   }
    public double   getOtherAmount()          { return( DdfTotals.OtherAmount );                      }
    public double   getUatpAmount()           { return( DdfTotals.UatpAmount );                       }
    public double   getBmlAmount()            { return( DdfTotals.BmlAmount );                        }
    public double   getTotalSettledAmount()   { return( DdfTotals.TotalAmount );                      }
    public double   getTotalSettledAmountAmex(){return( DdfTotals.TotalAmountAmex );                  }
    public double   getRejectsAmount()        { return( DdfTotals.RejectsAmount );                    }
    public double   getRejectsAmountAmex()    { return( DdfTotals.RejectsAmountAmex );                }
    public double   getFxMarkupAmount()       { return( DdfTotals.FxMarkupAmount );                   }
    public double   getFxMarkupAmountAmex()   { return( DdfTotals.FxMarkupAmountAmex );               }
    public double   getGrossFundedAmount()    { return( getTotalAmount(AtsTotals.GrossAmount) );      }
    public double   getGrossFundedAmountStd() { return( AtsTotals.GrossAmount[0] );                   }
    public double   getGrossFundedAmountAmex(){ return( AtsTotals.GrossAmount[1] );                   }
    public double   getDailyDiscIcAmount()    { return( getTotalAmount(AtsTotals.DailyDiscIcAmount) );}
    public double   getDailyDiscIcAmountStd() { return( AtsTotals.DailyDiscIcAmount[0] );             }
    public double   getDailyDiscIcAmountAmex(){ return( AtsTotals.DailyDiscIcAmount[1] );             }
    public double   getNetFundedAmount()      { return( getTotalAmount(AtsTotals.NetAmount) );        }
    public double   getNetFundedAmountStd()   { return( AtsTotals.NetAmount[0] );                     }
    public double   getNetFundedAmountAmex()  { return( AtsTotals.NetAmount[1] );                     }
    public double   getSuspendedAmount()      { return( getTotalAmount(AtsTotals.SuspendedAmount) );  }
    public double   getSuspendedAmountStd()   { return( AtsTotals.SuspendedAmount[0] );               }
    public double   getSuspendedAmountAmex()  { return( AtsTotals.SuspendedAmount[1] );               }
    public double   getDisqualAmount()        { return( getTotalAmount(AtsTotals.DisqualAmount) );    }
    public double   getDisqualAmountStd()     { return( AtsTotals.DisqualAmount[0] );                 }
    public double   getDisqualAmountAmex()    { return( AtsTotals.DisqualAmount[1] );                 }
    public double   getReleasedAmount()       { return( getTotalAmount(AtdTotals.ReleasedAmount) );   }
    public double   getReleasedAmountStd()    { return( AtdTotals.ReleasedAmount[0] );                }
    public double   getReleasedAmountAmex()   { return( AtdTotals.ReleasedAmount[1] );                }
    public double   getAmexDepAmount()        { return( AtdTotals.AmexDepAmount );                    }
    public double   getMerchDepAmount()       { return( AtdTotals.MerchDepAmount );                   }
    public double   getTotalDepAmount()       { return( AtdTotals.TotalAmount );                      }
    
    
    public double getTotalAmount( double[] values )
    {
      double  retVal    = 0.0;
      
      for( int i = 0; i < values.length; ++i )
      {
        retVal += values[i];
      }
      return( retVal );
    }
    
    public boolean hasData()
    {
      // add up every possible value to determine if this object has data
      double hashAmount =   getVisaAmount()             + getMasterCardAmount()
                          + getDebitAmount()            + getAmexAmount()
                          + getAmexAmountSabre()        + getDiscoverAmount()
                          + getOtherAmount()            + getUatpAmount()
                          + getBmlAmount()              + getTotalSettledAmount()
                          + getTotalSettledAmountAmex() + getRejectsAmount()
                          + getRejectsAmountAmex()      + getGrossFundedAmount()
                          + getGrossFundedAmountStd()   + getGrossFundedAmountAmex()
                          + getDailyDiscIcAmount()      + getDailyDiscIcAmountStd()
                          + getDailyDiscIcAmountAmex()  + getNetFundedAmount()
                          + getNetFundedAmountStd()     + getNetFundedAmountAmex()
                          + getSuspendedAmount()        + getSuspendedAmountStd()
                          + getSuspendedAmountAmex()    + getDisqualAmount()
                          + getDisqualAmountStd()       + getDisqualAmountAmex()
                          + getReleasedAmount()         + getReleasedAmountStd()
                          + getReleasedAmountAmex()     + getAmexDepAmount()
                          + getMerchDepAmount()         + getTotalDepAmount();
                          
      return ( !"$0.00".equals(MesMath.toCurrency(hashAmount)) );
    }
  }
  
  
  public static class AchFileData
  {
    public    int         AchCount          = 0;
    public    double      AchDiscIc         = 0.0;
    public    Date        AchFileDate       = null;
    public    long        AchFileId         = 0L;
    public    String      AchFilename       = null;
    public    double      AchGross          = 0.0;
    public    double      AchNet            = 0.0;
  
    public AchFileData( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      AchFileDate     = resultSet.getDate   ("ach_file_date");
      AchFileId       = resultSet.getLong   ("ach_file_id");
      AchFilename     = resultSet.getString ("ach_filename");
      AchCount        = resultSet.getInt    ("ach_count");
      AchGross        = resultSet.getDouble ("ach_gross");
      AchDiscIc       = resultSet.getDouble ("ach_daily_disc_ic");
      AchNet          = resultSet.getDouble ("ach_net");
    }
    
    public String getAchFilename()
    {
      return( AchFilename == null ? "" : AchFilename );
    }
    
    public String getAchFileDate()
    {
      String    retVal    = "";
      if ( AchFileDate == null ) 
      {
        retVal = ((AchFilename == null) ? "Pending" : "");
      }
      else
      {
        retVal = DateTimeFormatter.getFormattedDate(AchFileDate,"MM/dd/yyyy");
      }
      return( retVal );
    }
  }

  public static class DepositData
  {
    public    int         AchCount          = 0;
    public    double      AchDiscIc         = 0.0;
    public    long        AchFileId         = 0L;
    public    double      AchGross          = 0.0;
    public    double      AchNet            = 0.0;
    public    Date        AchRunDate        = null;
    public    boolean     Balanced          = false;
    public    Date        BatchDate         = null;
    public    int         DepositCount      = 0;
    public    double      DepositDiscIc     = 0.0;
    public    double      DepositGross      = 0.0;
    public    double      DepositNet        = 0.0;
  
    public DepositData( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      AchFileId       = resultSet.getLong   ("ach_file_id");
      AchRunDate      = resultSet.getDate   ("ach_run_date");
      BatchDate       = resultSet.getDate   ("batch_date");
      DepositCount    = resultSet.getInt    ("deposit_count");
      DepositGross    = resultSet.getDouble ("deposit_gross");
      DepositDiscIc   = resultSet.getDouble ("deposit_disc_ic");
      DepositNet      = resultSet.getDouble ("deposit_net");
      AchCount        = resultSet.getInt    ("ach_count");
      AchGross        = resultSet.getDouble ("ach_gross");
      AchDiscIc       = resultSet.getDouble ("ach_disc_ic");
      AchNet          = resultSet.getDouble ("ach_net");
      Balanced        = "Y".equals(resultSet.getString("in_balance"));
    }
    
    public String getAchRunDateDisplay()
    {
      String    retVal    = "";
      if ( AchRunDate == null ) 
      {
        retVal = "Pending";
      }
      else
      {
        retVal = DateTimeFormatter.getFormattedDate(AchRunDate,"MM/dd/yyyy");
      }
      return( retVal );
    }
    
    public void setAchFileId( long value ) { AchFileId = value; } 
  }

  public class DetailData
  {
    public    boolean     Balanced        = false;
    public    Date        BatchDate       = null;
    public    String      CardType        = null; 
    public    int         ClearedCount    = 0;
    public    double      ClearedAmount   = 0.0;
    public    int         CurrentCount    = 0;
    public    double      CurrentAmount   = 0.0;
    public    int         CarryOverCount  = 0;
    public    double      CarryOverAmount = 0.0;
    public    long        LoadFileId      = 0L;
    public    String      LoadFilename    = null;
    public    int         SettledCount    = 0;
    public    double      SettledAmount   = 0.0;
    
    public DetailData( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      Balanced        = "Y".equals(resultSet.getString("in_balance"));
      CardType        = resultSet.getString ("card_type");
      CurrentCount    = resultSet.getInt    ("current_count");
      CurrentAmount   = resultSet.getDouble ("current_amount");
      CarryOverCount  = resultSet.getInt    ("carry_over_count");
      CarryOverAmount = resultSet.getDouble ("carry_over_amount");
      ClearedCount    = resultSet.getInt    ("cleared_count");
      ClearedAmount   = resultSet.getDouble ("cleared_amount");
      SettledCount    = resultSet.getInt    ("settled_count");
      SettledAmount   = resultSet.getDouble ("settled_amount");
      LoadFilename    = resultSet.getString ("load_filename");
      LoadFileId      = resultSet.getLong   ("load_file_id");
      BatchDate       = resultSet.getDate   ("batch_date");
    }      
  }
  
  public class ReconciliationEntry
  {
    public double     Amount        = 0.0;
    public int        Count         = 0;
    public String     EntryDesc     = null;
    
    public ReconciliationEntry( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      Amount    = resultSet.getDouble("amount");
      Count     = resultSet.getInt("count");
      EntryDesc = resultSet.getString("entry_desc");
    }
  }
  
  public class SummaryData
  {
    public    Date          BatchDate       = null;
    public    double        CashAdvAmount   = 0.0;
    public    int           CashAdvCount    = 0;
    public    double        CreditsAmount   = 0.0;
    public    int           CreditsCount    = 0;
    public    Timestamp     FileTS          = null;
    public    long          LoadFileId      = 0L;
    public    String        LoadFilename    = null;
    public    double        NetAmount       = 0.0;
    public    double        SalesAmount     = 0.0;
    public    int           SalesCount      = 0;
    public    int           TranCount       = 0;
    
    public SummaryData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      BatchDate       = resultSet.getDate("batch_date");
      LoadFileId      = resultSet.getLong("load_file_id");
      LoadFilename    = resultSet.getString("load_filename");
      FileTS          = resultSet.getTimestamp("file_ts");
      TranCount       = resultSet.getInt("record_count");
      NetAmount       = resultSet.getDouble("net_amount");
      SalesCount      = resultSet.getInt    ("sales_count");
      SalesAmount     = resultSet.getDouble ("sales_amount");
      CashAdvCount    = resultSet.getInt    ("ca_count");
      CashAdvAmount   = resultSet.getDouble ("ca_amount");
      CreditsCount    = resultSet.getInt    ("credits_count");
      CreditsAmount   = resultSet.getDouble ("credits_amount");
    }      
  }
  
  public MbsReconcileDataBean( )
  {
    super(true);   // enable field bean support
  }
  
  protected void createFields(HttpServletRequest request)
  {
    DropDownField   dropDownField     = null;
    
    try
    {
      super.createFields(request);
    
      FieldGroup fgroup = (FieldGroup)getField("searchFields");
      fgroup.deleteField("portfolioId");
      fgroup.deleteField("zip2Node");
      int summaryReportType = HttpHelper.getInt(request,"reportType",RT_SUMMARY);
      if (summaryReportType  == RT_FUNDING_SUMMARY)
      {
         hideSearchField("endDate");

      }

      fgroup.add(new DropDownField( "cardType","Card Type", new CardTypeDropDown(false), false ));
      
      switch(summaryReportType)
      {
        case RT_FUNDING_SUMMARY:
        case RT_CLEARING_SUMMARY:
        case RT_CARRYOVER_SUMMARY:
        case RT_SUMMARY:
          fgroup.add(new DropDownField( "bankNumber","Bank Number", new MbsBankNumberDropDownTable(getReportHierarchyNode()), true ));
          break;
          
        default:
          fgroup.add( new HiddenField("bankNumber") );
          break;          
      }
      
      fgroup.add( new HiddenField("merchantId") );
      fgroup.add( new HiddenField("lfid") );
      fgroup.add( new HiddenField("amexOnly") );
      fgroup.add( new HiddenField("sixDayFunding") );

      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.DAY_OF_MONTH,-1);
      setReportDateEnd(new java.sql.Date( cal.getTime().getTime() ));
  }
    catch( Exception e )
    {
      logEntry("createFields()",e.toString());
    }
    finally
    {
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    int     reportType      = getReportType();
    
    line.setLength(0);
    if ( reportType == RT_DETAILS )
    {
    }
    else if ( reportType == RT_FUNDING_SUMMARY )
    {
    }
    else if ( reportType == RT_CARRYOVER_SUMMARY )
    {
      line.append("\"Card Type\",");
      line.append("\"BIN Number\",");
      line.append("\"Load Filename\",");
      line.append("\"Batch Date\",");
      line.append("\"Clearing Date\",");
      line.append("\"Carryover Amount\"");
    }
    else if( reportType == RT_CLEARING_SUMMARY )
    {
    }
    else    // RT_SUMMARY
    {
    }      
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    int     reportType    = getReportType();
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    
    if ( reportType == RT_DETAILS )
    {
    }
    else if ( reportType == RT_FUNDING_SUMMARY )
    {
      FundingSummary entry = (FundingSummary)obj;
      
      // calculate some totals
      double    achOutgoing   = 0.0;
      double    variance      = 0.0;
      String[]  names         = null;
      double[]  values        = null;
    
      if ( getBoolean("amexOnly") )
      {
        achOutgoing   = entry.getNetFundedAmountAmex() 
                        - entry.getSuspendedAmountAmex()
                        - entry.getDisqualAmountAmex()
                        + entry.getReleasedAmountAmex();
        variance      = achOutgoing - entry.getAmexDepAmount();
    
        names  = new String[]
        {
          "American Express"                    , 
          "MBS Rejects"                         , 
          "FX Markup"                           , 
          "Gross Settled (DDF)"                 , 
          "Gross Funded (ATS)"                  , 
          "Daily Discount/IC (ATS)"             , 
          "Net Funded (ATS)"                    , 
          "Suspended Funds (ATS)"               , 
          "Same Day Disqualified Funds (ATS)"   , 
          "Released Funds (ATS)"                , 
          "ACH Outgoing"                        , 
          "**SPACER**"                          ,
          "Total Funding (ATD)"                 ,
          "**SPACER**"                          ,
        };

        values = new double[]
        {
          entry.getAmexAmount()             ,
          -entry.getRejectsAmountAmex()     ,
          -entry.getFxMarkupAmountAmex()    ,
          entry.getTotalSettledAmountAmex() ,
          entry.getGrossFundedAmountAmex()  ,
          -entry.getDailyDiscIcAmountAmex() ,
          entry.getNetFundedAmountAmex()    ,
          -entry.getSuspendedAmountAmex()   ,
          -entry.getDisqualAmountAmex()     ,
          entry.getReleasedAmountAmex()     ,
          achOutgoing                       ,
          0.0                               ,   // spacer
          entry.getAmexDepAmount()          ,
          0.0                               ,   // spacer
        };
      }
      else    // show all card types
      {
        achOutgoing   = entry.getNetFundedAmount() 
                        - entry.getSuspendedAmountStd()
                        - entry.getSuspendedAmountAmex()
                        - entry.getDisqualAmount()
                        + entry.getReleasedAmountStd()
                        + entry.getReleasedAmountAmex();
        variance      = achOutgoing - entry.getTotalDepAmount();
    
        names  = new String[]
        {
          "Visa"                                , 
          "MasterCard"                          , 
          "Debit"                               , 
          "American Express"                    , 
          "Discover"                            , 
          "Other"                               , 
          "Sabre American Express"              , 
          "Sabre UATP"                          , 
          "MBS Rejects"                         , 
          "FX Amount"                           , 
          "Gross Settled (DDF)"                 , 
          "Gross Funded (ATS)"                  , 
          "Daily Discount/IC (ATS)"             , 
          "Net Funded (ATS)"                    , 
          "Suspended Funds Standard (ATS)"      , 
          "Suspended Funds Amex (ATS)"          , 
          "Same Day Disqualified Funds (ATS)"   , 
          "Released Funds Standard (ATS)"       , 
          "Released Funds Amex (ATS)"           , 
          "ACH Outgoing"                        , 
          "**SPACER**"                          ,
          "Amex Funding (ATD)"                  ,
          "Standard Funding (ATD)"              ,
          "Total Funding (ATD)"                 ,
          "**SPACER**"                          ,
        };

        values = new double[]
        {
          entry.getVisaAmount()           ,
          entry.getMasterCardAmount()     ,
          entry.getDebitAmount()          ,
          entry.getAmexAmount()           ,
          entry.getDiscoverAmount()       ,
          entry.getOtherAmount()          ,
          entry.getAmexAmountSabre()      ,
          entry.getUatpAmount()           ,
          -entry.getRejectsAmount()       ,
          -entry.getFxMarkupAmount()      ,
          entry.getTotalSettledAmount()   ,
          entry.getGrossFundedAmount()    ,
          -entry.getDailyDiscIcAmount()   ,
          entry.getNetFundedAmount()      ,
          -entry.getSuspendedAmountStd()  ,
          -entry.getSuspendedAmountAmex() ,
          -entry.getDisqualAmount()       ,
          entry.getReleasedAmountStd()    ,
          entry.getReleasedAmountAmex()   ,
          achOutgoing                     ,
          0.0                             ,   // spacer
          entry.getAmexDepAmount()        ,
          entry.getMerchDepAmount()       ,
          entry.getTotalDepAmount()       ,
          0.0                             ,   // spacer
        };
      }
      
      for( int i = 0; i < names.length; ++i )
      {
        if ( "**SPACER**".equals(names[i]) )
        {
          line.append(",");
        }
        else
        {
          line.append("\"");
          line.append(names[i]);
          line.append("\",");
          line.append(values[i]);
        }
        line.append(System.getProperty("line.separator"));
      }
    }
    else if ( reportType == RT_CARRYOVER_SUMMARY )
    {
      CarryoverSummary entry = (CarryoverSummary)obj;
      
      line.append("\"");
      line.append(entry.getCardType());
      line.append("\",\"");
      line.append(entry.getBinNumber());
      line.append("\",\"");
      line.append(entry.getLoadFilename());
      line.append("\",");
      line.append(DateTimeFormatter.getFormattedDate(entry.getBatchDate(),"MM/dd/yyyy"));
      line.append(",");
      line.append(DateTimeFormatter.getFormattedDate(entry.getSettlementDate(),"MM/dd/yyyy"));
      line.append(",");
      line.append(entry.getCarryoverAmount());
    }
    else if ( reportType == RT_CLEARING_SUMMARY )
    {
      ClearingSummary entry = (ClearingSummary)obj;
      
      Map binMap = entry.getBinData();
      BinSummary binSummary = null;
      
      for( Iterator it = binMap.keySet().iterator(); it.hasNext(); )
      {
        String binKey = (String)it.next();
        binSummary = (BinSummary)binMap.get(binKey);
        
        double outgoingAmount = binSummary.getSalesAmount() 
                                - binSummary.getCreditsAmount()
                                + binSummary.getCashAdvanceAmount()
                                + binSummary.getReprocessedRejectsAmount()
                                - binSummary.getReversalsAmount()
                                - binSummary.getRejectsAmount()
                                + binSummary.getIncomingReversalsAmount()
                                + binSummary.getFxAdjustmentAmount()
                                - binSummary.getIncomingChargebacksAmount()
                                + binSummary.getSecondPresentmentsAmount()
                                - binSummary.getSecondPresentmentRejectsAmount()
                                + binSummary.getFeeCollectionAmount()
                                + binSummary.getReimbursementFees()
                                + binSummary.getAssocFees();
        
        // encode this BIN's data
        line.append("\"BIN #\",\"");
        line.append(binKey);
        line.append("\"\n");

        line.append("\"Date\",\"");
        line.append(DateTimeFormatter.getFormattedDate(getReportDateBegin(),"MM/dd/yyyy"));
        line.append("\"\n");

        line.append("\"Card Type\",\"");
        line.append(getData("cardType"));
        line.append("\"\n");

        line.append("\"Sales\",\"");
        line.append(binSummary.getSalesAmount());
        line.append("\"\n");

        line.append("\"Credits\",\"");
        line.append(-binSummary.getCreditsAmount());
        line.append("\"\n");

        line.append("\"Cash Advance\",\"");
        line.append(binSummary.getCashAdvanceAmount());
        line.append("\"\n");

        line.append("\"Outgoing Total\",\"");
        line.append(binSummary.getSalesAmount() - binSummary.getCreditsAmount() + binSummary.getCashAdvanceAmount());
        line.append("\"\n");

        line.append("\"Reprocessed Rejects\",\"");
        line.append(binSummary.getReprocessedRejectsAmount());
        line.append("\"\n");

        line.append("\"Outgoing Reversals\",\"");
        line.append(-binSummary.getReversalsAmount());
        line.append("\"\n");

        line.append("\"Rejects\",\"");
        line.append(-binSummary.getRejectsAmount());
        line.append("\"\n");

        line.append("\"Incoming Reversals\",\"");
        line.append(binSummary.getIncomingReversalsAmount());
        line.append("\"\n");

        line.append("\"FX Adjustment\",\"");
        line.append(binSummary.getFxAdjustmentAmount());
        line.append("\"\n");

        line.append("\"Incoming Chargebacks\",\"");
        line.append(-binSummary.getIncomingChargebacksAmount());
        line.append("\"\n");

        line.append("\"Second Presentments\",\"");
        line.append(binSummary.getSecondPresentmentsAmount());
        line.append("\"\n");

        line.append("\"Second Presentment Rejects\",\"");
        line.append(-binSummary.getSecondPresentmentRejectsAmount());
        line.append("\"\n");

        line.append("\"Fee Collection\",\"");
        line.append(binSummary.getFeeCollectionAmount());
        line.append("\"\n");

        line.append("\"Reimbursement Fees (Association Report)\",\"");
        line.append(binSummary.getReimbursementFees());
        line.append("\"\n");

        line.append("\"Fees (Association Report)\",\"");
        line.append(binSummary.getAssocFees());
        line.append("\"\n");

        line.append("\"Calculated\",\"");
        line.append(outgoingAmount);
        line.append("\"\n");

        line.append("\"Actual (Association Report)\",\"");
        line.append(binSummary.getActualAmount());
        line.append("\"\n");

        line.append("\"Variance\",\"");
        line.append( MesMath.round(outgoingAmount - binSummary.getActualAmount(),2) );
        line.append("\"\n");

      }
    }
    else    // RT_SUMMARY
    {
    }
  }
  
  public String getDownloadFilenameBase()
  {
    Date            beginDate       = getReportDateBegin();
    Date            endDate         = getReportDateBegin();
    StringBuffer    filename        = new StringBuffer("");
    int             reportType      = getReportType();
    java.util.Date  settlementDate  = null;
    
    if ( reportType == RT_CARRYOVER_SUMMARY )
    {
      filename.append("mbs_carryover_summary_");
    }
    else if ( reportType == RT_DETAILS )
    {
      filename.append("mbs_recon_details_");
    }
    else  // RT_SUMMARY
    {
      filename.append("mbs_recon_summary_");
    }
    
    if ( settlementDate != null ) // use the settlement date in the filename
    {
      filename.append( DateTimeFormatter.getFormattedDate( settlementDate, "MMddyyyy" ) );
    }
    else      // use std report begin/end dates
    {
      // build the first date into the filename
      filename.append( DateTimeFormatter.getFormattedDate( beginDate, "MMddyyyy" ) );
      if ( !beginDate.equals(endDate) )
      {
        filename.append("_to_");
        filename.append( DateTimeFormatter.getFormattedDate( endDate, "MMddyyyy" ) );
      }      
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadAchFileData( )
  {
    loadAchFileData(getLong("lfid"));
  }
  
  public void loadAchFileData( long loadFileId )
  {
    int                   bankNumber      = getInt("bankNumber", ReportUserBean.getBankNumber());
    ResultSetIterator     it              = null;
    ResultSet             resultSet       = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
    
      /*@lineinfo:generated-code*//*@lineinfo:1122^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (ad idx_ach_trident_dtl_trace_date) */
//                  decode(ad.disqualified,'Y','disqualified',ad.load_filename)
//                                                    as ach_filename,
//                  ad.load_file_id                   as ach_file_id,
//                  ad.transmission_date              as ach_file_date,
//                  count(1)                          as ach_count,
//                  sum( ats.sales_amount - 
//                       ats.credits_amount )         as ach_gross,
//                  sum( ats.daily_discount_amount + 
//                       ats.daily_ic_amount )        as ach_daily_disc_ic,
//                  sum( decode(substr(ats.transaction_code,-1),'7',-1,1) *
//                       ats.ach_amount )             as ach_net
//          from    ach_trident_statement   ats,
//                  ach_trident_detail      ad,
//                  mif                     mf
//          where   ats.load_file_id = :loadFileId
//                  and ad.trace_number = ats.trace_number
//                  --and ad.post_date = ats.post_date_actual
//                  and mf.merchant_number = ad.merchant_number
//                  and 
//                  (
//                    (:bankNumber = 3943 and mf.bank_number = 3943 and mf.wf_conversion_date < ad.ach_run_date) or
//                    (:bankNumber = 3941 and mf.bank_number in (3941, 3943) and nvl(mf.wf_conversion_date, '31-Dec-9999') >= ad.ach_run_date) or
//                    (:bankNumber not in (3941, 3943) and :bankNumber = mf.bank_number)
//                  )
//          group by ad.disqualified, ad.load_filename, ad.load_file_id, ad.transmission_date
//          order by ach_file_id, ach_filename nulls first
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (ad idx_ach_trident_dtl_trace_date) */\n                decode(ad.disqualified,'Y','disqualified',ad.load_filename)\n                                                  as ach_filename,\n                ad.load_file_id                   as ach_file_id,\n                ad.transmission_date              as ach_file_date,\n                count(1)                          as ach_count,\n                sum( ats.sales_amount - \n                     ats.credits_amount )         as ach_gross,\n                sum( ats.daily_discount_amount + \n                     ats.daily_ic_amount )        as ach_daily_disc_ic,\n                sum( decode(substr(ats.transaction_code,-1),'7',-1,1) *\n                     ats.ach_amount )             as ach_net\n        from    ach_trident_statement   ats,\n                ach_trident_detail      ad,\n                mif                     mf\n        where   ats.load_file_id =  :1 \n                and ad.trace_number = ats.trace_number\n                --and ad.post_date = ats.post_date_actual\n                and mf.merchant_number = ad.merchant_number\n                and \n                (\n                  ( :2  = 3943 and mf.bank_number = 3943 and mf.wf_conversion_date < ad.ach_run_date) or\n                  ( :3  = 3941 and mf.bank_number in (3941, 3943) and nvl(mf.wf_conversion_date, '31-Dec-9999') >= ad.ach_run_date) or\n                  ( :4  not in (3941, 3943) and  :5  = mf.bank_number)\n                )\n        group by ad.disqualified, ad.load_filename, ad.load_file_id, ad.transmission_date\n        order by ach_file_id, ach_filename nulls first";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setInt(3,bankNumber);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setInt(5,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1151^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new AchFileData( resultSet ) );
      }
      resultSet.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadAchFileData(" + loadFileId + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public long loadAchFileId( long loadFileId, Date batchDate )
  {
    long                  achFileId   = 0L;
    ResultSetIterator     it          = null;
    ResultSet             resultSet   = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1178^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index(ad idx_ach_trident_dtl_trace_date) */
//                  distinct 
//                  ad.load_filename      as load_filename,
//                  ad.load_file_id       as load_file_id
//          from    ach_trident_statement   ats,
//                  ach_trident_detail      ad
//          where   ats.load_file_id = :loadFileId
//                  and ats.batch_date = :batchDate
//                  and ad.trace_number = ats.trace_number
//                  and ad.post_date between ats.batch_date and ats.batch_date+2
//                  and not ad.load_filename is null
//                  and substr(ad.load_filename,14,1) = '_'
//          order by load_file_id 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index(ad idx_ach_trident_dtl_trace_date) */\n                distinct \n                ad.load_filename      as load_filename,\n                ad.load_file_id       as load_file_id\n        from    ach_trident_statement   ats,\n                ach_trident_detail      ad\n        where   ats.load_file_id =  :1 \n                and ats.batch_date =  :2 \n                and ad.trace_number = ats.trace_number\n                and ad.post_date between ats.batch_date and ats.batch_date+2\n                and not ad.load_filename is null\n                and substr(ad.load_filename,14,1) = '_'\n        order by load_file_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setDate(2,batchDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1193^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        achFileId = resultSet.getLong("load_file_id");
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry( "loadAchFileId(" + loadFileId +")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
    return( achFileId );
  }
  
  public void loadCarryoverSummary()
  {
    int                   bankNumber        = getInt("bankNumber",ReportUserBean.getBankNumber());
    int                   bankNumberNew     = (bankNumber == 3941 ? 3943 : -1);
    Date                  cpd               = getReportDateBegin();
    int                   inc               = 0;
    ResultSetIterator     it                = null;
    ResultSet             resultSet         = null;
    
    try
    {
      Calendar cal = Calendar.getInstance();
      cal.setTime(cpd);
    
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:1229^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (vs idx_visa_settle_merch_date) */
//                  'VS'                                            as card_type,
//                  vs.bin_number                                   as bin_number,
//                  vs.load_filename                                as load_filename,
//                  vs.load_file_id                                 as load_file_id,
//                  vs.batch_date                                   as batch_date,
//                  vs.settlement_date                              as settlement_date,
//                  sum(decode(vs.debit_credit_indicator,'C',-1,1)
//                              * vs.transaction_amount)            as carryover_amount
//          from    visa_settlement         vs
//          where   vs.merchant_number in (select mf.merchant_number from mif mf where mf.bank_number in (:bankNumber,:bankNumberNew))
//                  and vs.batch_date = :cpd
//                  and vs.bank_number = :bankNumber
//                  and vs.batch_date != nvl(vs.settlement_date,'31-dec-9999')
//                  and not vs.output_filename like 'hold%'
//          group by vs.bin_number,vs.load_filename,vs.load_file_id,vs.batch_date,vs.settlement_date
//          union
//          select  /*+ index (mc idx_mc_settle_merch_date) */
//                  'MC'                                            as card_type,
//                  mc.bin_number                                   as bin_number,
//                  mc.load_filename                                as load_filename,
//                  mc.load_file_id                                 as load_file_id,
//                  mc.batch_date                                   as batch_date,
//                  mc.settlement_date                              as settlement_date,
//                  sum(decode(mc.debit_credit_indicator,'C',-1,1)
//                              * mc.transaction_amount)            as carryover_amount
//          from    mc_settlement         mc
//          where   mc.merchant_number in (select mf.merchant_number from mif mf where mf.bank_number in (:bankNumber,:bankNumberNew))
//                  and mc.batch_date = :cpd
//                  and mc.bank_number = :bankNumber
//                  and mc.batch_date != nvl(mc.settlement_date,'31-dec-9999')
//                  and not mc.output_filename like 'hold%'
//          group by mc.bin_number,mc.load_filename,mc.load_file_id,mc.batch_date,mc.settlement_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (vs idx_visa_settle_merch_date) */\n                'VS'                                            as card_type,\n                vs.bin_number                                   as bin_number,\n                vs.load_filename                                as load_filename,\n                vs.load_file_id                                 as load_file_id,\n                vs.batch_date                                   as batch_date,\n                vs.settlement_date                              as settlement_date,\n                sum(decode(vs.debit_credit_indicator,'C',-1,1)\n                            * vs.transaction_amount)            as carryover_amount\n        from    visa_settlement         vs\n        where   vs.merchant_number in (select mf.merchant_number from mif mf where mf.bank_number in ( :1 , :2 ))\n                and vs.batch_date =  :3 \n                and vs.bank_number =  :4 \n                and vs.batch_date != nvl(vs.settlement_date,'31-dec-9999')\n                and not vs.output_filename like 'hold%'\n        group by vs.bin_number,vs.load_filename,vs.load_file_id,vs.batch_date,vs.settlement_date\n        union\n        select  /*+ index (mc idx_mc_settle_merch_date) */\n                'MC'                                            as card_type,\n                mc.bin_number                                   as bin_number,\n                mc.load_filename                                as load_filename,\n                mc.load_file_id                                 as load_file_id,\n                mc.batch_date                                   as batch_date,\n                mc.settlement_date                              as settlement_date,\n                sum(decode(mc.debit_credit_indicator,'C',-1,1)\n                            * mc.transaction_amount)            as carryover_amount\n        from    mc_settlement         mc\n        where   mc.merchant_number in (select mf.merchant_number from mif mf where mf.bank_number in ( :5 , :6 ))\n                and mc.batch_date =  :7 \n                and mc.bank_number =  :8 \n                and mc.batch_date != nvl(mc.settlement_date,'31-dec-9999')\n                and not mc.output_filename like 'hold%'\n        group by mc.bin_number,mc.load_filename,mc.load_file_id,mc.batch_date,mc.settlement_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,bankNumberNew);
   __sJT_st.setDate(3,cpd);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setInt(5,bankNumber);
   __sJT_st.setInt(6,bankNumberNew);
   __sJT_st.setDate(7,cpd);
   __sJT_st.setInt(8,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1264^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        ReportRows.add(new CarryoverSummary(resultSet));
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e ) 
    {
      logEntry( "loadCarryoverSummary(" + bankNumber + "," + cpd + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }

  public void loadClearingSummaryAmex(int bankNumber, Date beginDate)
  {
    String                aggregatorSEs           = null;
    Date                  cpd                     = beginDate==null ? getReportDateBegin() : beginDate;
    ClearingSummary       clearingSummary         = new ClearingSummary();
    int                   inc                     = 0;
    ResultSetIterator     it                      = null;
    ResultSet             resultSet               = null;
    
    try
    {
      Calendar cal = Calendar.getInstance();
      cal.setTime(cpd);
      inc = ((cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) ? 1 : 0);
    
      ReportRows.clear();
      ReportRows.add(clearingSummary);
      
      /*@lineinfo:generated-code*//*@lineinfo:1302^7*/

//  ************************************************************
//  #sql [Ctx] { select  am_aggregators
//          
//          from    mbs_banks 
//          where   bank_number = :bankNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  am_aggregators\n         \n        from    mbs_banks \n        where   bank_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MbsReconcileDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   aggregatorSEs = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1308^7*/
      
      // 1. outgoing transactions for this cpd
      /*@lineinfo:generated-code*//*@lineinfo:1311^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  am.amex_se_number                           as bin_number,
//                  sum( decode(am.debit_credit_indicator,'C',0,1) )
//                                                              as sales_count,
//                  sum( decode(am.debit_credit_indicator,'C',0,1)
//                       * am.transaction_amount )              as sales_amount,
//                  sum( decode(am.debit_credit_indicator,'C',1,0) )
//                                                              as credits_count,
//                  sum( decode(am.debit_credit_indicator,'C',1,0)
//                       * am.transaction_amount )              as credits_amount,
//                  0                                           as ca_count,
//                  0                                           as ca_amount,
//                  sum( decode(am.debit_credit_indicator,'C',-1,1)
//                       * nvl(am.funding_amount,am.transaction_amount) )
//                                                              as funding_amount
//          from    amex_settlement     am
//          where   am.amex_se_number in ( :aggregatorSEs )
//                  and am.amex_se_number not in
//                  ( select amex_optblue_se_number
//                    from   amex_se_mapping
//                  )
//                  and am.settlement_date between :cpd and :cpd+:inc
//          group by am.amex_se_number      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  am.amex_se_number                           as bin_number,\n                sum( decode(am.debit_credit_indicator,'C',0,1) )\n                                                            as sales_count,\n                sum( decode(am.debit_credit_indicator,'C',0,1)\n                     * am.transaction_amount )              as sales_amount,\n                sum( decode(am.debit_credit_indicator,'C',1,0) )\n                                                            as credits_count,\n                sum( decode(am.debit_credit_indicator,'C',1,0)\n                     * am.transaction_amount )              as credits_amount,\n                0                                           as ca_count,\n                0                                           as ca_amount,\n                sum( decode(am.debit_credit_indicator,'C',-1,1)\n                     * nvl(am.funding_amount,am.transaction_amount) )\n                                                            as funding_amount\n        from    amex_settlement     am\n        where   am.amex_se_number in (  ");
   __sjT_sb.append(aggregatorSEs);
   __sjT_sb.append("  )\n                and am.amex_se_number not in\n                ( select amex_optblue_se_number\n                  from   amex_se_mapping\n                )\n                and am.settlement_date between  ?  and  ? + ? \n        group by am.amex_se_number");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "4com.mes.reports.MbsReconcileDataBean:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1335^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addFirstPresentments( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 2. reprocessed rejects and reversals
      /*@lineinfo:generated-code*//*@lineinfo:1346^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  am.amex_se_number                           as bin_number,
//                  sum( decode(ama.action_code,'X',1,0)
//                       * decode(am.tran_type,'CA',0,1)
//                       * decode(am.debit_credit_indicator,'C',0,1) )
//                                                              as sales_count,
//                  sum( decode(ama.action_code,'X',1,0)
//                       * decode(am.tran_type,'CA',0,1)
//                       * decode(am.debit_credit_indicator,'C',0,1)
//                       * am.transaction_amount )              as sales_amount,
//                  sum( decode(ama.action_code,'X',1,0)
//                       * decode(am.tran_type,'CA',0,1)
//                       * decode(am.debit_credit_indicator,'C',1,0) )
//                                                              as credits_count,
//                  sum( decode(ama.action_code,'X',1,0)
//                       * decode(am.tran_type,'CA',0,1)
//                       * decode(am.debit_credit_indicator,'C',1,0)
//                       * am.transaction_amount )              as credits_amount,
//                  sum( decode(ama.action_code,'X',1,0)
//                       * decode(am.tran_type,'CA',1,0) )      as ca_count,
//                  sum( decode(ama.action_code,'X',1,0)
//                       * decode(am.tran_type,'CA',1,0)
//                       * am.transaction_amount )              as ca_amount,
//                  sum( decode(ama.action_code,'X',1,0) )      as reject_count,
//                  sum( decode(ama.action_code,'X',1,0)
//                       * decode(am.debit_credit_indicator,'C',-1,1)
//                       * am.transaction_amount )              as reject_amount,
//                  sum( decode(ama.action_code,'R',1,0) )      as reversal_count,
//                  sum( decode(ama.action_code,'R',1,0)
//                       * decode(am.debit_credit_indicator,'C',-1,1)
//                       * am.transaction_amount )              as reversal_amount                     
//          from    amex_settlement_activity    ama,
//                  amex_settlement             am
//          where   ama.settlement_date between :cpd and :cpd+:inc
//                  and am.rec_id = ama.rec_id
//                  and am.amex_se_number in ( :aggregatorSEs )
//                  and am.amex_se_number not in
//                  ( select amex_optblue_se_number
//                    from   amex_se_mapping
//                  )
//          group by am.amex_se_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  am.amex_se_number                           as bin_number,\n                sum( decode(ama.action_code,'X',1,0)\n                     * decode(am.tran_type,'CA',0,1)\n                     * decode(am.debit_credit_indicator,'C',0,1) )\n                                                            as sales_count,\n                sum( decode(ama.action_code,'X',1,0)\n                     * decode(am.tran_type,'CA',0,1)\n                     * decode(am.debit_credit_indicator,'C',0,1)\n                     * am.transaction_amount )              as sales_amount,\n                sum( decode(ama.action_code,'X',1,0)\n                     * decode(am.tran_type,'CA',0,1)\n                     * decode(am.debit_credit_indicator,'C',1,0) )\n                                                            as credits_count,\n                sum( decode(ama.action_code,'X',1,0)\n                     * decode(am.tran_type,'CA',0,1)\n                     * decode(am.debit_credit_indicator,'C',1,0)\n                     * am.transaction_amount )              as credits_amount,\n                sum( decode(ama.action_code,'X',1,0)\n                     * decode(am.tran_type,'CA',1,0) )      as ca_count,\n                sum( decode(ama.action_code,'X',1,0)\n                     * decode(am.tran_type,'CA',1,0)\n                     * am.transaction_amount )              as ca_amount,\n                sum( decode(ama.action_code,'X',1,0) )      as reject_count,\n                sum( decode(ama.action_code,'X',1,0)\n                     * decode(am.debit_credit_indicator,'C',-1,1)\n                     * am.transaction_amount )              as reject_amount,\n                sum( decode(ama.action_code,'R',1,0) )      as reversal_count,\n                sum( decode(ama.action_code,'R',1,0)\n                     * decode(am.debit_credit_indicator,'C',-1,1)\n                     * am.transaction_amount )              as reversal_amount                     \n        from    amex_settlement_activity    ama,\n                amex_settlement             am\n        where   ama.settlement_date between  ?  and  ? + ? \n                and am.rec_id = ama.rec_id\n                and am.amex_se_number in (  ");
   __sjT_sb.append(aggregatorSEs);
   __sjT_sb.append("  )\n                and am.amex_se_number not in\n                ( select amex_optblue_se_number\n                  from   amex_se_mapping\n                )\n        group by am.amex_se_number");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "5com.mes.reports.MbsReconcileDataBean:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1388^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addReprocessedRejects( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 3. outgoing FP rejects and incoming returns
      /*@lineinfo:generated-code*//*@lineinfo:1399^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  am.amex_se_number                           as bin_number,
//                  count(1)                                    as reject_count,
//                  sum( decode(am.debit_credit_indicator,'C',-1,1)
//                       * am.transaction_amount )              as reject_amount
//          from    amex_settlement       am
//          where   am.rec_id in
//                  (
//                    select  distinct rr.rec_id
//                    from    reject_record   rr
//                    where   rr.reject_date between :cpd and :cpd+:inc
//                            and nvl(rr.presentment,0) = 0
//                            and rr.reject_type = 'AM'
//                            and not rr.reject_id like 'mbs%'
//                  )
//                  and am.amex_se_number in ( :aggregatorSEs )
//                  and am.amex_se_number not in
//                  ( select amex_optblue_se_number
//                    from   amex_se_mapping
//                  )
//          group by am.amex_se_number      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  am.amex_se_number                           as bin_number,\n                count(1)                                    as reject_count,\n                sum( decode(am.debit_credit_indicator,'C',-1,1)\n                     * am.transaction_amount )              as reject_amount\n        from    amex_settlement       am\n        where   am.rec_id in\n                (\n                  select  distinct rr.rec_id\n                  from    reject_record   rr\n                  where   rr.reject_date between  ?  and  ? + ? \n                          and nvl(rr.presentment,0) = 0\n                          and rr.reject_type = 'AM'\n                          and not rr.reject_id like 'mbs%'\n                )\n                and am.amex_se_number in (  ");
   __sjT_sb.append(aggregatorSEs);
   __sjT_sb.append("  )\n                and am.amex_se_number not in\n                ( select amex_optblue_se_number\n                  from   amex_se_mapping\n                )\n        group by am.amex_se_number");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "6com.mes.reports.MbsReconcileDataBean:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1421^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addRejects( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 4. incoming chargebacks
      /*@lineinfo:generated-code*//*@lineinfo:1432^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cbam.se_number                as bin_number,
//                  count(1)                      as cb_count,
//                  sum(-cbam.chargeback_amount)  as cb_amount                          
//          from    network_chargeback_amex       cbam
//          where   cbam.se_number in ( :aggregatorSEs )
//                  and cbam.se_number not in
//                  ( select amex_optblue_se_number
//                    from   amex_se_mapping
//                  )
//                  and cbam.settlement_date between :cpd and :cpd+:inc
//          group by cbam.se_number      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  cbam.se_number                as bin_number,\n                count(1)                      as cb_count,\n                sum(-cbam.chargeback_amount)  as cb_amount                          \n        from    network_chargeback_amex       cbam\n        where   cbam.se_number in (  ");
   __sjT_sb.append(aggregatorSEs);
   __sjT_sb.append("  )\n                and cbam.se_number not in\n                ( select amex_optblue_se_number\n                  from   amex_se_mapping\n                )\n                and cbam.settlement_date between  ?  and  ? + ? \n        group by cbam.se_number");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "7com.mes.reports.MbsReconcileDataBean:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1445^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addIncomingChargebacks( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 5. incoming chargeback reversals
      /*@lineinfo:generated-code*//*@lineinfo:1456^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cbam.se_number                as bin_number,
//                  count(1)                      as reversal_count,
//                  sum(-cbam.chargeback_amount)  as reversal_amount,
//                  sum( nvl(cbam.reversal_amount,-cbam.chargeback_amount) - -cbam.chargeback_amount )
//                                                as fx_adj_amount
//          from    network_chargeback_amex     cbam
//          where   cbam.se_number in ( :aggregatorSEs )
//                  and cbam.se_number not in
//                  ( select amex_optblue_se_number
//                    from   amex_se_mapping
//                  )
//                  and cbam.reversal_date = :cpd
//          group by cbam.se_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  cbam.se_number                as bin_number,\n                count(1)                      as reversal_count,\n                sum(-cbam.chargeback_amount)  as reversal_amount,\n                sum( nvl(cbam.reversal_amount,-cbam.chargeback_amount) - -cbam.chargeback_amount )\n                                              as fx_adj_amount\n        from    network_chargeback_amex     cbam\n        where   cbam.se_number in (  ");
   __sjT_sb.append(aggregatorSEs);
   __sjT_sb.append("  )\n                and cbam.se_number not in\n                ( select amex_optblue_se_number\n                  from   amex_se_mapping\n                )\n                and cbam.reversal_date =  ? \n        group by cbam.se_number");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "8com.mes.reports.MbsReconcileDataBean:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1471^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addIncomingReversals( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 6. second presentments
      /*@lineinfo:generated-code*//*@lineinfo:1482^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cbam.se_number              as bin_number,
//                  count(1)                    as sp_count,
//                  sum( cb.tran_amount)        as sp_amount
//          from    network_chargeback_activity cba,
//                  chargeback_action_codes     ac,
//                  network_chargebacks         cb,
//                  network_chargeback_amex     cbam
//          where   cba.settlement_date between :cpd and :cpd+:inc
//                  and ac.short_action_code = cba.action_code
//                  and nvl(ac.represent,'N') = 'Y'
//                  and cb.cb_load_sec = cba.cb_load_sec
//                  and cb.card_type = 'AM'
//                  and cbam.load_sec = cb.cb_load_sec
//          group by cbam.se_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cbam.se_number              as bin_number,\n                count(1)                    as sp_count,\n                sum( cb.tran_amount)        as sp_amount\n        from    network_chargeback_activity cba,\n                chargeback_action_codes     ac,\n                network_chargebacks         cb,\n                network_chargeback_amex     cbam\n        where   cba.settlement_date between  :1  and  :2 + :3 \n                and ac.short_action_code = cba.action_code\n                and nvl(ac.represent,'N') = 'Y'\n                and cb.cb_load_sec = cba.cb_load_sec\n                and cb.card_type = 'AM'\n                and cbam.load_sec = cb.cb_load_sec\n        group by cbam.se_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1498^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addSecondPresentments( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 7. second presentment rejects and returns
      /*@lineinfo:generated-code*//*@lineinfo:1509^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cbam.se_number                  as bin_number,
//                  count(1)                        as reject_count,
//                  sum( cb.tran_amount )           as reject_amount                
//          from    network_chargebacks         cb,
//                  network_chargeback_amex     cbam
//          where   cb.cb_load_sec in
//                  (
//                    select  distinct rr.rec_id
//                    from    reject_record         rr
//                    where   rr.reject_date between :cpd and :cpd+:inc
//                            and nvl(rr.presentment,0) = 2
//                            and rr.reject_type = 'AM'
//                            and not rr.reject_id like 'mbs%'
//                  )
//                  and cbam.load_sec = cb.cb_load_sec
//          group by cbam.se_number        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cbam.se_number                  as bin_number,\n                count(1)                        as reject_count,\n                sum( cb.tran_amount )           as reject_amount                \n        from    network_chargebacks         cb,\n                network_chargeback_amex     cbam\n        where   cb.cb_load_sec in\n                (\n                  select  distinct rr.rec_id\n                  from    reject_record         rr\n                  where   rr.reject_date between  :1  and  :2 + :3 \n                          and nvl(rr.presentment,0) = 2\n                          and rr.reject_type = 'AM'\n                          and not rr.reject_id like 'mbs%'\n                )\n                and cbam.load_sec = cb.cb_load_sec\n        group by cbam.se_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1527^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addSecondPresentmentRejects( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 8. fee collection
      /*@lineinfo:generated-code*//*@lineinfo:1538^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ar.amex_se_number                     as bin_number,
//                  count(1)                              as fees_count,
//                  sum((case when other_fee_description like 'NON-SWIPED ADJUSTMENT%' then 0 else 1 end
//                       * nvl(ar.other_fee_amount,0))
//                      + nvl(ar.adjustment_amount,0) )   as fees_amount
//          from    amex_settlement_recon         ar
//          where   ar.amex_se_number in ( :aggregatorSEs )
//                  and ar.amex_se_number not in
//                  ( select amex_optblue_se_number
//                    from   amex_se_mapping
//                  )
//                  and ar.recon_date = :cpd
//                  and (nvl(ar.adjustment_amount,0) + nvl(ar.other_fee_amount,0)) != 0
//                  and nvl(ar.pay_in_gross_indicator,'N') != 'Y' -- no m/e fees
//                  and nvl(ar.cb_load_sec,0) = 0                 -- no chargebacks
//          group by ar.amex_se_number       
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  ar.amex_se_number                     as bin_number,\n                count(1)                              as fees_count,\n                sum((case when other_fee_description like 'NON-SWIPED ADJUSTMENT%' then 0 else 1 end\n                     * nvl(ar.other_fee_amount,0))\n                    + nvl(ar.adjustment_amount,0) )   as fees_amount\n        from    amex_settlement_recon         ar\n        where   ar.amex_se_number in (  ");
   __sjT_sb.append(aggregatorSEs);
   __sjT_sb.append("  )\n                and ar.amex_se_number not in\n                ( select amex_optblue_se_number\n                  from   amex_se_mapping\n                )\n                and ar.recon_date =  ? \n                and (nvl(ar.adjustment_amount,0) + nvl(ar.other_fee_amount,0)) != 0\n                and nvl(ar.pay_in_gross_indicator,'N') != 'Y' -- no m/e fees\n                and nvl(ar.cb_load_sec,0) = 0                 -- no chargebacks\n        group by ar.amex_se_number");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "11com.mes.reports.MbsReconcileDataBean:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1556^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addFeeCollection( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 9. reimbursement fees, amex fees and report totals
      /*@lineinfo:generated-code*//*@lineinfo:1567^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ar.amex_se_number                       as bin_number,
//                  sum((ar.net_soc_amount - ar.soc_amount)
//                      + (nvl(ar.net_adjustment_amount,0) - nvl(ar.adjustment_amount,0))
//                      + (decode(ar.pay_in_gross_indicator,'Y',1,0)
//                         * nvl(ar.other_fee_amount,0)) )  as reimbursement_fees,
//                  sum(case when other_fee_description like 'NON-SWIPED ADJUSTMENT%' then 1 else 0 end
//                      * nvl(ar.other_fee_amount,0))       as assoc_fees,
//                  sum(ar.net_soc_amount 
//                      + nvl(ar.net_adjustment_amount,0) 
//                      + nvl(ar.other_fee_amount,0))       as total_amount                                  
//          from    amex_settlement_recon       ar
//          where   ar.amex_se_number in ( :aggregatorSEs )
//                  and ar.amex_se_number not in
//                  ( select amex_optblue_se_number
//                    from   amex_se_mapping
//                  )
//                  and ar.recon_date = :cpd
//          group by ar.amex_se_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  ar.amex_se_number                       as bin_number,\n                sum((ar.net_soc_amount - ar.soc_amount)\n                    + (nvl(ar.net_adjustment_amount,0) - nvl(ar.adjustment_amount,0))\n                    + (decode(ar.pay_in_gross_indicator,'Y',1,0)\n                       * nvl(ar.other_fee_amount,0)) )  as reimbursement_fees,\n                sum(case when other_fee_description like 'NON-SWIPED ADJUSTMENT%' then 1 else 0 end\n                    * nvl(ar.other_fee_amount,0))       as assoc_fees,\n                sum(ar.net_soc_amount \n                    + nvl(ar.net_adjustment_amount,0) \n                    + nvl(ar.other_fee_amount,0))       as total_amount                                  \n        from    amex_settlement_recon       ar\n        where   ar.amex_se_number in (  ");
   __sjT_sb.append(aggregatorSEs);
   __sjT_sb.append("  )\n                and ar.amex_se_number not in\n                ( select amex_optblue_se_number\n                  from   amex_se_mapping\n                )\n                and ar.recon_date =  ? \n        group by ar.amex_se_number");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "12com.mes.reports.MbsReconcileDataBean:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1587^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addAssocData( resultSet );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e ) 
    {
      logEntry( "loadClearingSummaryAmex()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  
  public void loadClearingSummaryAmexOptblue(int bankNumber, Date beginDate)
  {
    String                aggregatorSEs           = null;
    Date                  cpd                     = beginDate==null ? getReportDateBegin() : beginDate;
    ClearingSummary       clearingSummary         = new ClearingSummary();
    int                   inc                     = 0;
    ResultSetIterator     it                      = null;
    ResultSet             resultSet               = null;
    
    int cutoffTime = 0;
    
    if ( MesDefaults.DK_AMEX_CLEARING_CUTOFF != null )
    {
      try{ cutoffTime = MesDefaults.getInt(MesDefaults.DK_AMEX_CLEARING_CUTOFF); } 
      catch( Exception ee ) 
      {
    	  logEntry("Error While fetching the AMEX Clearing Cut OFF time from Properties file",ee.toString());}
    }
       
    try
    {
      Calendar cal = Calendar.getInstance();
      cal.setTime(cpd);
      inc = ((cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) ? 1 : 0);
    
      ReportRows.clear();
      ReportRows.add(clearingSummary);
      
      /*@lineinfo:generated-code*//*@lineinfo:1626^7*/

//  ************************************************************
//  #sql [Ctx] { select  am_aggregators
//          
//          from    mbs_banks 
//          where   bank_number = :bankNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  am_aggregators\n         \n        from    mbs_banks \n        where   bank_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.MbsReconcileDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   aggregatorSEs = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1632^7*/
      
      // 1. outgoing transactions for this cpd

//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
	  String theSqlTS = "select  am.amex_se_number as bin_number,"
		   		+ "   sum( decode(am.debit_credit_indicator,'C',0,1) ) as sales_count,"
		   		+ "   sum( decode(am.debit_credit_indicator,'C',0,1) * am.transaction_amount ) as sales_amount,"
		   		+ "   sum( decode(am.debit_credit_indicator,'C',1,0) ) as credits_count,"
		   		+ "   sum( decode(am.debit_credit_indicator,'C',1,0) * am.transaction_amount )  as credits_amount,"
		   		+ "   0    as ca_count,"
		   		+ "   0    as ca_amount,"
		   		+ "   sum( decode(am.debit_credit_indicator,'C',-1,1) * nvl(am.funding_amount,am.transaction_amount) ) as funding_amount"
		   		+ "   from amex_settlement am"
		   		+ "   where"
		   		+ "   am.settlement_date between decode((to_char(am.last_modified_date, 'hh24mi') - :1) - ABS( (to_char(am.last_modified_date, 'hh24mi') - :2)), 0, :3 + 1, :4)"
		   		+ "   and decode((to_char(last_modified_date, 'hh24mi') - :5) - ABS( (to_char(last_modified_date, 'hh24mi') - :6)), 0, :7 + 1, :8 + :9)"
		   		+ "   and am.amex_se_number in"
		   		+ "   ( select distinct amse.amex_optblue_se_number"
		   		+ "    from   mif mf, amex_se_mapping amse"
		   		+ "    where  mf.bank_number =  am.bank_number and mf.bank_number =  :10  and amse.amex_optblue_se_number = mf.damexse )"
		   		+ "    group by am.amex_se_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,cutoffTime);
   __sJT_st.setInt(2,cutoffTime);
   __sJT_st.setDate(3,cpd);
   __sJT_st.setDate(4,cpd);
   __sJT_st.setInt(5,cutoffTime);
   __sJT_st.setInt(6,cutoffTime);
   __sJT_st.setDate(7,cpd);
   __sJT_st.setDate(8,cpd);
   __sJT_st.setInt(9,inc);
   __sJT_st.setInt(10,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1661^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addFirstPresentments( resultSet );
        System.out.println("1. outgoing transactions for this cpd");
      }
      resultSet.close();
      it.close();
     
      // 2. reprocessed rejects and reversals
{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  am.amex_se_number as bin_number,sum(decode(ama.action_code,'X',1,0) * decode(am.tran_type,'CA',0,1)"
   		+ "* decode(am.debit_credit_indicator,'C',0,1) ) as sales_count,"
   		+ "sum( decode(ama.action_code,'X',1,0)* decode(am.tran_type,'CA',0,1) * decode(am.debit_credit_indicator,'C',0,1) * am.transaction_amount) as sales_amount,"
   		+ "sum( decode(ama.action_code,'X',1,0) * decode(am.tran_type,'CA',0,1)* decode(am.debit_credit_indicator,'C',1,0))  as credits_count,"
   		+ "sum( decode(ama.action_code,'X',1,0)* decode(am.tran_type,'CA',0,1) * decode(am.debit_credit_indicator,'C',1,0) * am.transaction_amount ) as credits_amount,"
   		+ "sum( decode(ama.action_code,'X',1,0)* decode(am.tran_type,'CA',1,0)) as ca_count,"
   		+ "sum( decode(ama.action_code,'X',1,0)* decode(am.tran_type,'CA',1,0) * am.transaction_amount) as ca_amount,sum( decode(ama.action_code,'X',1,0) ) as reject_count,"
   		+ "sum( decode(ama.action_code,'X',1,0)* decode(am.debit_credit_indicator,'C',-1,1)* am.transaction_amount )as reject_amount,"
   		+ "sum( decode(ama.action_code,'R',1,0))as reversal_count,sum( decode(ama.action_code,'R',1,0)* decode(am.debit_credit_indicator,'C',-1,1)* am.transaction_amount) "
   		+ "as reversal_amount from amex_settlement_activity ama,amex_settlement am where   ama.settlement_date between :1 and :2 + :3 "
   		+ "and am.rec_id = ama.rec_id and am.amex_se_number in (select distinct amse.amex_optblue_se_number from   mif mf,amex_se_mapping amse "
   		+ "where  mf.bank_number =  am.bank_number and mf.bank_number = :4 and amse.amex_optblue_se_number = mf.damexse)group by am.amex_se_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   __sJT_st.setInt(4,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1717^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addReprocessedRejects( resultSet );
      }
      resultSet.close();
      it.close();
      System.out.println("2. reprocessed rejects and reversals");
      
      // 3. outgoing FP rejects and incoming returns
      
{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  am.amex_se_number  as bin_number, count(1)as reject_count,"
   				   + "sum( decode(am.debit_credit_indicator,'C',-1,1)* am.transaction_amount ) as reject_amount "
   				   + "from amex_settlement am where am.rec_id in"
   				   + "(select distinct rr.rec_id from reject_record rr where rr.reject_date between :1 and :2 + :3 and nvl(rr.presentment,0) = 0 "
   				   + "and rr.reject_type = 'AM' and not rr.reject_id like 'mbs%') "
   				   + "and am.amex_se_number in ( select distinct amse.amex_optblue_se_number from   mif mf,amex_se_mapping amse "
   				   + "where mf.bank_number = am.bank_number and mf.bank_number =:4 and amse.amex_optblue_se_number = mf.damexse) group by am.amex_se_number ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   __sJT_st.setInt(4,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1753^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addRejects( resultSet );
      }
      resultSet.close();
      it.close();
      System.out.println("3. outgoing FP rejects and incoming returns");
     
      // 4. incoming chargebacks

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
	  String theSqlTS = "select  cbam.se_numb as bin_number, count(1)as cb_count,sum(-cbam.cb_amount)  as cb_amount "
	  					+ " from network_chargeback_amex_optb cbam "
	  					+ "where   cbam.settlement_date between  :1   and  :2  + :3  and cbam.bank_number=:4 "
	  					+ " \n        group by cbam.se_numb";
		
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   __sJT_st.setInt(4,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1773^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addIncomingChargebacks( resultSet );
      }
      resultSet.close();
      it.close();
      System.out.println("4. incoming chargebacks");
     
      // 5. incoming chargeback reversals
{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cbam.se_numb as bin_number, count(1) as reversal_count,"
   				   + "sum(-cbam.cb_amount)  as reversal_amount,sum( nvl(cbam.reversal_amount,-cbam.cb_amount) - -cbam.cb_amount )as fx_adj_amount "
   				   + "from network_chargeback_amex_optb cbam "
   				   + "where cbam.reversal_date =:1 and cbam.bank_number=:2 group by cbam.se_numb ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setInt(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1795^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addIncomingReversals( resultSet );
      }
      resultSet.close();
      it.close();
      System.out.println("5. incoming chargeback reversals");
      
      // 6. second presentments
{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select cbam.se_numb as bin_number,count(1) as sp_count,sum( cb.tran_amount) as sp_amount "
   					+"from network_chargeback_activity  cba,chargeback_action_codes ac, network_chargebacks cb, network_chargeback_amex_optb cbam "
   					+"where cba.settlement_date between :1 and :2 + :3 and ac.short_action_code = cba.action_code and nvl(ac.represent,'N') = 'Y' "
   					+"and cb.cb_load_sec = cba.cb_load_sec and cb.bank_number = :4 and cb.card_type = 'AM'and cbam.load_sec = cb.cb_load_sec "
   					+ "group by cbam.se_numb";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   __sJT_st.setInt(4, bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1823^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addSecondPresentments( resultSet );
      }
      resultSet.close();
      it.close();
      System.out.println("6. second presentments");
     
      // 7. second presentment rejects and returns
{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select cbam.se_numb as bin_number,count(1) as reject_count,sum( cb.tran_amount ) as reject_amount "
   		             +"from network_chargebacks cb,network_chargeback_amex_optb cbam where   cb.cb_load_sec in "
   		             +"(select distinct rr.rec_id from reject_record rr where rr.reject_date between :1 and :2 + :3 "
   		             +"and nvl(rr.presentment,0) = 2 and rr.reject_type = 'AM' and not rr.reject_id like 'mbs%') "
   		             +"and cb.bank_number = :4 "
   		             +"and cbam.load_sec = cb.cb_load_sec "
   		             +"group by cbam.se_numb ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   __sJT_st.setInt(4,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1853^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addSecondPresentmentRejects( resultSet );
      }
      resultSet.close();
      it.close();
      System.out.println("7. second presentment rejects and returns");
      
			if (bankNumber == mesConstants.BANK_ID_MES || bankNumber == mesConstants.BANK_ID_MES_WF) {

				// 8. fee collection
				// declare temps
				oracle.jdbc.OraclePreparedStatement __sJT_st = null;
				sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx;
				if (__sJT_cc == null)
					sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
				sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext() == null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
				 try {
    			//@formatter:off
    			  String theSqlTS = "select ar.amex_se_number  as bin_number,"
    					  + " count(1) as fees_count,  sum( (case when other_fee_description like 'NON-SWIPED ADJUSTMENT%' then 0 "
    					  + "else 1 "
    					  + "end  "
    					  + "* nvl(ar.other_fee_amount,0))  + nvl(ar.adjustment_amount,0) )  as fees_amount "
    					  + "  from   amex_settlement_recon_optblue ar "
    					  + " where  ar.load_fileid in   ( select distinct load_fileid  from   amex_settlement_recon_optblue where  recon_date =  :1 )"
    					  + " and ar.record_type = 2  and (nvl(ar.adjustment_amount,0) + nvl(ar.other_fee_amount,0)) != 0  and nvl(ar.pay_in_gross_indicator,'N') != 'Y'"
    					  + " and nvl(ar.cb_load_sec,0) = 0 and ar.AMEX_PAYEE_NUMBER in "
    					  + " ( select CAP_NUMBER from amex_optblue_caps where BANK_NUMBER= :2)  group by ar.amex_se_number";
    			//@formatter:on
    			  __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.reports.MbsReconcileDataBean",theSqlTS);
    			  // set IN parameters
    			  __sJT_st.setDate(1,cpd);
    			  __sJT_st.setInt(2,bankNumber);
    			  // execute query
    			  it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.reports.MbsReconcileDataBean",null));
    		  } finally { __sJT_ec.oracleCloseQuery(); }


    	  
    	  resultSet = it.getResultSet();

    	  while ( resultSet.next() )
    	  {
    		  clearingSummary.addFeeCollection( resultSet );
    	  }
    	  resultSet.close();
    	  it.close();

				// 9. reimbursement fees, amex fees and report totals
    	  
				  try {
    			//@formatter:off
    			  String theSqlTS = " select ar.amex_se_number  as bin_number, "
    					  + " sum((-discount_amount) + (-service_fee_amount) +  (-other_fee_amount)) as reimbursement_fees, "
    					  + " sum( case when other_fee_description like 'NON-SWIPED ADJUSTMENT%' then 1 else 0 end * nvl(ar.other_fee_amount,0)) as assoc_fees,"
    					  + " sum( ar.net_soc_amount + nvl(ar.net_adjustment_amount,0) + nvl(ar.other_fee_amount,0)) as total_amount "
    					  + " from   amex_settlement_recon_optblue ar "
    					  + " where  ar.load_fileid in "
    					  + " (select distinct load_fileid from   amex_settlement_recon_optblue "
    					  + " where  ar.amex_process_date =  :1 ) and ar.record_type = 2 "
    					  + " and ar.AMEX_PAYEE_NUMBER in (select CAP_NUMBER from amex_optblue_caps where BANK_NUMBER= :2) group by ar.amex_se_number";
    			//@formatter:on
    			  __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.reports.MbsReconcileDataBean",theSqlTS);
    			  // set IN parameters
    			  __sJT_st.setDate(1,cpd);
    			  __sJT_st.setInt(2,bankNumber);
    			  // execute query
    			  it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.reports.MbsReconcileDataBean",null));
    		  } finally { __sJT_ec.oracleCloseQuery(); }



    	  resultSet = it.getResultSet();

    	  while ( resultSet.next() )
    	  {
    		  clearingSummary.addAssocData( resultSet );
    	  }
    	  resultSet.close();
    	  it.close();

      }
      
      else if (bankNumber==mesConstants.BANK_ID_STERLING || bankNumber==mesConstants.BANK_ID_SVB)
      {
     
      // 8. fee collection
      /*@lineinfo:generated-code*//*@lineinfo:1865^7*/

//  ************************************************************
//  #sql [Ctx] it = { select ar.amex_se_number                     as bin_number,
//                 count(1)                              as fees_count,
//                 sum( (case when other_fee_description like 'NON-SWIPED ADJUSTMENT%' then 0 else 1 end
//                      * nvl(ar.other_fee_amount,0))
//                      + nvl(ar.adjustment_amount,0) )  as fees_amount
//          from   amex_settlement_recon_optblue ar
//          where  ar.load_fileid in
//                     ( select distinct load_fileid
//                       from   amex_settlement_recon_optblue
//                       where  recon_date = :cpd+1
//                     )
//                 and ar.record_type = 2
//                 and (nvl(ar.adjustment_amount,0) + nvl(ar.other_fee_amount,0)) != 0
//                 and nvl(ar.pay_in_gross_indicator,'N') != 'Y' -- no m/e fees
//                 and nvl(ar.cb_load_sec,0) = 0                 -- no chargebacks
//                 and ar.amex_se_number in
//                     ( select damexse
//                       from   mif
//                       where  bank_number = :bankNumber
//                     )
//          group by ar.amex_se_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select ar.amex_se_number                     as bin_number,\n               count(1)                              as fees_count,\n               sum( (case when other_fee_description like 'NON-SWIPED ADJUSTMENT%' then 0 else 1 end\n                    * nvl(ar.other_fee_amount,0))\n                    + nvl(ar.adjustment_amount,0) )  as fees_amount\n        from   amex_settlement_recon_optblue ar\n        where  ar.load_fileid in\n                   ( select distinct load_fileid\n                     from   amex_settlement_recon_optblue\n                     where  recon_date =  :1 +1\n                   )\n               and ar.record_type = 2\n               and (nvl(ar.adjustment_amount,0) + nvl(ar.other_fee_amount,0)) != 0\n               and nvl(ar.pay_in_gross_indicator,'N') != 'Y' -- no m/e fees\n               and nvl(ar.cb_load_sec,0) = 0                 -- no chargebacks\n               and ar.amex_se_number in\n                   ( select damexse\n                     from   mif\n                     where  bank_number =  :2 \n                   )\n        group by ar.amex_se_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setInt(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1888^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addFeeCollection( resultSet );
      }
      resultSet.close();
      it.close();
     
      // 9. reimbursement fees, amex fees and report totals

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
	//@formatter:off
   String theSqlTS = " select ar.amex_se_number  as bin_number,"
			  + " sum((-discount_amount) + (-service_fee_amount) +  (-other_fee_amount)) as reimbursement_fees,"
			  + " sum( case when other_fee_description like 'NON-SWIPED ADJUSTMENT%' then 1 else 0 end * nvl(ar.other_fee_amount,0)) as assoc_fees,"
			  + " sum( ar.net_soc_amount + nvl(ar.net_adjustment_amount,0) + nvl(ar.other_fee_amount,0)) as total_amount "
			  + " from   amex_settlement_recon_optblue ar "
			  + " where  ar.load_fileid in "
			  + "(select distinct load_fileid from   amex_settlement_recon_optblue "
			  + " where  ar.amex_process_date =  :1) and ar.record_type = 2 and ar.amex_se_number in "
			  + "(select damexse  from   mif where  bank_number =  :2 ) "
			  + "group by ar.amex_se_number";
 //@formatter:on
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setInt(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1924^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addAssocData( resultSet );
      }
      resultSet.close();
      it.close();
      System.out.println("9. reimbursement fees, amex fees and report totals");
    }
  }
    catch( Exception e ) 
    {
      logEntry( "loadClearingSummaryAmexOptblue()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  public void loadClearingSummaryDiscover(int bankNumber, Date beginDate)
  {
    Date                  cpd                     = beginDate==null ? getReportDateBegin() : beginDate;
    ClearingSummary       clearingSummary         = new ClearingSummary();
    int                   inc                     = 0;
    ResultSetIterator     it                      = null;
    ResultSet             resultSet               = null;
    
    try
    {
      Calendar cal = Calendar.getInstance();
      cal.setTime(cpd);
      inc = ((cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) ? 1 : 0);
    
      ReportRows.clear();
      ReportRows.add(clearingSummary);
      
      // 1. outgoing transactions for this cpd
      /*@lineinfo:generated-code*//*@lineinfo:1963^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (ds idx_ds_settle_acq_id_sdate) */
//                  ds.acquirer_id                              as bin_number,
//                  sum( decode(ds.tran_type,'CA',0,1)
//                       * decode(ds.debit_credit_indicator,'C',0,1) )
//                                                              as sales_count,
//                  sum( decode(ds.tran_type,'CA',0,1)
//                       * decode(ds.debit_credit_indicator,'C',0,1)
//                       * ds.transaction_amount )              as sales_amount,
//                  sum( decode(ds.tran_type,'CA',0,1)
//                       * decode(ds.debit_credit_indicator,'C',1,0) )
//                                                              as credits_count,
//                  sum( decode(ds.tran_type,'CA',0,1)
//                       * decode(ds.debit_credit_indicator,'C',1,0)
//                       * ds.transaction_amount )              as credits_amount,
//                  sum( decode(ds.tran_type,'CA',1,0) )        as ca_count,
//                  sum( decode(ds.tran_type,'CA',1,0)
//                       * ds.transaction_amount )              as ca_amount,
//                  sum( decode(ds.debit_credit_indicator,'C',-1,1)
//                       * nvl(ds.funding_amount,ds.transaction_amount) )
//                                                              as funding_amount
//          from    discover_settlement     ds
//          where   ds.acquirer_id in ( select mb.ds_acquirer_id from mbs_banks mb where mb.bank_number = :bankNumber )
//                  and ds.settlement_date between :cpd-:inc and :cpd
//          group by ds.acquirer_id      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (ds idx_ds_settle_acq_id_sdate) */\n                ds.acquirer_id                              as bin_number,\n                sum( decode(ds.tran_type,'CA',0,1)\n                     * decode(ds.debit_credit_indicator,'C',0,1) )\n                                                            as sales_count,\n                sum( decode(ds.tran_type,'CA',0,1)\n                     * decode(ds.debit_credit_indicator,'C',0,1)\n                     * ds.transaction_amount )              as sales_amount,\n                sum( decode(ds.tran_type,'CA',0,1)\n                     * decode(ds.debit_credit_indicator,'C',1,0) )\n                                                            as credits_count,\n                sum( decode(ds.tran_type,'CA',0,1)\n                     * decode(ds.debit_credit_indicator,'C',1,0)\n                     * ds.transaction_amount )              as credits_amount,\n                sum( decode(ds.tran_type,'CA',1,0) )        as ca_count,\n                sum( decode(ds.tran_type,'CA',1,0)\n                     * ds.transaction_amount )              as ca_amount,\n                sum( decode(ds.debit_credit_indicator,'C',-1,1)\n                     * nvl(ds.funding_amount,ds.transaction_amount) )\n                                                            as funding_amount\n        from    discover_settlement     ds\n        where   ds.acquirer_id in ( select mb.ds_acquirer_id from mbs_banks mb where mb.bank_number =  :1  )\n                and ds.settlement_date between  :2 - :3  and  :4 \n        group by ds.acquirer_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   __sJT_st.setDate(4,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"23com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1989^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addFirstPresentments( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 2. reprocessed rejects and reversals
      /*@lineinfo:generated-code*//*@lineinfo:2000^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ds.acquirer_id                              as bin_number,
//                  sum( decode(dsa.action_code,'X',1,0)
//                       * decode(ds.tran_type,'CA',0,1)
//                       * decode(ds.debit_credit_indicator,'C',0,1) )
//                                                              as sales_count,
//                  sum( decode(dsa.action_code,'X',1,0)
//                       * decode(ds.tran_type,'CA',0,1)
//                       * decode(ds.debit_credit_indicator,'C',0,1)
//                       * ds.transaction_amount )              as sales_amount,
//                  sum( decode(dsa.action_code,'X',1,0)
//                       * decode(ds.tran_type,'CA',0,1)
//                       * decode(ds.debit_credit_indicator,'C',1,0) )
//                                                              as credits_count,
//                  sum( decode(dsa.action_code,'X',1,0)
//                       * decode(ds.tran_type,'CA',0,1)
//                       * decode(ds.debit_credit_indicator,'C',1,0)
//                       * ds.transaction_amount )              as credits_amount,
//                  sum( decode(dsa.action_code,'X',1,0)
//                       * decode(ds.tran_type,'CA',1,0) )      as ca_count,
//                  sum( decode(dsa.action_code,'X',1,0)
//                       * decode(ds.tran_type,'CA',1,0)
//                       * ds.transaction_amount )              as ca_amount,
//                  sum( decode(dsa.action_code,'X',1,0) )      as reject_count,
//                  sum( decode(dsa.action_code,'X',1,0)
//                       * decode(ds.debit_credit_indicator,'C',-1,1)
//                       * ds.transaction_amount )              as reject_amount,
//                  sum( decode(dsa.action_code,'R',1,0) )      as reversal_count,
//                  sum( decode(dsa.action_code,'R',1,0)
//                       * decode(ds.debit_credit_indicator,'C',-1,1)
//                       * ds.transaction_amount )              as reversal_amount                     
//          from    discover_settlement_activity  dsa,
//                  discover_settlement           ds
//          where   dsa.settlement_date between :cpd-:inc and :cpd
//                  and ds.rec_id = dsa.rec_id
//                  and ds.acquirer_id in ( select mb.ds_acquirer_id from mbs_banks mb where mb.bank_number = :bankNumber )
//          group by ds.acquirer_id        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ds.acquirer_id                              as bin_number,\n                sum( decode(dsa.action_code,'X',1,0)\n                     * decode(ds.tran_type,'CA',0,1)\n                     * decode(ds.debit_credit_indicator,'C',0,1) )\n                                                            as sales_count,\n                sum( decode(dsa.action_code,'X',1,0)\n                     * decode(ds.tran_type,'CA',0,1)\n                     * decode(ds.debit_credit_indicator,'C',0,1)\n                     * ds.transaction_amount )              as sales_amount,\n                sum( decode(dsa.action_code,'X',1,0)\n                     * decode(ds.tran_type,'CA',0,1)\n                     * decode(ds.debit_credit_indicator,'C',1,0) )\n                                                            as credits_count,\n                sum( decode(dsa.action_code,'X',1,0)\n                     * decode(ds.tran_type,'CA',0,1)\n                     * decode(ds.debit_credit_indicator,'C',1,0)\n                     * ds.transaction_amount )              as credits_amount,\n                sum( decode(dsa.action_code,'X',1,0)\n                     * decode(ds.tran_type,'CA',1,0) )      as ca_count,\n                sum( decode(dsa.action_code,'X',1,0)\n                     * decode(ds.tran_type,'CA',1,0)\n                     * ds.transaction_amount )              as ca_amount,\n                sum( decode(dsa.action_code,'X',1,0) )      as reject_count,\n                sum( decode(dsa.action_code,'X',1,0)\n                     * decode(ds.debit_credit_indicator,'C',-1,1)\n                     * ds.transaction_amount )              as reject_amount,\n                sum( decode(dsa.action_code,'R',1,0) )      as reversal_count,\n                sum( decode(dsa.action_code,'R',1,0)\n                     * decode(ds.debit_credit_indicator,'C',-1,1)\n                     * ds.transaction_amount )              as reversal_amount                     \n        from    discover_settlement_activity  dsa,\n                discover_settlement           ds\n        where   dsa.settlement_date between  :1 - :2  and  :3 \n                and ds.rec_id = dsa.rec_id\n                and ds.acquirer_id in ( select mb.ds_acquirer_id from mbs_banks mb where mb.bank_number =  :4  )\n        group by ds.acquirer_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setInt(2,inc);
   __sJT_st.setDate(3,cpd);
   __sJT_st.setInt(4,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"24com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2038^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addReprocessedRejects( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 3. outgoing FP rejects and incoming returns
      /*@lineinfo:generated-code*//*@lineinfo:2049^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ds.acquirer_id                              as bin_number,
//                  count(1)                                    as reject_count,
//                  sum( decode(ds.debit_credit_indicator,'C',-1,1)
//                       * ds.transaction_amount )              as reject_amount
//          from    discover_settlement       ds
//          where   ds.rec_id in
//                  (
//                    select  distinct rr.rec_id
//                    from    reject_record   rr
//                    where   rr.reject_date between :cpd-:inc and :cpd
//                            and nvl(rr.presentment,0) = 0
//                            and rr.reject_type = 'DS'
//                            and not rr.reject_id like 'mbs%'
//                  )
//                  and ds.acquirer_id in ( select mb.ds_acquirer_id from mbs_banks mb where mb.bank_number = :bankNumber )
//          group by ds.acquirer_id        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ds.acquirer_id                              as bin_number,\n                count(1)                                    as reject_count,\n                sum( decode(ds.debit_credit_indicator,'C',-1,1)\n                     * ds.transaction_amount )              as reject_amount\n        from    discover_settlement       ds\n        where   ds.rec_id in\n                (\n                  select  distinct rr.rec_id\n                  from    reject_record   rr\n                  where   rr.reject_date between  :1 - :2  and  :3 \n                          and nvl(rr.presentment,0) = 0\n                          and rr.reject_type = 'DS'\n                          and not rr.reject_id like 'mbs%'\n                )\n                and ds.acquirer_id in ( select mb.ds_acquirer_id from mbs_banks mb where mb.bank_number =  :4  )\n        group by ds.acquirer_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setInt(2,inc);
   __sJT_st.setDate(3,cpd);
   __sJT_st.setInt(4,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"25com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2067^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addRejects( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 4. incoming chargebacks
      if( bankNumber == 3003 )// I'm tired of hearing SVB complain about how discover chargebacks don't reconcile
      {
        /*@lineinfo:generated-code*//*@lineinfo:2080^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  dsr.acquirer_id           as bin_number,
//                    dsr.item_count            as cb_count,
//                    dsr.item_amount*-1           as cb_amount
//            from    discover_settlement_recon dsr
//            where   dsr.bank_number = :bankNumber
//                    and dsr.settlement_date = :cpd
//                    and dsr.entry_description = 'DISPUTES'                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dsr.acquirer_id           as bin_number,\n                  dsr.item_count            as cb_count,\n                  dsr.item_amount*-1           as cb_amount\n          from    discover_settlement_recon dsr\n          where   dsr.bank_number =  :1 \n                  and dsr.settlement_date =  :2 \n                  and dsr.entry_description = 'DISPUTES'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"26com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2089^9*/
      }
      else
      {

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
	//@formatter:off
	String theSqlTS = " select cb.bin_number as bin_number, "
			  		+ " sum(decode(cb.first_time_chargeback,'Y',1,'P',1,0) ) as cb_count, "
			  		+ " sum(decode(cb.first_time_chargeback,'Y',1,'P',1,0) * cb.tran_amount) as cb_amount "
			  		+ " from network_chargebacks cb "
			  		+ " inner join "
			  		+ " network_chargeback_activity cba on cba.CB_LOAD_SEC=cb.CB_LOAD_SEC "
			  		+ " where cb.bank_number = :1 and "
			  		+ " cba.action_date between  :2 -1 -:3   and :4 -1  and "
			  		+ " ((cba.action_code='D' and cba.user_message ='Auto Action - Discover') "
			  		+ " OR (cba.action_code='D' and cba.action_source ='4')) "
			  		+ " and cb.card_type = 'DS' "
			  		+ " group by cb.bin_number";
	//@formatter:on	
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   __sJT_st.setDate(4,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"27com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2105^9*/
      }

      resultSet = it.getResultSet();
    
      while ( resultSet.next() )
      {
        clearingSummary.addIncomingChargebacks( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 5. incoming chargeback reversals

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   //@formatter:off
   String theSqlTS = "select  cb.bin_number  as bin_number, "
			  	   + "count(1)   as reversal_count, "
			  	   + "sum( cb.tran_amount) as reversal_amount, "
			  	   + "sum( nvl(cb.reversal_amount,cb.tran_amount) - cb.tran_amount )  as fx_adj_amount "
			  	   + "from network_chargebacks cb "
			  	   + "inner join "
			  	   + "network_chargeback_activity cba on cba.CB_LOAD_SEC=cb.CB_LOAD_SEC "
			  	   + "where cb.bank_number = :1 "
			  	   + "and cba.action_date between  :2-1 -:3 and  :4-1 "
			  	   + "and cba.action_code='J' and cba.user_message ='Reversal' and cba.action_source='7' "
			  	   + "and cb.card_type = 'DS' "
			  	   + "group by cb.bin_number";
	  //@formatter:on	
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   __sJT_st.setDate(4,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"28com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2130^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addIncomingReversals( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 6. second presentments
      /*@lineinfo:generated-code*//*@lineinfo:2141^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.bin_number               as bin_number,
//                  count(1)                    as sp_count,
//                  sum( cb.tran_amount)        as sp_amount
//          from    network_chargeback_activity cba,
//                  chargeback_action_codes     ac,
//                  network_chargebacks         cb
//          where   cba.settlement_date between :cpd-:inc and :cpd
//                  and ac.short_action_code = cba.action_code
//                  and nvl(ac.represent,'N') = 'Y'
//                  and cb.cb_load_sec = cba.cb_load_sec
//                  and cb.bank_number = :bankNumber
//                  and cb.card_type = 'DS'
//                  and cb.first_time_chargeback = 'Y'
//          group by cb.bin_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.bin_number               as bin_number,\n                count(1)                    as sp_count,\n                sum( cb.tran_amount)        as sp_amount\n        from    network_chargeback_activity cba,\n                chargeback_action_codes     ac,\n                network_chargebacks         cb\n        where   cba.settlement_date between  :1 - :2  and  :3 \n                and ac.short_action_code = cba.action_code\n                and nvl(ac.represent,'N') = 'Y'\n                and cb.cb_load_sec = cba.cb_load_sec\n                and cb.bank_number =  :4 \n                and cb.card_type = 'DS'\n                and cb.first_time_chargeback = 'Y'\n        group by cb.bin_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setInt(2,inc);
   __sJT_st.setDate(3,cpd);
   __sJT_st.setInt(4,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"29com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2157^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addSecondPresentments( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 7. second presentment rejects and returns
      /*@lineinfo:generated-code*//*@lineinfo:2168^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.bin_number                   as bin_number,
//                  count(1)                        as reject_count,
//                  sum( cb.tran_amount )           as reject_amount                
//          from    network_chargebacks   cb
//          where   cb.cb_load_sec in
//                  (
//                    select  distinct rr.rec_id
//                    from    reject_record   rr
//                    where   rr.reject_date between :cpd-:inc and :cpd
//                            and nvl(rr.presentment,0) = 2
//                            and rr.reject_type = 'DS'
//                            and not rr.reject_id like 'mbs%'
//                  )
//                  and cb.bank_number = :bankNumber
//          group by cb.bin_number        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.bin_number                   as bin_number,\n                count(1)                        as reject_count,\n                sum( cb.tran_amount )           as reject_amount                \n        from    network_chargebacks   cb\n        where   cb.cb_load_sec in\n                (\n                  select  distinct rr.rec_id\n                  from    reject_record   rr\n                  where   rr.reject_date between  :1 - :2  and  :3 \n                          and nvl(rr.presentment,0) = 2\n                          and rr.reject_type = 'DS'\n                          and not rr.reject_id like 'mbs%'\n                )\n                and cb.bank_number =  :4 \n        group by cb.bin_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setInt(2,inc);
   __sJT_st.setDate(3,cpd);
   __sJT_st.setInt(4,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"30com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2185^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addSecondPresentmentRejects( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 8. fee collection
      /*@lineinfo:generated-code*//*@lineinfo:2196^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  fc.acquirer_id        as bin_number,
//                  count(1)              as fees_count,
//                  sum( fc.fee_amount )  as fees_amount
//          from    discover_settlement_fee_coll    fc
//          where   fc.acquirer_id in ( select mb.ds_acquirer_id from mbs_banks mb where mb.bank_number = :bankNumber )
//                  and fc.settlement_date between :cpd-:inc and :cpd
//          group by fc.acquirer_id       
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  fc.acquirer_id        as bin_number,\n                count(1)              as fees_count,\n                sum( fc.fee_amount )  as fees_amount\n        from    discover_settlement_fee_coll    fc\n        where   fc.acquirer_id in ( select mb.ds_acquirer_id from mbs_banks mb where mb.bank_number =  :1  )\n                and fc.settlement_date between  :2 - :3  and  :4 \n        group by fc.acquirer_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   __sJT_st.setDate(4,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"31com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2205^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addFeeCollection( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 9. reimbursement fees, discover fees and recon totals
      /*@lineinfo:generated-code*//*@lineinfo:2216^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dr.acquirer_id                      as bin_number,
//                  sum(decode(dr.entry_description,'ACQUIRER INTERCHANGE ASSESSED',1,'CASH REIMBURSEMENT',1,0)
//                      * dr.item_amount )              as reimbursement_fees,
//                  sum(decode(dr.entry_description,'ACQUIRER ASSESSMENTS',1,'ACQUIRER FEES',1,'CORRECTIONS',1,0)
//                      * dr.item_amount )              as assoc_fees,
//                  sum(decode(dr.entry_description,'',1,0)
//                      * dr.item_amount )              as total_amount                                  
//          from    discover_settlement_recon   dr
//          where   dr.acquirer_id in ( select mb.ds_acquirer_id from mbs_banks mb where mb.bank_number = :bankNumber )
//                  and dr.settlement_date between :cpd-:inc and :cpd
//          group by dr.acquirer_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dr.acquirer_id                      as bin_number,\n                sum(decode(dr.entry_description,'ACQUIRER INTERCHANGE ASSESSED',1,'CASH REIMBURSEMENT',1,0)\n                    * dr.item_amount )              as reimbursement_fees,\n                sum(decode(dr.entry_description,'ACQUIRER ASSESSMENTS',1,'ACQUIRER FEES',1,'CORRECTIONS',1,0)\n                    * dr.item_amount )              as assoc_fees,\n                sum(decode(dr.entry_description,'',1,0)\n                    * dr.item_amount )              as total_amount                                  \n        from    discover_settlement_recon   dr\n        where   dr.acquirer_id in ( select mb.ds_acquirer_id from mbs_banks mb where mb.bank_number =  :1  )\n                and dr.settlement_date between  :2 - :3  and  :4 \n        group by dr.acquirer_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setInt(3,inc);
   __sJT_st.setDate(4,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"32com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2229^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addAssocData( resultSet );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e ) 
    {
      logEntry( "loadClearingSummaryDiscover()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }

  public void loadClearingSummaryMasterCard() {
    loadClearingSummary("MC");
  }

  public void loadClearingSummaryAmex() {
    loadClearingSummary("AM");
  }
  
  public void loadClearingSummaryAmexOptblue() {
	    loadClearingSummary("AB");
  }
  public void loadClearingSummaryDiscover() {
    loadClearingSummary("DS");
  }

  public void loadClearingSummaryVisa() {
    loadClearingSummary("VS");
  }

  public void loadClearingSummaryMasterCard(boolean regenerate) {
    int bankNumber = getInt("bankNumber", ReportUserBean.getBankNumber() );
    Date cpd = getReportDateBegin();
    String cardType="MC";

    if(regenerate) {
      deleteReports(bankNumber, cpd, cardType);
      loadClearingSummaryMasterCard(bankNumber, cpd);
      storeClearingSummary(bankNumber, cardType, cpd);
    } else {
      loadClearingSummary(cardType);
    }
  }

  public void loadClearingSummaryAmex(boolean regenerate) {
    int bankNumber = getInt("bankNumber", ReportUserBean.getBankNumber() );
    Date cpd = getReportDateBegin();
    String cardType="AM";

    if(regenerate) {
      deleteReports(bankNumber, cpd, cardType);
      loadClearingSummaryAmex(bankNumber, cpd);
      storeClearingSummary(bankNumber, cardType, cpd);
    } else {
      loadClearingSummary(cardType);
    }
  }

  public void loadClearingSummaryAmexOptblue(boolean regenerate) {
	    int bankNumber = getInt("bankNumber", ReportUserBean.getBankNumber() );
	    Date cpd = getReportDateBegin();
	    String cardType="AM";

	    if(regenerate) {
	      deleteReports(bankNumber, cpd, cardType);
	      loadClearingSummaryAmexOptblue(bankNumber, cpd);
	      storeClearingSummary(bankNumber, cardType, cpd);
	    } else {
	      cardType="AB";
	      loadClearingSummary(cardType);
	    }
	  }

  public void loadClearingSummaryDiscover(boolean regenerate) {
    int bankNumber = getInt("bankNumber", ReportUserBean.getBankNumber() );
    Date cpd = getReportDateBegin();
    String cardType="DS";

    if(regenerate) {
      deleteReports(bankNumber, cpd, cardType);
      loadClearingSummaryDiscover(bankNumber, cpd);
      storeClearingSummary(bankNumber, cardType, cpd);
    } else {
      loadClearingSummary(cardType);
    }
  }

  public void loadClearingSummaryVisa(boolean regenerate) {
    int bankNumber = getInt("bankNumber", ReportUserBean.getBankNumber() );
    Date cpd = getReportDateBegin();
    String cardType="VS";

    if(regenerate) {
      deleteReports(bankNumber, cpd, cardType);
      loadClearingSummaryVisa(bankNumber, cpd);
      storeClearingSummary(bankNumber, cardType, cpd);
    } else {
      loadClearingSummary(cardType);
    }
  }

  public void deleteReports(int bankNumber, Date cpd, String cardType) {
    try {
      System.out.println("deleting reports for: " + bankNumber + ", " + cardType + " for date " + com.mes.support.DateTimeFormatter.getFormattedDate(cpd, "MM/dd/yyyy") );
      /*@lineinfo:generated-code*//*@lineinfo:2342^7*/

//  ************************************************************
//  #sql [Ctx] { delete from daily_clearing_summary
//          where   clearing_date = :cpd
//                  and card_type = :cardType
//                  and bank_number = :bankNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from daily_clearing_summary\n        where   clearing_date =  :1 \n                and card_type =  :2 \n                and bank_number =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"33com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setString(2,cardType);
   __sJT_st.setInt(3,bankNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2347^7*/
      commit();

    } catch( Exception e ) {
        logEntry( "deleteReports()", e.toString() );
    }
  }

  private void loadClearingSummary(String cardType) {
    ClearingSummary clearingSummary = new ClearingSummary();
    ReportRows.clear();
    ReportRows.add(clearingSummary);

    int bankNumber = getInt("bankNumber", ReportUserBean.getBankNumber() );
    Date beginDate = getReportDateBegin();
    Date endDate = getReportDateEnd();
    ResultSetIterator it = null;
    ResultSet resultSet = null;
    String seL = "";
    

    try {
    	if(cardType == "AB"){
    		seL = "and bin_number in (select distinct amex_optblue_se_number from amex_se_mapping)";
    		cardType = "AM";
        } else if(cardType == "AM") {
        	seL = "and bin_number not in (select distinct amex_optblue_se_number from amex_se_mapping)";
        }
      /*@lineinfo:generated-code*//*@lineinfo:2375^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bank_number,
//                  bin_number,
//                  sum(sales_amount)                    as sales_amount,
//                  sum(credits_amount)                  as credits_amount,
//                  sum(cash_advance_amount)             as cash_advance_amount,
//                  sum(reprocessed_rejects_amount)      as reprocessed_rejects_amount,
//                  sum(reversals_amount)                as reversals_amount,
//                  sum(rejects_amount)                  as rejects_amount,
//                  sum(incoming_reversals_amount)       as incoming_reversals_amount,
//                  sum(funding_amount)                  as funding_amount,
//                  sum(fx_adjustment_amount)            as fx_adjustment_amount,
//                  sum(incoming_chargebacks_amount)     as incoming_chargebacks_amount,
//                  sum(second_presentments_amount)      as second_presentments_amount,
//                  sum(second_presentment_rejects_amt)  as second_presentment_rejects_amt,
//                  sum(reimbursement_fees)              as reimbursement_fees,
//                  sum(assoc_fees)                      as assoc_fees,
//                  sum(fee_collection_amount)           as fee_collection_amount,
//                  sum(actual_amount)                   as actual_amount
//          from daily_clearing_summary
//          where   bank_number = :bankNumber
//                  and clearing_date between :beginDate and :endDate
//                  and card_type = :cardType
//                  :seL
//          group by bank_number, bin_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  bank_number,\n                bin_number,\n                sum(sales_amount)                    as sales_amount,\n                sum(credits_amount)                  as credits_amount,\n                sum(cash_advance_amount)             as cash_advance_amount,\n                sum(reprocessed_rejects_amount)      as reprocessed_rejects_amount,\n                sum(reversals_amount)                as reversals_amount,\n                sum(rejects_amount)                  as rejects_amount,\n                sum(incoming_reversals_amount)       as incoming_reversals_amount,\n                sum(funding_amount)                  as funding_amount,\n                sum(fx_adjustment_amount)            as fx_adjustment_amount,\n                sum(incoming_chargebacks_amount)     as incoming_chargebacks_amount,\n                sum(second_presentments_amount)      as second_presentments_amount,\n                sum(second_presentment_rejects_amt)  as second_presentment_rejects_amt,\n                sum(reimbursement_fees)              as reimbursement_fees,\n                sum(assoc_fees)                      as assoc_fees,\n                sum(fee_collection_amount)           as fee_collection_amount,\n                sum(actual_amount)                   as actual_amount\n        from daily_clearing_summary\n        where   bank_number =  ? \n                and clearing_date between  ?  and  ? \n                and card_type =  ? \n                 ");
   __sjT_sb.append(seL);
   __sjT_sb.append(" \n        group by bank_number, bin_number");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "34com.mes.reports.MbsReconcileDataBean:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   __sJT_st.setString(4,cardType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2401^7*/

      resultSet = it.getResultSet();
      while ( resultSet.next() )
      {
        clearingSummary.addBinSummary(resultSet);
      }
      resultSet.close();
      it.close();

    } catch( Exception e ) {
      logEntry( "loadClearingSummary()", e.toString() );
    } finally {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }

  public void loadClearingSummaryMasterCard(int bankNumber, Date beginDate)
  {
    Date                  cpd                     = beginDate==null ? getReportDateBegin() : beginDate;
    int                   inc                     = 0;
    
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(cpd);
			inc = ((cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) ? 1 : 0);
			
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			Date truncatedCpd = new Date(cal.getTimeInMillis());

			ReportRows.clear();
			DailyClearingSummaryReport aMasterCardClearingSummary= DailyClearingSummaryReport.getInstance("MC");
			ReportRows.add(aMasterCardClearingSummary.loadClearingSummary(truncatedCpd, inc, bankNumber));

		}
		catch (Exception e) {
			logEntry("loadClearingSummaryMasterCard()", e.toString());
		}
  }
  
  public void loadClearingSummaryVisa(int bankNumber, Date beginDate)
  {
    Date                  cpd                     = beginDate==null ? getReportDateBegin() : beginDate;
    ClearingSummary       clearingSummary         = new ClearingSummary();
    ResultSetIterator     it                      = null;
    ResultSet             resultSet               = null;
    
    try
    {
      ReportRows.clear();
      ReportRows.add(clearingSummary);
      
      // 1. outgoing transactions for this cpd
      /*@lineinfo:generated-code*//*@lineinfo:2787^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (vs idx_vs_bin_settle_date) */
//                  vs.bin_number,
//                  sum( decode(vs.cash_disbursement,'Y',0,1)
//                       * decode(vs.debit_credit_indicator,'C',0,1) )
//                                                              as sales_count,
//                  sum( round( (
//                       decode(vs.cash_disbursement,'Y',0,1)
//                       * decode(vs.debit_credit_indicator,'C',0,1)
//                       * case
//                           when dcc.base_currency_code is null then 1
//                           else dcc.sell_rate
//                         end
//                       * vs.transaction_amount ),2 ) )        as sales_amount,
//                  sum( decode(vs.cash_disbursement,'Y',0,1)
//                       * decode(vs.debit_credit_indicator,'C',1,0) )
//                                                              as credits_count,
//                  sum( round( (
//                       decode(vs.cash_disbursement,'Y',0,1)
//                       * decode(vs.debit_credit_indicator,'C',1,0)
//                       * case
//                           when dcc.base_currency_code is null then 1
//                           else dcc.buy_rate
//                         end
//                       * vs.transaction_amount ),2 ) )        as credits_amount,
//                  sum( decode(vs.cash_disbursement,'Y',1,0) ) as ca_count,
//                  sum( round( (
//                       decode(vs.cash_disbursement,'Y',1,0)
//                       * case
//                           when dcc.base_currency_code is null then 1
//                           else dcc.buy_rate
//                         end
//                       * vs.transaction_amount ),2 ) )        as ca_amount,
//                  sum( decode(vs.debit_credit_indicator,'C',-1,1)
//                       * nvl(vs.funding_amount,vs.transaction_amount) )
//                                                              as funding_amount
//          from    visa_settlement       vs,
//                  visa_settlement_dcc   dcc
//          where   vs.bank_number = :bankNumber
//                  and vs.settlement_date = :cpd
//                  and dcc.effective_date(+) = :cpd
//                  and dcc.counter_currency_code(+) = vs.currency_code
//                  and dcc.base_currency_code(+) = nvl(vs.funding_currency_code,'840')
//          group by vs.bin_number      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (vs idx_vs_bin_settle_date) */\n                vs.bin_number,\n                sum( decode(vs.cash_disbursement,'Y',0,1)\n                     * decode(vs.debit_credit_indicator,'C',0,1) )\n                                                            as sales_count,\n                sum( round( (\n                     decode(vs.cash_disbursement,'Y',0,1)\n                     * decode(vs.debit_credit_indicator,'C',0,1)\n                     * case\n                         when dcc.base_currency_code is null then 1\n                         else dcc.sell_rate\n                       end\n                     * vs.transaction_amount ),2 ) )        as sales_amount,\n                sum( decode(vs.cash_disbursement,'Y',0,1)\n                     * decode(vs.debit_credit_indicator,'C',1,0) )\n                                                            as credits_count,\n                sum( round( (\n                     decode(vs.cash_disbursement,'Y',0,1)\n                     * decode(vs.debit_credit_indicator,'C',1,0)\n                     * case\n                         when dcc.base_currency_code is null then 1\n                         else dcc.buy_rate\n                       end\n                     * vs.transaction_amount ),2 ) )        as credits_amount,\n                sum( decode(vs.cash_disbursement,'Y',1,0) ) as ca_count,\n                sum( round( (\n                     decode(vs.cash_disbursement,'Y',1,0)\n                     * case\n                         when dcc.base_currency_code is null then 1\n                         else dcc.buy_rate\n                       end\n                     * vs.transaction_amount ),2 ) )        as ca_amount,\n                sum( decode(vs.debit_credit_indicator,'C',-1,1)\n                     * nvl(vs.funding_amount,vs.transaction_amount) )\n                                                            as funding_amount\n        from    visa_settlement       vs,\n                visa_settlement_dcc   dcc\n        where   vs.bank_number =  :1 \n                and vs.settlement_date =  :2 \n                and dcc.effective_date(+) =  :3 \n                and dcc.counter_currency_code(+) = vs.currency_code\n                and dcc.base_currency_code(+) = nvl(vs.funding_currency_code,'840')\n        group by vs.bin_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"44com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setDate(3,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"44com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2832^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addFirstPresentments( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 2. reprocessed rejects and reversals
      /*@lineinfo:generated-code*//*@lineinfo:2843^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  vs.bin_number,
//                  sum( decode(vsa.action_code,'X',1,0)
//                       * decode(vs.cash_disbursement,'Y',0,1)
//                       * decode(vs.debit_credit_indicator,'C',0,1) )
//                                                              as sales_count,
//                  sum( round( (
//                       decode(vsa.action_code,'X',1,0)
//                       * decode(vs.cash_disbursement,'Y',0,1)
//                       * decode(vs.debit_credit_indicator,'C',0,1)
//                       * case
//                           when dcc.base_currency_code is null then 1
//                           else dcc.sell_rate
//                         end
//                       * vs.transaction_amount ),2 ) )        as sales_amount,
//                  sum( decode(vsa.action_code,'X',1,0)
//                       * decode(vs.cash_disbursement,'Y',0,1)
//                       * decode(vs.debit_credit_indicator,'C',1,0) )
//                                                              as credits_count,
//                  sum( round( (
//                       decode(vsa.action_code,'X',1,0)
//                       * decode(vs.cash_disbursement,'Y',0,1)
//                       * decode(vs.debit_credit_indicator,'C',1,0)
//                       * case
//                           when dcc.base_currency_code is null then 1
//                           else dcc.buy_rate
//                         end
//                       * vs.transaction_amount ),2 ) )        as credits_amount,
//                  sum( decode(vsa.action_code,'X',1,0)
//                       * decode(vs.cash_disbursement,'Y',1,0) ) as ca_count,
//                  sum( round( (
//                       decode(vsa.action_code,'X',1,0)
//                       * decode(vs.cash_disbursement,'Y',1,0)
//                       * case
//                           when dcc.base_currency_code is null then 1
//                           else dcc.buy_rate
//                         end
//                       * vs.transaction_amount ),2 ) )        as ca_amount,
//                  sum( decode(vsa.action_code,'X',1,0) )      as reject_count,
//                  sum( round( (
//                       decode(vsa.action_code,'X',1,0)
//                       * decode(vs.debit_credit_indicator,'C',-1,1)
//                       * case
//                           when dcc.base_currency_code is null then 1
//                           when vs.cash_disbursement = 'Y' or vs.debit_credit_indicator = 'C' then dcc.buy_rate
//                           else dcc.sell_rate
//                         end
//                       * vs.transaction_amount ),2 ) )        as reject_amount,
//                  sum( decode(vsa.action_code,'R',1,0) )      as reversal_count,
//                  sum( round( (
//                       decode(vsa.action_code,'R',1,0)
//                       * decode(vs.debit_credit_indicator,'C',-1,1)
//                       * case
//                           when dcc.base_currency_code is null then 1
//                           when vs.cash_disbursement = 'Y' or vs.debit_credit_indicator = 'C' then dcc.buy_rate
//                           else dcc.sell_rate
//                         end
//                       * vs.transaction_amount ),2 ) )        as reversal_amount                     
//          from    visa_settlement_activity  vsa,
//                  visa_settlement           vs,
//                  visa_settlement_dcc       dcc
//          where   vsa.settlement_date = :cpd
//                  and vs.rec_id = vsa.rec_id
//                  and vs.bank_number = :bankNumber
//                  and dcc.effective_date(+) = :cpd
//                  and dcc.counter_currency_code(+) = vs.currency_code
//                  and dcc.base_currency_code(+) = nvl(vs.funding_currency_code,'840')
//          group by vs.bin_number        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vs.bin_number,\n                sum( decode(vsa.action_code,'X',1,0)\n                     * decode(vs.cash_disbursement,'Y',0,1)\n                     * decode(vs.debit_credit_indicator,'C',0,1) )\n                                                            as sales_count,\n                sum( round( (\n                     decode(vsa.action_code,'X',1,0)\n                     * decode(vs.cash_disbursement,'Y',0,1)\n                     * decode(vs.debit_credit_indicator,'C',0,1)\n                     * case\n                         when dcc.base_currency_code is null then 1\n                         else dcc.sell_rate\n                       end\n                     * vs.transaction_amount ),2 ) )        as sales_amount,\n                sum( decode(vsa.action_code,'X',1,0)\n                     * decode(vs.cash_disbursement,'Y',0,1)\n                     * decode(vs.debit_credit_indicator,'C',1,0) )\n                                                            as credits_count,\n                sum( round( (\n                     decode(vsa.action_code,'X',1,0)\n                     * decode(vs.cash_disbursement,'Y',0,1)\n                     * decode(vs.debit_credit_indicator,'C',1,0)\n                     * case\n                         when dcc.base_currency_code is null then 1\n                         else dcc.buy_rate\n                       end\n                     * vs.transaction_amount ),2 ) )        as credits_amount,\n                sum( decode(vsa.action_code,'X',1,0)\n                     * decode(vs.cash_disbursement,'Y',1,0) ) as ca_count,\n                sum( round( (\n                     decode(vsa.action_code,'X',1,0)\n                     * decode(vs.cash_disbursement,'Y',1,0)\n                     * case\n                         when dcc.base_currency_code is null then 1\n                         else dcc.buy_rate\n                       end\n                     * vs.transaction_amount ),2 ) )        as ca_amount,\n                sum( decode(vsa.action_code,'X',1,0) )      as reject_count,\n                sum( round( (\n                     decode(vsa.action_code,'X',1,0)\n                     * decode(vs.debit_credit_indicator,'C',-1,1)\n                     * case\n                         when dcc.base_currency_code is null then 1\n                         when vs.cash_disbursement = 'Y' or vs.debit_credit_indicator = 'C' then dcc.buy_rate\n                         else dcc.sell_rate\n                       end\n                     * vs.transaction_amount ),2 ) )        as reject_amount,\n                sum( decode(vsa.action_code,'R',1,0) )      as reversal_count,\n                sum( round( (\n                     decode(vsa.action_code,'R',1,0)\n                     * decode(vs.debit_credit_indicator,'C',-1,1)\n                     * case\n                         when dcc.base_currency_code is null then 1\n                         when vs.cash_disbursement = 'Y' or vs.debit_credit_indicator = 'C' then dcc.buy_rate\n                         else dcc.sell_rate\n                       end\n                     * vs.transaction_amount ),2 ) )        as reversal_amount                     \n        from    visa_settlement_activity  vsa,\n                visa_settlement           vs,\n                visa_settlement_dcc       dcc\n        where   vsa.settlement_date =  :1 \n                and vs.rec_id = vsa.rec_id\n                and vs.bank_number =  :2 \n                and dcc.effective_date(+) =  :3 \n                and dcc.counter_currency_code(+) = vs.currency_code\n                and dcc.base_currency_code(+) = nvl(vs.funding_currency_code,'840')\n        group by vs.bin_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"45com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setDate(3,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"45com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2912^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addReprocessedRejects( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 3. outgoing FP rejects and incoming returns
      /*@lineinfo:generated-code*//*@lineinfo:2923^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  vs.bin_number                               as bin_number,
//                  count(1)                                    as reject_count,
//                  sum( round( (
//                       decode(vs.debit_credit_indicator,'C',-1,1)
//                       * case
//                           when dcc.base_currency_code is null then 1
//                           when vs.cash_disbursement = 'Y' or vs.debit_credit_indicator = 'C' then dcc.buy_rate
//                           else dcc.sell_rate
//                         end
//                       * vs.transaction_amount ),2 ) )        as reject_amount
//          from    visa_settlement       vs,
//                  visa_settlement_dcc   dcc
//          where   vs.rec_id in
//                  (
//                    select  distinct rr.rec_id
//                    from    reject_record   rr
//                    where   rr.reject_date = :cpd
//                            and nvl(rr.presentment,0) = 0
//                            and rr.reject_type = 'VS'
//                            and not rr.reject_id like 'mbs%'
//                  )
//                  and vs.bank_number = :bankNumber
//                  and dcc.effective_date(+) = :cpd
//                  and dcc.counter_currency_code(+) = vs.currency_code
//                  and dcc.base_currency_code(+) = nvl(vs.funding_currency_code,'840')
//          group by vs.bin_number        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vs.bin_number                               as bin_number,\n                count(1)                                    as reject_count,\n                sum( round( (\n                     decode(vs.debit_credit_indicator,'C',-1,1)\n                     * case\n                         when dcc.base_currency_code is null then 1\n                         when vs.cash_disbursement = 'Y' or vs.debit_credit_indicator = 'C' then dcc.buy_rate\n                         else dcc.sell_rate\n                       end\n                     * vs.transaction_amount ),2 ) )        as reject_amount\n        from    visa_settlement       vs,\n                visa_settlement_dcc   dcc\n        where   vs.rec_id in\n                (\n                  select  distinct rr.rec_id\n                  from    reject_record   rr\n                  where   rr.reject_date =  :1 \n                          and nvl(rr.presentment,0) = 0\n                          and rr.reject_type = 'VS'\n                          and not rr.reject_id like 'mbs%'\n                )\n                and vs.bank_number =  :2 \n                and dcc.effective_date(+) =  :3 \n                and dcc.counter_currency_code(+) = vs.currency_code\n                and dcc.base_currency_code(+) = nvl(vs.funding_currency_code,'840')\n        group by vs.bin_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"46com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setDate(3,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"46com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2951^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addRejects( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 4. incoming chargebacks
      /*@lineinfo:generated-code*//*@lineinfo:2962^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.bin_number             as bin_number,
//                  sum( decode(cb.first_time_chargeback,'Y',1,0) )                 
//                                            as cb_count,
//                  sum( decode(cb.first_time_chargeback,'Y',1,0) 
//                       * cb.tran_amount)    as cb_amount                          
//          from    network_chargebacks         cb
//          where   cb.bank_number = :bankNumber
//                  and cb.incoming_date = :cpd
//                  and cb.card_type = 'VS'
//                  and not cb.cb_ref_num is null -- ignore SMS entries
//          group by cb.bin_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.bin_number             as bin_number,\n                sum( decode(cb.first_time_chargeback,'Y',1,0) )                 \n                                          as cb_count,\n                sum( decode(cb.first_time_chargeback,'Y',1,0) \n                     * cb.tran_amount)    as cb_amount                          \n        from    network_chargebacks         cb\n        where   cb.bank_number =  :1 \n                and cb.incoming_date =  :2 \n                and cb.card_type = 'VS'\n                and not cb.cb_ref_num is null -- ignore SMS entries\n        group by cb.bin_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"47com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"47com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2975^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addIncomingChargebacks( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 5. incoming chargeback reversals
      /*@lineinfo:generated-code*//*@lineinfo:2986^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.bin_number             as bin_number,
//                  count(1)                  as reversal_count,
//                  sum( cb.tran_amount)      as reversal_amount,
//                  sum( nvl(cb.reversal_amount,cb.tran_amount) - cb.tran_amount )
//                                            as fx_adj_amount
//          from    network_chargebacks         cb
//          where   cb.bank_number = :bankNumber
//                  and cb.reversal_date = :cpd
//                  and cb.card_type = 'VS'
//          group by cb.bin_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.bin_number             as bin_number,\n                count(1)                  as reversal_count,\n                sum( cb.tran_amount)      as reversal_amount,\n                sum( nvl(cb.reversal_amount,cb.tran_amount) - cb.tran_amount )\n                                          as fx_adj_amount\n        from    network_chargebacks         cb\n        where   cb.bank_number =  :1 \n                and cb.reversal_date =  :2 \n                and cb.card_type = 'VS'\n        group by cb.bin_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"48com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"48com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2998^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addIncomingReversals( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 6. second presentments
      /*@lineinfo:generated-code*//*@lineinfo:3009^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.bin_number               as bin_number,
//                  count(1)                    as sp_count,
//                  sum( cb.tran_amount)        as sp_amount
//          from    network_chargeback_activity cba,
//                  chargeback_action_codes     ac,
//                  network_chargebacks         cb
//          where   cba.settlement_date = :cpd-1
//                  and ac.short_action_code = cba.action_code
//                  and nvl(ac.represent,'N') = 'Y'
//                  and cb.cb_load_sec = cba.cb_load_sec
//                  and cb.bank_number = :bankNumber
//                  and cb.card_type = 'VS'
//                  and cb.first_time_chargeback = 'Y'
//          group by cb.bin_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.bin_number               as bin_number,\n                count(1)                    as sp_count,\n                sum( cb.tran_amount)        as sp_amount\n        from    network_chargeback_activity cba,\n                chargeback_action_codes     ac,\n                network_chargebacks         cb\n        where   cba.settlement_date =  :1 -1\n                and ac.short_action_code = cba.action_code\n                and nvl(ac.represent,'N') = 'Y'\n                and cb.cb_load_sec = cba.cb_load_sec\n                and cb.bank_number =  :2 \n                and cb.card_type = 'VS'\n                and cb.first_time_chargeback = 'Y'\n        group by cb.bin_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"49com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setInt(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"49com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3025^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addSecondPresentments( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 7. second presentment rejects and returns
      /*@lineinfo:generated-code*//*@lineinfo:3036^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.bin_number                   as bin_number,
//                  count(1)                        as reject_count,
//                  sum( cb.tran_amount )           as reject_amount                
//          from    network_chargebacks   cb
//          where   cb.cb_load_sec in
//                  (
//                    select  distinct rr.rec_id
//                    from    reject_record   rr
//                    where   rr.reject_date = :cpd
//                            and nvl(rr.presentment,0) = 2
//                            and rr.reject_type = 'VS'
//                            and not rr.reject_id like 'mbs%'
//                  )
//                  and cb.bank_number = :bankNumber
//          group by cb.bin_number        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.bin_number                   as bin_number,\n                count(1)                        as reject_count,\n                sum( cb.tran_amount )           as reject_amount                \n        from    network_chargebacks   cb\n        where   cb.cb_load_sec in\n                (\n                  select  distinct rr.rec_id\n                  from    reject_record   rr\n                  where   rr.reject_date =  :1 \n                          and nvl(rr.presentment,0) = 2\n                          and rr.reject_type = 'VS'\n                          and not rr.reject_id like 'mbs%'\n                )\n                and cb.bank_number =  :2 \n        group by cb.bin_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"50com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setInt(2,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"50com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3053^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addSecondPresentmentRejects( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 8. fee collection
      /*@lineinfo:generated-code*//*@lineinfo:3064^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  fc.acquirer_bin                       as bin_number,
//                  count(1)                              as fees_count,
//                  sum( decode(fc.debit_credit_indicator,'C',-1,1) 
//                       * fc.transaction_amount )        as fees_amount
//          from    visa_settlement_fee_collect   fc
//          where   fc.bank_number = :bankNumber
//                  and fc.settlement_date = :cpd
//          group by fc.acquirer_bin       
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  fc.acquirer_bin                       as bin_number,\n                count(1)                              as fees_count,\n                sum( decode(fc.debit_credit_indicator,'C',-1,1) \n                     * fc.transaction_amount )        as fees_amount\n        from    visa_settlement_fee_collect   fc\n        where   fc.bank_number =  :1 \n                and fc.settlement_date =  :2 \n        group by fc.acquirer_bin";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"51com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"51com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3074^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addFeeCollection( resultSet );
      }
      resultSet.close();
      it.close();
      
      // 9. reimbursement fees, visa fees and vss-115 totals
      /*@lineinfo:generated-code*//*@lineinfo:3085^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  vr.bin_number                       as bin_number,
//                  sum( decode(vr.amount_type,'F',1,0)
//                       * decode(vr.total_amount_sign,'DB',-1,1)
//                       * vr.total_amount )            as reimbursement_fees,
//                  sum( decode(vr.amount_type,'C',1,0)
//                       * decode(vr.total_amount_sign,'DB',-1,1)
//                       * vr.total_amount )            as assoc_fees,
//                  sum( decode(vr.amount_type,'T',1,0)
//                       * decode(vr.total_amount_sign,'DB',-1,1)
//                       * vr.total_amount )            as total_amount                                  
//          from    visa_settlement_recon_vss115    vr
//          where   vr.bank_number = :bankNumber
//                  and vr.settlement_date = :cpd
//                  and vr.amount_type in ('F','C','T') 
//                  and decode( vr.amount_type,'T',9,1 ) = vr.business_mode
//          group by vr.bin_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vr.bin_number                       as bin_number,\n                sum( decode(vr.amount_type,'F',1,0)\n                     * decode(vr.total_amount_sign,'DB',-1,1)\n                     * vr.total_amount )            as reimbursement_fees,\n                sum( decode(vr.amount_type,'C',1,0)\n                     * decode(vr.total_amount_sign,'DB',-1,1)\n                     * vr.total_amount )            as assoc_fees,\n                sum( decode(vr.amount_type,'T',1,0)\n                     * decode(vr.total_amount_sign,'DB',-1,1)\n                     * vr.total_amount )            as total_amount                                  \n        from    visa_settlement_recon_vss115    vr\n        where   vr.bank_number =  :1 \n                and vr.settlement_date =  :2 \n                and vr.amount_type in ('F','C','T') \n                and decode( vr.amount_type,'T',9,1 ) = vr.business_mode\n        group by vr.bin_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"52com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"52com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3103^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        clearingSummary.addAssocData( resultSet );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e ) 
    {
      logEntry( "loadClearingSummaryVisa()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  public void loadDepositData( )
  {
    loadDepositData(getLong("lfid"),getReportDateBegin());
  }
  
  public void loadDepositData( long loadFileId, Date cpd )
  {
    long                  achFileId       = 0L;
    int                   bankNumber      = getInt("bankNumber",ReportUserBean.getBankNumber());
    ResultSetIterator     it              = null;
    String                loadFilename    = null;
    ResultSet             resultSet       = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      achFileId = loadAchFileId(loadFileId,cpd);
      
      /*@lineinfo:generated-code*//*@lineinfo:3142^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_file_id_to_load_filename(:loadFileId)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_file_id_to_load_filename( :1 )\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"53com.mes.reports.MbsReconcileDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFilename = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3147^7*/
    
      /*@lineinfo:generated-code*//*@lineinfo:3149^7*/

//  ************************************************************
//  #sql [Ctx] it = { with
//            ach as
//            (
//              select  ats.batch_date                    as batch_date,
//                      count(1)                          as ach_count,
//                      sum( ats.sales_amount - 
//                           ats.credits_amount )         as ach_gross,
//                      sum( ats.daily_discount_amount + 
//                           ats.daily_ic_amount )        as ach_daily_disc_ic,
//                      sum( decode(substr(ats.transaction_code,-1),'7',-1,1) *
//                           ats.ach_amount )             as ach_net
//              from    ach_trident_statement   ats,
//                      mif                     mf
//              where   ats.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                      and mf.merchant_number = ats.merchant_number
//                      and 
//                      (
//                        (:bankNumber = 3943 and mf.bank_number = 3943 and mf.wf_conversion_date < ats.batch_date) or
//                        (:bankNumber = 3941 and mf.bank_number in (3941, 3943) and nvl(mf.wf_conversion_date, '31-Dec-9999') >= ats.batch_date) or
//                        (:bankNumber not in (3941, 3943) and :bankNumber = mf.bank_number)
//                      )
//              group by ats.batch_date
//            ),
//            dt as
//            (
//              select  dt.batch_date                     as batch_date,
//                      dt.ach_run_date                   as ach_run_date,
//                      count(distinct dt.batch_number)   as deposit_count,
//                      sum( decode(dt.debit_credit_indicator,'C',-1,1) *
//                          dt.transaction_amount )       as deposit_gross,
//                      sum( dt.discount_paid + 
//                           dt.ic_paid )                 as deposit_daily_disc_ic
//              from    daily_detail_file_dt    dt,
//                      mif                     mf
//              where   dt.load_filename = :loadFilename
//                      --and dt.batch_date = :cpd
//                      --and dt.ach_flag = 'Y'
//                      and mf.merchant_number = dt.merchant_account_number
//                      and 
//                      (
//                        (:bankNumber = 3943 and mf.bank_number = 3943 and mf.wf_conversion_date < dt.batch_date) or
//                        (:bankNumber = 3941 and mf.bank_number in (3941, 3943) and nvl(mf.wf_conversion_date, '31-Dec-9999') >= dt.batch_date) or
//                        (:bankNumber not in (3941, 3943) and :bankNumber = mf.bank_number)
//                      )
//              group by dt.batch_date, dt.ach_run_date                
//            )
//          select  :achFileId                        as ach_file_id,
//                  dt.ach_run_date                   as ach_run_date,
//                  dt.batch_date                     as batch_date,
//                  dt.deposit_count                  as deposit_count,
//                  dt.deposit_gross                  as deposit_gross,
//                  dt.deposit_daily_disc_ic          as deposit_disc_ic,
//                  (   dt.deposit_gross 
//                    - dt.deposit_daily_disc_ic )    as deposit_net,
//                  ach.ach_count                     as ach_count,
//                  ach.ach_gross                     as ach_gross,
//                  ach.ach_daily_disc_ic             as ach_disc_ic,
//                  ach.ach_net                       as ach_net,
//                  case
//                    when (dt.deposit_gross - dt.deposit_daily_disc_ic) = ach.ach_net then 'Y'
//                    else 'N'
//                  end                               as in_balance
//          from    ach, dt
//          where   dt.batch_date(+) = ach.batch_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "with\n          ach as\n          (\n            select  ats.batch_date                    as batch_date,\n                    count(1)                          as ach_count,\n                    sum( ats.sales_amount - \n                         ats.credits_amount )         as ach_gross,\n                    sum( ats.daily_discount_amount + \n                         ats.daily_ic_amount )        as ach_daily_disc_ic,\n                    sum( decode(substr(ats.transaction_code,-1),'7',-1,1) *\n                         ats.ach_amount )             as ach_net\n            from    ach_trident_statement   ats,\n                    mif                     mf\n            where   ats.load_file_id = load_filename_to_load_file_id( :1 )\n                    and mf.merchant_number = ats.merchant_number\n                    and \n                    (\n                      ( :2  = 3943 and mf.bank_number = 3943 and mf.wf_conversion_date < ats.batch_date) or\n                      ( :3  = 3941 and mf.bank_number in (3941, 3943) and nvl(mf.wf_conversion_date, '31-Dec-9999') >= ats.batch_date) or\n                      ( :4  not in (3941, 3943) and  :5  = mf.bank_number)\n                    )\n            group by ats.batch_date\n          ),\n          dt as\n          (\n            select  dt.batch_date                     as batch_date,\n                    dt.ach_run_date                   as ach_run_date,\n                    count(distinct dt.batch_number)   as deposit_count,\n                    sum( decode(dt.debit_credit_indicator,'C',-1,1) *\n                        dt.transaction_amount )       as deposit_gross,\n                    sum( dt.discount_paid + \n                         dt.ic_paid )                 as deposit_daily_disc_ic\n            from    daily_detail_file_dt    dt,\n                    mif                     mf\n            where   dt.load_filename =  :6 \n                    --and dt.batch_date = :cpd\n                    --and dt.ach_flag = 'Y'\n                    and mf.merchant_number = dt.merchant_account_number\n                    and \n                    (\n                      ( :7  = 3943 and mf.bank_number = 3943 and mf.wf_conversion_date < dt.batch_date) or\n                      ( :8  = 3941 and mf.bank_number in (3941, 3943) and nvl(mf.wf_conversion_date, '31-Dec-9999') >= dt.batch_date) or\n                      ( :9  not in (3941, 3943) and  :10  = mf.bank_number)\n                    )\n            group by dt.batch_date, dt.ach_run_date                \n          )\n        select   :11                         as ach_file_id,\n                dt.ach_run_date                   as ach_run_date,\n                dt.batch_date                     as batch_date,\n                dt.deposit_count                  as deposit_count,\n                dt.deposit_gross                  as deposit_gross,\n                dt.deposit_daily_disc_ic          as deposit_disc_ic,\n                (   dt.deposit_gross \n                  - dt.deposit_daily_disc_ic )    as deposit_net,\n                ach.ach_count                     as ach_count,\n                ach.ach_gross                     as ach_gross,\n                ach.ach_daily_disc_ic             as ach_disc_ic,\n                ach.ach_net                       as ach_net,\n                case\n                  when (dt.deposit_gross - dt.deposit_daily_disc_ic) = ach.ach_net then 'Y'\n                  else 'N'\n                end                               as in_balance\n        from    ach, dt\n        where   dt.batch_date(+) = ach.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"54com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setInt(3,bankNumber);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setInt(5,bankNumber);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setInt(7,bankNumber);
   __sJT_st.setInt(8,bankNumber);
   __sJT_st.setInt(9,bankNumber);
   __sJT_st.setInt(10,bankNumber);
   __sJT_st.setLong(11,achFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"54com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3215^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new DepositData( resultSet ) );
      }
      resultSet.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadDepositData(" + loadFileId + "," + cpd +")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadData( long orgId, Date beginDate, Date endDate )
  {
    switch( getReportType() )
    {
      case RT_DETAILS:
        loadDetailData(orgId,beginDate,endDate);
        break;
        
      case RT_CLEARING_SUMMARY:
        if ( "MC".equals( getData("cardType") ) )
        {
          loadClearingSummaryMasterCard();
        }
        else if ( "AM".equals( getData("cardType") ) )
        {
          loadClearingSummaryAmex();
        }
        else if ( "DS".equals( getData("cardType") ) )
        {
          loadClearingSummaryDiscover();
        }
        else
        {
          loadClearingSummaryVisa();
        }
        break;
        
      case RT_CARRYOVER_SUMMARY:
        loadCarryoverSummary();
        break;
        
      case RT_FUNDING_SUMMARY:
        loadFundingSummary();
        break;
        
      default:  // assume RT_SUMMARY
        loadSummaryData(orgId,beginDate,endDate);
        break;
    }
  }
  
  public void loadDetailData( long orgId, Date beginDate, Date endDate )
  {
    int                   bankNumber      = getInt("bankNumber",ReportUserBean.getBankNumber());
    ResultSetIterator     it              = null;
    long                  loadFileId      = 0L;
    String                loadFilename    = null;
    ResultSet             resultSet       = null;
    
    try
    {
      loadFileId  = getLong("lfid");
      
      /*@lineinfo:generated-code*//*@lineinfo:3287^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_file_id_to_load_filename(:loadFileId)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_file_id_to_load_filename( :1 )\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"55com.mes.reports.MbsReconcileDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFilename = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3292^7*/
      
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:3297^7*/

//  ************************************************************
//  #sql [Ctx] it = { with 
//            dt as 
//            (
//              select  decode(substr(dt.card_type,1,1),
//                             'V','VS','M','MC',
//                             dt.card_type)          as card_type,
//                      count(1)                      as tran_count,
//                      sum( decode(dt.debit_credit_indicator,'C',-1,1) *
//                          dt.transaction_amount )   as net_amount      
//              from    daily_detail_file_dt    dt,
//                      mif                     mf
//              where   dt.load_filename = :loadFilename
//                      and dt.batch_date = :beginDate
//                      and mf.merchant_number = dt.merchant_account_number
//                      and 
//                      (
//                        (:bankNumber = 3943 and mf.bank_number = 3943 and mf.wf_conversion_date < dt.batch_date) or
//                        (:bankNumber = 3941 and mf.bank_number in (3941, 3943) and nvl(mf.wf_conversion_date, '31-Dec-9999') >= dt.batch_date) or
//                        (:bankNumber not in (3941, 3943) and :bankNumber = mf.bank_number)
//                      )
//              group by decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type)
//            ),
//            settle as 
//            (
//              select  'VS'                          as card_type,
//                      sum( case when nvl(vs.settlement_date,'31-dec-9999') > :beginDate then 0
//                           else 1 end )             as current_count,
//                      sum( case when nvl(vs.settlement_date,'31-dec-9999') > :beginDate then 0
//                           else 1 end *
//                           decode(vs.debit_credit_indicator,'C',-1,1) *
//                           vs.transaction_amount )  as current_amount,
//                      sum( case when nvl(vs.settlement_date,'31-dec-9999') > :beginDate then 1
//                           else 0 end )             as carry_over_count,
//                      sum( case when nvl(vs.settlement_date,'31-dec-9999') > :beginDate then 1
//                           else 0 end *
//                           decode(vs.debit_credit_indicator,'C',-1,1) *
//                           vs.transaction_amount )  as carry_over_amount,
//                      count(1)                      as total_count,
//                      sum( decode(vs.debit_credit_indicator,'C',-1,1) *
//                          vs.transaction_amount )   as total_amount      
//              from    visa_settlement     vs
//              where   vs.load_file_id = :loadFileId
//                      and vs.batch_date = :beginDate
//                      and vs.bank_number = :bankNumber
//              union
//              select  'MC'                          as card_type,
//                      sum( case when nvl(mc.settlement_date,'31-dec-9999') > :beginDate then 0
//                           else 1 end )             as current_count,
//                      sum( case when nvl(mc.settlement_date,'31-dec-9999') > :beginDate then 0
//                           else 1 end *
//                           decode(mc.debit_credit_indicator,'C',-1,1) *
//                           mc.transaction_amount )  as current_amount,
//                      sum( case when nvl(mc.settlement_date,'31-dec-9999') > :beginDate then 1
//                           else 0 end )             as carry_over_count,
//                      sum( case when nvl(mc.settlement_date,'31-dec-9999') > :beginDate then 1
//                           else 0 end *
//                           decode(mc.debit_credit_indicator,'C',-1,1) *
//                           mc.transaction_amount )  as carry_over_amount,
//                      count(1)                      as total_count,
//                      sum( decode(mc.debit_credit_indicator,'C',-1,1) *
//                          mc.transaction_amount )   as total_amount      
//              from    mc_settlement       mc
//              where   mc.load_file_id = :loadFileId
//                      and mc.batch_date = :beginDate
//                      and mc.bank_number = :bankNumber
//            )            
//          select  :beginDate                as batch_date,
//                  :loadFilename             as load_filename,
//                  :loadFileId               as load_file_id,
//                  dt.card_type              as card_type,
//                  dt.tran_count             as settled_count, 
//                  settle.total_count        as outgoing_count,
//                  dt.net_amount             as settled_amount, 
//                  settle.total_amount       as outgoing_amount,
//                  case
//                    when dt.net_amount = settle.total_amount then 'Y'
//                    else 'N'
//                  end                       as in_balance,
//                  settle.current_count      as current_count,
//                  settle.current_amount     as current_amount,
//                  settle.carry_over_count   as carry_over_count,
//                  settle.carry_over_amount  as carry_over_amount,
//                  settle.total_count        as cleared_count,
//                  settle.carry_over_amount  as cleared_amount 
//          from    dt, settle
//          where   settle.card_type(+) = dt.card_type          
//          order by card_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "with \n          dt as \n          (\n            select  decode(substr(dt.card_type,1,1),\n                           'V','VS','M','MC',\n                           dt.card_type)          as card_type,\n                    count(1)                      as tran_count,\n                    sum( decode(dt.debit_credit_indicator,'C',-1,1) *\n                        dt.transaction_amount )   as net_amount      \n            from    daily_detail_file_dt    dt,\n                    mif                     mf\n            where   dt.load_filename =  :1 \n                    and dt.batch_date =  :2 \n                    and mf.merchant_number = dt.merchant_account_number\n                    and \n                    (\n                      ( :3  = 3943 and mf.bank_number = 3943 and mf.wf_conversion_date < dt.batch_date) or\n                      ( :4  = 3941 and mf.bank_number in (3941, 3943) and nvl(mf.wf_conversion_date, '31-Dec-9999') >= dt.batch_date) or\n                      ( :5  not in (3941, 3943) and  :6  = mf.bank_number)\n                    )\n            group by decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type)\n          ),\n          settle as \n          (\n            select  'VS'                          as card_type,\n                    sum( case when nvl(vs.settlement_date,'31-dec-9999') >  :7  then 0\n                         else 1 end )             as current_count,\n                    sum( case when nvl(vs.settlement_date,'31-dec-9999') >  :8  then 0\n                         else 1 end *\n                         decode(vs.debit_credit_indicator,'C',-1,1) *\n                         vs.transaction_amount )  as current_amount,\n                    sum( case when nvl(vs.settlement_date,'31-dec-9999') >  :9  then 1\n                         else 0 end )             as carry_over_count,\n                    sum( case when nvl(vs.settlement_date,'31-dec-9999') >  :10  then 1\n                         else 0 end *\n                         decode(vs.debit_credit_indicator,'C',-1,1) *\n                         vs.transaction_amount )  as carry_over_amount,\n                    count(1)                      as total_count,\n                    sum( decode(vs.debit_credit_indicator,'C',-1,1) *\n                        vs.transaction_amount )   as total_amount      \n            from    visa_settlement     vs\n            where   vs.load_file_id =  :11 \n                    and vs.batch_date =  :12 \n                    and vs.bank_number =  :13 \n            union\n            select  'MC'                          as card_type,\n                    sum( case when nvl(mc.settlement_date,'31-dec-9999') >  :14  then 0\n                         else 1 end )             as current_count,\n                    sum( case when nvl(mc.settlement_date,'31-dec-9999') >  :15  then 0\n                         else 1 end *\n                         decode(mc.debit_credit_indicator,'C',-1,1) *\n                         mc.transaction_amount )  as current_amount,\n                    sum( case when nvl(mc.settlement_date,'31-dec-9999') >  :16  then 1\n                         else 0 end )             as carry_over_count,\n                    sum( case when nvl(mc.settlement_date,'31-dec-9999') >  :17  then 1\n                         else 0 end *\n                         decode(mc.debit_credit_indicator,'C',-1,1) *\n                         mc.transaction_amount )  as carry_over_amount,\n                    count(1)                      as total_count,\n                    sum( decode(mc.debit_credit_indicator,'C',-1,1) *\n                        mc.transaction_amount )   as total_amount      \n            from    mc_settlement       mc\n            where   mc.load_file_id =  :18 \n                    and mc.batch_date =  :19 \n                    and mc.bank_number =  :20 \n          )            \n        select   :21                 as batch_date,\n                 :22              as load_filename,\n                 :23                as load_file_id,\n                dt.card_type              as card_type,\n                dt.tran_count             as settled_count, \n                settle.total_count        as outgoing_count,\n                dt.net_amount             as settled_amount, \n                settle.total_amount       as outgoing_amount,\n                case\n                  when dt.net_amount = settle.total_amount then 'Y'\n                  else 'N'\n                end                       as in_balance,\n                settle.current_count      as current_count,\n                settle.current_amount     as current_amount,\n                settle.carry_over_count   as carry_over_count,\n                settle.carry_over_amount  as carry_over_amount,\n                settle.total_count        as cleared_count,\n                settle.carry_over_amount  as cleared_amount \n        from    dt, settle\n        where   settle.card_type(+) = dt.card_type          \n        order by card_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"56com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setInt(3,bankNumber);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setInt(5,bankNumber);
   __sJT_st.setInt(6,bankNumber);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDate(8,beginDate);
   __sJT_st.setDate(9,beginDate);
   __sJT_st.setDate(10,beginDate);
   __sJT_st.setLong(11,loadFileId);
   __sJT_st.setDate(12,beginDate);
   __sJT_st.setInt(13,bankNumber);
   __sJT_st.setDate(14,beginDate);
   __sJT_st.setDate(15,beginDate);
   __sJT_st.setDate(16,beginDate);
   __sJT_st.setDate(17,beginDate);
   __sJT_st.setLong(18,loadFileId);
   __sJT_st.setDate(19,beginDate);
   __sJT_st.setInt(20,bankNumber);
   __sJT_st.setDate(21,beginDate);
   __sJT_st.setString(22,loadFilename);
   __sJT_st.setLong(23,loadFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"56com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3386^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new DetailData( resultSet ) );
      }
      resultSet.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadDetailData(" + loadFileId + "," + beginDate +")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void loadFundingSummary( )
  {
    int               bankNumber              = getInt("bankNumber",ReportUserBean.getBankNumber());
    int               bankNumberNew           = (bankNumber == 3941 ? 3943 : -1);
    Date              cpd                     = getReportDateBegin();
    String            dateClause              = null;
    FundingSummary    fundingSummary          = new FundingSummary();
    int               inc                     = 0;
    ResultSetIterator it                      = null;
    long              merchantId              = getLong("merchantId",0L);
    ResultSet         resultSet               = null;
    
    try
    {
      Calendar cal = Calendar.getInstance();
      cal.setTime(cpd);
    
      String toDateStr  = "to_date('" + DateTimeFormatter.getFormattedDate(cal.getTime(),"MM/dd/yyyy") + "','mm/dd/yyyy')";
    
      if ( getBoolean("sixDayFunding") )
      {
        inc = ((cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) ? 1 : 0);
      
        //
        // to improve report performace (particularly query 2) build
        // a dynamic date clause for the SQL statement.
        // if the CPD is sunday
        //    clause = "between to_date('11/14/2010','mm/dd/yyyy')-1 and to_date('11/14/2010','mm/dd/yyyy') 
        // else 
        //    clause = "= to_date('11/15/2010','mm/dd/yyyy')"
        //
        if ( cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY )
        {
          dateClause = "between " + toDateStr + "-1 and " + toDateStr;
        }
        else
        {
          dateClause = "= " + toDateStr;
        }
      }
      else
      {
        dateClause = "= " + toDateStr;
      }
      
      ReportRows.clear();
      ReportRows.add(fundingSummary);
      
      // query 1:  first load all the transactions for this cpd
      /*@lineinfo:generated-code*//*@lineinfo:3454^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /* index (sm pk_ddf_summary) */
//                  sum(   (sm.bank_sales_amount + sm.nonbank_sales_amount) 
//                       - (sm.bank_credits_amount + sm.nonbank_credits_amount) 
//                       - (sm.bank_reject_amount + sm.nonbank_reject_amount) 
//                     )                                                      as total_amount,
//                  sum( sm.visa_sales_amount
//                       - sm.visa_credits_amount   
//                       + nvl(sm.visa_fx_markup_amount,0) )                  as vs_amount,
//                  sum( sm.mc_sales_amount
//                       - sm.mc_credits_amount
//                       + nvl(sm.mc_fx_markup_amount,0) )                    as mc_amount,
//                  sum( sm.debit_sales_amount  - sm.debit_credits_amount  )  as db_amount,
//                  sum( case when sm.load_filename like '256s%' then 1 else 0 end *
//                       (sm.amex_sales_amount  - sm.amex_credits_amount) )   as am_amount_sabre,
//                  sum( case when sm.load_filename like '256s%' then 0 else 1 end *
//                       (sm.amex_sales_amount  - sm.amex_credits_amount) )   as am_amount,
//                  sum( sm.disc_sales_amount   - sm.disc_credits_amount )    as ds_amount,
//                  sum( sm.diners_sales_amount - sm.diners_credits_amount )  as dn_amount,
//                  sum( sm.jcb_sales_amount    - sm.jcb_credits_amount    )  as jc_amount,
//                  sum( case when sm.load_filename like '256s%' then 1 else 0 end *  -- uatp
//                       (sm.other_sales_amount - sm.other_credits_amount) )  as ot_amount_sabre,
//                  sum( case when sm.load_filename like '256s%' then 0 else 1 end *  -- not uatp
//                       case when sm.load_filename like 'bml%' then 0 else 1 end *   -- not bml
//                       (sm.other_sales_amount - sm.other_credits_amount) )  as ot_amount,
//                  sum( case when sm.load_filename like 'bml%' then 1 else 0 end *   -- bml
//                       (sm.other_sales_amount - sm.other_credits_amount) )  as bml_amount,
//                  sum( sm.bank_reject_amount + sm.nonbank_reject_amount )   as rejects_amount,
//                  sum( nvl(sm.visa_fx_markup_amount,0)
//                       + nvl(sm.mc_fx_markup_amount,0) )                    as fx_markup_amount,
//                  0                                                         as fx_markup_amount_amex
//          from    daily_detail_file_summary   sm
//          where   sm.merchant_number in (select merchant_number from mif where bank_number in (:bankNumber,:bankNumberNew) and :merchantId in (0,merchant_number))
//                  and sm.batch_date :dateClause
//                  --and sm.batch_date = :cpd
//                  and substr(sm.load_filename,1,3) not in ( 'ddf','bml')
//                  and sm.bank_number = :bankNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  /* index (sm pk_ddf_summary) */\n                sum(   (sm.bank_sales_amount + sm.nonbank_sales_amount) \n                     - (sm.bank_credits_amount + sm.nonbank_credits_amount) \n                     - (sm.bank_reject_amount + sm.nonbank_reject_amount) \n                   )                                                      as total_amount,\n                sum( sm.visa_sales_amount\n                     - sm.visa_credits_amount   \n                     + nvl(sm.visa_fx_markup_amount,0) )                  as vs_amount,\n                sum( sm.mc_sales_amount\n                     - sm.mc_credits_amount\n                     + nvl(sm.mc_fx_markup_amount,0) )                    as mc_amount,\n                sum( sm.debit_sales_amount  - sm.debit_credits_amount  )  as db_amount,\n                sum( case when sm.load_filename like '256s%' then 1 else 0 end *\n                     (sm.amex_sales_amount  - sm.amex_credits_amount) )   as am_amount_sabre,\n                sum( case when sm.load_filename like '256s%' then 0 else 1 end *\n                     (sm.amex_sales_amount  - sm.amex_credits_amount) )   as am_amount,\n                sum( sm.disc_sales_amount   - sm.disc_credits_amount )    as ds_amount,\n                sum( sm.diners_sales_amount - sm.diners_credits_amount )  as dn_amount,\n                sum( sm.jcb_sales_amount    - sm.jcb_credits_amount    )  as jc_amount,\n                sum( case when sm.load_filename like '256s%' then 1 else 0 end *  -- uatp\n                     (sm.other_sales_amount - sm.other_credits_amount) )  as ot_amount_sabre,\n                sum( case when sm.load_filename like '256s%' then 0 else 1 end *  -- not uatp\n                     case when sm.load_filename like 'bml%' then 0 else 1 end *   -- not bml\n                     (sm.other_sales_amount - sm.other_credits_amount) )  as ot_amount,\n                sum( case when sm.load_filename like 'bml%' then 1 else 0 end *   -- bml\n                     (sm.other_sales_amount - sm.other_credits_amount) )  as bml_amount,\n                sum( sm.bank_reject_amount + sm.nonbank_reject_amount )   as rejects_amount,\n                sum( nvl(sm.visa_fx_markup_amount,0)\n                     + nvl(sm.mc_fx_markup_amount,0) )                    as fx_markup_amount,\n                0                                                         as fx_markup_amount_amex\n        from    daily_detail_file_summary   sm\n        where   sm.merchant_number in (select merchant_number from mif where bank_number in ( ? , ? ) and  ?  in (0,merchant_number))\n                and sm.batch_date  ");
   __sjT_sb.append(dateClause);
   __sjT_sb.append(" \n                --and sm.batch_date = :cpd\n                and substr(sm.load_filename,1,3) not in ( 'ddf','bml')\n                and sm.bank_number =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "57com.mes.reports.MbsReconcileDataBean:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,bankNumberNew);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setInt(4,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3492^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        DdfData ddfData = new DdfData(resultSet);
        fundingSummary.addDdfData( ddfData );
      
        if ( getBoolean("amexOnly") )
        {
          double amexRejectsAmount = 0.0;
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:3505^13*/

//  ************************************************************
//  #sql [Ctx] { select  sum( case when dt.load_filename like '256s%' then 0 else 1 end
//                             * decode(dt.debit_credit_indicator,'C',-1,1)
//                             * dt.transaction_amount)
//                
//                from    daily_detail_file_dt    dt
//                where   dt.merchant_account_number in (select merchant_number from mif where bank_number in (:bankNumber,:bankNumberNew) and :merchantId in (0,merchant_number))
//                        and dt.batch_date between :cpd-:inc and :cpd
//                        and dt.card_type = 'AM'
//                        and dt.reject_reason is not null
//                        and dt.bank_number = :bankNumber
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( case when dt.load_filename like '256s%' then 0 else 1 end\n                           * decode(dt.debit_credit_indicator,'C',-1,1)\n                           * dt.transaction_amount)\n               \n              from    daily_detail_file_dt    dt\n              where   dt.merchant_account_number in (select merchant_number from mif where bank_number in ( :1 , :2 ) and  :3  in (0,merchant_number))\n                      and dt.batch_date between  :4 - :5  and  :6 \n                      and dt.card_type = 'AM'\n                      and dt.reject_reason is not null\n                      and dt.bank_number =  :7";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"58com.mes.reports.MbsReconcileDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,bankNumberNew);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setDate(4,cpd);
   __sJT_st.setInt(5,inc);
   __sJT_st.setDate(6,cpd);
   __sJT_st.setInt(7,bankNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amexRejectsAmount = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3517^13*/
          }
          catch(sqlj.runtime.SQLNullException sne)
          {
            // this just means that there weren't any rejects, so leave the default 0.0
          }
          ddfData.setRejectsAmountAmex(amexRejectsAmount);
        }
      }
      resultSet.close();
      it.close();
      
      // query 2 - second load all the ach statement entries for this cpd
      /*@lineinfo:generated-code*//*@lineinfo:3530^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      index (ats idx_ats_merch_batch_date) 
//                      use_hash(adtc,ats)
//                   */
//                  sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end
//                       * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end
//                       * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as std_standard_amount,
//                  sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end
//                       * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end
//                       * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as amex_standard_amount,
//                  sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end
//                       * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end
//                       * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as std_suspended_amount,
//                  sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end
//                       * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end
//                       * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as amex_suspended_amount,
//                  sum( case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 1 else 0 end
//                       * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as std_disqual_amount,
//                  sum( case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 1 else 0 end
//                       * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as amex_disqual_amount,
//                  sum( case when ats.entry_description = 'MERCH DEP' then 1 else 0 end
//                       * (ats.sales_amount - ats.credits_amount) )  
//                                                            as std_gross_amount,
//                  sum( case when ats.entry_description = 'AMEX DEP' then 1 else 0 end
//                       * (ats.sales_amount - ats.credits_amount) )  
//                                                            as amex_gross_amount,
//                  sum( case when ats.entry_description = 'MERCH DEP' then 1 else 0 end
//                       * (ats.daily_discount_amount + ats.daily_ic_amount) )  
//                                                            as std_disc_ic_amount,    
//                  sum( case when ats.entry_description = 'AMEX DEP' then 1 else 0 end
//                       * (ats.daily_discount_amount + ats.daily_ic_amount) )  
//                                                            as amex_disc_ic_amount,    
//                  sum( case when ats.entry_description = 'MERCH DEP' then 1 else 0 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1)
//                       * ats.ach_amount )                   as std_net_amount,
//                  sum( case when ats.entry_description = 'AMEX DEP' then 1 else 0 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1)
//                       * ats.ach_amount )                   as amex_net_amount
//          from    ach_trident_statement   ats,
//                  ach_detail_tran_code    adtc
//          where   ats.merchant_number in (select merchant_number from mif where bank_number in (:bankNumber,:bankNumberNew) and :merchantId in (0,merchant_number))
//                  --and ats.batch_date = :cpd
//                  and ats.batch_date :dateClause
//                  and ats.post_date between :cpd-15 and :cpd+180
//                  and ( ats.entry_description = 'AMEX DEP' 
//                        or ats.entry_description = nvl('MERCH DEP', uid))
//                  and ats.bank_number = :bankNumber                      
//                  and adtc.ach_detail_trans_code = ats.transaction_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  /*+ \n                    index (ats idx_ats_merch_batch_date) \n                    use_hash(adtc,ats)\n                 */\n                sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end\n                     * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end\n                     * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as std_standard_amount,\n                sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end\n                     * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end\n                     * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as amex_standard_amount,\n                sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end\n                     * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end\n                     * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as std_suspended_amount,\n                sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end\n                     * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end\n                     * case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 0 else 1 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as amex_suspended_amount,\n                sum( case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 1 else 0 end\n                     * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as std_disqual_amount,\n                sum( case when not ats.disqualified_date is null and get_cpd_ach(ats.disqualified_date) = get_cpd_ach(ats.batch_date) then 1 else 0 end\n                     * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as amex_disqual_amount,\n                sum( case when ats.entry_description = 'MERCH DEP' then 1 else 0 end\n                     * (ats.sales_amount - ats.credits_amount) )  \n                                                          as std_gross_amount,\n                sum( case when ats.entry_description = 'AMEX DEP' then 1 else 0 end\n                     * (ats.sales_amount - ats.credits_amount) )  \n                                                          as amex_gross_amount,\n                sum( case when ats.entry_description = 'MERCH DEP' then 1 else 0 end\n                     * (ats.daily_discount_amount + ats.daily_ic_amount) )  \n                                                          as std_disc_ic_amount,    \n                sum( case when ats.entry_description = 'AMEX DEP' then 1 else 0 end\n                     * (ats.daily_discount_amount + ats.daily_ic_amount) )  \n                                                          as amex_disc_ic_amount,    \n                sum( case when ats.entry_description = 'MERCH DEP' then 1 else 0 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1)\n                     * ats.ach_amount )                   as std_net_amount,\n                sum( case when ats.entry_description = 'AMEX DEP' then 1 else 0 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1)\n                     * ats.ach_amount )                   as amex_net_amount\n        from    ach_trident_statement   ats,\n                ach_detail_tran_code    adtc\n        where   ats.merchant_number in (select merchant_number from mif where bank_number in ( ? , ? ) and  ?  in (0,merchant_number))\n                --and ats.batch_date = :cpd\n                and ats.batch_date  ");
   __sjT_sb.append(dateClause);
   __sjT_sb.append(" \n                and ats.post_date between  ? -15 and  ? +180\n                and ( ats.entry_description = 'AMEX DEP' \n                      or ats.entry_description = nvl('MERCH DEP', uid))\n                and ats.bank_number =  ?                       \n                and adtc.ach_detail_trans_code = ats.transaction_code");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "59com.mes.reports.MbsReconcileDataBean:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,bankNumberNew);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setDate(4,cpd);
   __sJT_st.setDate(5,cpd);
   __sJT_st.setInt(6,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3592^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        fundingSummary.addAtsData( new AtsData(resultSet) );
      }
      resultSet.close();
      it.close();
      
      // query 3 - third load all the ach detail entries joined to statement entries for this cpd
      /*@lineinfo:generated-code*//*@lineinfo:3603^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      index (ad idx_ach_trident_dtl_mid_tdate)
//                      use_hash(adtc,ats) 
//                  */
//                  sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end ) 
//                                                            as standard_count, 
//                  sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end 
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as standard_amount, 
//                  sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end 
//                       * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end ) 
//                                                            as std_released_count, 
//                  sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end 
//                       * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as std_released_amount, 
//                  sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end 
//                       * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end ) 
//                                                            as amex_released_count, 
//                  sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end 
//                       * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as amex_released_amount, 
//                  sum( ats.daily_discount_amount + 
//                       ats.daily_ic_amount )                as daily_disc_ic, 
//                  count(1)                                  as total_count, 
//                  sum( decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                   as total_amount 
//          from    ach_trident_detail      ad, 
//                  ach_trident_statement   ats, 
//                  ach_detail_tran_code    adtc 
//          where   ad.merchant_number in (select merchant_number from mif where bank_number in (:bankNumber,:bankNumberNew) and :merchantId in (0,merchant_number))
//                  and ad.origin_node like :bankNumber||'%' 
//                  and ad.transmission_date = :cpd
//                  and ad.post_date between (:cpd-decode(:cpd,to_date('07/28/2010','mm/dd/yyyy'),30,2)) and :cpd
//                  and ( ad.entry_description = 'AMEX DEP' 
//                        or ad.entry_description = nvl('MERCH DEP', uid))
//                  and ad.tsys_load_filename is null   -- ignore all TSYS entries
//                  and ats.trace_number = ad.trace_number 
//                  and ats.post_date between :cpd-15 and :cpd+180
//                  and adtc.ach_detail_trans_code = ats.transaction_code 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                    index (ad idx_ach_trident_dtl_mid_tdate)\n                    use_hash(adtc,ats) \n                */\n                sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end ) \n                                                          as standard_count, \n                sum( case when get_cpd_ach(ats.batch_date) = ats.post_date_actual then 1 else 0 end \n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as standard_amount, \n                sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end \n                     * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end ) \n                                                          as std_released_count, \n                sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end \n                     * case when ats.entry_description = 'MERCH DEP' then 1 else 0 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as std_released_amount, \n                sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end \n                     * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end ) \n                                                          as amex_released_count, \n                sum( case when get_cpd_ach(ats.batch_date) != ats.post_date_actual then 1 else 0 end \n                     * case when ats.entry_description = 'AMEX DEP' then 1 else 0 end\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as amex_released_amount, \n                sum( ats.daily_discount_amount + \n                     ats.daily_ic_amount )                as daily_disc_ic, \n                count(1)                                  as total_count, \n                sum( decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                   as total_amount \n        from    ach_trident_detail      ad, \n                ach_trident_statement   ats, \n                ach_detail_tran_code    adtc \n        where   ad.merchant_number in (select merchant_number from mif where bank_number in ( :1 , :2 ) and  :3  in (0,merchant_number))\n                and ad.origin_node like  :4 ||'%' \n                and ad.transmission_date =  :5 \n                and ad.post_date between ( :6 -decode( :7 ,to_date('07/28/2010','mm/dd/yyyy'),30,2)) and  :8 \n                and ( ad.entry_description = 'AMEX DEP' \n                      or ad.entry_description = nvl('MERCH DEP', uid))\n                and ad.tsys_load_filename is null   -- ignore all TSYS entries\n                and ats.trace_number = ad.trace_number \n                and ats.post_date between  :9 -15 and  :10 +180\n                and adtc.ach_detail_trans_code = ats.transaction_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"60com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,bankNumberNew);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setDate(5,cpd);
   __sJT_st.setDate(6,cpd);
   __sJT_st.setDate(7,cpd);
   __sJT_st.setDate(8,cpd);
   __sJT_st.setDate(9,cpd);
   __sJT_st.setDate(10,cpd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"60com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3646^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        fundingSummary.addAtdData( new AtdData(resultSet) );
      }
      resultSet.close();
      it.close();
      
      // query 4 - fourth load only ach detail entries for validation cpd
      /*@lineinfo:generated-code*//*@lineinfo:3657^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      index (ad idx_ach_trident_dtl_mid_tdate) 
//                      use_hash(adtc,ad) 
//                  */
//                  sum( decode(ad.entry_description,'AMEX DEP',1,0)
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ad.ach_amount )                   as amex_dep_amount,
//                  sum( decode(ad.entry_description,'MERCH DEP',1,0)
//                       * decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ad.ach_amount )                   as merch_dep_amount,
//                  sum( decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ad.ach_amount )                   as total_amount 
//          from    ach_trident_detail      ad, 
//                  ach_detail_tran_code    adtc 
//          where   ad.merchant_number in (select merchant_number from mif where bank_number in (:bankNumber,:bankNumberNew) and :merchantId in (0,merchant_number))
//                  and ad.origin_node = :bankNumber*100000
//                  and ad.transmission_date = :cpd
//                  and ad.post_date between (:cpd-decode(:cpd,to_date('07/28/2010','mm/dd/yyyy'),30,2)) and :cpd
//                  and ( ad.entry_description = 'AMEX DEP' 
//                        or ad.entry_description = nvl('MERCH DEP', uid))
//                  and ad.tsys_load_filename is null   -- ignore all TSYS entries
//                  -- only pull files for the top level node
//                  and substr(ad.load_filename,2,13) = ('ach' || to_char(:bankNumber) || '00000_')
//                  and adtc.ach_detail_trans_code = ad.transaction_code 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                    index (ad idx_ach_trident_dtl_mid_tdate) \n                    use_hash(adtc,ad) \n                */\n                sum( decode(ad.entry_description,'AMEX DEP',1,0)\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ad.ach_amount )                   as amex_dep_amount,\n                sum( decode(ad.entry_description,'MERCH DEP',1,0)\n                     * decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ad.ach_amount )                   as merch_dep_amount,\n                sum( decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ad.ach_amount )                   as total_amount \n        from    ach_trident_detail      ad, \n                ach_detail_tran_code    adtc \n        where   ad.merchant_number in (select merchant_number from mif where bank_number in ( :1 , :2 ) and  :3  in (0,merchant_number))\n                and ad.origin_node =  :4 *100000\n                and ad.transmission_date =  :5 \n                and ad.post_date between ( :6 -decode( :7 ,to_date('07/28/2010','mm/dd/yyyy'),30,2)) and  :8 \n                and ( ad.entry_description = 'AMEX DEP' \n                      or ad.entry_description = nvl('MERCH DEP', uid))\n                and ad.tsys_load_filename is null   -- ignore all TSYS entries\n                -- only pull files for the top level node\n                and substr(ad.load_filename,2,13) = ('ach' || to_char( :9 ) || '00000_')\n                and adtc.ach_detail_trans_code = ad.transaction_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"61com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,bankNumberNew);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setDate(5,cpd);
   __sJT_st.setDate(6,cpd);
   __sJT_st.setDate(7,cpd);
   __sJT_st.setDate(8,cpd);
   __sJT_st.setInt(9,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"61com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3683^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        AtdData data = fundingSummary.getAtdData();
        data.addFileTotals(resultSet);
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e ) 
    {
      logEntry( "loadFundingSummary()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  public void loadSummaryData( long orgId, Date beginDate, Date endDate )
  {
    int                 bankNumber  = getInt("bankNumber",ReportUserBean.getBankNumber());
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
    
      /*@lineinfo:generated-code*//*@lineinfo:3715^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ordered */
//                  dt.batch_date                 as batch_date,
//                  lfi.load_file_id              as load_file_id,
//                  lfi.load_filename             as load_filename,
//                  lfi.creation_date             as file_ts,
//                  count(1)                      as record_count,
//                  sum( decode(dt.debit_credit_indicator,'D',1,0) )
//                                                as sales_count,
//                  sum( decode(dt.debit_credit_indicator,'D',1,0) *
//                       dt.transaction_amount )   as sales_amount,
//                  sum( decode(dt.cash_disbursement,'Y',1,0) )
//                                                as ca_count,
//                  sum( decode(dt.cash_disbursement,'Y',1,0) *
//                       dt.transaction_amount )   as ca_amount,
//                  sum( decode(dt.debit_credit_indicator,'C',1,0) )
//                                                as credits_count,
//                  sum( decode(dt.debit_credit_indicator,'C',1,0) *
//                      dt.transaction_amount )   as credits_amount,
//                  sum( decode(dt.debit_credit_indicator,'C',-1,1) *
//                      dt.transaction_amount )   as net_amount      
//          from    mif                       mf,
//                  daily_detail_file_dt      dt,
//                  load_file_index           lfi
//          where   mf.bank_number in (:bankNumber,decode(:bankNumber,3941,3943,-1))
//                  and dt.merchant_account_number = mf.merchant_number
//                  and dt.batch_date = :beginDate
//                  and 
//                  (
//                       dt.load_filename like 'cdf%'     -- vital cdf files
//                    or dt.load_filename like 'mddf%'    -- mes ddf files
//                    or dt.load_filename like '256s%'    -- sabre d256 files
//                  )
//                  and dt.bank_number = :bankNumber
//                  and lfi.load_filename = dt.load_filename
//          group by dt.batch_date, lfi.load_file_id, lfi.load_filename, lfi.creation_date
//          order by load_file_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ ordered */\n                dt.batch_date                 as batch_date,\n                lfi.load_file_id              as load_file_id,\n                lfi.load_filename             as load_filename,\n                lfi.creation_date             as file_ts,\n                count(1)                      as record_count,\n                sum( decode(dt.debit_credit_indicator,'D',1,0) )\n                                              as sales_count,\n                sum( decode(dt.debit_credit_indicator,'D',1,0) *\n                     dt.transaction_amount )   as sales_amount,\n                sum( decode(dt.cash_disbursement,'Y',1,0) )\n                                              as ca_count,\n                sum( decode(dt.cash_disbursement,'Y',1,0) *\n                     dt.transaction_amount )   as ca_amount,\n                sum( decode(dt.debit_credit_indicator,'C',1,0) )\n                                              as credits_count,\n                sum( decode(dt.debit_credit_indicator,'C',1,0) *\n                    dt.transaction_amount )   as credits_amount,\n                sum( decode(dt.debit_credit_indicator,'C',-1,1) *\n                    dt.transaction_amount )   as net_amount      \n        from    mif                       mf,\n                daily_detail_file_dt      dt,\n                load_file_index           lfi\n        where   mf.bank_number in ( :1 ,decode( :2 ,3941,3943,-1))\n                and dt.merchant_account_number = mf.merchant_number\n                and dt.batch_date =  :3 \n                and \n                (\n                     dt.load_filename like 'cdf%'     -- vital cdf files\n                  or dt.load_filename like 'mddf%'    -- mes ddf files\n                  or dt.load_filename like '256s%'    -- sabre d256 files\n                )\n                and dt.bank_number =  :4 \n                and lfi.load_filename = dt.load_filename\n        group by dt.batch_date, lfi.load_file_id, lfi.load_filename, lfi.creation_date\n        order by load_file_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"62com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setInt(4,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"62com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3753^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new SummaryData(resultSet) );
      }
      resultSet.close();
      
/***********************************************      
//@        ReportRows.addElement( new ReconciliationEntry( "sales"   , resultSet.getInt("sales_count"), resultSet.getDouble("sales_amount") );
//@        ReportRows.addElement( new ReconciliationEntry( "credits" , resultSet.getInt("credits_count"), resultSet.getDouble("credits_amount") );
//@        ReportRows.addElement( new ReconciliationEntry( "cash adv", resultSet.getInt("credits_count"), resultSet.getDouble("credits_amount") );
//@        ReportRows.addElement( new ReconciliationEntry( "rejects" , resultSet.getInt("cash_adv_count"), resultSet.getDouble("cash_adv_amount") );
      
      #sql [Ctx] it = 
      { 
        select  count(1)                                          as cb_count,
                sum( decode(cb.debit_credit_indicator,'C',-1,1) *
                     cb.tran_amount )                             as cb_amount
        from    network_chargebacks           cb,
                network_chargeback_activity   cba
        where   cba.load_filename = 'vs_wcb3941_032510_001.ctf'
                and cb.cb_load_sec = cba.cb_load_sec
      };
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.addElement( new ReconciliationEntry( "sales"   , resultSet.getInt("sales_count"), resultSet.getDouble("sales_amount") );
        ReportRows.addElement( new ReconciliationEntry( "credits" , resultSet.getInt("credits_count"), resultSet.getDouble("credits_amount") );
        ReportRows.addElement( new ReconciliationEntry( "cash adv", resultSet.getInt("credits_count"), resultSet.getDouble("credits_amount") );
        ReportRows.addElement( new ReconciliationEntry( "rejects" , resultSet.getInt("cash_adv_count"), resultSet.getDouble("cash_adv_amount") );
      }
      resultSet.close();
***********************************************/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadSummaryData(" + beginDate + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public String getLoadFilename( long loadFileId )
  {
    String    loadFilename      = "";
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:3805^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_file_id_to_load_filename(:loadFileId)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_file_id_to_load_filename( :1 )\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"63com.mes.reports.MbsReconcileDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFilename = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3810^7*/
    }
    catch( Exception e )
    {
      logEntry("getLoadFilename(" + loadFileId + ")",e.toString());
    }
    return( loadFilename );
  }
  
  protected void postHandleRequest( HttpServletRequest request )
  {
    super.postHandleRequest( request );
    
    Calendar cal = null;
  
    if ( HttpHelper.getString(request,"beginDate.month",null) == null )
    {
      cal = Calendar.getInstance();
      cal.add( Calendar.DAY_OF_MONTH, -1 );
      ((ComboDateField)getField("beginDate")).setUtilDate( cal.getTime() );
    }
    
    if ( HttpHelper.getString(request,"sixDayFunding",null) == null )
    {
      setData("sixDayFunding","true");  // default to 6 day funding
    }
    
    if ( HttpHelper.getString(request,"bankNumber",null) == null )
    {
      setData("bankNumber",String.valueOf(getReportBankIdDefault()));
    }
    
    switch ( getReportType() )
    {
      case RT_FUNDING_SUMMARY:
        if ( getBoolean("sixDayFunding") )
        {
          cal = Calendar.getInstance();
          cal.setTime(getReportDateBegin());
          if ( cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY )
          {
            cal.add(Calendar.DAY_OF_MONTH,1);   // ach is sunday through friday
          }
          setReportDateBegin( new java.sql.Date(cal.getTime().getTime()) );
        }
        break;
     
      case RT_CLEARING_SUMMARY:
        if ( "MC,DS".indexOf(getData("cardType")) >= 0 )
        {
          cal = checkMcDsReportDate(getReportDateBegin());
          setReportDateBegin(new java.sql.Date(cal.getTime().getTime()));

          cal = checkMcDsReportDate(getReportDateEnd());
          setReportDateEnd(new java.sql.Date(cal.getTime().getTime()));
        }
        else if ( "AM".equals(getData("cardType")) )
        {
          cal = checkAmReportDate(getReportDateBegin());
          setReportDateBegin(new java.sql.Date(cal.getTime().getTime()));

          cal = checkAmReportDate(getReportDateEnd());
          setReportDateEnd(new java.sql.Date(cal.getTime().getTime()));
        }

        break;
    }      
  }

  private Calendar checkMcDsReportDate(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    if ( cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY )
    {
      cal.add(Calendar.DAY_OF_MONTH,1);   // clearing is monday through saturday
    }

    return cal;
  }

  private Calendar checkAmReportDate(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    if ( cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY )
    {
      cal.add(Calendar.DAY_OF_MONTH,-1);  // amex sets the cpd to friday for friday and saturday volume
    }

    return cal;
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
  
  public void validateAtsToAtd( )
  {
    int               bankNumber              = getInt("bankNumber",ReportUserBean.getBankNumber());
    Date              cpd                     = getReportDateBegin();
    int               inc                     = 0;
    ResultSetIterator it                      = null;
    ResultSet         resultSet               = null;
    
    try
    {
      Calendar cal = Calendar.getInstance();
      cal.setTime(cpd);
      inc = ((cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) ? 1 : 0);
      
      ReportRows.clear();
      
      HashMap atsMap      = new HashMap();
      HashMap atdMap      = new HashMap();
      String  traceNumber = null;
      Object  value       = null;
      double  atsAmount   = 0.0;
      double  atdAmount   = 0.0;
      
      System.out.println("Collecting ATD data...");//@
      /*@lineinfo:generated-code*//*@lineinfo:3929^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (ad idx_ach_trident_dtl_mid_tdate) */
//                  ad.trace_number                           as trace_number,
//                  sum( decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ad.ach_amount )                    as ach_amount
//          from    mif mf,
//                  ach_trident_detail      ad,
//                  ach_detail_tran_code    adtc
//          where   mf.bank_number not in (1925, 3870, 1001, 1000, 1010, 3860)
//                  and mf.merchant_number = ad.merchant_number
//                  and ad.transmission_date = :cpd
//                  and ad.post_date between :cpd-2 and :cpd
//                  and ad.entry_description in ('AMEX DEP','MERCH DEP' )
//                  --and ad.merchant_number != 941000001088 -- ignore payment accounts
//                  and substr(ad.load_filename,2,13) = ('ach' || to_char(:bankNumber) || '00000_')
//                  and adtc.ach_detail_trans_code = ad.transaction_code
//                  and 
//                  (
//                    (:bankNumber = 3943 and mf.bank_number = 3943 and mf.wf_conversion_date < ad.ach_run_date) or
//                    (:bankNumber = 3941 and mf.bank_number in (3941, 3943) and nvl(mf.wf_conversion_date, '31-Dec-9999') >= ad.ach_run_date) or
//                    (:bankNumber not in (3941, 3943) and :bankNumber = mf.bank_number)
//                  )
//          group by ad.trace_number
//          order by ad.trace_number      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (ad idx_ach_trident_dtl_mid_tdate) */\n                ad.trace_number                           as trace_number,\n                sum( decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ad.ach_amount )                    as ach_amount\n        from    mif mf,\n                ach_trident_detail      ad,\n                ach_detail_tran_code    adtc\n        where   mf.bank_number not in (1925, 3870, 1001, 1000, 1010, 3860)\n                and mf.merchant_number = ad.merchant_number\n                and ad.transmission_date =  :1 \n                and ad.post_date between  :2 -2 and  :3 \n                and ad.entry_description in ('AMEX DEP','MERCH DEP' )\n                --and ad.merchant_number != 941000001088 -- ignore payment accounts\n                and substr(ad.load_filename,2,13) = ('ach' || to_char( :4 ) || '00000_')\n                and adtc.ach_detail_trans_code = ad.transaction_code\n                and \n                (\n                  ( :5  = 3943 and mf.bank_number = 3943 and mf.wf_conversion_date < ad.ach_run_date) or\n                  ( :6  = 3941 and mf.bank_number in (3941, 3943) and nvl(mf.wf_conversion_date, '31-Dec-9999') >= ad.ach_run_date) or\n                  ( :7  not in (3941, 3943) and  :8  = mf.bank_number)\n                )\n        group by ad.trace_number\n        order by ad.trace_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"64com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setDate(3,cpd);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setInt(5,bankNumber);
   __sJT_st.setInt(6,bankNumber);
   __sJT_st.setInt(7,bankNumber);
   __sJT_st.setInt(8,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"64com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3954^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        atdMap.put( resultSet.getString("trace_number"), resultSet.getDouble("ach_amount"));
      }
      resultSet.close();
      it.close();
      
      System.out.println("Collecting ATS data...");//@
      /*@lineinfo:generated-code*//*@lineinfo:3965^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      index (ad idx_ach_trident_dtl_mid_tdate)  
//                      index (ats idx_ach_trident_stmt_trace)      
//                  */
//                  ad.trace_number                             as trace_number,
//                  sum( decode(adtc.debit_credit_indicator, 'C', 1, -1) 
//                       * ats.ach_amount )                     as ach_amount
//          from    mif mf,
//                  ach_trident_detail      ad,                
//                  ach_trident_statement   ats,
//                  ach_detail_tran_code    adtc
//          where   mf.bank_number not in (1925, 3870, 1001, 1000, 1010, 3860)
//                  and mf.merchant_number = ad.merchant_number
//                  and ad.transmission_date = :cpd
//                  and ad.post_date between :cpd-2 and :cpd
//                  and ad.entry_description in ('AMEX DEP','MERCH DEP' )
//                  and ats.trace_number = ad.trace_number 
//                  and adtc.ach_detail_trans_code = ats.transaction_code
//                  and 
//                  (
//                    (:bankNumber = 3943 and mf.bank_number = 3943 and mf.wf_conversion_date < ad.ach_run_date) or
//                    (:bankNumber = 3941 and mf.bank_number in (3941, 3943) and nvl(mf.wf_conversion_date, '31-Dec-9999') >= ad.ach_run_date) or
//                    (:bankNumber not in (3941, 3943) and :bankNumber = mf.bank_number)
//                  )
//                  --and ats.batch_date = ats.post_date_actual
//          group by ad.trace_number
//          order by ad.trace_number      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                    index (ad idx_ach_trident_dtl_mid_tdate)  \n                    index (ats idx_ach_trident_stmt_trace)      \n                */\n                ad.trace_number                             as trace_number,\n                sum( decode(adtc.debit_credit_indicator, 'C', 1, -1) \n                     * ats.ach_amount )                     as ach_amount\n        from    mif mf,\n                ach_trident_detail      ad,                \n                ach_trident_statement   ats,\n                ach_detail_tran_code    adtc\n        where   mf.bank_number not in (1925, 3870, 1001, 1000, 1010, 3860)\n                and mf.merchant_number = ad.merchant_number\n                and ad.transmission_date =  :1 \n                and ad.post_date between  :2 -2 and  :3 \n                and ad.entry_description in ('AMEX DEP','MERCH DEP' )\n                and ats.trace_number = ad.trace_number \n                and adtc.ach_detail_trans_code = ats.transaction_code\n                and \n                (\n                  ( :4  = 3943 and mf.bank_number = 3943 and mf.wf_conversion_date < ad.ach_run_date) or\n                  ( :5  = 3941 and mf.bank_number in (3941, 3943) and nvl(mf.wf_conversion_date, '31-Dec-9999') >= ad.ach_run_date) or\n                  ( :6  not in (3941, 3943) and  :7  = mf.bank_number)\n                )\n                --and ats.batch_date = ats.post_date_actual\n        group by ad.trace_number\n        order by ad.trace_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"65com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,cpd);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setDate(3,cpd);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setInt(5,bankNumber);
   __sJT_st.setInt(6,bankNumber);
   __sJT_st.setInt(7,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"65com.mes.reports.MbsReconcileDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3994^7*/
      resultSet = it.getResultSet();
      
      while ( resultSet.next() )
      {
        atsMap.put( resultSet.getString("trace_number"), resultSet.getDouble("ach_amount"));
      }
      resultSet.close();
      it.close();
      
      System.out.println("Scanning ATD => ATS...");
      for( Iterator i = atdMap.keySet().iterator(); i.hasNext(); )
      {
        traceNumber = (String)i.next();
        atsAmount = ((value = atsMap.get(traceNumber)) == null ? 0.0 : ((Double)value).doubleValue());
        atdAmount = ((value = atdMap.get(traceNumber)) == null ? 0.0 : ((Double)value).doubleValue());
        
        if ( atdAmount != atsAmount ) 
        {
          System.out.println("  " + traceNumber + " " + atsAmount + " " + atdAmount + " " + MesMath.toCurrency(atsAmount-atdAmount));
        }
      }
      
      System.out.println("Scanning ATS => ATD...");
      for( Iterator i = atsMap.keySet().iterator(); i.hasNext(); )
      {
        traceNumber = (String)i.next();
        atsAmount = ((value = atsMap.get(traceNumber)) == null ? 0.0 : ((Double)value).doubleValue());
        atdAmount = ((value = atdMap.get(traceNumber)) == null ? 0.0 : ((Double)value).doubleValue());
        
        if ( atdAmount != atsAmount ) 
        {
          System.out.println("  " + traceNumber + " " + atsAmount + " " + atdAmount + " " + MesMath.toCurrency(atsAmount-atdAmount));
        }
      }
    }
    catch( Exception e ) 
    {
      logEntry( "validateAtsToAtd()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }

  public void storeClearingSummary(int bankNumber, String cardType, Date beginDate) {
    ClearingSummary entry = (ClearingSummary)ReportRows.elementAt(0);
    HashMap map = entry.getBinData();

    Set keys =  map.keySet();
    BinSummary binSummary = null;
    String binNum = "";
    //for(Object key : keys) {
    for( Iterator it = keys.iterator(); it.hasNext(); ) {
      //binNum = (String)key;
      binNum = (String)it.next();
      System.out.println("storing daily clearing summary data for: " + bankNumber + ", " + cardType + ", " + binNum);
      binSummary = (BinSummary)map.get(binNum);
        try {
          /*@lineinfo:generated-code*//*@lineinfo:4054^11*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_clearing_summary
//              (
//                actual_amount,
//                assoc_fees,
//                bank_number,
//                bin_number,
//                card_type,
//                cash_advance_amount,
//                clearing_date,
//                credits_amount,
//                fee_collection_amount,
//                funding_amount,
//                fx_adjustment_amount,
//                incoming_chargebacks_amount,
//                incoming_reversals_amount,
//                reimbursement_fees,
//                rejects_amount,
//                reprocessed_rejects_amount,
//                reversals_amount,
//                sales_amount,
//                second_presentments_amount,
//                second_presentment_rejects_amt
//              )
//              values
//              (
//                :binSummary.getActualAmount(),
//                :binSummary.getAssocFees(),
//                :bankNumber,
//                :binNum,
//                :cardType,
//                :binSummary.getCashAdvanceAmount(),
//                :beginDate,
//                :binSummary.getCreditsAmount(),
//                :binSummary.getFeeCollectionAmount(),
//                :binSummary.getFundingAmount(),
//                :binSummary.getFxAdjustmentAmount(),
//                :binSummary.getIncomingChargebacksAmount(),
//                :binSummary.getIncomingReversalsAmount(),
//                :binSummary.getReimbursementFees(),
//                :binSummary.getRejectsAmount(),
//                :binSummary.getReprocessedRejectsAmount(),
//                :binSummary.getReversalsAmount(),
//                :binSummary.getSalesAmount(),
//                :binSummary.getSecondPresentmentsAmount(),
//                :binSummary.getSecondPresentmentRejectsAmount()
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_1125 = binSummary.getActualAmount();
 double __sJT_1126 = binSummary.getAssocFees();
 double __sJT_1127 = binSummary.getCashAdvanceAmount();
 double __sJT_1128 = binSummary.getCreditsAmount();
 double __sJT_1129 = binSummary.getFeeCollectionAmount();
 double __sJT_1130 = binSummary.getFundingAmount();
 double __sJT_1131 = binSummary.getFxAdjustmentAmount();
 double __sJT_1132 = binSummary.getIncomingChargebacksAmount();
 double __sJT_1133 = binSummary.getIncomingReversalsAmount();
 double __sJT_1134 = binSummary.getReimbursementFees();
 double __sJT_1135 = binSummary.getRejectsAmount();
 double __sJT_1136 = binSummary.getReprocessedRejectsAmount();
 double __sJT_1137 = binSummary.getReversalsAmount();
 double __sJT_1138 = binSummary.getSalesAmount();
 double __sJT_1139 = binSummary.getSecondPresentmentsAmount();
 double __sJT_1140 = binSummary.getSecondPresentmentRejectsAmount();
   String theSqlTS = "insert into daily_clearing_summary\n            (\n              actual_amount,\n              assoc_fees,\n              bank_number,\n              bin_number,\n              card_type,\n              cash_advance_amount,\n              clearing_date,\n              credits_amount,\n              fee_collection_amount,\n              funding_amount,\n              fx_adjustment_amount,\n              incoming_chargebacks_amount,\n              incoming_reversals_amount,\n              reimbursement_fees,\n              rejects_amount,\n              reprocessed_rejects_amount,\n              reversals_amount,\n              sales_amount,\n              second_presentments_amount,\n              second_presentment_rejects_amt\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 ,\n               :7 ,\n               :8 ,\n               :9 ,\n               :10 ,\n               :11 ,\n               :12 ,\n               :13 ,\n               :14 ,\n               :15 ,\n               :16 ,\n               :17 ,\n               :18 ,\n               :19 ,\n               :20 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"66com.mes.reports.MbsReconcileDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_1125);
   __sJT_st.setDouble(2,__sJT_1126);
   __sJT_st.setInt(3,bankNumber);
   __sJT_st.setString(4,binNum);
   __sJT_st.setString(5,cardType);
   __sJT_st.setDouble(6,__sJT_1127);
   __sJT_st.setDate(7,beginDate);
   __sJT_st.setDouble(8,__sJT_1128);
   __sJT_st.setDouble(9,__sJT_1129);
   __sJT_st.setDouble(10,__sJT_1130);
   __sJT_st.setDouble(11,__sJT_1131);
   __sJT_st.setDouble(12,__sJT_1132);
   __sJT_st.setDouble(13,__sJT_1133);
   __sJT_st.setDouble(14,__sJT_1134);
   __sJT_st.setDouble(15,__sJT_1135);
   __sJT_st.setDouble(16,__sJT_1136);
   __sJT_st.setDouble(17,__sJT_1137);
   __sJT_st.setDouble(18,__sJT_1138);
   __sJT_st.setDouble(19,__sJT_1139);
   __sJT_st.setDouble(20,__sJT_1140);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4101^11*/

          int updateCount = Ctx.getExecutionContext().getUpdateCount();
          if(updateCount==0) {
            logEntry( "MbsReconcileDataBean.storeClearingSummary()", "Error storing clearing summary for card type " + cardType + ", bank number " + bankNumber + ", and report date " + beginDate.toString());
          }
        } catch( Exception e ) {
          logEntry( "MbsReconcileDataBean.storeClearingSummary()", e.toString() );
        }
      }
  }
  
  public void validateFundingData( Date beginDate, Date endDate )
  {
//@    Date                  beginDate         = getReportDateBegin();
//@    Date                  endDate           = getReportDateEnd();
    FundingSummary        entry             = null;
    double                achOutgoing       = 0.0;
    double                variance          = 0.0;
    Date                  reportDate        = null;
    
    try
    {
      Calendar    cal = Calendar.getInstance();
      cal.clear();
      cal.setTime(endDate);
      
      Calendar    start = Calendar.getInstance();
      start.clear();
      start.setTime(beginDate);
      
      System.out.println("Validating " + beginDate + " to " + endDate );
      while( cal.getTime().after( start.getTime() ) )
      {
        if ( cal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY ) 
        {
          reportDate = new java.sql.Date(cal.getTime().getTime());
          System.out.print("Scanning " + reportDate );
        
          setReportDateBegin( reportDate );
          loadFundingSummary();
          entry = (FundingSummary)ReportRows.elementAt(0);
        
          if ( entry.getTotalSettledAmount() != entry.getGrossFundedAmount() )
          {
            double ddfToAtsDiff = (entry.getTotalSettledAmount() - entry.getGrossFundedAmount());
            System.out.print("  DDF to ATS: " + MesMath.toCurrency(ddfToAtsDiff));
          }
          else 
          {
            System.out.print("  DDF to ATS: Balanced");
          }
        
          achOutgoing   = entry.getNetFundedAmount() 
                          - entry.getSuspendedAmountStd()
                          - entry.getSuspendedAmountAmex()
                          - entry.getDisqualAmount()
                          + entry.getReleasedAmountStd()
                          + entry.getReleasedAmountAmex();
          variance      = achOutgoing - entry.getTotalDepAmount();
        
          if ( !"$0.00".equals(MesMath.toCurrency(variance)) )
          {
            System.out.print("  ATS to ATD: " + MesMath.toCurrency(variance));
          }
          else 
          {
            System.out.print("  ATS to ATD: Balanced");
          }
          System.out.println();
        }          
        cal.add(Calendar.DAY_OF_MONTH,-1);
      }
    }
    catch( Exception e )
    {
      logEntry( "validateFundingData(" + beginDate + "," + endDate + ")", e.toString() );
    }
    finally
    {
    }
  }
  
  public static void main( String[] args )
  {
    MbsReconcileDataBean    dataBean  = null;
    
    try
    {
      com.mes.database.SQLJConnectionBase.initStandalone("DEBUG");
      
      dataBean = new MbsReconcileDataBean();
      
      dataBean.connect(true);
      dataBean.initialize();
      dataBean.autoSetFields((HttpServletRequest)null);
      
      if ( "validateAtsToAtd".equals(args[0]) )
      {
        Date reportDate = new java.sql.Date( DateTimeFormatter.parseDate(args[1],"MM/dd/yyyy").getTime() );
        dataBean.setReportDateBegin(reportDate);
        dataBean.validateAtsToAtd();
      }
      else if ( "validateFundingData".equals(args[0]) )
      {
//@        dataBean.setReportDateBegin( new java.sql.Date( DateTimeFormatter.parseDate(args[1],"MM/dd/yyyy").getTime() ) );
//@        dataBean.setReportDateEnd  ( new java.sql.Date( DateTimeFormatter.parseDate(args[2],"MM/dd/yyyy").getTime() ) );
        Date beginDate  = new java.sql.Date( DateTimeFormatter.parseDate(args[1],"MM/dd/yyyy").getTime() );
        Date endDate    = new java.sql.Date( DateTimeFormatter.parseDate(args[2],"MM/dd/yyyy").getTime() );
        dataBean.validateFundingData(beginDate,endDate);
      }
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
      try { dataBean.cleanUp(); } catch( Exception ee ) {}
    }
  }
}/*@lineinfo:generated-code*/