package com.mes.reports.mc;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import com.mes.database.MesResultSet;
import com.mes.reports.DailyClearingSummaryReport;
import com.mes.reports.MbsReconcileDataBean.ClearingSummary;

public class MasterCardClearingSummary extends DailyClearingSummaryReport {

	private static final Logger log = Logger.getLogger(MasterCardClearingSummary.class);

	//@formatter:off
	
	  String preparedSqlAddFirstPresentment=
			 	" SELECT /*+ index (mc idx_mc_ica_settle_date) */ "
			 	+" mc.bin_number AS bin_number,  "
	            +" SUM( DECODE(mc.cash_disbursement,'Y',0,1) * DECODE(mc.debit_credit_indicator,'C',0,1) )  AS sales_count, "
	            +" SUM( ROUND( (  DECODE(mc.cash_disbursement,'Y',0,1)  * DECODE(mc.debit_credit_indicator,'C',0,1)  * "
	            +" mc.funding_amount ),2 ) )                 AS sales_amount, "
	            +" SUM( DECODE(mc.cash_disbursement,'Y',0,1)             * DECODE(mc.debit_credit_indicator,'C',1,0) )  AS credits_count, "
	            +" SUM( ROUND( (  DECODE(mc.cash_disbursement,'Y',0,1)  * DECODE(mc.debit_credit_indicator,'C',1,0)  * "
	            +" mc.funding_amount ),2 ) )          AS credits_amount, "
	            +" SUM( DECODE(mc.cash_disbursement,'Y',1,0) ) AS ca_count,"
	            +" SUM( ROUND( (  DECODE(mc.cash_disbursement,'Y',1,0) * "
	            +" mc.funding_amount ),2 ) ) AS ca_amount ,"
	            +" SUM( DECODE(mc.debit_credit_indicator,'C',-1,1) * NVL(mc.funding_amount,mc.transaction_amount) ) AS funding_amount "
	            +" FROM mc_settlement mc"
	            +" WHERE mc.bank_number = ? "
	            +" AND mc.settlement_date BETWEEN ? - ? AND ? "
	            +" GROUP BY mc.bin_number";
	
		
	  String preparedSqlAddReprocessedRejects=" SELECT mc.bin_number AS bin_number, "
				+ " SUM( DECODE(mca.action_code,'X',1,0)             * DECODE(mc.cash_disbursement,'Y',0,1)  * DECODE(mc.debit_credit_indicator,'C',0,1) )  AS sales_count,"
				+ " SUM( ROUND( (  DECODE(mca.action_code,'X',1,0)  * DECODE(mc.cash_disbursement,'Y',0,1)  * DECODE(mc.debit_credit_indicator,'C',0,1)  * "
				+ " mc.funding_amount ),2 ) ) as sales_amount, "
				+ " SUM( DECODE(mca.action_code,'X',1,0) "
				+ "    * DECODE(mc.cash_disbursement,'Y',0,1)"
				+ "    * DECODE(mc.debit_credit_indicator,'C',1,0) )  AS credits_count, "
				+ " SUM( ROUND( (  DECODE(mca.action_code,'X',1,0)  * DECODE(mc.cash_disbursement,'Y',0,1)  * DECODE(mc.debit_credit_indicator,'C',1,0)  * "
				+ " mc.funding_amount ),2 ) )          AS credits_amount,"
				+ " SUM( DECODE(mca.action_code,'X',1,0) * DECODE(mc.cash_disbursement,'Y',1,0) ) AS ca_count,"
				+ " SUM( ROUND( (  DECODE(mca.action_code,'X',1,0)  * DECODE(mc.cash_disbursement,'Y',1,0)  * "
				+ " mc.funding_amount ),2 ) )     AS ca_amount,"
				+ " SUM( DECODE(mca.action_code,'X',1,0) ) AS reject_count,"
				+ " SUM( ROUND( (  DECODE(mca.action_code,'X',1,0)  * DECODE(mc.debit_credit_indicator,'C',-1,1)  *"
				+ " mc.funding_amount ),2 ) )     AS reject_amount, "
				+ " SUM( DECODE(mca.action_code,'R',1,0) ) AS reversal_count, "
				+ " SUM( ROUND( (  DECODE(mca.action_code,'R',1,0)  * DECODE(mc.debit_credit_indicator,'C',-1,1)   * "
				+ " mc.funding_amount ),2 ) ) AS reversal_amount "
				+ " FROM mc_settlement_activity mca,"
				+ " mc_settlement mc"
				+ " WHERE mca.settlement_date BETWEEN ? - ? AND ? "
				+ " AND mc.rec_id                    = mca.rec_id   "
				+ " AND mc.bank_number               = ?   "
				+ " GROUP BY mc.bin_number";
	  
	  
	  String preparedSqlAddRejects = 
				 " SELECT mc.bin_number AS bin_number,"
			   + " COUNT(1)           AS reject_count, "
			   + " SUM( ROUND( ( DECODE(mc.debit_credit_indicator,'C',-1,1) * "
			   + " mc.funding_amount ),2 ) ) AS reject_amount"
			   + " FROM mc_settlement mc "
			   + " WHERE mc.rec_id IN "
			   + " ( SELECT DISTINCT rr.rec_id "
			   + "   FROM reject_record rr "
			   + "   WHERE rr.reject_date BETWEEN ? - ? AND ? "
			   + "   AND NVL(rr.presentment,0) = 0 "
			   + "   AND rr.reject_type        = 'MC' "
			   + "   AND NOT rr.reject_id LIKE 'mbs%' ) "
			   + " AND mc.bank_number               = ? "
			   + " GROUP BY mc.bin_number";
	  
	  
	  String preparedSqlAddIncomingChargebacks = 
			    "select  cb.bin_number as bin_number,"
			  + " count(1) as cb_count,"
			  + " sum( cb.tran_amount) as cb_amount "
			  + " from    network_chargebacks cb "
			  + " where   cb.bank_number =  ? "
			  + " and cb.incoming_date between  ? - ?  and  ? "
			  + " and cb.card_type = 'MC' "
			  + " and not cb.cb_ref_num is null "
			  + " and cb.first_time_chargeback in ('Y','N') "
			  + " group by cb.bin_number";

	  String preparedSqlAddIncomingReversals = 
			    "select  cb.bin_number as bin_number,"
			  + " count(1) as reversal_count,"
			  + " sum( cb.tran_amount) as reversal_amount,"
			  + " sum( nvl(cb.reversal_amount,cb.tran_amount) - cb.tran_amount ) as fx_adj_amount"
			  + " from network_chargebacks cb where   cb.bank_number =  ? "
			  + " and cb.reversal_date between  ? - ? and  ?"
			  + " and cb.card_type = 'MC' "
			  + " group by cb.bin_number";
	  
	  String preparedSqlAddSecondPresentments = 
			    "select  cb.bin_number as bin_number,"
			  + " count(1) as sp_count,"
			  + " sum( cb.tran_amount) as sp_amount "
			  + " from network_chargeback_activity cba,chargeback_action_codes ac,"
			  + " network_chargebacks cb"
			  + " where   cba.settlement_date between  ? - ?  and ? "
			  + " and ac.short_action_code = cba.action_code"
			  + " and nvl(ac.represent,'N') = 'Y' "
			  + " and cb.cb_load_sec = cba.cb_load_sec "
			  + " and cb.bank_number =  ? "
			  + " and cb.card_type = 'MC' "
			  + " group by cb.bin_number";
	  
	  
	  String preparedSqlAddSecondPresentmentRejects = 
			    "select  cb.bin_number as bin_number,"
	  		  + " count(1) as reject_count,"
	  		  + " sum( cb.tran_amount ) as reject_amount"
	  		  + " from network_chargebacks cb"
	  		  + " where cb.cb_load_sec in "
	  		  + " ( select  distinct rr.rec_id from reject_record rr"
	  		  + "   where   rr.reject_date between ? - ? and ? "
	  		  + "   and nvl(rr.presentment,0) = 2  and rr.reject_type = 'MC' "
	  		  + "   and not rr.reject_id like 'mbs%' )"
	  		  + " and cb.bank_number =  ?"
	  		  + " group by cb.bin_number";
	  
	  
	  String preparedSqlAddFeeCollection = 
			    "select  fc.acquirer_bin as bin_number,"
			  + " count(1) as fees_count,"
			  + " sum( decode(fc.debit_credit_indicator,'C',-1,1) * fc.recon_amount ) as fees_amount"
			  + " from mc_settlement_fee_collect fc "
			  + " where   fc.bank_number =  ? "
			  + " and fc.recon_date between  ? - ?  and ? "
			  + " group by fc.acquirer_bin";
	  
	  String preparedSqlAddAssocData = 
			     "select  mr.bin_number as bin_number,"
			   + " sum( mr.fees_amount ) as reimbursement_fees,"
			   + " sum( decode(mr.file_type,901,mr.net_amount,0) ) as assoc_fees,"
			   + " sum( mr.net_amount ) as total_amount "
			   + " from mc_settlement_recon mr "
			   + " where mr.bank_number =  ? "
			   + " and mr.recon_date between  ? - ?  and ? "
			   + " group by mr.bin_number";
	  
	//@formatter:on

	/**
	 * Load the data for First Presentment for clearing summary
	 * @param bankNumber
	 * @param beginDate - report begin date
	 * @param cutOff - based on configuration in TE properties it will be 1 if the begin date is on Monday, otherwise 0
	 */
	protected void loadFirstPresentment(ClearingSummary clearingSummary, Date beginDate, int cutOff, int bankNumber) {
		if (log.isDebugEnabled()) {
			log.debug(String.format(DEBUG_LOG_FORMAT, "loadFirstPresentment", bankNumber, beginDate));
		}
		try {
			MesResultSet resultSet = (MesResultSet) getQueryHandler().executePreparedStatement("0com.mes.reports.mc.MasterCardClearingSummary",
					preparedSqlAddFirstPresentment, bankNumber, beginDate, cutOff, getEndDate(beginDate));
			while (resultSet.next()) {
				clearingSummary.addFirstPresentments(resultSet);
			}
		}
		catch (SQLException e) {
			log.error(String.format(ERROR_LOG_FORMAT, "loadFirstPresentment", e.getMessage()), e);
		}
	}

	/**
	 * Load the data for Reprocessed Rejects for clearing summary
	 * @param bankNumber
	 * @param beginDate - report begin date
	 * @param cutOff - based on configuration in TE properties it will be 1 if the begin date is on Monday, otherwise 0
	 */
	protected void loadReprocessedRejects(ClearingSummary clearingSummary, Date beginDate, int cutOff, int bankNumber) {
		if (log.isDebugEnabled()) {
			log.debug(String.format(DEBUG_LOG_FORMAT, "loadReprocessedRejects", bankNumber, beginDate));
		}
		try {		
			MesResultSet resultSet = (MesResultSet) getQueryHandler().executePreparedStatement("1com.mes.reports.mc.MasterCardClearingSummary",
					preparedSqlAddReprocessedRejects, beginDate, cutOff, getEndDate(beginDate), bankNumber);
			while (resultSet.next()) {
				clearingSummary.addReprocessedRejects(resultSet);
			}
		}
		catch (SQLException e) {
			log.error(String.format(ERROR_LOG_FORMAT, "loadReprocessedRejects", e.getMessage()), e);
		}
	}

	/**
	 * Load the data for Rejects for clearing summary
	 * @param bankNumber
	 * @param beginDate - report begin date
	 * @param cutOff - based on configuration in TE properties it will be 1 if the begin date is on Monday, otherwise 0
	 */
	protected void loadRejects(ClearingSummary clearingSummary, Date beginDate, int cutOff, int bankNumber) {
		if (log.isDebugEnabled()) {
			log.debug(String.format(DEBUG_LOG_FORMAT, "loadRejects", bankNumber, beginDate));
		}
		try {
			MesResultSet resultSet = (MesResultSet) getQueryHandler().executePreparedStatement("2com.mes.reports.mc.MasterCardClearingSummary",
					preparedSqlAddRejects, beginDate, cutOff, getEndDate(beginDate), bankNumber);
			while (resultSet.next()) {
				clearingSummary.addRejects(resultSet);
			}
		}
		catch (SQLException e) {
			log.error(String.format(ERROR_LOG_FORMAT, "loadRejects", e.getMessage()), e);
		}
	}

	/**
	 * Load the data for Incoming Chargebacks for clearing summary
	 * @param bankNumber
	 * @param beginDate - report begin date
	 * @param cutOff - based on configuration in TE properties it will be 1 if the begin date is on Monday, otherwise 0
	 */

	protected void loadIncomingChargebacks(ClearingSummary clearingSummary, Date beginDate, int cutOff, int bankNumber) {
		if (log.isDebugEnabled()) {
			log.debug(String.format(DEBUG_LOG_FORMAT, "loadIncomingChargebacks", bankNumber, beginDate));
		}
		try {
			MesResultSet resultSet = (MesResultSet) getQueryHandler().executePreparedStatement("3com.mes.reports.mc.MasterCardClearingSummary",
					preparedSqlAddIncomingChargebacks, bankNumber, beginDate, cutOff, getEndDate(beginDate));
			while (resultSet.next()) {
				clearingSummary.addIncomingChargebacks(resultSet);
			}
		}
		catch (SQLException e) {
			log.error(String.format(ERROR_LOG_FORMAT, "loadIncomingChargebacks", e.getMessage()), e);
		}
	}

	/**
	 * Load the data for Incoming Chargebacks reversals for clearing summary
	 * @param bankNumber
	 * @param beginDate - report begin date
	 * @param cutOff - based on configuration in TE properties it will be 1 if the begin date is on Monday, otherwise 0
	 */
	protected void loadIncomingChargebackReversals(ClearingSummary clearingSummary, Date beginDate, int cutOff, int bankNumber) {
		if (log.isDebugEnabled()) {
			log.debug(String.format(DEBUG_LOG_FORMAT, "loadIncomingChargebackReversals", bankNumber, beginDate));
		}
		try {
			MesResultSet resultSet = (MesResultSet) getQueryHandler().executePreparedStatement("4com.mes.reports.mc.MasterCardClearingSummary",
					preparedSqlAddIncomingReversals, bankNumber, beginDate, cutOff, getEndDate(beginDate));
			while (resultSet.next()) {
				clearingSummary.addIncomingReversals(resultSet);
			}
		}
		catch (SQLException e) {
			log.error(String.format(ERROR_LOG_FORMAT, "loadIncomingChargebackReversals", e.getMessage()), e);
		}
	}

	/**
	 * Load the data for Second Presentments for clearing summary
	 * @param bankNumber
	 * @param beginDate - report begin date
	 * @param cutOff - based on configuration in TE properties it will be 1 if the begin date is on Monday, otherwise 0
	 */
	protected void loadSecondPresentments(ClearingSummary clearingSummary, Date beginDate, int cutOff, int bankNumber) {
		if (log.isDebugEnabled()) {
			log.debug(String.format(DEBUG_LOG_FORMAT, "loadSecondPresentments", bankNumber, beginDate));
		}
		try {
			MesResultSet resultSet = (MesResultSet) getQueryHandler().executePreparedStatement("5com.mes.reports.mc.MasterCardClearingSummary",
					preparedSqlAddSecondPresentments, beginDate, cutOff, getEndDate(beginDate), bankNumber);
			while (resultSet.next()) {
				clearingSummary.addSecondPresentments(resultSet);
			}
		}
		catch (SQLException e) {
			log.error(String.format(ERROR_LOG_FORMAT, "loadSecondPresentments", e.getMessage()), e);
		}
	}

	/**
	 * Load the data for Second Presentments rejects for clearing summary
	 * @param bankNumber
	 * @param beginDate - report begin date
	 * @param cutOff - based on configuration in TE properties it will be 1 if the begin date is on Monday, otherwise 0
	 */
	protected void loadSecondPresentmentRejects(ClearingSummary clearingSummary, Date beginDate, int cutOff, int bankNumber) {
		if (log.isDebugEnabled()) {
			log.debug(String.format(DEBUG_LOG_FORMAT, "loadSecondPresentmentRejects", bankNumber, beginDate));
		}
		try {
			MesResultSet resultSet = (MesResultSet) getQueryHandler().executePreparedStatement("6com.mes.reports.mc.MasterCardClearingSummary",
					preparedSqlAddSecondPresentmentRejects, beginDate, cutOff, getEndDate(beginDate), bankNumber);
			while (resultSet.next()) {
				clearingSummary.addSecondPresentmentRejects(resultSet);
			}
		}
		catch (SQLException e) {
			log.error(String.format(ERROR_LOG_FORMAT, "loadSecondPresentmentRejects", e.getMessage()), e);
		}
	}

	/**
	 * Load the data for Fee Collection for clearing summary
	 * @param bankNumber
	 * @param beginDate - report begin date
	 * @param cutOff - based on configuration in TE properties it will be 1 if the begin date is on Monday, otherwise 0
	 */
	protected void loadFeeCollection(ClearingSummary clearingSummary, Date beginDate, int cutOff, int bankNumber) {
		if (log.isDebugEnabled()) {
			log.debug(String.format(DEBUG_LOG_FORMAT, "loadFeeCollection", bankNumber, beginDate));
		}
		try {
			MesResultSet resultSet = (MesResultSet) getQueryHandler().executePreparedStatement("7com.mes.reports.mc.MasterCardClearingSummary",
					preparedSqlAddFeeCollection, bankNumber, beginDate, cutOff, getEndDate(beginDate));
			while (resultSet.next()) {
				clearingSummary.addFeeCollection(resultSet);
			}
		}
		catch (SQLException e) {
			log.error(String.format(ERROR_LOG_FORMAT, "loadFeeCollection", e.getMessage()), e);
		}
	}

	/**
	 * Load the data for Association fees ,Reimbursement fees for clearing summary
	 * @param bankNumber
	 * @param beginDate - report begin date
	 * @param cutOff - based on configuration in TE properties it will be 1 if the begin date is on Monday, otherwise 0
	 */
	protected void loadAssocData(ClearingSummary clearingSummary, Date beginDate, int cutOff, int bankNumber) {
		if (log.isDebugEnabled()) {
			log.debug(String.format(DEBUG_LOG_FORMAT, "loadAssocData", bankNumber, beginDate));
		}
		try {
			MesResultSet resultSet = (MesResultSet) getQueryHandler().executePreparedStatement("8com.mes.reports.mc.MasterCardClearingSummary",
					preparedSqlAddAssocData, bankNumber, beginDate, cutOff, getEndDate(beginDate));
			while (resultSet.next()) {
				clearingSummary.addAssocData(resultSet);
			}
		}
		catch (SQLException e) {
			log.error(String.format(ERROR_LOG_FORMAT, "loadAssocData", e.getMessage()), e);
		}
	}
	
	private Date getEndDate(Date beginDt) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(beginDt);
		cal.set(Calendar.HOUR_OF_DAY, 24);
		return new Date(cal.getTimeInMillis());	
	}
}
