/*@lineinfo:filename=TridentBatchExtractor*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/TridentBatchExtractor.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-03-07 09:59:06 -0800 (Thu, 07 Mar 2013) $
  Version            : $Revision: 20976 $

  Change History:
     See VSS database

  Copyright (C) 2000-2004,2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.settlement.SettlementDb;
import com.mes.support.TridentTools;
import masthead.formats.visak.Batch;
import masthead.formats.visak.DetailRecord;
import sqlj.runtime.ResultSetIterator;

public class TridentBatchExtractor extends EventBase
{
  static Logger log = Logger.getLogger(TridentBatchExtractor.class);
  
  // maximum number of batches to process in one sweep
  private static final int  BATCH_PROCESS_MAX_COUNT   = 1000;
  
  private int maxCount = BATCH_PROCESS_MAX_COUNT;
  
  public Vector   batches = new Vector();
  
  public TridentBatchExtractor(int newMax)
  {
    super();
    
    maxCount = newMax;
  }
  
  public TridentBatchExtractor( )
  {
    super();
  }
  
  public boolean execute()
  {
    boolean               result        = false;
    ResultSetIterator     it            = null;
    ResultSet             rs            = null;
    long                  achSequence   = 0L;
    SettlementDb          db            = null;
    
    try
    {
      connect();
      
      setAutoCommit(false);
      
      long startTime = System.currentTimeMillis();
      int  maxRetry  = MesDefaults.getInt(MesDefaults.BATCH_PROCESS_MAX_RETRY);
      
      log.debug("STARTING PROCESS for " + maxCount + " records: " + startTime);
      
      // get batches to process
      //(skip empty bank numbers because that means the merchant isn't in the mif yet)
      /*@lineinfo:generated-code*//*@lineinfo:85^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tc.rec_id           rec_id,
//                  tc.retry_count      retry_count
//          from    trident_capture tc
//          where   tc.ach_sequence = 0
//                  and tc.bank_number is not null
//                  and rownum <= :maxCount
//          order by tc.rec_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tc.rec_id           rec_id,\n                tc.retry_count      retry_count\n        from    trident_capture tc\n        where   tc.ach_sequence = 0\n                and tc.bank_number is not null\n                and rownum <=  :1 \n        order by tc.rec_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TridentBatchExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,maxCount);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.TridentBatchExtractor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:94^7*/
      rs = it.getResultSet();

      while( rs.next() )
      {
        int  retry = rs.getInt("retry_count");
        long recId = rs.getLong("rec_id");

        if( retry >= maxRetry ) {
          /*@lineinfo:generated-code*//*@lineinfo:103^11*/

//  ************************************************************
//  #sql [Ctx] { update  trident_capture
//              set     ach_sequence = -9999
//              where   rec_id = :recId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_capture\n            set     ach_sequence = -9999\n            where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.TridentBatchExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:108^11*/

          // send email
          sendEmail( recId );
        }
        else {
          batches.add( recId );
        }
      }
      commit();
      rs.close();
      it.close();

      int batchCount = batches.size();
      
      log.debug("found " + batchCount + " batches left to process");
      
      if(batchCount > 0)
      {
        Batch       batchData     = null;
        long        batchId       = 0L;

        // get ach sequence
        /*@lineinfo:generated-code*//*@lineinfo:131^9*/

//  ************************************************************
//  #sql [Ctx] { select  trident_capture_totals_seq.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trident_capture_totals_seq.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.TridentBatchExtractor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   achSequence = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:136^9*/
        
        // now process batches
        db = new SettlementDb();
        db.connect(true);
        
        log.debug("processing batches");
        for( int i=0; i < batchCount; ++i )
        {
          batchId   = ((Long)batches.elementAt(i)).longValue();

          /*@lineinfo:generated-code*//*@lineinfo:147^11*/

//  ************************************************************
//  #sql [Ctx] { update trident_capture
//              set    retry_count = nvl(retry_count,0) + 1
//              where  rec_id = :batchId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update trident_capture\n            set    retry_count = nvl(retry_count,0) + 1\n            where  rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.TridentBatchExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,batchId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:152^11*/
          commit();
          
          // load the batch totals
          double    totalCreditsAmount  = 0.0;
          int       totalCreditsCount   = 0;
          double    totalSalesAmount    = 0.0;
          int       totalSalesCount     = 0;
    
          batchData = db._loadRawBatchDataVisak( batchId );
          Iterator detailsIter = batchData.getDetailRecords().iterator();
          while(detailsIter.hasNext())
          {
            DetailRecord  detail      = (DetailRecord) detailsIter.next();
            double        tranAmount  = TridentTools.decodeAmount(detail.getSettlementAmount());
      
            if ( TridentTools.isVisakCredit( detail.getTransactionCode() ) )
            {
              totalCreditsCount++;
              totalCreditsAmount += tranAmount;
            }
            else
            {
              totalSalesCount++;
              totalSalesAmount += tranAmount;
            }
          }
          
          // update trident capture record to show that this batch has been processed
          /*@lineinfo:generated-code*//*@lineinfo:181^11*/

//  ************************************************************
//  #sql [Ctx] { update  trident_capture
//              set     ach_sequence = :achSequence
//              where   rec_id = :batchId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_capture\n            set     ach_sequence =  :1 \n            where   rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.TridentBatchExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,achSequence);
   __sJT_st.setLong(2,batchId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:186^11*/
          
          // insert the row into the common table
          /*@lineinfo:generated-code*//*@lineinfo:189^11*/

//  ************************************************************
//  #sql [Ctx] { insert into mbs_batches 
//              (                       
//                bank_number,          
//                merchant_number,      
//                terminal_id,          
//                merchant_batch_date,  
//                batch_number,         
//                batch_id,             
//                trident_batch_id,     
//                mbs_batch_type,       
//                mesdb_timestamp,      
//                detail_timestamp_min, 
//                detail_timestamp_max, 
//                test_flag,            
//                response_code,        
//                currency_code,
//                sales_count,          
//                sales_amount,         
//                credits_count,        
//                credits_amount,       
//                total_count,          
//                net_amount,
//                load_file_id,
//                load_filename
//              )
//              select    tc.bank_number,
//                        tp.merchant_number,
//                        tc.profile_id,
//                        tc.received_date,
//                        decode( is_number(tc.batch_number), 0, 9999, tc.batch_number ),
//                        tc.rec_id,
//                        tc.trident_batch_id,
//                        1, -- :(mesConstants.MBS_BT_VISAK),
//                        tc.mesdb_timestamp,
//                        tc.mesdb_timestamp,
//                        tc.mesdb_timestamp,
//                        decode(tc.testing_flag,0,'N','Y'),
//                        tc.response_code,
//                        tc.currency_code,
//                        :totalSalesCount,
//                        :totalSalesAmount,
//                        :totalCreditsCount,
//                        :totalCreditsAmount,
//                        tc.detail_record_count,
//                        tc.net_amount,
//                        case
//                          when tc.testing_flag != 0 or tc.response_code != 0 then -2
//                          else -1
//                        end                   as load_file_id,
//                        case
//                          when tc.testing_flag != 0 or tc.response_code != 0 then 'not-processed'
//                          else null
//                        end                   as load_filename
//              from      trident_capture         tc,
//                        trident_profile         tp
//              where     tc.rec_id = :batchId
//                        and tp.terminal_id = tc.profile_id                        
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mbs_batches \n            (                       \n              bank_number,          \n              merchant_number,      \n              terminal_id,          \n              merchant_batch_date,  \n              batch_number,         \n              batch_id,             \n              trident_batch_id,     \n              mbs_batch_type,       \n              mesdb_timestamp,      \n              detail_timestamp_min, \n              detail_timestamp_max, \n              test_flag,            \n              response_code,        \n              currency_code,\n              sales_count,          \n              sales_amount,         \n              credits_count,        \n              credits_amount,       \n              total_count,          \n              net_amount,\n              load_file_id,\n              load_filename\n            )\n            select    tc.bank_number,\n                      tp.merchant_number,\n                      tc.profile_id,\n                      tc.received_date,\n                      decode( is_number(tc.batch_number), 0, 9999, tc.batch_number ),\n                      tc.rec_id,\n                      tc.trident_batch_id,\n                      1, -- :(mesConstants.MBS_BT_VISAK),\n                      tc.mesdb_timestamp,\n                      tc.mesdb_timestamp,\n                      tc.mesdb_timestamp,\n                      decode(tc.testing_flag,0,'N','Y'),\n                      tc.response_code,\n                      tc.currency_code,\n                       :1 ,\n                       :2 ,\n                       :3 ,\n                       :4 ,\n                      tc.detail_record_count,\n                      tc.net_amount,\n                      case\n                        when tc.testing_flag != 0 or tc.response_code != 0 then -2\n                        else -1\n                      end                   as load_file_id,\n                      case\n                        when tc.testing_flag != 0 or tc.response_code != 0 then 'not-processed'\n                        else null\n                      end                   as load_filename\n            from      trident_capture         tc,\n                      trident_profile         tp\n            where     tc.rec_id =  :5 \n                      and tp.terminal_id = tc.profile_id";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.TridentBatchExtractor",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,totalSalesCount);
   __sJT_st.setDouble(2,totalSalesAmount);
   __sJT_st.setInt(3,totalCreditsCount);
   __sJT_st.setDouble(4,totalCreditsAmount);
   __sJT_st.setLong(5,batchId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:248^11*/

          commit();
        }
      }
      
      commit();
      
      log.debug("done (" + (System.currentTimeMillis() - startTime) + " ms)");
      
      result = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
      rollback();
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      try { db.cleanUp(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( result );
  }

  private void sendEmail(long batchId)
  {
    String        fileTypeString  = "";
    StringBuffer  subject         = new StringBuffer("");
    StringBuffer  body            = new StringBuffer("");
    
    try
    {
      subject.append("TridentBatchExtractor Failed");

      subject.append(fileTypeString);

      body.append("Failed to process Batch ID: ");
      body.append( String.valueOf(batchId) );

      MailMessage msg = new MailMessage();
      msg.setFrom(MesDefaults.getString(MesDefaults.BATCH_EXTRACTOR_NOTIFICATION_FROM));
      msg.addTo(MesDefaults.getString(MesDefaults.BATCH_EXTRACTOR_NOTIFICATION_TO));

      msg.setSubject(subject.toString());
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "sendEmail()", e.toString());
      logEntry("sendEmail()", e.toString());
    }
  }

  public static void main( String[] args )
  {
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      
      int maxCount = (args.length > 0 ? Integer.parseInt(args[0]) : TridentBatchExtractor.BATCH_PROCESS_MAX_COUNT);
      
      TridentBatchExtractor grunt = new TridentBatchExtractor();
      
      grunt.execute();
    }
    catch(Exception e)
    {
      log.error("main(): " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/