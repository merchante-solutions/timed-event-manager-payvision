/*@lineinfo:filename=VisaMPSUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/VisaMPSUpload.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.settlement.SettlementRecord;
import sqlj.runtime.ResultSetIterator;

public class VisaMPSUpload extends VisaFileBase
{
  public VisaMPSUpload( )
  {
    PropertiesFilename          = "visa-mps.properties";

    CardTypeTable               = "visa_mps";
    CardTypeTableActivity       = null;
    CardTypePostTotals          = "vs_mps";
    CardTypeFileDesc            = "Visa MPS";
    DkOutgoingHost              = MesDefaults.DK_VISA_EP_HOST;
    DkOutgoingUser              = MesDefaults.DK_VISA_EP_USER;
    DkOutgoingPassword          = MesDefaults.DK_VISA_EP_PASSWORD;
    DkOutgoingPath              = MesDefaults.DK_VISA_EP_OUTGOING_PATH;
    DkOutgoingUseBinary         = false;
    DkOutgoingSendFlagFile      = true;
    SettlementAddrsNotify       = MesEmails.MSG_ADDRS_OUTGOING_VISA_NOTIFY;
    SettlementAddrsFailure      = MesEmails.MSG_ADDRS_OUTGOING_VISA_FAILURE;
    SettlementRecMT             = SettlementRecord.MT_VISA_MPS;
  }
  
  protected boolean processTransactions()
  {
    ResultSetIterator         it                = null;
    long                      workFileId        = 0L;
    String                    workFilename      = null;
    
    try
    {
      if ( TestFilename == null )
      {
        FileTimestamp   = new Timestamp(Calendar.getInstance().getTime().getTime());



        workFilename    = generateFilename("vs_mps"                                                     // base name
                                          + String.valueOf(Integer.parseInt( getEventArg(0) )),         // bank number
                                          ".ctf");                                                      // file extension
        workFileId      = loadFilenameToLoadFileId(workFilename);

        /*@lineinfo:generated-code*//*@lineinfo:74^9*/

//  ************************************************************
//  #sql [Ctx] { update  visa_merchant_profile_service   mps
//            set     mps.output_filename  = :workFilename,
//                    mps.output_file_id   = :workFileId
//            where   mps.output_file_id = 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  visa_merchant_profile_service   mps\n          set     mps.output_filename  =  :1 ,\n                  mps.output_file_id   =  :2 \n          where   mps.output_file_id = 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.VisaMPSUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setLong(2,workFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:80^9*/
      }
      else    // re-build a previous file
      {
        FileTimestamp   = new Timestamp( getFileDate(TestFilename).getTime() );
        workFilename    = TestFilename;
        workFileId      = loadFilenameToLoadFileId(workFilename);
      }

      // select the transactions to process
      /*@lineinfo:generated-code*//*@lineinfo:90^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  null                                  as action_code,
//                  0                                     as batch_id,
//                  0                                     as batch_record_id,
//                  mps.merchant_number                   as merchant_number,
//                  -- transpose source/dest bins from incoming to outgoing
//                  mps.dest_bin                          as bin_number,  
//                  mps.source_bin                        as issuer_bin,
//                  mps.mps_id_number                     as mps_id_number,
//                  substr(mf.dba_name,1,25)              as dba_name,
//                  substr(mf.dmaddr,1,30)                as dba_address,
//                  substr(mf.dmcity,1,20)                as dba_city,
//                  mf.dmstate                            as dba_state,
//                  lpad(substr(mf.dmzip,1,5),5,'0')      as dba_zip,
//                  mf.date_stat_chgd_to_dcb              as close_date,
//                  null                                  as portfolio_sale_bin,
//                  substr(mf.name2_line_1,1,30)          as legal_corp_name,
//                  lpad(mf.federal_tax_id,9,'0')         as merchant_tax_id,
//                  decode( mr.bustype_code,
//                          1,1,  -- Sole Proprietorship
//                          2,3,  -- Corporation
//                          3,2,  -- Partnership
//                          4,5,  -- Medical - Legal Corp
//                          5,6,  -- Association-Estate-Trust
//                          6,7,  -- Non-Profit/Tax Exempt
//                          --7     -- Government
//                          8,9,  -- Limited Liability Company
//                          1     -- default to sole prop
//                        )                               as incorporation_status_code,
//                  case 
//                    when instr(trim(mf.owner_name),' ') > 0 then substr(trim(mf.owner_name),1,instr(trim(mf.owner_name),' ')) 
//                    else null
//                  end                                   as owner_first_name, 
//                  extract_last_name(mf.owner_name)      as owner_last_name,                      
//                  null                                  as owner_middle_initial,
//                  0                                     as reimbursement_attribute
//          from    visa_merchant_profile_service   mps,
//                  mif                             mf,
//                  merchant                        mr
//          where   mps.output_file_id = :workFileId
//                  and mf.merchant_number = mps.merchant_number
//                  and mr.merch_number(+) = mps.merchant_number
//          order by mps.merchant_number      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  null                                  as action_code,\n                0                                     as batch_id,\n                0                                     as batch_record_id,\n                mps.merchant_number                   as merchant_number,\n                -- transpose source/dest bins from incoming to outgoing\n                mps.dest_bin                          as bin_number,  \n                mps.source_bin                        as issuer_bin,\n                mps.mps_id_number                     as mps_id_number,\n                substr(mf.dba_name,1,25)              as dba_name,\n                substr(mf.dmaddr,1,30)                as dba_address,\n                substr(mf.dmcity,1,20)                as dba_city,\n                mf.dmstate                            as dba_state,\n                lpad(substr(mf.dmzip,1,5),5,'0')      as dba_zip,\n                mf.date_stat_chgd_to_dcb              as close_date,\n                null                                  as portfolio_sale_bin,\n                substr(mf.name2_line_1,1,30)          as legal_corp_name,\n                lpad(mf.federal_tax_id,9,'0')         as merchant_tax_id,\n                decode( mr.bustype_code,\n                        1,1,  -- Sole Proprietorship\n                        2,3,  -- Corporation\n                        3,2,  -- Partnership\n                        4,5,  -- Medical - Legal Corp\n                        5,6,  -- Association-Estate-Trust\n                        6,7,  -- Non-Profit/Tax Exempt\n                        --7     -- Government\n                        8,9,  -- Limited Liability Company\n                        1     -- default to sole prop\n                      )                               as incorporation_status_code,\n                case \n                  when instr(trim(mf.owner_name),' ') > 0 then substr(trim(mf.owner_name),1,instr(trim(mf.owner_name),' ')) \n                  else null\n                end                                   as owner_first_name, \n                extract_last_name(mf.owner_name)      as owner_last_name,                      \n                null                                  as owner_middle_initial,\n                0                                     as reimbursement_attribute\n        from    visa_merchant_profile_service   mps,\n                mif                             mf,\n                merchant                        mr\n        where   mps.output_file_id =  :1 \n                and mf.merchant_number = mps.merchant_number\n                and mr.merch_number(+) = mps.merchant_number\n        order by mps.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.VisaMPSUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,workFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.VisaMPSUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:134^7*/
      handleSelectedRecords( it, workFilename );
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "processTransactions()", e.toString());
      logEntry("processTransactions()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return( true );
  }
  
  public static void main( String[] args )
  {
      try
      {
          if (args.length > 0 && args[0].equals("testproperties")) {
              EventBase.printKeyListStatus(new String[]{
                      MesDefaults.DK_VISA_EP_HOST,
                      MesDefaults.DK_VISA_EP_USER,
                      MesDefaults.DK_VISA_EP_PASSWORD,
                      MesDefaults.DK_VISA_EP_OUTGOING_PATH
              });
          }
      } catch (Exception e) {
          System.out.println(e.toString());
      }

    VisaMPSUpload                     test          = null;
    
    try
    { 
      test = new VisaMPSUpload();
      test.executeCommandLine( args );
    }
    catch( Exception e )
    {
      log.debug(e.toString());
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}/*@lineinfo:generated-code*/