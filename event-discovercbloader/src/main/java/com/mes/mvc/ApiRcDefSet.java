package com.mes.mvc;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ApiRcDefSet
{
  private Map defs = new HashMap();

  public ApiRcDefSet()
  {
  }
  public ApiRcDefSet(ApiRcDefSet baseDefs)
  {
    addDefs(baseDefs);
  }

  public void addDef(ApiRcDef def)
  {
    if (getDef(def.getCode()) != null)
    {
      throw new RuntimeException(
        "Attempt to define duplicate response code def: " + def);
    }
    defs.put(def.getCode(),def);
  }

  public void addDefs(ApiRcDefSet addDefs)
  {
    for (Iterator i = addDefs.defs.values().iterator(); i.hasNext();)
    {
      addDef((ApiRcDef)i.next());
    }
  }

  public ApiRcDef getDef(String code)
  {
    return (ApiRcDef)defs.get(code);
  }

  public String toString()
  {
    StringBuffer buf = new StringBuffer();
    buf.append("ApiRcDefSet [ defs: ");
    for (Iterator i = defs.values().iterator(); i.hasNext();)
    {
      buf.append(""+i.next());
      buf.append((i.hasNext() ? ", " : ""));
    }
    buf.append(" ]");

    return buf.toString();
  }
}