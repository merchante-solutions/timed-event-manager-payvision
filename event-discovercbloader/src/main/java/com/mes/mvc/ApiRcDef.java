package com.mes.mvc;

public class ApiRcDef
{
  private String  code;
  private String  msg;

  public ApiRcDef(String code, String msg)
  {
    this.code = code;
    this.msg = msg;
  }

  public String getCode()
  {
    return code;
  }

  public String getMsg()
  {
    return msg;
  }
  
  public String toString()
  {
    return "ApiRcDef [ " + 
      "code: " + code +
      ", msg: " + msg +
      " ]";
  }
}