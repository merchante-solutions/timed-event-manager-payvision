package com.mes.mvc;

import java.io.InputStream;

public interface ApiResponse
{
  public void setParameter(String name, String value);
  public String getParameter(String name);
  public void setResponseCode(String code, String msg);
  public void setResponseCode(String code);
  public void setDownloadFile(String dlFileName, InputStream in);
  public void setDownloadFile(String dlFileName, InputStream in,
    String dlContentType);
  public void setDownloadFile(String dlFileName, byte[] dlData, 
    String dlContentType);
  public void setDownloadFile(String dlFileName, byte[] data);
  public boolean isDownload();
  public String getTextResponse();
  public String getResponseCode();
  public String getResponseMessage();
  public void send();
}
