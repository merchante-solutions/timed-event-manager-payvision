package com.mes.mvc.mes;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import com.mes.mvc.ApiRcDef;
import com.mes.mvc.ApiRcDefSet;
import com.mes.mvc.ApiResponse;
import com.mes.mvc.ParameterEncoder;

public class MesApiResponse implements ApiResponse
{
  static Logger log = Logger.getLogger(MesApiResponse.class);

  // core mes response codes, these are hardcoded into the
  // mes implementation of ApiResponse
  public static final String RC_OK                  = "000";
  public static final String RC_SYSTEM_ERROR        = "001";
  public static final String RC_AUTHENTICATE_ERROR  = "002";
  public static final String RC_UPLOAD_ERROR        = "003";
  public static final String RC_INVALID_PARM        = "004";
  public static final String RC_MISSING_PARM        = "005";
  public static final String RC_INVALID_REQUEST     = "006";

  public static ApiRcDefSet coreRcDefs = new ApiRcDefSet();

  static
  {
    coreRcDefs.addDef(
      new ApiRcDef(RC_OK,                 "OK"));
    coreRcDefs.addDef(
      new ApiRcDef(RC_SYSTEM_ERROR,       "System error"));
    coreRcDefs.addDef(
      new ApiRcDef(RC_AUTHENTICATE_ERROR, "Authentication error"));
    coreRcDefs.addDef(
      new ApiRcDef(RC_UPLOAD_ERROR,       "Upload error"));
    coreRcDefs.addDef(
      new ApiRcDef(RC_INVALID_PARM,       "Invalid parameter"));
    coreRcDefs.addDef(
      new ApiRcDef(RC_MISSING_PARM,       "Missing parameter"));
    coreRcDefs.addDef(
      new ApiRcDef(RC_INVALID_REQUEST,    "Invalid request"));
  }

  // standard response parameters
  public static final String PARM_RSP_CODE          = "rspCode";
  public static final String PARM_RSP_MSG           = "rspMessage";

  protected HttpServletResponse response;
  protected Map parmMap = new HashMap();
  protected List parmOrder = new ArrayList();

  // text response
  private String textResponse;

  // download handling
  private String      dlFileName;
  private InputStream dlIn;
  private String      dlContentType;
  
  private ApiRcDefSet rcDefs = new ApiRcDefSet(coreRcDefs);

  public MesApiResponse(HttpServletResponse response, ApiRcDefSet extRcDefs)
  {
    this.response = response;
    if (extRcDefs != null)
    {
      rcDefs.addDefs(extRcDefs);
    }
    setResponseCode(RC_OK);
  }
  public MesApiResponse(HttpServletResponse response)
  {
    this(response,null);
  }

  public void setParameter(String name, String value)
  {
    if (parmMap.get(name) == null)
    {
      parmOrder.add(name);
    }
    parmMap.put(name,value);
  }

  public String getParameter(String name)
  {
    return (String)parmMap.get(name);
  }
  
  public String getResponseCode()
  {
    return getParameter(PARM_RSP_CODE);
  }
  
  public String getResponseMessage()
  {
    return getParameter(PARM_RSP_MSG);
  }

  /**
   * Set the response code to be returned.  If response code is not valid
   * an exception is thrown.  If msg is null the default msg is fetched
   * from response code def set.
   */
  public void setResponseCode(String code, String msg)
  {
    ApiRcDef rcDef = rcDefs.getDef(code);
    if (rcDef == null)
    {
      throw new MesApiSystemErrorException("Undefined response code '" 
        + code + "'");
    }
    if (msg == null)
    {
      msg = rcDef.getMsg();
    }

    setParameter(PARM_RSP_CODE,code);
    setParameter(PARM_RSP_MSG,msg);
  }

  /**
   * Set the response code, use default response text.
   */
  public void setResponseCode(String code)
  {
    setResponseCode(code,null);
  }

  /**
   * Encode all parameters and return as a String.
   */
  private String generateResponseMessage()
  {
    ParameterEncoder encoder = new ParameterEncoder();
    return encoder.encode(parmOrder,parmMap);
  }

  public void setDownloadFile(String dlFileName, InputStream dlIn,
    String dlContentType)
  {
    this.dlFileName     = dlFileName;
    this.dlIn           = dlIn;
    this.dlContentType  = dlContentType;
  }
  public void setDownloadFile(String dlFileName, InputStream dlIn)
  {
    setDownloadFile(dlFileName,dlIn,"text/plain");
  }
  public void setDownloadFile(String dlFileName, byte[] dlData, 
    String dlContentType)
  {
    ByteArrayInputStream dlIn = new ByteArrayInputStream(dlData);
    setDownloadFile(dlFileName,dlIn,dlContentType);
  }
  public void setDownloadFile(String dlFileName, byte[] data)
  {
    setDownloadFile(dlFileName,data,"text/plain");
  }

  public boolean isDownload()
  {
    return dlIn != null;
  }
  
  public String getTextResponse()
  {
    return textResponse;
  }

  /**
   * Send download file data.
   */
  private void sendDownload() throws Exception
  {
    response.setContentType(dlContentType);
    response.setHeader("Content-Disposition","attachment; filename=\"" 
      + dlFileName + "\";");
    try
    {
      OutputStream out = response.getOutputStream();
      byte[] buf = new byte[10000];
      int readCount = 0;
      while ((readCount = dlIn.read(buf)) != -1)
      {
        out.write(buf,0,readCount);
      }
      out.flush();
    }
    finally
    {
      try { dlIn.close(); } catch (Exception e) { }
    }
  }

  /**
   * Send response back using the servlet response object.
   */
  public void send()
  {
    try
    {
      if (isDownload())
      {
        sendDownload();
        return;
      }

      response.setContentType("text/plain");
    
      textResponse = generateResponseMessage();
      response.setContentLength(textResponse.length());

      log.debug("sending response text: '" + textResponse + "'");
      ServletOutputStream out = response.getOutputStream();
      out.print(textResponse);
      out.flush();
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }
}
