package com.mes.mvc.mes;

import org.apache.log4j.Logger;
import com.mes.mvc.ApiException;

public class MesApiSystemErrorException extends ApiException
{
  static Logger log = Logger.getLogger(MesApiSystemErrorException.class);

  public MesApiSystemErrorException(String description)
  {
    super(MesApiResponse.RC_SYSTEM_ERROR);
    // TODO: implement error logging here...
    log.error("System error: " + description);
  }
}
