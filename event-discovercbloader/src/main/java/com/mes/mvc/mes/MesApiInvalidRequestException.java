package com.mes.mvc.mes;

import com.mes.mvc.ApiException;

public class MesApiInvalidRequestException extends ApiException
{
  public MesApiInvalidRequestException(String reqName)
  {
    super(MesApiResponse.RC_INVALID_REQUEST,"Invalid request " + reqName);
  }
}
