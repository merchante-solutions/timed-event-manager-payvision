package com.mes.mvc;

import org.apache.log4j.Logger;

public class ApiException extends RuntimeException
{
  static Logger log = Logger.getLogger(ApiException.class);

  private String code;
  private String msg;

  public ApiException(String code)
  {
    super("Response code " + code);
    this.code = code;
  }
  public ApiException(String code, String msg)
  {
    super("Response code " + code + ", " + msg);
    this.code = code;
    this.msg = msg;;
  }

  public String getCode()
  {
    return code;
  }

  public String getMsg()
  {
    return msg;
  }
}
