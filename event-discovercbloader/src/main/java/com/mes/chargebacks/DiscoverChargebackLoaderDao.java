package com.mes.chargebacks;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import org.apache.log4j.Logger;
import com.mes.data.transaction.chargebacks.SplitFundUtil;

public class DiscoverChargebackLoaderDao {
	private Logger log = Logger.getLogger(DiscoverChargebackLoaderDao.class);
	private static final String SQL_UPDATE_SPLIT_INDICATOR = "update network_chargebacks set split_funding_ind = 'Y' where cb_load_sec = ?";
	private HashSet<Long> cbLoadSecs = new HashSet<>();
	private Connection con;
	private static final String SQL_GET_DISCOVER_CB_RECORDS =
			"SELECT CB.load_sec AS CB_ID, "
			+ "       CB.incoming_date AS CB_INCOMING_DATE, "
			+ "       CB.merchant_number AS CB_MERCHANT_NUMBER, "
			+ "       CB.dispute_amount AS CB_AMOUNT, "
			+ "       DS.trident_tran_id AS TRANSACTION_ID, "
			+ "       DS.transaction_amount AS TRANSACTION_AMOUNT "
			+ "FROM   network_chargeback_discover CB, "
			+ "       network_chargeback_discover_mt MT, "
			+ "       discover_settlement DS "
			+ "WHERE  CB.load_file_id = load_filename_to_load_file_id(?) "
			+ "       AND DS.rec_id(+) = CB.settlement_rec_id "
			+ "       AND MT.message_type(+) = CB.message_type "
			+ "       AND MT.function_code(+) = CB.function_code "
			+ "       AND MT.action_code(+) = CB.action_code ";
	public DiscoverChargebackLoaderDao(Connection connection) {
		this.con = connection;
	}

	public boolean loadCbSplitRuleDiscover(String filename) {
		log.debug("Inside DiscoverChargebackLoaderDao::loadCbSplitRuleDiscover() method");
		boolean splitFundingInd = false;

		try (PreparedStatement ps = con.prepareStatement(SQL_GET_DISCOVER_CB_RECORDS)) {
			ps.setString(1, filename);

			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					Long chargebackId = rs.getLong("CB_ID");
					Date cbIncomingDate = rs.getDate("CB_INCOMING_DATE");
					String transactionId = rs.getString("TRANSACTION_ID");
					Long merchantOfRecord = rs.getLong("CB_MERCHANT_NUMBER");
					Double cbAmount = rs.getDouble("CB_AMOUNT");
					Double transactionAmount = rs.getDouble("TRANSACTION_AMOUNT");
					splitFundingInd = SplitFundUtil.splitChargebackDiscover(transactionId, transactionAmount,chargebackId, merchantOfRecord, cbIncomingDate, cbAmount);

					if (splitFundingInd) {
						cbLoadSecs.add(chargebackId);
					}
				}

			}

			updateSplitIndicator(cbLoadSecs);
		}
		catch (Exception e) {
			log.error("Exception in DiscoverChargebackLoaderDao::loadCbSplitRuleDiscover()", e);
		}

		return splitFundingInd;
	}

	private void updateSplitIndicator(HashSet<Long> cbLoadSecs) {
		log.debug("Inside DiscoverChargebackLoaderDao::updateSplitIndicator() method");

		if (cbLoadSecs == null) {
			return;
		}

		try (PreparedStatement ps = con.prepareStatement(SQL_UPDATE_SPLIT_INDICATOR)) {
			for (Long cbLoadSec : cbLoadSecs) {
				ps.setLong(1, cbLoadSec);
				ps.addBatch();
			}

			ps.executeBatch();
		}
		catch (SQLException e) {
			log.error("Exception in DiscoverChargebackLoaderDao:updateSplitIndicator()", e);
		}
	}

}
