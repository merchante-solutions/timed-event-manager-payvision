/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/DiscoverCBLoader.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.mes.chargebacks.DiscoverChargebackLoaderDao;
import com.mes.config.MesDefaults;
import com.mes.constants.MesChargebacks;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.MesQueues;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.mvc.mes.MesApiInvalidRequestException;
import com.mes.net.MailMessage;
import com.mes.queues.QueueNotes;
import com.mes.queues.QueueTools;
import com.mes.settlement.ArdefLoader;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import com.mes.support.StringUtilities;
import com.mes.support.TridentTools;
import com.mes.tools.ChargebackTools;
import com.mes.tools.FileUtils;
import oracle.jdbc.OraclePreparedStatement;
import sqlj.runtime.ExecutionContext;
import sqlj.runtime.ExecutionContext.OracleContext;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.error.RuntimeRefErrors;
import sqlj.runtime.ref.DefaultContext;

public class DiscoverCBLoader extends EventBase
{
	private static final long serialVersionUID = 1L;

static Logger log = Logger.getLogger(DiscoverCBLoader.class);

  public static final int     FT_INVALID            = -1;
  public static final int     FT_CHARGEBACKS  		  = 0;
  public static final int     FT_RECON              = 1;
  
  public static class NetworkChargebackDiscoverRecord {

    private String discoverMerchantNumber;
    private String messageType;
    private long actionCode;
    private long functionCode;
    private String transactionDate;
    private double transactionAmount;
    private String disputeCaseNumber; 
    private String cardNumber;
    
    public NetworkChargebackDiscoverRecord() {
      super();
    }

    public NetworkChargebackDiscoverRecord(String discoverMerchantNumber, String messageType,
        long actionCode, long functionCode, String transactionDate, double transactionAmount, String disputeCaseNumber) {
      super();
      this.discoverMerchantNumber = discoverMerchantNumber;
      this.messageType = messageType;
      this.actionCode = actionCode;
      this.functionCode = functionCode;
      this.transactionDate = transactionDate;
      this.transactionAmount = transactionAmount;
      this.disputeCaseNumber = disputeCaseNumber;
    }

    

    public String getDiscoverMerchantNumber() {
      return discoverMerchantNumber;
    }

    public void setDiscoverMerchantNumber(String discoverMerchantNumber) {
      this.discoverMerchantNumber = discoverMerchantNumber;
    }

    public String getMessageType() {
      return messageType;
    }

    public void setMessageType(String messageType) {
      this.messageType = messageType;
    }

    public long getActionCode() {
      return actionCode;
    }

    public void setActionCode(long actionCode) {
      this.actionCode = actionCode;
    }

    public long getFunctionCode() {
      return functionCode;
    }

    public void setFunctionCode(long functionCode) {
      this.functionCode = functionCode;
    }

    public String getTransactionDate() {
      return transactionDate;
    }

    public void setTransactionDate(String date) {
      this.transactionDate = date;
    }

    public double getTransactionAmount() {
      return transactionAmount;
    }

    public void setTransactionAmount(double transactionAmount) {
      this.transactionAmount = transactionAmount;
    }

    public String getDisputeCaseNumber() {
      return disputeCaseNumber;
    }

    public void setDisputeCaseNumber(String disputeCaseNumber) {
      this.disputeCaseNumber = disputeCaseNumber;
    }   

    public String getCardNumber() {
      return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
      this.cardNumber = cardNumber;
    }

    public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + (int) (actionCode ^ (actionCode >>> 32));
      result = prime * result
          + ((discoverMerchantNumber == null) ? 0 : discoverMerchantNumber.hashCode());
      result = prime * result + ((disputeCaseNumber == null) ? 0 : disputeCaseNumber.hashCode());
      result = prime * result + (int) (functionCode ^ (functionCode >>> 32));
      result = prime * result + ((messageType == null) ? 0 : messageType.hashCode());
      long temp;
      temp = Double.doubleToLongBits(transactionAmount);
      result = prime * result + (int) (temp ^ (temp >>> 32));
      result = prime * result + ((transactionDate == null) ? 0 : transactionDate.hashCode());
      return result;
    }

    public boolean equals(Object obj) {
      if (this == obj)
        return true;
      if (obj == null)
        return false;
      if (getClass() != obj.getClass())
        return false;
      NetworkChargebackDiscoverRecord other = (NetworkChargebackDiscoverRecord) obj;      
      if (actionCode != other.actionCode)
        return false;
      if (discoverMerchantNumber == null) {
        if (other.discoverMerchantNumber != null)
          return false;
      } else if (!discoverMerchantNumber.equals(other.discoverMerchantNumber))
        return false;
      if (disputeCaseNumber == null) {
        if (other.disputeCaseNumber != null)
          return false;
      } else if (!disputeCaseNumber.equals(other.disputeCaseNumber))
        return false;
      if (functionCode != other.functionCode)
        return false;
      if (messageType == null) {
        if (other.messageType != null)
          return false;
      } else if (!messageType.equals(other.messageType))
        return false;
      if (Double.doubleToLongBits(transactionAmount) != Double
          .doubleToLongBits(other.transactionAmount))
        return false;
      if (transactionDate == null) {
        if (other.transactionDate != null)
          return false;
      } else if (!transactionDate.equals(other.transactionDate))
        return false;
      return true;
    }

    public String toString() {
      return "NetworkChargebackDiscoverRecord [discoverMerchantNumber=" + discoverMerchantNumber
          + ", messageType=" + messageType + ", actionCode=" + actionCode + ", functionCode="
          + functionCode + ", transactionDate=" + transactionDate + ", transactionAmount="
          + transactionAmount + ", disputeCaseNumber=" + disputeCaseNumber + "]";
    }

  }
  
  public static class DisputeType
  {
    public    String            ItemType      = null;
    public    String            FirstTimeInd  = null;
    
    public DisputeType()
    {
    }
    
    public boolean isChargeback()
    {
      return( "C".equals(ItemType) );
    }
    
    public boolean isChargebackFirstTime()
    {
      return( isChargeback() && "Y".equals(FirstTimeInd) );
    }
    
    public boolean isChargebackNotification()
    {
      return( isChargeback() && "X".equals(FirstTimeInd) );
    }
    
    public boolean isChargebackPreArb()
    {
      return( isChargeback() && "P".equals(FirstTimeInd) );
    }
    
    public boolean isChargebackReversal()
    {
      return( isChargeback() && "R".equals(FirstTimeInd) );
    }
    
    public boolean isChargebackSecondTime()
    {
      return( isChargeback() && "N".equals(FirstTimeInd) );
    }
    
    public boolean isRetrievalRequest()
    {
      return( "R".equals(ItemType) );
    }
    
    public void setData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ItemType      = resultSet.getString("item_type");
      FirstTimeInd  = resultSet.getString("first_time_indicator");
    }
  }
  
  protected int         FileType        = FT_INVALID;
  
  private static final HashMap  FilePrefixes  =
    new HashMap()
    {
      {
        put("DISPNTCE"        , "ds_cb"     );
        put("NERSARI8"        , "ds_recon"  );
        put("ACTIVEIIN"       , "ds_ardef"  );
        put("ANRRPT"          , "ds_imap"  );
      }
    };
    
  private static final HashMap  HeaderDateOffsets  =
    new HashMap()
    {
      {
        put("DISPNTCE"        , "4"   );
        put("NERSARI8"        , "1"   );
        put("ACTIVEIIN"       , "42"  );
      }
    };
    
  public DiscoverCBLoader( )
  {
    PropertiesFilename = "discover-cb-loader.properties";
  }
  
  protected void createProcessTableEntries( String loadFilename )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:330^7*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_process
//          (
//            process_sequence,
//            process_type,
//            load_filename
//          )
//          values
//          (
//            0,
//            1,
//            :loadFilename
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_process\n        (\n          process_sequence,\n          process_type,\n          load_filename\n        )\n        values\n        (\n          0,\n          1,\n           :1 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:344^7*/
    }
    catch( Exception e )
    {
      logEntry("createProcessTableEntries(" + loadFilename + ")",e.toString());
    }
    finally
    {
    }
  }
  
  private Date decodeCurrencyConversionDate( String convDateStr )
  {
    java.util.Date    javaDate    = null;
    Date              retVal      = null;
  
    try
    {
      if ( !"".equals(convDateStr) )
      {
        javaDate = DateTimeFormatter.parseDate(convDateStr,"MMdd");
        
        if ( javaDate != null )
        {
          Calendar cal = Calendar.getInstance();
          if ( javaDate.after(cal.getTime()) )
          {
            cal.setTime(javaDate);
            cal.add(Calendar.YEAR,-1);
            javaDate = cal.getTime();
          }
          retVal = new java.sql.Date(cal.getTime().getTime());
        }
      }
    }
    catch( Exception e )
    {
      logEntry("decodeCurrencyConversionDate(" + convDateStr + ")",e.toString());
    }
    return( retVal );
  }
  
  private double decodeCurrencyConversionRate( String rateStr )
  {
    return( TridentTools.decodeCobolAmount(rateStr.substring(1),Integer.parseInt(String.valueOf(rateStr.charAt(0)))) );
  }

  public boolean execute()
  {
    try
    {
      // get an automated timeout exempt connection
      connect(true);
    
      String fileType = getEventArg(0);
      
      if ( "NERSARI8".equals(fileType) )        // recon
      {
        HashMap bankToMailboxMap = loadBankToMailboxMap();
        
        // each bank has a unique mailbox for recon data
        for( Iterator it = bankToMailboxMap.keySet().iterator(); it.hasNext(); )
        {
        int bankNumber = Integer.parseInt( (String)it.next() );
	String mailboxId = (String)bankToMailboxMap.get(String.valueOf(bankNumber));

	if(bankNumber == 3003) {
		mailboxId = MesDefaults.getString(MesDefaults.DS_RECON_MAILBOX_3003);
	}

          loadFiles(bankNumber,                                                   // bank
                    mailboxId,	  // mailbox
                    MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PRESP_USER),// user
                    MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_ARI_PW),    // pw
                    fileType);                                                    // type
        }
      }
      else if ( "DISPNTCE".equals(fileType) )   // chargebacks
      {
        // all chargebacks come in via a common mailbox (same as registration)
        loadFiles(9999,                                                         // default bank
        		  "MESN04O",													// mailbox
                  MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PRESP_USER),// user
                  MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PRESP_PW),  // pw
                  fileType);                                                    // type
      }
      else if ( "ACTIVEIIN".equals(fileType) )   // IIN file 
      {
        // IIN (aka ardef) comes in via the common mailbox
        loadFiles(9999,                                                         // default bank
         		  "MESN04O",												    // mailbox
                  MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PRESP_USER),// user
                  MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PRESP_PW),  // pw
                  fileType);                                                    // type
      }
      else if ( "ANRRPT".equals(fileType) )   // Advance Notification Report (a.k.a. IMAP) file 
      {
        // IIN (aka IMAP) comes in via the common mailbox
        loadFiles(9999,                                                         // default bank
         		  "MESREPO",												    // mailbox
                  MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PRESP_USER),// user
                  MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PRESP_PW),  // pw
                  fileType);                                                    // type
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( true );
  }
  
  protected void loadArdefFile( String inputFilename )
  {
    ArdefLoader   loader    = null;
    
    try
    {
      loader = new ArdefLoader();
      loader.loadArdefFile(inputFilename);
    }
    catch( Exception e )
    {
      logEntry("loadArdefFile(" + inputFilename + ")",e.toString());
    }
    finally
    {
    }
  }
  
  protected HashMap loadBankToMailboxMap( )
  {
    ResultSetIterator       it        = null;
    ResultSet               resultSet = null;
    HashMap                 retVal    = new HashMap();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:481^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mb.bank_number              as bank_number,
//                  (mb.ds_acquirer_id || 'O')  as mailbox_id
//          from    mbs_banks   mb
//          where   mb.processor_id = 1                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mb.bank_number              as bank_number,\n                (mb.ds_acquirer_id || 'O')  as mailbox_id\n        from    mbs_banks   mb\n        where   mb.processor_id = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.DiscoverCBLoader",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.DiscoverCBLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:487^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        retVal.put( resultSet.getString("bank_number"),
                    resultSet.getString("mailbox_id") );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadBankToMailboxMap()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
    return( retVal );
  }
  
  protected void loadFiles( int bankNumber, String mailboxId, String user, String pw, String fileType )
  {
    BufferedReader    in                = null;
    java.util.Date    fileDate          = null;
    String            filePrefix        = null;
    String            line              = null;
    String            loadFilename      = null;
    String            remoteFilename    = null;
    String            tempFilename      = null;
    Sftp              sftp              = null;

    try
    {
      //get the Sftp connection
      sftp = getSftp( MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_PROFILE_HOST),
                      user,        // user
                      pw,               // password
                      MesDefaults.getString(MesDefaults.DK_DISCOVER_MAP_INCOMING_PATH),  // inbound path
                      false             // !binary
                    );
      
     SimpleDateFormat sfm = new SimpleDateFormat("yyyyDDD");
    	  String fileMaskDate = sfm.format(new java.util.Date()); 
    	  String fileMask = null;

    	  fileMask = mailboxId + "."+fileType+".J"+fileMaskDate+".*";
      
     // String fileMask = "\\%DSCVROUT\\%" + mailboxId + "\\%" + fileType + "\\%POLLABLE\\%.*";
      EnumerationIterator enumi = new EnumerationIterator();
      Iterator it = enumi.iterator(sftp.getNameListing(fileMask));
      
      while( it.hasNext() )
      {
        remoteFilename  = (String)it.next();
        filePrefix      = (String)FilePrefixes.get(fileType);
        tempFilename    = filePrefix + String.valueOf(bankNumber) + ".dat";

        System.out.println("Downloading '" + remoteFilename + "'");//@
        sftp.download(tempFilename,remoteFilename);

        in = new BufferedReader( new FileReader(tempFilename) );
        if ( (line = in.readLine()) != null )
        {
          String offsetStr = null;
          if ( (offsetStr = (String)HeaderDateOffsets.get(fileType)) != null )
          {
            int offset = Integer.parseInt(offsetStr);
            fileDate = DateTimeFormatter.parseDate(line.substring(offset,offset+8),"yyyyMMdd");
          }
          else
          {
            fileDate = null;
          }
          
          if ( fileDate == null )
          {
            fileDate = Calendar.getInstance().getTime();
          }
        }
        in.close();
        
        // generate a filename for this incoming file
        loadFilename  = generateFilename(filePrefix + String.valueOf(bankNumber), fileDate);
        
        System.out.println("Renaming '" + tempFilename + "' to '" + loadFilename + "'");//@
        File f = new File(tempFilename);
        f.renameTo( new File(loadFilename) );

        if ( "NERSARI8".equals(fileType) )
        {
          loadReconFile(loadFilename);
        }
        else if ( "DISPNTCE".equals(fileType) )
        {
          loadChargebackFile  ( loadFilename );
          loadChargebackSystem( loadFilename );
          loadRetrievalSystem ( loadFilename );
          notifyChargebackDept( loadFilename );
        }
        else if ( "ACTIVEIIN".equals(fileType) )
        {
          loadArdefFile(loadFilename);
        }
        else if ( "ANRRPT".equals(fileType) )
        {
          loadIMapFile(loadFilename);
        }
        archiveDailyFile(loadFilename);
      }
    }
    catch( Exception e )
    {
      logEntry("loadFiles(" + fileType + "," + remoteFilename + ")",e.toString());
    }
    finally
    {
      try{ sftp.disconnect(); } catch( Exception ee ) {}
    }
  }

  protected void loadChargebackFile( String inputFilename )
  {
    DisputeType       disputeType     = null;
    FlatFileRecord    ffd             = null;
    HashMap           ffds            = new HashMap();
    BufferedReader    in              = null;
    ResultSetIterator it              = null;
    String            line            = null;
    long              loadFileId      = 0L;
    String            loadFilename    = null;
    long              loadSec         = 0L;
    long              merchantId      = 0L;
    String            recId           = null;
    String            recType         = null;
    String            refNum          = null;
    ResultSet         resultSet       = null;
    String            tableName       = null;

    try
    {
      System.out.println("Loading Chargeback File (" + inputFilename + ")");
      ffds.put("2804/841" ,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_DISCOVER_CB_HDR));
      ffds.put("DTL"      ,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_DISCOVER_CB_DTL_INCOMING));
      ffds.put("2804/842" ,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_DISCOVER_CB_TRL));
      
      System.out.println("getting real load filename");
      loadFilename  = FileUtils.getNameFromFullPath(inputFilename).toLowerCase();
      System.out.println("real load filename = " + loadFilename);
      loadFileId    = loadFilenameToLoadFileId(loadFilename);
      System.out.println("loadFileId = " + loadFileId);

      in = new BufferedReader( new FileReader(inputFilename) );

      int recNum = 1;
      while( (line = in.readLine()) != null )
      {
        System.out.println("processing chargeback file line: " + recNum + " ("+line.substring(0,19) + ")");
        ++recNum;
        if ( line.charAt(1) == '8' )    // header or trailer
        {
          recType = line.substring(0,4) + "/" + line.substring(12,15);
        }
        else
        {
          recType = "DTL";    // use common detail record
        }
        ffd = (FlatFileRecord)ffds.get(recType);

        if ( ffd != null )
        {
          ffd.resetAllFields();
          ffd.suck(line);
          
          if ( "DTL".equals(recType) )
          {
            tableName   = null;
            disputeType = loadDisputeType(ffd.getFieldData("message_type"),
                                          ffd.getFieldData("function_code"),
                                          ffd.getFieldData("action_code"));
          
            if ( disputeType.isRetrievalRequest() )
            {
              tableName = "network_retrieval_discover";
            }
            else if ( disputeType.isChargebackFirstTime() ||
                      disputeType.isChargebackPreArb() )
            {
              tableName = "network_chargeback_discover";
            }
            else if ( disputeType.isChargebackReversal() )
            {
              processChargebackReversal(ffd);
            }
            else if ( disputeType.isChargebackNotification() )
            {
              System.out.println("processing chargeback notification");            
              processChargebackNotification(ffd);
            }
            else if ( disputeType.isChargebackSecondTime() )
            {            
              processChargebackSecondTime(ffd);
            }
            
            System.out.println("Done processing chargeback types");
            
            if ( tableName != null )
            {
              loadSec     = ChargebackTools.getNewLoadSec();
              recId       = ffd.getFieldData("acq_reference_number").trim();
          
              /*@lineinfo:generated-code*//*@lineinfo:699^15*/

//  ************************************************************
//  #sql [Ctx] it = { select  ds.rec_id,
//                          ds.merchant_number,
//                          ds.reference_number
//                  from    discover_settlement   ds
//                  where   ds.rec_id = :recId
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ds.rec_id,\n                        ds.merchant_number,\n                        ds.reference_number\n                from    discover_settlement   ds\n                where   ds.rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,recId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.DiscoverCBLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:706^15*/
              resultSet = it.getResultSet();
          
              if ( resultSet.next() )
              {                      
                merchantId  = resultSet.getLong("merchant_number");
                recId       = resultSet.getString("rec_id");
                refNum      = resultSet.getString("reference_number");
              }
              else
              {
                merchantId  = 0L;
                recId       = null;
                refNum      = ffd.getFieldData("acq_reference_number").trim();
              }
              resultSet.close();
              it.close();
          try{
              /*@lineinfo:generated-code*//*@lineinfo:724^15*/

//  ************************************************************
//  #sql [Ctx] { insert into :tableName
//                  (
//                    load_sec,
//                    message_type,
//                    merchant_number,
//                    discover_merchant_number,
//                    transmission_date,
//                    card_number,
//                    transaction_amount,
//                    reconciliation_amount,
//                    settlement_date,
//                    transaction_date,
//                    transaction_time,
//                    transaction_date_local,
//                    transaction_time_local,
//                    merchant_name_location_data,
//                    processing_code,
//                    acq_reference_number,
//                    network_reference_id,
//                    supporting_document_ind,
//                    currency_conversion_rate,
//                    system_trace_audit_number,
//                    currency_conversion_date,
//                    life_cycle_id_data,
//                    pos_data_code,
//                    function_code,
//                    dispute_reason_code,
//                    sic_code,
//                    retrieval_response_due_date,
//                    dispute_amount,
//                    acquirer_id,
//                    forwarding_institution_id,
//                    approval_code,
//                    action_code,
//                    additional_response_data,
//                    additional_comments,
//                    system_generated_comments,
//                    reference_data,
//                    error_field_number,
//                    foreign_currency_amount,
//                    foreign_currency_code,
//                    settlement_rec_id,
//                    load_filename,
//                    load_file_id
//                  )
//                  values
//                  (
//                    :loadSec,
//                    :ffd.getFieldData("message_type").trim(),
//                    :merchantId,
//                    :ffd.getFieldData("discover_merchant_number").trim(),
//                    to_date(:ffd.getFieldData("transmission_date").trim(),'yyyymmdd'),
//                    :ffd.getFieldData("card_number").trim(),
//                    :TridentTools.decodeCobolAmount(ffd.getFieldData("transaction_amount").trim(),2),
//                    :TridentTools.decodeCobolAmount(ffd.getFieldData("reconciliation_amount").trim(),2),
//                    to_date(:ffd.getFieldData("settlement_date").trim(),'yyyymmdd'),
//                    to_date(:ffd.getFieldData("transaction_date").trim(),'yyyymmdd'),
//                    to_date(:ffd.getFieldData("transaction_date") + ffd.getFieldData("transaction_time").trim(),'yyyymmddhh24miss'),
//                    to_date(:ffd.getFieldData("transaction_date_local").trim(),'yyyymmdd'),
//                    to_date(:ffd.getFieldData("transaction_date_local") + ffd.getFieldData("transaction_time_local").trim(),'yyyymmddhh24miss'),
//                    :ffd.getFieldData("merchant_name_location_data").trim(),
//                    :ffd.getFieldData("processing_code").trim(),
//                    :refNum,
//                    :ffd.getFieldData("network_reference_id").trim(),
//                    :ffd.getFieldData("supporting_document_ind").trim(),
//                    :decodeCurrencyConversionRate(ffd.getFieldData("currency_conversion_rate").trim()),
//                    :ffd.getFieldData("system_trace_audit_number").trim(),
//                    :decodeCurrencyConversionDate(ffd.getFieldData("currency_conversion_date").trim()),
//                    :ffd.getFieldData("life_cycle_id_data").trim(),
//                    :ffd.getFieldData("pos_data_code").trim(),
//                    :ffd.getFieldData("function_code").trim(),
//                    :ffd.getFieldData("dispute_reason_code").trim(),
//                    :ffd.getFieldData("sic_code").trim(),
//                    to_date(:ffd.getFieldData("retrieval_response_due_date").trim(),'yyyymmdd'),
//                    :TridentTools.decodeCobolAmount(ffd.getFieldData("dispute_amount").trim(),2),
//                    :ffd.getFieldData("acquirer_id").trim(),
//                    :ffd.getFieldData("forwarding_institution_id").trim(),
//                    :ffd.getFieldData("approval_code").trim(),
//                    :ffd.getFieldData("action_code").trim(),
//                    :ffd.getFieldData("additional_response_data").trim(),
//                    :ffd.getFieldData("additional_comments").trim(),
//                    :ffd.getFieldData("system_generated_comments").trim(),
//                    :ffd.getFieldData("reference_data").trim(),
//                    :ffd.getFieldData("error_field_number").trim(),
//                    :TridentTools.decodeCobolAmount(ffd.getFieldData("foreign_currency_amount").trim(),2),
//                    :ffd.getFieldData("foreign_currency_code").trim(),
//                    :recId,
//                    :loadFilename,
//                    :loadFileId
//                  )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_0 = ffd.getFieldData("message_type").trim();
 String __sJT_1 = ffd.getFieldData("discover_merchant_number").trim();
 String __sJT_2 = ffd.getFieldData("transmission_date").trim();
 String __sJT_3 = ffd.getFieldData("card_number").trim();
 double __sJT_4 = TridentTools.decodeCobolAmount(ffd.getFieldData("transaction_amount").trim(),2);
 double __sJT_5 = TridentTools.decodeCobolAmount(ffd.getFieldData("reconciliation_amount").trim(),2);
 String __sJT_6 = ffd.getFieldData("settlement_date").trim();
 String __sJT_7 = ffd.getFieldData("transaction_date").trim();
 String __sJT_8 = ffd.getFieldData("transaction_date") + ffd.getFieldData("transaction_time").trim();
 String __sJT_9 = ffd.getFieldData("transaction_date_local").trim();
 String __sJT_10 = ffd.getFieldData("transaction_date_local") + ffd.getFieldData("transaction_time_local").trim();
 String __sJT_11 = ffd.getFieldData("merchant_name_location_data").trim();
 String __sJT_12 = ffd.getFieldData("processing_code").trim();
 String __sJT_13 = ffd.getFieldData("network_reference_id").trim();
 String __sJT_14 = ffd.getFieldData("supporting_document_ind").trim();
 double __sJT_15 = decodeCurrencyConversionRate(ffd.getFieldData("currency_conversion_rate").trim());
 String __sJT_16 = ffd.getFieldData("system_trace_audit_number").trim();
 java.sql.Date __sJT_17 = decodeCurrencyConversionDate(ffd.getFieldData("currency_conversion_date").trim());
 String __sJT_18 = ffd.getFieldData("life_cycle_id_data").trim();
 String __sJT_19 = ffd.getFieldData("pos_data_code").trim();
 String __sJT_20 = ffd.getFieldData("function_code").trim();
 String __sJT_21 = ffd.getFieldData("dispute_reason_code").trim();
 String __sJT_22 = ffd.getFieldData("sic_code").trim();
 String __sJT_23 = ffd.getFieldData("retrieval_response_due_date").trim();
 double __sJT_24 = TridentTools.decodeCobolAmount(ffd.getFieldData("dispute_amount").trim(),2);
 String __sJT_25 = ffd.getFieldData("acquirer_id").trim();
 String __sJT_26 = ffd.getFieldData("forwarding_institution_id").trim();
 String __sJT_27 = ffd.getFieldData("approval_code").trim();
 String __sJT_28 = ffd.getFieldData("action_code").trim();
 String __sJT_29 = ffd.getFieldData("additional_response_data").trim();
 String __sJT_30 = ffd.getFieldData("additional_comments").trim();
 String __sJT_31 = ffd.getFieldData("system_generated_comments").trim();
 String __sJT_32 = ffd.getFieldData("reference_data").trim();
 String __sJT_33 = ffd.getFieldData("error_field_number").trim();
 double __sJT_34 = TridentTools.decodeCobolAmount(ffd.getFieldData("foreign_currency_amount").trim(),2);
 String __sJT_35 = ffd.getFieldData("foreign_currency_code").trim();
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("insert into  ");
   __sjT_sb.append(tableName);
   __sjT_sb.append(" \n                (\n                  load_sec,\n                  message_type,\n                  merchant_number,\n                  discover_merchant_number,\n                  transmission_date,\n                  card_number,\n                  transaction_amount,\n                  reconciliation_amount,\n                  settlement_date,\n                  transaction_date,\n                  transaction_time,\n                  transaction_date_local,\n                  transaction_time_local,\n                  merchant_name_location_data,\n                  processing_code,\n                  acq_reference_number,\n                  network_reference_id,\n                  supporting_document_ind,\n                  currency_conversion_rate,\n                  system_trace_audit_number,\n                  currency_conversion_date,\n                  life_cycle_id_data,\n                  pos_data_code,\n                  function_code,\n                  dispute_reason_code,\n                  sic_code,\n                  retrieval_response_due_date,\n                  dispute_amount,\n                  acquirer_id,\n                  forwarding_institution_id,\n                  approval_code,\n                  action_code,\n                  additional_response_data,\n                  additional_comments,\n                  system_generated_comments,\n                  reference_data,\n                  error_field_number,\n                  foreign_currency_amount,\n                  foreign_currency_code,\n                  settlement_rec_id,\n                  load_filename,\n                  load_file_id\n                )\n                values\n                (\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                  to_date( ? ,'yyyymmdd'),\n                   ? ,\n                   ? ,\n                   ? ,\n                  to_date( ? ,'yyyymmdd'),\n                  to_date( ? ,'yyyymmdd'),\n                  to_date( ? ,'yyyymmddhh24miss'),\n                  to_date( ? ,'yyyymmdd'),\n                  to_date( ? ,'yyyymmddhh24miss'),\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                  to_date( ? ,'yyyymmdd'),\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? ,\n                   ? \n                )");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "3com.mes.startup.DiscoverCBLoader:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,__sJT_0);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setString(4,__sJT_1);
   __sJT_st.setString(5,__sJT_2);
   __sJT_st.setString(6,__sJT_3);
   __sJT_st.setDouble(7,__sJT_4);
   __sJT_st.setDouble(8,__sJT_5);
   __sJT_st.setString(9,__sJT_6);
   __sJT_st.setString(10,__sJT_7);
   __sJT_st.setString(11,__sJT_8);
   __sJT_st.setString(12,__sJT_9);
   __sJT_st.setString(13,__sJT_10);
   __sJT_st.setString(14,__sJT_11);
   __sJT_st.setString(15,__sJT_12);
   __sJT_st.setString(16,refNum);
   __sJT_st.setString(17,__sJT_13);
   __sJT_st.setString(18,__sJT_14);
   __sJT_st.setDouble(19,__sJT_15);
   __sJT_st.setString(20,__sJT_16);
   __sJT_st.setDate(21,__sJT_17);
   __sJT_st.setString(22,__sJT_18);
   __sJT_st.setString(23,__sJT_19);
   __sJT_st.setString(24,__sJT_20);
   __sJT_st.setString(25,__sJT_21);
   __sJT_st.setString(26,__sJT_22);
   __sJT_st.setString(27,__sJT_23);
   __sJT_st.setDouble(28,__sJT_24);
   __sJT_st.setString(29,__sJT_25);
   __sJT_st.setString(30,__sJT_26);
   __sJT_st.setString(31,__sJT_27);
   __sJT_st.setString(32,__sJT_28);
   __sJT_st.setString(33,__sJT_29);
   __sJT_st.setString(34,__sJT_30);
   __sJT_st.setString(35,__sJT_31);
   __sJT_st.setString(36,__sJT_32);
   __sJT_st.setString(37,__sJT_33);
   __sJT_st.setDouble(38,__sJT_34);
   __sJT_st.setString(39,__sJT_35);
   __sJT_st.setString(40,recId);
   __sJT_st.setString(41,loadFilename);
   __sJT_st.setLong(42,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:816^15*/
          }catch(SQLException sqle){            
            logEntry("loadChargebackData("+loadFilename+")", sqle.toString());
          }catch(Exception e){
            logEntry("loadChargebackData("+loadFilename+")", e.toString());
          }
            }   // end if ( tableName != null )
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadChargebackData("+loadFilename+")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      try{ in.close(); } catch( Exception e ) {}
    }
  }

  public void loadChargebackSystem( String loadFilename )
  {
    boolean     autoCommit      = getAutoCommit();
    
    DiscoverChargebackLoaderDao discoverChargebackLoaderDao = new DiscoverChargebackLoaderDao(con);
    
    try
    {
      setAutoCommit(false);
     
      /*@lineinfo:generated-code*//*@lineinfo:846^7*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargebacks
//          (
//            cb_load_sec,
//            cb_ref_num,
//            merchant_number,
//            bank_number,
//            bin_number,
//            incoming_date,
//            reference_number,
//            card_number,
//            card_type,
//            tran_date,
//            tran_amount,
//            merchant_name,
//            reason_code,
//            load_filename,
//            first_time_chargeback,
//            mes_ref_num,
//            debit_credit_ind,
//            tran_date_missing,
//            card_number_enc,
//            dt_batch_date,
//            dt_batch_number,
//            dt_ddf_dt_id,
//            trident_tran_id,
//            dt_purchase_id,
//            client_reference_number
//          )
//          select  cb.load_sec                       as load_sec,
//                  cb.dispute_case_number            as cb_ref_num,
//                  nvl(cb.merchant_number,0)         as merchant_number,
//                  nvl(cb.bank_number,0)             as bank_number,
//                  cb.acquirer_id                    as bin_number,
//                  cb.incoming_date                  as incoming_date,
//                  cb.acq_reference_number           as reference_number,
//                  cb.card_number                    as card_number,
//                  'DS'                              as card_type,
//                  cb.transaction_date               as tran_date,
//                  cb.dispute_amount                 as tran_amount,
//                  substr(cb.dba_name,1,32)          as merchant_name,
//                  cb.dispute_reason_code_alpha      as reason_code,
//                  cb.load_filename                  as load_filename,
//                  nvl(mt.first_time_indicator,'Y')  as first_time,
//                  null                              as mes_ref_num,
//                  decode( (cb.dispute_amount/abs(cb.dispute_amount)),
//                          -1, 'C',
//                          'D' )                     as debit_credit_ind,
//                  'N'                               as tran_date_missing,
//                  cb.card_number_enc                as card_number_enc,
//                  ds.batch_date                     as batch_date,
//                  ds.batch_id                       as batch_number,
//                  ds.batch_record_id                as ddf_dt_id,
//                  ds.trident_tran_id                as trident_tran_id,
//                  ds.purchase_id                    as purchase_id,
//                  ds.client_reference_number        as client_ref_num
//          from    network_chargeback_discover     cb,
//                  discover_settlement             ds,
//                  network_chargeback_discover_mt  mt
//          where   cb.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                  and ds.rec_id(+) = cb.settlement_rec_id
//                  and mt.message_type(+) = cb.message_type
//                  and mt.function_code(+) = cb.function_code
//                  and mt.action_code(+) = cb.action_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into network_chargebacks\n        (\n          cb_load_sec,\n          cb_ref_num,\n          merchant_number,\n          bank_number,\n          bin_number,\n          incoming_date,\n          reference_number,\n          card_number,\n          card_type,\n          tran_date,\n          tran_amount,\n          merchant_name,\n          reason_code,\n          load_filename,\n          first_time_chargeback,\n          mes_ref_num,\n          debit_credit_ind,\n          tran_date_missing,\n          card_number_enc,\n          dt_batch_date,\n          dt_batch_number,\n          dt_ddf_dt_id,\n          trident_tran_id,\n          dt_purchase_id,\n          client_reference_number\n        )\n        select  cb.load_sec                       as load_sec,\n                cb.dispute_case_number            as cb_ref_num,\n                nvl(cb.merchant_number,0)         as merchant_number,\n                nvl(cb.bank_number,0)             as bank_number,\n                cb.acquirer_id                    as bin_number,\n                cb.incoming_date                  as incoming_date,\n                cb.acq_reference_number           as reference_number,\n                cb.card_number                    as card_number,\n                'DS'                              as card_type,\n                cb.transaction_date               as tran_date,\n                cb.dispute_amount                 as tran_amount,\n                substr(cb.dba_name,1,32)          as merchant_name,\n                cb.dispute_reason_code_alpha      as reason_code,\n                cb.load_filename                  as load_filename,\n                nvl(mt.first_time_indicator,'Y')  as first_time,\n                null                              as mes_ref_num,\n                decode( (cb.dispute_amount/abs(cb.dispute_amount)),\n                        -1, 'C',\n                        'D' )                     as debit_credit_ind,\n                'N'                               as tran_date_missing,\n                cb.card_number_enc                as card_number_enc,\n                ds.batch_date                     as batch_date,\n                ds.batch_id                       as batch_number,\n                ds.batch_record_id                as ddf_dt_id,\n                ds.trident_tran_id                as trident_tran_id,\n                ds.purchase_id                    as purchase_id,\n                ds.client_reference_number        as client_ref_num\n        from    network_chargeback_discover     cb,\n                discover_settlement             ds,\n                network_chargeback_discover_mt  mt\n        where   cb.load_file_id = load_filename_to_load_file_id( :1 )\n                and ds.rec_id(+) = cb.settlement_rec_id\n                and mt.message_type(+) = cb.message_type\n                and mt.function_code(+) = cb.function_code\n                and mt.action_code(+) = cb.action_code";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}

//splitFunding table loader	
discoverChargebackLoaderDao.loadCbSplitRuleDiscover(loadFilename);


{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
//@formatter:off
  String theSqlTS = "insert into network_chargeback_activity"
   					+ " (cb_load_sec, file_load_sec,action_code,action_date,"
   					+ " user_message,action_source,user_login)"
   					+ " select  cb.load_sec,1,'D',trunc(sysdate),'Auto Action - Discover',"
   					+ " :1 ,'system' "
   					+ " from network_chargeback_discover cb "
   					+ " where "
   					+ " cb.load_file_id = load_filename_to_load_file_id( :2 )"
   					+ " and cb.message_type not in (select distinct (MESSAGE_TYPE) from network_chargeback_discover_mt where FUNCTION_DESC like '%Arbitration%')"
   					+ " and not cb.settlement_rec_id is null    -- only work resolved items";
//@formatter:on
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesChargebacks.AS_AUTO_DISCOVER);
   __sJT_st.setString(2,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}




{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
//@formatter:off
  String theSqlTS = "insert into q_data "
	   				+ " (id, type, item_type, owner, date_created,source,affiliate,last_changed,last_user)"
	   				+ " select  cb.load_sec as id, "
	   				+ " case  "
	   				+ " when cb.settlement_rec_id is null then :1"
	   				+ " when cb.settlement_rec_id is not null and cb.message_type in"
	   				+ " (select distinct (MESSAGE_TYPE) from network_chargeback_discover_mt where FUNCTION_DESC like '%Arbitration%') then :2"
	   				+ " else :3 end as type,"
	   				+ " 18 as item_type,1 as owner, cb.incoming_date as date_created,'discover' as source,"
	   				+ " to_char(nvl(mf.bank_number,3941))  as affiliate,sysdate as last_changed,'system' as last_user"
	   				+ " from network_chargeback_discover cb, mif mf"
	   				+ " where "
	   		    	+ " cb.load_file_id = load_filename_to_load_file_id( :4 ) and"
	   		    	+ " mf.merchant_number(+) = cb.merchant_number";
//@formatter:on
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_CHARGEBACKS_DISCOVER);
   __sJT_st.setInt(2,MesQueues.Q_CHARGEBACKS_DISCOVER_PRE_ARB);
   __sJT_st.setInt(3,MesQueues.Q_CHARGEBACKS_MCHB);
   __sJT_st.setString(4,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:968^7*/

      // queue the loading of chargeback risk points
      createProcessTableEntries(loadFilename);
      
      /*@lineinfo:generated-code*//*@lineinfo:973^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:976^7*/
    }
    catch(Exception e)
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:980^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:980^34*/ } catch( Exception sqe ) {}
      logEntry("loadChargebackSystem(" + loadFilename + ")",e.toString());
    }
    finally
    {
      setAutoCommit(autoCommit);
    }
  }
  
  protected DisputeType loadDisputeType( String mt, String fc, String ac )
  {
    DisputeType           disputeType     = new DisputeType();
    ResultSetIterator     it              = null;
    ResultSet             resultSet       = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:997^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  item_type,first_time_indicator
//          from    network_chargeback_discover_mt
//          where   message_type = :mt
//                  and function_code = :fc
//                  and action_code = :ac
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  item_type,first_time_indicator\n        from    network_chargeback_discover_mt\n        where   message_type =  :1 \n                and function_code =  :2 \n                and action_code =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,mt);
   __sJT_st.setString(2,fc);
   __sJT_st.setString(3,ac);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.DiscoverCBLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1004^7*/
      resultSet = it.getResultSet();
      resultSet.next();
      disputeType.setData(resultSet);
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("loadDisputeType(" + mt + "," + fc + "," + ac + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
    return( disputeType );
  }

  public void loadReconFile( String inputFilename )
  {
    long              acquirerId      = 0L;
    Date              activityDate    = null;
    int               bankNumber      = 0;
    FlatFileRecord    ffd             = null;
    HashMap           ffds            = new HashMap();
    BufferedReader    in              = null;
    int               keyLen          = 4;
    String            line            = null;
    long              loadFileId      = 0L;
    String            loadFilename    = null;
    long              loadSec         = 0L;
    String            recKey          = null;

    try
    {
      ffds.put("5011"   , new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_DS_RECON_SETTLE_ACTIVITY   ) );
      ffds.put("5012"   , new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_DS_RECON_BANK_ACTIVITY     ) );
      ffds.put("5050"   , new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_DS_RECON_FEE_ACTIVITY      ) );
      ffds.put("50701"  , new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_DS_RECON_FILE_REJECT       ) );
      ffds.put("50703"  , new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_DS_RECON_BATCH_REJECT      ) );
      ffds.put("50704"  , new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_DS_RECON_TRAN_REJECT       ) );

      loadFilename  = FileUtils.getNameFromFullPath(inputFilename).toLowerCase();
      loadFileId    = loadFilenameToLoadFileId(loadFilename);
      bankNumber    = getFileBankNumber(loadFilename);

      /*@lineinfo:generated-code*//*@lineinfo:1049^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    discover_settlement_recon
//          where   load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    discover_settlement_recon\n        where   load_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1054^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:1056^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    discover_settlement_fee_coll
//          where   load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    discover_settlement_fee_coll\n        where   load_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1061^7*/

      in = new BufferedReader( new FileReader(inputFilename) );

      while( (line = in.readLine()) != null )
      { 
        if (line.charAt(0) == '2' )     // file header
        {
          java.util.Date javaDate = DateTimeFormatter.parseDate(line.substring(1,9),"yyyyMMdd");
          activityDate  = new java.sql.Date( javaDate.getTime() );
          acquirerId    = Long.parseLong(line.substring(17,28));
          continue;
        }
        
        keyLen  = (line.startsWith("5070") ? 5 : 4);
        recKey  = line.substring(0,keyLen);
        ffd     = (FlatFileRecord)ffds.get(recKey);  
        
        if ( ffd != null )
        {
          ffd.suck(line);
          
          int recType = Integer.parseInt(recKey);
          switch( recType )
          {
            case 5011:      // settlement activity
            case 5012:
              String activityType   = (recType == 5011 ? ffd.getFieldData("activity_type") : null);
              String activityCount  = (recType == 5011 ? ffd.getFieldData("activity_count") : null);
              double activityAmount = TridentTools.decodeCobolAmount(ffd.getFieldData( (recType == 5011 ? "activity_amount" : "settlement_amount") ),2);
              String dda            = (recType == 5011 ? null : ffd.getFieldData("bank_account_number"));
              
              /*@lineinfo:generated-code*//*@lineinfo:1093^15*/

//  ************************************************************
//  #sql [Ctx] { insert into discover_settlement_recon
//                  (
//                    bank_number,
//                    acquirer_id,
//                    activity_date,
//                    settlement_date,
//                    record_key,
//                    record_type,
//                    section_code,
//                    sub_section_code,
//                    entry_description,
//                    item_count,
//                    item_amount,
//                    dda,
//                    load_filename,
//                    load_file_id
//                  )
//                  values
//                  (
//                    :bankNumber,
//                    :acquirerId,
//                    :activityDate,
//                    :activityDate+1,
//                    :recKey,
//                    :ffd.getFieldData("record_type"),
//                    :ffd.getFieldData("section_code"),
//                    :ffd.getFieldData("sub_section_code"),
//                    trim(:activityType),:activityCount,:activityAmount,
//                    :dda,
//                    :loadFilename,
//                    :loadFileId
//                  )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_36 = ffd.getFieldData("record_type");
 String __sJT_37 = ffd.getFieldData("section_code");
 String __sJT_38 = ffd.getFieldData("sub_section_code");
   String theSqlTS = "insert into discover_settlement_recon\n                (\n                  bank_number,\n                  acquirer_id,\n                  activity_date,\n                  settlement_date,\n                  record_key,\n                  record_type,\n                  section_code,\n                  sub_section_code,\n                  entry_description,\n                  item_count,\n                  item_amount,\n                  dda,\n                  load_filename,\n                  load_file_id\n                )\n                values\n                (\n                   :1 ,\n                   :2 ,\n                   :3 ,\n                   :4 +1,\n                   :5 ,\n                   :6 ,\n                   :7 ,\n                   :8 ,\n                  trim( :9 ), :10 , :11 ,\n                   :12 ,\n                   :13 ,\n                   :14 \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setLong(2,acquirerId);
   __sJT_st.setDate(3,activityDate);
   __sJT_st.setDate(4,activityDate);
   __sJT_st.setString(5,recKey);
   __sJT_st.setString(6,__sJT_36);
   __sJT_st.setString(7,__sJT_37);
   __sJT_st.setString(8,__sJT_38);
   __sJT_st.setString(9,activityType);
   __sJT_st.setString(10,activityCount);
   __sJT_st.setDouble(11,activityAmount);
   __sJT_st.setString(12,dda);
   __sJT_st.setString(13,loadFilename);
   __sJT_st.setLong(14,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1127^15*/
              break;
              
            case 5050:
              /*@lineinfo:generated-code*//*@lineinfo:1131^15*/

//  ************************************************************
//  #sql [Ctx] { insert into discover_settlement_fee_coll
//                  (
//                    bank_number,
//                    acquirer_id,
//                    activity_date,
//                    settlement_date,
//                    record_key,
//                    record_type,
//                    section_code,
//                    sub_section_code,
//                    fee_amount,
//                    fee_class_code,
//                    fee_class_desc,
//                    fee_type_code,
//                    fee_type_desc,
//                    load_filename,
//                    load_file_id
//                  )
//                  values
//                  (
//                    :bankNumber,
//                    :acquirerId,
//                    :activityDate,
//                    :activityDate+1,
//                    :recKey,
//                    :ffd.getFieldData("record_type"),
//                    :ffd.getFieldData("section_code"),
//                    :ffd.getFieldData("sub_section_code"),
//                    :TridentTools.decodeCobolAmount(ffd.getFieldData("total_fees_amount"),2),
//                    trim(:ffd.getFieldData("fee_class_code")),
//                    trim(:ffd.getFieldData("fee_class_desc")),
//                    trim(:ffd.getFieldData("fee_type_code")),
//                    trim(:ffd.getFieldData("fee_type_desc")),
//                    :loadFilename,
//                    :loadFileId
//                  )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_39 = ffd.getFieldData("record_type");
 String __sJT_40 = ffd.getFieldData("section_code");
 String __sJT_41 = ffd.getFieldData("sub_section_code");
 double __sJT_42 = TridentTools.decodeCobolAmount(ffd.getFieldData("total_fees_amount"),2);
 String __sJT_43 = ffd.getFieldData("fee_class_code");
 String __sJT_44 = ffd.getFieldData("fee_class_desc");
 String __sJT_45 = ffd.getFieldData("fee_type_code");
 String __sJT_46 = ffd.getFieldData("fee_type_desc");
   String theSqlTS = "insert into discover_settlement_fee_coll\n                (\n                  bank_number,\n                  acquirer_id,\n                  activity_date,\n                  settlement_date,\n                  record_key,\n                  record_type,\n                  section_code,\n                  sub_section_code,\n                  fee_amount,\n                  fee_class_code,\n                  fee_class_desc,\n                  fee_type_code,\n                  fee_type_desc,\n                  load_filename,\n                  load_file_id\n                )\n                values\n                (\n                   :1 ,\n                   :2 ,\n                   :3 ,\n                   :4 +1,\n                   :5 ,\n                   :6 ,\n                   :7 ,\n                   :8 ,\n                   :9 ,\n                  trim( :10 ),\n                  trim( :11 ),\n                  trim( :12 ),\n                  trim( :13 ),\n                   :14 ,\n                   :15 \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setLong(2,acquirerId);
   __sJT_st.setDate(3,activityDate);
   __sJT_st.setDate(4,activityDate);
   __sJT_st.setString(5,recKey);
   __sJT_st.setString(6,__sJT_39);
   __sJT_st.setString(7,__sJT_40);
   __sJT_st.setString(8,__sJT_41);
   __sJT_st.setDouble(9,__sJT_42);
   __sJT_st.setString(10,__sJT_43);
   __sJT_st.setString(11,__sJT_44);
   __sJT_st.setString(12,__sJT_45);
   __sJT_st.setString(13,__sJT_46);
   __sJT_st.setString(14,loadFilename);
   __sJT_st.setLong(15,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1169^15*/
              break;
              
            case 50701:   // file reject
            case 50703:   // batch reject
            case 50704:   // tran reject
              storeIncomingReject(loadFilename,ffd);
              break;
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadReconFile("+loadFilename+")",e.toString());
    }
    finally
    {
      try{ in.close(); } catch( Exception e ) {}
    }
  }

  protected void loadRetrievalSystem( String loadFilename )
  {
    boolean             autoCommit      = getAutoCommit();
    ResultSetIterator   it              = null;
    ResultSet           resultSet       = null;

    try
    {
      setAutoCommit(false);
      
      /*@lineinfo:generated-code*//*@lineinfo:1201^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  retr.load_sec                     as load_sec,
//                  nvl(retr.merchant_number,0)       as merchant_number,
//                  retr.incoming_date                as incoming_date,
//                  nvl(retr.acq_reference_number,0)  as ref_num,
//                  retr.card_number                  as card_number,
//                  retr.transaction_date             as tran_date,
//                  retr.dispute_amount               as tran_amount,
//                  substr(retr.dba_name,1,32)        as merchant_name,
//                  retr.dispute_reason_code_alpha    as reason_code,
//                  retr.load_filename                as load_filename,
//                  retr.bank_number                  as bank_number,
//                  retr.card_number_enc              as card_number_enc,
//                  retr.dispute_case_number          as retrieval_request_id,
//                  decode(retr.merchant_number,                                        
//                         null,'Y','N')              as merchant_number_missing,
//                  'N'                               as tran_date_missing,
//                  ds.batch_date                     as dt_batch_date,
//                  ds.batch_id                       as dt_batch_number,
//                  ds.batch_record_id                as dt_ddf_dt_id,
//                  ds.trident_tran_id                as trident_tran_id,
//                  ds.purchase_id                    as dt_purchase_id,
//                  ds.client_reference_number        as client_ref_num,
//                  retr.additional_comments          as addl_comments,
//                  retr.additional_response_data     as addl_resp_data,
//                  decode(retr.message_type,
//                         2422,'DSP','INQ')          as inquiry_type
//          from    network_retrieval_discover  retr,
//                  discover_settlement         ds
//          where   retr.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                  and ds.rec_id(+) = retr.settlement_rec_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  retr.load_sec                     as load_sec,\n                nvl(retr.merchant_number,0)       as merchant_number,\n                retr.incoming_date                as incoming_date,\n                nvl(retr.acq_reference_number,0)  as ref_num,\n                retr.card_number                  as card_number,\n                retr.transaction_date             as tran_date,\n                retr.dispute_amount               as tran_amount,\n                substr(retr.dba_name,1,32)        as merchant_name,\n                retr.dispute_reason_code_alpha    as reason_code,\n                retr.load_filename                as load_filename,\n                retr.bank_number                  as bank_number,\n                retr.card_number_enc              as card_number_enc,\n                retr.dispute_case_number          as retrieval_request_id,\n                decode(retr.merchant_number,                                        \n                       null,'Y','N')              as merchant_number_missing,\n                'N'                               as tran_date_missing,\n                ds.batch_date                     as dt_batch_date,\n                ds.batch_id                       as dt_batch_number,\n                ds.batch_record_id                as dt_ddf_dt_id,\n                ds.trident_tran_id                as trident_tran_id,\n                ds.purchase_id                    as dt_purchase_id,\n                ds.client_reference_number        as client_ref_num,\n                retr.additional_comments          as addl_comments,\n                retr.additional_response_data     as addl_resp_data,\n                decode(retr.message_type,\n                       2422,'DSP','INQ')          as inquiry_type\n        from    network_retrieval_discover  retr,\n                discover_settlement         ds\n        where   retr.load_file_id = load_filename_to_load_file_id( :1 )\n                and ds.rec_id(+) = retr.settlement_rec_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.startup.DiscoverCBLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1233^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        long loadSec = resultSet.getLong("load_sec");
        try{
        /*@lineinfo:generated-code*//*@lineinfo:1240^9*/

//  ************************************************************
//  #sql [Ctx] { insert into network_retrievals
//            (
//              retr_load_sec,
//              merchant_number,
//              incoming_date,
//              inquiry_type,
//              reference_number,
//              card_number,
//              tran_date,
//              tran_amount,
//              merchant_name,
//              reason_code,
//              load_filename,
//              bank_number,
//              card_number_enc,
//              retrieval_request_id,
//              merchant_number_missing,
//              tran_date_missing,
//              dt_batch_date,
//              dt_batch_number,
//              dt_ddf_dt_id,
//              trident_tran_id,
//              dt_purchase_id,
//              client_reference_number
//            )
//            values
//            (
//              :loadSec,
//              :resultSet.getLong("merchant_number"),
//              :resultSet.getDate("incoming_date"),
//              :resultSet.getString("inquiry_type"),
//              :resultSet.getLong("ref_num"),
//              :resultSet.getString("card_number"),
//              :resultSet.getDate("tran_date"),
//              :resultSet.getDouble("tran_amount"),
//              :resultSet.getString("merchant_name"),
//              :resultSet.getString("reason_code"),
//              :resultSet.getString("load_filename"),
//              :resultSet.getInt("bank_number"),
//              :resultSet.getString("card_number_enc"),
//              :resultSet.getString("retrieval_request_id"),
//              :resultSet.getString("merchant_number_missing"),
//              :resultSet.getString("tran_date_missing"),
//              :resultSet.getDate("dt_batch_date"),
//              :resultSet.getLong("dt_batch_number"),
//              :resultSet.getLong("dt_ddf_dt_id"),
//              :resultSet.getString("trident_tran_id"),
//              :resultSet.getString("dt_purchase_id"),
//              :resultSet.getString("client_ref_num")
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_47 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_48 = resultSet.getDate("incoming_date");
 String __sJT_49 = resultSet.getString("inquiry_type");
 long __sJT_50 = resultSet.getLong("ref_num");
 String __sJT_51 = resultSet.getString("card_number");
 java.sql.Date __sJT_52 = resultSet.getDate("tran_date");
 double __sJT_53 = resultSet.getDouble("tran_amount");
 String __sJT_54 = resultSet.getString("merchant_name");
 String __sJT_55 = resultSet.getString("reason_code");
 String __sJT_56 = resultSet.getString("load_filename");
 int __sJT_57 = resultSet.getInt("bank_number");
 String __sJT_58 = resultSet.getString("card_number_enc");
 String __sJT_59 = resultSet.getString("retrieval_request_id");
 String __sJT_60 = resultSet.getString("merchant_number_missing");
 String __sJT_61 = resultSet.getString("tran_date_missing");
 java.sql.Date __sJT_62 = resultSet.getDate("dt_batch_date");
 long __sJT_63 = resultSet.getLong("dt_batch_number");
 long __sJT_64 = resultSet.getLong("dt_ddf_dt_id");
 String __sJT_65 = resultSet.getString("trident_tran_id");
 String __sJT_66 = resultSet.getString("dt_purchase_id");
 String __sJT_67 = resultSet.getString("client_ref_num");
   String theSqlTS = "insert into network_retrievals\n          (\n            retr_load_sec,\n            merchant_number,\n            incoming_date,\n            inquiry_type,\n            reference_number,\n            card_number,\n            tran_date,\n            tran_amount,\n            merchant_name,\n            reason_code,\n            load_filename,\n            bank_number,\n            card_number_enc,\n            retrieval_request_id,\n            merchant_number_missing,\n            tran_date_missing,\n            dt_batch_date,\n            dt_batch_number,\n            dt_ddf_dt_id,\n            trident_tran_id,\n            dt_purchase_id,\n            client_reference_number\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setLong(2,__sJT_47);
   __sJT_st.setDate(3,__sJT_48);
   __sJT_st.setString(4,__sJT_49);
   __sJT_st.setLong(5,__sJT_50);
   __sJT_st.setString(6,__sJT_51);
   __sJT_st.setDate(7,__sJT_52);
   __sJT_st.setDouble(8,__sJT_53);
   __sJT_st.setString(9,__sJT_54);
   __sJT_st.setString(10,__sJT_55);
   __sJT_st.setString(11,__sJT_56);
   __sJT_st.setInt(12,__sJT_57);
   __sJT_st.setString(13,__sJT_58);
   __sJT_st.setString(14,__sJT_59);
   __sJT_st.setString(15,__sJT_60);
   __sJT_st.setString(16,__sJT_61);
   __sJT_st.setDate(17,__sJT_62);
   __sJT_st.setLong(18,__sJT_63);
   __sJT_st.setLong(19,__sJT_64);
   __sJT_st.setString(20,__sJT_65);
   __sJT_st.setString(21,__sJT_66);
   __sJT_st.setString(22,__sJT_67);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1292^9*/
        }catch(Exception e){
          logEntry("loadRetrievalSystem(" + loadFilename + ") - insert stmt ",e.toString());
        }
        // retrievals are put into the MES queue system
        // by a trigger on the network_retrievals table
        
        
        // add additional comment (if present) to the q_notes
        for( int i = 0; i < 2; ++i )
        {
          String  value = resultSet.getString( ((i == 0) ? "addl_comments" : "addl_resp_data") );
          
          if ( value != null && !value.trim().equals("") )
          {
            QueueNotes.addNote(loadSec,MesQueues.Q_RETRIEVALS_DS_INCOMING,"system",value.trim());
          }
        }
      }
      resultSet.close();
      it.close();

      // queue the retrieval points and the linking of transaction and credits
      createProcessTableEntries(loadFilename);

      /*@lineinfo:generated-code*//*@lineinfo:1317^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1320^7*/
    }
    catch(Exception e)
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:1324^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1324^34*/ } catch( Exception sqe ) {}
      logEntry("loadRetrievalSystem(" + loadFilename + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      setAutoCommit(autoCommit);
    }
  }
  
  protected void notifyAdmins( String caseNumber )
  {
    try
    {
      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_OUTGOING_CB_FAILURE);
      msg.setSubject("Discover Chargeback Resolution Failure - " + caseNumber);
      msg.setText("Failed to find a valid control number for case number '" + caseNumber + "'.");
      msg.send();
    }
    catch( Exception e )
    {
      logEntry("notifyAdmins(" + caseNumber + ")",e.toString());
    }
  }
  
  protected void notifyChargebackDept( String loadFilename )
  {
    int                   addrId        = -1;
    ResultSetIterator     it            = null;
    ResultSet             resultSet     = null;
    int                   rowCount      = 0;
    String                subject       = null;
    String                tableName     = null;
    
    String[]              subjects      = new String[] {"Incoming Discover Chargebacks",
                                                        "Incoming Discover Retrievals"};
    String[]              tableNames    = new String[] {"network_chargeback_discover",
                                                        "network_retrieval_discover"};
    int[]                 addrIds       = new int[]    {MesEmails.MSG_ADDRS_DISCOVER_CB_NOTIFY,
                                                        MesEmails.MSG_ADDRS_DISCOVER_RETR_NOTIFY};                                                         

    try
    {
      for( int i = 0; i < 2; ++ i )
      {
        addrId    = addrIds[i];
        tableName = tableNames[i];
        subject   = subjects[i];
        rowCount  = 0;

        /*@lineinfo:generated-code*//*@lineinfo:1375^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  'C'                       as item_type,
//                    acquirer_id               as acquirer_id,
//                    count(1)                  as total_count,
//                    sum(transaction_amount)   as total_amount
//            from    :tableName
//            where   load_file_id = load_filename_to_load_file_id(:loadFilename)
//            group by acquirer_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  'C'                       as item_type,\n                  acquirer_id               as acquirer_id,\n                  count(1)                  as total_count,\n                  sum(transaction_amount)   as total_amount\n          from     ");
   __sjT_sb.append(tableName);
   __sjT_sb.append(" \n          where   load_file_id = load_filename_to_load_file_id( ? )\n          group by acquirer_id");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "14com.mes.startup.DiscoverCBLoader:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1384^9*/
        resultSet = it.getResultSet();

        StringBuffer buffer = new StringBuffer();
        buffer.append( StringUtilities.leftJustify("Acquirer ID",11,' ') );
        buffer.append( StringUtilities.rightJustify("Count"     , 9,' ') );
        buffer.append( StringUtilities.rightJustify("Amount"    ,15,' ') );
        buffer.append("\n");
        buffer.append( StringUtilities.leftJustify(""         , 40,'=') );
        buffer.append("\n");

        while( resultSet.next() )
        {
          buffer.append( StringUtilities.leftJustify  (resultSet.getString("acquirer_id") ,11,' ') );
          buffer.append( StringUtilities.rightJustify (resultSet.getString("total_count") , 9,' ') );
          buffer.append( StringUtilities.rightJustify (MesMath.toCurrency(resultSet.getDouble("total_amount")), 15,' ') );
          buffer.append("\n");
          ++rowCount;
        }
        resultSet.close();
        it.close();

        if ( rowCount > 0 )
        {
          MailMessage msg = new MailMessage();
          msg.setAddresses(addrId);
          msg.setSubject(subject + " - " + loadFilename);
          msg.setText(buffer.toString());
          msg.send();
        }
      }
    }
    catch( Exception e )
    {
      logEntry("notifyChargebackDept(" + loadFilename + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ){}
    }
  }
  
  protected void processChargebackNotification( FlatFileRecord ffd )
  {
    String                caseNumber        = null;
    long                  loadSec           = 0L;
    
    try
    {
      System.out.println("processing chargeback notification");
      caseNumber  = ffd.getFieldData("life_cycle_id_data").substring(1,11);
      System.out.println("caseNumber = " + caseNumber);
      
      loadSec     = resolveLoadSec(caseNumber);
      
      if ( loadSec != 0L )
      {
        String  note    = ffd.getFieldData("additional_comments").trim() + "\n" + 
                          ffd.getFieldData("system_generated_comments").trim();
        int     qtype   = QueueTools.getCurrentQueueType(loadSec, MesQueues.Q_ITEM_TYPE_CHARGEBACK);
                          
        if ( note.trim().length() > 1 && qtype > 0 )
        {
          QueueNotes.addNote(loadSec,qtype,"system",note);
        }
      }
      else    // failed to resolve chargeback, alert admins
      {
        notifyAdmins(caseNumber);
      }
    }
    catch( Exception e )
    {
      logEntry("processChargebackNotification(" + caseNumber + ")", e.toString());
    }
    finally
    {
    }
  }
  
  protected void processChargebackReversal( FlatFileRecord ffd )
  {
    String                caseNumber        = null;
    long                  loadSec           = 0L;
    
    try
    {
      caseNumber  = ffd.getFieldData("life_cycle_id_data").substring(1,11);
      loadSec     = resolveLoadSec(caseNumber);
      
      if ( loadSec != 0L )
      {
        java.util.Date javaDate = DateTimeFormatter.parseDate(ffd.getFieldData("transmission_date").trim(),"yyyyMMdd");
        if ( javaDate == null )
        {
          javaDate = Calendar.getInstance().getTime();
        }
        java.sql.Date reversalDate = new java.sql.Date(javaDate.getTime());
      
        ChargebackTools.reverseChargeback(Ctx,"DS",
                                          loadSec,
                                          reversalDate,
                                          TridentTools.decodeCobolAmount(ffd.getFieldData("dispute_amount").trim(),2));
      }
      else    // failed to resolve chargeback, alert admins
      {
        notifyAdmins(caseNumber);
      }
    }
    catch( Exception e )
    {
      logEntry("processChargebackReversal(" + caseNumber + ")", e.toString());
    }
    finally
    {
    }
  }
  
  protected void processChargebackSecondTime( FlatFileRecord ffd )
  {
    String                caseNumber        = null;
    int                   cbAdjPos          = 0;
    long                  loadSec           = 0L;
    
    try
    {
      caseNumber  = ffd.getFieldData("life_cycle_id_data").substring(1,11);
      loadSec     = resolveLoadSec(caseNumber);
      
      if ( loadSec != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1515^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl( sum(decode(ac.adj_tran_code,'9071',1,'9077',-1,0)), 0 ) 
//            
//            from    network_chargeback_activity   cba,
//                    chargeback_action_codes       ac
//            where   cba.cb_load_sec(+) = :loadSec
//                    and ac.short_action_code(+) = cba.action_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl( sum(decode(ac.adj_tran_code,'9071',1,'9077',-1,0)), 0 ) \n           \n          from    network_chargeback_activity   cba,\n                  chargeback_action_codes       ac\n          where   cba.cb_load_sec(+) =  :1 \n                  and ac.short_action_code(+) = cba.action_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.DiscoverCBLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cbAdjPos = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1523^9*/
        
        if ( cbAdjPos == 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:1527^11*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargeback_activity
//              (
//                cb_load_sec,
//                file_load_sec,
//                action_code,
//                action_date,
//                user_message,
//                action_source,
//                user_login
//              )
//              select  :loadSec,
//                      nvl(max(cba.file_load_sec),0)+1,
//                      'D',
//                      trunc(sysdate),
//                      'Auto Action - Discover Representment Denied',
//                      :MesChargebacks.AS_AUTO_DISCOVER, -- 11 auto discover
//                      'system'
//              from    network_chargeback_activity   cba
//              where   cba.cb_load_sec(+) = :loadSec
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into network_chargeback_activity\n            (\n              cb_load_sec,\n              file_load_sec,\n              action_code,\n              action_date,\n              user_message,\n              action_source,\n              user_login\n            )\n            select   :1 ,\n                    nvl(max(cba.file_load_sec),0)+1,\n                    'D',\n                    trunc(sysdate),\n                    'Auto Action - Discover Representment Denied',\n                     :2 , -- 11 auto discover\n                    'system'\n            from    network_chargeback_activity   cba\n            where   cba.cb_load_sec(+) =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setInt(2,MesChargebacks.AS_AUTO_DISCOVER);
   __sJT_st.setLong(3,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1548^11*/
        
          int curQueue = QueueTools.getCurrentQueueType(loadSec, MesQueues.Q_ITEM_TYPE_CHARGEBACK);
          if ( curQueue > 0 )
          {
            QueueTools.moveQueueItem(loadSec,
                                     curQueue,
                                     MesQueues.Q_CHARGEBACKS_MCHB,
                                     null,
                                     null);
          }                                   
        }
      }
      else    // failed to resolve chargeback, alert admins
      {
        notifyAdmins(caseNumber);
      }
    }
    catch( Exception e )
    {
      logEntry("processChargebackSecondTime(" + caseNumber + ")", e.toString());
    }
    finally
    {
    }
  }
  
  public void reloadDisputesFile( String loadFilename )
  {
    loadChargebackFile  ( loadFilename );
    loadChargebackSystem( loadFilename );
    loadRetrievalSystem ( loadFilename );
    notifyChargebackDept( loadFilename );
  }
  
  protected long resolveLoadSec( String caseNumber )
  {
    ResultSetIterator     it            = null;
    long                  loadSec       = 0L;
    ResultSet             resultSet     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1591^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cbd.load_sec          as load_sec
//          from    network_chargeback_discover   cbd
//          where   cbd.dispute_case_number = :caseNumber
//          order by cbd.incoming_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cbd.load_sec          as load_sec\n        from    network_chargeback_discover   cbd\n        where   cbd.dispute_case_number =  :1 \n        order by cbd.incoming_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,caseNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.startup.DiscoverCBLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1597^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        loadSec = resultSet.getLong("load_sec");
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("resolveLoadSec(" + caseNumber + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
    return( loadSec );
  }
  
  protected void storeIncomingReject( String loadFilename, FlatFileRecord ffd ) 
  {
    String  detailRecord      = ffd.getFieldData("detail_record");
    long    discoverMid       = 0L;
    int     recCount          = 0;
    long    recId             = 0L;
    Vector  recIds            = new Vector();
    int     rejectType        = -1;
    
    try
    {
      rejectType  = ffd.getFieldAsInt ("reject_type");
      
      if ( rejectType == 1 )          // full file reject
      {
        //@ TODO:  determine which file rejected and select all rec_ids
        //@ #sql [Ctx] it = 
        //@ {
        //@   select  rec_id
        //@   from    discover_settlement   s
        //@   where   output_file_id = :loadFileId
        //@ };
        logEntry("storeIncomingReject(" + loadFilename + ")","File reject detected");
      }
      else if ( rejectType == 3 )     // batch reject
      {
        ResultSetIterator it        = null;
        ResultSet         resultSet = null;
      
        try
        {
          java.util.Date  javaDate  = DateTimeFormatter.parseDate(detailRecord.substring(41,49).trim(),"yyyyMMdd");
          java.sql.Date   fileDate  = new java.sql.Date(javaDate.getTime());
          
          // get the discover mid from the batch header record
          discoverMid = Long.parseLong(detailRecord.substring(13,28).trim());
          /*@lineinfo:generated-code*//*@lineinfo:1653^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  rec_id
//              from    discover_settlement   s
//              where   s.discover_merchant_number = :discoverMid
//                      and s.batch_date between :fileDate-3 and :fileDate
//                      and s.batch_number = :detailRecord.substring(33,41).trim()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_68 = detailRecord.substring(33,41).trim();
  try {
   String theSqlTS = "select  rec_id\n            from    discover_settlement   s\n            where   s.discover_merchant_number =  :1 \n                    and s.batch_date between  :2 -3 and  :3 \n                    and s.batch_number =  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,discoverMid);
   __sJT_st.setDate(2,fileDate);
   __sJT_st.setDate(3,fileDate);
   __sJT_st.setString(4,__sJT_68);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.startup.DiscoverCBLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1660^11*/
          resultSet = it.getResultSet();
        
          while( resultSet.next() )
          {
            recIds.addElement(resultSet.getString("rec_id"));
          }
          resultSet.close();
          it.close();
        }
        catch( Exception batch_e )
        {
          logEntry("storeIncomingReject(" + loadFilename + ")",batch_e.toString());
        }
        finally
        {
          try{ it.close(); } catch( Exception ee ) {}
        }
      }
      else if ( rejectType == 4 )     // detail reject
      {
        discoverMid = ffd.getFieldAsLong("discover_merchant_number");
        recIds.addElement(detailRecord.substring(181,204).trim());
      }
      else
      {
        logEntry("storeIncomingReject(" + loadFilename + ")","Invalid Discover reject type " + rejectType);
      }
    
      for( int i = 0; i < recIds.size(); ++i )
      {
        try
        {
          recId = Long.parseLong((String)recIds.elementAt(i));        
        }
        catch( NumberFormatException nfe )
        {
          continue;
        }
      
        /*@lineinfo:generated-code*//*@lineinfo:1700^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//            
//            from    discover_settlement   ds
//            where   ds.rec_id = :recId
//                    and ds.discover_merchant_number = :discoverMid
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n           \n          from    discover_settlement   ds\n          where   ds.rec_id =  :1 \n                  and ds.discover_merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.startup.DiscoverCBLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setLong(2,discoverMid);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1707^9*/

        if ( recCount == 0 )
        {
          try
          {
            Date    tranDate = new Date( DateTimeFormatter.parseDate(detailRecord.substring(21,29),"yyyyMMdd").getTime() );
            String  refNum   = detailRecord.substring(61,77).trim();
          
            /*@lineinfo:generated-code*//*@lineinfo:1716^13*/

//  ************************************************************
//  #sql [Ctx] { select  rec_id
//                
//                from    discover_settlement ds
//                where   ds.discover_merchant_number = :discoverMid
//                        and ds.transaction_date = :tranDate
//                        and ds.reference_number = to_number(:refNum)
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  rec_id\n               \n              from    discover_settlement ds\n              where   ds.discover_merchant_number =  :1 \n                      and ds.transaction_date =  :2 \n                      and ds.reference_number = to_number( :3 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.startup.DiscoverCBLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,discoverMid);
   __sJT_st.setDate(2,tranDate);
   __sJT_st.setString(3,refNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1724^13*/
          }
          catch( Exception nodata_e )
          {
            recId = 0L;
          }
        }
      
        if ( recId != 0L )
        {
          int round = 0;
          /*@lineinfo:generated-code*//*@lineinfo:1735^11*/

//  ************************************************************
//  #sql [Ctx] { select  (count(1) + 1)
//              
//              from    reject_record
//              where   rec_id = :recId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  (count(1) + 1)\n             \n            from    reject_record\n            where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.startup.DiscoverCBLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   round = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1741^11*/
      
          String reasonCode = ffd.getFieldData("reject_reason_code").trim();
          /*@lineinfo:generated-code*//*@lineinfo:1744^11*/

//  ************************************************************
//  #sql [Ctx] { insert into reject_record
//              (
//                rec_id,
//                reject_id,
//                reject_type,
//                status,
//                round,
//                reject_date,
//                presentment,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :recId,
//                :reasonCode,
//                'DS',
//                -1,
//                :round,
//                get_file_date(:loadFilename),
//                0,
//                :loadFilename,
//                load_filename_to_load_file_id(:loadFilename)
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into reject_record\n            (\n              rec_id,\n              reject_id,\n              reject_type,\n              status,\n              round,\n              reject_date,\n              presentment,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n              'DS',\n              -1,\n               :3 ,\n              get_file_date( :4 ),\n              0,\n               :5 ,\n              load_filename_to_load_file_id( :6 )\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setString(2,reasonCode);
   __sJT_st.setInt(3,round);
   __sJT_st.setString(4,loadFilename);
   __sJT_st.setString(5,loadFilename);
   __sJT_st.setString(6,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1770^11*/
        
          /*@lineinfo:generated-code*//*@lineinfo:1772^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//              
//              from    reject_type
//              where   type = 'DS' 
//                      and id = :reasonCode
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n             \n            from    reject_type\n            where   type = 'DS' \n                    and id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.startup.DiscoverCBLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,reasonCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1779^11*/
        
          if ( recCount == 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:1783^13*/

//  ************************************************************
//  #sql [Ctx] { insert into reject_type
//                  ( type,id,description )
//                values
//                  ( 'DS',:reasonCode,trim(:ffd.getFieldData("reject_reason_desc")) )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_69 = ffd.getFieldData("reject_description");
   String theSqlTS = "insert into reject_type\n                ( type,id,description )\n              values\n                ( 'DS', :1 ,trim( :2 ) )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,reasonCode);
   __sJT_st.setString(2,__sJT_69);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1789^13*/
          }
        
          if ( QueueTools.hasItemEnteredQueue(recId, MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS) )
          {
            QueueTools.moveQueueItem(recId,
                                     QueueTools.getCurrentQueueType(recId, MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS),
                                     MesQueues.Q_REJECTS_UNWORKED,
                                     null,
                                     null);
          }
          else  //then need to add item to q_data
          {
            QueueTools.insertQueue(recId, MesQueues.Q_REJECTS_UNWORKED);
          }
        }
      }   // end for loop through rec ids
    }
    catch( Exception e )
    {
      logEntry("storeIncomingReject(" + discoverMid + ")",e.toString());
    }
    finally
    {
    }
  }

  /***
   * This method take filename and reloads chargeback and retrieval entries if not already
   * loaded in database tables. The original file is compared with DB records and unmatched records are copied to another file.
   * That new file is sent to loadChargebackFile(String filename) method.
   * 
   * Example : 
   * Orginal file name = ds_cb9999_101313_002.dat
   * New file will be generated with incremented number = ds_cb9999_101313_003.dat
   * 
   * Warning: This method can duplicate records if same original file is used more than once. 
   * 
   * @param filename
   */
  private void reloadDiscoverChargeBacks(String filename) {
    final String NETWORK_CHARGEBACK_DISCOVER = "network_chargeback_discover";
    final String NETWORK_RETRIEVAL_DISCOVER = "network_retrieval_discover";
    
    log.info("Reloading chargeback file : " + filename );
    ArrayList recordsToLoad = new ArrayList();    

    //validate filename is not blank.    
    if (StringUtils.isBlank(filename)) {
      logEntry("reloadDiscoverChargeBacks() ", "filename is blank or null");
      throw new MesApiInvalidRequestException("File is blank or null : " + filename);
    }


    HashMap allRecords = getFileRecords(filename);
    
    HashMap objectMap = (HashMap)allRecords.get("ncd_object_map");    

    // chargeback records from file
    Collection cbRecordsFromFile = (ArrayList) allRecords.get(NETWORK_CHARGEBACK_DISCOVER);
    log.info("Number of chargeback records from file = " + cbRecordsFromFile.size());
    if (cbRecordsFromFile == null || cbRecordsFromFile.size() == 0) {
      logEntry("reloadDiscoverChargeBacks() ", "File does not have any chargeback records to reload.");
    }
    
    //retrieval records from file
    Collection retrievalRecordsFromFile = (ArrayList) allRecords.get(NETWORK_RETRIEVAL_DISCOVER);
    log.info("Number of retrieval records from file = " + retrievalRecordsFromFile.size());
    if (retrievalRecordsFromFile == null || retrievalRecordsFromFile.size() == 0) {
      logEntry("reloadDiscoverChargeBacks() ", "File does not have any retrieval records to reload.");
    } 

    // records from db
    Collection chbackDBRecords = getDBRecords(FileUtils.getNameFromFullPath(filename).toLowerCase(), NETWORK_CHARGEBACK_DISCOVER);
    Collection retrievalDBRecords = getDBRecords(FileUtils.getNameFromFullPath(filename).toLowerCase(), NETWORK_RETRIEVAL_DISCOVER);
    log.info("Number of matching records in charegeback table = " + chbackDBRecords.size());
    log.info("Number of matching records in retrieval table= " + retrievalDBRecords.size());
    


    //compare file and db records. check in network_chargeback_discover and then in network_charegebaks table. If not found there then add to new collection.

    try{
      Iterator itr = cbRecordsFromFile.iterator();    
      //Chargeback records compare
      if (chbackDBRecords != null && chbackDBRecords.size() > 0) {     
        while (itr.hasNext()) {
          NetworkChargebackDiscoverRecord cb = (NetworkChargebackDiscoverRecord) itr.next();
          if (!checkIfLoaded(cb, chbackDBRecords)) {         
            //Now check in network_chargebacks table.
            if(!isInNetworkChargebacks(cb)){
              recordsToLoad.add(objectMap.get(cb.getDisputeCaseNumber()));   
            }
          }
        }

      } else {
        // all records are new. add all records to collection
        while(itr.hasNext()){
          NetworkChargebackDiscoverRecord cb = (NetworkChargebackDiscoverRecord) itr.next();        
          recordsToLoad.add(objectMap.get(cb.getDisputeCaseNumber()));
        }
      } 
      
      
      //retrieval records compare
      Iterator retrievalItr = retrievalRecordsFromFile.iterator();
      
      if(retrievalDBRecords != null && retrievalDBRecords.size() > 0){
        while(retrievalItr.hasNext()){
          NetworkChargebackDiscoverRecord cb = (NetworkChargebackDiscoverRecord)retrievalItr.next();
          if(!checkIfLoaded(cb, retrievalDBRecords)){
            if(!isInNetworkChargebacks(cb)){
              recordsToLoad.add(objectMap.get(cb.getDisputeCaseNumber()));   
            }
          }
        }
        
      }else{
        //all are new. add all records to reload
        while(itr.hasNext()){
          NetworkChargebackDiscoverRecord cb = (NetworkChargebackDiscoverRecord) retrievalItr.next();        
          recordsToLoad.add(objectMap.get(cb.getDisputeCaseNumber()));
        }
      }
      
    }catch(Exception e){
      logEntry("reloadDiscoverChargeBacks() ", e);
    }


    //copy to another file and load
    if (recordsToLoad.size() > 0) {     
      String fileToload = copyFile(filename, recordsToLoad);
      log.info("Calling loadChargebackFile() with filename = " + fileToload); 
      loadChargebackFile(fileToload);
      log.info("Done reloading records.");
    } else {
      log.info("No missing records found");
    }
  }

  /***
   * This method verify if the transaction was manually loaded in network_chargebacks table
   * @param cb
   */
  private boolean isInNetworkChargebacks(NetworkChargebackDiscoverRecord cb) {    
    ResultSetIterator it = null;
    ResultSet resultSet = null;
    double tranAmt = cb.getTransactionAmount();

    String tranDate = cb.getTransactionDate();    
    java.util.Date dt = formattedStringDate(tranDate, "yyyyMMdd");
    tranDate = formattedDateAsString(dt, "dd-MMM-yy");    

    String cardNumber = cb.getCardNumber();    
    try{
      /*@lineinfo:generated-code*//*@lineinfo:1946^7*/

//  ************************************************************
//  #sql [Ctx] it = { select * from network_chargebacks nc
//            where NC.CARD_TYPE ='DS'
//            and nc.load_filename = 'system'
//            and nc.tran_amount = :tranAmt
//            and nc.tran_date = :tranDate
//            and nc.card_number = :cardNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select * from network_chargebacks nc\n          where NC.CARD_TYPE ='DS'\n          and nc.load_filename = 'system'\n          and nc.tran_amount =  :1 \n          and nc.tran_date =  :2 \n          and nc.card_number =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.startup.DiscoverCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,tranAmt);
   __sJT_st.setString(2,tranDate);
   __sJT_st.setString(3,cardNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"25com.mes.startup.DiscoverCBLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1954^9*/
      resultSet = it.getResultSet();
      if(resultSet.next()){        
        log.info("Item found in network_chargebacks table = " + resultSet.getString("card_number"));
        return true;
      }

    }catch(Exception e){
      logEntry("isInNetworkChargebacks()", e.getMessage());
      e.printStackTrace();
    }

    return false;
  }

  private String copyFile(String originalFileName, ArrayList cbRecords) {  
    BufferedReader in = null;
    BufferedWriter out = null;
    String line = null;
    //String fileName = FileUtils.getNameFromFullPath(originalFileName).toLowerCase();
    String newFileName = getNewFileName(originalFileName);
    log.info("Copying to new file = " + newFileName);

    File newFile = new File(newFileName);

    try {
      in = new BufferedReader(new FileReader(originalFileName));
      out = new BufferedWriter(new FileWriter(newFile));    

      while ((line = in.readLine()) != null) {
        if (line.charAt(1) == '8') // header or trailer
        {
          if (line.substring(12, 15).endsWith("841")) {            
            out.write(line);
            out.newLine();

            // copy records
            Iterator itr = cbRecords.iterator();
            while (itr.hasNext()) {
              String str = (String)itr.next();
              out.write(str);
              out.newLine();
            }
          }else if (line.substring(12, 15).endsWith("842")){
            //trailer line            
            out.write(line);
            out.newLine();
          }

        } 
      }      
      out.flush();
      out.close();
      in.close();
    } catch (FileNotFoundException e) {
      logEntry("copyFile() ", e.getMessage());
    } catch (IOException e) {
      logEntry("copyFile() ", e.getMessage());      
    }
    return newFileName;
  }

  /***
   * 
   * @param fileName  eg:C:\ds_cb9999_10131_003.dat
   * @return filename with incremented file number . Eg : C:\ds_cb9999_101313_003.dat
   */
  private String getNewFileName(String filename) {
    if (null == filename || StringUtils.isBlank(filename)) {
      return filename;
    } else {
      String fileNumber = filename.substring(filename.lastIndexOf("_") + 1,
          filename.lastIndexOf("."));
      fileNumber = NumberFormatter.getPaddedInt(Integer.parseInt(fileNumber) + 1, 3);

      String newName = filename.replaceFirst(
          filename.substring(filename.lastIndexOf("_") + 1, filename.lastIndexOf(".")), fileNumber);

      return newName;
    }

  }

  private boolean checkIfLoaded(NetworkChargebackDiscoverRecord ncdFromFile, Collection dbCollection) {
    if (dbCollection.contains(ncdFromFile))
      return true;

    return false;
  }

  private Collection getDBRecords(String filename, String tableName) {
    ResultSetIterator it = null;
    ResultSet rs = null;
    NetworkChargebackDiscoverRecord ncdObject = null;
    Collection coll = new ArrayList();
    try{
      /*@lineinfo:generated-code*//*@lineinfo:2050^7*/

//  ************************************************************
//  #sql [Ctx] it = { select ncd.load_filename,
//            ncd.message_type,
//            ncd.action_code,
//            ncd.function_code,
//            ncd.merchant_number,
//            ncd.discover_merchant_number,
//            ncd.transaction_date,
//            ncd.transaction_amount,
//            ncd.dispute_case_number,
//            ncd.card_number
//            from :tableName ncd
//            where ncd.load_filename = :filename
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select ncd.load_filename,\n          ncd.message_type,\n          ncd.action_code,\n          ncd.function_code,\n          ncd.merchant_number,\n          ncd.discover_merchant_number,\n          ncd.transaction_date,\n          ncd.transaction_amount,\n          ncd.dispute_case_number,\n          ncd.card_number\n          from  ");
   __sjT_sb.append(tableName);
   __sjT_sb.append("  ncd\n          where ncd.load_filename =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "26com.mes.startup.DiscoverCBLoader:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,filename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2064^9*/

      rs = it.getResultSet();
      while(rs.next()){
        ncdObject = new NetworkChargebackDiscoverRecord();
        ncdObject.setActionCode(rs.getInt("action_code"));
        ncdObject.setMessageType(rs.getString("message_type"));
        ncdObject.setFunctionCode(rs.getInt("function_code"));
        ncdObject.setDisputeCaseNumber(rs.getString("dispute_case_number"));        
        ncdObject.setDiscoverMerchantNumber(rs.getString("discover_merchant_number"));
        ncdObject.setTransactionAmount(rs.getDouble("transaction_amount"));
        ncdObject.setTransactionDate(formattedDateAsString(rs.getDate("transaction_date"),"yyyyMMdd"));  
        ncdObject.setCardNumber(rs.getString("card_number"));        
        coll.add(ncdObject);  
      }

    }catch(SQLException e){
      logEntry("getDBRecords()", e.getMessage());
    }catch(Exception e){
      logEntry("getDBRecords()", e.getMessage());
    }

    return coll;

  }

  private HashMap getFileRecords(String filename) {    
    FlatFileRecord ffd = null;
    HashMap ffds = new HashMap();
    BufferedReader in = null;
    String line = null;
    String recType = null;
    DisputeType disputeType = null;
    String tableName = null;
    Collection retrivalCollection = new ArrayList();
    Collection chargebackCollection = new ArrayList();    
    HashMap ffdMap =  new HashMap();
    HashMap allRecordsMap = new HashMap();

    log.info("Reading Chargeback File (" + filename + ")");

    ffds.put("2804/841", new FlatFileRecord(Ctx, MesFlatFiles.DEF_TYPE_DISCOVER_CB_HDR));
    ffds.put("DTL", new FlatFileRecord(Ctx, MesFlatFiles.DEF_TYPE_DISCOVER_CB_DTL_INCOMING));
    ffds.put("2804/842", new FlatFileRecord(Ctx, MesFlatFiles.DEF_TYPE_DISCOVER_CB_TRL));

    String loadFilename = FileUtils.getNameFromFullPath(filename).toLowerCase();
    try {
      in = new BufferedReader(new FileReader(filename));

      int recNum = 1;
      while ((line = in.readLine()) != null) {        
        ++recNum;
        if (line.charAt(1) == '8') // header or trailer
        {
          recType = line.substring(0, 4) + "/" + line.substring(12, 15);
        } else {
          recType = "DTL"; // use common detail record
        }
        ffd = (FlatFileRecord) ffds.get(recType);

        if (ffd != null) {
          ffd.resetAllFields();
          ffd.suck(line);

          if ("DTL".equals(recType)) {
            tableName = null;
            disputeType = loadDisputeType(ffd.getFieldData("message_type"),
                ffd.getFieldData("function_code"), ffd.getFieldData("action_code"));

            if (disputeType.isRetrievalRequest()) {
              tableName = "network_retrieval_discover";
              //convert ffd record to object
              NetworkChargebackDiscoverRecord ncd =  getNetworkChargebackRecordObject(ffd);
              ffdMap.put(ncd.getDisputeCaseNumber(), ffd.spew());
              retrivalCollection.add(ncd);              
             
            } else if (disputeType.isChargebackFirstTime() || disputeType.isChargebackPreArb()) {
              tableName = "network_chargeback_discover";
              // convert ffd record to object
              NetworkChargebackDiscoverRecord ncd =  getNetworkChargebackRecordObject(ffd);
              ffdMap.put(ncd.getDisputeCaseNumber(), ffd.spew());
              chargebackCollection.add(ncd);              
            }
          }
        }
      }
      
      allRecordsMap.put("network_chargeback_discover", chargebackCollection);
      allRecordsMap.put("network_retrieval_discover", retrivalCollection);      
      allRecordsMap.put("ncd_object_map", ffdMap);

    } catch (FileNotFoundException e) {

      e.printStackTrace();
    } catch (IOException e) {

      e.printStackTrace();
    }

    return allRecordsMap;
  }

  private NetworkChargebackDiscoverRecord getNetworkChargebackRecordObject(FlatFileRecord ffd) {
    NetworkChargebackDiscoverRecord ncd = new NetworkChargebackDiscoverRecord();
    ncd.setActionCode(ffd.getFieldAsLong("action_code"));
    ncd.setMessageType(ffd.getFieldData("message_type"));
    ncd.setFunctionCode(ffd.getFieldAsInt("function_code"));
    ncd.setDiscoverMerchantNumber(ffd.getFieldData("discover_merchant_number"));
    ncd.setTransactionAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("transaction_amount")
        .trim(), 2));
    ncd.setTransactionDate(formattedDateAsString(ffd.getFieldAsDate("transaction_date"), "yyyyMMdd"));
    ncd.setDisputeCaseNumber(ffd.getFieldData("life_cycle_id_data").substring(1, 11).trim());    
    ncd.setCardNumber(ffd.getFieldData("card_number").trim());
    return ncd;
  }

  private String formattedDateAsString(java.util.Date date, String pattern) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
    return dateFormat.format(date);
  }

  private java.util.Date formattedStringDate(String date, String pattern) {
    java.util.Date dt = null;
    SimpleDateFormat formatter = new SimpleDateFormat(pattern);
    try {
      dt = formatter.parse(date);
    } catch (ParseException e) {     
      e.printStackTrace();
    }
    return dt;
  }


  public static void main(String[] args) {
    
    DiscoverCBLoader t = null;    

     try {
       if (args.length > 0 && args[0].equals("testproperties")) {
         EventBase.printKeyListStatus(new String[]{
                 MesDefaults.DK_DISCOVER_MAP_ARI_PW,
                 MesDefaults.DK_DISCOVER_MAP_PRESP_USER,
                 MesDefaults.DK_DISCOVER_MAP_PRESP_PW,
                 MesDefaults.DK_DISCOVER_MAP_PROFILE_HOST
         });
       }


     SQLJConnectionBase.initStandalone("DEBUG");
    
     t = new DiscoverCBLoader();
     t.connect(true);
    
     if ("archiveDailyFile".equals(args[0])) {
     t.archiveDailyFile(args[1]);
     } else if ("loadArdefFile".equals(args[0])) {
     t.loadArdefFile(args[1]);
     } else if ("loadChargebackFile".equals(args[0])) {
     t.loadChargebackFile(args[1]);
     } else if ("loadChargebackSystem".equals(args[0])) {
     t.loadChargebackSystem(args[1]);
     } else if ("loadRetrievalSystem".equals(args[0])) {
     t.loadRetrievalSystem(args[1]);
     } else if ("loadReconFile".equals(args[0])) {
     t.loadReconFile(args[1]);
     } else if ("loadIMapFile".equalsIgnoreCase(args[0])) {
     t.loadIMapFile(args[1]);
     } else if ("notifyChargebackDept".equals(args[0])) {
     t.notifyChargebackDept(args[1]);
     } else if ("reloadDisputesFile".equals(args[0])) {
     t.reloadDisputesFile(args[1]);
     } else if ("reloadChargebackFile".equals(args[0])) {
     t.reloadDiscoverChargeBacks(args[1]);
     } else if ("execute".equals(args[0])) {
     t.setEventArgs(args[1]);
     t.execute();
     } else {
     System.out.println("Unrecognized command: " + args[0]);
     }
     } catch (Exception e) {
       e.printStackTrace();
     } finally {
     try {
     t.cleanUp();
     } catch (Exception e) {
     }
     try {
     OracleConnectionPool.getInstance().cleanUp();
     } catch (Exception e) {
     }
     }
     Runtime.getRuntime().exit(0);

 }
 
	public void loadIMapFile(String inputFilename) {
		boolean autoCommit = getAutoCommit();
		ResultSetIterator it = null;
		ResultSet resultSet = null;
		long loadFileId = 0L;
		String loadFilename = null;
		String discCaid = null;
		Date batchDate = null;
		double amount = 0.0;

		try {
			log.info("Loading IMap File (" + inputFilename + ")");
			Map allRecords = getReportRecords(inputFilename);
			log.info("getting real load filename...");
			loadFilename = FileUtils.getNameFromFullPath(inputFilename).toLowerCase();
			log.info("...real load filename = " + loadFilename);
			loadFileId = loadFilenameToLoadFileId(loadFilename);
			log.info("loadFileId = " + loadFileId);

			Iterator<Map.Entry<?, ?>> entries = allRecords.entrySet().iterator();
			while (entries.hasNext()) {
				Map.Entry<?, ?> entry = entries.next();
				Map detailsMap = (HashMap) entry.getValue();
				if (!detailsMap.isEmpty()) {
					discCaid = (String) detailsMap.get("discCaid");
					batchDate = Date.valueOf((String) detailsMap.get("batchDate"));
					amount = Double.valueOf((String) detailsMap.get("amount"));
				}

				try {
					setAutoCommit(false);

					// declare temps
					OraclePreparedStatement pStatement = null;
					DefaultContext defaultCtx = Ctx;

					if (defaultCtx == null) {
						RuntimeRefErrors.raise_NULL_CONN_CTX();
					} else {
						OracleContext oracleCtx = ((defaultCtx.getExecutionContext() == null)
								? ExecutionContext.raiseNullExecCtx()
								: defaultCtx.getExecutionContext().getOracleContext());
						try {
							String theSqlTS = "select -1, 1, 0, sysdate, trunc(sysdate), merchant_number, rec_id, 'I001', 'DS',  :1 ,  :2 "
									+ "from discover_settlement ds " + "where ds.merchant_number in "
									+ "(select distinct merchant_number from MES.TRIDENT_PROFILE tp where tp.disc_caid =  :3 ) "
									+ "and ds.batch_date =  :4  and ds.transaction_amount =  :5 ";
							pStatement = oracleCtx.prepareOracleStatement(defaultCtx,
									"27com.mes.startup.DiscoverCBLoader", theSqlTS);
							// set IN parameters
							pStatement.setString(1, loadFilename);
							pStatement.setLong(2, loadFileId);
							pStatement.setString(3, discCaid);
							pStatement.setDate(4, batchDate);
							pStatement.setDouble(5, amount);
							// execute query
							it = new sqlj.runtime.ref.ResultSetIterImpl(
									new sqlj.runtime.ref.OraRTResultSet(oracleCtx.oracleExecuteQuery(), pStatement,
											"27com.mes.startup.DiscoverCBLoader", null));
						} finally {
							oracleCtx.oracleCloseQuery();
						}
					}

					resultSet = (it == null) ? null : it.getResultSet();
					if (resultSet != null && resultSet.next()) {

						// declare temps
						pStatement = null;

						if (defaultCtx == null) {
							RuntimeRefErrors.raise_NULL_CONN_CTX();
						} else {
							OracleContext oracleCtx = ((defaultCtx.getExecutionContext() == null)
									? ExecutionContext.raiseNullExecCtx()
									: defaultCtx.getExecutionContext().getOracleContext());
							String theSqlTS = "insert into reject_record "
									+ "(status, round, presentment, creation_date, reject_date, merchant_number, rec_id, reject_id, reject_type, load_filename, load_file_id) "
									+ "select -1, 1, 0, sysdate, trunc(sysdate), merchant_number, rec_id, 'I001', 'DS',  :1 ,  :2 "
									+ "from discover_settlement ds " + "where ds.merchant_number in "
									+ "(select distinct merchant_number from MES.TRIDENT_PROFILE tp where tp.disc_caid =  :3 ) "
									+ "and ds.batch_date =  :4  and ds.transaction_amount =  :5 ";
							pStatement = oracleCtx.prepareOracleBatchableStatement(defaultCtx,
									"28com.mes.startup.DiscoverCBLoader", theSqlTS);
							// set IN parameters
							pStatement.setString(1, loadFilename);
							pStatement.setLong(2, loadFileId);
							pStatement.setString(3, discCaid);
							pStatement.setDate(4, batchDate);
							pStatement.setDouble(5, amount);
							// execute statement
							oracleCtx.oracleExecuteBatchableUpdate();
						}

						// declare temps
						pStatement = null;

						if (defaultCtx == null) {
							RuntimeRefErrors.raise_NULL_CONN_CTX();
						} else {
							OracleContext oracleCtx = ((defaultCtx.getExecutionContext() == null)
									? ExecutionContext.raiseNullExecCtx()
									: defaultCtx.getExecutionContext().getOracleContext());
							String theSqlTS = "insert into q_data "
									+ "(type, item_type, owner, is_rush, date_created, last_changed, last_user, source, id) "
									+ "select 371, 44, 1, 0, sysdate, sysdate, 'SYSTEM', 'SYSTEM', rec_id "
									+ "from discover_settlement ds " + "where ds.merchant_number in "
									+ "(select distinct merchant_number from MES.TRIDENT_PROFILE tp where tp.disc_caid =  :1 ) "
									+ "and ds.batch_date =  :2  and ds.transaction_amount =  :3";
							pStatement = oracleCtx.prepareOracleBatchableStatement(defaultCtx,
									"29com.mes.startup.DiscoverCBLoader", theSqlTS);
							// set IN parameters
							pStatement.setString(1, discCaid);
							pStatement.setDate(2, batchDate);
							pStatement.setDouble(3, amount);
							// execute statement
							oracleCtx.oracleExecuteBatchableUpdate();
						}

						((Ctx.getExecutionContext() == null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx()
								: Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);

					} else {
						log.error((String) entry.getKey() + " NOT found in discover imap report.");
					}
				} catch (Exception e) {
					try {
						((Ctx.getExecutionContext() == null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx()
								: Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);

					} catch (Exception sqe) {
					}
					log.error(getClass().getName() + "\nException: " + e.toString());
				} finally {
					try {
						it.close();
					} catch (Exception ee) {
					}
					try {
						resultSet.close();
					} catch (Exception ee) {
					}
					setAutoCommit(autoCommit);
				}
			}
			log.info(allRecords.size() + " rejected item/s to be processed in file " + loadFilename);
		} catch (Exception e) {
			log.error(getClass().getName() + "\nException: " + e.toString());
		}
	}

	private Map getReportRecords(String aFileName) {
		String discCAID = null;
		Map rejectMap = new HashMap();
		final Charset encoding = StandardCharsets.UTF_8;

		String batchDatePatternString = "RUN DATE:\\s+";
		String discCAIDPatternString = "MERCHANT #\\s+";
		String tranAmountPatternString = " +(\\d)+ +[0-9]{2}[/][0-9]{2}[/][0-9]{2} +";
		Date runDate = new Date(new java.util.Date().getTime());
		StringBuilder tranDiscCaidDateAmount = new StringBuilder("");
		Pattern regexpBatchDate = Pattern.compile(batchDatePatternString);
		Matcher matcherBatchDate = regexpBatchDate.matcher("");
		Pattern regexpDiscCAID = Pattern.compile(discCAIDPatternString);
		Matcher matcherDiscCAID = regexpDiscCAID.matcher("");
		Pattern regexpTranAmount = Pattern.compile(tranAmountPatternString);
		Matcher matcherTranAmount = regexpTranAmount.matcher("");
		Path path = Paths.get(aFileName);
		BufferedReader reader = null;

		try {
			reader = Files.newBufferedReader(path, encoding);
			LineNumberReader lineReader = new LineNumberReader(reader);
			String line = null;

			while ((line = lineReader.readLine()) != null) {
				matcherBatchDate.reset(line);
				matcherDiscCAID.reset(line);
				matcherTranAmount.reset(line);

				if (matcherBatchDate.find()) {
					runDate = DateTimeFormatter.parseSQLDate(line.substring(125 - 1, 132).trim(), "MM/dd/yy");
				} else if (matcherDiscCAID.find()) {
					discCAID = line.substring(12 - 1, 26).trim();
				} else if (matcherTranAmount.find()) {
					Map detailsMap = new HashMap();
					String tranAmount = line.substring(32 - 1, 46).trim();

					detailsMap.put("discCaid", discCAID);
					detailsMap.put("batchDate", (runDate != null) ? runDate.toString() : null);
					detailsMap.put("amount", tranAmount);

					tranDiscCaidDateAmount.append(discCAID);
					tranDiscCaidDateAmount.append("_");
					tranDiscCaidDateAmount.append((runDate != null) ? runDate.toString() : null);
					tranDiscCaidDateAmount.append("_");
					tranDiscCaidDateAmount.append(tranAmount);
					rejectMap.put(tranDiscCaidDateAmount.toString(), detailsMap);
					tranDiscCaidDateAmount = new StringBuilder("");
				}
			}

		} catch (Exception ex) {
			log.error(getClass().getName() + "\nException: " + ex.toString());
		} finally {
			try {
				reader.close();
			} catch (Exception ee) {
			}
		}
		return rejectMap;
	}
}
