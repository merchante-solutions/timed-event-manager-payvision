/*@lineinfo:filename=MerchantSupplyConfig*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************
  Description:

    MerchantSupplyConfig

    Represents a single merchant configuration in relation to
    supply rules

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.supply;

import java.sql.ResultSet;
import com.mes.constants.mesConstants;

// log4j
//import org.apache.log4j.Logger;

import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class MerchantSupplyConfig extends SQLJConnectionBase
{
  //public static final Logger log = Logger.getLogger(MerchantSupplyConfig.class);

  private String merchantNum;
  private String billingAccount = null;
  private int    bankNum;
  private double taxRate;
  private double markUp;
  private double markUpExtra = 1.0d; //unless changed, never affects markUp
  private double handleFee;
  private double handleRushFee;

  //these attributes will get config'd either through
  //the ACR process or from the completed order
  //so we just leave them empty for now
  private String shipName="";
  private String shipLine1="";
  private String shipLine2="";
  private String shipCity="";
  private String shipState="";
  private String shipZip="";

  //these attributes will remain their default value
  //unless changed by included business rules (initiated
  //by CBT affiliate/club programs.
  private boolean isClub        = false;
  private int    classification = CL_NONE;
  private int    markUpType     = MU_COMPOUND;

  //constants
  public static final double DEFAULT_MARKUP       = 1.3; //30%
  public static final double DEFAULT_HANDLING_FEE = 5.00;

  //markup types
  public static final int MU_CUMULATIVE           = 1;
  public static final int MU_COMPOUND             = 2;

  //merchant classifications
  public static final int CL_NONE                 = 0;
  public static final int CL_CBT_MERCHANT         = 1;
  public static final int CL_CBT_AFFILIATE        = 2;
  public static final int CL_CBT_AFFILIATE_PILOT  = 3;


  public MerchantSupplyConfig()
  {
    merchantNum="";
    taxRate=0.0;
    markUp=DEFAULT_MARKUP;
    handleFee=DEFAULT_HANDLING_FEE;
    handleRushFee=DEFAULT_HANDLING_FEE;
  }

  public MerchantSupplyConfig(String merchantNum)
  {
    this.merchantNum=merchantNum;
    findBankNumber();
    init();
  }

  public MerchantSupplyConfig(String merchantNum, int bankNum)
  {
    this.merchantNum=merchantNum;
    this.bankNum = bankNum;
    init();
  }

  protected void init()
  {
    //for all supply calculations
    configMultipliers();

    //for any additional hierarchy-specific relationships
    configRelationship();
  }

  private void configMultipliers()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    try
    {
      connect();
      
      //allows for layered assignment
      //in supply_config table
      boolean markupFound = false;
      boolean handleFound = false;
      boolean handleRushFound = false;

      //First, look for the merchant number in supply_config
      //it will be found in association_node (a bit misleading there...)
      /*@lineinfo:generated-code*//*@lineinfo:123^7*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT
//            sc.handling_standard,
//            sc.handling_rush,
//            sc.markup_multiplier
//          FROM
//            supply_config sc
//          WHERE
//            sc.association_node = :merchantNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n          sc.handling_standard,\n          sc.handling_rush,\n          sc.markup_multiplier\n        FROM\n          supply_config sc\n        WHERE\n          sc.association_node =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.supply.MerchantSupplyConfig",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.supply.MerchantSupplyConfig",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:133^7*/

      rs = it.getResultSet();
      if(rs.next())
      {
        //check for markup
        if(null != rs.getString("markup_multiplier"))
        {
          markUp=rs.getDouble("markup_multiplier");
          markupFound = true;
        }

        //check for handling fee
        if(null != rs.getString("handling_standard"))
        {
          handleFee=rs.getDouble("handling_standard");
          handleFound = true;
        }

        //check for rush handling fee
        if(null != rs.getString("handling_rush"))
        {
          handleRushFee=rs.getDouble("handling_rush");
          handleRushFound = true;
        }
      }

      rs.close();
      it.close();

      if(!markupFound || !handleFound || !handleRushFound)
      {
        //we haven't found some or all the information -
        //so look for an association-level configuration
        /*@lineinfo:generated-code*//*@lineinfo:167^9*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT
//              th.ancestor,
//              sc.handling_standard,
//              sc.handling_rush,
//              sc.markup_multiplier
//            FROM
//              t_hierarchy th,
//              mif m,
//              supply_config sc
//            WHERE
//              m.merchant_number = :merchantNum
//            AND
//              m.association_node = th.descendent
//            AND
//              th.ancestor = sc.association_node
//            ORDER BY
//              th.relation asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n            th.ancestor,\n            sc.handling_standard,\n            sc.handling_rush,\n            sc.markup_multiplier\n          FROM\n            t_hierarchy th,\n            mif m,\n            supply_config sc\n          WHERE\n            m.merchant_number =  :1 \n          AND\n            m.association_node = th.descendent\n          AND\n            th.ancestor = sc.association_node\n          ORDER BY\n            th.relation asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.supply.MerchantSupplyConfig",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.supply.MerchantSupplyConfig",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:186^9*/

        rs = it.getResultSet();

        while (rs.next())
        {
          //check for markup
          if(null != rs.getString("markup_multiplier") && !markupFound)
          {
            markUp=rs.getDouble("markup_multiplier");
            markupFound = true;
          }

          //check for handling fee
          if(null != rs.getString("handling_standard") && !handleFound)
          {
            handleFee=rs.getDouble("handling_standard");
            handleFound = true;
          }

          //check for rush handling fee
          if(null != rs.getString("handling_rush") && !handleRushFound)
          {
            handleRushFee=rs.getDouble("handling_rush");
            handleRushFound = true;
          }

          if(markupFound && handleFound && handleRushFound)
          {
            break;
          }
        }//while
      }

      //check all values, revert to default if not found
      if(!markupFound)
      {
        markUp=DEFAULT_MARKUP;
      }
      if(!handleFound)
      {
        handleFee=DEFAULT_HANDLING_FEE;
      }
      if(!handleRushFound)
      {
        handleRushFee=DEFAULT_HANDLING_FEE;
      }
    }
    catch (Exception e)
    {
      //log.debug("MerchantConfig: init() EXCEPTION - resetting to defaults...");
      markUp=DEFAULT_MARKUP;
      handleFee=DEFAULT_HANDLING_FEE;
      handleRushFee=DEFAULT_HANDLING_FEE;
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void configRelationship()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;

    try
    {
      connect();
      
      //if this is a CBT bank, set up relationships
      if( bankNum==mesConstants.BANK_ID_CBT )
      {
        //look for CBT affiliate and club status
        /*@lineinfo:generated-code*//*@lineinfo:262^9*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT
//              th.ancestor,
//              cac.markup_extra      as markup_extra,
//              cac.association_type  as classification,
//              nvl(cac.billing_account, :merchantNum)
//                                    as billing_account,
//              nvl(scc.is_Club,'n')  as isClub
//            FROM
//              t_hierarchy th,
//              mif m,
//              cbt_account_class cac,
//              supply_club_config scc
//            WHERE
//              m.merchant_number = :merchantNum
//            AND
//              m.association_node = th.descendent
//            AND
//              th.ancestor = cac.association_node
//            AND
//              m.merchant_number = scc.merchant_number(+)
//            ORDER BY
//              th.relation asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n            th.ancestor,\n            cac.markup_extra      as markup_extra,\n            cac.association_type  as classification,\n            nvl(cac.billing_account,  :1 )\n                                  as billing_account,\n            nvl(scc.is_Club,'n')  as isClub\n          FROM\n            t_hierarchy th,\n            mif m,\n            cbt_account_class cac,\n            supply_club_config scc\n          WHERE\n            m.merchant_number =  :2 \n          AND\n            m.association_node = th.descendent\n          AND\n            th.ancestor = cac.association_node\n          AND\n            m.merchant_number = scc.merchant_number(+)\n          ORDER BY\n            th.relation asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.supply.MerchantSupplyConfig",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNum);
   __sJT_st.setString(2,merchantNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.supply.MerchantSupplyConfig",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:286^9*/

        rs = it.getResultSet();

        if(rs.next())
        {
          classification = rs.getInt("classification");
          markUpExtra = rs.getDouble("markup_extra");
          isClub = rs.getString("isClub").equals("n")?false:true;
          if(isClub)
          {
            billingAccount = rs.getString("billing_account");
          }
          else
          {
            billingAccount = merchantNum;
          }
        }

        rs.close();
        it.close();

        switch(classification)
        {

          case(CL_CBT_MERCHANT):
          case(CL_CBT_AFFILIATE_PILOT):

            //reset markup if club
            if(isClub)
            {
              markUpExtra = 1.0;
            }
            break;

          default:
            //nothing to do
            break;
        }
      }
      else // if( bankNum==mesConstants.BANK_ID_CBT )
      {
        // all non-CBT merchants just look at SUPPLY_CLUB_CONFIG table to 
        // determine if they are a club merchant
        /*@lineinfo:generated-code*//*@lineinfo:330^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  lower(scc.is_club)  is_club
//            from    supply_club_config scc
//            where   scc.merchant_number = :merchantNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  lower(scc.is_club)  is_club\n          from    supply_club_config scc\n          where   scc.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.supply.MerchantSupplyConfig",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.supply.MerchantSupplyConfig",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:335^9*/
        
        rs = it.getResultSet();
        
        if(rs.next() && rs.getString("is_club").equals("y"))
        {
          isClub = true;
        }
        else
        {
          isClub = false;
        }
        
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("configRelationship(" + merchantNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void findBankNumber()
  {
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:370^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT
//            nvl(bank_number,-1)
//          
//          FROM
//            mif
//          WHERE
//            merchant_number = :merchantNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT\n          nvl(bank_number,-1)\n         \n        FROM\n          mif\n        WHERE\n          merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.supply.MerchantSupplyConfig",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchantNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNum = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:380^7*/
    }
    catch (Exception e)
    {
      //log.debug("findBankNumber():  EXCEPTION: "+e.getMessage());
    }
    finally
    {
      cleanUp();
    }
  }

  public void setTaxRate()
  {
    try
    {
      connect();
      
      if( getShipZip() != null && ! getShipZip().equals("") )
      {
        //will finish once tax tables are in
        double tempRate=0.00;

        /*@lineinfo:generated-code*//*@lineinfo:403^9*/

//  ************************************************************
//  #sql [Ctx] { SELECT
//              sales_tax_rate(substr(:getShipZip(), 1, 5), :getShipCity())
//            
//            FROM
//              dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_464 = getShipZip();
 String __sJT_465 = getShipCity();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT\n            sales_tax_rate(substr( :1 , 1, 5),  :2 )\n           \n          FROM\n            dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.supply.MerchantSupplyConfig",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_464);
   __sJT_st.setString(2,__sJT_465);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   tempRate = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:411^9*/

        setTaxRate(tempRate);
      }
    }
    catch (Exception e)
    {
      //log.debug("setTaxRate():  EXCEPTION: "+e.getMessage());
    }
    finally
    {
      cleanUp();
    }
  }

//mutators
  public void setMerchantNum (String string){
    this.merchantNum = string;
  }

  public void setTaxRate (double amount){
    this.taxRate = amount;
  }

  public void setMarkUp (double amount){
    this.markUp = amount;
  }

  public void setMarkUpExtra (double amount){
    this.markUpExtra = amount;
  }

  public void setMarkUpType (int type)
  {
    switch(type)
    {
      case MU_CUMULATIVE:
      case MU_COMPOUND:
        markUpType = type;
        break;

      default:
        markUpType = MU_COMPOUND;
    }
  }

  public void setHandleFee (double amount){
    this.handleFee = amount;
  }

  public void setShipName (String string){
    this.shipName = string;
  }

  public void setShipLine1 (String string){
    this.shipLine1 = string;
  }

  public void setShipLine2 (String string){
    this.shipLine2 = string;
  }

  public void setShipCity (String string){
    this.shipCity = string;
  }

  public void setShipState (String string){
    this.shipState = string;
  }

  public void setShipZip (String string){
    this.shipZip = string;
  }

  public void clearAddress()
  {
    setShipName("");
    setShipLine1("");
    setShipLine2("");
    setShipCity("");
    setShipState("");
    setShipZip("");
  }

//accessors
  public String getMerchantNum ()
  {
    return merchantNum;
  }

  public double getTaxRate ()
  {
    return taxRate;
  }

  public int getBankNumber()
  {
    return bankNum;
  }

  public boolean isClub()
  {
    return isClub;
  }

  public int getClassification()
  {
    return classification;
  }

  public String getBillingAccount()
  {
    if(billingAccount==null)
    {
      return merchantNum;
    }
    else
    {
      return billingAccount;
    }
  }

  public double getMarkUp ()
  {

    switch(getMarkUpType())
    {

      case MU_CUMULATIVE:
        //add the corresponding markUp fractionals
        return 1 + (markUp-1) + (markUpExtra-1);

        //multiply the markup effect
      case MU_COMPOUND:
        return markUp*markUpExtra;

        //just return the base
      default:
        return markUp;

    }
  }

  public int getMarkUpType()
  {
    return markUpType;
  }

  public double getMarkUpExtra ()
  {
    return markUpExtra;
  }

  public double getHandleFee (){
    return handleFee;
  }

  public String getShipName (){
    return shipName;
  }

  public String getShipLine1 (){
    return shipLine1;
  }

  public String getShipLine2 (){
    return shipLine2;
  }

  public String getShipCity (){
    return shipCity;
  }

  public String getShipState (){
    return shipState;
  }

  public String getShipZip (){
    return shipZip;
  }

  public String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("MerchantSupplyConfig details--------------->\n");
    sb.append("merchantNum = ").append(merchantNum).append("\n");
    sb.append("billingAccount = ").append(billingAccount).append("\n");
    sb.append("bankNum = ").append(bankNum).append("\n");
    sb.append("taxRate = ").append(taxRate).append("\n");
    sb.append("markUp = ").append(markUp).append("\n");
    sb.append("markUpExtra = ").append(markUpExtra).append("\n");
    sb.append("handleFee = ").append(handleFee).append("\n");
    sb.append("handleRushFee = ").append(handleRushFee).append("\n");
    sb.append("shipName = ").append(shipName).append("\n");
    sb.append("shipLine1 = ").append(shipLine1).append("\n");
    sb.append("shipLine2 = ").append(shipLine2).append("\n");
    sb.append("shipCity = ").append(shipCity).append("\n");
    sb.append("shipState = ").append(shipState).append("\n");
    sb.append("shipZip = ").append(shipZip).append("\n");
    return sb.toString();
  }
}/*@lineinfo:generated-code*/