/*@lineinfo:filename=GCFOrderFile*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/flatfile/GCFOrderFile.sqlj $

  Description:

    GCFOrderFile

    Concrete representation of OrderFile
    currently used by SupplyOrderLoad

 Last Modified By   : $Author: vbannikov $
 Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
 Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.flatfile;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;
// log4j
import org.apache.log4j.Logger;
import com.jscape.inet.ftps.Ftps;
import com.mes.config.MesDefaults;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.net.FTPClient;
import com.mes.net.FTPTransferType;
import com.mes.supply.MerchantSupplyConfig;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;

public class GCFOrderFile extends FlatFileRecord implements OrderFile
{
  // create class log category
  static Logger log = Logger.getLogger(GCFOrderFile.class);

  private int procSeq = 0;
  private StringBuffer gcfFile = new StringBuffer("");
  private List orders = new ArrayList();
  private String delimiter;
  private List errorList = new ArrayList();
  private String identifier;

  private String UPLOAD_DIRECTORY = "upload";
  private String ARC_DIR = "arc/daily";
  private int MAX_FTP_ATTEMPTS = 10;
  private int TRAILER_LENGTH = 5; //defined by client

    public static String  FTP_ARC_HOST            = "";
    public static int     FTP_ARC_PORT            = 0;
    public static String  FTP_ARC_USER            = "";
    public static String  FTP_ARC_PASSWORD        = "";
    private long[] internalUsers                        = new long[]{};

  public GCFOrderFile()
  {
    super();
    delimiter="|";
    identifier="gcf";
        try {
          FTP_ARC_HOST            = MesDefaults.getString(MesDefaults.GCF_ORDER_FILE_FTP_ARC_HOST);
          FTP_ARC_PORT            = MesDefaults.getInt(MesDefaults.GCF_ORDER_FILE_FTP_ARC_PORT);
          FTP_ARC_USER            = MesDefaults.getString(MesDefaults.GCF_ORDER_FILE_FTP_ARC_USER);
          FTP_ARC_PASSWORD        = MesDefaults.getString(MesDefaults.GCF_ORDER_FILE_FTP_ARC_PASSWORD);
          
          String internalUsersStr = MesDefaults.getString(MesDefaults.GCF_ORDER_FILE_INTERNAL_USERS);
          String[] internalUsersList = internalUsersStr.split(",");
          internalUsers           = new long[internalUsersList.length];
          for (int i = 0; i < internalUsersList.length; i++) {
            internalUsers[i] = Long.parseLong(internalUsersList[i].trim());
          }
        } catch (Exception e) {
          logEntry("", e.toString());
        }
        
  }

  /**
   * ORDERFILE interface methods
   * Allows for abstraction at the OrderLoad level
   **/

  public boolean hasErrors(){
    return errorList.size()>0;
  }

  public String getErrors(){

    StringBuffer sb = new StringBuffer();
    if(hasErrors()){
      for(int i=0;i<errorList.size();i++){
        sb.append((String)errorList.get(i)).append("\n");
      }
    }else{
      sb.append("");
    }
    return sb.toString();
  }

  public String toString(){
    return gcfFile.toString();
  }

  public String getIdentifier(){

    StringBuffer sb = new StringBuffer(identifier);
    sb.append("_");
    sb.append(DateTimeFormatter.getCurDateString("MMddyya"));
    return sb.toString();

  }

  private String getArchiveIdentifier()
  {
    String dateString = DateTimeFormatter.getCurDateString("MMddyya");
    StringBuffer sb = new StringBuffer(identifier);
    sb.append("_");
    sb.append(dateString);
    sb.append("_");
    if( dateString.indexOf("AM") == -1){
      sb.append("002");
    }else{
      sb.append("001");
    }
    return sb.toString();
  }

  /**
   * build() - creates the active supply order.
   * NOTE: there is no mechanism to determine whether the order is a "rush"
   * or not, simply because that will be determined via the processing at the
   * supplier's end... all orders marked rush will be treated as such, but the
   * time of reception of this file will determine the speed at which the orders
   * are completed.
   **/

  public boolean build()
  {
    //log.debug("START buildGCFFile()");
    ResultSet rs = null;
    ResultSetIterator it = null;

    try
    {
      connect();

      int count=0;
      /*@lineinfo:generated-code*//*@lineinfo:170^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT count(supply_order_id)
//          
//          FROM supply_order
//          WHERE process_sequence is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT count(supply_order_id)\n         \n        FROM supply_order\n        WHERE process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.flatfile.GCFOrderFile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:176^7*/

      if(count==0){
        //errorList.add("No outstanding orders found.");
        return false;
      }

      //get next process_seq for supply orders
      /*@lineinfo:generated-code*//*@lineinfo:184^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT supply_order_process_sequence.nextval
//          
//          FROM dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT supply_order_process_sequence.nextval\n         \n        FROM dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.flatfile.GCFOrderFile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procSeq = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:189^7*/

      //insert process_seq into applicable orders
      /*@lineinfo:generated-code*//*@lineinfo:192^7*/

//  ************************************************************
//  #sql [Ctx] { UPDATE supply_order
//          SET process_sequence=:procSeq
//          WHERE process_sequence is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "UPDATE supply_order\n        SET process_sequence= :1 \n        WHERE process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.flatfile.GCFOrderFile",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSeq);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:197^7*/

      // retrieve order ids and place into list
      /*@lineinfo:generated-code*//*@lineinfo:200^7*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT  supply_order_id
//          FROM    supply_order
//          WHERE   process_sequence = :procSeq
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT  supply_order_id\n        FROM    supply_order\n        WHERE   process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.flatfile.GCFOrderFile",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSeq);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.flatfile.GCFOrderFile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:205^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        orders.add(rs.getLong("supply_order_id"));
      }

      rs.close();
      it.close();


      setDefType(MesFlatFiles.DEF_TYPE_GCF_SUPPLY_ORDER);

      for(int i=0;i<orders.size();i++){

        resetAllFields();
        buildOrderRecord(i);
        gcfFile.append(trimFields(spew(delimiter)));
        gcfFile.append(delimiter);
        gcfFile.append("\n");

      }

      //append requested trailer tag (as of 5/19/04)
      gcfFile.append(generateTrailer(orders.size()));


    }
    catch(Exception e)
    {
      errorList.add("buildGCFFile() EXCEPTION: "+e.getMessage());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
      //log.debug("END buildGCFFile()");
    }

    return !hasErrors();

  }

  /**
   * rebuild() - allows a recreation of an order based on the process sequence.
   * for order reg only, and outside the actual system (locally controlled)
   **/

  public boolean rebuild(long processSeq){

    //log.debug("START rebuild()");
    ResultSet rs = null;
    ResultSetIterator it = null;

    try{

      connect();

      int count=0;
      /*@lineinfo:generated-code*//*@lineinfo:267^7*/

//  ************************************************************
//  #sql [Ctx] { SELECT count(supply_order_id)
//          
//          FROM supply_order
//          WHERE process_sequence = :processSeq
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "SELECT count(supply_order_id)\n         \n        FROM supply_order\n        WHERE process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.flatfile.GCFOrderFile",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,processSeq);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:273^7*/

      if(count==0){
        //errorList.add("No outstanding orders found.");
        return false;
      }

      // retrieve order ids and place into list
      /*@lineinfo:generated-code*//*@lineinfo:281^7*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT  supply_order_id
//          FROM    supply_order
//          WHERE   process_sequence = :processSeq
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT  supply_order_id\n        FROM    supply_order\n        WHERE   process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.flatfile.GCFOrderFile",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,processSeq);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.flatfile.GCFOrderFile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:286^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        orders.add(rs.getLong("supply_order_id"));
      }

      rs.close();
      it.close();


      setDefType(MesFlatFiles.DEF_TYPE_GCF_SUPPLY_ORDER);

      for(int i=0;i<orders.size();i++){

        resetAllFields();
        buildOrderRecord(i);
        gcfFile.append(trimFields(spew(delimiter)));
        gcfFile.append(delimiter);
        gcfFile.append("\n");

      }

      //append requested trailer tag (as of 5/19/04)
      gcfFile.append(generateTrailer(orders.size()));

    }
    catch(Exception e)
    {
      errorList.add("buildGCFFile() EXCEPTION: "+e.getMessage());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
      //log.debug("END rebuild()");
    }

    return !hasErrors();

  }


  /**
   * transmit() - sends the order file.
   * In this case, via SFTP
   **/
  public boolean transmit()
  {
    //log.debug("START transmitFile()");

    boolean result  = false;
    int counter     = 1;
    Ftps ftps       = null;

    while(result==false && counter<=MAX_FTP_ATTEMPTS)
    {
      try
      {
        String ftpsHost      = MesDefaults.getString(MesDefaults.DK_GCF_FTPS_IP_ADDRESS);
        int    ftpsPort      = MesDefaults.getInt(MesDefaults.DK_GCF_FTPS_PORT);
        String ftpsUser      = MesDefaults.getString(MesDefaults.DK_GCF_USER);
        String ftpsPassword  = MesDefaults.getString(MesDefaults.DK_GCF_PASSWORD);

        log.debug("ftpsHost = "+ftpsHost);
        log.debug("ftpsPort = "+ftpsPort);
        log.debug("ftpsUser = "+ftpsUser);
        log.debug("ftpsPassword = "+ftpsPassword);

        ftps     = new Ftps(ftpsHost,ftpsUser,ftpsPassword,ftpsPort);

        //changes here to see if we can resolve PASV connection timeout
        //(tested) nope, need to leave passive
        //ftps.setPassive(false);

        ftps.connect();
        ftps.setDir(UPLOAD_DIRECTORY);
        ftps.setAscii();
        ftps.upload(this.toString().getBytes(), getIdentifier() + ".dat");

        result = true;
      }
      catch (Exception e1)
      {

       //drop FTPS
       try
       {
         ftps.disconnect();
       }
        catch(Exception e)
       {
         //do nothing
       }

       //while working kinks out for ftps, default to ftp
       if(counter==MAX_FTP_ATTEMPTS)
       {
         logEntry("transmit()::FTPS Connection", e1.toString());
         //result = _transmit();
         resetRecords();
         errorList.add("GCFOrderFile.transmit():: Order Count--"+orders.size()+"-- EXCEPTION: "+e1.getMessage());
       }
       else
       {
         //might be a network thing...sleep it off.
         try
         {
           Thread.sleep(10000L);
         }
         catch (InterruptedException ie)
         {
         }
       }

       counter++;
      }
      finally
      {
        try{ ftps.disconnect(); } catch(Exception e) {}
      }
    }
    //log.debug("END transmitFile()");
    return result;
  }

  /**
   * OLD METHOD of TRANSMIT
   * _transmit() - sends the order file.
   * In this case, via FTP
   **/
  public boolean _transmit()
  {
    //log.debug("START transmitFile()");

    boolean result = false;
    int counter=1;
    FTPClient ftp=null;

    while(result==false && counter<=MAX_FTP_ATTEMPTS)
    {
      try
      {

        String ftpHost      = MesDefaults.getString(MesDefaults.DK_GCF_IP_ADDRESS);
        String ftpUser      = MesDefaults.getString(MesDefaults.DK_GCF_USER);
        String ftpPassword  = MesDefaults.getString(MesDefaults.DK_GCF_PASSWORD);

        ftp = new FTPClient(ftpHost, 21);
        ftp.login(ftpUser, ftpPassword);
        ftp.setType(FTPTransferType.ASCII);
        ftp.chdir(UPLOAD_DIRECTORY);
        ftp.put(this.toString().getBytes(), getIdentifier() + ".dat");

        result = true;
      }
      catch (Exception e1)
      {
       if(counter==MAX_FTP_ATTEMPTS)
       {
         resetRecords();
         errorList.add("GCFOrderFile._transmit() EXCEPTION: "+e1.getMessage());
       }
       counter++;

       //drop FTP
       try
       {
         ftp.quit();
       }
        catch(Exception e)
       {
         //do nothing
       }

       //might be a network thing...sleep it off.
       try
       {
         Thread.sleep(10000L);
       }
       catch (InterruptedException ie)
       {
       }

      }
    }
    //log.debug("END transmitFile()");
    return result;
  }

  public void bill()
  {
    ResultSet         rs    = null;
    ResultSetIterator rsItr = null;

    try
    {
      connect();

      if(orders.size() > 0)
      {
        long orderId      =0L;

        //4 STAGES for each order
        for(int i=0;i<orders.size();i++)
        {
          orderId = ((Long)orders.get(i)).longValue();
          double amount     =0.0d;
          long merchantNum  =0L;

          //STAGE 1 *******************************************
          //build order total and merchant number
          /*@lineinfo:generated-code*//*@lineinfo:501^11*/

//  ************************************************************
//  #sql [Ctx] rsItr = { SELECT
//                merch_num,
//                total_cost
//              FROM
//                supply_order
//              WHERE
//                supply_order_id = :orderId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n              merch_num,\n              total_cost\n            FROM\n              supply_order\n            WHERE\n              supply_order_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.flatfile.GCFOrderFile",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orderId);
   // execute query
   rsItr = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.flatfile.GCFOrderFile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:510^11*/

          rs = rsItr.getResultSet();

          if(rs.next())
          {
            amount = rs.getDouble("total_cost");
            merchantNum = rs.getLong("merch_num");
          }

          rsItr.close();
          rs.close();

          //STAGE 2 *******************************************
          //establish billing merchant and billing schedule
          //(only applies to CBT right now)

          boolean autobillMerchant  = true;
          String billStatus         = "N";

          MerchantSupplyConfig merch =
            new MerchantSupplyConfig(""+merchantNum);

          //must check merchant number for CBT relationship
          //bank_number(3858 = CBT)? a club member? not an affiliate?
          if ( merch.getBankNumber() == mesConstants.BANK_ID_CBT &&
               merch.isClub() &&
               merch.getClassification() != MerchantSupplyConfig.CL_CBT_AFFILIATE )
          {
              //in this case, manual billing will be used
              //to bill the appropriate accounts - otherwise
              //billing is done automatically
              autobillMerchant = false;
              billStatus = "M";
          }
          else if( merch.getBankNumber() != mesConstants.BANK_ID_CBT &&
                   merch.isClub() )
          {
            autobillMerchant = false;
            billStatus = "M";
          }

          //STAGE 3 *******************************************
          //only insert if amount is greater than 0
          if( autobillMerchant && amount > 0 )
          {
            //insert into ACH billing process
            /*@lineinfo:generated-code*//*@lineinfo:557^13*/

//  ************************************************************
//  #sql [Ctx] { insert into bankserv_ach_detail
//                (
//                  merchant_number,
//                  amount,
//                  entry_description,
//                  credit_debit_ind,
//                  created_by,
//                  source_type,
//                  source_id
//                )
//                values
//                (
//                  : merch.getBillingAccount(),
//                  :amount,
//                  'MER SUPPLY',
//                  'D',
//                  'SYSTEM',
//                  :mesConstants.BANKSERV_SOURCE_SUPPLY,
//                  :orderId
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_466 =  merch.getBillingAccount();
   String theSqlTS = "insert into bankserv_ach_detail\n              (\n                merchant_number,\n                amount,\n                entry_description,\n                credit_debit_ind,\n                created_by,\n                source_type,\n                source_id\n              )\n              values\n              (\n                 :1 ,\n                 :2 ,\n                'MER SUPPLY',\n                'D',\n                'SYSTEM',\n                 :3 ,\n                 :4 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.flatfile.GCFOrderFile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_466);
   __sJT_st.setDouble(2,amount);
   __sJT_st.setInt(3,mesConstants.BANKSERV_SOURCE_SUPPLY);
   __sJT_st.setLong(4,orderId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:579^13*/

            billStatus = "Y";

          }//end if amount > 0

          //STAGE 4 *******************************************
          //update bill status in order
          /*@lineinfo:generated-code*//*@lineinfo:587^11*/

//  ************************************************************
//  #sql [Ctx] { UPDATE
//                supply_order
//              SET
//                bill_status = :billStatus
//              WHERE
//                supply_order_id = :orderId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "UPDATE\n              supply_order\n            SET\n              bill_status =  :1 \n            WHERE\n              supply_order_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.flatfile.GCFOrderFile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,billStatus);
   __sJT_st.setLong(2,orderId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:595^11*/

        }//end for
      }
      else
      {
        errorList.add("GCFOrderFile.bill() Error: Orders shipping... billing failed");
      }
    }
    catch(Exception e)
    {
      errorList.add("GCFOrderFile.billing() EXCEPTION: "+e.getMessage());
      logEntry("bill()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { rsItr.close(); } catch(Exception e) {}
      cleanUp();
    }
  }//bill()

  /**
   * archive() - archives the order file.
   * both saves to the DB, and FTPs a compressed
   * version of itself onto the archive server
   **/
  public void archive()
  {
    //log.debug("START insertArchiveFilename()");
    //use existing order list, add new archive filename
    //where procSequence is current
    String arcName = getArchiveIdentifier();

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:633^7*/

//  ************************************************************
//  #sql [Ctx] { UPDATE
//            supply_order
//          SET
//            load_filename=(:arcName)
//            ,status=9
//          WHERE
//            process_sequence = :procSeq
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "UPDATE\n          supply_order\n        SET\n          load_filename=( :1 )\n          ,status=9\n        WHERE\n          process_sequence =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.flatfile.GCFOrderFile",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,arcName);
   __sJT_st.setInt(2,procSeq);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:642^7*/

      ftpArchive(arcName);

    }
    catch(Exception e)
    {
      //log.debug("insertArchiveFilename() EXCEPTION: "+e.getMessage());
      errorList.add("buildGCFFile() EXCEPTION: "+e.getMessage());
    }
    finally
    {
      cleanUp();
    }
    //log.debug("END insertArchiveFilename()");
  }

  /**
   * GCFOrderFile private methods
   **/

  private void ftpArchive(String arcName)
  {
    //FTP to archive server
    FTPClient ftp=null;
    byte[] fileBytes = gcfFile.toString().getBytes();
    try{

      ftp = new FTPClient(FTP_ARC_HOST, FTP_ARC_PORT);
      ftp.login(FTP_ARC_USER, FTP_ARC_PASSWORD);
      ftp.setType(FTPTransferType.BINARY);
      ftp.chdir(ARC_DIR);

      // send the zip file to the archive host
      ftp.putZipped(fileBytes, arcName+".zip", arcName+".dat");

      // send flag file to archive host so that it will be encrypted
      String flag = "flagged";
      ftp.put(flag.getBytes(), arcName+".flg");

    }
    catch (Exception e1)
    {
      //log.debug("Unable to FTP archive file: "+ arcName);
      errorList.add("Unable to FTP archive file: "+ arcName);
    }
    finally
    {
      //drop FTP
      try
      {
       ftp.quit();
      }
      catch(Exception e)
      {
       //do nothing
      }
    }
  }

  /**
   * buildOrderRecord
   * Creates the individual order record as defined by the
   * reference to the orders List
   *
   * @param index int reference to determine orderid from List
   *
   **/

  private void buildOrderRecord(int index)
  {
    //log.debug("START buildOrderRecord()");

    long orderId = ((Long)orders.get(index)).longValue();
    buildOrderDetails(orderId);
    buildOrderItemDetails(orderId);

    //log.debug("END buildOrderRecord()");
  }


  /**
   * buildOrderDetails
   * Generates general order details from orderid
   *
   * @param orderid long
   *
   **/
  private void buildOrderDetails(long orderId){


    //log.debug("START buildOrderDetails()");
    ResultSet rs = null;
    ResultSetIterator it = null;

    try
    {
      connect();

      //ONLY get order general info - break out item info
      /*@lineinfo:generated-code*//*@lineinfo:742^7*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT
//            so.supply_order_id as customer_PO_number
//            ,so.ship_address_name merch_name
//            ,so.ship_address_line1
//            ,so.ship_address_line2
//            ,so.ship_address_city
//            ,so.ship_address_state
//            ,so.ship_address_zip
//            ,so.requestor_name
//            ,so.requestor_phone
//            ,(case so.is_rush
//              when 'y' then 'UPS-NE'
//              else 'UPS' end) is_rush
//            ,so.merch_num
//          FROM
//            supply_order so
//          WHERE
//            so.supply_order_id = :orderId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n          so.supply_order_id as customer_PO_number\n          ,so.ship_address_name merch_name\n          ,so.ship_address_line1\n          ,so.ship_address_line2\n          ,so.ship_address_city\n          ,so.ship_address_state\n          ,so.ship_address_zip\n          ,so.requestor_name\n          ,so.requestor_phone\n          ,(case so.is_rush\n            when 'y' then 'UPS-NE'\n            else 'UPS' end) is_rush\n          ,so.merch_num\n        FROM\n          supply_order so\n        WHERE\n          so.supply_order_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.flatfile.GCFOrderFile",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orderId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.flatfile.GCFOrderFile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:762^7*/

      rs = it.getResultSet();

      long merchNum = 0L;

      if(rs.next()){
        merchNum = rs.getLong("merch_num");
        setAllFieldData(rs);
      }
      /*DEPR. as of 8/18/04
      //check merchNum to see if this is an internal file order
      //if so, switch gcf_assigned_key to MeS key name; otherwise
      //it stays at the default "MERCH.SOL"
      if(isInternalOrder(merchNum)){
        FlatFileRecordField field = findFieldByName("gcf_assigned_key");
        if(null!=field){
          log.debug("field data = "+field.getRawData());
          field.setData("ME.SOLUTIONS");
        }
      }
      */
    }catch(Exception e){
      //log.debug("EXCEPTION buildOrderRecord(): "+e.getMessage());
      errorList.add("buildOrderRecord() EXCEPTION: "+e.getMessage());
    }finally{
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    //log.debug("END buildOrderDetails()");

  }

  /**
   * buildOrderItemDetails
   * Completes order generation by filling in order item details
   *
   * @param orderid long
   *
   **/
  private void buildOrderItemDetails(long orderId){

    //log.debug("START buildOrderItemDetails()");
    ResultSet rs = null;
    ResultSetIterator it = null;

    try
    {
      connect();

      //now get specific order item info for that order
      /*@lineinfo:generated-code*//*@lineinfo:814^7*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT
//            soi.quantity
//            ,sut.gcf_name
//            ,si.partnum
//          FROM
//            supply_order_item soi
//            ,supply_item si
//            ,supply_unit_type sut
//          WHERE
//            soi.supply_order_id = :orderId
//          AND
//            soi.supply_item_id=si.supply_item_id
//          AND
//            si.unit_type_id = sut.unit_type_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT\n          soi.quantity\n          ,sut.gcf_name\n          ,si.partnum\n        FROM\n          supply_order_item soi\n          ,supply_item si\n          ,supply_unit_type sut\n        WHERE\n          soi.supply_order_id =  :1 \n        AND\n          soi.supply_item_id=si.supply_item_id\n        AND\n          si.unit_type_id = sut.unit_type_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.flatfile.GCFOrderFile",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orderId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.flatfile.GCFOrderFile",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:830^7*/

      rs = it.getResultSet();

      int idx = 1;

      while(rs.next())
      {

        ResultSetMetaData   md = rs.getMetaData();

        for(int i=1; i <= md.getColumnCount(); ++i)
        {
          String columnName = md.getColumnName(i).toLowerCase();
          //log.debug("SEARCHING: columnName ="+columnName);
          StringBuffer fieldName = new StringBuffer(columnName);
          fieldName.append("_").append(Integer.toString(idx));

          switch(findFieldByName(fieldName.toString()).getType())
          {
            case FlatFileRecordField.FIELD_TYPE_NUMERIC:
              setFieldData(fieldName.toString(), rs.getDouble(columnName));
              break;

            case FlatFileRecordField.FIELD_TYPE_DATE:
              setFieldData(fieldName.toString(), new java.sql.Date(rs.getTimestamp(columnName).getTime()));
              break;

            case FlatFileRecordField.FIELD_TYPE_ALPHA:
            default:
              setFieldData(fieldName.toString(), rs.getString(columnName));
              break;
          }
        }

        ++idx;
      }

    }catch(Exception e){
      //log.debug("EXCEPTION buildOrderItemDetails(): "+e.getMessage());
      errorList.add("buildOrderItemDetails() EXCEPTION: "+e.getMessage());
    }finally{
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  /**
   * trimFields
   * method to remove the 'suffix' unwanted delimiters/fields
   * that are a by-product of the FlatFile process
   *
   * @param dirtyString String that needs to be cleaned of trailing delimiters
   *
   **/
  private String trimFields(String dirtyString){

    if (null==dirtyString||dirtyString.length()==0)
    {
      return dirtyString;
    }

    String cleanString="";

    char c;
    for(int i=dirtyString.length()-1;i>0;i--)
    {
      c = dirtyString.charAt(i);
      if(c!=delimiter.charAt(0))
      {
        cleanString = dirtyString.substring(0,i+1);
        break;
      }
    }
    return cleanString;
  }


  /**
   * resetRecords
   * method to reset DB records in event of exceptions - allows for the
   * process to be retriggered.
   *
   **/
  private void resetRecords()
  {
    try
    {
      connect();

      //insert null back into process_sequence
      /*@lineinfo:generated-code*//*@lineinfo:922^7*/

//  ************************************************************
//  #sql [Ctx] { UPDATE supply_order
//          SET process_sequence=null
//          WHERE process_sequence = :procSeq
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "UPDATE supply_order\n        SET process_sequence=null\n        WHERE process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.flatfile.GCFOrderFile",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSeq);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:927^7*/
    }
    catch(Exception e)
    {
      errorList.add("resetRecords() EXCEPTION: "+e.getMessage());
    }
    finally
    {
      cleanUp();
    }
  }

  /**
   * generateTrailer
   * method to reset DB records in event of exceptions - allows for the
   * process to be retriggered.
   * @param orderSize - int indicating number of orders
   **/
  private String generateTrailer (int orderSize)
  {
   int padSize = TRAILER_LENGTH;
   StringBuffer sb = new StringBuffer(10);
   sb.append(NumberFormatter.getPaddedInt(orderSize,padSize));
   sb.append(DateTimeFormatter.getCurDateString("MMddyy"));
   //log.debug("generateTrailer = "+ sb.toString());
   return sb.toString();
  }

  /**
  * isInternalOrder
  * method to check merchant number against list - might
  * need to migrate to hierarchy check
  * @param merchId - long merchant number
  **/
  private boolean isInternalOrder (long merchNum)
  {
   boolean isInternal = false;
   for(int i=0;i<internalUsers.length;i++)
   {
     if(merchNum==internalUsers[i])
     {
       isInternal=true;
       break;
     }
   }
   return isInternal;
  }
  
  public void testTransmission()
  {
    Ftps ftps = null;
    
    try
    {
      String ftpsHost      = MesDefaults.getString(MesDefaults.DK_GCF_FTPS_IP_ADDRESS);
      int    ftpsPort      = MesDefaults.getInt(MesDefaults.DK_GCF_FTPS_PORT);
      String ftpsUser      = MesDefaults.getString(MesDefaults.DK_GCF_USER);
      String ftpsPassword  = MesDefaults.getString(MesDefaults.DK_GCF_PASSWORD);
      
      log.debug("ftpsHost = "+ftpsHost);
      log.debug("ftpsPort = "+ftpsPort);
      log.debug("ftpsUser = "+ftpsUser);
      log.debug("ftpsPassword = "+ftpsPassword);
      
      ftps     = new Ftps(ftpsHost,ftpsUser,ftpsPassword,ftpsPort);
      
      log.debug("CONNECTING...");
      ftps.connect();
      
      log.debug("setting directory to: " + UPLOAD_DIRECTORY);
      ftps.setDir(UPLOAD_DIRECTORY);
      
      log.debug("setting ASCII mode");
      ftps.setAscii();
      
      String testFile = "This is a test file, please ignore";
      
      log.debug("uploading test file");
      ftps.upload(testFile.getBytes(), "TESTFILE.DAT");
    }
    catch(Exception e)
    {
      log.error("testTransmission(): " + e.getMessage());
    }
    finally
    {
      try{ ftps.disconnect(); } catch(Exception e) {}
    }
  }
  
  public static void main (String[] args)
  {
    try
    {
      com.mes.database.SQLJConnectionBase.initStandalone();
      
      GCFOrderFile test = new GCFOrderFile();
      
      test.testTransmission();
    }
    catch(Exception e)
    {
      System.out.println("ERROR: main(): " + e.toString());
    }
  }

}/*@lineinfo:generated-code*/