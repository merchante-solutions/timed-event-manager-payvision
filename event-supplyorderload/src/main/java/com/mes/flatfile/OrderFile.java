/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/flatfile/OrderFile.java $

  Description:

    OrderFile

    Interface for OrderFiles handled by SupplyOrderLoad

  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 10/06/04 1:30p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.flatfile;

public interface OrderFile
{
  public boolean build();
  public boolean rebuild(long procSeq);
  public boolean transmit();
  public void archive();
  public String getIdentifier();
  public String getErrors();
  public boolean hasErrors();
  public void bill();
}