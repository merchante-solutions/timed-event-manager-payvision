/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/flatfile/OrderFileFactory.java $

  Description:OrderFileFactory

    Concrete factory class to allow for different types of OrderFiles
    to be created - currently only one: GCF. This could all be altered to allow
    database to control which is the current supply contact...that identifier
    would then be used to generate the appropriate OrderFile interface..

  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 5/06/04 11:16a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.flatfile;

public class OrderFileFactory
{
  public static final int GCF = 1;

  public static final int SUPPLY_CURRENT = GCF;

  public static OrderFile createOrderFile(int type)
  {
    switch(type) {
      case GCF:
        return new GCFOrderFile();
      default:
        return null;
    }
  }

} // class OrderFileFactory