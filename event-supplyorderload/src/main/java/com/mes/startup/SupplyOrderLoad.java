/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/SupplyOrderLoad.java $

  Description:

    SupplyOrderLoad

    Timed Event for creating and sending Supply Orders

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

// log4j
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.OrderFile;
import com.mes.flatfile.OrderFileFactory;
import com.mes.net.MailMessage;

public class SupplyOrderLoad extends EventBase
{

  // create class log category
  static Logger log = Logger.getLogger(SupplyOrderLoad.class);

  //will be something like: "./supplyorder/"
  public static final String ORDER_FILE_PATH = "c:/temp/";

  /**
   * METHOD execute()
   *
   * generates/processes file for supply ordering
   * keep methods seperate for future flexibility
   */

  public boolean execute()
  {
    return processOrderFile();
  }

  /**
   * METHOD processOrderFile()
   *
   * Uses abstract OrderFile to generate/process
   * the existing orders - only a single instance
   * can be currently used (i.e. only one Supplier
   * to provide all order generation)
   */
  private boolean processOrderFile()
  {
    boolean result = false;
    try
    {
      // instantiate specified class
      OrderFile orderFile = OrderFileFactory.createOrderFile(OrderFileFactory.SUPPLY_CURRENT);
      //StringBuffer newFileName = new StringBuffer("");
      log.debug("building...");
      // build file
      if(orderFile.build())
      {

        //OK? then transmit...
        log.debug("transmitting...");

        // generate new file name for processing and archiving
        // (will match the filename sent to whoever is processing
        // this order)
        //newFileName.append(orderFile.getIdentifier());

        if(orderFile.transmit())
        {

          try
          {
            //will only bill for >0 order totals
            orderFile.bill();

            //save file for archiving purposes
            orderFile.archive();

          }
          catch(Exception e)
          {
            //log.debug("processOrderFile(): " + e.toString());
            logEntry("processOrderFile()::bill_archive", e.toString());
          }

          result = true;
        }

        //notify(orderFile, newFileName.toString());
        notify(orderFile);
      }
/*

         //for file regeneration
      //long procSeq = 235L;
      //long procSeq = 283L;
      //long procSeq = 541L;
      //long procSeq = 778L;
      if(orderFile.rebuild(procSeq))
      {
        newFileName.append("rebuilt_order.dat");
        String orderFileString = orderFile.toString();

        BufferedWriter out = new BufferedWriter(new FileWriter(ORDER_FILE_PATH + newFileName.toString()));
        out.write(orderFileString);
        out.close();

        result = true;
      }
       */

    }
    catch(Exception be)
    {
      logEntry("processOrderFile()::build_transmit",be.toString());
      try
      {
        notify(null,null);
      }
      catch(Exception a)
      {
        logEntry("can't even email outta this...",be.toString());
      }
    }

    return result;

  }//processOrderFile()


  private void notify(OrderFile orderFile)
  throws Exception
  {
    notify(orderFile, orderFile.getIdentifier());
  }

  private void notify(OrderFile orderFile, String newFileName)
  throws Exception
  {

    //this? or use email to notify of errors...??
    StringBuffer message  = new StringBuffer("");
    StringBuffer subject  = new StringBuffer("");

    if(orderFile!=null)
    {
      if(orderFile.hasErrors()){

        // failure
        //log.debug("processOrderFile()\n: " +orderFile.getErrors());
        logEntry("processOrderFile()\n",orderFile.getErrors());

        subject.append("Supply Upload Failure");

        message.append("Supply file ");
        message.append(newFileName);
        message.append(".dat failed to process completely.\n\n");
        message.append("Errors:\n");
        message.append(orderFile.getErrors());

      }else{

        // success
        subject.append("Supply Upload Success");

        message.append("Supply file ");
        message.append(newFileName);
        message.append(".dat processed successfully.");
      }
    }
    else
    {
      subject.append("**SUPPLY ORDER SNAFU**");
      message.append("select * from supply_order where load_filename is null and process_sequence is not null ");
    }

    MailMessage msg = new MailMessage();
    msg.setAddresses(MesEmails.MSG_ADDRS_SUPPLY_ORDER_NOTIFY);
    msg.setSubject(subject.toString());
    msg.setText(message.toString());
    msg.send();

  }


  public static void main( String[] args )
  {
    try {

      if (args.length > 0 && args[0].equals("testproperties")) {
        EventBase.printKeyListStatus(new String[]{
                MesDefaults.GCF_ORDER_FILE_FTP_ARC_HOST,
                MesDefaults.GCF_ORDER_FILE_FTP_ARC_PORT,
                MesDefaults.GCF_ORDER_FILE_FTP_ARC_USER,
                MesDefaults.GCF_ORDER_FILE_FTP_ARC_PASSWORD,
                MesDefaults.GCF_ORDER_FILE_INTERNAL_USERS,
                MesDefaults.DK_GCF_FTPS_IP_ADDRESS,
                MesDefaults.DK_GCF_FTPS_PORT,
                MesDefaults.DK_GCF_USER,
                MesDefaults.DK_GCF_PASSWORD,
                MesDefaults.DK_GCF_IP_ADDRESS
        });
      }
    } catch (Exception e) {
      System.out.println(e.toString());
    }


    SQLJConnectionBase.initStandalone();

    SupplyOrderLoad load = new SupplyOrderLoad();

    if(load.execute())
    {
      System.out.println("SUCCESS!");
    }
    else
    {
      System.out.println("FAILURE!");
    }
  }

}
