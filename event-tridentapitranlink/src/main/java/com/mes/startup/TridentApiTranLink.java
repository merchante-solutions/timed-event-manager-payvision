/*@lineinfo:filename=TridentApiTranLink*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/TridentApiTranLink.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2010-08-09 11:17:10 -0700 (Mon, 09 Aug 2010) $
  Version            : $Revision: 17681 $

  Change History:
     See VSS database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class TridentApiTranLink extends EventBase
{
  static Logger log = Logger.getLogger(TridentApiTranLink.class);
  
  public static final int         PT_LINK_EXT_DT      = 0;
  public static final int         PT_LINK_DDF         = 1;
  public static final int         PT_LINK_CB          = 2;
  
  public static class DdfData
  {
    public      Date        BatchDate   = null;
    public      String      BatchNumber = null;
    public      String      CardNumber  = null;
    public      long        DdfDtId     = 0L;
    public      long        MerchantId  = 0L;
    public      String      PurchaseId  = null;
    public      String      RefNum      = null;
    public      double      TranAmount  = 0.0;
    public      Date        TranDate    = null;
  }
  
  public static class ExtDtData
  {
    public      Date        BatchDate   = null;
    public      String      CardNumber  = null;
    public      long        MerchantId  = 0L;
    public      String      PurchaseId  = null;
    public      String      RefNum      = null;
    public      long        SeqNum      = 0L;
    public      double      TranAmount  = 0.0;
    public      Date        TranDate    = null;
  }
  
  public class LinkRecord
  {
    protected   long        ApiRecId        = 0L;
    protected   String      PurchaseId      = null;
    protected   Date        TranDate        = null;
    protected   String      TridentTranId   = null;
    protected   String      ClientRefNum    = null;
    
    private     DdfData     Ddf             = null;
    private     ExtDtData   ExtDt           = null;
    
    public LinkRecord( )
    {
    }
  
    public void addApiData( ResultSet resultSet ) 
      throws java.sql.SQLException
    {
      ApiRecId      = resultSet.getLong("api_rec_id");
      PurchaseId    = resultSet.getString("purchase_id");
      TranDate      = resultSet.getDate("tran_date");
      TridentTranId = resultSet.getString("trident_tran_id");
      ClientRefNum  = resultSet.getString("client_ref_num");
    }
    
    public void addDdfData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      if ( Ddf == null )
      {
        Ddf = new DdfData();
      }
      // extract primary key fields
      Ddf.BatchDate   = resultSet.getDate("batch_date");
      Ddf.BatchNumber = resultSet.getString("batch_number");
      Ddf.DdfDtId     = resultSet.getLong("ddf_dt_id");
      
      // extract search fields
      Ddf.MerchantId  = resultSet.getLong("merchant_number");
      Ddf.CardNumber  = resultSet.getString("card_number");
      Ddf.TranAmount  = resultSet.getDouble("tran_amount");
      Ddf.TranDate    = resultSet.getDate("tran_date");
      Ddf.RefNum      = resultSet.getString("ref_num");
      Ddf.PurchaseId  = resultSet.getString("purchase_id");
    }
    
    public void addExtDtData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      if ( ExtDt == null )
      {
        ExtDt = new ExtDtData();
      }
      // extract primary key fields
      ExtDt.BatchDate   = resultSet.getDate("batch_date");
      ExtDt.MerchantId  = resultSet.getLong("merchant_number");
      ExtDt.SeqNum      = resultSet.getLong("row_seq");
      
      // extract search fields
      ExtDt.CardNumber  = resultSet.getString("card_number");
      ExtDt.TranAmount  = resultSet.getDouble("tran_amount");
      ExtDt.TranDate    = resultSet.getDate("tran_date");
      ExtDt.RefNum      = resultSet.getString("ref_num");
      ExtDt.PurchaseId  = resultSet.getString("purchase_id");
    }
    
    protected void create()
      throws java.sql.SQLException
    {
      /*@lineinfo:generated-code*//*@lineinfo:140^7*/

//  ************************************************************
//  #sql [Ctx] { insert into trident_api_detail_lookup
//          (
//            api_rec_id,
//            transaction_date,
//            trident_tran_id
//          )
//          values
//          (
//            :ApiRecId,
//            :TranDate,
//            :TridentTranId
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into trident_api_detail_lookup\n        (\n          api_rec_id,\n          transaction_date,\n          trident_tran_id\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.TridentApiTranLink",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ApiRecId);
   __sJT_st.setDate(2,TranDate);
   __sJT_st.setString(3,TridentTranId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:154^7*/
    }
    
    public long getApiRecId()
    {
      return( ApiRecId );
    }
    
    public Date getDdfBatchDate()
    {
      return( Ddf.BatchDate );
    }
    
    public String getDdfBatchNumber()
    {
      return( Ddf.BatchNumber );
    }
    
    public String getDdfCardNumber()
    {
      return( Ddf.CardNumber );
    }
    
    public long getDdfDtId()
    {
      return( Ddf.DdfDtId );
    }
    
    public long getDdfMerchantId()
    {
      return( Ddf.MerchantId );
    }
    
    public String getDdfPurchaseId()
    {
      return( Ddf.PurchaseId );
    }
    
    public String getDdfRefNum()
    {
      return( Ddf.RefNum );
    }
    
    public double getDdfTranAmount()
    {
      return( Ddf.TranAmount );
    }
    
    public Date getDdfTranDate()
    {
      return( Ddf.TranDate );
    }
    
    public Date getExtDtBatchDate()
    {
      return( ExtDt.BatchDate );
    }
    
    public String getExtDtCardNumber()
    {
      return( ExtDt.CardNumber );
    }
    
    public long getExtDtMerchantId()
    {
      return( ExtDt.MerchantId );
    }
    
    public String getExtDtPurchaseId()
    {
      return( ExtDt.PurchaseId );
    }
    
    public String getExtDtRefNum()
    {
      return( ExtDt.RefNum );
    }
    
    public long getExtDtSeqNum()
    {
      return( ExtDt.SeqNum );
    }
    
    public double getExtDtTranAmount()
    {
      return( ExtDt.TranAmount );
    }
    
    protected boolean hasRecord()
      throws java.sql.SQLException
    {
      int recCount  = 0;
      /*@lineinfo:generated-code*//*@lineinfo:246^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)  
//          from    trident_api_detail_lookup tadl
//          where   tadl.api_rec_id = :ApiRecId
//                  and tadl.transaction_date = :TranDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)   \n        from    trident_api_detail_lookup tadl\n        where   tadl.api_rec_id =  :1 \n                and tadl.transaction_date =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.TridentApiTranLink",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,ApiRecId);
   __sJT_st.setDate(2,TranDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:252^7*/
      return( recCount > 0 );
    }
    
    public void storeDdf( )
    {
      try
      {
        if ( !hasRecord() )
        {
          create();
        }
        
        Date    batchDate   = getDdfBatchDate();
        String  batchNumber = getDdfBatchNumber();
        long    ddfDtId     = getDdfDtId();
        long    merchantId  = getDdfMerchantId();
      
        /*@lineinfo:generated-code*//*@lineinfo:270^9*/

//  ************************************************************
//  #sql [Ctx] { update  trident_api_detail_lookup
//            set     dt_batch_date = :batchDate,
//                    dt_batch_number = :batchNumber,
//                    dt_ddf_dt_id = :ddfDtId,
//                    merchant_number = nvl(merchant_number,:merchantId)
//            where   api_rec_id = :ApiRecId
//                    and transaction_date = :TranDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_api_detail_lookup\n          set     dt_batch_date =  :1 ,\n                  dt_batch_number =  :2 ,\n                  dt_ddf_dt_id =  :3 ,\n                  merchant_number = nvl(merchant_number, :4 )\n          where   api_rec_id =  :5 \n                  and transaction_date =  :6";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.TridentApiTranLink",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,batchDate);
   __sJT_st.setString(2,batchNumber);
   __sJT_st.setLong(3,ddfDtId);
   __sJT_st.setLong(4,merchantId);
   __sJT_st.setLong(5,ApiRecId);
   __sJT_st.setDate(6,TranDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:279^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:281^9*/

//  ************************************************************
//  #sql [Ctx] { update  daily_detail_file_dt
//            set     trident_tran_id = :TridentTranId,
//                    purchase_id = nvl(purchase_id,:PurchaseId),
//                    client_reference_number = nvl(client_reference_number, :ClientRefNum)
//            where   batch_date = :batchDate 
//                    and batch_number = :batchNumber
//                    and ddf_dt_id = :ddfDtId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  daily_detail_file_dt\n          set     trident_tran_id =  :1 ,\n                  purchase_id = nvl(purchase_id, :2 ),\n                  client_reference_number = nvl(client_reference_number,  :3 )\n          where   batch_date =  :4  \n                  and batch_number =  :5 \n                  and ddf_dt_id =  :6";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.TridentApiTranLink",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,TridentTranId);
   __sJT_st.setString(2,PurchaseId);
   __sJT_st.setString(3,ClientRefNum);
   __sJT_st.setDate(4,batchDate);
   __sJT_st.setString(5,batchNumber);
   __sJT_st.setLong(6,ddfDtId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:290^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:292^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:295^9*/
      }
      catch( Exception e )
      {
        logEntry( "storeDdf(" + ApiRecId + ")", e.toString() );
      }
    }
    
    public void storeExtDt( )
    {
      try
      {
        if ( !hasRecord() )
        {
          create();
        }
        
        Date  batchDate   = getExtDtBatchDate();
        long  merchantId  = getExtDtMerchantId();
        long  seqNum      = getExtDtSeqNum();
      
        /*@lineinfo:generated-code*//*@lineinfo:316^9*/

//  ************************************************************
//  #sql [Ctx] { update  trident_api_detail_lookup
//            set     ext_dt_batch_date = :batchDate,
//                    merchant_number = nvl(merchant_number,:merchantId),
//                    ext_dt_row_seq = :seqNum
//            where   api_rec_id = :ApiRecId
//                    and transaction_date = :TranDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  trident_api_detail_lookup\n          set     ext_dt_batch_date =  :1 ,\n                  merchant_number = nvl(merchant_number, :2 ),\n                  ext_dt_row_seq =  :3 \n          where   api_rec_id =  :4 \n                  and transaction_date =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.TridentApiTranLink",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,batchDate);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setLong(3,seqNum);
   __sJT_st.setLong(4,ApiRecId);
   __sJT_st.setDate(5,TranDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:324^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:326^9*/

//  ************************************************************
//  #sql [Ctx] { update  daily_detail_file_ext_dt
//            set     trident_tran_id = :TridentTranId,
//                    purchase_id = nvl(purchase_id,:PurchaseId),
//                    client_reference_number = nvl(client_reference_number, :ClientRefNum)
//            where   batch_date = :batchDate 
//                    and merchant_number = :merchantId
//                    and row_sequence = :seqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  daily_detail_file_ext_dt\n          set     trident_tran_id =  :1 ,\n                  purchase_id = nvl(purchase_id, :2 ),\n                  client_reference_number = nvl(client_reference_number,  :3 )\n          where   batch_date =  :4  \n                  and merchant_number =  :5 \n                  and row_sequence =  :6";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.TridentApiTranLink",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,TridentTranId);
   __sJT_st.setString(2,PurchaseId);
   __sJT_st.setString(3,ClientRefNum);
   __sJT_st.setDate(4,batchDate);
   __sJT_st.setLong(5,merchantId);
   __sJT_st.setLong(6,seqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:335^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:337^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:340^9*/
      }
      catch( Exception e )
      {
        logEntry( "storeExtDt(" + ApiRecId + ")", e.toString() );
      }
    }
  }
  
  private   int         SkipPurchaseIdTest    = 0;
  
  public boolean execute()
  {
    ProcessTable.ProcessTableEntry  entry         = null;
    boolean                         retVal        = false;
    ProcessTable                    pt            = null;

    try
    {
      connect();
      
      pt = new ProcessTable("trident_api_lookup_process","tapi_lookup_process_sequence",ProcessTable.PT_ALL);
      
      if ( pt.hasPendingEntries() )
      {
        pt.preparePendingEntries();
        
        Vector entries = pt.getEntryVector();
        for( int i = 0; i < entries.size(); ++i )
        {
          entry = (ProcessTable.ProcessTableEntry)entries.elementAt(i);
          
          switch( entry.getProcessType() )
          {
            case PT_LINK_EXT_DT:
              entry.recordTimestampBegin();
              linkBatchToApi(entry.getLoadFilename());
              entry.recordTimestampEnd();
              break;
              
            case PT_LINK_DDF:
              entry.recordTimestampBegin();
              linkDdfToApi(entry.getLoadFilename());
              entry.recordTimestampEnd();
              break;
              
            case PT_LINK_CB:
              entry.recordTimestampBegin();
              //@linkChargebacksToApi();
              entry.recordTimestampEnd();
              break;
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  protected void linkBatchToApi( String loadFilename )
  {
    long                apiRecId        = 0L;
    Vector              entries         = new Vector();
    ResultSetIterator   it              = null;
    LinkRecord          linkItem        = null;
    String              purchaseId      = null;
    ResultSet           resultSet       = null;
    String              tridentTranId   = null;
    
    // timing variables
    int             recCount        = 0;
    long            queryTimeBegin  = 0L;
    long            queryTimeMax    = 0L;
    long            queryTimeCurrent= 0L;
    long            queryTimeTotal  = 0L;
    int             unlinkedCount   = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:426^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_number            as merchant_number,
//                  dt.batch_date                 as batch_date,
//                  dt.transaction_date           as tran_date,
//                  dt.row_sequence               as row_seq,
//                  dt.card_number                as card_number,
//                  dt.transaction_amount         as tran_amount,
//                  ltrim(rtrim(dt.purchase_id))  as purchase_id,
//                  dt.reference_number           as ref_num
//          from    daily_detail_file_ext_dt  dt,
//                  trident_profile           tp
//          where   dt.load_filename = :loadFilename
//                  and tp.catid = dt.pos_terminal_id
//                  and nvl(tp.api_enabled,'N') = 'Y'
//                  and dt.trident_tran_id is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_number            as merchant_number,\n                dt.batch_date                 as batch_date,\n                dt.transaction_date           as tran_date,\n                dt.row_sequence               as row_seq,\n                dt.card_number                as card_number,\n                dt.transaction_amount         as tran_amount,\n                ltrim(rtrim(dt.purchase_id))  as purchase_id,\n                dt.reference_number           as ref_num\n        from    daily_detail_file_ext_dt  dt,\n                trident_profile           tp\n        where   dt.load_filename =  :1 \n                and tp.catid = dt.pos_terminal_id\n                and nvl(tp.api_enabled,'N') = 'Y'\n                and dt.trident_tran_id is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.TridentApiTranLink",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.startup.TridentApiTranLink",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:442^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        linkItem = new LinkRecord();
        linkItem.addExtDtData(resultSet);
        entries.addElement( linkItem );
      }        
      resultSet.close();
      it.close();
      
      for( int i = 0; i < entries.size(); ++i )
      {
        linkItem    = (LinkRecord) entries.elementAt(i);
        purchaseId  = linkItem.getExtDtPurchaseId();
        
        queryTimeBegin = System.currentTimeMillis();
        /*@lineinfo:generated-code*//*@lineinfo:460^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                        ordered 
//                    */
//                    tapi.rec_id                             as api_rec_id,
//                    tapi.trident_tran_id                    as trident_tran_id,
//                    tapi.purchase_id                        as purchase_id,
//                    tapi.client_reference_number            as client_ref_num,
//                    tapi.transaction_date                   as tran_date
//            from    trident_capture_api         tapi,
//                    trident_api_detail_lookup   tadl
//            where   tapi.merchant_number = :linkItem.getExtDtMerchantId()
//                    and tapi.batch_date = :linkItem.getExtDtBatchDate()
//                    and tapi.reference_number = :linkItem.getExtDtRefNum()
//                    and (
//                          :SkipPurchaseIdTest = 1 or
//                          :purchaseId is null or
//                          nvl(tapi.purchase_id,'none') = :purchaseId
//                        )
//                    and tapi.card_number = :linkItem.getExtDtCardNumber()
//                    and tapi.transaction_amount = :linkItem.getExtDtTranAmount()
//                    and tadl.api_rec_id(+) = tapi.rec_id
//                    and tadl.transaction_date(+) = tapi.transaction_date
//                    and tadl.ext_dt_batch_date is null
//            order by tapi.rec_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4867 = linkItem.getExtDtMerchantId();
 java.sql.Date __sJT_4868 = linkItem.getExtDtBatchDate();
 String __sJT_4869 = linkItem.getExtDtRefNum();
 String __sJT_4870 = linkItem.getExtDtCardNumber();
 double __sJT_4871 = linkItem.getExtDtTranAmount();
  try {
   String theSqlTS = "select  /*+ \n                      ordered \n                  */\n                  tapi.rec_id                             as api_rec_id,\n                  tapi.trident_tran_id                    as trident_tran_id,\n                  tapi.purchase_id                        as purchase_id,\n                  tapi.client_reference_number            as client_ref_num,\n                  tapi.transaction_date                   as tran_date\n          from    trident_capture_api         tapi,\n                  trident_api_detail_lookup   tadl\n          where   tapi.merchant_number =  :1 \n                  and tapi.batch_date =  :2 \n                  and tapi.reference_number =  :3 \n                  and (\n                         :4  = 1 or\n                         :5  is null or\n                        nvl(tapi.purchase_id,'none') =  :6 \n                      )\n                  and tapi.card_number =  :7 \n                  and tapi.transaction_amount =  :8 \n                  and tadl.api_rec_id(+) = tapi.rec_id\n                  and tadl.transaction_date(+) = tapi.transaction_date\n                  and tadl.ext_dt_batch_date is null\n          order by tapi.rec_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.TridentApiTranLink",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4867);
   __sJT_st.setDate(2,__sJT_4868);
   __sJT_st.setString(3,__sJT_4869);
   __sJT_st.setInt(4,SkipPurchaseIdTest);
   __sJT_st.setString(5,purchaseId);
   __sJT_st.setString(6,purchaseId);
   __sJT_st.setString(7,__sJT_4870);
   __sJT_st.setDouble(8,__sJT_4871);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.TridentApiTranLink",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:486^9*/
        
        // timing stuff
        queryTimeCurrent = (System.currentTimeMillis() - queryTimeBegin);
        if ( queryTimeCurrent > queryTimeMax )
        {
          queryTimeMax = queryTimeCurrent;
        }
        queryTimeTotal += queryTimeCurrent;
        
        // check for results
        resultSet = it.getResultSet();
        
        if ( resultSet.next() )   // found EXT_DT data
        {
          linkItem.addApiData(resultSet);
          linkItem.storeExtDt();
        }
        else
        {
          if ( unlinkedCount == 0 ) { log.debug("\nKey = " + linkItem.getExtDtMerchantId() + "," + linkItem.getExtDtBatchDate() + "," + linkItem.getExtDtRefNum()); }//@
          ++unlinkedCount;
        }
        resultSet.close();
        it.close();
        
        //System.out.print("Processed Count: " + ++recCount + "  Max Query Time: " + queryTimeMax + "  Last Query Time: " + queryTimeCurrent + "  Avg Query Time: " + (queryTimeTotal/recCount) + "  Unlinked: " + unlinkedCount + "                 \r");//@
      }
    }
    catch( Exception e )
    {
      // ignore?
      logEntry("linkBatchToApi( " + loadFilename + " )", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e){}
    }
  }
  
  protected void linkDdfToApi( String loadFilename )
  {
    long                apiRecId        = 0L;
    Vector              entries         = new Vector();
    ResultSetIterator   it              = null;
    LinkRecord          linkItem        = null;
    String              purchaseId      = null;
    ResultSet           resultSet       = null;
    String              tridentTranId   = null;
    
    // timing variables
    int             recCount        = 0;
    long            queryTimeBegin  = 0L;
    long            queryTimeMax    = 0L;
    long            queryTimeCurrent= 0L;
    long            queryTimeTotal  = 0L;
    int             unlinkedCount   = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:546^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_account_number        as merchant_number,
//                  dt.batch_date                     as batch_date,
//                  dt.batch_number                   as batch_number,
//                  dt.ddf_dt_id                      as ddf_dt_id,
//                  dt.transaction_date               as tran_date,
//                  dt.cardholder_account_number      as card_number,                
//                  dt.transaction_amount             as tran_amount,
//                  ltrim(rtrim(dt.purchase_id))      as purchase_id,
//                  to_number(substr(dt.reference_number,12,11))  as ref_num
//          from    daily_detail_file_dt    dt
//          where   dt.load_filename = :loadFilename
//                  and exists
//                  (
//                    select  tp.merchant_number
//                    from    trident_profile tp
//                    where   tp.merchant_number = dt.merchant_account_number
//                            and nvl(tp.api_enabled,'N') = 'Y'
//                  )
//                  and dt.trident_tran_id is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_account_number        as merchant_number,\n                dt.batch_date                     as batch_date,\n                dt.batch_number                   as batch_number,\n                dt.ddf_dt_id                      as ddf_dt_id,\n                dt.transaction_date               as tran_date,\n                dt.cardholder_account_number      as card_number,                \n                dt.transaction_amount             as tran_amount,\n                ltrim(rtrim(dt.purchase_id))      as purchase_id,\n                to_number(substr(dt.reference_number,12,11))  as ref_num\n        from    daily_detail_file_dt    dt\n        where   dt.load_filename =  :1 \n                and exists\n                (\n                  select  tp.merchant_number\n                  from    trident_profile tp\n                  where   tp.merchant_number = dt.merchant_account_number\n                          and nvl(tp.api_enabled,'N') = 'Y'\n                )\n                and dt.trident_tran_id is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.TridentApiTranLink",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.startup.TridentApiTranLink",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:567^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        linkItem = new LinkRecord();
        linkItem.addDdfData(resultSet);
        entries.addElement( linkItem );
      }        
      resultSet.close();
      it.close();
      
      for( int i = 0; i < entries.size(); ++i )
      {
        linkItem    = (LinkRecord) entries.elementAt(i);
        purchaseId  = linkItem.getDdfPurchaseId();
        
        queryTimeBegin = System.currentTimeMillis();
        /*@lineinfo:generated-code*//*@lineinfo:585^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                        ordered 
//                    */
//                    tapi.rec_id                             as api_rec_id,
//                    tapi.trident_tran_id                    as trident_tran_id,
//                    tapi.purchase_id                        as purchase_id,
//                    tapi.client_reference_number            as client_ref_num,
//                    tapi.transaction_date                   as tran_date
//            from    trident_capture_api         tapi,
//                    trident_api_detail_lookup   tadl
//            where   tapi.merchant_number = :linkItem.getDdfMerchantId()
//                    and tapi.transaction_date = :linkItem.getDdfTranDate()
//                    and tapi.reference_number = :linkItem.getDdfRefNum()
//                    and (
//                          :SkipPurchaseIdTest = 1 or
//                          :purchaseId is null or
//                          nvl(tapi.purchase_id,'none') = :purchaseId
//                        )
//                    and tapi.card_number = :linkItem.getDdfCardNumber()
//                    and tapi.transaction_amount = :linkItem.getDdfTranAmount()
//                    and tadl.api_rec_id(+) = tapi.rec_id
//                    and tadl.transaction_date(+) = tapi.transaction_date
//                    and tadl.dt_batch_date is null
//            order by tapi.rec_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4872 = linkItem.getDdfMerchantId();
 java.sql.Date __sJT_4873 = linkItem.getDdfTranDate();
 String __sJT_4874 = linkItem.getDdfRefNum();
 String __sJT_4875 = linkItem.getDdfCardNumber();
 double __sJT_4876 = linkItem.getDdfTranAmount();
  try {
   String theSqlTS = "select  /*+ \n                      ordered \n                  */\n                  tapi.rec_id                             as api_rec_id,\n                  tapi.trident_tran_id                    as trident_tran_id,\n                  tapi.purchase_id                        as purchase_id,\n                  tapi.client_reference_number            as client_ref_num,\n                  tapi.transaction_date                   as tran_date\n          from    trident_capture_api         tapi,\n                  trident_api_detail_lookup   tadl\n          where   tapi.merchant_number =  :1 \n                  and tapi.transaction_date =  :2 \n                  and tapi.reference_number =  :3 \n                  and (\n                         :4  = 1 or\n                         :5  is null or\n                        nvl(tapi.purchase_id,'none') =  :6 \n                      )\n                  and tapi.card_number =  :7 \n                  and tapi.transaction_amount =  :8 \n                  and tadl.api_rec_id(+) = tapi.rec_id\n                  and tadl.transaction_date(+) = tapi.transaction_date\n                  and tadl.dt_batch_date is null\n          order by tapi.rec_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.TridentApiTranLink",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4872);
   __sJT_st.setDate(2,__sJT_4873);
   __sJT_st.setString(3,__sJT_4874);
   __sJT_st.setInt(4,SkipPurchaseIdTest);
   __sJT_st.setString(5,purchaseId);
   __sJT_st.setString(6,purchaseId);
   __sJT_st.setString(7,__sJT_4875);
   __sJT_st.setDouble(8,__sJT_4876);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.startup.TridentApiTranLink",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:611^9*/
        
        // timing stuff
        queryTimeCurrent = (System.currentTimeMillis() - queryTimeBegin);
        if ( queryTimeCurrent > queryTimeMax )
        {
          queryTimeMax = queryTimeCurrent;
        }
        queryTimeTotal += queryTimeCurrent;
        
        // check for results
        resultSet = it.getResultSet();
        
        if ( resultSet.next() )   // found DDF data
        {
          linkItem.addApiData(resultSet);
          linkItem.storeDdf();
        }
        else
        {
          if ( unlinkedCount == 0 ) { log.debug("\nKey = " + linkItem.getDdfMerchantId() + "," + linkItem.getDdfBatchDate() + "," + linkItem.getDdfRefNum()); }//@
          ++unlinkedCount;
        }
        resultSet.close();
        it.close();
        
        //System.out.print("Processed Count: " + ++recCount + "  Max Query Time: " + queryTimeMax + "  Last Query Time: " + queryTimeCurrent + "  Avg Query Time: " + (queryTimeTotal/recCount) + "  Unlinked: " + unlinkedCount + "                 \r");//@
      }
    }
    catch( Exception e )
    {
      // ignore?
      logEntry("linkDdfToApi( " + loadFilename + " )", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e){}
    }
  }
  
  public void setSkipPurchaseIdTest( String newValue )
  {
    SkipPurchaseIdTest = Integer.parseInt(newValue); 
  }
  
  public static void main( String[] args )
  {
    TridentApiTranLink    loader   = null;
    
    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      
      loader = new TridentApiTranLink();
      loader.connect(true);
      
      if ( args.length > 0 && args[0].equals("linkBatchToApi") )
      {
        log.debug("linkBatchToApi( " + args[1] + " )");
        if ( args.length > 2 )
        {
          loader.setSkipPurchaseIdTest(args[2]);
        }
        loader.linkBatchToApi(args[1]);
      }
      else if ( args.length > 0 && args[0].equals("linkDdfToApi") )
      {
        log.debug("linkDdfToApi( " + args[1] + " )");
        if ( args.length > 2 )
        {
          loader.setSkipPurchaseIdTest(args[2]);
        }
        loader.linkDdfToApi(args[1]);
      }
      else
      {
        log.debug("execute()");
        loader.execute();
      }
    }
    catch( Exception e )
    {
      log.error(e.toString());
    }
    finally
    {
      try{ loader.cleanUp(); }catch( Exception e ){}
    }
  }
}/*@lineinfo:generated-code*/