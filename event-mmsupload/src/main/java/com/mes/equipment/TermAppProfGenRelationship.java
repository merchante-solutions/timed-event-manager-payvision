/*@lineinfo:filename=TermAppProfGenRelationship*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.equipment;

import java.sql.ResultSet;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;


/**
 * TermAppProfGenRelationship
 * 
 * Represents the relationship(s) between equip profile generators and terminal applications.
 * Specifically, provides the set of supported profile generators for a given terminal application.
 * Referneces mesdb.EQUIP_TERMAPP_PROFGEN table.
 */
public class TermAppProfGenRelationship extends SQLJConnectionBase
{
  public static String getDefaultProfGenTermApp(int profGenCode)
  {
    switch(profGenCode) {
      
      // mms
      default:
      case mesConstants.PROFGEN_MMS:
        return mesConstants.DEFAULT_TERMAPP_MMS;
      
      // Vericentre
      case mesConstants.PROFGEN_VERICENTRE:
        return mesConstants.DEFAULT_TERMAPP_VERICENTRE;
      
      // TermMaster
      case mesConstants.PROFGEN_TERMMASTER:
        return mesConstants.DEFAULT_TERMAPP_TERMMASTER;
    }
  }

  /**
   * getPrimaryProfileGenerator()
   *
   * Pigeon holes the specified term app to a single profile generator.
   * Where the M.O. is to seek non-mms prof gen if available otherwise go mms.
   * Returns mesConstants.PROFGEN_UNDEFINED when "indeterminate".
   */
  public int getPrimaryProfileGenerator(String termApp)
  {
    int[]   sta   = getSupportedProfileGeneratorCodes(termApp);
    int     ppg   = mesConstants.PROFGEN_UNDEFINED;
    int     mmsta = -1;
    
    for(int i=0;i<sta.length;i++) {
      if(sta[i]!=mesConstants.PROFGEN_MMS) {
        if(ppg!=mesConstants.PROFGEN_UNDEFINED)
          return mesConstants.PROFGEN_UNDEFINED;
        ppg=sta[i];
      } else
        mmsta=i;
    }
    if(ppg==mesConstants.PROFGEN_UNDEFINED && mmsta>=0)
      ppg=mesConstants.PROFGEN_MMS;

    return ppg;
  }

  public TermAppProfGenRelationship()
  {
  }
  
  public boolean doesProfGenSprtTermApp(int profGenCode,String termApp)
  {
    int[] sprtdProfGens = getSupportedProfileGeneratorCodes(termApp);
    
    if(sprtdProfGens == null || sprtdProfGens.length < 1)
      return false;
    
    for(int i=0;i<sprtdProfGens.length;i++) {
      if(profGenCode == sprtdProfGens[i])
        return true;
    }
    
    return false;
  }
  
  public int[] getSupportedProfileGeneratorCodes(String termApp)
  {
    if(termApp == null || termApp.length() < 1)
      return null;
    
    int[] rval = null;
    
    ResultSetIterator it = null;
    ResultSet rs = null;
    
    try 
    {
      connect();
      
      // get the count
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:99^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(etp.profgen_code)
//          
//          from    equip_termapp_profgen     etp
//                  ,EQUIP_PROFILE_GENERATORS epg
//          where   etp.profgen_code=epg.code
//                  and terminal_application = :termApp
//                  and epg.is_on=1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(etp.profgen_code)\n         \n        from    equip_termapp_profgen     etp\n                ,EQUIP_PROFILE_GENERATORS epg\n        where   etp.profgen_code=epg.code\n                and terminal_application =  :1 \n                and epg.is_on=1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.equipment.TermAppProfGenRelationship",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,termApp);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:108^7*/
      
      if(recCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:112^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  etp.profgen_code
//            from    equip_termapp_profgen     etp
//                    ,EQUIP_PROFILE_GENERATORS epg
//            where   etp.profgen_code=epg.code
//                    and terminal_application = :termApp
//                    and epg.is_on=1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  etp.profgen_code\n          from    equip_termapp_profgen     etp\n                  ,EQUIP_PROFILE_GENERATORS epg\n          where   etp.profgen_code=epg.code\n                  and terminal_application =  :1 \n                  and epg.is_on=1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.equipment.TermAppProfGenRelationship",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,termApp);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.equipment.TermAppProfGenRelationship",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:120^9*/
      
        rs = it.getResultSet();
        rval = new int[recCount];
        recCount = 0;
      
        while(rs.next())
        {
          rval[recCount++] = rs.getInt(1);
        }
      
        rs.close();
        it.close();
      }
    }
    catch(Exception e) 
    {
      logEntry("getSupportedProfileGeneratorCodes()",e.toString());
    }
    finally 
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return rval;
  }

}/*@lineinfo:generated-code*/