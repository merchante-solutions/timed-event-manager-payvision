/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/support/PGPCmdLine.java $

  Description:  


  Last Modified By   : $Author: Jkirton $
  Last Modified Date : $Date: 8/28/02 4:05p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.util.Date;


/**
 * PGPCmdLine
 * 
 * UTILITY class
 * Wrapper around: PGP.exe Command Line (v6.5.8)
 * (PGP: http://web.mit.edu/network/pgp.html)
 *                  
 */
public final class PGPCmdLine
{
  
  // constants
  private static final long        MAX_ALLOWED_PROCESS_TIME_MILIS  = 1000*5;
  
  // data members
  private boolean isCmdIssued;
  private long mstart;
  private Process process;
  private String cmd;
  private int cmdRval;
  
  // construction
  public PGPCmdLine()
  {
    isCmdIssued=false;
    mstart=0;
    process=null;
    cmd=null;
    cmdRval=-3;
  }
  
  private void clear()
  {
    if(process!=null)
      process.destroy();
    
    isCmdIssued=false;
    process=null;
    cmd=null;
    cmdRval=-3;
  }
  
  
  /**
   * encrypt() 
   * 
   * Encrypts spec. file via PGP.exe producing ADJACENT encrypted file w/ .pgp extension added
   * Success when no exception thrown.
   * 
   * @param  pubring        (currently NOT used) public PGP keyring abs path filename
   * @param  userid         identifies the desired public key to employ
   * @param  plaintextfile  abs path filename of file to encrypt
   * @param  encFilename    FILLED IN: filename of encrypted file (has .pgp extension)
   */
  public void encrypt (String pubring, String userid, String plaintextfile, StringBuffer encFilename)
    throws Exception
  {
    boolean bClear = false;
    
    try {
      
      if(isCmdIssued)
        throw new Exception("Unable to PGP encrypt file - Currently command in waiting.");
      
      bClear=true;
        
      encFilename.delete(0,encFilename.length());  // clear buffer
        
      // command line FORMAT: 
      //    "pgp -e textfile her_userid [other userids]"
      //    (produces textfile.pgp)
      cmd = "pgp +batchmode +force -e " + plaintextfile + " " + userid;
        
      processCommand();
        
      if(cmdRval == 0) {
        encFilename.append(plaintextfile);
        encFilename.append(".pgp");
      } else
        throw new Exception("Error attempting to encrypt file: '"+getRvalDesc(cmdRval)+"'.");
    }
    catch(Exception e) {
      throw e;
    }
    finally {
      if(bClear)
        clear();
    }
  }

  /**
   * decrypt()
   * 
   * Decrypts spec. file via PGP.exe.
   * Success when no exception thrown.
   * 
   * @param  prvring        (currently NOT used) private PGP keyring abs path filename
   * @param  passwd         (currently NOT used) private key passphrase to idenfity the private key to decrypt
   * @param  encFile        abs path filename of cipher file to decrypt
   * @param  plaintextfile  FILLED IN: filename of decrypted file
   */
  public void decrypt (String prvring, String passwd, String encFile, StringBuffer plaintextfile)
    throws Exception
  {
    boolean bClear = false;
    
    try {
      
      if(isCmdIssued)
        throw new Exception("Unable to decrypt - Currently command in waiting.");
      
      bClear=true;
      
      plaintextfile.delete(0,plaintextfile.length());  // clear buffer

      // command line FORMAT: 
      //    "pgp ciphertextfile [plaintextfile]"
      
      cmd = "pgp +force " + encFile;
      
      processCommand();
      
      if(cmdRval == 0)
        plaintextfile.append(encFile.substring(0,encFile.length()-4));
      else
        throw new Exception("Error attempting to decrypt file ('"+encFile+"'): '"+getRvalDesc(cmdRval)+"'.");
    }
    catch(Exception e) {
      throw e;
    }
    finally {
      if(bClear)
        clear();
    }
  }
  
  /**
   * processCommand()
   * 
   * Recursive function to manage executing command
   * Monitors amount of time to execute and kills command contained process if max time elapsed
   */
  private void processCommand()
    throws Exception
  {
    try {
      
      // issue command if not already
      if(!isCmdIssued) {
        mstart=new Date().getTime();
        
        process = Runtime.getRuntime().exec(cmd);
        isCmdIssued=true;
      }
      
      if((new Date().getTime() - mstart) >= MAX_ALLOWED_PROCESS_TIME_MILIS) {
        cmdRval=-2;
        throw new Exception("Max allowed time elapsed for process to complete.  Aborting.");
      }
      
      Thread.currentThread().sleep(5);  // wait

      cmdRval=process.exitValue();
    
    }
    catch(IllegalThreadStateException itse) {
      processCommand();
    }
    catch(Exception e) {
      cmdRval = -1;
      throw new Exception("PGPCmdLine.ExecAndWait Exception: '"+e.getMessage()+"'.");
    }
  }
  
  // class functions
  
  private static String getRvalDesc(int rvalCode)
  {
    
    switch(rvalCode) {
      
      case -3:  return "No command issued.";
      case -2:  return "Commmand timed out.";
      case -1:  return "Commmand not executed successfully.";

      case 0:  return "Exit OK, no error";
      case 1:  return "invalid file";
      case 2:  return "file not found";
      case 3:  return "unknownfile";
      case 4:  return "batchmode error";
      case 5:  return "bad argument";
      case 6:  return "process interrupted";
      case 7:  return "out of memory error";

      case 10:  return "key generation error";
      case 11:  return "non-existing key error";
      case 12:  return "keyring add error";
      case 13:  return "keyring extract error";
      case 14:  return "keyring edit error";
      case 15:  return "keyring view error";
      case 16:  return "keyring removal error";
      case 17:  return "keyring check error";
      case 18:  return "key signature error or key signature revoke error";
      case 19:  return "key signature removal error";

      case 20:  return "signature error";
      case 21:  return "public key encryption error";
      case 22:  return "encryption error";
      case 23:  return "compression error";

      case 30:  return "signature check error";
      case 31:  return "public key decryption error";
      case 32:  return "decryption error";
      case 33:  return "decompression error";
            
      default:
        return "";
    }
    
  }

} // class PGPCmdLine