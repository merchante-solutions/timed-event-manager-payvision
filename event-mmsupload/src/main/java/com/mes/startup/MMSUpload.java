/*@lineinfo:filename=MMSUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/MMSUpload.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-07-29 13:51:34 -0700 (Wed, 29 Jul 2015) $
  Version            : $Revision: 23755 $

  Change History:
     See VSS database

  Copyright (C) 2000-2004,2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Enumeration;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.mes.config.MesDefaults;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.flatfile.FlatFileRecordField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import com.mes.support.PGPCmdLine;
import sqlj.runtime.ResultSetIterator;

public class MMSUpload extends EventBase
{
  static Logger log = Logger.getLogger(MMSUpload.class);

  protected String MMSVersion = "";
  
  // files extensions are set by Vital.  MES "outgoing" equals vital "incoming"
  protected static final String     MMSOutgoingFileExtension   = ".MMSI";
  protected static final String     MMSIncomingFileExtension   = ".MMSO";
  
  protected static final String[]   MMSLegacyFileExtensions =
  {
    ".dat.pgp",
    ".err.pgp",
    ".lst.pgp",
    ".dat",
    ".err",
    ".lst",
  };
  
  public static class MMSBank
  {
    public String       AppSource     = null;
    public int          BankNumber    = 0;
    public String       Bin           = null;
    
    public MMSBank( int bankNumber, String appSource, String bin )
    {
      AppSource   = appSource;
      BankNumber  = bankNumber;
      Bin         = bin;
    }
  }
   
  private final static MMSBank [] ActiveBanks = 
  {
    new MMSBank( 3941, "MESR", "433239" ),
    new MMSBank( 3867, "BBT" , "407314" ),
    new MMSBank( 3942, "STRL", "430193" )
  };
  
  protected boolean           EncryptFile       = false;
  protected int               FileRecordCount   = 0;
  protected boolean           TestMode          = false;
  protected String            WorkFilename      = null;
  
  public MMSUpload( )
  {
    try {
      MMSVersion    = MesDefaults.getString(MesDefaults.MMS_VERSION);
    }
    catch(Exception e)
    {
      logEntry("MMSUpload( ) constructor - set the MMS version", e.toString());
      log.error(e.getMessage());
    }
  }
  
  public void buildMMSRecord( BufferedWriter out, ResultSet resultSet )
  {                               
    String              appType     = null;
    FlatFileRecord      ffd         = null;
    int                 flatFileId  = 0;
      
    try
    {
      appType     = resultSet.getString("term_application");
      flatFileId  = getFlatFileType( appType );
      
      if ( flatFileId != 0 )
      {
        ffd = new FlatFileRecord( flatFileId );
        ffd.setAllFieldData( resultSet );
        out.write( ffd.spew("|", FlatFileRecord.TC_UPPER ) );  // upper case only
        out.newLine();
      
        FileRecordCount++;
      
        if ( !TestMode )
        {
          long reqId = resultSet.getLong("req_id");
        
          // update the status in the database for this record
          /*@lineinfo:generated-code*//*@lineinfo:133^11*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info
//              set     process_start_date = sysdate,
//                      process_status     = :mesConstants.ETPPS_INPROCESS,
//                      output_filename    = :WorkFilename
//              where   request_id = :reqId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mms_stage_info\n            set     process_start_date = sysdate,\n                    process_status     =  :1 ,\n                    output_filename    =  :2 \n            where   request_id =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.MMSUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,mesConstants.ETPPS_INPROCESS);
   __sJT_st.setString(2,WorkFilename);
   __sJT_st.setLong(3,reqId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:140^11*/
        }        
      }        
    }
    catch(Exception e)
    {
      logEntry("buildMMSRecord(" + appType + ")", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
    }
  }
  
  protected BufferedWriter createNewRequestFile( String bin )
  {
    StringBuffer        buffer    = new StringBuffer();
    BufferedWriter      out       = null;
    
    try
    {
      // build the output filename
      buffer.setLength(0);
      buffer.append(bin);
      buffer.append( DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"yyyyMMdd") );
      buffer.append( DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"HHmm") );
      buffer.append( MMSOutgoingFileExtension );
      WorkFilename = buffer.toString();

      log.debug("Generating file: " + WorkFilename);//@

      // open the data file
      out = new BufferedWriter( new FileWriter( WorkFilename, false ) );
      
      // reset file variables
      FileRecordCount = 0;
    }
    catch( Exception e )
    {
      logEntry("createNewRequestFile(" + bin + ")", e.toString());
    }
    return( out );
  }
  
  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    try
    {
      // get an automated timeout exempt connection
      connect(true);
      
      processOutgoing();    // send new setups to vital
      processIncoming();    // pick up and available responses
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( true );
  }
  
  protected String extractOutputFilename( String filePath )
  {
    StringBuffer    buffer          = new StringBuffer();
    String          filename        = filePath;
    int             offset          = 0;
    
    try
    {
      offset = filename.lastIndexOf( System.getProperty("file.separator") );
      if ( offset >= 0 )
      {
        filename = filePath.substring(offset+1);
      }
      
      if ( isLegacyFile( filename ) )
      {
        buffer.append( filename.substring(0,filename.lastIndexOf(".")) );
        buffer.append( ".txt" );
      }
      else  // V 1.1.4
      {
        //                      012345678901234567
        // filenames begin with bbbbbbyyyymmddhhmi
        // bbbbbb = BIN
        // yyyy   = year
        // mm     = month
        // dd     = day
        // hh     = hour
        // mi     = minute
        //
        buffer.append( filename.substring(0,18) );
        buffer.append( MMSOutgoingFileExtension );
      }
    }
    catch( Exception e )
    {
      logEntry( "extractOutputFilename(" + filePath + ")", e.toString());
    }
    return( buffer.toString() );
  }
  
  protected int getFlatFileType( String appType )
  {
    int         defType     = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:259^7*/

//  ************************************************************
//  #sql [Ctx] { select  def_type 
//          from    flat_file_def_types
//          where   name = rtrim('MMS: ' || :appType || ' ' || :MMSVersion)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  def_type  \n        from    flat_file_def_types\n        where   name = rtrim('MMS: ' ||  :1  || ' ' ||  :2 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.MMSUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,appType);
   __sJT_st.setString(2,MMSVersion);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   defType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:264^7*/
    }
    catch( Exception e )
    {
      defType = 0;
      logEntry( "getFlatFileType( " + appType + ")", e.toString());
    }
    return( defType );
  }
  
  protected long getRequestId( long merchantId, int store, int term, String outputFilename )
  {
    long    retVal      = 0L;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:280^7*/

//  ************************************************************
//  #sql [Ctx] { select  mms.request_id 
//          from    mms_stage_info    mms
//          where   mms.merch_number = :merchantId and
//                  mms.store_number = :store and
//                  mms.terminal_number = :term and
//                  mms.output_filename = :outputFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mms.request_id  \n        from    mms_stage_info    mms\n        where   mms.merch_number =  :1  and\n                mms.store_number =  :2  and\n                mms.terminal_number =  :3  and\n                mms.output_filename =  :4";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.MMSUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setInt(2,store);
   __sJT_st.setInt(3,term);
   __sJT_st.setString(4,outputFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:288^7*/
    }
    catch(Exception e)
    {
      retVal = 0L;
    }
    return( retVal );  
  }
  
  protected boolean isLegacyFile( String filename )
  {
    boolean       retVal      = false;
    
    for( int i = 0; i < MMSLegacyFileExtensions.length; ++i )
    {
      if ( filename.endsWith( MMSLegacyFileExtensions[i] ) )
      {
        retVal = true;
        break;
      }
    }
    return( retVal );
  }
  
  protected boolean isResponseFile( String filename )
  {
    boolean   retVal    = false;
    try 
    {
      retVal = ( filename.endsWith(".dat.pgp") || 
                 filename.endsWith(".err.pgp") || 
                 filename.endsWith(".lst.pgp") || 
                 filename.endsWith(".dat") || 
                 filename.endsWith(".err") || 
                 filename.endsWith(".lst") ||
                 filename.endsWith("DataRpt.MMSO") || 
                 filename.endsWith("ErrorRpt.MMSO") || 
                 filename.endsWith("ListRpt.MMSO") );
    }
    catch(Exception e) 
    {
      logEntry("isResponseFile()", e.toString());
    }
    
    return( retVal );
  }
  
  protected void processIncoming() {
  
    try  {
    
      // if necessary create a directory for the response files
      File respDir = new File("response");
      if (!respDir.exists() || !respDir.isDirectory()) {
        respDir.mkdirs();
      }

      // create an sftp client object      
      Sftp sftp = getSftp(MesDefaults.getString(MesDefaults.DK_MMS_MES_HOST),
                          MesDefaults.getString(MesDefaults.DK_MMS_MES_USER),
                          MesDefaults.getString(MesDefaults.DK_MMS_MES_PASSWORD),
                          MesDefaults.getString(MesDefaults.DK_MMS_MES_INCOMING_PATH),
                          false);

      // process all files in the remote directory
      for (Enumeration e = sftp.getNameListing(); e.hasMoreElements();) {
      
        // get next filename, create local file to download to
        String filename = ""+e.nextElement();
        File localFile = new File(respDir,filename);
        
        // ignore remote directories (arc, etc.) and invalid file types
        if (sftp.isDirectory(filename) || !isResponseFile(filename)) {
          continue;
        }
        // make sure file doesn't already exist locally
        else if (!localFile.exists()) {
        
          // download file
          FileOutputStream localOut = new FileOutputStream(localFile);
          sftp.download(localOut,filename);
          
          // delete remote file
          // TODO: delete file after successful processing?
          sftp.deleteFile(filename);
          
          // process downloaded file
          processResponseFile(localFile.getName());
        }
        // skip files that already exist locally
        else {
          logEntry("processIncoming()",
            "Found FTP Batch response file to download but local file of" +
            " same name exists ('" + localFile.getName() + "').  File" +
            " skipped.");
        }
      }
    }
    catch(Exception e) {
    
      logEntry("processIncoming()", e.toString());
      log.error(e);
      e.printStackTrace();
    }
  }

  protected void processOutgoing( )
  {
    String              appSource       = null;
    ResultSetIterator   it              = null;
    BufferedWriter      out             = null;
    ResultSet           resultSet       = null;
    int                 testMode        = 0;

    try
    {
      testMode = (TestMode == true ? 1 : 0);

      for( int i = 0; i < ActiveBanks.length; ++i )
      {
        appSource = ActiveBanks[i].AppSource;
        out       = null;

        /*@lineinfo:generated-code*//*@lineinfo:411^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    mms.request_id                      as req_id,
//                      decode(appt.separate_mms_file,
//                             'Y', app.appsrctype_code,
//                             'MESR')                      as app_source,
//                      mms.app_seq_num                     as app_seq_num,
//                      -- not used?
//                      mms.app_date                        as app_date,
//                      mms.equip_model                     as equip_model,
//                      mms.equip_refurb                    as equip_refurb,
//                      mms.front_end                       as front_end,
//                      mms.location_number                 as location_number,
//                      mms.process_end_date                as process_end_date,
//                      mms.process_method                  as process_method,
//                      mms.process_response                as process_response,
//                      mms.process_start_date              as process_start_date,
//                      mms.process_status                  as process_status,
//                      mms.profile_generator_code          as profile_generator_code,
//                      mms.special_handling                as special_handling,
//                      mms.term_application_profgen        as term_application_profgen,
//                      mms.time_zone                       as time_zone,
//                      -- flat file fields
//                      'I'                                 as opmode,
//                      null                                as vnum,
//                      mms.bin                             as bin,
//                      mms.agent                           as agent,
//                      mms.chain                           as chain,
//                      mms.merch_number                    as merchant_number,
//                      mms.store_number                    as store,
//                      mms.terminal_number                 as term,
//                      upper(mms.business_name)            as dba_name,
//                      upper(replace(mms.business_address,',',' '))
//                                                          as addr1,
//                      upper(mms.business_city)            as city,
//                      upper(mms.business_state)           as state,
//                      substr(mms.business_zip,1,5)        as zip,
//                      mms.merch_phone                     as phone_number,
//                      upper(mms.service_level)            as service_level,
//                      upper(mms.attachment_code)          as attachment_code,
//                      lpad(mms.mcc,4,'0')                 as mcc,
//                      mms.term_application                as term_application,
//                      mms.disc                            as disc,
//                      mms.disc_ac                         as disc_accept,
//                      mms.amex                            as amex,
//                      mms.amex_ac                         as amex_accept,
//                      mms.split_dial                      as amex_split_dial,
//                      mms.dinr                            as dinr,
//                      mms.dinr_ac                         as dinr_accept,
//                      mms.jcb                             as jcb,
//                      mms.jcb_ac                          as jcb_accept,
//                      mms.term_voice_id                   as term_voice_id,
//                      mms.voice_auth_phone                as voice_auth_phone,
//                      mms.cdset                           as cdset,
//                      mms.project_num                     as project_number,
//                      mms.customer_service_num            as customer_service_number,
//                      upper(mms.dst)                      as dst,
//                      upper(mms.contact_name)             as contact_name,
//                      upper(mms.mcfs)                     as mcfs,
//                      mms.gift_payment_service            as gift_payment_service,
//                      mms.gift_accepted                   as gift_accepted,
//                      mms.gift_activation                 as gift_activation,
//                      nvl(mms.merchant_aba,'000000000')   as merchant_aba,
//                      mms.check_payment                   as check_payment,
//                      mms.check_merch_id                  as check_merch_id,
//                      mms.check_voice_id                  as check_voice_id,
//                      mms.check_auth_phone                as check_auth_phone,
//                      mms.comments_1                      as comments_1,
//                      mms.comments_2                      as comments_2,
//                      -- expanded fields for terminal setups
//                      mmse.manufacturer                   as manufacturer,
//                      mmse.term_model                     as term_model,
//                      mmse.printer_model                  as printer_model,
//                      mmse.printer_type                   as printer_type,
//                      nvl(mmse.fraud,' ')                 as fraud,
//                      mmse.receipt_truncation             as receipt_truncation,
//                      nvl(mmse.check_payment,' ')         as check_payment,
//                      mmse.check_mid                      as check_mid,
//                      mmse.check_voice                    as check_voice_id,
//                      mmse.check_auth_phone               as check_auth_phone,
//                      mmse.tip_option                     as tip_option,
//                      mmse.avs                            as avs,
//                      mmse.purchasing_card                as purchasing_card,
//                      mmse.invoice_number                 as invoice_number,
//                      mmse.auto_close_ind                 as auto_close_ind,
//                      mmse.auto_close_time                as auto_close_time,
//                      mmse.check_totals_ind               as check_totals_ind,
//                      mmse.force_settle_ind               as force_settle_ind,
//                      mmse.merchant_aba                   as merchant_aba,
//                      mmse.merch_settlement_agent         as merch_settlement_agent,
//                      mmse.reimbursement_attributes       as reimbursement_attributes,
//                      mmse.atm_receipt_sign_line          as atm_receipt_sign_line,
//                      mmse.cash_back                      as cash_back,
//                      mmse.atm_cancel_return              as atm_cancel_return,
//                      mmse.cash_back_limit                as cash_back_limit,
//                      mmse.ebt_trans_type                 as ebt_trans_type,
//                      mmse.ebt_fcs_id                     as ebt_fcs_id,
//                      mmse.parent_vnumber                 as parent_vnumber,
//                      mmse.pass_protect                   as password_protect,
//                      mmse.pinpad_available               as pinpad_available,
//                      mmse.pinpad_type                    as pinpad_type,
//                      mmse.primary_data_capture_phone     as primary_data_capture_phone,
//                      mmse.primary_dial_access            as primary_dial_access,
//                      mmse.reminder_msg                   as reminder_msg,
//                      mmse.reminder_msg_ind               as reminder_msg_ind,
//                      mmse.reminder_msg_time              as reminder_msg_time,
//                      mmse.secondary_data_capture_phone   as secondary_data_capture_phone,
//                      mmse.secondary_dial_access          as secondary_dial_access,
//                      mmse.settle_to_pc                   as settle_to_pc,
//                      mmse.sharing_groups                 as sharing_groups,
//                      mmse.cash_back_tip                  as cash_back_tip,
//                      mmse.auto_close_time2               as auto_close_time_2,
//                      mmse.auto_close_reports             as auto_close_reports,
//                      mmse.restaurant_processing          as restaurant_processing,
//                      mmse.srv_clk_processing             as srv_clk_processing,
//                      mmse.idle_prompt                    as idle_prompt,
//                      mmse.header_line4                   as header_line_4,
//                      mmse.header_line5                   as header_line_5,
//                      mmse.footer_line1                   as footer_line_1,
//                      mmse.footer_line2                   as footer_line_2,
//                      mmse.prim_auth_dial_access          as prim_auth_dial_access,
//                      mmse.sec_auth_dial_access           as sec_auth_dial_access,
//                      mmse.surcharge_ind                  as surcharge_ind,
//                      mmse.surcharge_amount               as surcharge_amount,
//                      mmse.primary_key1                   as primary_key_1,
//                      mmse.primary_key2                   as primary_key_2,
//                      mmse.time_difference                as time_difference,
//                      mmse.eci_type                       as eci_type,
//                      mmse.clerk_server_mode              as clerk_server_mode,
//                      mmse.overage_percentage             as overage_percentage,
//                      mmse.check_reader                   as check_reader,
//                      mmse.riid                           as riid,
//                      mmse.check_host                     as check_host,
//                      mmse.check_host_format              as check_host_format,
//                      mmse.amex_option                    as amex_option,
//                      mmse.merchant_id_amex               as merchant_id_amex,
//                      mmse.baud_rate_amexhost             as baud_rate_amexhost,
//                      mmse.amex_pabx                      as amex_pabx,
//                      mmse.debit_settle_num               as debit_settle_num,
//                      mmse.download_speed                 as download_speed,
//                      mmse.dial_speed                     as dial_speed,
//                      mmse.tm_options                     as tm_options,
//                      mmse.baud_rate_host1                as baud_rate_host1,
//                      mmse.card_present                   as card_present,
//                      mmse.force_dial                     as force_dial,
//                      mmse.dup_tran                       as dup_tran,
//                      mmse.inv_num_keys                   as inv_num_keys,
//                      mmse.inv_prompt                     as inv_prompt,
//                      mmse.auto_tip_percent               as auto_tip_percent,
//                      mmse.tip_time_sale                  as tip_time_sale,
//                      mmse.tip_assist                     as tip_assist,
//                      mmse.tip_assist_percent_1           as tip_assist_percent_1,
//                      mmse.prompt_for_clerk               as prompt_for_clerk,
//                      mmse.folio_guest_check_processing   as folio_guest_check_processing,
//                      mmse.folio_room_prompt              as folio_room_prompt,
//                      mmse.tip_assist_percent_2           as tip_assist_percent_2,
//                      mmse.tip_assist_percent_3           as tip_assist_percent_3,
//                      mmse.print_customer_copy            as print_customer_copy,
//                      nvl(mmse.reset_reference,'N')       as reset_reference,
//                      nvl(mmse.manager_password,'BOSS')   as manager_password,
//                      nvl(mmse.atm_debit_trans,'N')       as atm_debit_trans,
//                      nvl(mmse.atm_pin_entry_req,'N')     as atm_pin_entry_req,
//                      nvl(mmse.atm_purchase,'N')          as atm_purchase,
//                      nvl(mmse.prestigious_property,'N')  as prestigious_property
//            from      mms_stage_info          mms,
//                      mms_expanded_info       mmse,
//                      application             app,
//                      app_type                appt
//            where     mmse.request_id(+) = mms.request_id and
//                      (
//                        (
//                          :testMode = 0 and
//                          process_status in ( 'new', 'resubmitted' ) and
//                          process_method = 'FTP'
//                        ) or
//                        (
//                          :testMode = 1 and
//                          process_status = 'test' and
//                          process_method = 'TEST'
//                        )
//                      ) and
//                      app.app_seq_num = mms.app_seq_num and
//                      appt.app_type_code = app.app_type and
//                      decode(appt.separate_mms_file,
//                             'Y', app.appsrctype_code,
//                             'MESR') = :appSource
//            order by  app_source, mms.process_start_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mms.request_id                      as req_id,\n                    decode(appt.separate_mms_file,\n                           'Y', app.appsrctype_code,\n                           'MESR')                      as app_source,\n                    mms.app_seq_num                     as app_seq_num,\n                    -- not used:1\n                    mms.app_date                        as app_date,\n                    mms.equip_model                     as equip_model,\n                    mms.equip_refurb                    as equip_refurb,\n                    mms.front_end                       as front_end,\n                    mms.location_number                 as location_number,\n                    mms.process_end_date                as process_end_date,\n                    mms.process_method                  as process_method,\n                    mms.process_response                as process_response,\n                    mms.process_start_date              as process_start_date,\n                    mms.process_status                  as process_status,\n                    mms.profile_generator_code          as profile_generator_code,\n                    mms.special_handling                as special_handling,\n                    mms.term_application_profgen        as term_application_profgen,\n                    mms.time_zone                       as time_zone,\n                    -- flat file fields\n                    'I'                                 as opmode,\n                    null                                as vnum,\n                    mms.bin                             as bin,\n                    mms.agent                           as agent,\n                    mms.chain                           as chain,\n                    mms.merch_number                    as merchant_number,\n                    mms.store_number                    as store,\n                    mms.terminal_number                 as term,\n                    upper(mms.business_name)            as dba_name,\n                    upper(replace(mms.business_address,',',' '))\n                                                        as addr1,\n                    upper(mms.business_city)            as city,\n                    upper(mms.business_state)           as state,\n                    substr(mms.business_zip,1,5)        as zip,\n                    mms.merch_phone                     as phone_number,\n                    upper(mms.service_level)            as service_level,\n                    upper(mms.attachment_code)          as attachment_code,\n                    lpad(mms.mcc,4,'0')                 as mcc,\n                    mms.term_application                as term_application,\n                    mms.disc                            as disc,\n                    mms.disc_ac                         as disc_accept,\n                    mms.amex                            as amex,\n                    mms.amex_ac                         as amex_accept,\n                    mms.split_dial                      as amex_split_dial,\n                    mms.dinr                            as dinr,\n                    mms.dinr_ac                         as dinr_accept,\n                    mms.jcb                             as jcb,\n                    mms.jcb_ac                          as jcb_accept,\n                    mms.term_voice_id                   as term_voice_id,\n                    mms.voice_auth_phone                as voice_auth_phone,\n                    mms.cdset                           as cdset,\n                    mms.project_num                     as project_number,\n                    mms.customer_service_num            as customer_service_number,\n                    upper(mms.dst)                      as dst,\n                    upper(mms.contact_name)             as contact_name,\n                    upper(mms.mcfs)                     as mcfs,\n                    mms.gift_payment_service            as gift_payment_service,\n                    mms.gift_accepted                   as gift_accepted,\n                    mms.gift_activation                 as gift_activation,\n                    nvl(mms.merchant_aba,'000000000')   as merchant_aba,\n                    mms.check_payment                   as check_payment,\n                    mms.check_merch_id                  as check_merch_id,\n                    mms.check_voice_id                  as check_voice_id,\n                    mms.check_auth_phone                as check_auth_phone,\n                    mms.comments_1                      as comments_1,\n                    mms.comments_2                      as comments_2,\n                    -- expanded fields for terminal setups\n                    mmse.manufacturer                   as manufacturer,\n                    mmse.term_model                     as term_model,\n                    mmse.printer_model                  as printer_model,\n                    mmse.printer_type                   as printer_type,\n                    nvl(mmse.fraud,' ')                 as fraud,\n                    mmse.receipt_truncation             as receipt_truncation,\n                    nvl(mmse.check_payment,' ')         as check_payment,\n                    mmse.check_mid                      as check_mid,\n                    mmse.check_voice                    as check_voice_id,\n                    mmse.check_auth_phone               as check_auth_phone,\n                    mmse.tip_option                     as tip_option,\n                    mmse.avs                            as avs,\n                    mmse.purchasing_card                as purchasing_card,\n                    mmse.invoice_number                 as invoice_number,\n                    mmse.auto_close_ind                 as auto_close_ind,\n                    mmse.auto_close_time                as auto_close_time,\n                    mmse.check_totals_ind               as check_totals_ind,\n                    mmse.force_settle_ind               as force_settle_ind,\n                    mmse.merchant_aba                   as merchant_aba,\n                    mmse.merch_settlement_agent         as merch_settlement_agent,\n                    mmse.reimbursement_attributes       as reimbursement_attributes,\n                    mmse.atm_receipt_sign_line          as atm_receipt_sign_line,\n                    mmse.cash_back                      as cash_back,\n                    mmse.atm_cancel_return              as atm_cancel_return,\n                    mmse.cash_back_limit                as cash_back_limit,\n                    mmse.ebt_trans_type                 as ebt_trans_type,\n                    mmse.ebt_fcs_id                     as ebt_fcs_id,\n                    mmse.parent_vnumber                 as parent_vnumber,\n                    mmse.pass_protect                   as password_protect,\n                    mmse.pinpad_available               as pinpad_available,\n                    mmse.pinpad_type                    as pinpad_type,\n                    mmse.primary_data_capture_phone     as primary_data_capture_phone,\n                    mmse.primary_dial_access            as primary_dial_access,\n                    mmse.reminder_msg                   as reminder_msg,\n                    mmse.reminder_msg_ind               as reminder_msg_ind,\n                    mmse.reminder_msg_time              as reminder_msg_time,\n                    mmse.secondary_data_capture_phone   as secondary_data_capture_phone,\n                    mmse.secondary_dial_access          as secondary_dial_access,\n                    mmse.settle_to_pc                   as settle_to_pc,\n                    mmse.sharing_groups                 as sharing_groups,\n                    mmse.cash_back_tip                  as cash_back_tip,\n                    mmse.auto_close_time2               as auto_close_time_2,\n                    mmse.auto_close_reports             as auto_close_reports,\n                    mmse.restaurant_processing          as restaurant_processing,\n                    mmse.srv_clk_processing             as srv_clk_processing,\n                    mmse.idle_prompt                    as idle_prompt,\n                    mmse.header_line4                   as header_line_4,\n                    mmse.header_line5                   as header_line_5,\n                    mmse.footer_line1                   as footer_line_1,\n                    mmse.footer_line2                   as footer_line_2,\n                    mmse.prim_auth_dial_access          as prim_auth_dial_access,\n                    mmse.sec_auth_dial_access           as sec_auth_dial_access,\n                    mmse.surcharge_ind                  as surcharge_ind,\n                    mmse.surcharge_amount               as surcharge_amount,\n                    mmse.primary_key1                   as primary_key_1,\n                    mmse.primary_key2                   as primary_key_2,\n                    mmse.time_difference                as time_difference,\n                    mmse.eci_type                       as eci_type,\n                    mmse.clerk_server_mode              as clerk_server_mode,\n                    mmse.overage_percentage             as overage_percentage,\n                    mmse.check_reader                   as check_reader,\n                    mmse.riid                           as riid,\n                    mmse.check_host                     as check_host,\n                    mmse.check_host_format              as check_host_format,\n                    mmse.amex_option                    as amex_option,\n                    mmse.merchant_id_amex               as merchant_id_amex,\n                    mmse.baud_rate_amexhost             as baud_rate_amexhost,\n                    mmse.amex_pabx                      as amex_pabx,\n                    mmse.debit_settle_num               as debit_settle_num,\n                    mmse.download_speed                 as download_speed,\n                    mmse.dial_speed                     as dial_speed,\n                    mmse.tm_options                     as tm_options,\n                    mmse.baud_rate_host1                as baud_rate_host1,\n                    mmse.card_present                   as card_present,\n                    mmse.force_dial                     as force_dial,\n                    mmse.dup_tran                       as dup_tran,\n                    mmse.inv_num_keys                   as inv_num_keys,\n                    mmse.inv_prompt                     as inv_prompt,\n                    mmse.auto_tip_percent               as auto_tip_percent,\n                    mmse.tip_time_sale                  as tip_time_sale,\n                    mmse.tip_assist                     as tip_assist,\n                    mmse.tip_assist_percent_1           as tip_assist_percent_1,\n                    mmse.prompt_for_clerk               as prompt_for_clerk,\n                    mmse.folio_guest_check_processing   as folio_guest_check_processing,\n                    mmse.folio_room_prompt              as folio_room_prompt,\n                    mmse.tip_assist_percent_2           as tip_assist_percent_2,\n                    mmse.tip_assist_percent_3           as tip_assist_percent_3,\n                    mmse.print_customer_copy            as print_customer_copy,\n                    nvl(mmse.reset_reference,'N')       as reset_reference,\n                    nvl(mmse.manager_password,'BOSS')   as manager_password,\n                    nvl(mmse.atm_debit_trans,'N')       as atm_debit_trans,\n                    nvl(mmse.atm_pin_entry_req,'N')     as atm_pin_entry_req,\n                    nvl(mmse.atm_purchase,'N')          as atm_purchase,\n                    nvl(mmse.prestigious_property,'N')  as prestigious_property\n          from      mms_stage_info          mms,\n                    mms_expanded_info       mmse,\n                    application             app,\n                    app_type                appt\n          where     mmse.request_id(+) = mms.request_id and\n                    (\n                      (\n                         :2  = 0 and\n                        process_status in ( 'new', 'resubmitted' ) and\n                        process_method = 'FTP'\n                      ) or\n                      (\n                         :3  = 1 and\n                        process_status = 'test' and\n                        process_method = 'TEST'\n                      )\n                    ) and\n                    app.app_seq_num = mms.app_seq_num and\n                    appt.app_type_code = app.app_type and\n                    decode(appt.separate_mms_file,\n                           'Y', app.appsrctype_code,\n                           'MESR') =  :4 \n          order by  app_source, mms.process_start_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.MMSUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,testMode);
   __sJT_st.setInt(2,testMode);
   __sJT_st.setString(3,appSource);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.MMSUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:598^9*/
        resultSet = it.getResultSet();

        while ( resultSet.next() )
        {
          if ( out == null )
          {
            out = createNewRequestFile(ActiveBanks[i].Bin);
          }

          // build the detail record for this row
          buildMMSRecord(out,resultSet);
        }

        if ( out != null )
        {
          out.close();
          sendRequestFile( ActiveBanks[i] );
        }
      }
    }
    catch( Exception e )
    {
      logEntry("processOutgoing()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception ee) {}
    }
  }

  protected void processResponseDataErrorFile( String filename )
  {
    boolean             errFile           = filename.endsWith(".err");
    FlatFileRecord      ffd               = null;
    BufferedReader      in                = null;
    String              line              = null;
    String              merchantId        = null;
    String              outputFilename    = extractOutputFilename(filename);
    String              processResponse   = null;
    String              processStatus     = null;
    String              storeNumber       = null;
    String              termAppType       = null;
    String              termNumber        = null;
    String              vnum              = null;

    try
    {
      ffd = new FlatFileRecord( MesFlatFiles.DEF_TYPE_MMS_BASE );

      in = new BufferedReader( new FileReader( filename ) );

      while ( (line = in.readLine()) != null )
      {
        if( !line.startsWith("I|") )
        {
          continue;     // skip internal error text passed back by Vital
        }

        ffd.suck(line,"|");

        merchantId  = ffd.getFieldRawData("merchant_number");
        storeNumber = ffd.getFieldRawData("store");
        termNumber  = ffd.getFieldRawData("term");
        termAppType = ffd.getFieldRawData("term_application");

        if ( errFile )
        {
          processStatus   = mesConstants.ETPPR_FAIL;
          vnum            = "";
          processResponse = "";
        }
        else    // data file
        {
          vnum          = ffd.getFieldRawData("vnum").trim();
          processStatus = mesConstants.ETPPS_RECEIVED;

          if ( vnum == null || vnum.equals("") )
          {
            processResponse = mesConstants.ETPPR_PENDING;
          }
          else
          {
            processResponse = mesConstants.ETPPR_SUCCESS;
          }
        }

        if ( TestMode == true )
        {
          //@+
          StringBuffer        buffer    = new StringBuffer();
          FlatFileRecordField field     = ffd.getFirstField();

          while ( field != null )
          {
            buffer.setLength(0);
            buffer.append(field.getNumber());
            while( buffer.length() < 5 ) { buffer.append(" "); }
            buffer.append(field.getName());
            while( buffer.length() < 40 ) { buffer.append(" "); }
            buffer.append(field.getDisplayData());
            log.debug( buffer.toString() );

            field = ffd.getNextField();
          }

          int recCount;
          /*@lineinfo:generated-code*//*@lineinfo:705^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(request_id) 
//              from    mms_stage_info      mms
//              where   mms.merch_number = :merchantId and
//                      mms.store_number = :storeNumber and
//                      mms.terminal_number = :termNumber and
//                      mms.term_application = :termAppType and
//                      mms.output_filename = :outputFilename
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(request_id)  \n            from    mms_stage_info      mms\n            where   mms.merch_number =  :1  and\n                    mms.store_number =  :2  and\n                    mms.terminal_number =  :3  and\n                    mms.term_application =  :4  and\n                    mms.output_filename =  :5";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.MMSUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchantId);
   __sJT_st.setString(2,storeNumber);
   __sJT_st.setString(3,termNumber);
   __sJT_st.setString(4,termAppType);
   __sJT_st.setString(5,outputFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:714^11*/
          log.debug("found " + recCount + " matching requests");
          log.debug("  * process status  : " + processStatus );
          log.debug("  * process response: " + processResponse );
          log.debug("  * vnumber         : " + vnum );
          //@-
        }
        else    // update mms data
        {
          // extract the request id using the terminal id data
          long                requestId = 0L;

          /*@lineinfo:generated-code*//*@lineinfo:726^11*/

//  ************************************************************
//  #sql [Ctx] { select  request_id  
//              from    mms_stage_info      mms
//              where   mms.merch_number = :merchantId and
//                      mms.store_number = :storeNumber and
//                      mms.terminal_number = :termNumber and
//                      mms.term_application = :termAppType and
//                      mms.output_filename = :outputFilename
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  request_id   \n            from    mms_stage_info      mms\n            where   mms.merch_number =  :1  and\n                    mms.store_number =  :2  and\n                    mms.terminal_number =  :3  and\n                    mms.term_application =  :4  and\n                    mms.output_filename =  :5";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.MMSUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchantId);
   __sJT_st.setString(2,storeNumber);
   __sJT_st.setString(3,termNumber);
   __sJT_st.setString(4,termAppType);
   __sJT_st.setString(5,outputFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   requestId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:735^11*/

          if ( processResponse == mesConstants.ETPPR_SUCCESS )
          {
            com.mes.ops.MmsStageSetupBean.updateVnumberTables(requestId,vnum);
          }
          else
          {
            com.mes.queues.QueueTools.insertQueue(requestId,com.mes.constants.MesQueues.Q_MMS_ERROR);
          }

          /*@lineinfo:generated-code*//*@lineinfo:746^11*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info    mms
//              set     process_end_date   = sysdate,
//                      process_status     = :processStatus,
//                      process_response   = :processResponse,
//                      vnum               = :vnum
//              where   request_id = :requestId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mms_stage_info    mms\n            set     process_end_date   = sysdate,\n                    process_status     =  :1 ,\n                    process_response   =  :2 ,\n                    vnum               =  :3 \n            where   request_id =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.MMSUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,processStatus);
   __sJT_st.setString(2,processResponse);
   __sJT_st.setString(3,vnum);
   __sJT_st.setLong(4,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:754^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("processResponseDataErrorFile(" + filename + ")", e.toString());
    }
    finally
    {
      try{ in.close(); } catch( Exception e ) {}
    }
  }

  protected void processResponseErrorListFile( String filename )
  {
    String          errorDesc         = null;
    String          errorType         = null;
    FlatFileRecord  ffd               = null;
    BufferedReader  in                = null;
    int             index             = 0;
    String          line              = null;
    String          outputFilename    = extractOutputFilename(filename);
    long            requestId         = 0L;

    try
    {
      ffd = new FlatFileRecord( MesFlatFiles.DEF_TYPE_MMS_BASE );

      in = new BufferedReader( new FileReader( filename ) );

      while ( (line = in.readLine()) != null )
      {
        if ( line.startsWith("I|") )
        {
          ffd.suck(line,"|");

          String tid = String.valueOf(ffd.getFieldAsLong("merchant_number")) +
                       NumberFormatter.getPaddedInt(ffd.getFieldAsInt("store"),4) +
                       NumberFormatter.getPaddedInt(ffd.getFieldAsInt("term"),4);

          requestId = getRequestId(ffd.getFieldAsLong("merchant_number"),
                                   ffd.getFieldAsInt("store"),
                                   ffd.getFieldAsInt("term"),
                                   outputFilename);

          log.debug("tid " + tid + " resolved to request id " + requestId);//@

          // remove existing entries
          /*@lineinfo:generated-code*//*@lineinfo:803^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    mms_current_errors
//              where   request_id = :requestId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    mms_current_errors\n            where   request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.MMSUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,requestId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:808^11*/
        }

        // skip invalid requests
        if ( requestId == 0L )
        {
          continue;
        }

        if ( line.indexOf("Error") > 0 && line.indexOf("--") > 0 )
        {
          index     = line.indexOf("--");
          errorType = line.substring(0,index);
          errorDesc = line.substring(index+2);

          log.debug("errorType = " + errorType);//@
          log.debug("errorDesc = " + errorDesc);//@
          /*@lineinfo:generated-code*//*@lineinfo:825^11*/

//  ************************************************************
//  #sql [Ctx] { insert into mms_current_errors
//              (
//                request_id,
//                error_code,
//                error_type,
//                error_description
//              )
//              values
//              (
//                :requestId,
//                null,
//                :errorType,
//                :errorDesc
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mms_current_errors\n            (\n              request_id,\n              error_code,\n              error_type,\n              error_description\n            )\n            values\n            (\n               :1 ,\n              null,\n               :2 ,\n               :3 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.MMSUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,requestId);
   __sJT_st.setString(2,errorType);
   __sJT_st.setString(3,errorDesc);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:841^11*/
        }
      }
      /*@lineinfo:generated-code*//*@lineinfo:844^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:847^7*/
    }
    catch(Exception e)
    {
      logEntry("processResponseErrorListFile(" + filename + ")", e.toString());
    }
    finally
    {
      try{ in.close(); } catch( Exception e ) {}
    }
  }

  protected void processResponseFile( String filename )
  {
    String          dataFilename      = filename;
    File            f                 = null;
    PGPCmdLine      pgp               = new PGPCmdLine();
    StringBuffer    sb                = new StringBuffer(0);

    try
    {
      if ( filename.endsWith(".pgp") )
      {
        dataFilename = filename.substring(0,filename.lastIndexOf("."));
        try
        {
          pgp.decrypt("","",((new File (".")).getCanonicalPath() + File.separator + filename),sb);

          // delete pgp file
          f = new File(filename);
          f.delete();
        }
        catch( Exception e )
        {
        }
      }

      if ( dataFilename.endsWith(".dat") ||
           dataFilename.endsWith(".err") ||
           dataFilename.endsWith("DataRpt.MMSO") ||
           dataFilename.endsWith("ErrorRpt.MMSO") )
      {
        processResponseDataErrorFile( dataFilename );
      }
      else if ( dataFilename.endsWith(".err") ||
                dataFilename.endsWith("ListRpt.MMSO") )   // error text
      {
        processResponseErrorListFile( dataFilename );
      }

    }
    catch(Exception e)
    {
      logEntry("processResponseFile(" + filename + ")", e.toString());
    }
    finally
    {
    }
  }

  protected void sendRequestFile( MMSBank bankData )
  {
    StringBuffer      dataFilename      = new StringBuffer();
    StringBuffer      encFilenameBuffer = new StringBuffer();
    String            outgoingFilename  = null;
    PGPCmdLine        pgp               = null;
    String            pgpUser           = null;
    boolean           sendFlagFile      = false;
    boolean           transferMode      = false; // ASCII

    try
    {
      if ( FileRecordCount == 0 )
      {
        (new File(WorkFilename)).delete();  // delete empty file
      }
      else    // process file
      {
        if ( EncryptFile )
        {
          pgp = new PGPCmdLine();

          // attempt to encrypt the file
          try
          {
            pgpUser = MesDefaults.getString(MesDefaults.DK_MMS_PGP_USER);

            dataFilename.append( (new File (".")).getCanonicalPath() );
            dataFilename.append( File.separator );
            dataFilename.append( WorkFilename );

            log.debug("data filename: " + dataFilename.toString());//@
            log.debug("pgp user     : " + pgpUser);//@

            pgp.encrypt("",
                        pgpUser,
                        dataFilename.toString(),
                        encFilenameBuffer);

            log.debug( WorkFilename + " encrypted to " + encFilenameBuffer.toString() );
          }
          catch( Exception pgpe )
          {
            log.debug(pgpe.toString());
          }

          // encFilenameBuffer will contain the full path, use file object
          // to extract just the file name
          outgoingFilename = ((new File(encFilenameBuffer.toString())).getName());
          transferMode  = true; // set to Binary transfer mode
          sendFlagFile  = false;
        }
        else  // no encryption, use data filename, ascii mode, send flag file
        {
          outgoingFilename = WorkFilename;
          transferMode     = false; // set to ASCII transfer mode
          sendFlagFile     = true;
        }

        if ( !TestMode )
        {

          sendDataFile( outgoingFilename,
                        MesDefaults.getString(MesDefaults.DK_MMS_MES_HOST),
                        MesDefaults.getString(MesDefaults.DK_MMS_MES_USER),
                        MesDefaults.getString(MesDefaults.DK_MMS_MES_PASSWORD),
                        MesDefaults.getString(MesDefaults.DK_MMS_MES_OUTGOING_PATH),
                        transferMode,
                        sendFlagFile );

          archiveDailyFile(WorkFilename);

          if ( EncryptFile )
          {
            (new File(outgoingFilename)).delete();  // delete pgp file
          }
        }
      }
    }
    catch( Exception e )
    {
      logEntry("sendRequestFile()", e.toString());
    }
  }

  protected void setTestMode( boolean testMode )
  {
    TestMode = testMode;
  }

  public static void main( String[] args )
  {
    MMSUpload        test          = null;


      try {
          if (args.length > 0 && args[0].equals("testproperties")) {
              EventBase.printKeyListStatus(new String[]{
                      MesDefaults.MMS_VERSION,
                      MesDefaults.DK_MMS_MES_HOST,
                      MesDefaults.DK_MMS_MES_USER,
                      MesDefaults.DK_MMS_MES_PASSWORD,
                      MesDefaults.DK_MMS_MES_INCOMING_PATH,
                      MesDefaults.DK_MMS_PGP_USER,
                      MesDefaults.DK_MMS_MES_HOST,
                      MesDefaults.DK_MMS_MES_USER,
                      MesDefaults.DK_MMS_MES_PASSWORD,
                      MesDefaults.DK_MMS_MES_OUTGOING_PATH
              });
          }
      } catch (Exception e) {
          log.error(e.toString());
      }

    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");

      test = new MMSUpload();

      if ( args.length > 0 )
      {
        test.connect();
        test.setTestMode(true);

        if ( args[0].equals("procOut") )
        {
          test.processOutgoing();
        }
        else if ( args[0].equals("procResp") )
        {
          if ( args.length > 2 && args[2].equals("prod") )
          {
            log.debug("production mode enabled");//@
            test.setTestMode(false);
          }
          test.processResponseFile(args[1]);
        }
        else if ( args[0].equals("procIn") )
        {
          test.processIncoming();
        }
        test.cleanUp();
      }
      else
      {
        test.execute();
      }
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/