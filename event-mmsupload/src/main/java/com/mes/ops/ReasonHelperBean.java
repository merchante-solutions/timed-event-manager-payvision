/*@lineinfo:filename=ReasonHelperBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/ReasonHelperBean.sqlj $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/17/01 5:42p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Vector;
import sqlj.runtime.ResultSetIterator;


public class ReasonHelperBean extends com.mes.database.SQLJConnectionBase
{
  private HashMap       declineCodeShort        = new HashMap();
  private HashMap       declineCodeLong         = new HashMap();
  private HashMap       pendCodeShort           = new HashMap();
  private HashMap       pendCodeLong            = new HashMap();
  private Vector        reasons                 = null;
  private int           currentIdx              = -1;
  private int           setReasonType           = 0;
  private boolean       moreAvailable           = false;

  public ReasonHelperBean()
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:57^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    *
//          from      qreason
//          order by  qreason_code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    *\n        from      qreason\n        order by  qreason_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.ReasonHelperBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.ReasonHelperBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:62^7*/
        
      rs = it.getResultSet();
        
      while(rs.next())
      {
        switch(rs.getInt("qreason_type"))
        {
          case QueueConstants.Q_STATUS_DECLINED:
            declineCodeShort.put(rs.getString("qreason_code"),rs.getString("qreason_short_descr"));
            declineCodeLong.put(rs.getString("qreason_code"),rs.getString("qreason_long_descr"));
          break;
          case QueueConstants.Q_STATUS_PENDING:
            pendCodeShort.put(rs.getString("qreason_code"),rs.getString("qreason_short_descr"));
            pendCodeLong.put(rs.getString("qreason_code"),rs.getString("qreason_long_descr"));
          break;
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "ReasonHelperBean(constructor): " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public ReasonHelperBean(String connectString)
  {
    super(connectString);
  }

  private void resetVars()
  {
    reasons                 = null;
    currentIdx              = -1;
    setReasonType           = 0;
    moreAvailable           = false;
  }
  
  public boolean setReasons(int reasonType, String reasonString)
  {
    boolean result = true;
    if(isBlank(reasonString))
    {
      resetVars();
      return false;
    }
    
    try
    {
      //reasonType must be either declined or pending or forget about it.
      switch(reasonType)
      {
        case QueueConstants.Q_STATUS_DECLINED:
        case QueueConstants.Q_CREDIT_DECLINE:
        case QueueConstants.CREDIT_DECLINE:
          setReasonType = QueueConstants.Q_STATUS_DECLINED;
        break;
        case QueueConstants.Q_STATUS_PENDING:
        case QueueConstants.Q_CREDIT_PEND:
        case QueueConstants.CREDIT_PEND:
          setReasonType = QueueConstants.Q_STATUS_PENDING;
        break;
        default:
          resetVars();
          result = false;
        break;
      }

      if(!result)
      {
        return result;
      }

      //break up reason string delimited by ',' and place in vector
      String tempStr = "";
      reasons = new Vector();
      for(int x=0; x<reasonString.length(); ++x)
      {
        if(reasonString.charAt(x) != ',')
        {
          tempStr += reasonString.charAt(x);
        }
        else
        {
          reasons.add(tempStr);
          tempStr = "";
        }
      }
      //add the last code to vector
      reasons.add(tempStr);
    }
    catch(Exception e)
    {
      resetVars();
      result = false;
    }
    return result;
  }

  public boolean hasNext()
  {
    try
    {
      currentIdx++;
      if(currentIdx < reasons.size())
      {
        moreAvailable = true;
      }
      else
      {
        moreAvailable = false;
      }
    }
    catch(Exception e)
    {
      resetVars();
    }  
    return moreAvailable;
  }

  public String getNextCode()
  {
    String result = "";
    if(!moreAvailable)
    {
      return "";
    }
    try
    {
      result = isBlank((String)reasons.elementAt(currentIdx)) ? "" : (String)reasons.elementAt(currentIdx);
    }
    catch(Exception e)
    {
      result = "";
    }
    return result;
  }

  public String getNextShortDesc()
  {
    String result = "";
    if(!moreAvailable)
    {
      return "";
    }
    try
    {
      switch(setReasonType)
      {
        case QueueConstants.Q_STATUS_DECLINED:
        case QueueConstants.Q_CREDIT_DECLINE:
        case QueueConstants.CREDIT_DECLINE:
          result = isBlank((String)declineCodeShort.get((String)reasons.elementAt(currentIdx))) ? "" : (String)declineCodeShort.get((String)reasons.elementAt(currentIdx));
        break;
        case QueueConstants.Q_STATUS_PENDING:
        case QueueConstants.Q_CREDIT_PEND:
        case QueueConstants.CREDIT_PEND:
          result = isBlank((String)pendCodeShort.get((String)reasons.elementAt(currentIdx)))    ? "" : (String)pendCodeShort.get((String)reasons.elementAt(currentIdx));
        break;
      }
    }
    catch(Exception e)
    {
      result = "";
    }
    return result;
  }

  public String getNextLongDesc()
  {
    String result = "";
    if(!moreAvailable)
    {
      return "";
    }
    try
    {
      switch(setReasonType)
      {
        case QueueConstants.Q_STATUS_DECLINED:
        case QueueConstants.Q_CREDIT_DECLINE:
        case QueueConstants.CREDIT_DECLINE:
          result = isBlank((String)declineCodeLong.get((String)reasons.elementAt(currentIdx))) ? "" : (String)declineCodeLong.get((String)reasons.elementAt(currentIdx));
        break;
        case QueueConstants.Q_STATUS_PENDING:
        case QueueConstants.Q_CREDIT_PEND:
        case QueueConstants.CREDIT_PEND:
          result = isBlank((String)pendCodeLong.get((String)reasons.elementAt(currentIdx)))    ? "" : (String)pendCodeLong.get((String)reasons.elementAt(currentIdx));
        break;
      }
    }
    catch(Exception e)
    {
      result = "";
    }
    return result;
  }


  public boolean isSelected(String code)
  {
    boolean result = false;
    try
    {
      if(isBlank(code))
      {
        return false;
      }
      
      for(int x=0; x<reasons.size(); x++)
      {
        if(code.equals((String)reasons.elementAt(x)))
        {
          result = true;
          break;
        }
      }
    }
    catch(Exception e)
    {
      result = false;
    }
    return result;
  }  

  public String getSelected(String code)
  {
    String result = "";
    try
    {
      if(isBlank(code))
      {
        return "";
      }
      
      for(int x=0; x<reasons.size(); x++)
      {
        if(code.equals((String)reasons.elementAt(x)))
        {
          result = "selected";
          break;
        }
      }
    }
    catch(Exception e)
    {
      result = "";
    }
    return result;
  }  

  public String getChecked(String code)
  {
    String result = "";
    try
    {
      if(isBlank(code))
      {
        return "";
      }
      
      for(int x=0; x<reasons.size(); x++)
      {
        if(code.equals((String)reasons.elementAt(x)))
        {
          result = "checked";
          break;
        }
      }
    }
    catch(Exception e)
    {
      result = "";
    }
    return result;
  }  

  public String getLongDesc(int reasonType, String code)
  {
    String result = "";
    try
    {
      switch(reasonType)
      {
        case QueueConstants.Q_STATUS_DECLINED:
        case QueueConstants.Q_CREDIT_DECLINE:
        case QueueConstants.CREDIT_DECLINE:
          result = isBlank((String)declineCodeLong.get(code)) ? "" : (String)declineCodeLong.get(code);
        break;
        case QueueConstants.Q_STATUS_PENDING:
        case QueueConstants.Q_CREDIT_PEND:
        case QueueConstants.CREDIT_PEND:
          result = isBlank((String)pendCodeLong.get(code)) ? "" : (String)pendCodeLong.get(code);
        break;
      }
    }
    catch(Exception e)
    {
      result = "";
    }
    return result;
  }  

  public String getShortDesc(int reasonType, String code)
  {
    String result = "";
    try
    {
      switch(reasonType)
      {
        case QueueConstants.Q_STATUS_DECLINED:
        case QueueConstants.Q_CREDIT_DECLINE:
        case QueueConstants.CREDIT_DECLINE:
          result = isBlank((String)declineCodeShort.get(code)) ? "" : (String)declineCodeShort.get(code);
        break;
        case QueueConstants.Q_STATUS_PENDING:
        case QueueConstants.Q_CREDIT_PEND:
        case QueueConstants.CREDIT_PEND:
          result = isBlank((String)pendCodeShort.get(code))    ? "" : (String)pendCodeShort.get(code);
        break;
      }
    }
    catch(Exception e)
    {
      result = "";
    }
    return result;
  }  


/*  
  public boolean entryExists(long appSeqNum, String tableName)
  {
    long                  count   = 0L;
    PreparedStatement     ps      = null;
    ResultSet             rs      = null;
    StringBuffer          qs      = new StringBuffer("");
    
    try
    {
      qs.append("select count(app_seq_num) as count ");
      qs.append("from ");
      qs.append(tableName);
      qs.append(" where app_seq_num = ?");
      
      ps = Ctx.getConnection().prepareStatement(qs.toString());
      ps.setLong(1, appSeqNum);
      
      rs = ps.executeQuery();
      
      if(rs.next())
      {
        count = rs.getInt("count");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::entryExists()", appSeqNum + ", " + tableName + ": " + e.toString());
    }
    
    return (count > 0);
  }

  public boolean insertCreditQueue(long appSeqNum)
  {
    boolean rc        = false;
    
    try
    {
      if(! entryExists(appSeqNum, "app_queue_credit"))
      {
        #sql [Ctx]
        {
          insert into app_queue_credit
          (
            app_seq_num,
            app_source_type
          )
          values
          (
            :appSeqNum,
            'USER'
          )
        };
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName() + "::insertCreditQueue()", appSeqNum + ": " + e.toString());
    }
    
    return rc;
  }
*/

  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }

}/*@lineinfo:generated-code*/