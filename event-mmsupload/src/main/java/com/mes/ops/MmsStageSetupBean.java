/*@lineinfo:filename=MmsStageSetupBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/MmsStageSetupBean.sqlj $

  Description:  
  
    AccountLookup

    Support bean for account lookup page in account maintenance.
    
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/28/04 9:56a $
  Version            : $Revision: 40 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.equipment.TermAppProfGenRelationship;
import com.mes.equipment.profile.EquipProfileGeneratorDictator;
import com.mes.queues.QueueTools;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;


public class MmsStageSetupBean extends SQLJConnectionBase
{

  public  static final String NOFILE                      = "NOFILE";
  public  static final String CARD_AC_AUTH_ONLY           = "0";
  public  static final String CARD_AC_EDC                 = "1";

  public  static final String YES                         = "Y";
  public  static final String NO                          = "N";

  //default values
  public  static final String BIN_NUMBER_CBT                    = "413747";
  public  static final String BIN_NUMBER_NBSC                   = "448203";
  public  static final String BIN_NUMBER_DEFAULT                = "433239";
  public  static final String CHAIN_NUMBER_NSI                  = "600019";
  public  static final String CHAIN_NUMBER_VERISIGN             = "600055";
  public  static final String CHAIN_NUMBER_DEFAULT              = "000000";
  public  static final String AGENT_NUMBER_DEFAULT              = "000000";
  public  static final String LOCATION_NUMBER_DEFAULT           = "00001";
  public  static final String STORE_NUMBER_DEFAULT              = "0001";
  public  static final String TERMINAL_NUMBER_DEFAULT           = "0001";
  public  static final String SERVICE_LEVEL_DEFAULT             = "RNO";
  public  static final String ATTACH_CODE_DEFAULT               = "IT";
  public  static final String VOICE_AUTH_PHONE_DEFAULT          = "18002914840";
  public  static final String CDSET_DEFAULT                     = "00";
  public  static final String NO_TERMINAL_SELECTED              = "na";
  public  static final String AUTO_FLAG_DEFAULT                 = "Y";
  public  static final String CUSTOMER_SERVICE_PHONE_DEFAULT    = "8882882692";
  public  static final String TERM_APP_DEFAULT                  = mesConstants.MMS_STAGE_BUILD_TERM_APP;

  private static final int    FIELD_LENGTH_BIN                  = 6;
  private static final int    FIELD_LENGTH_AGENT                = 6;
  private static final int    FIELD_LENGTH_CHAIN                = 6;
  private static final int    FIELD_LENGTH_MERCHNUM             = 12;
  private static final int    FIELD_LENGTH_STORENUM             = 4;
  private static final int    FIELD_LENGTH_TERMINALNUM          = 4;
  private static final int    FIELD_LENGTH_MERCHNAME            = 23;
  private static final int    FIELD_LENGTH_ADDRESS              = 23;
  private static final int    FIELD_LENGTH_CITY                 = 13;
  private static final int    FIELD_LENGTH_STATE                = 2;
  private static final int    FIELD_LENGTH_ZIP                  = 5;
  private static final int    FIELD_LENGTH_MERCHPHONE           = 12;
  private static final int    FIELD_LENGTH_SERVICELEVEL         = 3;
  private static final int    FIELD_LENGTH_ATTACHCODE           = 2;
  private static final int    FIELD_LENGTH_MCC                  = 4;
  private static final int    FIELD_LENGTH_TERMAPP              = 7;
  private static final int    FIELD_LENGTH_DISCNUM              = 15;
  private static final int    FIELD_LENGTH_DISCAC               = 1;
  private static final int    FIELD_LENGTH_AMEXNUM              = 10;
  private static final int    FIELD_LENGTH_AMEXAC               = 1;
  private static final int    FIELD_LENGTH_AMEXSPLIT            = 1;
  private static final int    FIELD_LENGTH_DINERSNUM            = 10;
  private static final int    FIELD_LENGTH_DINERSAC             = 1;
  private static final int    FIELD_LENGTH_JCBNUM               = 10;
  private static final int    FIELD_LENGTH_JCBAC                = 1;
  private static final int    FIELD_LENGTH_TERMVOICEID          = 16;
  private static final int    FIELD_LENGTH_VOICEAUTHPHONE       = 12;
  private static final int    FIELD_LENGTH_CDSET                = 2;
  private static final int    FIELD_LENGTH_PROJECTNUM           = 4;
  private static final int    FIELD_LENGTH_CUSTOMERSERVICENUM   = 10;
  private static final int    FIELD_LENGTH_DST                  = 1;
  private static final int    FIELD_LENGTH_CONTACTNAME          = 20;
  private static final int    FIELD_LENGTH_MCFS                 = 15;



  private long                primaryKey                  = 0L;
  private long                requestId                   = 0L;

  private int                 appType                     = -1;
  private String              appTypeDesc                 = "";
  private String              appTypeNonDeploy            = "";
  private String              includeFileName             = NOFILE;
    
  private ResultSetIterator   it                          = null;

  private boolean             submitted                   = false;
  private boolean             reSubmitted                 = false;
  private boolean             manSubmitted                = false;
  private boolean             allowManUpdate              = false;
  private boolean             expandedFields              = false;
  private boolean             okToGetGets                 = true;

  private boolean             queuedForTrans              = false;
  private boolean             errorStatus                 = false;

  private boolean             showTimestamps              = false;


  private Timestamp           today                       = null;

  private String              vNum                        = "";
  private String              binNum                      = "";
  private String              agentNum                    = "";
  private String              chainNum                    = "";
  private String              merchNum                    = "";
  private String              storeNum                    = "";
  private String              terminalNum                 = "";
  private String              equipModel                  = "";
  private String              equipModelRefurb            = "";
  
  private String              merchName                   = "";
  private String              merchAddress                = "";
  private String              merchCity                   = "";
  private String              merchState                  = "";
  private String              merchZip                    = "";
  private String              merchPhone                  = "";
  private String              serviceLevel                = "";
  private String              attachCode                  = "";
  private String              mcc                         = "";
  private String              termApp                     = "";
  private String              termAppProfGen              = "";
  private String              discNum                     = "";
  private String              discAc                      = "";
  private String              amexNum                     = "";
  private String              amexAc                      = "";
  private String              amexSplit                   = "";
  private String              dinersNum                   = "";
  private String              dinersAc                    = "";
  private String              jcbNum                      = "";
  private String              jcbAc                       = "";
  private String              termVoiceId                 = "";
  private String              voiceAuthPhone              = "";
  private String              cdSet                       = "";
  private String              timeZone                    = "";
  private String              locationNum                 = "";
  
  // Certegy Check and Valutec
  private boolean             CheckAccepted               = false;
  private String              CheckProvider               = "";
  private String              CheckMerchId                = "";
  private String              CheckTermId                 = "";
  private boolean             ValutecAccepted             = false;
  private String              ValutecMerchId              = "";
  private String              ValutecTermId               = "";

  private String              dst                         = "";
  private String              contactName                 = "";
  private String              projectNum                  = "";
  private String              mcfs                        = "";
  private String              customerServiceNum          = "";


  private String              transmissionMethod          = "";
  private String              transmissionStatus          = "";
  private String              transmissionResponse        = "";

  private String              dateTimeOfTransmission      = "";
  private String              dateTimeOfResponse          = "";

  private String              specialHandling             = "";

  private int                 profileGeneratorCode        = mesConstants.PROFGEN_UNDEFINED;
    // the entity [company] slated to generate the equipment profile (not material to the actual mms request)

  //variable for updating status info manually
  private String              manVnum                     = "";
  private String              manStatus                   = "";
  private String              manResponse                 = "";

  private String              errorDescriptions           = "";

  public  Vector              states                      = new Vector();
  public  Vector              errors                      = new Vector();
  public  Vector              msgs                        = new Vector();

  public  Vector              sicCodes                    = new Vector();
  public  Vector              sicCodeDesc                 = new Vector();

  public  Vector              attachCodes                 = new Vector();
  public  Vector              attachCodeDesc              = new Vector();

  public  Vector              binNums                     = new Vector();

  public  Vector              termApps                    = new Vector();
  public  Vector              termAppDesc                 = new Vector();

  public  Vector              cardAcCodes                 = new Vector();
  public  Vector              cardAcDesc                  = new Vector();

  public  Vector              status                      = new Vector();
  public  Vector              response                    = new Vector();

  public  Vector              timeZoneCodes               = new Vector();
  public  Vector              timeZoneDesc                = new Vector();

  public  Vector              equipModelDesc              = new Vector();
  public  Vector              equipModelDescCode          = new Vector();

  public  Vector              profGenNames                = new Vector();
  public  Vector              profGenCodes                = new Vector();

  
  public String[][] nv_eciType = 
  {
     {"Select",""}
    ,{"Off","Off"}
    ,{"Prompt","Prompt"}
    ,{"Secure","Secure"}
  };
  
  public String[][] nv_clerkServerMode = 
  {
     {"Select",""}
    ,{"None","None"}
    ,{"Logon","Logon"}
    ,{"Prompt","Prompt"}
  };

  public String[][] nv_amexOption = 
  {
     {"Select",""}
    ,{"None","None"}
    ,{"Split","Split"}
    ,{"PIP","PIP"}
  };

  public String[][] nv_baudRateHost1 = 
  {
     {"Select",""}
    ,{"300","300"}
    ,{"600","600"}
    ,{"1200","1200"}
    ,{"2400","2400"}
  };

  public String[][] nv_baudRateAmexHost = 
  {
     {"Select",""}
    ,{"300","300"}
    ,{"600","600"}
    ,{"1200","1200"}
    ,{"2400","2400"}
  };

  public MmsStageSetupBean()
  {
    fillDropDowns();

    //when this method is called all the records in the mms_stage_info table that
    //have been in process for more than 2 days are changed to a response of failed
    //and a status of no response.
    
    //updateAllFails();
  }

  public String getTitle()
  {
    // based on the resident mms setup queue of the this mms request
    String title;
    switch(QueueTools.getCurrentQueueType(requestId,MesQueues.Q_ITEM_TYPE_MMS)) {
      default:
      case MesQueues.Q_MMS_NEW:
        title = "MMS Screen";
        break;
      case MesQueues.Q_MMS_VCTM_NEW:
        title = "VC/TM - MMS Screen";
        break;
    }

    return title;
  }

  private void updateAllFails()
  {
    try
    {
      connect();
     
      /*@lineinfo:generated-code*//*@lineinfo:331^7*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info
//  
//          set     process_response   = :mesConstants.ETPPR_FAIL,
//                  process_status     = :mesConstants.ETPPS_NO_RESPONSE
//  
//          where   process_start_date is not null and process_end_date is null and (months_between(:today,process_start_date) > .07)
//  
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mms_stage_info\n\n        set     process_response   =  :1 ,\n                process_status     =  :2 \n\n        where   process_start_date is not null and process_end_date is null and (months_between( :3 ,process_start_date) > .07)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,mesConstants.ETPPR_FAIL);
   __sJT_st.setString(2,mesConstants.ETPPS_NO_RESPONSE);
   __sJT_st.setTimestamp(3,today);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:341^7*/
    }
    catch(Exception e)
    {
      logEntry("updateAllFails()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public String getTermAppDescriptor()
  {
    StringBuffer sb = new StringBuffer();

    if(profileGeneratorCode == mesConstants.PROFGEN_MMS) {
      sb.append(termAppProfGen);
    } else {
      sb.append("MMS: '");
      sb.append(termApp);
      sb.append("' - ");
      sb.append(EquipProfileGeneratorDictator.getEquipmentProfileGeneratorName(profileGeneratorCode));
      sb.append(": '");
      sb.append(termAppProfGen);
      sb.append("'");
    }

    return sb.toString();
  }
  
  /*
  ** METHOD fillDropDowns
  **
  ** Fills the drop down boxes
  */  
  public void fillDropDowns()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:386^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   countrystate_code 
//          from     countrystate
//          order by countrystate_code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   countrystate_code \n        from     countrystate\n        order by countrystate_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.MmsStageSetupBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:391^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        states.add(rs.getString("countrystate_code"));
      }
 
      it.close();
    

      /*@lineinfo:generated-code*//*@lineinfo:403^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   sic_code,merchant_type 
//          from     sic_codes
//          order by sic_code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   sic_code,merchant_type \n        from     sic_codes\n        order by sic_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.MmsStageSetupBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:408^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        sicCodes.add(rs.getString("sic_code"));
        sicCodeDesc.add(rs.getString("merchant_type"));
      }
 
      it.close();


      /*@lineinfo:generated-code*//*@lineinfo:421^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   attachment_code,
//                   attachment_code_desc 
//          from     mms_attachment_codes
//          order by attachment_code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   attachment_code,\n                 attachment_code_desc \n        from     mms_attachment_codes\n        order by attachment_code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.MmsStageSetupBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:427^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        attachCodes.add(rs.getString("attachment_code"));
        attachCodeDesc.add(rs.getString("attachment_code_desc"));
      }
 
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:439^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   code,
//                   description 
//          from     time_zones
//          order by code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   code,\n                 description \n        from     time_zones\n        order by code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.MmsStageSetupBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:445^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        timeZoneCodes.add(rs.getString("code"));
        timeZoneDesc.add(rs.getString("description"));
      }
 
      it.close();


      /*@lineinfo:generated-code*//*@lineinfo:458^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   bin_number
//          from     mms_bin_numbers
//          order by sort_order asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   bin_number\n        from     mms_bin_numbers\n        order by sort_order asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.MmsStageSetupBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:463^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        binNums.add(rs.getString("bin_number"));
      }
 
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:474^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   terminal_application,
//                   terminal_application_desc
//          from     mms_terminal_applications
//          order by terminal_application asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   terminal_application,\n                 terminal_application_desc\n        from     mms_terminal_applications\n        order by terminal_application asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.MmsStageSetupBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:480^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        termApps.add(rs.getString("terminal_application"));
        termAppDesc.add(rs.getString("terminal_application_desc"));
      }
 
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:492^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   equip_model,
//                   equip_descriptor
//          from     equipment
//          where    equiptype_code in (1,5,6)
//          order by equip_descriptor asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   equip_model,\n                 equip_descriptor\n        from     equipment\n        where    equiptype_code in (1,5,6)\n        order by equip_descriptor asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.MmsStageSetupBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:499^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        equipModelDesc.add(rs.getString("equip_descriptor"));
        equipModelDescCode.add(rs.getString("equip_model"));
      }
 
      it.close();

      int pgc_exclude   = mesConstants.PROFGEN_UNDEFINED;
      int pgc_exclude2  = mesConstants.PROFGEN_VCTM;

      /*@lineinfo:generated-code*//*@lineinfo:514^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   code,
//                   name
//          from     equip_profile_generators
//          where    code <> :pgc_exclude
//                   and code <> :pgc_exclude2
//          order by code asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   code,\n                 name\n        from     equip_profile_generators\n        where    code <>  :1 \n                 and code <>  :2 \n        order by code asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,pgc_exclude);
   __sJT_st.setInt(2,pgc_exclude2);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:522^7*/
    
      rs = it.getResultSet();

      while(rs.next())
      {
        profGenNames.add(rs.getString("name"));
        profGenCodes.add(rs.getString("code"));
      }

      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:534^7*/

//  ************************************************************
//  #sql [Ctx] { select    sysdate 
//          from      dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    sysdate  \n        from      dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.MmsStageSetupBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   today = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:538^7*/

      cardAcCodes.add(CARD_AC_AUTH_ONLY);
      cardAcDesc.add("Auth Only");
      cardAcCodes.add(CARD_AC_EDC);
      cardAcDesc.add("EDC");

    }
    catch(Exception e)
    {
      logEntry("fillDropDowns()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    //statuses
    status.add(mesConstants.ETPPS_NEW);
    status.add(mesConstants.ETPPS_INPROCESS);
    status.add(mesConstants.ETPPS_RECEIVED);
    status.add(mesConstants.ETPPS_RESUBMITTED);
    status.add(mesConstants.ETPPS_NO_RESPONSE);

    //RESPONSES
    response.add(mesConstants.ETPPR_PENDING);
    response.add(mesConstants.ETPPR_SUCCESS);
    response.add(mesConstants.ETPPR_FAIL);

  }
  

  public void setDefaults()
  {
    
    switch(this.appType)
    {
      case mesConstants.APP_TYPE_CBT:
      case mesConstants.APP_TYPE_CBT_NEW:
        binNum           = isBlank(binNum)          ?   BIN_NUMBER_CBT            : binNum;
      break;
      case mesConstants.APP_TYPE_NBSC:
        binNum           = isBlank(binNum)          ?   BIN_NUMBER_NBSC           : binNum;
      break;
      default:
        binNum           = isBlank(binNum)          ?   BIN_NUMBER_DEFAULT        : binNum;
      break;
    }

    switch(this.appType)
    {
      case mesConstants.APP_TYPE_VERISIGN:
        chainNum         = isBlank(chainNum)        ?   CHAIN_NUMBER_VERISIGN     : chainNum;
      break;
      case mesConstants.APP_TYPE_NSI:
        chainNum         = isBlank(chainNum)        ?   CHAIN_NUMBER_NSI          : chainNum;
      break;
      default:
        chainNum         = isBlank(chainNum)        ?   CHAIN_NUMBER_DEFAULT      : chainNum;
      break;                                                                  
    }

    termApp              = isBlank(termApp)             ?   TERM_APP_DEFAULT : termApp;
    termAppProfGen       = isBlank(termAppProfGen)      ?   TermAppProfGenRelationship.getDefaultProfGenTermApp(profileGeneratorCode) : termAppProfGen;

    agentNum             = isBlank(agentNum)            ?   AGENT_NUMBER_DEFAULT            : agentNum;
    storeNum             = isBlank(storeNum)            ?   STORE_NUMBER_DEFAULT            : storeNum;
    locationNum          = isBlank(locationNum)         ?   LOCATION_NUMBER_DEFAULT         : locationNum;
    terminalNum          = isBlank(terminalNum)         ?   TERMINAL_NUMBER_DEFAULT         : terminalNum;
    serviceLevel         = isBlank(serviceLevel)        ?   SERVICE_LEVEL_DEFAULT           : serviceLevel;
    attachCode           = isBlank(attachCode)          ?   ATTACH_CODE_DEFAULT             : attachCode;
    voiceAuthPhone       = isBlank(voiceAuthPhone)      ?   VOICE_AUTH_PHONE_DEFAULT        : voiceAuthPhone;
    cdSet                = isBlank(cdSet)               ?   CDSET_DEFAULT                   : cdSet;
    specialHandling      = isBlank(specialHandling)     ?   NO                              : specialHandling;
    customerServiceNum   = isBlank(customerServiceNum)  ?   CUSTOMER_SERVICE_PHONE_DEFAULT  : customerServiceNum;
  }

  public long startNewApplication()
  {
    return setReqId();
  }

  public long figureOutPrimaryKey(long reqId)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    long                result      = 0L;

    try
    {
      
      connect();
   
      /*@lineinfo:generated-code*//*@lineinfo:631^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num
//          from    mms_stage_info
//          where   request_id   = :reqId 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num\n        from    mms_stage_info\n        where   request_id   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,reqId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:636^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        result = rs.getLong("app_seq_num");
      }
 
      it.close();

    }
    catch(Exception e)
    {
      logEntry("figureOutPrimaryKey()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  
  public void getData(long pk, long id)
  {
    getAppTypeInfo(pk);
    getAppContent (pk,  id);
  }


  private void getAppTypeInfo(long pk)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;

    try
    {
      System.out.println("getting app type info");
      connect();
   
      /*@lineinfo:generated-code*//*@lineinfo:678^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app.app_type, 
//                  at.app_description,
//                  at.non_deploy_app
//          from    application app, 
//                  app_type at
//          where   app.app_seq_num   = :pk and
//                  app.app_type      = at.app_type_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app.app_type, \n                at.app_description,\n                at.non_deploy_app\n        from    application app, \n                app_type at\n        where   app.app_seq_num   =  :1  and\n                app.app_type      = at.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:687^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        appType           = rs.getInt("app_type");
        System.out.println("  appType = " + appType);
        appTypeDesc       = rs.getString("app_description");
        appTypeNonDeploy  = isBlank(rs.getString("non_deploy_app")) ? "N" : rs.getString("non_deploy_app");
      }
 
      it.close();

    }
    catch(Exception e)
    {
      logEntry("getAppTypeInfo()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public int getAppType(HttpServletRequest request)
  {
    int               result  = -1;
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    try
    {
      connect();
      
      // find the app seq num from input parameters
      if(HttpHelper.getInt(request, "primaryKey", -1) != -1)
      {
        /*@lineinfo:generated-code*//*@lineinfo:725^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  app.app_type  app_type
//            from    application app
//            where   app_seq_num = :HttpHelper.getInt(request, "primaryKey")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_791 = HttpHelper.getInt(request, "primaryKey");
  try {
   String theSqlTS = "select  app.app_type  app_type\n          from    application app\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_791);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:730^9*/
      }
      else if(HttpHelper.getInt(request, "requestId", -1) != -1)
      {
        /*@lineinfo:generated-code*//*@lineinfo:734^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  app.app_type  app_type
//            from    application app,
//                    mms_stage_info msi
//            where   msi.request_id = :HttpHelper.getInt(request, "requestId") and
//                    msi.app_seq_num = app.app_seq_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_792 = HttpHelper.getInt(request, "requestId");
  try {
   String theSqlTS = "select  app.app_type  app_type\n          from    application app,\n                  mms_stage_info msi\n          where   msi.request_id =  :1  and\n                  msi.app_seq_num = app.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_792);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:741^9*/
      }
      else if(HttpHelper.getInt(request, "merchant", -1) != -1)
      {
        /*@lineinfo:generated-code*//*@lineinfo:745^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  app.app_type  app_type
//            from    application app,
//                    merchant mr
//            where   mr.merch_number = :HttpHelper.getInt(request, "merchant") and
//                    mr.app_seq_num = app.app_seq_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_793 = HttpHelper.getInt(request, "merchant");
  try {
   String theSqlTS = "select  app.app_type  app_type\n          from    application app,\n                  merchant mr\n          where   mr.merch_number =  :1  and\n                  mr.app_seq_num = app.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_793);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:752^9*/
      }
      else if(HttpHelper.getInt(request, "id", -1) != -1)
      {
        /*@lineinfo:generated-code*//*@lineinfo:756^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  app.app_type  app_type
//            from    application app,
//                    mms_stage_info msi
//            where   msi.request_id = :HttpHelper.getInt(request, "id") and
//                    msi.app_seq_num = app.app_seq_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_794 = HttpHelper.getInt(request, "id");
  try {
   String theSqlTS = "select  app.app_type  app_type\n          from    application app,\n                  mms_stage_info msi\n          where   msi.request_id =  :1  and\n                  msi.app_seq_num = app.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_794);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:763^9*/
      }
      
      if(it != null)
      {
        rs = it.getResultSet();
        if(rs.next())
        {
          result = rs.getInt("app_type");
        }
        
        rs.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      System.out.println("getAppType(request): " + e.toString());
      logEntry("getAppType(request)", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }

  /*
  ** METHOD public void truncateAppData()
  **
  ** Truncates relevant OLA related data members to the length dictated by the 
  ** associated field length constant.
  */
  private void truncateAppData()
  {
    if(merchName.length()>FIELD_LENGTH_MERCHNAME)
      merchName       = merchName.substring(0,FIELD_LENGTH_MERCHNAME);
    if(merchAddress.length()>FIELD_LENGTH_ADDRESS)
      merchAddress    = merchAddress.substring(0,FIELD_LENGTH_ADDRESS);
    if(merchCity.length()>FIELD_LENGTH_CITY)
      merchCity       = merchCity.substring(0,FIELD_LENGTH_CITY);
    if(merchZip.length()>FIELD_LENGTH_ZIP)
      merchZip        = merchZip.substring(0,FIELD_LENGTH_ZIP);
    if(contactName.length()>FIELD_LENGTH_CONTACTNAME)
      contactName     = contactName.substring(0,FIELD_LENGTH_CONTACTNAME);
  }

  /*
  ** METHOD public void getAppContent()
  **
  */
  public void getAppContent(long pk, long id)
  {
    
    String    dateOfTransmission = "";
    String    timeOfTransmission = "";
    String    dateOfResponse     = "";
    String    timeOfResponse     = "";

    try
    {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:829^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//            from    mms_stage_info
//            where   request_id = :id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n          from    mms_stage_info\n          where   request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:834^9*/
        
      
        ResultSet rs = it.getResultSet();
 
        if(rs.next())
        {
          queuedForTrans             = true;

          this.primaryKey            = rs.getLong("APP_SEQ_NUM");
          this.requestId             = rs.getLong("request_id");
          this.transmissionMethod    = isBlank(rs.getString("process_method"))      ? ""  : rs.getString("process_method");
          this.transmissionStatus    = isBlank(rs.getString("process_status"))      ? ""  : rs.getString("process_status");
          this.transmissionResponse  = isBlank(rs.getString("process_response"))    ? ""  : rs.getString("process_response");
          this.specialHandling       = isBlank(rs.getString("SPECIAL_HANDLING"))    ? "N" : rs.getString("SPECIAL_HANDLING");

          if((transmissionResponse.equals(mesConstants.ETPPR_PENDING) || transmissionResponse.equals(mesConstants.ETPPR_FAIL)) && !transmissionStatus.equals(mesConstants.ETPPS_RESUBMITTED))
          {
            this.errorStatus = true;
          }

          this.vNum                  = isBlank(rs.getString("VNUM"))                  ? ""  : rs.getString("VNUM");
          
          this.binNum                = isBlank(rs.getString("BIN"))                   ? ""  : rs.getString("BIN");
          this.agentNum              = isBlank(rs.getString("AGENT"))                 ? ""  : rs.getString("AGENT");
          this.chainNum              = isBlank(rs.getString("CHAIN"))                 ? ""  : rs.getString("CHAIN");
          this.merchNum              = isBlank(rs.getString("MERCH_NUMBER"))          ? ""  : rs.getString("MERCH_NUMBER");
          this.locationNum           = isBlank(rs.getString("LOCATION_NUMBER"))       ? ""  : rs.getString("LOCATION_NUMBER");
          this.storeNum              = isBlank(rs.getString("STORE_NUMBER"))          ? ""  : rs.getString("STORE_NUMBER");
          this.terminalNum           = isBlank(rs.getString("TERMINAL_NUMBER"))       ? ""  : rs.getString("TERMINAL_NUMBER");
          this.equipModel            = isBlank(rs.getString("EQUIP_MODEL"))           ? ""  : rs.getString("EQUIP_MODEL");
          this.equipModelRefurb      = isBlank(rs.getString("EQUIP_REFURB"))          ? ""  : rs.getString("EQUIP_REFURB");
          this.merchName             = isBlank(rs.getString("BUSINESS_NAME"))         ? ""  : rs.getString("BUSINESS_NAME");
          this.merchAddress          = isBlank(rs.getString("BUSINESS_ADDRESS"))      ? ""  : rs.getString("BUSINESS_ADDRESS");
          this.merchCity             = isBlank(rs.getString("BUSINESS_CITY"))         ? ""  : rs.getString("BUSINESS_CITY");
          this.merchState            = isBlank(rs.getString("BUSINESS_STATE"))        ? ""  : rs.getString("BUSINESS_STATE");
          this.merchZip              = isBlank(rs.getString("BUSINESS_ZIP"))          ? ""  : rs.getString("BUSINESS_ZIP");
          this.merchPhone            = isBlank(rs.getString("MERCH_PHONE"))           ? ""  : rs.getString("MERCH_PHONE");
          this.serviceLevel          = isBlank(rs.getString("SERVICE_LEVEL"))         ? ""  : rs.getString("SERVICE_LEVEL");
          this.attachCode            = isBlank(rs.getString("ATTACHMENT_CODE"))       ? ""  : rs.getString("ATTACHMENT_CODE");
          this.mcc                   = isBlank(rs.getString("MCC"))                   ? ""  : rs.getString("MCC");
          this.termApp               = isBlank(rs.getString("TERM_APPLICATION"))      ? ""  : rs.getString("TERM_APPLICATION");
          this.termAppProfGen        = isBlank(rs.getString("TERM_APPLICATION_PROFGEN")) ? termApp  : rs.getString("TERM_APPLICATION_PROFGEN");
          this.timeZone              = isBlank(rs.getString("TIME_ZONE"))             ? ""  : rs.getString("TIME_ZONE");

          this.discNum               = isBlank(rs.getString("DISC"))                  ? ""  : rs.getString("DISC");
          this.discAc                = isBlank(rs.getString("DISC_AC"))               ? ""  : rs.getString("DISC_AC");
          this.amexNum               = isBlank(rs.getString("AMEX"))                  ? ""  : rs.getString("AMEX");
          this.amexAc                = isBlank(rs.getString("AMEX_AC"))               ? ""  : rs.getString("AMEX_AC");
          this.amexSplit             = isBlank(rs.getString("SPLIT_DIAL"))            ? ""  : rs.getString("SPLIT_DIAL");
          this.dinersNum             = isBlank(rs.getString("DINR"))                  ? ""  : rs.getString("DINR");
          this.dinersAc              = isBlank(rs.getString("DINR_AC"))               ? ""  : rs.getString("DINR_AC");
          this.jcbNum                = isBlank(rs.getString("JCB"))                   ? ""  : rs.getString("JCB");
          this.jcbAc                 = isBlank(rs.getString("JCB_AC"))                ? ""  : rs.getString("JCB_AC");
          this.termVoiceId           = isBlank(rs.getString("TERM_VOICE_ID"))         ? ""  : rs.getString("TERM_VOICE_ID");
          this.voiceAuthPhone        = isBlank(rs.getString("VOICE_AUTH_PHONE"))      ? ""  : rs.getString("VOICE_AUTH_PHONE");
          this.cdSet                 = isBlank(rs.getString("CDSET"))                 ? ""  : rs.getString("CDSET");
          
          dateOfTransmission         = isBlank(rs.getString("process_start_date"))    ? ""  : java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("process_start_date"));
          timeOfTransmission         = isBlank(rs.getString("process_start_date"))    ? ""  : java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("process_start_date"));
          dateTimeOfTransmission     = glueDate(dateOfTransmission,timeOfTransmission);

          dateOfResponse             = isBlank(rs.getString("process_end_date"))      ? ""  : java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("process_end_date"));
          timeOfResponse             = isBlank(rs.getString("process_end_date"))      ? ""  : java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("process_end_date"));
          dateTimeOfResponse         = glueDate(dateOfResponse,timeOfResponse);

          dst                        = isBlank(rs.getString("DST"))                   ? ""  : rs.getString("DST");
          contactName                = isBlank(rs.getString("CONTACT_NAME"))          ? ""  : rs.getString("CONTACT_NAME");
          projectNum                 = isBlank(rs.getString("PROJECT_NUM"))           ? ""  : rs.getString("PROJECT_NUM");
          mcfs                       = isBlank(rs.getString("MCFS"))                  ? ""  : rs.getString("MCFS");
          customerServiceNum         = isBlank(rs.getString("CUSTOMER_SERVICE_NUM"))  ? ""  : rs.getString("CUSTOMER_SERVICE_NUM");

          profileGeneratorCode       = isBlank(rs.getString("PROFILE_GENERATOR_CODE"))? mesConstants.DEFAULT_PROFGEN  : rs.getInt("PROFILE_GENERATOR_CODE");

          rs.close();
          it.close();
          
          /*@lineinfo:generated-code*//*@lineinfo:911^11*/

//  ************************************************************
//  #sql [Ctx] it = { select    merchpo_provider_name       as provider,
//                        cardtype_code               as app_card_type,
//                        merchpo_card_merch_number   as merchant_number,
//                        merchpo_tid                 as tid
//              from      merchpayoption mpo
//              where     mpo.app_seq_num = :pk and
//                        mpo.cardtype_code in
//                        (
//                          :mesConstants.APP_CT_CHECK_AUTH,        -- 18,
//                          :mesConstants.APP_CT_VALUTEC_GIFT_CARD  -- 24
//                        )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    merchpo_provider_name       as provider,\n                      cardtype_code               as app_card_type,\n                      merchpo_card_merch_number   as merchant_number,\n                      merchpo_tid                 as tid\n            from      merchpayoption mpo\n            where     mpo.app_seq_num =  :1  and\n                      mpo.cardtype_code in\n                      (\n                         :2 ,        -- 18,\n                         :3   -- 24\n                      )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   __sJT_st.setInt(2,mesConstants.APP_CT_CHECK_AUTH);
   __sJT_st.setInt(3,mesConstants.APP_CT_VALUTEC_GIFT_CARD);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:924^11*/
          rs = it.getResultSet();
          
          while( rs.next() )
          {
            switch( rs.getInt("app_card_type") )
            {
              case mesConstants.APP_CT_CHECK_AUTH:
                CheckAccepted = true;
                CheckTermId   = rs.getString("tid");
                CheckMerchId  = rs.getString("merchant_number");
                CheckProvider = MmsManagementBean.decodeCheckProviderName(rs.getString("merchpo_provider_name"));
                break;
                
              case mesConstants.APP_CT_VALUTEC_GIFT_CARD:
                ValutecAccepted = true;
                ValutecTermId   = rs.getString("tid");
                ValutecMerchId  = rs.getString("merchant_number");
                break;
                
            }
          }
          rs.close();
          it.close();
          
          //checks to see if we should show the include or not
          //must be after termApp.. cause uses that info 
          //to make decision and to get proper include
          checkExpandedFields(mesConstants.MMS_STAGE_BUILD_TERM_APP);

          //try to get errors if they exist
          getCurrentErrors(id);
        }
        else
        {
          
          /*@lineinfo:generated-code*//*@lineinfo:960^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch.app_seq_num                   APP_SEQ_NUM,
//                      merch.merch_business_name           BUSINESS_NAME,
//                      merch.sic_code                      MCC,
//                      merch.merch_number                  MERCH_NUMBER,
//                      add1.ADDRESS_LINE1                  BUSINESS_ADDRESS,
//                      add1.ADDRESS_CITY                   BUSINESS_CITY,
//                      add1.COUNTRYSTATE_CODE              BUSINESS_STATE,
//                      add1.ADDRESS_ZIP                    BUSINESS_ZIP,
//                      add1.ADDRESS_PHONE                  BUSINESS_PHONE,
//                      cont.MERCHCONT_PRIM_FIRST_NAME      CONTACT_FIRST_NAME,
//                      cont.MERCHCONT_PRIM_LAST_NAME       CONTACT_LAST_NAME,
//                      cont.MERCHCONT_PRIM_PHONE           MERCH_PHONE,
//                      mdar.DECISION_STATUS                DISC_STATUS,
//                      mdar.DISCOVER_MERCHANT_NUM          DISC_MERCHANT_NUM
//  
//              from    merchant                            merch,
//                      address                             add1,
//                      merchcontact                        cont,
//                      MERCHANT_DISCOVER_APP_RESPONSE      mdar
//  
//              where   merch.app_seq_num = :pk                 and
//                      merch.app_seq_num = cont.app_seq_num    and
//                      merch.app_seq_num = mdar.app_seq_num(+) and
//                      merch.app_seq_num = add1.app_seq_num    and 
//                      add1.ADDRESSTYPE_CODE  = 1
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch.app_seq_num                   APP_SEQ_NUM,\n                    merch.merch_business_name           BUSINESS_NAME,\n                    merch.sic_code                      MCC,\n                    merch.merch_number                  MERCH_NUMBER,\n                    add1.ADDRESS_LINE1                  BUSINESS_ADDRESS,\n                    add1.ADDRESS_CITY                   BUSINESS_CITY,\n                    add1.COUNTRYSTATE_CODE              BUSINESS_STATE,\n                    add1.ADDRESS_ZIP                    BUSINESS_ZIP,\n                    add1.ADDRESS_PHONE                  BUSINESS_PHONE,\n                    cont.MERCHCONT_PRIM_FIRST_NAME      CONTACT_FIRST_NAME,\n                    cont.MERCHCONT_PRIM_LAST_NAME       CONTACT_LAST_NAME,\n                    cont.MERCHCONT_PRIM_PHONE           MERCH_PHONE,\n                    mdar.DECISION_STATUS                DISC_STATUS,\n                    mdar.DISCOVER_MERCHANT_NUM          DISC_MERCHANT_NUM\n\n            from    merchant                            merch,\n                    address                             add1,\n                    merchcontact                        cont,\n                    MERCHANT_DISCOVER_APP_RESPONSE      mdar\n\n            where   merch.app_seq_num =  :1                  and\n                    merch.app_seq_num = cont.app_seq_num    and\n                    merch.app_seq_num = mdar.app_seq_num(+) and\n                    merch.app_seq_num = add1.app_seq_num    and \n                    add1.ADDRESSTYPE_CODE  = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:987^11*/

          rs = it.getResultSet();
 
          if(rs.next())
          {
            this.primaryKey            = rs.getLong("APP_SEQ_NUM");
            this.merchNum              = isBlank(rs.getString("MERCH_NUMBER"))        ? "" : rs.getString("MERCH_NUMBER");
            defaultMcfs(merchNum);
            this.merchName             = isBlank(rs.getString("BUSINESS_NAME"))       ? "" : rs.getString("BUSINESS_NAME");
            this.merchAddress          = isBlank(rs.getString("BUSINESS_ADDRESS"))    ? "" : rs.getString("BUSINESS_ADDRESS");
            this.merchCity             = isBlank(rs.getString("BUSINESS_CITY"))       ? "" : rs.getString("BUSINESS_CITY");
            this.merchState            = isBlank(rs.getString("BUSINESS_STATE"))      ? "" : rs.getString("BUSINESS_STATE");
            this.merchZip              = isBlank(rs.getString("BUSINESS_ZIP"))        ? "" : rs.getString("BUSINESS_ZIP");
            this.customerServiceNum    = isBlank(rs.getString("BUSINESS_PHONE"))      ? "" : rs.getString("BUSINESS_PHONE"); 
            this.merchPhone            = isBlank(rs.getString("BUSINESS_PHONE"))      ? "" : rs.getString("BUSINESS_PHONE");
            this.mcc                   = isBlank(rs.getString("MCC"))                 ? "" : rs.getString("MCC");
            this.termVoiceId           = isBlank(rs.getString("MERCH_NUMBER"))        ? "" : rs.getString("MERCH_NUMBER");
            this.contactName           = isBlank(rs.getString("CONTACT_FIRST_NAME"))  ? "" : (rs.getString("CONTACT_FIRST_NAME") + " ");
            this.contactName          += isBlank(rs.getString("CONTACT_LAST_NAME"))   ? "" : rs.getString("CONTACT_LAST_NAME");
            this.contactName           = this.contactName.trim();

            //try to get disc merch # from easi link first.. 
            if(!isBlank(rs.getString("DISC_STATUS")) && rs.getString("DISC_STATUS").equals("A"))
            {
              this.discNum = isBlank(rs.getString("DISC_MERCHANT_NUM")) ? "" : rs.getString("DISC_MERCHANT_NUM");
              if(!isBlank(discNum))
              {
                this.discAc = "1";
              }
            }

          }
         
          it.close();


          // load the accepted cards from the application data
          /*@lineinfo:generated-code*//*@lineinfo:1025^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  * 
//              from    merchpayoption
//              where   app_seq_num = :pk 
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  * \n            from    merchpayoption\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1030^11*/

          rs = it.getResultSet();
 
          while(rs.next())
          {
            switch(rs.getInt("cardtype_code"))
            {
              case mesConstants.APP_CT_DISCOVER:
                
                //if we already got a disc merchant number get out of here...
                if(!isBlank(this.discNum))
                {
                  break;
                }

                this.discNum    = isBlank(rs.getString("MERCHPO_CARD_MERCH_NUMBER"))  ? "" : rs.getString("MERCHPO_CARD_MERCH_NUMBER");
                if(isBlank(this.discNum) || this.discNum.equals("0"))
                {
                  this.discNum    = "";
                  this.discAc     = "";
                }
                else
                {
                  this.discAc     = "1";
                }
              break;

              case mesConstants.APP_CT_AMEX:
    
                this.amexNum    = isBlank(rs.getString("MERCHPO_CARD_MERCH_NUMBER"))  ? "" : rs.getString("MERCHPO_CARD_MERCH_NUMBER");
                if(isBlank(this.amexNum) || this.amexNum.equals("0"))
                {
                  this.amexNum    = "";
                  this.amexAc     = "";
                  this.amexSplit  = "";
                }
                else
                {
                  this.amexAc     = "1";
                  this.amexSplit  = "N";
                }
              break;

              case mesConstants.APP_CT_DINERS_CLUB:

                this.dinersNum  = isBlank(rs.getString("MERCHPO_CARD_MERCH_NUMBER"))  ? "" : rs.getString("MERCHPO_CARD_MERCH_NUMBER");
                if(isBlank(this.dinersNum) || this.dinersNum.equals("0"))
                {
                  this.dinersNum  = "";
                  this.dinersAc   = "";
                }
                else
                {
                  this.dinersAc   = "1";
                }
              break;

              case mesConstants.APP_CT_JCB:

                this.jcbNum     = isBlank(rs.getString("MERCHPO_CARD_MERCH_NUMBER"))  ? "" : rs.getString("MERCHPO_CARD_MERCH_NUMBER");
                if(isBlank(this.jcbNum) || this.jcbNum.equals("0"))
                {
                  this.jcbNum     = "";
                  this.jcbAc      = "";
                }
                else
                {
                  this.jcbAc      = "1";
                }
              break;
              
              case mesConstants.APP_CT_CHECK_AUTH:
                CheckAccepted     = true;
                CheckTermId       = rs.getString("merchpo_tid");
                CheckMerchId      = rs.getString("merchpo_card_merch_number");
                CheckProvider     = MmsManagementBean.decodeCheckProviderName(rs.getString("merchpo_provider_name"));
                break;
                
              case mesConstants.APP_CT_VALUTEC_GIFT_CARD:
                ValutecAccepted = true;
                ValutecMerchId  = rs.getString("merchpo_card_merch_number");
                ValutecTermId   = rs.getString("merchpo_tid");
                break;
            }
          }
          it.close();
        }

        truncateAppData();
    }
    catch(Exception e)
    {
      logEntry("getAppContent()", e.toString());
      addError("Error: Complications occurred while retrieving data!  Please refresh.");
    }
    finally
    {
      cleanUp();
    }
  }

  private void defaultMcfs(String mNum)
  {
    this.mcfs = mNum.trim();
    
    if(mcfs.length() < FIELD_LENGTH_MCFS)
    {
     
     String mcfsAddOn = "";
     for(int y=0; y<(FIELD_LENGTH_MCFS - mcfs.length()); y++)
     {
       mcfsAddOn += "0";
     }
     
     this.mcfs += mcfsAddOn.trim();

    }
    else if(mcfs.length() > FIELD_LENGTH_MCFS)
    {
      this.mcfs = this.mcfs.substring(0,16);
    }

  }

  public void getCurrentErrors(long id)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             useCleanUp  = false;

    try
    {
      
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
   
      /*@lineinfo:generated-code*//*@lineinfo:1170^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  error_description
//          from    mms_current_errors
//          where   request_id = :id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  error_description\n        from    mms_current_errors\n        where   request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1175^7*/

      rs = it.getResultSet();

      errorDescriptions = "";
      while(rs.next())
      {
        errorDescriptions += (rs.getString("error_description") + ";<br>");
      }
 
      it.close();

    }
    catch(Exception e)
    {
      logEntry("getCurrentErrors()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  
  }

  /*
  ** METHOD public void submitData()
  **
  */
  public void submitData(UserBean user)
  {
    try
    {
      connect();
      
      if(recordExists())
      {
        /*@lineinfo:generated-code*//*@lineinfo:1214^9*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info
//            set     VNUM                      = :vNum,          
//                    BIN                       = :binNum,
//                    AGENT                     = :agentNum,      
//                    CHAIN                     = :chainNum,         
//                    MERCH_NUMBER              = :merchNum,        
//                    LOCATION_NUMBER           = :locationNum,
//                    STORE_NUMBER              = :storeNum,          
//                    TERMINAL_NUMBER           = :terminalNum,        
//                    EQUIP_MODEL               = :equipModel,
//                    EQUIP_REFURB              = :equipModelRefurb,
//                    BUSINESS_NAME             = :merchName,                  
//                    BUSINESS_ADDRESS          = :merchAddress,
//                    BUSINESS_CITY             = :merchCity,        
//                    BUSINESS_STATE            = :merchState,       
//                    BUSINESS_ZIP              = :merchZip,         
//                    MERCH_PHONE               = :merchPhone,     
//                    SERVICE_LEVEL             = :serviceLevel,        
//                    ATTACHMENT_CODE           = :attachCode,       
//                    MCC                       = :mcc,         
//                    TERM_APPLICATION          = :termApp,
//                    TERM_APPLICATION_PROFGEN  = :termAppProfGen,
//                    DISC                      = :discNum,     
//                    DISC_AC                   = :discAc,
//                    AMEX                      = :amexNum,        
//                    AMEX_AC                   = :amexAc,       
//                    SPLIT_DIAL                = :amexSplit,         
//                    DINR                      = :dinersNum,      
//                    DINR_AC                   = :dinersAc,       
//                    JCB                       = :jcbNum,
//                    JCB_AC                    = :jcbAc,
//                    TERM_VOICE_ID             = :termVoiceId,
//                    VOICE_AUTH_PHONE          = :voiceAuthPhone,
//                    CDSET                     = :cdSet,
//                    SPECIAL_HANDLING          = :specialHandling,
//                    TIME_ZONE                 = :timeZone,
//                    DST                       = :dst,
//                    CONTACT_NAME              = :contactName,
//                    PROJECT_NUM               = :projectNum,
//                    MCFS                      = :mcfs,
//                    CUSTOMER_SERVICE_NUM      = :customerServiceNum,
//                    PROFILE_GENERATOR_CODE    = :profileGeneratorCode
//            where   REQUEST_ID = :requestId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mms_stage_info\n          set     VNUM                      =  :1 ,          \n                  BIN                       =  :2 ,\n                  AGENT                     =  :3 ,      \n                  CHAIN                     =  :4 ,         \n                  MERCH_NUMBER              =  :5 ,        \n                  LOCATION_NUMBER           =  :6 ,\n                  STORE_NUMBER              =  :7 ,          \n                  TERMINAL_NUMBER           =  :8 ,        \n                  EQUIP_MODEL               =  :9 ,\n                  EQUIP_REFURB              =  :10 ,\n                  BUSINESS_NAME             =  :11 ,                  \n                  BUSINESS_ADDRESS          =  :12 ,\n                  BUSINESS_CITY             =  :13 ,        \n                  BUSINESS_STATE            =  :14 ,       \n                  BUSINESS_ZIP              =  :15 ,         \n                  MERCH_PHONE               =  :16 ,     \n                  SERVICE_LEVEL             =  :17 ,        \n                  ATTACHMENT_CODE           =  :18 ,       \n                  MCC                       =  :19 ,         \n                  TERM_APPLICATION          =  :20 ,\n                  TERM_APPLICATION_PROFGEN  =  :21 ,\n                  DISC                      =  :22 ,     \n                  DISC_AC                   =  :23 ,\n                  AMEX                      =  :24 ,        \n                  AMEX_AC                   =  :25 ,       \n                  SPLIT_DIAL                =  :26 ,         \n                  DINR                      =  :27 ,      \n                  DINR_AC                   =  :28 ,       \n                  JCB                       =  :29 ,\n                  JCB_AC                    =  :30 ,\n                  TERM_VOICE_ID             =  :31 ,\n                  VOICE_AUTH_PHONE          =  :32 ,\n                  CDSET                     =  :33 ,\n                  SPECIAL_HANDLING          =  :34 ,\n                  TIME_ZONE                 =  :35 ,\n                  DST                       =  :36 ,\n                  CONTACT_NAME              =  :37 ,\n                  PROJECT_NUM               =  :38 ,\n                  MCFS                      =  :39 ,\n                  CUSTOMER_SERVICE_NUM      =  :40 ,\n                  PROFILE_GENERATOR_CODE    =  :41 \n          where   REQUEST_ID =  :42";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,vNum);
   __sJT_st.setString(2,binNum);
   __sJT_st.setString(3,agentNum);
   __sJT_st.setString(4,chainNum);
   __sJT_st.setString(5,merchNum);
   __sJT_st.setString(6,locationNum);
   __sJT_st.setString(7,storeNum);
   __sJT_st.setString(8,terminalNum);
   __sJT_st.setString(9,equipModel);
   __sJT_st.setString(10,equipModelRefurb);
   __sJT_st.setString(11,merchName);
   __sJT_st.setString(12,merchAddress);
   __sJT_st.setString(13,merchCity);
   __sJT_st.setString(14,merchState);
   __sJT_st.setString(15,merchZip);
   __sJT_st.setString(16,merchPhone);
   __sJT_st.setString(17,serviceLevel);
   __sJT_st.setString(18,attachCode);
   __sJT_st.setString(19,mcc);
   __sJT_st.setString(20,termApp);
   __sJT_st.setString(21,termAppProfGen);
   __sJT_st.setString(22,discNum);
   __sJT_st.setString(23,discAc);
   __sJT_st.setString(24,amexNum);
   __sJT_st.setString(25,amexAc);
   __sJT_st.setString(26,amexSplit);
   __sJT_st.setString(27,dinersNum);
   __sJT_st.setString(28,dinersAc);
   __sJT_st.setString(29,jcbNum);
   __sJT_st.setString(30,jcbAc);
   __sJT_st.setString(31,termVoiceId);
   __sJT_st.setString(32,voiceAuthPhone);
   __sJT_st.setString(33,cdSet);
   __sJT_st.setString(34,specialHandling);
   __sJT_st.setString(35,timeZone);
   __sJT_st.setString(36,dst);
   __sJT_st.setString(37,contactName);
   __sJT_st.setString(38,projectNum);
   __sJT_st.setString(39,mcfs);
   __sJT_st.setString(40,customerServiceNum);
   __sJT_st.setInt(41,profileGeneratorCode);
   __sJT_st.setLong(42,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1259^9*/
      }
      // update existing records
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1264^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mms_stage_info
//            ( 
//              APP_SEQ_NUM,
//              REQUEST_ID,
//              APP_DATE,
//              VNUM,
//              BIN,
//              AGENT,
//              CHAIN,
//              MERCH_NUMBER,
//              LOCATION_NUMBER,
//              STORE_NUMBER,          
//              TERMINAL_NUMBER,
//              EQUIP_MODEL,
//              EQUIP_REFURB,
//              BUSINESS_NAME,
//              BUSINESS_ADDRESS,
//              BUSINESS_CITY,
//              BUSINESS_STATE,
//              BUSINESS_ZIP,
//              MERCH_PHONE,
//              SERVICE_LEVEL,
//              ATTACHMENT_CODE,
//              MCC,
//              TERM_APPLICATION,
//              TERM_APPLICATION_PROFGEN,
//              DISC,
//              DISC_AC,
//              AMEX,
//              AMEX_AC,
//              SPLIT_DIAL,
//              DINR,
//              DINR_AC,
//              JCB,
//              JCB_AC,
//              TERM_VOICE_ID,
//              VOICE_AUTH_PHONE,
//              CDSET,
//              PROCESS_METHOD,
//              PROCESS_STATUS,
//              SPECIAL_HANDLING,
//              TIME_ZONE,
//              DST,
//              CONTACT_NAME,
//              PROJECT_NUM,
//              MCFS,
//              CUSTOMER_SERVICE_NUM,
//              PROFILE_GENERATOR_CODE
//            )
//            values
//            ( 
//              :primaryKey,
//              :requestId,
//              :today,
//              :vNum,
//              :binNum,
//              :agentNum,
//              :chainNum,
//              :merchNum,
//              :locationNum,
//              :storeNum,
//              :terminalNum,
//              :equipModel,
//              :equipModelRefurb,
//              :merchName,
//              :merchAddress,
//              :merchCity,
//              :merchState,
//              :merchZip,
//              :merchPhone,
//              :serviceLevel,
//              :attachCode,
//              :mcc,
//              :termApp,
//              :termAppProfGen,
//              :discNum,
//              :discAc,
//              :amexNum,
//              :amexAc,
//              :amexSplit,
//              :dinersNum,
//              :dinersAc,
//              :jcbNum,
//              :jcbAc,
//              :termVoiceId,
//              :voiceAuthPhone,
//              :cdSet,
//              :transmissionMethod,
//              :mesConstants.ETPPS_NEW,
//              :specialHandling,
//              :timeZone,
//              :dst,                 
//              :contactName,        
//              :projectNum,         
//              :mcfs,                
//              :customerServiceNum,
//              :profileGeneratorCode
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mms_stage_info\n          ( \n            APP_SEQ_NUM,\n            REQUEST_ID,\n            APP_DATE,\n            VNUM,\n            BIN,\n            AGENT,\n            CHAIN,\n            MERCH_NUMBER,\n            LOCATION_NUMBER,\n            STORE_NUMBER,          \n            TERMINAL_NUMBER,\n            EQUIP_MODEL,\n            EQUIP_REFURB,\n            BUSINESS_NAME,\n            BUSINESS_ADDRESS,\n            BUSINESS_CITY,\n            BUSINESS_STATE,\n            BUSINESS_ZIP,\n            MERCH_PHONE,\n            SERVICE_LEVEL,\n            ATTACHMENT_CODE,\n            MCC,\n            TERM_APPLICATION,\n            TERM_APPLICATION_PROFGEN,\n            DISC,\n            DISC_AC,\n            AMEX,\n            AMEX_AC,\n            SPLIT_DIAL,\n            DINR,\n            DINR_AC,\n            JCB,\n            JCB_AC,\n            TERM_VOICE_ID,\n            VOICE_AUTH_PHONE,\n            CDSET,\n            PROCESS_METHOD,\n            PROCESS_STATUS,\n            SPECIAL_HANDLING,\n            TIME_ZONE,\n            DST,\n            CONTACT_NAME,\n            PROJECT_NUM,\n            MCFS,\n            CUSTOMER_SERVICE_NUM,\n            PROFILE_GENERATOR_CODE\n          )\n          values\n          ( \n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 ,\n             :25 ,\n             :26 ,\n             :27 ,\n             :28 ,\n             :29 ,\n             :30 ,\n             :31 ,\n             :32 ,\n             :33 ,\n             :34 ,\n             :35 ,\n             :36 ,\n             :37 ,\n             :38 ,\n             :39 ,\n             :40 ,\n             :41 ,                 \n             :42 ,        \n             :43 ,         \n             :44 ,                \n             :45 ,\n             :46 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setLong(2,requestId);
   __sJT_st.setTimestamp(3,today);
   __sJT_st.setString(4,vNum);
   __sJT_st.setString(5,binNum);
   __sJT_st.setString(6,agentNum);
   __sJT_st.setString(7,chainNum);
   __sJT_st.setString(8,merchNum);
   __sJT_st.setString(9,locationNum);
   __sJT_st.setString(10,storeNum);
   __sJT_st.setString(11,terminalNum);
   __sJT_st.setString(12,equipModel);
   __sJT_st.setString(13,equipModelRefurb);
   __sJT_st.setString(14,merchName);
   __sJT_st.setString(15,merchAddress);
   __sJT_st.setString(16,merchCity);
   __sJT_st.setString(17,merchState);
   __sJT_st.setString(18,merchZip);
   __sJT_st.setString(19,merchPhone);
   __sJT_st.setString(20,serviceLevel);
   __sJT_st.setString(21,attachCode);
   __sJT_st.setString(22,mcc);
   __sJT_st.setString(23,termApp);
   __sJT_st.setString(24,termAppProfGen);
   __sJT_st.setString(25,discNum);
   __sJT_st.setString(26,discAc);
   __sJT_st.setString(27,amexNum);
   __sJT_st.setString(28,amexAc);
   __sJT_st.setString(29,amexSplit);
   __sJT_st.setString(30,dinersNum);
   __sJT_st.setString(31,dinersAc);
   __sJT_st.setString(32,jcbNum);
   __sJT_st.setString(33,jcbAc);
   __sJT_st.setString(34,termVoiceId);
   __sJT_st.setString(35,voiceAuthPhone);
   __sJT_st.setString(36,cdSet);
   __sJT_st.setString(37,transmissionMethod);
   __sJT_st.setString(38,mesConstants.ETPPS_NEW);
   __sJT_st.setString(39,specialHandling);
   __sJT_st.setString(40,timeZone);
   __sJT_st.setString(41,dst);
   __sJT_st.setString(42,contactName);
   __sJT_st.setString(43,projectNum);
   __sJT_st.setString(44,mcfs);
   __sJT_st.setString(45,customerServiceNum);
   __sJT_st.setInt(46,profileGeneratorCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1364^9*/
        
        if(wasSentManual())
        {
          updateStartDate();
        }
      }
      //updateTransmissionStatus(primaryKey);
      
      // update the check and valutec ids if necessary
      submitCheckValutecData();

      // update app_tracking to show the user login name that submitted this request
      updateAppTrackingMMS(user.getLoginName());

      //insert place holder in legacy vnumber tables
      insertIntoMerchVnumber();
      insertIntoVnumbers();
    }
    catch(Exception e)
    {
      logEntry("submitData()", e.toString());
      addError("Error: Complications occurred while storing data!  Please resubmit.");
    }
    finally
    {
      cleanUp();
      insertIntoProgramming(user);
    }
  }
  
  public void submitCheckValutecData( )
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1401^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchpayoption  mpo
//          set     mpo.merchpo_tid   = :CheckTermId,
//                  mpo.merchpo_card_merch_number = :CheckMerchId
//          where   mpo.app_seq_num = :this.primaryKey and
//                  mpo.cardtype_code = :mesConstants.APP_CT_CHECK_AUTH
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchpayoption  mpo\n        set     mpo.merchpo_tid   =  :1 ,\n                mpo.merchpo_card_merch_number =  :2 \n        where   mpo.app_seq_num =  :3  and\n                mpo.cardtype_code =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,CheckTermId);
   __sJT_st.setString(2,CheckMerchId);
   __sJT_st.setLong(3,this.primaryKey);
   __sJT_st.setInt(4,mesConstants.APP_CT_CHECK_AUTH);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1408^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:1410^7*/

//  ************************************************************
//  #sql [Ctx] { update  merchpayoption  mpo
//          set     mpo.merchpo_tid               = :ValutecTermId,
//                  mpo.merchpo_card_merch_number = :ValutecMerchId
//          where   mpo.app_seq_num   = :this.primaryKey and
//                  mpo.cardtype_code = :mesConstants.APP_CT_VALUTEC_GIFT_CARD
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchpayoption  mpo\n        set     mpo.merchpo_tid               =  :1 ,\n                mpo.merchpo_card_merch_number =  :2 \n        where   mpo.app_seq_num   =  :3  and\n                mpo.cardtype_code =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,ValutecTermId);
   __sJT_st.setString(2,ValutecMerchId);
   __sJT_st.setLong(3,this.primaryKey);
   __sJT_st.setInt(4,mesConstants.APP_CT_VALUTEC_GIFT_CARD);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1417^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:1419^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1422^7*/
    }
    catch(Exception e)
    {
      logEntry("submitCheckValutecData()", e.toString());
      addError("Error: Complications occurred while storing check/gift card data!  Please resubmit.");
    }
    finally
    {
      cleanUp();
    }
  }
  
  private void updateAppTrackingMMS(String loginName)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1439^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//          set     contact_name  = :loginName
//          where   app_seq_num   = :primaryKey and
//                  dept_code     = :QueueConstants.DEPARTMENT_MMS
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n        set     contact_name  =  :1 \n        where   app_seq_num   =  :2  and\n                dept_code     =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loginName);
   __sJT_st.setLong(2,primaryKey);
   __sJT_st.setInt(3,QueueConstants.DEPARTMENT_MMS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1445^7*/
    }
    catch(Exception e)
    {
      logEntry("updateAppTrackingMMS(" + loginName + ")", e.toString());
    }
  }

  private void insertIntoProgramming(UserBean user)
  {
     
    int type = MesQueues.Q_NONE;

    if(specialHandling.equals("Y"))
    {
      type = MesQueues.Q_PROGRAMMING_SPECIAL;
    }
    else
    {
      // determine programming queue as a function of the profile generator
      switch(profileGeneratorCode) {
        case mesConstants.PROFGEN_VERICENTRE:
          if(transmissionResponse!=null && transmissionResponse.equals(mesConstants.ETPPR_PENDING))
            type = MesQueues.Q_PROGRAMMING_VC_PENDING;
          else
            type = MesQueues.Q_PROGRAMMING_VC_NEW;
          if(appTypeNonDeploy.equals("Y")) //if non deploy we put directly into completed
            type = MesQueues.Q_PROGRAMMING_VC_COMPLETED;
          break;
        case mesConstants.PROFGEN_TERMMASTER:
          if(transmissionResponse!=null && transmissionResponse.equals(mesConstants.ETPPR_PENDING))
            type = MesQueues.Q_PROGRAMMING_TM_PENDING;
          else
            type = MesQueues.Q_PROGRAMMING_TM_NEW;
          if(appTypeNonDeploy.equals("Y"))//if non deploy we put directly into completed
            type = MesQueues.Q_PROGRAMMING_TM_COMPLETED;
          break;
        default:
          type = MesQueues.Q_PROGRAMMING_REGULAR;
          if(appTypeNonDeploy.equals("Y"))//if non deploy we put directly into completed
            type = MesQueues.Q_PROGRAMMING_COMPLETE;
          break;
      }
    }

    // kill any existing programming queue entries under this request id (e.g.: re-submissions)
    QueueTools.removeQueueByItemType(requestId,MesQueues.Q_ITEM_TYPE_PROGRAMMING);
    
    //this puts every request into the queue the id must be the requestId
    QueueTools.insertQueue(requestId, type, user);

    ///this is going to be obsolete real soon
    insertIntoProgrammingOld();
  }

  private void insertIntoProgrammingOld()
  {
    boolean useCleanUp = false;

    try
    {

      if(alreadyExistsInQueue(requestId))
      {
        return;
      }

      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }


      /*@lineinfo:generated-code*//*@lineinfo:1519^7*/

//  ************************************************************
//  #sql [Ctx] { insert into temp_activation_queue
//          ( 
//            APP_SEQ_NUM,
//            REQUEST_ID,
//            DATE_IN_QUEUE,
//            Q_STAGE,
//            Q_TYPE
//          )
//          values
//          ( 
//            :primaryKey,
//            :requestId,
//            :today,
//            :QueueConstants.Q_PROGRAMMING_NEW,
//            :QueueConstants.QUEUE_PROGRAMMING
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into temp_activation_queue\n        ( \n          APP_SEQ_NUM,\n          REQUEST_ID,\n          DATE_IN_QUEUE,\n          Q_STAGE,\n          Q_TYPE\n        )\n        values\n        ( \n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setLong(2,requestId);
   __sJT_st.setTimestamp(3,today);
   __sJT_st.setInt(4,QueueConstants.Q_PROGRAMMING_NEW);
   __sJT_st.setInt(5,QueueConstants.QUEUE_PROGRAMMING);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1537^7*/
    }
    catch(Exception e)
    {
      logEntry("insertIntoProgrammingOld()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  }




  private void updateStartDate()
  {
    boolean useCleanUp = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }

      /*@lineinfo:generated-code*//*@lineinfo:1567^7*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info
//          set     process_start_date  = :today
//          where   REQUEST_ID          = :requestId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mms_stage_info\n        set     process_start_date  =  :1 \n        where   REQUEST_ID          =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
   __sJT_st.setLong(2,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1572^7*/
    }
    catch(Exception e)
    {
      logEntry("updateStartDate()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  }

  /*
  ** METHOD public void resubmitData()
  **
  */
  public void reSubmitData(UserBean user)
  {
    try
    {

      connect();

      /*@lineinfo:generated-code*//*@lineinfo:1598^7*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info
//          
//            set   VNUM                  = :vNum,          
//                  BIN                   = :binNum,
//                  AGENT                 = :agentNum,      
//                  CHAIN                 = :chainNum,         
//                  MERCH_NUMBER          = :merchNum,        
//                  LOCATION_NUMBER       = :locationNum,
//                  STORE_NUMBER          = :storeNum,          
//                  TERMINAL_NUMBER       = :terminalNum,        
//                  EQUIP_MODEL           = :equipModel,
//                  EQUIP_REFURB          = :equipModelRefurb,
//                  BUSINESS_NAME         = :merchName,                  
//                  BUSINESS_ADDRESS      = :merchAddress,
//                  BUSINESS_CITY         = :merchCity,        
//                  BUSINESS_STATE        = :merchState,       
//                  BUSINESS_ZIP          = :merchZip,         
//                  MERCH_PHONE           = :merchPhone,     
//                  SERVICE_LEVEL         = :serviceLevel,        
//                  ATTACHMENT_CODE       = :attachCode,       
//                  MCC                   = :mcc,         
//                  TERM_APPLICATION      = :termApp,        
//                  TERM_APPLICATION_PROFGEN      = :termAppProfGen,
//                  DISC                  = :discNum,     
//                  DISC_AC               = :discAc,
//                  AMEX                  = :amexNum,        
//                  AMEX_AC               = :amexAc,       
//                  SPLIT_DIAL            = :amexSplit,         
//                  DINR                  = :dinersNum,      
//                  DINR_AC               = :dinersAc,       
//                  JCB                   = :jcbNum,
//                  JCB_AC                = :jcbAc,
//                  TERM_VOICE_ID         = :termVoiceId,
//                  VOICE_AUTH_PHONE      = :voiceAuthPhone,
//                  CDSET                 = :cdSet,
//                  process_status        = :mesConstants.ETPPS_RESUBMITTED,
//                  SPECIAL_HANDLING      = :specialHandling,
//                  TIME_ZONE             = :timeZone,
//                  DST                   = :dst,
//                  CONTACT_NAME          = :contactName,
//                  PROJECT_NUM           = :projectNum,
//                  MCFS                  = :mcfs,
//                  CUSTOMER_SERVICE_NUM  = :customerServiceNum,
//                  PROFILE_GENERATOR_CODE = :profileGeneratorCode
//  
//          where   REQUEST_ID          = :requestId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mms_stage_info\n        \n          set   VNUM                  =  :1 ,          \n                BIN                   =  :2 ,\n                AGENT                 =  :3 ,      \n                CHAIN                 =  :4 ,         \n                MERCH_NUMBER          =  :5 ,        \n                LOCATION_NUMBER       =  :6 ,\n                STORE_NUMBER          =  :7 ,          \n                TERMINAL_NUMBER       =  :8 ,        \n                EQUIP_MODEL           =  :9 ,\n                EQUIP_REFURB          =  :10 ,\n                BUSINESS_NAME         =  :11 ,                  \n                BUSINESS_ADDRESS      =  :12 ,\n                BUSINESS_CITY         =  :13 ,        \n                BUSINESS_STATE        =  :14 ,       \n                BUSINESS_ZIP          =  :15 ,         \n                MERCH_PHONE           =  :16 ,     \n                SERVICE_LEVEL         =  :17 ,        \n                ATTACHMENT_CODE       =  :18 ,       \n                MCC                   =  :19 ,         \n                TERM_APPLICATION      =  :20 ,        \n                TERM_APPLICATION_PROFGEN      =  :21 ,\n                DISC                  =  :22 ,     \n                DISC_AC               =  :23 ,\n                AMEX                  =  :24 ,        \n                AMEX_AC               =  :25 ,       \n                SPLIT_DIAL            =  :26 ,         \n                DINR                  =  :27 ,      \n                DINR_AC               =  :28 ,       \n                JCB                   =  :29 ,\n                JCB_AC                =  :30 ,\n                TERM_VOICE_ID         =  :31 ,\n                VOICE_AUTH_PHONE      =  :32 ,\n                CDSET                 =  :33 ,\n                process_status        =  :34 ,\n                SPECIAL_HANDLING      =  :35 ,\n                TIME_ZONE             =  :36 ,\n                DST                   =  :37 ,\n                CONTACT_NAME          =  :38 ,\n                PROJECT_NUM           =  :39 ,\n                MCFS                  =  :40 ,\n                CUSTOMER_SERVICE_NUM  =  :41 ,\n                PROFILE_GENERATOR_CODE =  :42 \n\n        where   REQUEST_ID          =  :43";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,vNum);
   __sJT_st.setString(2,binNum);
   __sJT_st.setString(3,agentNum);
   __sJT_st.setString(4,chainNum);
   __sJT_st.setString(5,merchNum);
   __sJT_st.setString(6,locationNum);
   __sJT_st.setString(7,storeNum);
   __sJT_st.setString(8,terminalNum);
   __sJT_st.setString(9,equipModel);
   __sJT_st.setString(10,equipModelRefurb);
   __sJT_st.setString(11,merchName);
   __sJT_st.setString(12,merchAddress);
   __sJT_st.setString(13,merchCity);
   __sJT_st.setString(14,merchState);
   __sJT_st.setString(15,merchZip);
   __sJT_st.setString(16,merchPhone);
   __sJT_st.setString(17,serviceLevel);
   __sJT_st.setString(18,attachCode);
   __sJT_st.setString(19,mcc);
   __sJT_st.setString(20,termApp);
   __sJT_st.setString(21,termAppProfGen);
   __sJT_st.setString(22,discNum);
   __sJT_st.setString(23,discAc);
   __sJT_st.setString(24,amexNum);
   __sJT_st.setString(25,amexAc);
   __sJT_st.setString(26,amexSplit);
   __sJT_st.setString(27,dinersNum);
   __sJT_st.setString(28,dinersAc);
   __sJT_st.setString(29,jcbNum);
   __sJT_st.setString(30,jcbAc);
   __sJT_st.setString(31,termVoiceId);
   __sJT_st.setString(32,voiceAuthPhone);
   __sJT_st.setString(33,cdSet);
   __sJT_st.setString(34,mesConstants.ETPPS_RESUBMITTED);
   __sJT_st.setString(35,specialHandling);
   __sJT_st.setString(36,timeZone);
   __sJT_st.setString(37,dst);
   __sJT_st.setString(38,contactName);
   __sJT_st.setString(39,projectNum);
   __sJT_st.setString(40,mcfs);
   __sJT_st.setString(41,customerServiceNum);
   __sJT_st.setInt(42,profileGeneratorCode);
   __sJT_st.setLong(43,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1646^7*/

      //once an mms has been resubmitted delete it from the error queue.
      QueueTools.removeQueue(requestId, MesQueues.Q_MMS_ERROR);
      
      //insert place holder in legacy vnumber tables
      insertIntoMerchVnumber();
      insertIntoVnumbers();

      //updateRetransmissionStatus(primaryKey);

    }
    catch(Exception e)
    {
      logEntry("resubmitData()", e.toString());
      addError("Error: Complications occurred while resubmitting data!  Please resubmit.");
    }
    finally
    {
      insertIntoProgramming(user);  // i.e. delete current and re-insert
      cleanUp();
    }
  }

  private void insertIntoMerchVnumber()
  {
    boolean useCleanUp = false;

    try
    {
      
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      if(alreadyExistsInMerchVnumber(requestId))
      {
        /*@lineinfo:generated-code*//*@lineinfo:1685^9*/

//  ************************************************************
//  #sql [Ctx] { update  merch_vnumber
//            set     terminal_number  = :terminalNum,
//                    store_number     = :storeNum,
//                    time_zone        = :timeZone,
//                    location_number  = :locationNum,
//                    app_code         = :termAppProfGen,
//                    equip_model      = :equipModel,
//                    PROFILE_GENERATOR_CODE = :profileGeneratorCode
//            where   REQUEST_ID       = :requestId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merch_vnumber\n          set     terminal_number  =  :1 ,\n                  store_number     =  :2 ,\n                  time_zone        =  :3 ,\n                  location_number  =  :4 ,\n                  app_code         =  :5 ,\n                  equip_model      =  :6 ,\n                  PROFILE_GENERATOR_CODE =  :7 \n          where   REQUEST_ID       =  :8";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"29com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,terminalNum);
   __sJT_st.setString(2,storeNum);
   __sJT_st.setString(3,timeZone);
   __sJT_st.setString(4,locationNum);
   __sJT_st.setString(5,termAppProfGen);
   __sJT_st.setString(6,equipModel);
   __sJT_st.setInt(7,profileGeneratorCode);
   __sJT_st.setLong(8,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1696^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1700^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_vnumber
//            ( 
//              APP_SEQ_NUM,
//              REQUEST_ID,
//              TERMINAL_NUMBER,
//              STORE_NUMBER,
//              TIME_ZONE,
//              LOCATION_NUMBER,
//              APP_CODE,
//              EQUIP_MODEL,
//              PROFILE_GENERATOR_CODE
//            )
//            values
//            ( 
//              :primaryKey,
//              :requestId,
//              :terminalNum,            
//              :storeNum,
//              :timeZone,
//              :locationNum,
//              :termAppProfGen,
//              :equipModel,
//              :profileGeneratorCode
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_vnumber\n          ( \n            APP_SEQ_NUM,\n            REQUEST_ID,\n            TERMINAL_NUMBER,\n            STORE_NUMBER,\n            TIME_ZONE,\n            LOCATION_NUMBER,\n            APP_CODE,\n            EQUIP_MODEL,\n            PROFILE_GENERATOR_CODE\n          )\n          values\n          ( \n             :1 ,\n             :2 ,\n             :3 ,            \n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"30com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setLong(2,requestId);
   __sJT_st.setString(3,terminalNum);
   __sJT_st.setString(4,storeNum);
   __sJT_st.setString(5,timeZone);
   __sJT_st.setString(6,locationNum);
   __sJT_st.setString(7,termAppProfGen);
   __sJT_st.setString(8,equipModel);
   __sJT_st.setInt(9,profileGeneratorCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1726^9*/
      }

    }
    catch(Exception e)
    {
      logEntry("insertIntoMerchVnumber()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

  }



  private void insertIntoVnumbers()
  {
    boolean useCleanUp = false;

    try
    {
      
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }

      if(alreadyExistsInVnumbers(requestId))
      {
        /*@lineinfo:generated-code*//*@lineinfo:1761^9*/

//  ************************************************************
//  #sql [Ctx] { update  v_numbers
//            set     terminal_number  = :terminalNum,
//                    model_index      = :terminalNum,
//                    store_number     = :storeNum,
//                    time_zone        = :timeZone,
//                    location_number  = :locationNum,
//                    app_code         = :termAppProfGen,
//                    equip_model      = :equipModel
//            where   REQUEST_ID       = :requestId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  v_numbers\n          set     terminal_number  =  :1 ,\n                  model_index      =  :2 ,\n                  store_number     =  :3 ,\n                  time_zone        =  :4 ,\n                  location_number  =  :5 ,\n                  app_code         =  :6 ,\n                  equip_model      =  :7 \n          where   REQUEST_ID       =  :8";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"31com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,terminalNum);
   __sJT_st.setString(2,terminalNum);
   __sJT_st.setString(3,storeNum);
   __sJT_st.setString(4,timeZone);
   __sJT_st.setString(5,locationNum);
   __sJT_st.setString(6,termAppProfGen);
   __sJT_st.setString(7,equipModel);
   __sJT_st.setLong(8,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1772^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1776^9*/

//  ************************************************************
//  #sql [Ctx] { insert into v_numbers
//            ( 
//              APP_SEQ_NUM,
//              REQUEST_ID,
//              TERMINAL_NUMBER,
//              MODEL_INDEX,
//              STORE_NUMBER,
//              TIME_ZONE,
//              LOCATION_NUMBER,
//              APP_CODE,
//              EQUIP_MODEL,
//              POS_CODE,
//              AUTO_FLAG
//            )
//            values
//            ( 
//              :primaryKey,
//              :requestId,
//              :terminalNum,            
//              :terminalNum,
//              :storeNum,
//              :timeZone,
//              :locationNum,
//              :termAppProfGen,
//              :equipModel,
//              :getPosCode(),
//              :AUTO_FLAG_DEFAULT
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_795 = getPosCode();
   String theSqlTS = "insert into v_numbers\n          ( \n            APP_SEQ_NUM,\n            REQUEST_ID,\n            TERMINAL_NUMBER,\n            MODEL_INDEX,\n            STORE_NUMBER,\n            TIME_ZONE,\n            LOCATION_NUMBER,\n            APP_CODE,\n            EQUIP_MODEL,\n            POS_CODE,\n            AUTO_FLAG\n          )\n          values\n          ( \n             :1 ,\n             :2 ,\n             :3 ,            \n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"32com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setLong(2,requestId);
   __sJT_st.setString(3,terminalNum);
   __sJT_st.setString(4,terminalNum);
   __sJT_st.setString(5,storeNum);
   __sJT_st.setString(6,timeZone);
   __sJT_st.setString(7,locationNum);
   __sJT_st.setString(8,termAppProfGen);
   __sJT_st.setString(9,equipModel);
   __sJT_st.setInt(10,__sJT_795);
   __sJT_st.setString(11,AUTO_FLAG_DEFAULT);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1806^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("insertIntoVnumbers()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

  }

  private int getPosCode()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             useCleanUp  = false;
    int                 result      = -1;

    try
    {
      
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:1839^7*/

//  ************************************************************
//  #sql [Ctx] { select POS_CODE
//          
//          from   merch_pos
//          where  APP_SEQ_NUM = :primaryKey
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select POS_CODE\n         \n        from   merch_pos\n        where  APP_SEQ_NUM =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.ops.MmsStageSetupBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   result = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1845^7*/
    
    }
    catch(Exception e)
    {
      logEntry("getPosCode()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
    return result;
  }


  public void submitManData()
  {

    try
    {
     
      connect(); 

      /*@lineinfo:generated-code*//*@lineinfo:1871^7*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info
//          
//          set     process_end_date        = :today,      
//                  vnum                    = :manVnum,
//                  process_status          = :manStatus,
//                  process_response        = :manResponse
//  
//          where   request_id              = :requestId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mms_stage_info\n        \n        set     process_end_date        =  :1 ,      \n                vnum                    =  :2 ,\n                process_status          =  :3 ,\n                process_response        =  :4 \n\n        where   request_id              =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"34com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,today);
   __sJT_st.setString(2,manVnum);
   __sJT_st.setString(3,manStatus);
   __sJT_st.setString(4,manResponse);
   __sJT_st.setLong(5,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1881^7*/
      
      updateMerchVnumber();
      updateVnumbers();

      //puts in app_acccount_complete and moves to v number assignment queue completed
      VNumberBean.executeAppAccountComplete(primaryKey);
      
      
      //updateResponseStatus(primaryKey);
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "submitManData: " + e.toString());
    } 
    finally
    {
      cleanUp();
    }
  }


  //static entry point to update vnumber tables with vnumber from auto mms process.
  //and to add entery to app_account_complete for xml stuff.. and to take out of v number queue
  public static void updateVnumberTables(long reqId, String v)
  {
    MmsStageSetupBean  mssb = new MmsStageSetupBean();
    try
    {
      mssb.setRequestId(Long.toString(reqId));
      mssb.setManVnum(v);
      
      mssb.updateMerchVnumber();
      mssb.updateVnumbers();

      //puts in app_acccount_complete and moves to v number assignment queue completed
      VNumberBean.executeAppAccountComplete(mssb.figureOutPrimaryKey(reqId));
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("MmsStageSetupBean.updateVnumberTables(" + reqId + ")", e.toString());
    }
  }


  private void updateMerchVnumber()
  {
    boolean             useCleanUp  = false;

    try
    {
     
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }

      /*@lineinfo:generated-code*//*@lineinfo:1939^7*/

//  ************************************************************
//  #sql [Ctx] { update  merch_vnumber
//          set     vnumber         = :manVnum
//          where   request_id      = :requestId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merch_vnumber\n        set     vnumber         =  :1 \n        where   request_id      =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"35com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,manVnum);
   __sJT_st.setLong(2,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1944^7*/
      
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "updateMerchVnumber: " + e.toString());
    } 
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  }

  private void updateVnumbers()
  {
    boolean             useCleanUp  = false;

    try
    {
     
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }

      /*@lineinfo:generated-code*//*@lineinfo:1973^7*/

//  ************************************************************
//  #sql [Ctx] { update  v_numbers
//          set     v_number        = :manVnum
//          where   request_id      = :requestId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  v_numbers\n        set     v_number        =  :1 \n        where   request_id      =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"36com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,manVnum);
   __sJT_st.setLong(2,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1978^7*/
      
    }  
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "updateVnumbers: " + e.toString());
    } 
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  }

  public boolean appApproved()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;
    boolean             useCleanUp  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
   
      /*@lineinfo:generated-code*//*@lineinfo:2009^7*/

//  ************************************************************
//  #sql [Ctx] it = { select merch_credit_status
//          from  merchant            
//          where app_seq_num = :primaryKey                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select merch_credit_status\n        from  merchant            \n        where app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"37com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"37com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2014^7*/

      rs = it.getResultSet();

      if(rs.next())
      {
        if(rs.getInt("merch_credit_status") == QueueConstants.CREDIT_APPROVE)
        {
          result = true;
        }
      }
 
      it.close();

    }
    catch(Exception e)
    {
      logEntry("appApproved()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;

  }


  private boolean alreadyExistsInQueue(long reqId)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;
    boolean             useCleanUp  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:2061^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    temp_activation_queue
//          where   request_id   = :reqId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    temp_activation_queue\n        where   request_id   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"38com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,reqId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"38com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2066^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }
 
      it.close();
    
    }
    catch(Exception e)
    {
      result = false;
      logEntry("alreadyExistsInQueue()", e.toString());
    }
    finally
    { 
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;
  }


  private boolean alreadyExistsInMerchVnumber(long reqId)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;
    boolean             useCleanUp  = false;
    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:2109^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    merch_vnumber
//          where   request_id   = :reqId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    merch_vnumber\n        where   request_id   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,reqId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"39com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2114^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }
 
      it.close();
    
    }
    catch(Exception e)
    {
      result = false;
      logEntry("alreadyExistsInMerchVnumber()", e.toString());
    }
    finally
    { 
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;
  }

  private boolean alreadyExistsInVnumbers(long reqId)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;
    boolean             useCleanUp  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:2157^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    v_numbers
//          where   request_id   = :reqId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    v_numbers\n        where   request_id   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"40com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,reqId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"40com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2162^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }
 
      it.close();
    
    }
    catch(Exception e)
    {
      result = false;
      logEntry("alreadyExistsInVnumbers()", e.toString());
    }
    finally
    { 
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;
  }


  private boolean trackRecordExists(long pk, int dept)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;
    boolean             useCleanUp  = false;
    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:2205^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num 
//          from    app_tracking
//          where   app_seq_num   = :pk and
//                  dept_code     = :dept
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num \n        from    app_tracking\n        where   app_seq_num   =  :1  and\n                dept_code     =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"41com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,pk);
   __sJT_st.setInt(2,dept);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"41com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2211^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }
 
      it.close();
    
    }
    catch(Exception e)
    {
      result = false;
      logEntry("trackRecordExists()", e.toString());
    }
    finally
    { 
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;
  }

  private boolean recordExists()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;
    boolean             useCleanUp  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:2254^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  app_seq_num 
//          from    mms_stage_info
//          where   request_id = :requestId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  app_seq_num \n        from    mms_stage_info\n        where   request_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"42com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,requestId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"42com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2259^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        result = true;
      }
 
      it.close();
    
    }
    catch(Exception e)
    {
      result = false;
      logEntry("recordExists()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;
  }


  public void checkExpandedFields(String appShown)
  {
    if(!isBlank(termAppProfGen) && hasExpandedFields(termAppProfGen)) 
    {
      //if first time submitting and expanded fields were never shown.. 
      //then we dont actually want to submit.. not yet..
      if(!expandedFields || !appShown.equals(termAppProfGen))
      {
        submitted   = false;
        reSubmitted = false;
        okToGetGets = false;
      }
      expandedFields = true;
      
      setIncludeFileName(); //based on term app.. 

      if(!submitted && !queuedForTrans) {
        addMsg("Please now specify the '"+termAppProfGen+"' terminal application specific fields.");
      }
    }
    else
    {
      if(!isBlank(appShown) && hasExpandedFields(appShown))
      {
        submitted   = false;
        reSubmitted = false;
        okToGetGets = false;
      }
      expandedFields = false;
    }
  }

  public boolean hasExpandedFields(String trmapp)
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             result      = false;
    boolean             useCleanUp  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }

      /*@lineinfo:generated-code*//*@lineinfo:2335^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  TERMINAL_APPLICATION_FILE_NAME 
//          from    mms_terminal_applications
//          where   TERMINAL_APPLICATION = :trmapp
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  TERMINAL_APPLICATION_FILE_NAME \n        from    mms_terminal_applications\n        where   TERMINAL_APPLICATION =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"43com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,trmapp);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"43com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2340^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        if(!isBlank(rs.getString("TERMINAL_APPLICATION_FILE_NAME")))
        {
          result = true;
        }
      }
 
      it.close();
    
    }
    catch(Exception e)
    {
      result = false;
      logEntry("hasExpandedFields()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }

    return result;

  }



  public void setIncludeFileName()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    boolean             useCleanUp  = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:2388^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  TERMINAL_APPLICATION_FILE_NAME 
//          from    mms_terminal_applications
//          where   TERMINAL_APPLICATION   = :termAppProfGen
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  TERMINAL_APPLICATION_FILE_NAME \n        from    mms_terminal_applications\n        where   TERMINAL_APPLICATION   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"44com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,termAppProfGen);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"44com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2393^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        this.includeFileName = isBlank(rs.getString("TERMINAL_APPLICATION_FILE_NAME")) ? NOFILE : rs.getString("TERMINAL_APPLICATION_FILE_NAME");
      }
      else
      {
        this.includeFileName = NOFILE;
      }

      it.close();
    
    }
    catch(Exception e)
    {
      logEntry("setIncludeFileName()", e.toString());
    }
    finally
    { 
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  }
  
  public String getIncludeFileName()
  {
    return this.includeFileName;
  }

  /*
  **Manual Update VALIDATION
  */  

  public boolean manValidate()
  {

    if(isBlank(manStatus))
    {
      addError("Please select a Status");
    }

    if(isBlank(manResponse))
    {
      addError("Please select a Response");
    }

    if(isBlank(manVnum))
    {
      addError("Please provide a V#");
    }
    else
    {
      manVnum = manVnum.toUpperCase();
    }

    return (!hasErrors());
  }

  /*
  **VALIDATION
  */  
  public boolean validate()
  {

     if(!appApproved())
     {
       addError("This application has been declined, please do not continue further");
     }
     
     if(isBlank(termAppProfGen))
     {
       addError("Please select a Terminal Application");
     }
     else if(!termAppProfGen.equals(mesConstants.MMS_STAGE_BUILD_TERM_APP) && !termAppProfGen.equals("DIALPAY"))
     {
       if(equipModel.equals(NO_TERMINAL_SELECTED))
       {
         addError("Please select a Terminal Model Desc");
       }
     }

     if((termAppProfGen.equals(mesConstants.MMS_STAGE_BUILD_TERM_APP) || termAppProfGen.equals("DIALPAY")) && !equipModel.equals(NO_TERMINAL_SELECTED) && !(profileGeneratorCode > mesConstants.PROFGEN_MMS))
     {
       addError("If selecting equipment, please select the specific terminal application for that equipment");
     }

     // verify the prof gen terminal app is supported by the profile generator
     {
        TermAppProfGenRelationship tapgr = new TermAppProfGenRelationship();
        if(!tapgr.doesProfGenSprtTermApp(profileGeneratorCode,termAppProfGen)) {
          addError("Terminal Application '"+termAppProfGen+"' is not supported by Profile Generator '"+EquipProfileGeneratorDictator.getEquipmentProfileGeneratorName(profileGeneratorCode)+"'.");
        }
     }

     // set mms term app (based on the prof gen and the prof gen term app)
     if(profileGeneratorCode!=mesConstants.PROFGEN_MMS)
       termApp = mesConstants.MMS_STAGE_BUILD_TERM_APP;
     else
       termApp = termAppProfGen;

     if(!isBlank(termApp) && !termApp.equals(mesConstants.MMS_STAGE_BUILD_TERM_APP) && !hasExpandedFields(termAppProfGen) && wasSentFtp())
     {
       addError(termAppProfGen + " Terminal Application can only be sent using the manual option");
     }

     if(isBlank(merchName))
     {
       addError("Please provide a Business Dba Name");
     }
     else if(merchName.length() > FIELD_LENGTH_MERCHNAME)
     {
       addError("Business Name (DBA) is too long.  Truncate to " + FIELD_LENGTH_MERCHNAME + " characters.");
     }

     if(isBlank(merchAddress))
     {
       addError("Please provide a Business Street Address");
     }
     else if(merchAddress.length() > FIELD_LENGTH_ADDRESS)
     {
       addError("Business Street Address is too long.  Truncate to "  + FIELD_LENGTH_ADDRESS + " characters.");
     }

     if(isBlank(merchCity))
     {
       addError("Please provide the City of your business address");
     }
     else if(merchCity.length() > FIELD_LENGTH_CITY)
     {
       addError("Business City is too long.  Truncate to 13 characters.");
     }
     
     if(isBlank(merchState))
     {
       addError("Please select the State of your business address");
     }
     
     if(isBlank(merchZip))
     {
       addError("Please provide the 5-digit zip code of your business address");
     }
     else if (countDigits(merchZip) != 5)
     {
       addError("Please provide a valid 5-digit business zip code.  e.g. (12345)");
     }
     else if(!isBlank(merchState)) //if zip and state present validate zipcode for state
     {
       merchZip = getDigitsOnly(merchZip);

       if(!validZipForState(merchState,merchZip.substring(0,5)))
       {
         addError("Business Zip Code " + merchZip + " is not a valid zip for Business State " + merchState);
       }
       else
       {
         if(merchZip.length() == 9)
         {
           merchZip = merchZip.substring(0,5) + "-" + merchZip.substring(5);
         }
       }

     }

     if(isBlank(timeZone))
     {
       addError("Please select a Time Zone for this merchant");
     }

     if(isBlank(dst))
     {
       addError("Please select whether or not they participate in day light savings time");
     }

     if(!isBlank(contactName) && contactName.length() > FIELD_LENGTH_CONTACTNAME)
     {
       addError("Contact name is too long, truncate to "+FIELD_LENGTH_CONTACTNAME+" characters");
     }

     if(isBlank(mcfs))
     {
       addError("Please provide an MCFS number");
     }
     else if(mcfs.length() != FIELD_LENGTH_MCFS)
     {
       addError("MCFS Number must be " + FIELD_LENGTH_MCFS + " digits long");
     }

              
     if(isBlank(customerServiceNum) || countDigits(customerServiceNum) != FIELD_LENGTH_CUSTOMERSERVICENUM)
     {
       addError("Please provide a 10 digit customer service phone number that appears on the customer billing statement");
     }
     else //we get digits only, we reformat when we display it
     {
       customerServiceNum = getDigitsOnly(customerServiceNum);
     }

     if(isBlank(merchPhone))
     {
       addError("Please provide your Merchant Phone Number");
     }
     else if (countDigits(merchPhone) != 10)
     {
       addError("Merchant Phone number must be a valid 10-digit number including area code. e.g. (4159991212)");
     }
     else //we get digits only, we reformat when we display it
     {
       merchPhone = getDigitsOnly(merchPhone);
     }

   
     if(isBlank(merchNum))
     {
       addError("Please provide the Merchant Number");
     }
     else //no fomatting 
     {
       merchNum = getDigitsOnly(merchNum);

       if(merchNum.length() > FIELD_LENGTH_MERCHNUM)
       {
         addError("Merchant Number is too long.  Truncate to " + FIELD_LENGTH_MERCHNUM + " characters.");
       }
     }
     
     if(isBlank(mcc))
     {
       addError("Please select a MCC/SIC Code");
     }

     if(isBlank(binNum))
     {
       addError("Please select a bin number for this merchant");
     }

     if(isBlank(agentNum))
     {
       addError("Please provide an agent number for this merchant");
     }

     if(isBlank(chainNum))
     {
       addError("Please provide a chain number for this merchant");
     }
 
     if(isBlank(storeNum))
     {
       addError("Please provide a store number for this merchant");
     }
     else
     {
       storeNum = padWithZero(storeNum, 4);
     }

     if(isBlank(locationNum))
     {
       addError("Please provide a location number");
     }

     if(isBlank(terminalNum))
     {
       addError("Please provide a terminal number for this merchant");
     }
     else
     {
       terminalNum = padWithZero(terminalNum, 4);
       if(terminalNumUsed(merchNum, terminalNum, requestId))
       {
         addError("Terminal Number " + terminalNum + " is already being used by this merchant");
       }
     }
  
     if(isBlank(serviceLevel))
     {
       addError("Please provide a service level code");
     }

     if(isBlank(termVoiceId))
     {
       addError("Please provide a terminal voice id");
     }
     else if(termVoiceId.length() > FIELD_LENGTH_TERMVOICEID)
     {
       addError("Terminal Voice ID is too long.  Truncate to " + FIELD_LENGTH_TERMVOICEID + " characters.");
     }

     if(isBlank(voiceAuthPhone))
     {
       addError("Please provide a voice auth phone");
     }
     else if(voiceAuthPhone.length() > FIELD_LENGTH_VOICEAUTHPHONE)
     {
       addError("Voice auth phone is too long.  Truncate to " + FIELD_LENGTH_VOICEAUTHPHONE + " characters.");
     }

     if(!isBlank(discNum))
     {
       if(discNum.length() > FIELD_LENGTH_DISCNUM)
       {
         addError("Discover Number is too long.  Truncate to " + FIELD_LENGTH_DISCNUM + " characters.");
       }
       
       if(isBlank(discAc))
       {
         addError("Please select a discover type");
       }
     }
     else if(!isBlank(discAc))
     {
       addError("Must deselect Discover type or provide a Discover merchant number");
     }

     if(!isBlank(amexNum))
     {
       if(amexNum.length() > FIELD_LENGTH_AMEXNUM)
       {
         addError("Amex Number is too long.  Truncate to " + FIELD_LENGTH_AMEXNUM + " characters.");
       }

       if(isBlank(amexAc))
       {
         addError("Please select an amex type");
       }
       if(isBlank(amexSplit))
       {
         addError("Please specify whether amex split dial or not");
       }
     }
     else if(!isBlank(amexAc))
     {
       addError("Must deselect amex type or provide an amex merchant number");
     }
     else if(!isBlank(amexSplit))
     {
       addError("Must deselect amex split dial or provide an amex merchant number and amex type");
     }

     if(!isBlank(dinersNum))
     {
       if(dinersNum.length() > FIELD_LENGTH_DINERSNUM)
       {
         addError("Diners Number is too long.  Truncate to " + FIELD_LENGTH_DINERSNUM + " characters.");
       }

       if(isBlank(dinersAc))
       {
         addError("Please select a diners type");
       }
     }
     else if(!isBlank(dinersAc))
     {
       addError("Must deselect Diners type or provide a Diners merchant number");
     }


     if(!isBlank(jcbNum))
     {
       if(jcbNum.length() > FIELD_LENGTH_JCBNUM)
       {
         addError("JCB Number is too long.  Truncate to " + FIELD_LENGTH_JCBNUM + " characters.");
       }
       if(isBlank(jcbAc))
       {
         addError("Please select a JCB type");
       }
     }
     else if(!isBlank(jcbAc))
     {
       addError("Must deselect JCB type or provide a JCB merchant number");
     }

    return (!hasErrors());
  }

  private String padWithZero(String var, int len)
  {
    while(var.length() < len)
    {
      var = "0" + var;
    }

    return var;
  }

  private boolean terminalNumUsed(String mNum, String tNum, long reqId)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             result  = false;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:2792^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  terminal_number,request_id
//          from    mms_stage_info
//          where   merch_number = :mNum and terminal_number = :tNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  terminal_number,request_id\n        from    mms_stage_info\n        where   merch_number =  :1  and terminal_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"45com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,mNum);
   __sJT_st.setString(2,tNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"45com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2797^7*/
    
      rs = it.getResultSet();

      if(rs.next() && rs.getLong("request_id") != reqId)
      {
        result = true;
      }
 
      it.close();
    }
    catch(Exception e)
    {
      result = false;
      logEntry("terminalNumUsed()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  private boolean validZipForState(String state, String zip)
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             result  = false;
    int                 zipNum  = 0;

    try
    {
      zipNum = Integer.parseInt(zip);

      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:2834^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  zip_low,zip_high
//          from    RAPP_APP_VALID_ZIPCODES
//          where   state_code = :state
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  zip_low,zip_high\n        from    RAPP_APP_VALID_ZIPCODES\n        where   state_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"46com.mes.ops.MmsStageSetupBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,state);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"46com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2839^7*/
    
      rs = it.getResultSet();

      if(rs.next())
      {
        int low   = rs.getInt("zip_low");
        int high  = rs.getInt("zip_high");
 
        if(zipNum >= low && zipNum <= high)
        {
          result = true;
        }
      }
 
      it.close();
    
      cleanUp();
    }
    catch(Exception e)
    {
      result = false;
      logEntry("validZipForState()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  /*
  ** ACCESSORS
  */

  public  String  getBinNum()
  {
    return this.binNum;
  }
  public  String  getAgentNum()
  {
    return this.agentNum;
  }
  public String   getChainNum()      
  {
    return this.chainNum;
  }
  public String   getMerchNum()
  {
    return this.merchNum;
  }
  public String   getLocationNum()
  {
    return this.locationNum;
  }
  public String   getStoreNum()
  {
    return this.storeNum;
  }
  public String   getTerminalNum()
  {
    return this.terminalNum;
  }
  public String   getEquipModel()
  {
    return this.equipModel;
  }
  
  public String   getEquipModelRefurb()
  {
    return this.equipModelRefurb;
  }

  public int      getProfileGeneratorCode()
  {
    return this.profileGeneratorCode;
  }
  public String   getProfileGeneratorName()
  {
    return EquipProfileGeneratorDictator.getEquipmentProfileGeneratorName(this.profileGeneratorCode);
  }
  public String   getMerchName()
  {
    return this.merchName;
  }
  public String   getMerchAddress()
  {
    return this.merchAddress;
  }
  public String   getMerchCity()
  {
    return this.merchCity;
  }
  public String   getMerchState()
  {
    return this.merchState;
  }
  public String   getMerchZip()
  {
    return this.merchZip;
  }
  public String   getMerchPhone()
  {
    return this.merchPhone;
  }
  public String   getServiceLevel()
  {
    return this.serviceLevel;
  }
  public String   getAttachCode()
  {
    return this.attachCode;
  }
  public String   getMcc()
  {
    return this.mcc;
  }
  public String   getTermApp()
  {
    return this.termApp;
  }
  public String   getTermAppProfGen()
  {
    return this.termAppProfGen;
  }
  public String   getDiscNum()
  {
    return this.discNum;
  }
  public String   getDiscAc()
  {
    return this.discAc;
  }
  public String   getAmexNum()
  {
    return this.amexNum;
  }
  public String   getAmexAc()
  {
    return this.amexAc;
  }
  public String   getAmexSplit()
  {
    return this.amexSplit;
  }
  public String   getDinersNum()
  {
    return this.dinersNum;
  }
  public String   getDinersAc()
  {
    return this.dinersAc;
  }
  public String   getJcbNum()
  {
    return this.jcbNum;
  }
  public String   getJcbAc()
  {
    return this.jcbAc;
  }
  public String   getTermVoiceId()
  {
    return this.termVoiceId;
  }
  public String   getVoiceAuthPhone()
  {
    return this.voiceAuthPhone;
  }
  public String   getCdSet()
  {
    return this.cdSet;
  }
  
  public String getSpecialHandling()
  {
    return this.specialHandling;
  }

  public String getTimeZone()
  {
    return this.timeZone;
  }

  public String getDst()
  {
    return this.dst;
  }
  public String getContactName()
  {
    return this.contactName;
  }
  public String getProjectNum()
  {
    return this.projectNum;
  }
  public String getMcfs()
  {
    return this.mcfs;
  }
  public String getCustomerServiceNum()
  {
    return this.customerServiceNum;
  }

  //setters

  public void setDst(String dst)
  {
    this.dst = dst;
  }
  public void setContactName(String contactName)
  {
    this.contactName = contactName;
  }
  public void setProjectNum(String projectNum)
  {
    this.projectNum = projectNum;
  }
  public void setMcfs(String mcfs)
  {
    this.mcfs = mcfs;
  }
  public void setCustomerServiceNum(String customerServiceNum)
  {
    this.customerServiceNum = customerServiceNum;
  }

  public void setTimeZone(String timeZone)
  {
    this.timeZone = timeZone;
  }

  public void setSpecialHandling(String specialHandling)
  {
    this.specialHandling = specialHandling;
  }
   
  public  void  setBinNum(String binNum)
  {
    this.binNum = binNum;
  }
  public  void  setAgentNum(String agentNum)
  {
    this.agentNum = agentNum;
  }
  public void   setChainNum(String chainNum)
  {
    this.chainNum = chainNum;
  }
  public void   setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public void   setLocationNum(String locationNum)
  {
    this.locationNum = locationNum;
  }
  public void   setStoreNum(String storeNum)
  {
    this.storeNum = storeNum;
  }
  public void   setTerminalNum(String terminalNum)
  {
    this.terminalNum = terminalNum;
  }
  public void setEquipModel(String equipModel)
  {
    this.equipModel = equipModel;
  }
  public void setEquipModelRefurb(String equipModelRefurb)
  {
    this.equipModelRefurb = equipModelRefurb;
  }
  public void setProfileGeneratorCode(int profileGeneratorCode)
  {
    this.profileGeneratorCode = profileGeneratorCode;
  }
  public void   setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public void   setMerchAddress(String merchAddress)
  {
    this.merchAddress = merchAddress;
  }
  public void   setMerchCity(String merchCity)
  {
    this.merchCity = merchCity;
  }
  public void   setMerchState(String merchState)
  {
    this.merchState = merchState;
  }
  public void   setMerchZip(String merchZip)
  {
    this.merchZip = merchZip;
  }
  public void   setMerchPhone(String merchPhone)
  {
    this.merchPhone = merchPhone;
  }
  public void   setServiceLevel(String serviceLevel)
  {
    this.serviceLevel = serviceLevel;
  }
  public void   setAttachCode(String attachCode)
  {
    this.attachCode = attachCode;
  }
  public void   setMcc(String mcc)
  {
    this.mcc = mcc;
  }
  public void   setTermApp(String termApp)
  {
    this.termApp = termApp;
  }
  public void   setTermAppProfGen(String termAppProfGen)
  {
    this.termAppProfGen = termAppProfGen;
  }
  public void   setDiscNum(String discNum)
  {
    this.discNum = discNum;
  }
  public void   setDiscAc(String discAc)
  {
    this.discAc = discAc;
  }
  public void   setAmexNum(String amexNum)
  {
    this.amexNum = amexNum;
  }
  public void   setAmexAc(String amexAc)
  {
    this.amexAc = amexAc;
  }
  public void   setAmexSplit(String amexSplit)
  {
    this.amexSplit = amexSplit;
  }
  public void   setDinersNum(String dinersNum)
  {
    this.dinersNum = dinersNum;
  }
  public void   setDinersAc(String dinersAc)
  {
    this.dinersAc = dinersAc;
  }
  public void   setJcbNum(String jcbNum)
  {
    this.jcbNum = jcbNum;
  }
  public void   setJcbAc(String jcbAc)
  {
    this.jcbAc = jcbAc;
  }
  public void   setTermVoiceId(String termVoiceId)
  {
    this.termVoiceId = termVoiceId;
  }
  public void   setVoiceAuthPhone(String voiceAuthPhone)
  {
    this.voiceAuthPhone = voiceAuthPhone;
  }
  public void   setCdSet(String cdSet)
  {
    this.cdSet = cdSet;
  }

  public int getAppType()
  {
    return this.appType;
  }

  public void setAppType(String appType)
  {
    try
    {
      this.appType = Integer.parseInt(appType);
    }
    catch(Exception e)
    {
      this.appType = -1;      
    }
  }

  public String getAppTypeDesc()
  {
    return this.appTypeDesc;
  }

  public void setAppTypeDesc(String appTypeDesc)
  {
    this.appTypeDesc = appTypeDesc;
  }

  public String getAppTypeNonDeploy()
  {
    return this.appTypeNonDeploy;
  }

  public void setAppTypeNonDeploy(String appTypeNonDeploy)
  {
    this.appTypeNonDeploy = appTypeNonDeploy;
  }


  public long getPrimaryKey()
  {
    return this.primaryKey;
  }
  
  public void setPrimaryKey(String primaryKey)
  {
    try
    {
      this.primaryKey = Long.parseLong(primaryKey);
    }
    catch(Exception e)
    {
      addError("Error: Please exit and restart application - Primary key not valid");
      this.primaryKey = 0L;
    }
  }

  public long getRequestId()
  {
    return this.requestId;
  }
  
  public void setRequestId(String requestId)
  {
    try
    {
      this.requestId = Long.parseLong(requestId);
    }
    catch(Exception e)
    {
      addError("Error: Please exit and restart application - Request Id not valid");
      this.requestId = 0L;
    }
  }


/* get all status info */
  public String getDateTimeOfTransmission()
  {
    return formatResponseData(dateTimeOfTransmission);
  }
  public String getDateTimeOfResponse()
  {
    return formatResponseData(dateTimeOfResponse);
  }

  private String formatResponseData(String data)
  {
    if(isBlank(data))
    {
      return "----";
    }

    return data;
  }



/* manual changes variables*/
  public String getManVnum()
  {
    return this.manVnum;
  }

  public void setManVnum(String manVnum)
  {
    this.manVnum = manVnum;
  }

  public String getManStatus()
  {
    return this.manStatus;
  }
  public void setManStatus(String manStatus)
  {
    this.manStatus = manStatus;
  }

  public String getManResponse()
  {
    return this.manResponse;
  }
  public void setManResponse(String manResponse)
  {
    this.manResponse = manResponse;
  }

  public boolean isSubmitted()
  {
    return this.submitted;
  }

  public boolean isErrorStatus()
  {
    return this.errorStatus;
  }

  public boolean showExpandedFields()
  {
    return this.expandedFields;
  }

  public boolean isOkToGetGets()
  {
    return this.okToGetGets;
  }

  public void setExpandedFields(String temp)
  {
    this.expandedFields = true;
  }

  public boolean isReSubmitted()
  {
    return this.reSubmitted;
  }

  public boolean isManSubmitted()
  {
    return this.manSubmitted;
  }
  
  public boolean isAllowManUpdate()
  {
    return this.allowManUpdate;
  }

  public boolean isShowTimestamps()
  {
    return this.showTimestamps;
  }

  public void dontAllowManUpdate()
  {
    this.allowManUpdate = false;
  }
  
  public void setAllowManUpdate(String temp)
  {
    this.allowManUpdate = true;
  }

  public boolean isQueuedForTrans()
  {
    return this.queuedForTrans;
  }

  public void setSubmit(String temp)
  {
    this.submitted = true;
  }
  
  public void setReSubmit(String temp)
  {
    this.reSubmitted = true;
  }

  public void setManSubmit(String temp)
  {
    this.manSubmitted = true;
  }

  public void setTransSubmit(String temp)
  {
    this.transmissionMethod = mesConstants.ETPPM_FTP;
    this.submitted = true;
  }
  public void setManualSubmit(String temp)
  {
    this.transmissionMethod = mesConstants.ETPPM_MANUAL;
    this.submitted = true;
  }

  public boolean wasSentFtp()
  {
    return this.transmissionMethod.equals(mesConstants.ETPPM_FTP);
  }

  public boolean wasSentManual()
  {
    return this.transmissionMethod.equals(mesConstants.ETPPM_MANUAL);
  }


  public String getTransmissionMethod()
  {
    return this.transmissionMethod;
  }

  public String getTransmissionStatus()
  {
    return formatResponseData(this.transmissionStatus);
  }
  public String getTransmissionResponse()
  {
    return formatResponseData(this.transmissionResponse);
  }

  public String getVnum()
  {
    return formatResponseData(this.vNum);
  }

  public String getErrorDescriptions()
  {
    return this.errorDescriptions;
  }

  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }

  public void addError(String err)
  {
    try
    {
      errors.add(err);
    }
    catch(Exception e)
    {
    }
  } 

  public boolean hasMsgs()
  {
    return(msgs.size() > 0);
  }

  public void addMsg(String msg)
  {
    try
    {
      msgs.add(msg);
    }
    catch(Exception e)
    {
    }
  } 

  private String glueDate(String dateStr, String timeStr)
  {
    String result = "";

    if(!isBlank(dateStr) && !isBlank(timeStr))
    {
      result = dateStr + " " + timeStr;
    }
    else if(!isBlank(dateStr))
    {
      result = dateStr;
    }
    
    return result;

  }

  public boolean isBlank(String test)
  {
    boolean pass = false;
    
    if(test == null || test.equals("") || test.length() == 0)
    {
      pass = true;
    }
    
    return pass;
  }
  
  public boolean isNumber(String test)
  {
    boolean pass = false;
    
    try
    {
      long i = Long.parseLong(test);
      pass = true;
    }
    catch(Exception e)
    {
    }
    
    return pass;
  }

  public boolean isRealNumber(String test)
  {
    boolean pass = false;
    
    try
    {
      double i = Double.parseDouble(test);
      pass = true;
    }
    catch(Exception e)
    {
    }
    
    return pass;
  }

  public long getDigits(String raw)
  {
    long          result      = 0L;
    StringBuffer  digits      = new StringBuffer("");
    
    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
      }
      
      result = Long.parseLong(digits.toString());
    }
    catch(Exception e)
    {
      if(raw != null && !raw.equals(""))
      {
        com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getDigits('" + raw + "'): " + e.toString());
      }
    }
    
    return result;
  }

  public String getDigitsOnly(String raw)
  {
    StringBuffer  digits      = new StringBuffer("");
    String        result      = "";

    try
    {
      for(int i=0; i<raw.length(); ++i)
      {
        if(Character.isDigit(raw.charAt(i)))
        {
          digits.append(raw.charAt(i));
        }
      }
      
      result = digits.toString();
    }
    catch(Exception e)
    {}
    
    return result;
  }

  
  public int countDigits(String raw)
  {
    int result = 0;
    
    for(int i=0; i<raw.length(); ++i)
    {
      if(Character.isDigit(raw.charAt(i)))
      {
        ++result;
      }
    }
    
    return result;
  }
  
  public boolean isCheckAccepted()
  {
    return( CheckAccepted );
  }
  
  public String getCheckProviderName( )
  {
    return( CheckProvider );
  }
  
  public String getCheckMerchId( )
  {
    return( CheckMerchId );
  }
  
  public String getCheckTermId( )
  {
    return( CheckTermId );
  }
  
  public boolean isValutecAccepted()
  {
    return( ValutecAccepted );
  }
  
  public String getValutecMerchId( )
  {
    return( ValutecMerchId );
  }
  
  public String getValutecTermId( )
  {
    return( ValutecTermId );
  }

  public boolean isEmail(String test)
  {
    boolean pass = false;
    
    int firstAt = test.indexOf('@');
    if (!isBlank(test) && firstAt > 0 && 
        test.indexOf('@',firstAt + 1) == -1)
    {
      pass = true;
    }
    
    return pass;
  }

  private String formatPhone(String phone)
  {
    if(isBlank(phone))
    {
      return "";
    }
    if(phone.length() != 10)
    {
      return "";
    }
    String areaCode   = phone.substring(0,3);
    String firstThree = phone.substring(3,6);
    String lastFour   = phone.substring(6);
    return ("(" + areaCode + ")" + " " + firstThree + "-" + lastFour);
  }

  private String formatSsn(String ssn)
  {
    if(isBlank(ssn))
    {
      return "";
    }
    if(ssn.length() != 9)
    {
      return ssn;
    }
    String part1   = ssn.substring(0,3);
    String part2   = ssn.substring(3,5);
    String part3   = ssn.substring(5);
    return (part1 + "-" + part2 + "-" + part3);
  }

  /*
  ** METHOD setReqId
  */
  private long setReqId()
  {
    ResultSetIterator   it            = null;
    
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:3710^7*/

//  ************************************************************
//  #sql [Ctx] it = { select   mms_application_sequence.nextval reqId
//          from     dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   mms_application_sequence.nextval reqId\n        from     dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"47com.mes.ops.MmsStageSetupBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"47com.mes.ops.MmsStageSetupBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3714^7*/
    
      ResultSet rs = it.getResultSet();
 
      if(rs.next())
      {
        this.requestId = rs.getLong("reqId");
      }
    }
    catch(Exception e)
    {
      logEntry("(setReqId)", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return this.requestId;
  }

/*
  private void updateRetransmissionStatus(long pk)
  {
    boolean useCleanUp = false;
    
    try
    {
      int status = QueueConstants.DEPT_STATUS_DISCOVER_RAP_RETRANSMISSION;

      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
     
      #sql [Ctx]
      {
        update  app_tracking
        set     status_code   = :status
        where   app_seq_num   = :pk and
                dept_code     = :(QueueConstants.DEPARTMENT_DISCOVER_RAP)
      };
    }
    catch(Exception e)
    {
      logEntry("updateRetransmissionStatus()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  }
*/


/*
  private void updateTransmissionStatus(long pk)
  {
    boolean useCleanUp = false;
    
    try
    {
      int status = 0;

      if(wasSentViaTransmission())
      {
        status = QueueConstants.DEPT_STATUS_DISCOVER_RAP_TRANSMISSION;
      }
      else
      {
        status = QueueConstants.DEPT_STATUS_DISCOVER_RAP_FAX_TO_DISCOVER;
      }      

      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
     

      if(!trackRecordExists(pk, QueueConstants.DEPARTMENT_DISCOVER_RAP))
      {
        //connect();
        #sql [Ctx]
        {
          insert into app_tracking
          (
            app_seq_num,
            dept_code,
            status_code
          )
          values
          (
            :pk,
            :(QueueConstants.DEPARTMENT_DISCOVER_RAP),
            :status
          )
        };
      }
      else  
      {
        //connect();
        #sql [Ctx]
        {
          update  app_tracking
          set     status_code   = :status
          where   app_seq_num   = :pk and
                  dept_code     = :(QueueConstants.DEPARTMENT_DISCOVER_RAP)
        };
      }
    }
    catch(Exception e)
    {
      logEntry("updateTransmissionStatus()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  }
*/

/*
  private void updateResponseStatus(long pk)
  {
    
    boolean useCleanUp = false;

    try
    {
      int status = 0;

      if(manDecision.equals(DECISION_STATUS_APPROVED))
      {
        status = QueueConstants.DEPT_STATUS_DISCOVER_RAP_LIVE_MANUAL;
      }
      else if(manDecision.equals(DECISION_STATUS_PENDING))
      {
        status = QueueConstants.DEPT_STATUS_DISCOVER_RAP_PENDING;
      }
      else if(manDecision.equals(DECISION_STATUS_DECLINED))
      {
        status = QueueConstants.DEPT_STATUS_DISCOVER_RAP_DECLINED;
      }      

      if(isConnectionStale())
      {
        connect();
        useCleanUp = true;
      }
      
      if(!trackRecordExists(pk, QueueConstants.DEPARTMENT_DISCOVER_RAP))
      {
        //connect();
        #sql [Ctx]
        {
          insert into app_tracking
          (
            app_seq_num,
            dept_code,
            status_code
          )
          values
          (
            :pk,
            :(QueueConstants.DEPARTMENT_DISCOVER_RAP),
            :status
          )
        };
      }
      else  
      {
        //connect();
        #sql [Ctx]
        {
          update  app_tracking
          set     status_code   = :status
          where   app_seq_num   = :pk and
                  dept_code     = :(QueueConstants.DEPARTMENT_DISCOVER_RAP)
        };
      }
    }
    catch(Exception e)
    {
      logEntry("updateResponseStatus()", e.toString());
    }
    finally
    {
      if(useCleanUp)
      {
        cleanUp();
      }
    }
  }
*/


}/*@lineinfo:generated-code*/