/*@lineinfo:filename=VNumberBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/VNumberBean.sqlj $

  Description:


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.Vector;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import com.mes.tools.AppNotifyBean;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class VNumberBean extends com.mes.database.SQLJConnectionBase
{
  private Vector          errors            = new Vector();

  private String          submitVal         = null;

  private String          loginName         = "";

  private long            recId             = 0L;
  private long            appSeqNum         = -1L;
  private int             posCode           = -1;
  private String          equipModel        = null;
  private int             modelIndex        = -1;

  private String          vNumber           = null;
  private String          tid               = null;
  private int             terminalNumber    = -1;
  private int             storeNumber       = -1;
  private int             locationNumber    = -1;
  private int             timezone          = -1;
  private String          appCode           = null;
  private String          commType          = "-1";
  private String          host              = Integer.toString(mesConstants.HOST_VITAL);

  private long            requestId         = -1;

  // default field values
  private int             dfltStore         = -1;
  private int             dfltLocation      = -1;
  private int             dfltZone          = -1;
  private int             dfltTerminal      = -1;
  private String          dfltAppCode       = null;
  private long            dfltAppSeqNum     = -1L;

  // add slot fields
  private int             newPosType        = 1;
  private String          newDialTerminal   = null;
  private String          newInternetPos    = null;
  private String          newPCPos          = null;

  /*
  ** ACCESSORS
  */
  public int getNewPosType()
  {
    return newPosType;
  }
  public void setNewPosType(String newPosType)
  {
    try
    {
      setNewPosType(Integer.parseInt(newPosType));
    }
    catch(Exception e)
    {
      logEntry("setNewPosType(" + newPosType + ")", e.toString());
    }
  }
  public void setNewPosType(int newPosType)
  {
    this.newPosType = newPosType;
  }
  public String getNewPosTypeSelected(int idx)
  {
    return (newPosType == idx ? "checked" : "");
  }

  public String getNewDialTerminal()
  {
    return newDialTerminal;
  }
  public void setNewDialTerminal(String newDialTerminal)
  {
    this.newDialTerminal = newDialTerminal;
  }

  public String getNewInternetPos()
  {
    return newInternetPos;
  }
  public void setNewInternetPos(String newInternetPos)
  {
    this.newInternetPos = newInternetPos;
  }

  public String getNewPCPos()
  {
    return newPCPos;
  }
  public void setNewPCPos(String newPCPos)
  {
    this.newPCPos = newPCPos;
  }

  public long getAppSeqNum()
  {
    return appSeqNum;
  }
  public void setAppSeqNum(String appSeqNum)
  {
    try
    {
      setAppSeqNum(Long.parseLong(appSeqNum));
    }
    catch(Exception e)
    {
      logEntry("setAppSeqNum(" + appSeqNum + ")", e.toString());
    }
  }
  public void setAppSeqNum(long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
  }

  public long getPrimaryKey()
  {
    return getAppSeqNum();
  }
  public void setPrimaryKey(String primaryKey)
  {
    try
    {
      setPrimaryKey(Long.parseLong(primaryKey));
    }
    catch(Exception e)
    {
      logEntry("setPrimaryKey(" + primaryKey + ")", e.toString());
    }
  }
  public void setPrimaryKey(long primaryKey)
  {
    setAppSeqNum(primaryKey);
  }

  public String getRecId()
  {
    return Long.toString(recId);
  }
  public void setRecid(String newRecId)
  {
    try
    {
      recId = Long.parseLong(newRecId);
    }
    catch(Exception e)
    {
      recId = 0L;
    }
  }
  public String getEquipModel()
  {
    return equipModel;
  }
  public void setEquipModel(String equipModel)
  {
    this.equipModel = equipModel;
  }

  public int getModelIndex()
  {
    return modelIndex;
  }
  public void setModelIndex(String modelIndex)
  {
    try
    {
      setModelIndex(Integer.parseInt(modelIndex));
    }
    catch(Exception e)
    {
      logEntry("setModelIndex(" + modelIndex + ")", e.toString());
    }
  }
  public void setModelIndex(int modelIndex)
  {
    this.modelIndex = modelIndex;
  }

  public String getAppCode()
  {
    return appCode;
  }
  public void setAppCode(String appCode)
  {
    this.appCode = appCode;
  }

  public String getCommType()
  {
    return commType;
  }
  public void setCommType(String commType)
  {
    this.commType = commType;
  }

  public String getHost()
  {
    return host;
  }
  public void setHost(String host)
  {
    this.host = host;
  }

  public long getRequestId()
  {
    return requestId;
  }
  public void setRequestId(long requestId)
  {
    this.requestId = requestId;
  }

  public String getTerminalNumber()
  {
    DecimalFormat formatter = new DecimalFormat("0000");
    return formatter.format(terminalNumber);
  }
  public void setTerminalNumber(String terminalNumber)
  {
    try
    {
      this.terminalNumber = Integer.parseInt(terminalNumber);
    }
    catch(Exception e)
    {
      this.terminalNumber = -1;
    }
  }

  public int getPosCode()
  {
    return posCode;
  }
  public void setPosCode(String posCode)
  {
    try
    {
      setPosCode(Integer.parseInt(posCode));
    }
    catch(Exception e)
    {
      logEntry("setPosCode(" + posCode + ")", e.toString());
    }
  }
  public void setPosCode(int posCode)
  {
    this.posCode = posCode;
  }

  public String getVnumber()
  {
    return this.vNumber;
  }
  public String getTid()
  {
    return this.tid;
  }
  public void setVnumber(String vNumber)
  {
    this.vNumber = vNumber;
  }
  public void setTid(String v)
  {
    this.tid=v;
  }

  public String getStoreNumber()
  {
    DecimalFormat formatter = new DecimalFormat("0000");
    return formatter.format(storeNumber);
  }
  public void setStoreNumber(String newStoreNumber)
  {
    try
    {
      storeNumber = Integer.parseInt(newStoreNumber);
    }
    catch (Exception e)
    {
      storeNumber = -1;
    }
  }

  public String getTimezone()
  {
    DecimalFormat formatter = new DecimalFormat("000");
    return formatter.format(timezone);
  }
  public void setTimezone(String newTimezone)
  {
    try
    {
      timezone = Integer.parseInt(newTimezone);
    }
    catch (Exception e)
    {
      timezone = -1;
    }
  }

  public String getLocationNumber()
  {
    DecimalFormat formatter = new DecimalFormat("00000");
    return formatter.format(locationNumber);
  }
  public void setLocationNumber(String newLocationNumber)
  {
    try
    {
      locationNumber = Integer.parseInt(newLocationNumber);
    }
    catch (Exception e)
    {
      locationNumber = -1;
    }
  }

  public String getSubmitVal()
  {
    return submitVal;
  }
  public void setSubmitVal(String submitVal)
  {
    this.submitVal = submitVal;
  }

  public void setLoginName(String loginName)
  {
    this.loginName = loginName;
  }

  public int getDfltTerminal()
  {
    return dfltTerminal;
  }

  /*
  ** private void loadDefaults() throws Exception
  **
  ** Loads some default values for new v_number records.  The default terminal
  ** number is set to the current highest terminal number plus one.  The default
  ** app seq num is updated to show that defaults have been loaded for the
  ** current app seq num.
  */
  private void loadDefaults() throws Exception
  {
    // determine the next available terminal number
    try
    {
      // load some default values to populate new slots with
      dfltStore     = MesDefaults.getInt(MesDefaults.DK_STORE_NUMBER);
      dfltLocation  = MesDefaults.getInt(MesDefaults.DK_LOCATION_NUMBER);
      dfltZone      = MesDefaults.getInt(MesDefaults.DK_TIME_ZONE);
      dfltAppCode   = null;

      /*@lineinfo:generated-code*//*@lineinfo:399^7*/

//  ************************************************************
//  #sql [Ctx] { select  max(terminal_number) 
//          from    v_numbers
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max(terminal_number)  \n        from    v_numbers\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.VNumberBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   dfltTerminal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:404^7*/
      ++dfltTerminal;
    }
    catch(sqlj.runtime.SQLNullException sne)
    {
      dfltTerminal = 1;
    }

    // set the deftault app seq num to the current app seq num
    dfltAppSeqNum = appSeqNum;
  }

  /*
  ** public void createNewSlot(int slotPosCode, String slotEquipModel, int slotModelIndex)
  **
  ** Creates a new v_number record for the current app seq num with the
  ** given pos type, equipment model and model index number and populates the
  ** record with default values.
  */
  protected long createNewSlot(int slotPosCode, String slotEquipModel, int slotModelIndex, boolean autoFlag, String newAppCode)
  {
    long newRecId = 0L;

    try
    {
      // make sure default values are available
      if (dfltAppSeqNum != appSeqNum)
      {
        loadDefaults();
      }

      // determine if any vnumbers in merch_vnumber are available
      // if this is an auto generated slot
      boolean reloadDefaults = false;
      String newVNumber = null;
      String newTid = null;
      if (autoFlag)
      {
        // look for pre-existing vnumbers in merch_vnumber that don't
        // conflict with vnumbers and terminal numbers already in v_numbers
        ResultSetIterator it = null;
        /*@lineinfo:generated-code*//*@lineinfo:445^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  terminal_number,
//                    vnumber,
//                    tid,
//                    store_number,
//                    time_zone,
//                    location_number
//            from    merch_vnumber
//            where   terminal_number not in
//                      ( select  nvl(terminal_number,0)
//                        from    v_numbers
//                        where   app_seq_num = :appSeqNum )
//                    and vnumber not in
//                      ( select  nvl(v_number,'0')
//                        from    v_numbers
//                        where   app_seq_num = :appSeqNum )
//                    and app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  terminal_number,\n                  vnumber,\n                  tid,\n                  store_number,\n                  time_zone,\n                  location_number\n          from    merch_vnumber\n          where   terminal_number not in\n                    ( select  nvl(terminal_number,0)\n                      from    v_numbers\n                      where   app_seq_num =  :1  )\n                  and vnumber not in\n                    ( select  nvl(v_number,'0')\n                      from    v_numbers\n                      where   app_seq_num =  :2  )\n                  and app_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.VNumberBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setLong(3,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.VNumberBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:463^9*/

        // if any were found, adjust the default values accordingly
        ResultSet rs = it.getResultSet();
        if (rs.next())
        {
          newVNumber      = rs.getString("vnumber");
          newTid          = rs.getString("tid");
          dfltTerminal    = rs.getInt   ("terminal_number");
          dfltStore       = rs.getInt   ("store_number");
          dfltLocation    = rs.getInt   ("location_number");
          dfltZone        = rs.getInt   ("time_zone");
          reloadDefaults  = true;
        }
      }

      String appCode = (newAppCode == null ? dfltAppCode : newAppCode);

      // get new rec id
      /*@lineinfo:generated-code*//*@lineinfo:482^7*/

//  ************************************************************
//  #sql [Ctx] { select  vnumber_rec_id_seq.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  vnumber_rec_id_seq.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.VNumberBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   newRecId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:487^7*/

      // insert new vnumber slot
      /*@lineinfo:generated-code*//*@lineinfo:490^7*/

//  ************************************************************
//  #sql [Ctx] { insert into v_numbers
//          (
//            app_seq_num,
//            v_number,
//            tid,
//            pos_code,
//            equip_model,
//            model_index,
//            terminal_number,
//            store_number,
//            time_zone,
//            location_number,
//            app_code,
//            auto_flag,
//            rec_id
//          )
//          values
//          (
//            :appSeqNum,
//            :newVNumber,
//            :newTid,
//            :slotPosCode,
//            :slotEquipModel,
//            :slotModelIndex,
//            :dfltTerminal++,
//            :dfltStore,
//            :dfltZone,
//            :dfltLocation,
//            :appCode,
//            :autoFlag ? "Y" : "N",
//            :newRecId
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_812 = dfltTerminal++;
 String __sJT_813 = autoFlag ? "Y" : "N";
   String theSqlTS = "insert into v_numbers\n        (\n          app_seq_num,\n          v_number,\n          tid,\n          pos_code,\n          equip_model,\n          model_index,\n          terminal_number,\n          store_number,\n          time_zone,\n          location_number,\n          app_code,\n          auto_flag,\n          rec_id\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6 ,\n           :7 ,\n           :8 ,\n           :9 ,\n           :10 ,\n           :11 ,\n           :12 ,\n           :13 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.VNumberBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,newVNumber);
   __sJT_st.setString(3,newTid);
   __sJT_st.setInt(4,slotPosCode);
   __sJT_st.setString(5,slotEquipModel);
   __sJT_st.setInt(6,slotModelIndex);
   __sJT_st.setInt(7,__sJT_812);
   __sJT_st.setInt(8,dfltStore);
   __sJT_st.setInt(9,dfltZone);
   __sJT_st.setInt(10,dfltLocation);
   __sJT_st.setString(11,appCode);
   __sJT_st.setString(12,__sJT_813);
   __sJT_st.setLong(13,newRecId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:524^7*/

      // check reload flag
      if (reloadDefaults)
      {
        loadDefaults();
      }
    }
    catch (Exception e)
    {
    }

    return newRecId;
  }

  /*
  ** public static void addSlots()
  **
  ** Static entry point for adding the "auto-generated" vnumber slots
  */
  public static void addSlots(long appSeqNum)
  {
    VNumberBean  vnb = new VNumberBean();
    try
    {
      vnb.setAppSeqNum(appSeqNum);
      vnb.connect();
      vnb.checkSlots();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("VNumberBean.addSlots(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      vnb.cleanUp();
    }
  }

  /*
  ** public void checkSlots()
  **
  ** Compares how many vnumber slots exists to how many are required by the
  ** application.  If too few slots exist the missing slots are created.
  */
  public void checkSlots()
  {
    try
    {
      // get quantities of slots required by pos code or terminal model type
      ResultSetIterator it = null;
      /*@lineinfo:generated-code*//*@lineinfo:575^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    mp.app_seq_num,
//                    mp.pos_code,
//                    e.equiptype_code,
//                    pc.pos_desc,
//                    nvl(me.equip_model,'na')            equip_model,
//                    nvl(me.merchequip_equip_quantity,1) app_count,
//                    nvl(vec1.model_count,0)
//                    + nvl(vec2.pos_count,0)             slot_count
//  
//          from      merch_pos       mp,
//                    pos_category    pc,
//                    equipment       e,
//                    ( select    e.*
//                      from      merchequipment e,
//                                merch_pos p
//                      where     e.app_seq_num = :appSeqNum
//                                and p.app_seq_num = e.app_seq_num
//                                and p.pos_code = 101
//                                and e.equiptype_code in ( 1, 5, 6 ) ) me,
//                    ( select    equip_model,
//                                count(model_index) model_count
//                      from      v_numbers
//                      where     app_seq_num = :appSeqNum
//                                and pos_code = 101
//                      group by  equip_model ) vec1,
//                    ( select    pos_code,
//                                count(model_index) pos_count
//                      from      v_numbers
//                      where     app_seq_num = :appSeqNum
//                                and pos_code <> 101
//                      group by  pos_code ) vec2
//  
//          where     mp.app_seq_num      = :appSeqNum
//                    and mp.pos_code     = pc.pos_code(+)
//                    and mp.app_seq_num  = me.app_seq_num(+)
//                    and mp.pos_code     = vec2.pos_code(+)
//                    and me.equip_model  = vec1.equip_model(+)
//                    and me.equip_model  = e.equip_model(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    mp.app_seq_num,\n                  mp.pos_code,\n                  e.equiptype_code,\n                  pc.pos_desc,\n                  nvl(me.equip_model,'na')            equip_model,\n                  nvl(me.merchequip_equip_quantity,1) app_count,\n                  nvl(vec1.model_count,0)\n                  + nvl(vec2.pos_count,0)             slot_count\n\n        from      merch_pos       mp,\n                  pos_category    pc,\n                  equipment       e,\n                  ( select    e.*\n                    from      merchequipment e,\n                              merch_pos p\n                    where     e.app_seq_num =  :1 \n                              and p.app_seq_num = e.app_seq_num\n                              and p.pos_code = 101\n                              and e.equiptype_code in ( 1, 5, 6 ) ) me,\n                  ( select    equip_model,\n                              count(model_index) model_count\n                    from      v_numbers\n                    where     app_seq_num =  :2 \n                              and pos_code = 101\n                    group by  equip_model ) vec1,\n                  ( select    pos_code,\n                              count(model_index) pos_count\n                    from      v_numbers\n                    where     app_seq_num =  :3 \n                              and pos_code <> 101\n                    group by  pos_code ) vec2\n\n        where     mp.app_seq_num      =  :4 \n                  and mp.pos_code     = pc.pos_code(+)\n                  and mp.app_seq_num  = me.app_seq_num(+)\n                  and mp.pos_code     = vec2.pos_code(+)\n                  and me.equip_model  = vec1.equip_model(+)\n                  and me.equip_model  = e.equip_model(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.VNumberBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,appSeqNum);
   __sJT_st.setLong(3,appSeqNum);
   __sJT_st.setLong(4,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.ops.VNumberBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:615^7*/

      // create slots for any pos codes/equipment model types that need them
      ResultSet rs = it.getResultSet();
      while (rs.next())
      {
        int     loopPosCode     = rs.getInt   ("pos_code");
        String  loopEquipModel  = rs.getString("equip_model");
        int     appCount        = rs.getInt   ("app_count");
        int     slotCount       = rs.getInt   ("slot_count");
        int     countDelta      = appCount - slotCount;

        if (countDelta > 0)
        {
          // determine the current maximum index
          // for the pos code/equipment model type
          int maxIndex = 0;
          /*@lineinfo:generated-code*//*@lineinfo:632^11*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(max(model_index),0) 
//              from    v_numbers
//              where   app_seq_num = :appSeqNum
//                      and equip_model = :loopEquipModel
//                      and pos_code = :loopPosCode
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(max(model_index),0)  \n            from    v_numbers\n            where   app_seq_num =  :1 \n                    and equip_model =  :2 \n                    and pos_code =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.VNumberBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,loopEquipModel);
   __sJT_st.setInt(3,loopPosCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   maxIndex = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:639^11*/
        }
      }
      it.close();
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::checkSlots()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      logEntry(func,desc);
    }
  }

  /*
  ** private boolean assignmentComplete()
  **
  ** Does a check to see if all v number slots have had v numbers assigned
  ** to them.
  **
  ** RETURNS: true if all slots are assigned, else false.
  */
  private boolean assignmentComplete()
  {
    boolean isComplete = false;
    try
    {
      // get a count of vnumbers that are unassigned
      int unassignedCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:667^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(*) 
//          from    v_numbers
//          where   app_seq_num = :appSeqNum
//                  and v_number is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(*)  \n        from    v_numbers\n        where   app_seq_num =  :1 \n                and v_number is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.ops.VNumberBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   unassignedCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:673^7*/
      isComplete = (unassignedCount == 0);
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::assignemntComplete()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      logEntry(func,desc);
    }

    return isComplete;
  }

  /*
  ** private void updateVNumberQueue()
  **
  ** Updates app_queue to indicate that the app seq num in question has
  ** completed v number assignment (moves the app from the v number pending
  ** to v number assigned status).
  */
  private void updateVNumberQueue()
  {
    try
    {
      // update the status
      /*@lineinfo:generated-code*//*@lineinfo:698^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_queue
//          set     app_queue_stage = :QueueConstants.Q_VNUMBER_ASSIGNED,
//                  app_status      = :QueueConstants.Q_STATUS_VNUMBER_ADDED,
//                  app_last_date   = sysdate,
//                  app_last_user   = :loginName
//          where   app_queue_type  = :QueueConstants.QUEUE_VNUMBER and
//                  app_seq_num     = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_queue\n        set     app_queue_stage =  :1 ,\n                app_status      =  :2 ,\n                app_last_date   = sysdate,\n                app_last_user   =  :3 \n        where   app_queue_type  =  :4  and\n                app_seq_num     =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.ops.VNumberBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,QueueConstants.Q_VNUMBER_ASSIGNED);
   __sJT_st.setInt(2,QueueConstants.Q_STATUS_VNUMBER_ADDED);
   __sJT_st.setString(3,loginName);
   __sJT_st.setInt(4,QueueConstants.QUEUE_VNUMBER);
   __sJT_st.setLong(5,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:707^7*/
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::updateVNumberQueue()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      logEntry(func,desc);
    }
  }

  /*
  ** public boolean entryExists(long appSeqNum, String tableName)
  **
  ** Used to determine if a record corresponding with a given app seq num
  ** exists in a table.
  **
  ** RETURNS: true if such a record exists, else false.
  */
  public boolean entryExists(long appSeqNum, String tableName)
  {
    long                  count   = 0L;
    PreparedStatement     ps      = null;
    ResultSet             rs      = null;
    StringBuffer          qs      = new StringBuffer("");

    try
    {
      qs.append("select count(app_seq_num) as count ");
      qs.append("from ");
      qs.append(tableName);
      qs.append(" where app_seq_num = ?");

      ps = Ctx.getConnection().prepareStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        count = rs.getInt("count");
      }

      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      String func = this.getClass().getName() + "::entryExists()";
      String desc = "appSeqNum = " + appSeqNum + ", tableName = "
        + tableName + ", " + e.toString();
      logEntry(func,desc);
    }

    return (count > 0);
  }



  /*
  ** public static void executeAppAccountComplete()
  **
  ** Static entry point for adding the "auto-generated" vnumber slots
  */
  public static void executeAppAccountComplete(long appSeqNum)
  {
    VNumberBean  vnb = new VNumberBean();
    try
    {
      vnb.setAppSeqNum(appSeqNum);
      vnb.setLoginName("SYSTEM");
      vnb.connect();
      vnb.updateAppAccountComplete();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("VNumberBean.executeAppAccountComplete(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      vnb.cleanUp();
    }
  }


  /*
  ** private void updateAppAccountComplete()
  **
  ** Determines if app_account_complete does not already contain an entry
  ** for this app seq num and if it does not then a new record is inserted
  ** for this app seq num and notification is made (xml message possibly sent).
  */
  public void updateAppAccountComplete()
  {
    try
    {
      if(!entryExists(appSeqNum,"app_account_complete"))
      {
        // update the vnumber queue status
        updateVNumberQueue();

        /*@lineinfo:generated-code*//*@lineinfo:807^9*/

//  ************************************************************
//  #sql [Ctx] { insert into app_account_complete
//            (
//              app_seq_num,
//              date_completed
//            )
//            values
//            (
//              :appSeqNum,
//              sysdate
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_account_complete\n          (\n            app_seq_num,\n            date_completed\n          )\n          values\n          (\n             :1 ,\n            sysdate\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.ops.VNumberBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:819^9*/

        // do notification
        AppNotifyBean nb = new AppNotifyBean();
        nb.notifyStatus(appSeqNum,mesConstants.APP_STATUS_SETUP_COMPLETE,-1);
      }
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::updateAppAccountComplete()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      logEntry(func,desc);
    }
  }

  public void updateMmsStageInfo()
  {
    if(requestId < 1)
      return;

    try {
      /*@lineinfo:generated-code*//*@lineinfo:840^7*/

//  ************************************************************
//  #sql [Ctx] { update  mms_stage_info
//          set     vnum            = :vNumber
//          where   request_id      = :requestId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mms_stage_info\n        set     vnum            =  :1 \n        where   request_id      =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.ops.VNumberBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,vNumber);
   __sJT_st.setLong(2,requestId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:845^7*/
    }
    catch(Exception e) {
      String func = this.getClass().getName() + "::updateMmsStageInfo()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      logEntry(func,desc);
    }
  }

  /*
  ** public void updateMerchVNumber(String curVNumber)
  **
  ** Updates the old merch_vnumber table with the new vnumber data.
  */
  public void updateMerchVNumber(String curVNumber)
  {
    try
    {
      // see if a vnumber record already exists
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:865^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(vnumber) 
//          from    merch_vnumber
//          where   app_seq_num = :appSeqNum
//                  and vnumber = :curVNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(vnumber)  \n        from    merch_vnumber\n        where   app_seq_num =  :1 \n                and vnumber =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.VNumberBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,curVNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:871^7*/

      if(recCount > 0)
      {
        // update the old record
        /*@lineinfo:generated-code*//*@lineinfo:876^9*/

//  ************************************************************
//  #sql [Ctx] { update  merch_vnumber
//            set     vnumber         = :vNumber,
//                    tid             = :tid,
//                    store_number    = :storeNumber,
//                    time_zone       = :timezone,
//                    location_number = :locationNumber,
//                    app_code        = :appCode,
//                    equip_model     = :equipModel,
//                    term_comm_type  = :commType,
//                    front_end       = :host
//            where   app_seq_num = :appSeqNum and
//                    vnumber = :curVNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merch_vnumber\n          set     vnumber         =  :1 ,\n                  tid             =  :2 ,\n                  store_number    =  :3 ,\n                  time_zone       =  :4 ,\n                  location_number =  :5 ,\n                  app_code        =  :6 ,\n                  equip_model     =  :7 ,\n                  term_comm_type  =  :8 ,\n                  front_end       =  :9 \n          where   app_seq_num =  :10  and\n                  vnumber =  :11";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.ops.VNumberBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,vNumber);
   __sJT_st.setString(2,tid);
   __sJT_st.setInt(3,storeNumber);
   __sJT_st.setInt(4,timezone);
   __sJT_st.setInt(5,locationNumber);
   __sJT_st.setString(6,appCode);
   __sJT_st.setString(7,equipModel);
   __sJT_st.setString(8,commType);
   __sJT_st.setString(9,host);
   __sJT_st.setLong(10,appSeqNum);
   __sJT_st.setString(11,curVNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:890^9*/
      }
      else
      {
        // insert new vnumber
        /*@lineinfo:generated-code*//*@lineinfo:895^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_vnumber
//            (
//              app_seq_num,
//              terminal_number,
//              vnumber,
//              tid,
//              store_number,
//              time_zone,
//              location_number,
//              app_code,
//              equip_model,
//              term_comm_type,
//              front_end
//            )
//            values
//            (
//              :appSeqNum,
//              :terminalNumber,
//              :vNumber,
//              :tid,
//              :storeNumber,
//              :timezone,
//              :locationNumber,
//              :appCode,
//              :equipModel,
//              :commType,
//              :host
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_vnumber\n          (\n            app_seq_num,\n            terminal_number,\n            vnumber,\n            tid,\n            store_number,\n            time_zone,\n            location_number,\n            app_code,\n            equip_model,\n            term_comm_type,\n            front_end\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.ops.VNumberBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,terminalNumber);
   __sJT_st.setString(3,vNumber);
   __sJT_st.setString(4,tid);
   __sJT_st.setInt(5,storeNumber);
   __sJT_st.setInt(6,timezone);
   __sJT_st.setInt(7,locationNumber);
   __sJT_st.setString(8,appCode);
   __sJT_st.setString(9,equipModel);
   __sJT_st.setString(10,commType);
   __sJT_st.setString(11,host);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:925^9*/
      }
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::updateMerchVNumber()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      logEntry(func,desc);
    }
  }

  /*
  ** public void submit()
  **
  ** Updates the current v_number record with new values.
  **
  ** requires all data elements
  */
  public void submit()
  {
    StatusUpdateBean sub = null;

    try
    {
      // get current vNumber to assist in updating merch_vnumber
      String curVNumber = "";
      /*@lineinfo:generated-code*//*@lineinfo:951^7*/

//  ************************************************************
//  #sql [Ctx] { select  v_number
//          
//          from    v_numbers
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  v_number\n         \n        from    v_numbers\n        where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.ops.VNumberBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   curVNumber = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:957^7*/

      // update the current record
      /*@lineinfo:generated-code*//*@lineinfo:960^7*/

//  ************************************************************
//  #sql [Ctx] { update  v_numbers
//          set     v_number          = :vNumber,
//                  tid               = :tid,
//                  terminal_number   = :terminalNumber,
//                  store_number      = :storeNumber,
//                  time_zone         = :timezone,
//                  equip_model       = :equipModel,
//                  location_number   = :locationNumber,
//                  app_code          = :appCode,
//                  term_comm_type    = :commType,
//                  front_end         = :host
//          where   rec_id            = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  v_numbers\n        set     v_number          =  :1 ,\n                tid               =  :2 ,\n                terminal_number   =  :3 ,\n                store_number      =  :4 ,\n                time_zone         =  :5 ,\n                equip_model       =  :6 ,\n                location_number   =  :7 ,\n                app_code          =  :8 ,\n                term_comm_type    =  :9 ,\n                front_end         =  :10 \n        where   rec_id            =  :11";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.ops.VNumberBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,vNumber);
   __sJT_st.setString(2,tid);
   __sJT_st.setInt(3,terminalNumber);
   __sJT_st.setInt(4,storeNumber);
   __sJT_st.setInt(5,timezone);
   __sJT_st.setString(6,equipModel);
   __sJT_st.setInt(7,locationNumber);
   __sJT_st.setString(8,appCode);
   __sJT_st.setString(9,commType);
   __sJT_st.setString(10,host);
   __sJT_st.setLong(11,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:974^7*/

      updateMerchVNumber(curVNumber);

      updateMmsStageInfo();

      // note this vnumber assignment
      sub = new StatusUpdateBean();
      sub.connect();
      sub.addNote(appSeqNum,
                  QueueConstants.Q_VNUMBER_NEW,
                  loginName,
                  QueueConstants.DEPARTMENT_MMS,
                  "Assigned V-Number (" + vNumber + ")");

      // mark account as complete since at least one vnumber has been assigned
      updateAppAccountComplete();
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::submit()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      logEntry(func,desc);
    }
    finally
    {
      if (sub != null)
      {
        sub.cleanUp();
      }
    }
  }

  /*
  ** public void deleteMerchVNumber(String curVNumber)
  **
  ** Deletes current v-number from merch_vnumber.
  */
  public void deleteMerchVNumber(String curVNumber)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1016^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merch_vnumber
//          where   app_seq_num = :appSeqNum and
//                  vnumber = :curVNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merch_vnumber\n        where   app_seq_num =  :1  and\n                vnumber =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.ops.VNumberBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,curVNumber);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1022^7*/
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::deleteMerchVNumber()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      logEntry(func,desc);
    }
  }

  /*
  ** public void delete()
  **
  ** Deletes the current v-number record from v_numbers and merch_vnumber.
  **
  */
  public void delete()
  {
    try
    {
      try
      {
        String curVNumber = "";
        // get the terminal number that corresponds with this v-number slot
        /*@lineinfo:generated-code*//*@lineinfo:1046^9*/

//  ************************************************************
//  #sql [Ctx] { select  v_number 
//            from    v_numbers
//            where   rec_id = :recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  v_number  \n          from    v_numbers\n          where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.ops.VNumberBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   curVNumber = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1051^9*/

        // delete the current record
        /*@lineinfo:generated-code*//*@lineinfo:1054^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    v_numbers
//            where   rec_id = :recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    v_numbers\n          where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.ops.VNumberBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1059^9*/

        // delete corresponding v-number in merch_vnumber
        deleteMerchVNumber(curVNumber);
      }
      catch (Exception e)
      {
        String func = this.getClass().getName() + "::delete(inner)";
        String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
        logEntry(func,desc);
      }

      // load the default v-number
      recId = 0L;
      loadSlot();
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::delete()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      logEntry(func,desc);
    }
  }

  /*
  ** public void loadSlot()
  **
  ** Loads the v-number slot specified by app seq num, pos code, model type
  ** and model index.  If only app seq num is defined then the first available
  ** slot is loaded.
  **
  ** requires appSeqNum, posCode, equipModel, modelIndex OR appSeqNum
  */
  public void loadSlot()
  {
    boolean             hasItems  = true;
    ResultSetIterator   it        = null;
    ResultSet           rs        = null;
    try
    {
      connect();

      // check to see if equipment model is defined
      if (recId == 0L)
      {
        // look for empty slots first
        /*@lineinfo:generated-code*//*@lineinfo:1105^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  rec_id
//            from    v_numbers
//            where   app_seq_num = :appSeqNum and
//                    v_number is null
//            order by  terminal_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rec_id\n          from    v_numbers\n          where   app_seq_num =  :1  and\n                  v_number is null\n          order by  terminal_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.ops.VNumberBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.ops.VNumberBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1112^9*/

        // if no empty slot, get first filled slot
        rs = it.getResultSet();

        hasItems = rs.next();

        if (! hasItems)
        {
          rs.close();
          it.close();

          /*@lineinfo:generated-code*//*@lineinfo:1124^11*/

//  ************************************************************
//  #sql [Ctx] it = { select    rec_id
//              from      v_numbers
//              where     app_seq_num = :appSeqNum
//              order by  terminal_number
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    rec_id\n            from      v_numbers\n            where     app_seq_num =  :1 \n            order by  terminal_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.ops.VNumberBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.ops.VNumberBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1130^11*/

          rs = it.getResultSet();

          hasItems = rs.next();
        }

        // set the first available equipment model
        // and index as the current slot
        if(hasItems)
        {
          recId       = rs.getLong("rec_id");
        }

        rs.close();
        it.close();
      }

      // load the current slot
      if(hasItems)
      {
        /*@lineinfo:generated-code*//*@lineinfo:1151^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(vn.v_number, ' '),
//                    nvl(vn.tid, ' '),
//                    vn.terminal_number,
//                    vn.store_number,
//                    vn.location_number,
//                    vn.time_zone,
//                    vn.app_code,
//                    nvl(msi.request_id,-1),
//                    nvl(vn.term_comm_type, :mesConstants.TERM_COMM_TYPE_DIAL),
//                    nvl(vn.front_end, :mesConstants.HOST_VITAL),
//                    vn.rec_id,
//                    vn.equip_model
//            
//            from    v_numbers vn,
//                    mms_stage_info msi
//            where   vn.rec_id = :recId
//                    and vn.v_number = msi.vnum(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(vn.v_number, ' '),\n                  nvl(vn.tid, ' '),\n                  vn.terminal_number,\n                  vn.store_number,\n                  vn.location_number,\n                  vn.time_zone,\n                  vn.app_code,\n                  nvl(msi.request_id,-1),\n                  nvl(vn.term_comm_type,  :1 ),\n                  nvl(vn.front_end,  :2 ),\n                  vn.rec_id,\n                  vn.equip_model\n           \n          from    v_numbers vn,\n                  mms_stage_info msi\n          where   vn.rec_id =  :3 \n                  and vn.v_number = msi.vnum(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.ops.VNumberBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.TERM_COMM_TYPE_DIAL);
   __sJT_st.setInt(2,mesConstants.HOST_VITAL);
   __sJT_st.setLong(3,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 12) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(12,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vNumber = (String)__sJT_rs.getString(1);
   tid = (String)__sJT_rs.getString(2);
   terminalNumber = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   storeNumber = __sJT_rs.getInt(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   locationNumber = __sJT_rs.getInt(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   timezone = __sJT_rs.getInt(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   appCode = (String)__sJT_rs.getString(7);
   requestId = __sJT_rs.getLong(8); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   commType = (String)__sJT_rs.getString(9);
   host = (String)__sJT_rs.getString(10);
   recId = __sJT_rs.getLong(11); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   equipModel = (String)__sJT_rs.getString(12);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1181^9*/
      }
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::loadSlot()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      logEntry(func,desc);
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  /*
  ** public void addSlot()
  **
  ** Adds a new v-number slot and selects it as current.
  **
  ** requires appSeqNum, posType and
  **  (newDialTerminal or newInternetPos or newPCPos or nothing)
  */
  public void addSlot()
  {
    String newAppCode = null;
    try
    {
      // determine the pos code and model index to use based on pos type
      // and relevant subordinate field
      switch (newPosType)
      {
        // dial terminals
        case 1:
          posCode = 101;
          equipModel = newDialTerminal;
          break;

        // internet solutions
        case 2:
          posCode = Integer.parseInt(newInternetPos);
          equipModel = "na";
          break;

        // pc solutions
        case 3:
          posCode = Integer.parseInt(newPCPos);
          equipModel = "na";
          break;

        // Voice Auth only
        case 4:
          posCode = 601;
          equipModel = "na";
          newAppCode = "STAGE";
          break;

        default:
          throw new Exception("Invalid POS type encountered when adding slot");
      }

      // determine the current max index for equip model
      int maxIndex = 0;
      /*@lineinfo:generated-code*//*@lineinfo:1246^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(max(model_index),0) 
//          from    v_numbers
//          where   app_seq_num = :appSeqNum
//                  and pos_code = :posCode
//                  and equip_model = :equipModel
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(max(model_index),0)  \n        from    v_numbers\n        where   app_seq_num =  :1 \n                and pos_code =  :2 \n                and equip_model =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.ops.VNumberBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,posCode);
   __sJT_st.setString(3,equipModel);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   maxIndex = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1253^7*/
      ++maxIndex;

      // create the new slot
      // (false for the auto flag means manually created slot)
      recId = createNewSlot(posCode,equipModel,maxIndex,false,newAppCode);

      // load the new slot as the selected slot
      loadSlot();
    }
    catch (Exception e)
    {
      String func = this.getClass().getName() + "::addSlot()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      logEntry(func,desc);
    }
  }

  /*
  ** public class VNumberRecord
  **
  ** Subclass that contains a v_number record's data.
  */
  public class VNumberRecord
  {
    private String          posDescription    = "";
    private int             posCode           = -1;
    private String          equipModel        = "";
    private int             modelIndex        = -1;
    private String          vNumber           = "";
    private String          tid               = "";
    private int             terminalNumber    = -1;
    private int             storeNumber       = -1;
    private int             locationNumber    = -1;
    private int             timezone          = -1;
    private String          appCode           = "";
    private boolean         isAuto            = false;
    private int             commType          = -1;
    private int             host              = -1;
    private long            recId             = 0L;

    public String getPosDescription()
    {
      return posDescription;
    }
    public int getPosCode()
    {
      return posCode;
    }
    public String getEquipModel()
    {
      return equipModel;
    }
    public int getModelIndex()
    {
      return modelIndex;
    }
    public String getVNumber()
    {
      return vNumber;
    }
    public String getTid()
    {
      return tid;
    }
    public String getTerminalNumber()
    {
      DecimalFormat df = new DecimalFormat("0000");
      return df.format(terminalNumber);
    }
    public String getStoreNumber()
    {
      DecimalFormat df = new DecimalFormat("0000");
      return df.format(storeNumber);
    }
    public String getLocationNumber()
    {
      DecimalFormat df = new DecimalFormat("00000");
      return df.format(locationNumber);
    }
    public int getTimeZone()
    {
      return timezone;
    }
    public String getAppCode()
    {
      return appCode;
    }
    public boolean getIsAuto()
    {
      return isAuto;
    }
    public int getCommType()
    {
      return commType;
    }
    public int getHost()
    {
      return host;
    }
    public String getHostString()
    {
      String result = "";
      switch(host)
      {
        case mesConstants.HOST_VITAL:
          result = "Vital";
          break;
        case mesConstants.HOST_TRIDENT:
          result = "Trident";
          break;
        default:
          result = "Unknown";
          break;
      }

      return( result );
    }
    public String getRecId()
    {
      return Long.toString(recId);
    }

    public VNumberRecord(ResultSet rs)
    {
      try
      {
        posDescription  = rs.getString("pos_desc");
        equipModel      = rs.getString("equip_model");
        if (!equipModel.equals("na"))
        {
          posDescription = posDescription + ": " + equipModel;
        }
        posCode         = rs.getInt   ("pos_code");
        modelIndex      = rs.getInt   ("model_index");
        vNumber         = rs.getString("v_number");
        tid             = rs.getString("tid");
        terminalNumber  = rs.getInt   ("terminal_number");
        storeNumber     = rs.getInt   ("store_number");
        locationNumber  = rs.getInt   ("location_number");
        timezone        = rs.getInt   ("time_zone");
        appCode         = rs.getString("app_code");
        String autoFlag = rs.getString("auto_flag");
        isAuto          = (autoFlag != null && autoFlag.equals("Y"));
        commType        = rs.getInt   ("term_comm_type");
        host            = rs.getInt   ("front_end");
        recId           = rs.getLong  ("rec_id");
      }
      catch (Exception e)
      {
        String func = this.getClass().getName()
          + "::VNumberRecord() (constructor)";
        String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
        logEntry(func,desc);
      }
    }
  }

  /*
  ** public Vector getVNumbers()
  **
  ** Loads all v_number records corresponding with app seq num into a
  ** Vector and returns them.
  **
  ** requires appSeqNum
  **
  ** RETURNS: Vector full of VNumberRecords.
  */
  public Vector getVNumbers()
  {
    ResultSet rs = null;
    Vector vNums = new Vector();

    try
    {
      ResultSetIterator it = null;
      /*@lineinfo:generated-code*//*@lineinfo:1429^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    pc.pos_desc,
//                    vn.pos_code,
//                    vn.equip_model,
//                    vn.model_index,
//                    vn.v_number,
//                    vn.tid,
//                    vn.terminal_number,
//                    vn.store_number,
//                    vn.time_zone,
//                    vn.location_number,
//                    vn.app_code,
//                    vn.auto_flag,
//                    vn.term_comm_type,
//                    vn.front_end,
//                    vn.rec_id
//          from      v_numbers vn,
//                    pos_category pc
//          where     vn.app_seq_num = :appSeqNum
//                    and vn.pos_code = pc.pos_code
//          order by  terminal_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    pc.pos_desc,\n                  vn.pos_code,\n                  vn.equip_model,\n                  vn.model_index,\n                  vn.v_number,\n                  vn.tid,\n                  vn.terminal_number,\n                  vn.store_number,\n                  vn.time_zone,\n                  vn.location_number,\n                  vn.app_code,\n                  vn.auto_flag,\n                  vn.term_comm_type,\n                  vn.front_end,\n                  vn.rec_id\n        from      v_numbers vn,\n                  pos_category pc\n        where     vn.app_seq_num =  :1 \n                  and vn.pos_code = pc.pos_code\n        order by  terminal_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.ops.VNumberBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.ops.VNumberBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1451^7*/

      rs = it.getResultSet();
      while (rs.next())
      {
        vNums.add(new VNumberRecord(rs));
      }
      it.close();
    }
    catch(Exception e)
    {
      String func = this.getClass().getName()
        + "::getVNumbers()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      logEntry(func,desc);
    }

    return vNums;
  }

  /*
  ** private boolean terminalNumberExists()
  **
  ** Sees if a terminal/store number combo already exists for the app seq num.
  **
  ** RETURNS: true if terminal number in use, else false.
  */
  private boolean terminalNumberExists()
  {
    int numCount = 0;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1483^7*/

//  ************************************************************
//  #sql [Ctx] { select count(terminal_number) 
//          from   v_numbers
//          where  app_seq_num = :appSeqNum
//                 and terminal_number = :terminalNumber
//                 and (  equip_model <> :equipModel
//                        or pos_code <> :posCode
//                        or model_index <> :modelIndex
//                        or store_number <> :storeNumber )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(terminal_number)  \n        from   v_numbers\n        where  app_seq_num =  :1 \n               and terminal_number =  :2 \n               and (  equip_model <>  :3 \n                      or pos_code <>  :4 \n                      or model_index <>  :5 \n                      or store_number <>  :6  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.ops.VNumberBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,terminalNumber);
   __sJT_st.setString(3,equipModel);
   __sJT_st.setInt(4,posCode);
   __sJT_st.setInt(5,modelIndex);
   __sJT_st.setInt(6,storeNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   numCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1493^7*/
    }
    catch (Exception e)
    {
      String func = this.getClass().getName()
        + "::terminalNumberExists()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      logEntry(func,desc);
    }

    return (numCount > 0);
  }

  /*
  ** public void addError(String errorMsg)
  **
  ** Adds an error message to the errors Vector.
  */
  public void addError(String errorMsg)
  {
    errors.add(errorMsg);
  }

  /*
  ** public boolean hasErrors()
  **
  ** Determines if errors exist.
  **
  ** RETURNS: true if errors exist, else false.
  */
  public boolean hasErrors()
  {
    return(errors.size() > 0);
  }

  /*
  ** public boolean validateForSave()
  **
  ** Validates the data being submitted for a v number slot.  Adds any errors
  ** found to the errors vector.
  **
  ** RETURNS: true if errors were found, else false.
  */
  public boolean validateForSave()
  {
    if(Integer.parseInt(host) == mesConstants.HOST_TRIDENT)
    {
      if(vNumber == null ||
         vNumber.equals("") ||
         vNumber.length() != 8)
      {
        addError("Format of Trident CATID (V-Number) is invalid.  Proper format is a '1','2','8', or '9' followed by 7 digits.");
      }
      else
      {
        try
        {
          long value = Long.parseLong(vNumber);
        }
        catch(Exception e)
        {
          addError("Format of Trident CATID (V-Number) is invalid.  Proper format is a '1','2','8', or '9' followed by 7 digits.");
        }
      }
    }
    else
    {
      if( vNumber == null ||
          vNumber.equals("") ||
          vNumber.length() != 8 ||
          vNumber.charAt(0) != 'V')
      {
        addError("Format of V-Number is invalid.  Proper format is 'V' followed by 7 digits e.g. V1234567");
      }
      else
      {
        String digits = vNumber.substring(1);

        try
        {
          long value = Long.parseLong(digits);
        }
        catch(Exception e)
        {
          addError("Format of V-Number is invalid.  Proper format is 'V' followed by 7 digits e.g. V1234567");
        }
      }

      if( tid != null && tid.length()>0 && recId == 0L)   // don't allow empty TIDs
      {
        if(tid.length() != 8 || (!Character.isLetter(tid.charAt(0)) && tid.charAt(0) != '7'))
        {
          addError("Format of TID is invalid.  Proper format is '[A-Z,7]' followed by 7 digits e.g. T1234567 or 71234567");
        }
        else
        {
          String digits = tid.substring(1);

          try
          {
            long value = Long.parseLong(digits);
          }
          catch(Exception e)
          {
            addError("Format of TID is invalid.  Proper format is '[A-Z,7]' followed by 7 digits e.g. T1234567 or 71234567");
          }
        }
      }

      if (terminalNumber <= 0)
      {
        addError("Invalid terminal number.");
      }
      else if (recId == 0L && terminalNumberExists())
      {
        addError("Terminal number is already in use.");
      }

      if (storeNumber <= 0)
      {
        addError("Invalid store number.");
      }

      if (locationNumber <= 0)
      {
        addError("Invalid location number.");
      }

      if (timezone <= 0)
      {
        addError("Invalid time zone.");
      }

      if (appCode == null || appCode.length() == 0)
      {
        addError("Please select an application code.");
      }

      if(commType == null || commType.equals("-1"))
      {
        addError("Please select a comm type.");
      }

      if(host == null || host.equals("-1"))
      {
        addError("Please select a host.");
      }
    }

    return hasErrors();
  }

  /*
  ** public boolean validateForAdd()
  **
  ** Validates the data being submitted to add a new slot.  Adds any errors
  ** found to the errors vector.
  **
  ** RETURNS: true if errors were found, else false.
  */
  public boolean validateForAdd()
  {
    switch (newPosType)
    {
      // dial terminals
      case 1:
        if (newDialTerminal.equals("select one"))
        {
          addError("Please select a dial terminal to add.");
        }
        break;

      // internet solutions
      case 2:
        if (newInternetPos.equals("select one"))
        {
          addError("Please select an internet solution to add.");
        }
        break;

      // pc solutions
      case 3:
        if (newPCPos.equals("select one"))
        {
          addError("Please select a PC solution to add.");
        }
        break;

      // dial pay
      case 4:
        break;
    }

    return hasErrors();
  }

  /*
  ** public Vector getErrors()
  **
  ** RETURNS: a reference to the errors vector.
  */
  public Vector getErrors()
  {
    return this.errors;
  }

  /*
  ** public int getAppType(long primaryKey)
  **
  ** Convenience routine to fetch the application type of an app seq num.
  **
  ** RETURNS: the application type of the app_seq_num.
  */
  public int getAppType(long appSeqNum)
  {
    int appType = -1;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1712^7*/

//  ************************************************************
//  #sql [Ctx] { select app_type 
//          from   application
//          where  app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select app_type  \n        from   application\n        where  app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.ops.VNumberBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1717^7*/
    }
    catch (Exception e)
    {
      String func = this.getClass().getName()
        + "::getAppType()";
      String desc = "appSeqNum = " + appSeqNum + ", " + e.toString();
      logEntry(func,desc);
    }

    return appType;
  }

  /*
  ** public class EquipTypeTable extends DropDownTable
  **
  ** Defines a drop down table containing equipment types
  */
  private class EquipTypeTable extends DropDownTable
  {
    public EquipTypeTable()
    {
      getData();
    }

    protected boolean getData()
    {
      boolean getOk         = false;
      ResultSetIterator it = null;
      ResultSet         rs = null;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:1752^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  equip_model code,
//                    equip_model description
//            from    equipment
//            where   used_in_inventory = 'Y' or
//                    equip_model = 'na'
//            order by equip_model
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  equip_model code,\n                  equip_model description\n          from    equipment\n          where   used_in_inventory = 'Y' or\n                  equip_model = 'na'\n          order by equip_model";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.ops.VNumberBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"25com.mes.ops.VNumberBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1760^9*/

        rs = it.getResultSet();

        while(rs.next())
        {
          addElement(rs);
          getOk = true;
        }
      }
      catch(Exception e)
      {
        logEntry("getData()", e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }

      return( getOk );
    }
  }
  /*
  ** public class TimezoneTable extends DropDownTable
  **
  ** Defines a drop down table containing time zone codes and descriptions.
  */
  private class TimezoneTable extends DropDownTable
  {
    /*
    ** CONSTRUCTOR public TimezoneTable()
    */
    public TimezoneTable()
    {
      getData();
    }

    /*
    ** METHOD protected boolean getData()
    **
    ** Loads the timezone list.
    **
    ** RETURNS: true if successful, else false.
    */
    protected boolean getData()
    {
      boolean             getOk   = false;
      ResultSetIterator   it      = null;
      ResultSet           rs      = null;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:1816^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    code,
//                      description
//            from      time_zones
//            order by  code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    code,\n                    description\n          from      time_zones\n          order by  code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.ops.VNumberBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"26com.mes.ops.VNumberBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1822^9*/

        rs = it.getResultSet();

        while (rs.next())
        {
          addElement(rs);
          getOk = true;
        }

        it.close();
      }
      catch (Exception e)
      {
        String func = this.getClass().getName()
          + "::getData()";
        String desc = e.toString();
        logEntry(func,desc);
      }
      finally
      {
        cleanUp();
      }

      return getOk;
    }
  }
  /*
  ** public DropDownTable getTimezoneTable()
  **
  ** Accessor for timezone drop down table.
  **
  ** RETURNS: a reference to a DropDownTable containing time zones.
  */
  public DropDownTable getTimezoneTable()
  {
    return new TimezoneTable();
  }

  /*
  ** pulic DropDownTable getEquipTypeTable()
  */
  public DropDownTable getEquipTypeTable()
  {
    return new EquipTypeTable();
  }

  /*
  ** public class DialTerminalTable extends DropDownTable
  **
  ** Defines a drop down table containing dial terminals.
  */
  private class DialTerminalTable extends DropDownTable
  {
    /*
    ** CONSTRUCTOR public DialTerminalTable()
    */
    public DialTerminalTable()
    {
      getData();
    }

    /*
    ** METHOD protected boolean getData()
    **
    ** Loads the timezone list.
    **
    ** RETURNS: true if successful, else false.
    */
    protected boolean getData()
    {
      boolean             getOk   = false;
      ResultSetIterator   it      = null;
      ResultSet           rs      = null;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:1901^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    equip_model,
//                      equip_descriptor
//            from      equipment
//            where     equiptype_code in ( 1, 5, 6 )
//            order by  equip_descriptor
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    equip_model,\n                    equip_descriptor\n          from      equipment\n          where     equiptype_code in ( 1, 5, 6 )\n          order by  equip_descriptor";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.ops.VNumberBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"27com.mes.ops.VNumberBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1908^9*/

        rs = it.getResultSet();

        while (rs.next())
        {
          addElement(rs);
          getOk = true;
        }

        it.close();
      }
      catch (Exception e)
      {
        String func = this.getClass().getName()
          + "::getData()";
        String desc = e.toString();
        logEntry(func,desc);
      }
      finally
      {
        cleanUp();
      }

      return getOk;
    }
  }
  /*
  ** public DropDownTable getDialTerminalTable()
  **
  ** Accessor for dial terminal drop down table.
  **
  ** RETURNS: a reference to a DropDownTable containing dial terminal
  **          equipment models.
  */
  public DropDownTable getDialTerminalTable()
  {
    return new DialTerminalTable();
  }

  /*
  ** public class InternetPosTable extends DropDownTable
  **
  ** Defines a drop down table containing time zone codes and descriptions.
  */
  private class InternetPosTable extends DropDownTable
  {
    /*
    ** CONSTRUCTOR public InternetPosTable()
    */
    public InternetPosTable()
    {
      getData();
    }

    /*
    ** METHOD protected boolean getData()
    **
    ** Loads the timezone list.
    **
    ** RETURNS: true if successful, else false.
    */
    protected boolean getData()
    {
      boolean             getOk   = false;
      ResultSetIterator   it      = null;
      ResultSet           rs      = null;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:1980^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    pos_code,
//                      pos_desc
//            from      pos_category
//            where     pos_type = 2
//            order by  pos_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    pos_code,\n                    pos_desc\n          from      pos_category\n          where     pos_type = 2\n          order by  pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.ops.VNumberBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"28com.mes.ops.VNumberBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1987^9*/

        rs = it.getResultSet();

        while (rs.next())
        {
          addElement(rs);
          getOk = true;
        }

        it.close();
      }
      catch (Exception e)
      {
        String func = this.getClass().getName()
          + "::getData()";
        String desc = e.toString();
        logEntry(func,desc);
      }
      finally
      {
        cleanUp();
      }

      return getOk;
    }
  }
  /*
  ** public DropDownTable getInternetPosTable()
  **
  ** Accessor for dial terminal drop down table.
  **
  ** RETURNS: a reference to a DropDownTable containing dial terminal
  **          equipment models.
  */
  public DropDownTable getInternetPosTable()
  {
    return new InternetPosTable();
  }

  /*
  ** public class PCPosTable extends DropDownTable
  **
  ** Defines a drop down table containing time zone codes and descriptions.
  */
  private class PCPosTable extends DropDownTable
  {
    /*
    ** CONSTRUCTOR public PCPosTable()
    */
    public PCPosTable()
    {
      getData();
    }

    /*
    ** METHOD protected boolean getData()
    **
    ** Loads the timezone list.
    **
    ** RETURNS: true if successful, else false.
    */
    protected boolean getData()
    {
      boolean             getOk   = false;
      ResultSetIterator   it      = null;
      ResultSet           rs      = null;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:2059^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    pos_code,
//                      pos_desc
//            from      pos_category
//            where     pos_type = 3
//            order by  pos_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    pos_code,\n                    pos_desc\n          from      pos_category\n          where     pos_type = 3\n          order by  pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.ops.VNumberBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"29com.mes.ops.VNumberBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2066^9*/

        rs = it.getResultSet();

        while (rs.next())
        {
          addElement(rs);
          getOk = true;
        }

        it.close();
      }
      catch (Exception e)
      {
        String func = this.getClass().getName()
          + "::getData()";
        String desc = e.toString();
        logEntry(func,desc);
      }
      finally
      {
        cleanUp();
      }

      return getOk;
    }
  }
  /*
  ** public DropDownTable getPCPosTable()
  **
  ** Accessor for dial terminal drop down table.
  **
  ** RETURNS: a reference to a DropDownTable containing dial terminal
  **          equipment models.
  */
  public DropDownTable getPCPosTable()
  {
    return new PCPosTable();
  }

  /*
  ** public class CommTypeTable extends DropDownTable
  **
  ** Defines a drop down table containing possible communication types for
  ** terminals
  */
  private class CommTypeTable extends DropDownTable
  {
    public CommTypeTable()
    {
      getData();
    }

    protected boolean getData()
    {
      try
      {
        addElement(Integer.toString(mesConstants.TERM_COMM_TYPE_DIAL), "Dial");
        addElement(Integer.toString(mesConstants.TERM_COMM_TYPE_IP),   "IP");
        addElement(Integer.toString(mesConstants.TERM_COMM_TYPE_WIRELESS),"Wireless");
      }
      catch(Exception e)
      {
        logEntry("CommTypeTable::getData()", e.toString());
      }

      return true;
    }
  }

  public class HostTable extends DropDownTable
  {
    public HostTable()
    {
      getData();
    }

    protected boolean getData()
    {
      try
      {
        addElement(Integer.toString(mesConstants.HOST_INVALID), "Select One");
        addElement(Integer.toString(mesConstants.HOST_VITAL), "Vital");
        addElement(Integer.toString(mesConstants.HOST_TRIDENT), "Trident");
      }
      catch(Exception e)
      {
        logEntry("HostTable::getData()", e.toString());
      }

      return true;
    }
  }

  /*
  ** public class AppCodeTable extends DropDownTable
  **
  ** Defines a drop down table containing pos application codes from
  ** the pos_app_codes table
  */
  private class AppCodeTable extends DropDownTable
  {
    /*
    ** CONSTRUCTOR public PCPosTable()
    */
    public AppCodeTable()
    {
      getData();
    }

    /*
    ** METHOD protected boolean getData()
    **
    ** Loads the timezone list.
    **
    ** RETURNS: true if successful, else false.
    */
    protected boolean getData()
    {
      boolean             getOk   = false;
      ResultSetIterator   it      = null;
      ResultSet           rs      = null;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:2193^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    terminal_application a,
//                      terminal_application b
//            from      mms_terminal_applications
//            order by  a
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    terminal_application a,\n                    terminal_application b\n          from      mms_terminal_applications\n          order by  a";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.ops.VNumberBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"30com.mes.ops.VNumberBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2199^9*/

        rs = it.getResultSet();

        while (rs.next())
        {
          addElement(rs);
          getOk = true;
        }

        it.close();
      }
      catch (Exception e)
      {
        String func = this.getClass().getName()
          + "::getData()";
        String desc = e.toString();
        logEntry(func,desc);
      }
      finally
      {
        cleanUp();
      }

      return getOk;
    }
  }
  /*
  ** public DropDownTable getAppCodeTable()
  **
  ** Accessor for app code drop down table.
  **
  ** RETURNS: a reference to a DropDownTable containing dial terminal
  **          equipment models.
  */
  public DropDownTable getAppCodeTable()
  {
    return new AppCodeTable();
  }

  /*
  ** public DropDownTable getCommTypeTable()
  **
  ** Accessor for comm type drop down table.
  **
  ** RETURNS: a reference to a DropDownTable containing communication types
  */
  public DropDownTable getCommTypeTable()
  {
    return new CommTypeTable();
  }

  public DropDownTable getHostTable()
  {
    return new HostTable();
  }
}/*@lineinfo:generated-code*/