/*@lineinfo:filename=MmsManagementBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/MmsManagementBean.sqlj $

  Description:  
    Contains logic for working with application queues (account servicing)


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 9/20/04 12:18p $
  Version            : $Revision: 19 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesQueues;
import com.mes.constants.mesConstants;
import com.mes.equipment.profile.EquipProfileGeneratorDictator;
import com.mes.queues.QueueTools;
import com.mes.support.DateTimeFormatter;
import com.mes.user.UserBean;
import sqlj.runtime.ResultSetIterator;

public class MmsManagementBean extends com.mes.screens.SequenceDataBean
{
  private Vector mmsRequests                      = new Vector();
  private Vector equipRequests                    = new Vector();
 
  private String dbaName                          = "";
  private String merchNum                         = "";
  private String controlNum                       = "";
  private int    appType                          = -1;
  private String appTypeDesc                      = "";
  private String appCreatedDate                   = "";
  private int    posType                          = -1;
  private String posDesc                          = "";
  private String posCategory                      = "";
  private int    currentStage                     = -1;
  private String currentStageDesc                 = "";

  // non-bank card statuses
  protected String    amexStatus                  = "";
  protected String    CheckStatus                 = null;
  protected String    CheckProvider               = null;
  protected String    discStatus                  = "";
  protected String    ValutecGiftCardStatus       = null;

  public MmsManagementBean()
  {
  }
  
  public static String decodeCheckProviderName( String val )
  {
    StringBuffer  retVal      = new StringBuffer();
    
    if ( val != null && !val.equals("") )
    {
      // have a value
      if ( val.startsWith("CPOther:") )
      {
        retVal.append( val.substring(8) );
      }
      else
      {
        String upperVal = val.toUpperCase();
        
        // first character is always first
        retVal.append(val.charAt(0));
        
        // scan string skipping the first character
        for( int i = 1; i < val.length(); ++i )
        {
          if ( val.charAt(i) == upperVal.charAt(i) )
          {
            retVal.append(" ");
          }
          retVal.append(val.charAt(i));
        }
      }
    }
    return( retVal.toString() );
  }
  
  public int numMmsRequests()
  {
    return mmsRequests.size();
  }

  public int numEquipRequests()
  {
    return equipRequests.size();
  }

  private void getMerchantGeneralInfo(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select                                       ");
      qs.append("merch.merch_business_name,                   "); 
      qs.append("merch.merch_number,                          ");
      qs.append("merch.merc_cntrl_number,                     ");
      qs.append("app.appsrctype_code,                         ");
      qs.append("app.app_created_date,                        ");
      qs.append("pos.pos_type,                                ");
      qs.append("pos.pos_desc                                 ");

      qs.append("from merchant merch,                         ");
      qs.append("application   app,                           ");
      qs.append("pos_category  pos,                           ");
      qs.append("merch_pos     mpos                           ");

      qs.append("where merch.app_seq_num  = ?                 ");
      qs.append("and merch.app_seq_num    = app.app_seq_num   ");
      qs.append("and merch.app_seq_num    = mpos.app_seq_num  ");
      qs.append("and mpos.pos_code        = pos.pos_code      ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);
      rs = ps.executeQuery();
    
      if(rs.next())
      {
        dbaName         = isBlank(rs.getString("merch_business_name"))  ? "" : rs.getString("merch_business_name");
        merchNum        = isBlank(rs.getString("merch_number"))         ? "" : rs.getString("merch_number");
        controlNum      = isBlank(rs.getString("merc_cntrl_number"))    ? "" : rs.getString("merc_cntrl_number");
        appTypeDesc     = isBlank(rs.getString("appsrctype_code"))      ? "" : rs.getString("appsrctype_code");
        appCreatedDate  = java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("app_created_date"));
        posType         = rs.getInt("pos_type");
        posDesc         = isBlank(rs.getString("pos_desc"))             ? "" : rs.getString("pos_desc");

        switch(posType)
        {
          case mesConstants.POS_DIAL_TERMINAL:
            posCategory = "Dial Terminal (EDC)";
          break;
          case mesConstants.POS_INTERNET:
            posCategory = "Internet";
          break;
          case mesConstants.POS_PC:
            posCategory = "POS Partner";
          break;
          case mesConstants.POS_DIAL_AUTH:
            posCategory = "Dial Pay";
          break;
          case mesConstants.POS_OTHER:
            posCategory = "Other Vital Certified Product";
          break;
          case mesConstants.POS_STAGE_ONLY:
            posCategory = "Stage Only";
          break;
          case mesConstants.POS_GLOBAL_PC:
            posCategory = "Global PC Windows";
          break;
          default:
            posCategory = "unknown";
          break;
        }

      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMerchantGeneralInfo: " + e.toString());
      addError("getMerchantGeneralInfo: " + e.toString());
    }
  }

  public void setAppType(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select app_type          ");
      qs.append("from   application       ");
      qs.append("where  app_seq_num  = ?  ");
     
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);
      rs = ps.executeQuery();
    
      if(rs.next())
      {
        appType = rs.getInt("app_type");
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setAppType: " + e.toString());
      addError("setAppType: " + e.toString());
    }
  }


  private void getEquipmentRequests(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      //select equipment requested on application thats not an other, or software.
      qs.append("select me.*,                                                             "); 
      qs.append("decode(po.prod_option_des, null, 'N/A', po.prod_option_des) prod_option, ");
      qs.append("eq.equip_descriptor,                                                     ");
      qs.append("eq.equip_model,                                                          ");
      qs.append("mfr.equipmfgr_mfr_name,                                                  ");
      qs.append("lt.equiplendtype_description,                                            ");
      qs.append("et.equiptype_description                                                 ");

      qs.append("from merchequipment  me,                                                 ");
      qs.append("equipment            eq,                                                 ");
      qs.append("equipmfgr            mfr,                                                ");
      qs.append("equiplendtype        lt,                                                 ");
      qs.append("prodoption           po,                                                 ");
      qs.append("equiptype            et                                                 ");

      qs.append("where me.app_seq_num = ?                                                 ");
      //qs.append("and me.equiplendtype_code != ?                                           ");
      qs.append("and me.equiptype_code not in (4,7,8,9)                                   ");
      qs.append("and me.prod_option_id = po.prod_option_id(+)                             ");
      qs.append("and eq.equip_model = me.equip_model                                      ");
      qs.append("and eq.equipmfgr_mfr_code = mfr.equipmfgr_mfr_code                       ");
      qs.append("and me.equiplendtype_code = lt.equiplendtype_code                        ");
      qs.append("and eq.equiptype_code = et.equiptype_code                                ");
      qs.append("order by mfr.equipmfgr_mfr_name                                          ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);
      //ps.setInt(2, com.mes.constants.mesConstants.APP_EQUIP_OWNED);
      rs = ps.executeQuery();
      
      EquipProfileGeneratorDictator epgd = new EquipProfileGeneratorDictator();
    
      while(rs.next())
      {
        int needed = 0;

        if(!isBlank(rs.getString("merchequip_equip_quantity")))
        {
          needed = rs.getInt("merchequip_equip_quantity");
        }
    
        EquipRec tempRec =  new EquipRec();

        tempRec.setPartNum      (rs.getString("equip_model"));
        tempRec.setProductName  (rs.getString("equipmfgr_mfr_name"));
        tempRec.setProductDesc  (rs.getString("equip_descriptor"));
        tempRec.setProductOption(rs.getString("prod_option"));
        tempRec.setUnitPrice    (formatCurr(rs.getString("merchequip_amount")));
        tempRec.setLendTypeDesc (rs.getString("equiplendtype_description"));
        tempRec.setLendTypeCode (rs.getString("equiplendtype_code"));
        tempRec.setEquipTypeCode(rs.getInt("equiptype_code"));
        tempRec.setEquipTypeDesc(rs.getString("equiptype_description"));
        tempRec.setQuanRequested(needed);
        
        tempRec.setProfileGeneratorCode(epgd.getEquipmentProfileGeneratorCode(rs.getString("equip_model"),rs.getInt("prod_option_id")));

        equipRequests.add(tempRec);
 
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getEquipmentRequests: " + e.toString());
      addError("getEquipmentRequests: " + e.toString());
    }
  }
  
  private void getMmsStageInfo(long primaryKey)
  {
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:313^7*/

//  ************************************************************
//  #sql [Ctx] { select  qd.type,
//                  qt.description
//          
//          from    q_data qd,
//                  q_group_to_types qgtt,
//                  q_types qt
//          where   qd.id = :primaryKey and
//                  qd.type = qgtt.queue_type and
//                  qgtt.group_type = :MesQueues.QG_MMS_QUEUES and
//                  qd.type = qt.type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  qd.type,\n                qt.description\n         \n        from    q_data qd,\n                q_group_to_types qgtt,\n                q_types qt\n        where   qd.id =  :1  and\n                qd.type = qgtt.queue_type and\n                qgtt.group_type =  :2  and\n                qd.type = qt.type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.MmsManagementBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   __sJT_st.setInt(2,MesQueues.QG_MMS_QUEUES);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   currentStage = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   currentStageDesc = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:326^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMmsStageInfo: " + e.toString());
      addError("getMmsStageInfo: " + e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public void moveToStage(long primaryKey, int dest, int type, UserBean user)
  {
    StringBuffer      qs      = new StringBuffer();
    PreparedStatement ps      = null;

    try 
    {
      //go from exception queue to exception queue completed
      if(dest == MesQueues.Q_CBT_MMS_COMPLETE && type == MesQueues.Q_CBT_MMS_EXCEPTION)
      {
        dest = MesQueues.Q_CBT_MMS_EXCEPTION_COMPLETE;
      }

      QueueTools.moveQueueItem(primaryKey, type, dest, user, null);

      //going from cbt exception completed to cbt new (this runs as a precaution incase its in exception completed queue)
      if(type == MesQueues.Q_CBT_MMS_COMPLETE && dest == MesQueues.Q_CBT_MMS_NEW)
      {
        type = MesQueues.Q_CBT_MMS_EXCEPTION_COMPLETE;
        QueueTools.moveQueueItem(primaryKey, type, dest, user, null);
      }

    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "moveToStage: " + e.toString());
      addError("moveToStage: " + e.toString());
    }
  }
  
  private void getMmsRequests(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
      qs.append("select   msi.*,                                                                ");
      qs.append("         decode(eq.equip_descriptor,null,'N/A',eq.equip_descriptor) equip_desc ");
      qs.append("         ,pg.name profgen_name                                                 ");
      qs.append("from     mms_stage_info  msi,                                                  ");
      qs.append("         equipment       eq                                                    ");
      qs.append("         ,EQUIP_PROFILE_GENERATORS       pg                                    ");
      qs.append("where    msi.app_seq_num = ? and                                               ");
      qs.append("         msi.equip_model = eq.equip_model(+)                                   ");
      qs.append("         and msi.PROFILE_GENERATOR_CODE = pg.code(+)                           ");
      qs.append("order by msi.app_date                                                          ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, primaryKey);

      rs = ps.executeQuery();
    
      while(rs.next())
      {
        RequestRec tempRec =  new RequestRec();

        tempRec.setRequestId      (rs.getString("request_id"));
        tempRec.setVnumber        (isBlank(rs.getString("vnum")) ? "----" : rs.getString("vnum"));
        tempRec.setTermApplication(rs.getString("term_application"));
        tempRec.setTermAppProfGen (isBlank(rs.getString("term_application_profgen")) ? "" : rs.getString("term_application_profgen"));
        tempRec.setPartNum        (rs.getString("equip_model"));
        tempRec.setPartDesc       (rs.getString("equip_desc"));
        tempRec.setAppDate        (java.text.DateFormat.getDateInstance(java.text.DateFormat.SHORT,Locale.US).format(rs.getTimestamp("app_date")));
        tempRec.setProcessStatus  (isBlank(rs.getString("process_status")) ? "----" : rs.getString("process_status"));
        tempRec.setProcessResponse(isBlank(rs.getString("process_response")) ? "----" : rs.getString("process_response"));
        tempRec.setProfGenName    (isBlank(rs.getString("profgen_name")) ? "----" : rs.getString("profgen_name"));

        mmsRequests.add(tempRec);
 
      }
      
      rs.close();
      ps.close();
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "getMmsRequests: " + e.toString());
      addError("getMmsRequests: " + e.toString());
    }
  }

  private void getNonBankCardData(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:432^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mpo_disc.merchpo_card_merch_number  as disc_merch_number,   
//                  mpo_disc.merchpo_rate               as disc_disc_rate,      
//                  mpo_amex.merchpo_card_merch_number  as amex_merch_number,   
//                  mpo_amex.merchpo_rate               as amex_disc_rate,      
//                  mda.transmission_method             as disc_transmit,       
//                  mda.app_date                        as disc_transmit_date,  
//                  mdar.decision_status                as disc_transmit_status,
//                  amex.submit_date                    as amex_transmit_date,
//                  mpo_check.merchpo_tid               as check_tid,
//                  mpo_check.merchpo_provider_name     as check_provider,
//                  mpo_vtec.merchpo_tid                as valutec_tid
//          from    merchant                            m,
//                  merchpayoption                      mpo_disc,
//                  merchpayoption                      mpo_amex,
//                  merchpayoption                      mpo_check,
//                  merchpayoption                      mpo_vtec,
//                  merchant_discover_app               mda,
//                  merchant_discover_app_response      mdar,
//                  amex_esa_submissions                amex
//          where   m.app_seq_num  = :primaryKey  and                                                            
//                  m.app_seq_num  = mda.app_seq_num(+)  and                                           
//                  m.app_seq_num  = mdar.app_seq_num(+) and                                           
//                  m.app_seq_num  = amex.app_seq_num(+) and                                           
//                  (m.app_seq_num = mpo_disc.app_seq_num(+) and 14 = mpo_disc.cardtype_code(+)) and  -- amex
//                  (m.app_seq_num = mpo_amex.app_seq_num(+) and 16 = mpo_amex.cardtype_code(+)) and  -- discover
//                  (m.app_seq_num = mpo_check.app_seq_num(+) and 18 = mpo_check.cardtype_code(+)) and  -- check
//                  (m.app_seq_num = mpo_vtec.app_seq_num(+) and 24 = mpo_vtec.cardtype_code(+))      -- valutec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mpo_disc.merchpo_card_merch_number  as disc_merch_number,   \n                mpo_disc.merchpo_rate               as disc_disc_rate,      \n                mpo_amex.merchpo_card_merch_number  as amex_merch_number,   \n                mpo_amex.merchpo_rate               as amex_disc_rate,      \n                mda.transmission_method             as disc_transmit,       \n                mda.app_date                        as disc_transmit_date,  \n                mdar.decision_status                as disc_transmit_status,\n                amex.submit_date                    as amex_transmit_date,\n                mpo_check.merchpo_tid               as check_tid,\n                mpo_check.merchpo_provider_name     as check_provider,\n                mpo_vtec.merchpo_tid                as valutec_tid\n        from    merchant                            m,\n                merchpayoption                      mpo_disc,\n                merchpayoption                      mpo_amex,\n                merchpayoption                      mpo_check,\n                merchpayoption                      mpo_vtec,\n                merchant_discover_app               mda,\n                merchant_discover_app_response      mdar,\n                amex_esa_submissions                amex\n        where   m.app_seq_num  =  :1   and                                                            \n                m.app_seq_num  = mda.app_seq_num(+)  and                                           \n                m.app_seq_num  = mdar.app_seq_num(+) and                                           \n                m.app_seq_num  = amex.app_seq_num(+) and                                           \n                (m.app_seq_num = mpo_disc.app_seq_num(+) and 14 = mpo_disc.cardtype_code(+)) and  -- amex\n                (m.app_seq_num = mpo_amex.app_seq_num(+) and 16 = mpo_amex.cardtype_code(+)) and  -- discover\n                (m.app_seq_num = mpo_check.app_seq_num(+) and 18 = mpo_check.cardtype_code(+)) and  -- check\n                (m.app_seq_num = mpo_vtec.app_seq_num(+) and 24 = mpo_vtec.cardtype_code(+))      -- valutec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.MmsManagementBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,primaryKey);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.ops.MmsManagementBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:461^7*/
      rs = it.getResultSet();      
    
      if(rs.next())
      {
        // check app for disc number
        if(rs.getString("disc_merch_number") != null && rs.getString("disc_merch_number").length() > 1)
        {
          // app shows a discover number, we are still good to go
          discStatus = "Discover Merchant # Available";
        }
        //check to see if they requested a discover account and to see if it has not been sent
        else if(rs.getString("disc_disc_rate") != null && rs.getString("disc_transmit") == null)
        {
          // no merchant number and not sent to discover yet
          discStatus = "Request For Discover Merchant # Not Yet Processed";
        }
        //check to see if they requested an account and it has been sent... then display transmit date and status
        //display merchant number if it came back.. i guess.. maybe not..
        else if(rs.getString("disc_disc_rate") != null && rs.getString("disc_transmit") != null)
        {
          if(rs.getString("disc_transmit_status") != null && rs.getString("disc_transmit_status").equals("A"))
          {
            discStatus = "Request For Discover Merchant # Has Been Successfully Processed";
          }
          else if(rs.getString("disc_transmit_status") != null && (rs.getString("disc_transmit_status").equals("P") || rs.getString("disc_transmit_status").equals("E")))
          {
            discStatus = "Request For Discover Merchant # Pending";
          }
          else if(rs.getString("disc_transmit_status") != null && rs.getString("disc_transmit_status").equals("D"))
          {
            discStatus = "Request For Discover Merchant # Declined";
          }
        }
        //else if they just dont want discover at all.. then fuck'em
        else
        {
          // no discover at all
          discStatus = "Discover Merchant # Not Needed";
        }

        //check to see if app show number.. if soo.. then we all good
        if(rs.getString("amex_merch_number") != null && rs.getString("amex_merch_number").length() > 1)
        {
          // app shows an amex number so we are good to go
          amexStatus = "Amex Merchant # Available";
        }

        //check to see if amex requested and to see if it has not been sent
        else if(rs.getString("amex_disc_rate") != null && rs.getString("amex_transmit_date") == null)
        {
          // amex number is pending
          amexStatus = "Request For Amex Merchant # Not Yet Processed";
        }

        //check to see if amex was requested and it was sent.. then we display date and status...
        else if(rs.getString("amex_disc_rate") != null && rs.getString("amex_transmit_date") != null)
        {
          // no merchant number but already sent to amex
          amexStatus = "Request For Amex Merchant # Has Been Sent To Amex On " + DateTimeFormatter.getFormattedDate(rs.getTimestamp("amex_transmit_date"), "MM/dd/y HH:mm");
        }
        else
        {
          // no amex at all
          amexStatus = "Amex Merchant # Not Needed";
        }
        
        CheckStatus = rs.getString("check_tid");
        if ( CheckStatus != null )
        {
          CheckStatus   = "Check TID Not Needed";
          CheckProvider = decodeCheckProviderName(rs.getString("check_provider"));
        }
        
        ValutecGiftCardStatus = rs.getString("valutec_tid");
        if ( ValutecGiftCardStatus != null )
        {
          ValutecGiftCardStatus = "Valutec Gift Card TID Not Needed";
        }
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("getNonBankCardData()", e.toString());
      addError("getNonBankCardData: " + e.toString());
    }
    finally
    {
      try{it.close();}catch(Exception e){}
      cleanUp();
    }
  }

  
  public String formatCurr(String curr)
  {
    String       result   = "";
    NumberFormat nf       = NumberFormat.getCurrencyInstance(Locale.US);

    try
    {
      result = nf.format(Double.parseDouble(curr));
    }
    catch(Exception e)
    {}
    return result;
  }
  
  public void getData(long primaryKey)
  {
    getMerchantGeneralInfo(primaryKey);
    getEquipmentRequests  (primaryKey);
    getMmsRequests        (primaryKey);  
    getMmsStageInfo       (primaryKey);
    getNonBankCardData    (primaryKey);
  }
  
  public boolean isMmsNewStage()
  {
    return (this.currentStage == MesQueues.Q_MMS_NEW || this.currentStage == MesQueues.Q_CBT_MMS_NEW);
  }
  public boolean isMmsPendStage()
  {
    return (this.currentStage == MesQueues.Q_MMS_PEND || this.currentStage == MesQueues.Q_CBT_MMS_PEND);
  }
  public boolean isMmsCompletedStage()
  {
    return (this.currentStage == MesQueues.Q_MMS_COMPLETE || this.currentStage == MesQueues.Q_CBT_MMS_COMPLETE || this.currentStage == MesQueues.Q_CBT_MMS_EXCEPTION_COMPLETE);
  }
  
  public String getCurrentStageDesc()
  {
    return this.currentStageDesc;
  }  
  public int getCurrentStage()
  {
    return this.currentStage;
  }

  public void setDefaults(String primaryKey)
  {
    try
    {
      setDefaults(Long.parseLong(primaryKey));
    }
    catch(Exception e)
    {
    }
  }
  public void setDefaults(long primaryKey)
  {
    StringBuffer      qs      = new StringBuffer("");
    PreparedStatement ps      = null;
    ResultSet         rs      = null;
    
    try
    {
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "setDefaults: " + e.toString());
      addError("setDefaults: " + e.toString());
    }
  }
  
  public String formatDate(Date raw)
  {
    String result   = "";
    if(raw == null)
    {
      return result;
    }
    try
    {
      DateFormat df   = new SimpleDateFormat("MM/dd/yy");
      result = df.format(raw);
      return result;
    }
    catch(Exception e)
    {
      //@ System.out.println("date crashed = " + e.toString());
    }
    return result;
  }
  
    
  public boolean validate()
  {
    return(! hasErrors());
  }

  public String encode(String url)
  {
    String body = url;
    try
    {
      StringBuffer    bodyBuff = null;
      int             index = 0;
      
      index = body.indexOf(' ');
      
      while(index > 0)
      {
        bodyBuff = new StringBuffer(body);
        bodyBuff.replace(index,index+1,"%20");
        body = bodyBuff.toString();
        index = body.indexOf(' ', index);
      }
    }
    catch(Exception e)
    {
      //@ System.out.println("error will robinson %20");
    }
    return body;

  }
  
  public void submitData(HttpServletRequest req, long primaryKey)
  {
  }
 
  public boolean appApproved(long appSeqNum)
  {
    StringBuffer        qs      = new StringBuffer("");
    PreparedStatement   ps      = null;
    ResultSet           rs      = null;
    boolean             result  = false;

    try
    {
      
      //check to see if merch_credit_status == approved
      qs.append("select merch_credit_status ");
      qs.append("from  merchant             ");
      qs.append("where app_seq_num = ?      ");
      
      ps = getPreparedStatement(qs.toString());
      ps.setLong(1, appSeqNum);

      rs = ps.executeQuery();

      if(rs.next())
      {
        if(rs.getInt("merch_credit_status") == QueueConstants.CREDIT_APPROVE)
        {
          result = true;
        }
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "appApproved: " + e.toString());
    }

    return result;

  }

  public boolean allowMMSLink(String partNum, int quanRequested)
  {
    int     qty    = 0;

    for (int i = 0; i < numMmsRequests(); ++i)
    {
      if(getRequestPartNum(i).equals(partNum))
      {
        qty++;
      }
    }

    return (quanRequested > qty);
  }

  public String getDbaName()
  {
    return dbaName;
  }
  public String getMerchNum()
  {
    return merchNum;
  }
  public String getControlNum()
  {
    return controlNum;
  }
  public String getAppTypeDesc()
  {
    return appTypeDesc;
  }

  public boolean isCbtAppType()
  {
    return (appType == mesConstants.APP_TYPE_CBT || appType == mesConstants.APP_TYPE_CBT_NEW);
  }

  public String getAppCreatedDate()
  {
    return appCreatedDate;
  }
  public int getPosType()
  {
    return posType;
  }
  public String getPosDesc()
  {
    return posDesc;
  }
  public String getPosCategory()
  {
    return posCategory;
  }

  public String getAmexStatus()
  {
    return( amexStatus );
  }
  
  public String getCheckStatus()
  {
    return( CheckStatus );
  }
  
  public String getCheckProvider()
  {
    return( CheckProvider );
  }
  
  public String getDiscStatus()
  {
    return( discStatus );
  }
  
  public String getValutecGiftCardStatus()
  {
    return( ValutecGiftCardStatus );
  }

  public String getUnitPrice(int idx)
  {
    EquipRec equip = (EquipRec)equipRequests.elementAt(idx);
    return(equip.getUnitPrice());
  }
  
  public String getPartNum(int idx)
  {
    EquipRec equip = (EquipRec)equipRequests.elementAt(idx);
    return(equip.getPartNum());
  }
  public String getProductName(int idx)
  {
    EquipRec equip = (EquipRec)equipRequests.elementAt(idx);
    return(equip.getProductName());
  }
  public String getLendTypeDesc(int idx)
  {                                        
    EquipRec equip = (EquipRec)equipRequests.elementAt(idx);
    return(equip.getLendTypeDesc());
  }

  public String getLendTypeCode(int idx)
  {
    EquipRec equip = (EquipRec)equipRequests.elementAt(idx);
    return(equip.getLendTypeCode());
  }

  public String getProductDesc(int idx)
  {
    EquipRec equip = (EquipRec)equipRequests.elementAt(idx);
    return(equip.getProductDesc());
  }
  
  public String getProductOption(int idx)
  {
    EquipRec equip = (EquipRec)equipRequests.elementAt(idx);
    return(equip.getProductOption());
  }

  public int getQuanRequested(int idx)
  {
    EquipRec equip = (EquipRec)equipRequests.elementAt(idx);
    return(equip.getQuanRequested());
  }

  public int getEquipTypeCode(int idx)
  {
    EquipRec equip = (EquipRec)equipRequests.elementAt(idx);
    return(equip.getEquipTypeCode());
  }

  public String getEquipTypeDesc(int idx)
  {
    EquipRec equip = (EquipRec)equipRequests.elementAt(idx);
    return(equip.getEquipTypeDesc());
  }
  
  public int getProfileGeneratorCode(int idx)
  {
    EquipRec equip = (EquipRec)equipRequests.elementAt(idx);
    return equip.getProfileGeneratorCode();
  }
  public String getProfileGeneratorName(int idx)
  {
    EquipRec equip = (EquipRec)equipRequests.elementAt(idx);
    return EquipProfileGeneratorDictator.getEquipmentProfileGeneratorName(equip.getProfileGeneratorCode());
  }

  public String getRequestId(int idx)
  {
    RequestRec req = (RequestRec)mmsRequests.elementAt(idx);
    return(req.getRequestId());
  }
  public String getVnumber(int idx)
  {
    RequestRec req = (RequestRec)mmsRequests.elementAt(idx);
    return(req.getVnumber());
  }
  public String getTermAppDescriptor(int idx)
  {
    RequestRec req = (RequestRec)mmsRequests.elementAt(idx);

    StringBuffer sb = new StringBuffer();
    String pgn = req.getProfGenName();
    String mmspgn = EquipProfileGeneratorDictator.getEquipmentProfileGeneratorName(mesConstants.PROFGEN_MMS);
    
    if(pgn.equals(mmspgn) || req.getTermAppProfGen().length()<1) {
      sb.append(req.getTermApplication());
    } else {
      sb.append(mmspgn);
      sb.append(": '");
      sb.append(req.getTermApplication());
      sb.append("' - ");
      sb.append(pgn);
      sb.append(": '");
      sb.append(req.getTermAppProfGen());
      sb.append("'");
    }

    return sb.toString();
  }
  public String getRequestPartNum(int idx)
  {
    RequestRec req = (RequestRec)mmsRequests.elementAt(idx);
    return(req.getPartNum());
  }
  public String getRequestPartDesc(int idx)
  {
    RequestRec req = (RequestRec)mmsRequests.elementAt(idx);
    return(req.getPartDesc());
  }

  public String getAppDate(int idx)
  {
    RequestRec req = (RequestRec)mmsRequests.elementAt(idx);
    return(req.getAppDate());
  }
  public String getProcessStatus(int idx)
  {
    RequestRec req = (RequestRec)mmsRequests.elementAt(idx);
    return(req.getProcessStatus());
  }
  public String getProcessResponse(int idx)
  {
    RequestRec req = (RequestRec)mmsRequests.elementAt(idx);
    return(req.getProcessResponse());
  }
  public String getProfGenName(int idx)
  {
    RequestRec req = (RequestRec)mmsRequests.elementAt(idx);
    return(req.getProfGenName());
  }

  public class EquipRec
  { 
    private String unitPrice      = "";
    private String partNum        = "";
    private String productName    = "";
    private String productDesc    = "";
    private String productOption  = "";
    private String lendTypeDesc   = "";
    private String lendTypeCode   = "";
    private int    quanRequested  = 0;
    private int    equipTypeCode  = -1;
    private String equipTypeDesc  = "";
    private int    profileGeneratorCode = mesConstants.PROFGEN_UNDEFINED;

    EquipRec()
    {}

    public void setProfileGeneratorCode(int v)
    {
      profileGeneratorCode=v;
    }
    public void setQuanRequested(int quanRequested)
    {
      this.quanRequested = quanRequested;
    }
    public void setUnitPrice(String unitPrice)
    {
      this.unitPrice = unitPrice;
    }
    public void setPartNum(String partNum)
    {
      this.partNum = partNum;
    }
    public void setProductName(String productName)
    {
      this.productName = productName;
    }
    public void setProductDesc(String productDesc)
    {
      this.productDesc = productDesc;
    }
    public void setLendTypeDesc(String lendTypeDesc)
    {
      this.lendTypeDesc = lendTypeDesc;
    }
    public void setLendTypeCode(String lendTypeCode)
    {
      this.lendTypeCode = lendTypeCode;
    }
    public void setEquipTypeCode(int equipTypeCode)
    {
      this.equipTypeCode = equipTypeCode;
    }
    public void setEquipTypeDesc(String equipTypeDesc)
    {
      this.equipTypeDesc = equipTypeDesc;
    }
    public void setProductOption(String productOption)
    {
      this.productOption = productOption;
    }

    public int getProfileGeneratorCode()
    {
      return profileGeneratorCode;
    }
    public int getQuanRequested()
    {
      return this.quanRequested;
    }
    public int getEquipTypeCode()
    {
      return this.equipTypeCode;
    }
    public String getEquipTypeDesc()
    {
      return this.equipTypeDesc;
    }
    public String getUnitPrice()
    {
      return this.unitPrice;
    }
    public String getPartNum()
    {
      return this.partNum;
    }
    public String getProductName()
    {
      return this.productName;
    }
    public String getLendTypeDesc()
    {
      return this.lendTypeDesc;
    }
    public String getLendTypeCode()
    {
      return this.lendTypeCode;
    }
    public String getProductDesc()
    {
      return this.productDesc;
    }
    public String getProductOption()
    {
      return this.productOption;
    }
  }

  public class RequestRec
  { 
    private String requestId        = "";
    private String vNumber          = "";
    private String termApplication  = "";
    private String termAppProfGen   = ""; // profile generator specific terminal application
    private String partNum          = "";
    private String partDesc         = "";
    private String appDate          = "";
    private String processStatus    = "";
    private String processResponse  = "";
    private String profGenName      = "";

    RequestRec()
    {}

    public void setRequestId(String requestId)
    {
      this.requestId = requestId;
    }
    public void setVnumber(String vNumber)
    {
      this.vNumber = vNumber;
    }
    public void setTermApplication(String termApplication)
    {
      this.termApplication = termApplication;
    }
    public void setTermAppProfGen(String v)
    {
      this.termAppProfGen = v;
    }
    public void setPartNum(String partNum)
    {
      this.partNum = partNum;
    }
    public void setPartDesc(String partDesc)
    {
      this.partDesc = partDesc;
    }
    public void setAppDate(String appDate)
    {
      this.appDate = appDate;
    }
    public void setProcessStatus(String processStatus)
    {
      this.processStatus = processStatus;
    }
    public void setProcessResponse(String processResponse)
    {
      this.processResponse = processResponse;
    }
    public void setProfGenName(String v)
    {
      this.profGenName = v;
    }


    public String getRequestId()
    {
      return this.requestId;
    }
    public String getVnumber()
    {
      return this.vNumber;
    }
    public String getTermApplication()
    {
      return this.termApplication;
    }
    public String getTermAppProfGen()
    {
      return this.termAppProfGen;
    }
    public String getPartNum()
    {
      return this.partNum;
    }
    public String getPartDesc()
    {
      return this.partDesc;
    }
    public String getAppDate()
    {
      return this.appDate;
    }
    public String getProcessStatus()
    {
      return this.processStatus;
    }
    public String getProcessResponse()
    {
      return this.processResponse;
    }
    public String getProfGenName()
    {
      return this.profGenName;
    }

  }

 
}/*@lineinfo:generated-code*/