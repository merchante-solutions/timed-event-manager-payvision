/*@lineinfo:filename=StatusUpdateBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/StatusUpdateBean.sqlj $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: Hsahourieh $
  Last Modified Date : $Date: 10/08/02 3:46p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ops;

import java.sql.ResultSet;

public class StatusUpdateBean extends com.mes.database.SQLJConnectionBase
{
  /*
  ** CONSTRUCTOR
  */
  public StatusUpdateBean()
  {
  }
  
  /*
  ** METHOD updateCreditStatus
  **
  ** Updates the app_tracking table when the credit status changes
  */
  public void updateCreditStatus(String   userName, 
                                 long     appSeqNum, 
                                 int      statusCode, 
                                 String   reasonCode)
  {
    ResultSet         rs      = null;
    
    int               trackingStatus  = 0;
    boolean           complete        = false;
    boolean           approved        = false;
    StringBuffer      statusDesc      = new StringBuffer("");
    ReasonHelperBean  rhb             = null;
    
    try
    {
      switch(statusCode)
      {
        case QueueConstants.Q_STATUS_APPROVED:
          trackingStatus = QueueConstants.DEPT_STATUS_CREDIT_APPROVED;
          complete = true;
          approved = true;
          statusDesc.append("Approved");
          break;
          
        case QueueConstants.Q_STATUS_DECLINED:
          trackingStatus = QueueConstants.DEPT_STATUS_CREDIT_DECLINED;
          complete = true;
          statusDesc.append("Declined");
          break;
          
        case QueueConstants.Q_STATUS_PENDING:
          trackingStatus = QueueConstants.DEPT_STATUS_CREDIT_PEND;
          statusDesc.append("Pend");
          break;
          
        case QueueConstants.Q_STATUS_CANCELLED:
          trackingStatus = QueueConstants.DEPT_STATUS_CREDIT_CANCEL;
          complete = true;
          statusDesc.append("Canceled");
          break;
          
        default:
          return;
      }
      
      // first update the status
      /*@lineinfo:generated-code*//*@lineinfo:92^7*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//          set     status_code   = :trackingStatus,
//                  contact_name  = :userName
//          where   app_seq_num   = :appSeqNum and
//                  dept_code     = :QueueConstants.DEPARTMENT_CREDIT
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n        set     status_code   =  :1 ,\n                contact_name  =  :2 \n        where   app_seq_num   =  :3  and\n                dept_code     =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.ops.StatusUpdateBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,trackingStatus);
   __sJT_st.setString(2,userName);
   __sJT_st.setLong(3,appSeqNum);
   __sJT_st.setInt(4,QueueConstants.DEPARTMENT_CREDIT);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:99^7*/
      
      // now update the date if completed
      if(complete)
      {
        /*@lineinfo:generated-code*//*@lineinfo:104^9*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//            set     date_completed = sysdate
//            where   app_seq_num = :appSeqNum and
//                    dept_code   = :QueueConstants.DEPARTMENT_CREDIT
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n          set     date_completed = sysdate\n          where   app_seq_num =  :1  and\n                  dept_code   =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.ops.StatusUpdateBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,QueueConstants.DEPARTMENT_CREDIT);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:110^9*/
        
        if(approved)
        {
          /*@lineinfo:generated-code*//*@lineinfo:114^11*/

//  ************************************************************
//  #sql [Ctx] { update  app_tracking
//              set     date_received = sysdate
//              where   app_seq_num = :appSeqNum and
//                      dept_code = :QueueConstants.DEPARTMENT_TSYS
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  app_tracking\n            set     date_received = sysdate\n            where   app_seq_num =  :1  and\n                    dept_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.ops.StatusUpdateBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,QueueConstants.DEPARTMENT_TSYS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:120^11*/
        }
      }
      
      if(!approved)
      {
        rhb = new ReasonHelperBean();
        rhb.connect();
        rhb.setReasons(statusCode,reasonCode);
        if(rhb.hasNext())
        {
          statusDesc.append(" - ");
          statusDesc.append(rhb.getNextLongDesc());
        }
        while(rhb.hasNext())
        {
          statusDesc.append(", " + rhb.getNextLongDesc());
        }
        
/*********************************************************
        String longDescr = "";
        #sql [Ctx]
        {
          select  qreason_long_descr into :longDescr
          from    qreason
          where   qreason_code = :reasonCode and
                  qreason_type = :statusCode
        };
        
        if(longDescr != null && !longDescr.equals(""))
        {
          statusDesc.append(" - ");
          statusDesc.append(longDescr);
        }
***********************************************************/
      }
        
      // add a note with details
      addNote(appSeqNum, 10, userName, QueueConstants.DEPARTMENT_CREDIT, statusDesc.toString());
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), 
        "updateCreditStatus (" +
        appSeqNum + ", " +
        statusCode + ", " +
        reasonCode + "): " +
        e.toString());
    }
    finally
    {
      if (rhb != null)
      {
        rhb.cleanUp();
      }
    }
  }
  
  public void addNote(long appSeqNum, 
                      int queueStage, 
                      String userName, 
                      int dept,
                      String note)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:186^7*/

//  ************************************************************
//  #sql [Ctx] { insert into app_notes
//          (
//            app_seq_num,
//            app_queue_stage,
//            app_note_date,
//            app_note_user,
//            app_note_message,
//            app_department
//          )
//          values
//          (
//            :appSeqNum,
//            :queueStage,
//            sysdate,
//            :userName,
//            :note,
//            :dept
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into app_notes\n        (\n          app_seq_num,\n          app_queue_stage,\n          app_note_date,\n          app_note_user,\n          app_note_message,\n          app_department\n        )\n        values\n        (\n           :1 ,\n           :2 ,\n          sysdate,\n           :3 ,\n           :4 ,\n           :5 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.ops.StatusUpdateBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,queueStage);
   __sJT_st.setString(3,userName);
   __sJT_st.setString(4,note);
   __sJT_st.setInt(5,dept);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:206^7*/
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), "addNote: " + e.toString());
    }
  }
}/*@lineinfo:generated-code*/