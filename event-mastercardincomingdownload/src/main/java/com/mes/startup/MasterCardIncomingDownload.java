/*@lineinfo:filename=MasterCardIncomingDownload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/MasterCardIncomingDownload.sqlj $

  Description:  
  

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2009-2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Date;
import java.util.Iterator;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.mes.config.MesDefaults;
import com.mes.constants.MesFlatFiles;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.support.StringUtilities;

public class MasterCardIncomingDownload 
  extends EventBase
{
  static Logger log = Logger.getLogger(MasterCardIncomingDownload.class);
  
  public MasterCardIncomingDownload()
  {
  }
  
  public boolean execute()
  {
    Vector    dataFilenames = new Vector();
    String    dataFilename  = null;
    String    flagFilename  = null;
    boolean   retVal        = false;
    Sftp      sftp          = null;
    
    
    try
    {
      //get the Sftp connection
      sftp = getSftp( MesDefaults.getString(MesDefaults.DK_MC_HOST),
                      MesDefaults.getString(MesDefaults.DK_MC_USER),
                      MesDefaults.getString(MesDefaults.DK_MC_PASSWORD),
                      MesDefaults.getString(MesDefaults.DK_MC_INCOMING_PATH),
                      true    // binary
                    );
      
      String filePrefix = (getEventArgCount() == 0 ? "" : getEventArg(0));
      EnumerationIterator enumi = new EnumerationIterator();
      Iterator it = enumi.iterator(sftp.getNameListing(filePrefix + ".*\\.flg"));
      
      while( it.hasNext() )
      {
        flagFilename = (String)it.next();
        dataFilename = flagFilename.substring(0,flagFilename.lastIndexOf(".")) + ".dat";
        
        sftp.download(dataFilename, dataFilename);
        sftp.deleteFile(flagFilename);
        sftp.deleteFile(dataFilename);
        
        dataFilenames.addElement(dataFilename);
      }
      
      // some files are processed by the edit package, 
      // others are loaded directly into the database
      for( int i = 0; i < dataFilenames.size(); ++i )
      {
        dataFilename = (String)dataFilenames.elementAt(i);
        
        if ( dataFilename.startsWith("mc_ti_fees") )
        {
          loadTifDetailData(dataFilename,true); // load then archive file
        }
      }
      
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ sftp.disconnect(); } catch( Exception ee ) {}
      
    }
    return( retVal );
  }
  
  protected String generateLoadFilename( String inputFilename ) 
  {
    String[][]    filePrefixes  =
    {
      {"mc_ti_fees"         , "mc_tif_dt"},
    };
    
    // strip the path from the input filename
    String loadFilename = inputFilename.replace('\\','/');
    if ( loadFilename.indexOf("/") >= 0 )
    {
      loadFilename = loadFilename.substring(loadFilename.lastIndexOf("/")+1);
    }
    
    for( int i = 0; i < filePrefixes.length; ++i )
    {
      if ( loadFilename.startsWith( filePrefixes[i][0] ) )
      {
        loadFilename = (filePrefixes[i][1] + 9999 + loadFilename.substring( filePrefixes[i][0].length() ));
        break;
      }
    }
    return( loadFilename );      
  }
  
  public void loadTifDetailData( String inputFilename, boolean archiveFile )
  {
    FlatFileRecord      ffd             = null;
    BufferedReader      in              = null;
    int                 insertCount     = 0;
    String              line            = null;
    String              loadFilename    = null;
    long                loadFileId      = 0L;
    long                merchantId      = 0L;
    Date                reportDate      = null;
    
    try
    {
      connect(true); // timeout exempt
      setAutoCommit(false);
      
      loadFilename = generateLoadFilename(inputFilename);
      
      File f = new File(inputFilename);
      f.renameTo( new File(loadFilename) );
      
      in  = new BufferedReader( new FileReader( loadFilename ) );
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_MC_TIF_DETAIL);
      
      System.out.println("Processing MasterCard TIF File - " + loadFilename);
      
      /*@lineinfo:generated-code*//*@lineinfo:164^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init(:loadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init( :1 )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.MasterCardIncomingDownload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:167^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:169^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:loadFilename),
//                  get_file_date(:loadFilename)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1 ),\n                get_file_date( :2 )\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.MasterCardIncomingDownload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setString(2,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   reportDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:175^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:177^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    mc_tif_detail           tif
//          where   load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    mc_tif_detail           tif\n        where   load_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.MasterCardIncomingDownload",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:182^7*/
      
      while( (line = in.readLine()) != null )
      {
        ffd.resetAllFields();
        ffd.suck(StringUtilities.leftJustify(line,219,' '));
        merchantId = resolveMid(ffd.getFieldData("merchant_number"));
        
        System.out.print("Processing " + ++insertCount + "            \r");//@
        /*@lineinfo:generated-code*//*@lineinfo:191^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mc_tif_detail
//            (
//              merchant_number,
//              dba_name_location,
//              sic_code,
//              ica,
//              forwarding_institution_id,
//              billing_event,
//              card_number,
//              report_date,
//              auth_date,
//              reversal_date,
//              auth_code,
//              product_code,
//              banknet_ref_num,
//              transaction_amount,
//              load_filename,
//              load_file_id
//            )
//            values
//            (
//              :merchantId,
//              upper(trim(:ffd.getFieldData("dba_name_location"))),
//              trim(:ffd.getFieldData("sic_code")),
//              :ffd.getFieldAsInt("ica"),
//              :ffd.getFieldAsInt("forwarding_institution_id"),
//              trim(:ffd.getFieldData("billing_event")),
//              lower(trim(:ffd.getFieldData("card_number"))),
//              :reportDate,
//              :ffd.getFieldAsSqlDate("auth_date"),
//              :ffd.getFieldAsSqlDate("reversal_date"),
//              trim(:ffd.getFieldData("auth_code")),
//              trim(:ffd.getFieldData("product_code")),
//              trim(:ffd.getFieldData("banknet_ref_num")),
//              :ffd.getFieldAsDouble("transaction_amount"),
//              :loadFilename,
//              :loadFileId
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4315 = ffd.getFieldData("dba_name_location");
 String __sJT_4316 = ffd.getFieldData("sic_code");
 int __sJT_4317 = ffd.getFieldAsInt("ica");
 int __sJT_4318 = ffd.getFieldAsInt("forwarding_institution_id");
 String __sJT_4319 = ffd.getFieldData("billing_event");
 String __sJT_4320 = ffd.getFieldData("card_number");
 java.sql.Date __sJT_4321 = ffd.getFieldAsSqlDate("auth_date");
 java.sql.Date __sJT_4322 = ffd.getFieldAsSqlDate("reversal_date");
 String __sJT_4323 = ffd.getFieldData("auth_code");
 String __sJT_4324 = ffd.getFieldData("product_code");
 String __sJT_4325 = ffd.getFieldData("banknet_ref_num");
 double __sJT_4326 = ffd.getFieldAsDouble("transaction_amount");
   String theSqlTS = "insert into mc_tif_detail\n          (\n            merchant_number,\n            dba_name_location,\n            sic_code,\n            ica,\n            forwarding_institution_id,\n            billing_event,\n            card_number,\n            report_date,\n            auth_date,\n            reversal_date,\n            auth_code,\n            product_code,\n            banknet_ref_num,\n            transaction_amount,\n            load_filename,\n            load_file_id\n          )\n          values\n          (\n             :1 ,\n            upper(trim( :2 )),\n            trim( :3 ),\n             :4 ,\n             :5 ,\n            trim( :6 ),\n            lower(trim( :7 )),\n             :8 ,\n             :9 ,\n             :10 ,\n            trim( :11 ),\n            trim( :12 ),\n            trim( :13 ),\n             :14 ,\n             :15 ,\n             :16 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.MasterCardIncomingDownload",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setString(2,__sJT_4315);
   __sJT_st.setString(3,__sJT_4316);
   __sJT_st.setInt(4,__sJT_4317);
   __sJT_st.setInt(5,__sJT_4318);
   __sJT_st.setString(6,__sJT_4319);
   __sJT_st.setString(7,__sJT_4320);
   __sJT_st.setDate(8,reportDate);
   __sJT_st.setDate(9,__sJT_4321);
   __sJT_st.setDate(10,__sJT_4322);
   __sJT_st.setString(11,__sJT_4323);
   __sJT_st.setString(12,__sJT_4324);
   __sJT_st.setString(13,__sJT_4325);
   __sJT_st.setDouble(14,__sJT_4326);
   __sJT_st.setString(15,loadFilename);
   __sJT_st.setLong(16,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:231^9*/
        
        if ( insertCount%200 == 0 ) { /*@lineinfo:generated-code*//*@lineinfo:233^39*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:233^59*/  }
      }
      
      // queue the billing summarization
      /*@lineinfo:generated-code*//*@lineinfo:237^7*/

//  ************************************************************
//  #sql [Ctx] { insert into mbs_process
//          (
//            process_type,process_sequence,load_filename
//          )
//          values
//          (
//            6,0,:loadFilename
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mbs_process\n        (\n          process_type,process_sequence,load_filename\n        )\n        values\n        (\n          6,0, :1 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.MasterCardIncomingDownload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:247^7*/
      
      if ( archiveFile )
      {
        archiveDailyFile(loadFilename);
      }
    }
    catch( Exception e )
    {
      logEntry("loadTifDetailData(" + loadFilename + ")", e.toString());
    }
    finally
    {
      setAutoCommit(true);
      try{ in.close(); }catch( Exception ee ){}
      cleanUp();
    }
  }
  
  public long resolveMid( String rawData )
  {
    long      merchantId    = 0L;
    String    temp          = (rawData == null ? null : rawData.trim());
  
    try
    {
      if ( isNumber(temp) )
      {
        while( merchantId == 0L && temp.length() > 0 )
        {
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:279^13*/

//  ************************************************************
//  #sql [Ctx] { select  mf.merchant_number
//                
//                from    mif     mf
//                where   mf.merchant_number = to_number(:temp)
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mf.merchant_number\n               \n              from    mif     mf\n              where   mf.merchant_number = to_number( :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.MasterCardIncomingDownload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,temp);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:285^13*/
          }
          catch( Exception ee )
          {
            temp = temp.substring(0,temp.length()-1); 
          }
        }
      }
    }
    catch( Exception e )
    {
      logEntry("resolveMid(" + rawData + ")",e.toString());
    }
    finally
    {
    }
    return( merchantId );
  }

  public boolean isNumber( String lval )
  {
    boolean       retVal      = false;
    
    try
    {
      long temp = Long.parseLong(lval);
      retVal = true;
    }
    catch( Exception e )
    {
      // ignore
    }
    return( retVal );
  }
  
  public static void main( String[] args )
  {
    MasterCardIncomingDownload  mc    = null;
    
    try
    {
        if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{
                    MesDefaults.DK_MC_HOST,
                    MesDefaults.DK_MC_USER,
                    MesDefaults.DK_MC_PASSWORD,
                    MesDefaults.DK_MC_INCOMING_PATH
            });
        }

      SQLJConnectionBase.initStandalone();
      
      mc = new MasterCardIncomingDownload();
      
      if ( args.length > 0 && "loadTifDetailData".equals(args[0]) )
      {
        mc.loadTifDetailData(args[1],false);  // load, don't archive
      }
      else    // default to standard event execute()
      {
        if ( args.length > 0 ) 
        {
          mc.setEventArgs(args[0]);
        }
        mc.execute();
      }
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ mc.cleanUp(); } catch( Exception ee ){}
    }
  }
}/*@lineinfo:generated-code*/