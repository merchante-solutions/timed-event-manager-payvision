/*************************************************************************

  FILE: $Archive: /src/test/com/mes/test/MockHttpServletRequest.java $

  Description:


  Last Modified By   : $Author:  $
  Last Modified Date : $Date:  $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.test;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.security.Principal;

import java.util.Collection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;

public class MockHttpServletRequest implements HttpServletRequest {
	public Hashtable RequestParameters = new Hashtable();

	public void setParameter(String key, String value) {
		RequestParameters.put(key, value);
	}

	public String getParameter(String key) {
		return((String)RequestParameters.get(key));
	}

	public Enumeration getParameterNames() {
		return( RequestParameters.keys() );
	}

	public Cookie[] getCookies() {return null;}
	public String getMethod(){return null;}
	public String getRequestURI(){return null;}
	public java.lang.StringBuffer getRequestURL(){return null;}
	public String getServletPath(){return null;}
	public String getPathInfo(){return null;}
	public String getPathTranslated(){return null;}
	public String getQueryString(){return null;}
	public String getRemoteUser(){return null;}
	public String getAuthType(){return null;}
	public String getHeader(String name){return null;}
	public int getIntHeader(String name){return 0;}
	public long getDateHeader(String name){return 0;}
	public Enumeration getHeaderNames(){return null;}
	public HttpSession getSession(boolean create){return null;}
	public String getRequestedSessionId(){return null;}
	public boolean isRequestedSessionIdValid(){return false;}
	public boolean isRequestedSessionIdFromCookie(){return false;}
	public boolean isRequestedSessionIdFromUrl(){return false;}
	@Override public boolean authenticate(HttpServletResponse httpServletResponse) throws IOException, ServletException {return false;}
	@Override public void login(String s, String s1) throws ServletException {}
	@Override public void logout() throws ServletException {}
	@Override public Collection<Part> getParts() throws IOException, IllegalStateException, ServletException {return null;}
	@Override public Part getPart(String s) throws IOException, IllegalStateException, ServletException {return null;}
	public int getContentLength(){return 0;}
	public String getContentType(){return null;}
	public String getProtocol(){return null;}
	public String getScheme(){return null;}
	public String getServerName(){return null;}
	public int getServerPort(){return 0;}
	public String getRemoteAddr(){return null;}
	public String getRemoteHost(){return null;}
	public String getRealPath(String path){return null;}
	@Override public int getRemotePort() {return 0;}
	@Override public String getLocalName() {return null;}
	@Override public String getLocalAddr() {return null;}
	@Override public int getLocalPort() {return 0;}
	@Override public ServletContext getServletContext() {return null;}
	@Override public AsyncContext startAsync() {return null;}
	@Override public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse) {return null;}
	@Override public boolean isAsyncStarted() {return false;}
	@Override public boolean isAsyncSupported() {return false;}
	@Override public AsyncContext getAsyncContext() {return null;}
	@Override public DispatcherType getDispatcherType() {return null;}
	public ServletInputStream getInputStream() throws IOException{return null;}
	public String[] getParameterValues(String name){return null;}
	public Enumeration getAttributeNames(){return null;}
	public Object getAttribute(String name){return null;}
	public HttpSession getSession(){return null;}
	public BufferedReader getReader() throws IOException{return null;}
	public String getCharacterEncoding(){return null;}
	public void setAttribute(String name, Object o) {}
	public boolean isRequestedSessionIdFromURL() {return false;}
  public Principal getUserPrincipal() { return(null); }
  public boolean isUserInRole(String role){ return(false); }
  public String getContextPath(){ return(null); }
  public Enumeration getHeaders(String name) { return(null); }
  public RequestDispatcher getRequestDispatcher(String path){ return(null); }
  public boolean isSecure(){ return(false); }
  public Locale getLocale() { return(null); }
  public Enumeration getLocales() { return(null); }
  public void removeAttribute(String name){}
  public Map getParameterMap() { return(null); }
  public void setCharacterEncoding(String env){}
}