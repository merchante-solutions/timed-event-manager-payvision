/*************************************************************************

  FILE: $Archive: /src/test/com/mes/reports/TestBankContractDataBean.sqlj $

  Description:  


  Last Modified By   : $Author:  $
  Last Modified Date : $Date:  $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.util.Calendar;

import com.mes.test.MesTestCase;
import com.mes.test.MockHttpServletRequest;
import com.mes.user.UserBean;
import com.mes.tools.HierarchyTree;

import junit.framework.*;

public class TestBankContractDataBean extends MesTestCase
{
  BankContractDataBean        TestBean    = new BankContractDataBean();
  
  public void test_getUserType() throws Exception
  {
    Date      activeDate      = null;
    
    try
    {
      Calendar  cal = Calendar.getInstance();
      cal.set(Calendar.MONTH,Calendar.AUGUST);
      cal.set(Calendar.DAY_OF_MONTH,1);
      cal.set(Calendar.YEAR,2008);
      activeDate = new java.sql.Date( cal.getTime().getTime() );
      
      TestBean.connect();
      TestBean.setReportDateBegin(activeDate);
      TestBean.setReportDateEnd(activeDate);
      
      TestBean.setReportHierarchyNode(394200000158L);
      TestBean.setReportHierarchyNodeDefault(394200000L);
      assertEquals("394200000158/394200000",BankContractDataBean.UT_CONTRACT_BANK,TestBean.getUserType());

      TestBean.setReportHierarchyNodeDefault(HierarchyTree.DEFAULT_HIERARCHY_NODE);
      assertEquals("394200000158/394200000",BankContractDataBean.UT_MES,TestBean.getUserType());
      
      TestBean.setReportHierarchyNode(941000000001L);
      TestBean.setReportHierarchyNodeDefault(3941600001L);
      assertEquals("941000000001/3941600001",BankContractDataBean.UT_MES,TestBean.getUserType());
      
      TestBean.setReportHierarchyNodeDefault(394100000L);
      assertEquals("941000000001/394100000",BankContractDataBean.UT_MES,TestBean.getUserType());
      
      TestBean.setReportHierarchyNodeDefault(HierarchyTree.DEFAULT_HIERARCHY_NODE);
      assertEquals("941000000001/DEFAULT_HIERARCHY_NODE",BankContractDataBean.UT_MES,TestBean.getUserType());
      
      TestBean.setReportHierarchyNode(3941400010L);
      TestBean.setReportHierarchyNodeDefault(3941400010L);
      assertEquals("3941400010/3941400010",BankContractDataBean.UT_CONTRACT_BANK,TestBean.getUserType());
      
      TestBean.setReportHierarchyNodeDefault(394100000L);
      assertEquals("3941400010/394100000",BankContractDataBean.UT_MES,TestBean.getUserType());
      
      TestBean.setReportHierarchyNodeDefault(HierarchyTree.DEFAULT_HIERARCHY_NODE);
      assertEquals("3941400010/DEFAULT_HIERARCHY_NODE",BankContractDataBean.UT_MES,TestBean.getUserType());
      
      TestBean.setReportHierarchyNode(10192058L);
      TestBean.setReportHierarchyNodeDefault(385800000L);
      assertEquals("10192058/385800000",BankContractDataBean.UT_VAR_BANK,TestBean.getUserType());
    }
    finally
    {
      TestBean.cleanUp();
    }      
  }
}