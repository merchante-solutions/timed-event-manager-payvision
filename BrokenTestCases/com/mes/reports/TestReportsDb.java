/*************************************************************************

  FILE: $Archive: /src/test/com/mes/reports/TestReportsDb.sqlj $

  Description:  


  Last Modified By   : $Author:  $
  Last Modified Date : $Date:  $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.util.Calendar;

import com.mes.test.MesTestCase;
import com.mes.test.MockHttpServletRequest;

import junit.framework.*;

public class TestReportsDb extends MesTestCase
{
  public void test_loadChargebackStatistics()
  {
    Calendar      cal       = Calendar.getInstance();
    Date          beginDate = null;
    Date          endDate   = null;
    
    cal.set(Calendar.MONTH,Calendar.SEPTEMBER);
    cal.set(Calendar.DAY_OF_MONTH,1);
    cal.set(Calendar.YEAR,2008);
    
    beginDate = new java.sql.Date(cal.getTime().getTime());
    endDate   = new java.sql.Date(cal.getTime().getTime());
    
    SalesStatistic cbStats = ReportsDb.loadChargebackStatistics(941000000002L,beginDate,endDate);
    
    assertNotNull("cbStats should not be null",cbStats);
    
    assertEquals("CB count invalid",0,cbStats.getCount());
    assertEquals("CB count MTD invalid",1,cbStats.getCountMTD());
    assertEquals("CB count QTD invalid",1,cbStats.getCountQTD());
    assertEquals("CB count YTD invalid",2,cbStats.getCountYTD());
    
    assertEquals("sales count invalid",71,cbStats.getVMCSalesCount());
    assertEquals("sales count MTD invalid",1692,cbStats.getVMCSalesCountMTD());
    assertEquals("sales count QTD invalid",4893,cbStats.getVMCSalesCountQTD());
    assertEquals("sales count YTD invalid",16759,cbStats.getVMCSalesCountYTD());
  }
}