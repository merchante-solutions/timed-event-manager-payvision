/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/test/com/mes/reports/TestMerchUnsettledDataBean.java $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2009-04-10 15:49:34 -0700 (Fri, 10 Apr 2009) $
  Version            : $Revision: 15978 $

  Change History:
     See SVN database

  Copyright (C) 2007-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;

import com.mes.user.UserBean;
import com.mes.test.MesTestCase;
import com.mes.test.MockHttpServletRequest;
import com.mes.settlement.MockTransactionFactory;

import junit.framework.*;

public class TestMerchUnsettledDataBean extends MesTestCase
{
  public void tearDown() throws Exception
  {
    fail("Failing due compilation error."); //FIXME: compilation error
//    MockTransactionFactory.removeAllTransactions();
    super.tearDown();
  }

  public void test_closeBatch()
    throws Exception
  {
    fail("Failing due compilation error."); //FIXME: compilation error
//    MerchUnsettledDataBean    testBean    = new MerchUnsettledDataBean();
//    String visaTranId = MockTransactionFactory.generateTranId();
//    String mcTranId = MockTransactionFactory.generateTranId();
//
//    System.out.println("visaTranId : " + visaTranId);
//    System.out.println("mcTranId   : " + mcTranId);
//
//    MockTransactionFactory.createSettledTransactionVisa(visaTranId);
//    MockTransactionFactory.clearBatchCloseData(visaTranId);
//    MockTransactionFactory.setTestFlag(visaTranId,"N");
//    MockTransactionFactory.createSettledTransactionVisa(mcTranId);
//    MockTransactionFactory.clearBatchCloseData(mcTranId);
//    MockTransactionFactory.setTestFlag(mcTranId,"N");
//
//    // validate the visa tran
//    PreparedStatement ps = DbConnection.getPreparedStatement("select * from trident_capture_api where trident_tran_id = ?");
//    ps.setString(1,visaTranId);
//    ResultSet rs = ps.executeQuery();
//    assertTrue("missing visa rec",rs.next());
//    assertNull("visa batch num",rs.getString("batch_number"));
//    assertNull("visa batch date",rs.getDate("batch_date"));
//    assertEquals("visa test flag","N",rs.getString("test_flag"));
//    rs.close();
//
//    // validate the MC tran
//    ps.setString(1,mcTranId);
//    rs = ps.executeQuery();
//    assertTrue("missing visa rec",rs.next());
//    assertNull("visa batch num",rs.getString("batch_number"));
//    assertNull("visa batch date",rs.getDate("batch_date"));
//    assertEquals("visa test flag","N",rs.getString("test_flag"));
//    rs.close();
//
//    // setup a dummy user bean
//    UserBean user = new UserBean();
//    user.forceValidate("941000086152");
//
//    // setup a mock servlet request
//    MockHttpServletRequest  request = new MockHttpServletRequest();
//    request.setParameter("profileId","94100008615200000001");
//
//    // simulate report creation
//    testBean.connect();
//    testBean.setReportUserBean(user);
//    testBean.initialize();
//    testBean.autoSetFields(request);
//
//    // validate that incoming params were loaded
//    assertEquals("profile id","94100008615200000001",testBean.getData("profileId"));
//
//    // close the batch
//    testBean.closeBatch();
//    testBean.cleanUp();
//
//    assertEquals("log entries",0,getLogEntryCount());
//
//    // validate the visa
//    ps.setString(1,visaTranId);
//    rs = ps.executeQuery();
//    assertTrue("missing visa",rs.next());
//    assertNotNull("visa batch number",rs.getString("batch_number"));
//    assertNotNull("visa batch date",rs.getDate("batch_date"));
//    rs.close();
//
//    // validate the visa
//    ps.setString(1,mcTranId);
//    rs = ps.executeQuery();
//    assertTrue("missing mc",rs.next());
//    assertNotNull("mc batch number",rs.getString("batch_number"));
//    assertNotNull("mc batch date",rs.getDate("batch_date"));
//    rs.close();
//
//    ps.close();   // close statement
  }
}