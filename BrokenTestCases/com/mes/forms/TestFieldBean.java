/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/test/com/mes/forms/TestFieldBean.java $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $LastChangedDate: 2009-05-11 09:11:49 -0700 (Mon, 11 May 2009) $
  Version            : $Revision: 16094 $

  Change History:
     See SVN database

  Copyright (C) 2007-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.forms;

import com.mes.test.MesTestCase;
import junit.framework.*;

public class TestFieldBean extends MesTestCase
{
  public void test_getPaddedString() throws Exception
  {
    FieldBean   testClass   = new FieldBean();
    
    assertEquals("bad empty string", "   ", testClass.getPaddedString("dummy",3));
    testClass.fields.add( new HiddenField("dummy") );
    
    String testValue = "silly-test-value";
    testClass.setData("dummy",testValue);
    assertEquals("bad string, no pad",testValue,testClass.getPaddedString("dummy",testValue.length()));
    assertEquals("bad string, with pad",testValue + "  ", testClass.getPaddedString("dummy",testValue.length()+2));
  }
}