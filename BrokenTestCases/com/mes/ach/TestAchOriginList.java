/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/test/com/mes/ach/TestAchOriginList.java $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $LastChangedDate: 2009-01-22 18:20:19 -0800 (Thu, 22 Jan 2009) $
  Version            : $Revision: 15736 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ach;

import java.sql.Date;
import java.util.Calendar;
import com.mes.database.SQLJConnectionBase;
import com.mes.test.MesTestCase;
import junit.framework.*;

public class TestAchOriginList extends MesTestCase
{
  AchOriginList         TestClass     = new AchOriginList();
  
  public void test_load()
  {
    fail("Failing due compilation error"); //FIXME: compilation error
//    TestClass.load( 0L, true );
//    assertNotNull("invalid vector",TestClass.getOriginVector());
//    assertTrue("no records found", (0 != TestClass.getOriginVector().size()));
  }    
  
  public void test_getMerchantOriginNode()
  {
    assertTrue("missing unit test acct",(0L != TestClass.getMerchantOriginNode(941000086152L,"MERCH DEP",true)));
    assertEquals("chargeback accept test",0L,TestClass.getMerchantOriginNode(941000086152L,"MERCH CHBK",true));
    //@ uncomment once JPMC chargebacks go into production for 3941
    //@assertEquals("chargeback accept prod",394100000L,TestClass.getMerchantOriginNode(941000086152L,"MERCH CHBK",false));
    assertEquals("missing unit prod acct",394100000L,TestClass.getMerchantOriginNode(941000086152L,"MANUAL ADJ",false));
    
    // use default (test mode = false) on a production account
    assertTrue("prod acct missing",(0L != TestClass.getMerchantOriginNode(394200100053L,"MERCH DEP")));
    assertEquals("prod acct missing",394200000L,TestClass.getMerchantOriginNode(394200100053L,"MERCH DEP"));
  }
  
  public void test_getMerchantOrigin()
  {
    AchOriginList.AchOrigin     origin      = null;
    long                        originNode  = 0L;

    fail("Failing due compilation error"); //FIXME: compilation error
//    TestClass.load( 0L, true );
//    originNode = TestClass.getMerchantOriginNode(941000086152L,"MERCH DEP",true);
//    assertEquals("bad node",941000086152L,originNode);
//
//    origin = TestClass.getMerchantOrigin(941000086152L,"MERCH DEP",true);
//    assertTrue("origin is null",(origin != null));
//    assertEquals("origin node",941000086152L,origin.getOriginNode());
//    assertTrue("merch dep",origin.isEntryDescriptionEnabled("MERCH DEP",true));
//    assertFalse("merch adj",origin.isEntryDescriptionEnabled("MERCH ADJ",true));
//    assertFalse("merch chbk",origin.isEntryDescriptionEnabled("MERCH CHBK",true));
//    assertFalse("merch dep prod",origin.isEntryDescriptionEnabled("MERCH DEP"));
  }
  
  public void test_isEntryDescriptionEnabled()
  {
    fail("Failing due compilation error"); //FIXME: compilation error
//    assertTrue("dep should be enabled",TestClass.isEntryDescriptionEnabled(941000086152L,"MERCH DEP",true));
//    assertTrue("adj should be enabled",TestClass.isEntryDescriptionEnabled(941000086152L,"MERCH ADJ",true));
//    assertFalse("cb should be disabled",TestClass.isEntryDescriptionEnabled(941000086152L,"MERCH CHBK",true));
//    assertFalse("dep should be disabled",TestClass.isEntryDescriptionEnabled(941000086152L,"MERCH DEP",false));
  }
}