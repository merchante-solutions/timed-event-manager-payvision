/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/test/com/mes/ach/TestAchDb.java $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $LastChangedDate: 2009-01-22 18:20:19 -0800 (Thu, 22 Jan 2009) $
  Version            : $Revision: 15736 $

  Change History:
     See SVN database

  Copyright (C) 2007-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.ach;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.Calendar;
import com.mes.test.MesTestCase;
import junit.framework.*;
@Ignore
public class TestAchDb extends MesTestCase
{
  protected static final  String  AchStatementTableName = "ach_trident_statement_test";
  protected static final  long    TestRecId             = -212L;
  
  public void tearDown() throws Exception
  {
    try
    {
      PreparedStatement ps = DbConnection.getPreparedStatement("delete from " + AchStatementTableName + " where rec_id = " + TestRecId);
      ps.executeUpdate();
      ps.close();
    }
    finally
    {
      super.tearDown();
    }
  }
  
  public void test_storeAchStatementRecord() throws Exception
  {
    double                  netAmount = 122.20;
    PreparedStatement       ps        = null;
    AchStatementRecord      rec       = new AchStatementRecord();
    ResultSet               rs        = null;
    
    try
    {
      rec.setRecId           ( TestRecId );
      rec.setMerchantId      ( 941000086152L );
      rec.setPostDate        ( new java.sql.Date(Calendar.getInstance().getTime().getTime()) );
      rec.setEntryDesc       ( "MERCH DEP" );
      rec.setSalesCount      ( 10 );
      rec.setSalesAmount     ( 127.35 );
      rec.setCreditsCount    ( 1 );
      rec.setCreditsAmount   ( 5.15 );
      rec.setNetAmount       ( netAmount );
      rec.setAchAmount       ( (netAmount >=0.0 ? netAmount : (netAmount * -1)) );
      rec.setTransactionCode ( (netAmount >=0.0 ? 22 : 27) );
    
      AchDb.storeAchStatementRecord(rec,AchStatementTableName);
    
      ps = DbConnection.getPreparedStatement("select count(1) as rec_count from " + AchStatementTableName + " where rec_id = " + TestRecId);
      rs = ps.executeQuery();
      assertTrue("insert failed",rs.next());
      assertEquals("rec count",1,rs.getInt("rec_count"));
      rs.close();
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
    }
  }
}