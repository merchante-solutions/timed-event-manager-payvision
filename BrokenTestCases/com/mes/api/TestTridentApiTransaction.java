/*************************************************************************

  FILE: $Archive: /src/test/com/mes/api/TestTridentApiTransaction.java $

  Description:  


  Last Modified By   : $Author:  $
  Last Modified Date : $Date:  $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import com.mes.settlement.MockTransactionFactory;
import org.apache.log4j.*;
import com.payvision.gateway.*;
import com.mes.config.MesDefaults;

import com.mes.test.MesTestCase;
import com.mes.test.MockHttpServletRequest;
import junit.framework.*;

public class TestTridentApiTransaction extends MesTestCase
{
  private   String      TestProfileId     = "94100008615200000001";
  
  public void tearDown() throws Exception
  {
//    MockTransactionFactory.removeAllTransactions();  //FIXME: compilation error
    super.tearDown();
  }
  
  public void test_isFullRefund() throws Exception
  {
    fail("Failing due compilation error."); //FIXME: compilation error
//    String                    apiTranId   = MockTransactionFactory.generateTranId();
//    TridentApiProfile         profile     = null;
//    MockHttpServletRequest    request     = new MockHttpServletRequest();
//    TridentApiTransaction     testTran    = null;
//
//    // create a dummy 10 EUR settled transaction in the trident_capture_api table
//    MockTransactionFactory.createSettledTransactionVisa(apiTranId,"978",true);
//
//    // mark the transaction as cleared
//    MockTransactionFactory.markTransactionCleared(apiTranId);
//
//    // build a mock servlet request to refund the transaction
//	  request.setParameter("profile_id",                TestProfileId);
//	  request.setParameter("profile_key",               "DZgZKrWxPKbcCCXSZEymdPCkjNhFwkaU");
//    request.setParameter("transaction_type",          "U");
//    request.setParameter("transaction_id",            apiTranId);
//
//    // create a profile and enable FX by setting the client id
//    profile = ApiDb.loadProfile(TestProfileId);
//    profile.setFxClientId(9999);
//
//    // create the test object
//    testTran = new TridentApiTransaction(MockTransactionFactory.generateTranId());
//    testTran.setProperties(request);
//    testTran.setProfile(profile);
//    testTran.loadData();
//
//    assertTrue("should be a full refund",testTran.isFullRefund());
//    testTran.setTranAmount(5.00);
//    assertFalse("should not be a full refund",testTran.isFullRefund());
  }
  
  public void test_isFxAmountValid()
  {
    TridentApiProfile         profile       = null;
    TridentApiTransaction     testTran      = null;
    double                    thresholdRate = 0.01;

    fail("Failing due compilation error."); //FIXME: compilation error

//    try
//    {
//      thresholdRate = (double)(MesDefaults.getInt(MesDefaults.DK_FX_CONVERSION_TOLERANCE,100) * 0.0001);
//    }
//    catch( Exception e )
//    {
//      // ignore, use default of 1%
//    }

//    profile = ApiDb.loadProfile(TestProfileId);
//    profile.setFxClientId(1027);
//    testTran = new TridentApiTransaction(MockTransactionFactory.generateTranId());
//    testTran.setProfile(profile);
    
    testTran.setCurrencyCode("EUR");
    testTran.setFxRateId(80990);
    testTran.setTranAmount(1540.00);
    testTran.setFxAmount(2000.00);
    
    ForeignExchangeRate fxRate = testTran.getFxRate();
    assertNotNull("fxRate should not be null",fxRate);
    double calcAmount = fxRate.convertAmount( testTran.getFxAmount() );
    double threshold = (testTran.getTranAmount() * thresholdRate);
    assertEquals("converted EUR amount mismatch (" + threshold + ")",testTran.getTranAmount(),calcAmount,threshold);
    assertTrue("EUR conversion failed failed",testTran.isFxAmountValid());
    
    // EUR using 81076
    testTran.setCurrencyCode("978");
    testTran.setFxRateId(81076);
    testTran.setTranAmount(32.76);
    testTran.setFxAmount(42.00);
    
    fxRate = testTran.getFxRate();
    assertNotNull("fxRate should not be null",fxRate);
    calcAmount = fxRate.convertAmount( testTran.getFxAmount() );
    threshold = (testTran.getTranAmount() * thresholdRate);
    assertEquals("converted 978 amount mismatch (" + threshold + ")",testTran.getTranAmount(),calcAmount,threshold);
    assertTrue("978 conversion failed failed",testTran.isFxAmountValid());
    
    // CAD using 80890
    testTran.setCurrencyCode("CAD");
    testTran.setFxRateId(80890);
    testTran.setTranAmount(339);
    testTran.setFxAmount(275);
    
    fxRate = testTran.getFxRate();
    assertNotNull("fxRate should not be null",fxRate);
    calcAmount = fxRate.convertAmount( testTran.getFxAmount() );
    threshold = (testTran.getTranAmount() * thresholdRate);
    assertEquals("converted CAD amount mismatch (" + threshold + ")",testTran.getTranAmount(),calcAmount,threshold);
    assertTrue("CAD conversion failed failed",testTran.isFxAmountValid());
    
    testTran.setCurrencyCode("826");
    testTran.setFxRateId(81163);
    testTran.setTranAmount(16.80);
    testTran.setFxAmount(28.00);
    
    fxRate = testTran.getFxRate();
    assertNotNull("fxRate should not be null",fxRate);
    calcAmount = fxRate.convertAmount( testTran.getFxAmount() );
    threshold = (testTran.getTranAmount() * thresholdRate);
    assertEquals("converted 826 amount mismatch (" + threshold + ")",testTran.getTranAmount(),calcAmount,threshold);
    assertTrue("826 conversion failed failed",testTran.isFxAmountValid());
    
    testTran.setCurrencyCode("124");
    testTran.setFxRateId(81148);
    testTran.setTranAmount(62.40);
    testTran.setFxAmount(52.80);
    
    fxRate = testTran.getFxRate();
    assertNotNull("fxRate should not be null",fxRate);
    calcAmount = fxRate.convertAmount( testTran.getFxAmount() );
    threshold = (testTran.getTranAmount() * thresholdRate);
    assertEquals("converted 124 amount mismatch (" + threshold + ")",testTran.getTranAmount(),calcAmount,threshold);
    assertTrue("124 conversion failed failed",testTran.isFxAmountValid());
  }

  public void test_isFxEnabled() 
  {
    fail("Failing due compilation error."); //FIXME: compilation error
//    TridentApiProfile         profile     = null;
//    TridentApiTransaction     testTran    = null;
//
//    profile = ApiDb.loadProfile(TestProfileId);
//    profile.setFxClientId(9999);
//    testTran = new TridentApiTransaction(MockTransactionFactory.generateTranId());
//    testTran.setProfile(profile);
//
//    assertTrue("FX should be enabled",testTran.isFxEnabled());
//    profile.setFxClientId(0);
//    assertFalse("FX should be disabled",testTran.isFxEnabled());
  }
  
  public void test_isValid_BatchClose() throws Exception
  {
    fail("Failing due compilation error."); //FIXME: compilation error
//    String                    apiTranId   = MockTransactionFactory.generateTranId();
//    TridentApiProfile         profile     = null;
//    MockHttpServletRequest    request     = new MockHttpServletRequest();
//    TridentApiTransaction     testTran    = null;
//
//    // build a mock servlet request to refund the transaction
//	  request.setParameter("profile_id",        TestProfileId);
//	  request.setParameter("profile_key",       "DZgZKrWxPKbcCCXSZEymdPCkjNhFwkaU");
//    request.setParameter("transaction_type",  TridentApiTranBase.TT_BATCH_CLOSE);
//
//    // create a profile and enable FX by setting the client id
//    profile = ApiDb.loadProfile(TestProfileId);
//
//    // create a new test object
//    testTran = new TridentApiTransaction(MockTransactionFactory.generateTranId());
//    testTran.setProperties(request);
//    testTran.setProfile(profile);
//    testTran.loadData();
//
//    assertFalse("tran validate should fail",testTran.isValid());
//    assertEquals("unexpected error code",TridentApiConstants.ER_BATCH_NUMBER_REQUIRED,testTran.getErrorCode());
//
//    // create a new test object
//    testTran = new TridentApiTransaction(MockTransactionFactory.generateTranId());
//    request.setParameter("batch_number",  "1001");
//    testTran.setProperties(request);
//    testTran.setProfile(profile);
//    testTran.loadData();
//
//    assertFalse("tran validate should fail",testTran.isValid());
//    assertEquals("unexpected error code",TridentApiConstants.ER_INVALID_BATCH_NUMBER,testTran.getErrorCode());
//
//    // create a new test object
//    testTran = new TridentApiTransaction(MockTransactionFactory.generateTranId());
//    request.setParameter("batch_number",  "901");
//    testTran.setProperties(request);
//    testTran.setProfile(profile);
//    testTran.loadData();
//
//    assertTrue("tran validate should pass",testTran.isValid());
//    assertEquals("unexpected error code",TridentApiConstants.ER_NONE,testTran.getErrorCode());
  }
  
  public void test_isValid_FxFullRefund() throws Exception
  {
    fail("Failing due compilation error."); //FIXME: compilation error
//    String                    apiTranId   = MockTransactionFactory.generateTranId();
//    TridentApiProfile         profile     = null;
//    MockHttpServletRequest    request     = new MockHttpServletRequest();
//    TridentApiTransaction     testTran    = null;
//
//    // create a dummy 10 EUR settled transaction in the trident_capture_api table
//    MockTransactionFactory.createSettledTransactionVisa(apiTranId,"978",true);
//
//    // mark the transaction as cleared
//    MockTransactionFactory.markTransactionCleared(apiTranId);
//
//    // build a mock servlet request to refund the transaction
//	  request.setParameter("profile_id",                TestProfileId);
//	  request.setParameter("profile_key",               "DZgZKrWxPKbcCCXSZEymdPCkjNhFwkaU");
//    request.setParameter("transaction_type",          "U");
//    request.setParameter("transaction_id",            apiTranId);
//
//    // create a profile and enable FX by setting the client id
//    profile = ApiDb.loadProfile(TestProfileId);
//    profile.setFxClientId(9999);
//
//    // create the test object
//    testTran = new TridentApiTransaction(MockTransactionFactory.generateTranId());
//    testTran.setProperties(request);
//    testTran.setProfile(profile);
//    testTran.loadData();
//
//    assertTrue("tran validate should pass",testTran.isValid());
//    assertEquals("unexpected error code",TridentApiConstants.ER_NONE,testTran.getErrorCode());
  }
  
  public void test_reqeustAuthorizationPayVision_MCSC() throws Exception
  {
    fail("Failing due compilation error."); //FIXME: compilation error
//    String                    apiTranId   = MockTransactionFactory.generateTranId();
//    String                    profileId   = "94100008615200000001";
//    MockHttpServletRequest    request     = new MockHttpServletRequest();
//    TridentApiTransaction     testTran    = null;
//
//    request.setParameter("profile_id",          profileId);
//    request.setParameter("profile_key",         "DZgZKrWxPKbcCCXSZEymdPCkjNhFwkaU");
//    request.setParameter("transaction_type",    "P");
//    request.setParameter("card_number",         "5434749000093028");
//    request.setParameter("card_exp_date",       "0610");
//    request.setParameter("transaction_amount",  "1");
//    request.setParameter("cvv2",                "897");
//    request.setParameter("invoice_number",      "12345");
//    request.setParameter("currency_code",       "EUR");
//    request.setParameter("ucaf_auth_data",      "AAABAYSBRBIwaENJkoFEEEfDMV8=");
//    request.setParameter("ucaf_collection_ind", "2");
//
//    testTran = new TridentApiTransaction(apiTranId);
//    testTran.setProperties(request);
//    testTran.setProfile(ApiDb.loadProfile(profileId));
//
//    assertTrue("pv auth request failed",testTran.requestAuthorizationPayVision());
//    assertEquals("java log entries found",0,getLogEntryCount());
  }
  
  public void test_requestAuthorizationPayVision() throws Exception
  {
    fail("Failing due compilation error."); //FIXME: compilation error
//    String                    apiTranId   = MockTransactionFactory.generateTranId();
//    MockHttpServletRequest    request     = new MockHttpServletRequest();
//    TridentApiTransaction     testTran    = null;
//
//	  request.setParameter("profile_id",                TestProfileId);
//	  request.setParameter("profile_key",               "DZgZKrWxPKbcCCXSZEymdPCkjNhFwkaU");
//    request.setParameter("merchant_name",             "JUNIT*TEST");
//    request.setParameter("transaction_type",          "P");
//    request.setParameter("card_number",               "5434749000093028");
//    request.setParameter("card_exp_date",             "0610");
//    request.setParameter("cvv2",                      "897");
//    request.setParameter("transaction_amount",        "3100");
//    request.setParameter("cardholder_street_address", "123 W 11th");
//    request.setParameter("cardholder_zip",            "97401");
//    request.setParameter("merchant_phone",            "8775551212");
//    request.setParameter("currency_code",             "GBP");
//    request.setParameter("moto_ecommerce_ind",        "6");
//    request.setParameter("invoice_number",            "123456");
//
//    testTran = new TridentApiTransaction(apiTranId);
//    testTran.setProperties(request);
//    testTran.setProfile(ApiDb.loadProfile(TestProfileId));
//
//    assertTrue("request validation failed",testTran.isValid());
//    assertEquals("invalid endpoint",TridentApiConstants.PAYVISION_ENDPOINT_TEST,testTran.getPayVisionEndpoint());
//    assertEquals("invalid member id",TridentApiConstants.PAYVISION_ECOMM_MEMBER_ID,testTran.getPayVisionMemberId());
//    assertEquals("invalid member guid",TridentApiConstants.PAYVISION_ECOMM_MEMBER_GUID,testTran.getPayVisionMemberGuid());
//    assertFalse("request auth",testTran.requestAuthorizationPayVision());
//
//    TransactionResult pvResp = testTran.getPayVisionResult();
//    assertFalse("payvision response is null", (pvResp == null));
//    assertEquals(3100, pvResp.getResult());
//    assertEquals(0,getLogEntryCount());
  }
  
  public void test_tranTypeRequiresFxAmount()
  {
    TridentApiProfile         profile     = null;
    TridentApiTransaction     testTran    = null;
    
    profile = ApiDb.loadProfile("94100008615200000001");
    profile.setFxClientId(9999);
    testTran = new TridentApiTransaction("no-tran-id");
    testTran.setProfile(profile);
    testTran.setCurrencyCode("EUR");
    
    String[] goodTranTypes = 
      {
        TridentApiTranBase.TT_FX_CONVERT_AMOUNT,
        TridentApiTranBase.TT_CREDIT,
        TridentApiTranBase.TT_DEBIT,
        TridentApiTranBase.TT_PRE_AUTH,
//@        TridentApiTranBase.TT_REFUND,
//@        TridentApiTranBase.TT_SETTLE,
      };
      
    String[] badTranTypes = 
      {
        TridentApiTranBase.TT_CARD_VERIFY,
        TridentApiTranBase.TT_BML_REQUEST,
        TridentApiTranBase.TT_3D_ENROLL_CHECK,
        TridentApiTranBase.TT_FX_GET_RATE,
        TridentApiTranBase.TT_CURRENCY_LOOKUP,
        TridentApiTranBase.TT_FORCE,
        TridentApiTranBase.TT_AUTH_REVERSAL,
        TridentApiTranBase.TT_STORE_CARD,
        TridentApiTranBase.TT_VOID,
        TridentApiTranBase.TT_DELETE_CARD_ID,
      };
    
    for ( int i = 0; i < goodTranTypes.length; ++i )
    {
      assertTrue("good tran type failed (" + goodTranTypes[i] + ")", 
                 testTran.tranTypeRequiresFxAmount(goodTranTypes[i]) );
    }
    
    for ( int i = 0; i < badTranTypes.length; ++i )
    {
      assertFalse("bad tran type passed (" + badTranTypes[i] + ")", 
                 testTran.tranTypeRequiresFxAmount(badTranTypes[i]) );
    }
    
    profile.setFxClientId(0);   // disable FX
    for( int j = 0; j < 2; ++j )
    {               
      String[] testTranTypes = ((j == 0) ? goodTranTypes : badTranTypes);
      
      for ( int i = 0; i < testTranTypes.length; ++i )
      {
        // convert amount always requires an FX amount
        if ( testTranTypes[i].equals(TridentApiTranBase.TT_FX_CONVERT_AMOUNT) )
        {
          assertTrue("convert amount tran type failed",
                     testTran.tranTypeRequiresFxAmount(TridentApiTranBase.TT_FX_CONVERT_AMOUNT) );
        }
        else    // all others should fail because FX is disabled
        {
          assertFalse("fx disabled - tran type passed (" + testTranTypes[i] + ")", 
                      testTran.tranTypeRequiresFxAmount(testTranTypes[i]) );
        }                      
      }
    }
  }
  
  public void test_tranTypeRequiresFxRateId()
  {
    TridentApiProfile         profile     = null;
    TridentApiTransaction     testTran    = null;
    
    profile = ApiDb.loadProfile("94100008615200000001");
    profile.setFxClientId(9999);
    testTran = new TridentApiTransaction("no-tran-id");
    testTran.setProfile(profile);
    testTran.setCurrencyCode("EUR");
    
    String[] goodTranTypes = 
      {
        TridentApiTranBase.TT_CREDIT,
        TridentApiTranBase.TT_DEBIT,
        TridentApiTranBase.TT_PRE_AUTH,
      };
      
    String[] badTranTypes = 
      {
        TridentApiTranBase.TT_FX_CONVERT_AMOUNT,
        TridentApiTranBase.TT_REFUND,
        TridentApiTranBase.TT_SETTLE,
        TridentApiTranBase.TT_CARD_VERIFY,
        TridentApiTranBase.TT_BML_REQUEST,
        TridentApiTranBase.TT_3D_ENROLL_CHECK,
        TridentApiTranBase.TT_FX_GET_RATE,
        TridentApiTranBase.TT_CURRENCY_LOOKUP,
        TridentApiTranBase.TT_FORCE,
        TridentApiTranBase.TT_AUTH_REVERSAL,
        TridentApiTranBase.TT_STORE_CARD,
        TridentApiTranBase.TT_VOID,
        TridentApiTranBase.TT_DELETE_CARD_ID,
      };
    
    for ( int i = 0; i < goodTranTypes.length; ++i )
    {
      assertTrue("good tran type failed (" + goodTranTypes[i] + ")", 
                 testTran.tranTypeRequiresFxRateId(goodTranTypes[i]) );
    }
    
    for ( int i = 0; i < badTranTypes.length; ++i )
    {
      assertFalse("bad tran type passed (" + badTranTypes[i] + ")", 
                 testTran.tranTypeRequiresFxRateId(badTranTypes[i]) );
    }
    
    profile.setFxClientId(0);   // disable FX
    for( int j = 0; j < 2; ++j )
    {               
      String[] testTranTypes = ((j == 0) ? goodTranTypes : badTranTypes);
      
      for ( int i = 0; i < testTranTypes.length; ++i )
      {
        // all types should fail because FX is disabled
        assertFalse("fx disabled - tran type passed (" + testTranTypes[i] + ")", 
                    testTran.tranTypeRequiresFxRateId(testTranTypes[i]) );
      }
    }
  }
}
