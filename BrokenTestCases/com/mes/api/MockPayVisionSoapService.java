/*************************************************************************

  FILE: $Archive: /src/test/com/mes/api/MockPayVisionSoapService.java $

  Description:  


  Last Modified By   : $Author:  $
  Last Modified Date : $Date:  $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.util.Calendar;
import java.math.BigDecimal;

import com.payvision.gateway.BasicOperationsSoap;
import com.payvision.gateway.TransactionResult;

public class MockPayVisionSoapService 
  implements com.payvision.gateway.BasicOperationsSoap
{
  public TransactionResult authorize(int memberId, String memberGuid, int countryId, BigDecimal amount, int currencyId, String trackingMemberCode, String cardNumber, String cardHolder, org.apache.axis.types.UnsignedByte cardExpiryMonth, short cardExpiryYear, String cardCvv, String cardType, String issueNumber, int merchantAccountType, String dynamicDescriptor, String avsAddress, String avsZip) 
    throws java.rmi.RemoteException
  {
    TransactionResult     retVal    = null;
    
    return( retVal );
  }
  
  public TransactionResult payment(int memberId, String memberGuid, int countryId, BigDecimal amount, int currencyId, String trackingMemberCode, String cardNumber, String cardHolder, org.apache.axis.types.UnsignedByte cardExpiryMonth, short cardExpiryYear, String cardCvv, String cardType, String issueNumber, int merchantAccountType, String dynamicDescriptor, String avsAddress, String avsZip) 
    throws java.rmi.RemoteException
  {
    TransactionResult     retVal    = null;
    
    return( retVal );
  }
  
  public TransactionResult credit(int memberId, String memberGuid, int countryId, BigDecimal amount, int currencyId, String trackingMemberCode, String cardNumber, String cardHolder, org.apache.axis.types.UnsignedByte cardExpiryMonth, short cardExpiryYear, String cardCvv, String cardType, String issueNumber, int merchantAccountType, String dynamicDescriptor, String avsAddress, String avsZip) 
    throws java.rmi.RemoteException
  {
    TransactionResult     retVal    = null;
    
    return( retVal );
  }
  
  public TransactionResult refund(int memberId, String memberGuid, int transactionId, String transactionGuid, BigDecimal amount, int currencyId, String trackingMemberCode) 
    throws java.rmi.RemoteException
  {
    TransactionResult     retVal    = null;
    
    retVal = new TransactionResult( 0, "JUnit Success", trackingMemberCode,
                                    transactionId, transactionGuid,
                                    Calendar.getInstance(),
                                    null, new Boolean(false) );

    return( retVal );
  }
  
  public TransactionResult capture(int memberId, String memberGuid, int transactionId, String transactionGuid, BigDecimal amount, int currencyId, String trackingMemberCode) 
    throws java.rmi.RemoteException
  {
    TransactionResult     retVal    = null;
    
    return( retVal );
  }
  
  public TransactionResult _void(int memberId, String memberGuid, int transactionId, String transactionGuid, String trackingMemberCode) 
    throws java.rmi.RemoteException
  {
    TransactionResult     retVal    = null;
    
    return( retVal );
  }
  
  public TransactionResult referralApproval(int memberId, String memberGuid, int transactionId, String transactionGuid, BigDecimal amount, int currencyId, String trackingMemberCode, String approvalCode) 
    throws java.rmi.RemoteException
  {
    TransactionResult     retVal    = null;
    
    return( retVal );
  }
  
  public TransactionResult cardFundTransfer(int memberId, String memberGuid, int countryId, BigDecimal amount, int currencyId, String trackingMemberCode, String cardNumber, String cardHolder, org.apache.axis.types.UnsignedByte cardExpiryMonth, short cardExpiryYear, String cardCvv, String cardType, String issueNumber, int merchantAccountType, String dynamicDescriptor, String avsAddress, String avsZip) 
    throws java.rmi.RemoteException
  {
    TransactionResult     retVal    = null;
    
    return( retVal );
  }
  
  public TransactionResult retrieveTransactionResult(int memberId, String memberGuid, String trackingMemberCode, java.util.Calendar transactionDate) 
    throws java.rmi.RemoteException
  {
    TransactionResult     retVal    = null;
    
    return( retVal );
  }
}


  