/*************************************************************************

  FILE: $Archive: /src/test/com/mes/api/TestRefundTransactionTask.sqlj $

  Description:  


  Last Modified By   : $Author:  $
  Last Modified Date : $Date:  $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2008 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.util.StringTokenizer;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URLDecoder;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.mes.settlement.MockTransactionFactory;
import com.mes.test.MesTestCase;
import com.mes.test.MockHttpServletRequest;
import com.mes.api.TridentApiConstants;
import com.mes.support.MesEncryption;
import junit.framework.*;

import org.safehaus.uuid.UUIDGenerator;
import org.safehaus.uuid.UUID;

public class TestRefundTransactionTask extends MesTestCase
{
  private     int               IDX_VISA      = 0;
  private     int               IDX_AMEX      = 1;
  private     int               IDX_DISC      = 2;
  
  private String    LoadFilenames[][] =
  {
    { "256m3941_092908_901.dat",      "4644633" },
    { "tapi_amex3941_092908_901.dat", "4644631" },
    { "tapi_disc3941_092908_901.dat", "4644632" },
  };
  
  private String    CardNumbers[][] = 
  {
    { "VS","426236xxxxxx6102","00000205530000010000001041FEE6AB4804EF3136EC0BF91809D6BB" },
    { "AM","341111xxxxxx2000","00000239830000010000000F0884B70BD54244E0462FB9F6C8086610" },
    { "DS","601100xxxxxx6639","00000239830000010000001011FBD5C8FF354C9FB7365E6B0060A2A6" },
  };

  private String    TridentTranIds[] = 
    {
      "9d4d541625853d1a929ad61f111a2906",
      "b1697308b9283e63a8266eac10e20582",
      "e79e088fcdf7373bb4abd1e2ad5c665f"
    };
    
  private String    InsertStatement = 
    " insert into trident_capture_api " +
    " (                               " +
    "   terminal_id,                  " +
    "   merchant_number,              " +
    "   bank_number,                  " +
    "   dba_name,                     " +
    "   dba_city,                     " +
    "   dba_state,                    " +
    "   dba_zip,                      " +
    "   sic_code,                     " +
    "   country_code,                 " +
    "   phone_number,                 " +
    "   transaction_date,             " +
    "   batch_date,                   " +
    "   batch_number,                 " +
    "   debit_credit_indicator,       " +
    "   transaction_amount,           " +
    "   auth_amount,                  " +
    "   auth_code,                    " +
    "   auth_source_code,             " +
    "   auth_response_code,           " +
    "   auth_date,                    " +
    "   auth_returned_aci,            " +
    "   auth_avs_response,            " +
    "   auth_ref_num,                 " +
    "   auth_tran_id,                 " +
    "   auth_val_code,                " +
    "   purchase_id,                  " +
    "   tax_amount,                   " +
    "   pos_entry_mode,               " +
    "   moto_ecommerce_ind,           " +
    "   recurring_payment_number,     " +
    "   recurring_payment_count,      " +
    "   test_flag,                    " +
    "   card_present,                 " +
    "   currency_code,                " +
    "   emulation,                    " +
    "   avs_zip,                      " +
    "   exp_date,                     " +
    "   server_name,                  " +
    "   transaction_type,             " +
    "   card_type,                    " +
    "   card_number,                  " +
    "   card_number_enc,              " +
    "   trident_tran_id               " +
    " )                               " +
    " values                          " +
    " (                               " +
    "   '94100008615200000001',       " +
    "   941000086152,                 " +
    "   3941,                         " +
    "   'JUNIT*TEST',                 " +
    "   'REDWOOD CITY',               " +
    "   'CA',                         " +
    "   '94065',                      " +
    "   5999,                         " +
    "   'US',                         " +
    "   '650-6286831',                " +
    "   trunc(sysdate),               " +
    "   trunc(sysdate),               " +
    "   901,                          " +
    "   'D',                          " +
    "   10.00,                        " +
    "   10.00,                        " +
    "   'T7748H',                     " +
    "   '7',                          " +
    "   '00',                         " +
    "   trunc(sysdate),               " +
    "   'V',                          " +
    "   'N',                          " +
    "   827023099256,                 " +
    "   'T81404329696484',            " +
    "   'T251',                       " +
    "   '123456',                     " +
    "   0.20,                         " +
    "   '01',                         " +
    "   '7',                          " +
    "   0,                            " +
    "   0,                            " +
    "   'Y',                          " +
    "   'N',                          " +
    "   '840',                        " +
    "   'TPG',                        " +
    "   '99212',                      " +
    "   '0415',                       " +
    "   'junit-test',                 " +
    "   'D',                          " +
    "   ?,                            " +
    "   ?,                            " +
    "   ?,                            " +
    "   ?                             " +
    " )                               ";
    
  private String[]  UpdateStatements  =
  {
    "update trident_capture_api set load_filename = ?, load_file_id = ? where trident_tran_id = ?",
    "update trident_capture_api set amex_load_filename = ?, amex_load_file_id = ? where trident_tran_id = ?",
    "update trident_capture_api set discover_load_filename = ?, discover_load_file_id = ? where trident_tran_id = ?",
  };    
  
  private String AuthReversalDeleteStatement =
    "delete from java_log where log_time between ? and sysdate and server_name = ? and log_source like 'com.mes.api.RefundTransactionTask::doTask(reversal-%'";
  
  // create test transaction entries for test_doTask(..)  
  private void doTaskEntriesCreate() 
    throws Exception
  {
    // insert test entries
    PreparedStatement s = DbConnection.getPreparedStatement(InsertStatement);
    for ( int i = 0; i < TridentTranIds.length; ++i )
    {
      s.setString(1, CardNumbers[i][0]);  // card type
      s.setString(2, CardNumbers[i][1]);  // card number
      s.setString(3, CardNumbers[i][2]);  // card number enc
      s.setString(4, TridentTranIds[i]);  // trident tran id
      s.executeUpdate();
    }
  }
  
  // remove test transaction entries for test_doTask(..)  
  private void doTaskEntriesDelete()
  {
    PreparedStatement s = null;
  
    try
    {
      for ( int qtype = 0; qtype < 2; ++qtype )
      {
        switch( qtype )
        {
          case 0:   // remove the sales
            s = DbConnection.getPreparedStatement("delete from trident_capture_api where trident_tran_id = ?");
            break;
          
          case 1:   // remove the refund credits
            s = DbConnection.getPreparedStatement("delete from trident_capture_api where original_trident_tran_id = ?");
            break;
        }
      
        for ( int i = 0; i < TridentTranIds.length; ++i )
        {
          s.setString(1, TridentTranIds[i]);
          s.executeUpdate();
        }
        s.close();
      }      
    }
    catch( Exception e )
    {
      DbConnection.logEntry("doTaskEntriesDelete()", e.toString());
    }
    finally
    {
      try{ s.close(); } catch( Exception ee ) {}
    }      
  }
    
  private String generateTranId()
  {
    StringBuffer      hostName    = new StringBuffer();
    UUID              uuid        = null;
    
    // build the unique tran id
    hostName.append(ServerName);
    hostName.append(System.currentTimeMillis());
    hostName.append(String.valueOf(this));
    
    // generate a unique hash of the unique id
    uuid = UUIDGenerator.getInstance().generateNameBasedUUID(null, hostName.toString() );
    
    // return the value without the separators
    return( uuid.toString().replaceAll("-", "") );
  }    
    
  private int getCreditCount( String tranId )
  {
    PreparedStatement   ps        = null;
    int                 recCount  = 0;
    ResultSet           resultSet = null;
    
    try
    {
      ps = DbConnection.getPreparedStatement("select count(1) as credit_count from trident_capture_api where original_trident_tran_id = ? and transaction_type = 'C' and transaction_amount = 2.50");
      ps.setString(1,tranId);
      resultSet = ps.executeQuery();
      resultSet.next();
      recCount = resultSet.getInt("credit_count");
      resultSet.close();
    }
    catch( Exception e )
    {
      DbConnection.logEntry("getCreditCount(" + tranId + ")", e.toString());
    }
    finally
    {
      try{ ps.close(); } catch( Exception ee ) {}
    }
    return( recCount );
  }      
  
  public void tearDown() throws Exception
  {
    fail("Failing due compilation error."); //FIXME: compilation error
//    MockTransactionFactory.removeAllTransactions();
    super.tearDown();
  }
  
  public void test_doTask_FxFullRefund() throws Exception
  {
    fail("Failing due compilation error."); //FIXME: compilation error
//    String                    apiTranId   = MockTransactionFactory.generateTranId();
//    TridentApiProfile         profile     = null;
//    MockHttpServletRequest    request     = new MockHttpServletRequest();
//    boolean                   retVal      = false;
//    RefundTransactionTask     testClass   = null;
//    TridentApiTransaction     testTran    = null;
//
//    // create a settled FX transaction that has been cleared
//    MockTransactionFactory.createSettledTransactionVisa(apiTranId,"978",true);
//    MockTransactionFactory.markTransactionCleared(apiTranId);
//
//    // build a mock servlet request to refund the transaction
//	  request.setParameter("profile_id",                "94100008615200000001");
//	  request.setParameter("profile_key",               "DZgZKrWxPKbcCCXSZEymdPCkjNhFwkaU");
//    request.setParameter("transaction_type",          "U");
//    request.setParameter("transaction_id",            apiTranId);
//
//    // create a profile and enable FX by setting the client id
//    profile = ApiDb.loadProfile("94100008615200000001");
//    profile.setFxClientId(1027);
//
//    // create the test transaction
//    testTran = new TridentApiTransaction(MockTransactionFactory.generateTranId());
//    testTran.setProperties(request);
//    testTran.setProfile(profile);
//    testTran.loadData();
//
//    // create the test object
//    testClass = new RefundTransactionTask(testTran);
//    testClass.setPayVisionService( new MockPayVisionSoapService() );
//    retVal = testClass.doTask();
//
//    // payvision refund should fail because the transaction data is dummy
//    assertTrue("FX refund should have passed " + testClass.getErrorCode() + " - " + testClass.getErrorDesc(),retVal);
  }
  
//@  public void test_doTask_FxPartialRefund() throws Exception
//@  {
//@  }
  
  public void test_doTask() throws Exception
  {
    PreparedStatement       ps        = null;
    int                     recCount  = 0;
    fail("Failing due compilation error");  //FIXME: compilation error
    MockHttpServletRequest  request   = new MockHttpServletRequest();
    ResultSet               resultSet = null;
    TridentApiTransaction   testTran  = new TridentApiTransaction(TridentTranIds[IDX_AMEX]);
    RefundTransactionTask   testClass = null;
    
    try
    {
      doTaskEntriesCreate();    // create the test transactions
    
      request.setParameter("profile_id",                "94100008615200000001");
      request.setParameter("profile_key",               "DZgZKrWxPKbcCCXSZEymdPCkjNhFwkaU");
      request.setParameter("transaction_type",          "U");
      request.setParameter("transaction_id",            TridentTranIds[IDX_VISA]);
      request.setParameter("transaction_amount",        "12.50");    
    
      testTran.setProperties(request);
      testTran.setProfile(ApiDb.loadProfile("94100008615200000001"));
      testTran.setCardNumberFull(MesEncryption.getClient().decrypt(CardNumbers[IDX_VISA][2]));
      testClass = new RefundTransactionTask(testTran);
    
      // attempt to refund $12.50 against the $10.00 original
      assertFalse("invalid amount",testClass.doTask());
      assertEquals("invalid amount error code",TridentApiConstants.ER_INVALID_REFUND_AMOUNT,testClass.getErrorCode());
    
      // attempt to refund an invalid tran id
      testTran.setTridentTranId("invalid-tran-id");
      assertFalse("invalid-tran-id passed",testClass.doTask());
      assertEquals("invalid-tran-id error code",TridentApiConstants.ER_INVALID_TRAN_ID,testClass.getErrorCode());
    
      // refund $2.50 on the unsettled visa transaction
      testTran.setTranAmount(2.5);
      testTran.setTridentTranId(TridentTranIds[IDX_VISA]);
      assertTrue("unsettled visa refund failed",testClass.doTask());
    
      // refund $2.50 on the unsettled amex transaction
      testTran.setTridentTranId(TridentTranIds[IDX_AMEX]);
      assertTrue("unsettled amex refund failed",testClass.doTask());
    
      // refund $2.50 on the unsettled Discover transaction
      testTran.setTridentTranId(TridentTranIds[IDX_DISC]);
      assertTrue("unsettled discover refund failed",testClass.doTask());
    
      // update the load filenames to indicate the transaction is settled
      for ( int i = 0; i < UpdateStatements.length; ++i )
      {
        ps = DbConnection.getPreparedStatement(UpdateStatements[i]);
        ps.setString(1,LoadFilenames[i][0]);
        ps.setString(2,LoadFilenames[i][1]);
        ps.setString(3,TridentTranIds[i]);
        ps.executeUpdate();
      }   
    
      // full refund on the settled partial capture visa transaction
      testTran.setTridentTranId(TridentTranIds[IDX_VISA]);
      testTran.setTranAmount(10.00);
      assertFalse("visa refund invalid amount",testClass.doTask());
      assertEquals("visa refund invalid amount error code",TridentApiConstants.ER_INVALID_REFUND_AMOUNT,testClass.getErrorCode());
    
      // partial refund on settled visa transaction
      testTran.setTranAmount(2.50);
      testTran.setApiTranId(generateTranId());
      assertTrue("visa refund credit failed",testClass.doTask());
    
      // verify that a visa credit has been issued
      assertEquals("missing visa credit",1,getCreditCount(TridentTranIds[IDX_VISA]));
    
      // refund $2.50 on the settled amex transaction
      testTran.setTridentTranId(TridentTranIds[IDX_AMEX]);
      testTran.setTranAmount(2.50);
      testTran.setApiTranId(generateTranId());
      assertTrue("amex refund credit failed",testClass.doTask());
    
      // verify that an amex credit has been issued
      assertEquals("missing amex credit",1,getCreditCount(TridentTranIds[IDX_AMEX]));
    
      // refund $2.50 on the settled Discover transaction
      testTran.setTridentTranId(TridentTranIds[IDX_DISC]);
      testTran.setTranAmount(2.50);
      testTran.setApiTranId(generateTranId());
      assertTrue("discover refund credit failed",testClass.doTask());
    
      // verify that a discover credit has been issued
      assertEquals("missing discover credit",1,getCreditCount(TridentTranIds[IDX_DISC]));
    
      // clear any java log entries related to the auth-reversal failing
      // the reversal will fail on all of these because the transaction
      // is a dummy and does not have a full auth response from Trident
      ps = DbConnection.getPreparedStatement(AuthReversalDeleteStatement);
      ps.setTimestamp(1,LogBeginTS);
      ps.setString(2,ServerName);
      ps.executeUpdate();
    
      // check for log entries
      assertEquals("java log entries",0,getLogEntryCount());
    }
    finally
    {      
      doTaskEntriesDelete();    // delete the test transactions
    }      
  }
  
  public void test_getRefundAction()
  {
    String                  actions[] = {"Credit","Refund","Void"};
    RefundTransactionTask   testClass = new RefundTransactionTask(null);

    for( int i = 0; i < actions.length; ++i )
    {
      testClass.RefundAction = actions[i];
      assertEquals("invalid refund action " + actions[i],actions[i],testClass.getRefundAction());    
    }
  }
  
  public void test_isTransactionRefundAvailable() 
    throws Exception
  {
    fail("Failing due compilation error"); //FIXME: compilation error
    MockHttpServletRequest  request   = new MockHttpServletRequest();
    ResultSet               resultSet = null;
    PreparedStatement       s         = null;
    TridentApiTransaction   testTran  = new TridentApiTransaction(generateTranId());
    RefundTransactionTask   testClass = null;
  
    try
    {
      // create a test transaction
      s = DbConnection.getPreparedStatement(InsertStatement);
      s.setString(1, CardNumbers[IDX_VISA][0]);  // card type
      s.setString(2, CardNumbers[IDX_VISA][1]);  // card number
      s.setString(3, CardNumbers[IDX_VISA][2]);  // card number enc
      s.setString(4, TridentTranIds[IDX_VISA]);  // trident tran id
      s.executeUpdate();
    
      request.setParameter("profile_id",          "94100008615200000001");
      request.setParameter("profile_key",         "DZgZKrWxPKbcCCXSZEymdPCkjNhFwkaU");
      request.setParameter("transaction_type",    "U");
      request.setParameter("transaction_id",      TridentTranIds[IDX_VISA]);
      request.setParameter("transaction_amount",  "2.50");    
    
      testTran.setProperties(request);
      testClass = new RefundTransactionTask(testTran);
    
      testTran.setOriginalTranAmount(10.00);
      assertTrue("refund not available",testClass.isTransactionRefundAvailable());
    
      testTran.setTranAmount(12.50);    // over $10 original amount
      assertFalse("refund allowed over original amount",testClass.isTransactionRefundAvailable());
    
      // mark as settled 
      s = DbConnection.getPreparedStatement("update trident_capture_api set transaction_type = 'V', load_filename = 'void' where trident_tran_id = ?");
      s.setString(1, TridentTranIds[IDX_VISA]);
      s.executeUpdate();
    
      assertFalse("refund allowed on void",testClass.isTransactionRefundAvailable());
    }
    finally
    {      
      // remove the test transaction
      s = DbConnection.getPreparedStatement("delete from trident_capture_api where trident_tran_id = ?");
      s.setString(1, TridentTranIds[IDX_VISA]);  // trident tran id
      s.executeUpdate();
    }      
  }
}