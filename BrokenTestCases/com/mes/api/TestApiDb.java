/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/test/com/mes/api/TestApiDb.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $LastChangedDate: 2009-04-23 08:36:57 -0700 (Thu, 23 Apr 2009) $
  Version            : $Revision: 16022 $

  Change History:
     See VSS database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.util.Calendar;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.PreparedStatement;

import com.mes.test.MesTestCase;
import junit.framework.*;

public class TestApiDb extends MesTestCase
{
  private   java.sql.Date   TestBatchDate   = null;
  private   int             TestBatchNumber = 901;
  private   String          TestProfileId   = "94100008615200000001";
  private   String          TestTranId      = null;
  
  
  protected void setUp() throws Exception
  {
    super.setUp();

    fail("Failing due compilation error."); //FIXME: compilation error
//    TestTranId = MockTransactionFactory.generateTranId();
//    MockTransactionFactory.createSettledTransactionVisa( TestTranId );
    
    // create a batch date object with all time elements zeroed
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR,0);
    cal.set(Calendar.MINUTE,0);
    cal.set(Calendar.SECOND,0);
    cal.set(Calendar.MILLISECOND,0);
    cal.clear(Calendar.AM_PM);
    TestBatchDate = new java.sql.Date(cal.getTime().getTime());
  }

  public void tearDown() throws Exception
  {
    fail("Failing due compilation error."); //FIXME: compilation error
//    // remove test transaction
//    MockTransactionFactory.removeAllTransactions();
    super.tearDown();
  }
      
  public void test_closeBatch() throws Exception
  {
    // update the dummy tranaction to allow it to be part of a closed batch
    String qs = 
      " update trident_capture_api  " +
      " set test_flag     = 'N',    " +
      "     batch_date    = null,   " +
      "     batch_number  = null    " +
      " where trident_tran_id = ?   ";
      
    PreparedStatement ps = DbConnection.getPreparedStatement(qs);
    ps.setString(1,TestTranId);
    ps.executeUpdate();

    fail("Failing due compilation error."); //FIXME: compilation error
    // close the batch
//    ApiDb.closeBatch(TestProfileId,TestBatchDate,TestBatchNumber);
    
    // verify that the batch has been closed
    qs = 
      " select  batch_date, batch_number, batch_id,   " +
      "         nvl(ach_sequence,-1) as ach_sequence  " + 
      " from    trident_capture_api       " +
      " where   trident_tran_id = ?       ";
    ps = DbConnection.getPreparedStatement(qs);
    ps.setString(1,TestTranId);
    ResultSet rs = ps.executeQuery();
    assertTrue("missing tran",rs.next());
    assertEquals("batch number",TestBatchNumber,rs.getInt("batch_number"));
    assertEquals("batch date",TestBatchDate,rs.getDate("batch_date"));
    assertTrue("batch id",(0L != rs.getLong("batch_id")));
    assertEquals("ach sequence",0L, rs.getLong("ach_sequence"));
    rs.close();
    ps.close();
    
    // verify that the profile has been updated
    qs = 
      " select  last_batch_date, last_batch_number  " + 
      " from    trident_profile_api                 " +
      " where   terminal_id = ?                     ";
    ps = DbConnection.getPreparedStatement(qs);
    ps.setString(1,TestProfileId);
    rs = ps.executeQuery();
    assertTrue("missing profile",rs.next());
    assertEquals("last batch number",TestBatchNumber,rs.getInt("last_batch_number"));
    assertEquals("last batch date",TestBatchDate,rs.getDate("last_batch_date"));
    rs.close();
    ps.close();
    
    // check for java log entries
    assertEquals(0,getLogEntryCount());
  }
  
  public void test_extractAchTridentStatementRecord() throws Exception
  {
    // update the dummy tranaction to allow it to be part of a closed batch
    String qs = 
      " update trident_capture_api  " +
      " set test_flag     = 'N',    " +
      "     batch_date    = ?,      " +
      "     batch_number  = ?,      " +
      "     batch_id      = 212     " +
      " where trident_tran_id = ?   ";
      
    PreparedStatement ps = DbConnection.getPreparedStatement(qs);
    ps.setDate(1,TestBatchDate);
    ps.setInt(2,TestBatchNumber);
    ps.setString(3,TestTranId);
    ps.executeUpdate();
    ps.close();
    
    ApiDb.extractAchTridentStatementRecord(TestProfileId,TestBatchDate,TestBatchNumber,true);
    
    qs =
      " select  trunc(sysdate + mf.suspended_days)  as post_date  " +
      " from    mif mf                                            " +
      " where   merchant_number = 941000086152                    ";
      
    ps = DbConnection.getPreparedStatement(qs);
    ResultSet rs = ps.executeQuery();
    rs.next();
    java.sql.Date postDate = rs.getDate("post_date");
    rs.close();
    ps.close();
    
    qs =
      " select  ach.ach_amount,                           " +
      "         ach.post_date,                            " +
      "         ach.post_date_actual,                     " +
      "         ach.transaction_code,                     " +
      "         ach.entry_description,                    " +
      "         ach.sales_count,                          " +
      "         ach.sales_amount,                         " +
      "         ach.credits_count,                        " +
      "         ach.credits_amount,                       " +
      "         ach.rec_id                  as batch_id   " +
      " from    ach_trident_statement_test  ach           " +
      " where   ach.rec_id = 212                          ";
      
    ps = DbConnection.getPreparedStatement(qs);
    rs = ps.executeQuery();
    assertTrue("missing record",rs.next());
    assertEquals("transaction code",22,rs.getInt("transaction_code"));
    assertEquals("post date",postDate,rs.getDate("post_date"));
    assertEquals("ach amount",10.00,rs.getDouble("ach_amount"),0.001);
    rs.close();
    ps.close();
    
    ps = DbConnection.getPreparedStatement("delete from ach_trident_statement_test where rec_id = 212" );
    ps.executeUpdate();
    ps.close();
  }
}