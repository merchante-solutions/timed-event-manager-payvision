/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/test/com/mes/api/TestBatchCloseTask.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $LastChangedDate: 2009-04-23 08:36:57 -0700 (Thu, 23 Apr 2009) $
  Version            : $Revision: 16022 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.util.Calendar;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.PreparedStatement;

import com.mes.ach.AchEntryData;
import com.mes.test.MesTestCase;
import com.mes.test.MockHttpServletRequest;

import junit.framework.*;

public class TestBatchCloseTask extends MesTestCase
{
  private   String          TestProfileId   = "94100008615200000001";
  private   Date            TestBatchDate   = null;
  private   long            TestBatchId     = 0L;
  private   String          TestTranId      = null;
  
  
  protected void setUp() throws Exception
  {
    super.setUp();

    fail("Failing due compilation error."); //FIXME: compilation error
//    TestTranId = MockTransactionFactory.generateTranId();
//    MockTransactionFactory.createSettledTransactionVisa( TestTranId );
    
    // create a batch date object with all time elements zeroed
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR,0);
    cal.set(Calendar.MINUTE,0);
    cal.set(Calendar.SECOND,0);
    cal.set(Calendar.MILLISECOND,0);
    cal.clear(Calendar.AM_PM);
    TestBatchDate = new java.sql.Date(cal.getTime().getTime());
  }

  public void tearDown() throws Exception
  {
    // remove test transaction
    PreparedStatement ps = DbConnection.getPreparedStatement("delete from trident_capture_api where trident_tran_id = ? and server_name = 'junit-test'");
    ps.setString(1,TestTranId);
    ps.executeUpdate();
    ps.close();
    
    // remove any test ACH entries
    if ( TestBatchId != 0L )
    {
      ps = DbConnection.getPreparedStatement("delete from ach_trident_statement_test where rec_id = ?");
      ps.setLong(1,TestBatchId);
      ps.executeUpdate();
      ps.close();
      TestBatchId = 0L;
    }
    
    super.tearDown();
  }
      
  public void test_doTask() throws Exception
  {
    // update the dummy tranaction to allow it to be part of a closed batch
    String qs = 
      " update trident_capture_api  " +
      " set test_flag     = 'N',    " +
      "     batch_date    = null,   " +
      "     batch_number  = null    " +
      " where trident_tran_id = ?   ";
      
    PreparedStatement ps = DbConnection.getPreparedStatement(qs);
    ps.setString(1,TestTranId);
    ps.executeUpdate();

    fail("Failing due compilation error");
    MockHttpServletRequest  request   = new MockHttpServletRequest();
    
    request.setParameter("profile_id",      TestProfileId);
    request.setParameter("profile_key",     "DZgZKrWxPKbcCCXSZEymdPCkjNhFwkaU");
    request.setParameter("transaction_type",TridentApiTranBase.TT_BATCH_CLOSE);
    request.setParameter("batch_number",    "123");

    fail("Failing due compilation error."); //FIXME: compilation error
//    TridentApiTransaction tran = new TridentApiTransaction(MockTransactionFactory.generateTranId());
//    tran.setProperties(request);
//    tran.setProfile(ApiDb.loadProfile(TestProfileId));
//    BatchCloseTask task = new BatchCloseTask(tran);
//
//    // close the batch using batch close task
//    task.doTask();
//
//    assertEquals("task error",TridentApiConstants.ER_NONE,task.getErrorCode());
//
//    // verify that the batch has been closed
//    qs =
//      " select  batch_date, batch_number, batch_id " +
//      " from    trident_capture_api       " +
//      " where   trident_tran_id = ?       ";
//    ps = DbConnection.getPreparedStatement(qs);
//    ps.setString(1,TestTranId);
//    ResultSet rs = ps.executeQuery();
//    assertTrue("missing tran",rs.next());
//    assertTrue("batch number",(rs.getInt("batch_number") > 0));
//    assertEquals("batch date",TestBatchDate,rs.getDate("batch_date"));
//    assertTrue("batch id",(0L != rs.getLong("batch_id")));
//    rs.close();
//    ps.close();
//
//    // validate the batch data
//    assertEquals("batch close info",
//                 ("Batch " + task.getBatchNumber() + " Closed - " +
//                  "Sales: 1 for $10.00 Credits: 0 for $0.00 Net: $10.00"),
//                  task.getBatchCloseInfo());
//
//    // check for java log entries
//    assertEquals("java log",0,getLogEntryCount());
  }
  
  public void test_doTask_MissingBatchNumber() throws Exception
  {
    fail("Failing due compilation error."); //FIXME: compilation error
//    MockTransactionFactory.clearBatchCloseData(TestTranId);
//
//    MockHttpServletRequest request = new MockHttpServletRequest();
//
//    request.setParameter("profile_id",      TestProfileId);
//    request.setParameter("profile_key",     "DZgZKrWxPKbcCCXSZEymdPCkjNhFwkaU");
//    request.setParameter("transaction_type",TridentApiTranBase.TT_BATCH_CLOSE);
//
//    TridentApiTransaction tran = new TridentApiTransaction(MockTransactionFactory.generateTranId());
//    tran.setProperties(request);
//    tran.setProfile(ApiDb.loadProfile(TestProfileId));
//    BatchCloseTask task = new BatchCloseTask(tran);
//
//    // close the batch using batch close task
//    assertFalse("bad batch should fail",task.doTask());
//    assertEquals("task error code",TridentApiConstants.ER_INVALID_BATCH_NUMBER,task.getErrorCode());
//
//    // check for java log entries
//    assertEquals("java log",0,getLogEntryCount());
  }
  
  public void test_doTask_QD() throws Exception
  {
    // update the dummy tranaction to allow it to be part of a closed batch
    String qs = 
      " update trident_capture_api  " +
      " set test_flag     = 'N',    " +
      "     batch_date    = trunc(sysdate), " +
      "     batch_number  = 901     " +
      " where trident_tran_id = ?   ";
      
    PreparedStatement ps = DbConnection.getPreparedStatement(qs);
    ps.setString(1,TestTranId);
    ps.executeUpdate();

    fail("Failing due compilation error");
    MockHttpServletRequest  request   = new MockHttpServletRequest();
    
    request.setParameter("profile_id",      TestProfileId);
    request.setParameter("profile_key",     "DZgZKrWxPKbcCCXSZEymdPCkjNhFwkaU");
    request.setParameter("transaction_type",TridentApiTranBase.TT_BATCH_CLOSE);
    request.setParameter("batch_number",    "901");

    fail("Failing due compilation error."); //FIXME: compilation error
//    TridentApiTransaction tran = new TridentApiTransaction(MockTransactionFactory.generateTranId());
//    tran.setProperties(request);
//    tran.setProfile(ApiDb.loadProfile(TestProfileId));
//    BatchCloseTask task = new BatchCloseTask(tran);
//
//    // close the batch using batch close task
//    assertFalse("qd should fail",task.doTask());
//    assertEquals("task error code",TridentApiConstants.ER_DUPLICATE_BATCH,task.getErrorCode());
//
//    // check for java log entries
//    assertEquals("java log",0,getLogEntryCount());
  }
  
  public void test_doTask_withAch() throws Exception
  {
    String profileId = "39420000005100000001";    // sterling test acct
    
    // update the dummy tranaction to allow it to be part of a closed batch
    String qs = 
      " update trident_capture_api    " +
      " set test_flag         = 'N',  " +
      "     batch_date        = null, " +
      "     batch_number      = null, " +
      "     terminal_id       = '39420000005100000001', " +
      "     merchant_number   = 394200000051 " +
      " where trident_tran_id = ?     ";
      
    PreparedStatement ps = DbConnection.getPreparedStatement(qs);
    ps.setString(1,TestTranId);
    ps.executeUpdate();

    fail("Failing due compilation error");
    MockHttpServletRequest  request   = new MockHttpServletRequest();
    
    request.setParameter("profile_id",      profileId);
    request.setParameter("profile_key",     "xkIsxLiTvXpqaxNDWpLzBOIoxDwfihkc");
    request.setParameter("transaction_type",TridentApiTranBase.TT_BATCH_CLOSE);
    request.setParameter("batch_number",    "123");

    //FIXME: compilation error
//    TridentApiTransaction tran = new TridentApiTransaction(MockTransactionFactory.generateTranId());
//    tran.setProperties(request);
//    tran.setProfile(ApiDb.loadProfile(tran.getProfileId()));
//    tran.loadData();   // load any required data from Oracle
//    BatchCloseTask task = new BatchCloseTask(tran);
//
//    task.setTestMode(true); // set test flag to cause ACH to go into test table
//    task.doTask();          // close the batch using batch close task
    
    // verify that the batch has been closed
    qs = 
      " select  batch_date, batch_number, batch_id,   " + 
      "         nvl(ach_sequence,-1) as ach_sequence  " + 
      " from    trident_capture_api       " +
      " where   trident_tran_id = ?       ";
    ps = DbConnection.getPreparedStatement(qs);
    ps.setString(1,TestTranId);
    ResultSet rs = ps.executeQuery();
    assertTrue("missing tran",rs.next());
    assertTrue("batch number",(rs.getInt("batch_number") > 0));
    assertEquals("batch date",TestBatchDate,rs.getDate("batch_date"));
    assertEquals("ach sequence",0L,rs.getLong("ach_sequence"));
    TestBatchId = rs.getLong("batch_id");
    assertTrue("batch id",(0L != TestBatchId));
    rs.close();
    ps.close();
    
    // verify the ACH extraction on batch close is disabled
    // ACH extraction has been moved to TridentBatchACH event.
    qs = 
      " select  ach_amount, transaction_code, entry_description " + 
      " from    ach_trident_statement_test    " +
      " where   rec_id = ?       ";
    ps = DbConnection.getPreparedStatement(qs);
    ps.setLong(1,TestBatchId);
    rs = ps.executeQuery();
    assertFalse("s/b no ach",rs.next());
    rs.close();
    ps.close();
    
    // check for java log entries
    assertEquals("java log",0,getLogEntryCount());
  }
  
  public void test_getBatchCloseInfo_EmptyBatch() throws Exception
  {
    //FIXME: compilation error
    // remove the batch number and batch date
//    MockTransactionFactory.clearBatchCloseData(TestTranId);

    fail("Failing due compilation error");
    MockHttpServletRequest  request   = new MockHttpServletRequest();

    request.setParameter("profile_id",      TestProfileId);
    request.setParameter("profile_key",     "DZgZKrWxPKbcCCXSZEymdPCkjNhFwkaU");
    request.setParameter("transaction_type",TridentApiTranBase.TT_BATCH_CLOSE);
    request.setParameter("batch_number",    "101");

//    TridentApiTransaction tran = new TridentApiTransaction(MockTransactionFactory.generateTranId());
//    tran.setProperties(request);
//    tran.setProfile(ApiDb.loadProfile(TestProfileId));
//    BatchCloseTask task = new BatchCloseTask(tran);
//    task.doTask();
     
//    assertEquals("batch close failed",TridentApiConstants.ER_NONE,task.getErrorCode());
//    assertEquals("response message","Empty Batch",task.getBatchCloseInfo());
    assertEquals("java log",0,getLogEntryCount());
  }
}