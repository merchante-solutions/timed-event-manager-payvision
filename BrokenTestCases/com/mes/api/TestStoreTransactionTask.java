/*************************************************************************

  FILE: $Archive: /src/test/com/mes/api/TestStoreTransactionTask.sqlj $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2009-03-25 17:18:26 -0700 (Wed, 25 Mar 2009) $
  Version            : $Revision: 15915 $

  Change History:
     See VSS database

  Copyright (C) 2008 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.api;

import java.util.StringTokenizer;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URLDecoder;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.mes.test.MesTestCase;
import com.mes.test.MockHttpServletRequest;
import com.mes.api.TridentApiConstants;
import junit.framework.*;

public class TestStoreTransactionTask extends MesTestCase
{
  String  TridentTranId   = "ea4c406d8c763446bacc9da907172a11";
  
  protected void tearDown() throws Exception
  {
    PreparedStatement ps = DbConnection.getPreparedStatement("delete from trident_capture_api where trident_tran_id = ?");
    ps.setString(1, TridentTranId );
    ps.executeUpdate();
    
    super.tearDown();   // destroy the connection pool
  }
  
  public void test_doTask_payVisionCredit() throws Exception
  {
    TridentApiProfile       profile   = null;
    TridentApiTransaction   testTran  = new TridentApiTransaction(TridentTranId);
    StoreTransactionTask    testClass = null;
    String                  tid       = "94100008615200000003";

    fail("Failing due compilation error");  //FIXME: compilation error
    MockHttpServletRequest  request   = new MockHttpServletRequest();
    
    request.setParameter("profile_id",          tid);
    request.setParameter("profile_key",         "DZgZKrWxPKbcCCXSZEymdPCkjNhFwkaU");
    request.setParameter("transaction_type",    "C");
    request.setParameter("card_number",         "4833009999994455");
    request.setParameter("card_exp_date",       "0912");
    request.setParameter("transaction_amount",  "1.15");
    
    testTran.setProperties(request);
    testTran.setProfile( ApiDb.loadProfile(tid) );
    testClass = new StoreTransactionTask(testTran);
    testClass.setProdFlagDefault(true);
   
    assertTrue("doTask",testClass.doTask());
    
    String qs = 
      " select  *                   " +
      " from    trident_capture_api " +
      " where   trident_tran_id = ? ";
      
    PreparedStatement ps = DbConnection.getPreparedStatement(qs);
    ps.setString(1,TridentTranId);
    ResultSet rs = ps.executeQuery();
    
    assertTrue("missing row",rs.next());
    assertEquals("load filename", "processed-payvision", rs.getString("load_filename"));
    assertEquals("tran amount",1.15,rs.getDouble("transaction_amount"),0.001);
    
    // check for log entries
    assertEquals("java log entries",0,getLogEntryCount());
  }
  
  public void test_doTask_pinDebit() throws Exception
  {
    TridentApiProfile       profile   = null;
    TridentApiTransaction   testTran  = new TridentApiTransaction(TridentTranId);
    StoreTransactionTask    testClass = null;

    fail("Failing due compilation error"); //FIXME: compilation error
    MockHttpServletRequest  request   = new MockHttpServletRequest();
    
    request.setParameter("profile_id",          "94100008615200000001");
    request.setParameter("profile_key",         "DZgZKrWxPKbcCCXSZEymdPCkjNhFwkaU");
    request.setParameter("transaction_type",    "D");
    request.setParameter("card_number",         "4833009999994455");
    request.setParameter("card_exp_date",       "0912");
    request.setParameter("transaction_amount",  "1.15");
    request.setParameter("debit_pin_block",     "dummy-key-block");
    request.setParameter("debit_ksn",           "dummy-key-serial-number");
    
    testTran.setProperties(request);
    testTran.setProfile( ApiDb.loadProfile("94100008615200000001") );
    testTran.setDebitNetworkId("Y");
    testClass = new StoreTransactionTask(testTran);
   
    assertTrue("doTask",testClass.doTask());
    
    String qs = 
      " select  *                   " +
      " from    trident_capture_api " +
      " where   trident_tran_id = ? ";
      
    PreparedStatement ps = DbConnection.getPreparedStatement(qs);
    ps.setString(1,TridentTranId);
    ResultSet rs = ps.executeQuery();
    
    assertTrue("missing row",rs.next());
    assertEquals("reimburse attr", "Z", rs.getString("reimburse_attribute"));
    assertEquals("card type","DB",rs.getString("card_type"));
    assertEquals("debit net","Y",rs.getString("debit_network_id"));
    assertEquals("tran amount",1.15,rs.getDouble("transaction_amount"),0.001);
    
    // check for log entries
    assertEquals("java log entries",0,getLogEntryCount());
  }
}