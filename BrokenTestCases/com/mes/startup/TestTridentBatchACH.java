/*************************************************************************

  FILE: $Archive: /src/test/com/mes/startup/TestTridentBatchACH.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-01-30 09:57:29 -0800 (Fri, 30 Jan 2009) $
  Version            : $Revision: 15757 $

  Change History:
     See VSS database

  Copyright (C) 2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.Calendar;
import com.mes.test.MesTestCase;
import com.mes.ach.AchEntryData;
import junit.framework.*;

public class TestTridentBatchACH extends MesTestCase
{
  protected long                  TestSeqNum            = -1L;
  
  public void tearDown() throws Exception
  {
    try
    {
      if ( TestSeqNum > 0L )
      {
        PreparedStatement ps = DbConnection.getPreparedStatement("delete from ach_trident_statement_test where rec_id in ( select rec_id from trident_capture_totals where process_sequence = " + TestSeqNum + ")" );
        ps.executeUpdate();
        ps.close();
      }        
    }
    finally
    {
      super.tearDown();
    }
  }
  
  public void test_extractAchTridentStatementRecords() throws Exception
  {
    PreparedStatement       ps          = null;
    String                  qs          = null;
    ResultSet               rs          = null;
    TridentBatchACH         testClass   = new TridentBatchACH();
    double                  totalAmount = 0.0;
    int                     totalCount  = 0;
    
    
    try
    {
      testClass.connect();
    
      qs =  " select  max(tct.process_sequence)       as sequence_number  " +
            " from    trident_capture           tc,                       " + 
            "         trident_capture_totals    tct,                      " + 
            "         trident_profile           tp                        " + 
            " where   tc.received_date = trunc(sysdate-2)                 " +
            "         and tct.rec_id = tc.rec_id                          " +
            "         and tp.terminal_id = tc.profile_id                  " +
            "         and exists                                          " +
            "         (                                                   " +
            "           select  atoc.origin_node                          " +
            "           from    ach_trident_origin_control  atoc,         " +
            "                   organization                o,            " +
            "                   group_merchant              gm            " +                    
            "           where   atoc.entry_description = 'MERCH DEP'      " +
            "                   and nvl(atoc.enabled,'N') = 'Y'           " +
            "                   and o.org_group = atoc.origin_node        " +
            "                   and gm.org_num = o.org_num                " +
            "                   and gm.merchant_number = tp.merchant_number " +          
            "         )                                                   ";

      ps = DbConnection.getPreparedStatement(qs);
      rs = ps.executeQuery();
      assertTrue("select seq num failed",rs.next());
      TestSeqNum = rs.getLong("sequence_number");
      rs.close();
      ps.close();
      
      assertTrue("missing test data",(TestSeqNum > 0L));
      
      qs =  " select  /*+ ordered use_nl (tct tc) */              " + 
            "         tc.rec_id             as rec_id,            " +
            "         tp.merchant_number    as merchant_number,   " + 
            "         sum(decode(tct.card_type,                   " + 
            "                    'VS', decode(substr(nvl(mf.visa_plan,'NN'),1,1), 'D', 1, 0)*tct.net_amount,      " + 
            "                    'MC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.net_amount,    " + 
            "                    'DB', decode(substr(nvl(mf.debit_plan,'NN'),1,1), 'D', 1, 0)*tct.net_amount,     " + 
            "                    'DC', decode(substr(nvl(mf.mastcd_plan,'NN'),1,1), 'D', 1, 0)*tct.net_amount,    " + 
            "                    'DS', decode(substr(nvl(mf.discover_plan,'NN'),1,1), 'D', 1, 0)*tct.net_amount,  " + 
            "                    'JC', decode(substr(nvl(mf.jcb_plan, 'NN'),1,1), 'D', 1, 0)*tct.net_amount,      " + 
            "                    'AM', decode(substr(nvl(mf.amex_plan, 'NN'),1,1), 'D', 1, 0)*tct.net_amount,     " + 
            "                    0))        as total_amount       " + 
            " from    trident_capture_totals    tct,              " + 
            "         trident_capture           tc,               " + 
            "         trident_profile           tp,               " + 
            "         mif                       mf                " +
            " where   tct.process_sequence = ?                    " +  
            "         and tc.rec_id = tct.rec_id                  " +
            "         and tc.response_code = 0                    " +
            "         and tp.terminal_id = tc.profile_id          " +
            "         and mf.merchant_number = tp.merchant_number " +
            " group by tc.rec_id,tp.merchant_number               ";

      ps = DbConnection.getPreparedStatement(qs);
      ps.setLong(1,TestSeqNum);
      rs = ps.executeQuery();
      
      assertTrue("select totals failed",rs.next());
      do
      {
        if ( testClass.nodeEnabled( rs.getLong("merchant_number"), AchEntryData.ED_MERCH_DEP ) )
        {
          double amount = rs.getDouble("total_amount");
          if ( amount != 0.0 )
          {
            ++totalCount;
            totalAmount += amount;
          }
        }          
      }
      while(rs.next());
      rs.close();
      ps.close();
      
      System.out.println("totalCount  : " + totalCount);//@
      System.out.println("totalAmount : " + totalAmount);//@

      fail("Failing due compilation error."); //FIXME: compilation error
      // extract the entries into the test table
//      testClass.extractAchTridentStatementRecords("TEST",TestSeqNum);
      
      qs = " select  count(1)                                as total_count,  " + 
           "         sum(ach_amount *                                         " + 
           "             decode(substr(transaction_code,-1),'7',-1,1)) as total_amount  " + 
           " from    ach_trident_statement_test                               " + 
           " where   rec_id in (select distinct rec_id from trident_capture_totals where process_sequence = ?)";
           
      ps = DbConnection.getPreparedStatement(qs);
      ps.setLong(1,TestSeqNum);
      rs = ps.executeQuery();
      assertTrue("select totals failed",rs.next());
      assertEquals("count mismatch",totalCount,rs.getInt("total_count"));
      assertEquals("amount mismatch",totalAmount,rs.getDouble("total_amount"),0.001);
      rs.close();
      ps.close();
      
      assertEquals("java log",0,getLogEntryCount());
    }
    finally
    {
      try{ testClass.cleanUp(); } catch( Exception ee ) {}
      try{ ps.close(); } catch( Exception ee ) {}
    }
  }
}