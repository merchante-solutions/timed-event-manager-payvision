/*************************************************************************

  FILE: $Archive: /src/test/com/mes/startup/TestAchStatementRecord.sqlj $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2008-12-31 10:18:58 -0800 (Wed, 31 Dec 2008) $
  Version            : $Revision: 15681 $

  Change History:
     See VSS database

  Copyright (C) 2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.util.Calendar;

import com.mes.ach.AchStatementRecord;
import com.mes.database.SQLJConnectionBase;
import com.mes.test.MesTestCase;
import junit.framework.*;

public class TestAchStatementRecord extends MesTestCase
{
  AchStatementRecord TestRecord    = new AchStatementRecord();
  
  public void test_getMerchantId()
  {
    TestRecord.setMerchantId(941000086152L);
    assertEquals("bad mid",941000086152L,TestRecord.getMerchantId());
  }
  
  public void test_setMerchantId()
  {
    long[]    mids = {941000000001L,941000000002L,941000000003L};
    
    for(int i = 0; i < mids.length; ++i)
    {
      TestRecord.setMerchantId(mids[i]);
      assertEquals("bad mid "+i,mids[i],TestRecord.getMerchantId());
    }
  }
  
  public void test_getPostDate()
  {
    Date sqlDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
    TestRecord.setPostDate(sqlDate);
    assertEquals("bad date",sqlDate,TestRecord.getPostDate());
  }
  
  public void test_setPostDate()
  {
    java.sql.Date   sqlDate = null;
    
    TestRecord.setPostDate(sqlDate);
    assertEquals("bad date 1",sqlDate,TestRecord.getPostDate());
    
    sqlDate = new java.sql.Date(0);
    TestRecord.setPostDate(sqlDate);
    assertEquals("bad date 2",sqlDate,TestRecord.getPostDate());
    
    sqlDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
    TestRecord.setPostDate(sqlDate);
    assertEquals("bad date 3",sqlDate,TestRecord.getPostDate());
  }
  
  public void test_getEntryDesc()
  {
    TestRecord.setEntryDesc("MERCH DEP");
    assertEquals("bad entry desc","MERCH DEP",TestRecord.getEntryDesc());
  }
  
  public void test_setEntryDesc()
  {
    String[]  values = {"MERCH DEP","MERCH CHBK","MANUAL ADJ","DAILY DISC","MERCH ADJ","BILLING"};
    
    for(int i = 0; i < values.length; ++i)
    {
      TestRecord.setEntryDesc(values[i]);
      assertEquals("bad entry desc "+i,values[i],TestRecord.getEntryDesc());
    }
  }
  
  public void test_getAchAmount()
  {
    TestRecord.setAchAmount(12239892.87);
    assertEquals("bad ach amt ",12239892.87,TestRecord.getAchAmount(),0.0);
  }
  
  public void test_setAchAmount()
  {
    double[]  values = {531.23,108.67,1532.93};
    
    for(int i = 0; i < values.length; ++i)
    {
      TestRecord.setAchAmount(values[i]);
      assertEquals("bad ach amt "+i,values[i],TestRecord.getAchAmount(),0.0);
    }
  }
  
  public void test_getTransactionCode()
  {
    TestRecord.setTransactionCode(99);
    assertEquals("bad tran code ",99,TestRecord.getTransactionCode());
  }
  
  public void test_setTransactionCode()
  {
    int[]  values = {22,27};
    
    for(int i = 0; i < values.length; ++i)
    {
      TestRecord.setTransactionCode(values[i]);
      assertEquals("bad tran code "+i,values[i],TestRecord.getTransactionCode());
    }
  }
  
  public void test_getSalesCount()
  {
    TestRecord.setSalesCount(999);
    assertEquals("bad sales count ",999,TestRecord.getSalesCount());
  }
  
  public void test_setSalesCount()
  {
    int[]  values = {123,189,155,167};
    
    for(int i = 0; i < values.length; ++i)
    {
      TestRecord.setSalesCount(values[i]);
      assertEquals("bad sales count "+i,values[i],TestRecord.getSalesCount());
    }
  }
  
  public void test_getSalesAmount()
  {
    TestRecord.setSalesAmount(999.99);
    assertEquals("bad sales amount",999.99,TestRecord.getSalesAmount(),0.0);
  }
  
  public void test_setSalesAmount()
  {
    double[]  values = {11.27,35.84,115.97,12.00};
    
    for(int i = 0; i < values.length; ++i)
    {
      TestRecord.setSalesAmount(values[i]);
      assertEquals("bad sales amount "+i,values[i],TestRecord.getSalesAmount(),0.0);
    }
  }
  
  public void test_getCreditsCount()
  {
    TestRecord.setCreditsCount(999);
    assertEquals("bad credits count ",999,TestRecord.getCreditsCount());
  }
  
  public void test_setCreditsCount()
  {
    int[]  values = {123,189,155,167};
    
    for(int i = 0; i < values.length; ++i)
    {
      TestRecord.setCreditsCount(values[i]);
      assertEquals("bad credits count "+i,values[i],TestRecord.getCreditsCount());
    }
  }
  
  public void test_getCreditsAmount()
  {
    TestRecord.setCreditsAmount(999.99);
    assertEquals("bad credits amount",999.99,TestRecord.getCreditsAmount(),0.0);
  }
  
  public void test_setCreditsAmount()
  {
    double[]  values = {11.27,35.84,115.97,12.00};
    
    for(int i = 0; i < values.length; ++i)
    {
      TestRecord.setCreditsAmount(values[i]);
      assertEquals("bad credits amount "+i,values[i],TestRecord.getCreditsAmount(),0.0);
    }
  }
  
  public void test_getNetAmount()
  {
    TestRecord.setNetAmount(9999.55);
    assertEquals("bad net amount",9999.55,TestRecord.getNetAmount(),0.0);
  }
  
  public void test_setNetAmount()
  {
    double[]  values = {11.27,35.84,115.97,12.00};
    
    for(int i = 0; i < values.length; ++i)
    {
      TestRecord.setNetAmount(values[i]);
      assertEquals("bad net amount "+i,values[i],TestRecord.getNetAmount(),0.0);
    }
  }
  
  public void test_getRecId()
  {
    TestRecord.setRecId(9567329047L);
    assertEquals("bad rec id",9567329047L,TestRecord.getRecId());
  }
  
  public void test_setRecId()
  {
    long[]  values = {7777777L,938089523L,759035790L,0L};
    
    for(int i = 0; i < values.length; ++i)
    {
      TestRecord.setRecId(values[i]);
      assertEquals("bad rec id "+i,values[i],TestRecord.getRecId());
    }
  }
}