/*************************************************************************

  FILE: $Archive: /src/test/com/mes/startup/TestFxServiceSyncEvent.sqlj $

  Description:  


  Last Modified By   : $Author:  $
  Last Modified Date : $Date:  $
  Version            : $Revision: $

  Change History:
     See VSS database

  Copyright (C) 2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import com.mes.test.MesTestCase;
import junit.framework.*;

public class TestFxServiceSyncEvent extends MesTestCase
{
  public void test_extractCbControlNumber()
  {
    FxServiceSyncEvent      testClass   = new FxServiceSyncEvent();
    
    String[]      testBatchIds = 
    {
      "ChgBk from 267csv ID:1660/73349/ TransID:258780",
      "ChgBk from 267csv ID:1650/73351/ TransID:279767",
      "ChgBk from 267csv ID:1641/73353/ TransID:261898",
      "ChgBk from 267csv ID:1639/73365/ TransID:379916",
      "ChgBk from 267csv ID:1626/73371/ TransID:433700",
    };
    
    String[]      testControlNumbers =
    {
      "73349",
      "73351",
      "73353",
      "73365",
      "73371",    
    };
    
    for( int i = 0; i < testBatchIds.length; ++i )
    {
      assertEquals("invalid control number extraction",
                    testControlNumbers[i],
                    testClass.extractCbId(testBatchIds[i]));
    }      
  }
  
  public void test_getAchBatchId() throws Exception
  {
    FxServiceSyncEvent      testClass   = new FxServiceSyncEvent();
    
    assertTrue("failed to get ach batch id",(testClass.getAchBatchId() != 0L));
    assertEquals("log entries detected",0,getLogEntryCount());
  }
}