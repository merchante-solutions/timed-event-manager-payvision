/*************************************************************************

  FILE: $Archive: /src/test/com/mes/startup/TestTPGBatchCutoff.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-01-30 09:57:29 -0800 (Fri, 30 Jan 2009) $
  Version            : $Revision: 15757 $

  Change History:
     See VSS database

  Copyright (C) 2007 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.util.Calendar;

import com.mes.test.MesTestCase;
import junit.framework.*;

public class TestTPGBatchCutoff extends MesTestCase
{
  public void tearDown() throws Exception
  {
    long                mid   = 0L;
    PreparedStatement   ps    = null;
    
    ps = DbConnection.getPreparedStatement("delete from ach_trident_statement_test where merchant_number = ?");
    ps.setLong(1,mid);
    ps.executeUpdate();
    ps.close();
    super.tearDown();
  }    

  public void test_execute() throws Exception
  {
    TPGBatchCutoff      testClass   = new TPGBatchCutoff();
    
    testClass.setTestTerminalId( "94100008615200000001" );
    boolean retVal = testClass.execute();
    assertTrue(retVal);
    assertEquals(0,getLogEntryCount());
  }
}