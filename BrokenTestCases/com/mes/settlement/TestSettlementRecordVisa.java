/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/test/com/mes/settlement/TestSettlementRecordVisa.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-12-11 09:46:45 -0800 (Fri, 11 Dec 2009) $
  Version            : $Revision: 16808 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import java.util.List;

import com.mes.test.MesTestCase;
import com.mes.test.MockHttpServletRequest;
import com.mes.tools.HierarchyTree;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import com.mes.support.MesEncryption;
import com.mes.flatfile.FlatFileRecord;
import com.mes.user.UserBean;

import junit.framework.*;

public class TestSettlementRecordVisa extends MesTestCase
{
  public void test_buildFlatFileRecords()
  {
    SettlementDb      db        = null;
    long              recId     = -1L;
    
    try
    {
      // get a valid record id from the visa settlement table
      PreparedStatement ps = DbConnection.getPreparedStatement("select max(rec_id) as recId from visa_settlement");
      ResultSet rs = ps.executeQuery();
      rs.next();
      recId = rs.getLong(1);
      rs.close();
      ps.close();
      
      db = new SettlementDb();
      db.connect();
      
      SettlementRecord rec = db._loadSettlementRecord("VS",recId);
      
      assertNotNull("settlement rec is null",rec);

      fail("Failing due compilation error."); //FIXME: compilation error
//      List flatFileRecs = rec.buildFlatFileRecords();
//
//      assertNotNull("flat file rec list is null",flatFileRecs);
//      assertTrue("flat file rec list is empty",(flatFileRecs.size() > 0));
//
//      for ( int i = 0; i < flatFileRecs.size(); ++i )
//      {
//        FlatFileRecord ffd = (FlatFileRecord)flatFileRecs.get(i);
//        ffd.showData();
//      }
//      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      DbConnection.logEntry("test_buildFlatFileRecords()",e.toString());
      e.printStackTrace();
      fail("exception - " + e.toString());
    }
    finally
    {
      try{ db.cleanUp(); }catch(Exception ee){}
    }      
  }
}