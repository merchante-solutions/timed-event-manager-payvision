/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/test/com/mes/settlement/TestArdefDataVisa.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-12-11 09:46:45 -0800 (Fri, 11 Dec 2009) $
  Version            : $Revision: 16808 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;

import com.mes.test.MesTestCase;
import com.mes.test.MockHttpServletRequest;
import com.mes.tools.HierarchyTree;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import com.mes.support.MesEncryption;
import com.mes.user.UserBean;

import junit.framework.*;

public class TestArdefDataVisa extends MesTestCase
{
  public void notest_getCardTypeFull()
    throws Exception
  {
    Date              beginDate       = null;
    String            cardNumberFull  = null;
    SettlementDb      db              = null;
    Date              endDate         = null;
    ArdefDataVisa     testBean        = null;
    
    PreparedStatement decryptPs = null;//@
    ResultSet         decryptRs = null;//@
    
    try
    {
      db = new SettlementDb();
      db.connect(true);
    
      beginDate = new java.sql.Date( DateTimeFormatter.parseDate("04/10/2009","MM/dd/yyyy").getTime() );
      endDate   = new java.sql.Date( DateTimeFormatter.parseDate("04/10/2009","MM/dd/yyyy").getTime() );
    
      String sqlText = 
        " select  dt.card_number_enc  as card_number_enc, " +
//@        "         dukpt_decrypt_wrapper(dt.card_number_enc)  as card_number_full, " +
        "         dt.card_type        as card_type    " +
        " from    daily_detail_file_dt    dt          " +
        " where                                       " +
        "         dt.load_filename = ?                " +
//@        "         dt.merchant_account_number = ?      " +
//@        "         and dt.batch_date between ? and ?   " +
//@        "         and dt.cardholder_account_number = '433477xxxxxx7538' " +
        "         and substr(dt.card_type,1,1) = 'V'  " +
        " order by dt.cardholder_account_number       ";
  
      PreparedStatement ps = DbConnection.getPreparedStatement(sqlText);
//@      ps.setLong(1,941000017716L);
//@      ps.setDate(2,beginDate);
//@      ps.setDate(3,endDate);
      ps.setString(1,"ddf3941_041409_001.dat");
      ResultSet rs = ps.executeQuery();
      
      String cardType = null;
      String ddfCardType = null;
      int recCount = 0;
      int loadCount = 0;
      
      decryptPs = DbConnection.getPreparedStatement("select dukpt_decrypt_wrapper(?) as card_number_full from dual");
      
      int retryLoop = 0;
      
      while(rs.next())
      {
        recCount++;
        double ratio = ((double)loadCount/(double)recCount);
        System.out.print("testing rec # " + recCount + " load # " + loadCount + " ratio " + NumberFormatter.getPercentString(ratio,3) + "     \r");
        
//@        decryptPs.setString(1,rs.getString("card_number_enc"));
//@        decryptRs = decryptPs.executeQuery();
//@        decryptRs.next();
//@        cardNumberFull = decryptRs.getString("card_number_full");
//@        decryptRs.close();
        //@cardNumberFull = rs.getString("card_number_full");
        
        for( retryLoop = 10; retryLoop > 0; --retryLoop )
        {
          try 
          {
            cardNumberFull = MesEncryption.decryptString(rs.getString("card_number_enc"));
            break;
          } 
          catch( Exception eee ) 
          {
            Thread.sleep(100);
          }
        }
        if ( retryLoop <= 0 ) { System.out.println("decrypt failed                         "); }
        
        if ( testBean == null || !testBean.isInRange(cardNumberFull) )
        {
          testBean = db._loadArdefVisa(cardNumberFull); 
          ++loadCount;
        }
        
        //assertNotNull("ardef entry null " + cardNumberFull,testBean);
        if ( testBean == null )
        {
          cardType = "VS";    // default to standard visa
        }
        else
        {
          cardType = testBean.getCardTypeFull();
        }
        ddfCardType = rs.getString("card_type");
        
        assertNotNull("bad ddf card type " + cardNumberFull,ddfCardType);
        //@assertEquals("bad decode " + cardNumberFull,ddfCardType,cardType );
        
        //@+ *************
        if ( !ddfCardType.equals(cardType) )
        {
          System.out.println("bad decode " + cardNumberFull + " " + ddfCardType + " " + cardType);
        }
        //@- *************
      }      
      rs.close();
      ps.close();
    
      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      System.out.println();
      System.out.println("" + cardNumberFull + " failed - " + e.toString());
      e.printStackTrace();
    }
    finally
    {
      System.out.println();
      try{ decryptPs.close(); }catch(Exception ee){}
      try{ db.cleanUp(); }catch(Exception ee){}
    }      
  }
}