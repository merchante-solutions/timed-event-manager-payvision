/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/test/com/mes/settlement/MockTransactionFactory.java $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2009-05-14 17:40:07 -0700 (Thu, 14 May 2009) $
  Version            : $Revision: 16129 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.util.Calendar;

import com.mes.support.DateTimeFormatter;

public class MockTransactionFactory 
{
  public static void setRewardsCardData( SettlementRecord rec )
  {
    rec.setData("card_number", "488893xxxxxx2565");
    rec.setData("card_number_enc", "000002213300000200000010B85E2B11EF39123E862EEC34A76F3EE5");
    rec.setData("card_type","VS");
    rec.setData("card_type_enhanced", "JYC");
    rec.setData("product_id", "B");
  }
  
  public static void setSignatureCardData( SettlementRecord rec )
  {
    rec.setData("card_number", "414778xxxxxx5387");
    rec.setData("card_number_enc", "0000021182000002000000104DC089C981E3816D498F619AABA4CF1B");
    rec.setData("card_type","VS");
    rec.setData("card_type_enhanced", "KYC");
    rec.setData("product_id", "C");
  }

  public static void setConsumerCardData( SettlementRecord rec )
  {
    rec.setData("card_number", "432419xxxxxx5669");
    rec.setData("card_number_enc", "0000021075000002000000109B1EC95128CA57E004FA57AAA79DEEEE");
    rec.setData("card_type","VS");
    rec.setData("card_type_enhanced", "CYC");
    rec.setData("product_id", "A");
  }
  
  public static void setConsumerCheckCardData( SettlementRecord rec )
  {
    rec.setData("card_number", "432377xxxxxx7317");
    rec.setData("card_number_enc", "000002213300000200000010D72725F3C74FA1A9F61BA5B52555C173");
    rec.setData("card_type","VD");
    rec.setData("card_type_enhanced", "P D");
    rec.setData("product_id", "H");
  }

  public static void setCorporateCardData( SettlementRecord rec )
  {
    rec.setData("card_number", "479804xxxxxx9682");
    rec.setData("card_number_enc", "000002107500000200000010A3523B2EEE4DC7E6C7C2CC7C4773E2F7");
    rec.setData("card_type","VB");
    rec.setData("card_type_enhanced", "R C");
    rec.setData("product_id", "K");
  }
  
  public static void setPurchasingCardData( SettlementRecord rec )
  {
    rec.setData("card_number", "471553xxxxxx8603");
    rec.setData("card_number_enc", "0000020624000002000000101A86A7291F217FD9E9D3765CB875EE89");
    rec.setData("card_type","VB");
    rec.setData("card_type_enhanced", "S C");
    rec.setData("product_id", "S");
  }
  
  public static void setCardNotPresentData( SettlementRecord rec )
  {
    rec.setData("auth_returned_aci", "V");      // CNP - AVS requested
    rec.setData("auth_avs_response", "Y");      // address + 5-digit ZIP match
    rec.setData("auth_cvv2_response", "M");     // CVV2 match
    rec.setData("pos_entry_mode", "01");        // keyed entry mode
    rec.setData("moto_ecommerce_ind", "1");     // one time MOTO
    rec.setData("card_present", "N");           // card is not present
    rec.setData("cardholder_id_method", "4");   // moto or ecommerce
  }
  
  public static void setCardPresentKeyedData( SettlementRecord rec )
  {
    rec.setData("auth_returned_aci", "K");      // card present key entry
    rec.setData("auth_avs_response", "Z");      // ZIP match
    rec.setData("auth_cvv2_response", "");      // no CVV2 request
    rec.setData("pos_entry_mode", "01");        // keyed entry mode
    rec.setData("moto_ecommerce_ind", "");      // not a moto transaction
    rec.setData("card_present", "Y");           // card present
    rec.setData("cardholder_id_method", "1");   // signature
  }
  
  public static void setLevelIIData( SettlementRecord rec )
  {
    setPurchaseIdDataRetail(rec);
    rec.setData("tax_amount", "0.01");
  }
  
  public static void setPurchaseIdDataRetail( SettlementRecord rec )
  {
    setPurchaseIdData(rec,"1");     // order number
  }
  
  public static void setPurchaseIdDataAutoRental( SettlementRecord rec )
  {
    setPurchaseIdData(rec,"3");     // rental agreement number
  }
  
  public static void setPurchaseIdDataHotel( SettlementRecord rec )
  {
    setPurchaseIdData(rec,"4");     // folio number
  }
  
  public static void setPurchaseIdData( SettlementRecord rec, String format )
  {
    rec.setData("purchase_id_format", format);
    rec.setData("purchase_id", "159847");
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CPS_Retail_Corporate()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_Retail_Base();
    setCorporateCardData(rec);
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CPS_Retail_Corporate_LevelII()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_CPS_Retail_Corporate();
    setLevelIIData(rec);
    return( rec );
  }    
  
  public static SettlementRecordVisa createSettlementRecordVisa_CPS_CardNotPresent_Corporate()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_Retail_Base();
    setCorporateCardData(rec);
    setCardNotPresentData(rec);
    setPurchaseIdDataRetail(rec);   // CNP requires order #
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CPS_CardNotPresent_Corporate_LevelII()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_CPS_CardNotPresent_Corporate();
    setLevelIIData(rec);
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CPS_Retail_Purchasing()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_Retail_Base();
    setPurchasingCardData(rec);
    setPurchaseIdDataRetail(rec);   // CNP requires order #
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CPS_Retail_Purchasing_LevelII()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_CPS_Retail_Purchasing();
    setLevelIIData(rec);
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CPS_CardNotPresent_Purchasing()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_Retail_Base();
    setPurchasingCardData(rec);
    setCardNotPresentData(rec);
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CPS_CardNotPresent_Purchasing_LevelII()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_CPS_CardNotPresent_Purchasing();
    setLevelIIData(rec);
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CPS_Retail_Consumer()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_Retail_Base();
    setConsumerCardData(rec);
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CPS_Retail_Consumer_CheckCard()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_Retail_Base();
    setConsumerCheckCardData(rec);
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CPS_Retail_Keyed_Consumer()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_CPS_Retail_Consumer();
    setCardPresentKeyedData(rec);
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CPS_Retail_Signature()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_Retail_Base();
    setSignatureCardData(rec);
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CPS_Retail_Rewards()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_Retail_Base();
    setRewardsCardData(rec);
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CPS_SmallTicket_Consumer()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_Base("5994","D");
    setConsumerCardData(rec);
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CreditVoucher_Consumer()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_Base("5999","C");
    setConsumerCardData(rec);       // set consumer card data
    rec.setData("product_id","");   // no product id on credits
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CreditVoucher_Consumer_CheckCard()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_Base("5999","C");
    setConsumerCheckCardData(rec);  // set consumer check card data
    rec.setData("product_id","");   // no product id on credits
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CreditVoucher_Corporate()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_Base("5999","C");
    setCorporateCardData(rec);      // set corporate card data
    rec.setData("product_id","");   // no product id on credits
    return( rec );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_CreditVoucher_Purchasing()
  {
    SettlementRecordVisa    rec  = createSettlementRecordVisa_Base("5999","C");
    setCorporateCardData(rec);      // set purchasing card data
    rec.setData("product_id","");   // no product id on credits
    return( rec );
  }

  public static SettlementRecordVisa createSettlementRecordVisa_Retail_Base()
  {
    return( createSettlementRecordVisa_Base("5999","D") );
  }
  
  public static SettlementRecordVisa createSettlementRecordVisa_Base(String sic, String debitCreditInd)
  {
    SettlementRecordVisa    rec         = new SettlementRecordVisa();
    String                  tranDateStr = DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy");
    
    rec.createFields(null);
    
    // base fields
    rec.setData("sic_code", sic);
    rec.setData("bin_number", "433239");
    rec.setData("merchant_number", "941000086152");
    rec.setData("dba_name", "J*UNIT TEST ACCOUNT");
    rec.setData("dba_city", "SPOKANE");
    rec.setData("dba_state", "WA");
    rec.setData("dba_zip", "99212");
    rec.setData("country_code", "US");
    rec.setData("phone_number", "509-2325610");
    rec.setData("reference_number", "24332399128001291376168");
    rec.setData("acq_reference_number", "129137616");
    rec.setData("us_issuer", "Y");
    rec.setData("batch_number", "33");
    rec.setData("batch_id", "198010507");
    rec.setData("batch_date", tranDateStr);
    rec.setData("transaction_date", tranDateStr);
    rec.setData("debit_credit_indicator", debitCreditInd);
    rec.setData("transaction_amount", "0.3");
    rec.setData("card_present", "Y");
    rec.setData("cardholder_id_method", "1");
    rec.setData("pos_entry_mode", "90");
    rec.setData("reimbursement_attribute", "0");
    
    if ( "D".equals(debitCreditInd) )
    {
      // set the authorization data
      rec.setData("auth_date", tranDateStr);
      rec.setData("auth_amount", "0.3");
      rec.setData("auth_amount_total", "0.3");
      rec.setData("auth_currency_code", "840");
      rec.setData("auth_code", "037644");
      rec.setData("auth_source_code", "5");
      rec.setData("auth_response_code", "00");
      rec.setData("auth_returned_aci", "E");
      rec.setData("auth_retrieval_ref_num", "911820735331");
      rec.setData("auth_tran_id", "289118741441279");
      rec.setData("auth_val_code", "P26G");
      rec.setData("auth_avs_response", "0");
      rec.setData("auth_rec_id", "186714672");
    }
    else    // credit, no auth data
    {
      rec.setData("auth_amount", "0.00");
      rec.setData("auth_source_code", "9");
    }
    return( rec );
  }    
}