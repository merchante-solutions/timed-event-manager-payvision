/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/test/com/mes/settlement/TestInterchangeVisa.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-12-11 09:46:45 -0800 (Fri, 11 Dec 2009) $
  Version            : $Revision: 16808 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;

import com.mes.test.MesTestCase;
import com.mes.test.MockHttpServletRequest;
import com.mes.tools.HierarchyTree;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import com.mes.support.MesEncryption;
import com.mes.user.UserBean;

import junit.framework.*;

public class TestInterchangeVisa extends MesTestCase
{
  private void showQualificationValues( SettlementRecord rec )
  {
    System.out.println( rec.getString("fee_program_indicator") + "," + 
                        rec.getString("reimbursement_attribute") + "," +
                        rec.getString("requested_payment_service") );
  }
  
  public void test_getInterchangeClass()
  {
    InterchangeVisa   testClass = new InterchangeVisa();
    String[]          classList = 
      {
        "IC_Visa_CPS_AccountFunding",
        "IC_Visa_CPS_AutomatedFuelDisp",
        "IC_Visa_CPS_CardNotPresent",
        "IC_Visa_CPS_ECommerceBasic",
        "IC_Visa_CPS_ECommercePrefRtl",
        "IC_Visa_CPS_Restaurant",
        "IC_Visa_CPS_Retail",
        "IC_Visa_CPS_Retail2",
        "IC_Visa_CPS_Retail2_Moto",
        "IC_Visa_CPS_RetailKeyEntry",
        "IC_Visa_CPS_RetailSvcStation",
        "IC_Visa_CPS_SmallTicket",
        "IC_Visa_CPS_Supermarket",
        "IC_Visa_CreditVoucher",
        "IC_Visa_Electronic",
        "IC_Visa_Standard",
      };
    
    for( int i = 0; i < classList.length; ++i )
    {
      assertNotNull("missing class " + classList[i], testClass.getInterchangeClass(classList[i]));
    }
  }
  
  public void test_getBestClass_CPS_Retail_Corporate()
  {
    Calendar                cal       = Calendar.getInstance();
    SettlementRecordVisa    rec       = null;
    InterchangeVisa         testClass = new InterchangeVisa();
  
    // test CPS/Retail CR with swiped corporate card
    rec = MockTransactionFactory.createSettlementRecordVisa_CPS_Retail_Corporate();
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid corp fpi","235",rec.getString("fee_program_indicator"));
    assertEquals("invalid corp ra","A",rec.getString("reimbursement_attribute"));
    assertEquals("invalid corp rps","A",rec.getString("requested_payment_service"));
    
    // test CPS/Retail CR with expired auth
    cal.add(Calendar.DAY_OF_MONTH,-5 );    // set to be too old to qualify
    rec.setData("auth_date", DateTimeFormatter.getFormattedDate(cal.getTime(),"MM/dd/yyyy"));
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid corp fpi","110",rec.getString("fee_program_indicator"));
    assertEquals("invalid corp ra","J",rec.getString("reimbursement_attribute"));
    assertEquals("invalid corp rps",null,rec.getString("requested_payment_service"));
  }
  
  public void test_getBestClass_CPS_CardNotPresent_Corporate()
  {
    Calendar                cal       = Calendar.getInstance();
    SettlementRecordVisa    rec       = null;
    InterchangeVisa         testClass = new InterchangeVisa();
  
    // test CPS/Card Not Present Credit with corporate card
    rec = MockTransactionFactory.createSettlementRecordVisa_CPS_CardNotPresent_Corporate();
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_CardNotPresent"));
    assertEquals("invalid CNP fpi","234",rec.getString("fee_program_indicator"));
    assertEquals("invalid CNP ra","A",rec.getString("reimbursement_attribute"));
    assertEquals("invalid CNP rps","7",rec.getString("requested_payment_service"));
    
    // test CPS/Card Not Present Credit with expired auth
    cal.add(Calendar.DAY_OF_MONTH,-10 );    // set to be too old to qualify
    rec.setData("auth_date", DateTimeFormatter.getFormattedDate(cal.getTime(),"MM/dd/yyyy"));
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_CardNotPresent"));
    assertEquals("invalid CNP fpi","110",rec.getString("fee_program_indicator"));
    assertEquals("invalid CNP ra","J",rec.getString("reimbursement_attribute"));
    assertEquals("invalid CNP rps",null,rec.getString("requested_payment_service"));
  }
  
  public void test_getBestClass_CPS_Retail_Corporate_LevelII()
  {
    Calendar                cal       = Calendar.getInstance();
    SettlementRecordVisa    rec       = null;
    InterchangeVisa         testClass = new InterchangeVisa();
  
    // test CPS/Retail CR with swiped corporate card
    rec = MockTransactionFactory.createSettlementRecordVisa_CPS_Retail_Corporate_LevelII();
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid corp fpi","171",rec.getString("fee_program_indicator"));
    assertEquals("invalid corp ra","A",rec.getString("reimbursement_attribute"));
    assertEquals("invalid corp rps","A",rec.getString("requested_payment_service"));
    
    // test CPS/Retail CR with expired auth
    cal.add(Calendar.DAY_OF_MONTH,-5 );    // set to be too old to qualify
    rec.setData("auth_date", DateTimeFormatter.getFormattedDate(cal.getTime(),"MM/dd/yyyy"));
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid CNP fpi","113",rec.getString("fee_program_indicator"));
    assertEquals("invalid CNP ra","J",rec.getString("reimbursement_attribute"));
    assertEquals("invalid CNP rps",null,rec.getString("requested_payment_service"));
  }
  
  public void test_getBestClass_CPS_CardNotPresent_Corporate_LevelII()
  {
    Calendar                cal       = Calendar.getInstance();
    SettlementRecordVisa    rec       = null;
    InterchangeVisa         testClass = new InterchangeVisa();
  
    // test CPS/Card Not Present Credit with corporate card
    rec = MockTransactionFactory.createSettlementRecordVisa_CPS_CardNotPresent_Corporate_LevelII();
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_CardNotPresent"));
    assertEquals("invalid CNP fpi","171",rec.getString("fee_program_indicator"));
    assertEquals("invalid CNP ra","A",rec.getString("reimbursement_attribute"));
    assertEquals("invalid CNP rps","7",rec.getString("requested_payment_service"));
    
    // test CPS/Card Not Present Credit with expired auth
    cal.add(Calendar.DAY_OF_MONTH,-10 );    // set to be too old to qualify
    rec.setData("auth_date", DateTimeFormatter.getFormattedDate(cal.getTime(),"MM/dd/yyyy"));
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_CardNotPresent"));
    assertEquals("invalid CNP fpi","113",rec.getString("fee_program_indicator"));
    assertEquals("invalid CNP ra","J",rec.getString("reimbursement_attribute"));
    assertEquals("invalid CNP rps",null,rec.getString("requested_payment_service"));
  }
  
  public void test_getBestClass_CPS_Retail_Purchasing_LevelII()
  {
    Calendar                cal       = Calendar.getInstance();
    SettlementRecordVisa    rec       = null;
    InterchangeVisa         testClass = new InterchangeVisa();
  
    // test CPS/Retail Credit with swiped purchasing card
    rec = MockTransactionFactory.createSettlementRecordVisa_CPS_Retail_Purchasing_LevelII();
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid VB fpi","172",rec.getString("fee_program_indicator"));
    assertEquals("invalid VB ra","A",rec.getString("reimbursement_attribute"));
    assertEquals("invalid VB rps","A",rec.getString("requested_payment_service"));
  }
  
  public void test_getBestClass_CPS_CardNotPresent_Purchasing_LevelII()
  {
    Calendar                cal       = Calendar.getInstance();
    SettlementRecordVisa    rec       = null;
    InterchangeVisa         testClass = new InterchangeVisa();
  
    // test CPS/Card Not Present Credit with purchasing card
    rec = MockTransactionFactory.createSettlementRecordVisa_CPS_CardNotPresent_Purchasing_LevelII();
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_CardNotPresent"));
    assertEquals("invalid CNP fpi","172",rec.getString("fee_program_indicator"));
    assertEquals("invalid CNP ra","A",rec.getString("reimbursement_attribute"));
    assertEquals("invalid CNP rps","7",rec.getString("requested_payment_service"));
    
    // test CPS/Card Not Present Credit with expired auth
    cal.add(Calendar.DAY_OF_MONTH,-10 );    // set to be too old to qualify
    rec.setData("auth_date", DateTimeFormatter.getFormattedDate(cal.getTime(),"MM/dd/yyyy"));
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_CardNotPresent"));
    assertEquals("invalid CNP fpi","114",rec.getString("fee_program_indicator"));
    assertEquals("invalid CNP ra","J",rec.getString("reimbursement_attribute"));
    assertEquals("invalid CNP rps",null,rec.getString("requested_payment_service"));
  }

  public void test_getBestClass_CPS_Retail_Consumer()
    throws Exception
  {
    Calendar                cal       = Calendar.getInstance();
    SettlementRecordVisa    rec       = null;
    InterchangeVisa         testClass = new InterchangeVisa();
    
    // test CPS/Retail Credit
    rec = MockTransactionFactory.createSettlementRecordVisa_CPS_Retail_Consumer();
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid fpi","106",rec.getString("fee_program_indicator"));
    assertEquals("invalid ra","A",rec.getString("reimbursement_attribute"));
    assertEquals("invalid rps","A",rec.getString("requested_payment_service"));
    
    // test CPS/Retail Credit with expired auth
    cal.add(Calendar.DAY_OF_MONTH,-5);    // set to be too old to qualify
    rec.setData("auth_date", DateTimeFormatter.getFormattedDate(cal.getTime(),"MM/dd/yyyy"));
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid eirf fpi","141",rec.getString("fee_program_indicator"));
    assertEquals("invalid eirf ra","J",rec.getString("reimbursement_attribute"));
    assertEquals("invalid eirf rps",null,rec.getString("requested_payment_service"));
    
    // test CPS/Retail Credit going to standard due to cardholder id method
    rec.setData("auth_date", DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy"));
    rec.setData("cardholder_id_method", "0");
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid std fpi","101",rec.getString("fee_program_indicator"));
    assertEquals("invalid std ra","0",rec.getString("reimbursement_attribute"));
    assertEquals("invalid std rps",null,rec.getString("requested_payment_service"));
    
    // test CPS/Retail Credit going to standard due to entry mode
    rec.setData("cardholder_id_method", "1");
    rec.setData("pos_entry_mode", "06");
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid entry mode fpi","101",rec.getString("fee_program_indicator"));
    assertEquals("invalid entry mode ra","0",rec.getString("reimbursement_attribute"));
    assertEquals("invalid entry mode rps",null,rec.getString("requested_payment_service"));
  }
  
  public void test_getBestClass_CPS_Retail_Consumer_CheckCard()
    throws Exception
  {
    Calendar                cal       = Calendar.getInstance();
    SettlementRecordVisa    rec       = null;
    InterchangeVisa         testClass = new InterchangeVisa();
    
    // test CPS/Retail DB
    rec = MockTransactionFactory.createSettlementRecordVisa_CPS_Retail_Consumer_CheckCard();
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid fpi","210",rec.getString("fee_program_indicator"));
    assertEquals("invalid ra","A",rec.getString("reimbursement_attribute"));
    assertEquals("invalid rps","A",rec.getString("requested_payment_service"));
    
    // test CPS/Retail DB with expired auth
    cal.add(Calendar.DAY_OF_MONTH,-5);    // set to be too old to qualify
    rec.setData("auth_date", DateTimeFormatter.getFormattedDate(cal.getTime(),"MM/dd/yyyy"));
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid eirf fpi","195",rec.getString("fee_program_indicator"));
    assertEquals("invalid eirf ra","J",rec.getString("reimbursement_attribute"));
    assertEquals("invalid eirf rps",null,rec.getString("requested_payment_service"));
    
    // test CPS/Retail DB going to standard due to cardholder id method
    rec.setData("auth_date", DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy"));
    rec.setData("cardholder_id_method", "0");
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid std fpi","196",rec.getString("fee_program_indicator"));
    assertEquals("invalid std ra","0",rec.getString("reimbursement_attribute"));
    assertEquals("invalid std rps",null,rec.getString("requested_payment_service"));
    
    // test CPS/Retail DB going to standard due to entry mode
    rec.setData("cardholder_id_method", "1");
    rec.setData("pos_entry_mode", "06");
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid entry mode fpi","196",rec.getString("fee_program_indicator"));
    assertEquals("invalid entry mode ra","0",rec.getString("reimbursement_attribute"));
    assertEquals("invalid entry mode rps",null,rec.getString("requested_payment_service"));
  }
  
  public void test_getBestClass_CPS_Retail_Rewards()
    throws Exception
  {
    Calendar                cal       = Calendar.getInstance();
    SettlementRecordVisa    rec       = null;
    InterchangeVisa         testClass = new InterchangeVisa();
    
    // test CPS/Retail CR with rewards card
    rec = MockTransactionFactory.createSettlementRecordVisa_CPS_Retail_Rewards();
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid fpi","219",rec.getString("fee_program_indicator"));
    assertEquals("invalid ra","A",rec.getString("reimbursement_attribute"));
    assertEquals("invalid rps","A",rec.getString("requested_payment_service"));
    
    // test CPS/Retail CR rewards card with expired auth
    cal.add(Calendar.DAY_OF_MONTH,-5);    // set to be too old to qualify
    rec.setData("auth_date", DateTimeFormatter.getFormattedDate(cal.getTime(),"MM/dd/yyyy"));
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid eirf fpi","141",rec.getString("fee_program_indicator"));
    assertEquals("invalid eirf ra","J",rec.getString("reimbursement_attribute"));
    assertEquals("invalid eirf rps",null,rec.getString("requested_payment_service"));
    
    // test CPS/Retail CR rewards card to standard due to cardholder id method
    rec.setData("auth_date", DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy"));
    rec.setData("cardholder_id_method", "0");
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid std fpi","101",rec.getString("fee_program_indicator"));
    assertEquals("invalid std ra","0",rec.getString("reimbursement_attribute"));
    assertEquals("invalid std rps",null,rec.getString("requested_payment_service"));
    
    // test CPS/Retail CR rewards card to standard due to entry mode
    rec.setData("cardholder_id_method", "1");
    rec.setData("pos_entry_mode", "06");
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid entry mode fpi","101",rec.getString("fee_program_indicator"));
    assertEquals("invalid entry mode ra","0",rec.getString("reimbursement_attribute"));
    assertEquals("invalid entry mode rps",null,rec.getString("requested_payment_service"));
  }
  
  public void test_getBestClass_CPS_Retail_Signature()
    throws Exception
  {
    Calendar                cal       = Calendar.getInstance();
    SettlementRecordVisa    rec       = null;
    InterchangeVisa         testClass = new InterchangeVisa();
    
    // test CPS/Retail CR with signature card
    rec = MockTransactionFactory.createSettlementRecordVisa_CPS_Retail_Signature();
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid fpi","219",rec.getString("fee_program_indicator"));
    assertEquals("invalid ra","A",rec.getString("reimbursement_attribute"));
    assertEquals("invalid rps","A",rec.getString("requested_payment_service"));
    
    // test CPS/Retail CR signature card with expired auth
    cal.add(Calendar.DAY_OF_MONTH,-5);    // set to be too old to qualify
    rec.setData("auth_date", DateTimeFormatter.getFormattedDate(cal.getTime(),"MM/dd/yyyy"));
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid eirf fpi","141",rec.getString("fee_program_indicator"));
    assertEquals("invalid eirf ra","J",rec.getString("reimbursement_attribute"));
    assertEquals("invalid eirf rps",null,rec.getString("requested_payment_service"));
    
    // test CPS/Retail CR signature card to standard due to cardholder id method
    rec.setData("auth_date", DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy"));
    rec.setData("cardholder_id_method", "0");
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid std fpi","101",rec.getString("fee_program_indicator"));
    assertEquals("invalid std ra","0",rec.getString("reimbursement_attribute"));
    assertEquals("invalid std rps",null,rec.getString("requested_payment_service"));
    
    // test CPS/Retail CR signature card to standard due to entry mode
    rec.setData("cardholder_id_method", "1");
    rec.setData("pos_entry_mode", "06");
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_Retail"));
    assertEquals("invalid entry mode fpi","101",rec.getString("fee_program_indicator"));
    assertEquals("invalid entry mode ra","0",rec.getString("reimbursement_attribute"));
    assertEquals("invalid entry mode rps",null,rec.getString("requested_payment_service"));
  }
  
  public void test_getBestClass_CPS_Retail_Keyed_Consumer()
    throws Exception
  {
    Calendar                cal       = Calendar.getInstance();
    SettlementRecordVisa    rec       = null;
    InterchangeVisa         testClass = new InterchangeVisa();
  
    rec = MockTransactionFactory.createSettlementRecordVisa_CPS_Retail_Keyed_Consumer();
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_RetailKeyEntry"));
    assertEquals("invalid fpi","152",rec.getString("fee_program_indicator"));
    assertEquals("invalid ra","A",rec.getString("reimbursement_attribute"));
    assertEquals("invalid rps","E",rec.getString("requested_payment_service"));
  }
  
  public void test_getBestClass_CPS_SmallTicket_Consumer()
    throws Exception
  {
    Calendar                cal       = Calendar.getInstance();
    SettlementRecordVisa    rec       = null;
    InterchangeVisa         testClass = new InterchangeVisa();
    
    // test CPS/Small Ticket CR
    rec = MockTransactionFactory.createSettlementRecordVisa_CPS_SmallTicket_Consumer();
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_SmallTicket,IC_Visa_CPS_Retail"));
    assertEquals("invalid small ticket fpi","179",rec.getString("fee_program_indicator"));
    assertEquals("invalid small ticket ra","A",rec.getString("reimbursement_attribute"));
    assertEquals("invalid small ticket rps","F",rec.getString("requested_payment_service"));
    
    // test that amounts over threshold go to CPS/Retail CR 
    rec.setData("transaction_amount","15.01");
    rec.setData("auth_amount","15.01");
    rec.setData("auth_amount_total","15.01");
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CPS_SmallTicket,IC_Visa_CPS_Retail"));
    assertEquals("invalid cps/retail fpi","106",rec.getString("fee_program_indicator"));
    assertEquals("invalid cps/retail ra","A",rec.getString("reimbursement_attribute"));
    assertEquals("invalid cps/retail rps","A",rec.getString("requested_payment_service"));
  }
  
  public void test_getBestClass_CreditVoucher_Consumer()
    throws Exception
  {
    Calendar                cal       = Calendar.getInstance();
    SettlementRecordVisa    rec       = null;
    InterchangeVisa         testClass = new InterchangeVisa();
  
    // test a standard retail account
    rec = MockTransactionFactory.createSettlementRecordVisa_CreditVoucher_Consumer();
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CreditVoucher"));
    assertEquals("invalid fpi","166",rec.getString("fee_program_indicator"));
    assertEquals("invalid ra","0",rec.getString("reimbursement_attribute"));
    assertEquals("invalid rps",null,rec.getString("requested_payment_service"));
    
    // test an account that does 70+ % moto or ecommerce
    rec.setData("moto_ecommerce_merchant","Y");  
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CreditVoucher"));
    assertEquals("invalid fpi","164",rec.getString("fee_program_indicator"));
    assertEquals("invalid ra","T",rec.getString("reimbursement_attribute"));
    assertEquals("invalid rps",null,rec.getString("requested_payment_service"));
  }
  
  public void test_getBestClass_CreditVoucher_Consumer_CheckCard()
    throws Exception
  {
    Calendar                cal       = Calendar.getInstance();
    SettlementRecordVisa    rec       = null;
    InterchangeVisa         testClass = new InterchangeVisa();
  
    // test a standard retail account
    rec = MockTransactionFactory.createSettlementRecordVisa_CreditVoucher_Consumer_CheckCard();
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CreditVoucher"));
    assertEquals("invalid fpi","198",rec.getString("fee_program_indicator"));
    assertEquals("invalid ra","0",rec.getString("reimbursement_attribute"));
    assertEquals("invalid rps",null,rec.getString("requested_payment_service"));
    
    // test an account that does 70+ % moto or ecommerce
    rec.setData("moto_ecommerce_merchant","Y");  
    assertTrue("getBestClass() returned false",testClass.getBestClass(rec,"IC_Visa_CreditVoucher"));
    assertEquals("invalid fpi","199",rec.getString("fee_program_indicator"));
    assertEquals("invalid ra","T",rec.getString("reimbursement_attribute"));
    assertEquals("invalid rps",null,rec.getString("requested_payment_service"));
  }
}