/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/test/com/mes/settlement/TestSettlementDb.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2009-12-11 09:46:45 -0800 (Fri, 11 Dec 2009) $
  Version            : $Revision: 16808 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import java.util.List;

import com.mes.test.MesTestCase;
import com.mes.test.MockHttpServletRequest;
import com.mes.tools.HierarchyTree;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesEncryption;
import com.mes.user.UserBean;

import junit.framework.*;

import masthead.formats.visak.*;

public class TestSettlementDb extends MesTestCase
{
  public void test_getBestIcInfo()
  {
    SettlementDb    db        = null;
    
    try
    {
      db = new SettlementDb();
      db.connect();

      fail("Failing due compilation error."); //FIXME: compilation error

      // set the central processing date to today
      Date cpd = new Date(Calendar.getInstance().getTime().getTime());
      
//      assertEquals("fpi should be 'JJJ'","JJJ-J",db._getBestIcInfo("VS",3,cpd,"JJJ-J","5999",1.00));
//
//      assertEquals("fpi should be '110-J'","110-J",db._getBestIcInfo("VS",3,cpd,"110-J","5999",1.00));
//
//      assertNull("fpi should be null",db._getBestIcInfo("VS",3,cpd,null,"5999",1.00));
//      assertEquals("fpi should be '216-XX'","216-XX",db._getBestIcInfo("VS",3,cpd,"216-XX,216-XX","5999",1.00));
//
//      // test CPS/Retail DB vs CPS/Retail 2 DB
//      assertEquals("fpi should be '210-XX'","210-XX",db._getBestIcInfo("VS",3,cpd,"193-XX,210-XX","5999",1.00));
//      assertEquals("fpi should be '210-XX'","210-XX",db._getBestIcInfo("VS",3,cpd,"193-XX,210-XX","5999",19.00));
//      assertEquals("fpi should be '210-XX'","210-XX",db._getBestIcInfo("VS",3,cpd,"193-XX,210-XX","5999",20.00));
//      assertEquals("fpi should be '210-XX'","210-XX",db._getBestIcInfo("VS",3,cpd,"193-XX,210-XX","5999",41.00));
//      assertEquals("fpi should be '210-XX'","210-XX",db._getBestIcInfo("VS",3,cpd,"193-XX,210-XX","5999",43.47));
//
//      assertEquals("fpi should be '193-XX'","193-XX",db._getBestIcInfo("VS",3,cpd,"193-XX,210-XX","5999",43.48));
//      assertEquals("fpi should be '193-XX'","193-XX",db._getBestIcInfo("VS",3,cpd,"193-XX,210-XX","5999",83.00));
//      assertEquals("fpi should be '193-XX'","193-XX",db._getBestIcInfo("VS",3,cpd,"193-XX,210-XX","5999",130.00));
//      assertEquals("fpi should be '193-XX'","193-XX",db._getBestIcInfo("VS",3,cpd,"193-XX,210-XX","5999",222.00));
//
      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      DbConnection.logEntry("test_getBestIcInfo()",e.toString());
      e.printStackTrace();
      fail("exception - " + e.toString());
    }
    finally
    {
      try{ db.cleanUp(); }catch(Exception ee){}
    }
  }
  
/*
  public void test_getBestIcCodeVisa()
  {
    SettlementDb      db        = null;
    
    try
    {
      db = new SettlementDb();
      db.connect();
      
      // set the central processing date to today
      Date cpd = new Date(Calendar.getInstance().getTime().getTime());
      
      assertNull("ic code should be null",db._getBestIcCodeVisa(cpd,null,"5999",1.00));
      assertEquals("ic code should be '455'","455",db._getBestIcCodeVisa(cpd,"216-XX,216-XX","5999",1.00));
      assertEquals("ic code should be '455' (111.42)","455",db._getBestIcCodeVisa(cpd,"216-XX,216-XX","5999",111.42));
      assertEquals("ic code should be '561' (111.43)","561",db._getBestIcCodeVisa(cpd,"216-XX,216-XX","5999",111.43));
      
      // test CPS/Retail DB vs CPS/Retail 2 DB
      assertEquals("ic code should be '381'","381",db._getBestIcCodeVisa(cpd,"193-XX,210-XX","5999",1.00));
      assertEquals("ic code should be '381'","381",db._getBestIcCodeVisa(cpd,"193-XX,210-XX","5999",19.00));
      assertEquals("ic code should be '381'","381",db._getBestIcCodeVisa(cpd,"193-XX,210-XX","5999",20.00));
      assertEquals("ic code should be '381'","381",db._getBestIcCodeVisa(cpd,"193-XX,210-XX","5999",41.00));
      assertEquals("ic code should be '381'","381",db._getBestIcCodeVisa(cpd,"193-XX,210-XX","5999",43.47));
      
      assertEquals("ic code should be '411'","411",db._getBestIcCodeVisa(cpd,"193-XX,210-XX","5999",43.48));
      assertEquals("ic code should be '411'","411",db._getBestIcCodeVisa(cpd,"193-XX,210-XX","5999",83.00));
      assertEquals("ic code should be '411'","411",db._getBestIcCodeVisa(cpd,"193-XX,210-XX","5999",130.00));
      assertEquals("ic code should be '411'","411",db._getBestIcCodeVisa(cpd,"193-XX,210-XX","5999",222.00));
      
      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      DbConnection.logEntry("test_getBestVisaFPI()",e.toString());
      e.printStackTrace();
      fail("exception - " + e.toString());
    }
    finally
    {
      try{ db.cleanUp(); }catch(Exception ee){}
    }
*/
  
  public void test_getFieldValueDesc()
  {
    assertEquals("invalid fpi desc",
                 "CPS/Retail Debit",
                 SettlementDb.getFieldValueDesc("fee_program_indicator","210"));
                 
    assertEquals("invalid pos entry mode desc",
                 "SWIPED CVV",
                 SettlementDb.getFieldValueDesc("pos_entry_mode","90"));
                 
    assertNull("desc should be null",
               SettlementDb.getFieldValueDesc("bad-field-name","random-value"));
  }
  
  public void test_getFileBankNumber()
  {
    SettlementDb      db        = null;
    String[]          prefixes  = { "256m","mddf","bad-prefix" };
    
    
    try
    {
      db = new SettlementDb();
      db.connect();
      
      int     bankNumber  = 0;
      String  filename    = null;
      
      for ( int i = 0; i < prefixes.length; ++i )
      {
        filename = prefixes[i] + "3941_043009_901.dat";
        bankNumber = db._getFileBankNumber(filename);
        
        if ( "bad-prefix".equals(prefixes[i]) )
        {
          assertEquals("bank number",0,bankNumber);
        }
        else  // good prefix
        {
          assertEquals("bank number",3941,bankNumber);
        }
      }
      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      DbConnection.logEntry("test_getFileBankNumber()",e.toString());
      e.printStackTrace();
      fail("exception - " + e.toString());
    }
    finally
    {
      try{ db.cleanUp(); }catch(Exception ee){}
    }
  }
  
  public void test_getNonSettlementDayCount()
  {
    SettlementDb      db        = null;

    fail("Failing due compilation error.");  //FIXME: compilation error

//    try
//    {
//      db = new SettlementDb();
//      db.connect();
//
//      Calendar  cal         = Calendar.getInstance();
//      int       currentYear = cal.get(Calendar.YEAR);
//      Date      beginDate   = null;
//      Date      endDate     = null;
//
//      cal.set(Calendar.MONTH,Calendar.JANUARY);
//      cal.set(Calendar.DAY_OF_MONTH,1);
//
//      beginDate = new Date(cal.getTime().getTime());
//
//      // advance the calendar to the first Sunday
//      while( cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY )
//      {
//        cal.add(Calendar.DAY_OF_MONTH,1);
//      }
//
//      // test the Sundays for the current year and following year
//      for ( ; cal.get(Calendar.YEAR) <= (currentYear+1); cal.add(Calendar.DAY_OF_MONTH,7) )
//      {
//        beginDate.setTime(cal.getTime().getTime());
//        assertEquals("sunday",1,db._getNonSettlementDayCount("VS",beginDate,beginDate));
//      }
//
//      // test the holidays for the current year and following year
//      for( int year = currentYear; year <= (currentYear+1); ++year )
//      {
//        beginDate.setTime( DateTimeFormatter.parseDate("01/01/"+String.valueOf(year),"MM/dd/yyyy").getTime() );
//        assertEquals( "new years", 1, db._getNonSettlementDayCount("VS",beginDate,beginDate));     // January 1st
//
//        beginDate.setTime( DateTimeFormatter.getNthWeekdayInMonthYear(year,Calendar.JANUARY,Calendar.MONDAY,3).getTime() );
//        assertEquals( "mlk day", 1, db._getNonSettlementDayCount("VS",beginDate,beginDate));       // 3rd Monday of January
//
//        beginDate.setTime( DateTimeFormatter.getNthWeekdayInMonthYear(year,Calendar.FEBRUARY,Calendar.MONDAY,3).getTime() );
//        assertEquals( "presidents day", 1, db._getNonSettlementDayCount("VS",beginDate,beginDate));// 3rd Monday of February
//
//        beginDate.setTime( DateTimeFormatter.getGoodFridayInYear(year).getTime() );
//        assertEquals( "good friday", 1, db._getNonSettlementDayCount("VS",beginDate,beginDate));   // Friday before Easter
//
//        beginDate.setTime( DateTimeFormatter.getLastWeekdayInMonthYear(year,Calendar.MAY,Calendar.MONDAY).getTime() );
//        assertEquals( "memorial day", 1, db._getNonSettlementDayCount("VS",beginDate,beginDate));  // last Monday of May
//
//        beginDate.setTime( DateTimeFormatter.parseDate("07/04/"+String.valueOf(year),"MM/dd/yyyy").getTime() );
//        assertEquals( "4th of july", 1, db._getNonSettlementDayCount("VS",beginDate,beginDate));   // July 4th
//
//        beginDate.setTime( DateTimeFormatter.getNthWeekdayInMonthYear(year,Calendar.SEPTEMBER,Calendar.MONDAY,1).getTime() );
//        assertEquals( "labor day", 1, db._getNonSettlementDayCount("VS",beginDate,beginDate));     // 1st Monday of September
//
//        beginDate.setTime( DateTimeFormatter.getNthWeekdayInMonthYear(year,Calendar.OCTOBER,Calendar.MONDAY,2).getTime() );
//        assertEquals( "columbus day", 1, db._getNonSettlementDayCount("VS",beginDate,beginDate));  // 2nd Monday of October
//
//        beginDate.setTime( DateTimeFormatter.parseDate("11/11/"+String.valueOf(year),"MM/dd/yyyy").getTime() );
//        assertEquals( "veterans day", 1, db._getNonSettlementDayCount("VS",beginDate,beginDate));  // November 11th
//
//        beginDate.setTime( DateTimeFormatter.getNthWeekdayInMonthYear(year,Calendar.NOVEMBER,Calendar.THURSDAY,4).getTime() );
//        assertEquals( "thanksgiving", 1, db._getNonSettlementDayCount("VS",beginDate,beginDate));  // 4th Thursday of November
//
//        beginDate.setTime( DateTimeFormatter.parseDate("12/25/"+String.valueOf(year),"MM/dd/yyyy").getTime() );
//        assertEquals( "christmas", 1, db._getNonSettlementDayCount("VS",beginDate,beginDate));     // December 25th
//      }
      
//      assertEquals("java log",0,getLogEntryCount());
//    }
//    catch( Exception e )
//    {
//      DbConnection.logEntry("test_getNonSettlementDayCount()",e.toString());
//      e.printStackTrace();
//      fail("exception - " + e.toString());
//    }
//    finally
//    {
//      try{ db.cleanUp(); }catch(Exception ee){}
//    }
  }
  
  public void test_loadFilenameToLoadFileId()
  {
    SettlementDb      db        = null;
    
    try
    {
      db = new SettlementDb();
      db.connect();
      
      assertEquals("load file id",4941154L,db._loadFilenameToLoadFileId("256m3941_043009_901.dat"));
      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      DbConnection.logEntry("test_loadFilenameToLoadFileId()",e.toString());
      e.printStackTrace();
      fail("exception - " + e.toString());
    }
    finally
    {
      try{ db.cleanUp(); }catch(Exception ee){}
    }
  }
  
  public void test_loadMotoMerchantFlag()
  {
    SettlementDb      db        = null;
    
    try
    {
      db = new SettlementDb();
      db.connect();
      fail("Failing due compilation error."); //FIXME: compilation error
//      assertEquals("should be moto","Y",db._loadMotoEcommerceMerchantFlag(941000000027L));
//      assertEquals("should not be moto","N",db._loadMotoEcommerceMerchantFlag(941000000002L));
//      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      DbConnection.logEntry("test_loadMotoMerchantFlag()",e.toString());
      e.printStackTrace();
      fail("exception - " + e.toString());
    }
    finally
    {
      try{ db.cleanUp(); }catch(Exception ee){}
    }      
  }
  
  public void test_loadTridentAuthData()
    throws Exception
  {
    SettlementDb      db        = null;
    
    try
    {
      db = new SettlementDb();
      db.connect();
      fail("Failing due compilation error."); //FIXME: compilation error
//      TridentAuthData authData = db.loadTridentAuthData(198010507L,0);
//      assertNotNull("authData is null",authData);
//      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      DbConnection.logEntry("test_loadTridentAuthData()",e.toString());
      e.printStackTrace();
      fail("exception - " + e.toString());
    }
    finally
    {
      try{ db.cleanUp(); }catch(Exception ee){}
    }      
  }
  
  public void test_loadVisakBatchData()
    throws Exception
  {
    SettlementDb      db        = null;
    
    try
    {
      db = new SettlementDb();
      db.connect();

      fail("Failing due compilation error."); //FIXME: compilation error
//      VisakBatchData batchData = db._loadVisakBatchData(198010507L);
//      List detailRecs = batchData.getDetailRecords();
//
//      assertNotNull("batchData is null",batchData);
//      assertNotNull("detailRecs list is null",detailRecs);
//      assertEquals("detail rec count",8,detailRecs.size());
//      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      DbConnection.logEntry("test_loadVisakBatchData()",e.toString());
      e.printStackTrace();
      fail("exception - " + e.toString());
    }
    finally
    {
      try{ db.cleanUp(); }catch(Exception ee){}
    }      
  }
  
  public void test_loadVisakBatchDetail()
    throws Exception
  {
    SettlementDb      db        = null;
    
    try
    {
      db = new SettlementDb();
      db.connect();

      fail("Failing due compilation error.");  //FIXME: compilation error
//      Batch batch = db._loadVisakBatchDetail(198026961L); //14421139L
//
//      assertNotNull("batch is null",batch);
//      assertEquals("batch amount",3.88,batch.getBatchNetDeposit(),0.001);
//      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      DbConnection.logEntry("test_loadVisakBatchDetail()",e.toString());
      e.printStackTrace();
      fail("exception - " + e.toString());
    }
    finally
    {
      try{ db.cleanUp(); }catch(Exception ee){}
    }      
  }
  
  public void test_loadVisakBatchesToExtract()
  {
    SettlementDb      db        = null;
    
    try
    {
      db = new SettlementDb();
      db.connect();
      fail("Failing due compilation error."); //FIXME: compilation error
//      Vector batchList = db._loadVisakBatchesToExtract(3941,"256m3941_043009_901.dat");
//
//      assertNotNull("batchList is null",batchList);
//      assertEquals("batch count",2,batchList.size());
//      assertEquals("batch rec id",198010507L,((Long)batchList.elementAt(0)).longValue());
//      assertEquals("batch rec id",198026961L,((Long)batchList.elementAt(1)).longValue());
//      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      DbConnection.logEntry("test_loadVisakBatchesToExtract()",e.toString());
      e.printStackTrace();
      fail("exception - " + e.toString());
    }
    finally
    {
      try{ db.cleanUp(); }catch(Exception ee){}
    }      
  }
  
  public void test_loadVisaSettlementRecord()
  {
    SettlementDb      db        = null;
    long              batchId   = 204501248L;
    int               recIdx    = 1;
    
    try
    {
      db = new SettlementDb();
      db.connect();
      fail("Failing due compilation error."); //FIXME: compilation error
//      long              recId   = db._getSettlementRecId("VS",batchId,recIdx);
//      SettlementRecord  rec     = db._loadSettlementRecord("VS",recId);
//
//      assertNotNull("rec is null",rec);
//      assertTrue("invalid record type", (rec instanceof SettlementRecordVisa));
//      assertEquals("bad data - card number", "450239xxxxxx1111", rec.getString("card_number"));
//      assertEquals("bad data - acq ref num", "133689951", rec.getString("acq_reference_number"));
//      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      DbConnection.logEntry("test_loadVisakSettlementRecord()",e.toString());
      e.printStackTrace();
      fail("exception - " + e.toString());
    }
    finally
    {
      try{ db.cleanUp(); }catch(Exception ee){}
    }      
  }
  
  public void test_loadPGSettlementRecord()
  {
    SettlementDb      db        = null;
    long              recId     = 10424460L;
    
    try
    {
      db = new SettlementDb();
      db.connect();
      fail("Failing due compilation error."); //FIXME: compilation error
//      SettlementRecord rec = db._loadPGSettlementRecord(recId);
//
//      assertNotNull("rec is null",rec);
//      assertTrue("invalid record type", (rec instanceof SettlementRecordVisa));
//      assertEquals("bad data - card number", "414709xxxxxx1550", rec.getString("card_number"));
//      assertEquals("bad data - acq ref num", "131719248", rec.getString("acq_reference_number"));
//      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      DbConnection.logEntry("test_loadPGSettlementRecord()",e.toString());
      e.printStackTrace();
      fail("exception - " + e.toString());
    }
    finally
    {
      try{ db.cleanUp(); }catch(Exception ee){}
    }      
  }
  
  public void test_loadVisakSettlementRecord()
  {
    SettlementDb      db        = null;
    long              batchId   = 204501248L;
    int               recIdx    = 1;
    
    try
    {
      db = new SettlementDb();
      db.connect();
      fail("Failing due compilation error."); //FIXME: compilation error
//      SettlementRecord rec = db._loadVisakSettlementRecord(batchId,recIdx);
//
//      assertNotNull("rec is null",rec);
//      assertTrue("invalid record type", (rec instanceof SettlementRecordVisa));
//      assertEquals("bad data - card number", "450239xxxxxx1111", rec.getString("card_number"));
//      assertEquals("bad data - acq ref num", "133689951", rec.getString("acq_reference_number"));
//      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      DbConnection.logEntry("test_loadVisakSettlementRecord()",e.toString());
      e.printStackTrace();
      fail("exception - " + e.toString());
    }
    finally
    {
      try{ db.cleanUp(); }catch(Exception ee){}
    }      
  }
  
  
  public void test_settlementRecordExists()
  {
    SettlementDb      db        = null;
    long              batchId   = 204501248L;
    int               recIdx    = 1;
    
    try
    {
      db = new SettlementDb();
      db.connect();
      fail("Failing due compilation error."); //FIXME: compilation error
//      assertTrue("visa should exist", db._settlementRecordExists("VS",batchId,recIdx));
//      assertFalse("mc should not exist", db._settlementRecordExists("MC",batchId,recIdx));
////@      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      DbConnection.logEntry("test_settlementRecordExists()",e.toString());
      e.printStackTrace();
      fail("exception - " + e.toString());
    }
    finally
    {
      try{ db.cleanUp(); }catch(Exception ee){}
    }      
  }
  
  public void test_getSettlementRecId()
  {
    SettlementDb      db        = null;
    long              batchId   = 204501248L;
    int               recIdx    = 1;
    
    try
    {
      db = new SettlementDb();
      db.connect();
      fail("Failing due compilation error."); //FIXME: compilation error
//      assertTrue("visa should exist", (db._getSettlementRecId("VS",batchId,recIdx) > 0L) );
//      assertTrue("mc should not exist", (db._getSettlementRecId("MC",batchId,recIdx) == 0L) );
////@      assertEquals("java log",0,getLogEntryCount());
    }
    catch( Exception e )
    {
      DbConnection.logEntry("test_getSettlementRecId()",e.toString());
      e.printStackTrace();
      fail("exception - " + e.toString());
    }
    {
      try{ db.cleanUp(); }catch(Exception ee){}
    }      
  }
}