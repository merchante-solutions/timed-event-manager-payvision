/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/test/com/mes/support/TestNumberFormatter.java $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2009-01-09 15:22:09 -0800 (Fri, 09 Jan 2009) $
  Version            : $Revision: 15699 $

  Change History:
     See VSS database

  Copyright (C) 2007-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import com.mes.test.MesTestCase;

public class TestNumberFormatter extends MesTestCase
{
  public void test_getDoubleString()
  {
    String format = "$###,###,##0.00;($###,###,##0.00)";
    
    assertEquals("dbl fmt","$0.00",NumberFormatter.getDoubleString(0.0, format));
    assertEquals("dbl lrg","$579,203.31",NumberFormatter.getDoubleString(579203.31, format));
    assertEquals("dbl neg","($32.80)",NumberFormatter.getDoubleString(-32.798, format));
  }
  
  public void test_getLongString()
  {
    String format = "###,###,##0;(###,###,##0)";
    
    assertEquals("int fmt","57",NumberFormatter.getLongString(57L, format));
    assertEquals("int lrg","579,203",NumberFormatter.getLongString(579203L, format));
    assertEquals("int neg","(32)",NumberFormatter.getLongString(-32L, format));
    assertEquals("int lrg neg","(1,234)",NumberFormatter.getLongString(-1234L, format));
  }

  public void test_getPaddedInt()
  {
    assertEquals("pad int","000517",NumberFormatter.getPaddedInt(517,6));
    assertEquals("pad int","1234567",NumberFormatter.getPaddedInt(1234567,6));
  }
  
  public void test_getPercentString()
  {
    assertEquals("percent a","34.57%",NumberFormatter.getPercentString(0.3457));
    assertEquals("percent b","34.58%",NumberFormatter.getPercentString(0.34578));
    assertEquals("percent c","34.57%",NumberFormatter.getPercentString(0.34573));
    assertEquals("percent d","11.3%",NumberFormatter.getPercentString(0.113,2));
    assertEquals("percent round up","15.76%",NumberFormatter.getPercentString(0.15756,2));
    assertEquals("percent round down","15.75%",NumberFormatter.getPercentString(0.15755,2));
    assertEquals("percent 3-digits","21.375%",NumberFormatter.getPercentString(0.21375,3));
  }
}
