/*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/test/com/mes/support/TestDateTimeFormatter.java $

  Description:  


  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2009-05-14 17:40:07 -0700 (Thu, 14 May 2009) $
  Version            : $Revision: 16129 $

  Change History:
     See SVN database

  Copyright (C) 2007-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.util.Calendar;
import java.util.Date;

import com.mes.test.MesTestCase;

public class TestDateTimeFormatter extends MesTestCase
{
  public void test_parseDate()
  {
    Calendar      cal         = Calendar.getInstance();
    
    // partial year
    cal.clear();
    cal.set(Calendar.MONTH,Calendar.AUGUST);
    cal.set(Calendar.DAY_OF_MONTH,27);
    cal.set(Calendar.YEAR,2008);
    assertEquals("Date format failed 082708",
                  cal.getTime(),
                  DateTimeFormatter.parseDate("082708","MMddyy"));
                  
    // full year                  
    cal.clear();
    cal.set(Calendar.MONTH,Calendar.OCTOBER);
    cal.set(Calendar.DAY_OF_MONTH,1);
    cal.set(Calendar.YEAR,2008);
    assertEquals("Date format failed 10012008",
                  cal.getTime(),
                  DateTimeFormatter.parseDate("10012008","MMddyyyy"));
                  
    // with slash separators
    cal.clear();
    cal.set(Calendar.MONTH,Calendar.JANUARY);
    cal.set(Calendar.DAY_OF_MONTH,5);
    cal.set(Calendar.YEAR,2008);
    assertEquals("Date format failed 1/5/2008",
                  cal.getTime(),
                  DateTimeFormatter.parseDate("1/5/2008","MM/dd/yyyy"));
                  
    // with dash separators, year first
    cal.clear();
    cal.set(Calendar.MONTH,Calendar.FEBRUARY);
    cal.set(Calendar.DAY_OF_MONTH,3);
    cal.set(Calendar.YEAR,2008);
    assertEquals("Date format failed 2008-02-03",
                  cal.getTime(),
                  DateTimeFormatter.parseDate("2008-02-03","yyyy-MM-dd"));
                
    // invalid date value, lenient should return a date
    assertNotNull("Lenient date format failed 000000",
                   DateTimeFormatter.parseDate("000000","MMddyy"));
                   
    // invalid date value, lenient set to false should return null
    assertNull("Strict date format failed 000000",
                DateTimeFormatter.parseDate("000000","MMddyy",false));
  }
  
  public void test_getEasterSundayInYear()
  {
    Calendar  cal   = Calendar.getInstance();
  
    Date[]  validValues =
    {
      DateTimeFormatter.parseDate("04/12/2009","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/04/2010","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/24/2011","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/08/2012","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("03/31/2013","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/20/2014","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/05/2015","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("03/27/2016","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/16/2017","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/01/2018","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/21/2019","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/12/2020","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/04/2021","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/17/2022","MM/dd/yyyy"),
    };
    
    for( int year = 2009; year < 2022; ++year )
    {
      cal.setTime( DateTimeFormatter.getEasterSundayInYear(year) );
      assertEquals("not a sunday " + year,Calendar.SUNDAY,cal.get(Calendar.DAY_OF_WEEK));
      assertEquals("bad easter " + year,validValues[year-2009],cal.getTime());
    }
  }
  
  public void test_getGoodFridayInYear()
  {
    Calendar  cal   = Calendar.getInstance();
    
    Date[]  validValues =
    {
      DateTimeFormatter.parseDate("04/10/2009","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/02/2010","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/22/2011","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/06/2012","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("03/29/2013","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/18/2014","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/03/2015","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("03/25/2016","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/14/2017","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("03/30/2018","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/19/2019","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/10/2020","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/02/2021","MM/dd/yyyy"),
      DateTimeFormatter.parseDate("04/15/2022","MM/dd/yyyy"),
    };
    
    for( int year = 2009; year < 2022; ++year )
    {
      cal.setTime( DateTimeFormatter.getEasterSundayInYear(year) );
      cal.add(Calendar.DAY_OF_MONTH,-2);
      assertEquals("not a friday " + year,Calendar.FRIDAY,cal.get(Calendar.DAY_OF_WEEK));
      assertEquals("bad good friday " + year,validValues[year-2009],cal.getTime());
    }
  }
  
  public void test_getNthWeekdayInMonthYear()
  {                   
    Calendar  cal           = Calendar.getInstance();
    int       count         = 0;
    int       currentYear   = cal.get(Calendar.YEAR);
    
    int       month         = -1;   // month to test
    int       dow           = -1;   // day of week
    int       num           = -1;   // occurrence number
    
    for ( int i = 0; i < 4; ++i )
    {
      switch(i)
      {
        case 0:   // MLK day, 3rd monday in january
          month = Calendar.JANUARY;
          dow   = Calendar.MONDAY;
          num   = 3;
          break;
          
        case 1:   // presidents day, 3rd monday in february
          month = Calendar.FEBRUARY;
          dow   = Calendar.MONDAY;
          num   = 3;
          break;
          
        case 2:   // columbus day, 2nd monday in october
          month = Calendar.OCTOBER;
          dow   = Calendar.MONDAY;
          num   = 2;
          break;
          
        case 3:   // thanksgiving, 4th thursday in november
          month = Calendar.NOVEMBER;
          dow   = Calendar.THURSDAY;
          num   = 4;
          break;
          
        default:
          continue;
      }
      
      cal.clear();
      cal.set(Calendar.MONTH,month);
      cal.set(Calendar.DAY_OF_MONTH,1);
      cal.set(Calendar.YEAR,currentYear);
      
      count = 0;
      while(true)
      {
        if ( cal.get(Calendar.DAY_OF_WEEK) == dow )
        {
          if ( ++count >= num )
          {
            break;
          }
        }
        cal.add(Calendar.DAY_OF_MONTH,1);
      }
      assertEquals("bad test #" + i,cal.getTime(),DateTimeFormatter.getNthWeekdayInMonthYear(currentYear,month,dow,num));
    }
  }
  
  public void test_getLastWeekdayInMonthYear()
  {
    Calendar  cal           = Calendar.getInstance();
    int       currentYear   = cal.get(Calendar.YEAR);
    
    for( int year = currentYear; year <= (currentYear+1); ++year )
    {
      for( int month = Calendar.JANUARY; month < Calendar.DECEMBER; ++month )
      {
        for ( int dow = Calendar.SUNDAY; dow < Calendar.SATURDAY; ++dow )
        {
          cal.setTime(DateTimeFormatter.getLastWeekdayInMonthYear(year,month,dow));
          assertEquals("bad month " + month, month, cal.get(Calendar.MONTH));
          assertEquals("bad day " + dow, dow, cal.get(Calendar.DAY_OF_WEEK));
          
          cal.add(Calendar.DAY_OF_MONTH,1);         // advance one day
          while( cal.get(Calendar.MONTH) == month ) // continue until month changes
          {
            if ( cal.get(Calendar.DAY_OF_WEEK) == dow )
            {
              fail("not the last of the month (" + dow + ")");
            }
            cal.add(Calendar.DAY_OF_MONTH,1);
          }
        }          
      }
    }
  }
}
