/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/test/com/mes/support/TestTridentTools.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $LastChangedDate: 2009-12-11 09:46:45 -0800 (Fri, 11 Dec 2009) $
  Version            : $Revision: 16808 $

  Change History:
     See SVN database

  Copyright (C) 2007-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.support;

import java.util.Calendar;
import java.util.Date;

import com.mes.test.MesTestCase;

public class TestTridentTools extends MesTestCase
{
  private String[]  TestCardNumbers = 
  {
    "4262360090326102",
    "4798040022209682",
    "4715532000128603",
    "5314100052584230",
    "5567112000032968",
    "5569540000200201",
    "341111597242000",
    "6011000992186639",
    "3566007770013931",
  };

  private String[]  TestReferenceNumbers  = 
  {
    "24332399015900910100014",
    "24332399015900910100022",
    "24332399015900910100048",
    "24332399015900910100055",
    "24332399015900910100071",
    "24332399015900910100113",
    "24332399015900910100147",
    "24332399015900910100154",
    "24332399015900910100170",
    "24332399015900910100196",
    "24332399015900910100238",
    "24332399015900910100287",
    "24332399015900910100329",
    "24332399015900910100345",
    "24332399015900910100402",
    "24332399015900910100410",
    "24332399015900910100485",
    "24332399015900910100501",
    "24332399015900910100519",
    "24332399015900910100527",
    "24332399015900910100535",
    "24332399015900910100543",
    "24332399015900910100550",
    "24332399015900910100568",
    "24332399015900910100576",
    "24332399015900910100584",
    "24332399015900910100592",
    "24332399015900910100634",
  };

  public void test_calcMod10CheckDigit()
  {
    String      testVal   = null;
      
    for( int i = 0; i < TestReferenceNumbers.length; ++i )
    {
      testVal = TestReferenceNumbers[i];
      int correctVal = Character.digit(testVal.charAt(testVal.length()-1),10);
      int checkDigit = TridentTools.calcMod10CheckDigit(testVal.substring(0,testVal.length()-1));
      assertEquals("bad ref calc (" + i + ")",correctVal,checkDigit);
    }        
    
    for( int i = 0; i < TestCardNumbers.length; ++i )
    {
      testVal = TestCardNumbers[i];
      int correctVal = Character.digit(testVal.charAt(testVal.length()-1),10);
      int checkDigit = TridentTools.calcMod10CheckDigit(testVal.substring(0,testVal.length()-1));
      assertEquals("bad card calc (" + i + ")",correctVal,checkDigit);
    }        
  }

  public void test_decodeVisakLevel3OrderDate()
  {
    String[]      formattedValues = 
    {
      "082708",
      "100108",
      null,
      null,
      null,
    };
  
    String[]      testValues = 
    {
      "080827",
      "081001",
      "000000",
      "999999",
      null,
    };

    fail("Failing due compilation error."); //FIXME: compilation error
//    for( int i = 0; i < testValues.length; ++i )
//    {
//      assertEquals("Level 3 order date format failed " + testValues[i],
//                    formattedValues[i],
//                    TridentTools.decodeVisakLevel3OrderDate(testValues[i]));
//    }
  }
  
  public void test_decodeVisakTranDate()
  {
    String[]      testValues  = 
    {
      "1231",
      "0101",
      "0605",
      "0910",
      "1101",
    };
    
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DAY_OF_MONTH,1);
    Date yesterday = cal.getTime();
    
    for( int i = 0; i < testValues.length; ++i )
    {
      Date testDate = DateTimeFormatter.parseDate(TridentTools.decodeVisakTranDate(testValues[i]),"MMddyy");
      assertTrue("Tran Date Format failed " + testValues[i],
                  testDate.before(yesterday));
    }
  }
  
  public void test_decodeVisakTranDateAsDate()
  {
    String[]      testValues  = 
    {
      "1231",
      "0101",
      "0605",
      "0910",
      "1101",
    };
    
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DAY_OF_MONTH,1);
    Date yesterday = cal.getTime();
    
    for( int i = 0; i < testValues.length; ++i )
    {
      assertTrue("Tran Date Format failed " + testValues[i],
                  TridentTools.decodeVisakTranDateAsDate(testValues[i]).before(yesterday));
    }
  }

  public void test_getFormattedPhone()
  {
    String        formattedValue  = "541-3436110";
    String[]      testValues      = 
    {
      "541.3436110",
      "541.343.6110",
      "541-3436110",
      "541-343-6110",
      "5413436110",
    };
    
    for( int i = 0; i < testValues.length; ++i )
    {
      assertEquals("Format failed " + testValues[i],
                    formattedValue,
                    TridentTools.getFormattedPhone(testValues[i]));
    }
  }
  
  public void test_isValidMerchantPhone()
  {
    String[]      badValues  = 
    {
      "3436110",
      "(541)343-6110",
      "bad-value",
    };
  
    String[]      goodValues  = 
    {
      "541.3436110",
      "541.343.6110",
      "541-3436110",
      "541-343-6110",
      "5413436110",
    };
    
    for( int i = 0; i < goodValues.length; ++i )
    {
      assertTrue("Good value failed validation (" + goodValues[i] + ")", 
                 TridentTools.isValidMerchantPhone(goodValues[i]) );
    }
    
    for( int i = 0; i < badValues.length; ++i )
    {
      assertFalse("Base value passed validation (" + badValues[i] + ")", 
                  TridentTools.isValidMerchantPhone(badValues[i]) );
    }
  }
  
  public void test_mod10Check()
  {
    for( int i = 0; i < TestReferenceNumbers.length; ++i )
    {
      assertTrue("ref num mod10 failed (" + i + ")",TridentTools.mod10Check(TestReferenceNumbers[i]));
    }        
    
    for( int i = 0; i < TestCardNumbers.length; ++i )
    {
      assertTrue("card mod10 failed (" + i + ")",TridentTools.mod10Check(TestCardNumbers[i]));
    }        
  }
}
