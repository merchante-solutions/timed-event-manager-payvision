/*@lineinfo:filename=VisaFANFSummaryEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/VisaFANFSummaryEvent.sqlj $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-11-12 15:13:32 -0800 (Thu, 12 Nov 2015) $
  Version            : $Revision: 23995 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Vector;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import com.mes.support.ThreadPool;
import sqlj.runtime.ResultSetIterator;

public class VisaFANFSummaryEvent
  extends EventBase
{
  private static final int        TIER_COUNT        = 18;
  
    // sales types
  private static final int        ST_CP             = 0;  // card present
  private static final int        ST_CNP            = 1;  // card not present
  private static final int        ST_TOTAL          = 2;  // total
  private static final int        ST_COUNT          = 3;
  
  private static final String     FANF_TABLE_1a     = "1a";
  private static final String     FANF_TABLE_1b     = "1b";
  private static final String     FANF_TABLE_2      = "2";
  
  private static final String[] FanfTableIds =
  {
    FANF_TABLE_1a,
    FANF_TABLE_1b,
    FANF_TABLE_2
  };
  
  private static final int[][]    LocationTiers =
  {
    { 1     , 1         },
    { 2     , 2         },
    { 3     , 3         },
    { 4     , 4         },
    { 5     , 5         },
    { 6     , 10        },
    { 11    , 20        },
    { 21    , 50        },
    { 51    , 100       },
    { 101   , 150       },
    { 151   , 200       },
    { 201   , 250       },
    { 251   , 500       },
    { 501   , 1000      },
    { 1001  , 1500      },
    { 1501  , 2000      },
    { 2001  , 4000      },
    { 4001  , 999999999 },
    {-1     , -1        },
    { 0     , 0         },

  };
  
  private static final double[][]    VolumeTiers =
  {
    {         0.00  ,          199.99 },
    {       200.00  ,         1249.99 },
    {      1250.00  ,         3999.99 },
    {      4000.00  ,         7999.99 },
    {      8000.00  ,        39999.99 },
    {     40000.00  ,       199999.99 },
    {    200000.00  ,       799999.99 },
    {    800000.00  ,      1999999.99 },
    {   2000000.00  ,      3999999.99 },
    {   4000000.00  ,      7999999.99 },
    {   8000000.00  ,     19999999.99 },
    {  20000000.00  ,     39999999.99 },
    {  40000000.00  ,     79999999.99 },
    {  80000000.00  ,    400000000.99 },
    { 400000001.00  , 999999999999.99 },
  };
  
  public static class MerchantData 
  {
    public  int             BankNumber        = 0;
    public  long            MerchantId        = 0L;
    public  long            vsBin             = 0L;
    public  String          address           = "";
    public  String          city              = "";
    public  String          state             = "";
    public  int             cpTranCount       = 0;
    public  int             cnpTranCount      = 0;
    public  double[]        SalesAmounts      = new double[ST_COUNT];
    
    private static final String[] AmountFieldNames = new String[]
    {
      "cp_sales_amount",
      "cnp_sales_amount",
      "tot_sales_amount",
    };
    
    public MerchantData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      BankNumber   = resultSet.getInt ("bank_number");
      MerchantId   = resultSet.getLong("merchant_number");
      vsBin        = resultSet.getLong("bin");
      address      = resultSet.getString("address");
      city         = resultSet.getString("city");
      state        = resultSet.getString("state");
      cpTranCount  = resultSet.getInt("cp_tran_count");
      cnpTranCount  = resultSet.getInt("cnp_tran_count");
      
      for( int i = 0; i < ST_COUNT; ++i )
      {
        SalesAmounts[i] = resultSet.getDouble(AmountFieldNames[i]);
      }      
    }
  }
  
  public class SummaryThread 
    extends SQLJConnectionBase
    implements Runnable
  {
    private   Vector        MerchantVector  = new Vector();
    private   String        TaxId           = null;
    
    public SummaryThread( String taxId )
    {
      TaxId        = taxId;
    }
    
    public void run()
    {
      String                bankWhereClause   = null;
      double                cnpSales          = 0.0;
      double                cpSales           = 0.0;
      double                fastFoodSales     = 0.0;
      double                highVolSales      = 0.0;
      int                   locCount          = 0;
      double                totalSales        = 0.0;
      double                uatSales          = 0.0;
      ResultSetIterator     it                = null;
      ResultSet             resultSet         = null;
      
      try
      {
        connect(true);
        MerchantVector.removeAllElements();
        
        if( BankNumber == 3941 || BankNumber == 3943 )
        {
          bankWhereClause = " in (3941,3943)";
        }
        else
        {
          bankWhereClause = " = " + BankNumber;
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:184^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index(dt idx_ddf_dt_bdate_merch_acct) */
//                    mb.bank_number                                as bank_number,
//                    mf.merchant_number                            as merchant_number,
//                    mf.addr1_line_1                               as address,
//                    mf.city1_line_4                               as city,
//                    mf.state1_line_4                              as state,
//                    mb.vs_bin_base_ii                             as bin, 
//                    sum(case
//                          when mf.sic_code between 3000 and 3299 then 1
//                          when mf.sic_code between 3300 and 3499 then 1
//                          when mf.sic_code between 3500 and 3999 then 1
//                          when mf.sic_code in (7011,7512,4511,4411,4829,5200,
//                                               5300,5309,5310,5311,5411,5511,
//                                               5532,5541,5542,5651,5655,5712,
//                                               5732,5912,5943,7012,7832) then 1
//                          else 0
//                        end                       
//                        * decode(dt.mo_to_indicator,null,1,0)
//                        * dt.transaction_amount)                  as high_vol_cp_sales_amount,
//                    sum(decode(mf.sic_code,'5814',1,0)
//                        * decode(dt.mo_to_indicator,null,1,0)
//                        * dt.transaction_amount)                  as ff_sales_amount,
//                     sum(decode(mf.sic_code,'5542',0,1) 
//                        * decode( dt.cardholder_i_d_method,3,1,0)
//                        * dt.transaction_amount)                  as uat_amount,    
//                    sum(decode(dt.mo_to_indicator,null,1,0)
//                        * dt.transaction_amount)                  as cp_sales_amount,
//                    sum( decode(dt.mo_to_indicator,null,1,0) )    as cp_tran_count,     
//                    sum(decode(dt.mo_to_indicator,null,0,1)
//                        * dt.transaction_amount)                  as cnp_sales_amount,
//                    sum(decode(dt.mo_to_indicator,null,0,1))      as cnp_tran_count,     
//                        
//                    sum(dt.transaction_amount)                    as tot_sales_amount
//            from    mif                     mf,
//                    mbs_banks               mb,
//                    daily_detail_file_dt    dt
//            where   mf.bank_number :bankWhereClause
//                    and mf.sic_code not in ('6010','6011')
//                    and decode( lpad(nvl(mf.federal_tax_id,'0'),9,'0'),
//                                '000000000',('mid-' || mf.merchant_number),
//                                '111111111',('mid-' || mf.merchant_number),
//                                '123456789',('mid-' || mf.merchant_number),
//                                '999999999',('mid-' || mf.merchant_number),
//                                lpad(mf.federal_tax_id,9,'0') ) = :TaxId
//                    and mb.bank_number = :BankNumber
//                    and dt.merchant_account_number = mf.merchant_number
//                    and dt.batch_date between :ActiveDate and last_day(:ActiveDate)
//                    and substr(dt.card_type,1,1) = 'V'
//                    and dt.debit_credit_indicator = 'D'
//                    and substr(dt.reference_number,2,6) = mb.vs_bin_base_ii
//                    and dt.reject_reason is null
//            group by mb.bank_number,mf.merchant_number,mf.addr1_line_1,mf.city1_line_4, mf.state1_line_4, mb.vs_bin_base_ii                    
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  /*+ index(dt idx_ddf_dt_bdate_merch_acct) */\n                  mb.bank_number                                as bank_number,\n                  mf.merchant_number                            as merchant_number,\n                  mf.addr1_line_1                               as address,\n                  mf.city1_line_4                               as city,\n                  mf.state1_line_4                              as state,\n                  mb.vs_bin_base_ii                             as bin, \n                  sum(case\n                        when mf.sic_code between 3000 and 3299 then 1\n                        when mf.sic_code between 3300 and 3499 then 1\n                        when mf.sic_code between 3500 and 3999 then 1\n                        when mf.sic_code in (7011,7512,4511,4411,4829,5200,\n                                             5300,5309,5310,5311,5411,5511,\n                                             5532,5541,5542,5651,5655,5712,\n                                             5732,5912,5943,7012,7832) then 1\n                        else 0\n                      end                       \n                      * decode(dt.mo_to_indicator,null,1,0)\n                      * dt.transaction_amount)                  as high_vol_cp_sales_amount,\n                  sum(decode(mf.sic_code,'5814',1,0)\n                      * decode(dt.mo_to_indicator,null,1,0)\n                      * dt.transaction_amount)                  as ff_sales_amount,\n                   sum(decode(mf.sic_code,'5542',0,1) \n                      * decode( dt.cardholder_i_d_method,3,1,0)\n                      * dt.transaction_amount)                  as uat_amount,    \n                  sum(decode(dt.mo_to_indicator,null,1,0)\n                      * dt.transaction_amount)                  as cp_sales_amount,\n                  sum( decode(dt.mo_to_indicator,null,1,0) )    as cp_tran_count,     \n                  sum(decode(dt.mo_to_indicator,null,0,1)\n                      * dt.transaction_amount)                  as cnp_sales_amount,\n                  sum(decode(dt.mo_to_indicator,null,0,1))      as cnp_tran_count,     \n                      \n                  sum(dt.transaction_amount)                    as tot_sales_amount\n          from    mif                     mf,\n                  mbs_banks               mb,\n                  daily_detail_file_dt    dt\n          where   mf.bank_number  ");
   __sjT_sb.append(bankWhereClause);
   __sjT_sb.append(" \n                  and mf.sic_code not in ('6010','6011')\n                  and decode( lpad(nvl(mf.federal_tax_id,'0'),9,'0'),\n                              '000000000',('mid-' || mf.merchant_number),\n                              '111111111',('mid-' || mf.merchant_number),\n                              '123456789',('mid-' || mf.merchant_number),\n                              '999999999',('mid-' || mf.merchant_number),\n                              lpad(mf.federal_tax_id,9,'0') ) =  ? \n                  and mb.bank_number =  ? \n                  and dt.merchant_account_number = mf.merchant_number\n                  and dt.batch_date between  ?  and last_day( ? )\n                  and substr(dt.card_type,1,1) = 'V'\n                  and dt.debit_credit_indicator = 'D'\n                  and substr(dt.reference_number,2,6) = mb.vs_bin_base_ii\n                  and dt.reject_reason is null\n          group by mb.bank_number,mf.merchant_number,mf.addr1_line_1,mf.city1_line_4, mf.state1_line_4, mb.vs_bin_base_ii");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "0com.mes.startup.VisaFANFSummaryEvent:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,TaxId);
   __sJT_st.setInt(2,BankNumber);
   __sJT_st.setDate(3,ActiveDate);
   __sJT_st.setDate(4,ActiveDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:238^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          if ( resultSet.getDouble("tot_sales_amount") > 0.0 )
          {
            MerchantVector.addElement(new MerchantData(resultSet));
        
            fastFoodSales += resultSet.getDouble("ff_sales_amount");
            cpSales       += resultSet.getDouble("cp_sales_amount");
            cnpSales      += resultSet.getDouble("cnp_sales_amount");
            highVolSales  += resultSet.getDouble("high_vol_cp_sales_amount");
            totalSales    += resultSet.getDouble("tot_sales_amount");
            uatSales      += resultSet.getDouble("uat_amount");
            
            if ( (resultSet.getDouble("cp_sales_amount") + resultSet.getDouble("high_vol_cp_sales_amount")) > 0.0 )
            {
              locCount++;   // only accounts with cp volume are "locations"
            }
          }
        }
        resultSet.close();

        // if fast food sales is >= 50% of the card present sales, then place in table 2
        if ( (cpSales != 0.0) && ((fastFoodSales/cpSales) >= 0.50) )
        {
          storeFanfData( FANF_TABLE_2, locCount, totalSales, ST_TOTAL );
        }
        else  // split volume based on category
        {
          if ( (cpSales != 0.0) && ((highVolSales/cpSales) >= 0.50) )
          {
            storeFanfData( FANF_TABLE_1a, locCount, cpSales, ST_CP );
          }
          else if((cpSales != 0.0) && ((uatSales/cpSales) >= 0.50))
          {
            storeFanfData( FANF_TABLE_2 , locCount, totalSales, ST_TOTAL );
          }
          else
          {
            storeFanfData( FANF_TABLE_1b, locCount, cpSales, ST_CP );
          }
          storeFanfData( FANF_TABLE_2 , locCount, cnpSales, ST_CNP );
        }
      }
      catch( Exception e )
      {
        logEntry("run()",e.toString());
      }
      finally
      {
        try{ it.close(); } catch( Exception ee ) {}
        cleanUp();
        updateDisplay();
      }
    }
    
    protected void storeFanfData( String tableId, int locCount, double salesAmount, int salesType )
      throws java.sql.SQLException
    {
      SummaryData         data        = null;
      ResultSetIterator   it          = null;
      ResultSet           resultSet   = null;
    
      try
      {
        if ( salesAmount != 0.0 )
        {
          data = new SummaryData();
      
          if ( FANF_TABLE_2.equals(tableId) )
          {
            data.setCNPData(salesAmount);
          }
          else    // card present data
          {
            data.setCPData(locCount,salesAmount);
          }
        }
      
        if ( data != null )
        {
          for( int i = 0; i < MerchantVector.size(); ++i )
          {
            MerchantData  merchantData        = (MerchantData)MerchantVector.elementAt(i);
            double        merchantSalesAmount = merchantData.SalesAmounts[salesType];
            int tranCount = 0;
            if(tableId.equalsIgnoreCase("2"))
                tranCount = merchantData.cnpTranCount;
            else
                tranCount = merchantData.cpTranCount;
            if ( merchantSalesAmount > 0.0 )
            {
              /*@lineinfo:generated-code*//*@lineinfo:332^15*/

//  ************************************************************
//  #sql [Ctx] { insert into visa_fanf_summary
//                  (
//                    bank_number,
//                    tax_id,
//                    merchant_number,
//                    active_date,
//                    table_id,
//                    tier,
//                    location_count,
//                    sales_amount,
//                    total_amount,
//                    fee,
//                    address,
//                    city,
//                    state,
//                    vs_bin,
//                    transaction_count                  
//                  )
//                  select  :merchantData.BankNumber,
//                          :TaxId,
//                          :merchantData.MerchantId,
//                          :ActiveDate,
//                          :tableId,
//                          :data.Tier,
//                          :locCount,
//                          :merchantSalesAmount,
//                          decode(:tableId,'2',:data.SalesAmount,null),
//                          round(decode(:tableId,'2', decode(:data.Tier,2,fp.fee*(:merchantSalesAmount),fp.fee*(:merchantSalesAmount/:data.SalesAmount)) ,
//                                  decode (:data.Tier,20,fp.fee*(:merchantSalesAmount),fp.fee)),2),
//                          :merchantData.address,
//                          :merchantData.city,
//                          :merchantData.state,
//                          :merchantData.vsBin,
//                          :tranCount                       
//                  from    visa_fanf_pricing   fp
//                  where   fp.table_id = :tableId
//                          and fp.tier = :data.Tier
//                          and :ActiveDate between fp.valid_date_begin and fp.valid_date_end
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into visa_fanf_summary\n                (\n                  bank_number,\n                  tax_id,\n                  merchant_number,\n                  active_date,\n                  table_id,\n                  tier,\n                  location_count,\n                  sales_amount,\n                  total_amount,\n                  fee,\n                  address,\n                  city,\n                  state,\n                  vs_bin,\n                  transaction_count                  \n                )\n                select   :1  ,\n                         :2  ,\n                         :3  ,\n                         :4  ,\n                         :5  ,\n                         :6  ,\n                         :7  ,\n                         :8  ,\n                        decode( :9  ,'2', :10  ,null),\n                        round(decode( :11  ,'2', decode( :12  ,2,fp.fee*( :13  ),fp.fee*( :14  / :15  )) ,\n                                decode ( :16  ,20,fp.fee*( :17  ),fp.fee)),2),\n                         :18  ,\n                         :19  ,\n                         :20  ,\n                         :21  ,\n                         :22                         \n                from    visa_fanf_pricing   fp\n                where   fp.table_id =  :23  \n                        and fp.tier =  :24  \n                        and  :25   between fp.valid_date_begin and fp.valid_date_end";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.VisaFANFSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,merchantData.BankNumber);
   __sJT_st.setString(2,TaxId);
   __sJT_st.setLong(3,merchantData.MerchantId);
   __sJT_st.setDate(4,ActiveDate);
   __sJT_st.setString(5,tableId);
   __sJT_st.setInt(6,data.Tier);
   __sJT_st.setInt(7,locCount);
   __sJT_st.setDouble(8,merchantSalesAmount);
   __sJT_st.setString(9,tableId);
   __sJT_st.setDouble(10,data.SalesAmount);
   __sJT_st.setString(11,tableId);
   __sJT_st.setInt(12,data.Tier);
   __sJT_st.setDouble(13,merchantSalesAmount);
   __sJT_st.setDouble(14,merchantSalesAmount);
   __sJT_st.setDouble(15,data.SalesAmount);
   __sJT_st.setInt(16,data.Tier);
   __sJT_st.setDouble(17,merchantSalesAmount);
   __sJT_st.setString(18,merchantData.address);
   __sJT_st.setString(19,merchantData.city);
   __sJT_st.setString(20,merchantData.state);
   __sJT_st.setLong(21,merchantData.vsBin);
   __sJT_st.setInt(22,tranCount);
   __sJT_st.setString(23,tableId);
   __sJT_st.setInt(24,data.Tier);
   __sJT_st.setDate(25,ActiveDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:372^15*/
            }
          }
        
          // correct any rounding errors when allocating for table 2
          // by adjusting the account with the largest allocation
          if ( FANF_TABLE_2.equals(tableId) && data.Tier != 2)
          {
            double    totalFee  = 0.0;
            double    variance  = 0.0;
            
            /*@lineinfo:generated-code*//*@lineinfo:383^13*/

//  ************************************************************
//  #sql [Ctx] { select  fp.fee
//                
//                from    visa_fanf_pricing   fp
//                where   fp.table_id = :FANF_TABLE_2
//                        and fp.tier = :data.Tier
//                        and :ActiveDate between fp.valid_date_begin and fp.valid_date_end
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  fp.fee\n               \n              from    visa_fanf_pricing   fp\n              where   fp.table_id =  :1  \n                      and fp.tier =  :2  \n                      and  :3   between fp.valid_date_begin and fp.valid_date_end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.VisaFANFSummaryEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,FANF_TABLE_2);
   __sJT_st.setInt(2,data.Tier);
   __sJT_st.setDate(3,ActiveDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   totalFee = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:391^13*/
            
            /*@lineinfo:generated-code*//*@lineinfo:393^13*/

//  ************************************************************
//  #sql [Ctx] { select  sum(fanf.fee)-:totalFee
//                
//                from    visa_fanf_summary   fanf
//                where   fanf.tax_id = :TaxId
//                        and fanf.bank_number = :BankNumber
//                        and fanf.active_date = :ActiveDate
//                        and fanf.table_id = :FANF_TABLE_2  -- '2'
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum(fanf.fee)- :1  \n               \n              from    visa_fanf_summary   fanf\n              where   fanf.tax_id =  :2  \n                      and fanf.bank_number =  :3  \n                      and fanf.active_date =  :4  \n                      and fanf.table_id =  :5    -- '2'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.VisaFANFSummaryEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDouble(1,totalFee);
   __sJT_st.setString(2,TaxId);
   __sJT_st.setInt(3,BankNumber);
   __sJT_st.setDate(4,ActiveDate);
   __sJT_st.setString(5,FANF_TABLE_2);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   variance = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:402^13*/
          
            // have a variance between the total fee amount
            // and the sum of the allocated fees.  adjust the
            // account with the largest allocation to eliminate
            if ( variance != 0.0 ) 
            {
              /*@lineinfo:generated-code*//*@lineinfo:409^15*/

//  ************************************************************
//  #sql [Ctx] it = { select  rowid       as row_id
//                  from    visa_fanf_summary   fanf
//                  where   fanf.tax_id = :TaxId
//                          and fanf.bank_number = :BankNumber
//                          and fanf.active_date = :ActiveDate
//                          and fanf.table_id = :FANF_TABLE_2  -- '2'
//                  order by fanf.fee desc                      
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rowid       as row_id\n                from    visa_fanf_summary   fanf\n                where   fanf.tax_id =  :1  \n                        and fanf.bank_number =  :2  \n                        and fanf.active_date =  :3  \n                        and fanf.table_id =  :4    -- '2'\n                order by fanf.fee desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.VisaFANFSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,TaxId);
   __sJT_st.setInt(2,BankNumber);
   __sJT_st.setDate(3,ActiveDate);
   __sJT_st.setString(4,FANF_TABLE_2);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.VisaFANFSummaryEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:418^15*/
              resultSet = it.getResultSet();
              resultSet.next();
            
              /*@lineinfo:generated-code*//*@lineinfo:422^15*/

//  ************************************************************
//  #sql [Ctx] { update  visa_fanf_summary   fanf
//                  set     fanf.fee = fanf.fee-:variance
//                  where   rowid = :resultSet.getString("row_id")
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_207 = resultSet.getString("row_id");
   String theSqlTS = "update  visa_fanf_summary   fanf\n                set     fanf.fee = fanf.fee- :1  \n                where   rowid =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.VisaFANFSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,variance);
   __sJT_st.setString(2,__sJT_207);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:427^15*/
              resultSet.close();
              it.close();
            }
          }
        }
      }
      finally
      {
        try{ it.close(); } catch( Exception ee ) {}
      }
    }
  }
  
  public static class SummaryData
  {
    public int            LocationCount     = 0;
    public double         SalesAmount       = 0.0;
    public int            Tier              = -1;
    
    public SummaryData()
    {
    }
    
    public void setCPData( int locCount, double salesAmount )
    {
      Tier          = decodeLocationTier(locCount);
      LocationCount = locCount;
      SalesAmount   = salesAmount;
      if(SalesAmount<200){
          Tier = 19;
      } else if(SalesAmount >= 200 && SalesAmount <1250){
          Tier = 20;
      } else {
          Tier = decodeLocationTier(locCount);
      }
      
    }
    
    public void setCNPData( double salesAmount )
    {
      Tier        = decodeVolumeTier(salesAmount);
      SalesAmount = salesAmount;
    }

    public int decodeLocationTier( int locCount )
    {
      int   retVal    = -1;
      
      for( int i = 0; i < LocationTiers.length; ++i )
      {
        if ( locCount >= LocationTiers[i][0] && locCount <= LocationTiers[i][1] )
        {
          retVal = (i+1);
          break;
        }
      }
      return( retVal );
    }
  
    public int decodeVolumeTier( double salesAmount )
    {
      int   retVal    = -1;
      
      for( int i = 0; i < VolumeTiers.length; ++i )
      {
        if ( salesAmount >= VolumeTiers[i][0] && salesAmount <= VolumeTiers[i][1] )
        {
          retVal = (i+1);
          break;
        }
      }
      return( retVal );
    }
  }
  
  private   Date                    ActiveDate      = null;
  private   int                     BankNumber      = 0;
  private   int                     RecCount        = 0;
  private   String                  TaxId           = null;
  private   int                     TotalCount      = 0;
  
  public VisaFANFSummaryEvent( )
  {
  }
  
  protected void createBillingChargeRecords()
  {
    try
    {
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:518^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)  
//          
//          from    mbs_charge_records
//          where   merchant_number in
//                  (
//                    select  fanf.merchant_number
//                    from    visa_fanf_summary   fanf
//                    where   fanf.bank_number = :BankNumber
//                            and fanf.active_date = :ActiveDate
//                  )
//                  and instr(statement_message,'FANF') > 0
//                  and substr(billing_frequency,to_char(:ActiveDate,'mm'),1) = 'Y'
//                  and :ActiveDate between start_date and end_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)  \n         \n        from    mbs_charge_records\n        where   merchant_number in\n                (\n                  select  fanf.merchant_number\n                  from    visa_fanf_summary   fanf\n                  where   fanf.bank_number =  :1  \n                          and fanf.active_date =  :2  \n                )\n                and instr(statement_message,'FANF') > 0\n                and substr(billing_frequency,to_char( :3  ,'mm'),1) = 'Y'\n                and  :4   between start_date and end_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.VisaFANFSummaryEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,BankNumber);
   __sJT_st.setDate(2,ActiveDate);
   __sJT_st.setDate(3,ActiveDate);
   __sJT_st.setDate(4,ActiveDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:533^7*/
      
      if ( recCount == 0 )    // not billed yet
      {
        /*@lineinfo:generated-code*//*@lineinfo:537^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mbs_charge_records
//            (
//              charge_type,
//              merchant_number,
//              billing_frequency,
//              item_count,
//              charge_amount,
//              transit_routing,
//              dda,
//              start_date,
//              end_date,
//              statement_message,
//              charge_acct_type,
//              enabled,
//              created_by
//            )
//            select  'MIS'                                 as charge_type,
//                    fanf.merchant_number                  as merchant_number,  
//                    case 
//                      when to_char(fanf.active_date,'mm') =  1 then 'YNNNNNNNNNNN'
//                      when to_char(fanf.active_date,'mm') =  2 then 'NYNNNNNNNNNN'        
//                      when to_char(fanf.active_date,'mm') =  3 then 'NNYNNNNNNNNN'        
//                      when to_char(fanf.active_date,'mm') =  4 then 'NNNYNNNNNNNN'        
//                      when to_char(fanf.active_date,'mm') =  5 then 'NNNNYNNNNNNN'        
//                      when to_char(fanf.active_date,'mm') =  6 then 'NNNNNYNNNNNN'        
//                      when to_char(fanf.active_date,'mm') =  7 then 'NNNNNNYNNNNN'        
//                      when to_char(fanf.active_date,'mm') =  8 then 'NNNNNNNYNNNN'        
//                      when to_char(fanf.active_date,'mm') =  9 then 'NNNNNNNNYNNN'        
//                      when to_char(fanf.active_date,'mm') = 10 then 'NNNNNNNNNYNN'        
//                      when to_char(fanf.active_date,'mm') = 11 then 'NNNNNNNNNNYN'
//                      when to_char(fanf.active_date,'mm') = 12 then 'NNNNNNNNNNNY'
//                      else                                       'NNNNNNNNNNNN'
//                    end                                   as billing_months,
//                    null                                  as item_count,
//                    fanf.fee                              as charge_amount,
//                    decode(mf.discount_routing, null, mf.transit_routng_num, to_char(mf.discount_routing))
//                                                          as transit_routing,
//                    decode(mf.discount_dda, null, mf.dda_num, mf.discount_dda)
//                                                          as dda,
//                    fanf.active_date                      as start_month,
//                    trunc(last_day(fanf.active_date)+15)  as expiration_date,
//                    (
//                      'VISA FIXED ACQUIRER NETWORK FEE (FANF)' ||
//                      decode(table_id,
//                              '1a',' CP/HV (1 OF ' || fanf.location_count || ')',
//                              '1b',' CP (1 OF ' || fanf.location_count || ')',
//                              '2', ' CNP/FF (' || round((fanf.sales_amount/fanf.total_amount)*100,2) || '%)',
//                              '') ||
//                      ' ' || to_char(fanf.active_date,'MON yyyy')
//                    )                           as statement_message,
//                    'CK'                        as charge_acct_type,
//                    'Y'                         as enabled,
//                    'SYSTEM'                    as created_by
//            from    mif                 mf,
//                    visa_fanf_summary   fanf        
//            where   mf.bank_number = :BankNumber
//                    and nvl(mf.visa_fanf,'N') = 'Y'
//                    and fanf.merchant_number = mf.merchant_number
//                    and fanf.active_date = :ActiveDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mbs_charge_records\n          (\n            charge_type,\n            merchant_number,\n            billing_frequency,\n            item_count,\n            charge_amount,\n            transit_routing,\n            dda,\n            start_date,\n            end_date,\n            statement_message,\n            charge_acct_type,\n            enabled,\n            created_by\n          )\n          select  'MIS'                                 as charge_type,\n                  fanf.merchant_number                  as merchant_number,  \n                  case \n                    when to_char(fanf.active_date,'mm') =  1 then 'YNNNNNNNNNNN'\n                    when to_char(fanf.active_date,'mm') =  2 then 'NYNNNNNNNNNN'        \n                    when to_char(fanf.active_date,'mm') =  3 then 'NNYNNNNNNNNN'        \n                    when to_char(fanf.active_date,'mm') =  4 then 'NNNYNNNNNNNN'        \n                    when to_char(fanf.active_date,'mm') =  5 then 'NNNNYNNNNNNN'        \n                    when to_char(fanf.active_date,'mm') =  6 then 'NNNNNYNNNNNN'        \n                    when to_char(fanf.active_date,'mm') =  7 then 'NNNNNNYNNNNN'        \n                    when to_char(fanf.active_date,'mm') =  8 then 'NNNNNNNYNNNN'        \n                    when to_char(fanf.active_date,'mm') =  9 then 'NNNNNNNNYNNN'        \n                    when to_char(fanf.active_date,'mm') = 10 then 'NNNNNNNNNYNN'        \n                    when to_char(fanf.active_date,'mm') = 11 then 'NNNNNNNNNNYN'\n                    when to_char(fanf.active_date,'mm') = 12 then 'NNNNNNNNNNNY'\n                    else                                       'NNNNNNNNNNNN'\n                  end                                   as billing_months,\n                  null                                  as item_count,\n                  fanf.fee                              as charge_amount,\n                  decode(mf.discount_routing, null, mf.transit_routng_num, to_char(mf.discount_routing))\n                                                        as transit_routing,\n                  decode(mf.discount_dda, null, mf.dda_num, mf.discount_dda)\n                                                        as dda,\n                  fanf.active_date                      as start_month,\n                  trunc(last_day(fanf.active_date)+15)  as expiration_date,\n                  (\n                    'VISA FIXED ACQUIRER NETWORK FEE (FANF)' ||\n                    decode(table_id,\n                            '1a',' CP/HV (1 OF ' || fanf.location_count || ')',\n                            '1b',' CP (1 OF ' || fanf.location_count || ')',\n                            '2', ' CNP/FF (' || round((fanf.sales_amount/fanf.total_amount)*100,2) || '%)',\n                            '') ||\n                    ' ' || to_char(fanf.active_date,'MON yyyy')\n                  )                           as statement_message,\n                  'CK'                        as charge_acct_type,\n                  'Y'                         as enabled,\n                  'SYSTEM'                    as created_by\n          from    mif                 mf,\n                  visa_fanf_summary   fanf        \n          where   mf.bank_number =  :1  \n                  and nvl(mf.visa_fanf,'N') = 'Y'\n                  and fanf.merchant_number = mf.merchant_number\n                  and fanf.active_date =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.VisaFANFSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,BankNumber);
   __sJT_st.setDate(2,ActiveDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:598^9*/
      }
      else
      {
        logEntry("createBillingChargeRecords(" + BankNumber + "," + ActiveDate + ")",
                  "WARNING - Duplicate FANF billing detected.  Charge record creation halted.");
      } 
    }
    catch( Exception e )
    {
      logEntry("createBillingChargeRecords(" + BankNumber + "," + ActiveDate + ")",e.toString());
    }
    finally
    {
    }
  }
  
  public boolean execute()
  {
    ResultSetIterator       it          = null;
    int                     lastDay     = 0;
    ThreadPool              pool        = null;
    boolean                 production  = false;
    ResultSet               resultSet   = null;
    boolean                 retVal      = false;
    Vector                  taxIds      = new Vector();
    
    try
    {
      connect(true);
    
      BankNumber = Integer.parseInt(getEventArg(0));
      production = "prod".equals(getEventArgCount() > 1 ? getEventArg(1) : "test");
      
      /*@lineinfo:generated-code*//*@lineinfo:632^7*/

//  ************************************************************
//  #sql [Ctx] { select  case when trunc(sysdate) = trunc(last_day(sysdate)) then 1 else 0 end 
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  case when trunc(sysdate) = trunc(last_day(sysdate)) then 1 else 0 end \n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.VisaFANFSummaryEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   lastDay = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:637^7*/
      
      // when in production mode, only execute 
      // on the last day of the month
      if ( !production || lastDay == 1 )
      {
        if ( ActiveDate == null )
        {
          // runs on the last business day of the month
          /*@lineinfo:generated-code*//*@lineinfo:646^11*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(sysdate,'month')
//              
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc(sysdate,'month')\n             \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.VisaFANFSummaryEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   ActiveDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:651^11*/
        }        
      
        /*@lineinfo:generated-code*//*@lineinfo:654^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    visa_fanf_summary
//            where   bank_number = :BankNumber
//                    and active_date = :ActiveDate
//                    and tax_id = nvl(:TaxId,tax_id)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    visa_fanf_summary\n          where   bank_number =  :1  \n                  and active_date =  :2  \n                  and tax_id = nvl( :3  ,tax_id)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.VisaFANFSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,BankNumber);
   __sJT_st.setDate(2,ActiveDate);
   __sJT_st.setString(3,TaxId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:661^9*/
      
        System.out.println("Loading data for bank " + BankNumber + " " + DateTimeFormatter.getFormattedDate(ActiveDate,"MMM yyyy") );
        /*@lineinfo:generated-code*//*@lineinfo:664^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct 
//                    decode( lpad(nvl(mf.federal_tax_id,'0'),9,'0'),
//                            '000000000',('mid-' || mf.merchant_number),
//                            '111111111',('mid-' || mf.merchant_number),
//                            '123456789',('mid-' || mf.merchant_number),
//                            '999999999',('mid-' || mf.merchant_number),
//                            lpad(mf.federal_tax_id,9,'0') )   as tax_id
//            from    mif   mf
//            where   (
//                      mf.bank_number = :BankNumber
//                      or (mf.bank_number in (3941,3943) and :BankNumber in (3941,3943))
//                    )
//                    and nvl(mf.date_stat_chgd_to_dcb,'31-DEC-9999') >= :ActiveDate
//                    and mf.sic_code not in ('6010','6011')
//                    and mf.federal_tax_id = nvl(:TaxId,mf.federal_tax_id)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct \n                  decode( lpad(nvl(mf.federal_tax_id,'0'),9,'0'),\n                          '000000000',('mid-' || mf.merchant_number),\n                          '111111111',('mid-' || mf.merchant_number),\n                          '123456789',('mid-' || mf.merchant_number),\n                          '999999999',('mid-' || mf.merchant_number),\n                          lpad(mf.federal_tax_id,9,'0') )   as tax_id\n          from    mif   mf\n          where   (\n                    mf.bank_number =  :1  \n                    or (mf.bank_number in (3941,3943) and  :2   in (3941,3943))\n                  )\n                  and nvl(mf.date_stat_chgd_to_dcb,'31-DEC-9999') >=  :3  \n                  and mf.sic_code not in ('6010','6011')\n                  and mf.federal_tax_id = nvl( :4  ,mf.federal_tax_id)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.VisaFANFSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,BankNumber);
   __sJT_st.setInt(2,BankNumber);
   __sJT_st.setDate(3,ActiveDate);
   __sJT_st.setString(4,TaxId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.startup.VisaFANFSummaryEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:681^9*/
        resultSet = it.getResultSet();
        while ( resultSet.next() )
        {
          taxIds.addElement(resultSet.getString("tax_id"));
        }
        resultSet.close();
        it.close();
      
        TotalCount = taxIds.size();
        log.debug("The Number of TaxIDs obtained are:" +TotalCount);
        
      
        pool = new ThreadPool(7);
      
        for( int i = 0; i < taxIds.size(); ++ i )
        {
          Thread thread = pool.getThread( new SummaryThread((String)taxIds.elementAt(i)) );
          thread.start();
        }
        pool.waitForAllThreads();
      
        // if this is a production run, then generate the billing records
        if ( production )
        {
          createBillingChargeRecords();
        }
      }
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  public void setActiveDate( Date activeDate )
  {
    ActiveDate = activeDate;
  }
  
  public void setBankNumber( int bankNumber )
  {
    BankNumber = bankNumber;
  }
  
  public void setTaxId( String taxId )
  {
    TaxId = taxId;
  }
  
  public synchronized void updateDisplay()
  {
    ++RecCount;
    System.out.print("Processed " + RecCount + " of " + TotalCount + " (" + NumberFormatter.getPercentString(((double)RecCount/(double)TotalCount),3) + ")             \r");
  }
  
  public static void main(String[] args)
  {
    VisaFANFSummaryEvent        t         = null;

    try
    {
      com.mes.database.SQLJConnectionBase.initStandalonePool("ERROR");
      
      t = new VisaFANFSummaryEvent();
      t.connect(true);

      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());

      if ( "execute".equals(args[0]) )
      {
        t.setEventArgs  ( (args.length > 1) ? args[1] : "3941" );
        t.setActiveDate ( (args.length > 2) ? new java.sql.Date(DateTimeFormatter.parseDate(args[2],"MM/dd/yyyy").getTime()) : null );
        t.setTaxId      ( (args.length > 3) ? args[3] : null );
        t.execute();
      }
      else if ( "createBillingChargeRecords".equals(args[0]) )
      {
        t.setBankNumber( Integer.parseInt(args[1]) );
        t.setActiveDate ( new java.sql.Date(DateTimeFormatter.parseDate(args[2],"MM/dd/yyyy").getTime()) );
        t.createBillingChargeRecords();
      }
      else
      {
        System.out.println("Unrecognized command: " + args[0]);
      }
      
      Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      long elapsed = (endTime.getTime() - beginTime.getTime());
      
      System.out.println();
      System.out.println("Begin Time: " + String.valueOf(beginTime));
      System.out.println("End Time  : " + String.valueOf(endTime));
      System.out.println("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));
    }
    catch(Exception e)
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ t.cleanUp(); } catch(Exception e){}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/