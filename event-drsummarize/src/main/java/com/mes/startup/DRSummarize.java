/*@lineinfo:filename=DRSummarize*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import sqlj.runtime.ResultSetIterator;

public class DRSummarize extends EventBase
{
  static Logger log = Logger.getLogger(DRSummarize.class);
  
  public boolean execute()
  {
    ResultSetIterator   it = null;
    ResultSet           rs = null;
    long  hierarchyNode = Long.parseLong(getEventArg(0));
    
    try
    {
      connect();
      
      // get MTD settlements
      log.debug("retrieving settlements for node: " + hierarchyNode);
      /*@lineinfo:generated-code*//*@lineinfo:24^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_number  merchant_number,
//                  dt.batch_date       activity_date,
//                  sum(decode(dt.debit_credit_indicator, 'C', -1, 1)*dt.transaction_amount)
//                                      gross_settlement,
//                  sum( case
//                    when dt.card_type in ('VS', 'MC') then
//                      decode(dt.debit_credit_indicator, 'C', -1, 1)*dt.transaction_amount
//                    else 0
//                    end)              net_settlement,
//                  sum( case
//                    when dt.card_type = 'AM' then
//                      decode(dt.debit_credit_indicator, 'C', -1, 1)*dt.transaction_amount
//                    else 0
//                    end)              net_amex,
//                  sum( case
//                    when dt.card_type = 'DS' then
//                      decode(dt.debit_credit_indicator, 'C', -1, 1)*dt.transaction_amount
//                    else 0
//                    end)              net_discover
//          from    t_hierarchy th,
//                  mif mf,
//                  daily_detail_file_ext_dt dt
//          where   th.ancestor = :hierarchyNode
//                  and th.descendent = mf.association_node
//                  and mf.merchant_number = dt.merchant_number
//                  and dt.batch_date between (trunc(sysdate,'mon')-1) and trunc(sysdate)
//          group by dt.merchant_number, dt.batch_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_number  merchant_number,\n                dt.batch_date       activity_date,\n                sum(decode(dt.debit_credit_indicator, 'C', -1, 1)*dt.transaction_amount)\n                                    gross_settlement,\n                sum( case\n                  when dt.card_type in ('VS', 'MC') then\n                    decode(dt.debit_credit_indicator, 'C', -1, 1)*dt.transaction_amount\n                  else 0\n                  end)              net_settlement,\n                sum( case\n                  when dt.card_type = 'AM' then\n                    decode(dt.debit_credit_indicator, 'C', -1, 1)*dt.transaction_amount\n                  else 0\n                  end)              net_amex,\n                sum( case\n                  when dt.card_type = 'DS' then\n                    decode(dt.debit_credit_indicator, 'C', -1, 1)*dt.transaction_amount\n                  else 0\n                  end)              net_discover\n        from    t_hierarchy th,\n                mif mf,\n                daily_detail_file_ext_dt dt\n        where   th.ancestor =  :1 \n                and th.descendent = mf.association_node\n                and mf.merchant_number = dt.merchant_number\n                and dt.batch_date between (trunc(sysdate,'mon')-1) and trunc(sysdate)\n        group by dt.merchant_number, dt.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.DRSummarize",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.DRSummarize",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:53^7*/
      
      rs = it.getResultSet();
      
      log.debug("adding results to vector");
      Vector items = new Vector();
      while(rs.next())
      {
        items.add( new SumItem(rs) );
      }
      
      rs.close();
      it.close();
      
      // add/update summary records
      log.debug("adding/updating daily_activity table");
      for( int i=0; i<items.size(); ++i )
      {
        SumItem si = (SumItem)(items.elementAt(i));
        
        int recCount = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:75^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//            
//            from    daily_activity
//            where   merchant_number = :si.merchantNumber
//                    and activity_date = :si.activityDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n           \n          from    daily_activity\n          where   merchant_number =  :1 \n                  and activity_date =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.DRSummarize",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,si.merchantNumber);
   __sJT_st.setDate(2,si.activityDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:82^9*/
        
        if( recCount > 0 )
        {
          log.debug("updating (" + si.merchantNumber + ", " + si.activityDate + ")" );
          /*@lineinfo:generated-code*//*@lineinfo:87^11*/

//  ************************************************************
//  #sql [Ctx] { update  daily_activity
//              set     gross_settlement  = :si.grossSettlement,
//                      net_settlement    = :si.netSettlement,
//                      net_amex          = :si.netAmex,
//                      net_discover      = :si.netDiscover
//              where   merchant_number   = :si.merchantNumber
//                      and activity_date = :si.activityDate
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  daily_activity\n            set     gross_settlement  =  :1 ,\n                    net_settlement    =  :2 ,\n                    net_amex          =  :3 ,\n                    net_discover      =  :4 \n            where   merchant_number   =  :5 \n                    and activity_date =  :6";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.DRSummarize",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,si.grossSettlement);
   __sJT_st.setDouble(2,si.netSettlement);
   __sJT_st.setDouble(3,si.netAmex);
   __sJT_st.setDouble(4,si.netDiscover);
   __sJT_st.setLong(5,si.merchantNumber);
   __sJT_st.setDate(6,si.activityDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:96^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:100^11*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_activity
//              (
//                merchant_number,
//                activity_date,
//                gross_settlement,
//                net_settlement,
//                net_amex,
//                net_discover
//              )
//              values
//              (
//                :si.merchantNumber,
//                :si.activityDate,
//                :si.grossSettlement,
//                :si.netSettlement,
//                :si.netAmex,
//                :si.netDiscover
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into daily_activity\n            (\n              merchant_number,\n              activity_date,\n              gross_settlement,\n              net_settlement,\n              net_amex,\n              net_discover\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.DRSummarize",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,si.merchantNumber);
   __sJT_st.setDate(2,si.activityDate);
   __sJT_st.setDouble(3,si.grossSettlement);
   __sJT_st.setDouble(4,si.netSettlement);
   __sJT_st.setDouble(5,si.netAmex);
   __sJT_st.setDouble(6,si.netDiscover);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:120^11*/
        }
      }

      // get MTD deposits
      log.debug("getting deposits for node: " + hierarchyNode);
      /*@lineinfo:generated-code*//*@lineinfo:126^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (ad IDX_ACH_DETAIL_MERCH_DATE) */
//                  ad.merchant_number    merchant_number,
//                  ad.post_date_option   activity_date,
//                  sum(decode(adtc.debit_credit_indicator, 'D', -1, 1)*ad.amount_of_transaction) gross_settlement,
//                  sum(0)                net_settlement,
//                  sum(0)                net_amex,
//                  sum(0)                net_discover
//          from    t_hierarchy th,
//                  mif mf,
//                  ach_detail ad,
//                  ach_detail_tran_code adtc,
//                  ach_batch ab
//          where   th.ancestor = :hierarchyNode
//                  and th.descendent = mf.association_node
//                  and mf.merchant_number = ad.merchant_number
//                  and ad.post_date_option between trunc(sysdate,'mon') and trunc(sysdate)
//                  and ad.transaction_code = adtc.ach_detail_trans_code
//                  and ad.batch_id = ab.batch_id
//                  and ab.company_entry_description = 'MERCH DEP'
//          group by ad.merchant_number, ad.post_date_option                                       
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (ad IDX_ACH_DETAIL_MERCH_DATE) */\n                ad.merchant_number    merchant_number,\n                ad.post_date_option   activity_date,\n                sum(decode(adtc.debit_credit_indicator, 'D', -1, 1)*ad.amount_of_transaction) gross_settlement,\n                sum(0)                net_settlement,\n                sum(0)                net_amex,\n                sum(0)                net_discover\n        from    t_hierarchy th,\n                mif mf,\n                ach_detail ad,\n                ach_detail_tran_code adtc,\n                ach_batch ab\n        where   th.ancestor =  :1 \n                and th.descendent = mf.association_node\n                and mf.merchant_number = ad.merchant_number\n                and ad.post_date_option between trunc(sysdate,'mon') and trunc(sysdate)\n                and ad.transaction_code = adtc.ach_detail_trans_code\n                and ad.batch_id = ab.batch_id\n                and ab.company_entry_description = 'MERCH DEP'\n        group by ad.merchant_number, ad.post_date_option";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.DRSummarize",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,hierarchyNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.DRSummarize",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:148^7*/
      
      rs = it.getResultSet();
      
      items.setSize(0);
      log.debug("adding results to vector");
      while(rs.next())
      {
        items.add( new SumItem(rs) );
      }
      
      rs.close();
      it.close();
      
      // add/update summary records
      log.debug("adding/updating daily_activity table");
      for( int i=0; i<items.size(); ++i )
      {
        SumItem si = (SumItem)(items.elementAt(i));
        
        int recCount = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:170^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//            
//            from    daily_activity
//            where   merchant_number = :si.merchantNumber
//                    and activity_date = :si.activityDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n           \n          from    daily_activity\n          where   merchant_number =  :1 \n                  and activity_date =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.DRSummarize",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,si.merchantNumber);
   __sJT_st.setDate(2,si.activityDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:177^9*/
        
        if( recCount > 0 )
        {
          log.debug("updating (" + si.merchantNumber + ", " + si.activityDate + ")");
          /*@lineinfo:generated-code*//*@lineinfo:182^11*/

//  ************************************************************
//  #sql [Ctx] { update  daily_activity
//              set     deposit_amount = :si.grossSettlement
//              where   merchant_number = :si.merchantNumber
//                      and activity_date = :si.activityDate
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  daily_activity\n            set     deposit_amount =  :1 \n            where   merchant_number =  :2 \n                    and activity_date =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.DRSummarize",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,si.grossSettlement);
   __sJT_st.setLong(2,si.merchantNumber);
   __sJT_st.setDate(3,si.activityDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:188^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:192^11*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_activity
//              (
//                merchant_number,
//                activity_date,
//                deposit_amount
//              )
//              values
//              (
//                :si.merchantNumber,
//                :si.activityDate,
//                :si.grossSettlement
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into daily_activity\n            (\n              merchant_number,\n              activity_date,\n              deposit_amount\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.DRSummarize",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,si.merchantNumber);
   __sJT_st.setDate(2,si.activityDate);
   __sJT_st.setDouble(3,si.grossSettlement);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:206^11*/
        }
      }
      
      commit();
      log.debug("DONE");
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( true );
  }
  
  public class SumItem
  {
    long          merchantNumber;
    java.sql.Date activityDate;
    double        grossSettlement;
    double        netSettlement;
    double        netAmex;
    double        netDiscover;
    double        depositAmount;
    
    public SumItem( ResultSet rs )
    {
      try
      {
        merchantNumber  = rs.getLong("merchant_number");
        activityDate    = rs.getDate("activity_date");
        
        grossSettlement = rs.getDouble("gross_settlement");
        netSettlement   = rs.getDouble("net_settlement");
        netAmex         = rs.getDouble("net_amex");
        netDiscover     = rs.getDouble("net_discover");
      }
      catch(Exception e)
      {
      }
    }
  }
  
  
  public static void main(String[] args)
  {
    com.mes.database.SQLJConnectionBase.initStandalonePool("DEBUG");
    
    DRSummarize worker = new DRSummarize();
    
    worker.setEventArgs(args[0]);
    
    worker.execute();
    
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/