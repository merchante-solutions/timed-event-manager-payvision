/*@lineinfo:filename=AuthDailyBilling*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/AuthDailyBilling.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $LastChangedDate: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.StringUtilities;
import sqlj.runtime.ResultSetIterator;

public class AuthDailyBilling extends EventBase
{
  static Logger log = Logger.getLogger(AuthDailyBilling.class);
  
  private final static int[] ActiveBanks = 
  {
    mesConstants.BANK_ID_MES,
  };
  
  public static final int CT_VS     = 0;
  public static final int CT_MC     = 1;
  public static final int CT_DB     = 2;
  public static final int CT_AM     = 3;
  public static final int CT_DS     = 4;
  public static final int CT_JC     = 5;
  public static final int CT_DC     = 6;
  public static final int CT_PL     = 7;
  public static final int CT_OT     = 8;
  public static final int CT_COUNT  = 9;
  
  // data types
  protected static final int DT_TRIDENT_AUTHS  = 0;
  protected static final int DT_PG_ACTIVITY    = 1;
  protected static final int DT_COUNT          = 2;
  
  public static class CardTotals
  {
    private static String[] CardTypeLabel = 
    {
      "Visa           : ", // CT_VS  
      "MC             : ", // CT_MC  
      "Debit          : ", // CT_DB  
      "Amex           : ", // CT_AM  
      "Discover       : ", // CT_DS  
      "JCB            : ", // CT_JC  
      "Diners         : ", // CT_DC  
      "Private        : ", // CT_PL  
      "Other          : ", // CT_OT  
    };
  
    private static String[][] CardTypeLookup = 
    {
      // vital     index 
      { "01", String.valueOf(CT_VS) },
      { "02", String.valueOf(CT_MC) },
      { "16", String.valueOf(CT_DB) },
      { "08", String.valueOf(CT_AM) },
      { "07", String.valueOf(CT_DS) },
      { "07", String.valueOf(CT_JC) },    // treat JCB like Discover
      { "06", String.valueOf(CT_DC) },
      { "09", String.valueOf(CT_DC) },
      { "03", String.valueOf(CT_PL) },
      { "00", String.valueOf(CT_OT) },
    };
    
    private double[]      AuthAmount  = new double[CT_COUNT];
    private int[]         AuthCount   = new int[CT_COUNT];
    private int[]         AVSCount    = new int[CT_COUNT];
    
    public CardTotals( )
    {
    }
    
    public void add( ResultSet resultSet )
      throws java.sql.SQLException
    {
      int     idx           = -1;
      String  vitalCardType = resultSet.getString("auth_plan_type");
      
      for( int i = 0; i < CardTypeLookup.length; ++i )
      {
        if ( CardTypeLookup[i][0].equals(vitalCardType) )
        {
          idx = Integer.parseInt(CardTypeLookup[i][1]);
          break;
        }
      }
      AVSCount[idx]   += resultSet.getInt("avs_count");
      AuthCount[idx]  += resultSet.getInt("total_auth_count");
      AuthAmount[idx] += resultSet.getDouble("real_auth_amount");
    }
    
    public double getAuthAmount(int idx)
    {
      return( AuthAmount[idx] );
    }
    
    public int getAuthCount(int idx)
    {
      return( AuthCount[idx] );
    }
    
    public int getAVSCount(int idx)
    {
      return( AVSCount[idx] );
    }
    
    public String getCardTypeLabel(int idx)
    {
      return( CardTypeLabel[idx] );
    }
    
    public void reset( )
    {
      for( int ct = 0; ct < CT_COUNT; ++ct )
      {
        AuthAmount[ct]  = 0.0;
        AuthCount[ct]   = 0;
        AVSCount[ct]    = 0;
      }
    }
  }
  
  // file statistics
  protected double        FileAuthAmount        = 0.0;
  protected int           FileAuthCount         = 0;
  protected int           FileAVSCount          = 0;
  protected int           FileRecordCount       = 0;
  protected CardTotals    FileCardTotals        = new CardTotals();
  
  // other vendor statistics
  protected int           PgIntlCount           = 0;
  protected int           PgCardinalCount       = 0;
  
  protected Date          FileActivityDate      = null;
  protected int           FileBankNumber        = 0;
  protected Date          TestFileDate          = null;
  
  public AuthDailyBilling( )
  {
    PropertiesFilename = "authbilling.properties";
  }
  
  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    String              dateString      = null;
    FlatFileRecord      ffd             = new FlatFileRecord(MesFlatFiles.DEF_TYPE_TSYS_AUTH_BILLING);
    ResultSetIterator   it              = null;
    BufferedWriter      out             = null;
    ResultSet           resultSet       = null;
    String              workFilename    = null;
    
    try
    {
      // get an automated timeout exempt connection
      connect(true);
      
      for( int i = 0; i < ActiveBanks.length; ++i )
      {
        FileBankNumber = ActiveBanks[i];
        
        // reset the file counters
        out             = null;
        FileRecordCount = 0;
        FileAuthAmount  = 0.0;
        FileAuthCount   = 0;
        FileAVSCount    = 0;
        PgIntlCount     = 0;
        PgCardinalCount = 0;
        FileCardTotals.reset();
        
        if ( TestFileDate == null )
        {
          /*@lineinfo:generated-code*//*@lineinfo:215^11*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(sysdate)-1 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc(sysdate)-1  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.AuthDailyBilling",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   FileActivityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:219^11*/
        }
        else
        {
          FileActivityDate = TestFileDate;
        }
      
        // setup a temp filename
        workFilename = generateFilename( "tauth" + FileBankNumber );
        
        log.debug("Bank Number   : " + FileBankNumber);
        log.debug("Activity Date : " + DateTimeFormatter.getFormattedDate(FileActivityDate,"MM/dd/yyyy"));
        log.debug("Work Filename : " + workFilename);
        
        for( int dataType = 0; dataType < DT_COUNT; ++dataType )
        {
          switch( dataType )
          {
            case DT_TRIDENT_AUTHS:
              // run the query to collect the auth counts
              /*@lineinfo:generated-code*//*@lineinfo:239^15*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                              index(mf idx_mif_bank_number)
//                              use_nl(mf auth)
//                           */
//                          decode( mf.bank_number,
//                                  3858, '011',  -- vital POS
//                                  decode( auth.developer_id,
//                                          'VCAUTH','011',   -- vital POS
//                                          'TELPAY','011',   -- vital POS
//                                          '009' )           -- in house
//                                )                         as vendor_id,
//                          mf.bank_number                  as bank_ica,
//                          ( 
//                            decode( mf.bank_number,
//                                    3858, '0', '' ) ||
//                            auth.merchant_number
//                          )                               as merchant_number,
//                          :FileActivityDate               as tran_date,
//                          decode( auth.card_type,
//                                  'VS', '01',
//                                  'VB', '01',
//                                  'VD', '01',
//                                  'MC', '02',
//                                  'MB', '02',
//                                  'MD', '02',
//                                  'PL', '03',
//                                  'DC', '06',
//                                  'DS', '07',
//                                  'JC', '07',   -- treat jcb like discover
//                                  'AM', '08',
//                                  'CB', '09',
//                                  'DB', '16',
//                                  '00' )                  as auth_plan_type,
//                          decode( auth.developer_id,
//                                  'VCAUTH','03',          -- ARU
//                                  'TELPAY','03',          -- ARU
//                                  decode( mf.bank_number,
//                                          3858, '02',     -- terminal
//                                          '06' )          -- fixed
//                                )                         as media_type,
//                          sum( decode(nvl(auth.avs_result,'0'),
//                                      '0',0,1) )          as avs_count,
//                          count(auth.merchant_number)     as fixed_count,
//                          count(auth.merchant_number)     as total_auth_count,
//                          case 
//                            when sum(auth.authorized_amount) > 9999999.99 then 9999999.99 -- hack to deal with limitations of file format
//                            else sum(auth.authorized_amount) 
//                            end                           as total_auth_amount,
//                          sum(auth.authorized_amount)     as real_auth_amount
//                  from    mif                     mf,
//                          tc33_trident            auth
//                  where   (
//                            (:FileBankNumber = 3941 and mf.bank_number in (3941,3858,3942)) 
//                            or (mf.bank_number = :FileBankNumber)
//                          ) 
//                          and nvl(mf.processor_id,0) = 0      -- TSYS only
//                          and nvl(mf.test_account,'N') != 'Y' -- exclude test accounts
//                          and auth.merchant_number = mf.merchant_number 
//                          and auth.transaction_date = :FileActivityDate
//                  group by  mf.bank_number,
//                            auth.merchant_number,
//                            auth.developer_id,
//                            decode( auth.card_type,
//                                    'VS', '01',
//                                    'VB', '01',
//                                    'VD', '01',
//                                    'MC', '02',
//                                    'MB', '02',
//                                    'MD', '02',
//                                    'PL', '03',
//                                    'DC', '06',
//                                    'DS', '07',
//                                    'JC', '07',
//                                    'AM', '08',
//                                    'CB', '09',
//                                    'DB', '16',
//                                    '00' )
//                  order by mf.bank_number, merchant_number, auth_plan_type     
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                            index(mf idx_mif_bank_number)\n                            use_nl(mf auth)\n                         */\n                        decode( mf.bank_number,\n                                3858, '011',  -- vital POS\n                                decode( auth.developer_id,\n                                        'VCAUTH','011',   -- vital POS\n                                        'TELPAY','011',   -- vital POS\n                                        '009' )           -- in house\n                              )                         as vendor_id,\n                        mf.bank_number                  as bank_ica,\n                        ( \n                          decode( mf.bank_number,\n                                  3858, '0', '' ) ||\n                          auth.merchant_number\n                        )                               as merchant_number,\n                         :1                as tran_date,\n                        decode( auth.card_type,\n                                'VS', '01',\n                                'VB', '01',\n                                'VD', '01',\n                                'MC', '02',\n                                'MB', '02',\n                                'MD', '02',\n                                'PL', '03',\n                                'DC', '06',\n                                'DS', '07',\n                                'JC', '07',   -- treat jcb like discover\n                                'AM', '08',\n                                'CB', '09',\n                                'DB', '16',\n                                '00' )                  as auth_plan_type,\n                        decode( auth.developer_id,\n                                'VCAUTH','03',          -- ARU\n                                'TELPAY','03',          -- ARU\n                                decode( mf.bank_number,\n                                        3858, '02',     -- terminal\n                                        '06' )          -- fixed\n                              )                         as media_type,\n                        sum( decode(nvl(auth.avs_result,'0'),\n                                    '0',0,1) )          as avs_count,\n                        count(auth.merchant_number)     as fixed_count,\n                        count(auth.merchant_number)     as total_auth_count,\n                        case \n                          when sum(auth.authorized_amount) > 9999999.99 then 9999999.99 -- hack to deal with limitations of file format\n                          else sum(auth.authorized_amount) \n                          end                           as total_auth_amount,\n                        sum(auth.authorized_amount)     as real_auth_amount\n                from    mif                     mf,\n                        tc33_trident            auth\n                where   (\n                          ( :2  = 3941 and mf.bank_number in (3941,3858,3942)) \n                          or (mf.bank_number =  :3 )\n                        ) \n                        and nvl(mf.processor_id,0) = 0      -- TSYS only\n                        and nvl(mf.test_account,'N') != 'Y' -- exclude test accounts\n                        and auth.merchant_number = mf.merchant_number \n                        and auth.transaction_date =  :4 \n                group by  mf.bank_number,\n                          auth.merchant_number,\n                          auth.developer_id,\n                          decode( auth.card_type,\n                                  'VS', '01',\n                                  'VB', '01',\n                                  'VD', '01',\n                                  'MC', '02',\n                                  'MB', '02',\n                                  'MD', '02',\n                                  'PL', '03',\n                                  'DC', '06',\n                                  'DS', '07',\n                                  'JC', '07',\n                                  'AM', '08',\n                                  'CB', '09',\n                                  'DB', '16',\n                                  '00' )\n                order by mf.bank_number, merchant_number, auth_plan_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.AuthDailyBilling",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,FileActivityDate);
   __sJT_st.setInt(2,FileBankNumber);
   __sJT_st.setInt(3,FileBankNumber);
   __sJT_st.setDate(4,FileActivityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.AuthDailyBilling",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:319^15*/
              break;
            
            case DT_PG_ACTIVITY:
              // run the query to collect the payment gateway counts
              /*@lineinfo:generated-code*//*@lineinfo:324^15*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                              use_nl( mf al )
//                              index( tapi idx_trident_cap_api_tran_id ) 
//                          */
//                          decode( mf.bank_number,
//                                  3858, '011',  -- vital POS
//                                  '009' ) /* in house */  as vendor_id,
//                          mf.bank_number                  as bank_ica,
//                          ( 
//                            decode( mf.bank_number,
//                                    3858, '0', '' ) ||
//                            al.merchant_number
//                          )                               as merchant_number,
//                          :FileActivityDate               as tran_date,
//                          decode( nvl( tapi.card_type,
//                                       decode_card_type(substr( al.request_params,
//                                                                (instr(al.request_params,'card_number=')+12),
//                                                                6),
//                                                        null) ),
//                                  'VS', '01',
//                                  'VB', '01',
//                                  'VD', '01',
//                                  'MC', '02',
//                                  'MB', '02',
//                                  'MD', '02',
//                                  'PL', '03',
//                                  'DC', '06',
//                                  'DS', '07',
//                                  'JC', '07',   -- treat jcb like discover
//                                  'AM', '08',
//                                  'CB', '09',
//                                  'DB', '16',
//                                  '03' ) -- default to private label 
//                                                          as auth_plan_type,
//                          '06' /* fixed */                as media_type,
//                          0                               as avs_count,
//                          sum( decode(al.currency_code,     -- payvision requests
//                                      '840',0,1) )        as switch_count,
//                          sum( decode(al.transaction_type,  -- vbv/msc requests
//                                      'E',1,0) )          as other_count,
//                          sum( decode(al.transaction_type,
//                                      'E',1,
//                                       decode(al.currency_code,'840',0,1) )
//                             )                            as total_auth_count,
//                          0                               as total_auth_amount,
//                          0                               as real_auth_amount
//                  from    mif                     mf,
//                          trident_api_access_log  al,
//                          trident_capture_api     tapi
//                  where   decode(mf.bank_number,
//                                 3942,3941,   -- 3942 sent in the 3941 file
//                                 3858,3941,   -- 3858 sent in the 3941 file
//                                 mf.bank_number) = :FileBankNumber 
//                          and nvl(mf.processor_id,0) = 0      -- TSYS only
//                          and nvl(mf.test_account,'N') != 'Y' -- exclude test accounts
//                          and al.merchant_number = mf.merchant_number 
//                          and al.request_date = :FileActivityDate 
//                          and
//                          ( 
//                            al.transaction_type = 'E' or
//                            al.currency_code != '840'
//                          ) 
//                          and tapi.trident_tran_id(+) = al.trident_tran_id
//                          and tapi.mesdb_timestamp(+) >= (:FileActivityDate-30)
//                  group by  mf.bank_number,
//                            al.merchant_number,
//                            decode( nvl( tapi.card_type,
//                                       decode_card_type(substr( al.request_params,
//                                                                (instr(al.request_params,'card_number=')+12),
//                                                                6),
//                                                        null) ),
//                                  'VS', '01',
//                                  'VB', '01',
//                                  'VD', '01',
//                                  'MC', '02',
//                                  'MB', '02',
//                                  'MD', '02',
//                                  'PL', '03',
//                                  'DC', '06',
//                                  'DS', '07',
//                                  'JC', '07',
//                                  'AM', '08',
//                                  'CB', '09',
//                                  'DB', '16',
//                                  '03' )
//                  order by mf.bank_number, merchant_number, auth_plan_type              
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                            use_nl( mf al )\n                            index( tapi idx_trident_cap_api_tran_id ) \n                        */\n                        decode( mf.bank_number,\n                                3858, '011',  -- vital POS\n                                '009' ) /* in house */  as vendor_id,\n                        mf.bank_number                  as bank_ica,\n                        ( \n                          decode( mf.bank_number,\n                                  3858, '0', '' ) ||\n                          al.merchant_number\n                        )                               as merchant_number,\n                         :1                as tran_date,\n                        decode( nvl( tapi.card_type,\n                                     decode_card_type(substr( al.request_params,\n                                                              (instr(al.request_params,'card_number=')+12),\n                                                              6),\n                                                      null) ),\n                                'VS', '01',\n                                'VB', '01',\n                                'VD', '01',\n                                'MC', '02',\n                                'MB', '02',\n                                'MD', '02',\n                                'PL', '03',\n                                'DC', '06',\n                                'DS', '07',\n                                'JC', '07',   -- treat jcb like discover\n                                'AM', '08',\n                                'CB', '09',\n                                'DB', '16',\n                                '03' ) -- default to private label \n                                                        as auth_plan_type,\n                        '06' /* fixed */                as media_type,\n                        0                               as avs_count,\n                        sum( decode(al.currency_code,     -- payvision requests\n                                    '840',0,1) )        as switch_count,\n                        sum( decode(al.transaction_type,  -- vbv/msc requests\n                                    'E',1,0) )          as other_count,\n                        sum( decode(al.transaction_type,\n                                    'E',1,\n                                     decode(al.currency_code,'840',0,1) )\n                           )                            as total_auth_count,\n                        0                               as total_auth_amount,\n                        0                               as real_auth_amount\n                from    mif                     mf,\n                        trident_api_access_log  al,\n                        trident_capture_api     tapi\n                where   decode(mf.bank_number,\n                               3942,3941,   -- 3942 sent in the 3941 file\n                               3858,3941,   -- 3858 sent in the 3941 file\n                               mf.bank_number) =  :2  \n                        and nvl(mf.processor_id,0) = 0      -- TSYS only\n                        and nvl(mf.test_account,'N') != 'Y' -- exclude test accounts\n                        and al.merchant_number = mf.merchant_number \n                        and al.request_date =  :3  \n                        and\n                        ( \n                          al.transaction_type = 'E' or\n                          al.currency_code != '840'\n                        ) \n                        and tapi.trident_tran_id(+) = al.trident_tran_id\n                        and tapi.mesdb_timestamp(+) >= ( :4 -30)\n                group by  mf.bank_number,\n                          al.merchant_number,\n                          decode( nvl( tapi.card_type,\n                                     decode_card_type(substr( al.request_params,\n                                                              (instr(al.request_params,'card_number=')+12),\n                                                              6),\n                                                      null) ),\n                                'VS', '01',\n                                'VB', '01',\n                                'VD', '01',\n                                'MC', '02',\n                                'MB', '02',\n                                'MD', '02',\n                                'PL', '03',\n                                'DC', '06',\n                                'DS', '07',\n                                'JC', '07',\n                                'AM', '08',\n                                'CB', '09',\n                                'DB', '16',\n                                '03' )\n                order by mf.bank_number, merchant_number, auth_plan_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.AuthDailyBilling",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,FileActivityDate);
   __sJT_st.setInt(2,FileBankNumber);
   __sJT_st.setDate(3,FileActivityDate);
   __sJT_st.setDate(4,FileActivityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.AuthDailyBilling",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:412^15*/
              break;
            
            default:
              it = null;
              break;
          }
        
          if ( it != null )
          {
            resultSet = it.getResultSet();

            while( resultSet.next() )
            {
              if ( out == null )
              {
                // open the data file
                out = new BufferedWriter( new FileWriter( workFilename, false ) );
              }       
              ffd.resetAllFields();
              ffd.setAllFieldData(resultSet);
              out.write( ffd.spew() );
              out.newLine();
          
              ++FileRecordCount;
              if ( dataType == DT_TRIDENT_AUTHS )
              {
                FileAuthCount   += resultSet.getInt("total_auth_count");
                FileAuthAmount  += resultSet.getDouble("real_auth_amount");
                FileAVSCount    += resultSet.getInt("avs_count");
                FileCardTotals.add(resultSet);
              }
              else if ( dataType == DT_PG_ACTIVITY )
              {
                PgIntlCount     += resultSet.getInt("switch_count");
                PgCardinalCount += resultSet.getInt("other_count");
              }
            }          
            resultSet.close();
            it.close();
          }          
        }   // end for loop through data types
        
        if ( FileRecordCount > 0 )
        {
          out.close();          // close the file buffer
          
          if ( TestFileDate == null ) // only send non-test files
          {
            sendFile(workFilename);
          }            
        }
        else    // nothing to process
        {
          log.debug("No outgoing records found");
        }
        
        if ( TestFileDate != null )
        {
          // exit the loop through active banks.
          break;
        }
      }        
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ it.close(); }catch(Exception e){}
      cleanUp();
    }
    return( true );
  }
  
  protected boolean sendFile( String dataFilename )
  {
    boolean     success     = false;
    
    try
    {
      if ( TestFileDate == null ) // only send non-test files
      {
        // ftp the file to the outgoing directory
        // note:  using same server as chargebacks for transfer
        success = sendDataFile( dataFilename, 
                                MesDefaults.getString(MesDefaults.DK_OUTGOING_CB_HOST),
                                MesDefaults.getString(MesDefaults.DK_OUTGOING_CB_USER),
                                MesDefaults.getString(MesDefaults.DK_OUTGOING_CB_PASSWORD) );
                                
        archiveDailyFile(dataFilename);
        sendStatusEmail( success, dataFilename );
      }        
      else
      {
        sendStatusEmail( success, dataFilename );
      }
    }      
    catch( Exception e )
    {
      logEntry("sendFile(" + dataFilename + ")", e.toString());
    }
    finally
    {
    }
    return( success );
  }    
  
  private void sendStatusEmail(boolean success, String fileName )
  {
    StringBuffer  subject         = new StringBuffer("");
    StringBuffer  body            = new StringBuffer("");
    
    try
    {
      if( success )
      {
        subject.append("Outgoing Auth Billing Success - " + fileName);
        
        body.append("[");
        body.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy hh:mm:ss"));
        body.append("] Successfully processed outgoing Auth Billing file: ");
        body.append(fileName);
        body.append("\n\n");
      }
      else
      {
        subject.append("Outgoing Auth Billing Error - " + fileName);
        
        // make sure the filename is in 
        // the message text for error research
        body.append("[");
        body.append(DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MM/dd/yyyy hh:mm:ss"));
        body.append("] Failed to process outgoing Auth Billing file: ");
        body.append(fileName);
        body.append("\n\n");
      }
      body.append("File Totals\n");
      body.append("========================================================\n");
      body.append("Record Count   : ");
      body.append( StringUtilities.rightJustify(String.valueOf(FileRecordCount),9,' ') );
      body.append("\n\n");
      body.append("========================================================\n");
      body.append("Card Type          Count            Amount        AVS   \n");
      body.append("========================================================\n");
      for( int ct = 0; ct < CT_COUNT; ++ct )
      {
        body.append( FileCardTotals.getCardTypeLabel(ct) );
        body.append( StringUtilities.rightJustify(String.valueOf(FileCardTotals.getAuthCount(ct)),9,' ') );
        body.append( " " );
        body.append( StringUtilities.rightJustify(MesMath.toCurrency(FileCardTotals.getAuthAmount(ct)),20,' ') );
        body.append( " " );
        body.append( StringUtilities.rightJustify(String.valueOf(FileCardTotals.getAVSCount(ct)), 9, ' ') );
        body.append( "\n");
      }
      body.append( "\n");
      body.append("Totals         : " + StringUtilities.rightJustify(String.valueOf(FileAuthCount),9,' ') );
      body.append( " " );
      body.append( StringUtilities.rightJustify(MesMath.toCurrency(FileAuthAmount),20,' ') );
      body.append( " " );
      body.append( StringUtilities.rightJustify(String.valueOf(FileAVSCount), 9, ' ' ) );
      body.append("\n\n");
      
      body.append("========================================================\n");
      body.append("Transaction Type                                Count   \n");
      body.append("========================================================\n");
      body.append( "International Gateway                       " );
      body.append( StringUtilities.rightJustify(String.valueOf(PgIntlCount), 9, ' ' ) );
      body.append( "\n");
      body.append( "Cardinal Commerce VBV/MSC                   " );
      body.append( StringUtilities.rightJustify(String.valueOf(PgCardinalCount), 9, ' ' ) );
      body.append("\n\n");
      
      MailMessage msg = new MailMessage();
      
      if(success)
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_AUTH_DAILY_BILLING_NOTIFY);
      }
      else
      {
        msg.setAddresses(MesEmails.MSG_ADDRS_AUTH_DAILY_BILLING_FAILURE);
      }
      
      msg.setSubject(subject.toString());
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("sendStatusEmail()", e.toString());
    }
  }
  
  public void setTestFileDate( Date activityDate )
  {
    TestFileDate = activityDate;
  }
  
  public void setTestFileDate( String activityDate )
  {
    if ( activityDate != null )
    {
      setTestFileDate ( new java.sql.Date( DateTimeFormatter.parseDate(activityDate,"MM/dd/yyyy").getTime() ) );
    }
  }
  
  public static void main( String[] args )
  {
      try
      {
        if (args.length > 0 && args[0].equals("testproperties")) {
        EventBase.printKeyListStatus(new String[]{
                MesDefaults.DK_OUTGOING_CB_HOST,
                MesDefaults.DK_OUTGOING_CB_USER,
                MesDefaults.DK_OUTGOING_CB_PASSWORD
        });
      }
    }
    catch(Exception e)
    {
      System.out.println("main(): " + e.toString());
      log.error(e.getMessage());
    }

    AuthDailyBilling        test          = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      test = new AuthDailyBilling();
      test.setTestFileDate( (args.length == 0) ? (String)null : args[0]);
      test.execute();
      
      if ( args.length > 0 )
      {
        test.sendStatusEmail(true,"Recreated file for activity date " + args[0]);
      }        
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/