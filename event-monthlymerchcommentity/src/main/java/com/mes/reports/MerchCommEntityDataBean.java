/*@lineinfo:filename=MerchCommEntityDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.support.CSVFileMemory;
import sqlj.runtime.ScrollableResultSetIterator;

public class MerchCommEntityDataBean extends SQLJConnectionBase {

  static Logger log = Logger.getLogger(MerchCommEntityDataBean.class);
  ArrayList<MerchCommEntityRecord> merchCommEntityRows = new ArrayList<MerchCommEntityRecord>();
  static String fromEmailAddress = null;
  static String toEmailAddress   = null;
  static String ccProjectManagerEmailAddress = null;

  CSVFileMemory csvMerchCommEntityFile = null;
  
  final static int MERCHANT_NAME   = 1;
  final static int MERCHANT_NUMBER = 2;
  final static int PSP_NAME        = 3;
  final static int PSP_ID          = 4;
  final static int MASTER_CARD_SALES_AMOUNT = 5;
  final static int VISA_CARD_SALES_AMOUNT   = 6;

  public void loadData(Date beginDate, Date endDate) {
    try {      
      log.info("Loading MerchantCommEntity Data for the period " + beginDate + " to " + endDate);
      connect();
      loadMerchCommEntityProperties();
      getMerchantCommercialEntityData(beginDate, endDate);      
      logEntry("loadData()", "sending email - number of rows in the report = " + merchCommEntityRows.size());            
      sendEmail();      
      log.info("Done!");
    } catch (Exception e) {
      logEntry("loadData()", e.toString());
    } finally {
      cleanUp();
    }

  }

  private void createMerchCommEntityRecord(ResultSet resultSet, Date endDate) throws SQLException {
   
    while (resultSet.next()) {
      MerchCommEntityRecord merchCommEntityRecord = new MerchCommEntityRecord();
      
      merchCommEntityRecord.setMerchantName(resultSet.getString(MERCHANT_NAME)); 
      merchCommEntityRecord.setMerchantNumber(resultSet.getLong(MERCHANT_NUMBER)); 
      merchCommEntityRecord.setPspName(resultSet.getString(PSP_NAME));
      merchCommEntityRecord.setPspId(resultSet.getLong(PSP_ID));
      merchCommEntityRecord.setMasterCardSalesAmount(resultSet.getDouble(MASTER_CARD_SALES_AMOUNT));
      merchCommEntityRecord.setVisaCardSalesAmount(resultSet.getDouble(VISA_CARD_SALES_AMOUNT));
      merchCommEntityRows.add(merchCommEntityRecord);
      System.out.println("merchantNumber: "+resultSet.getLong(MERCHANT_NUMBER)); 
      updateMerchantCommercialEntity(resultSet.getLong(MERCHANT_NUMBER), endDate);
    }    
    logInfo("MerchCommEntity record collection size = " + merchCommEntityRows.size());
    System.out.println("MerchCommEntity record collection size = " + merchCommEntityRows.size());
  }

 private void updateMerchantCommercialEntity(long merchantNumber, Date endDate) 
  {
    try
    { 
//    //check if the merchant is under two nodes
//      int recCount = 0;
//      #sql [Ctx]
//      {
//        select  count(1)
//        into    :recCount
//        from    merchant_commercial_entity
//        where   merchant_number = :merchantNumber
//      };

//      if ( recCount == 0 ) {
        /*@lineinfo:generated-code*//*@lineinfo:86^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant_commercial_entity
//            ( merchant_number,
//              move_date )
//            values
//            ( :merchantNumber,
//              :endDate 
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchant_commercial_entity\n          ( merchant_number,\n            move_date )\n          values\n          (  :1 ,\n             :2  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.reports.MerchCommEntityDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDate(2,endDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:95^9*/
//      }
    }
    catch(Exception e)
    {
      logEntry("updateMerchantCommercialEntity(merchantNumber, endDate)::  (" + merchantNumber + ", " + endDate + "): ", e.toString());
    }
  }

  private void getMerchantCommercialEntityData(Date beginDate, Date endDate) {
    ScrollableResultSetIterator   it                = null;  
    ResultSet                     resultSet         = null;  
   
    try {
   
      /*@lineinfo:generated-code*//*@lineinfo:110^7*/

//  ************************************************************
//  #sql [Ctx] it = { select mf.dba_name                       as "Merchant Name",
//                   mf.merchant_number                as "Merchant Number", 
//                   o.org_name                        as "Psp Name", 
//                   o.org_group                       as "Psp Id",
//                   sum(nvl(mes.mc_sales_amount,0))   as "Master Card Sales Amount", 
//                   sum(nvl(mes.visa_sales_amount,0)) as "Visa Card Sales Amount" 
//            from   mif mf, 
//                   Monthly_extract_summary mes,
//                   organization            o,
//                   group_merchant          gm,
//                   psp_report_node         prn
//            where  mf.merchant_number = mes.merchant_number
//                   and o.org_group = prn.hierarchy_node
//                   and gm.org_num  = o.org_num
//                   and mf.merchant_number = gm.merchant_number
//                   and mes.active_date between :beginDate and :endDate 
//                   and not exists
//                   (
//                      select mce.merchant_number
//                      from   merchant_commercial_entity mce
//                      where  mce.merchant_number = mf.merchant_number          
//                   )
//            group  by mf.dba_name, mf.merchant_number, o.org_name, o.org_group 
//            having sum(mes.mc_sales_amount) >= 100000 or sum(mes.visa_sales_amount) >= 100000
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select mf.dba_name                       as \"Merchant Name\",\n                 mf.merchant_number                as \"Merchant Number\", \n                 o.org_name                        as \"Psp Name\", \n                 o.org_group                       as \"Psp Id\",\n                 sum(nvl(mes.mc_sales_amount,0))   as \"Master Card Sales Amount\", \n                 sum(nvl(mes.visa_sales_amount,0)) as \"Visa Card Sales Amount\" \n          from   mif mf, \n                 Monthly_extract_summary mes,\n                 organization            o,\n                 group_merchant          gm,\n                 psp_report_node         prn\n          where  mf.merchant_number = mes.merchant_number\n                 and o.org_group = prn.hierarchy_node\n                 and gm.org_num  = o.org_num\n                 and mf.merchant_number = gm.merchant_number\n                 and mes.active_date between  :1  and  :2  \n                 and not exists\n                 (\n                    select mce.merchant_number\n                    from   merchant_commercial_entity mce\n                    where  mce.merchant_number = mf.merchant_number          \n                 )\n          group  by mf.dba_name, mf.merchant_number, o.org_name, o.org_group \n          having sum(mes.mc_sales_amount) >= 100000 or sum(mes.visa_sales_amount) >= 100000";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchCommEntityDataBean",theSqlTS,1004,1007);
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,endDate);
   // execute query
   it = new sqlj.runtime.ref.ScrollableResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchCommEntityDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:136^9*/
      resultSet = it.getResultSet();
      
      if (resultSet != null) {          
        createMerchCommEntityRecord(resultSet, endDate);
        resultSet.beforeFirst(); 
        csvMerchCommEntityFile = new CSVFileMemory(resultSet, true); // copy to csv file
        resultSet.close();  
      }
    
    } catch(Exception e) {
      logEntry("Exception in getMerchantCommercialEntityData() : ", e.toString());
    } finally {
      try{
        logInfo("Closing SQL connection.");        
        it.close();       
      } catch(Exception e) {
        logEntry("Exception in finally block of getMerchantCommercialEntityData() : ", e.toString());
      }
    }
  }

  private void loadMerchCommEntityProperties() throws Exception {
    fromEmailAddress = MesDefaults.getString(MesDefaults.MERCH_PSP_FROM_EMAIL_ADDRESS);
    toEmailAddress   = MesDefaults.getString(MesDefaults.MERCH_COMM_ENTITY_TO_EMAIL_ADDRESS);   
    ccProjectManagerEmailAddress = MesDefaults.getString(MesDefaults.MERCH_PSP_CC_PROJECT_MANAGER_EMAIL_ADDRESS);
  }

  private void sendEmail() {
    try {
      // send email
      log.info("Sending notification email");
      sendNotificationEmail(true, "Report is generated successfully");
    } catch (Exception e) {
      logEntry("sendEmail()", e.toString());
      sendNotificationEmail(false, "Failed to generate the report : " + e.getMessage());
      e.printStackTrace();
    }
  }
  
  public CSVFileMemory createCSVFile(ResultSet anotherSet, boolean withHeader) {
    CSVFileMemory file = new CSVFileMemory(anotherSet, withHeader);
    return file;
  }

  private void sendNotificationEmail(boolean wasSentSuccessfully, String body) {
    String subject = "A list of PSP Merchants who are processing over $100K ";

    try {
      MailMessage msg = new MailMessage();
      msg.setFrom(fromEmailAddress);
      if (wasSentSuccessfully) {
        msg.addTo(toEmailAddress);
        
        if (ccProjectManagerEmailAddress != null) {
          msg.addCC(ccProjectManagerEmailAddress);
        }
      } else {
        msg.addTo(ccProjectManagerEmailAddress);
      }

      if (csvMerchCommEntityFile != null) {
        msg.addFile("MerchCommEntityRecords.csv", csvMerchCommEntityFile.toString().getBytes());
      }
      msg.setSubject(subject);
      msg.setText(body);

      msg.send();
    } catch (Exception e) {
      logEntry("sendNotificationEmail(): ", e.getMessage());

    }
  }

  public static void main(String args[]) {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.MONTH, -1);
    cal.set(Calendar.DAY_OF_MONTH, 1);
    
    Date toDate = new java.sql.Date(cal.getTime().getTime());
    System.out.println("toDate = " + toDate);
    cal.setTime(toDate);
    cal.add(Calendar.YEAR, -1);
    Date fromDate = new java.sql.Date(cal.getTime().getTime());
    System.out.println("fromDate = " + fromDate);

    MerchCommEntityDataBean merchCommEntityDataBean = new MerchCommEntityDataBean();
    merchCommEntityDataBean.loadData(fromDate, toDate);
//    try{
//      merchCommEntityDataBean.loadMerchCommEntityProperties();
//      System.out.println(" ***************sendNotificationEmail********");
//      merchCommEntityDataBean.sendNotificationEmail("Test email");  
//      System.out.println("DONE");
//    } catch(Exception e) {
//      e.printStackTrace();
//    }

  }

}/*@lineinfo:generated-code*/