package com.mes.reports;


public class MerchCommEntityRecord { 
  
  String merchantName; 
  long merchantNumber; 
  String pspName; 
  long pspId;
  double masterCardSalesAmount; 
  double visaCardSalesAmount;
  
  public String getMerchantName() {
    return merchantName;
  }
  public void setMerchantName(String merchantName) {
    this.merchantName = merchantName;
  }
  public long getMerchantNumber() {
    return merchantNumber;
  }
  public void setMerchantNumber(long merchantNumber) {
    this.merchantNumber = merchantNumber;
  }
  public String getPspName() {
    return pspName;
  }
  public void setPspName(String pspName) {
    this.pspName = pspName;
  }
  public long getPspId() {
    return pspId;
  }
  public void setPspId(long pspId) {
    this.pspId = pspId;
  }
  public double getMasterCardSalesAmount() {
    return masterCardSalesAmount;
  }
  public void setMasterCardSalesAmount(double masterCardSalesAmount) {
    this.masterCardSalesAmount = masterCardSalesAmount;
  }
  public double getVisaCardSalesAmount() {
    return visaCardSalesAmount;
  }
  public void setVisaCardSalesAmount(double visaCardSalesAmount) {
    this.visaCardSalesAmount = visaCardSalesAmount;
  } 
  
  
}
