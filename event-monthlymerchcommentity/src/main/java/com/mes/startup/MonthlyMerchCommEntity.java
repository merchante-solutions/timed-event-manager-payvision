/*@lineinfo:filename=MonthlyMerchCommEntity*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  Description:

  Last Modified By   : $Author: mjanbay $
  Last Modified Date : $Date: 2014-06-03 15:06:04 -0800 (Tue, 03 Jun 2014) $
  Version            : $Revision: 21863 $

  Change History:
     See VSS database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.
  

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.util.Calendar;
import com.mes.config.MesDefaults;
import com.mes.reports.MerchCommEntityDataBean;
import com.mes.support.DateTimeFormatter;

public class MonthlyMerchCommEntity extends EventBase {

  private Date manualActiveDate = null;

  public boolean execute() {
    Date fromDate = null;
    Date toDate = null;

    Calendar cal = Calendar.getInstance();

    try {
    
      if (manualActiveDate == null) {
        //default is the previous month
        cal.add(Calendar.MONTH, -1);
      } else {
        // use user provided date
        cal.setTime(manualActiveDate);
      }
      
      cal.set(Calendar.DAY_OF_MONTH, 1);
      cal.set(Calendar.HOUR_OF_DAY, 0);
	  cal.set(Calendar.MINUTE, 0);
	  cal.set(Calendar.SECOND, 0);
	  cal.set(Calendar.MILLISECOND, 0);
      toDate = new java.sql.Date(cal.getTime().getTime());

      // MBS uses calendar month as the billing month
      cal.setTime(toDate);
      cal.add(Calendar.YEAR, -1);
      fromDate = new java.sql.Date(cal.getTime().getTime());     
     
    //  logEntry("MonthlyMerchCommEntity execute()", "Monthly MerchCommEntity for the period  " + fromDate.toString() + " to " + toDate.toString());
      generateMerchCommEntityDataBean(fromDate, toDate);
    } catch (Exception e) {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    } 
    return (true);
  }

  public Date getManualActiveDate() {
    return manualActiveDate;
  }

  public void setManualActiveDate(Date manualActiveDate) {
    this.manualActiveDate = manualActiveDate;
  }

  private void generateMerchCommEntityDataBean(Date activeDate, Date monthEndDate) {
    log.info("Calling loadData() of MerchCommEntityDataBean");    
    MerchCommEntityDataBean merchCommEntityDataBean = new MerchCommEntityDataBean();
    merchCommEntityDataBean.loadData(activeDate, monthEndDate);    
  }

  public static void main(String[] args) {  
   // com.mes.support.HttpHelper.setTestServer(true);

    try {
      if (args.length > 0 && args[0].equals("testproperties")) {
        EventBase.printKeyListStatus(new String[]{
                MesDefaults.MERCH_PSP_FROM_EMAIL_ADDRESS,
                MesDefaults.MERCH_COMM_ENTITY_TO_EMAIL_ADDRESS,
                MesDefaults.MERCH_PSP_CC_PROJECT_MANAGER_EMAIL_ADDRESS
        });
      }
    } catch (Exception e) {
      log.error(e.toString());
    }

    MonthlyMerchCommEntity monthlyMerchCommEntity = new MonthlyMerchCommEntity();
    if (args.length > 0 && "execute".equals(args[0])) {
      monthlyMerchCommEntity.setManualActiveDate((args.length > 1) ? new Date(DateTimeFormatter.parseDate(args[1], "MM/dd/yyyy").getTime()) : null);
      System.out.println("Calling Execute()");      
      monthlyMerchCommEntity.execute();
      System.exit(0);
    } else {
      System.out.println("Invalid Command");
      System.exit(1);
    }

  }

}/*@lineinfo:generated-code*/