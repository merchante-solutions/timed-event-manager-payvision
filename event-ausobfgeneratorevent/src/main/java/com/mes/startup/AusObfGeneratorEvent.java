package com.mes.startup;

import org.apache.log4j.Logger;
import com.mes.aus.file.job.ObfGeneratorJob;

public final class AusObfGeneratorEvent extends EventBase {

  static Logger log = Logger.getLogger(AusObfGeneratorEvent.class);

  public boolean execute() {
  
    try {
      String eventArg0 = getEventArg(0);
      log.info("EventArg0: " + eventArg0);
      boolean testFlag = "test".equals(getEventArg(0).toLowerCase());
      String sysStr = getEventArg(1);
      log.info("EventArg1: " + sysStr);
      (new ObfGeneratorJob(sysStr,testFlag)).run();
      log.info("Run event instance OK");
      return true;
    }
    catch (Exception e) {
    
      log.error("AUS outbound file generator event error: ", e);
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }

    return false;
  }
  
  
  public static void main(String[] args) {
  
    int exitVal = 0;
    
    try {
    
      AusObfGeneratorEvent event = new AusObfGeneratorEvent();
      event.setEventArgs(args);
      event.execute();
    }
    catch (Exception e) {
    
      log.error("Error executing event",e);
      exitVal = -1;
    }
    
    System.exit(exitVal);
  }
  
}