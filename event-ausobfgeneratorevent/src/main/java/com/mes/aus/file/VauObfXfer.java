package com.mes.aus.file;

import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.jscape.inet.ssh.util.SshParameters;
import com.mes.aus.Notifier;
import com.mes.aus.OutboundFile;

public class VauObfXfer extends AutomatedProcess {
	static Logger log = Logger.getLogger(VauObfXfer.class);
	private boolean testFlag;

	public VauObfXfer(boolean directFlag, boolean testFlag) {
		super(directFlag);
		this.testFlag = testFlag;
	}
	public boolean isTest() {
		return testFlag;
	}
	public void transfer(OutboundFile obf) {

		try {
			SshParameters sshParms = new SshParameters(db.getVauEndpointHost(), db.getVauEndpointUser(), db.getVauEndpointPassword());

			Sftp sftp = new Sftp(sshParms);
			sftp.connect();
			sftp.setDir(db.getVauObXferLoc());
			// VAU seems to handle the file just fine when
			// transferred in binary mode, and ascii mode
			// makes this transfer VERY slow
			sftp.setBinary();

			// upload outbound file
			sftp.upload(obf.getInputStream(), obf.getFileName());

			// create flag file
			byte[] flagData = new byte[0];
			sftp.upload(flagData, obf.getFileName() + ".flg");

			sftp.disconnect();

			// flag outbound file as delivered
			db.setOutboundFileDeliveryFlag(obf.getObId(), true);

			Notifier.notifyDeveloper("VAU Outbound File Transfer", "VAU outbound file transferred: " + obf, directFlag);
		}
		catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "transfer()");
		}
	}
}