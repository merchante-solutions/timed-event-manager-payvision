package com.mes.aus.file;

import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.aus.Notifier;
import com.mes.aus.OutboundFile;
import com.mes.aus.RequestFile;
import com.mes.aus.RequestFileBatch;
import com.mes.aus.RequestRecord;
import com.mes.aus.RequestRecordHandler;
import com.mes.aus.SysUtil;

public class DauObfGenerator extends AutomatedProcess implements RequestRecordHandler {
	static Logger log = Logger.getLogger(DauObfGenerator.class);

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	private OutboundFile obf;
	private OutputStream out;
	private String processorId;
	private String curAcquirerId;
	private int batchDtlCount;
	private int fileDtlCount;
	private String curFileId;
	private boolean testFlag;

	public DauObfGenerator(boolean directFlag, boolean testFlag) {
		super(directFlag);
		this.testFlag = testFlag;
	}
	public boolean isTest() {
		return testFlag;
	}
	/**
	 * MES processor ID with Discover for use in the file header.
	 *
	 * 610069
	 */
	private String getProcessorId() {
		if (processorId == null) {
			// TODO: load from db here...
			processorId = "610069";
		}
		return processorId;
	}
	private void write(String data) throws Exception {
		if (out == null) {
			out = obf.getOutputStream();
		}
		out.write(data.getBytes());
	}

	/**
	 * File Header Record
	 *
	 * 1-2 2 byte AN Req. Record Type '01'
	 *
	 * 3-13 11 byte N Req. Client ID MES Processor ID (610069)
	 *
	 * 14-17 4 byte AN Req. Role Class Ident. 'TPAP' - Acquirer Processor
	 *
	 * MES is considered an acquirer processor (TPAP - indicates processor id in
	 * client id), not an acquirer (TPA - indicates acquirer id in client id)
	 *
	 * 18-32 15 byte AN Req. Reserved space filled
	 *
	 * 33-37 5 byte AN Req. Version Indicator '03092'
	 *
	 * 38-45 8 byte N Req. Processing Date YYYYMMDD
	 *
	 * 46-55 10 byte AN Opt. Client Usage DAU File Seq Number (may be space
	 * filled)
	 *
	 * Limit of 10 bytes, currently storing the DAU file sequence number to tie
	 * inbound file back to outbound file. Would prefer the outbound ID, but
	 * this will be problematic when the outbound ID exceeds 10 digits (max 12).
	 *
	 * 56 1 byte AN Req. Reserved Space filled
	 *
	 * 57-350 294 byte AN Req. Filler Space filled
	 *
	 */
	private void generateFileHeader() throws Exception {
		// record type, '01'
		write("01");

		// client id, MES Discover processor id 610069
		write(GenUtil.zeroPad(getProcessorId(), 11));

		// role class identifier, 'TPAP' = acquirer processor
		write("TPAP");

		// filler, 15 bytes
		write(GenUtil.spaces(15));

		// version indicator, '03092'
		write("03092");

		// processing date, YYYYMMDD
		write(sdf.format(obf.getCreateDate()));

		// client useage, use file seq number
		write(GenUtil.padRight("" + obf.getFileSeqId(), 10));

		// filler
		write(GenUtil.spaces(295));

		// end of line
		write("\n");
	}

	/**
	 * Batch Header Record
	 *
	 * 1-2 2 byte AN Req. Record Type '02'
	 *
	 * 3-13 11 byte N Req. Acquirer ID 650436 3941 Mes Direct 650495 3858 CB&T
	 * Portfolio 650494 3943 Well Fargo 650502 3942 Sterling Savings
	 *
	 * DAU acquirer ID is available in request file object.
	 *
	 * 14-28 15 byte AN Req. Reserved space filled
	 *
	 * 29-350 322 byte AN Req. Filler space filled
	 *
	 */
	private void generateBatchHeader(String acquirerId) throws Exception {
		// reset batch detail count, set cur acquirer id
		batchDtlCount = 0;
		curAcquirerId = acquirerId;

		// record type, '02'
		write("02");

		// acquirer id
		write(GenUtil.zeroPad(acquirerId, 11));

		// filler
		write(GenUtil.spaces(337));

		// end of line
		write("\n");
	}

	/**
	 * Detail Record
	 *
	 * 1-2 2 byte AN Req. Record Type '05'
	 *
	 * 3-17 15 byte N Req. Discover Merch ID Discover merchant ID
	 *
	 * 18-36 19 byte AN Req. Account Number Discover account number
	 *
	 * 37-40 4 byte AN Req. Exp. Date Expiration date, MMYY
	 *
	 * 41-80 40 byte AN Opt. Discretionary Data 1-12 reqfId, 13-24 reqId, 25-36
	 * obId
	 *
	 * 81-350 270 byte AN Req. Filler space filled
	 *
	 */

	private void generateDetail(RequestFile reqf, RequestRecord request) throws Exception {
		// generate header if on first detail in batch
		if (batchDtlCount == 0) {
			generateBatchHeader(curFileId);
		}

		// increment batch detail count
		batchDtlCount++;

		// record type, '05'
		write("05");

		// discover merchant id
		write(GenUtil.padRight(reqf.getDiscMerchNum(), 15));

		// discover card num
		write(GenUtil.padRight(request.getAccountNum(), 19));

		// exp date MMYY
		write(request.getExpDate().substring(2));
		write(request.getExpDate().substring(0, 2));

		// discretionary data
		write(GenUtil.padLeft("" + request.getReqfId(), 12));
		write(GenUtil.padLeft("" + request.getReqId(), 12));
		write(GenUtil.padLeft("" + obf.getObId(), 12));
		write(GenUtil.spaces(4));

		// filler
		write(GenUtil.spaces(270));

		// end of line
		write("\n");
	}

	/**
	 * Called from db to process request records.
	 */
	public void handleRequests(RequestFile reqf, List requests) throws Exception {
		for (Iterator i = requests.iterator(); i.hasNext();) {
			generateDetail(reqf, (RequestRecord) i.next());
		}
	}

	/**
	 * Call db method to process request file request records into details.
	 */
	private void generateDetailSet(RequestFile reqf) throws Exception {
		db.handleRequests(this, reqf, SysUtil.SYS_DAU);
	}

	private void generateBatchTrailer() throws Exception {
		fileDtlCount += batchDtlCount;
		write("09");
		write(GenUtil.zeroPad(curAcquirerId, 11));
		// write(GenUtil.padRight(curAcquirerId,11));
		write(GenUtil.spaces(15));
		write(GenUtil.padRight("" + batchDtlCount, 9));
		write(GenUtil.spaces(313));
		write("\n");
	}
	private void generateFileTrailer() throws Exception {
		write("99");
		write(GenUtil.zeroPad(getProcessorId(), 11));
		// write(GenUtil.padRight(getProcessorId(),11));
		write("TPAP");
		write(GenUtil.spaces(15));
		write(GenUtil.padRight("" + fileDtlCount, 9));
		write(GenUtil.spaces(309));
		write("\n");
	}
	/**
	 * Create a new outbound file object, initialize file counters.
	 */
	private void startNewFile() throws Exception {
		obf = new OutboundFile();
		obf.setObId(db.getNewId());
		obf.setCreateDate(Calendar.getInstance().getTime());
		obf.setSysCode(SysUtil.SYS_DAU);
		obf.setFileSeqId(Long.parseLong(db.getDauFileSeqNum(testFlag)));
		obf.setFileName("dau-" + obf.getFileSeqId() + (testFlag ? "-test" : "") + ".txt");
		obf.setTestFlag(testFlag ? "y" : "n");
		// reset detail counts and batch acquirer id
		fileDtlCount = 0;
		batchDtlCount = 0;
		curAcquirerId = null;
	}
	public OutboundFile generateFile(List batches) {
		try {
			// don't generate empty test files
			if (batches.isEmpty()) {
				return null;
			}
			startNewFile();
			generateFileHeader();

			// for each batch generate batch detail set
			for (Iterator b = batches.iterator(); b.hasNext();) {
				RequestFileBatch batch = (RequestFileBatch) b.next();
				for (Iterator i = batch.getReqfList().iterator(); i.hasNext();) {
					RequestFile reqf = (RequestFile) i.next();
					curFileId = batch.getObfType().getFileId();
					batchDtlCount = 0;
					generateDetailSet(reqf);
					if (batchDtlCount > 0) {
						generateBatchTrailer();
					}
					obf.addRfMapping(reqf);
				}
			}
			generateFileTrailer();
			try {
				out.flush();
			}
			catch (Exception e) {}
			try {
				out.close();
			}
			catch (Exception e) {}

			// create outbound file even if empty, need to insert file
			// to generate reqf -> obf mappings to flag reqf file as
			// processed
			//
			// HACK: passing null for outbound file type since it's not
			// used for DAU outbound files...
			//
			if (!db.insertOutboundFile(obf, null, true)) {
				throw new RuntimeException("Error inserting outbound file");
			}
			Notifier.notifyDeveloper("DAU Outbound File Generation", "DAU outbound file generated: " + obf, directFlag);
			return obf;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "generateFile()");
		}
		finally {
			try {
				out.close();
			}
			catch (Exception e) {}
		}
		return null;
	}
}