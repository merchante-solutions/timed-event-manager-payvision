package com.mes.aus.file;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.jscape.inet.ssh.util.SshParameters;
import com.mes.aus.Notifier;
import com.mes.aus.OutboundFile;
import com.mes.support.config.CryptCalloutConfig;

public class DauObfXfer extends AutomatedProcess {
	static Logger log = Logger.getLogger(DauObfXfer.class);
	private boolean testFlag;
	private OutboundFile obf;

	public DauObfXfer(boolean directFlag, boolean testFlag) {
		super(directFlag);
		this.testFlag = testFlag;
	}
	public DauObfXfer(OutboundFile obf, boolean directFlag, boolean testFlag) {
		this(directFlag, testFlag);
		this.obf = obf;
	}
	public boolean isTest() {
		return testFlag;
	}
	/**
	 * Send a file to DAU server via sftp.
	 */
	private void sendFileToDiscover(File file) throws Exception {
		Sftp sftp = null;
		try {
			log.debug("dau endpoint host: " + db.getDauEndpointHost(testFlag));
			log.debug("dau ob endpoint user: " + db.getDauObEndpointUser());
			log.debug("dau ob endpoint password: " + db.getDauObEndpointPassword());
			log.debug("dau ob xfer loc: " + db.getDauObXferLoc());
			SshParameters sshParms = new SshParameters(
					db.getDauEndpointHost(testFlag), db.getDauObEndpointUser(),
					db.getDauObEndpointPassword());
			sftp = new Sftp(sshParms);
			sftp.connect();
			sftp.setDir(db.getDauObXferLoc());
			sftp.setBinary();
			log.debug("uploading file: " + file);
			sftp.upload(file);
			
			//Flag file for discover outbound
			
			int offset          = -1;
			String flagFilename = null;
			String postFix = ".flg";
			if(testFlag){
				postFix = ".tst";
			}
			
			// chop the extension from the data filename to build the flag filename
			
		      offset = file.getName().lastIndexOf('.');
		      if ( offset < 0 )
		      {
		        flagFilename  = file.getName() + postFix;
		      }
		      else
		      {
		        flagFilename  = file.getName().substring(0,offset) + postFix;
		      }
		      
		      byte[] fileData = file.getName().getBytes();
		      
		      // send the flag file
		      log.debug("uploading file: " + flagFilename);
		      sftp.upload(fileData, flagFilename);
			
			log.debug("done.");
		}
		finally {
			try {
				sftp.disconnect();
			}
			catch (Exception e) {}
		}
	}
	/**
	 * Write input stream to a file.
	 */
	private void writeToFile(InputStream in, File f) throws Exception {
		OutputStream out = null;

		try {
			out = new FileOutputStream(f);
			byte[] inBuf = new byte[5000];
			int readCount = 0;
			while ((readCount = in.read(inBuf)) != -1) {
				out.write(inBuf, 0, readCount);
			}
		}
		finally {
			try {
				out.flush();
			}
			catch (Exception e) {}
			try {
				out.close();
			}
			catch (Exception e) {}
		}
	}
	/**
	 * Make call to system to create a PGP encrypted file containing the data
	 * from the given input stream.
	 */
	private File encryptFile(InputStream in, String sourceName)	throws Exception {
		// write stream to temporary file
		File rawFile = new File(sourceName);
		writeToFile(in, rawFile);

		// create reference to encrypted file, delete
		// it if it's already present to allow new
		// encrypted file to be created
		String pgpFileName = "" + rawFile + ".pgp";
		File pgpFile = new File(pgpFileName);
		if (pgpFile.exists()) {
			log.debug("Deleting older file: " + pgpFile);
			pgpFile.delete();
		}

		// issue external command to generate encrypted file
		// String[] cmd = { "/bin/ksh","-c","gpg -a -r DFS_GXS --no-tty -o " +
		// pgpFile
		// + " -e " + rawFile + " 2>> dau-gpg.log" };
		CryptCalloutConfig cfg = new CryptCalloutConfig();
		String[] cmd = cfg.getEncryptRuntimeStrs("aus-discover", "" + rawFile, "" + pgpFile);
		log.debug("Executing command: " + cmd[0] + ", " + cmd[1] + ", " + cmd[2]);
		Process process = Runtime.getRuntime().exec(cmd);
		process.waitFor();
		log.debug("Encryption command completed.");

		// make sure file was generated
		if (!pgpFile.exists()) {
			throw new Exception("File encryption failed for outbound file: " + sourceName);
		}
		return pgpFile;
	}
	/**
	 * Encrypt outbound file data. Returns encrypted file handle.
	 */
	private File encryptOutboundFile(OutboundFile obf) throws Exception {
		return encryptFile(obf.getInputStream(), obf.getFileName());
	}

	/**
	 * Transfer a file to Discover DAU server.
	 */
	private void transfer() {
		if (obf == null) {
			throw new NullPointerException("Outbound file must be specified in constructor or transfer call");
		}

		File pgpFile = null;
		try {
			pgpFile = encryptOutboundFile(obf);
			sendFileToDiscover(pgpFile);
			db.setOutboundFileDeliveryFlag(obf.getObId(), true);
			Notifier.notifyDeveloper("DAU Outbound File Transfer", "DAU outbound file transferred: " + obf, directFlag);
		}
		catch (Exception e) {
			log.error("Error: " + e);
			e.printStackTrace();
			notifyError(e, "transfer()");
		}
		finally {
			try {
				pgpFile.delete();
			}
			catch (Exception e) {}
			try {
				obf.release();
			}
			catch (Exception e) {}
		}
	}
	public void transfer(OutboundFile obf) {
		this.obf = obf;
		transfer();
	}
	public void run() {
		transfer();
	}
}
