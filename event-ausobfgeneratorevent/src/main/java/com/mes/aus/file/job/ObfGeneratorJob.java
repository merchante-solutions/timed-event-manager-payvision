package com.mes.aus.file.job;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import com.mes.aus.OutboundFile;
import com.mes.aus.RequestFileBatch;
import com.mes.aus.SysUtil;
import com.mes.aus.file.AbuObfGenerator;
import com.mes.aus.file.DauObfGenerator;
import com.mes.aus.file.DauObfXfer;
import com.mes.aus.file.VauObfGenerator;
import com.mes.aus.file.VauObfXfer;

public class ObfGeneratorJob extends Job {
	static Logger log = Logger.getLogger(ObfGeneratorJob.class);
	private String sysStr;
	private boolean testFlag;

	public ObfGeneratorJob(String sysStr, boolean testFlag, boolean directFlag) {
		super(directFlag);
		this.sysStr = sysStr;
		this.testFlag = testFlag;
		log.info("2 - SysStr: " + this.sysStr + " - testFlag: " + testFlag);
	}
	public ObfGeneratorJob(String sysStr, boolean testFlag) {
		// default direct flag to true
		this(sysStr, testFlag, true);
		log.info("1 - SysStr: " + this.sysStr + " - testFlag: " + testFlag);
	}
	private void validateSysCode(String sysCode) {
		log.info("Begin validateSysCode");
		if (!SysUtil.SYS_ABU.equals(sysCode) && !SysUtil.SYS_VAU.equals(sysCode) && !SysUtil.SYS_DAU.equals(sysCode)) {
			throw new RuntimeException("Invalid system code: '" + sysCode + "'");
		}
		log.info("End validateSysCode");		
	}

	public void run() {
		try {
			log.info("Begin ObfGeneratorJob run");
			
			// validate systems, parse into list
			Pattern p = Pattern.compile("([^,; ]+)[,; ]?");
			Matcher m = p.matcher(sysStr);
			List sysCodeList = new ArrayList();
			while (m.find()) {
				String sysCode = m.group(1);
				// validateSysCode(sysCode);
				SysUtil.validateSysCode(sysCode);
				sysCodeList.add(sysCode);
			}

			// generate outbound files for each specified system
			for (Iterator i = sysCodeList.iterator(); i.hasNext();) {
				String sysCode = (String) i.next();
				log.info("Generating outbound files for system code " + sysCode);

				List batches = db.getOutboundRequestFileBatches(sysCode, testFlag);
				log.info("Found " + batches.size() + " request file batches to process");

				// ABU - mc account updater
				if (SysUtil.SYS_ABU.equals(sysCode)) {
					try {
						// generate abu outbound files for each batch
						for (Iterator b = batches.iterator(); b.hasNext();) {
							RequestFileBatch batch = (RequestFileBatch) b.next();
							log.info("Processing MC request file batch:\n" + batch);
							(new AbuObfGenerator(directFlag, testFlag)).generateFile(batch);
						}
					}
					catch (Exception ie) {
						log.error("Error: " + ie);
						notifyError(ie, "run(inner,ABU)");
					}
				}

				// VAU - visa account updater
				else if (SysUtil.SYS_VAU.equals(sysCode)) {
					String progress = "generator";
					try {
						// generate vau outbound files for each batch
						for (Iterator b = batches.iterator(); b.hasNext();) {
							RequestFileBatch batch = (RequestFileBatch) b.next();
							log.info("Processing VS request file batch:\n" + batch);
							OutboundFile obf = (new VauObfGenerator(directFlag, testFlag)).generateFile(batch);

							// if file was generated, transfer it to visa endpoint server
							if (obf != null) {
								progress = "transfer";
								(new VauObfXfer(directFlag, testFlag)).transfer(obf);
							}
						}
					}
					catch (Exception ie) {
						log.error("Error: " + ie);
						notifyError(ie, "run(inner,VAU," + progress + ")");
					}
				}

				// DAU - discover account updater
				else {
					String progress = "generator";
					try {
						log.info("Processing DC batches for DAU");
						// generate dau outbound file for all batches
						OutboundFile obf = (new DauObfGenerator(directFlag, testFlag)).generateFile(batches);

						// if file was generated, transfer it to discover endpoint server
						if (obf != null) {
							progress = "transfer";
							(new DauObfXfer(directFlag, testFlag)).transfer(obf);
						}
					}
					catch (Exception ie) {
						log.error("Error: " + ie);
						notifyError(ie, "run(inner,DAU," + progress + ")");
					}
				}
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "run()");
		}
	}
}