package com.mes.aus.file;

import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.aus.Notifier;
import com.mes.aus.OutboundFile;
import com.mes.aus.OutboundFileType;
import com.mes.aus.RequestFile;
import com.mes.aus.RequestFileBatch;
import com.mes.aus.RequestRecord;
import com.mes.aus.RequestRecordHandler;
import com.mes.aus.SysUtil;

public class AbuObfGenerator extends AutomatedProcess implements RequestRecordHandler {
	static Logger log = Logger.getLogger(AbuObfGenerator.class);

	private SimpleDateFormat sdf = new SimpleDateFormat("MMddyy");
	private OutboundFile obf;
	private OutputStream out;
	private String memberId;
	private int detailCount;
	private boolean testFlag;

	public AbuObfGenerator(boolean directFlag, boolean testFlag) {
		super(directFlag);
		this.testFlag = testFlag;
	}

	public boolean isDirect() {
		return directFlag;
	}

	public boolean isTest() {
		return testFlag;
	}

	// OBSOLETE
	private String getMemberId() {
		if (memberId == null) {
			// TODO: load from db here...
			// memberId = "11606";
			memberId = "5185"; // temp testing id (cb&t ica)
		}
		return memberId;
	}

	private void write(String data) throws Exception {
		out.write(data.getBytes());
	}

	/**
	 * Header Record
	 *
	 * 1 1 AN Header Identifier
	 *
	 * Value H
	 *
	 * 2–21 20 AN Filler
	 *
	 * Spaces
	 *
	 * 22–32 11 N Acquirer MasterCard Member ID
	 * 
	 * Acquirer MasterCard Member ID: • Must be numeric • Must already exist on
	 * current, active acquirer list maintained at MasterCard
	 *
	 * 33–38 6 N File Date
	 * 
	 * Date file sent to MasterCard: • DDMMYYYY format
	 *
	 * (NOTE: this is 8 digits, conflicts with stated field length)
	 *
	 * CORRECTION: format is MMDDYY
	 *
	 * 39–89 51 AN Filler
	 * 
	 * Spaces
	 */
	private void generateHeader(OutboundFileType obft) throws Exception {
		// header identifier
		write("H");

		// filler
		write(GenUtil.spaces(20));

		// acquirer mastercard member id
		write(GenUtil.zeroPad(obft.getFileId(), 11));

		// file date
		write(sdf.format(Calendar.getInstance().getTime()));

		// filler
		write(GenUtil.spaces(51));

		// end of line
		write("\n");
	}

	private void generateDummyDetail() throws Exception {
		// record identifier
		write("D");

		// merchant ID
		write(GenUtil.zeroPad(db.getAbuDummyMerchNum(), 15));

		// discretionary data 1: ob_id
		// (this is the point of the dummy detail)
		write(GenUtil.padLeft("" + obf.getObId(), 10));

		// dummy account number
		write(GenUtil.padRight("5000000000000000", 19));

		// dummy exp date
		write(GenUtil.padRight("0000", 4));

		// discretionary data 2
		// 1-10 reqf_id, 11-20 req_id
		write(GenUtil.padLeft("", 20));

		// filler
		write(GenUtil.spaces(20));

		// end of line
		write("\n");
	}

	/**
	 * Detail Record
	 *
	 * 1 1 AN Detail Identifier
	 * 
	 * Value D
	 * 
	 * 2–16 15 N Merchant ID
	 * 
	 * Valid Merchant ID, previously registered with MasterCard
	 * 
	 * 17–26 10 AN Acquirer Discretionary Data 1
	 * 
	 * • For use by acquirer • Data in this field is returned to the acquirer •
	 * May be used for matching to original merchant inquiry
	 * 
	 * 27–45 19 AN Old Account Number
	 * 
	 * Account Number requested for matching: • Must be left-justified, numeric
	 * • Must be space-filled to the right • Must not contain embedded spaces •
	 * Must be 13–19 positions
	 * 
	 * 46–49 4 N Old Expiration Date
	 * 
	 * Must be in YYMM format.
	 * 
	 * 50–69 20 AN Acquirer Discretionary Data 2
	 * 
	 * 1. For use by acquirer 2. Data in this field is returned to the acquirer
	 * 
	 * May be used for matching to original merchant inquiry
	 * 
	 * 70–89 20 AN Filler
	 * 
	 * Spaces
	 * 
	 */
	private void generateDetail(RequestFile reqf, RequestRecord request) throws Exception {
		// record identifier
		write("D");

		// merchant ID
		write(GenUtil.zeroPad(reqf.getMerchNum(), 15));

		// discretionary data 1
		// 1-10 ob_id
		write(GenUtil.padLeft("" + obf.getObId(), 10));

		// old account number
		write(GenUtil.padRight(request.getAccountNum(), 19));

		// old exp date
		write(GenUtil.padRight(request.getExpDate(), 4));

		// discretionary data 2
		// 1-10 reqf_id, 11-20 req_id
		write(GenUtil.padLeft("" + request.getReqfId(), 10));
		write(GenUtil.padLeft("" + request.getReqId(), 10));

		// filler
		write(GenUtil.spaces(20));

		// end of line
		write("\n");
		++detailCount;
	}

	/**
	 * Called from db to process request records.
	 */
	public void handleRequests(RequestFile reqf, List requests)
			throws Exception {
		for (Iterator i = requests.iterator(); i.hasNext();) {
			generateDetail(reqf, (RequestRecord) i.next());
		}
	}

	/**
	 * Call db method to process request file request records into details.
	 */
	private void generateDetailSet(RequestFile reqf) throws Exception {
		db.handleRequests(this, reqf, SysUtil.SYS_ABU);
	}

	/**
	 * 1 1 AN Trailer Identifier
	 * 
	 * Value T
	 * 
	 * 2–12 11 N Acquirer MasterCard Member ID
	 * 
	 * Must match the header Acquirer MasterCard Member ID
	 * 
	 * 13–21 9 N Record Count
	 * 
	 * Total number of records in the file for the acquirer MasterCard Member ID
	 * in the header, including header and trailer record
	 * 
	 * 22–89 68 AN Filler
	 * 
	 * Spaces
	 */
	private void generateTrailer() throws Exception {
		// record indicator, detail = '1'
		write("T");

		// segment id
		// TODO: figure out what this id is
		write(GenUtil.zeroPad(memberId, 11));

		// record count
		write(GenUtil.zeroPad("" + (detailCount + 2), 9));

		// filler
		write(GenUtil.padLeft("", 68));

		// end of line
		write("\n");
	}

	private void startNewFile(OutboundFileType obft) throws Exception {
		obf = new OutboundFile();
		obf.setObId(db.getNewId());
		obf.setCreateDate(Calendar.getInstance().getTime());
		obf.setSysCode(SysUtil.SYS_ABU);
		obf.setFileType(obft.getFileType());
		obf.setFileSeqId(Long.parseLong(db.getAbuFileSeqNum(testFlag)));
		// TODO: make this more dynamic
		obf.setFileName("abu-" + (testFlag ? "test-" : "") + "out-" + obf.getFileSeqId() + ".txt");
		obf.setTestFlag(testFlag ? "y" : "n");
		detailCount = 0;
		out = obf.getOutputStream();
	}

	public OutboundFile generateFile(RequestFileBatch batch) {
		try {
			// don't generate empty test files
			List reqfList = batch.getReqfList();
			if (reqfList.isEmpty() && isTest()) {
				return null;
			}

			startNewFile(batch.getObfType());
			generateHeader(batch.getObfType());

			for (Iterator i = reqfList.iterator(); i.hasNext();) {
				RequestFile reqf = (RequestFile) i.next();
				generateDetailSet(reqf);
				obf.addRfMapping(reqf);
			}

			// if no details generated from request files and configured,
			// generate dummy detail to make sure obId is available in response file
			if (detailCount == 0 && "y".equals(db.getAbuDummyDetailFlag().toLowerCase())) {
				generateDummyDetail();
				++detailCount;
			}
			generateTrailer();

			try {
				out.flush();
			}
			catch (Exception e) {}
			try {
				out.close();
			}
			catch (Exception e) {}

			// flag outbound file as already delivered if detail count is 0
			// to prevent delivering an empty file to
			obf.setDeliveryFlag(detailCount == 0 ? obf.FLAG_YES : obf.FLAG_NO);

			// create outbound file even if empty, need to insert file
			// to generate reqf -> obf mappings to flag reqf file as
			// processed
			if (!db.insertOutboundFile(obf, batch.getObfType(), detailCount > 0)) {
				throw new RuntimeException("Error inserting outbound file");
			}

			Notifier.notifyDeveloper("ABU Outbound File Generation","ABU outbound file generated: " + obf, directFlag);
			return obf;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "generateFile()");
		}
		finally {
			try {
				out.close();
			}
			catch (Exception e) {}
		}
		return null;
	}
}