/*@lineinfo:filename=AmexESAEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/AmexESAEvent.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000-2004,2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.StringReader;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.flatfile.FlatFileRecordField;
import com.mes.net.MailMessage;
import com.mes.ops.AmexESADataBean;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;
import com.mes.support.StringUtilities;
import sqlj.runtime.ResultSetIterator;
import wsdl.com.americanexpress.merchant.esa.connect.internetservice.H2HServiceSOA.IH2HServiceServiceLocator;
import wsdl.com.americanexpress.merchant.esa.connect.internetservice.H2HServiceSOA.IH2HServiceSoapBindingStub;

public class AmexESAEvent extends EventBase
{
  static Logger log = Logger.getLogger(AmexESAEvent.class);
  
  public  static final int          SOAP_RETRY_COUNT    = 5;
  public  static final int          ZIP_BUFFER_LEN      = 4096;
  
  private int               FileBatchId           = 0;
  private int               FileRecordCount       = 0;
  private Timestamp         FileTimestamp         = null;
  private PropertiesFile    PropsFile             = null;
  private String            WorkFilename          = null;
  
  private boolean           TestMode              = false;
  private int               TestBatchId            = 0;
  
  protected final String[] ErrorFieldNames =
  {
    "amex_error_code_1",
    "amex_error_code_2",
    "amex_error_code_3",
    "amex_error_code_4",
    "amex_error_code_5",
    "amex_error_code_6",
    "amex_error_code_7",
    "amex_error_code_8",
    "amex_error_code_9",
    "amex_error_code_10",
  };
  
  protected class OrderStatus
  {
    public long           AppSeqNum       = 0L;
    public String         DbaName         = "";
    public long           MerchantId      = 0L;
    public String         SeNumber        = "";
    public int            SourceCode      = -1;
    public int            StatusCode      = -1;
    public String         StatusDesc      = "";
    
    public OrderStatus( int sourceCode, long orderId, int status )
    {
      MerchantId  = orderId;
      SourceCode  = sourceCode;
      StatusCode  = status;
      
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:109^9*/

//  ************************************************************
//  #sql [Ctx] { select  sc.status_desc_short 
//            from    amex_esa_setup_status_codes   sc
//            where   sc.status_code = :StatusCode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sc.status_desc_short  \n          from    amex_esa_setup_status_codes   sc\n          where   sc.status_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.AmexESAEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,StatusCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   StatusDesc = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:114^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:116^9*/

//  ************************************************************
//  #sql [Ctx] { select  mr.app_seq_num,
//                    nvl(mf.dba_name,mr.merch_business_name)
//            
//            from    merchant    mr,
//                    mif         mf
//            where   mr.merch_number = :MerchantId and
//                    mf.merchant_number(+) = mr.merch_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mr.app_seq_num,\n                  nvl(mf.dba_name,mr.merch_business_name)\n           \n          from    merchant    mr,\n                  mif         mf\n          where   mr.merch_number =  :1  and\n                  mf.merchant_number(+) = mr.merch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.AmexESAEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   AppSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   DbaName = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:125^9*/
      }
      catch( Exception e )
      {
        logEntry("OrderStatus(" + orderId + "," + status + ")",e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    
    public void setSeNumber( String seNumber )
    {
      SeNumber = seNumber;
    }
  }
  
  public AmexESAEvent( )
  {
    PropertiesFilename = "amex-esa.properties";
    
    try
    {
      PropsFile = new PropertiesFile( PropertiesFilename );
    }
    catch( Exception e) 
    {
    }
  }

  protected void batchStatusRecover( String batchId )
  {
    Vector                      params      = new Vector();
    String                      resp        = null;
    IH2HServiceSoapBindingStub  service     = null;
    String                      sourceCode  = null;
  
    try
    {
      service = initService();
      
      try
      {
        resp = service.recoverBatchStatus( batchId );
        handleBatchStatusResponse(resp,true);
      }
      catch( Exception rpce )
      {
        handleFault(rpce);
      }
    }
    catch( Exception e )
    {
      logEntry("batchStatusRecover()", e.toString());
    }
    finally
    {
    }
  }    
  
  private void buildBatchStream( StringBuffer buffer, String dataFilename )
  {
    BufferedReader  in              = null;
    String          line            = null;

    try
    {  
      buffer.setLength(0);
      buffer.append("<H2HInput>\n");
      buffer.append("<BatchStream>\n");
      buffer.append("  <Header>\n");
      buffer.append("    <SubmissionDate>");
      buffer.append( DateTimeFormatter.getFormattedDate(FileTimestamp,"dd-MM-yyyy") );
      buffer.append("</SubmissionDate>\n");
      buffer.append("      <SubmissionTime>");
      buffer.append( DateTimeFormatter.getFormattedDate(FileTimestamp,"hh:mm:ss a") );
      buffer.append("</SubmissionTime>\n");
      buffer.append("      <BatchId>");
      buffer.append( FileBatchId );
      buffer.append("</BatchId>\n");
      buffer.append("      <DataRecordCount>");
      buffer.append( FileRecordCount );
      buffer.append("</DataRecordCount>\n");
      buffer.append("    </Header>\n");
      buffer.append("    <DetailOrders><![CDATA[");
    
      // add the data
      in = new BufferedReader( new FileReader( dataFilename ) );
      while( (line = in.readLine()) != null )
      {
        buffer.append(line);
        buffer.append("\n");
      }
      in.close();
    
      buffer.append("]]>");
      buffer.append("</DetailOrders>\n");
      buffer.append("    <Trailer></Trailer>\n");
      buffer.append("  </BatchStream>\n");
      buffer.append("</H2HInput>\n");
    }
    catch( Exception e )
    {
      logEntry("buildBatchStream()",e.toString());
    }    
    finally
    {
      try{ in.close(); } catch(Exception ee) {}
    }
  }
  
  private Document buildDoc( String rawXml )
  {
    Document        doc   = null;
    
    try
    {
      // create xdoc document to parse data
      SAXBuilder builder = new SAXBuilder();
      doc = builder.build(new ByteArrayInputStream(rawXml.getBytes()));
    }
    catch( Exception e )
    {
      logEntry( "buildDoc()", e.toString() );
    }
    return( doc );      
  }
  
  private void buildSetupFile( int batchId )
  {
    AmexESADataBean   dataBean            = null;
    FlatFileRecord    ffd                 = null;
    ResultSetIterator it                  = null;
    BufferedWriter    out                 = null;
    ResultSet         resultSet           = null;
    
    try
    {
      // build the transmission header
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_SETUP);
      ffd.connect(true);
      
      dataBean = new AmexESADataBean();
      
      // select the transactions to process
      /*@lineinfo:generated-code*//*@lineinfo:271^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  esp.app_seq_num     as app_seq_num
//          from    amex_esa_setup_process    esp
//          where   esp.batch_id = :batchId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  esp.app_seq_num     as app_seq_num\n        from    amex_esa_setup_process    esp\n        where   esp.batch_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.AmexESAEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,batchId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.AmexESAEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:276^7*/
      resultSet = it.getResultSet();
      
      // reset the record count
      FileRecordCount = 0;
      
      // setup the file batch id
      FileTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
      FileBatchId   = batchId;
      
      // open the file and output the transmission header
      out = new BufferedWriter( new FileWriter( WorkFilename, false ) );
        
      log.debug("Building '" + WorkFilename + "'");//@
      
      while( resultSet.next() )
      {
        // load the app data into the flat file record
        dataBean.loadAppData(resultSet.getLong("app_seq_num"),ffd);
        
        ffd.setFieldData("global_user_id","");
        ffd.setFieldData("batch_id",FileBatchId);
        out.write( ffd.spew() );
        out.newLine();
        
        ++FileRecordCount;
        
        //System.out.print("Processed: " + FileRecordCount + "\r" );//@
      }
      resultSet.close();
      it.close();        
      out.close();
    }
    catch(Exception e)
    {
      logEntry("buildSetupFile()", e.toString());
    }
    finally
    {
      try { ffd.cleanUp(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
  }

  
  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    try
    {
      // get an automated timeout exempt connection
      connect(true);
      
      // pick up any completed applications 
      setupRequestsReceive();
      
      // send new applications
      setupRequestsSend();
      
      if ( timeToCheckReviewAccounts() )
      {
        reviewAccountsReceive();
      }
    }
    catch( Exception e )
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return( true );
  }
  
  protected long extractOrderId( long value )
  {
    long      orderId       = value;
    int       recCount      = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:363^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(mr.merch_number) 
//          from    merchant    mr
//          where   mr.merch_number = :value
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mr.merch_number)  \n        from    merchant    mr\n        where   mr.merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.AmexESAEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,value);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:368^7*/
      
      if ( recCount == 0 )
      {
        // convert app seq num order id into merchant # order id
        /*@lineinfo:generated-code*//*@lineinfo:373^9*/

//  ************************************************************
//  #sql [Ctx] { select  mr.merch_number 
//            from    merchant mr
//            where   mr.app_seq_num = :value
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mr.merch_number  \n          from    merchant mr\n          where   mr.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.AmexESAEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,value);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   orderId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:378^9*/
      }
    }
    catch( Exception e )
    {
      logEntry( "extractOrderId(" + value + ")", e.toString());
    }
    return( orderId );
  }
  
  protected void handleBatchSetupResponse( String rawXml )
  {
    String          batchId = null;
    String          msg     = null;
    String          status  = null;
    Document        xmlDoc  = null;
    
    try
    {
      xmlDoc = buildDoc( rawXml );
  
      log.debug("message:  " + xmlDoc.getRootElement().getChild("BatchResponse").getChildText("Message"));
      log.debug("batch id: " + xmlDoc.getRootElement().getChild("BatchResponse").getChildText("BatchId"));
      
      msg     = xmlDoc.getRootElement().getChild("BatchResponse").getChildText("Message");
      batchId = xmlDoc.getRootElement().getChild("BatchResponse").getChildText("BatchId");
      
      if( msg.indexOf("Success") < 0 )
      {
        status = String.valueOf( AmexESADataBean.ESA_SOAP_ERROR );
      }
      else
      {
        status = String.valueOf( AmexESADataBean.ESA_PENDING );
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:414^7*/

//  ************************************************************
//  #sql [Ctx] { update  amex_esa_setup_process
//          set     setup_status = :status,
//                  status_date = sysdate
//          where   batch_id = :batchId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  amex_esa_setup_process\n        set     setup_status =  :1 ,\n                status_date = sysdate\n        where   batch_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.AmexESAEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,status);
   __sJT_st.setString(2,batchId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:420^7*/
    }
    catch( Exception e )
    {
      logEntry("handleBatchSetupResponse()",e.toString());
    }
  }
  
  protected void handleBatchStatusResponse( String rawXml )
  {
    handleBatchStatusResponse( rawXml, false );
  }
  
  protected void handleBatchStatusResponse( String rawXml, boolean incompleteOnly )
  {
    StringBuffer    errorCodes    = new StringBuffer();
    String          errorCode     = null;
    FlatFileRecord  ffd           = null;
    BufferedReader  in            = null;
    String          line          = null;
    long            orderId       = 0L;
    Vector          orderStatuses = new Vector();
    String          results       = null;
    String          seNumber      = null;
    int             sourceCode    = -1;
    int             status        = -1;
    OrderStatus     statusObj     = null;
    Document        xmlDoc        = null;
    
    try
    {
      xmlDoc = buildDoc( rawXml );
  
      log.debug("message: " + xmlDoc.getRootElement().getChild("BatchResponse").getChildText("Message"));
    
      ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_SETUP);

      // extract the batch result data
      results = xmlDoc.getRootElement().getChild("BatchStream").getChildText("DetailOrders");
      
      in = new BufferedReader( new StringReader( results ) );
      
      while ( (line = in.readLine()) != null )
      {
        ffd.suck(line);
        
        orderId = extractOrderId( ffd.getFieldAsLong("order_number") );
        
        // extract the current status to use as default
        // in the event that amex returns a bad record
        /*@lineinfo:generated-code*//*@lineinfo:470^9*/

//  ************************************************************
//  #sql [Ctx] { select  setup_status 
//            from    amex_esa_setup_process
//            where   merchant_number = :orderId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  setup_status  \n          from    amex_esa_setup_process\n          where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.AmexESAEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,orderId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   status = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:475^9*/          
        
        // skip accounts that are completed when processing incomplete only
        if ( incompleteOnly && isStatusComplete(status) )
        {
          continue;
        }
        
        // extract the new data from the xml message
        status      = ffd.getFieldAsInt("scout_status_code",status);
        sourceCode  = ffd.getFieldAsInt("source_code");
        seNumber    = ffd.getFieldRawData("amex_se_number").trim();
        
        switch( status )
        {
          case AmexESADataBean.ESA_REVIEW:
            // when an account is leaves review the scout status
            // remains 1 (review) and the application status is
            // found in the contract review indicator field.
            status = ffd.getFieldAsInt("contract_review_ind",status);
            break;
            
          case AmexESADataBean.ESA_DUPLICATE:
            seNumber = ffd.getFieldRawData("duplicate_merchant_number").trim();
            break;
        }
        
        if ( seNumber != null && !seNumber.equals("") )
        {
          // update the application data
          int recCount = 0;
          
          /*@lineinfo:generated-code*//*@lineinfo:507^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(mr.app_seq_num)
//              
//              from    merchant mr,
//                      merchpayoption mpo
//              where   mr.merch_number = :orderId
//                      and mr.app_seq_num = mpo.app_seq_num
//                      and mpo.cardtype_code = :mesConstants.APP_CT_AMEX
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mr.app_seq_num)\n             \n            from    merchant mr,\n                    merchpayoption mpo\n            where   mr.merch_number =  :1 \n                    and mr.app_seq_num = mpo.app_seq_num\n                    and mpo.cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.AmexESAEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,orderId);
   __sJT_st.setInt(2,mesConstants.APP_CT_AMEX);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:516^11*/
          
          if( recCount > 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:520^13*/

//  ************************************************************
//  #sql [Ctx] { update  merchpayoption  mpo
//                set     mpo.merchpo_card_merch_number = :seNumber
//                where   mpo.app_seq_num = 
//                        (
//                          select  mr.app_seq_num
//                          from    merchant      mr
//                          where   mr.merch_number = :orderId
//                        ) and
//                        mpo.cardtype_code = :mesConstants.APP_CT_AMEX -- 16
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchpayoption  mpo\n              set     mpo.merchpo_card_merch_number =  :1 \n              where   mpo.app_seq_num = \n                      (\n                        select  mr.app_seq_num\n                        from    merchant      mr\n                        where   mr.merch_number =  :2 \n                      ) and\n                      mpo.cardtype_code =  :3  -- 16";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.AmexESAEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,seNumber);
   __sJT_st.setLong(2,orderId);
   __sJT_st.setInt(3,mesConstants.APP_CT_AMEX);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:531^13*/
          }
          else
          {
            long  appSeqNum = 0L;
            int   cardSeq   = 0;
            
            try
            {
              /*@lineinfo:generated-code*//*@lineinfo:540^15*/

//  ************************************************************
//  #sql [Ctx] { select  mr.app_seq_num
//                  
//                  from    merchant mr
//                  where   mr.merch_number = :orderId
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mr.app_seq_num\n                 \n                from    merchant mr\n                where   mr.merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.AmexESAEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,orderId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:546^15*/
              
              /*@lineinfo:generated-code*//*@lineinfo:548^15*/

//  ************************************************************
//  #sql [Ctx] { select  max(nvl(card_sr_number,0)+1)
//                  
//                  from    merchpayoption
//                  where   app_seq_num = :appSeqNum
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max(nvl(card_sr_number,0)+1)\n                 \n                from    merchpayoption\n                where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.AmexESAEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cardSeq = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:554^15*/
              
              /*@lineinfo:generated-code*//*@lineinfo:556^15*/

//  ************************************************************
//  #sql [Ctx] { insert into merchpayoption
//                  (
//                    app_seq_num,
//                    cardtype_code,
//                    card_sr_number,
//                    merchpo_card_merch_number
//                  )
//                  values
//                  (
//                    :appSeqNum,
//                    :mesConstants.APP_CT_AMEX,
//                    :cardSeq,
//                    :seNumber
//                  )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merchpayoption\n                (\n                  app_seq_num,\n                  cardtype_code,\n                  card_sr_number,\n                  merchpo_card_merch_number\n                )\n                values\n                (\n                   :1 ,\n                   :2 ,\n                   :3 ,\n                   :4 \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.AmexESAEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_AMEX);
   __sJT_st.setInt(3,cardSeq);
   __sJT_st.setString(4,seNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:572^15*/
            }
            catch(Exception e)
            {
            }
          }

          // update mif_non_bank_cards
          /*@lineinfo:generated-code*//*@lineinfo:580^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//              
//              from    mif_non_bank_cards
//              where   merchant_number = :orderId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n             \n            from    mif_non_bank_cards\n            where   merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.AmexESAEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,orderId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:586^11*/
        
          if(recCount > 0)
          {
            /*@lineinfo:generated-code*//*@lineinfo:590^13*/

//  ************************************************************
//  #sql [Ctx] { update  mif_non_bank_cards
//                set     amex = :seNumber
//                where   merchant_number = :orderId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif_non_bank_cards\n              set     amex =  :1 \n              where   merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.startup.AmexESAEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,seNumber);
   __sJT_st.setLong(2,orderId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:595^13*/
          }
          else
          {
            /*@lineinfo:generated-code*//*@lineinfo:599^13*/

//  ************************************************************
//  #sql [Ctx] { insert into mif_non_bank_cards
//                (
//                  merchant_number,
//                  amex
//                )
//                values
//                (
//                  :orderId,
//                  :seNumber
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mif_non_bank_cards\n              (\n                merchant_number,\n                amex\n              )\n              values\n              (\n                 :1 ,\n                 :2 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.startup.AmexESAEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orderId);
   __sJT_st.setString(2,seNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:611^13*/
          }
        }
        
        // build the error codes list (if any)
        errorCodes.setLength(0);
        for ( int i = 0; i < ErrorFieldNames.length; ++i )
        {
          errorCode = ffd.getFieldRawData(ErrorFieldNames[i]).trim();
          if ( errorCode != null && !errorCode.equals("") )
          {
            if ( errorCodes.length() > 0 )
            {
              errorCodes.append(",");
            }
            errorCodes.append(errorCode);
          }
        }  
        
        log.debug("order number : " + orderId);
        log.debug("scout status : " + status);
        
        /*@lineinfo:generated-code*//*@lineinfo:633^9*/

//  ************************************************************
//  #sql [Ctx] { update  amex_esa_setup_process
//            set     setup_status = :status,
//                    status_date = sysdate,
//                    error_codes = :errorCodes.toString()
//            where   merchant_number = :orderId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3774 = errorCodes.toString();
   String theSqlTS = "update  amex_esa_setup_process\n          set     setup_status =  :1 ,\n                  status_date = sysdate,\n                  error_codes =  :2 \n          where   merchant_number =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.startup.AmexESAEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,status);
   __sJT_st.setString(2,__sJT_3774);
   __sJT_st.setLong(3,orderId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:640^9*/
        
        // collect email data for mes accounts
        if ( sourceCode != AmexESADataBean.SC_BBT ) // ignore BB&T
        {
          statusObj = new OrderStatus(sourceCode,orderId,status);
          statusObj.setSeNumber( seNumber );
          orderStatuses.addElement(statusObj);
        }          
        
        //@+
        StringBuffer        buffer    = new StringBuffer();
        FlatFileRecordField f         = ffd.getFirstField();
        
        while ( f != null )
        {
          buffer.setLength(0);
          buffer.append(f.getNumber());
          while( buffer.length() < 5 ) { buffer.append(" "); }
          buffer.append(f.getName());
          while( buffer.length() < 40 ) { buffer.append(" "); }
          buffer.append(f.getDisplayData());
          log.debug( buffer.toString() );
          
          f = ffd.getNextField();
        }
        //@-
        
        // commit changes for this row
        /*@lineinfo:generated-code*//*@lineinfo:669^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:672^9*/
      }
      
      notifySetupGroup(orderStatuses);
    }
    catch( Exception e )
    {
      logEntry("handleBatchStatusResponse(" + orderId + ")",e.toString());
    }
  }
  
  protected void handleFault( Exception fault )
  {
    StringBuffer      buffer      = new StringBuffer();
    
    try
    {
      buffer.setLength(0);
      buffer.append("SOAP Call Generated a Fault\n\n");
      buffer.append( fault.toString() );
    
      sendStatusEmail(WorkFilename, buffer.toString());
    }
    catch( Exception e )
    {
      logEntry("handleFault()",e.toString());
    }      
  }

  // return an initialized SOAP call object
  protected IH2HServiceSoapBindingStub initService( )
  {
    IH2HServiceServiceLocator   sl      = null;
    IH2HServiceSoapBindingStub  service = null;
  
    try
    {
      sl      =  new IH2HServiceServiceLocator(); 
      service = (IH2HServiceSoapBindingStub)sl.getIH2HService(); 
    }
    catch( Exception e )
    {
      logEntry("initService()",e.toString());
    }
    return( service );
  }
  
  protected boolean inTestMode()
  {
    return( TestMode );
  }
  
  protected boolean isStatusComplete( int status )
  {
    return ( status != AmexESADataBean.ESA_REVIEW &&
             status != AmexESADataBean.ESA_DUP_REVIEW &&
             status != AmexESADataBean.ESA_PENDING );
  }
  
  protected void notifySetupGroup( Vector orderStatuses )
  {
    StringBuffer  subject         = new StringBuffer();
    StringBuffer  body            = new StringBuffer();
    
    try
    {
      if ( orderStatuses != null && orderStatuses.size() > 0 )
      {
        subject.append("Amex ESA Order Status Update");
        
        body.append( StringUtilities.leftJustify("Merchant ID", 18,' ') );
        body.append( StringUtilities.leftJustify("DBA Name", 30,' ') );
        body.append( StringUtilities.leftJustify("Status", 17,' ') );
        body.append( StringUtilities.leftJustify("SE Number", 11,' ') );
        body.append("\n");
        
        body.append( StringUtilities.leftJustify("", 18,'=') );
        body.append( StringUtilities.leftJustify("", 30,'=') );
        body.append( StringUtilities.leftJustify("", 17,'=') );
        body.append( StringUtilities.leftJustify("", 11,'=') );
        body.append("\n");
        
        for ( int i = 0; i < orderStatuses.size(); ++i )
        {
          OrderStatus orderStatus = (OrderStatus) orderStatuses.elementAt(i);
          
          // make sure the filename is in 
          // the message text for error research
          
          body.append( StringUtilities.leftJustify(String.valueOf(orderStatus.MerchantId), 18,' ') );
          body.append( StringUtilities.leftJustify(orderStatus.DbaName, 30,' ') );
          body.append( StringUtilities.leftJustify(orderStatus.StatusDesc, 17,' ') );
          body.append( StringUtilities.leftJustify(orderStatus.SeNumber, 11,' ') );
          body.append("\n");
        }          
        MailMessage msg = new MailMessage();
      
        msg.setAddresses(MesEmails.MSG_ADDRS_AMEX_ESA_SETUP_STATUS);
        msg.setSubject(subject.toString());
        msg.setText(body.toString());
        msg.send();
      }
    }
    catch(Exception e)
    {
      logEntry("notifySetupGroup()", e.toString());
    }
  }
  
  protected void reviewAccountsReceive( )
  {
    String                        batchId     = null;
    ResultSetIterator             it          = null;
    Vector                        params      = new Vector();
    int                           recCount    = 0;
    String                        resp        = null;
    ResultSet                     resultSet   = null;
    IH2HServiceSoapBindingStub    service     = null;
    String                        sourceCode  = null;
  
    try
    {
      // check for accounts that are under review
      /*@lineinfo:generated-code*//*@lineinfo:795^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(esp.setup_status) 
//          from    amex_esa_setup_process  esp
//          where   not esp.batch_id is null and
//                  esp.setup_status = :AmexESADataBean.ESA_REVIEW -- 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(esp.setup_status)  \n        from    amex_esa_setup_process  esp\n        where   not esp.batch_id is null and\n                esp.setup_status =  :1  -- 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.AmexESAEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,AmexESADataBean.ESA_REVIEW);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:801^7*/
      
      if ( recCount > 0 )
      {
        // setup the SOAP call
        service = initService();
        
        try
        {
          resp = service.getDispositionStatus();
          handleBatchStatusResponse( resp );
        }
        catch( Exception rpce )
        {
          handleFault(rpce);
        }
        
        // attempt to recover any review accounts that have been
        // in review for more than x days (defined in mes_defaults table)
        // this is necessary because some percentage of review accounts
        // never receive a status using the getDispositionStatus SOAP 
        // call.  the reason for this is unclear, but after several days
        // we are going to try and pro-actively recover these using
        // the recoverBatchStatus SOAP call.
        int reviewDays = MesDefaults.getInt(MesDefaults.DK_AMEX_ESA_BATCH_RECOVER_DAYS);
        
        /*@lineinfo:generated-code*//*@lineinfo:827^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct esp.batch_id     as batch_id
//            from    amex_esa_setup_process esp
//            where   not esp.batch_id is null and
//                    esp.setup_status = :AmexESADataBean.ESA_REVIEW and -- 1 and
//                    esp.status_date < (sysdate - :reviewDays)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct esp.batch_id     as batch_id\n          from    amex_esa_setup_process esp\n          where   not esp.batch_id is null and\n                  esp.setup_status =  :1  and -- 1 and\n                  esp.status_date < (sysdate -  :2 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.AmexESAEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,AmexESADataBean.ESA_REVIEW);
   __sJT_st.setInt(2,reviewDays);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.startup.AmexESAEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:834^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          batchStatusRecover( resultSet.getString("batch_id") );
        }
        resultSet.close();
        it.close();
      }
      else
      {
        log.debug("No accounts in review");
      }
      
      // store the last disposition check date
      PropsFile.setProperty("last.disp.check",
                            DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MMddyyyy"));
      PropsFile.store( PropertiesFilename );
    }
    catch( Exception e )
    {
      logEntry("reviewAccountsReceive()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }    
  
  protected void saveXmlResponse( String rawXml )
  {
    String            filename    = null;
    BufferedWriter    out         = null;
    String            respDir     = "response";
    
    try
    {
      if ( PropsFile.getBoolean("response.save.enabled",false) )
      {
        filename = buildFilename("amex_esar3941",".xml");
        
        // if necessary create a directory for the response files
        File f = new File(respDir);
        if ( !f.exists() || !f.isDirectory() )
        {
          f.mkdirs();
        }

        out = new BufferedWriter( new FileWriter( respDir +  File.separator + filename, false ) );
        out.write(rawXml);
        out.close();
      }
    }
    catch( Exception e )
    {
      logEntry("saveXmlResponse()",e.toString());
    }
  }
  
  private void sendStatusEmail(String fileName, String message)
  {
    StringBuffer  subject         = new StringBuffer("");
    StringBuffer  body            = new StringBuffer("");
    
    try
    {
      subject.append("Outgoing Amex ESA Setup Error");
      
      // make sure the filename is in 
      // the message text for error research
      body.append("Filename: ");
      body.append(fileName);
      body.append("\n\n");
      
      body.append(message);
      
      MailMessage msg = new MailMessage();
      
      msg.setAddresses(MesEmails.MSG_ADDRS_AMEX_ESA_SETUP_ERROR);
      
      msg.setSubject(subject.toString());
      msg.setText(body.toString());
      msg.send();
    }
    catch(Exception e)
    {
      logEntry("sendStatusEmail()", e.toString());
    }
  }
  
  protected void setTestMode( boolean inTest )
  {
    TestMode = inTest;
  }
  
  protected void setTestBatchId( int batchId )
  {
    TestBatchId = batchId;
    setTestMode(true);
  }
  
  protected void setupRequestsReceive( )
  {
    String                      batchId     = null;
    ResultSetIterator           it          = null;
    Vector                      params      = new Vector();
    String                      resp        = null;
    ResultSet                   resultSet   = null;
    IH2HServiceSoapBindingStub  service     = null;
    String                      sourceCode  = null;
  
    try
    {
      if ( TestBatchId != 0 )  // re-build a previous file
      {
        /*@lineinfo:generated-code*//*@lineinfo:950^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  :TestBatchId as batch_id
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   :1  as batch_id\n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.startup.AmexESAEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,TestBatchId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.startup.AmexESAEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:954^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:958^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct esp.batch_id  as batch_id
//            from    amex_esa_setup_process esp
//            where   not esp.batch_id is null and
//                    esp.setup_status = :AmexESADataBean.ESA_PENDING -- 98
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct esp.batch_id  as batch_id\n          from    amex_esa_setup_process esp\n          where   not esp.batch_id is null and\n                  esp.setup_status =  :1  -- 98";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.startup.AmexESAEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,AmexESADataBean.ESA_PENDING);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.startup.AmexESAEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:964^9*/
      }
              
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        service = initService();
        
        do 
        {
          batchId = resultSet.getString("batch_id");
          
          try
          {
            resp = service.getBatchStatus( batchId );
            saveXmlResponse(resp);
            handleBatchStatusResponse( resp );
          }
          catch( Exception rpce )
          {
            boolean   faultHandled    = false;
            String    faultString     = rpce.toString();
          
            if ( faultString.indexOf("STA02") >= 0 )
            {
              // status 02, all batch items have been 
              // returned in previous calls to getBatchStatus
              // 
              // This means we are out of synch with the amex
              // front end system.  We need to request a retrieval
              // of the entire batch to unpend the missing items.
              batchStatusRecover( String.valueOf(batchId) );
            
              faultHandled = true;
            }
          
            if ( faultHandled == false )
            {
              // generic action, alert the system admin
              handleFault( rpce );
            }
          }
        }
        while( resultSet.next() );
        
        resultSet.close();
        it.close();
      }
      else
      {
        log.debug("No pending setups");
      }
    }
    catch( Exception e )
    {
      logEntry("setupRequestsReceive()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }    
  
  protected void setupRequestsSend( )
  {
    int                         batchId       = 0;
    StringBuffer                buffer        = new StringBuffer();
    String                      dateString    = null;
    int                         recCount      = 0;
    String                      resp          = null;
    IH2HServiceSoapBindingStub  service       = null;
  
    try
    {
      setAutoCommit(false);
      
      if ( TestBatchId != 0 )  // re-build a previous file
      {
        batchId = TestBatchId;    
        
        /*@lineinfo:generated-code*//*@lineinfo:1045^9*/

//  ************************************************************
//  #sql [Ctx] { select  output_filename,
//                    count(merchant_number)  
//            
//            from    amex_esa_setup_process esp
//            where   esp.batch_id = :batchId
//            group by output_filename
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  output_filename,\n                  count(merchant_number)  \n           \n          from    amex_esa_setup_process esp\n          where   esp.batch_id =  :1 \n          group by output_filename";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.startup.AmexESAEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,batchId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   WorkFilename = (String)__sJT_rs.getString(1);
   recCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1053^9*/
      }
      else    // new file
      {
        /*@lineinfo:generated-code*//*@lineinfo:1057^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)  
//            from    amex_esa_setup_process  esp
//            where   esp.batch_id is null and
//                    exists
//                    (
//                      select  mpo.cardtype_code
//                      from    merchpayoption mpo
//                      where   mpo.app_seq_num = esp.app_seq_num and
//                              mpo.cardtype_code = :mesConstants.APP_CT_AMEX -- 16
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)   \n          from    amex_esa_setup_process  esp\n          where   esp.batch_id is null and\n                  exists\n                  (\n                    select  mpo.cardtype_code\n                    from    merchpayoption mpo\n                    where   mpo.app_seq_num = esp.app_seq_num and\n                            mpo.cardtype_code =  :1  -- 16\n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.startup.AmexESAEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.APP_CT_AMEX);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1069^9*/
        
        if ( recCount > 0 )
        {
          // build the outgoing filename
          WorkFilename = buildFilename( "amex_esa3941" );
        
          /*@lineinfo:generated-code*//*@lineinfo:1076^11*/

//  ************************************************************
//  #sql [Ctx] { select  amex_esa_setup_sequence.nextval 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  amex_esa_setup_sequence.nextval  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.startup.AmexESAEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   batchId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1080^11*/
          
          String testSystem = null;
          if ( PropsFile.getString("endpoint.url").indexOf("qwww") >= 0 )
          {
            testSystem = "Y";
          }
          
          /*@lineinfo:generated-code*//*@lineinfo:1088^11*/

//  ************************************************************
//  #sql [Ctx] { update  amex_esa_setup_process esp
//              set     esp.batch_id = :batchId,
//                      esp.output_filename = :WorkFilename,
//                      esp.test_system = :testSystem
//              where   esp.batch_id is null and
//                      exists
//                      (
//                        select  mpo.cardtype_code
//                        from    merchpayoption mpo
//                        where   mpo.app_seq_num = esp.app_seq_num and
//                                mpo.cardtype_code = :mesConstants.APP_CT_AMEX -- 16
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  amex_esa_setup_process esp\n            set     esp.batch_id =  :1 ,\n                    esp.output_filename =  :2 ,\n                    esp.test_system =  :3 \n            where   esp.batch_id is null and\n                    exists\n                    (\n                      select  mpo.cardtype_code\n                      from    merchpayoption mpo\n                      where   mpo.app_seq_num = esp.app_seq_num and\n                              mpo.cardtype_code =  :4  -- 16\n                    )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.startup.AmexESAEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,batchId);
   __sJT_st.setString(2,WorkFilename);
   __sJT_st.setString(3,testSystem);
   __sJT_st.setInt(4,mesConstants.APP_CT_AMEX);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1102^11*/
        }
      }
      
      if ( recCount > 0 )
      {
        buildSetupFile( batchId );
        
        if ( FileRecordCount > 0 )
        {
          service = initService();
          
          buildBatchStream(buffer,WorkFilename);
          
          try
          {
            resp = service.setupBatch( buffer.toString() ); 
            handleBatchSetupResponse( resp );
          }
          catch( Exception rpce )
          {
            handleFault( rpce );
          }
          
//@          int retryCount = 4;
//@          
//@          while( retryCount > 0 )
//@          {
//@            resp = invokeSOAPCall(call);
//@            
//@            if ( resp.generatedFault() ) 
//@            {
//@              int decrement = retryCount; // set retry to zero by default
//@              
//@              handleFault(resp);
//@            
//@              //@ need to add handling to re-process these files?
//@              Fault fault = resp.getFault();
//@              if ( fault.getFaultCode().equals("SOAP-ENV:Client") )
//@              {
//@                String faultString = fault.getFaultString();
//@                if ( faultString.startsWith("Flamenco Caught SOAP Error:") )
//@                {
//@                  if ( faultString.indexOf("Application Error. No Object returned" ) > 0 )
//@                  {
//@                    decrement = 1;
//@                  } 
//@                }
//@                else if ( faultString.startsWith("Exception from service object:") )
//@                {
//@                  if ( faultString.matches("SET..-") )
//@                  {
//@                  }
//@                }
//@              }
//@              retryCount = (retryCount - decrement);
//@            }
//@            else
//@            {
//@              handleBatchSetupResponse((String)resp.getReturnValue().getValue());
//@              break;  // exit retry loop
//@            }
//@          }            
          
          // send the file to archive server (only in prod mode)
          if ( !inTestMode() )
          {
            archiveDailyFile( WorkFilename );
          }          
        }
      }
    }
    catch( Exception e )
    {
      // attempt to rollback the database change to force
      // the records to send again in the next cycle
      try
      { 
        /*@lineinfo:generated-code*//*@lineinfo:1180^9*/

//  ************************************************************
//  #sql [Ctx] { rollback 
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1183^9*/ 
      } 
      catch( Exception ee ) 
      {
      }
      
      logEntry("setupRequestsSend()", e.toString());
    }
    finally
    {
      setAutoCommit(true);    // reset auto commit to true
    }
  }    
  
  public void testIt( String filename )
  {
    StringBuffer        buffer  = new StringBuffer();
    BufferedReader      in      = null;
    String              line    = null;
    
    try
    {
      log.debug("loading " + filename);
      in = new BufferedReader( new FileReader( filename ) );
      
      while( (line = in.readLine()) != null )
      {
        buffer.append(line);
        buffer.append("\n");
      }
      in.close();
      handleBatchStatusResponse(buffer.toString());
    }
    catch( Exception e )
    {
      log.error(e.toString());
    }
  }
  
  protected boolean timeToCheckReviewAccounts( )
  {
    Calendar    cal       = Calendar.getInstance();
    String      curDate   = null;
    int         curHour   = 0;
    String      lastDate  = null;
    boolean     retVal    = false;
    
    curHour = cal.get(Calendar.HOUR_OF_DAY) * 100;
    curHour += cal.get(Calendar.MINUTE);
    
    if ( curHour >= 1700 && curHour <= 1730 ) 
    {
      lastDate  = PropsFile.getString("last.disp.check");
      curDate   = DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(),"MMddyyyy");
      
      if ( lastDate == null || !curDate.equals(lastDate) )
      {
        retVal = true;
      }
    }
    return( retVal );
  }

  public static void main( String[] args )
  {
    AmexESAEvent        test          = null;
    
    try
    {
        if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{MesDefaults.DK_AMEX_ESA_BATCH_RECOVER_DAYS});
        }

      SQLJConnectionBase.initStandalone();
      
      test = new AmexESAEvent();
      
      if ( args.length > 0 )
      {
        test.connect();
        
        if ( args[0].equals("getDispositionStatus") )
        {
          test.reviewAccountsReceive();
        }
        else if ( args[0].equals("recoverBatchStatus") )
        {
          test.batchStatusRecover( args[1] );
        }
        else if ( args[0].equals("testIt") )
        {
          test.testIt(args[1]);
        }
        else if ( args[0].equals("getBatchStatus") )
        {
          // setup a test batch and execute the event in test mode
          test.setTestMode( true );
          test.setTestBatchId( Integer.parseInt(args[1]) );
          test.setupRequestsReceive();
        }
        else if ( args[0].equals("setupBatch") )
        {
          // setup a test batch and execute the event in test mode
          test.setTestMode( true );
          test.setTestBatchId( Integer.parseInt(args[1]) );
          test.setupRequestsSend();
        }
        else if ( args[0].equals("test") )
        {
          // setup a test batch and execute the event in test mode
          test.setTestMode( true );
          test.setTestBatchId( Integer.parseInt(args[1]) );
          test.execute();
        }
        else
        {
          log.debug("Invalid test command '" + args[0] + "'");
        }
        test.cleanUp();
      }        
      else
      {
        // no args, just execute the event
        test.execute();
      }        
    }
    catch( Exception e )
    {
      log.error(e.toString());
    }
    finally
    {
    }
  }
}/*@lineinfo:generated-code*/