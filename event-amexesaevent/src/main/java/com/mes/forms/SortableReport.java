/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/SortableReport.java $

  Description:
  
  SortableReport
  
  Contains functionality to facilitate reports that need to sort by 
  different columns.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.sql.Timestamp;
import com.mes.user.UserBean;

public class SortableReport extends FieldBean
{
  public SortableReport(UserBean user)
  {
    super(user);
  }
  
  public SortableReport()
  {
  }
  
  /*
  ** public class SortByType
  **
  ** This internal class is used to synchronize the sorting of 
  ** statement summary rows (see below).
  */
  public class SortByType
  {
    // default to 1, (0 cannot be reversed)
    private int     sortBy = 1;
    private boolean reverseSort = false;
    
    public SortByType()
    {
    }
    public SortByType(int sortBy)
    {
      this.sortBy = sortBy;
      reverseSort = (sortBy < 0);
      if (reverseSort)
      {
        this.sortBy *= (-1);
      }
    }
    public void setSortBy(int sortBy)
    {
      this.sortBy = sortBy;
      reverseSort = (sortBy < 0);
      if (reverseSort)
      {
        this.sortBy *= (-1);
      }
    }
    public int getSortBy()
    {
      return sortBy;
    }
    public boolean getReverseSort()
    {
      return reverseSort;
    }
  }
  
  /*
  ** int compare(int a, int b)
  ** int compare(long a, long b)
  ** int compare(float a, float b)
  ** int compare(String a, String b)
  ** int compare(Timestamp a, Timestamp b)
  ** int compare(boolean a, boolean b)
  **
  ** Each of these comparison routines compares the two values passed.
  ** In the case of boolean values, false < true
  **
  ** RETURNS: less than 0 if a < b, 0 if a == b, greater than 0 if a > b.
  */
  public static int compare(int a, int b)
  {
    return (a < b ? -1 : (a > b ? 1 : 0));
  }
  public static int compare(long a, long b)
  {
    return (a < b ? -1 : (a > b ? 1 : 0));
  }
  public static int compare(float a, float b)
  {
    return (a < b ? -1 : (a > b ? 1 : 0));
  }
  public static int compare(String a, String b)
  {
    return a.compareTo(b);
  }
  public static int compare(Timestamp a, Timestamp b)
  {
    return (a.before(b) ? -1 : (a.after(b) ? 1 : 0));
  }
  public static int compare(boolean a, boolean b)
  {
    return ((!a && b) ? -1 : ((a && !b) ? 1 : 0));
  }
}
  
