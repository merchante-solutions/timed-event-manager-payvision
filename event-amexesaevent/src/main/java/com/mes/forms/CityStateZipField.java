/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/CityStateZipField.java $

  Description:
  
  CityStateZipField
  
  Specialized field group that contains three fields representing the city,
  state and zip of an address.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-31 11:13:04 -0800 (Mon, 31 Jan 2011) $
  Version            : $Revision: 18354 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.LinkedList;
import java.util.StringTokenizer;

public class CityStateZipField extends FieldGroup
{
  private static String[] states =
  {
    "AK","AL","AR","AZ","CA","CO","CT","DC","DE","FL","GA","HI",
    "IA","ID","IL","IN","KS","KY","LA","MA","MD","ME","MI","MN",
    "MO","MS","MT","NC","ND","NE","NH","NJ","NM","NV","NY","OH",
    "OK","OR","PA","PR","RI","SC","SD","TN","TX","UT","VA","VT","WA",
    "WI","WV","WY"
  };
      
  public class StateField extends DropDownField
  {
    public class StateValidation implements Validation
    {
      private String stateCode = null;
      
      public boolean validate(String fieldData)
      {
        // don't invalidate empty data
        if (fieldData == null || fieldData.length() == 0)
        {
          return true;
        }
        
        // store the field data in case error needs to report it
        stateCode = fieldData;
        
        String checkData = fieldData.toUpperCase();
        
        // look for invalid length
        if (checkData.length() != 2)
        {
          return false;
        }
        
        // look for a match in the valid state codes list
        for (int i = 0; i < states.length; ++i)
        {
          if (checkData.equals(states[i]))
          {
            return true;
          }
        }
        
        // not found in state list, invalid
        return false;
      }
      
      public String getErrorText()
      {
        return "Invalid state code" 
          + (stateCode != null ? ": '" + stateCode + "'" : "");
      }
    }
    
    public StateField(String fname, String label, boolean nullAllowed)
    {
      super(fname,label,new StateDropDownTable(),nullAllowed);
      
      addValidation(new StateValidation(),"statecheck");
    }
    public StateField(String fname, boolean nullAllowed)
    {
      this(fname,fname,nullAllowed);
    }
    
    public String getData()
    {
      if (fdata.length() == 2)
      {
        return fdata.toUpperCase();
      }
      return fdata;
    }
  }
  
  private Field       cityField;
  private StateField  stateField;
  private ZipField    zipField;

  public class CityStateZipValidation implements Validation
  {
    private String errorText = null;
    
    public boolean validate(String fdata)
    {
      return stateField.isValid() && zipField.isValid() && cityField.isValid();
    }
    
    public String getErrorText()
    {
      return "Invalid city, state or zip";
    }
  }
  
  public class IncompleteValidation implements Validation
  {
    private Field field1;
    private Field field2;
    
    public IncompleteValidation(Field field1, Field field2)
    {
      this.field1 = field1;
      this.field2 = field2;
    }
    
    public boolean validate(String fdata)
    {
      // determine if either of the two dependent fields have data
      String fdata1 = field1.getData();
      String fdata2 = field2.getData();
      if ((fdata1 != null && fdata1.length() > 0)
          || (fdata2 != null && fdata2.length() > 0))
      {
        // at least one dependent field has data, so return true
        // only if this field has data as well (valid)
        // otherwise return false (indicates address data is incomplete)
        return (fdata != null && fdata.length() > 0);
      }
      // neither dependent field has data so this field is considered valid
      // whether or not it contains data
      return true;
    }
    
    public String getErrorText()
    {
      return "Incomplete";
    }
  }

  public CityStateZipField(String fname, String label, int length, int htmlSize, boolean nullAllowed)
  {
    super(fname,label);
    
    if (!nullAllowed)
    {
      addValidation(new RequiredValidation(),"required");
    }
    
    addValidation(new CityStateZipValidation(),"citystatezip");
    
    cityField   = new Field     (fname + "City", "City",length,htmlSize,true);
    stateField  = new StateField(fname + "State","State",true);
    zipField    = new ZipField  (fname + "Zip",  "Zip",true,stateField);
    
    cityField.addValidation(new IncompleteValidation(stateField,zipField));
    stateField.addValidation(new IncompleteValidation(cityField,zipField));
    zipField.addValidation(new IncompleteValidation(cityField,stateField));
    
    add(cityField);
    add(stateField);
    add(zipField);
    
  }

  
  public CityStateZipField(String fname, String label, int htmlSize,
    boolean nullAllowed)
  {
    super(fname,label);
    
    this.length   = 40;
    this.htmlSize = htmlSize;
    
    if (!nullAllowed)
    {
      addValidation(new RequiredValidation(),"required");
    }
    
    addValidation(new CityStateZipValidation(),"citystatezip");
    
    cityField   = new Field     (fname + "City", "City",25,25,true);
    stateField  = new StateField(fname + "State","State",true);
    zipField    = new ZipField  (fname + "Zip",  "Zip",true,stateField);
    
    cityField.addValidation(new IncompleteValidation(stateField,zipField));
    stateField.addValidation(new IncompleteValidation(cityField,zipField));
    zipField.addValidation(new IncompleteValidation(cityField,stateField));
    
    add(cityField);
    add(stateField);
    add(zipField);
  }
  public CityStateZipField(String fname, int htmlSize, boolean nullAllowed)
  {
    this(fname,fname,htmlSize,nullAllowed);
  }
  
  public String getData()
  {
    if (hasError)
    {
      return fdata;
    }
    
    StringBuffer data = new StringBuffer();
    
    String cityData   = cityField.getRenderData();
    String stateData  = stateField.getRenderData();
    String zipData    = zipField.getRenderData();
    
    data.append(cityData);
    
    if (data.length() > 0 && stateData.length() > 0)
    {
      data.append(", ");
    }
    
    if (!stateData.equals("--"))
    {
      data.append(stateData);
    }
    
    if (data.length() > 0 && zipData.length() > 0)
    {
      data.append(" ");
    }
    
    data.append(zipData);
    
    return data.toString();
  }
  
  protected String processData(String rawData)
  {
    if (rawData != null)
    {
      zipField.setData("");
      stateField.setData("");
      cityField.setData("");
      
      StringBuffer cityBuf = new StringBuffer();
      StringTokenizer tok = new StringTokenizer(rawData," ,");
      LinkedList list = new LinkedList();
      while (tok.hasMoreTokens())
      {
        list.add(tok.nextToken());
      }
      
      while (!list.isEmpty())
      {
        switch (list.size())
        {
          case 1:
            zipField.setData((String)list.removeFirst());
            break;
            
          case 2:
            stateField.setData((String)list.removeFirst());
            break;
          
          default:
            if (cityBuf.length() > 0)
            {
              cityBuf.append(" ");
            }
            cityBuf.append((String)list.removeFirst());
            break;
        }
      }
      
      if (cityBuf.length() > 0)
      {
        cityField.setData(cityBuf.toString());
      }
    }
    
    return rawData;
  }
}
