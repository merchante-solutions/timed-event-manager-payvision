/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/NameField.java $

  Description:
  
  NameField
  
  Specialized field group that contains two fields holding the last name
  and first name of a person.  The first name will hold extra initials and
  names.  The last name may be made optional.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.Stack;
import java.util.StringTokenizer;

public class NameField extends FieldGroup
{
  private Field   firstField;
  private Field   lastField;
  private boolean lastOptional;

  
  public class NameFieldReqValidation implements Validation
  {
    private String errorText = "";
    
    public boolean validate(String fdata)
    {
      boolean result = true;

      if(!firstField.isValid())
      {
        errorText = "Field Required";
        result = false;
      }
      else if(!lastField.isValid())
      {
        errorText = "First and last name is required";
        result = false;
      }
    
      return result;
    }

    public String getErrorText()
    {
      return errorText;
    }
  }

  
  public NameField(String fname, String label, int htmlSize, 
    boolean nullAllowed, boolean lastOptional)
  {
    super(fname,label);
    
    this.length       = 40;
    this.htmlSize     = htmlSize;
    this.nullAllowed  = nullAllowed;
    this.lastOptional = lastOptional;
    
    addValidation(new NameFieldReqValidation(), "nameFieldReq");

    firstField  = new Field(fname + "First",40,20, nullAllowed);
    lastField   = new Field(fname + "Last", 40,20, (lastOptional || nullAllowed));
    
    add(firstField);
    add(lastField);
  }
  
  public NameField(String fname, int htmlSize, boolean nullAllowed, 
    boolean lastOptional)
  {
    this(fname,fname,htmlSize,nullAllowed,lastOptional);
  }
  
  public void makeOptional(boolean lastOptional)
  {
    makeOptional();
    
    this.lastOptional = lastOptional;
  }
  
  public void makeOptional()
  {
    super.makeOptional();
    firstField.makeOptional();
    lastField.makeOptional();
  }
  
  public void makeRequired()
  {
    super.makeRequired();
    firstField.makeRequired();
    lastField.makeRequired();
  }
    
  
  public String getData()
  {
    StringBuffer data = new StringBuffer();
    
    String firstData  = firstField.getRenderData();
    String lastData   = lastField.getRenderData();
    
    data.append(firstData);
    
    if (data.length() > 0 && lastData.length() > 0)
    {
      data.append(" ");
    }
    
    data.append(lastData);
    
    return data.toString();
  }
  
  protected String processData(String rawData)
  {
    if (rawData != null)
    {
      String last   = null;
      
      // parse each individual name from raw data into stack
      StringTokenizer tok = new StringTokenizer(rawData);
      Stack stack = new Stack();
      while (tok.hasMoreTokens())
      {
        stack.push(tok.nextToken());
      }
      
      int itemCount = 0;
      
      // assign last most name to last string
      if (!stack.empty())
      {
        last = (String)stack.pop();
        ++itemCount;
      }
      
      // assign remaining names to first string
      StringBuffer remainder = new StringBuffer();
      while (!stack.empty())
      {
        if (remainder.length() > 0)
        {
          remainder.insert(0," ");
        }
        remainder.insert(0,(String)stack.pop());
        ++itemCount;
      }
      
      // if only one name parsed then assign
      // it to the first name field
      if (itemCount == 1)
      {
        firstField.setData(last);
      }
      // otherwise assign last name parsed to
      // last name field, and the remainder to
      // first name field
      else if (itemCount > 1)
      {
        lastField.setData(last);
        firstField.setData(remainder.toString());
      }
    }
    
    return rawData;
  }
}