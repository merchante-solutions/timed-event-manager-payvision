/*@lineinfo:filename=AmexESADataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/ops/AmexESADataBean.sqlj $

  Description:
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-09-08 12:54:39 -0700 (Mon, 08 Sep 2008) $
  Version            : $Revision: 15328 $

  Change History:
     See VSS database

  Copyright (C) 2000-2004,2005 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/
package com.mes.ops;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.flatfile.FlatFileRecord;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.CityStateZipField;
import com.mes.forms.CurrencyField;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.HiddenField;
import com.mes.forms.NameField;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.SortableReport;
import com.mes.forms.TaxIdField;
import com.mes.forms.Validation;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.tools.DropDownTable;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class AmexESADataBean extends SortableReport
{ 
  public static final int   SB_APP_SEQ_NUM        = 1;
  public static final int   SB_DBA_NAME           = 2;
  public static final int   SB_MERCHANT_NUMBER    = 3;
  public static final int   SB_SE_NUMBER          = 4;
  public static final int   SB_STATUS             = 5;
  public static final int   SB_SUBMIT_DATE        = 6;
  public static final int   SB_STATUS_DATE        = 7;
  public static final int   SB_SUBMITTED_BY       = 8;
  public static final int   SB_REP_CODE           = 9;
  
  public static final int   SC_MES                = 298;
  public static final int   SC_VERISIGN           = 352;
  public static final int   SC_BBT                = 404;
  
  public static String      BLANK_VALUE_STRING    = "-----";
  
  public  static final int          ESA_APPROVED        = 0;
  public  static final int          ESA_REVIEW          = 1;
  public  static final int          ESA_REJECTED        = 2;
  public  static final int          ESA_DUPLICATE       = 3;
  public  static final int          ESA_DUP_REVIEW      = 4;
  public  static final int          ESA_SOAP_ERROR      = 80;
  public  static final int          ESA_SUBMIT          = 96;
  public  static final int          ESA_RESUBMIT        = 97;
  public  static final int          ESA_PENDING         = 98;
  public  static final int          ESA_NEW             = 99;
  
  protected class OwnershipTypeTable extends DropDownTable
  {
    public OwnershipTypeTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("S","Sole Proprietorship");
      addElement("C","Corporation");
      addElement("P","Partnership");
      addElement("F","Franchise");
    }
  }
  
  public static class HierarchyTypeTable extends DropDownTable
  {
    public HierarchyTypeTable( )
    {
      addElement("","-- use default --");
      addElement("C","CAP");
      addElement("S","Service Establishment");
    }
  }
  
  public class ESARequest implements Comparable
  {
    public long             AppSeqNum       = 0L;
    public Timestamp        DateCreated     = null;
    public String           DbaName         = null;
    public String           ErrorCodes      = "";
    public long             MerchantId      = 0L;
    public String           RepCode         = null;
    public int              StatusCode      = -1;
    public Timestamp        StatusDate      = null;
    public String           StatusDesc      = null;
    public String           SeNumber        = null;
    public String           SubmittedBy     = null;
    
    public ESARequest( ResultSet resultSet )
      throws java.sql.SQLException
    {
      AppSeqNum       = resultSet.getLong("app_seq_num");
      DateCreated     = resultSet.getTimestamp("date_created");
      DbaName         = processString( resultSet.getString("dba_name") );
      ErrorCodes      = formatBlankValue( resultSet.getString("error_codes") );
      MerchantId      = resultSet.getLong("merchant_number");
      RepCode         = formatBlankValue(resultSet.getString("rep_code"));
      StatusCode      = resultSet.getInt("status_code");
      StatusDate      = resultSet.getTimestamp("status_date");
      StatusDesc      = formatBlankValue(resultSet.getString("status_desc"));
      SubmittedBy     = formatBlankValue(resultSet.getString("submitted_by"));
      SeNumber        = formatBlankValue(resultSet.getString("se_number"));
    }
    
    public int compareTo(Object obj)
    {
      ESARequest  compareObj  = (ESARequest)obj;
      int         retVal      = 0;
      
      try
      {
        compareObj  = (ESARequest)obj;
        
        switch ( SortBy.getSortBy() ) 
        {
          case SB_APP_SEQ_NUM:
            retVal = compare(AppSeqNum,compareObj.AppSeqNum);
            break;
          
          case SB_DBA_NAME:
          default:
            retVal = compare(DbaName.toUpperCase(),compareObj.DbaName.toUpperCase());
            break;
          
          case SB_MERCHANT_NUMBER:
            retVal = compare(MerchantId,compareObj.MerchantId);
            break;
          
          case SB_SUBMITTED_BY:
            retVal = compare(SubmittedBy,compareObj.SubmittedBy);
            break;

          case SB_STATUS_DATE:
            retVal = compare(StatusDate,compareObj.StatusDate);
            break;
            
          case SB_SE_NUMBER:
            retVal = compare(SeNumber,compareObj.SeNumber);
            break;
          
          case SB_STATUS:
            retVal = compare(StatusDesc,compareObj.StatusDesc);
            break;
          
          case SB_SUBMIT_DATE:
            retVal = compare(DateCreated,compareObj.DateCreated);
            break;
            
          case SB_REP_CODE:
            retVal = compare(RepCode,compareObj.RepCode);
            break;
        }
      }
      catch (Exception e) 
      {
        logEntry("compare(1)",e.toString());
      }
      
      if (retVal == 0)
      {
        try
        {
          retVal = compare(AppSeqNum,compareObj.AppSeqNum);
        }
        catch (Exception e) 
        {
          logEntry("compare(2)",e.toString());
        }
      }
          
      // if reverse sort specified, flip the retVal around
      if ( SortBy.getReverseSort() )
      {
        retVal *= (-1);
      }
      
      return( retVal );
    }
    
    public String formatBlankValue(String test)
    {
      String result = test;

      if(test == null || test.equals(""))
      {
        result = BLANK_VALUE_STRING;
      }

      return result;
    }
    
    public String getDateCreated( )
    {
      return( DateTimeFormatter.getFormattedDate( DateCreated, DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT ) );
    }
    
    public String getStatusDate( )
    {
      return( DateTimeFormatter.getFormattedDate( StatusDate, DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT ) );
    }
    
  }
  
  public class ErrorMessage
  {
    public String        ErrorCode     = null;
    public String        ErrorDesc     = null;
    
    public ErrorMessage( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ErrorCode = processString(resultSet.getString("error_code"));
      ErrorDesc = processString(resultSet.getString("error_desc"));
    }
    
    public ErrorMessage( String ecode )
    {
      ErrorCode = ecode;
      ErrorDesc = "UNKNOWN ERROR CODE (" + ecode + ")";
    }
  }
  
  public class StatusDropDownTable 
    extends DropDownTable
  {
    public StatusDropDownTable()
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      
      try
      {
        connect();
        
        addElement("-1","All Statuses");
        
        /*@lineinfo:generated-code*//*@lineinfo:263^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    sc.status_code,
//                      sc.status_desc_short
//            from      amex_esa_setup_status_codes   sc
//            order by  sc.status_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    sc.status_code,\n                    sc.status_desc_short\n          from      amex_esa_setup_status_codes   sc\n          order by  sc.status_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.ops.AmexESADataBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.ops.AmexESADataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:269^9*/
        
        rs = it.getResultSet();
  
        while (rs.next())
        {
          addElement(rs);
        }
        rs.close();
        it.close();
      }
      catch( Exception e )
      {
        logEntry( "StatusDropDownTable()", e.toString() );
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
    }
  }
  
  public static class AmexFlatFeeValidation implements Validation
  {
    protected Field   AnnualSalesField  = null;
    protected String  ErrorMessage      = null;
    
    public AmexFlatFeeValidation( Field salesField )
    {
      AnnualSalesField  = salesField;
    }
    
    public boolean validate(String fdata)
    {
      ErrorMessage = null;
      
      try
      {
        if ( fdata.equals("y") || fdata.equals("Y") )
        {
          if ( AnnualSalesField.asDouble() >= 5000 )
          {
            ErrorMessage = "Annual sales must be less that $5,000 to qualify";
          }
        }
      }
      catch( Exception e )
      {
        ErrorMessage = "Invalid Amex flat rate";
      }
      return( (ErrorMessage == null) );
    }
    
    public String getErrorText()
    {
      return( ErrorMessage );
    }
  }
  
  // class variables
  protected   boolean       FirstTime       = true;
  protected   TreeSet       ReportRows      = new TreeSet();
  protected   SortByType    SortBy          = new SortByType(SB_SUBMIT_DATE * -1);
  protected   boolean       Submitted       = false;

  //constructor
  public AmexESADataBean()
  {
    DateField         temp      = null;
    
    fields.add( new HiddenField("sortBy") );
    fields.add( new ButtonField("search") );
    
    // add the report date range
    temp = new DateField( "beginDate","Submit Date From: ", true );
    temp.setDateFormat("MM/dd/yyyy");
    fields.add(temp );
    
    temp = new DateField( "endDate","To:", true );
    temp.setDateFormat("MM/dd/yyyy");
    fields.add(temp );
    
    Calendar cal = Calendar.getInstance();
    getField("endDate").setData(DateTimeFormatter.getFormattedDate(cal.getTime(), "MM/dd/yyyy"));
    cal.add(Calendar.DATE, -30);
    getField("beginDate").setData(DateTimeFormatter.getFormattedDate(cal.getTime(), "MM/dd/yyyy"));
    
    fields.add(new DropDownField("status", new StatusDropDownTable(), true));
    setData("status","-1"); // set to the default
  
    fields.add(new Field("lookupValue",25,0,true));
    
    // initialize the sort by field
    int sortByInt = (SortBy.getSortBy() * (SortBy.getReverseSort() ? -1 : 1));
    setData("sortBy", String.valueOf(sortByInt) );
    
    fields.addHtmlExtra("class=\"formFields\"");
  }
  
  protected boolean autoLoad()
  {
    long appSeqNum = fields.getField("appSeqNum").asLong();
    
    // if the app seq num was not passed in, then 
    // call createNewApp() to resolve or generate
    // a new OLA for the account.
    if ( appSeqNum == 0L )
    {
      createNewApp();
    
      // get the new app seq num
      appSeqNum = fields.getField("appSeqNum").asLong();
    }
    
    return( loadAppData(appSeqNum) );
  }
  
  protected boolean autoSubmit( )
  {
    long          appSeqNum = fields.getField("appSeqNum").asLong();
    boolean       retVal    = true;
    
    if (appSeqNum == 0L)
    {
      createNewApp();
    }
    retVal = submitAppData();
    
    return( retVal );
  }
  
  public void createFields()
  {
    FieldGroup appFields = null;
    
    try
    {
      connect();
      
      appFields = (FieldGroup) getField("appFields");
    
      if ( appFields == null )
      {
        appFields = new FieldGroup("appFields");
        fields.add(appFields);
      }
      appFields.removeAllFields();
      
      appFields.add( new HiddenField("appSeqNum") );
      appFields.add( new Field("merchantNumber",         "Merchant Number",25,30,false) );
      appFields.add( new Field("spid",                   "SPID",20,30,false) );
      appFields.add( new Field("legalBusinessName",      "Legal Name",50,50,false) );
      appFields.add( new Field("dbaName",                "DBA Name",25,50,false) );
      appFields.add( new Field("businessAddressLine1",   "Address 1",32,50,false) );
      appFields.add( new Field("businessAddressLine2",   "Address 2",32,50,true) );
      appFields.add( new CityStateZipField("businessCsz","City, State, Zip",50,true ));
      
      appFields.add( new PhoneField("businessPhone",        "Business Phone",false) );
      appFields.add( new NameField("contactName",           "Contact Name",50,false,false) );
      appFields.add( new NumberField("sicCode",             "SIC",4,6,false,0) );
      appFields.add( new NameField("signerName",            "Primary Signer Name",30,false,false) );
      appFields.add( new Field("signerAddressLine1",        "Address",32,30,true) );
      appFields.add( new CityStateZipField("signerCsz",     "City, State, Zip",30,true));
      appFields.add( new TaxIdField("primaryAuthSignerSSN", "Primary Signer SSN",false) );
      appFields.add( new Field("primaryAuthSignerTitle",    "Primary Signer Title",40,15,false) );
      
      appFields.add( new TaxIdField("federalTaxId","Federal Tax ID", true) );
      appFields.add( new PhoneField("signerPhone",              "Primary Signer Phone",true) );
      appFields.add( new NumberField("discountRate",            "Discount Rate",5,5,false,2) );
      appFields.add( new CurrencyField("estimatedAnnualVolume", "Est Annual Volume",11,9,false) );
      appFields.add( new CurrencyField("estimatedAverageRoc",   "Est Average Ticket",11,9,false) );
      appFields.add( new NumberField("affiliationAmexSeNum",    "Franchise #",10,10,true,0) );
      appFields.add( new Field("abaNumber",                     "Transit Routing",9,15,false) );
      appFields.add( new Field("ddaNumber",                     "Checking Account",17,15,false) );
      appFields.add( new Field("nameOnAchBankAcct",             "Name on Account",50,30,true) );
      appFields.add( new Field("bankName",                      "Bank Name",30,30,false) );
      appFields.add( new Field("bankAddressLine1",              "Address",32,30,false) );
      appFields.add( new CityStateZipField("bankCsz",           "City, State, Zip",30,false) );
      
      appFields.add( new ButtonField("submit","Submit Application") );
      appFields.add( new ButtonField("resubmit","Re-Submit Application") );
      appFields.add( new DropDownField("ownershipType","Ownership Type",new OwnershipTypeTable(),false));
      appFields.add( new CheckboxField("capNeeded","Apply for Franchise #",false));
      appFields.add( new CheckboxField("flatFee","Monthly Flat Fee",false));
      appFields.getField("flatFee").addValidation( new AmexFlatFeeValidation( appFields.getField("estimatedAnnualVolume") ) );
      appFields.add( new Field("iataNumber","IATA/ARC Number",8,8,true));
      
      appFields.getField("merchantNumber").makeReadOnly();
      appFields.getField("ddaNumber").makeReadOnly();
      appFields.getField("abaNumber").makeReadOnly();
    }
    catch( Exception e )
    {
      logEntry("createFields()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  public long createNewApp( )
  {
    return( createNewApp( fields.getField("merchantNumber").asLong() ) );
  }
  
  public long createNewApp( long merchantId )
  {
    int     appExists     = 0;
    long    appSeqNum     = 0L;
    int     appType       = 0;
    int     bankNumber    = 0;
    int     mifExists     = 0;

    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:489^7*/

//  ************************************************************
//  #sql [Ctx] { select   count(mr.merch_number) 
//          from     merchant mr
//          where    mr.merch_number = :merchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select   count(mr.merch_number)  \n        from     merchant mr\n        where    mr.merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.ops.AmexESADataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appExists = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:494^7*/

      /*@lineinfo:generated-code*//*@lineinfo:496^7*/

//  ************************************************************
//  #sql [Ctx] { select   count(mf.merchant_number) 
//          from     mif  mf
//          where    mf.merchant_number = :merchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select   count(mf.merchant_number)  \n        from     mif  mf\n        where    mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.ops.AmexESADataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mifExists = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:501^7*/
      
      if( mifExists == 0 )
      {
        addError("Invalid merchant number.");
      }
      else
      {
        if ( appExists == 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:511^11*/

//  ************************************************************
//  #sql [Ctx] { select  mf.bank_number 
//              from    mif   mf
//              where   mf.merchant_number = :merchantId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  mf.bank_number  \n            from    mif   mf\n            where   mf.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.ops.AmexESADataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:516^11*/
          
          switch( bankNumber )
          {
            case mesConstants.BANK_ID_BBT:
              appType = mesConstants.APP_TYPE_BBT;
              break;
              
            case mesConstants.BANK_ID_CBT:
              appType = mesConstants.APP_TYPE_CBT_NEW;
              break;
              
            default:
              appType = mesConstants.APP_TYPE_MES_NEW;
              break;
          }
          
          // this procedure will only create the app 
          // if one does not already exist
          /*@lineinfo:generated-code*//*@lineinfo:535^11*/

//  ************************************************************
//  #sql [Ctx] { call create_shadow_app(:merchantId, :appType )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN create_shadow_app( :1 ,  :2  )\n          \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setInt(2,appType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:538^11*/
        }          

        // retrieve the application sequence number
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:544^11*/

//  ************************************************************
//  #sql [Ctx] { select   mr.app_seq_num 
//              from     merchant mr
//              where    mr.merch_number = :merchantId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select   mr.app_seq_num  \n            from     merchant mr\n            where    mr.merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.ops.AmexESADataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:549^11*/
        }
        catch( SQLException sqle )
        {
          appSeqNum = 0L;
        }
        
        // store the app seq num
        fields.setData("appSeqNum",String.valueOf(appSeqNum));
      }
    }
    catch(Exception e)
    {
      logEntry(("createNewApp(" + merchantId + ")"), e.toString());
      addError("Error creating new application");
    }
    finally
    {
      cleanUp();
    }
    return( appSeqNum );
  }
  
  protected int getRowCount( String tableName )
  {
    return( getRowCount( tableName,("app_seq_num = " + getData("appSeqNum")) ) );
  }
  
  protected int getRowCount( String tableName, String whereClause )
  {
    int     recCount    = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:583^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num) 
//          from    :tableName
//          where   :whereClause
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  count(app_seq_num)  \n        from     ");
   __sjT_sb.append(tableName);
   __sjT_sb.append(" \n        where    ");
   __sjT_sb.append(whereClause);
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "6com.mes.ops.AmexESADataBean:" + __sjT_sql;
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:588^7*/
    }
    catch( Exception e )
    {
      logEntry("getRowCount(" + tableName + ")", e.toString());
    }
    return( recCount );
  }
  
  public int getSortByType()
  {
    return( getInt("sortBy") );
  }
  
  public Vector getSubmitButtons()
  {
    long          appSeqNum = fields.getField("appSeqNum").asLong();
    Vector        buttons   = new Vector();
    int           status    = -1;
    
    try
    {
      connect();
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:614^9*/

//  ************************************************************
//  #sql [Ctx] { select  esp.setup_status 
//            from    amex_esa_setup_process esp
//            where   esp.app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  esp.setup_status  \n          from    amex_esa_setup_process esp\n          where   esp.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.ops.AmexESADataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   status = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:619^9*/
      }
      catch( Exception ei )
      {
        status = ESA_NEW;
      }
      
      switch( status )
      {
        case ESA_NEW:
          buttons.add( getField("submit") );
          break;
          
        case ESA_REJECTED:
        case ESA_SOAP_ERROR:
          buttons.add( getField("resubmit") );
          break;
          
        case ESA_APPROVED:
        case ESA_REVIEW:
        case ESA_DUPLICATE:
        case ESA_DUP_REVIEW:
        case ESA_RESUBMIT:
        case ESA_PENDING:
          break;
      }            
    }
    catch( Exception e )
    {
      logEntry("getSubmitButtons()",e.toString());  
    }
    finally
    {
      cleanUp();
    }
    return( buttons );
  }
  
  public boolean isAppSumitted( )
  {
   return( isAppSubmitted( fields.getField("appSeqNum").asLong() ) ); 
  }
  
  public boolean isAppSubmitted( long appSeqNum )
  {
    int     recCount    = 0;
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:669^7*/

//  ************************************************************
//  #sql [Ctx] { select count(esp.app_seq_num) 
//          from    amex_esa_setup_process esp
//          where   esp.app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(esp.app_seq_num)  \n        from    amex_esa_setup_process esp\n        where   esp.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.ops.AmexESADataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:674^7*/
    }
    catch(Exception e)
    {
      logEntry("isAppSubmitted()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( recCount > 0 );
  }
  
  public Iterator iterator() 
    throws Exception
  {
    // load data if this is a new search or first time to the page
    if ( FirstTime || Submitted )
    {
      FirstTime = false;
      loadData();
    }
    
    // if the sort has been changed since the
    // data was originally loaded, then re-sort
    int sortType  = getInt("sortBy");
    int sbType    = (SortBy.getSortBy() * (SortBy.getReverseSort() ? -1 : 1));
    
    if ( sortType != sbType )
    {
      // update the sort by type object
      // and reload the data into the TreeSet
      SortBy.setSortBy( sortType );
      TreeSet sortedData = new TreeSet();
      for (Iterator i = ReportRows.iterator(); i.hasNext();)
      {
        sortedData.add(i.next());
      }
      ReportRows = sortedData;
    }
    
    // return an iterator into the report data
    return( ReportRows.iterator() );
  }
  
  public boolean loadAppData( long appSeqNum )
  {
    return( loadAppData( appSeqNum, null ) );
  }
  
  public boolean loadAppData( long appSeqNum, FlatFileRecord ffd )
  {
    ResultSetIterator         it          = null;
    ResultSet                 resultSet   = null;
    boolean                   retVal      = false;
    
    try
    {
      connect();
      
      // select the transactions to process
      /*@lineinfo:generated-code*//*@lineinfo:735^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mr.app_seq_num                            as app_seq_num,
//                  app.app_type                              as app_type,
//                  mr.merch_number                           as order_number,
//                  mr.merch_number                           as merchant_number,
//                  mr.iata_number                            as iata_number,
//                  nvl( mr.merch_legal_name,
//                       mr.merch_business_name )             as legal_business_name,
//                  mr.merch_business_name                    as dba_name,
//                  a1.address_line1                          as business_address_line1,
//                  a1.address_line2                          as business_address_line2,
//                  a1.address_city                           as business_csz_city,
//                  a1.countrystate_code                      as business_csz_state,
//                  a1.address_zip                            as business_csz_zip,
//                  substr(a1.address_zip,1,5)                as business_zip,
//                  substr(a1.address_zip,6)                  as business_zip_ext,
//                  substr(a1.address_phone,1,3)              as business_phone_area_code,
//                  substr(a1.address_phone,4,7)              as business_phone_number,
//                  a1.address_phone                          as business_phone,
//                  decode( mc.merchcont_prim_first_name,
//                          null, ( bo.busowner_first_name ||' '|| 
//                                  bo.busowner_last_name ), 
//                          ( mc.merchcont_prim_first_name ||' '||
//                            mc.merchcont_prim_last_name )
//                        )                                   as contact_name,
//                  nvl(ea.sic_code,mr.sic_code)              as sic_code,
//                  ( bo.busowner_first_name || ' ' || 
//                    bo.busowner_last_name )                 as signer_name,
//                  ( bo.busowner_last_name || ', ' || 
//                    bo.busowner_first_name )                as primary_auth_signer_name,
//                  ( a2.address_line1 ||' '||
//                    a2.address_line2 )                      as signer_address_line1,
//                  nvl(a2.address_city,
//                      a1.address_city)                      as signer_csz_city,
//                  nvl(a2.countrystate_code,
//                      a1.countrystate_code)                 as signer_csz_state,
//                  nvl(a2.address_zip,a1.address_zip)        as signer_csz_zip,
//                  substr(nvl(a2.address_zip,
//                             a1.address_zip),1,5)           as signer_zip,
//                  substr(nvl(a2.address_zip,
//                             a1.address_zip),6)             as signer_zip_ext,
//                  lpad(bo.busowner_ssn,9,'0')               as primary_auth_signer_ssn,
//                  bo.busowner_title                         as primary_auth_signer_title,
//                  mr.merch_federal_tax_id                   as federal_tax_id,
//                  substr(a2.address_phone,1,3)              as signer_phone_area_code,
//                  substr(a2.address_phone,4,7)              as signer_phone_number,
//                  a2.address_phone                          as signer_phone,
//                  decode( mpo.group_merchant_number,
//                          null,'N','Y')                     as affiliation_ind,          
//                  mpo.group_merchant_number                 as affiliation_amex_se_num,
//                  mpo.merchpo_rate                          as discount_rate,
//                  mpo.annual_sales                          as estimated_annual_volume,
//                  nvl( mpo.average_ticket,                   
//                       nvl(mr.merch_average_cc_tran,0) )    as estimated_average_roc,
//                  lpad(decode(mf.transit_routng_num,
//                    null, mb.merchbank_transit_route_num,
//                    mf.transit_routng_num),9,'0')           as aba_number,
//                  decode(mf.dda_num,
//                    null, mb.merchbank_acct_num,
//                    mf.dda_num)                             as dda_number,
//                  nvl(mr.merch_legal_name,
//                      mr.merch_business_name)               as name_on_ach_bank_acct,
//                  mb.merchbank_name                         as bank_name,
//                  a3.address_line1                          as bank_address_line1,
//                  a3.address_city                           as bank_csz_city,
//                  a3.countrystate_code                      as bank_csz_state,
//                  a3.address_zip                            as bank_csz_zip,
//                  substr(a3.address_zip,1,5)                as bank_zip,
//                  substr(a3.address_zip,6)                  as bank_zip_ext,
//                  mr.merch_number                           as processor_id_num,
//                  nvl( ea.ownership_type,
//                       decode( mr.bustype_code,
//                               1, 'S',
//                               2, 'C',
//                               3, 'P',
//                               'S') )                       as ownership_type,
//                  nvl( ea.spid,                         
//                      ( to_char(app.appsrctype_code) ||'/'||
//                        decode( nvl(app.app_type,0),
//                                11, to_char(mr.merch_rep_code),  -- BB&T application
//                                app.app_user_login ) ) )    as spid,
//                  nvl(appt.amex_esa_source_code,'0298')     as source_code,
//                  lower(nvl(ea.flat_fee,'N'))               as flat_fee,
//                  decode( nvl(ea.flat_fee,'N'),
//                          'Y', '01',
//                          null )                            as pricing_code,
//                  lower( decode( nvl(ea.cap_number_requested,'N'),
//                                 'Y',decode(mpo.group_merchant_number,null,'Y','N'),
//                                 'N') )                     as cap_needed
//          from    merchant                  mr,
//                  application               app,
//                  app_type                  appt,
//                  address                   a1,
//                  merchcontact              mc,
//                  businessowner             bo,
//                  address                   a2,
//                  merchpayoption            mpo,
//                  merchbank                 mb,
//                  address                   a3,
//                  amex_esa_app_data         ea,
//                  mif                       mf
//          where   mr.app_seq_num = :appSeqNum and
//                  app.app_seq_num = mr.app_seq_num and
//                  appt.app_type_code = app.app_type and
//                  a1.app_seq_num(+) = mr.app_seq_num and
//                  a1.addresstype_code(+) = 1 and
//                  mc.app_seq_num(+) = mr.app_seq_num and
//                  bo.app_seq_num(+) = mr.app_seq_num and
//                  bo.busowner_num(+) = 1 and
//                  a2.app_seq_num(+) = mr.app_seq_num and
//                  a2.addresstype_code(+) = 4 and
//                  mpo.app_seq_num(+) = mr.app_seq_num and
//                  mpo.cardtype_code(+) = 16 and
//                  mb.app_seq_num(+) = mr.app_seq_num and
//                  a3.app_seq_num(+) = mr.app_seq_num and
//                  a3.addresstype_code(+) = 9 and
//                  ea.app_seq_num(+) = mr.app_seq_num and
//                  mr.merch_number = mf.merchant_number(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mr.app_seq_num                            as app_seq_num,\n                app.app_type                              as app_type,\n                mr.merch_number                           as order_number,\n                mr.merch_number                           as merchant_number,\n                mr.iata_number                            as iata_number,\n                nvl( mr.merch_legal_name,\n                     mr.merch_business_name )             as legal_business_name,\n                mr.merch_business_name                    as dba_name,\n                a1.address_line1                          as business_address_line1,\n                a1.address_line2                          as business_address_line2,\n                a1.address_city                           as business_csz_city,\n                a1.countrystate_code                      as business_csz_state,\n                a1.address_zip                            as business_csz_zip,\n                substr(a1.address_zip,1,5)                as business_zip,\n                substr(a1.address_zip,6)                  as business_zip_ext,\n                substr(a1.address_phone,1,3)              as business_phone_area_code,\n                substr(a1.address_phone,4,7)              as business_phone_number,\n                a1.address_phone                          as business_phone,\n                decode( mc.merchcont_prim_first_name,\n                        null, ( bo.busowner_first_name ||' '|| \n                                bo.busowner_last_name ), \n                        ( mc.merchcont_prim_first_name ||' '||\n                          mc.merchcont_prim_last_name )\n                      )                                   as contact_name,\n                nvl(ea.sic_code,mr.sic_code)              as sic_code,\n                ( bo.busowner_first_name || ' ' || \n                  bo.busowner_last_name )                 as signer_name,\n                ( bo.busowner_last_name || ', ' || \n                  bo.busowner_first_name )                as primary_auth_signer_name,\n                ( a2.address_line1 ||' '||\n                  a2.address_line2 )                      as signer_address_line1,\n                nvl(a2.address_city,\n                    a1.address_city)                      as signer_csz_city,\n                nvl(a2.countrystate_code,\n                    a1.countrystate_code)                 as signer_csz_state,\n                nvl(a2.address_zip,a1.address_zip)        as signer_csz_zip,\n                substr(nvl(a2.address_zip,\n                           a1.address_zip),1,5)           as signer_zip,\n                substr(nvl(a2.address_zip,\n                           a1.address_zip),6)             as signer_zip_ext,\n                lpad(bo.busowner_ssn,9,'0')               as primary_auth_signer_ssn,\n                bo.busowner_title                         as primary_auth_signer_title,\n                mr.merch_federal_tax_id                   as federal_tax_id,\n                substr(a2.address_phone,1,3)              as signer_phone_area_code,\n                substr(a2.address_phone,4,7)              as signer_phone_number,\n                a2.address_phone                          as signer_phone,\n                decode( mpo.group_merchant_number,\n                        null,'N','Y')                     as affiliation_ind,          \n                mpo.group_merchant_number                 as affiliation_amex_se_num,\n                mpo.merchpo_rate                          as discount_rate,\n                mpo.annual_sales                          as estimated_annual_volume,\n                nvl( mpo.average_ticket,                   \n                     nvl(mr.merch_average_cc_tran,0) )    as estimated_average_roc,\n                lpad(decode(mf.transit_routng_num,\n                  null, mb.merchbank_transit_route_num,\n                  mf.transit_routng_num),9,'0')           as aba_number,\n                decode(mf.dda_num,\n                  null, mb.merchbank_acct_num,\n                  mf.dda_num)                             as dda_number,\n                nvl(mr.merch_legal_name,\n                    mr.merch_business_name)               as name_on_ach_bank_acct,\n                mb.merchbank_name                         as bank_name,\n                a3.address_line1                          as bank_address_line1,\n                a3.address_city                           as bank_csz_city,\n                a3.countrystate_code                      as bank_csz_state,\n                a3.address_zip                            as bank_csz_zip,\n                substr(a3.address_zip,1,5)                as bank_zip,\n                substr(a3.address_zip,6)                  as bank_zip_ext,\n                mr.merch_number                           as processor_id_num,\n                nvl( ea.ownership_type,\n                     decode( mr.bustype_code,\n                             1, 'S',\n                             2, 'C',\n                             3, 'P',\n                             'S') )                       as ownership_type,\n                nvl( ea.spid,                         \n                    ( to_char(app.appsrctype_code) ||'/'||\n                      decode( nvl(app.app_type,0),\n                              11, to_char(mr.merch_rep_code),  -- BB&T application\n                              app.app_user_login ) ) )    as spid,\n                nvl(appt.amex_esa_source_code,'0298')     as source_code,\n                lower(nvl(ea.flat_fee,'N'))               as flat_fee,\n                decode( nvl(ea.flat_fee,'N'),\n                        'Y', '01',\n                        null )                            as pricing_code,\n                lower( decode( nvl(ea.cap_number_requested,'N'),\n                               'Y',decode(mpo.group_merchant_number,null,'Y','N'),\n                               'N') )                     as cap_needed\n        from    merchant                  mr,\n                application               app,\n                app_type                  appt,\n                address                   a1,\n                merchcontact              mc,\n                businessowner             bo,\n                address                   a2,\n                merchpayoption            mpo,\n                merchbank                 mb,\n                address                   a3,\n                amex_esa_app_data         ea,\n                mif                       mf\n        where   mr.app_seq_num =  :1  and\n                app.app_seq_num = mr.app_seq_num and\n                appt.app_type_code = app.app_type and\n                a1.app_seq_num(+) = mr.app_seq_num and\n                a1.addresstype_code(+) = 1 and\n                mc.app_seq_num(+) = mr.app_seq_num and\n                bo.app_seq_num(+) = mr.app_seq_num and\n                bo.busowner_num(+) = 1 and\n                a2.app_seq_num(+) = mr.app_seq_num and\n                a2.addresstype_code(+) = 4 and\n                mpo.app_seq_num(+) = mr.app_seq_num and\n                mpo.cardtype_code(+) = 16 and\n                mb.app_seq_num(+) = mr.app_seq_num and\n                a3.app_seq_num(+) = mr.app_seq_num and\n                a3.addresstype_code(+) = 9 and\n                ea.app_seq_num(+) = mr.app_seq_num and\n                mr.merch_number = mf.merchant_number(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.ops.AmexESADataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:854^7*/
      resultSet = it.getResultSet();
      
      resultSet.next();
      
      if ( ffd != null )    // encode as flat file record
      {
        ffd.setAllFieldData(resultSet);
      }
      else    // return in data structure
      {
        // the the fields without 
        // advancing the result set
        setFields(resultSet,false);
      }
      
      resultSet.close();
      it.close();
      
      retVal = true;
    }
    catch( Exception e )
    {
      logEntry("loadAppData(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      try{ resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch( Exception e ) {}
      cleanUp();
    }
    return( retVal );
  }
  
  public void loadData() throws Exception
  {
    Date                  beginDate     = null;
    Date                  endDate       = null;
    ResultSetIterator     it            = null;
    long                  longLookup    = 0L;
    long                  nodeId        = 0L;
    ResultSet             resultSet     = null;
    String                status        = null;
    String                stringLookup  = null;
    
    try
    {
      connect();
      
      ReportRows.clear();
      
      nodeId        = user.getHierarchyNode();
      if ( nodeId == HierarchyTree.DEFAULT_HIERARCHY_NODE )
      {
        nodeId = 394100000L;
      }
      
      status        = getData("status").trim();
      stringLookup  = getData("lookupValue").toUpperCase().trim();
      
      beginDate  = ((DateField)getField("beginDate")).getSqlDate();
      endDate    = ((DateField)getField("endDate")).getSqlDate();
      
      try
      {
        longLookup = Long.parseLong(stringLookup);
      }
      catch( Exception ee )
      {
        longLookup = -1L;
      }
      
      if ( stringLookup.equals(""))
      {
        stringLookup = "passall";
      }
      else
      {
        stringLookup = ("%" + stringLookup + "%");
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:935^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  esp.merchant_number             as merchant_number,
//                  mr.app_seq_num                  as app_seq_num,
//                  nvl(mf.dba_name,
//                      mr.merch_business_name)     as dba_name,
//                  mr.merch_rep_code               as rep_code,
//                  esp.setup_status                as status_code,
//                  sc.status_desc_short            as status_desc,
//                  esp.status_date                 as status_date,
//                  esp.date_created                as date_created,
//                  ''                              as submitted_by,
//                  esp.error_codes                 as error_codes,
//                  mpo.merchpo_card_merch_number   as se_number
//          from    t_hierarchy                   th,
//                  app_status_app_types          asat,
//                  application                   app,
//                  amex_esa_setup_process        esp,
//                  merchant                      mr,
//                  amex_esa_setup_status_codes   sc,
//                  mif                           mf,
//                  merchpayoption                mpo
//          where   th.hier_type = 1 and
//                  th.ancestor = :nodeId and
//                  asat.hierarchy_node = th.descendent and
//                  app.app_type = asat.app_type and
//                  esp.app_seq_num = app.app_seq_num and
//                  ( 
//                    :beginDate is null  or  
//                    trunc(esp.date_created) between :beginDate and :endDate
//                  ) and
//                  ('-1' = :status or esp.setup_status     = :status) and
//                  ( 'passall'                   = :stringLookup
//                    or esp.merchant_number      = :longLookup
//                    or mr.app_seq_num           = :longLookup
//                    or upper(mr.merch_business_name) like :stringLookup ) and
//                  sc.status_code = esp.setup_status and
//                  mr.app_seq_num = esp.app_seq_num and
//                  mf.merchant_number(+) = mr.merch_number and
//                  mpo.app_seq_num(+) = mr.app_seq_num and
//                  mpo.cardtype_code(+) = :mesConstants.APP_CT_AMEX -- 16 
//          order by esp.status_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  esp.merchant_number             as merchant_number,\n                mr.app_seq_num                  as app_seq_num,\n                nvl(mf.dba_name,\n                    mr.merch_business_name)     as dba_name,\n                mr.merch_rep_code               as rep_code,\n                esp.setup_status                as status_code,\n                sc.status_desc_short            as status_desc,\n                esp.status_date                 as status_date,\n                esp.date_created                as date_created,\n                ''                              as submitted_by,\n                esp.error_codes                 as error_codes,\n                mpo.merchpo_card_merch_number   as se_number\n        from    t_hierarchy                   th,\n                app_status_app_types          asat,\n                application                   app,\n                amex_esa_setup_process        esp,\n                merchant                      mr,\n                amex_esa_setup_status_codes   sc,\n                mif                           mf,\n                merchpayoption                mpo\n        where   th.hier_type = 1 and\n                th.ancestor =  :1  and\n                asat.hierarchy_node = th.descendent and\n                app.app_type = asat.app_type and\n                esp.app_seq_num = app.app_seq_num and\n                ( \n                   :2  is null  or  \n                  trunc(esp.date_created) between  :3  and  :4 \n                ) and\n                ('-1' =  :5  or esp.setup_status     =  :6 ) and\n                ( 'passall'                   =  :7 \n                  or esp.merchant_number      =  :8 \n                  or mr.app_seq_num           =  :9 \n                  or upper(mr.merch_business_name) like  :10  ) and\n                sc.status_code = esp.setup_status and\n                mr.app_seq_num = esp.app_seq_num and\n                mf.merchant_number(+) = mr.merch_number and\n                mpo.app_seq_num(+) = mr.app_seq_num and\n                mpo.cardtype_code(+) =  :11  -- 16 \n        order by esp.status_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setDate(4,endDate);
   __sJT_st.setString(5,status);
   __sJT_st.setString(6,status);
   __sJT_st.setString(7,stringLookup);
   __sJT_st.setLong(8,longLookup);
   __sJT_st.setLong(9,longLookup);
   __sJT_st.setString(10,stringLookup);
   __sJT_st.setInt(11,mesConstants.APP_CT_AMEX);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.ops.AmexESADataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:977^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.add( new ESARequest(resultSet) );  
      }
      
      resultSet.close();
      it.close();
    }
    catch (Exception e)
    {
      logEntry("loadData()", e.toString());
    }
    finally
    {
      try { resultSet.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }
  
  public Vector loadErrorMessages( String errorCodes )
  {
    ResultSetIterator   it        = null;
    ResultSet           resultSet = null;
    Vector              retVal    = new Vector();
    String              token     = null;
    StringTokenizer     tokens    = null;
    
    try
    {
      connect();
      
      if (  errorCodes != null && 
            !errorCodes.equals("") &&
            !errorCodes.equals(BLANK_VALUE_STRING) )
      {
        tokens = new StringTokenizer( errorCodes, "," );
        
        while ( tokens.hasMoreTokens() )
        {
          token = tokens.nextToken().trim();
          
          /*@lineinfo:generated-code*//*@lineinfo:1022^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  ec.error_code,
//                      ec.error_desc
//              from    amex_esa_setup_error_codes ec
//              where   ec.error_code = :token
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ec.error_code,\n                    ec.error_desc\n            from    amex_esa_setup_error_codes ec\n            where   ec.error_code =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,token);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.ops.AmexESADataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1028^11*/
          resultSet = it.getResultSet();
          
          if ( resultSet.next() )
          {
            retVal.add( new ErrorMessage(resultSet) );
          }
          else if ( !token.equals("") )
          {
            retVal.add( new ErrorMessage(token) );
          }
          resultSet.close();
          it.close();
        }
      }
    }
    catch( Exception e )
    {
      logEntry("loadErrorMessages()", e.toString());
    }
    finally
    {
      try { resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e){}
      cleanUp();
    }
    return( retVal );
  }
  
  public ESARequest loadESARequest( long merchantId )
  {
    ESARequest  retVal  = null;
    try
    {
      // setup the summary search fields
      setData("beginDate","");
      setData("endDate","");
      setData("lookupValue",String.valueOf(merchantId));
      
      loadData();   // load the summary record
      
      if ( ReportRows.size() > 0 )
      {
        retVal = (ESARequest)ReportRows.first();
      }
    }
    catch( Exception e )
    {
      logEntry("loadESARequest(" + merchantId + ")", e.toString());
    }
    return( retVal );
  }
  
  public boolean needsEsaApp( long appSeqNum )
  {
    int   recCount    = 0;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1089^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(mpo.cardtype_code) 
//          from    merchpayoption mpo
//          where   mpo.app_seq_num = :appSeqNum and
//                  mpo.cardtype_code = :mesConstants.APP_CT_AMEX and -- 16 and
//                  not mpo.merchpo_rate is null and
//                  mpo.merchpo_card_merch_number is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(mpo.cardtype_code)  \n        from    merchpayoption mpo\n        where   mpo.app_seq_num =  :1  and\n                mpo.cardtype_code =  :2  and -- 16 and\n                not mpo.merchpo_rate is null and\n                mpo.merchpo_card_merch_number is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.ops.AmexESADataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_AMEX);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1097^7*/
    }
    catch(Exception e)
    {
      logEntry("needsEsaApp(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( recCount > 0 );
  }
  
  public void setFields(HttpServletRequest request)
  {
    super.setFields(request);
    
    Submitted  = HttpHelper.getBoolean(request,"submitted",false);
  }
  
  protected void submitAddress( int addrType, String fieldPrefix )
  {
    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      String whereClause = "app_seq_num = " + appSeqNum + 
                           " and addresstype_code = " + addrType;
      
      if ( getRowCount( "address", whereClause ) == 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1128^9*/

//  ************************************************************
//  #sql [Ctx] { insert into address
//            (
//              app_seq_num,
//              addresstype_code,
//              address_line1,
//              address_line2,
//              address_city,
//              countrystate_code,
//              address_zip,
//              address_phone
//            )
//            values
//            (
//              :appSeqNum,
//              :addrType,
//              :getData(fieldPrefix + "AddressLine1"),
//              :getData(fieldPrefix + "AddressLine2"),
//              :getData(fieldPrefix + "CszCity"),
//              :getData(fieldPrefix + "CszState"),
//              :getData(fieldPrefix + "CszZip"),
//              :getData(fieldPrefix + "Phone")
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_631 = getData(fieldPrefix + "AddressLine1");
 String __sJT_632 = getData(fieldPrefix + "AddressLine2");
 String __sJT_633 = getData(fieldPrefix + "CszCity");
 String __sJT_634 = getData(fieldPrefix + "CszState");
 String __sJT_635 = getData(fieldPrefix + "CszZip");
 String __sJT_636 = getData(fieldPrefix + "Phone");
   String theSqlTS = "insert into address\n          (\n            app_seq_num,\n            addresstype_code,\n            address_line1,\n            address_line2,\n            address_city,\n            countrystate_code,\n            address_zip,\n            address_phone\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,addrType);
   __sJT_st.setString(3,__sJT_631);
   __sJT_st.setString(4,__sJT_632);
   __sJT_st.setString(5,__sJT_633);
   __sJT_st.setString(6,__sJT_634);
   __sJT_st.setString(7,__sJT_635);
   __sJT_st.setString(8,__sJT_636);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1152^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1156^9*/

//  ************************************************************
//  #sql [Ctx] { update  address
//            set     address_line1        = :getData(fieldPrefix + "AddressLine1"),
//                    address_line2        = :getData(fieldPrefix + "AddressLine2"),
//                    address_city         = :getData(fieldPrefix + "CszCity"),
//                    countrystate_code    = :getData(fieldPrefix + "CszState"),
//                    address_zip          = :getData(fieldPrefix + "CszZip"),
//                    address_phone        = :getData(fieldPrefix + "Phone")
//            where   app_seq_num = :appSeqNum and
//                    addresstype_code = :addrType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_637 = getData(fieldPrefix + "AddressLine1");
 String __sJT_638 = getData(fieldPrefix + "AddressLine2");
 String __sJT_639 = getData(fieldPrefix + "CszCity");
 String __sJT_640 = getData(fieldPrefix + "CszState");
 String __sJT_641 = getData(fieldPrefix + "CszZip");
 String __sJT_642 = getData(fieldPrefix + "Phone");
   String theSqlTS = "update  address\n          set     address_line1        =  :1 ,\n                  address_line2        =  :2 ,\n                  address_city         =  :3 ,\n                  countrystate_code    =  :4 ,\n                  address_zip          =  :5 ,\n                  address_phone        =  :6 \n          where   app_seq_num =  :7  and\n                  addresstype_code =  :8";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_637);
   __sJT_st.setString(2,__sJT_638);
   __sJT_st.setString(3,__sJT_639);
   __sJT_st.setString(4,__sJT_640);
   __sJT_st.setString(5,__sJT_641);
   __sJT_st.setString(6,__sJT_642);
   __sJT_st.setLong(7,appSeqNum);
   __sJT_st.setInt(8,addrType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1167^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("submitAddress()", e.toString());
    }
  }
  
  public boolean submitAppData( )
  {
    int         cardNum     = 0;
    int         recCount    = 0;
    boolean     retVal      = false;
    
    try
    {
      connect();
      
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      if ( appSeqNum != 0L )
      {
        long groupNumber = getLong("affiliationAmexSeNum");
        
        /*@lineinfo:generated-code*//*@lineinfo:1192^9*/

//  ************************************************************
//  #sql [Ctx] { select  sum(decode(mpo.cardtype_code,
//                               :mesConstants.APP_CT_AMEX,1, -- 16,1,
//                               0)),
//                    nvl(max(mpo.card_sr_number),0)
//            
//            from    merchant          mr,
//                    merchpayoption    mpo
//            where   -- query through merchant to insure a return 
//                    -- value from shadow applications
//                    mr.app_seq_num = :appSeqNum and
//                    mpo.app_seq_num(+) = mr.app_seq_num
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum(decode(mpo.cardtype_code,\n                              :1 ,1, -- 16,1,\n                             0)),\n                  nvl(max(mpo.card_sr_number),0)\n           \n          from    merchant          mr,\n                  merchpayoption    mpo\n          where   -- query through merchant to insure a return \n                  -- value from shadow applications\n                  mr.app_seq_num =  :2  and\n                  mpo.app_seq_num(+) = mr.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.ops.AmexESADataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,mesConstants.APP_CT_AMEX);
   __sJT_st.setLong(2,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cardNum = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1205^9*/
        
        if ( recCount == 0 )
        {
          ++cardNum;
          
          /*@lineinfo:generated-code*//*@lineinfo:1211^11*/

//  ************************************************************
//  #sql [Ctx] { insert into merchpayoption
//              (
//                app_seq_num,
//                cardtype_code,
//                card_sr_number,
//                merchpo_rate,
//                annual_sales,
//                average_ticket,
//                group_merchant_number
//              )
//              values
//              (
//                :appSeqNum,
//                :mesConstants.APP_CT_AMEX,
//                :cardNum,
//                :getDouble("discountRate"),
//                :getDouble("estimatedAnnualVolume"),
//                :getDouble("estimatedAverageRoc"),
//                decode( :groupNumber, 0, null, :groupNumber)
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_643 = getDouble("discountRate");
 double __sJT_644 = getDouble("estimatedAnnualVolume");
 double __sJT_645 = getDouble("estimatedAverageRoc");
   String theSqlTS = "insert into merchpayoption\n            (\n              app_seq_num,\n              cardtype_code,\n              card_sr_number,\n              merchpo_rate,\n              annual_sales,\n              average_ticket,\n              group_merchant_number\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 ,\n              decode(  :7 , 0, null,  :8 )\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_AMEX);
   __sJT_st.setInt(3,cardNum);
   __sJT_st.setDouble(4,__sJT_643);
   __sJT_st.setDouble(5,__sJT_644);
   __sJT_st.setDouble(6,__sJT_645);
   __sJT_st.setLong(7,groupNumber);
   __sJT_st.setLong(8,groupNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1233^11*/
        }
        else
        {
          // update the Amex only fields
          /*@lineinfo:generated-code*//*@lineinfo:1238^11*/

//  ************************************************************
//  #sql [Ctx] { update  merchpayoption mpo
//              set     mpo.merchpo_rate          = :getDouble("discountRate"),
//                      mpo.annual_sales          = :getDouble("estimatedAnnualVolume"),
//                      mpo.average_ticket        = :getDouble("estimatedAverageRoc"),
//                      mpo.group_merchant_number = decode( :groupNumber, 0, null, :groupNumber)
//              where   mpo.app_seq_num = :appSeqNum and
//                      mpo.cardtype_code = :mesConstants.APP_CT_AMEX -- 16
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_646 = getDouble("discountRate");
 double __sJT_647 = getDouble("estimatedAnnualVolume");
 double __sJT_648 = getDouble("estimatedAverageRoc");
   String theSqlTS = "update  merchpayoption mpo\n            set     mpo.merchpo_rate          =  :1 ,\n                    mpo.annual_sales          =  :2 ,\n                    mpo.average_ticket        =  :3 ,\n                    mpo.group_merchant_number = decode(  :4 , 0, null,  :5 )\n            where   mpo.app_seq_num =  :6  and\n                    mpo.cardtype_code =  :7  -- 16";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_646);
   __sJT_st.setDouble(2,__sJT_647);
   __sJT_st.setDouble(3,__sJT_648);
   __sJT_st.setLong(4,groupNumber);
   __sJT_st.setLong(5,groupNumber);
   __sJT_st.setLong(6,appSeqNum);
   __sJT_st.setInt(7,mesConstants.APP_CT_AMEX);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1247^11*/
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:1250^9*/

//  ************************************************************
//  #sql [Ctx] { delete 
//            from    amex_esa_app_data ea
//            where   ea.app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n          from    amex_esa_app_data ea\n          where   ea.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1255^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:1257^9*/

//  ************************************************************
//  #sql [Ctx] { insert into amex_esa_app_data
//            (
//              app_seq_num,
//              ownership_type,
//              spid,
//              sic_code,
//              flat_fee,
//              cap_number_requested
//            )
//            values
//            (
//              :appSeqNum,
//              :getData("ownershipType"),
//              :getData("spid"),
//              :getData("sicCode"),
//              upper( :getData("flatFee") ),
//              upper( :getData("capNeeded") )
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_649 = getData("ownershipType");
 String __sJT_650 = getData("spid");
 String __sJT_651 = getData("sicCode");
 String __sJT_652 = getData("flatFee");
 String __sJT_653 = getData("capNeeded");
   String theSqlTS = "insert into amex_esa_app_data\n          (\n            app_seq_num,\n            ownership_type,\n            spid,\n            sic_code,\n            flat_fee,\n            cap_number_requested\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n            upper(  :5  ),\n            upper(  :6  )\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_649);
   __sJT_st.setString(3,__sJT_650);
   __sJT_st.setString(4,__sJT_651);
   __sJT_st.setString(5,__sJT_652);
   __sJT_st.setString(6,__sJT_653);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1277^9*/
        
        // update the iata number
        /*@lineinfo:generated-code*//*@lineinfo:1280^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant mr
//            set     mr.iata_number          = :getData("iataNumber"),
//                    mr.merch_legal_name     = :getData("legalBusinessName"),
//                    mr.merch_business_name  = :getData("dbaName"),
//                    mr.merch_federal_tax_id = :getData("federalTaxId")
//            where   mr.app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_654 = getData("iataNumber");
 String __sJT_655 = getData("legalBusinessName");
 String __sJT_656 = getData("dbaName");
 String __sJT_657 = getData("federalTaxId");
   String theSqlTS = "update  merchant mr\n          set     mr.iata_number          =  :1 ,\n                  mr.merch_legal_name     =  :2 ,\n                  mr.merch_business_name  =  :3 ,\n                  mr.merch_federal_tax_id =  :4 \n          where   mr.app_seq_num =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_654);
   __sJT_st.setString(2,__sJT_655);
   __sJT_st.setString(3,__sJT_656);
   __sJT_st.setString(4,__sJT_657);
   __sJT_st.setLong(5,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1288^9*/
        
        // add application fields only if they were not 
        // already assigned by the applicant
        submitAddress( mesConstants.ADDR_TYPE_BUSINESS, "business" );
        submitAddress( mesConstants.ADDR_TYPE_OWNER1, "signer" );
        submitAddress( mesConstants.ADDR_TYPE_CHK_ACCT_BANK, "bank" );
        
        submitBankInfo();
        submitBusinessOwner();
        submitContact();
        
        submitEsaApp(appSeqNum);
        
        /*@lineinfo:generated-code*//*@lineinfo:1302^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1305^9*/
      }        
      
      retVal = true;
    }
    catch( Exception e )
    {
      logEntry("submitAppData()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  protected void submitBankInfo( )
  {
    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      String whereClause = "app_seq_num = " + appSeqNum + 
                           " and merchbank_acct_srnum = 1";
                           
      if ( getRowCount( "merchbank", whereClause ) == 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1332^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchbank
//            (
//              app_seq_num,
//              merchbank_name,
//              bankacc_type,
//              merchbank_acct_srnum,
//              merchbank_acct_num,
//              merchbank_transit_route_num
//            )
//            values
//            (
//              :appSeqNum,
//              :getData("bankName"),
//              2,      -- default to checking
//              1,      -- primary account
//              :getData("ddaNumber"),
//              :getData("abaNumber")
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_658 = getData("bankName");
 String __sJT_659 = getData("ddaNumber");
 String __sJT_660 = getData("abaNumber");
   String theSqlTS = "insert into merchbank\n          (\n            app_seq_num,\n            merchbank_name,\n            bankacc_type,\n            merchbank_acct_srnum,\n            merchbank_acct_num,\n            merchbank_transit_route_num\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n            2,      -- default to checking\n            1,      -- primary account\n             :3 ,\n             :4 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_658);
   __sJT_st.setString(3,__sJT_659);
   __sJT_st.setString(4,__sJT_660);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1352^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1356^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchbank
//            set     merchbank_name              = :getData("bankName"),
//                    merchbank_acct_num          = :getData("ddaNumber"),
//                    merchbank_transit_route_num = :getData("abaNumber")
//            where   app_seq_num   = :appSeqNum and
//                    merchbank_acct_srnum  = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_661 = getData("bankName");
 String __sJT_662 = getData("ddaNumber");
 String __sJT_663 = getData("abaNumber");
   String theSqlTS = "update  merchbank\n          set     merchbank_name              =  :1 ,\n                  merchbank_acct_num          =  :2 ,\n                  merchbank_transit_route_num =  :3 \n          where   app_seq_num   =  :4  and\n                  merchbank_acct_srnum  = 1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_661);
   __sJT_st.setString(2,__sJT_662);
   __sJT_st.setString(3,__sJT_663);
   __sJT_st.setLong(4,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1364^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("submitBankInfo()",e.toString());
    }
  }
  
  protected void submitBusinessOwner( )
  {
    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      String whereClause = "app_seq_num = " + appSeqNum + 
                           " and busowner_num = 1";
                           
      if ( getRowCount( "businessowner", whereClause ) == 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1384^9*/

//  ************************************************************
//  #sql [Ctx] { insert into businessowner
//            (
//              app_seq_num,
//              busowner_num,
//              busowner_first_name,
//              busowner_last_name,
//              busowner_ssn,
//              busowner_title
//            )
//            values
//            (
//              :appSeqNum,
//              1,
//              :getData("signerNameFirst"),
//              :getData("signerNameLast"),
//              :getData("primaryAuthSignerSSN"),
//              :getData("primaryAuthSignerTitle")
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_664 = getData("signerNameFirst");
 String __sJT_665 = getData("signerNameLast");
 String __sJT_666 = getData("primaryAuthSignerSSN");
 String __sJT_667 = getData("primaryAuthSignerTitle");
   String theSqlTS = "insert into businessowner\n          (\n            app_seq_num,\n            busowner_num,\n            busowner_first_name,\n            busowner_last_name,\n            busowner_ssn,\n            busowner_title\n          )\n          values\n          (\n             :1 ,\n            1,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_664);
   __sJT_st.setString(3,__sJT_665);
   __sJT_st.setString(4,__sJT_666);
   __sJT_st.setString(5,__sJT_667);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1404^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1408^9*/

//  ************************************************************
//  #sql [Ctx] { update  businessowner
//            set     busowner_first_name = :getData("signerNameFirst"),
//                    busowner_last_name  = :getData("signerNameLast"),
//                    busowner_ssn    = :getData("primaryAuthSignerSSN"),
//                    busowner_title  = :getData("primaryAuthSignerTitle")
//            where   app_seq_num   = :appSeqNum and
//                    busowner_num  = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_668 = getData("signerNameFirst");
 String __sJT_669 = getData("signerNameLast");
 String __sJT_670 = getData("primaryAuthSignerSSN");
 String __sJT_671 = getData("primaryAuthSignerTitle");
   String theSqlTS = "update  businessowner\n          set     busowner_first_name =  :1 ,\n                  busowner_last_name  =  :2 ,\n                  busowner_ssn    =  :3 ,\n                  busowner_title  =  :4 \n          where   app_seq_num   =  :5  and\n                  busowner_num  = 1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_668);
   __sJT_st.setString(2,__sJT_669);
   __sJT_st.setString(3,__sJT_670);
   __sJT_st.setString(4,__sJT_671);
   __sJT_st.setLong(5,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1417^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("submitBusinessOwner()",e.toString());
    }
  }
  
  protected void submitContact( )
  {
    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      if ( getRowCount( "merchcontact" ) == 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1434^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchcontact
//            (
//              app_seq_num,
//              merchcont_prim_first_name,
//              merchcont_prim_last_name
//            )
//            values
//            (
//              :appSeqNum,
//              :getData("contactNameFirst"),
//              :getData("contactNameLast")
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_672 = getData("contactNameFirst");
 String __sJT_673 = getData("contactNameLast");
   String theSqlTS = "insert into merchcontact\n          (\n            app_seq_num,\n            merchcont_prim_first_name,\n            merchcont_prim_last_name\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_672);
   __sJT_st.setString(3,__sJT_673);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1448^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1452^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchcontact
//            set   merchcont_prim_first_name = :getData("contactNameFirst"),
//                  merchcont_prim_last_name  = :getData("contactNameLast")
//            where app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_674 = getData("contactNameFirst");
 String __sJT_675 = getData("contactNameLast");
   String theSqlTS = "update  merchcontact\n          set   merchcont_prim_first_name =  :1 ,\n                merchcont_prim_last_name  =  :2 \n          where app_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_674);
   __sJT_st.setString(2,__sJT_675);
   __sJT_st.setLong(3,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1458^9*/
      }
    }
    catch(Exception e)
    {
      logEntry("submitContact()",e.toString());
    }
  }
  
  protected void submitEsaApp( long appSeqNum )
  {
    try
    {
      connect();
      
      if ( getRowCount("amex_esa_setup_process") == 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1475^9*/

//  ************************************************************
//  #sql [Ctx] { insert into amex_esa_setup_process 
//            (
//              app_seq_num,
//              setup_status,
//              status_date
//            )
//            values
//            (
//              :appSeqNum,
//              :ESA_SUBMIT,
//              sysdate
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into amex_esa_setup_process \n          (\n            app_seq_num,\n            setup_status,\n            status_date\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n            sysdate\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,ESA_SUBMIT);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1489^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1493^9*/

//  ************************************************************
//  #sql [Ctx] { update amex_esa_setup_process
//            set setup_status = :ESA_RESUBMIT,
//                status_date = sysdate,
//                batch_id = null
//            where app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update amex_esa_setup_process\n          set setup_status =  :1 ,\n              status_date = sysdate,\n              batch_id = null\n          where app_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.ops.AmexESADataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,ESA_RESUBMIT);
   __sJT_st.setLong(2,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1500^9*/
      }            
    }
    catch( Exception e )
    {
      logEntry("submitEsaApp(" + appSeqNum + ")", e.toString());
    }        
    finally
    {
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/