package com.mes.startup;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.apache.log4j.Logger;
import com.cielo.security.IPasswordHashing;
import com.cielo.security.PasswordHashingException;
import com.cielo.security.PasswordHashingFactory;
import com.mes.data.DaoException;
import com.mes.data.DaoTools;
import com.mes.data.MesDataSourceManager;
import com.mes.data.bp.user.MerchantUser;
import com.mes.data.bp.user.User;
import com.mes.data.bp.user.UserService;
import com.mes.data.merchant.Merchant;
import com.mes.data.merchant.MerchantService;
import com.mes.data.sales_channel.SalesChannelOrganization;
import com.mes.data.sales_channel.UserCreationSettings;

public class RefreshHierarchy extends EventBase{

	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(RefreshHierarchy.class);
	private static HashSet<Integer> banksThatNeedInsertMifNonBankCards;
	
	static{
		banksThatNeedInsertMifNonBankCards = new HashSet<Integer>();
		banksThatNeedInsertMifNonBankCards.add(3941);
		banksThatNeedInsertMifNonBankCards.add(3942);
	}
	
	@Override
	public boolean execute(){
		Connection con = null;
		try {
			con = MesDataSourceManager.INSTANCE.getDataSource().getConnection();
			// get a process sequence
			long procSequence = DaoTools.getSequenceNextVal(con, "hierarchy_refresh_sequence");

			HierarchyProcessDao hierarchyProcessDao = new HierarchyProcessDao(con);
			List<Merchant> merchants = hierarchyProcessDao.getMerchantsToProcess();
			for (Merchant merchant : merchants) {
				try {
					log.info("Processing Merchant: " + merchant.getMerchantId());
					con.setAutoCommit(false);
					hierarchyProcessDao.markJobAsStarted(merchant);
					processMerchant(con, merchant);
					hierarchyProcessDao.markJobAsCompleted(procSequence, merchant);
					con.commit();
					log.info("Successfully processed Merchant: " + merchant.getMerchantId());
				}
				catch (Exception e) {
					try {
						con.rollback();
					}
					catch (SQLException e1) {
						log.error("Failed to rollback failed process for Merchant: " + merchant.getMerchantId(), e1);
					}
					log.error("Failed to process Merchant: " + merchant.getMerchantId(), e);
				}
			}
		}
		catch (Exception e2) {
			log.error("Failed to execute RefreshHierarchy", e2);
		}
		finally{
			close(con);
		}
		return false;
	}

	private static void processMerchant(Connection con, Merchant merchant) {
		MerchantService merchantService = new MerchantService();
		merchantService.linkToOrganization(merchant);
		log.info("Linked Merchant: " + merchant.getMerchantId() + " to Organization: " + merchant.getOrganizationId());
		
		// create new BP User
		UserService userService = new UserService();
		if (userService.getByLoginName(Long.toString(merchant.getMerchantId())) == null) {
			UserCreationSettings userCreationSettings = getUserCreationSettings(merchant);
			if(userCreationSettings != null && userCreationSettings.isAutoCreate()){
				String userPassword = getNewUserPassword(userCreationSettings, merchant);
				String userPasswordHashed;
				try {
					userPasswordHashed = hashPassword(userPassword);
				}
				catch (PasswordHashingException e) {
					throw new RuntimeException("Failed to hash password while trying to create new User for Merchant :" + merchant.getMerchantId(), e);
				}
				User newUser = new MerchantUser(merchant, userCreationSettings.getUserType(), Long.toString(merchant.getMerchantId()), merchant.getDbaName(), userPasswordHashed, 2, merchant.getMerchantApplication() == null ? null : merchant.getMerchantApplication().getEmailAddress(), !userCreationSettings.isCreateAsDisabled());
				userService.insert(newUser, "SYSTEM");
				log.info("Created new BP User: " + newUser);
			}
		}

		// update mif_non_bank_cards if necessary (bank 3941 and 3942)
		if(merchant.getMerchantApplication() != null && banksThatNeedInsertMifNonBankCards.contains(merchant.getBankId())){
			MifNonBankCardsDao mifNonBankCardsDao = new MifNonBankCardsDao(con);
			if (!mifNonBankCardsDao.hasMifNonBankCardsRecord(merchant)) {
				MerchPayOptionDao merchPayOptionDao = new MerchPayOptionDao(con);
				Long discoverMerchantId = merchPayOptionDao.getCardBrandMerchantId(14, merchant.getMerchantApplication().getId());
				Long amexMerchantId = merchPayOptionDao.getCardBrandMerchantId(16, merchant.getMerchantApplication().getId());
				Long dinersMerchantId = merchPayOptionDao.getCardBrandMerchantId(10, merchant.getMerchantApplication().getId());
				Long jcbMerchantId = merchPayOptionDao.getCardBrandMerchantId(15, merchant.getMerchantApplication().getId());

				mifNonBankCardsDao.insertMifNonBankCard(merchant, discoverMerchantId, amexMerchantId, dinersMerchantId, jcbMerchantId);
				log.info("Created new MIF_NON_BANK_CARDS record for Merchant: " + merchant.getMerchantId());
			}
		}
	}

	private static String hashPassword(String plainTextPassword) throws PasswordHashingException {
		PasswordHashingFactory passwordHashingFactory = PasswordHashingFactory.getInstance();
		IPasswordHashing ipasswordHashing = passwordHashingFactory.getHashingStrategy(PasswordHashingFactory.ALGORITHM_PBKDF2);
		return ipasswordHashing.createHash(plainTextPassword);
	}

	private static UserCreationSettings getUserCreationSettings(Merchant merchant) {
		SalesChannelOrganization salesChannelOrganization = merchant.getSalesChannelOrganization();
		UserCreationSettings userCreationSettings = salesChannelOrganization.getUserCreationSettings();
		while (userCreationSettings == null && salesChannelOrganization != null) {
			salesChannelOrganization = salesChannelOrganization.getParent();
			if (salesChannelOrganization != null) {
				userCreationSettings = salesChannelOrganization.getUserCreationSettings();
			}
		}
		return userCreationSettings;
	}

	private static String getNewUserPassword(UserCreationSettings userCreationSettings, Merchant merchant) {
		switch (userCreationSettings.getInitialPasswordType()) {
		case PHONE_NUMBER:
			return merchant.getPhone();
		case MERCHANT_ID:
			return Long.toString(merchant.getMerchantId());
		default:
			throw new IllegalStateException("Unhandeled InitialPasswordType found: " + userCreationSettings.getInitialPasswordType());
		}
	}

	private static void close(AutoCloseable closeable){
		try {
			closeable.close();
		}
		catch (Exception e) {}
	}
	
	/**
	 * DAO class to access MIF_NON_BANK_CARDS table
	 * @author mbush
	 *
	 */
	private static class MifNonBankCardsDao {
		//@formatter:off
		private static final String SQL_INSERT = 
				"INSERT INTO MIF_NON_BANK_CARDS "
				+ "(MERCHANT_NUMBER, DISCOVER, AMEX, DINERS, JCB) "
				+ "VALUES(?, ?, ?, ?, ?)";
		
		private static final String SQL_SELECT_COUNT_BY_MERCHANT_ID = 
				"SELECT "
				+ "		COUNT(*) AS NUM_RECORDS "
				+ "FROM "
				+ "		MIF_NON_BANK_CARDS "
				+ "WHERE "
				+ "		MERCHANT_NUMBER = ?";
		//@formatter:on

		private Connection con;

		MifNonBankCardsDao(Connection con) {
			this.con = con;
		}

		boolean hasMifNonBankCardsRecord(Merchant merchant) {
			PreparedStatement ps = null;
			try {
				ps = con.prepareStatement(SQL_SELECT_COUNT_BY_MERCHANT_ID);
				ps.setLong(1, merchant.getMerchantId());
				ResultSet rs = null;
				try {
					rs = ps.executeQuery();
					rs.next();
					return rs.getLong("NUM_RECORDS") > 0;
				}
				finally{
					close(rs);
				}
			}
			catch (Exception e) {
				throw new DaoException("Failed to select count(*) from MIF_NON_BANK_CARDS for Merchant: " + merchant.getMerchantId(), e);
			}
			finally{
				close(ps);
			}
		}

		void insertMifNonBankCard(Merchant merchant, Long discoverMerchantId, Long amexMerchantId, Long dinersMerchantId, Long jcbMerchantId) {
			PreparedStatement ps = null;
			try {
				ps = con.prepareStatement(SQL_INSERT);
				ps.setLong(1, merchant.getMerchantId());
				DaoTools.setLong(ps, 2, discoverMerchantId);
				DaoTools.setLong(ps, 3, amexMerchantId);
				DaoTools.setLong(ps, 4, dinersMerchantId);
				DaoTools.setLong(ps, 5, jcbMerchantId);
				ps.executeUpdate();
			}
			catch (Exception e) {
				throw new DaoException("Failed to insert into MIF_NON_BANK_CARDS for Merchant: " + merchant.getMerchantId(), e);
			}
			finally{
				close(ps);
			}
		}
	}

	/**
	 * DAO class to access MERCHPAYOPTION table
	 * @author mbush
	 *
	 */
	private static class MerchPayOptionDao {
		//@formatter:off
		private static final String SQL_SELECT_BY_CARD_TYPE_AND_APPLICATION = 
				"SELECT "
				+ "		MERCHPO_CARD_MERCH_NUMBER "
				+ "FROM "
				+ "		MERCHPAYOPTION "
				+ "WHERE "
				+ "		CARDTYPE_CODE = ? AND "
				+ "		APP_SEQ_NUM = ?";
		//@formatter:on

		private Connection con;

		MerchPayOptionDao(Connection con) {
			this.con = con;
		}

		Long getCardBrandMerchantId(int cardBrandCode, long merchantApplicationId) {
			PreparedStatement ps = null;
			try {
				ps = con.prepareStatement(SQL_SELECT_BY_CARD_TYPE_AND_APPLICATION);
				ps.setInt(1, cardBrandCode);
				ps.setLong(2, merchantApplicationId);
				ResultSet rs = null;
				try {
					rs = ps.executeQuery();
					if (rs.next()) {
						return DaoTools.getLong(rs, "merchpo_card_merch_number");
					}
					return null;
				}
				finally{
					close(rs);
				}
			}
			catch (Exception e) {
				throw new DaoException("Failed to get MERCHPO_CARD_MERCH_NUMBER from MERCHPAYOPTION for CARDTYPE_CODE= " + cardBrandCode + " and APP_SEQ_NUM=" + merchantApplicationId, e);
			}
			finally{
				close(ps);
			}
		}

	}

	/**
	 * DAO class to access HIERARCHY_PROCESS table
	 * @author mbush
	 *
	 */
	private static class HierarchyProcessDao {
		//@formatter:off
		private static final String SQL_SELECT_UNPROCESSED = 
				"SELECT "
				+ "		M.MERCHANT_NUMBER MERCHANT_NUMBER "

				+ "FROM "
				+ "		HIERARCHY_PROCESS HP "
				+ "		INNER JOIN MIF M ON HP.MERCHANT_NUMBER = M.MERCHANT_NUMBER "
				+ "WHERE "
				+ "		HP.PROCESS_SEQUENCE IS NULL";
		
		private static final String SQL_UPDATE_STARTED = 
				"UPDATE HIERARCHY_PROCESS SET "
				+ "		DATE_STARTED = SYSDATE "
				+ "WHERE "
				+ "		DATE_STARTED IS NULL AND "
				+ "		PROCESS_SEQUENCE IS NULL AND "
				+ "		MERCHANT_NUMBER = ?";
		
		private static final String SQL_UPDATE_COMPLETED = 
				"UPDATE HIERARCHY_PROCESS SET "
				+ "		PROCESS_SEQUENCE = ?, "
				+ "		COMPLETE = 'Y', "
				+ "		DATE_COMPLETED = SYSDATE "
				+ "WHERE "
				+ "		DATE_STARTED IS NOT NULL AND"
				+ "		PROCESS_SEQUENCE IS NULL AND "
				+ "		MERCHANT_NUMBER  =  ?";
		//@formatter:on

		private Connection con;

		HierarchyProcessDao(Connection con) {
			this.con = con;
		}

		List<Merchant> getMerchantsToProcess() {

			MerchantService merchantService = new MerchantService();
			List<Merchant> merchants = new ArrayList<Merchant>();
			PreparedStatement ps = null;
			try {
				ps = con.prepareStatement(SQL_SELECT_UNPROCESSED);
				ResultSet rs = null;
				try {
					rs = ps.executeQuery();
					while (rs.next()) {
						merchants.add(merchantService.getById(rs.getLong("MERCHANT_NUMBER")));
					}
					return merchants;
				}
				finally{
					close(rs);
				}
			}
			catch (Exception e) {
				throw new DaoException("Failed get Merchants that need to be process", e);
			}
			finally{
				close(ps);
			}
		}

		void markJobAsStarted(Merchant merchant) {
			PreparedStatement ps = null;
			int rowsAffected = 0;
			try {
				ps = con.prepareStatement(SQL_UPDATE_STARTED);
				ps.setLong(1, merchant.getMerchantId());
				rowsAffected = ps.executeUpdate();
			}
			catch (Exception e) {
				throw new DaoException("Failed to mark job as started for Merchant: " + merchant.getMerchantId(), e);
			}
			finally{
				close(ps);
			}
			if(rowsAffected == 0){
				throw new DaoException("No rows affected; Job already started by another process or does not exist");
			}
		}

		void markJobAsCompleted(long processSequence, Merchant merchant) {
			PreparedStatement ps = null;
			int rowsAffected = 0;
			try {
				ps = con.prepareStatement(SQL_UPDATE_COMPLETED);
				ps.setLong(1, processSequence);
				ps.setLong(2, merchant.getMerchantId());
				rowsAffected = ps.executeUpdate();
			}
			catch (Exception e) {
				throw new DaoException("Failed to mark job as completed for Merchant: " + merchant.getMerchantId(), e);
			}
			finally{
				close(ps);
			}
			if(rowsAffected == 0){
				throw new DaoException("No rows affected; Job already completed by another process or does not exist");
			}
		}
	}

}
