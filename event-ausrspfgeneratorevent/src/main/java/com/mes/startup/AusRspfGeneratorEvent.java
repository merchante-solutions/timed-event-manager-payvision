package com.mes.startup;

import org.apache.log4j.Logger;
import com.mes.aus.file.RspfGenerator;

public final class AusRspfGeneratorEvent extends EventBase
{
  static Logger log = Logger.getLogger(AusRspfGeneratorEvent.class);

  public boolean execute()
  {
    try
    {
      RspfGenerator.generate();
      return true;
    }
    catch (Exception e)
    {
      log.error("AUS request file process event error: " + e);
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }

    return false;
  }
  
    public static void main(String[] args) {

    int exitVal = 0;

    try {
      AusRspfGeneratorEvent arge = new AusRspfGeneratorEvent();
      arge.setEventArgs(args);
      arge.execute();
    }
    catch (Exception e) {
      log.error(e);
      exitVal = -1;
    }

    System.exit(exitVal);
  }
}