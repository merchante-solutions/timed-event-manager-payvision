Description:
============
This document describes the build and packaging instructions for timed event manager.


##Build timed-event-manager:

**Maven Commands to run build**

`mvn clean install`

This command generates deployment artifact for timed-event-manager:  

_timed-event-manager-<Version>-SNAPSHOT.jar_  

##Package dependencies:

`mvn assembly:single`

This command generates bundles of dependent artifacts for the timed-event-manager library:  

_timed-event-manager-<Version>-SNAPSHOT-bin.tar.gz_  
