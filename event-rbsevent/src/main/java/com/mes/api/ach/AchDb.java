package com.mes.api.ach;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.safehaus.uuid.UUID;
import org.safehaus.uuid.UUIDGenerator;
import com.mes.api.TridentApiConstants;
import com.mes.database.BaseDb;
import com.mes.database.DbcType;
import com.mes.support.MesAccountNumber;

public class AchDb extends BaseDb
{
  static Logger log = Logger.getLogger(AchDb.class);

  public class AccountNumHelper
  {
    public String accountNum;
    public String accountNumTrunc;
    public byte[] accountNumEnc;
    public String profileId;
    public String merchNum;
    public String nodeId;

    public AccountNumHelper(AchRequest request)
    {
      profileId = request.getProfileId();
      if (request.getProfile() != null)
      {
        merchNum = ""+request.getProfile().getMerchantId();
      }
      accountNum = getAccountNum(request);
      if (accountNum != null)
      {
        MesAccountNumber man = new MesAccountNumber(accountNum);
        accountNumTrunc = man.truncated();
        accountNumEnc = man.getEncodedData();
      }
      nodeId = ""+request.getCardStoreNodeId();
    }

    private String getAccountNum(AchRequest r)
    {
      if (r instanceof AchTranRequest)
      {
        return ((AchTranRequest)r).getAccountNum();
      }
      else if (r instanceof AchStoreRequest)
      {
        return ((AchStoreRequest)r).getAccountNum();
      }
      return null;
    }

    public String toString()
    {
      return "AccountNumHelper [ "
        + "accountNum: " + accountNum
        + ", accountNumTrunc: " + accountNumTrunc
        + ", accountNumEnc: " + accountNumEnc
        + ", profileId: " + profileId
        + ", merchNum: " + merchNum
        + ", nodeId: " + nodeId
        + " ]";
    }
  }

  public AchDb(DbcType dbc)
  {
    super(DbcType.DIRECT.equals(dbc),"achp_id_sequence","achp_config");
  }
  public AchDb()
  {
    this(DbcType.POOL);
  }

  /**
   * Method template.
   */
  public void skeleton(String id)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
    }
    catch (Exception e)
    {
      handleException("skeleton(id=" + id + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  private void assignId(AchRequest request) throws Exception
  {
    if (request.getRecId() != 0L) return;
    request.setRecId(getNewId());
  }

  public boolean storeAchTranRequest(AchTranRequest request)
  {
    PreparedStatement ps = null;
    
    try
    {
      // get encoded and truncated versions of account number
      AccountNumHelper anh = new AccountNumHelper(request);

      ps = connectPrepared(
        " insert into achp_pmg_requests (   " +
        "  profile_id,                      " +
        "  request_ts,                      " +
        "  trident_tran_id,                 " +
        "  ref_num,                         " +
        "  auth_type,                       " +
        "  transit_num,                     " +
        "  account_num,                     " +
        "  account_num_enc,                 " +
        "  account_type,                    " +
        "  amount,                          " +
        "  cust_name,                       " +
        "  cust_id,                         " +
        "  recur_flag,                      " +
        "  server_name,                     " +
        "  tran_type,                       " +
        "  tpg_msg_id,                      " +
        "  tpg_msg,                         " +
        "  test_flag,                       " +
        "  ip_address,                      " +
        "  tran_source,                     " +
        "  account_id )                     " +
        " values (                          " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )   ");

      int col = 1;
      ps.setString    (col++,trunc(request.getProfileId(),20));
      ps.setTimestamp (col++,toTimestamp(request.getRequestDate()));
      ps.setString    (col++,trunc(request.getTridentTranId(),40));
      ps.setString    (col++,trunc(request.getRefNum(),96));
      ps.setString    (col++,trunc(request.getAuthType(),4));
      ps.setString    (col++,trunc(request.getTransitNum(),9));
      ps.setString    (col++,trunc(anh.accountNumTrunc,24));
      ps.setBytes     (col++,anh.accountNumEnc);
      ps.setString    (col++,trunc(request.getAccountType(),1));
      ps.setBigDecimal(col++,request.getAmount());
      ps.setString    (col++,trunc(request.getCustName(),22));
      ps.setString    (col++,trunc(request.getCustId(),15));
      ps.setString    (col++,trunc(request.getRecurFlag(),1));
      ps.setString    (col++,trunc(request.getServerName(),32));
      ps.setString    (col++,trunc(request.getRequestType(),8));
      ps.setString    (col++,trunc(request.getErrorCode(),6));
      ps.setString    (col++,trunc(request.getErrorDesc(),400));
      ps.setString    (col++,trunc(request.getTestFlag(),1));
      ps.setString    (col++,trunc(request.getIpAddress(),16));
      ps.setString    (col++,trunc(request.getTranSource(),8));
      ps.setString    (col++,trunc(request.getAccountId(),32));
      ps.executeUpdate();
      return true;
    }
    catch (Exception e)
    {
      handleException("storeAchTranRequest(" + request + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
    return false;
  }

  public boolean storeAchStoreRequest(AchStoreRequest request)
  {
    PreparedStatement ps = null;
    
    try
    {
      // get encoded and truncated versions of account number
      AccountNumHelper anh = new AccountNumHelper(request);

      ps = connectPrepared(
        " insert into achp_pmg_requests ( " +
        "  profile_id,          " +
        "  request_ts,          " +
        "  trident_tran_id,     " +
        "  account_num,         " +
        "  account_num_enc,     " +
        "  server_name,         " +
        "  tran_type,           " +
        "  tpg_msg_id,          " +
        "  tpg_msg,             " +
        "  test_flag,           " +
        "  tran_source,         " +
        "  account_id )         " +
        " values (              " +
        "  ?, ?, ?, ?, ?, ?,    " +
        "  ?, ?, ?, ?, ?, ? )   ");

      int col = 1;
      ps.setString    (col++,trunc(request.getProfileId(),20));
      ps.setTimestamp (col++,toTimestamp(request.getRequestDate()));
      ps.setString    (col++,trunc(request.getTridentTranId(),40));
      ps.setString    (col++,trunc(anh.accountNumTrunc,24));
      ps.setBytes     (col++,anh.accountNumEnc);
      ps.setString    (col++,trunc(request.getServerName(),32));
      ps.setString    (col++,trunc(request.getRequestType(),8));
      ps.setString    (col++,trunc(request.getErrorCode(),6));
      ps.setString    (col++,trunc(request.getErrorDesc(),400));
      ps.setString    (col++,trunc(request.getTestFlag(),1));
      ps.setString    (col++,trunc(request.getTranSource(),8));
      ps.setString    (col++,trunc(request.getAccountId(),32));
      ps.executeUpdate();
      return true;
    }
    catch (Exception e)
    {
      handleException("storeAchStoreRequest(" + request + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
    return false;
  }

  public boolean storeAchDeleteRequest(AchDeleteRequest request)
  {
    PreparedStatement ps = null;
    
    try
    {
      ps = connectPrepared(
        " insert into achp_pmg_requests ( " +
        "  profile_id,                    " +
        "  request_ts,                    " +
        "  trident_tran_id,               " +
        "  server_name,                   " +
        "  tran_type,                     " +
        "  tpg_msg_id,                    " +
        "  tpg_msg,                       " +
        "  test_flag,                     " +
        "  tran_source,                   " +
        "  account_id )                   " +
        " values (                        " +
        "  ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ");

      int col = 1;
      ps.setString    (col++,trunc(request.getProfileId(),20));
      ps.setTimestamp (col++,toTimestamp(request.getRequestDate()));
      ps.setString    (col++,trunc(request.getTridentTranId(),40));
      ps.setString    (col++,trunc(request.getServerName(),32));
      ps.setString    (col++,trunc(request.getRequestType(),8));
      ps.setString    (col++,trunc(request.getErrorCode(),6));
      ps.setString    (col++,trunc(request.getErrorDesc(),400));
      ps.setString    (col++,trunc(request.getTestFlag(),1));
      ps.setString    (col++,trunc(request.getTranSource(),8));
      ps.setString    (col++,trunc(request.getAccountId(),32));
      ps.executeUpdate();
      return true;
    }
    catch (Exception e)
    {
      handleException("storeAchDeleteRequest(" + request + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
    return false;
  }

  public static boolean storeAchRequest(AchRequest request)
  {
    if (request instanceof AchTranRequest)
    {
      return (new AchDb()).storeAchTranRequest((AchTranRequest)request);
    }
    else if (request instanceof AchStoreRequest)
    {
      return (new AchDb()).storeAchStoreRequest((AchStoreRequest)request);
    }
    else if (request instanceof AchDeleteRequest)
    {
      return (new AchDb()).storeAchDeleteRequest((AchDeleteRequest)request);
    }
    else
    {
      log.error("Unsupported ACH request type: " + request.getClass().getName());
    }
    return false;
  }

  /**
   * Fetch transit number records more recent then date given.  No more than
   * max records will be returned.
   */
  public List lookupNewTransitNumRecords(Integer lastRecId, int max)
  {
    PreparedStatement ps = null;
    List recs = new ArrayList();
    
    try
    {
      ps = connectPrepared(
        " select                  " +
        "  rec_id,                " +
        "  transit_routing_num,   " +
        "  bank_name,             " +
        "  fraud_flag             " +
        " from                    " +
        "  rap_app_bank_aba_test  " +
        " where                   " +
        "  rec_id > ?             " +
        " order by rec_id         ");
      ps.setInt(1,lastRecId.intValue()); 
      ResultSet rs = ps.executeQuery();
      int count = 0;
      while (rs.next() && count < max)
      {
        TransitNumRecord rec = 
          new TransitNumRecord(
            rs.getInt("rec_id"),
            rs.getString("transit_routing_num"),
            rs.getString("bank_name"),
            rs.getString("fraud_flag"));
        recs.add(rec);
        ++count;
      }
    }
    catch (Exception e)
    {
      handleException("lookupNewTransitNumRecords(lastRecId="
        + lastRecId + ", max=" + max + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
    return recs;
  }

  private String createTridentTranId(String id)
  {
    String str = "ACHPSYSTEM" + System.currentTimeMillis() + id;
    UUID uuId = UUIDGenerator.getInstance().generateNameBasedUUID(null,str);
    return uuId.toString().replaceAll("-","");
  }

  /**
   * Base query string to search for stored account records.  Requires
   * store node id to be determined in separate query (use 
   * Transaction.getCardStoreNodeId()).
   */
  private String baseStoredAcctQs =
      " select                                        " +
      "  ad.profile_id,                               " +
      "  ad.merch_num,                                " +
      "  ad.account_id,                               " +
      "  ad.account_num         account_num_trunc,    " +
      "  dukpt_decrypt_wrapper( ad.account_num_enc )  " +
      "                         account_num           " +
      " from                                          " +
      "  organization      o,                         " +
      "  group_merchant    gm,                        " +
      "  achp_account_data ad                         " +
      " where                                         " +
      "  o.org_group = ?                              " +
      "  and gm.org_num = o.org_num                   " +
      "  and ad.merch_num = gm.merchant_number        ";

  /**
   * Modify base query string above as follows:
   *
   *  search by accountId
   *
   *   and ad.account_id = :accountId
   *  
   *  search by truncated account num
   *
   *   and ad.account_num = :accountNumTrunc
   */
      
  /**
   * Determine if the given account number is already stored for the
   * merchant number.  An account must match account number and merchant
   * number to be considered a duplicate or match merchant number
   * under profile association node if account store association feature
   * enabled in profile.
   *
   * Now relies on separate call to fetch node id in request base class.
   
   * Returns account ID if duplicate account found.
   *
   * TODO: potential for duplicate account numbers with different transit
   *       routing numbers exists due to stripping out transit num 
   */
  private String findDuplicateAccount(AccountNumHelper anh)
  {
    PreparedStatement ps = null;

    try
    {
      ps = connectPrepared(baseStoredAcctQs +
        " and ad.account_num = '" + anh.accountNumTrunc + "' ");
      ps.setString(1,anh.nodeId);
      ResultSet rs = ps.executeQuery();
      while (rs.next())
      {
        if (rs.getString("account_num").equals(anh.accountNum))
        {
          return rs.getString("account_id");
        }
      }
    }
    catch (Exception e)
    {
      handleException("findDuplicateAccount(anh=" + anh + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
    return null;
  }
  
  /**
   * Store account data.  Checks for duplicate account data, if none found
   * a new account ID is generated and the account data is stored in 
   * achp_account_data.  The request is updated with the new account ID.
   *
   * Duplicate account data will cause an AchValidationException.
   */
  public boolean storeAccountData(AchStoreRequest request) 
  {
    PreparedStatement ps = null;

    try
    {
      // get encoded and truncated versions of account number
      AccountNumHelper anh = new AccountNumHelper(request);

      // check for duplicate stored account data
      String accountId = findDuplicateAccount(anh);
      if (accountId != null)
      {
        // have request return account id found (but as OK response)
        request.setAccountId(accountId);
        return true;
      }

      // generate new account id and place in request
      accountId = createTridentTranId(request.getProfileId());
      request.setAccountId(accountId);

      ps = connectPrepared(
        " insert into           " +
        "  achp_account_data (  " +
        "   profile_id,         " +
        "   account_id,         " +
        "   account_num,        " +
        "   account_num_enc )   " +
        " values ( ?, ?, ?, ? ) ");
      int col = 1;
      ps.setString      (col++,trunc(request.getProfileId(),20));
      ps.setString      (col++,trunc(request.getAccountId(),32));
      ps.setString      (col++,trunc(anh.accountNumTrunc,24));
      ps.setBytes       (col++,anh.accountNumEnc);
      ps.executeUpdate();
      return true;
    }
    catch (Exception e)
    {
      handleException("storeAccountData(" + request + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
    return false;
  }

  public static boolean storeAchAccountData(AchStoreRequest request)
    throws AchValidationException
  {
    return (new AchDb()).storeAccountData(request);
  }

  /**
   * Deletes account data record in database with the account id in the 
   * request.  
   *
   * If no matching account id is found or profile merch id does not match
   * stored account record merch id a validation exception is generated.
   */
  public boolean deleteAccountData(AchDeleteRequest request) 
    throws AchValidationException
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      int rowCount = s.executeUpdate(
        " delete from achp_account_data " +
        " where account_id = '" + request.getAccountId() + "' " +
        "  and merch_num = '" + request.getProfile().getMerchantId() + "' ");
      if (rowCount <= 0)
      {
        throw new AchValidationException(
          TridentApiConstants.ER_ACH_ACCOUNT_ID_INVALID,
          "Invalid account ID");
      }
      return true;
    }
    catch (AchValidationException ve)
    {
      // pass validation exceptions out to the caller
      throw ve;
    }
    catch (Exception e)
    {
      handleException("deleteAccountData(" + request + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return false;
  }

  public static boolean deleteAchAccountData(AchDeleteRequest request)
    throws AchValidationException
  {
    return (new AchDb()).deleteAccountData(request);
  }

  /**
   * Loads account data record.  If found the account data is placed into 
   * the request object.  If not found a validation error is generated.
   * Request merchant id must match in the record or merchant id must
   * match merchants under association node of profile if stored account
   * association feature enabled in profile.
   *
   * Now relies on separate call to fetch node id in request base class.
   */  
  public boolean loadAccountData(AchTranRequest request) 
    throws AchValidationException
  {
    PreparedStatement ps = null;

    try
    {
      String qs = baseStoredAcctQs +
        " and ad.account_id = '" + request.getAccountId() + "'";
      ps = connectPrepared(qs);
      ps.setString(1,""+request.getCardStoreNodeId());
      ResultSet rs = ps.executeQuery();
      if (!rs.next())
      {
        throw new AchValidationException(
          TridentApiConstants.ER_ACH_ACCOUNT_ID_INVALID,
          "Invalid account ID");
      }
      request.setAccountNum(rs.getString("account_num"));
      return true;
    }
    catch (AchValidationException ve)
    {
      // pass validation exceptions out to the caller
      throw ve;
    }
    catch (Exception e)
    {
      handleException("loadAccountData(" + request + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
    return false;
  }

  public static boolean loadAchAccountData(AchTranRequest request)
    throws AchValidationException
  {
    return (new AchDb()).loadAccountData(request);
  }
}
