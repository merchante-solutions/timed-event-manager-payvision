package com.mes.api.ach;

public class AchValidationException extends Exception
{
  private String errorCode;

  public AchValidationException(String errorCode, String message)
  {
    super(message);
    this.errorCode = errorCode;
  }

  public String getErrorCode()
  {
    return errorCode;
  }
}