package com.mes.api.ach;

import java.io.Serializable;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;

public class TransitNumRecord implements Serializable
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID based on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes.
  //
  static final long serialVersionUID    = TridentApiConstants.ApiVersionId;

  static Logger log = Logger.getLogger(TransitNumRecord.class);

  public final static String FLAG_YES = "Y";
  public final static String FLAG_NO  = "N";

  private Integer recId;
  private String  transitNum;
  private String  bankName;
  private String  fraudFlag;

  public TransitNumRecord()
  {
  }
  public TransitNumRecord(Integer recId, String transitNum, 
    String bankName, String fraudFlag)
  {
    this.recId = recId;
    this.transitNum = transitNum;
    this.bankName = bankName;
    this.fraudFlag = fraudFlag;
  }

  public void setRecId(Integer recId)
  {
    this.recId = recId;
  }
  public Integer getRecId()
  {
    return recId;
  }

  public void setTransitNum(String transitNum)
  {
    this.transitNum = transitNum;
  }
  public String getTransitNum()
  {
    return transitNum;
  }

  public void setBankName(String bankName)
  {
    this.bankName = bankName;
  }
  public String getBankName()
  {
    return bankName;
  }

  public void setFraudFlag(String fraudFlag)
  {
    this.fraudFlag = fraudFlag;
  }
  public String getFraudFlag()
  {
    return fraudFlag;
  }

  public String toString()
  {
    return "TransitNumRecord [ "
      + "recId: " + recId
      + ", transitNum: " + transitNum
      + ", bankName: " + bankName
      + ", fraudFlag: " + fraudFlag
      + " ]";
  }
}
