package com.mes.api.ach;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiProfile;
import com.mes.mvc.ParameterEncoder;
import com.mes.support.HttpHelper;

/**
 * Used to submit an ACH sale or credit transaction.
 */
public class AchTranRequest extends AchRequest
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID d on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes. 
  //
  static final long serialVersionUID = TridentApiConstants.ApiVersionId;

  static Logger log = Logger.getLogger(AchTranRequest.class);

  // ip address validation pattern
  public static final String IP_ADDR_PATTERN =
		    "^(0?0?[1-9]|0?[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\." +
		    "(0?0?[1-9]|0?[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\." +
		    "(0?0?[1-9]|0?[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\." +
		    "(0?0?[1-9]|0?[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])$";
  public static Pattern ipPattern = Pattern.compile(IP_ADDR_PATTERN);

  // ach auth types
  public static final String  AUTH_WEB            = "WEB";
  public static final String  AUTH_TEL            = "TEL";
  public static final String  AUTH_PPD            = "PPD";
  public static final String  AUTH_CCD            = "CCD";

  // account types
  public static final String  AT_CHECKING         = "C";
  public static final String  AT_SAVINGS          = "S";

  // max/min
  public static final int     MAX_LEN_ACCOUNT_NUM = 17;
  public static final int     MAX_LEN_CUST_NAME   = 22;
  public static final int     MAX_LEN_CUST_ID     = 15;
  public static final BigDecimal  
                              MAX_AMOUNT          = new BigDecimal("99999999.99");

  // request data

  // ach sale/credit request fields
  public static final String  FN_AUTH_TYPE        = "auth_type";
  public static final String  FN_TRANSIT_NUM      = "transit_num";
  public static final String  FN_ACCOUNT_NUM      = "account_num";
  public static final String  FN_ACCOUNT_NUM_ENC  = "account_num_enc";
  public static final String  FN_ACCOUNT_TYPE     = "account_type";
  public static final String  FN_AMOUNT           = "amount";
  public static final String  FN_CUST_NAME        = "cust_name";
  public static final String  FN_CUST_ID          = "cust_id";
  public static final String  FN_RECUR_FLAG       = "recur_flag";
  public static final String  FN_REF_NUM          = "ref_num";
  public static final String  FN_ACCOUNT_ID       = "account_id";

  protected String            authType;
  protected String            transitNum;
  protected String            accountNum;
  protected String            accountType;
  protected String            amountStr;
  protected BigDecimal        amount;
  protected String            custName;
  protected String            custId;
  protected String            recurFlag;
  protected String            refNum;
  protected String            accountId;

  private AchValidationException loadException;

  // Addition fields used from parent class:
  //  IpAddress

  /** 
   * Constructor.
   */
  public AchTranRequest(String tranId)
  {
    super(tranId);
  }

  /**
   * Accessors
   */

  /**
   * Override parent method to set default auth type.
   */
  public void setProfile(TridentApiProfile Profile)
  {
    super.setProfile(Profile);
    if (Profile != null && authType == null)
    {
      // TODO: add default ach auth type to profile, fetch it here
      authType = AUTH_WEB;
    }
  }

  public String getAuthType()
  {
    if (authType != null)
    {
      return authType;
    }
    if (Profile != null)
    {
      // TODO: add default auth type to profile
      return null;
    }
    return null;
  }

  public void setTransitNum(String transitNum)
  {
    this.transitNum = transitNum;
  }
  public String getTransitNum()
  {
    return transitNum;
  }

  public void setAccountNum(String accountNum)
  {
    this.accountNum = accountNum;
  }
  public String getAccountNum()
  {
    return accountNum;
  }

  public void setAccountType(String accountType)
  {
    this.accountType = accountType;
  }
  public String getAccountType()
  {
    return accountType;
  }

  public String getAmountStr()
  {
    return amountStr;
  }

  public BigDecimal getAmount()
  {
    return amount;
  }

  public void setCustName(String custName)
  { 
    this.custName = custName;
  }
  public String getCustName()
  {
    return custName;
  }

  public void setCustId(String custId)
  { 
    this.custId = custId;
  }
  public String getCustId()
  {
    return custId;
  }

  public String getRecurFlag()
  {
    return recurFlag;
  }

  public String getRefNum()
  {
    return refNum;
  }

  public String getAccountId()
  {
    return accountId;
  }

  public boolean hasAccountId()
  {
    return accountId != null;
  }

  /**
   * Sale/credit validation
   */

  /**
   * Valid auth type string?
   */
  private boolean isValidAuthType()
  {
    return  AUTH_WEB.equals(authType) ||
            AUTH_TEL.equals(authType) ||
            AUTH_PPD.equals(authType) ||
            AUTH_CCD.equals(authType);
  }

  /**
   * Is auth type enabled in profile?
   */
  private boolean isEnabledAuthType()
  {
    if (AUTH_WEB.equals(authType))
    {
      return Profile.isAchpWebEnabled();
    }
    if (AUTH_TEL.equals(authType))
    {
      return Profile.isAchpTelEnabled();
    }
    if (AUTH_PPD.equals(authType))
    {
      return Profile.isAchpPpdEnabled();
    }
    if (AUTH_CCD.equals(authType))
    {
      return Profile.isAchpCcdEnabled();
    }
    return false;
  }

  /**
   * Auth type validation: 
   *  required
   *  must be profile enabled
   *  WEB, TEL, PPD, CCD
   *  WEB credits disallowed
   *
   * References profile and request type (validate these first).
   */
  private void validateAuthType() throws AchValidationException
  {
    if (isBlank(authType))
    {
      throw new AchValidationException(
        tac.ER_ACH_AUTH_TYPE_INVALID,"Missing auth type");
    }
    if (!isValidAuthType())
    {
      throw new AchValidationException(
        tac.ER_ACH_AUTH_TYPE_INVALID,"Invalid auth type");
    }
    if (RT_CREDIT.equals(requestType) && AUTH_WEB.equals(authType))
    {
      throw new AchValidationException(
        tac.ER_ACH_AUTH_TYPE_INVALID,"Auth type WEB, credits not allowed");
    }
    if (!isEnabledAuthType())
    {
      throw new AchValidationException(
        tac.ER_ACH_AUTH_TYPE_DISABLED,"Auth type disabled");
    }
  }

  /**
   * Transit number validation:
   *  required
   *  present in transit num table
   */
  private void validateTransitNum() throws AchValidationException
  {
    if (isBlank(transitNum))
    {
      throw new AchValidationException(
        tac.ER_INVALID_REQUEST,"Missing transit number");
    }
    if (!isValidTransitNum(transitNum))
    {
      throw new AchValidationException(
        tac.ER_ACH_TRANSIT_NUM_INVALID,"Invalid transit number");
    }
  }

  /**
   * Account number validation:
   *  required
   *  non-numeric
   *  max length MAX_LEN_ACCOUNT_NUM (17)
   */
  private void validateAccountNum() throws AchValidationException
  {
    // look for account id, if present load account data from database
    if (accountNum == null && accountId != null)
    {
      AchDb.loadAchAccountData(this);
    }
    if (isBlank(accountNum))
    {
      throw new AchValidationException(
        tac.ER_INVALID_REQUEST,"Missing account number");
    }
    if (accountNum.length() >  MAX_LEN_ACCOUNT_NUM)
    {
      throw new AchValidationException(
        tac.ER_ACH_ACCOUNT_NUM_INVALID,"Account number too long");
    }
    if (!isNumeric(accountNum))
    {
      throw new AchValidationException(
        tac.ER_ACH_ACCOUNT_NUM_INVALID,"Non-numeric account number");
    }
  }

  private boolean isValidAccountType()
  {
    return AT_CHECKING.equals(accountType) || AT_SAVINGS.equals(accountType);
  }

  /**
   * Account type validation:
   *  required
   *  C, S (checking, savings)
   */
  private void validateAccountType() throws AchValidationException
  {
    if (isBlank(accountType))
    {
      throw new AchValidationException(
        tac.ER_INVALID_REQUEST,"Missing account type");
    }
    if (!isValidAccountType())
    {
      throw new AchValidationException(
        tac.ER_ACH_ACCOUNT_TYPE_INVALID,"Invalid account type");
    }
  }

  /**
   * Amount validation:
   *  required
   *  greater than zero
   *  not greater than 99999999.99
   */
  private void validateAmount() throws AchValidationException
  {
    if (isBlank(amountStr))
    {
      throw new AchValidationException(
        tac.ER_INVALID_REQUEST,"Missing amount");
    }
    if (amount.doubleValue() <= 0)
    {
      throw new AchValidationException(
        tac.ER_ACH_AMOUNT_INVALID,"Amount not greater than zero");
    }
    if (amount.compareTo(MAX_AMOUNT) > 0)
    {
      throw new AchValidationException(
        tac.ER_ACH_AMOUNT_INVALID,"Amount too large");
    }
  }

  /**
   * Customer name validation:
   *  required
   *  max length 22
   */
  private void validateCustName() throws AchValidationException
  {
    if (isBlank(custName))
    {
      throw new AchValidationException(
        tac.ER_INVALID_REQUEST,"Missing customer name");
    }
    if (custName.length() > MAX_LEN_CUST_NAME)
    {
      throw new AchValidationException(
        tac.ER_ACH_CUST_NAME_INVALID,"Customer name too long");
    }
  }

  /**
   * Customer ID validation:
   *  required
   *  max length 15
   */
  private void validateCustId() throws AchValidationException
  {
    if (isBlank(custId))
    {
      throw new AchValidationException(
        tac.ER_INVALID_REQUEST,"Customer ID missing");
    }
    if (custId.length() > MAX_LEN_CUST_ID)
    {
      throw new AchValidationException(
        tac.ER_ACH_CUST_ID_INVALID,"Customer ID too long");
    }
  }

  /**
   * Recurring transaction flag validation:
   *  optional
   *  Y, N
   */
  private void validateRecurFlag() throws AchValidationException
  {
    if (!isBlank(recurFlag) && !isValidFlag(recurFlag))
    {
      throw new AchValidationException(
        tac.ER_ACH_RECUR_FLAG_INVALID,"Recurring flag invalid");
    }
  }

  /**
   * Run the ip address through the regular expression checker.
   */
  private boolean isValidIpAddress()
  {
    Matcher matcher = ipPattern.matcher(IpAddress);
    boolean isValid = matcher.matches();
    return isValid;
  }

  /**
   * IP address validation:
   *  required if web authorization
   *  valid ip address
   */
  protected void validateIpAddress() throws AchValidationException
  {
    if (isBlank(IpAddress))
    {
      if (AUTH_WEB.equals(authType))
      {
        throw new AchValidationException(
          tac.ER_ACH_IP_ADDRESS_MISSING,"IP address required on WEB transactions");
      }
      return;
    }
    if (!isValidIpAddress())
    {
      throw new AchValidationException(
        tac.ER_ACH_IP_ADDRESS_INVALID,"IP address invalid");
    }
  }

  /**
   * Sales/credit data validation.
   */
  public void validateRequest() throws Exception
  {
    validateAuthType();
    validateTransitNum();
    validateAccountNum();
    validateAccountType();
    validateCustName();
    validateCustId();
    validateAmount();
    validateRecurFlag();
    validateIpAddress();
  }

  /**
   * Set sale/credit transaction data from http request
   */
  public void setRequestProperties(HttpServletRequest request)
  {
    IpAddress     = HttpHelper.getString(request,tac.FN_IP_ADDRESS,null);
    transitNum    = HttpHelper.getString(request,FN_TRANSIT_NUM,null);
    accountNum    = HttpHelper.getString(request,FN_ACCOUNT_NUM,null);
    accountType   = HttpHelper.getString(request,FN_ACCOUNT_TYPE,null);
    custName      = HttpHelper.getString(request,FN_CUST_NAME,null);
    custId        = HttpHelper.getString(request,FN_CUST_ID,null);
    authType      = HttpHelper.getString(request,FN_AUTH_TYPE,null);
    amountStr     = HttpHelper.getString(request,FN_AMOUNT,null);
    amount        = stringToBigDecimal(amountStr);
    recurFlag     = HttpHelper.getString(request,FN_RECUR_FLAG,FLAG_NO).toUpperCase();
    refNum        = HttpHelper.getString(request,FN_REF_NUM,null);
    tranSource    = HttpHelper.getString(request,FN_TRAN_SOURCE,TSRC_PMG);
    accountId     = HttpHelper.getString(request,FN_ACCOUNT_ID,null);
  }

  public void addResponseParameters(String msgId, ParameterEncoder parms)
  {
    // nothing to add
  }

  public String toString()
  {
    return "AchTranRequest [ "
      + "TranType: " + TranType
      + ", ProfileId: " + ProfileId
      + ", RecId: " + RecId
      + ", ServerName: " + ServerName
      + ", IpAddress: " + IpAddress
      + ", requestDate: " + requestDate
      + ", requestType: " + requestType
      + ", authType: " + authType
      + ", transitNum: " + transitNum
      + ", accountNum: " + accountNum
      + ", accountType: " + accountType
      + ", amount: " + amount
      + ", custName: " + custName
      + ", custId: " + custId
      + ", recurFlag: " + recurFlag
      + ", refNum: " + refNum
      + ", ErrorCode: " + getErrorCode()
      + ", ErrorDesc: " + getErrorDesc()
      + ", testFlag: " + testFlag
      + ", tranSource: " + tranSource
      + ", accountId: " + accountId
      + " ] ";
  }
}