package com.mes.api.ach;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.mvc.ParameterEncoder;
import com.mes.support.HttpHelper;

/**
 * Used to store ACH account data in the ACH account data table.
 */
public class AchDeleteRequest extends AchRequest
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID d on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes. 
  //
  static final long serialVersionUID = TridentApiConstants.ApiVersionId;

  static Logger log = Logger.getLogger(AchDeleteRequest.class);

  // ach account store request fields
  public static final String FN_ACCOUNT_ID        = "account_id";

  private String accountId;

  /** 
   * Constructor.
   */
  public AchDeleteRequest(String tranId)
  {
    super(tranId);
  }

  /**
   * Accessors
   */

  public String getAccountId()
  {
    return accountId;
  }

  /**
   * Account delete validation
   */

  /**
   * Account ID validation:
   *  required
   *
   * NOTE: AchDeleteAccountTask will determine if account exists in db, 
   * returning appropriate error code at that time if necessary.
   */
  private void validateAccountId() throws AchValidationException
  {
    if (isBlank(accountId))
    {
      throw new AchValidationException(
        tac.ER_INVALID_REQUEST,"Account ID missing");
    }
  }

  /**
   * Account store data validation.
   */
  public void validateRequest() throws Exception
  {
    validateAccountId();
  }

  /**
   * Set account store data from http request
   */
  public void setRequestProperties(HttpServletRequest request)
  {
    accountId = HttpHelper.getString(request,FN_ACCOUNT_ID,null);
  }

  /**
   * Return the generated account ID.
   */
  public void addResponseParameters(String msgId, ParameterEncoder parms)
  {
    // nothing to do here
  }

  public String toString()
  {
    return "AchDeleteRequest [ "
      + "TranType: " + TranType
      + ", ProfileId: " + ProfileId
      + ", RecId: " + RecId
      + ", ServerName: " + ServerName
      + ", requestDate: " + requestDate
      + ", requestType: " + requestType
      + ", accountId: " + accountId
      + ", ErrorCode: " + getErrorCode()
      + ", ErrorDesc: " + getErrorDesc()
      + ", testFlag: " + testFlag
      + ", tranSource: " + tranSource
      + " ] ";
  }
}