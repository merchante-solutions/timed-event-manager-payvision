package com.mes.api.ach;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiTransaction;
import com.mes.database.SleepyCatManager;
import com.mes.mvc.ParameterEncoder;
import com.mes.support.HttpHelper;

public abstract class AchRequest extends TridentApiTransaction
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID d on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes. 
  //
  static final long serialVersionUID = TridentApiConstants.ApiVersionId;

  static Logger log = Logger.getLogger(AchRequest.class);

  public static TridentApiConstants tac = new TridentApiConstants();

  // ach request types
  public static final String  RT_SALE             = "SALE";
  public static final String  RT_CREDIT           = "CREDIT";
  public static final String  RT_STORE            = "STORE";
  public static final String  RT_DELETE           = "DELETE";

  // flag values
  public static final String  FLAG_YES            = "Y";
  public static final String  FLAG_NO             = "N";

  // tran sources (must appear in achp_tran_sources)
  public static final String  TSRC_PMG            = "PMG";  // default
  public static final String  TSRC_BO             = "BO";   // back office

  // tpg api request field names (received from merchant)
  // common to all requests
  public static final String  FN_REQUEST_TYPE     = "ach_request";
  public static final String  FN_TEST_FLAG        = "test_flag";
  public static final String  FN_TRAN_SOURCE      = "tran_source";

  // Additional fields/methods used from parent class:
  //  TranType
  //  ProfileId
  //  RecId
  //  ServerName
  //  getErrorCode()
  //  setErrorDesc()

  // ach request fields
  protected String            requestType;
  protected String            testFlag;
  protected String            tranSource;

  protected Date requestDate  = 
                      new Date(Calendar.getInstance().getTime().getTime());

  protected Map responseData  = new HashMap();

  /**
   * Constructor.  Pass along trident tran id.
   */
  public AchRequest(String tranId)
  {
    super(tranId);
  }

  public String getRequestType()
  {
    return requestType;
  }

  public void setRecId(long recId)
  {
    RecId = recId;
  }

  public String getTestFlag()
  {
    return testFlag;
  }
  public void setTestFlag(String testFlag)
  {
    this.testFlag = testFlag;
  }

  public String getTranSource()
  {
    return tranSource;
  }

  public Date getRequestDate()
  {
    return requestDate;
  }

  /**
   * Convert string to BigDecimal amount if possible.
   */
  protected BigDecimal stringToBigDecimal(String str)
  {
    BigDecimal bigDec = null;

    try
    {
      bigDec = new BigDecimal(str).setScale(2);
    }
    catch (Exception e)
    {
      log.warn("Invalid amount string: '" + str + "'");
    }

    return bigDec;
  }

  /**
   * Validation common to some or all requests.
   */

  protected boolean isValidFlag(String flag)
  {
    // case insensitive here...
    return FLAG_YES.equals(flag.toUpperCase()) || 
           FLAG_NO.equals(flag.toUpperCase());
  }

  /**
   * Verify profile ID was provided and a profile was loaded.
   */
  private void validateProfileId() throws AchValidationException
  {
    if (ProfileId == null)
    {
      throw new AchValidationException(
        tac.ER_INVALID_PROFILE,"Missing profile ID parameter");
    }
    if (Profile == null)
    {
      throw new AchValidationException(
        tac.ER_INVALID_PROFILE,"No profile loaded");
    }
  }

  /**
   * Request type validation: required, SALE, CREDIT, STORE, DELETE
   */
  private void validateRequestType() throws AchValidationException
  {
    if (isBlank(requestType))
    {
      throw new AchValidationException(
        tac.ER_INVALID_REQUEST,"Missing request type");
    }
    if (!RT_SALE.equals(requestType) &&
        !RT_CREDIT.equals(requestType) &&
        !RT_STORE.equals(requestType) &&
        !RT_DELETE.equals(requestType))
    {
      throw new AchValidationException(
        tac.ER_ACH_REQUEST_TYPE_INVALID,"Invalid request type");
    }
  }

  /** 
   * True if tran source is recognized: PMG, BO
   */
  private boolean isValidTranSource()
  {
    return TSRC_PMG.equals(tranSource) || TSRC_BO.equals(tranSource);
  }

  /**
   * Transaction source validation:
   *  optional (will be defaulted to PMG)
   *  PMG, BO
   */
  private void validateTranSource() throws AchValidationException
  {
    if (!isBlank(tranSource) && !isValidTranSource())
    {
      throw new AchValidationException(
        tac.ER_ACH_TRAN_SOURCE_INVALID,"Transaction source invalid");
    }
  }

  /**
   * Lookup transit number in transit num table, return true if found.
   */
  protected boolean isValidTransitNum(String transitNum)
  {
    try
    {
      SleepyCatManager mgr = SleepyCatManager.getInstance(
        TridentApiConstants.getApiDatabasePath());
      Map recMap = 
        mgr.getStoredMap(TridentApiConstants.API_TABLE_ACH_TRANSIT_NUMS);
      TransitNumRecord rec = (TransitNumRecord)recMap.get(transitNum);
      return rec != null;
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
    
    return false;
  }

  /**
   * Scans string, returns true if it contains only numeric digits,
   * or if is null or empty.  Used to validate account number, etc.
   */
  protected boolean isNumeric(String str)
  {
    if (str != null)
    {
      for (int i = 0; i < str.length(); ++i)
      {
        if (!Character.isDigit(str.charAt(i)))
        {
          return false;
        }        
      }
      return true;
    }
    return true;
  }

  // child classes implement to validate specific data
  public abstract void validateRequest() throws Exception;

  /**
   * Validate common request data.
   * Calls abstract child class specific validateRequest()
   */
  public final boolean isValid()
  {
    setError(tac.ER_NONE);

    try
    {
      validateProfileId();
      validateRequestType();
      validateTranSource();
      validateRequest();
    }
    catch (AchValidationException ve)
    {
      setError(ve.getErrorCode());
      log.error("Invalid ACH transaction: " + ve);
    }
    catch (Exception e)
    {
      setError(tac.ER_ACH_REQUEST_FAILED);
      log.error("ACH transaction validation error: " + e);
    }

    return !hasError();
  }

  // child classes implement to set specific request properties
  public abstract void setRequestProperties(HttpServletRequest request);

  /**
   * Set transaction data from http request
   */
  public void setProperties(HttpServletRequest request)
  {
    ProfileId     = HttpHelper.getString(request,tac.FN_TID,null);
    TranType      = HttpHelper.getString(request,tac.FN_TRAN_TYPE,TT_INVALID);
    requestType   = HttpHelper.getString(request,FN_REQUEST_TYPE,null);
    tranSource    = HttpHelper.getString(request,FN_TRAN_SOURCE,TSRC_PMG);
    setRequestProperties(request);
  }

  /**
   * DB operations not supported within ACH request objects.
   */
  public void setProperties(ResultSet rs)
  {
    log.error("AchRequest.setProperties(ResultSet rs) not supported");
    //throw new RuntimeException("Invalid operation");
  }

  // child classes implement to add specific 
  // response parameters to responseData hash map
  public abstract void addResponseParameters(String msgId, 
    ParameterEncoder parms);

  /**
   * Encodes the TPG tran id, msg id and msg text along with any name value
   * pairs present in the response data map.  Returns a string suitable
   * for http posting (URLEncoded name value pairs).
   */
  public String encodeResponse(String tranId, String msgId, String msgText)
  {
    try
    {
      ParameterEncoder parms = new ParameterEncoder();

      // standard tpg response fields
      parms.add(tac.FN_TRAN_ID,tranId);
      parms.add(tac.FN_ERROR_CODE,msgId);
      if (msgText != null && msgText.trim().length() > 0)
      {
        parms.add(tac.FN_AUTH_RESP_TEXT,msgText);
      }

      // add child class specific response parameters
      addResponseParameters(msgId,parms);

      // request-specific response data
      for (Iterator i = responseData.entrySet().iterator(); i.hasNext();)
      {
        Map.Entry e = (Map.Entry)i.next();
        String name = (String)e.getKey();
        String value = (String)e.getValue();
        if (value == null || value.length() == 0)
        {
          continue;
        }
        parms.add(name,value);
      }
      return parms.encode();
    }
    catch (Exception e)
    {
      log.error("Error encoding response: " + e);
    }
    return "Error encoding response";
  }

  /**
   * Encodes response, fetches error code, desc.
   */

  public String encodeResponse()
  {
    String tranId = ApiTranId;
    String msgId = getErrorCode();
    String msgText = getErrorDesc();
    if (msgText == null || msgText.equals(""))
    {
      msgText = tac.getErrorDesc(msgId);
    }
    if (msgText == null || msgText.equals("") && "000".equals(msgId))
    {
      msgText = "OK";
    }
    return encodeResponse(ApiTranId,msgId,msgText);
  }
}