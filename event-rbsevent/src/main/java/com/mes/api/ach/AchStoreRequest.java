package com.mes.api.ach;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.api.TridentApiConstants;
import com.mes.mvc.ParameterEncoder;
import com.mes.support.HttpHelper;

/**
 * Used to store ACH account data in the ACH account data table.
 */
public class AchStoreRequest extends AchRequest
{
  //
  // if you do not specify the serial version ID then Java will default
  // to a UID d on the fields in the class (see <jdk>/bin/serialver).  
  // failure to specify a static version will cause a mismatch exception 
  // if the object definition is changed.
  //
  // NOTE: it is necessary to specify a static serialVersionUID for all
  //       child classes. 
  //
  static final long serialVersionUID = TridentApiConstants.ApiVersionId;

  static Logger log = Logger.getLogger(AchStoreRequest.class);

  // account types
  public static final String  AT_CHECKING         = "C";
  public static final String  AT_SAVINGS          = "S";

  // max/min
  public static final int     MAX_LEN_ACCOUNT_NUM = 17;
  public static final int     MAX_LEN_CUST_NAME   = 22;
  public static final int     MAX_LEN_CUST_ID     = 15;

  // request data

  // ach account store request fields
  public static final String  FN_ACCOUNT_NUM      = "account_num";
  public static final String  FN_ACCOUNT_NUM_ENC  = "account_num_enc";

  private String accountNum;

  // ach account store response fields
  public static final String FN_ACCOUNT_ID        = "account_id";

  private String accountId;

  /** 
   * Constructor.
   */
  public AchStoreRequest(String tranId)
  {
    super(tranId);
  }

  /**
   * Accessors
   */

  public String getAccountNum()
  {
    return accountNum;
  }

  public void setAccountId(String accountId)
  {
    this.accountId = accountId;
  }
  public String getAccountId()
  {
    return accountId;
  }

  /**
   * Account store validation
   */

  /**
   * Account number validation:
   *  required
   *  numeric only
   *  max length MAX_LEN_ACCOUNT_NUM (17)
   *  TODO: min length?
   */
  private void validateAccountNum() throws AchValidationException
  {
    if (isBlank(accountNum))
    {
      throw new AchValidationException(
        tac.ER_INVALID_REQUEST,"Missing account number");
    }
    if (accountNum.length() >  MAX_LEN_ACCOUNT_NUM)
    {
      throw new AchValidationException(
        tac.ER_ACH_ACCOUNT_NUM_INVALID,"Account number too long");
    }
    if (!isNumeric(accountNum))
    {
      throw new AchValidationException(
        tac.ER_ACH_ACCOUNT_NUM_INVALID,"Non-numeric account number");
    }
  }

  /**
   * Account store data validation.
   */
  public void validateRequest() throws Exception
  {
    validateAccountNum();
  }

  /**
   * Set account store data from http request
   */
  public void setRequestProperties(HttpServletRequest request)
  {
    accountNum    = HttpHelper.getString(request,FN_ACCOUNT_NUM,null);
  }

  /**
   * Replace the tran id with the account id to mimic the 
   * card store functionality.
   */
  public void addResponseParameters(String msgId, ParameterEncoder parms)
  {
    if (accountId != null)
    {
      parms.replace(tac.FN_TRAN_ID,accountId);
    }
    if (tac.ER_NONE.equals(msgId))
    {
      parms.replace(tac.FN_AUTH_RESP_TEXT,"Account Data Stored");
    }
  }

  public String toString()
  {
    return "AchStoreRequest [ "
      + "TranType: " + TranType
      + ", ProfileId: " + ProfileId
      + ", RecId: " + RecId
      + ", ServerName: " + ServerName
      + ", requestDate: " + requestDate
      + ", requestType: " + requestType
      + ", accountNum: " + accountNum
      + ", ErrorCode: " + getErrorCode()
      + ", ErrorDesc: " + getErrorDesc()
      + ", testFlag: " + testFlag
      + ", tranSource: " + tranSource
      + ", accountId: " + accountId
      + " ] ";
  }
}