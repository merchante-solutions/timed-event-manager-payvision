package com.mes.rbs;

import org.apache.log4j.Logger;
import com.mes.database.DbcType;

public class RbsJobHandler extends RbsBase
{
  static Logger log = Logger.getLogger(RbsJobHandler.class);

  private SysType sys;
  private RbsDb db;

  public RbsJobHandler(SysType sys, DbcType dbc)
  {
    this.sys = sys;
    db = new RbsDb(dbc);
  }

  public void run(RbsJob job)
  {
    try
    {
      job.run(sys,db);

      // TODO: create job entry in database

    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }
}