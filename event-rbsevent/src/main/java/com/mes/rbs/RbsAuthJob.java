package com.mes.rbs;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.ads.AccountType;
import com.mes.ads.AchpAccount;
import com.mes.ads.CcAccount;
import com.mes.api.TridentApiConstants;
import com.mes.api.TridentApiRequest;
import com.mes.api.TridentApiResponse;
import com.mes.api.TridentApiTranBase;
import com.mes.api.ach.AchTranRequest;

public class RbsAuthJob extends RbsJob
{
  static Logger log = Logger.getLogger(RbsAuthJob.class);

  private SysType sys;
  private RbsDb db;

  private void setSysType(SysType sys)
  {
    this.sys = sys;
  }

  private void setDb(RbsDb db)
  {  
    this.db = db;
  }

  private String getPmgUrl()
  {
    return db.getPmgUrl(sys);
  }
  
  private void processCc(AuthQueueItem item)
  {
    try
    {
      // build and send auth request
      TridentApiRequest request = new TridentApiRequest(getPmgUrl());

      // basic required request fields
      CcAccount account = (CcAccount)item.getAccount();
      request.addArg(TridentApiConstants.FN_TID,        item.getProfileId());
      request.addArg(TridentApiConstants.FN_TERM_PASS,  item.getProfileKey());
      if (account.hasCardId())
      {
        request.addArg(TridentApiConstants.FN_CARD_ID,  account.getCardId());
      }
      else
      {
        request.addArg(TridentApiConstants.FN_CARD_NUMBER,account.getAccountNum());
        request.addArg(TridentApiConstants.FN_EXP_DATE, account.getExpDate());
      }
      request.addArg(TridentApiConstants.FN_TRAN_TYPE,  TridentApiTranBase.TT_DEBIT);
      request.addArg(TridentApiConstants.FN_AMOUNT,     ""+item.getAmount());

      // customer id -> invoice number
      request.addArg(TridentApiConstants.FN_INV_NUM,    ""+item.getCustomerId());

      // recurring indicator
      request.addArg(TridentApiConstants.FN_MOTO_IND,   TridentApiConstants.FV_MOTO_RECURRING_MOTO);

      // client ref num
      if (item.getClientRefNum() != null)
      {
        request.addArg(TridentApiConstants.FN_CLIENT_REF_NUM,item.getClientRefNum());
      }

      // dynamic dba fields
      if (item.getMerchName() != null)
      {
        request.addArg(TridentApiConstants.FN_DBA_NAME,item.getMerchName());
        if (item.getMerchPhone() != null)
        {
          request.addArg(TridentApiConstants.FN_PHONE,item.getMerchPhone());
        }
      }

      // avs fields
      if (account.getAvsStreet() != null)
      {
        request.addArg(TridentApiConstants.FN_AVS_STREET,account.getAvsStreet());
      }
      if (account.getAvsZip() != null)
      {
        request.addArg(TridentApiConstants.FN_AVS_ZIP,account.getAvsZip());
      }

      log.debug(""+request);
      TridentApiResponse response = request.post();

      // retrieve response data
      item.setResponseCode    (response.getParameter(TridentApiConstants.FN_ERROR_CODE));
      item.setResponseMessage (response.getParameter(TridentApiConstants.FN_AUTH_RESP_TEXT));
      item.setTridentTranId   (response.getParameter(TridentApiConstants.FN_TRAN_ID));

      // remove the item from the queue 
      // (as well as registering the response)
      db.deleteAuthQueueItem(item);
    }
    catch (Exception e)
    {
      // TODO: need to do proper error handling here...
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }
  
  private void processAchp(AuthQueueItem item)
  {
    try
    {
      // build and send auth request
      TridentApiRequest request = new TridentApiRequest(getPmgUrl());

      // basic required request fields
      AchpAccount account = (AchpAccount)item.getAccount();
      request.addArg(TridentApiConstants.FN_TID,        item.getProfileId());
      request.addArg(TridentApiConstants.FN_TERM_PASS,  item.getProfileKey());
      request.addArg(TridentApiConstants.FN_TRAN_TYPE,  TridentApiTranBase.TT_ACH_REQUEST);
      request.addArg(AchTranRequest.FN_REQUEST_TYPE,    AchTranRequest.RT_SALE);
      request.addArg(AchTranRequest.FN_ACCOUNT_NUM,     account.getAccountNum());
      request.addArg(AchTranRequest.FN_TRANSIT_NUM,     account.getTransitNum());
      request.addArg(AchTranRequest.FN_ACCOUNT_TYPE,    ""+account.getDfiType());
      request.addArg(AchTranRequest.FN_AMOUNT,          ""+item.getAmount());
      request.addArg(AchTranRequest.FN_AUTH_TYPE,       account.getSecCode());
      
      if (AchTranRequest.AUTH_WEB.equals(account.getSecCode()))
      {
        request.addArg(TridentApiConstants.FN_IP_ADDRESS,account.getIpAddress());
      }

      // customer id -> invoice number
      request.addArg(TridentApiConstants.FN_INV_NUM,    ""+item.getCustomerId());

      // recurring indicator (needs to be set for WEB)
      request.addArg(AchTranRequest.FN_RECUR_FLAG,      AchTranRequest.FLAG_YES);

      // client ref num
      if (item.getClientRefNum() != null)
      {
        request.addArg(TridentApiConstants.FN_CLIENT_REF_NUM,item.getClientRefNum());
      }
      
      // customer data
      if (account.getCustomerName() != null)
      {
        request.addArg(AchTranRequest.FN_CUST_NAME,     account.getCustomerName());
      }
      if (account.getCustomerId() != null)
      {
        request.addArg(AchTranRequest.FN_CUST_ID,       account.getCustomerId());
      }

      log.debug(""+request);
      TridentApiResponse response = request.post();

      // retrieve response data
      item.setResponseCode    (response.getParameter(TridentApiConstants.FN_ERROR_CODE));
      item.setResponseMessage (response.getParameter(TridentApiConstants.FN_AUTH_RESP_TEXT));
      item.setTridentTranId   (response.getParameter(TridentApiConstants.FN_TRAN_ID));

      // remove the item from the queue 
      // (as well as registering the response)
      db.deleteAuthQueueItem(item);
    }
    catch (Exception e)
    {
      // TODO: need to do proper error handling here...
      log.error("Error: " + e);
      e.printStackTrace();
    }
  }
  
  private void process(AuthQueueItem item)
  {
    //System.out.println("attempting to process: " + item);
    AccountType at = item.getAccount().getAccountType();
    if (AccountType.CC.equals(at))
    {
      processCc(item);
    }
    else if (AccountType.ACHP.equals(at))
    { 
      processAchp(item);
    }
  }
  
  public void run(SysType sys, RbsDb db)
  {
    setSysType(sys);
    setDb(db);

    // get list of auth queue items
    List items = db.getAuthQueueItems(sys);
    
    // process items until no more available
    while (!items.isEmpty())
    {
      for (Iterator i = items.iterator(); i.hasNext();)
      {
        process((AuthQueueItem)i.next());
      }
      
      // look for more items
      items = db.getAuthQueueItems(sys);
    }
  }
  
  public void testRun(SysType sys, RbsDb db, String profileId, 
    AccountType at)
  {
    setSysType(sys);
    setDb(db);

    // get list of auth queue items
    List items = db.getAuthQueueItems(sys,profileId,at);
    
    // process items until no more available
    while (!items.isEmpty())
    {
      for (Iterator i = items.iterator(); i.hasNext();)
      {
        process((AuthQueueItem)i.next());
      }
      
      // look for more items
      items = db.getAuthQueueItems(sys,profileId,at);
    }
  }
}