package com.mes.rbs;

public abstract class RbsJob extends RbsBase
{
  public abstract void run(SysType sys, RbsDb db);

  public void run(SysType sys)
  {
    run(sys,null);
  }
}