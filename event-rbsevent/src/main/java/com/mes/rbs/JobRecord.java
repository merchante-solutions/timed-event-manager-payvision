package com.mes.rbs;

import java.sql.Timestamp;
import java.util.Date;

public class JobRecord extends RbsBase
{
  private long jobId;
  private Date jobDate;
  private String name;
  private String result;
  private SysType sys;

  public void setJobId(long jobId)
  {
    this.jobId = jobId;
  }
  public long getJobId()
  {
    return jobId;
  }

  public void setJobDate(Date jobDate)
  {
    this.jobDate = jobDate;
  }
  public Date getJobDate()
  {
    return jobDate;
  }
  public void setJobTs(Timestamp jobTs)
  {
    jobDate = toDate(jobTs);
  }
  public Timestamp getJobTs()
  {
    return toTimestamp(jobDate);
  }

  public void setName(String name)
  {
    this.name = name;
  }
  public String getName()
  {
    return name;
  }

  public void setResult(String result)
  {
    this.result = result;
  }
  public String getResult()
  {
    return result;
  }

  public void setSys(SysType sys)
  {
    this.sys = sys;
  }
  public SysType getSys()
  {
    return sys;
  }
  public boolean isTest()
  {
    return SysType.TEST.equals(sys);
  }

  public String toString()
  {
    return "JobRecord ["
      + "jobId: " + jobId
      + ", jobDate: " + jobDate
      + ", name: " + name
      + ", result: " + result
      + ", sys: " + sys
      + " ]";
  }
}