package com.mes.rbs;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.ads.Account;
import com.mes.ads.AccountType;
import com.mes.ads.AdsDb;
import com.mes.database.BaseDb;
import com.mes.database.DbcType;
import com.mes.support.HttpHelper;

public class RbsDb extends BaseDb
{
  static Logger log = Logger.getLogger(RbsDb.class);

  public static final String CFG_AUTH_BATCH_SIZE  = "auth_batch_size";
  public static final String CFG_PMG_URL_PREFIX   = "pmg_url_";

  public RbsDb(DbcType dbc)
  {
    super(dbc.equals(DbcType.DIRECT),"rbs_id_seq","rbs_config");
  }
  public RbsDb()
  {
    this(DbcType.POOL);
  }

  /**
   * Method template.
   */
  public void skeleton(String id)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
    }
    catch (Exception e)
    {
      handleException("skeleton(id=" + id + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Config convenience methods.
   */
  public int getAuthBatchSize()
  {
    return getValueAsInt(CFG_AUTH_BATCH_SIZE);
  }

  public String getPmgUrl(SysType sys)
  {
    // translates to pmg_url_test, pmg_url_prod, pmg_url_dev
    return getValue(CFG_PMG_URL_PREFIX + sys);
  }

  /**
   * For purposes of test flags in db records all system types except
   * production are considered to be test.
   */
  private String getTestFlagForSys(SysType sys)
  {
    return !SysType.PROD.equals(sys) ? "Y" : "N";
  }

  /**
   * Validates a profile ID against trident_profile_api table.
   */
  public boolean profileIdExists(String profileId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select * from trident_profile_api " +
        " where terminal_id = '" + profileId + "'");
      return rs.next();
    }
    catch (Exception e)
    {
      handleException("profileIdExists(profileId=" + profileId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return false;
  }

  /**
   * ADS helper db.
   */
  private AdsDb adsDb;
  
  private AdsDb ads()
  {
    if (adsDb == null)
    {
      adsDb = new AdsDb(getDbc());
    }
    return adsDb;
  }
  
  /**
   * BILLING RECORD
   */

  /**
   * Instantiates billing record and loads result set row into it.
   */
  private BillingRecord createBillingRecord(ResultSet rs) throws Exception
  {
    BillingRecord rec = new BillingRecord();
    rec.setRbsId        (rs.getLong       ("rbs_id"));
    rec.setProfileId    (rs.getString     ("profile_id"));
    rec.setMerchNum     (rs.getString     ("merch_num"));
    rec.setTestFlag     (rs.getString     ("test_flag"));
    rec.setCustomerId   (rs.getString     ("customer_id"));
    rec.setUserName     (rs.getString     ("create_user"));
    rec.setCreateTs     (rs.getTimestamp  ("create_ts"));
    rec.setAmount       (rs.getBigDecimal ("amount").setScale(2));
    rec.setStartTs      (rs.getTimestamp  ("start_ts"));
    rec.setNextTs       (rs.getTimestamp  ("next_ts"));
    rec.setPayTotal     (rs.getInt        ("pay_total"));
    rec.setPayCount     (rs.getInt        ("pay_count"));
    rec.setFrequency    (rs.getString     ("frequency"));
    rec.setTaxAmount    (rs.getBigDecimal ("tax_amount").setScale(2));
    rec.setClientRefNum (rs.getString     ("client_ref_num"));
    rec.setMerchName    (rs.getString     ("merch_name"));
    rec.setMerchPhone   (rs.getString     ("merch_phone"));
    rec.setCurrencyCode (rs.getInt        ("currency_code"));
    rec.setAdsId        (rs.getLong       ("ads_id"));
    rec.setAccountType  (AccountType.forType(
                         rs.getString     ("ads_type")));
    return rec;
  }

  /**
   * Fetches billing record associated with the rbs ID.
   */
  public BillingRecord getBillingRecord(long rbsId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select                  " +
        "  r.rbs_id,              " +
        "  r.profile_id,          " +
        "  r.merch_num,           " +
        "  r.test_flag,           " +
        "  r.customer_id,         " +
        "  r.create_user,         " +
        "  r.create_ts,           " +
        "  r.amount,              " +
        "  r.start_ts,            " +
        "  r.next_ts,             " +
        "  r.pay_total,           " +
        "  r.pay_count,           " +
        "  r.frequency,           " +
        "  r.tax_amount,          " +
        "  r.client_ref_num,      " +
        "  r.merch_name,          " +
        "  r.merch_phone,         " +
        "  r.currency_code,       " +
        "  r.ads_id,              " +
        "  r.ads_type             " +
        " from                    " +
        "  rbs_billing_records r  " +
        " where                   " +
        "  rbs_id = " + rbsId);
      if (rs.next())
      {
        BillingRecord record = createBillingRecord(rs);
        ads().loadContainer(record);
        return record;
      }
    }
    catch (Exception e)
    {
      handleException("getBillingRecord(rbsId=" + rbsId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return null;
  }

  /**
   * Create a recurring billing record.
   *
   * Merchant number column will be populated by trigger via profile lookup.
   *
   * Returns newly created record (including any fields auto populated 
   * during insert.
   */
  public BillingRecord insertBillingRecord(BillingRecord rec)
  {
    CallableStatement cs = null;

    try
    {
      // store account data in ads system
      Account acct = ads().insertAccount(rec.getAccount());
      rec.setAccount(acct);
      
      cs = connectCallable(
        " begin                             " +
        " insert into rbs_billing_records   " +
        " ( profile_id,                     " +
        "   test_flag,                      " +
        "   create_user,                    " +
        "   customer_id,                    " +
        "   amount,                         " +
        "   start_ts,                       " +
        "   pay_total,                      " +
        "   frequency,                      " +
        "   tax_amount,                     " +
        "   client_ref_num,                 " +
        "   merch_name,                     " +
        "   merch_phone,                    " +
        "   currency_code,                  " +
        "   ads_id,                         " +
        "   ads_type )                      " +
        " values                            " +
        " ( ?, ?, ?, ?, ?, ?, ?, ?,         " +
        "   ?, ?, ?, ?, ?, ?, ? )           " +
        " returning rbs_id into ?; end;     ");

      int colNum = 1;
      cs.setString    (colNum++,rec.getProfileId());
      cs.setString    (colNum++,rec.getTestFlag());
      cs.setString    (colNum++,rec.getUserName());
      cs.setString    (colNum++,rec.getCustomerId());
      cs.setBigDecimal(colNum++,rec.getAmount());
      cs.setTimestamp (colNum++,rec.getStartTs());
      cs.setInt       (colNum++,rec.getPayTotal());
      cs.setString    (colNum++,rec.getFrequency());
      cs.setBigDecimal(colNum++,rec.getTaxAmount());
      cs.setString    (colNum++,rec.getClientRefNum());
      cs.setString    (colNum++,rec.getMerchName());
      cs.setString    (colNum++,rec.getMerchPhone());
      cs.setInt       (colNum++,rec.getCurrencyCode());
      cs.setLong      (colNum++,rec.getAdsId());
      cs.setString    (colNum++,""+rec.getAccountType());
      cs.registerOutParameter(colNum,java.sql.Types.INTEGER);
      cs.executeUpdate();
      long rbsId = cs.getLong(colNum);
      return getBillingRecord(rbsId);
    }
    catch (Exception e)
    {
      handleException("insertBillingRecord(" + rec + ")",e);
    }
    finally
    {
      cleanUp(cs);
    }
    return null;
  }

  /**
   * Fetches billing record associated with profile ID + customer ID.
   */
  public BillingRecord getBillingRecord(String profileId, String customerId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select                  " +
        "  r.rbs_id,              " +
        "  r.profile_id,          " +
        "  r.merch_num,           " +
        "  r.test_flag,           " +
        "  r.customer_id,         " +
        "  r.create_user,         " +
        "  r.create_ts,           " +
        "  r.amount,              " +
        "  r.start_ts,            " +
        "  r.next_ts,             " +
        "  r.pay_total,           " +
        "  r.pay_count,           " +
        "  r.frequency,           " +
        "  r.tax_amount,          " +
        "  r.client_ref_num,      " +
        "  r.merch_name,          " +
        "  r.merch_phone,         " +
        "  r.currency_code,       " +
        "  r.ads_id,              " +
        "  r.ads_type             " +
        " from                    " +
        "  rbs_billing_records r  " +
        " where                   " +
        "  profile_id = '" + profileId + "'  " +
        "  and customer_id = '" + customerId + "' ");
      if (rs.next())
      {
        BillingRecord record = createBillingRecord(rs);
        ads().loadContainer(record);
        return record;
      }
    }
    catch (Exception e)
    {
      handleException("getBillingRecord(profileId=" + profileId 
        + ", customerId=" + customerId+ ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return null;
  }

  /**
   * Updates billing record.  Returns reloaded record.
   */
  public BillingRecord updateBillingRecord(BillingRecord rec)
  {
    PreparedStatement ps = null;

    try
    {
      // update account data in ads system
      ads().updateAccount(rec.getAccount());
      
      connect();
      ps = connectPrepared(
        " update rbs_billing_records set  " +
        "   amount = ?,                   " +
        "   tax_amount = ?,               " +
        "   next_ts = ?,                  " +
        "   pay_total = ?,                " +
        "   pay_count = ?,                " +
        "   frequency = ?,                " +
        "   currency_code = ?,            " +
        "   client_ref_num = ?,           " +
        "   merch_name = ?,               " +
        "   merch_phone = ?               " +
        " where rbs_id = ?                ");

      int colNum = 1;
      ps.setBigDecimal(colNum++,rec.getAmount());
      ps.setBigDecimal(colNum++,rec.getTaxAmount());
      ps.setTimestamp (colNum++,rec.getNextTs());
      ps.setInt       (colNum++,rec.getPayTotal());
      ps.setInt       (colNum++,rec.getPayCount());
      ps.setString    (colNum++,rec.getFrequency());
      ps.setInt       (colNum++,rec.getCurrencyCode());
      ps.setString    (colNum++,rec.getClientRefNum());
      ps.setString    (colNum++,rec.getMerchName());
      ps.setString    (colNum++,rec.getMerchPhone());
      ps.setLong      (colNum++,rec.getRbsId());
      ps.executeUpdate();
      return getBillingRecord(rec.getRbsId());
    }
    catch (Exception e)
    {
      handleException("updateBillingRecord(" + rec + ")",e);
    }
    finally
    {
      cleanUp(ps);
    }
    
    return null;
  }

  /**
   * Deletes a billing record permanently.
   */
  public void deleteBillingRecord(String profileId, String customerId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      s.executeUpdate(
        " delete from rbs_billing_records " +
        " where profile_id = '" + profileId + "' " +
        "  and customer_id = '" + customerId + "'");
    }
    catch (Exception e)
    {
      handleException("deleteBillingRecord(profileId=" + profileId 
        + ", customerId=" + customerId+ ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }
  
  public void purgeBillingRecords(SysType sys, String profileId)
  {
    Statement s = null;
  
    try
    {
      // determine test flag based on server context
      String testFlag = getTestFlagForSys(sys);
      s = connectStatement();
      s.executeUpdate(
        " delete from rbs_billing_records " +
        " where profile_id = '" + profileId + "' " +
        "  and test_flag = '" + testFlag + "' ");
    }
    catch (Exception e)
    {
      handleException("purgeBillingRecords(profileId=" + profileId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }
  public void purgeBillingRecords(String profileId)
  {
    SysType sys   
      = HttpHelper.isProdServer(null) ? SysType.PROD : SysType.TEST;
    purgeBillingRecords(sys,profileId);
  }

  public List getBillingRecords(String profileId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select r.* from rbs_billing_records r   " +
        " where profile_id = '" + profileId + "'  ");
      List records = new ArrayList();
      while (rs.next())
      {
        records.add(createBillingRecord(rs));
      }
      if (!records.isEmpty())
      {
        ads().loadContainers(records);
      }
      return records;
    }
    catch (Exception e)
    {
      handleException("getBillingRecords(profileId=" + profileId
        + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return null;
  }
  
  /**
   * Determines if the profile ID and customer ID exist together in 
   * rbs_billing_records.
   */
  public boolean customerIdExists(String profileId, String customerId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select * from rbs_billing_records       " +
        " where profile_id = '" + profileId + "'  " +
        "  and customer_id = '" + customerId + "' ");
      return rs.next();
    }
    catch (Exception e)
    {
      handleException("customerIdExists(profileId=" + profileId 
        + ",customerId=" + customerId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return false;
  }

  /**
   * USER PROFILE LINK
   */

  private UserProfileLink createUserProfileLink(ResultSet rs) 
    throws Exception
  {
    UserProfileLink link = new UserProfileLink();
    link.setUplId     (rs.getLong     ("upl_id"));
    link.setProfileId (rs.getString   ("profile_id"));
    link.setMerchNum  (rs.getString   ("merch_num"));
    link.setMerchName (rs.getString   ("merch_name"));
    link.setLoginName (rs.getString   ("login_name"));
    link.setUserName  (rs.getString   ("user_name"));
    link.setCreateTs  (rs.getTimestamp("create_ts"));
    link.setEnableFlag(rs.getString   ("enable_flag"));
    return link;
  }

  /**
   * Returns link with profile ID and login name.
   */
  public UserProfileLink getUserProfileLink(String loginName, String profileId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select                                   " +
        "  up.upl_id,                              " +
        "  up.profile_id,                          " +
        "  to_char(tp.merchant_number) merch_num,  " +
        "  to_char(tp.merchant_name) merch_name,   " +
        "  up.login_name,                          " +
        "  users.name user_name,                   " +
        "  up.create_ts,                           " +
        "  up.enable_flag                          " +
        " from                                     " +
        "  rbs_user_profiles up,                   " +
        "  trident_profile tp,                     " +
        "  users                                   " +
        " where                                    " +
        "  up.profile_id = tp.terminal_id(+)       " +
        "  and up.login_name = users.login_name(+) " +
        "  and up.login_name = '" + loginName + "' " +
        "  and up.profile_id = '" + profileId + "' ");
      if (rs.next())
      {
        return createUserProfileLink(rs);
      }
    }
    catch (Exception e)
    {
      handleException("getUserProfileLink(loginName=" + loginName + ", pid=" 
        + profileId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }

    return null;
  }

  /**
   * Returns link with given id.
   */
  public UserProfileLink getUserProfileLink(long uplId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select                                   " +
        "  up.upl_id,                              " +
        "  up.profile_id,                          " +
        "  to_char(tp.merchant_number) merch_num,  " +
        "  to_char(tp.merchant_name) merch_name,   " +
        "  up.login_name,                          " +
        "  users.name user_name,                   " +
        "  up.create_ts,                           " +
        "  up.enable_flag                          " +
        " from                                     " +
        "  rbs_user_profiles up,                   " +
        "  trident_profile tp,                     " +
        "  users                                   " +
        " where                                    " +
        "  up.profile_id = tp.terminal_id(+)       " +
        "  and up.login_name = users.login_name(+) " +
        "  and up.upl_id = " + uplId + "           ");
      if (rs.next())
      {
        return createUserProfileLink(rs);
      }
    }
    catch (Exception e)
    {
      handleException("getUserProfileLink(uplId=" + uplId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }

    return null;
  }

  /**
   * Returns list of links associated with given profile ID.
   */
  public List getUserProfileLinks(String profileId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select                                   " +
        "  up.upl_id,                              " +
        "  up.profile_id,                          " +
        "  to_char(tp.merchant_number) merch_num,  " +
        "  to_char(tp.merchant_name) merch_name,   " +
        "  up.login_name,                          " +
        "  users.name user_name,                   " +
        "  up.create_ts,                           " +
        "  up.enable_flag                          " +
        " from                                     " +
        "  rbs_user_profiles up,                   " +
        "  trident_profile tp,                     " +
        "  users                                   " +
        " where                                    " +
        "  up.profile_id = tp.terminal_id(+)       " +
        "  and up.login_name = users.login_name(+) " +
        "  and up.profile_id = '" + profileId + "' ");
      List links = new ArrayList();
      while (rs.next())
      {
        links.add(createUserProfileLink(rs));
      }
      return links;
    }
    catch (Exception e)
    {
      handleException("getUserProfileLinks(profileId='"
        + profileId + "')",e);
    }
    finally
    {
      cleanUp(s);
    }

    return null;
  }

  /**
   * Returns true if user profile link with the login name and profile ID 
   * is enabled.
   */
  public boolean profileLinkEnabled(String loginName, String profileId)
  {
    UserProfileLink link = getUserProfileLink(loginName,profileId);
    return link != null && link.isEnabled();
  }
  
  /**
   * RBS PROFILE
   */
   
  private RbsProfile createRbsProfile(ResultSet rs) 
    throws Exception
  {
    RbsProfile profile = new RbsProfile();
    profile.setRbspId    (rs.getLong     ("rbsp_id"));
    profile.setProfileId (rs.getString   ("profile_id"));
    profile.setMerchNum  (rs.getString   ("merch_num"));
    profile.setMerchName (rs.getString   ("merch_name"));
    profile.setCreateTs  (rs.getTimestamp("create_ts"));
    profile.setEnableFlag(rs.getString   ("enable_flag"));
    return profile;
  }

  /**
   * Returns link with profile ID and login name.
   */
  public RbsProfile getRbsProfile(String profileId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select                                  " +
        "  p.rbsp_id,                             " +
        "  p.profile_id,                          " +
        "  to_char(tp.merchant_number) merch_num, " +
        "  to_char(tp.merchant_name) merch_name,  " +
        "  p.create_ts,                           " +
        "  p.enable_flag                          " +
        " from                                    " +
        "  rbs_profiles p,                        " +
        "  trident_profile tp                     " +
        " where                                   " +
        "  p.profile_id = tp.terminal_id(+)       " +
        "  and p.profile_id = '" + profileId + "' ");
      if (rs.next())
      {
        return createRbsProfile(rs);
      }
    }
    catch (Exception e)
    {
      handleException("getRbsProfile(profileId=" + profileId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }

    return null;
  }
  
  /**
   * Inserts rbs profile record.  Only requrired column is profile ID.
   * Returns profile object loaded from database (including auto filled data).
   */
  public RbsProfile insertRbsProfile(RbsProfile profile)
  {
    Statement s = null;
    
    try
    {
      s = connectStatement();
      s.executeUpdate(
        " insert into rbs_profiles ( profile_id, enable_flag ) " +
        " values ( '" + profile.getProfileId() + "', " +
        "  '" + (profile.isEnabled() ? "Y" : "N") + "' ) ");
      return getRbsProfile(profile.getProfileId());
    }
    catch (Exception e)
    {
      handleException("insertRbsProfile(profile=" + profile + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    
    return null;
  }
  
  /**
   * Returns true if rbs profile record exists.
   */
  public boolean profileExists(String profileId)
  {
    return getRbsProfile(profileId) != null;
  }

  /**
   * Returns true if profile record with profile ID is enabled.
   */
  public boolean profileEnabled(String profileId)
  {
    RbsProfile profile = getRbsProfile(profileId);
    return profile != null && profile.isEnabled();
  }
  
  /**
   * Toggles profile enable flag.
   */
  public void toggleProfileEnable(String profileId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      s.executeUpdate(
        " update rbs_profiles set enable_flag =   " +
        "  decode(enable_flag,'Y','N','Y')        " +
        " where profile_id = '" + profileId + "'  ");
    }
    catch (Exception e)
    {
      handleException("toggleProfileEnable(profileId=" + profileId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Validates user login name.
   */
  public boolean loginNameExists(String loginName)
  {
    Statement s = null;
    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select login_name from users " +
        " where login_name = '" + loginName + "' ");
      return rs.next();
    }
    catch (Exception e)
    {
      handleException("loginNameExists(loginName=" + loginName + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return false;
  }

  /**
   * Validates profile id.
   */
  public boolean tridentProfileExists(String profileId)
  {
    Statement s = null;
    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select terminal_id from trident_profile " +
        " where terminal_id = '" + profileId + "' ");
      return rs.next();
    }
    catch (Exception e)
    {
      handleException("tridentProfileExists(profileId=" + profileId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return false;
  }

  /**
   * Inserts new user profile link.  Returns reloaded link from database
   * with user name, merch name and create date.
   */
  public UserProfileLink insertUserProfileLink(UserProfileLink link)
  {
    CallableStatement cs = null;

    try
    {
      cs = connectCallable(
        " begin                   " +
        "  insert into            " +
        "   rbs_user_profiles     " +
        "  ( upl_id,              " +
        "    profile_id,          " +
        "    login_name,           " +
        "    create_ts,           " +
        "    enable_flag )        " +
        "  values                 " +
        "  ( ?, ?, ?, ?, ? )   " +
        "  returning upl_id       " +
        "   into ?;               " +
        " end;                    ");

      int colNum = 1;
      cs.setLong      (colNum++,link.getUplId());
      cs.setString    (colNum++,link.getProfileId());
      cs.setString    (colNum++,link.getLoginName());
      cs.setTimestamp (colNum++,link.getCreateTs());
      cs.setString    (colNum++,link.getEnableFlag());
      cs.registerOutParameter(colNum,java.sql.Types.INTEGER);
      cs.executeUpdate();
      long uplId = cs.getLong(colNum);
      log.debug("uplId=" + uplId);
      return getUserProfileLink(uplId);
    }
    catch (Exception e)
    {
      handleException("insertUserProfileLink(" + link + ")",e);
    }
    finally
    {
      cleanUp(cs);
    }

    return null;
  }

  public void deleteUserProfileLink(long uplId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      s.executeUpdate(
        " delete from rbs_user_profiles where upl_id = " + uplId);
    }
    catch (Exception e)
    {
      handleException("deleteUserProfileLink(uplId=" + uplId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * JOB RECORD
   */

  /**
   * Intantiate job.
   */

  private JobRecord createJob(ResultSet rs) throws Exception
  {
    JobRecord job = new JobRecord();
    job.setJobId  (rs.getLong     ("job_id"));
    job.setJobTs  (rs.getTimestamp("job_ts"));
    job.setName   (rs.getString   ("name"));
    job.setResult (rs.getString   ("result"));
    return job;
  }

  /**
   * Fetch job with id.
   */
  public JobRecord getJob(long jobId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select * from rbs_jobs where job_id = " + jobId);

      if (rs.next())
      {
        return createJob(rs);
      }
    }
    catch (Exception e)
    {
      handleException("getJob(jobId=" + jobId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }

    return null;
  }

  /**
   * Insert job.
   *
   * Returns newly created job from db (gets assigned id and timestamp).
   */
  public JobRecord insertJob(JobRecord job)
  {
    CallableStatement cs = null;

    try
    {
      cs = connectCallable(
        " begin                             " +
        " insert into rbs_jobs              " +
        " ( name, result ) values ( ?, ? )  " +
        " returning job_id into ?; end;     ");

      int colNum = 1;
      cs.setString    (colNum++,job.getName());
      cs.setString    (colNum++,job.getResult());
      cs.registerOutParameter(colNum,java.sql.Types.INTEGER);
      cs.executeUpdate();
      long jobId = cs.getLong(colNum);
      log.debug("jobId=" + jobId);
      return getJob(jobId);
    }
    catch (Exception e)
    {
      handleException("insertJob(" + job + ")",e);
    }
    finally
    {
      cleanUp(cs);
    }
    return null;
  }

  /**
   * AUTH QUEUE ITEM (Credit card transactions)
   */

  /**
   * Create auth queue item from result set row.
   */
  private AuthQueueItem createAuthQueueItem(ResultSet rs) throws Exception
  {
    AuthQueueItem item = new AuthQueueItem();
    item.setAuthId          (rs.getLong       ("auth_id"));
    item.setAuthTs          (rs.getTimestamp  ("auth_ts"));
    item.setRbsId           (rs.getLong       ("rbs_id"));
    item.setProfileId       (rs.getString     ("profile_id"));
    item.setProfileKey      (rs.getString     ("profile_key"));
    item.setMerchNum        (rs.getString     ("merch_num"));
    item.setTestFlag        (rs.getString     ("test_flag"));
    item.setCustomerId      (rs.getString     ("customer_id"));
    item.setAmount          (rs.getBigDecimal ("amount"));
    item.setTaxAmount       (rs.getBigDecimal ("tax_amount"));
    item.setCurrencyCode    (rs.getInt        ("currency_code"));
    item.setClientRefNum    (rs.getString     ("client_ref_num"));
    item.setMerchName       (rs.getString     ("merch_name"));
    item.setMerchPhone      (rs.getString     ("merch_phone"));
    item.setAdsId           (rs.getLong       ("ads_id"));
    item.setAccountType     (AccountType.forType(
                             rs.getString     ("ads_type")));
    item.setStateId         (rs.getLong       ("state_id"));
    item.setResponseCode    (rs.getString     ("response_code"));
    item.setResponseMessage (rs.getString     ("response_message"));
    item.setTridentTranId   (rs.getString     ("trident_tran_id"));
    return item;
  }

  /**
   * Fetches queue auth item associated with the auth id.
   */
  public AuthQueueItem getAuthQueueItem(long authId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select * from rbs_auth_queue " +
        " where auth_id = " + authId);
      if (rs.next())
      {
        AuthQueueItem item = createAuthQueueItem(rs);
        ads().loadContainer(item);
        return item;
      }
    }
    catch (Exception e)
    {
      handleException("getAuthQueueItem(authId=" + authId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return null;
  }

  /**
   * Insert an auth queue item.
   *
   * Returns newly created item (including any fields auto populated 
   * during insert.
   */
  public AuthQueueItem insertAuthQueueItem(AuthQueueItem item)
  {
    CallableStatement cs = null;

    try
    {
      cs = connectCallable(
        " begin                         " +
        " insert into                   " +
        "  rbs_auth_queue               " +
        " ( rbs_id,                     " +
        "   batch_id,                   " +
        "   profile_id,                 " +
        "   profile_key,                " +
        "   test_flag,                  " +
        "   customer_id,                " +
        "   amount,                     " +
        "   tax_amount,                 " +
        "   currency_code,              " +
        "   client_ref_num,             " +
        "   merch_name,                 " +
        "   merch_phone,                " +
        "   ads_id,                     " +
        "   ads_type,                   " +
        "   state_id )                  " +
        " values                        " +
        " ( ?, ?, ?, ?, ?, ?, ?, ?,     " +
        "   ?, ?, ?, ?, ?, ?, ?, )      " +
        " returning rbs_id into ?;      " +
        " end;                          ");

      int colNum = 1;
      cs.setLong      (colNum++,item.getRbsId());
      cs.setLong      (colNum++,item.getBatchId());
      cs.setString    (colNum++,item.getProfileId());
      cs.setString    (colNum++,item.getProfileKey());
      cs.setString    (colNum++,item.getTestFlag());
      cs.setString    (colNum++,item.getCustomerId());
      cs.setBigDecimal(colNum++,item.getAmount());
      cs.setBigDecimal(colNum++,item.getTaxAmount());
      cs.setInt       (colNum++,item.getCurrencyCode());
      cs.setString    (colNum++,item.getClientRefNum());
      cs.setString    (colNum++,item.getMerchName());
      cs.setString    (colNum++,item.getMerchPhone());
      cs.setLong      (colNum++,item.getAdsId());
      cs.setString    (colNum++,""+item.getAccountType());
      cs.setLong      (colNum++,item.getStateId());
      cs.registerOutParameter(colNum,java.sql.Types.INTEGER);
      cs.executeUpdate();
      long authId = cs.getLong(colNum);
      log.debug("authId=" + authId);
      return getAuthQueueItem(authId);
    }
    catch (Exception e)
    {
      handleException("insertAuthQueueItem(" + item + ")",e);
    }
    finally
    {
      cleanUp(cs);
    }
    return null;
  }
  
  /**
   * TRANSACTION QUEUE HANDLING
   *
   * Handles generation of batches of credit card and ACHP transactions
   * from due billing records.
   */
   
  /**
   * Insert all ready billing records into the authorization queue.
   */
  public void loadAuthQueue(SysType sys, String profileId)
  {
    boolean opSuccess = false;
    Statement s = null;

    try
    {
      startTransaction();

      s = connectStatement();

      // use a batch id to tag all records involved in the transaction
      long batchId = getNewId();

      // generate batch of queue items for eligable billing records
      String testFlag = getTestFlagForSys(sys);
      s.executeUpdate(
        " insert into                 " +
        "  rbs_auth_queue             " +
        " ( rbs_id,                   " +
        "   batch_id,                 " +
        "   profile_id,               " +
        "   profile_key,              " +
        "   merch_num,                " +
        "   customer_id,              " +
        "   amount,                   " +
        "   tax_amount,               " +
        "   currency_code,            " +
        "   client_ref_num,           " +
        "   merch_name,               " +
        "   merch_phone,              " +
        "   ads_id,                   " +
        "   ads_type,                 " +
        "   test_flag )               " +
        " ( select                    " +
        "    r.rbs_id,                " +
        "    " + batchId + ",         " +
        "    r.profile_id,            " +
        "    t.merchant_key,          " +
        "    r.merch_num,             " +
        "    r.customer_id,           " +
        "    r.amount,                " +
        "    r.tax_amount,            " +
        "    r.currency_code,         " +
        "    r.client_ref_num,        " +
        "    r.merch_name,            " +
        "    r.merch_phone,           " +
        "    r.ads_id,                " +
        "    r.ads_type,              " +
        "    r.test_flag              " +
        "   from                      " +
        "    rbs_billing_records r,   " +
        "    trident_profile t,       " +
        "    rbs_profiles p           " +
        "   where                     " +
        "    r.status = 'ACTIVE'      " +
        (profileId != null ? " and r.profile_id = '" + profileId + "' " : "") +
        "    and r.profile_id = t.terminal_id             " +
        "    and r.profile_id = p.profile_id (+)          " +
        "    and nvl(p.enable_flag,'Y') = 'Y'             " +
        "    and r.test_flag = '" + testFlag + "'         " +
        "    and trunc( sysdate ) >= trunc ( r.next_ts )  " +
        "    and ( r.last_ts is null or                   " +
        "     trunc( r.last_ts ) < trunc ( sysdate ) )    " +
        "    and ( r.pay_total = 0 or                     " +
        "     nvl(r.pay_count,0) < pay_total ) )          ");

      // update last ts and payment counts for the batch
      s.executeUpdate(
        " update rbs_billing_records  " +
        " set last_ts = sysdate,      " +
        "  pay_count = nvl(pay_count,0) + 1 " +
        " where rbs_id in (           " +
        "  select rbs_id              " +
        "  from rbs_auth_queue        " +
        "  where batch_id = " + batchId + " ) ");
        
      // update daily rec next dates
      int count = -1;
      while (count != 0)
      {
        count = s.executeUpdate(
          " update rbs_billing_records  " +
          " set next_ts = next_ts + 1   " +
          " where frequency = 'DAILY'   " +
          "  and rbs_id in (            " +
          "   select rbs_id             " +
          "   from rbs_auth_queue       " +
          "   where batch_id = " + batchId + "  " +
          "    and next_ts < sysdate )          ");
      }
      
      // update monthly rec next dates
      count = -1;
      while (count != 0)
      {
        count = s.executeUpdate(
          " update rbs_billing_records                          " +
          " set next_ts =                                       " +
          "  case when extract(day from next_ts) > 28           " +
          "   then last_day(add_months(next_ts,                 " +
          "    decode(frequency,'MONTHLY',1,'QUARTERLY',3,12))) " +
          "   else add_months(next_ts,                          " +
          "    decode(frequency,'MONTHLY',1,'QUARTERLY',3,12))  " +
          "  end                                      " +
          " where frequency in                        " +
          "   ( 'MONTHLY', 'QUARTERLY', 'ANNUALLY' )  " +
          "  and rbs_id in (                          " +
          "   select rbs_id                           " +
          "   from rbs_auth_queue                     " +
          "   where batch_id = " + batchId + "        " +
          "    and next_ts < sysdate )                ");
      }
      
      opSuccess= true;
    }
    catch (Exception e)
    {
      handleException("insertAuthQueueBatch()",e);
    }
    finally
    {
      cleanUp(s);
      if (opSuccess) { commitTransaction(); } else { rollbackTransaction(); }
    }
  }
  public void loadAuthQueue(SysType sys)
  {
    loadAuthQueue(sys,null);
  }
  
  /**
   * Fetch a batch of auths records to authorize.  Account type may be null
   * but if not account type is used to filter queue items.  Batch size is
   * used to limit how many at a time to process.  Sys specifies test or
   * production items.
   */
  public List getAuthQueueItems(SysType sys, int batchSize, AccountType at)
  {
    Statement s = null;
    List items = null;

    try
    {
      String testFlag = getTestFlagForSys(sys);

      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select * from rbs_auth_queue " +
        " where test_flag = '" + testFlag + "' " +
        "  and rownum <= " + batchSize +
        (at != null ? " and ads_type = '" + at + "'" : ""));
      items = new ArrayList();
      while (rs.next())
      {
        items.add(createAuthQueueItem(rs));
      }
      if (!items.isEmpty())
      {
        ads().loadContainers(items);
      }
    }
    catch (Exception e)
    {
      handleException("getAuthQueueItems(sys=" + sys + ", batchSize=" 
        + batchSize + ", at=" + at + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return items;
  }
  public List getAuthQueueItems(SysType sys, int batchSize)
  {
    return getAuthQueueItems(sys,batchSize,null);
  }
  public List getAuthQueueItems(SysType sys, AccountType at)
  {
    return getAuthQueueItems(sys,getAuthBatchSize(),at);
  }
  public List getAuthQueueItems(SysType sys)
  {
    return getAuthQueueItems(sys,getAuthBatchSize());
  }
  
  /**
   * Used for testing
   */
  public List getAuthQueueItems(SysType sys, String profileId, AccountType at)
  {
    Statement s = null;
    List items = null;

    try
    {
      String testFlag = getTestFlagForSys(sys);

      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select * from rbs_auth_queue " +
        " where test_flag = '" + testFlag + "' " +
        "  and profile_id = '" + profileId + "' " +
        (at != null ? " and ads_type = '" + at + "'" : ""));
      items = new ArrayList();
      while (rs.next())
      {
        items.add(createAuthQueueItem(rs));
      }
      if (!items.isEmpty())
      {
        ads().loadContainers(items);
      }
    }
    catch (Exception e)
    {
      handleException("getAuthQueueItems(sys=" + sys + ", profileId=" 
        + profileId + ", at=" + at + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return items;
  }
  public List getAuthQueueItems(SysType sys, String profileId)
  {
    return getAuthQueueItems(sys,profileId,null);
  }

  /**
   * This updates the auth queue item with response data in order
   * to log it and then deletes the queue item.
   */
  public void deleteAuthQueueItem(AuthQueueItem item)
  {
    Statement s = null;
    boolean opSuccess = false;

    try
    {
      startTransaction();
      s = connectStatement();
      Account a = item.getAccount();
      s.executeUpdate(
        " update rbs_auth_queue set " +
        (a != null ? " state_id = " + a.getStateId() + ", " : "") +
        "  response_code = '" + item.getResponseCode() + "', " +
        "  response_message = '" + item.getResponseMessage() + "', " +
        "  trident_tran_id = '" + item.getTridentTranId() + "' " +
        " where auth_id = " + item.getAuthId());
      s.executeUpdate(
        " delete from rbs_auth_queue " +
        " where auth_id = " + item.getAuthId());
      opSuccess = true;
    }
    catch (Exception e)
    {
      handleException("deleteAuthQueueItem(" + item + ")",e);
    }
    finally
    {
      cleanUp(s);
      if (opSuccess) { commitTransaction(); } else { rollbackTransaction(); }
    }
  }
  
  public void purgeAuthQueue(SysType sys, String profileId)
  {
    Statement s = null;
    boolean opSuccess = false;

    try
    {
      String testFlag = getTestFlagForSys(sys);
      s = connectStatement();
      s.executeUpdate(
        " delete from rbs_auth_queue " +
        " where test_flag = '" + testFlag + "' " +
        "  and profile_id = '" + profileId + "'");
    }
    catch (Exception e)
    {
      handleException("purgAuthQueue(sys=" + sys + ", profileId=" 
        + profileId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * PROFILE REFS
   */

  /**
   * ProfileRef
   */

  private ProfileRef createProfileRef(ResultSet rs) throws Exception
  {
    ProfileRef ref = new ProfileRef();
    ref.setProfileId(rs.getString ("profile_id"));
    ref.setMerchNum (rs.getString ("merch_num"));
    ref.setMerchName(rs.getString ("merch_name"));
    ref.setRecCount (rs.getInt    ("rec_count"));
    ref.setUserCount(rs.getInt    ("user_count"));
    ref.setEnableFlag(rs.getString("enable_flag"));
    return ref;
  }

  /**
   * Finds any profile ref objects matching a search term.  If more than
   * 500 matches are found null is returned. User profile links are not
   * loaded.
   */
  public List lookupProfileRefs(String searchTerm)
  {
    Statement s = null;

    try
    {
      s = connectStatement();

      // limit number of 
      ResultSet rs = s.executeQuery(
        " select        " +
        "  count(tp.terminal_id) " +
        " from                                      " +
        "  trident_profile tp,                      " +
        "  mif                                      " +
        " where                                     " +
        "  tp.merchant_number = mif.merchant_number " +
        "  and ( tp.terminal_id                     " +
        "    like '%" + searchTerm + "%'            " +
        "   or to_char( tp.merchant_number )        " + 
        "    like '%" + searchTerm + "%'            " +
        "   or upper( tp.merchant_name ) like       " +
        "    '%" + searchTerm.toUpperCase() + "%' ) ");
      if (rs.next() && rs.getInt(1) > 500)
      {
        return null;
      }

      // load profile refs        
      rs = s.executeQuery(
        " select                                    " +
        "  tp.terminal_id           profile_id,     " +
        "  tp.merchant_number       merch_num,      " +
        "  tp.merchant_name         merch_name,     " +
        "  nvl(rp.enable_flag,'N')  enable_flag,    " +
        "  nvl(rc.rec_count,0)      rec_count,      " +
        "  nvl(uc.user_count,0)     user_count      " +
        " from                                      " +
        "  trident_profile tp,                      " +
        "  mif,                                     " +
        "  rbs_profiles rp,                         " +
        "  ( select                                 " +
        "     profile_id,                           " +
        "     count( rbs_id ) rec_count             " +
        "    from                                   " +
        "     rbs_billing_records                   " +
        "    group by profile_id ) rc,              " +
        "  ( select                                 " +
        "     profile_id,                           " +
        "     count( upl_id ) user_count            " +
        "    from                                   " +
        "     rbs_user_profiles                     " +
        "    group by profile_id ) uc               " +
        " where                                     " +
        "  tp.merchant_number = mif.merchant_number " +
        "  and ( tp.terminal_id                     " +
        "    like '%" + searchTerm + "%'            " +
        "   or to_char( tp.merchant_number )        " + 
        "    like '%" + searchTerm + "%'            " +
        "   or upper( tp.merchant_name ) like       " +
        "    '%" + searchTerm.toUpperCase() + "%' ) " +
        "  and tp.terminal_id = rp.profile_id(+)    " +
        "  and tp.terminal_id = rc.profile_id(+)    " +
        "  and tp.terminal_id = uc.profile_id(+)    " +
        " order by tp.terminal_id                   ");

      List refs = new ArrayList();
      while (rs.next())
      {
        refs.add(createProfileRef(rs));
      }
      return refs;
    }
    catch (Exception e)
    {
      handleException("lookupProfileRefs(searchTerm='" + searchTerm + "')",e);
    }
    finally
    {
      cleanUp(s);
    }

    return null;
  }
  
  /**
   * Finds any profile ref objects matching a search term that are linked
   * in the RBS system.
   */
  public List lookupLinkedProfileRefs(String searchTerm, String loginName)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      
      log.debug("searchTerm: " + searchTerm + ", loginName: " + loginName);

      // load profile refs        
      ResultSet rs = s.executeQuery(
        " select                                        " +
        "  tp.terminal_id           profile_id,         " +
        "  tp.merchant_number       merch_num,          " +
        "  tp.merchant_name         merch_name,         " +
        "  nvl(rp.enable_flag,'N')  enable_flag,        " +
        "  nvl(rc.rec_count,0)      rec_count,          " +
        "  nvl(uc.user_count,0)     user_count          " +
        " from                                          " +
        "  trident_profile tp,                          " +
        "  mif,                                         " +
        "  rbs_profiles rp,                             " +
        "  ( select                                     " +
        "     unique( profile_id ) profile_id           " +
        "    from                                       " +
        "     rbs_user_profiles                         " +
        ( loginName != null && loginName.length() > 0 ?
        "    where                                      " +
        "     login_name = '" + loginName + "'          " : "" ) +
        "  ) lp,                                        " +
        "  ( select                                     " +
        "     profile_id,                               " +
        "     count( rbs_id ) rec_count                 " +
        "    from                                       " +
        "     rbs_billing_records                       " +
        "    group by profile_id ) rc,                  " +
        "  ( select                                     " +
        "     profile_id,                               " +
        "     count( upl_id ) user_count                " +
        "    from                                       " +
        "     rbs_user_profiles                         " +
        "    group by profile_id ) uc                   " +
        " where                                         " +
        "  tp.merchant_number = mif.merchant_number     " +
        ( searchTerm != null && searchTerm.length() > 0 ?
        "  and (                                        " +
        "   tp.terminal_id                              " +
        "    like '%" + searchTerm + "%'                " +
        "   or to_char( tp.merchant_number )            " +
        "    like '%" + searchTerm + "%'                " +
        "   or upper( tp.merchant_name ) like           " +
        "    '%' || upper('" + searchTerm + "') || '%' ) " : "" ) +
        "  and tp.terminal_id = lp.profile_id           " +
        "  and tp.terminal_id = rp.profile_id(+)    " +
        "  and tp.terminal_id = rc.profile_id(+)        " +
        "  and tp.terminal_id = uc.profile_id(+)        " +
        " order by tp.terminal_id                       ");

      List refs = new ArrayList();
      while (rs.next())
      {
        refs.add(createProfileRef(rs));
      }
      return refs;
    }
    catch (Exception e)
    {
      handleException("lookupLinkedProfileRefs(searchTerm='" + searchTerm + "')",e);
    }
    finally
    {
      cleanUp(s);
    }

    return null;
  }

  /**
   * Returns profile ref with given profile id.  User profile links
   * are loaded if present.
   */
  public ProfileRef getProfileRef(String profileId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select                                    " +
        "  tp.terminal_id           profile_id,     " +
        "  tp.merchant_number       merch_num,      " +
        "  tp.merchant_name         merch_name,     " +
        "  nvl(rp.enable_flag,'N')  enable_flag,    " +
        "  nvl(rc.rec_count,0)      rec_count,      " +
        "  nvl(uc.user_count,0)     user_count      " +
        " from                                      " +
        "  trident_profile tp,                      " +
        "  mif,                                     " +
        "  rbs_profiles rp,                         " +
        "  ( select                                 " +
        "     profile_id,                           " +
        "     count( rbs_id ) rec_count             " +
        "    from                                   " +
        "     rbs_billing_records                   " +
        "    group by profile_id ) rc,              " +
        "  ( select                                 " +
        "     profile_id,                           " +
        "     count( upl_id ) user_count            " +
        "    from                                   " +
        "     rbs_user_profiles                     " +
        "    group by profile_id ) uc               " +
        " where                                     " +
        "  tp.merchant_number = mif.merchant_number " +
        "  and tp.terminal_id = '" + profileId + "' " +
        "  and tp.terminal_id = rp.profile_id(+)    " +
        "  and tp.terminal_id = rc.profile_id(+)    " +
        "  and tp.terminal_id = uc.profile_id(+)    ");

      ProfileRef ref = null;
      if (rs.next())
      {
        ref = createProfileRef(rs);
        ref.setUserLinks(getUserProfileLinks(profileId));
      }
      return ref;
    }
    catch (Exception e)
    {
      handleException("getProfileRef(profileId='" + profileId + "')",e);
    }
    finally
    {
      cleanUp(s);
    }

    return null;
  }
  
  public int getFailedPaymentCount(BillingRecord rec)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select                                  " +
        "  count(l.auth_id) fail_count            " +
        " from                                    " +
        "  rbs_billing_records r,                 " +
        "  rbs_auth_queue_log l                   " +
        " where                                   " +
        "  r.rbs_id = " + rec.getRbsId() +
        "  and r.rbs_id = l.rbs_id                " +
        "  and l.response_code not in ('000','0') ");
      if (rs.next())
      {
        return rs.getInt("fail_count");
      }
    }
    catch (Exception e)
    {
      handleException("getFailedPaymentCount(rec=" + rec + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    
    return 0;
  }
}