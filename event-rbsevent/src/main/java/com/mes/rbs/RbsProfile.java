package com.mes.rbs;

import java.sql.Timestamp;
import java.util.Date;

public class RbsProfile extends RbsBase
{
  private long rbspId;
  private String profileId;
  private String merchNum;
  private String merchName;
  private Date createDate;
  private String enableFlag;

  public void setRbspId(long rbspId)
  {
    this.rbspId = rbspId;
  }
  public long getRbspId()
  {
    return rbspId;
  }

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public String getMerchName()
  {
    return merchName;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setEnableFlag(String enableFlag)
  {
    validateFlag(enableFlag,"enable");
    this.enableFlag = enableFlag;
  }
  public String getEnableFlag()
  {
    return enableFlag;
  }
  public boolean isEnabled()
  {
    return flagBoolean(enableFlag);
  }

  public String toString()
  {
    return "RbsProfile ["
      + "rbspId: " + rbspId
      + ", profileId: " + profileId
      + ", merchNum: " + merchNum
      + ", merchName: " + merchName
      + ", createDate: " + createDate
      + ", isEnabled: " + isEnabled()
      + " ]";
  }
}