package com.mes.rbs;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import com.mes.ads.Account;
import com.mes.ads.AccountContainer;
import com.mes.ads.AccountType;

public class AuthQueueItem extends RbsBase implements AccountContainer
{
  private long authId;
  private Date authDate;
  private long rbsId;
  private long batchId;
  private String profileId;
  private String profileKey;
  private String merchNum;
  private String testFlag;
  private String customerId;
  private String status;
  private BigDecimal amount = new BigDecimal(0).setScale(2);
  private BigDecimal taxAmount = new BigDecimal(0).setScale(2);
  private int currencyCode;
  private String clientRefNum;
  private String merchName;
  private String merchPhone;
  
  private AccountType accountType;
  private long adsId;
  private Account account;
  
  private String responseCode;
  private String responseMessage;
  private String tridentTranId;
  private long stateId;

  public void setAuthId(long authId)
  {
    this.authId = authId;
  }
  public long getAuthId()
  {
    return authId;
  }

  public void setAuthDate(Date authDate)
  {
    this.authDate = authDate;
  }
  public Date getAuthDate()
  {
    return authDate;
  }
  public void setAuthTs(Timestamp authTs)
  {
    authDate = toDate(authTs);
  }
  public Timestamp getAuthTs()
  {
    return toTimestamp(authDate);
  }

  public void setRbsId(long rbsId)
  {
    this.rbsId = rbsId;
  }
  public long getRbsId()
  {
    return rbsId;
  }

  public void setBatchId(long batchId)
  {
    this.batchId = batchId;
  }
  public long getBatchId()
  {
    return batchId;
  }

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }

  public void setProfileKey(String profileKey)
  {
    this.profileKey = profileKey;
  }
  public String getProfileKey()
  {
    return profileKey;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return testFlag;
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  public void setCustomerId(String customerId)
  {
    this.customerId = customerId;
  }
  public String getCustomerId()
  {
    return customerId;
  }

  public void setStatus(String status)
  {
    this.status = status;
  }
  public String getStatus()
  {
    return status;
  }

  public void setAmount(BigDecimal amount)
  {
    this.amount = amount;
  }
  public BigDecimal getAmount()
  {
    return amount;
  }

  public void setTaxAmount(BigDecimal taxAmount)
  {
    this.taxAmount = taxAmount;
  }
  public BigDecimal getTaxAmount()
  {
    return taxAmount;
  }

  public void setCurrencyCode(int currencyCode)
  {
    this.currencyCode = currencyCode;
  }
  public int getCurrencyCode()
  {
    return currencyCode;
  }

  public void setClientRefNum(String clientRefNum)
  {
    this.clientRefNum = clientRefNum;
  }
  public String getClientRefNum()
  {
    return clientRefNum;
  }

  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public String getMerchName()
  {
    return merchName;
  }

  public void setMerchPhone(String merchPhone)
  {
    this.merchPhone = merchPhone;
  }
  public String getMerchPhone()
  {
    return merchPhone;
  }

  /**
   * ACCOUNT DATA (AccountContainer implementation)
   */

  public void setAdsId(long adsId)
  {
    this.adsId = adsId;
  }
  public long getAdsId()
  {
    return adsId;
  }
  
  public void setAccountType(AccountType accountType)
  {
    this.accountType = accountType;
  }
  public AccountType getAccountType()
  {
    return accountType;
  }
  
  public void setAccount(Account account)
  {
    this.account = account;
    if (account != null)
    {
      stateId = account.getStateId();
    }
  }
  public Account getAccount()
  {
    return account;
  }
  
  /**
   * RESPONSE DATA
   */
   
  public void setResponseCode(String responseCode)
  {
    this.responseCode = responseCode;
  }
  public String getResponseCode()
  {
    return responseCode;
  }

  public void setResponseMessage(String responseMessage)
  {
    this.responseMessage = responseMessage;
  }
  public String getResponseMessage()
  {
    return responseMessage;
  }

  public void setTridentTranId(String tridentTranId)
  {
    this.tridentTranId = tridentTranId;
  }
  public String getTridentTranId()
  {
    return tridentTranId;
  }

  public void setStateId(long stateId)
  {
    this.stateId = stateId;
  }
  public long getStateId()
  {
    return stateId;
  }

  public String toString()
  {
    return "AuthQueueItem ["
      + "authId: " + authId
      + ", authDate: " + authDate
      + ", batchId: " + batchId
      + ", rbsId: " + rbsId
      + ", profileId: " + profileId
      + ", profileKey: " + profileKey
      + ", merchNum: " + merchNum
      + ", isTest: " + isTest()
      + ", customerId: " + customerId
      + ", status; " + status
      + ", amount: " + amount
      + ", taxAmount: " + taxAmount
      + ", currencyCode: " + currencyCode
      + ", clientRefNum: " + clientRefNum
      + ", merchName: " + merchName
      + ", merchPhone: " + merchPhone
      + ", accountType: " + accountType
      + ", adsId: " + adsId
      + ", account: " + account
      + ", responseCode: " + responseCode
      + ", responseMessage: " + responseMessage
      + ", tridentTranId: " + tridentTranId
      + ", stateId: " + stateId
      + " ]";
  }
}