package com.mes.rbs;

import org.apache.log4j.Logger;

public class RbsLoadJob extends RbsJob
{
  static Logger log = Logger.getLogger(RbsLoadJob.class);

  public void run(SysType sys, RbsDb db)
  {
    db.loadAuthQueue(sys);
  }
}