package com.mes.rbs;

import com.mes.support.CodeType;

public class SysType extends CodeType
{
  public static SysType TEST  = new SysType("test");
  public static SysType PROD  = new SysType("prod");
  public static SysType DEV   = new SysType("dev");

  static
  {
    codeHash.put(""+TEST,TEST);
    codeHash.put(""+PROD,PROD);
    codeHash.put(""+DEV,DEV);
  };

  private SysType(String code)
  {
    super(code);
  }

  public static SysType sysTypeFor(String forStr, SysType dflt)
  {
    return (SysType)forCode(forStr,dflt);
  }

  public static SysType sysTypeFor(String forStr)
  {
    return (SysType)forCode(forStr);
  }
}
   
