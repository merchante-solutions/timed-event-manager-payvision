package com.mes.rbs;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import com.mes.ads.Account;
import com.mes.ads.AccountContainer;
import com.mes.ads.AccountType;
import com.mes.ads.AchpAccount;
import com.mes.ads.AchpDfiType;
import com.mes.ads.CcAccount;
import com.mes.ads.CcCardType;
import com.mes.ads.ManContainer;
import com.mes.pach.SecCode;

public class BillingRecord extends RbsBase implements AccountContainer
{
  public static final String FREQ_MONTHLY         = "MONTHLY";
  public static final String FREQ_QUARTERLY       = "QUARTERLY";
  public static final String FREQ_ANNUALLY        = "ANNUALLY";

  // identity
  private long rbsId;
  private String userName;
  private Date createDate;
  private String profileId;
  private String merchNum;
  private String testFlag;

  // recurring billing details
  private String customerId;
  private BigDecimal amount = new BigDecimal(0).setScale(2);
  private int payTotal;
  private int payCount;
  private String frequency;
  private Date startDate;
  private Date nextDate;
  private int currencyCode;
  private BigDecimal taxAmount = new BigDecimal(0).setScale(2);
  private String clientRefNum;

  // additional transaction data (dynamic dba)
  private String merchName;
  private String merchPhone;

  // account data
  private long adsId;
  private Account account;
  
  /**
   * IDENTITY
   */
  
  public void setRbsId(long rbsId)
  {
    this.rbsId = rbsId;
  }
  public long getRbsId()
  {
    return rbsId;
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
    syncAccount();
  }
  public String getProfileId()
  {
    return profileId;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
    syncAccount();
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setTestFlag(String testFlag)
  {
    validateFlag(testFlag,"test");
    this.testFlag = testFlag;
    syncAccount();
  }
  public String getTestFlag()
  {
    return testFlag;
  }
  public boolean isTest()
  {
    return flagBoolean(testFlag);
  }

  /**
   * RECURRING BILLING DETAILS
   */
   
  public void setCustomerId(String customerId)
  {
    this.customerId = customerId;
  }
  public String getCustomerId()
  {
    return customerId;
  }

  public void setAmount(BigDecimal amount)
  {
    this.amount = amount;
  }
  public BigDecimal getAmount()
  {
    return amount;
  }

  public void setPayTotal(int payTotal)
  {
    this.payTotal = payTotal;
  }
  public int getPayTotal()
  {
    return payTotal;
  }

  public void setPayCount(int payCount)
  {
    this.payCount = payCount;
  }
  public int getPayCount()
  {
    return payCount;
  }

  /**
   * Validate frequency.
   */
  public static void validateFrequency(String frequency)
  {
    if (!FREQ_MONTHLY.equals(frequency) &&
        !FREQ_QUARTERLY.equals(frequency) &&
        !FREQ_ANNUALLY.equals(frequency))
    {
      throw new RuntimeException("Invalid frequency: '" + frequency + "'");
    }
  }

  public void setFrequency(String frequency)
  {
    validateFrequency(frequency);
    this.frequency = frequency;
  }
  public String getFrequency()
  {
    return frequency;
  }

  public void setStartDate(Date startDate)
  {
    this.startDate = startDate;
  }
  public Date getStartDate()
  {
    return startDate;
  }
  public void setStartTs(Timestamp startTs)
  {
    startDate = toDate(startTs);
  }
  public Timestamp getStartTs()
  {
    return toTimestamp(startDate);
  }

  public void setNextDate(Date nextDate)
  {
    this.nextDate = nextDate;
  }
  public Date getNextDate()
  {
    return nextDate;
  }
  public void setNextTs(Timestamp nextTs)
  {
    nextDate = toDate(nextTs);
  }
  public Timestamp getNextTs()
  {
    return toTimestamp(nextDate);
  }

  public void setCurrencyCode(int currencyCode)
  {
    this.currencyCode = currencyCode;
  }
  public int getCurrencyCode()
  {
    return currencyCode;
  }

  public void setTaxAmount(BigDecimal taxAmount)
  {
    this.taxAmount = taxAmount;
  }
  public BigDecimal getTaxAmount()
  {
    return taxAmount;
  }

  public void setClientRefNum(String clientRefNum)
  {
    this.clientRefNum = clientRefNum;
  }
  public String getClientRefNum()
  {
    return clientRefNum;
  }

  /**
   * CREDIT CARD TRANSACTION DATA (DYNAMIC DBA)
   */
  
  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public String getMerchName()
  {
    return merchName;
  }

  public void setMerchPhone(String merchPhone)
  {
    this.merchPhone = merchPhone;
  }
  public String getMerchPhone()
  {
    return merchPhone;
  }
  
  /** 
   * ACCOUNT DATA
   */
   
  public long getAdsId()
  {
    return adsId;
  }
  public void setAdsId(long adsId)
  {
    this.adsId = adsId;
    syncAccount();
  }
  
  public AccountType getAccountType()
  {
    return account != null ? account.getAccountType() : null;
  }
  public void setAccountType(AccountType t)
  {
    createAccount(t);
  }
  
  public Account getAccount()
  {
    return account;
  }
  public void setAccount(Account account)
  {
    this.account = account;
    if (account != null)
    {
      adsId = account.getAdsId();
    }
  }
  
  private void createAccount(AccountType t)
  {
    if (account != null)
    {
      throw new RuntimeException("Account already set");
    }
    if (AccountType.CC.equals(t))
    {
      account = new CcAccount();
    }
    else if (AccountType.ACHP.equals(t))
    {
      account = new AchpAccount();
    }
    else
    {
      throw new IllegalArgumentException("Account type not supported: " + t);
    }
    syncAccount();
  }
  
  /**
   * Sync overlapping billing record and account data.
   */
  private void syncAccount()
  {
    if (account != null)
    {
      if (adsId != 0) 
        account.setAdsId(adsId);
      if (profileId != null) 
        account.setProfileId(profileId);
      if (merchNum != null)
        account.setMerchNum(merchNum);
      if (testFlag != null)
        account.setTestFlag(testFlag);
    }
  }
  
  /**
   * ACCOUNT TYPE HELPERS
   */
   
  public CcAccount getCcAccount()
  {
    return (CcAccount)account;
  }
  
  public AchpAccount getAchpAccount()
  {
    return (AchpAccount)account;
  }
  
  public ManContainer getManContainer()
  {
    return (ManContainer)account;
  }
  
  public boolean isCc()
  {
    return AccountType.CC.equals(getAccountType());
  }
  
  public boolean isAchp()
  {
    return AccountType.ACHP.equals(getAccountType());
  }
  
  /**
   * ACCOUNT NUMBER HELPERS
   */
   
  public String getAccountNum()
  {
    return account != null ? getManContainer().getAccountNum() : null;
  }
  public void setAccountNum(String accountNum)
  {
    getManContainer().setAccountNum(accountNum);
  }
  public String getAccountNumTrunc()
  {
    return account != null ? getManContainer().getMan().truncated() : null;
  }
    
  /**
   * CREDIT CARD ACCOUNT DATA
   */
   
  public void setStoreId(String storeId)
  {
    getCcAccount().setCardId(storeId);
  }
  public String getStoreId()
  {
    return getCcAccount().getCardId();
  }
  public boolean hasStoreId()
  {
    return isCc() && getStoreId() != null;
  }

  public CcCardType getCardType()
  {
    return getCcAccount().getCardType();
  }

  public String getExpDate()
  {
    return getCcAccount().getExpDate();
  }
  public void setExpDate(String expDate)
  {
    getCcAccount().setExpDate(expDate);
  }

  public String getCardName()
  {
    return getCcAccount().getCardName();
  }
  public void setCardName(String cardName)
  {
    getCcAccount().setCardName(cardName);
  }
  
  public void setAvsStreet(String avsStreet)
  {
    getCcAccount().setAvsStreet(avsStreet);
  }
  public String getAvsStreet()
  {
    return getCcAccount().getAvsStreet();
  }

  public void setAvsZip(String avsZip)
  {
    getCcAccount().setAvsZip(avsZip);
  }
  public String getAvsZip()
  {
    return getCcAccount().getAvsZip();
  }
  
  /**
   * ACHP ACCOUNT DATA
   */
  
  public AchpDfiType getDfiType()
  {
    return getAchpAccount().getDfiType();
  }
  public void setDfiType(AchpDfiType dfiType)
  {
    getAchpAccount().setDfiType(dfiType);
  }
  
  public String getTransitNum()
  {
    return getAchpAccount().getTransitNum();
  }
  public void setTransitNum(String transitNum)
  {
    getAchpAccount().setTransitNum(transitNum);
  }
  
  public String getAchCustomerName()
  {
    return getAchpAccount().getCustomerName();
  }
  public void setAchCustomerName(String customerName)
  {
    getAchpAccount().setCustomerName(customerName);
  }
  
  public String getAchCustomerId()
  {
    return getAchpAccount().getCustomerId();
  }
  public void setAchCustomerId(String achCustomerId)
  {
    getAchpAccount().setCustomerId(achCustomerId);
  }
  
  public SecCode getAchAuthType()
  {
    return SecCode.codeFor(getAchpAccount().getSecCode());
  }
  public void setAchAuthType(SecCode achAuthType)
  {
    getAchpAccount().setSecCode(""+achAuthType);
  }
  
  public String getIpAddress()
  {
    return getAchpAccount().getIpAddress();
  }
  public void setIpAddress(String ipAddress)
  {
    getAchpAccount().setIpAddress(ipAddress);
  }

  public String toString()
  {
    return "BillingRecord [ "
      + "rbsId: " + rbsId
      + ", userName: " + userName
      + ", createDate: " + createDate
      + ", profileId: " + profileId
      + ", merchNum: " + merchNum
      + ", isTest: " + isTest()
      + ", customerId: " + customerId
      + ", amount: " + amount
      + ", payTotal: " + payTotal
      + ", payCount: " + payCount
      + ", frequency: " + frequency
      + ", startDate: " + startDate
      + ", nextDate: " + nextDate
      + ", currencyCode: " + currencyCode
      + ", taxAmount: " + taxAmount
      + ", clientRefNum: " + clientRefNum
      + ", merchName: " + merchName
      + ", merchPhone: " + merchPhone
      + ", adsId: " + adsId
      + ", account: " + account
      + " ]";
  }
}