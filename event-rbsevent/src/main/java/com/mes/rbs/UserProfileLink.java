package com.mes.rbs;

import java.sql.Timestamp;
import java.util.Date;

public class UserProfileLink extends RbsBase
{

  private long uplId;
  private String profileId;
  private String merchNum;
  private String merchName;
  private String userName;
  private String loginName;
  private Date createDate;
  private String enableFlag;

  public void setUplId(long uplId)
  {
    this.uplId = uplId;
  }
  public long getUplId()
  {
    return uplId;
  }

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public String getMerchName()
  {
    return merchName;
  }

  public void setLoginName(String loginName)
  {
    this.loginName = loginName;
  }
  public String getLoginName()
  {
    return loginName;
  }

  public void setUserName(String userName)
  {
    this.userName = userName;
  }
  public String getUserName()
  {
    return userName;
  }

  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return toTimestamp(createDate);
  }

  public void setEnableFlag(String enableFlag)
  {
    validateFlag(enableFlag,"enable");
    this.enableFlag = enableFlag;
  }
  public String getEnableFlag()
  {
    return enableFlag;
  }
  public boolean isEnabled()
  {
    return flagBoolean(enableFlag);
  }

  public String toString()
  {
    return "UserProfileLink ["
      + "uplId: " + uplId
      + ", profileId: " + profileId
      + ", merchNum: " + merchNum
      + ", merchName: " + merchName
      + ", loginName: " + loginName
      + ", userName: " + userName
      + ", createDate: " + createDate
      + ", isEnabled: " + isEnabled()
      + " ]";
  }
}