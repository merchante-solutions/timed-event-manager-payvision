package com.mes.rbs;

import java.util.List;

public class ProfileRef extends RbsBase
{
  private String profileId;
  private String merchNum;
  private String merchName;
  private String enableFlag;
  private int recCount;
  private int userCount;
  private List userLinks;

  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  public String getProfileId()
  {
    return profileId;
  }

  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  public String getMerchNum()
  {
    return merchNum;
  }

  public void setMerchName(String merchName)
  {
    this.merchName = merchName;
  }
  public String getMerchName()
  {
    return merchName;
  }

  public void setEnableFlag(String enableFlag)
  {
    validateFlag(enableFlag,"enable");
    this.enableFlag = enableFlag;
  }
  public String getEnableFlag()
  {
    return enableFlag;
  }
  public boolean isEnabled()
  {
    return flagBoolean(enableFlag);
  }

  public void setRecCount(int recCount)
  {
    this.recCount = recCount;
  }
  public int getRecCount()
  {
    return recCount;
  }

  public void setUserCount(int userCount)
  {
    this.userCount = userCount;
  }
  public int getUserCount()
  {
    return userCount;
  }

  public void setUserLinks(List userLinks)
  {
    this.userLinks = userLinks;
  }
  public List getUserLinks()
  {
    return userLinks;
  }

  public String toString()
  {
    return "ProfileRef ["
      + "profileId: " + profileId
      + ", merchNum: " + merchNum
      + ", merchName: " + merchName
      + ", isEnabled: " + isEnabled()
      + ", recCount: " + recCount
      + ", userCount: " + userCount
      + ", userLinks: " + userLinks
      + " ]";
  }
}