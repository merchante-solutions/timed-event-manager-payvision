package com.mes.support;

import org.apache.log4j.Logger;
import com.mes.support.dc.MesEncryptedDataContainer;

public class MesAccountNumber extends MesEncryptedDataContainer
{
  static Logger log = Logger.getLogger(MesAccountNumber.class);

  private String _truncated;

  public MesAccountNumber()
  {
  }
  public MesAccountNumber(String accountNum)
  {
    setData(accountNum);
  }

  public String clearText()
  {
    return empty() ? null : new String(getData());
  }

  public String truncated()
  {
    if (!empty() && _truncated == null)
    {
      try
      {
        _truncated = 
          TridentTools.encodeCardNumber(clearText());
      }
      catch (Exception e)
      {
        log.error("Truncation error: " + e);
        e.printStackTrace();
      }
    }
    return _truncated;
  }

  public void setEncodedData(byte[] data)
  {
    super.setEncodedData(data);
    _truncated = null;
  }

  public String toString()
  {
    return "MesAccountNumber [ "
      + "acct num: " + truncated()
      + " ]";
  }
}

