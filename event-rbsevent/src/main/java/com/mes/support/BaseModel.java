package com.mes.support;

import java.sql.Timestamp;
import java.util.Date;

public class BaseModel
{
  /** Format a date suitable for html display. */
   
  public static String forDisplay(Object obj)
  {
    return obj != null && (""+obj).length() > 0 ? ""+obj : "--";
  }
  
  public static String formatHtmlDate(Date date)
  {
    return forDisplay(MesCalendar.formatDate(date,MesCalendar.FMT_HTML));
  }
  
  public static String formatHtmlDateSecs(Date date)
  {
    return forDisplay(MesCalendar.formatDate(date,MesCalendar.FMT_HTML2));
  }

  /** Convert between java.util.Date and java.sql.Timestamp */
  
  protected Date toDate(Timestamp ts)
  {
    return MesCalendar.toDate(ts);
  }
  
  protected Timestamp toTimestamp(Date date)
  {
    return MesCalendar.toTimestamp(date);
  }

  /** Manage boolean values to be stored in DB as 'Y' or 'N'. */
  
  public static final String FLAG_YES   = "Y";
  public static final String FLAG_NO    = "N";

  public static void validateFlag(String flag, String val1, String val2, 
    String descriptor)
  {
    if (!flag.equals(val1) && !flag.equals(val2))
    {
      throw new RuntimeException("Invalid " + descriptor + " flag value '" 
        +  flag + "'" + ", must be '" + val1 + "' or '" + val2 + "'");
    }
  }
  
  public static void validateFlag(String flag, String descriptor)
  {
    validateFlag(flag,FLAG_YES,FLAG_NO,descriptor);
  }
  
  public static String flagValue(String flag)
  {
    return flag != null ? flag : FLAG_NO;
  }
  
  public static boolean flagBoolean(String flag)
  {
    return flag != null && flag.equals(FLAG_YES);
  }
}