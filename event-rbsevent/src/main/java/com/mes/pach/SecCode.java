package com.mes.pach;

import java.util.HashMap;

public class SecCode
{
  // supported standard entry class (SEC) types 
  public static final SecCode CCD = new SecCode("CCD");
  public static final SecCode PPD = new SecCode("PPD");
  public static final SecCode WEB = new SecCode("WEB");
  public static final SecCode TEL = new SecCode("TEL");

  private static HashMap typeHash = new HashMap();

  static
  {
    typeHash.put(""+CCD,CCD);
    typeHash.put(""+PPD,PPD);
    typeHash.put(""+WEB,WEB);
    typeHash.put(""+TEL,TEL);
  };

  private String codeStr;

  private SecCode(String codeStr)
  {
    this.codeStr = codeStr;
  }

  public static SecCode codeFor(String forStr, SecCode dflt)
  {
    SecCode secCode = (SecCode)typeHash.get(forStr);
    return secCode != null ? secCode : dflt;
  }
  public static SecCode codeFor(String forStr)
  {
    SecCode secCode = codeFor(forStr,null);
    if (secCode == null)
    {
      throw new NullPointerException("Invalid code string: '" + forStr + "'");
    }
    return secCode;
  }

  public boolean equals(Object o)
  {
    if (!(o instanceof SecCode))
    {
      return false;
    }
    SecCode that = (SecCode)o;
    return that.codeStr.equals(codeStr);
  }

  public String toString()
  {
    return codeStr;
  }
}