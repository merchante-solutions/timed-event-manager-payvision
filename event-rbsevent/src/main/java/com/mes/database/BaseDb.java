package com.mes.database;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
//import java.util.regex.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.support.MesCalendar;
import sqlj.runtime.ResultSetIterator;

public class BaseDb extends ConnectionBase
{
  static Logger log = Logger.getLogger(BaseDb.class);

  // conntection type, POOL or DIRECT
  private DbcType dbc;

  // name of id sequence to get new unique ids from
  private String idSeqName;

  // name of parameter table containing
  // name value pairs for application configuration
  private String cfgTblName;

  /**
   * CONSTRUCTORS
   */

  public BaseDb(DbcType dbc, String idSeqName, String cfgTblName)
  {
    // initialize connection pool or standalone 
    // connection based on direct flag
    super(DbcType.DIRECT.equals(dbc) ? 
          SQLJConnectionBase.getDirectConnectString() :
          DEFAULT_CONNECTION_STRING);

    if (dbc == null)
    {
      throw new NullPointerException("Null database connect type not allowed");
    }
    this.dbc = dbc;

    if (idSeqName == null)
    {
      throw new NullPointerException("Null id sequence not allowed");
    }
    this.idSeqName = idSeqName;

    if (cfgTblName == null)
    {
      throw new NullPointerException("Null config table name not allowed");
    }
    this.cfgTblName = cfgTblName;
  }
  public BaseDb(boolean directFlag, String idSeqName, String cfgTblName)
  {
    this(directFlag ? DbcType.DIRECT : DbcType.POOL, idSeqName, cfgTblName);
  }
  
  /**
   * cleanUp -> cleanup
   */

  public void cleanUp()                                   { cleanup();      }
  public void cleanUp(PreparedStatement ps)               { cleanup(ps);    }
  public void cleanUp(PreparedStatement ps, ResultSet rs) { cleanup(ps,rs); }
  public void cleanUp(ResultSetIterator it, ResultSet rs) { cleanup(it,rs); }
  public void cleanUp(Statement s)                        { cleanup(s);     }
  public void cleanUp(Statement s, ResultSet rs)          { cleanup(s,rs);  }
  public void cleanUp(CallableStatement cs)               { cleanup(cs);    }

  
  /**
   * Returns true if NOT using connection pool (standalone apps) or false
   * if using connection pool (web apps).
   */
  public boolean isDirect()
  {
    return DbcType.DIRECT.equals(dbc);
  }
  
  public DbcType getDbc()
  {
    return dbc;
  }

  /**
   * Truncate a string.  Converts null to empty string.  Returns
   * string that is no longer than the maximum length specified.
   */
  public static String trunc(String src, int maxLen)
  {
    if (src == null)
    {
      return "";
    }
    if (src.length() > maxLen)
    {
      return src.substring(0,maxLen);
    }
    return src;
  }

  /**
   * java.util.Date <-> java.sql.Timestamp conversion
   */

  public static Date toDate(Timestamp ts)
  {
    return ts != null ? new Date(ts.getTime()) : null;
  }
  public static Timestamp toTimestamp(Date date)
  {
    return date != null ? new Timestamp(date.getTime()) : null;
  }
  
  /**
   * Date/time formatted string generation for use in non-prepared
   * statements.
   */
   
  public static String oraDateExpression(Date date)
  {
    return "to_date(" + MesCalendar.formatDate(date,FMT_ORA_DATE) 
      + ",'DD-MON-RR')";
  }
  
  public static String oraDateTimeExpression(Date date)
  {
    return "to_date(" + MesCalendar.formatDate(date,FMT_ORA_DT)
      + ",'DD-MON-RR HH:MI:SS AM')";
  }

  /**
   * Fetches next value from id sequence named in idSeqName.
   */
  public long getNewId() throws SQLException
  {
    Statement s = null;

    try
    {
      connect();
      s = con.createStatement();
      ResultSet rs = s.executeQuery(
        " select " + idSeqName + ".nextval from dual ");
      rs.next();
      long newId = rs.getLong(1);
      return newId;
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * CONFIGURATION PARAMETERS
   *
   * (transaction safe)
   */

  protected String getConfigValue(String name)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select value from " + cfgTblName + " where name = '" + name + "'");
      if (rs.next())
      {
        return rs.getString("value");
      }
    }
    catch (Exception e)
    {
      handleException("getConfigValue(name='" + name + "')",e);
    }
    finally
    {
      cleanUp(s);
    }
    return null;
  }

  protected void setConfigValue(String name, String value)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      int rowCount = s.executeUpdate(
        " update " + cfgTblName + " set value = '" + value + "' where name = '" 
          + name + "'");
      if (rowCount < 1)
      {
        s.executeUpdate(
          " insert into " + cfgTblName + " ( name, value ) values ( '" 
            + name + "', '" + value + "' ) ");
      } 
    }
    catch (Exception e)
    {
      handleException("setConfigValueInt('" + name + "'='" + value + "')",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  protected void deleteConfigValue(String name)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      int rowCount = s.executeUpdate(
        " delete from " + cfgTblName + " where name = '" + name + "'");
    }
    catch (Exception e)
    {
      throw new RuntimeException("deleteConfigValue('" + name + "')",e);
    }
    finally
    {
      cleanUp(s);
    }
  }

  /**
   * Config parm caching scheme
   */

  // default number of milliseconds to expire parms: 5 secs
  private static long PARM_CACHE_TIMEOUT = 5000;

  private static Map parms = new Hashtable();

  private long timeout = PARM_CACHE_TIMEOUT;

  public class Parm
  {
    public String name;
    public String value;
    public long valueTs;

    public Parm(String name, String value)
    {
      this.name = name;
      this.value = value;
      valueTs = now();
    }

    private long now()
    {
      return Calendar.getInstance().getTime().getTime();
    }

    public boolean expired()
    {
      return (now() - valueTs) > timeout;
    }
  }

  protected String getValue(String name)
  {
    String value = null;
    Parm parm = (Parm)parms.get(name);
    if (parm == null || parm.expired())
    {
      value = getConfigValue(name);
      if (value != null)
      {
        parms.put(name,new Parm(name,value));
      }
    }
    else
    {
      value = parm.value;
    }
    return value;
  }

  protected void setValue(String name, String value)
  {
    if (value != null)
    {
      setConfigValue(name,value);
      parms.put(name,new Parm(name,value));
    }
    else
    {
      deleteConfigValue(name);
      parms.remove(name);
    }
  }
  protected void setValue(String name, int value)
  {
    setValue(name,String.valueOf(value));
  }
  protected void setValue(String name, Date date, String pattern)
  {
    try
    {
      setValue(name,MesCalendar.formatDate(date,pattern));
    }
    catch (Exception e)
    {
      handleException("setValue('" + name + "', date=" + date 
        + ", pat='" + pattern + "')",e);
    }
  }

  /**
   * Config parameter convenience functions
   */

  protected long getValueAsLong(String name)
  {
    String value = getValue(name);
    try
    {
      return Long.parseLong(value);
    }
    catch (Exception e)
    {
      handleException("getValueAsLong(name='" + name + "')",e);
    }
    return 0;
  }

  protected int getValueAsInt(String name)
  {
    String value = getValue(name);
    try
    {
      return Integer.parseInt(value);
    }
    catch (Exception e)
    {
      handleException("getValueAsInt(name='" + name + "')",e);
    }
    return 0;
  }

  protected Date getValueAsDate(String name, String pattern)
  {
    return MesCalendar.stringToDate(getValue(name),pattern);
  }

  /**
   * Parses value into a timestamp (java.util.Date) object.
   */
  protected Date getValueAsTs(String name, String[] patterns)
  {
    String value = getValue(name);
    Date ts = MesCalendar.stringToDateTime(getValue(name),patterns);
    if (ts == null)
    {
      throw new RuntimeException("Unparseable date found for name '" 
        + name + "': '" + value + "'");
    }
    return ts;
  }
  protected Date getValueAsTs(String name)
  {
    String value = getValue(name);
    Date ts = MesCalendar.stringToDateTime(getValue(name));
    if (ts == null)
    {
      throw new RuntimeException("Unparseable date found for name '" 
        + name + "': '" + value + "'");
    }
    return ts;
  }

  /**
   * Returns parsed time from a config value, the date fields
   * will be set to the current date.
   */
  protected Date getValueAsTime(String name, String[] patterns)
  {
    String value = getValue(name);
    Date time = MesCalendar.stringToTime(value,patterns);
    if (time == null)
    {
      throw new RuntimeException("Unparseable time found for name '" 
        + name + "': '" + value + "'");
    }
    return time;
  }
  protected Date getValueAsTime(String name)
  {
    String value = getValue(name);
    Date time = MesCalendar.stringToTime(value);
    if (time == null)
    {
      throw new RuntimeException("Unparseable time found for name '" 
        + name + "': '" + value + "'");
    }
    return time;
  }
}
