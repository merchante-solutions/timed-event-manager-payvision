package com.mes.database;

import com.mes.support.CodeType;

public class DbcType extends CodeType
{
  public static DbcType DIRECT  = new DbcType("direct");
  public static DbcType POOL    = new DbcType("pool");

  static
  {
    codeHash.put(""+DIRECT,DIRECT);
    codeHash.put(""+POOL,POOL);
  };

  private DbcType(String code)
  {
    super(code);
  }

  public static DbcType dbcTypeFor(String forStr, DbcType dflt)
  {
    return (DbcType)forCode(forStr,dflt);
  }

  public static DbcType dbcTypeFor(String forStr)
  {
    return (DbcType)forCode(forStr);
  }
}
   
