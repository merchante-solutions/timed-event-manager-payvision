package com.mes.startup;

import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.database.DbcType;
import com.mes.rbs.RbsAuthJob;
import com.mes.rbs.RbsJobHandler;
import com.mes.rbs.RbsLoadJob;
import com.mes.rbs.SysType;

public final class RbsEvent extends EventBase
{
  static Logger log = Logger.getLogger(RbsEvent.class);
  
  public static final String ARG_EVENT_TYPE = "event";
  public static final String ARG_SYS_TYPE   = "sys";

  public static final String ET_LOAD        = "load";
  public static final String ET_AUTH        = "auth";

  public static final String SYS_PROD       = ""+SysType.PROD;
  public static final String SYS_TEST       = ""+SysType.TEST;
  public static final String SYS_DEV        = ""+SysType.DEV;

  private static String[] VALS_EVENT_TYPE   = { ET_LOAD, ET_AUTH };
  private static String[] VALS_SYS_TYPE     = { SYS_PROD, SYS_TEST, SYS_DEV };

  private static Map argValsMap = new HashMap();

  static
  {
    argValsMap.put(ARG_EVENT_TYPE,VALS_EVENT_TYPE);
    argValsMap.put(ARG_SYS_TYPE,VALS_SYS_TYPE);
  }

  private String getValidEventArg(String arg, int argNum)
  {
    String[] allowedValues = (String[])argValsMap.get(arg);
    if (allowedValues == null)
    {
      throw new NullPointerException("Event argument '" + arg + "' invalid");
    }

    String value = getEventArg(argNum).toLowerCase();
    for (int i = 0; i < allowedValues.length; ++i)
    {
      if (allowedValues[i].toLowerCase().equals(value))
      {
        return value;
      }
    }

    throw new RuntimeException("Invalid value '" + value 
      + "' for event argument " + argNum);
  }

  public boolean execute()
  {
    try
    {
      String eventType = getValidEventArg(ARG_EVENT_TYPE,0);
      SysType sys = SysType.sysTypeFor(getValidEventArg(ARG_SYS_TYPE,1));

      log.debug(
        "RbsEvent executing: eventType=" + eventType + ", sys=" + sys);
      //System.out.println(
      //  "RbsEvent executing: eventType=" + eventType + ", sys=" + sys);

      RbsJobHandler handler = new RbsJobHandler(sys,DbcType.POOL);
      if (eventType.equals(ET_LOAD))
      {
        handler.run(new RbsLoadJob());
      }
      else if (eventType.equals(ET_AUTH))
      {
        handler.run(new RbsAuthJob());
      }
      return true;
    }
    catch (Exception e)
    {
      log.error(""+e);
      e.printStackTrace();
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    return false;
  }

  public static void main(String[] args) {
    try {
      if (args.length > 0 && args[0].equals("testproperties")) {
        EventBase.printKeyListStatus(new String[]{
                MesDefaults.TRIDENT_API_REQUEST_URL
        });
      }
    } catch (Exception e) {
      log.error(e.toString());
    }
  }}