package com.mes.ads;

import java.sql.Timestamp;
import java.util.Date;

public interface Account
{
  public long getAdsId();
  public void setAdsId(long adsId);
  public Date getCreateDate();
  public void setCreateDate(Date date);
  public Timestamp getCreateTs();
  public void setCreateTs(Timestamp ts);
  public Date getUpdateDate();
  public void setUpdateDate(Date date);
  public Timestamp getUpdateTs();
  public void setUpdateTs(Timestamp ts);
  public void setStateId(long stateId);
  public long getStateId();
  public Date getStartDate();
  public void setStartTs(Timestamp ts);
  public Date getEndDate();
  public void setEndTs(Timestamp ts);
  public String getProfileId();
  public void setProfileId(String profileId);
  public String getMerchNum();
  public void setMerchNum(String merchNum);
  public AccountType getAccountType();
  public String getTestFlag();
  public void setTestFlag(String testFlag);
  public boolean isTest();
}