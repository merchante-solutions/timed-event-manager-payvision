package com.mes.ads;

import com.mes.support.CodeType;

public class CcCardType extends CodeType
{
  public static CcCardType VISA  = new CcCardType("VISA");
  public static CcCardType MC    = new CcCardType("MC");
  public static CcCardType AMEX  = new CcCardType("AMEX");
  public static CcCardType DISC  = new CcCardType("DISC");

  static
  {
    codeHash.put(""+VISA,VISA);
    codeHash.put(""+MC,MC);
    codeHash.put(""+AMEX,AMEX);
    codeHash.put(""+DISC,DISC);
  };

  private CcCardType(String code)
  {
    super(code);
  }

  public static CcCardType forType(String forStr, CcCardType dflt)
  {
    return (CcCardType)forCode(forStr,dflt);
  }

  public static CcCardType forType(String forStr)
  {
    return (CcCardType)forCode(forStr);
  }
}
   
