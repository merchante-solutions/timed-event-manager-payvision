package com.mes.ads;

import org.apache.log4j.Logger;
import com.mes.support.MesAccountNumber;

public class CcAccount extends AccountBase implements ManContainer
{
  static Logger log = Logger.getLogger(CcAccount.class);
  
  private MesAccountNumber man = new MesAccountNumber();
  private CcCardType cardType;
  private String expDate;
  private String cardName;
  private String avsStreet;
  private String avsZip;
  private String cardId;
  
  public AccountType getAccountType()
  {
    return AccountType.CC;
  }
  
  public String getAccountNum()
  {
    return man != null ? man.clearText() : null;
  }
  public void setAccountNum(String accountNum)
  {
    man.setData(accountNum.getBytes());
  }
  public MesAccountNumber getMan()
  {
    return man;
  }
  public void setMan(MesAccountNumber man)
  {
    this.man = man;
  }
  
  /**
   * Return a card type determined from the card number.
   */
  public CcCardType getCardType()
  {
    String cardNum = getAccountNum();
    if (cardNum == null)
    {
      return null;
    }
    if (cardNum.startsWith("4"))
    {
      return CcCardType.VISA;
    }
    if (cardNum.startsWith("5"))
    {
      return CcCardType.MC;
    }
    if (cardNum.startsWith("6"))
    {
      return CcCardType.DISC;
    }
    if (cardNum.startsWith("3"))
    {
      return CcCardType.AMEX;
    }
    throw new RuntimeException(
      "Card type cannot be determined from card number: " + cardNum);
  }
  
  public void setExpDate(String expDate)
  {
    this.expDate = expDate;
  }
  public String getExpDate()
  {
    return expDate;
  }

  public String getCardName()
  {
    return cardName;
  }
  public void setCardName(String cardName)
  {
    this.cardName = cardName;
  }
  
  public String getAvsStreet()
  {
    return avsStreet;
  }
  public void setAvsStreet(String avsStreet)
  {
    this.avsStreet = avsStreet;
  }
  
  public String getAvsZip()
  {
    return avsZip;
  }
  public void setAvsZip(String avsZip)
  {
    this.avsZip = avsZip;
  }
  
  public String getCardId()
  {
    return cardId;
  }
  public void setCardId(String cardId)
  {
    this.cardId = cardId;
  }
  public boolean hasCardId()
  {
    return cardId != null;
  }
  
  public String toString()
  {
    return "CcAccount [ "
      + baseToString()
      + ", accountNum: " + getAccountNum()
      + ", cardType: " + getCardType()
      + ", expDate: " + expDate
      + ", cardName: " + cardName
      + ", avsStreet: " + avsStreet
      + ", avsZip: " + avsZip
      + ", cardId: " + cardId;
  }
}