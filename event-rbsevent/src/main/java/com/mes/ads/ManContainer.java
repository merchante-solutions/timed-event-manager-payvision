package com.mes.ads;

import com.mes.support.MesAccountNumber;

public interface ManContainer extends Account
{
  public String getAccountNum();
  public void setAccountNum(String accountNum);
  public MesAccountNumber getMan();
  public void setMan(MesAccountNumber man);
}