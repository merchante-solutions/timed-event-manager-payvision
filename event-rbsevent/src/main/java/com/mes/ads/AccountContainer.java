package com.mes.ads;

public interface AccountContainer
{
  public Account getAccount();
  public void setAccount(Account acct);
  public long getAdsId();
  public AccountType getAccountType();
}