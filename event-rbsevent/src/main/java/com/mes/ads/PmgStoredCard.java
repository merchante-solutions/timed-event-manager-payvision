package com.mes.ads;

import com.mes.support.TridentTools;

public class PmgStoredCard
{
  private String profileId;
  private String merchNum;
  private String storeId;
  private String cardNum;
  private String expDate;
  private String cardName;

  public String getProfileId()
  {
    return profileId;
  }
  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  
  public String getMerchNum()
  {
    return merchNum;
  }
  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  
  public String getStoreId()
  {
    return storeId;
  }
  public void setStoreId(String storeId)
  {
    this.storeId = storeId;
  }
  
  public String getCardNum()
  {
    return cardNum;
  }
  public void setCardNum(String cardNum)
  {
    this.cardNum = cardNum;
  }
  public String getCardNumTrunc()
  {
    String truncated = null;
    try
    {
      truncated = 
        TridentTools.encodeCardNumber(cardNum);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    
    return truncated;
  }
  
  public String getExpDate()
  {
    return expDate;
  }
  public void setExpDate(String expDate)
  {
    this.expDate = expDate;
  }
  
  public String getCardName()
  {
    return cardName;
  }
  public void setCardName(String cardName)
  {
    this.cardName = cardName;
  }
  
  public String toString()
  {
    return "PmgStoredCard [ "
      + "profileId: " + profileId
      + ", merchNum: " + merchNum
      + ", storeId: " + storeId
      + ", cardNum: " + cardNum
      + ", expDate: " + expDate
      + ", cardName: " + cardName
      + " ]";
  }
}