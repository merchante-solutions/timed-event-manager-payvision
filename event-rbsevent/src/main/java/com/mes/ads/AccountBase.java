package com.mes.ads;

import java.sql.Timestamp;
import java.util.Date;
import com.mes.support.BaseModel;
import com.mes.support.MesCalendar;

public abstract class AccountBase implements Account
{
  protected long adsId;
  protected String testFlag;
  protected long stateId;
  protected String storeId;
  protected Date createDate;
  protected Date updateDate;
  protected String profileId;
  protected String merchNum;
  protected AccountType accountType;
  protected Date startDate;
  protected Date endDate;
  
  public long getAdsId()
  {
    return adsId;
  }
  public void setAdsId(long adsId)
  {
    this.adsId = adsId;
  }
  
  public void setCreateDate(Date createDate)
  {
    this.createDate = createDate;
  }
  public Date getCreateDate()
  {
    return createDate;
  }
  public void setCreateTs(Timestamp createTs)
  {
    createDate = MesCalendar.toDate(createTs);
  }
  public Timestamp getCreateTs()
  {
    return MesCalendar.toTimestamp(createDate);
  }
  
  public void setUpdateDate(Date updateDate)
  {
    this.updateDate = updateDate;
  }
  public Date getUpdateDate()
  {
    return updateDate;
  }
  public void setUpdateTs(Timestamp updateTs)
  {
    updateDate = MesCalendar.toDate(updateTs);
  }
  public Timestamp getUpdateTs()
  {
    return MesCalendar.toTimestamp(updateDate);
  }

  public void setStateId(long stateId)
  {
    this.stateId = stateId;
  }
  public long getStateId()
  {
    return stateId;
  }
    
  public String getStoreId()
  {
    return storeId;
  }
  public void setStoreId(String storeId)
  {
    this.storeId = storeId;
  }
  
  public Date getStartDate()
  {
    return startDate;
  }
  public void setStartTs(Timestamp startTs)
  {
    startDate = MesCalendar.toDate(startTs);
  }
  
  public Date getEndDate()
  {
    return endDate;
  }
  public void setEndTs(Timestamp endTs)
  {
    endDate = MesCalendar.toDate(endTs);
  }
  
  public String getProfileId()
  {
    return profileId;
  }
  public void setProfileId(String profileId)
  {
    this.profileId = profileId;
  }
  
  public String getMerchNum()
  {
    return merchNum;
  }
  public void setMerchNum(String merchNum)
  {
    this.merchNum = merchNum;
  }
  
  public void setTestFlag(String testFlag)
  {
    BaseModel.validateFlag(testFlag,"test");
    this.testFlag = testFlag;
  }
  public String getTestFlag()
  {
    return testFlag;
  }
  public boolean isTest()
  {
    return BaseModel.flagBoolean(testFlag);
  }
  
  protected String baseToString()
  {
    return 
      "adsId: " + adsId
      + ", createDate: " + createDate
      + ", updateDate: " + updateDate
      + ", stateId: " + stateId
      + ", storeId: " + (storeId != null ? storeId : "--")
      + ", startDate: " + startDate
      + ", endDate: " + (endDate != null ? ""+endDate : "--")
      + ", profileId: " + profileId
      + ", merchNum: " + merchNum
      + ", accountType: " + getAccountType()
      + ", isTest: " + isTest();
  }
  
  public String toString()
  {
    return getClass().getName() + " [ "
      + baseToString()
      + " ]";
  }
}