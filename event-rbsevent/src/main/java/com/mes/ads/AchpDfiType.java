package com.mes.ads;

import com.mes.support.CodeType;

public class AchpDfiType extends CodeType
{
  public static AchpDfiType CHECKING  = new AchpDfiType("C");
  public static AchpDfiType SAVINGS   = new AchpDfiType("S");

  static
  {
    codeHash.put(""+CHECKING,CHECKING);
    codeHash.put(""+SAVINGS,SAVINGS);
  };

  private AchpDfiType(String code)
  {
    super(code);
  }

  public static AchpDfiType forType(String forStr, AchpDfiType dflt)
  {
    return (AchpDfiType)forCode(forStr,dflt);
  }

  public static AchpDfiType forType(String forStr)
  {
    return (AchpDfiType)forCode(forStr);
  }
}
   
