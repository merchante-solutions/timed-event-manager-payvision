package com.mes.ads;

import org.apache.log4j.Logger;
import com.mes.support.MesAccountNumber;

public class AchpAccount extends AccountBase implements ManContainer
{
  static Logger log = Logger.getLogger(AchpAccount.class);
  
  private MesAccountNumber man = new MesAccountNumber();
  private AchpDfiType dfiType;
  private String transitNum;
  private String customerName;
  private String customerId;
  private String secCode;
  private String ipAddress;

  public AccountType getAccountType()
  {
    return AccountType.ACHP;
  }
    
  public String getAccountNum()
  {
    return man != null ? man.clearText() : null;
  }
  public void setAccountNum(String accountNum)
  {
    man.setData(accountNum.getBytes());
  }
  public MesAccountNumber getMan()
  {
    return man;
  }
  public void setMan(MesAccountNumber man)
  {
    this.man = man;
  }
  
  public AchpDfiType getDfiType()
  {
    return dfiType;
  }
  public void setDfiType(AchpDfiType dfiType)
  {
    this.dfiType = dfiType;
  }
  
  public String getTransitNum()
  {
    return transitNum;
  }
  public void setTransitNum(String transitNum)
  {
    this.transitNum = transitNum;
  }
  
  public String getCustomerName()
  {
    return customerName;
  }
  public void setCustomerName(String customerName)
  {
    this.customerName = customerName;
  }
  
  public String getCustomerId()
  {
    return customerId;
  }
  public void setCustomerId(String customerId)
  {
    this.customerId = customerId;
  }
  
  public String getSecCode()
  {
    return secCode;
  }
  public void setSecCode(String secCode)
  {
    this.secCode = secCode;
  }
  
  public String getIpAddress()
  {
    return ipAddress;
  }
  public void setIpAddress(String ipAddress)
  {
    this.ipAddress = ipAddress;
  }
  
  public String toString()
  {
    return "AchpAccount [ "
      + baseToString()
      + ", accountNum: " + getAccountNum()
      + ", transitNum: " + transitNum
      + ", dfiType: " + dfiType
      + ", customerName: " + customerName
      + ", customerId: " + customerId
      + ", secCode: " + secCode
      + ", ipAddress: " + ipAddress
      + " ]";
  }
}