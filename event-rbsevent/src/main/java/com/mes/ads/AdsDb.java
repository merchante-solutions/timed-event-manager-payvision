package com.mes.ads;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.safehaus.uuid.UUID;
import org.safehaus.uuid.UUIDGenerator;
import com.mes.database.BaseDb;
import com.mes.database.DbcType;

public class AdsDb extends BaseDb
{
  static Logger log = Logger.getLogger(AdsDb.class);

  public AdsDb(DbcType dbc)
  {
    super(dbc.equals(DbcType.DIRECT),"ads_id_seq","ads_config");
  }
  public AdsDb()
  {
    this(DbcType.POOL);
  }
  
  /**
   * Method template.
   */
  public void skeleton(String id)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
    }
    catch (Exception e)
    {
      handleException("skeleton(id=" + id + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
  }
  
  /**
   * BASE ACCOUNT FUNCTIONALITY
   */
  
  /**
   * Loads data common to all accounts from result set into account object.
   */
  private void loadBaseAccount(Account acct, ResultSet rs) throws Exception
  {
    acct.setAdsId       (rs.getLong     ("ads_id"     ));
    acct.setCreateTs    (rs.getTimestamp("create_ts"  ));
    acct.setUpdateTs    (rs.getTimestamp("update_ts"  ));
    acct.setStateId     (rs.getLong     ("state_id"   ));
    acct.setStartTs     (rs.getTimestamp("start_ts"   ));
    acct.setEndTs       (rs.getTimestamp("end_ts"     ));
    acct.setProfileId   (rs.getString   ("profile_id" ));
    acct.setMerchNum    (rs.getString   ("merch_num"  ));
    acct.setTestFlag    (rs.getString   ("test_flag"  ));
  }
  
  /**
   * Inserts base account record, returns new ads id generated for record.
   * This method intended to be called as part of transaction, it does not
   * handle exceptions.
   */
  private long insertBaseAccount(Account acct) throws Exception
  {
    CallableStatement cs = null;
    
    try
    {
      // base account record, returning ads_id
      cs = connectCallable(
        " begin             " +
        " insert into       " +
        " acct_data_store   " +
        " ( profile_id,     " +
        "   merch_num,      " +
        "   ads_type,       " +
        "   test_flag )     " +
        " values            " +
        " ( ?, ?, ?, ? )    " +
        " returning ads_id  " +
        "  into ?; end;     ");

      int colNum = 1;
      cs.setString(colNum++,acct.getProfileId());
      cs.setString(colNum++,acct.getMerchNum());
      cs.setString(colNum++,""+acct.getAccountType());
      cs.setString(colNum++,acct.getTestFlag());
      cs.registerOutParameter(colNum,java.sql.Types.INTEGER);
      cs.executeUpdate();
      return cs.getLong(colNum);
    }
    finally
    {
      cleanUp(cs);
    }
  }
  
  /**
   * Updates existing base account record.  Intended for use in
   * transactions, no exception handling.
   */
  private void updateBaseAccount(Account acct) throws Exception
  {
    PreparedStatement ps = null;
    
    try
    {
      // base account record, returning ads_id
      ps = connectPrepared(
        " update            " +
        "  acct_data_store  " +
        " set               " +
        "  profile_id = ?,  " +
        "  merch_num = ?,   " +
        "  test_flag = ?    " +
        " where             " +
        "  ads_id = ?       ");  

      int colNum = 1;
      ps.setString(colNum++,acct.getProfileId());
      ps.setString(colNum++,acct.getMerchNum());
      ps.setString(colNum++,acct.getTestFlag());
      ps.setLong  (colNum++,acct.getAdsId());
      ps.executeUpdate();
    }
    finally
    {
      cleanUp(ps);
    }
  }
  
  /**
   * CREDIT CARD ACCOUNTS
   */
   
  /**
   * Creates a credit card account object and loads it from the given
   * result set.
   */
  private CcAccount createCcAccount(ResultSet rs) throws Exception
  {
    CcAccount acct = new CcAccount();
    
    // load base account data
    loadBaseAccount(acct,rs);
    
    // load cc data
    acct.setAccountNum(rs.getString   ("acct_num_decoded" ));
    acct.setExpDate   (rs.getString   ("exp_date"         ));
    acct.setCardName  (rs.getString   ("card_name"        ));
    acct.setAvsStreet (rs.getString   ("avs_street"       ));
    acct.setAvsZip    (rs.getString   ("avs_zip"          ));
    acct.setCardId    (rs.getString   ("card_id"          ));
    
    return acct;
  }
  
  public static final String CC_COLUMNS =
    " cc.state_id,                            " +
    " cc.start_ts,                            " +
    " cc.end_ts,                              " +
    " nvl(dukpt_decrypt_wrapper(              " +
    "    cc.acct_num_enc),'40000000000000000')" +
    "      acct_num_decoded,                  " +
    " nvl(cc.card_type,'VISA') card_type,     " +
    " nvl(cc.exp_date,'0000') exp_date,       " +
    " cc.card_name,                           " +
    " cc.avs_street,                          " +
    " cc.avs_zip,                             " +
    " cc.card_id                              ";
  

  /**
   * Fetches credit card account data from the data store for the given id
   * and returns a CcAccount object containing the data.
   */
  public CcAccount getCcAccount(long adsId)
  {
    Statement s = null;
    
    try
    {
      s = connectStatement();
      
      String qs = 
        " select            " +
        "  ads.ads_id,      " +
        "  ads.create_ts,   " +
        "  ads.update_ts,   " +
        "  ads.profile_id,  " +
        "  ads.merch_num,   " +
        "  ads.test_flag,   " +
        CC_COLUMNS +
        " from  " +
        "  acct_data_store          ads,  " +
        "  acct_data_store_cc       cc    " +
        " where " +
        "  ads.ads_id = " + adsId +
        "  and ads.ads_id = cc.ads_id      " +
        "  and ads.state_id = cc.state_id  ";
      log.debug("getCcAccount start " + Calendar.getInstance() + ": \n" + qs);
      ResultSet rs = s.executeQuery(qs);
      log.debug("getCcAccount done " + Calendar.getInstance());
      if (rs.next())
      {
        return createCcAccount(rs);
      }
    }
    catch (Exception e)
    {
      handleException("getCcAccount(adsId=" + adsId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    
    return null;
  }
  
  /**
   * Inserts new cc account state record.
   * 
   * Used in transactions, no exception handling.
   */
  private void insertCcAccountState(CcAccount acct) throws Exception
  {
    PreparedStatement ps = null;
    try
    {
      ps = con.prepareStatement(
        " insert into         " +
        "  acct_data_store_cc " +
        " ( ads_id,           " +
        "   acct_num,         " +
        "   acct_num_enc,     " +
        "   card_type,        " +
        "   exp_date,         " +
        "   card_name,        " +
        "   avs_street,       " +
        "   avs_zip,          " +
        "   card_id )         " +
        " values              " +
        " ( ?, ?, ?, ?, ?,    " +
        "   ?, ?, ?, ? )      ");
      int colNum = 1;
      ps.setLong  (colNum++,acct.getAdsId());
      ps.setString(colNum++,acct.getMan().truncated());
      ps.setBytes (colNum++,acct.getMan().getEncodedData());
      String cardType = acct.getCardType() != null ? ""+acct.getCardType() : null;
      ps.setString(colNum++,cardType);
      ps.setString(colNum++,acct.getExpDate());
      ps.setString(colNum++,acct.getCardName());
      ps.setString(colNum++,acct.getAvsStreet());
      ps.setString(colNum++,acct.getAvsZip());
      ps.setString(colNum++,acct.getCardId());
      ps.executeUpdate();
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
    }
  }
  
  /**
   * Inserts new credit card account data.  Returns updated account object.
   */
  public CcAccount insertCcAccount(CcAccount acct)
  {
    PreparedStatement ps = null;
    boolean opSuccess = false;
    
    try
    {
      startTransaction();
      
      // insert base account data, store id
      acct.setAdsId(insertBaseAccount(acct));
      
      // generate new cc account state rec
      insertCcAccountState(acct);

      // reload account to get auto generated data
      acct = getCcAccount(acct.getAdsId());
      
      // signal commit
      opSuccess = true;

      return acct;
    }
    catch (Exception e)
    {
      handleException("insertCcAccount(" + acct + ")",e);
    }
    finally
    {
      cleanUp(ps);
      if (opSuccess) { commitTransaction(); } else { rollbackTransaction(); }
    }
    
    return null;
  }
  
  /**
   * Updates existing credit card account data.  Returns updated object.
   */
  public CcAccount updateCcAccount(CcAccount acct)
  {
    PreparedStatement ps = null;
    boolean opSuccess = false;
    
    try
    {
      startTransaction();
      
      // update base account data
      updateBaseAccount(acct);
      
      // generate new cc account state rec
      insertCcAccountState(acct);

      // reload account to get auto generated data
      acct = getCcAccount(acct.getAdsId());
      
      // signal commit
      opSuccess = true;

      return acct;
    }
    catch (Exception e)
    {
      handleException("updateCcAccount(" + acct + ")",e);
    }
    finally
    {
      cleanUp(ps);
      if (opSuccess) { commitTransaction(); } else { rollbackTransaction(); }
    }
    
    return null;
  }
  
  /**
   * ACHP ACCOUNTS
   */

  /**
   * Creates an achp account object and loads it from the given
   * result set.
   */
  private AchpAccount createAchpAccount(ResultSet rs) throws Exception
  {
    AchpAccount acct = new AchpAccount();
    
    // load base account data
    loadBaseAccount(acct,rs);
    
    // load achp data
    acct.setAccountNum  (rs.getString   ("acct_num_decoded" ));
    acct.setDfiType     (AchpDfiType.forType(
                         rs.getString   ("dfi_type"         )));
    acct.setTransitNum  (rs.getString   ("transit_num"      ));
    acct.setCustomerName(rs.getString   ("cust_name"        ));
    acct.setCustomerId  (rs.getString   ("cust_id"          ));
    acct.setSecCode     (rs.getString   ("sec_code"         ));
    acct.setIpAddress   (rs.getString   ("ip_address"       ));
    
    return acct;
  }
  
  public static final String ACHP_COLUMNS =
    " achp.state_id,          " +
    " achp.start_ts,          " +
    " achp.end_ts,            " +
    " dukpt_decrypt_wrapper(  " +
    "  achp.acct_num_enc)     " +
    "   acct_num_decoded,     " +
    " achp.dfi_type,          " +
    " achp.transit_num,       " +
    " achp.cust_name,         " +
    " achp.cust_id,           " +
    " achp.sec_code,          " +
    " achp.ip_address         ";
  
  
  /**
   * Fetches achp account data from the data store for the given id.
   */
  public AchpAccount getAchpAccount(long adsId)
  {
    Statement s = null;
    
    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select            " +
        "  ads.ads_id,      " +
        "  ads.create_ts,   " +
        "  ads.update_ts,   " +
        "  ads.profile_id,  " +
        "  ads.merch_num,   " +
        "  ads.test_flag,   " +
        ACHP_COLUMNS +
        " from  " +
        "  acct_data_store        ads,  " +
        "  acct_data_store_achp   achp  " +
        " where " +
        "  ads.ads_id = " + adsId +
        "  and ads.ads_id = achp.ads_id      " +
        "  and ads.state_id = achp.state_id  ");
      if (rs.next())
      {
        return createAchpAccount(rs);
      }
    }
    catch (Exception e)
    {
      handleException("getAchpAccount(adsId=" + adsId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    
    return null;
  }
  
  /**
   * Inserts new achp account state record.
   * 
   * Used in transactions, no exception handling.
   */
  private void insertAchpAccountState(AchpAccount acct) throws Exception
  {
    PreparedStatement ps = null;
    try
    {
      ps = con.prepareStatement(
        " insert into           " +
        "  acct_data_store_achp " +
        " ( ads_id,             " +
        "   acct_num,           " +
        "   acct_num_enc,       " +
        "   dfi_type,           " +
        "   transit_num,        " +
        "   cust_name,          " +
        "   cust_id,            " +
        "   sec_code,           " +
        "   ip_address )        " +
        " values                " +
        " ( ?, ?, ?, ?, ?,      " +
        "   ?, ?, ?, ? )        ");
      int colNum = 1;
      ps.setLong  (colNum++,acct.getAdsId());
      ps.setString(colNum++,acct.getMan().truncated());
      ps.setBytes (colNum++,acct.getMan().getEncodedData());
      ps.setString(colNum++,""+acct.getDfiType());
      ps.setString(colNum++,acct.getTransitNum());
      ps.setString(colNum++,acct.getCustomerName());
      ps.setString(colNum++,acct.getCustomerId());
      ps.setString(colNum++,acct.getSecCode());
      ps.setString(colNum++,acct.getIpAddress());
      ps.executeUpdate();
    }
    finally
    {
      try { ps.close(); } catch (Exception e) { }
    }
  }
  
  /**
   * Inserts new achp account data.
   */
  public AchpAccount insertAchpAccount(AchpAccount acct)
  {
    PreparedStatement ps = null;
    boolean opSuccess = false;
    
    try
    {
      startTransaction();
      
      // insert base account data, store id
      acct.setAdsId(insertBaseAccount(acct));
      
      // generate new achp account state rec
      insertAchpAccountState(acct);

      // reload account to get auto generated data
      acct = getAchpAccount(acct.getAdsId());
      
      // signal commit
      opSuccess = true;

      return acct;
    }
    catch (Exception e)
    {
      handleException("insertAchpAccount(" + acct + ")",e);
    }
    finally
    {
      cleanUp(ps);
      if (opSuccess) { commitTransaction(); } else { rollbackTransaction(); }
    }
    
    return null;
  }

  /**
   * Updates existing achp account data.  Returns updated object.
   * actually inserts new state record.
   */
  public AchpAccount updateAchpAccount(AchpAccount acct)
  {
    PreparedStatement ps = null;
    boolean opSuccess = false;
    
    try
    {
      startTransaction();
      
      // update base account data
      updateBaseAccount(acct);
      
      // generate new achp account state rec
      insertAchpAccountState(acct);

      // reload account to get auto generated data
      acct = getAchpAccount(acct.getAdsId());
      
      // signal commit
      opSuccess = true;

      return acct;
    }
    catch (Exception e)
    {
      handleException("updateAchpAccount(" + acct + ")",e);
    }
    finally
    {
      cleanUp(ps);
      if (opSuccess) { commitTransaction(); } else { rollbackTransaction(); }
    }
    
    return null;
  }
  
  /**
   * ROUTING
   */

  public Account insertAccount(Account acct)
  {
    if (acct instanceof AchpAccount)
    {
      return insertAchpAccount((AchpAccount)acct);
    }
    else if (acct instanceof CcAccount)
    {
      return insertCcAccount((CcAccount)acct);
    }
    throw new ClassCastException("Unsupported account class: " 
      + acct.getClass().getName());
  }
  
  public Account updateAccount(Account acct)
  {
    if (acct instanceof AchpAccount)
    {
      return updateAchpAccount((AchpAccount)acct);
    }
    else if (acct instanceof CcAccount)
    {
      return updateCcAccount((CcAccount)acct);
    }
    throw new ClassCastException("Unsupported account class: " 
      + acct.getClass().getName());
  }
  
  public Account getAccount(long adsId, AccountType acctType)
  {
    if (AccountType.CC.equals(acctType))
    {
      return getCcAccount(adsId);
    }
    else if (AccountType.ACHP.equals(acctType))
    {
      return getAchpAccount(adsId);
    }
    throw new IllegalArgumentException("Unsupported account type: " 
      + acctType);
  }
  
  /**
   * CONTAINERS
   */

  /**
   * Fetch account for account container, load it into the container.
   */
  public void loadContainer(AccountContainer ac)
  {
    ac.setAccount(getAccount(ac.getAdsId(),ac.getAccountType()));
  }
  

  /**
   * Generates list of accounts of the given type using the
   * query string provided.
   */
  public List getAccounts(String qs, AccountType at)
  {
    Statement s = null;
    
    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(qs);
      List accounts = new ArrayList();
      while (rs.next())
      {
        if (AccountType.CC.equals(at))
        {
          Account a = createCcAccount(rs);
          //System.out.println("got account: " + a);
          accounts.add(a);
        }
        else if (AccountType.ACHP.equals(at))
        {
          accounts.add(createAchpAccount(rs));
        }
        else
        {
          throw new RuntimeException("Invalid accountType: " + at);
        }
      }
      return accounts;
    }
    catch (Exception e)
    {
      handleException("getAccounts(at=" + at + "\nqs=" + qs + "\n)",e);
    }
    finally
    {
      cleanUp(s);
    }
    
    return null;
  }

  /**
   * Generates a query to fetch accounts of the given type.  inStr 
   * needs to contain a String of comma-separated adsId values that
   * can be uses as the values in an SQL IN clause.
   */
  private String generateAccountQs(AccountType at, String inStr)
  {
    // base select columns
    StringBuffer qs = new StringBuffer();
    qs.append(
        " select            " +
        "  ads.ads_id,      " +
        "  ads.create_ts,   " +
        "  ads.update_ts,   " +
        "  ads.profile_id,  " +
        "  ads.merch_num,   " +
        "  ads.test_flag,   ");

    // credit card 
    if (AccountType.CC.equals(at))
    {
      qs.append(CC_COLUMNS);
      qs.append(
        " from                              " +
        "  acct_data_store          ads,    " +
        "  acct_data_store_cc       cc      " +
        " where                             " +
        "  ads.ads_id = cc.ads_id           " +
        "  and ads.state_id = cc.state_id   ");
    }
    // achp
    else if (AccountType.ACHP.equals(at))
    {
      qs.append(ACHP_COLUMNS);
      qs.append(
        " from  " +
        "  acct_data_store      ads,  " +
        "  acct_data_store_achp achp  " +
        " where " +
        "  ads.ads_id = achp.ads_id          " +
        "  and ads.state_id = achp.state_id  ");
    }
    else
    {
      throw new RuntimeException("Unsupported account type: " + at);
    }

    // in clause
    qs.append(" and ads.ads_id in ( " + inStr + " ) \n");
    
    return ""+qs;
  }
  
  /**
   * Takes a mapping of adsId's to containers and an account type.
   * Generates a query to load the account data based on the account type
   * and processes result set into accounts that are inserted into
   * each matching container.
   */
  private void loadMappedContainers_old(Map acMap, AccountType at)
  {
    StringBuffer inBuf = new StringBuffer();
    int inCount = 0;
    for (Iterator i = acMap.keySet().iterator(); i.hasNext();)
    {
      String idStr = ""+i.next();
      inBuf.append((inCount > 0 ? "," : "") + idStr);
      ++inCount;
      if (inCount >= 1000 || !i.hasNext())
      {
        String qs = generateAccountQs(at,""+inBuf);
        //System.out.println("getAccounts start " + Calendar.getInstance().getTime() + ": \n" + qs);
        List accounts = getAccounts(qs,at);
        //System.out.println("getAccounts done " + Calendar.getInstance().getTime()); 
        for (Iterator j = accounts.iterator(); j.hasNext();)
        {
          Account account = (Account)j.next();
          AccountContainer ac =   
            (AccountContainer)acMap.get(""+account.getAdsId());
          ac.setAccount(account);
          //System.out.println("assigned account: " + account + "\nto container: " + ac);
        }
        inCount = 0;
        inBuf = new StringBuffer();
      }
    }
  }
  
  /**
   * Fetch accounts for containers, load them into containers.
   */
  public void loadContainers_old(List containers)
  {
    // sort by account type into maps
    Map ccMap = new HashMap();
    Map achpMap = new HashMap();
    for (Iterator i = containers.iterator(); i.hasNext();)
    {
      AccountContainer ac = (AccountContainer)i.next();
      if (AccountType.CC.equals(ac.getAccountType()))
      {
        ccMap.put(""+ac.getAdsId(),ac);
      }
      else if (AccountType.ACHP.equals(ac.getAccountType()))
      {
        achpMap.put(""+ac.getAdsId(),ac);
      }
      else
      {
        throw new RuntimeException("Unuspported account type: " 
          + ac.getAccountType());
      }
    }
    
    // load credit cards
    if (!ccMap.isEmpty())
    {
      loadMappedContainers(ccMap,AccountType.CC);
    }
    
    // load achp accounts
    if (!achpMap.isEmpty())
    {
      loadMappedContainers(achpMap,AccountType.ACHP);
    }
  }
  
  private void loadMappedContainers(Map acMap, AccountType at)
  {
    StringBuffer inBuf = new StringBuffer();
    int inCount = 0;
    for (Iterator i = acMap.keySet().iterator(); i.hasNext();)
    {
      String idStr = ""+i.next();
      inBuf.append((inCount > 0 ? "," : "") + idStr);
      ++inCount;
      if (inCount >= 1000 || !i.hasNext())
      {
        String qs = generateAccountQs(at,""+inBuf);
        List accounts = getAccounts(qs,at);
        for (Iterator j = accounts.iterator(); j.hasNext();)
        {
          Account account = (Account)j.next();
          Object mapObj = acMap.get(""+account.getAdsId());
          if (mapObj instanceof AccountContainer)
          {
            ((AccountContainer)mapObj).setAccount(account);
          }
          else if (mapObj instanceof List)
          {
            List l = (List)mapObj;
            for (Iterator k = l.iterator(); k.hasNext();)
            {
              ((AccountContainer)k.next()).setAccount(account);
            }
          }
        }
        inCount = 0;
        inBuf = new StringBuffer();
      }
    }
  }
  
  public void loadContainers(List containers)
  {
    // sort by account type into maps
    Map ccMap = new HashMap();
    Map achpMap = new HashMap();
    for (Iterator i = containers.iterator(); i.hasNext();)
    {
      AccountContainer ac = (AccountContainer)i.next();
      if (AccountType.CC.equals(ac.getAccountType()))
      {
        Object acDup = ccMap.get(""+ac.getAdsId());
        if (acDup == null)
        {
          ccMap.put(""+ac.getAdsId(),ac);
        }
        else if (acDup instanceof List)
        {
          List acList = (List)acDup;
          acList.add(ac);
        }
        else
        {
          List acList = new ArrayList();
          acList.add(acDup);
          acList.add(ac);
          ccMap.put(""+ac.getAdsId(),acList);
        }
      }
      else if (AccountType.ACHP.equals(ac.getAccountType()))
      {
        Object acDup = achpMap.get(""+ac.getAdsId());
        if (acDup == null)
        {
          achpMap.put(""+ac.getAdsId(),ac);
        }
        else if (acDup instanceof List)
        {
          List acList = (List)acDup;
          acList.add(ac);
        }
        else
        {
          List acList = new ArrayList();
          acList.add(acDup);
          acList.add(ac);
          achpMap.put(""+ac.getAdsId(),acList);
        }
      }
      else
      {
        throw new RuntimeException("Unuspported account type: " 
          + ac.getAccountType());
      }
    }
    
    // load credit cards
    if (!ccMap.isEmpty())
    {
      loadMappedContainers(ccMap,AccountType.CC);
    }
    
    // load achp accounts
    if (!achpMap.isEmpty())
    {
      loadMappedContainers(achpMap,AccountType.ACHP);
    }
    
  }
  
  /**
   * STORED CARD DATA
   */
   
  public static final String CARD_STORE_TABLE = "trident_api_card_data";
   
  /**
   * Create PmgStoredCard from result set.
   */
  private PmgStoredCard createPmgStoredCard(ResultSet rs) throws Exception
  {
    PmgStoredCard card = new PmgStoredCard();
    card.setProfileId (rs.getString("terminal_id"     ));
    card.setMerchNum  (rs.getString("merchant_number" ));
    card.setStoreId   (rs.getString("card_id"         ));
    card.setExpDate   (rs.getString("exp_date"        ));
    card.setCardNum   (rs.getString("card_num"        ));
    card.setCardName  (rs.getString("cardholder_name" ));
    return card;
  }

  /**
   * Fetch stored card data with a given store id and profile id.
   */
  public PmgStoredCard getPmgStoredCard(String profileId, String storeId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      String qs =
        " select                              " +
        "  terminal_id,                       " +
        "  merchant_number,                   " +
        "  card_id,                           " +
        "  exp_date,                          " +
        "  dukpt_decrypt_wrapper(             " +
        "   card_number_enc) card_num,        " +
        "  cardholder_name                    " +
        " from " + CARD_STORE_TABLE + "       " +
        " where                               " +
        "  terminal_id = '" + profileId + "'  " +
        "  and card_id = '" + storeId + "'    ";
      log.debug("stored card qs:\n" + qs);
      ResultSet rs = s.executeQuery(qs);
      if (rs.next())
      {
        return createPmgStoredCard(rs);
      }
    }
    catch (Exception e)
    {
      handleException("getPmgStoredCard(profileId=" + profileId + ", storeId=" 
      + storeId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return null;
  }
  
  /**
   * Fetch stored card data with a given store id.
   */
  public PmgStoredCard getPmgStoredCard(String storeId)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select                        " +
        "  terminal_id,                 " +
        "  merchant_number,             " +
        "  card_id,                     " +
        "  exp_date,                    " +
        "  dukpt_decrypt_wrapper(       " +
        "   card_number_enc) card_num,  " +
        "  cardholder_name              " +
        " from " + CARD_STORE_TABLE + " " +
        " where                         " +
        "  card_id = '" + storeId + "'  ");
      if (rs.next())
      {
        return createPmgStoredCard(rs);
      }
    }
    catch (Exception e)
    {
      handleException("getPmgStoredCard(storeId=" + storeId + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return null;
  }
  
  /**
   * Fetch stored card data with card number and profile id.  Used to
   * avoid creating duplicate card numbers under a profile in the
   * stored card table.
   */
  public PmgStoredCard findPmgStoredCard(String profileId, String cardNum)
  {
    Statement s = null;

    try
    {
      s = connectStatement();
      ResultSet rs = s.executeQuery(
        " select *                                " +
        " from                                    " +
        " ( select                                " +
        "    terminal_id,                         " +
        "    merchant_number,                     " +
        "    card_id,                             " +
        "    exp_date,                            " +
        "    dukpt_decrypt_wrapper(               " +
        "      card_number_enc) card_num,         " +
        "    cardholder_name                      " +
        "   from " + CARD_STORE_TABLE + "         " +
        "   where                                 " +
        "    terminal_id = '" + profileId + "' )  " +
        " where                                   " +
        "  card_num = '" + cardNum + "'           ");
      if (rs.next())
      {
        return createPmgStoredCard(rs);
      }
    }
    catch (Exception e)
    {
      handleException("findPmgStoredCard(profileId=" + profileId 
      + ", cardNum=" + cardNum + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return null;
  }

  /**
   * Generates new stored card id.  This is 
   */
  private String getNewStoreId(PmgStoredCard psc)
  {
    UUID uuid = 
      UUIDGenerator.getInstance()
        .generateNameBasedUUID(null,
          "ads-" + psc + System.currentTimeMillis());
    return uuid.toString().replaceAll("-","");
  }
  
  /**
   * Inserts PMG stored card record.  Returns the stored card record.
   */
  public PmgStoredCard insertPmgStoredCard(PmgStoredCard psc) 
  {
    Statement s = null;
    
    try
    {
      // generate stored card id
      String storeId = getNewStoreId(psc);
      
      // insert new card record
      s = connectStatement();
      s.executeUpdate(
        " insert into                     " +
        "  " + CARD_STORE_TABLE + "       " +
        " ( terminal_id,                  " +
        "   merchant_number,              " +
        "   card_id,                      " +
        "   card_number,                  " +
        "   exp_date,                     " +
        "   cardholder_name )             " +
        " select                          " +
        "  '" + psc.getProfileId() + "',  " +
        "  merchant_number,               " +
        "  '" + storeId + "',             " +
        "  '" + psc.getCardNum() + "',    " +
        "  '" + psc.getExpDate() + "',    " +
        "  '" + psc.getCardName() + "'    " +
        " from trident_profile where      " +
        "  terminal_id = '" + psc.getProfileId() + "'");
      return getPmgStoredCard(storeId);
    }
    catch (Exception e)
    {
      handleException("insertPmgStoredCard(psc=" + psc + ")",e);
    }
    finally
    {
      cleanUp(s);
    }
    return psc;
  }
}