package com.mes.ads;

import com.mes.support.CodeType;

public class AccountType extends CodeType
{
  public static AccountType CC    = new AccountType("CC","Credit Card");
  public static AccountType ACHP  = new AccountType("ACHP","ACH Payment");

  static
  {
    codeHash.put(""+CC,CC);
    codeHash.put(""+ACHP,ACHP);
  };

  private AccountType(String code, String description)
  {
    super(code,description);
  }

  public static AccountType forType(String forStr, AccountType dflt)
  {
    return (AccountType)forCode(forStr,dflt);
  }

  public static AccountType forType(String forStr)
  {
    return (AccountType)forCode(forStr);
  }
}
   
