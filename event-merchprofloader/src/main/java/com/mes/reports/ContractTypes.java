/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ContractTypes.java $

  Description:  


  Last Modified By   : $Author: mjanbay $
  Last Modified Date : $Date: 2014-07-24 16:08:43 -0700 (Thu, 24 Jul 2014) $
  Version            : $Revision: 22922 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.Vector;
import com.mes.support.MesMath;

public class ContractTypes
{
  public static final int       CONTRACT_SOURCE_INVALID           = -1;   //
  public static final int       CONTRACT_SOURCE_RESELLER          =  0;   //
  public static final int       CONTRACT_SOURCE_LIABILITY         =  1;   //
  public static final int       CONTRACT_SOURCE_REFERRAL          =  2;   //
  public static final int       CONTRACT_SOURCE_VITAL             =  3;   //
  public static final int       CONTRACT_SOURCE_THIRD_PARTY       =  4;   //
  public static final int       CONTRACT_SOURCE_GRIN              =  5;   //
  public static final int       CONTRACT_SOURCE_COUNT             =  6;   // must be last
  public static final int       CONTRACT_SOURCE_ALL               =  CONTRACT_SOURCE_COUNT;  

  public static final int       CONTRACT_TYPE_INCOME              = 0;
  public static final int       CONTRACT_TYPE_EXPENSE             = 1;
  public static final int       CONTRACT_TYPE_COUNT               = 2;
  
  public static final int       CONTRACT_DATA_MERCH_DIRECT        = 0;
  public static final int       CONTRACT_DATA_MES                 = 1;
  public static final int       CONTRACT_DATA_COUNT               = 2;

  public static class TsysSysGenerateIncome
  {
    public String              StatementDesc    =  null;
    public double              StatementFee     = 0.0;
    
    public TsysSysGenerateIncome( ResultSet resultSet ) throws java.sql.SQLException
    {
      StatementDesc  = resultSet.getString("st_statement_desc");
      StatementFee   = resultSet.getDouble("st_fee_amount");
    }
  }

  public static class ChargeRecordRevenue
  {
    public String              ChargeRecMsg    =  null;
    public double              Revenue         = 0.0;
    
    public ChargeRecordRevenue( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ChargeRecMsg  = resultSet.getString("statement_msg");
      Revenue       = resultSet.getDouble("revenue");
    }
  }
  
  public static class RevenueMaskItem
  {
    public static final int   MT_PARTNERSHIP        = 1;
    public static final int   MT_AGENT_BANK         = 2;
    
    public      String              ChargeDescPrefix;
    public      int                 GroupId;
    private     int                 MaskType;
    
    public RevenueMaskItem( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      ChargeDescPrefix  = resultSet.getString("charge_rec_prefix");
      GroupId           = resultSet.getInt("billing_cat");
      MaskType          = resultSet.getInt("mask_type");
    }
    
    public int getMaskType( )
    {
      return( MaskType );
    }
    
    public boolean isAgentBankMask( )
    {
      return( MaskType == MT_AGENT_BANK );
    }
    
    public boolean isGroupMask( )
    {
      return( ChargeDescPrefix == null );
    }
    
    public boolean isPartnershipMask( )
    {
      return( MaskType == MT_PARTNERSHIP );
    }
  }
  
  public static class PerItemRecord
  {
    public Date         ActiveDate      = null;
    public int          BetType         = 0;
    public int          Count           = 0;
    public int          ContractSource  = CONTRACT_SOURCE_INVALID;
    public String       Description     = null;
    public boolean      ExcludeFromProf = false;
    public int          ExemptItemCount = 0;
    public double       PerItemRate     = 0.0;
    
    public PerItemRecord( Date activeDate, int contractSrc, int betType )
    {
      initPerItem( activeDate, contractSrc, betType, 0, 0.0, 0 );
    }
    
    public PerItemRecord( Date activeDate, int contractSrc, int betType, int count, double perItemRate )
    {
      initPerItem( activeDate, contractSrc, betType, count, perItemRate, 0 );
    }
    
    public PerItemRecord( Date activeDate, int contractSrc, int betType, int count, double perItemRate, int exemptCount )
    {
      initPerItem( activeDate, contractSrc, betType, count, perItemRate, exemptCount );
    }
    
    public double getAmount( )
    {
      return( getPerItemAmount() );
    }
    
    public int getBetType( )
    {
      return( BetType );
    }
    
    public int getContractSource( )
    {
      return( ContractSource );
    }
    
    public String getDescription( )
    {
      return( Description );
    }
    
    public double getExcludedAmount( )
    {
      double    retVal      = 0.0;
      
      if ( ExcludeFromProf == true )
      {
        // exempt amount is already negative
        retVal = ( getAmount() + getExemptItemAmount() );
      }
      return( retVal );
    }
    
    public double getExemptItemAmount( )
    {
      return( -1 * PerItemRate * Math.min( ExemptItemCount, Count ) );
    }
    
    public int getExemptItemCount( )
    {
      return( Math.min( ExemptItemCount, Count ) );
    }
    
    public double getNetAmount( )
    {
      double    retVal      = 0.0;
      
      if ( ExcludeFromProf == false )
      {
        // exempt amount is already negative
        retVal = ( getAmount() + getExemptItemAmount() );
      }
      return( retVal );        
    }
    
    public double getPerItemAmount( )
    {
      return( MesMath.round( (PerItemRate * Math.abs( Count )), 2 ) );
    }
    
    public int getPerItemCount( )
    {
      return( Math.abs( Count ) );
    }
    
    public double getPerItemRate( )
    {
      return( PerItemRate );
    }
    
    private final void initPerItem( Date activeDate, int contractSrc, int betType, int count, double perItemRate, int exemptCount )
    {
      ActiveDate      = activeDate;
      BetType         = betType;
      ContractSource  = contractSrc;
      Count           = count;  
      ExemptItemCount = exemptCount;
      PerItemRate     = perItemRate;
    }
    
    public boolean isExcluded( )
    {
      return( ExcludeFromProf );
    }
    
    public boolean isSummaryRecord( )
    {
      // summary records are indicated
      // by a negative item count
      return( ( this.Count < 0 ) ? true : false );
    }
    
    public void setDescription( String customDesc )
    {
      Description = customDesc;
    }
    
    public void setExemptItemCount( int newCount )
    {
      ExemptItemCount = newCount;
    }
    
    public void setExcludeStatus( boolean exclude )
    {
      ExcludeFromProf = exclude;
    }
    
    public void showData( java.io.PrintStream out )
    {
      out.println("PerItemRecord.Source      = " + ContractSource );
      out.println("PerItemRecord.BetType     = " + getDescription() );
      out.println("PerItemRecord.Count       = " + Count );
      out.println("PerItemRecord.PerItemRate = " + MesMath.toCurrency( PerItemRate ) );
      out.println("PerItemRecord.PerItemAmt  = " + MesMath.toCurrency( PerItemRate * Count ) );
      out.println("PerItemRecord.ExemptCount = " + ExemptItemCount );
    }
  }
  
  public static class AdjustmentRecord extends PerItemRecord
  {
    public AdjustmentRecord( Date activeDate, int betType, int count, double amount )
    {
      // for this to be a summary record by 
      // setting the count to a negative value
      super( activeDate, CONTRACT_SOURCE_VITAL, betType, -count, amount );
    }
    
    public double getAmount( )
    { 
      // overload the base class which would have 
      // returned Count * PerItemRate.  this class
      // reused the PerItemRate member of the base
      // class to hold the total adjustment amount.
      // simply return the the current value.
      return( PerItemRate );
    }
    
    public double getPerItemRate( )
    {
      // the PerItemRate field is being used to 
      // hold the total adjustment amount, just
      // return 0
      return( 0.0 );
    }
    
    public void showData( java.io.PrintStream out )
    {
      out.println("AdjustmentRecord.BetType  = " + getDescription() );
      out.println("AdjustmentRecord.Count    = " + Count );
      out.println("AdjustmentRecord.Amount   = " + MesMath.toCurrency( getAmount() ) );
    }
  }
  
  public static class PartnershipRecord extends PerItemRecord
  {
    public  double          ContractExpense       = 0.0;  // MeS contract expense
    public  double          DiscIcAdjAmount       = 0.0;  // Disc/IC related DCE adjustments
    public  double          EquipCost             = 0.0;  // Cost of equipment
    public  double          FeeAdjAmount          = 0.0;  // Fee related DCE adjustments
    public  double          IcExpense             = 0.0;  // interchange expense
    public  double          PartnershipRate       = 0.0;  // share rate 
    public  double          ProcRevenue           = 0.0;  // processor revenue (money collected from merchant accounts)
    
    public PartnershipRecord( Date activeDate, int betType )
    {
      super( activeDate, CONTRACT_SOURCE_LIABILITY, betType );
    }
    
    public PartnershipRecord( int betType, double rate, 
                              Vector revenueMask, 
                              Vector chargeRecRevenue,
                              ResultSet resultSet )
      throws java.sql.SQLException
    {
      super( resultSet.getDate("active_date"), CONTRACT_SOURCE_LIABILITY, betType );
      
      ChargeRecordRevenue   cgRecInc  = null;
      RevenueMaskItem       maskItem  = null;

      PartnershipRate = rate;
      ProcRevenue     = resultSet.getDouble("proc_inc");
      IcExpense       = (resultSet.getDouble("ic_exp") +
                         resultSet.getDouble("assessment"));
      DiscIcAdjAmount = resultSet.getDouble("disc_ic_adj_amount");
      FeeAdjAmount    = resultSet.getDouble("fee_adj_amount");
      ContractExpense = resultSet.getDouble("liability");
      
      if ( resultSet.getInt("mes_inventory") == 1 )
      {
        EquipCost = (resultSet.getDouble("equip_sales_base_cost") +
                     resultSet.getDouble("equip_rental_base_cost"));
      }
      else
      {
        EquipCost = resultSet.getDouble("equip_transfer_cost");
      }
      
      //
      // BankContractBean will automatically load a revenue mask 
      // from the agent_bank_revenue_mask tabke when loading the 
      // agent bank contract.  The revenue mask is passed into this
      // constructor to provide a custom merchant income mask.
      //
      // To mask a particular income category (i.e. equip sales) for 
      // an agent bank (group) set the income_category column in the 
      // table agent_bank_revenue_mask.
      //
      // (see BankContractBean for category group values) 
      //
      if ( revenueMask.size() > 0 ) 
      {
        String              fieldName       = null;
        String[]            fieldNames      = new String[] 
                                              {
                                                "disc_ic_inc",      // BET_GROUP_DISC_IC
                                                "capture_inc",      // BET_GROUP_CAPTURE_FEES
                                                "debit_inc",        // BET_GROUP_DEBIT_FEES
                                                "plan_inc",         // BET_GROUP_PLAN_FEES
                                                "auth_inc",         // BET_GROUP_AUTH_FEES   
                                                "sys_gen_inc",      // BET_GROUP_SYSTEM_FEES 
                                                "equip_rental_inc", // BET_GROUP_EQUIP_RENTAL
                                                "equip_sales_inc"   // BET_GROUP_EQUIP_SALES 
                                              };
      
        for( int i = 0; i < revenueMask.size(); ++i ) 
        {
          maskItem = (RevenueMaskItem) revenueMask.elementAt(i);
          try
          {
            // remove groups that are excluded from the partnership
            // revenue calculation.
            if ( maskItem.isPartnershipMask() && maskItem.isGroupMask() )
            {
              fieldName     = fieldNames[maskItem.GroupId];
              decreaseProcRevenue( resultSet.getDouble( fieldName ) );
            }              
          }
          catch( ArrayIndexOutOfBoundsException oob_e )
          {
          }
        }
      }        
    }
    
    public void decreaseProcRevenue( double adjustAmount )
    {
      ProcRevenue -= adjustAmount;
    }
    
    public double getAmount( )
    {
      return( (getNetRevenue() * PartnershipRate * 0.01) );
    }
    
    public double getNetExpense( )
    {
      return( IcExpense + ContractExpense + EquipCost );
    }
    
    public double getNetIncome( )
    {
      return( ProcRevenue + DiscIcAdjAmount + FeeAdjAmount );
    }
    
    public double getNetRevenue( )
    {
      // income less expenses
      return( getNetIncome() - getNetExpense() );
    }
    
    public double getRate( )
    {
      return( PartnershipRate );
    }
    
    public void showData( java.io.PrintStream out )
    {
      NumberFormat          percentFormat       = NumberFormat.getPercentInstance();
      
      percentFormat.setMaximumFractionDigits(3);
      
      out.println( "PartnershipRecord.ProcRevenue    = " + MesMath.toCurrency( ProcRevenue ) );
      out.println( "PartnershipRecord.IcExpense      = " + MesMath.toCurrency( IcExpense ) );
      out.println( "PartnershipRecord.DiscIcAdjAmount= " + MesMath.toCurrency( DiscIcAdjAmount ) );
      out.println( "PartnershipRecord.FeeAdjAmount   = " + MesMath.toCurrency( FeeAdjAmount ) );
      out.println( "PartnershipRecord.ContractExpense= " + MesMath.toCurrency( ContractExpense ) );
      out.println( "PartnershipRecord.NetRevenue     = " + MesMath.toCurrency( getNetRevenue() ) );
      out.println( "PartnershipRecord.Rate           = " + percentFormat.format( PartnershipRate / 100 ) );
      out.println( "PartnershipRecord.Amount         = " + MesMath.toCurrency( getAmount() ) );
    }
  }
  
  public static class VolumeRecord extends PerItemRecord
  {
    public double       Amount;
    public double       CorrectDiscountAmount;    // only on vital records
    public double       PercentageRate;
    
    public VolumeRecord( Date activeDate, int contractSrc, int betType )
    {
      super( activeDate, contractSrc, betType );
      initDiscount( 0, 0.0, 0.0 );
    }
    
    public VolumeRecord( Date activeDate, int contractSrc, int betType, int count, double amount, double perItemRate, double percentRate, double correctDiscAmt )
    {
      super( activeDate, contractSrc, betType, count, perItemRate );
      initDiscount( amount, percentRate, correctDiscAmt );
    }
    
    public VolumeRecord( Date activeDate, int contractSrc, int betType, int count, double amount, double perItemRate, double percentRate )
    {
      super( activeDate, contractSrc, betType, count, perItemRate );
      initDiscount( amount, percentRate, 0. );
    }
    
    public double getAmount( )
    {
      return( getDiscountAmount() + getPerItemAmount() );
    }
    
    protected double getCalculatedDiscountAmount( )
    {
      // percentage rate is stored internally as 2.35 for 
      // 2.35 percent it is therefore necessary to divide 
      // the result by 100 to get the correct amount
      return( MesMath.round( ((PercentageRate * Amount)/100), 2 ) );
    }
    
    protected double getCorrectDiscountAmount( )
    {
      return( CorrectDiscountAmount );
    }
    
    public double getDiscountAmount( )
    {
      double      retVal = getCorrectDiscountAmount();
      
      if ( retVal == 0.0 )
      {
        retVal = getCalculatedDiscountAmount();
      }
      return( retVal );
    }
    
    public double getDiscountRate( )
    {
      return( PercentageRate );
    }
    
    public double getVolumeAmount( )
    {
      return( Amount );
    }
    
    private final void initDiscount( double amount, double percentRate, double correctDiscAmt )
    {
      Amount                    = amount;
      CorrectDiscountAmount     = correctDiscAmt;
      PercentageRate            = percentRate;
    }
    
    public void showData( java.io.PrintStream out )
    {
      NumberFormat          percentFormat       = NumberFormat.getPercentInstance();
      
      percentFormat.setMaximumFractionDigits(3);
      
      super.showData( out );
      out.println( "VolumeRecords.Amount       = " + MesMath.toCurrency( Amount ) );
      out.println( "VolumeRecords.DiscountRate = " + percentFormat.format( PercentageRate / 100 ) );
      out.println( "VolumeRecords.Discount     = " + MesMath.toCurrency( getDiscountAmount() ) );
    }
  }
  
  public static class SalesRecord extends VolumeRecord  {
    public double       Amount       = 0;
    public double       VolumeAmount = 0;
    public SalesRecord( Date activeDate, int contractSrc, int betType )
    {
      super( activeDate, contractSrc, betType );
    }
    
    public SalesRecord( Date activeDate, int contractSrc, int betType, int count, double amount, double perItemRate, double percentRate, double correctDiscAmt )
    {
      super( activeDate, contractSrc, betType, count, amount, perItemRate, percentRate, correctDiscAmt );
    }
    
    public SalesRecord( Date activeDate, int contractSrc, int betType, int count, double amount, double perItemRate, double percentRate )
    {
      super( activeDate, contractSrc, betType, count, amount, perItemRate, percentRate);
    }
    public void setAmount(double Amount )
    {
      this.Amount = Amount;
    }
    public double getAmount( )
    {
      return( Amount );
    }
    public void setVolumeAmount(double VolumeAmount )
    {
      this.VolumeAmount = VolumeAmount;
    }
    public double getVolumeAmount( )
    {
      return( VolumeAmount );
    }
  }
  
  // cannot create one, just a container class
  private ContractTypes( )
  {
  }
}