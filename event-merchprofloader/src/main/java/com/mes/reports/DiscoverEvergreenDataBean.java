/*@lineinfo:filename=DiscoverEvergreenDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/DiscoverEvergreenDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 10/16/03 2:37p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class DiscoverEvergreenDataBean extends ReportSQLJBean
{
  public class RowDetailData
  {
    public    String                AccountStatus           = null;
    public    long                  AssociationNode         = 0L;
    public    long                  BankId                  = 0L;
    public    String                BankCity                = null;
    public    String                BankName                = null;
    public    String                BankState               = null;
    public    Date                  BillingDateBegin        = null;
    public    Date                  BillingDateEnd          = null;
    public    Date                  DateActivated           = null;
    public    Date                  DateActivationCutoff    = null;
    public    Date                  DateOpened              = null;
    public    String                DbaName                 = null;
    public    long                  DiscoverId              = 0L;
    public    long                  MerchantId              = 0L;
    public    int                   NewAccount              = 0;
    public    int                   OpenToActivateDays      = 0;
    public    double                RebatePerItemAmount     = 0.0;
    public    double                RebateVolumeAmount      = 0.0;
    public    String                RepId                   = null;
    public    String                RepName                 = null;
    public    double                VmcVolAmount            = 0.0;
    public    double                VmcVolAmountQTD         = 0.0;
    public    double                VmcVolAmountYTD         = 0.0;
    
    public RowDetailData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      AssociationNode         = resultSet.getLong("assoc_node");
      RepId                   = resultSet.getString("rep_id");
      RepName                 = resultSet.getString("rep_name");
      BankId                  = resultSet.getLong("bank_id");
      BankName                = resultSet.getString("bank_name");
      BankCity                = resultSet.getString("bank_city");
      BankState               = resultSet.getString("bank_state");
      DiscoverId              = resultSet.getLong("discover_number");
      MerchantId              = resultSet.getLong("merchant_number");
      DbaName                 = resultSet.getString("dba_name");
      DateOpened              = resultSet.getDate("date_opened");
      DateActivated           = resultSet.getDate("date_activation");
      DateActivationCutoff    = resultSet.getDate("date_activation_cutoff");
      OpenToActivateDays      = resultSet.getInt("open_to_active_days");
      AccountStatus           = resultSet.getString("account_status");
      BillingDateBegin        = resultSet.getDate("billing_date_begin");
      BillingDateEnd          = resultSet.getDate("billing_date_end");
      VmcVolAmount            = resultSet.getDouble("vmc_vol_amount");
      VmcVolAmountQTD         = resultSet.getDouble("vmc_vol_amount_qtd");
      VmcVolAmountYTD         = resultSet.getDouble("vmc_vol_amount_ytd");
      NewAccount              = resultSet.getInt("new_account");
    }
    
    public void addPerItemRebate( double value )
    {
      RebatePerItemAmount += value;
    }
    
    public void addVolumeRebate( double value )
    {
      RebateVolumeAmount += value;
    }
  }
  
  public class RowSummaryData
  {
    public    int                   AccountsOnFile          = 0;
    public    long                  HierarchyNode           = 0L;
    public    int                   NewAccounts             = 0;
    public    long                  OrgId                   = 0L;
    public    String                OrgName                 = null;
    public    double                RebatePerItemAmount     = 0.0;
    public    double                RebateVolumeAmount      = 0.0;
    public    double                VmcVolAmount            = 0.0;
    public    double                VmcVolAmountQTD         = 0.0;
    public    double                VmcVolAmountYTD         = 0.0;
    
    public RowSummaryData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      AccountsOnFile      = resultSet.getInt("accounts_on_file");
      HierarchyNode       = resultSet.getLong("hierarchy_node");
      OrgId               = resultSet.getLong("org_num");
      OrgName             = resultSet.getString("org_name");
      VmcVolAmount        = resultSet.getDouble("vmc_vol_amount");
      VmcVolAmountQTD     = resultSet.getDouble("vmc_vol_amount_qtd");
      VmcVolAmountYTD     = resultSet.getDouble("vmc_vol_amount_ytd");
      NewAccounts         = resultSet.getInt("new_accounts");
      RebatePerItemAmount = resultSet.getDouble("rebate_per_item_amount");
      RebateVolumeAmount  = resultSet.getDouble("rebate_volume_amount");
    }
  }
  
  protected DiscoverEvergreenContract   EvergreenContract   = null;
  
  public DiscoverEvergreenDataBean( )
  {
    EvergreenContract = new DiscoverEvergreenContract();
  }
  
  public void cleanUp()
  {
    try
    {
      EvergreenContract.cleanUp();
    }
    finally
    {
      super.cleanUp();
    }      
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    if( ReportType == RT_DETAILS )
    {
      line.append("\"Rep ID\",");
      line.append("\"Rep Name\",");
      line.append("\"Merchant ID\",");
      line.append("\"Discover ID\",");
      line.append("\"DBA Name\",");
      line.append("\"Date Opened\",");
      line.append("\"Date Activated\",");
      line.append("\"Date Activation Cutoff\",");
      line.append("\"Days To Activate\",");
      line.append("\"New Account\",");
      line.append("\"VMC Net Volume\",");
      line.append("\"VMC Net Volume QTD\",");
      line.append("\"VMC Net Volume YTD\",");
      line.append("\"Rebate Setup\",");
      line.append("\"Rebate Volume\",");
      line.append("\"Rebate Total\"");
    }
    else    // RT_SUMMARY
    {
      line.append("\"Hierarchy Node\",");
      line.append("\"Org Name\",");
      line.append("\"New Accounts\",");
      line.append("\"Accounts On File\",");
      line.append("\"VMC Net Volume\",");
      line.append("\"VMC Net Volume QTD\",");
      line.append("\"VMC Net Volume YTD\",");
      line.append("\"Rebate Setup\",");
      line.append("\"Rebate Volume\",");
      line.append("\"Rebate Total\"");
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    
    if( ReportType == RT_DETAILS )
    {
      RowDetailData detail    = (RowDetailData)obj;
      
      line.append("\"");
      line.append(detail.RepId);
      line.append("\",\"");
      line.append(detail.RepName);
      line.append("\",");
      line.append(encodeHierarchyNode(detail.MerchantId));
      line.append(",");
      line.append(encodeHierarchyNode(detail.DiscoverId));
      line.append(",\"");
      line.append(detail.DbaName);
      line.append("\",");
      line.append(DateTimeFormatter.getFormattedDate(detail.DateOpened,"MM/dd/yyyy"));
      line.append(",");
      line.append(DateTimeFormatter.getFormattedDate(detail.DateActivated,"MM/dd/yyyy"));
      line.append(",");
      line.append(DateTimeFormatter.getFormattedDate(detail.DateActivationCutoff,"MM/dd/yyyy"));
      line.append(",");
      line.append(detail.OpenToActivateDays);
      line.append(",");
      line.append(detail.NewAccount);
      line.append(",");
      line.append(detail.VmcVolAmount);
      line.append(",");
      line.append(detail.VmcVolAmountQTD);
      line.append(",");
      line.append(detail.VmcVolAmountYTD);
      line.append(",");
      line.append(detail.RebatePerItemAmount);
      line.append(",");
      line.append(detail.RebateVolumeAmount);
      line.append(",");
      line.append((detail.RebatePerItemAmount + detail.RebateVolumeAmount));
    }
    else    // RT_SUMMARY
    {
      RowSummaryData        summary   = (RowSummaryData)obj;
      
      line.append(encodeHierarchyNode(summary.HierarchyNode));
      line.append(",\"");
      line.append(summary.OrgName);
      line.append("\",");
      line.append(summary.NewAccounts);
      line.append(",");
      line.append(summary.AccountsOnFile);
      line.append(",");
      line.append(summary.VmcVolAmount);
      line.append(",");
      line.append(summary.VmcVolAmountQTD);
      line.append(",");
      line.append(summary.VmcVolAmountYTD);
      line.append(",");
      line.append(summary.RebatePerItemAmount);
      line.append(",");
      line.append(summary.RebateVolumeAmount);
      line.append(",");
      line.append((summary.RebatePerItemAmount + summary.RebateVolumeAmount));
    }
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    if ( ReportType == RT_DETAILS )
    {
      filename.append("evergreen_referral_details_");
    }
    else
    {
      filename.append("evergreen_referral_summary_");
    }
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMMyyyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMMyyyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( )
  {
    loadData( getReportHierarchyNode(), ReportDateBegin );
  }
  
  public void loadData( long nodeId, Date activeDate )
  {
    RowDetailData                 detailRec         = null;
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( ReportType == RT_DETAILS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:324^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    /*+ index (sm idx_mon_ext_sum_ahn_ad) */
//                      u.third_party_user_id               as rep_id,
//                      u.name                              as rep_name,
//                      (3941 || rb.bank_association)       as bank_node,
//                      rb.bank_association                 as bank_id,
//                      rb.bank_name                        as bank_name,           
//                      rb.bank_city                        as bank_city, 
//                      rb.bank_state                       as bank_state,
//                      sm.merchant_number                  as merchant_number,
//                      mf.dba_name                         as dba_name,
//                      mf.association_node                 as assoc_node,
//                      nvl(mf.dmdsnum, 
//                          mpo.merchpo_card_merch_number)  as discover_number,
//                      to_date(mf.date_opened,'mmddrr')    as date_opened,
//                      mf.activation_date                  as date_activation,
//                      to_date(mf.date_opened,'mmddrr')+90 as date_activation_cutoff,          
//                      ( mf.activation_date - 
//                        to_date(mf.date_opened,'mmddrr')) as open_to_active_days,
//                      sm.merchant_status                  as account_status,
//                      sm.active_date                      as active_date,
//                      sm.month_begin_date                 as billing_date_begin,
//                      sm.month_end_date                   as billing_date_end,
//                      sm.vmc_vol_amount                   as vmc_vol_amount,
//                      smy.vmc_vol_amount_qtd              as vmc_vol_amount_qtd,          
//                      smy.vmc_vol_amount_ytd              as vmc_vol_amount_ytd,
//                      decode(mf_act.merchant_number,
//                             null,0,1)                    as new_account
//            from      t_hierarchy                 th,
//                      discover_referral_banks     rb,
//                      t_hierarchy                 thc,
//                      monthly_extract_summary     sm,
//                      mif                         mf,
//                      mif                         mf_act,
//                      merchant                    mr,
//                      application                 app,
//                      merchpayoption              mpo,
//                      users                       u,
//                      ( select  /*+ index (sm_yr IDX_MON_EXT_SUM_AHN_AD) */
//                                sm_yr.merchant_number       as merchant_number,
//                                sum( decode( trunc(sm_yr.active_date,'Q'),
//                                             trunc(:activeDate,'Q'), sm_yr.vmc_vol_amount,
//                                             0 ) )          as vmc_vol_amount_qtd,
//                                sum(sm_yr.vmc_vol_amount)   as vmc_vol_amount_ytd
//                        from    t_hierarchy               th_yr,
//                                monthly_extract_summary   sm_yr
//                        where   th_yr.hier_type = 1 and
//                                th_yr.ancestor = :nodeId and
//                                th_yr.entity_type = 4 and
//                                sm_yr.assoc_hierarchy_node = th_yr.descendent and
//                                sm_yr.active_date between trunc(:activeDate,'year') and :activeDate
//                        group by sm_yr.merchant_number
//                        order by sm_yr.merchant_number                    
//                      )                           smy
//            where     th.hier_type = 1 and
//                      th.ancestor = :nodeId and
//                      th.descendent = (3941 || rb.bank_association) and
//                      thc.hier_type = 1 and
//                      thc.ancestor = th.descendent and
//                      thc.entity_type = 4 and
//                      sm.assoc_hierarchy_node = thc.descendent and
//                      sm.active_date = :activeDate and
//                      mf.merchant_number = sm.merchant_number and
//                      mr.merch_number = mf.merchant_number and
//                      app.app_seq_num = mr.app_seq_num and
//                      mpo.app_seq_num(+)  = mr.app_seq_num and
//                      mpo.cardtype_code(+)= :mesConstants.APP_CT_DISCOVER and -- 14 and
//                      u.user_id = app.app_user_id and
//                      mf_act.merchant_number(+) = sm.merchant_number and
//                      mf_act.activation_date(+) between sm.month_begin_date and sm.month_end_date and
//                      ( mf_act.activation_date(+) - 
//                        to_date(mf_act.date_opened(+),'mmddrr')) <= 90 and
//                      smy.merchant_number = sm.merchant_number                      
//            order by bank_name, bank_id, merchant_number       
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    /*+ index (sm idx_mon_ext_sum_ahn_ad) */\n                    u.third_party_user_id               as rep_id,\n                    u.name                              as rep_name,\n                    (3941 || rb.bank_association)       as bank_node,\n                    rb.bank_association                 as bank_id,\n                    rb.bank_name                        as bank_name,           \n                    rb.bank_city                        as bank_city, \n                    rb.bank_state                       as bank_state,\n                    sm.merchant_number                  as merchant_number,\n                    mf.dba_name                         as dba_name,\n                    mf.association_node                 as assoc_node,\n                    nvl(mf.dmdsnum, \n                        mpo.merchpo_card_merch_number)  as discover_number,\n                    to_date(mf.date_opened,'mmddrr')    as date_opened,\n                    mf.activation_date                  as date_activation,\n                    to_date(mf.date_opened,'mmddrr')+90 as date_activation_cutoff,          \n                    ( mf.activation_date - \n                      to_date(mf.date_opened,'mmddrr')) as open_to_active_days,\n                    sm.merchant_status                  as account_status,\n                    sm.active_date                      as active_date,\n                    sm.month_begin_date                 as billing_date_begin,\n                    sm.month_end_date                   as billing_date_end,\n                    sm.vmc_vol_amount                   as vmc_vol_amount,\n                    smy.vmc_vol_amount_qtd              as vmc_vol_amount_qtd,          \n                    smy.vmc_vol_amount_ytd              as vmc_vol_amount_ytd,\n                    decode(mf_act.merchant_number,\n                           null,0,1)                    as new_account\n          from      t_hierarchy                 th,\n                    discover_referral_banks     rb,\n                    t_hierarchy                 thc,\n                    monthly_extract_summary     sm,\n                    mif                         mf,\n                    mif                         mf_act,\n                    merchant                    mr,\n                    application                 app,\n                    merchpayoption              mpo,\n                    users                       u,\n                    ( select  /*+ index (sm_yr IDX_MON_EXT_SUM_AHN_AD) */\n                              sm_yr.merchant_number       as merchant_number,\n                              sum( decode( trunc(sm_yr.active_date,'Q'),\n                                           trunc( :1 ,'Q'), sm_yr.vmc_vol_amount,\n                                           0 ) )          as vmc_vol_amount_qtd,\n                              sum(sm_yr.vmc_vol_amount)   as vmc_vol_amount_ytd\n                      from    t_hierarchy               th_yr,\n                              monthly_extract_summary   sm_yr\n                      where   th_yr.hier_type = 1 and\n                              th_yr.ancestor =  :2  and\n                              th_yr.entity_type = 4 and\n                              sm_yr.assoc_hierarchy_node = th_yr.descendent and\n                              sm_yr.active_date between trunc( :3 ,'year') and  :4 \n                      group by sm_yr.merchant_number\n                      order by sm_yr.merchant_number                    \n                    )                           smy\n          where     th.hier_type = 1 and\n                    th.ancestor =  :5  and\n                    th.descendent = (3941 || rb.bank_association) and\n                    thc.hier_type = 1 and\n                    thc.ancestor = th.descendent and\n                    thc.entity_type = 4 and\n                    sm.assoc_hierarchy_node = thc.descendent and\n                    sm.active_date =  :6  and\n                    mf.merchant_number = sm.merchant_number and\n                    mr.merch_number = mf.merchant_number and\n                    app.app_seq_num = mr.app_seq_num and\n                    mpo.app_seq_num(+)  = mr.app_seq_num and\n                    mpo.cardtype_code(+)=  :7  and -- 14 and\n                    u.user_id = app.app_user_id and\n                    mf_act.merchant_number(+) = sm.merchant_number and\n                    mf_act.activation_date(+) between sm.month_begin_date and sm.month_end_date and\n                    ( mf_act.activation_date(+) - \n                      to_date(mf_act.date_opened(+),'mmddrr')) <= 90 and\n                    smy.merchant_number = sm.merchant_number                      \n          order by bank_name, bank_id, merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.DiscoverEvergreenDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setDate(3,activeDate);
   __sJT_st.setDate(4,activeDate);
   __sJT_st.setLong(5,nodeId);
   __sJT_st.setDate(6,activeDate);
   __sJT_st.setInt(7,mesConstants.APP_CT_DISCOVER);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.DiscoverEvergreenDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:399^9*/
      }
      else    // RT_SUMMARY
      {
        if ( isNodeAssociation(nodeId) )
        {
          /*@lineinfo:generated-code*//*@lineinfo:405^11*/

//  ************************************************************
//  #sql [Ctx] it = { select    /*+ index (es idx_disc_evergreen_asn_ad) */
//                        o.org_num                           as org_num,
//                        o.org_group                         as hierarchy_node,
//                        o.org_name                          as org_name,
//                        count(sm.merchant_number)           as accounts_on_file,
//                        sum(es.referral_per_item_amount)    as rebate_per_item_amount,
//                        sum(es.referral_volume_amount)      as rebate_volume_amount,
//                        sum(es.vmc_vol_amount)              as vmc_vol_amount,
//                        sum(es.vmc_vol_amount_qtd)          as vmc_vol_amount_qtd,          
//                        sum(es.vmc_vol_amount_ytd)          as vmc_vol_amount_ytd,
//                        sum(decode(mf_act.merchant_number,  
//                                   null,0,1))               as new_accounts
//              from      discover_evergreen_summary  es,
//                        monthly_extract_summary     sm,
//                        mif                         mf_act,
//                        organization                o
//              where     es.association_node = :nodeId and
//                        es.active_date = :activeDate and
//                        sm.merchant_number = es.merchant_number and
//                        sm.active_date = es.active_date and
//                        mf_act.merchant_number(+) = sm.merchant_number and
//                        mf_act.activation_date(+) between sm.month_begin_date and sm.month_end_date and
//                        ( mf_act.activation_date(+) - 
//                          to_date(mf_act.date_opened(+),'mmddrr')) <= 90 and
//                        o.org_group = es.merchant_number
//              group by  o.org_num, o.org_group, o.org_name 
//              order by o.org_name, o.org_group        
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    /*+ index (es idx_disc_evergreen_asn_ad) */\n                      o.org_num                           as org_num,\n                      o.org_group                         as hierarchy_node,\n                      o.org_name                          as org_name,\n                      count(sm.merchant_number)           as accounts_on_file,\n                      sum(es.referral_per_item_amount)    as rebate_per_item_amount,\n                      sum(es.referral_volume_amount)      as rebate_volume_amount,\n                      sum(es.vmc_vol_amount)              as vmc_vol_amount,\n                      sum(es.vmc_vol_amount_qtd)          as vmc_vol_amount_qtd,          \n                      sum(es.vmc_vol_amount_ytd)          as vmc_vol_amount_ytd,\n                      sum(decode(mf_act.merchant_number,  \n                                 null,0,1))               as new_accounts\n            from      discover_evergreen_summary  es,\n                      monthly_extract_summary     sm,\n                      mif                         mf_act,\n                      organization                o\n            where     es.association_node =  :1  and\n                      es.active_date =  :2  and\n                      sm.merchant_number = es.merchant_number and\n                      sm.active_date = es.active_date and\n                      mf_act.merchant_number(+) = sm.merchant_number and\n                      mf_act.activation_date(+) between sm.month_begin_date and sm.month_end_date and\n                      ( mf_act.activation_date(+) - \n                        to_date(mf_act.date_opened(+),'mmddrr')) <= 90 and\n                      o.org_group = es.merchant_number\n            group by  o.org_num, o.org_group, o.org_name \n            order by o.org_name, o.org_group";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.DiscoverEvergreenDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.DiscoverEvergreenDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:434^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:438^11*/

//  ************************************************************
//  #sql [Ctx] it = { select    /*+ index (es idx_disc_evergreen_asn_ad) */
//                        o.org_num                           as org_num,
//                        o.org_group                         as hierarchy_node,
//                        o.org_name                          as org_name,
//                        count(sm.merchant_number)           as accounts_on_file,
//                        sum(es.referral_per_item_amount)    as rebate_per_item_amount, 
//                        sum(es.referral_volume_amount)      as rebate_volume_amount, 
//                        sum(es.vmc_vol_amount)              as vmc_vol_amount,
//                        sum(es.vmc_vol_amount_qtd)          as vmc_vol_amount_qtd,          
//                        sum(es.vmc_vol_amount_ytd)          as vmc_vol_amount_ytd,
//                        sum(decode(mf_act.merchant_number,  
//                                   null,0,1))               as new_accounts
//              from      t_hierarchy                 th,
//                        t_hierarchy                 thc,
//                        discover_evergreen_summary  es,
//                        monthly_extract_summary     sm,
//                        mif                         mf_act,
//                        organization                o
//              where     th.hier_type = 1 and
//                        th.ancestor = :nodeId and
//                        th.relation = 1 and
//                        thc.hier_type = 1 and
//                        thc.ancestor = th.descendent and
//                        thc.entity_type = 4 and
//                        es.association_node = thc.descendent and
//                        es.active_date = :activeDate and
//                        sm.merchant_number = es.merchant_number and
//                        sm.active_date = es.active_date and
//                        mf_act.merchant_number(+) = sm.merchant_number and
//                        mf_act.activation_date(+) between sm.month_begin_date and sm.month_end_date and
//                        ( mf_act.activation_date(+) - 
//                          to_date(mf_act.date_opened(+),'mmddrr')) <= 90 and
//                        o.org_group = th.descendent
//              group by  o.org_num, o.org_group, o.org_name 
//              order by o.org_name, o.org_group        
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    /*+ index (es idx_disc_evergreen_asn_ad) */\n                      o.org_num                           as org_num,\n                      o.org_group                         as hierarchy_node,\n                      o.org_name                          as org_name,\n                      count(sm.merchant_number)           as accounts_on_file,\n                      sum(es.referral_per_item_amount)    as rebate_per_item_amount, \n                      sum(es.referral_volume_amount)      as rebate_volume_amount, \n                      sum(es.vmc_vol_amount)              as vmc_vol_amount,\n                      sum(es.vmc_vol_amount_qtd)          as vmc_vol_amount_qtd,          \n                      sum(es.vmc_vol_amount_ytd)          as vmc_vol_amount_ytd,\n                      sum(decode(mf_act.merchant_number,  \n                                 null,0,1))               as new_accounts\n            from      t_hierarchy                 th,\n                      t_hierarchy                 thc,\n                      discover_evergreen_summary  es,\n                      monthly_extract_summary     sm,\n                      mif                         mf_act,\n                      organization                o\n            where     th.hier_type = 1 and\n                      th.ancestor =  :1  and\n                      th.relation = 1 and\n                      thc.hier_type = 1 and\n                      thc.ancestor = th.descendent and\n                      thc.entity_type = 4 and\n                      es.association_node = thc.descendent and\n                      es.active_date =  :2  and\n                      sm.merchant_number = es.merchant_number and\n                      sm.active_date = es.active_date and\n                      mf_act.merchant_number(+) = sm.merchant_number and\n                      mf_act.activation_date(+) between sm.month_begin_date and sm.month_end_date and\n                      ( mf_act.activation_date(+) - \n                        to_date(mf_act.date_opened(+),'mmddrr')) <= 90 and\n                      o.org_group = th.descendent\n            group by  o.org_num, o.org_group, o.org_name \n            order by o.org_name, o.org_group";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.DiscoverEvergreenDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.DiscoverEvergreenDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:475^11*/
        }          
      }
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        if ( ReportType == RT_DETAILS )
        {
          // details require the contract data to be loaded
          detailRec = new RowDetailData( resultSet );
          processContract( resultSet, detailRec );
          
          ReportRows.addElement( detailRec );
        }
        else // RT_SUMMARY
        {
          ReportRows.addElement( new RowSummaryData( resultSet ) );
        }          
      }
      resultSet.close();
      it.close();   
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",nodeId,activeDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void processContract( ResultSet resultSet, RowDetailData detail )
  {
    double                              amount            = 0.0;
    DiscoverEvergreenContract.RowData   contractElement   = null;
    Vector                              elements          = null;
    double                              volume            = 0.0;
    
    try
    {
      // load the contract elements for this bank
      EvergreenContract.connect();
      EvergreenContract.loadData( resultSet.getLong("bank_node"), null, null );
      elements = EvergreenContract.getReportRows();
      
      for( int i = 0; i < elements.size(); ++i )
      {
        contractElement = (DiscoverEvergreenContract.RowData) elements.elementAt(i);
        
        volume = resultSet.getDouble(contractElement.VolumeColName);
        
        // there is volume and this particular account qualifies
        // for the rebate program.
        if ( ( volume > 0 ) && 
              contractElement.isActive( resultSet.getDate("active_date"),
                                        resultSet.getInt("open_to_active_days") ) )
        {
          if( contractElement.RateType.equals("P") )
          {
            /*@lineinfo:generated-code*//*@lineinfo:536^13*/

//  ************************************************************
//  #sql [Ctx] { select round( (:volume * :contractElement.Rate),2)
//                        
//                from dual
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select round( ( :1  *  :2 ),2)\n                       \n              from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.DiscoverEvergreenDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDouble(1,volume);
   __sJT_st.setDouble(2,contractElement.Rate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amount = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:541^13*/
          
            detail.addPerItemRebate( amount );
          }
          else // rate
          {
            /*@lineinfo:generated-code*//*@lineinfo:547^13*/

//  ************************************************************
//  #sql [Ctx] { select round( (:volume * :contractElement.Rate * 0.01),2)
//                        
//                from dual
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select round( ( :1  *  :2  * 0.01),2)\n                       \n              from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.DiscoverEvergreenDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDouble(1,volume);
   __sJT_st.setDouble(2,contractElement.Rate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amount = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:552^13*/
            
            detail.addVolumeRebate( amount );
          }            
        }                                        
      }
    }
    catch(Exception e)
    {
      logEntry("processContract()",e.toString());
    }
  }
  
  public void setProperties( HttpServletRequest request )
  {
    super.setProperties(request);
    
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      cal.set(Calendar.DAY_OF_MONTH,1);     // first of current month
      cal.add(Calendar.DAY_OF_MONTH,-1);    // last of previous month
      cal.set(Calendar.DAY_OF_MONTH,1);     // first of previous month
      
      // set both the begin and end dates
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      setReportDateEnd  ( getReportDateBegin() );
    }    
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/