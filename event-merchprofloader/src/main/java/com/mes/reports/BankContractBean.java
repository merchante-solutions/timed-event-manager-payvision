/*@lineinfo:filename=BankContractBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/reports/BankContractBean.sqlj $

  Description:  


  Last Modified By   : $Author: tbaker $
  Last Modified Date : $Date: 2014-07-30 16:13:34 -0700 (Wed, 30 Jul 2014) $
  Version            : $Revision: 22963 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
//import oracle.jdbc.driver.OracleTypes;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.DateField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.Validation;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesSystem;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;

public class BankContractBean extends ReportSQLJBean
{
  public static final int   BET_CASH_ADV_TRANSACTION_FEE        =  0;   //
  public static final int   BET_PROCESSING_PER_ITEM             =  1;   //
  public static final int   BET_PROCESSING_RATE                 =  2;   //
  public static final int   BET_IC_DOWNGRADE_FEE                =  3;   // ?
  public static final int   BET_AMEX_AUTHORIZATION_FEE          =  4;   //
  public static final int   BET_DISC_AUTHORIZATION_FEE          =  5;   //
  public static final int   BET_DINERS_AUTHORIZATION_FEE        =  6;   //
  public static final int   BET_JCB_AUTHORIZATION_FEE           =  7;   //
  public static final int   BET_INCOMING_CHARGEBACK_PER_ITEM    =  8;
  public static final int   BET_DEBIT_TRANSACTION_FEE           =  9;   //
  public static final int   BET_CHECK_SERVICE_AUTH_FEE          = 10;
  public static final int   BET_OUTGOING_CHARGEBACK_PER_ITEM    = 11;   //
  public static final int   BET_NEW_ACCOUNT_SETUP_FEE           = 12;
  public static final int   BET_ISP_FEE                         = 13;
  public static final int   BET_DIAL_PAY_TRANSACTION_FEE        = 14;
  public static final int   BET_EQUIPMENT_RENTAL_FEE            = 15;   //
  public static final int   BET_STATEMENT_FEE                   = 16;   //
  public static final int   BET_HELP_DESK_FEE                   = 17;   //
  public static final int   BET_WEB_REPORTING_FEE               = 18;   //
  public static final int   BET_TERMINAL_DEPLOYMENT             = 19;   
  public static final int   BET_VERISIGN_PAYLINK_SETUP_FEE      = 20;   //
  public static final int   BET_VERISIGN_PAYFLOW_SETUP_FEE      = 21;   //
  public static final int   BET_POS_PARTNER_SETUP_FEE           = 22;   //
  public static final int   BET_INCOMING_RETRIEVAL_REQUEST      = 23;
  public static final int   BET_VMC_ASSESSMENT_FEE              = 24;   //
  public static final int   BET_POS_PARTNER_FEE                 = 25;   //
  public static final int   BET_VERISIGN_PAYLINK_FEE            = 26;   //
  public static final int   BET_VERISIGN_PAYFLOW_FEE            = 27;   //
  public static final int   BET_VMC_AUTHORIZATION_FEE           = 28;   //
  public static final int   BET_AMEX_TRANSACTION_FEE            = 29;   //
  public static final int   BET_DISC_TRANSACTION_FEE            = 30;   //
  public static final int   BET_DINERS_TRANSACTION_FEE          = 31;   //
  public static final int   BET_JCB_TRANSACTION_FEE             = 32;   //
  public static final int   BET_MONTHLY_ACTIVE_MERCH_FEE        = 33;   //  
  public static final int   BET_CASH_ADVANCE_INTERCHANGE        = 34;   //  Not needed in database
  public static final int   BET_RISK_MONITORING                 = 35;
  public static final int   BET_RISK_MANAGEMENT_REPORTING       = 36;
  public static final int   BET_CUSTOMER_SERVICE_CALLS          = 37;
  public static final int   BET_INCOMING_FT_CHARGEBACK_PER_ITEM = 38;
  public static final int   BET_ACTIVATION_FEE                  = 39;
  public static final int   BET_AMEX_SETUP_FEE                  = 40;
  public static final int   BET_DISCOVER_SETUP_FEE              = 41;
  public static final int   BET_SUPPLY_PER_AUTH_FEE             = 42;
  public static final int   BET_PARTNERSHIP_RATE                = 43;
  public static final int   BET_DISC_IC_DCE_ADJUSTMENTS         = 44;
  public static final int   BET_FEE_DCE_ADJUSTMENTS             = 45;
  public static final int   BET_MONTHLY_PRODUCT_FEE             = 46;
  public static final int   BET_ECOMMERCE_SETUP_FEE             = 47;
  public static final int   BET_ECOMMERCE_MONTHLY_FEE           = 48;
  public static final int   BET_EBT_TRANSACTION_FEE             = 49;
  public static final int   BET_AVS_AUTHORIZATION_FEE           = 50;
  public static final int   BET_ACH_REJECT_FEE                  = 51;
  public static final int   BET_AUTHORIZATION_CREDIT            = 52;
  public static final int   BET_THIRD_PARTY_AUTH_FEE            = 53;
  public static final int   BET_VS_SPONSORSHIP                  = 54;
  public static final int   BET_MC_SPONSORSHIP                  = 55;
  public static final int   BET_MONTHLY_SUPPLY_FEE              = 56;
  public static final int   BET_APPLICATION_FEE                 = 57;
  public static final int   BET_EQUIPMENT_SWAP                  = 58;   
  public static final int   BET_AMEX_INTERCHANGE                = 59;   
  public static final int   BET_GATEWAY_AUTH_FEE                = 60;   
  public static final int   BET_RISK_MONITORING_RETAIL_VOLUME   = 61;   
  public static final int   BET_RISK_MONITORING_MOTO_VOLUME     = 62;   
  public static final int   BET_DINERS_INTERCHANGE              = 63;   
  public static final int   BET_PL1_INTERCHANGE                 = 64;   
  public static final int   BET_VISA_AUTH_ACCESS_FEE            = 65;   
  public static final int   BET_MC_SETTLE_ACCESS_FEE            = 66;   
  public static final int   BET_MC_CROSS_BORDER_FEE             = 67;   
  public static final int   BET_PRODUCT_SETUP_FEE               = 68;   
  public static final int   BET_DISCOVER_INTERCHANGE            = 69;   
  public static final int   BET_NEW_ACCOUNT_REFERRAL_FEE        = 70;   
  public static final int   BET_REFERRAL_RESIDUAL               = 71;   
  public static final int   BET_IC_CORRECTION_FEE               = 72;   
  public static final int   BET_VISA_AUTH_ACCESS_FEE_NON_VISA   = 73;
  public static final int   BET_AUTH_LOG_FEE                    = 74;
  public static final int   BET_TPG_AUTH_FEE                    = 75;
  public static final int   BET_TRIDENT_AUTH_FEE                = 76;
  public static final int   BET_VMC_AUTH_FEE_TRIDENT            = 77;
  public static final int   BET_AMEX_AUTH_FEE_TRIDENT           = 78;
  public static final int   BET_DISCOVER_AUTH_FEE_TRIDENT       = 79;
  public static final int   BET_DINERS_AUTH_FEE_TRIDENT         = 80;
  public static final int   BET_JCB_AUTH_FEE_TRIDENT            = 81;
  public static final int   BET_DEBIT_AUTH_FEE_TRIDENT          = 82;
  public static final int   BET_TWO_FACTOR_AUTHENTICATION_FEE   = 83;
  public static final int   BET_MC_FOREIGN_STD_FEE              = 84;
  public static final int   BET_MC_FOREIGN_ELEC_FEE             = 85;
  public static final int   BET_PCI_ANNUAL_REG_FEE              = 86;
  public static final int   BET_CREDIT_SCORING_FEE              = 87;
  public static final int   BET_MC_NABU_FEE                     = 88;
  public static final int   BET_DEBIT_TRANSACTION_RATE          = 89;
  public static final int   BET_ACHP_TRANSACTION_FEE            = 90;
  public static final int   BET_ACHP_RETURNED_ITEM_FEE          = 91;
  public static final int   BET_DS_SPONSORSHIP                  = 92;
  public static final int   BET_AM_SPONSORSHIP                  = 93;
  public static final int   BET_IRS_REPORTING_FEE               = 94;
  public static final int   BET_DEBIT_AUTH_FEE                  = 95;
  public static final int   BET_AMEX_GLOBAL_ACQ_FEE		        = 97;
  public static final int   BET_AMEX_PROCESSING_FEE		        = 98;
  public static final int   BET_AMEX_DIRECT_AUTH_FEE            = 99;
  public static final int   BET_AMEX_DIRECT_AUTH_FEE_TRIDENT    = 100;
  public static final int   BET_AMEX_PROCESSING_RATE            = 101;
  public static final int   BET_PIN_DEBIT_PASS_THROUGH_FLAG     = 102;
  public static final int   BET_PAPER_STATEMENT_FEE     		= 104;
  public static final int   BET_BATCH_FEE     					= 105;
  /* Code Modified by RS  - Added ACH PS Agent Bank Specific Items */  
  public static final int   BET_ACH_TRANSACTION_FEE             = 240;
  public static final int   BET_ACH_RET_REJ_FEE                 = 241;
  public static final int   BET_ACH_UNAUTH_RETURN               = 242;
  public static final int   BET_ACH_MONHLY_FEE                  = 243;
  public static final int   BET_ACH_SMRTPY_MONHLY_FEE           = 244;
  public static final int   BET_ACH_HOSTED_PY_MONHLY_FEE        = 245;
  public static final int   BET_ACH_SMRTPY_EXPR_SETUP_FEE       = 246;
  public static final int   BET_COUNT                           = 247;   //  must be last!!
  
  // special value, BET_NONE is always outside of the valid bet type range
  public static final int   BET_NONE                            = BET_COUNT;
  
  public static final long  REC_ID_NEW                          = 0L;
  
  public static final int   DEBIT_CAT_COUNT                     = 10;
  
  //
  // Groups are used to summarize merchant profitability at
  // the merchant level.
  // 
  // Currently there are the following categories for
  // merchant income/expense.
  //
  //  Discount/Interchange - V/MC Discount & Interchange
  //  Capture Fees         - Not supported
  //  Debit Network Fees   - Fees from various debit network BETs at Vital
  //  Transaction Fees     - Fees from the Vital DRTs (plan records)
  //  Authorization Fees   - Fees from authorization BETs at Vital
  //  System Fees          - All other fees (i.e. setup, rental, isp etc)
  //
  //  These lists apply MES contract BET ids to 
  //  the above categories.  
  //    
  //  Calling a summary method in ChildProfitabilitySummary 
  //  ( see BankContractDataBean.sqlj for definition of class )
  //  will add the Vital data (if any) with the MES contract 
  //  elements for the category.
  //
  //  Calling a first/next item method with the group identifier
  //  will return the first contract element of in the contract
  //  that has a BET type contained in the specified group.
  //
  //  Group Indexes
  //    NOTE:  (-1)BET_GROUP_.. is always a member of the group.
  //           This allows summary items to be inserted into the
  //           data set.  For examples refer to these methods:
  //
  //           groupIndexToBetType(..)
  //           isItemInGroup(..)
  //
  public static final int   BET_GROUP_ALL                 = -1;
  public static final int   BET_GROUP_DISC_IC             = 0;
  public static final int   BET_GROUP_CAPTURE_FEES        = 1;
  public static final int   BET_GROUP_DEBIT_FEES          = 2;
  public static final int   BET_GROUP_PLAN_FEES           = 3;
  public static final int   BET_GROUP_AUTH_FEES           = 4;
  public static final int   BET_GROUP_SYSTEM_FEES         = 5;
  public static final int   BET_GROUP_EQUIP_RENTAL        = 6;
  public static final int   BET_GROUP_EQUIP_SALES         = 7;
    /* Code modified by RS Added offset for ACH PS Specific Billing Group */ 
  public static final int   BET_GROUP_ACH                 = 8;
  public static final int   BET_GROUP_PARTNERSHIP         = 9;
  public static final int   BET_GROUP_ADJUSTMENTS         = 10;
  public static final int   BET_GROUP_COUNT_DEFAULT       = 11; // 1 more than last default group
  
  // extension groups:  these groups are available for 3941 accounts only.
  public static final int   BET_GROUP_TAX_COLLECTED       = BET_GROUP_COUNT_DEFAULT + 0;
  public static final int   BET_GROUP_EXCLUDED_FEES       = BET_GROUP_COUNT_DEFAULT + 1;
  public static final int   BET_GROUP_EXCLUDED_EQUIP      = BET_GROUP_COUNT_DEFAULT + 2;
  public static final int   BET_GROUP_COUNT               = BET_GROUP_COUNT_DEFAULT + 3;  // must be last
  
  // bet group displayNames names  
  protected static final int  BET_GROUP_NAME_SHORT              = 0;
  protected static final int  BET_GROUP_NAME_LONG               = 1;
  protected static final String[][] BET_GROUP_NAMES =
  {
    { "Disc/IC"       , "V/MC Discount & Interchange" },  // BET_GROUP_DISC_IC
    { "Capture/SCAN"  , "Transaction/Capture Fees" },     // BET_GROUP_CAPTURE_FEES
    { "Debit"         , "Debit Networks" },               // BET_GROUP_DEBIT_FEES
    { "Ind.Plan"      , "Individual Plans" },             // BET_GROUP_PLAN_FEES
    { "Auth"          , "Authorization" },                // BET_GROUP_AUTH_FEES
    { "Misc/System"   , "Misc/System Fees" },             // BET_GROUP_SYSTEM_FEES
    { "Equip.Rental"  , "Equipment Rental" },             // BET_GROUP_EQUIP_RENTAL
    { "Equip.Sales"   , "Equipment Sales" },              // BET_GROUP_EQUIP_SALES
    /* Code modified by RS Added ACH PS Specific Billing Group Name */ 
   	{  "ACH"          , "ACH Discount & Fees"},           // BET_GROUP_ACH
    { "Partnership"   , "Partnership" },                  // BET_GROUP_PARTNERSHIP
    { "Adjustments"   , "DCE Adjustments" },              // BET_GROUP_ADJUSTMENTS
    { "Sales Tax"     , "Sales Tax Collected" },          // BET_GROUP_TAX_COLLECTED
    { "Excl Fees"     , "Product Fees" },                 // BET_GROUP_EXCLUDED_FEES
    { "Excl Equip"    , "Cost of Goods Sold" },           // BET_GROUP_EXCLUDED_EQUIP
  };
  
  // Group Definitions
  protected static final int[][] BET_GROUPS =
  {
    // BET_GROUP_DISC_IC
    {
      BET_PROCESSING_PER_ITEM,  // normally the seperate items are
      BET_PROCESSING_RATE,      // only used during contract editing
                                // but they are technically part
                                // of the DISC_IC group.
      BET_AMEX_PROCESSING_RATE,
      BET_VS_SPONSORSHIP,
      BET_MC_SPONSORSHIP,
      BET_AM_SPONSORSHIP,
      BET_DS_SPONSORSHIP,
      BET_VMC_ASSESSMENT_FEE,
      BET_CASH_ADVANCE_INTERCHANGE,
      BET_AMEX_INTERCHANGE,
      BET_RISK_MONITORING_RETAIL_VOLUME,
      BET_RISK_MONITORING_MOTO_VOLUME,
      BET_DINERS_INTERCHANGE,
      BET_PL1_INTERCHANGE,
      BET_MC_SETTLE_ACCESS_FEE,
      BET_MC_CROSS_BORDER_FEE,
      BET_DISCOVER_INTERCHANGE,
      BET_REFERRAL_RESIDUAL,
      BET_IC_CORRECTION_FEE,
      BET_MC_FOREIGN_STD_FEE,
      BET_MC_FOREIGN_ELEC_FEE,
      BET_MC_NABU_FEE,
      BET_AMEX_GLOBAL_ACQ_FEE,
      BET_AMEX_PROCESSING_FEE,
    },
    // BET_GROUP_CAPTURE_FEES 
    { 
      // not supported 
    },
    // BET_GROUP_DEBIT_FEES
    {
      BET_DEBIT_TRANSACTION_FEE,
      BET_DEBIT_TRANSACTION_RATE,
      BET_PIN_DEBIT_PASS_THROUGH_FLAG
    },
    // BET_GROUP_PLAN_FEES
    {
      BET_AMEX_TRANSACTION_FEE,
      BET_DISC_TRANSACTION_FEE,
      BET_DINERS_TRANSACTION_FEE,
      BET_JCB_TRANSACTION_FEE,
      BET_CASH_ADV_TRANSACTION_FEE,
      BET_EBT_TRANSACTION_FEE,
      BET_ACHP_TRANSACTION_FEE,
      BET_ACHP_RETURNED_ITEM_FEE,
    },
    // BET_GROUP_AUTH_FEES
    {
      BET_VMC_AUTHORIZATION_FEE,
      BET_AMEX_AUTHORIZATION_FEE,
      BET_DINERS_AUTHORIZATION_FEE,
      BET_DISC_AUTHORIZATION_FEE,
      BET_JCB_AUTHORIZATION_FEE,
      BET_AVS_AUTHORIZATION_FEE,
      BET_DIAL_PAY_TRANSACTION_FEE,
      BET_AUTHORIZATION_CREDIT,
      BET_THIRD_PARTY_AUTH_FEE,
      BET_GATEWAY_AUTH_FEE,
      BET_VISA_AUTH_ACCESS_FEE,
      BET_VISA_AUTH_ACCESS_FEE_NON_VISA,
      BET_AUTH_LOG_FEE,
      BET_TPG_AUTH_FEE,
      BET_TRIDENT_AUTH_FEE,
      BET_VMC_AUTH_FEE_TRIDENT,
      BET_AMEX_AUTH_FEE_TRIDENT,
      BET_DISCOVER_AUTH_FEE_TRIDENT,
      BET_DINERS_AUTH_FEE_TRIDENT,
      BET_JCB_AUTH_FEE_TRIDENT,
      BET_DEBIT_AUTH_FEE_TRIDENT,
      BET_DEBIT_AUTH_FEE,
      BET_AMEX_DIRECT_AUTH_FEE,
      BET_AMEX_DIRECT_AUTH_FEE_TRIDENT,
    },
    // BET_GROUP_SYSTEM_FEES 
    {
      BET_CHECK_SERVICE_AUTH_FEE,
      BET_INCOMING_FT_CHARGEBACK_PER_ITEM,
      BET_INCOMING_CHARGEBACK_PER_ITEM,
      BET_INCOMING_RETRIEVAL_REQUEST,
      BET_OUTGOING_CHARGEBACK_PER_ITEM,
      BET_ISP_FEE,
      BET_STATEMENT_FEE,
      BET_HELP_DESK_FEE,
      BET_WEB_REPORTING_FEE,
      BET_TERMINAL_DEPLOYMENT,
      BET_MONTHLY_ACTIVE_MERCH_FEE,
      BET_RISK_MONITORING,
      BET_RISK_MANAGEMENT_REPORTING,
      BET_CUSTOMER_SERVICE_CALLS,
      BET_SUPPLY_PER_AUTH_FEE,
      BET_MONTHLY_PRODUCT_FEE,
      BET_ECOMMERCE_MONTHLY_FEE,
      BET_ACH_REJECT_FEE,
      BET_MONTHLY_SUPPLY_FEE,
      BET_NEW_ACCOUNT_SETUP_FEE,
      BET_ECOMMERCE_SETUP_FEE,
      BET_ACTIVATION_FEE,
      BET_APPLICATION_FEE,
      BET_DISCOVER_SETUP_FEE,
      BET_AMEX_SETUP_FEE,
      BET_EQUIPMENT_SWAP,
      BET_PRODUCT_SETUP_FEE,
      BET_NEW_ACCOUNT_REFERRAL_FEE,
      BET_TWO_FACTOR_AUTHENTICATION_FEE,
      BET_PCI_ANNUAL_REG_FEE,
      BET_CREDIT_SCORING_FEE,
      BET_IRS_REPORTING_FEE,
      BET_PAPER_STATEMENT_FEE,
      BET_BATCH_FEE,
    },
    // BET_GROUP_EQUIP_RENTAL
    {
      BET_EQUIPMENT_RENTAL_FEE,
      BET_POS_PARTNER_FEE,
      BET_VERISIGN_PAYLINK_FEE,
      BET_VERISIGN_PAYFLOW_FEE,
    },
    // BET_GROUP_EQUIP_SALES
    {
      BET_VERISIGN_PAYLINK_SETUP_FEE,
      BET_VERISIGN_PAYFLOW_SETUP_FEE,
      BET_POS_PARTNER_SETUP_FEE,
    },
    /* Code modified by RS Added ACH PS Specific Billing Group Definitions */ 
	{
	 BET_ACH_TRANSACTION_FEE, 
	 BET_ACH_RET_REJ_FEE, 
	 BET_ACH_UNAUTH_RETURN,
	 BET_ACH_MONHLY_FEE,
	 BET_ACH_SMRTPY_MONHLY_FEE,
	 BET_ACH_HOSTED_PY_MONHLY_FEE,
	 BET_ACH_SMRTPY_EXPR_SETUP_FEE,
	 },
    {
      BET_PARTNERSHIP_RATE,
    },
    // BET_GROUP_ADJUSTMENTS
    {
      BET_DISC_IC_DCE_ADJUSTMENTS,
      BET_FEE_DCE_ADJUSTMENTS,
    },
    // BET_GROUP_TAX_COLLECTED
    {
      // no elements, just a place holder in the array
      BET_NONE,
    },
    // BET_GROUP_EXCLUDED_FEES
    {
      // no elements, just a place holder in the array
      BET_NONE,   
    },
    // BET_GROUP_EXCLUDED_EQUIP
    {
      // no elements, just a place holder in the array
      BET_NONE,   
    },
  };
  
  //
  // Cash Advance Bet Group 
  //
  // BET Ids listed here are considered cash advance only
  // and therefore only relate to cash advance data from 
  // the queries (see BankContractDataBean).
  protected static final int[] CASH_ADVANCE_BET_GROUP = 
  {
    BET_CASH_ADV_TRANSACTION_FEE,
    BET_AMEX_AUTHORIZATION_FEE,
    BET_DINERS_AUTHORIZATION_FEE,
    BET_DISC_AUTHORIZATION_FEE,
    BET_JCB_AUTHORIZATION_FEE,
    BET_AMEX_TRANSACTION_FEE,
    BET_DISC_TRANSACTION_FEE,
    BET_DINERS_TRANSACTION_FEE,
    BET_JCB_TRANSACTION_FEE,
    BET_PARTNERSHIP_RATE,
  };
  
  protected static final int[] SETUP_FEES_BET_GROUP = 
  {
    BET_NEW_ACCOUNT_SETUP_FEE,
    BET_ECOMMERCE_SETUP_FEE,
    BET_ACTIVATION_FEE,
    BET_APPLICATION_FEE,
    BET_DISCOVER_SETUP_FEE,
    BET_AMEX_SETUP_FEE,
    BET_PRODUCT_SETUP_FEE,
  };
  
  
  private static final String SQL_AGENT_BANK_CONTRACT = 
            "          select    abc.rec_id                      as rec_id, "
          + "                    abc.contract_type               as contract_type, "
          + "                    nvl(ct.contract_desc, "
          + "                        'Undefined Type (' || "
          + "                          abc.contract_type || ')') as contract_desc, "
          + "                    abc.hierarchy_node              as hierarchy_node, "
          + "                    o.org_name                      as org_name, "
          + "                    abc.billing_element_type        as item_type, "
          + "                    bed.billing_element_desc        as item_desc, "
          + "                    abc.product_code                as product_code, "
          + "                    abc.exempt_item_count           as exempt_count, "
          + "                    abc.valid_date_begin            as valid_date_begin, "
          + "                    abc.valid_date_end              as valid_date_end, "
          + "                    decode(abc.exclude_from_prof, "
          + "                           'Y',1,0)                 as exclude, "
          + "                    nvl(abc.billing_months,'YYYYYYYYYYYY') "
          + "                                                    as billing_months, "
          + "                    nvl(bed.element_type,'P')       as rate_type, "
          + "                    abc.rate                        as rate "
          + "          from      agent_bank_contract       abc, "
          + "                    billing_element_desc      bed, "
          + "                    agent_bank_contract_type  ct, "
          + "                    organization          o "
          + "          where     (  (abc.contract_type  = ? ) or "
          + "                       (? = (?)) "
          + "                    ) and "
          + "                    abc.hierarchy_node = ? and "
          + "                    ( "
          + "                    ? is null or "
          + "                    ? between abc.valid_date_begin and abc.valid_date_end "
          + "                    ) and "
          + "                    bed.billing_element_type = abc.billing_element_type and "
          + "                    ct.contract_type(+) = abc.contract_type and "
          + "                    o.org_group(+) = abc.hierarchy_node and "
          + "                    ( "
          + "                     (abc.billing_element_type  not between 240 and 246) "
          + "                     or "
          + "                     ( "
          + "                        (abc.billing_element_type  between 240 and 246) "
          + "                         and exists "
          + "                         (select '' "
          + "                           from   monthly_extract_st st "
          + "                          where st.hh_load_sec = ? and "
          + "                                upper(st.st_statement_desc) = upper(bed.billing_element_desc) "
          + "                          ) "
          + "                      ) "
          + "                     ) "
          + "          order by  o.org_name, abc.contract_type, bed.billing_element_desc, "
          + "                    abc.valid_date_end";
  
  //
  // this class is used as storage to define the relationships 
  // between the monthly extract data in the database and the 
  // java contract data class
  // 
  public static class BetTypeMap
  {
    public int          BetType;      // contract bet type
    public String       FieldName;    // database field name or value
    
    public BetTypeMap( String fieldName, int betType )
    {
      BetType   = betType;
      FieldName = fieldName;
    }
  }
  
  public class ContractItem
  {
    public String   BillingMonths       = null;
    public String   ContractDesc        = null;
    public int      ContractType        = 0;
    public int      ExemptItemCount     = 0;
    public boolean  ExcludeFromProf     = false;
    public int      ItemType            = 0;
    public String   ItemDescription     = null;
    public double   ItemRate            = 0.0;
    public String   ItemRateType        = null;
    public long     NodeId              = 0L;
    public String   OrgName             = null;
    public String   ProductCode         = null;
    public long     RecordId            = -1L;
    public Date     ValidDateBegin      = null;
    public Date     ValidDateEnd        = null;;
    
    public ContractItem( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      RecordId          = resultSet.getLong("rec_id");
      ContractType      = resultSet.getInt("contract_type");
      ContractDesc      = resultSet.getString("contract_desc");
      NodeId            = resultSet.getLong("hierarchy_node");
      OrgName           = resultSet.getString("org_name");
      ItemType          = resultSet.getInt("item_type");
      ItemDescription   = resultSet.getString("item_desc");
      ProductCode       = resultSet.getString("product_code");
      ValidDateBegin    = resultSet.getDate("valid_date_begin");
      ValidDateEnd      = resultSet.getDate("valid_date_end");
      ItemRate          = resultSet.getDouble("rate");
      ItemRateType      = resultSet.getString("rate_type");
      ExemptItemCount   = resultSet.getInt("exempt_count");
      ExcludeFromProf   = ( resultSet.getInt("exclude") == 1 );
      BillingMonths     = resultSet.getString("billing_months");
    }
    
    public String getProductDesc( )
    {
      String            retVal      = ProductCode;
      
      if ( ProductCode == null )
      {
        retVal = "Default";
      }
      else if( ItemType == BET_DEBIT_TRANSACTION_FEE )
      {
        retVal = loadDebitCatDesc( ProductCode );
      }
      else
      {
        retVal = loadProductDesc( ProductCode );
      }
      return( retVal );
    }
    
    public boolean isActive( )
    {
      boolean     retVal      = false;
      
      if ( ( ValidDateEnd != null ) &&
           !( ValidDateEnd.before(Calendar.getInstance().getTime()) ) )
      {
        retVal = true;
      }           
      return( retVal );
    }
  }
  
  public static class ContractItemsTable extends DropDownTable
  {
    public ContractItemsTable( )
      throws java.sql.SQLException
    {
      ResultSetIterator       it              = null;
      ResultSet               resultSet       = null;
      
      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:548^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  billing_element_type, 
//                    billing_element_desc 
//            from    billing_element_desc 
//            order by billing_element_desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  billing_element_type, \n                  billing_element_desc \n          from    billing_element_desc \n          order by billing_element_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.BankContractBean",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.BankContractBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:554^9*/
        resultSet = it.getResultSet();
    
        addElement( "", "select one" );
        while( resultSet.next() )
        {
          addElement( resultSet );
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }
  
  public class ContractNodesTable extends DropDownTable
  {
    public ContractNodesTable( )
      throws java.sql.SQLException
    {
      ResultSetIterator       it              = null;
      long                    nodeId          = getReportHierarchyNode();
      ResultSet               resultSet       = null;
      
      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:586^9*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct
//                   o.org_group                          as hierarchy_node, 
//                   substr( decode( abc.hierarchy_node,
//                                   null, (o.org_group || ' - ' || o.org_name),
//                                   ('* ' || o.org_group || ' - ' || o.org_name)
//                                 ), 1,96 )              as org_name 
//            from   t_hierarchy          th,
//                   organization         o,
//                   agent_bank_contract  abc
//            where  th.hier_type = 1 and
//                   th.ancestor = :nodeId and
//                   o.org_group = th.descendent and
//                   abc.hierarchy_node(+) = o.org_group
//            order by org_name        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct\n                 o.org_group                          as hierarchy_node, \n                 substr( decode( abc.hierarchy_node,\n                                 null, (o.org_group || ' - ' || o.org_name),\n                                 ('* ' || o.org_group || ' - ' || o.org_name)\n                               ), 1,96 )              as org_name \n          from   t_hierarchy          th,\n                 organization         o,\n                 agent_bank_contract  abc\n          where  th.hier_type = 1 and\n                 th.ancestor =  :1   and\n                 o.org_group = th.descendent and\n                 abc.hierarchy_node(+) = o.org_group\n          order by org_name";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.BankContractBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:602^9*/
        
        resultSet = it.getResultSet();
    
        addElement( "", "select one" );
        while( resultSet.next() )
        {
          addElement( resultSet );
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }
  
  public class BankContractTable extends DropDownTable
  {
    int             ContractNodeCount      = 0;
    String          ContractNode           = null;

    public BankContractTable()
      throws java.sql.SQLException
    {
      ResultSetIterator       it              = null;
      ResultSet               resultSet       = null;
      long                    nodeId          = getReportHierarchyNode();
      
      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:638^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.org_group   as hierarchy_node,
//                    ('* ' || o.org_group || ' - ' || o.org_name) as org_name
//            from    t_hierarchy          th,
//                    organization         o
//            where   th.hier_type = 1 and
//                    th.ancestor = :nodeId and 
//                    o.org_group = th.descendent and
//                    exists
//                    ( 
//                      select abc.HIERARCHY_NODE
//                      from   agent_bank_contract  abc
//                      where  abc.HIERARCHY_NODE = o.org_group
//                    )
//            order by org_group       
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.org_group   as hierarchy_node,\n                  ('* ' || o.org_group || ' - ' || o.org_name) as org_name\n          from    t_hierarchy          th,\n                  organization         o\n          where   th.hier_type = 1 and\n                  th.ancestor =  :1   and \n                  o.org_group = th.descendent and\n                  exists\n                  ( \n                    select abc.HIERARCHY_NODE\n                    from   agent_bank_contract  abc\n                    where  abc.HIERARCHY_NODE = o.org_group\n                  )\n          order by org_group";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.BankContractBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:654^9*/
        
        resultSet = it.getResultSet();
    
        addElement( "", "select one" );
        while( resultSet.next() )
        {
          addElement( resultSet );
          ContractNodeCount++;
          if( ContractNodeCount == 1 )
          {
            ContractNode = resultSet.getString("hierarchy_node");
          }          
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
    
    public int getContractNodeCount()
    {
      return( ContractNodeCount );
    }
    
    public String getContractNode()
    {
      return( ContractNode );
    }
  }
  
  public static class ContractTypesTable extends DropDownTable
  {
    public ContractTypesTable( )
      throws java.sql.SQLException
    {
      ResultSetIterator       it              = null;
      ResultSet               resultSet       = null;
      
      try
      {
        connect();
        
        /*@lineinfo:generated-code*//*@lineinfo:701^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  contract_type     as contract_type, 
//                    contract_desc     as contract_desc 
//            from    agent_bank_contract_type 
//            where   contract_type >= 0 and
//                    contract_type != :ContractTypes.CONTRACT_SOURCE_VITAL
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  contract_type     as contract_type, \n                  contract_desc     as contract_desc \n          from    agent_bank_contract_type \n          where   contract_type >= 0 and\n                  contract_type !=  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,ContractTypes.CONTRACT_SOURCE_VITAL);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.BankContractBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:708^9*/
        resultSet = it.getResultSet();
    
        addElement( "", "select one" );
        while( resultSet.next() )
        {
          addElement( resultSet );
        }
        resultSet.close();
        it.close();
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        cleanUp();
      }
    }
  }
  
  public static class BillingMonthsValidation implements Validation
  {
    private String ErrorMessage     = null;
    
    public String getErrorText()
    {
      return( ErrorMessage );
    }
    
    public boolean validate( String fdata )
    {
      ErrorMessage = null;
      
      if ( fdata == null || fdata.length() != 12 )
      {
        ErrorMessage = "Billing months must be 12 characters";
      }
      else
      {
        String temp = fdata.toUpperCase();
        
        for( int i = 0; i < 12; ++i )
        {
          char ch = temp.charAt(i);
          if ( ch != 'N' && ch != 'Y' )
          {
            ErrorMessage = "Billing month flags must be either Y or N";
            break;  
          }
        }
      }
      return( ErrorMessage == null );
    }
  }
  
  public class NodeIdValidation implements Validation
  {
    private String ErrorMessage     = null;
    
    public String getErrorText()
    {
      return( ErrorMessage );
    }
    
    public boolean validate( String fdata )
    {
      int     recCount    = 0;
      long    nodeId      = getReportHierarchyNodeDefault();
      
      ErrorMessage = null;
 
      if( String.valueOf(nodeId).equals(fdata) )
      {
        recCount = 1;
      }
      else
      {
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:786^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(th.ancestor) 
//              from    t_hierarchy     th
//              where   th.hier_type = 1 and
//                      th.ancestor = :nodeId and
//                      th.descendent = :fdata
//  
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(th.ancestor)  \n            from    t_hierarchy     th\n            where   th.hier_type = 1 and\n                    th.ancestor =  :1   and\n                    th.descendent =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.BankContractBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setString(2,fdata);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:794^11*/
        }
        catch( java.sql.SQLException e )
        {
          ErrorMessage = "NodeId validation failed";
        }        
      }
      
      if ( recCount == 0 )
      {
        ErrorMessage = "Invalid Node Id";
        addError( ErrorMessage );
      }
        
      
      return( ErrorMessage == null );
    }
  }
  
  private long      ContractNode              = 0L;
  private int       ContractSource            = ContractTypes.CONTRACT_SOURCE_INVALID;
  private Vector    ExcludeMask               = new Vector();
  private int       ReportState               = 0;
  private Vector    RevenueMask               = new Vector();
  private boolean   ShowingInactive           = false;
  
  private static BetTypeMap[]           
    MerchantMonthlyFees = 
      {  
        new BetTypeMap( mesConstants.VITAL_ME_POS_PARTNER           , BET_POS_PARTNER_FEE ),      // POS Partner
        new BetTypeMap( mesConstants.VITAL_ME_POS_PARTNER_2000      , BET_POS_PARTNER_FEE ),      // POS Partner 2000
        new BetTypeMap( mesConstants.VITAL_ME_VERISIGN_PAYFLOW_LINK , BET_VERISIGN_PAYLINK_FEE ), // Verisign PayLink
        new BetTypeMap( mesConstants.VITAL_ME_VERISIGN_PAYFLOW_PRO  , BET_VERISIGN_PAYFLOW_FEE ), // Verisign PayFlow Pro
//@        new BetTypeMap( mesConstants.VITAL_ME_DIAL_TERMINAL          , BET_ ), // Dial Terminal
//@        new BetTypeMap( mesConstants.VITAL_ME_DIAL_PAY_AUTH_CAPTURE  , BET_ ), // Dial Pay Auth & Capture
//@        new BetTypeMap( mesConstants.VITAL_ME_VIRTUAL_NET            , BET_ ), // Virtual Net
//@        new BetTypeMap( mesConstants.VITAL_ME_ACTIVE_C_V28_SOFTWARE  , BET_ ), // Active C v2.8 Software
//@        new BetTypeMap( mesConstants.VITAL_ME_ACTIVE_C_MANAGED       , BET_ ), // Active C Managed Solution (Hosted)
//@        new BetTypeMap( mesConstants.VITAL_ME_CYBERCASH              , BET_ ), // Cybercash
//@        new BetTypeMap( mesConstants.VITAL_ME_CYBERSOURCE            , BET_ )  // CybersourceOthers TBD
      };
  
  private static BetTypeMap[]           
    MerchantSetupFees = 
      {  
        new BetTypeMap( mesConstants.VITAL_ME_POS_PARTNER           , BET_POS_PARTNER_SETUP_FEE ),      // POS Partner
        new BetTypeMap( mesConstants.VITAL_ME_POS_PARTNER_2000      , BET_POS_PARTNER_SETUP_FEE ),      // POS Partner 2000
        new BetTypeMap( mesConstants.VITAL_ME_VERISIGN_PAYFLOW_LINK , BET_VERISIGN_PAYLINK_SETUP_FEE ), // Verisign PayLink
        new BetTypeMap( mesConstants.VITAL_ME_VERISIGN_PAYFLOW_PRO  , BET_VERISIGN_PAYFLOW_SETUP_FEE ), // Verisign PayFlow Pro
//@        new BetTypeMap( mesConstants.VITAL_ME_DIAL_TERMINAL          , BET_ ), // Dial Terminal
//@        new BetTypeMap( mesConstants.VITAL_ME_DIAL_PAY_AUTH_CAPTURE  , BET_ ), // Dial Pay Auth & Capture
//@        new BetTypeMap( mesConstants.VITAL_ME_VIRTUAL_NET            , BET_ ), // Virtual Net
//@        new BetTypeMap( mesConstants.VITAL_ME_ACTIVE_C_V28_SOFTWARE  , BET_ ), // Active C v2.8 Software
//@        new BetTypeMap( mesConstants.VITAL_ME_ACTIVE_C_MANAGED       , BET_ ), // Active C Managed Solution (Hosted)
//@        new BetTypeMap( mesConstants.VITAL_ME_CYBERCASH              , BET_ ), // Cybercash
//@        new BetTypeMap( mesConstants.VITAL_ME_CYBERSOURCE            , BET_ )  // CybersourceOthers TBD
      };
      
  private static BetTypeMap[]
    PlanFees =
      {
        new BetTypeMap( mesConstants.VITAL_ME_PL_VISA    , groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC ) ),
        new BetTypeMap( mesConstants.VITAL_ME_PL_VSBS    , groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC ) ),
        new BetTypeMap( mesConstants.VITAL_ME_PL_VSDB    , groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC ) ),
        new BetTypeMap( mesConstants.VITAL_ME_PL_VS$     , groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC ) ),
        new BetTypeMap( mesConstants.VITAL_ME_PL_VLGT    , groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC ) ),
        new BetTypeMap( mesConstants.VITAL_ME_PL_MC      , groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC ) ),
        new BetTypeMap( mesConstants.VITAL_ME_PL_MCBS    , groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC ) ),
        new BetTypeMap( mesConstants.VITAL_ME_PL_MCDB    , groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC ) ),
        new BetTypeMap( mesConstants.VITAL_ME_PL_MC$     , groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC ) ),
        new BetTypeMap( mesConstants.VITAL_ME_PL_MLGT    , groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC ) ),
        new BetTypeMap( mesConstants.VITAL_ME_PL_DISC    , BET_DISC_TRANSACTION_FEE    ),
        new BetTypeMap( mesConstants.VITAL_ME_PL_DINERS  , BET_DINERS_TRANSACTION_FEE  ),
        new BetTypeMap( mesConstants.VITAL_ME_PL_JCB     , BET_JCB_TRANSACTION_FEE     ),
        new BetTypeMap( mesConstants.VITAL_ME_PL_AMEX    , BET_AMEX_TRANSACTION_FEE    ),    // Amex transaction fees
        new BetTypeMap( mesConstants.VITAL_ME_PL_DEBIT   , BET_DEBIT_TRANSACTION_FEE   ),
        new BetTypeMap( mesConstants.VITAL_ME_PL_VMC     , BET_PROCESSING_PER_ITEM            ),
//@        new BetTypeMap( mesConstants.VITAL_ME_PL_PL1     , BankContractBean.  ),
//@        new BetTypeMap( mesConstants.VITAL_ME_PL_PL2     , BankContractBean.  ),
//@        new BetTypeMap( mesConstants.VITAL_ME_PL_PL3     , BankContractBean.  ),
      };
      
  private static BetTypeMap[]
    AuthFees =
      {
        new BetTypeMap( mesConstants.VITAL_ME_AP_DISC           , BET_DISC_AUTHORIZATION_FEE ),
        new BetTypeMap( mesConstants.VITAL_ME_AP_AMEX           , BET_AMEX_AUTHORIZATION_FEE ),
        new BetTypeMap( mesConstants.VITAL_ME_AP_DINERS         , BET_DINERS_AUTHORIZATION_FEE ),
        new BetTypeMap( mesConstants.VITAL_ME_AP_CARTE_BLANCHE  , BET_DINERS_AUTHORIZATION_FEE ),
        new BetTypeMap( mesConstants.VITAL_ME_AP_JCB            , BET_JCB_AUTHORIZATION_FEE ),
        new BetTypeMap( mesConstants.VITAL_ME_AP_VISA           , BET_VMC_AUTHORIZATION_FEE ),
        new BetTypeMap( mesConstants.VITAL_ME_AP_MC             , BET_VMC_AUTHORIZATION_FEE ),
      };
      
  private static String[]
    ECommerceProductCodes =
      {
        "VF",
        "VK",
        "VL",
        "VN",
        "VP",
        "CY",
        "TI"      // tias dealers
      };      
      
  public BankContractBean()
  {
  }
  
  static public int betTypeToGroupIndex( int groupIndex )
  {
    //
    // system bet types can be converted to group indexes
    // by converting them to a positive value and subtracting
    // 1 to make them 0 base indexes.
    //
    return( ( (-1 * groupIndex) - 1 ) );
  }
  
  public boolean contractItemExists( int itemType )
  {
    boolean     retVal = false;
    
    for ( int i = 0; i < ReportRows.size(); ++i )
    {
      if ( itemType == ((ContractItem)ReportRows.elementAt(i)).ItemType )
      {
        retVal = true;
        break;
      }
    }
    return( retVal );
  }
  
  public boolean copyContract()
  {
    long         contractNode      = 0L; 
    long         newContractNode   = 0L; 
    Date         validDateBegin    = null;
    boolean      retVal            = false;
    
    if ( fields.isValid() )
    {
      contractNode       = fields.getField("contractNode").asLong();
      newContractNode    = fields.getField("newContractNode").asLong();
      validDateBegin     = ((DateField)fields.getField("validDateBegin")).getSqlDate();
 
      loadContract( contractNode );
     
      if( ReportRows.size() == 0 )
      {
        addError( "No Contract Found for Node ID " + contractNode );
      }
      else
      {
        retVal = storeContract( newContractNode, validDateBegin );        
      }     
    }
    return( retVal );    
  }
  
  public void deleteContractItem( long recordId )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:960^7*/

//  ************************************************************
//  #sql [Ctx] { delete from agent_bank_contract 
//          where rec_id = :recordId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from agent_bank_contract \n        where rec_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recordId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:964^7*/
      /*@lineinfo:generated-code*//*@lineinfo:965^7*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:965^27*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("delete()", e.toString());
      addError("deleteContractItem: " + e.toString());
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    line.append("\"Org Id\",");
    line.append("\"Org Name\",");
    line.append("\"Contract Type\",");
    line.append("\"Item Description\",");
    line.append("\"Product Code\",");
    line.append("\"Begin Date\",");
    line.append("\"End Date\",");
    line.append("\"Billing Months\",");
    line.append("\"Rate/Per Item\",");
    line.append("\"# Exempt Items\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    ContractItem  row = (ContractItem) obj;
  
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    
    line.append( encodeHierarchyNode(row.NodeId) );
    line.append( ",\"" );
    line.append( row.OrgName );
    line.append( "\",\"" );
    line.append( row.ContractDesc );
    line.append( "\",\"" );
    line.append( row.ItemDescription );
    line.append( "\",\"" );
    line.append( processString(row.ProductCode) );
    line.append( "\"," );
    line.append( DateTimeFormatter.getFormattedDate(row.ValidDateBegin,"MM/dd/yyyy") );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(row.ValidDateEnd,"MM/dd/yyyy") );
    line.append( "," );
    line.append( row.BillingMonths );
    line.append( "," );
    line.append( row.ItemRate );
    line.append( "," );
    line.append( row.ExemptItemCount );
  }
  
  // overloaded from base class
  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date beginDate, Date endDate )
  {
    // not calling super because all we want is the orgId 
    // contract type and state.  super class will add the
    // report date range
    buffer.append( ( ( buffer.toString().indexOf('?') < 0 ) ? '?' : '&' ) );
    buffer.append("com.mes.HierarchyNode=");
    buffer.append(nodeId);
    buffer.append("&nodeId=");
    buffer.append(nodeId);
    buffer.append("&state=");
    buffer.append(ReportState);    
    buffer.append("&contractSource=");
    buffer.append(ContractSource);
  }
  
  public ContractItem findContractItemByType( int itemType )
  {
    return( findContractItemByType(itemType,null) );
  }  
  
  public ContractItem findContractItemByType( int itemType, String productCode )
  {
    ContractItem          defaultItem   = null;
    ContractItem          item          = null;
    boolean               productMatch  = false;
    ContractItem          retVal        = null;
    
    for ( int i = 0; i < ReportRows.size(); ++i )
    {
      item = (ContractItem)ReportRows.elementAt(i);
      
      // store the default item for this BET type
      if (itemType == item.ItemType)
      {
        if ( item.ProductCode == null )
        {
          defaultItem = item;
        
          // if we are looking for the default, then
          // store it and exit for loop
          if( productCode == null ) 
          {
            retVal = defaultItem;
            break;
          }
        }
        else if ( (productCode != null) && productCode.equals(item.ProductCode) )
        {
          retVal = item;
          break;
        }
      }
    }
    
    // if no match was found, then
    // return the default BET item
    if ( retVal == null )
    {
      retVal = defaultItem;
    }
    return( retVal );
  }
  
  public ContractItem findContractItemById( long recordId )
  {
    ContractItem          item = null;
    for ( int index = 0; index < ReportRows.size(); ++index )
    {
      if ( recordId == ((ContractItem)ReportRows.elementAt(index)).RecordId )
      {
        item = (ContractItem)ReportRows.elementAt(index);
        break;
      }
    }
    return( item );
  }
  
  public static int getAuthBetType( String cardTypeString )
  {
    int       betType = BET_NONE;
    
    for( int i = 0; i < AuthFees.length; ++i )
    {
      if ( AuthFees[i].FieldName.equals( cardTypeString ) )
      {
        betType = AuthFees[i].BetType;
        break;                                                   
      }
    }
    return( betType );
  }
  
  public ContractItem getContractItem( int index )
  {
    ContractItem      item = null;
    try
    {
      item = (ContractItem)ReportRows.elementAt(index);
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
    }
    return( item );
  }
  
  public int getContractItemCount( )
  {
    return( ReportRows.size() );
  }
  
  public String getContractItemDesc( int itemType )
  {
    String               retVal = "No item found";
    ContractItem         item   = findContractItemByType( itemType );
    
    try
    {
      retVal = item.ItemDescription;
    }
    catch( NullPointerException e )
    {
      // not found
    }
    return( retVal );
  }
  
  public int getContractItemExemptCount( int betType )
  {
    return( getContractItemExemptCount( betType, null ) );
  }
  
  public int getContractItemExemptCount( int betType, String productCode )
  {
    ContractItem  item    = findContractItemByType( betType, productCode );
    int           retVal  = 0;
    
    try
    {
      retVal = item.ExemptItemCount;
    }
    catch( NullPointerException e )
    {
      // not found
    }
    return( retVal );
  }
  
  public double getContractItemRate( int betType )
  {
    return( getContractItemRate( betType, null ) );
  }
  
  public double getContractItemRate( int betType, String productCode )
  {
    ContractItem  item    = findContractItemByType( betType, productCode );
    double        retVal  = 0.0;
    
    try
    {
      retVal = item.ItemRate;
    }
    catch( NullPointerException e )
    {
      // not found
    }
    return( retVal );
  }
  
  public long getContractItemRecordId( int index )
  {
    long               retVal = -1;
    
    try
    {
      retVal = ((ContractItem) ReportRows.elementAt(index)).RecordId;
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
    }
    return( retVal );
  }
  
  public int getContractItemType( int index )
  {
    int               retVal = -1;
    
    try
    {
      retVal = ((ContractItem) ReportRows.elementAt(index)).ItemType;
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
    }
    return( retVal );
  }
  
  public String getContractItemTypeList( )
  {
    // must always return at least one entry so that the
    // query in the JSP will always work (even on an empty contract)
    StringBuffer      itemsList = new StringBuffer("( -1");
    
    for( int i = 0; i < ReportRows.size(); ++i )
    {
      // there will always be at least one entry (-1)
      // so it is necessary to insert a ',' before 
      // the next entry.  
      itemsList.append(",");
      itemsList.append( ((ContractItem)ReportRows.elementAt(i)).ItemType );
    }
    itemsList.append(")");
    
    return( itemsList.toString() );
  }
  
  public long getContractNodeId( )
  {
    return( ContractNode );
  }
  
  public int getContractSource()
  {
    return( ContractSource );
  }
  
  public String getContractSourceDesc()
  {
    String          retVal = "Invalid Contract Source";
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1251^7*/

//  ************************************************************
//  #sql [Ctx] { select contract_desc 
//          from   agent_bank_contract_type
//          where  contract_type = :ContractSource
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select contract_desc  \n        from   agent_bank_contract_type\n        where  contract_type =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.BankContractBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,ContractSource);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1256^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    filename.append("_bank_contract_summary_");
    filename.append( DateTimeFormatter.getFormattedDate( Calendar.getInstance().getTime(),"MMddyy" ) );
    return ( filename.toString() );
  }
  
  public Vector getExcludeMask( )
  {
    return( ExcludeMask );
  }
  
  static public String getGroupName( int groupId )
  {
    return( getGroupName( groupId, BET_GROUP_NAME_LONG ) );
  }
  
  static public String getGroupName( int groupId, int nameType )
  {
    String          retVal = "Undefined";
    
    try
    {
      retVal = BET_GROUP_NAMES[ groupId ][ nameType ];
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
    }
    return( retVal );
  }
  
  static public String getGroupNameShort( int groupId )
  {
    return( getGroupName( groupId, BET_GROUP_NAME_SHORT ) );
  }
  
  public int getItemGroup( int itemType ) 
  {
    int             retVal    = BET_GROUP_COUNT;
    
    for( int groupIndex = 0; groupIndex < BET_GROUP_COUNT; ++groupIndex )
    {
      if ( isItemInGroup( itemType, groupIndex ) == true )
      {
        retVal = groupIndex;
        break;
      }
    }
    return( retVal );
  }
  
  public static int getMonthlyFeeBetType( String productType )
  {
    int         retVal = BET_NONE;
  
    for( int i = 0; i < MerchantMonthlyFees.length; ++i )
    {
      if ( MerchantMonthlyFees[i].FieldName.equals( productType ) )
      {
        retVal = MerchantMonthlyFees[i].BetType;
        break;    // exit for loop                                                   
      }
    }
    return( retVal );
  }
  
  public static int getPlanBetType( String planType )
  {
    int         retVal = BET_NONE;
    
    for( int i = 0; i < PlanFees.length; ++i )
    {
      if ( PlanFees[i].FieldName.equals( planType ) )
      {
        retVal = PlanFees[i].BetType;
        break;                                                   
      }
    }
    return( retVal );
  }
  
  public int getReportState( )
  {
    return( ReportState );
  }
  
  public Vector getRevenueMask( )
  {
    return( RevenueMask );
  }
  
  public static int getSetupFeeBetType( String productType )
  {
    int         retVal = BET_NONE;
  
    for( int i = 0; i < MerchantSetupFees.length; ++i )
    {
      if ( MerchantSetupFees[i].FieldName.equals( productType ) )
      {
        retVal = MerchantSetupFees[i].BetType;
        break;    // exit for loop                                                   
      }
    }
    return( retVal );
  }
  
  static public int groupIndexToBetType( int groupIndex )
  {
    //
    // group indexes can be converted to system Bet Ids
    // by adding 1 and converting them to their negative 
    // counter part.  the additional is necessary because
    // the group indexes are 0 based and all group bet ids
    // need to be both negative and 1 based values.
    //
    return( ( -1 * ( groupIndex + 1 ) ) );
  }
  
  public boolean hasCashAdvanceReferral( )
  {
    int       itemCount = 0;
    boolean   retVal    = false;
    
    try
    {
      if ( ContractSource == ContractTypes.CONTRACT_SOURCE_REFERRAL )
      {
        if ( findContractItemByType(BET_CASH_ADV_TRANSACTION_FEE) != null )
        {
          retVal = true;
        }
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1401^9*/

//  ************************************************************
//  #sql [Ctx] { select count(abc.hierarchy_node) 
//            from   agent_bank_contract abc
//            where  abc.hierarchy_node = :ContractNode and
//                   abc.contract_type  = :ContractTypes.CONTRACT_SOURCE_REFERRAL and
//                   abc.billing_element_type = :BET_CASH_ADV_TRANSACTION_FEE
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(abc.hierarchy_node)  \n          from   agent_bank_contract abc\n          where  abc.hierarchy_node =  :1   and\n                 abc.contract_type  =  :2   and\n                 abc.billing_element_type =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.BankContractBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,ContractNode);
   __sJT_st.setInt(2,ContractTypes.CONTRACT_SOURCE_REFERRAL);
   __sJT_st.setInt(3,BET_CASH_ADV_TRANSACTION_FEE);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1408^9*/
        if ( itemCount > 0 ) 
        {
          retVal = true;
        }
      }        
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "hasCashAdvanceReferral()", e.toString());
      addError( "hasCashAdvanceReferral: " + e.toString());
    }
    return( retVal );
  }
  
  // returns true if there is a liability contract
  // at the same node as this contract node.
  public boolean hasLiabilityContract( )
  {
    int       itemCount = 0;
    boolean   retVal    = false;
    
    try
    {
      if ( ContractSource == ContractTypes.CONTRACT_SOURCE_LIABILITY )
      {
        if ( ReportRows.size() > 0 )
        {
          retVal = true;
        }          
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:1441^9*/

//  ************************************************************
//  #sql [Ctx] { select count(abc.hierarchy_node) 
//            from   agent_bank_contract abc
//            where  abc.hierarchy_node = :ContractNode and
//                   abc.contract_type  = :ContractTypes.CONTRACT_SOURCE_LIABILITY
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(abc.hierarchy_node)  \n          from   agent_bank_contract abc\n          where  abc.hierarchy_node =  :1   and\n                 abc.contract_type  =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.BankContractBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,ContractNode);
   __sJT_st.setInt(2,ContractTypes.CONTRACT_SOURCE_LIABILITY);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1447^9*/
        if ( itemCount > 0 ) 
        {
          retVal = true;
        }
      }        
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "hasLiabilityContract()", e.toString());
      addError( "hasLiabilityContract: " + e.toString());
    }
    return( retVal );
  }
  
  protected void initialize( int contractSource, long hierarchyNode )
  {
    ContractSource  = contractSource;
    ContractNode   = hierarchyNode;
  }
  
  public static boolean isCashAdvanceBetType( int betType )
  {
    boolean         retVal = false;
    
    for ( int i = 0; i < CASH_ADVANCE_BET_GROUP.length; ++i )
    {
      if ( betType == CASH_ADVANCE_BET_GROUP[i] )
      {
        retVal = true;
        break;
      }
    }
    return( retVal );
  }
  
  public boolean isElementEnabled( int betType, Date activeDate )
  {
    return( isElementEnabled(betType,activeDate,null) );
  }
  
  public boolean isElementEnabled( int betType, Date activeDate, String productCode )
  {
    ContractItem    item    = findContractItemByType( betType, productCode );
    boolean         retVal  = false;
    
    try
    {
      String fdata = item.BillingMonths;
      Calendar cal = Calendar.getInstance();
      cal.setTime(activeDate);
      int idx = cal.get(Calendar.MONTH);
      
      if ( fdata != null && idx < fdata.length() )
      {
        fdata = fdata.toUpperCase();
        retVal = (fdata.charAt(idx) == 'Y');
      }
    }
    catch( NullPointerException e )
    {
      // contract item not found
    }
    return( retVal );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    
    if( fileFormat == FF_CSV )
    {
      retVal = true;
    }
    return(retVal);
  }
  
  public boolean isItemExcluded( int betType )
  {
    return( isItemExcluded(betType,null) );
  }
  
  public boolean isItemExcluded( int betType, String productCode )
  {
    ContractItem    item    = findContractItemByType( betType, productCode );
    boolean         retVal  = false;
    
    try
    {
      retVal = item.ExcludeFromProf;
    }
    catch( NullPointerException e )
    {
      // not found
    }
    return( retVal );
  }
  
  public static boolean isItemInGroup( int betType, int[] groupIndexes )
  {
    boolean       retVal      = false;
    
    for( int i = 0; i < groupIndexes.length; ++i )
    {
      if ( isItemInGroup( betType, groupIndexes[i] ) == true )
      {
        retVal = true;
        break;
      }
    }
    return( retVal );
  }
  
  public static boolean isItemInGroup( int betType, int groupIndex )
  {
    int[]           groupIds  = null;
    boolean         retVal    = false;
    
    try
    {
      // bet types of (-1) groupId are always 
      // considered part of the group
      if ( betType == groupIndexToBetType( groupIndex ) )
      {
        retVal = true;
      }
      else
      {
        groupIds = BET_GROUPS[ groupIndex ];
      
        for ( int i = 0; i < groupIds.length; ++i )
        {
          if ( betType == groupIds[i] )
          {
            retVal = true;
            break;
          }
        }
      }
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      // invalid group id
    }
    return( retVal );
  }
  
  public boolean isItemInPartnership( int itemType )
  {
    return( isGroupInPartnership( getItemGroup( itemType ) ) );
  }
  
  public boolean isGroupExcluded( int groupId ) 
  {
    ContractTypes.RevenueMaskItem     maskItem    = null;
    boolean                           retVal      = false;
    
    for( int i = 0; i < ExcludeMask.size(); ++i )
    {
      maskItem = (ContractTypes.RevenueMaskItem)ExcludeMask.elementAt(i);
      
      // ignore mask items that only apply to items to charge records
      // with the specified text contained in the statement msg.
      if ( ( maskItem.isGroupMask() == true ) &&
           ( maskItem.GroupId == groupId ) )
      {
        retVal = true;
        break;
      }          
    }      
    return( retVal );
  }
  
  public boolean isGroupInPartnership( int groupId )
  {
    ContractTypes.RevenueMaskItem     maskItem    = null;
    boolean                           retVal      = true;
    
    if ( groupId == BET_GROUP_PARTNERSHIP )
    {
      retVal = false;     // partnership items are never include in revenue
    }
    else
    {
      for( int i = 0; i < RevenueMask.size(); ++i )
      {
        maskItem = (ContractTypes.RevenueMaskItem)RevenueMask.elementAt(i);
        
        // ignore mask items that only apply to items to charge records
        // with the specified text contained in the statement msg.
        if ( ( maskItem.isGroupMask() == true ) &&
             ( maskItem.GroupId == groupId ) )
        {
          retVal = false;
          break;
        }          
      }
    }      
    return( retVal );
  }
  
  public static boolean isECommerceProduct( String productCode )
  {
    boolean       retVal      = false;
   
    try
    { 
      for( int i = 0; i < ECommerceProductCodes.length; ++i )
      {
        if ( productCode.equals( ECommerceProductCodes[i] ) )
        {
          retVal = true;
          break;
        }
      }
    }
    catch( NullPointerException e )
    {
      // ignore when product code is null
    }
    
    return( retVal );      
  }
  
  public static boolean isSetupBetType( int betType )
  {
    boolean         retVal = false;
    
    for ( int i = 0; i < SETUP_FEES_BET_GROUP.length; ++i )
    {
      if ( betType == SETUP_FEES_BET_GROUP[i] )
      {
        retVal = true;
        break;
      }
    }
    return( retVal );
  }
  
  protected String loadContractItemDesc( int itemType )
  {
    String              retVal      = "Invalid";
    
    try
    {
      // group types are < 0
      if ( itemType < 0 )
      {
        retVal = getGroupName( betTypeToGroupIndex( itemType ) );
      }
      else    // must be a individual element type
      {
        /*@lineinfo:generated-code*//*@lineinfo:1699^9*/

//  ************************************************************
//  #sql [Ctx] { select billing_element_desc 
//            from   billing_element_desc 
//            where  billing_element_type = :itemType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select billing_element_desc  \n          from   billing_element_desc \n          where  billing_element_type =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.BankContractBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,itemType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1704^9*/
      }
    }
    catch( java.sql.SQLException e )
    {
      retVal = "Invalid (" + itemType + ")";
    }
    return( retVal );
  }
  
  public void loadContract( )
  {
    loadContract( getReportOrgId(), ContractSource, null );
  }
  
  public void loadContract( int contractSource )
  {
    // load using the current report org id
    loadContract( getReportOrgId(), contractSource, null );
  }
  
  public void loadContract( long orgId, int contractSource )
  {
    loadContract( orgId, contractSource, null );
  }
  
  public void loadContract( long orgId, int contractSource, Date activeDate )
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    String debug = "";//@
    
    try
    {
      initialize( contractSource, orgIdToHierarchyNode( orgId ) );
     
      reset(); 
      
      debug = "q1";//@
      /*@lineinfo:generated-code*//*@lineinfo:1744^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    abc.rec_id                      as rec_id,
//                    abc.contract_type               as contract_type,
//                    nvl(ct.contract_desc,
//                        'Undefined Type (' ||
//                          abc.contract_type || ')') as contract_desc,
//                    abc.hierarchy_node              as hierarchy_node,
//                    o.org_name                      as org_name,
//                    abc.billing_element_type        as item_type,
//                    bed.billing_element_desc        as item_desc,
//                    abc.product_code                as product_code,
//                    abc.exempt_item_count           as exempt_count,
//                    abc.valid_date_begin            as valid_date_begin,
//                    abc.valid_date_end              as valid_date_end,
//                    decode(abc.exclude_from_prof,
//                           'Y',1,0)                 as exclude,
//                    nvl(abc.billing_months,'YYYYYYYYYYYY')
//                                                    as billing_months,
//                    nvl(bed.element_type,'P')       as rate_type,
//                    abc.rate                        as rate
//          from      agent_bank_contract       abc,
//                    billing_element_desc      bed,
//                    agent_bank_contract_type  ct,
//                    organization          o
//          where     ( ( abc.contract_type  = :contractSource ) or
//                      ( :contractSource = :ContractTypes.CONTRACT_SOURCE_ALL ) ) and
//                    abc.hierarchy_node = :ContractNode and
//                    (
//                      :activeDate is null or
//                      :activeDate between abc.valid_date_begin and abc.valid_date_end
//                    ) and
//                    bed.billing_element_type = abc.billing_element_type and
//                    ct.contract_type(+) = abc.contract_type and
//                    o.org_group(+) = abc.hierarchy_node
//          order by  o.org_name, abc.contract_type, bed.billing_element_desc,
//                    abc.valid_date_end
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    abc.rec_id                      as rec_id,\n                  abc.contract_type               as contract_type,\n                  nvl(ct.contract_desc,\n                      'Undefined Type (' ||\n                        abc.contract_type || ')') as contract_desc,\n                  abc.hierarchy_node              as hierarchy_node,\n                  o.org_name                      as org_name,\n                  abc.billing_element_type        as item_type,\n                  bed.billing_element_desc        as item_desc,\n                  abc.product_code                as product_code,\n                  abc.exempt_item_count           as exempt_count,\n                  abc.valid_date_begin            as valid_date_begin,\n                  abc.valid_date_end              as valid_date_end,\n                  decode(abc.exclude_from_prof,\n                         'Y',1,0)                 as exclude,\n                  nvl(abc.billing_months,'YYYYYYYYYYYY')\n                                                  as billing_months,\n                  nvl(bed.element_type,'P')       as rate_type,\n                  abc.rate                        as rate\n        from      agent_bank_contract       abc,\n                  billing_element_desc      bed,\n                  agent_bank_contract_type  ct,\n                  organization          o\n        where     ( ( abc.contract_type  =  :1   ) or\n                    (  :2   =  :3   ) ) and\n                  abc.hierarchy_node =  :4   and\n                  (\n                     :5   is null or\n                     :6   between abc.valid_date_begin and abc.valid_date_end\n                  ) and\n                  bed.billing_element_type = abc.billing_element_type and\n                  ct.contract_type(+) = abc.contract_type and\n                  o.org_group(+) = abc.hierarchy_node\n        order by  o.org_name, abc.contract_type, bed.billing_element_desc,\n                  abc.valid_date_end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,contractSource);
   __sJT_st.setInt(2,contractSource);
   __sJT_st.setInt(3,ContractTypes.CONTRACT_SOURCE_ALL);
   __sJT_st.setLong(4,ContractNode);
   __sJT_st.setDate(5,activeDate);
   __sJT_st.setDate(6,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.BankContractBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1781^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.add( new ContractItem(resultSet) ); 
      }
      it.close();

      debug = "q2";//@
      /*@lineinfo:generated-code*//*@lineinfo:1791^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rm.income_category    as billing_cat,
//                  rm.charge_rec_prefix  as charge_rec_prefix,
//                  rm.mask_type          as mask_type
//          from    agent_bank_revenue_mask   rm
//          where   rm.hierarchy_node = :ContractNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rm.income_category    as billing_cat,\n                rm.charge_rec_prefix  as charge_rec_prefix,\n                rm.mask_type          as mask_type\n        from    agent_bank_revenue_mask   rm\n        where   rm.hierarchy_node =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ContractNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.reports.BankContractBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1798^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        RevenueMask.addElement( new ContractTypes.RevenueMaskItem(resultSet) );
      }
      resultSet.close();
      it.close();
      
      
      debug = "q3";//@
      /*@lineinfo:generated-code*//*@lineinfo:1810^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ep.expense_category     as billing_cat,
//                  null                    as charge_rec_prefix,
//                  null                    as mask_type
//          from    agent_bank_exclude_prof     ep
//          where   ep.hierarchy_node = :ContractNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ep.expense_category     as billing_cat,\n                null                    as charge_rec_prefix,\n                null                    as mask_type\n        from    agent_bank_exclude_prof     ep\n        where   ep.hierarchy_node =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ContractNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.reports.BankContractBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1817^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ExcludeMask.addElement( new ContractTypes.RevenueMaskItem(resultSet) );
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadContract(" + ContractNode + "," + debug + ")", e.toString());
      addError("loadContract: " + e.toString());
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
  }
  /* RS Changes for ACH PS - Contract Load by passing contract elements not billed for merchant */  
  public void loadContract(long orgId, int contractSource, Date activeDate, long loadSec) {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    String debug = "";
    
    try
    {
      initialize( contractSource, orgIdToHierarchyNode( orgId ) );
     
      reset(); 
      
      debug = "q1";

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.BankContractBean",SQL_AGENT_BANK_CONTRACT);
   // set IN parameters
   __sJT_st.setInt(1,contractSource);
   __sJT_st.setInt(2,contractSource);
   __sJT_st.setInt(3,ContractTypes.CONTRACT_SOURCE_ALL);
   __sJT_st.setLong(4,ContractNode);
   __sJT_st.setDate(5,activeDate);
   __sJT_st.setDate(6,activeDate);
   __sJT_st.setLong(7,loadSec);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.reports.BankContractBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1897^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.add( new ContractItem(resultSet) ); 
      }
      it.close();

      debug = "q2";
      /*@lineinfo:generated-code*//*@lineinfo:1907^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rm.income_category    as billing_cat,
//                  rm.charge_rec_prefix  as charge_rec_prefix,
//                  rm.mask_type          as mask_type
//          from    agent_bank_revenue_mask   rm
//          where   rm.hierarchy_node = :ContractNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rm.income_category    as billing_cat,\n                rm.charge_rec_prefix  as charge_rec_prefix,\n                rm.mask_type          as mask_type\n        from    agent_bank_revenue_mask   rm\n        where   rm.hierarchy_node =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ContractNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.reports.BankContractBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1914^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        RevenueMask.addElement( new ContractTypes.RevenueMaskItem(resultSet) );
      }
      resultSet.close();
      it.close();
      
      
      debug = "q3";
      /*@lineinfo:generated-code*//*@lineinfo:1926^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ep.expense_category     as billing_cat,
//                  null                    as charge_rec_prefix,
//                  null                    as mask_type
//          from    agent_bank_exclude_prof     ep
//          where   ep.hierarchy_node = :ContractNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ep.expense_category     as billing_cat,\n                null                    as charge_rec_prefix,\n                null                    as mask_type\n        from    agent_bank_exclude_prof     ep\n        where   ep.hierarchy_node =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,ContractNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.reports.BankContractBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1933^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ExcludeMask.addElement( new ContractTypes.RevenueMaskItem(resultSet) );
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadContract(" + ContractNode + "," + debug + ")", e.toString());
      addError("loadContract: " + e.toString());
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
	
}
  
  
  public void loadData( )
  {
    ResultSetIterator   it          = null;
    long                nodeId      = 0L;
    ResultSet           resultSet   = null;
    
    try
    {
      ReportRows.removeAllElements();
      
      nodeId = getReportHierarchyNode();
      
      /*@lineinfo:generated-code*//*@lineinfo:1968^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    abc.rec_id                      as rec_id,
//                    abc.contract_type               as contract_type,
//                    nvl(ct.contract_desc,
//                        'Undefined Type (' ||
//                          abc.contract_type || ')') as contract_desc,
//                    abc.hierarchy_node              as hierarchy_node,
//                    o.org_name                      as org_name,
//                    abc.billing_element_type        as item_type,
//                    bed.billing_element_desc        as item_desc,
//                    abc.product_code                as product_code,
//                    abc.exempt_item_count           as exempt_count,
//                    abc.valid_date_begin            as valid_date_begin,
//                    abc.valid_date_end              as valid_date_end,
//                    decode(abc.exclude_from_prof,
//                            'Y',1,0)                as exclude,
//                    nvl(abc.billing_months,'YYYYYYYYYYYY')
//                                                    as billing_months,
//                    nvl(bed.element_type,'P')       as rate_type,
//                    abc.rate                        as rate
//          from      t_hierarchy               th,
//                    agent_bank_contract       abc,
//                    billing_element_desc      bed,
//                    agent_bank_contract_type  ct,
//                    organization              o
//          where     th.hier_type = 1 and
//                    th.ancestor = :nodeId and
//                    abc.hierarchy_node = th.descendent and
//                    ct.contract_type(+) = abc.contract_type and
//                    bed.billing_element_type = abc.billing_element_type and
//                    o.org_group(+) = abc.hierarchy_node
//          order by  o.org_name, abc.hierarchy_node,
//                    contract_desc, bed.billing_element_desc,
//                    product_code, abc.valid_date_end      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    abc.rec_id                      as rec_id,\n                  abc.contract_type               as contract_type,\n                  nvl(ct.contract_desc,\n                      'Undefined Type (' ||\n                        abc.contract_type || ')') as contract_desc,\n                  abc.hierarchy_node              as hierarchy_node,\n                  o.org_name                      as org_name,\n                  abc.billing_element_type        as item_type,\n                  bed.billing_element_desc        as item_desc,\n                  abc.product_code                as product_code,\n                  abc.exempt_item_count           as exempt_count,\n                  abc.valid_date_begin            as valid_date_begin,\n                  abc.valid_date_end              as valid_date_end,\n                  decode(abc.exclude_from_prof,\n                          'Y',1,0)                as exclude,\n                  nvl(abc.billing_months,'YYYYYYYYYYYY')\n                                                  as billing_months,\n                  nvl(bed.element_type,'P')       as rate_type,\n                  abc.rate                        as rate\n        from      t_hierarchy               th,\n                  agent_bank_contract       abc,\n                  billing_element_desc      bed,\n                  agent_bank_contract_type  ct,\n                  organization              o\n        where     th.hier_type = 1 and\n                  th.ancestor =  :1   and\n                  abc.hierarchy_node = th.descendent and\n                  ct.contract_type(+) = abc.contract_type and\n                  bed.billing_element_type = abc.billing_element_type and\n                  o.org_group(+) = abc.hierarchy_node\n        order by  o.org_name, abc.hierarchy_node,\n                  contract_desc, bed.billing_element_desc,\n                  product_code, abc.valid_date_end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.reports.BankContractBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2003^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.add( new ContractItem(resultSet) ); 
      }
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadData()", e.toString());
      addError("loadData: " + e.toString());
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
  }
  
  public String loadDebitCatDesc( String catCode )
  {
    String            retVal    = "Cat " + catCode;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2029^7*/

//  ************************************************************
//  #sql [Ctx] { select  product_desc 
//          from    ic_category_desc icd
//          where   icd.card_type = 'DB' and
//                  icd.category = :catCode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  product_desc  \n        from    ic_category_desc icd\n        where   icd.card_type = 'DB' and\n                icd.category =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.reports.BankContractBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,catCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2035^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  public ContractItem loadContractRecord( long recId )
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    ContractItem        retVal      = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2051^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    abc.rec_id                      as rec_id,
//                    abc.contract_type               as contract_type,
//                    nvl(ct.contract_desc,
//                        'Undefined Type (' ||
//                          abc.contract_type || ')') as contract_desc,
//                    abc.hierarchy_node              as hierarchy_node,
//                    o.org_name                      as org_name,
//                    abc.billing_element_type        as item_type,
//                    bed.billing_element_desc        as item_desc,
//                    abc.product_code                as product_code,
//                    abc.exempt_item_count           as exempt_count,
//                    abc.valid_date_begin            as valid_date_begin,
//                    abc.valid_date_end              as valid_date_end,
//                    decode(abc.exclude_from_prof,
//                            'Y',1,0)                as exclude,
//                    nvl(abc.billing_months,'YYYYYYYYYYYY')
//                                                    as billing_months,
//                    nvl(bed.element_type,'P')       as rate_type,
//                    abc.rate                        as rate
//          from      t_hierarchy               th,
//                    agent_bank_contract       abc,
//                    billing_element_desc      bed,
//                    agent_bank_contract_type  ct,
//                    organization              o
//          where     abc.rec_id = :recId and
//                    ct.contract_type(+) = abc.contract_type and
//                    bed.billing_element_type = abc.billing_element_type and
//                    o.org_group(+) = abc.hierarchy_node
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    abc.rec_id                      as rec_id,\n                  abc.contract_type               as contract_type,\n                  nvl(ct.contract_desc,\n                      'Undefined Type (' ||\n                        abc.contract_type || ')') as contract_desc,\n                  abc.hierarchy_node              as hierarchy_node,\n                  o.org_name                      as org_name,\n                  abc.billing_element_type        as item_type,\n                  bed.billing_element_desc        as item_desc,\n                  abc.product_code                as product_code,\n                  abc.exempt_item_count           as exempt_count,\n                  abc.valid_date_begin            as valid_date_begin,\n                  abc.valid_date_end              as valid_date_end,\n                  decode(abc.exclude_from_prof,\n                          'Y',1,0)                as exclude,\n                  nvl(abc.billing_months,'YYYYYYYYYYYY')\n                                                  as billing_months,\n                  nvl(bed.element_type,'P')       as rate_type,\n                  abc.rate                        as rate\n        from      t_hierarchy               th,\n                  agent_bank_contract       abc,\n                  billing_element_desc      bed,\n                  agent_bank_contract_type  ct,\n                  organization              o\n        where     abc.rec_id =  :1   and\n                  ct.contract_type(+) = abc.contract_type and\n                  bed.billing_element_type = abc.billing_element_type and\n                  o.org_group(+) = abc.hierarchy_node";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.reports.BankContractBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2081^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        retVal = new ContractItem(resultSet);
      }        
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadContractRecord()", e.toString());
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
    return( retVal );
  }
  
  protected void loadEditRecord( long editId )
  {
    ResultSetIterator   it          = null;
    ResultSet           resultSet   = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2109^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    abc.rec_id                      as rec_id,
//                    abc.contract_type               as contract_type,
//                    abc.rate                        as rate,
//                    abc.hierarchy_node              as contract_node,
//                    abc.billing_element_type        as item_type,
//                    bed.billing_element_desc        as item_desc,
//                    abc.product_code                as product_code,
//                    abc.exempt_item_count           as exempt_count,
//                    abc.valid_date_begin            as valid_date_begin,
//                    abc.valid_date_end              as valid_date_end,
//                    nvl(abc.billing_months,'YYYYYYYYYYYY')
//                                                    as billing_months
//          from      agent_bank_contract abc,
//                    billing_element_desc bed
//          where     abc.rec_id = :editId and
//                    bed.billing_element_type = abc.billing_element_type 
//          order by  abc.contract_type, bed.billing_element_desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    abc.rec_id                      as rec_id,\n                  abc.contract_type               as contract_type,\n                  abc.rate                        as rate,\n                  abc.hierarchy_node              as contract_node,\n                  abc.billing_element_type        as item_type,\n                  bed.billing_element_desc        as item_desc,\n                  abc.product_code                as product_code,\n                  abc.exempt_item_count           as exempt_count,\n                  abc.valid_date_begin            as valid_date_begin,\n                  abc.valid_date_end              as valid_date_end,\n                  nvl(abc.billing_months,'YYYYYYYYYYYY')\n                                                  as billing_months\n        from      agent_bank_contract abc,\n                  billing_element_desc bed\n        where     abc.rec_id =  :1   and\n                  bed.billing_element_type = abc.billing_element_type \n        order by  abc.contract_type, bed.billing_element_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,editId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.reports.BankContractBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2128^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        // use the static method in the forms.FormBean to 
        // extract the data from the result set.
        FieldBean.setFields(fields,resultSet,false);
      }        
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("loadEditRecord()", e.toString());
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
  }
  
  public String loadProductDesc( String productCode )
  {
    String            retVal    = "Product Code: " + productCode;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2155^7*/

//  ************************************************************
//  #sql [Ctx] { select  product_desc 
//          from    agent_bank_product_codes  
//          where   product_code = :productCode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  product_desc  \n        from    agent_bank_product_codes  \n        where   product_code =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.reports.BankContractBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,productCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2160^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  /*
  ** METHOD register()
  **
  ** registers this user with the connection
  */
  public void register(String user, HttpServletRequest request)
  {
    MesSystem.registerRequest(this, user, request);
  }
  
  public void reset( )
  {
    try
    {
        // empty the vectors
      ReportRows.removeAllElements();
      RevenueMask.removeAllElements();
      ExcludeMask.removeAllElements();
    }
    finally
    {
    }      
  }
  
  public void setContractItemRate( long recordId, double newRate )
  {
    ContractItem      item    = null;
    
    if ( newRate != -1.0 )
    {
      item = findContractItemById( recordId );
      
      if ( item != null )
      {
        item.ItemRate = newRate;
      }
    }      
  }
  
  public void setProperties(HttpServletRequest request)
  {
    boolean    addRec             = false;
    long       editRecId          = -1L;
    boolean    saveRec            = false;
    boolean    saveCopy           = false;
    boolean    addContractNode    = false;
    boolean    copyContractNode   = false;
    
    super.setProperties(request);
    
    ReportState      = HttpHelper.getInt(request,"state",0);
    ContractSource   = HttpHelper.getInt(request,"contractSource",ContractTypes.CONTRACT_SOURCE_ALL);
    ShowingInactive  = HttpHelper.getBoolean(request,"showInactive",false);    
    addRec           = HttpHelper.getBoolean(request,"addRec",false);
    editRecId        = HttpHelper.getLong(request, "editId", -1L);
    saveRec          = HttpHelper.getString(request,"Submit","null").equals("Save");    
    addContractNode  = HttpHelper.getString(request,"task","null").equals("addContractNode");
    copyContractNode = HttpHelper.getString(request,"task","null").equals("copyContractNode");
    saveCopy         = HttpHelper.getString(request,"Submit","null").equals("Copy");

    // create the input fields if adding or editing
    try
    {
      Calendar cal = Calendar.getInstance();
      cal.set(Calendar.DAY_OF_MONTH,1);
      fields.add(new DateField("validDateBegin",false));
      ((DateField)fields.getField("validDateBegin")).setDateFormat("MM/dd/yyyy");
      ((DateField)fields.getField("validDateBegin")).setUtilDate(cal.getTime());

      if ( addRec || saveRec || addContractNode || (editRecId != -1L) )
      {
        fields.add( new DropDownField("contractNode", new ContractNodesTable(),false) );
        fields.add( new HiddenField("recId"));
        fields.add( new Field("productCode",2,2,true) );
        fields.add( new Field("billingMonths",12,15,true) );
        fields.add( new NumberField("exemptCount",10,10,false,0) );
        fields.add( new NumberField("rate",10,10,false,4) );
        fields.add( new DropDownField("itemType", new ContractItemsTable(),false) );
        fields.add( new DropDownField("contractType", new ContractTypesTable(),false) );
        
        fields.getField("billingMonths").addValidation( new BillingMonthsValidation() );
        fields.setData("billingMonths","YYYYYYYYYYYY");

        if ( addRec || addContractNode )
        {
          fields.getField("recId").setData(Long.toString(REC_ID_NEW));
        }
        
        setFields(request);
        
        if ( editRecId != -1L )
        {
          loadEditRecord(editRecId);
        }
      }
      else if( copyContractNode || saveCopy )
      {
        BankContractTable table = new BankContractTable();
        fields.add( new DropDownField("contractNode", table,false) );
        if( table.getContractNodeCount() == 1 )
        {
          fields.getField("contractNode").setData(table.getContractNode()); 
          fields.getField("contractNode").makeReadOnly();          
        }
        
        fields.add( new NumberField("newContractNode",10,10,false,0) );
        fields.getField("newContractNode").addValidation( new NodeIdValidation() );        
      }
      
      fields.addHtmlExtra("class=\"formFields\"");
    }
    catch( java.sql.SQLException e )
    {
      logEntry("setProperties()", e.toString() );
    }        
    setFields( request );
  }
  
  public boolean showingInactiveRates()
  {
    return( ShowingInactive );
  }
  
  
  public boolean storeData( )
  {
    String              billingMonths   = null;
    long                contractNode    = 0L;
    int                 contractType    = 0;
    int                 exemptCount     = 0;
    ResultSetIterator   it              = null;
    int                 itemType        = 0;
    String              productCode     = null;
    double              rate            = 0.0;
    int                 recCount        = 0;
    long                recId           = 0L;
    ResultSet           resultSet       = null;
    boolean             retVal          = false;
    Date                tempDate        = null;
    Date                validDateBegin  = null;
    
    try
    {
      if ( fields.isValid() )
      {
        recId           = fields.getField("recId").asLong();
        rate            = fields.getField("rate").asDouble();
        contractType    = fields.getField("contractType").asInteger();
        contractNode    = fields.getField("contractNode").asLong();
        itemType        = fields.getField("itemType").asInteger();
        productCode     = fields.getField("productCode").getData();
        billingMonths   = fields.getField("billingMonths").getData();
        exemptCount     = fields.getField("exemptCount").asInteger();
        validDateBegin  = ((DateField)fields.getField("validDateBegin")).getSqlDate();
      
        if ( recId == REC_ID_NEW )
        {
          /*@lineinfo:generated-code*//*@lineinfo:2325^11*/

//  ************************************************************
//  #sql [Ctx] { select  agent_bank_contract_sequence.nextval  
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  agent_bank_contract_sequence.nextval   \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.reports.BankContractBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2329^11*/
        
          /*@lineinfo:generated-code*//*@lineinfo:2331^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  abc.rec_id
//              from    agent_bank_contract abc
//              where   abc.hierarchy_node = :contractNode and
//                      abc.contract_type = :contractType and
//                      abc.billing_element_type = :itemType and
//                      nvl(abc.product_code,'null') = nvl(:productCode,'null') and
//                      abc.valid_date_begin < :validDateBegin and
//                      abc.valid_date_end = '31-DEC-9999'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  abc.rec_id\n            from    agent_bank_contract abc\n            where   abc.hierarchy_node =  :1   and\n                    abc.contract_type =  :2   and\n                    abc.billing_element_type =  :3   and\n                    nvl(abc.product_code,'null') = nvl( :4  ,'null') and\n                    abc.valid_date_begin <  :5   and\n                    abc.valid_date_end = '31-DEC-9999'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,contractNode);
   __sJT_st.setInt(2,contractType);
   __sJT_st.setInt(3,itemType);
   __sJT_st.setString(4,productCode);
   __sJT_st.setDate(5,validDateBegin);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.reports.BankContractBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2341^11*/
          resultSet = it.getResultSet();
          
          while( resultSet.next() )
          {
            recId = resultSet.getLong("rec_id");
            /*@lineinfo:generated-code*//*@lineinfo:2347^13*/

//  ************************************************************
//  #sql [Ctx] { update  agent_bank_contract abc
//                set     abc.valid_date_end = (trunc(:validDateBegin,'month')-1)
//                where   abc.rec_id = :recId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  agent_bank_contract abc\n              set     abc.valid_date_end = (trunc( :1  ,'month')-1)\n              where   abc.rec_id =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,validDateBegin);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2352^13*/
          }
          resultSet.close();
          it.close();
                  
          /*@lineinfo:generated-code*//*@lineinfo:2357^11*/

//  ************************************************************
//  #sql [Ctx] { insert into agent_bank_contract 
//              ( 
//                rec_id,
//                contract_type, 
//                hierarchy_node, 
//                billing_element_type, 
//                product_code,
//                billing_months,
//                rate, 
//                exempt_item_count,
//                valid_date_begin, 
//                valid_date_end
//              ) 
//              values 
//              ( 
//                null,
//                :contractType,
//                :contractNode,
//                :itemType,
//                :productCode,
//                upper(:billingMonths),
//                :rate,
//                :exemptCount,
//                trunc(:validDateBegin,'month'),
//                '31-DEC-9999'
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into agent_bank_contract \n            ( \n              rec_id,\n              contract_type, \n              hierarchy_node, \n              billing_element_type, \n              product_code,\n              billing_months,\n              rate, \n              exempt_item_count,\n              valid_date_begin, \n              valid_date_end\n            ) \n            values \n            ( \n              null,\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n              upper( :5  ),\n               :6  ,\n               :7  ,\n              trunc( :8  ,'month'),\n              '31-DEC-9999'\n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,contractType);
   __sJT_st.setLong(2,contractNode);
   __sJT_st.setInt(3,itemType);
   __sJT_st.setString(4,productCode);
   __sJT_st.setString(5,billingMonths);
   __sJT_st.setDouble(6,rate);
   __sJT_st.setInt(7,exemptCount);
   __sJT_st.setDate(8,validDateBegin);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2385^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:2389^11*/

//  ************************************************************
//  #sql [Ctx] { select  valid_date_begin 
//              from    agent_bank_contract   abc
//              where   abc.rec_id = :recId 
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  valid_date_begin  \n            from    agent_bank_contract   abc\n            where   abc.rec_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.reports.BankContractBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   tempDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2394^11*/
        
          // determine if the user has change the begin date
          if ( !tempDate.equals( validDateBegin ) )
          {
            /*@lineinfo:generated-code*//*@lineinfo:2399^13*/

//  ************************************************************
//  #sql [Ctx] { update  agent_bank_contract abc 
//                set     abc.valid_date_end = (trunc(:validDateBegin,'month')-1)
//                where   abc.hierarchy_node = :contractNode and
//                        abc.contract_type = :contractType and
//                        abc.billing_element_type = :itemType and
//                        abc.product_code = :productCode and
//                        abc.valid_date_end = (trunc(:tempDate,'month')-1)
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  agent_bank_contract abc \n              set     abc.valid_date_end = (trunc( :1  ,'month')-1)\n              where   abc.hierarchy_node =  :2   and\n                      abc.contract_type =  :3   and\n                      abc.billing_element_type =  :4   and\n                      abc.product_code =  :5   and\n                      abc.valid_date_end = (trunc( :6  ,'month')-1)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,validDateBegin);
   __sJT_st.setLong(2,contractNode);
   __sJT_st.setInt(3,contractType);
   __sJT_st.setInt(4,itemType);
   __sJT_st.setString(5,productCode);
   __sJT_st.setDate(6,tempDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2408^13*/
          }
        
          /*@lineinfo:generated-code*//*@lineinfo:2411^11*/

//  ************************************************************
//  #sql [Ctx] { update  agent_bank_contract abc
//              set     abc.rate              = :rate,
//                      abc.product_code      = :productCode,
//                      abc.valid_date_begin  = :validDateBegin,
//                      abc.exempt_item_count = :exemptCount,
//                      abc.billing_months    = upper(:billingMonths)
//              where   abc.rec_id = :recId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  agent_bank_contract abc\n            set     abc.rate              =  :1  ,\n                    abc.product_code      =  :2  ,\n                    abc.valid_date_begin  =  :3  ,\n                    abc.exempt_item_count =  :4  ,\n                    abc.billing_months    = upper( :5  )\n            where   abc.rec_id =  :6 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,rate);
   __sJT_st.setString(2,productCode);
   __sJT_st.setDate(3,validDateBegin);
   __sJT_st.setInt(4,exemptCount);
   __sJT_st.setString(5,billingMonths);
   __sJT_st.setLong(6,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2420^11*/
        }
        retVal = true;
      }
    }
    catch(Exception e)
    {
      logEntry( "storeData()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
    return( retVal );
  }
  
  public void loadContract( long nodeId )
  {
    ResultSet           resultSet;
    ResultSetIterator   it              = null;

    try
    {
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:2445^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  abc.rec_id                 as rec_id,
//                  abc.hierarchy_node         as hierarchy_node,
//                  abc.contract_type          as contract_type,
//                  abc.billing_element_type   as item_type,
//                  abc.valid_date_begin       as valid_date_begin,
//                  abc.valid_date_end         as valid_date_end,
//                  abc.rate                   as rate,
//                  abc.product_code           as product_code,
//                  abc.exempt_item_count      as exempt_count,
//                  abc.exclude_from_prof      as exclude,
//                  abc.billing_months         as billing_months,
//                  null                       as contract_desc,
//                  null                       as org_name,
//                  null                       as item_desc,
//                  null                       as rate_type
//          from    agent_bank_contract abc
//          where   abc.hierarchy_node = :nodeId 
//                  and abc.valid_date_end = '31-DEC-9999'
//          order by abc.rec_id asc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  abc.rec_id                 as rec_id,\n                abc.hierarchy_node         as hierarchy_node,\n                abc.contract_type          as contract_type,\n                abc.billing_element_type   as item_type,\n                abc.valid_date_begin       as valid_date_begin,\n                abc.valid_date_end         as valid_date_end,\n                abc.rate                   as rate,\n                abc.product_code           as product_code,\n                abc.exempt_item_count      as exempt_count,\n                abc.exclude_from_prof      as exclude,\n                abc.billing_months         as billing_months,\n                null                       as contract_desc,\n                null                       as org_name,\n                null                       as item_desc,\n                null                       as rate_type\n        from    agent_bank_contract abc\n        where   abc.hierarchy_node =  :1   \n                and abc.valid_date_end = '31-DEC-9999'\n        order by abc.rec_id asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"28com.mes.reports.BankContractBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2466^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        ReportRows.add( new ContractItem(resultSet) ); 
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry( "loadContract(" + nodeId + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  public boolean storeContract( long nodeId, Date activeDate )
  {
    ContractItem         item       = null;
    boolean              retVal     = false;
    long                 recId      = 0L;
    ResultSetIterator    it         = null;
    
    try
    {
      for ( int i = 0; i < ReportRows.size(); ++i )
      {
        item = (ContractItem)ReportRows.elementAt(i);

        /*@lineinfo:generated-code*//*@lineinfo:2499^9*/

//  ************************************************************
//  #sql [Ctx] { select  agent_bank_contract_sequence.nextval  
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  agent_bank_contract_sequence.nextval   \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.reports.BankContractBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2503^9*/

        /*@lineinfo:generated-code*//*@lineinfo:2505^9*/

//  ************************************************************
//  #sql [Ctx] { insert into agent_bank_contract 
//            (
//              rec_id,
//              contract_type, 
//              hierarchy_node, 
//              billing_element_type, 
//              product_code,
//              rate, 
//              exempt_item_count,
//              valid_date_begin, 
//              valid_date_end,
//              billing_months
//            ) 
//            values 
//            ( 
//              :recId,
//              :item.ContractType,
//              :nodeId,
//              :item.ItemType,
//              :item.ProductCode,
//              :item.ItemRate,
//              :item.ExemptItemCount,
//              trunc(:activeDate,'month'),
//              :item.ValidDateEnd,
//              :item.BillingMonths
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into agent_bank_contract \n          (\n            rec_id,\n            contract_type, \n            hierarchy_node, \n            billing_element_type, \n            product_code,\n            rate, \n            exempt_item_count,\n            valid_date_begin, \n            valid_date_end,\n            billing_months\n          ) \n          values \n          ( \n             :1  ,\n             :2  ,\n             :3  ,\n             :4  ,\n             :5  ,\n             :6  ,\n             :7  ,\n            trunc( :8  ,'month'),\n             :9  ,\n             :10  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"30com.mes.reports.BankContractBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setInt(2,item.ContractType);
   __sJT_st.setLong(3,nodeId);
   __sJT_st.setInt(4,item.ItemType);
   __sJT_st.setString(5,item.ProductCode);
   __sJT_st.setDouble(6,item.ItemRate);
   __sJT_st.setInt(7,item.ExemptItemCount);
   __sJT_st.setDate(8,activeDate);
   __sJT_st.setDate(9,item.ValidDateEnd);
   __sJT_st.setString(10,item.BillingMonths);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2533^9*/
      }
      /*@lineinfo:generated-code*//*@lineinfo:2535^7*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2535^27*/
      
      retVal = true;
    }
    catch(Exception e)
    {
      addError("storeContract(" + nodeId + ") " + e.toString());
      logEntry( "storeContract(" + nodeId + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
    return retVal;
  }
  
  public static void main( String[] args )
  {
    double                authRate = 0.0;
    String                cardType = null;
    BankContractBean      dataBean = new BankContractBean();
    String                productCode = null;
    
    try
    {
      try{ cardType = args[0]; } catch(Exception e) {}
      try{ productCode = args[1]; } catch(Exception e) {}
      
//@      dataBean.loadData( dataBean.hierarchyNodeToOrgId(3941400017L), ContractTypes.CONTRACT_SOURCE_LIABILITY );

      authRate = dataBean.getContractItemRate( dataBean.getAuthBetType(cardType), productCode );
      
      System.out.println("auth per item (" + cardType + "," + productCode + "): " + authRate ); 
    }
    finally
    {
      dataBean.cleanUp();
      com.mes.database.OracleConnectionPool.getInstance().cleanUp();
    }
  }
}/*@lineinfo:generated-code*/