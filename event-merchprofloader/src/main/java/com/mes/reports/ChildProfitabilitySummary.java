/*@lineinfo:filename=ChildProfitabilitySummary*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/ChildProfitabilitySummary.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-04-05 17:22:51 -0700 (Tue, 05 Apr 2011) $
  Version            : $Revision: 18675 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.util.Calendar;
import java.util.Vector;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ref.DefaultContext;

public class ChildProfitabilitySummary implements Comparable
{
  protected DefaultContext  Ctx                         = null;
  public Vector[][]         ContractElements            = null;
  
  // vital merchant totals
  protected boolean         DirectMerchDataPresent      = false;
  protected double          DirectMerchVMCSalesAmount   = 0.0;
  protected int             DirectMerchVMCSalesCount    = 0;
  protected double          DirectMerchVMCCreditsAmount = 0.0;
  protected int             DirectMerchVMCCreditsCount  = 0;
  public double             MerchNetIncome              = 0.0;
  public double             MerchTotalIncome            = 0.0;
  public double             MerchTotalExpense           = 0.0;
  
  // this objects org data
  protected long            AssocNumber                 = 0L;
  protected long            HierarchyNode               = 0L;
  public long               OrgId                       = -1L;
  public String             OrgName                     = "Unitialized";
  protected Date            ReportDateBegin             = null;
  protected Date            ReportDateEnd               = null;

  public ChildProfitabilitySummary( DefaultContext ctx, long orgId, String orgName, Date beginDate, Date endDate )
  {
    initialize( ctx, orgId, orgName, beginDate, endDate );
  }

  public ChildProfitabilitySummary( DefaultContext ctx, long orgId, Date beginDate, Date endDate )
  {
    initialize( ctx, orgId, null, beginDate, endDate );
  }

  public void addContractElement( int dataType, int contractType, ContractTypes.PerItemRecord record )
  {
    try
    {
      //
      // if the record does not have a description, then
      // load the default description for this type of
      // record into the description field.
      //
      if ( record.getDescription() == null )
      {
        StringBuffer      desc = new StringBuffer();
        
        desc.append( loadContractItemDesc( record.getBetType() ) );
        desc.append( " " );
        desc.append( DateTimeFormatter.getFormattedDate( record.ActiveDate, "MMM yyyy" ) );
        record.setDescription(desc.toString());
      }
      ContractElements[dataType][contractType].add( record );
    }
    catch( Exception e )
    {
      logEntry(  "addContractElement: ", e.toString());
    }      
  }
  
  public int compareTo( Object obj )
  {
    ChildProfitabilitySummary   compareObj    = (ChildProfitabilitySummary)obj;
    double                      diff          = 0.0;
    int                         retVal        = 0;
    
    
    diff = (getNetProfitLoss() - compareObj.getNetProfitLoss());
    
    if ( diff == 0.0 )
    {
      if ( (retVal = OrgName.compareTo(compareObj.OrgName)) == 0 )
      {
        if ( (retVal = (int)(HierarchyNode - compareObj.HierarchyNode)) == 0 )
        {
          retVal = (int)(OrgId - compareObj.OrgId);
        }
      }        
    }
    else      // amount is different
    {
      retVal = ( diff < 0.0 ) ? -1 : 1;
    }
    return(retVal);
  }
  
  public double getAdjustmentsAmount( int contractType )
  {
    Vector                          contractElements    = null;
    ContractTypes.PerItemRecord     item                = null;
    double                          retVal              = 0.0;
  
    try
    {
      for( int dataType = 0; dataType < ContractTypes.CONTRACT_DATA_COUNT; ++dataType )
      {
        contractElements = ContractElements[ dataType ][ contractType ];
        for( int i = 0; i < contractElements.size(); ++i )
        {
          item = (ContractTypes.PerItemRecord)contractElements.elementAt(i);
          if ( BankContractBean.isItemInGroup( item.getBetType(), BankContractBean.BET_GROUP_ADJUSTMENTS ) )
          {
            retVal += item.getAmount();
          }
        }
      }
    }
    catch( Exception e )
    {
      logEntry(  "getAdjustmentsAmount(" + contractType + "): ", e.toString());
    }
    return( retVal );
  }
  
  public double getAdjustmentsExpense( )
  {
    return( getAdjustmentsAmount( ContractTypes.CONTRACT_TYPE_EXPENSE ) );
  }
  
  public double getAdjustmentsIncome( )
  {
    return( getAdjustmentsAmount( ContractTypes.CONTRACT_TYPE_INCOME ) );
  }
  
  private double getAppMonthlySales()
  {
    double      retVal = 0.0;
    
    try
    {
      // get the annual sales from the online app
      /*@lineinfo:generated-code*//*@lineinfo:162^7*/

//  ************************************************************
//  #sql [Ctx] { select (decode( mr.MERCH_MONTH_VISA_MC_SALES, null, 0, mr.MERCH_MONTH_VISA_MC_SALES )) 
//          from   merchant mr,
//                 orgmerchant om
//          where  om.org_num = :OrgId and
//                 mr.merch_number = om.org_merchant_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select (decode( mr.MERCH_MONTH_VISA_MC_SALES, null, 0, mr.MERCH_MONTH_VISA_MC_SALES ))  \n        from   merchant mr,\n               orgmerchant om\n        where  om.org_num =  :1  and\n               mr.merch_number = om.org_merchant_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.ChildProfitabilitySummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,OrgId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:169^7*/
    }
    catch( Exception e )
    {
    }
    return( retVal );      
  }
  
  public long getAssocNumber ( )
  {
    return( AssocNumber );
  }
  
  public double getAvgNetDiscount( )
  {
    double        retVal      = ( getDiscIcIncome() - getDiscIcExpense() );
    int           totalCount  = getDirectMerchVMCTotalCount();
    
    if ( totalCount > 1 )
    {
      retVal = ( retVal / totalCount );
    }
    return( retVal );
  }
  
  private double getContractExpense( )
  {
    Vector                        contractElements  = null;
    ContractTypes.PerItemRecord   item              = null;
    double                        retVal            = 0.0;
  
    try
    {
      contractElements = ContractElements[ContractTypes.CONTRACT_DATA_MES][ContractTypes.CONTRACT_TYPE_EXPENSE];
      for( int i = 0; i < contractElements.size(); ++i )
      {
        item = (ContractTypes.PerItemRecord)contractElements.elementAt(i);
        retVal += item.getAmount();
      }
    }
    catch( Exception e )
    {
      logEntry( "getContractExpense()", e.toString());
    }
    return( retVal );
  }
  
  public Vector getDataElements( int dataType, int contractType )
  {
    Vector      elements = null;
    try
    {
      elements = ContractElements[dataType][contractType];
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
    }
    return( elements );
  }
  
  public double getDirectMerchVMCCreditsAmount( )
  {
    return( DirectMerchVMCCreditsAmount );
  }
  
  public int getDirectMerchVMCCreditsCount( )
  {
    return( DirectMerchVMCCreditsCount );
  }
  
  public double getDirectMerchVMCNetAmount( )
  {
    return( DirectMerchVMCSalesAmount - DirectMerchVMCCreditsAmount );
  }
  
  public double getDirectMerchVMCSalesAmount( )
  {
    return( DirectMerchVMCSalesAmount );
  }
  
  public int getDirectMerchVMCSalesCount( )
  {
    return( DirectMerchVMCSalesCount );
  }
  
  public int getDirectMerchVMCTotalCount( )
  {
    return( DirectMerchVMCSalesCount + DirectMerchVMCCreditsCount );
  }
  
  protected double getDiscIcAmount( int contractType )
  {
    Vector                        contractElements  = null;
    ContractTypes.PerItemRecord   item              = null;
    double                        retVal            = 0.0;
  
    try
    {
      for ( int dataType = 0; dataType < ContractTypes.CONTRACT_DATA_COUNT; ++dataType )
      {
        contractElements = ContractElements[dataType][contractType];
        for( int i = 0; i < contractElements.size(); ++i )
        {
          item = (ContractTypes.PerItemRecord)contractElements.elementAt(i);
          if ( BankContractBean.isItemInGroup( item.getBetType(), BankContractBean.BET_GROUP_DISC_IC ) )
          {
            retVal += item.getAmount();
          }
        }
      }
    }
    catch( Exception e )
    {
      logEntry(  "getDiscIcAmount(" + contractType + "): ", e.toString());
    }
    return( retVal );
  }
  
  public double getDiscIcExpense( )
  {
    return( getDiscIcAmount( ContractTypes.CONTRACT_TYPE_EXPENSE ) );
  }
  
  public double getDiscIcIncome( )
  {
    return( getDiscIcAmount( ContractTypes.CONTRACT_TYPE_INCOME ) );
  }
  
  protected double getFeesTotalAmount( int contractType )
  {
    Vector                        contractElements    = null;
    ContractTypes.PerItemRecord   item                = null;
    double                        retVal              = 0.0;
  
    try
    {
      for( int dataType = 0; dataType < ContractTypes.CONTRACT_DATA_COUNT; ++dataType )
      {
        contractElements = ContractElements[ dataType ][ contractType ];
        for( int i = 0; i < contractElements.size(); ++i )
        {
          item = (ContractTypes.PerItemRecord)contractElements.elementAt(i);
          if ( ! BankContractBean.isItemInGroup( item.getBetType(), BankContractBean.BET_GROUP_DISC_IC ) && 
               ! BankContractBean.isItemInGroup( item.getBetType(), BankContractBean.BET_GROUP_PARTNERSHIP ) &&
               ! BankContractBean.isItemInGroup( item.getBetType(), BankContractBean.BET_GROUP_ADJUSTMENTS ) )
          {
            retVal += item.getAmount();  
          }
        }
      }
    }
    catch( Exception e )
    {
      logEntry(  "getFeesTotalAmount(" + contractType + "): ", e.toString());
    }
    return( retVal );
  }
  
  public double getFeesTotalExpense( )
  {
    return( getFeesTotalAmount( ContractTypes.CONTRACT_TYPE_EXPENSE ) );
  }
  
  public double getFeesTotalIncome( )
  {
    return( getFeesTotalAmount( ContractTypes.CONTRACT_TYPE_INCOME ) );
  }

  protected double getGroupAmount( int contractType, int groupType )
  {
    Vector                          contractElements  = null;
    ContractTypes.PerItemRecord     itemRecord        = null;
    double                          retVal            = 0.0;
    
    for ( int dataType = 0; dataType < ContractTypes.CONTRACT_DATA_COUNT; ++dataType )
    {
      contractElements = ContractElements[ dataType ][ contractType ];
      for( int i = 0; i < contractElements.size(); ++i )
      {
        itemRecord = (ContractTypes.PerItemRecord)contractElements.elementAt(i);
  
        if( BankContractBean.isItemInGroup( itemRecord.getBetType(), groupType ) )
        {
          retVal += itemRecord.getAmount();
        }
      }
    }      
    return( retVal );
  }
  
  public double getGroupExpense( int groupId )
  {
    return( getGroupAmount( ContractTypes.CONTRACT_TYPE_EXPENSE, groupId ) );
  }
  
  public double getGroupIncome( int groupId )
  {
    return( getGroupAmount( ContractTypes.CONTRACT_TYPE_INCOME, groupId ) );
  }
  
  public long getHierarchyNode( )
  {
    return( HierarchyNode );
  }
  
  private double getIcExpense( )
  {
    Vector                        contractElements  = null;
    ContractTypes.PerItemRecord   item              = null;
    double                        retVal            = 0.0;
  
    try
    {
      contractElements = ContractElements[ContractTypes.CONTRACT_DATA_MERCH_DIRECT][ContractTypes.CONTRACT_TYPE_EXPENSE];
      for( int i = 0; i < contractElements.size(); ++i )
      {
        item = (ContractTypes.PerItemRecord)contractElements.elementAt(i);
        retVal += item.getAmount();
      }
    }
    catch( Exception e )
    {
      logEntry( "getContractExpense()", e.toString());
    }
    return( retVal );
  }
  
  public double getMerchantAnnualSales( )
  {
    int             activeMonths  = 0;
    Calendar        cal           = Calendar.getInstance();  
    Date            dateOpened    = null;
    double          monthlySales  = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:405^7*/

//  ************************************************************
//  #sql [Ctx] { select to_date( decode( mf.date_opened, 
//                                  '000000', '010100', 
//                                  mf.date_opened),'mmddyy') 
//          from   mif mf,
//                 orgmerchant om
//          where  om.org_num = :OrgId and
//                 mf.merchant_number = om.org_merchant_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select to_date( decode( mf.date_opened, \n                                '000000', '010100', \n                                mf.date_opened),'mmddyy')  \n        from   mif mf,\n               orgmerchant om\n        where  om.org_num =  :1  and\n               mf.merchant_number = om.org_merchant_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.ChildProfitabilitySummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,OrgId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   dateOpened = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:414^7*/
    }
    catch( Exception e )
    {
    }
    
    if( dateOpened == null )
    {
      cal.set( Calendar.MONTH, Calendar.JANUARY );
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      cal.set( Calendar.YEAR, 2000 );
      dateOpened = new java.sql.Date( cal.getTime().getTime() );
    }
    else if ( dateOpened.after( cal.getTime() ) )
    {
      cal.setTime( dateOpened );
      cal.set( Calendar.YEAR, -100 );
      dateOpened = new java.sql.Date( cal.getTime().getTime() );
    }
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:436^7*/

//  ************************************************************
//  #sql [Ctx] { select count(sm.merchant_number),
//                 sum(sm.vmc_sales_amount)/count(sm.merchant_number) 
//          from   monthly_extract_summary sm,
//                 orgmerchant             om
//          where  om.org_num = :OrgId and
//                 sm.merchant_number = om.org_merchant_num and
//                 sm.active_date between (:ReportDateEnd-395) and :ReportDateEnd and
//                 sm.active_date > :dateOpened and
//                 sm.vmc_sales_count > 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(sm.merchant_number),\n               sum(sm.vmc_sales_amount)/count(sm.merchant_number)  \n        from   monthly_extract_summary sm,\n               orgmerchant             om\n        where  om.org_num =  :1  and\n               sm.merchant_number = om.org_merchant_num and\n               sm.active_date between ( :2 -395) and  :3  and\n               sm.active_date >  :4  and\n               sm.vmc_sales_count > 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.ChildProfitabilitySummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,OrgId);
   __sJT_st.setDate(2,ReportDateEnd);
   __sJT_st.setDate(3,ReportDateEnd);
   __sJT_st.setDate(4,dateOpened);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activeMonths = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   monthlySales = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:447^7*/
    }
    catch( Exception e )
    {
    }

    if( activeMonths < 6 )
    {
      monthlySales = getAppMonthlySales();
    }
    
    return( monthlySales * 12 );
  }
  
  public double getNetProfitLoss( )
  {
    double          expense       = 0.0;
    double          income        = 0.0;
    
    // sum the income
    income += getDiscIcIncome();
    income += getFeesTotalIncome();
    income += getPartnershipIncome();
    income += getAdjustmentsIncome();
    
    // sum the expense
    expense += getDiscIcExpense();
    expense += getFeesTotalExpense();
    expense += getPartnershipExpense();
    expense += getAdjustmentsExpense();
    
    // return the net
    return( income - expense );
  }
  
  public long getOrgId( )
  {
    return( OrgId );
  }

  public String getOrgName( )
  {
    return( OrgName );
  }

  public double getPartnershipAmount( int contractType )
  {
    Vector                          contractElements    = null;
    ContractTypes.PerItemRecord     item                = null;
    double                          retVal              = 0.0;
  
    try
    {
      for( int dataType = 0; dataType < ContractTypes.CONTRACT_DATA_COUNT; ++dataType )
      {
        contractElements = ContractElements[ dataType ][ contractType ];
        for( int i = 0; i < contractElements.size(); ++i )
        {
          item = (ContractTypes.PerItemRecord)contractElements.elementAt(i);
          if ( BankContractBean.isItemInGroup( item.getBetType(), BankContractBean.BET_GROUP_PARTNERSHIP ) )
          {
            retVal += item.getAmount();
          }
        }
      }
    }
    catch( Exception e )
    {
      logEntry(  "getPartnershipAmount(" + contractType + "): ", e.toString());
    }
    return( retVal );
  }
  
  public double getPartnershipExpense( )
  {
    return( getPartnershipAmount( ContractTypes.CONTRACT_TYPE_EXPENSE ) );
  }
  
  public double getPartnershipIncome( )
  {
    return( getPartnershipAmount( ContractTypes.CONTRACT_TYPE_INCOME ) );
  }
  
  public double getReferralContractExpense( )
  {
    Vector                          contractElements    = null;
    ContractTypes.PerItemRecord     item                = null;
    double                          retVal              = 0.0;
  
    try
    {
      contractElements = ContractElements[ContractTypes.CONTRACT_DATA_MES][ContractTypes.CONTRACT_TYPE_EXPENSE];
      for( int i = 0; i < contractElements.size(); ++i )
      {
        item = (ContractTypes.PerItemRecord)contractElements.elementAt(i);
        if ( item.getContractSource() == ContractTypes.CONTRACT_SOURCE_REFERRAL )
        {
          retVal += item.getAmount();
        }
      }
    }
    catch( Exception e )
    {
      logEntry(  "getReferralContractExpense(): ", e.toString());
    }
    return( retVal );
  }
  
  public boolean hasDirectMerchData( )
  {
    return( DirectMerchDataPresent );
  }
  
  public boolean hasVolumeRecords( )
  {
    Vector      elements    = null;
    boolean     retVal      = false;
    
    for ( int contractType = 0; contractType < ContractTypes.CONTRACT_TYPE_COUNT; ++contractType )
    {
      for ( int dataType = 0; dataType < ContractTypes.CONTRACT_DATA_COUNT; ++dataType )
      {
        elements = getDataElements( dataType, contractType );
    
        if ( elements != null )
        {
          for ( int i = 0; i < elements.size(); ++i )
          {
            if ( elements.elementAt(i) instanceof ContractTypes.VolumeRecord )
            {
              retVal = true;
              break;
            }
          }
        }
      }
    }
    return( retVal );
  }
  
  protected void initialize( DefaultContext ctx, long orgId, String orgName, Date beginDate, Date endDate )
  {
    Ctx           = ctx;          // store the database context
    
    ContractElements     = new Vector[ContractTypes.CONTRACT_DATA_COUNT][ContractTypes.CONTRACT_TYPE_COUNT];
    
    for ( int dataType = 0; dataType < ContractTypes.CONTRACT_DATA_COUNT; ++dataType )
    {
      for( int i = 0; i < ContractTypes.CONTRACT_TYPE_COUNT; ++i )
      {
        ContractElements[dataType][i] = new Vector();
      }
    }
    
    OrgId = orgId;
    if ( orgName == null )
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:606^9*/

//  ************************************************************
//  #sql [Ctx] { select org_name  
//            from organization 
//            where org_num = :orgId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select org_name   \n          from organization \n          where org_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.ChildProfitabilitySummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   OrgName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:611^9*/
      }
      catch( java.sql.SQLException e )
      {
        OrgName = "INVALID";
      }
    }
    else
    {
      OrgName = orgName;
    }
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:625^7*/

//  ************************************************************
//  #sql [Ctx] { select  o.org_group,
//                  nvl(mf.association_node,0)
//                    
//          from    organization        o,
//                  mif                 mf
//          where   o.org_num = :orgId and
//                  mf.merchant_number(+) = o.org_group
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  o.org_group,\n                nvl(mf.association_node,0)\n                   \n        from    organization        o,\n                mif                 mf\n        where   o.org_num =  :1  and\n                mf.merchant_number(+) = o.org_group";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.ChildProfitabilitySummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   HierarchyNode = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   AssocNumber = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:634^7*/
    }
    catch( java.sql.SQLException e )
    {
      HierarchyNode = 0L;
      AssocNumber   = 0L;
    }
    
    // store the report date range
    ReportDateBegin   = beginDate;
    ReportDateEnd     = endDate;
  }
  
  public boolean isMerchantDataValid()
  {
    Vector                        contractElements    = null;
    ContractTypes.PerItemRecord   item                = null;
    double                        contractSum         = 0.0;
    boolean                       retVal              = false;
    
    for ( int contractType = 0; contractType < ContractTypes.CONTRACT_TYPE_COUNT; ++contractType )
    {
      contractElements = ContractElements[ContractTypes.CONTRACT_DATA_MERCH_DIRECT][contractType];
      contractSum      = 0.0;
      
      // summarize all the elements in the contract
      for ( int i = 0; i < contractElements.size(); ++i )
      {
        item         = (ContractTypes.PerItemRecord)contractElements.elementAt(i);
        contractSum += item.getAmount();
      }
      
      // compared the sum of the elements to the appropriate summary 
      // values (income or expense).
      retVal = ( contractSum == ( ( contractType == ContractTypes.CONTRACT_TYPE_INCOME ) ? MerchTotalIncome : MerchTotalExpense ) );
      
      if ( retVal == false )
      {
        // the values do not match, exit the loop now
        break;
      }
    }           
    return( retVal );
  }
  
  public boolean isOrgMerchant()
  {
    int       merchCount = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:685^7*/

//  ************************************************************
//  #sql [Ctx] { select count( merchant_number ) 
//          from   mif
//          where  merchant_number = :HierarchyNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count( merchant_number )  \n        from   mif\n        where  merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.ChildProfitabilitySummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,HierarchyNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:690^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return( merchCount != 0 );
  }
  
  protected String loadContractItemDesc( int itemType )
  {
    String              retVal      = "Invalid";
    
    try
    {
      if ( itemType < 0 )
      {
        retVal = BankContractBean.getGroupName( BankContractBean.betTypeToGroupIndex(itemType) );
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:710^9*/

//  ************************************************************
//  #sql [Ctx] { select billing_element_desc 
//            from   billing_element_desc 
//            where  billing_element_type = :itemType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select billing_element_desc  \n          from   billing_element_desc \n          where  billing_element_type =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.ChildProfitabilitySummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,itemType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:715^9*/
      }
    }
    catch( java.sql.SQLException e )
    {
      retVal = "Invalid (" + itemType + ")";
    }
    return( retVal );
  }
  
  protected void logEntry( String method, String message )
  {
    StringBuffer  errMsg      = new StringBuffer("");
    
    try
    {
      errMsg.append(this.getClass().getName());
      errMsg.append("::");
      errMsg.append(method);
      
      /*@lineinfo:generated-code*//*@lineinfo:735^7*/

//  ************************************************************
//  #sql [Ctx] { insert into java_log
//          (
//            log_time,
//            log_error,
//            log_source,
//            log_message
//          )
//          values
//          (
//            sysdate,
//            0,
//            :errMsg.toString(),
//            :message
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1113 = errMsg.toString();
   String theSqlTS = "insert into java_log\n        (\n          log_time,\n          log_error,\n          log_source,\n          log_message\n        )\n        values\n        (\n          sysdate,\n          0,\n           :1 ,\n           :2 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.reports.ChildProfitabilitySummary",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_1113);
   __sJT_st.setString(2,message);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:751^7*/
    }
    catch(Exception e)
    {
      // can't really do anything about this
      System.out.println(this.getClass().getName() + "::logEntry() - " + e.toString());
    }
    finally
    {
    }
  }
  
  public void setDirectMerchVMCVolume( int salesCount,  double salesAmount, 
                                       int creditsCount,double creditsAmount )
  {
    DirectMerchVMCSalesAmount  = salesAmount;
    DirectMerchVMCSalesCount   = salesCount;
    DirectMerchVMCCreditsAmount= creditsAmount;
    DirectMerchVMCCreditsCount = creditsCount;
    DirectMerchDataPresent     = true;
  }
  
  public void setMerchantData( double merchTotalIncome, 
                               double merchTotalExpense,
                               double merchNetIncome )
                               
  {
    // store the totals, these are only used
    // to validate the seperate items
    MerchTotalIncome          = merchTotalIncome;
    MerchTotalExpense         = merchTotalExpense;
    MerchNetIncome            = merchNetIncome;
    DirectMerchDataPresent = true;
  }                                 
  
  public void showData( java.io.PrintStream out )
  {
    Vector        contractElements = null;
    double        discAmount;
    double        perItemAmount;
  
    out.println();
    out.println("************************************************");
    out.println("*  Profitability Data " );
    out.println("*    Org #          : " + OrgId );
    out.println("*    Org Name       : " + OrgName );
    if ( isOrgMerchant() )
    {
      out.println("*    Merchant #     : " + HierarchyNode );
    }
    else
    {
      out.println("*    Hierarchy Node : " + HierarchyNode );
    }
    
    for ( int contractType = 0; contractType < ContractTypes.CONTRACT_TYPE_COUNT; ++contractType )
    {
      if ( contractType == ContractTypes.CONTRACT_TYPE_EXPENSE )
      {
        out.println("************************************************");
        out.println("*  Merchant Expense Data" );
        out.println("************************************************");
      }
      else // income
      {
        out.println("************************************************");
        out.println("*  Merchant Income Data" );
        out.println("************************************************");
      }
      contractElements = ContractElements[ContractTypes.CONTRACT_DATA_MERCH_DIRECT][contractType];
      for( int i = 0; i < contractElements.size(); ++i )
      {
        try
        {
          ((ContractTypes.PerItemRecord)contractElements.get(i)).showData( out );
        }
        catch( NullPointerException e )
        {
        }
      }
    }
    
    for ( int contractType = 0; contractType < ContractTypes.CONTRACT_TYPE_COUNT; ++contractType )
    {
      if ( contractType == ContractTypes.CONTRACT_TYPE_EXPENSE )
      {
        out.println("************************************************");
        out.println("*  Expense Contract Data" );
        out.println("************************************************");
      }
      else // income
      {
        out.println("************************************************");
        out.println("*  Income Contract Data" );
        out.println("************************************************");
      }
      contractElements = ContractElements[ContractTypes.CONTRACT_DATA_MES][contractType];
      for( int i = 0; i < contractElements.size(); ++i )
      {
        try
        {
          ((ContractTypes.PerItemRecord)contractElements.get(i)).showData( out );
        }
        catch( NullPointerException e )
        {
        }
      }
    }
  }
}/*@lineinfo:generated-code*/