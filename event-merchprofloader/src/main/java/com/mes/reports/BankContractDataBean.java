/*@lineinfo:filename=BankContractDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/reports/BankContractDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-10-28 17:42:24 -0700 (Wed, 28 Oct 2015) $
  Version            : $Revision: 23920 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import com.mes.forms.CurrencyField;
import com.mes.forms.Field;
import com.mes.ops.QueueConstants;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.NumberFormatter;
import com.mes.tools.HierarchyTree;
import sqlj.runtime.ResultSetIterator;

public class BankContractDataBean extends ReportSQLJBean
{
  public static final int   MERCH_PROF_TYPE_SUMMARY               = 0;
  public static final int   MERCH_PROF_TYPE_UNDER_WATER           = 1;
  public static final int   MERCH_PROF_TYPE_NET_REVENUE           = 2;
  public static final int   MERCH_PROF_TYPE_AVG_NET_DISC          = 3;
  public static final int   MERCH_PROF_TYPE_ME_REVENUE            = 4;
  public static final int   MERCH_PROF_TYPE_RETENTION             = 5;
  public static final int   MERCH_PROF_TYPE_REVENUE_PLUS_EQUIP    = 6;
  public static final int   MERCH_PROF_TYPE_TPS_GRIN              = 7;
  public static final int   MERCH_PROF_TYPE_NET_REVENUE_RETENTION = 8;
  public static final int   MERCH_PROF_TYPE_DEFAULT               = MERCH_PROF_TYPE_SUMMARY;
  
  public static final int   UT_INVALID                            = -1;
  public static final int   UT_MES                                = 0; 
  public static final int   UT_RESELLER                           = 1; 
  public static final int   UT_CONTRACT_BANK                      = 2; 
  public static final int   UT_VAR_BANK                           = 3; 
  private long hhLoadSec = 0;
  private double achDiscLiability = 0.0;
  public long getHHLoadSec() { return hhLoadSec; }
  public void setHHLoadSec ( long inhhLoadSec ) { hhLoadSec = inhhLoadSec; } 
  public double getAchDiscLiability() { return achDiscLiability; }
  
  public class SummaryData implements Comparable
  {
    public      Date        ActiveDate          = null;
    public      double      CashAdvanceAmount   = 0.0;
    public      int         CashAdvanceCount    = 0;
    protected   double[]    Expense             = null;
    public      long        HierarchyNode       = 0L;
    protected   double[]    Income              = null;
    public      long        OrgId               = 0L;
    public      String      OrgName             = null;
    public      String      PortfolioName       = null;
    public      long        PortfolioNode       = 0L;
    protected   double      ReferralFees        = 0.0;
    protected   int         UserType            = UT_INVALID;
    public      double      VmcCreditsAmount    = 0.0;
    public      int         VmcCreditsCount     = 0;
    public      double      VmcSalesAmount      = 0.0;
    public      int         VmcSalesCount       = 0;
        
    // report specific values
    public      String      AccountStatus       = "";
    public      int         ActivatedCount      = 0;
    public      int         ActiveCount         = 0;
    public      double      AdjustmentsAmount   = 0.0;
    public      double      COGS                = 0.0;
    public      double      EquipmentExpense    = 0.0;
    private     double[]    GRINExpense         = null;
    public      double      InterchangeExpense  = 0.0;
    public      double      NetRevenue          = 0.0;
    public      double      ProcessingFees      = 0.0;
    public      double      MerchantIncome      = 0.0;
    public      double      PartnershipIncome   = 0.0;
    public      double      ResellerIncome      = 0.0;
    public      double      ThirdPartyFees      = 0.0;
    public      double      VmcFees             = 0.0;
    private     double      AchDiscInc          = 0.0;
    private     double      AchDiscExp          = 0.0;
         
    
    public SummaryData( ResultSet resultSet, int userType )
      throws java.sql.SQLException
    {
      UserType      = userType;
      
      HierarchyNode = resultSet.getLong("hierarchy_node");
      OrgId         = resultSet.getLong("org_num");
      OrgName       = resultSet.getString("org_name");
      
      try
      {
        PortfolioNode = resultSet.getLong("portfolio_node");
        PortfolioName = getNodeName( PortfolioNode );
      }
      catch(Exception e)
      {
        PortfolioNode = 0;
        PortfolioName = "Not Available";
      }
      
      Expense     = new double[ BankContractBean.BET_GROUP_COUNT ];
      Income      = new double[ BankContractBean.BET_GROUP_COUNT ];
      
      // report specific initialization
      switch( getProfReportType() )
      {
        case MERCH_PROF_TYPE_NET_REVENUE_RETENTION:
          ActiveDate      = resultSet.getDate("active_date");
          break;
          
        case MERCH_PROF_TYPE_TPS_GRIN:
          GRINExpense = new double[ BankContractBean.BET_GROUP_COUNT ];
          break;
      }
    }
    
    public void addData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      CashAdvanceCount  += resultSet.getInt("cash_advance_vol_count");
      CashAdvanceAmount += resultSet.getDouble("cash_advance_vol_amount");
      
      VmcSalesCount     += resultSet.getInt("vmc_sales_count");
      VmcSalesAmount    += resultSet.getDouble("vmc_sales_amount");
      VmcCreditsCount   += resultSet.getInt("vmc_credits_count");
      VmcCreditsAmount  += resultSet.getDouble("vmc_credits_amount");
      
      // store report specific data
      switch ( getProfReportType() )
      {
        case MERCH_PROF_TYPE_NET_REVENUE_RETENTION:
          ActivatedCount  += resultSet.getInt("activated_count");
          ActiveCount     += resultSet.getInt("active_count");
          break;
      }
      
      
      if ( UserType == UT_RESELLER )
      {
        // test if this is a liability bank
        if ( resultSet.getInt("liability_contract") == 1 )
        {
          // if there is a cash advance referral in place, then 
          // reseller gets merchant income, agent gets referral fee
          // mes collects reseller contract income.
          if ( resultSet.getInt("cash_advance_referral") == 1 )
          {
            // cash advance merchants do not receive merchant income or expense
            Income[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("disc_ic_inc");
            Income[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_inc");
            Income[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_inc");
            Income[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_inc");
            Income[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_inc");
            Income[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_inc");
            Income[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_inc");
            Income[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_inc");
            Income[ BankContractBean.BET_GROUP_TAX_COLLECTED] += resultSet.getDouble("equip_sales_tax");
            Income[ BankContractBean.BET_GROUP_ADJUSTMENTS  ] += (resultSet.getDouble("disc_ic_adj") + resultSet.getDouble("fee_adj"));
            Income[ BankContractBean.BET_GROUP_ACH          ] += resultSet.getDouble("ach_inc");
            AchDiscInc += resultSet.getDouble("ach_disc_inc");
            Expense[ BankContractBean.BET_GROUP_DISC_IC     ] += resultSet.getDouble("ic_exp");
            Expense[ BankContractBean.BET_GROUP_DISC_IC     ] += resultSet.getDouble("assessment");
            NetRevenue += extractNetRevenue(resultSet);
          } 
          
          // resellers collect the liablity income from the agents they sign                        
          Income[ BankContractBean.BET_GROUP_DISC_IC ]       += resultSet.getDouble("ndr_liability");
          Income[ BankContractBean.BET_GROUP_CAPTURE_FEES ]  += resultSet.getDouble("capture_liability");
          Income[ BankContractBean.BET_GROUP_DEBIT_FEES   ]  += resultSet.getDouble("debit_liability");
          Income[ BankContractBean.BET_GROUP_PLAN_FEES    ]  += resultSet.getDouble("plan_liability");
          Income[ BankContractBean.BET_GROUP_AUTH_FEES    ]  += resultSet.getDouble("auth_liability");
          Income[ BankContractBean.BET_GROUP_SYSTEM_FEES  ]  += resultSet.getDouble("sys_gen_liability");
          Income[ BankContractBean.BET_GROUP_PARTNERSHIP  ]  += resultSet.getDouble("partnership"); 
          Income[ BankContractBean.BET_GROUP_EQUIP_RENTAL ]  += resultSet.getDouble("equip_rental_liability");
          Income[ BankContractBean.BET_GROUP_EQUIP_SALES  ]  += resultSet.getDouble("equip_sales_liability");
          Income[ BankContractBean.BET_GROUP_ACH          ]  += resultSet.getDouble("ach_liability");
          AchDiscInc += resultSet.getDouble("ach_disc_liability"); 
          NetRevenue += extractLiabilityContract(resultSet);
        }
        else    // no liability contract merchant is a direct merchant of reseller
        {
          // merchant income goes to reseller
          Income[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("disc_ic_inc");
          Income[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_inc");
          Income[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_inc");
          Income[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_inc");
          Income[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_inc");
          Income[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_inc");
          Income[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_inc");
          Income[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_inc");
          Income[ BankContractBean.BET_GROUP_TAX_COLLECTED] += resultSet.getDouble("equip_sales_tax");
          Income[ BankContractBean.BET_GROUP_ADJUSTMENTS  ] += (resultSet.getDouble("disc_ic_adj") + resultSet.getDouble("fee_adj"));
          Income[ BankContractBean.BET_GROUP_ACH          ] += resultSet.getDouble("ach_inc");
          AchDiscInc += resultSet.getDouble("ach_disc_inc");
          // interchange and assessment go to reseller
          Expense[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("ic_exp");
          Expense[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("assessment");
          Expense[BankContractBean.BET_GROUP_ACH           ] += resultSet.getDouble("ps_exp");
          AchDiscExp += resultSet.getDouble("ps_disc_exp"); 
          NetRevenue += extractNetRevenue(resultSet);
        }
        // add the reseller contract to the expense side
        Expense[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("ndr_reseller");
        Expense[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_reseller");
        Expense[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_reseller");
        Expense[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_reseller");
        Expense[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_reseller");
        Expense[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_reseller");
        Expense[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_reseller");
        Expense[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_reseller");
        
        // add the referral contract to the expense side
        Expense[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("ndr_referral");
        Expense[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_referral");
        Expense[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_referral");
        Expense[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_referral");
        Expense[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_referral");
        Expense[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_referral");
        Expense[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_referral");
        Expense[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_referral");
        Expense[ BankContractBean.BET_GROUP_ACH          ] += resultSet.getDouble("ach_referral");
        AchDiscExp += resultSet.getDouble("ach_disc_referral"); 
        
      }
      else if ( (UserType == UT_CONTRACT_BANK) || (UserType == UT_VAR_BANK) )
      {
        // contract & var banks always treat referral as income
        Income[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("ndr_referral");
        Income[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_referral");
        Income[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_referral");
        Income[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_referral");
        Income[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_referral");
        Income[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_referral");
        Income[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_referral");
        Income[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_referral");
        Income[ BankContractBean.BET_GROUP_ACH          ] += resultSet.getDouble("ach_referral");
        AchDiscInc += resultSet.getDouble("ach_disc_referral"); 
        
        //
        // process the other income/expense components for
        // 1) liability contract banks (SVB, MtnWest etc)
        // 2) VAR banks (UBOC,BB&T etc) 
        //
        if ( ( resultSet.getInt("liability_contract") == 1 ) ||
             ( UserType == UT_VAR_BANK ) )
        {
          // if this is not a cash advance account or there is not
          // contract element for cash advances, then the liability
          // bank owns all the merchant revenue.
          if ( resultSet.getInt("cash_advance_referral") == 0 )
          {
            // cash advance merchants do not receive merchant income or expense
            Income[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("disc_ic_inc");
            Income[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_inc");
            Income[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_inc");
            Income[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_inc");
            Income[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_inc");
            Income[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_inc");
            Income[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_inc");
            Income[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_inc");
            Income[ BankContractBean.BET_GROUP_TAX_COLLECTED] += resultSet.getDouble("equip_sales_tax");
            Income[ BankContractBean.BET_GROUP_ADJUSTMENTS  ] += (resultSet.getDouble("disc_ic_adj") + resultSet.getDouble("fee_adj"));
            Income[ BankContractBean.BET_GROUP_ACH          ] += resultSet.getDouble("ach_inc");
            AchDiscInc += resultSet.getDouble("ach_disc_inc");
      
            Expense[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("ic_exp");
            Expense[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("assessment");
            
            
            // remove MES only revenue (if any)
            Income[ BankContractBean.BET_GROUP_DISC_IC      ] -= resultSet.getDouble("mes_only_disc_ic");
            Income[ BankContractBean.BET_GROUP_CAPTURE_FEES ] -= resultSet.getDouble("mes_only_capture");
            Income[ BankContractBean.BET_GROUP_DEBIT_FEES   ] -= resultSet.getDouble("mes_only_debit");
            Income[ BankContractBean.BET_GROUP_PLAN_FEES    ] -= resultSet.getDouble("mes_only_plan");
            Income[ BankContractBean.BET_GROUP_AUTH_FEES    ] -= resultSet.getDouble("mes_only_auth");
            Income[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] -= resultSet.getDouble("mes_only_sys_gen");
          } 
          else    // has cash advance referral
          {
            // special processing required for Epicor entries,
            // need to post TSYS revenue, then back out via
            // the processing fees line.
            ProcessingFees += extractNetRevenue(resultSet);
            
            // special processing.  MES Only fees will be added
            // to the processing fees for all accounts types below
            // so back them out in advance for the cash advance referrals
            ProcessingFees -= extractMesOnlyFees(resultSet);
          }
          
          if ( UserType == UT_VAR_BANK )
          {
            // VAR banks track expenses using the processor BETs
            Expense[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_exp");
            Expense[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_exp");
            Expense[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_exp");
            Expense[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_exp");
            Expense[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_exp");
            Expense[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_exp");
            Expense[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_exp");
            
            // add in club merchant supply cost
            Expense[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("supply_cost");
            // Add PS buy-back Expense on  Expense side
            if (resultSet.getInt("liability_contract") == 0)
            {
                Expense[BankContractBean.BET_GROUP_ACH]     += resultSet.getDouble("ps_exp");
                AchDiscExp += resultSet.getDouble("ps_disc_exp"); 
            }
            
          }
          else    // use contract expense
          {
            // instead of the back end BETs, use the 
            // MES liability contract for the expense side
            Expense[ BankContractBean.BET_GROUP_DISC_IC ]      += resultSet.getDouble("ndr_liability");
            Expense[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_liability");
            Expense[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_liability");
            Expense[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_liability");
            Expense[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_liability");
            Expense[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_liability");
            Expense[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_liability");
            Expense[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_liability");
            Expense[ BankContractBean.BET_GROUP_PARTNERSHIP  ] += resultSet.getDouble("partnership"); 
            Expense[ BankContractBean.BET_GROUP_EXCLUDED_FEES ]+= resultSet.getDouble("excluded_fees");
            Expense[ BankContractBean.BET_GROUP_ACH           ]+= resultSet.getDouble("ach_liability");
            AchDiscExp += resultSet.getDouble("ach_disc_liability"); 
            // add in any third party expenses the bank defines
            Expense[ BankContractBean.BET_GROUP_DISC_IC ]      += resultSet.getDouble("ndr_external");
            Expense[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_external");
            Expense[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_external");
            Expense[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_external");
            Expense[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_external");
            Expense[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_external");
            Expense[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_external");
            Expense[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_external");
            
          
            // MES owns this inventory so base cost (from price list)
            // is expense to the contract bank
            if ( resultSet.getInt("mes_inventory") == 1 )
            {
              // show the base cost for the equipment less any 
              // amounts excluded from the processing p/l numbers
              Expense[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += ( resultSet.getDouble("equip_sales_base_cost") -
                                                                      resultSet.getDouble("excluded_equip_sales") );
              Expense[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += ( resultSet.getDouble("equip_rental_base_cost") -
                                                                      resultSet.getDouble("excluded_equip_rental") );
              // excluded rental is only supported when the 
              // account is using MES owned inventory.
              Expense[ BankContractBean.BET_GROUP_EXCLUDED_EQUIP ] += resultSet.getDouble("excluded_equip_rental");
              
              EquipmentExpense += ( resultSet.getDouble("equip_sales_base_cost") +
                                    resultSet.getDouble("equip_rental_base_cost") );
            }
            else // client owned inventory, only cost is transfers
            {
              Expense[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += ( resultSet.getDouble("equip_transfer_cost") - 
                                                                      resultSet.getDouble("excluded_equip_sales") );
                                                                      
              EquipmentExpense += resultSet.getDouble("equip_transfer_cost");
            }
            // add the excluded sales to the excluded equip total.  this
            // will be used to determine the overall p/l for the 
            // agent bank.  overall p/l is the payment amount
            Expense[ BankContractBean.BET_GROUP_EXCLUDED_EQUIP ] += resultSet.getDouble("excluded_equip_sales");
          }          
          
          // store the TSYS net revenue 
          // (only for accounts with a liability contract)
          NetRevenue += extractNetRevenue(resultSet);
        }     // if ( liability bank )
        
        // store the processing fees
        ProcessingFees += extractLiabilityContract(resultSet);
        ProcessingFees += extractMesOnlyFees(resultSet);
        ProcessingFees += extractExcludedFees(resultSet); 
        
        // get the referral fees
        ReferralFees   += extractReferralContract(resultSet);
      }
      else    // mes user (i.e. 394100000 or 9999999999)
      {
        if ( resultSet.getInt("reseller_contract") == 1 )
        {
          // direct account belonging to the reseller.  we get paid the
          // contract fees and the have no expense on this deal 
          Income[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("ndr_reseller");
          Income[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_reseller");
          Income[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_reseller");
          Income[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_reseller");
          Income[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_reseller");
          Income[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_reseller");
          Income[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_reseller");
          Income[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_reseller");
          
          // reseller gets net of liability contract less reseller contract
          ResellerIncome  += ( extractLiabilityContract(resultSet) -
                               extractResellerContract(resultSet) );
        }
        else    // not under reseller contract
        {
          if ( resultSet.getInt("liability_contract") == 1 )
          {
            // if this is a cash advance account and this node
            // has a cash advance contract element in place,
            // then the bank level gets the merchant income.
            if ( resultSet.getInt("cash_advance_referral") == 1 ) 
            {
              // 3941 only gets cash advance merchant income and expense, all
              // other merchant income and expense belongs to the agent bank
              Income[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("disc_ic_inc");
              Income[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_inc");
              Income[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_inc");
              Income[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_inc");
              Income[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_inc");
              Income[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_inc");
              Income[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_inc");
              Income[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_inc");
              Income[ BankContractBean.BET_GROUP_TAX_COLLECTED] += resultSet.getDouble("equip_sales_tax");
              Income[ BankContractBean.BET_GROUP_ADJUSTMENTS  ] += (resultSet.getDouble("disc_ic_adj") + resultSet.getDouble("fee_adj"));
              Income[ BankContractBean.BET_GROUP_ACH          ] += resultSet.getDouble("ach_inc");
              AchDiscInc                                        += resultSet.getDouble("ach_disc_inc");
            
              Expense[ BankContractBean.BET_GROUP_DISC_IC     ] += resultSet.getDouble("ic_exp");
              Expense[ BankContractBean.BET_GROUP_DISC_IC     ] += resultSet.getDouble("assessment");
              
            }
          
            // add the liability contract to the income side
            Income[ BankContractBean.BET_GROUP_DISC_IC ]      += resultSet.getDouble("ndr_liability");
            Income[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_liability");
            Income[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_liability");
            Income[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_liability");
            Income[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_liability");
            Income[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_liability");
            Income[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_liability");
            Income[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_liability");
            Income[ BankContractBean.BET_GROUP_PARTNERSHIP  ] += resultSet.getDouble("partnership"); 
            Income[ BankContractBean.BET_GROUP_ACH     ]      += resultSet.getDouble("ach_liability");
            AchDiscInc                                        += resultSet.getDouble("ach_disc_liability"); 
            
            // add any MES only merchant renveue revenue
            Income[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("mes_only_disc_ic");
            Income[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("mes_only_capture");
            Income[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("mes_only_debit");
            Income[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("mes_only_plan");
            Income[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("mes_only_auth");
            Income[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("mes_only_sys_gen");
          
            // M/E revenue numbers for epicor
            NetRevenue        += extractNetRevenue(resultSet);
            PartnershipIncome += resultSet.getDouble("partnership"); 
            ProcessingFees    += ( extractLiabilityContract(resultSet) +
                                   extractMesOnlyFees(resultSet) +
                                   extractExcludedFees(resultSet) );
                                  
            // partnerships are separate
            if ( resultSet.getDouble("partnership") == 0.0 )
            {
              COGS            += resultSet.getDouble("equip_sales_exp");
            
              // if this merchant uses MES owned inventory 
              // then the base cost is rental/sales base cost
              if ( resultSet.getInt("mes_inventory") == 1 )
              {
                // add the values for month end revenue report
                ProcessingFees += resultSet.getDouble("equip_sales_base_cost");
                ProcessingFees += resultSet.getDouble("equip_rental_base_cost");
              
                // add values for income/expense report
                Income[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_base_cost");
                Income[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_base_cost");
              }
              else  // bank owns equipment, only cost is transfers made to the bank
              {
                // add the values for month end revenue report
                ProcessingFees += resultSet.getDouble("equip_transfer_cost");
              
                // add values for income/expense report
                Income[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_transfer_cost");
              }              
            }
          }
          else    // no liability or reseller contract
          {
            // a bank receives all the merchant income and all the expense
            Income[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("disc_ic_inc");
            Income[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_inc");
            Income[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_inc");
            Income[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_inc");
            Income[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_inc");
            Income[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_inc");
            Income[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_inc");
            Income[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_inc");
            Income[ BankContractBean.BET_GROUP_TAX_COLLECTED] += resultSet.getDouble("equip_sales_tax");
            Income[ BankContractBean.BET_GROUP_ADJUSTMENTS  ] += (resultSet.getDouble("disc_ic_adj") + resultSet.getDouble("fee_adj"));
            Income[ BankContractBean.BET_GROUP_ACH          ] += resultSet.getDouble("ach_inc");
            AchDiscInc                                        += resultSet.getDouble("ach_disc_inc");
      
            Expense[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("ic_exp");
            Expense[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("assessment");
            Expense[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_exp");
            Expense[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_exp");
            Expense[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_exp");
            Expense[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_exp");
            Expense[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_exp");
            Expense[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_exp");
            Expense[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_exp");
            Expense[BankContractBean.BET_GROUP_ACH]            += resultSet.getDouble("ps_exp");
            AchDiscExp                                         += resultSet.getDouble("ps_disc_exp"); 
          
            if ( resultSet.getInt("mes_inventory") == 1 )
            {                                
              COGS += resultSet.getDouble("equip_sales_exp");
            }
          }
          
          // GRIN revenue numbers (TPS only)
          if ( getProfReportType() == MERCH_PROF_TYPE_TPS_GRIN )
          {
            GRINExpense[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("ndr_grin");
            GRINExpense[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_grin");
            GRINExpense[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_grin");
            GRINExpense[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_grin");
            GRINExpense[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_grin");
            GRINExpense[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_grin");
            GRINExpense[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_grin");
            GRINExpense[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_grin");
          }            
          
          // all referral contract data is expense to the bank level
          Expense[ BankContractBean.BET_GROUP_DISC_IC      ] += resultSet.getDouble("ndr_referral");
          Expense[ BankContractBean.BET_GROUP_CAPTURE_FEES ] += resultSet.getDouble("capture_referral");
          Expense[ BankContractBean.BET_GROUP_DEBIT_FEES   ] += resultSet.getDouble("debit_referral");
          Expense[ BankContractBean.BET_GROUP_PLAN_FEES    ] += resultSet.getDouble("plan_referral");
          Expense[ BankContractBean.BET_GROUP_AUTH_FEES    ] += resultSet.getDouble("auth_referral");
          Expense[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] += resultSet.getDouble("sys_gen_referral");
          Expense[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] += resultSet.getDouble("equip_rental_referral");
          Expense[ BankContractBean.BET_GROUP_EQUIP_SALES  ] += resultSet.getDouble("equip_sales_referral");
          Expense[ BankContractBean.BET_GROUP_ACH  ] += resultSet.getDouble("ach_referral");
          AchDiscExp                                 += resultSet.getDouble("ach_disc_referral"); 
          
          
        }   // not a reseller
        
        // store the month end revenue report numbers
        // store the merchant billing data separately
        MerchantIncome      +=  ( resultSet.getDouble("disc_ic_inc") +
                                  resultSet.getDouble("capture_inc") +
                                  resultSet.getDouble("debit_inc") +
                                  resultSet.getDouble("plan_inc") +
                                  resultSet.getDouble("auth_inc") +
                                  resultSet.getDouble("sys_gen_inc") +
                                  resultSet.getDouble("equip_rental_inc") +
                                  resultSet.getDouble("equip_sales_inc") +
                                  resultSet.getDouble("equip_sales_tax") +
                                  resultSet.getDouble("ach_inc"));
        AdjustmentsAmount   +=  ( resultSet.getDouble("disc_ic_adj") + 
                                  resultSet.getDouble("fee_adj") );
        InterchangeExpense  += ( resultSet.getDouble("ic_exp") + 
                                 resultSet.getDouble("assessment") );
        VmcFees             += resultSet.getDouble("vmc_fees");
      
        // store referral expense separately because it is needed
        // by the net revenue report
        ReferralFees += extractReferralContract(resultSet);
      }     // default user type (MES)
      
      // add merchant status if present
      if( isNodeAssociation( getReportHierarchyNode() ) == true )
      {
        AccountStatus = resultSet.getString("account_status");
      }
    }
    
    public int compareTo( Object obj )
    {
      SummaryData       compareObj    = (SummaryData) obj;
      double            diff          = 0.0;
      int               retVal        = 0;
      
      switch( getProfReportType() )
      {
        case MERCH_PROF_TYPE_UNDER_WATER:
          if ( (retVal = (int)(PortfolioNode - compareObj.PortfolioNode)) == 0 )
          {
            if ( (diff = (getNetIncomeExpense() - compareObj.getNetIncomeExpense())) != 0.0 )
            {
              retVal = (( diff > 0.0 ) ? 1 : -1);
              break;
            }
          }
          else
          {
            break;    // not the same portfolio, break
          }
          // else fall through 
          
        default:
          if ( (retVal = OrgName.compareTo(compareObj.OrgName)) == 0 )
          {
            if ( (retVal = (int)(HierarchyNode - compareObj.HierarchyNode)) == 0 )
            {
              retVal = (int)(OrgId - compareObj.OrgId);
            }
          }
          break;
      }
      return( retVal );
    }
    
    protected double extractExcludedFees( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return( resultSet.getDouble("excluded_fees") );
    }
    
    protected double extractLiabilityContract( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return( resultSet.getDouble("ndr_liability") +
              resultSet.getDouble("capture_liability") +
              resultSet.getDouble("debit_liability") +
              resultSet.getDouble("plan_liability") +
              resultSet.getDouble("auth_liability") +
              resultSet.getDouble("sys_gen_liability") +
              resultSet.getDouble("equip_rental_liability") +
              resultSet.getDouble("equip_sales_liability") +
              resultSet.getDouble("ach_liability") );
    }
    
    protected double extractMesOnlyFees( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return(  resultSet.getDouble("mes_only_disc_ic") +
               resultSet.getDouble("mes_only_capture") +
               resultSet.getDouble("mes_only_debit") +
               resultSet.getDouble("mes_only_plan") +
               resultSet.getDouble("mes_only_auth") +
               resultSet.getDouble("mes_only_sys_gen") );
    }
    
    protected double extractNetRevenue( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return( ( resultSet.getDouble("disc_ic_inc") +
                resultSet.getDouble("capture_inc") +
                resultSet.getDouble("debit_inc") +
                resultSet.getDouble("plan_inc") +
                resultSet.getDouble("auth_inc") +
                resultSet.getDouble("sys_gen_inc") +
                resultSet.getDouble("equip_rental_inc") +
                resultSet.getDouble("equip_sales_inc") +
                resultSet.getDouble("equip_sales_tax") +
                resultSet.getDouble("disc_ic_adj") + 
                resultSet.getDouble("fee_adj") +
                resultSet.getDouble("ach_inc")
              ) -
              ( resultSet.getDouble("ic_exp") + 
                resultSet.getDouble("assessment") 
              )
            );
    }
    
    protected double extractReferralContract( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return( resultSet.getDouble("ndr_referral") +
              resultSet.getDouble("capture_referral") +
              resultSet.getDouble("debit_referral") +
              resultSet.getDouble("plan_referral") +
              resultSet.getDouble("auth_referral") +
              resultSet.getDouble("sys_gen_referral") +
              resultSet.getDouble("equip_rental_referral") +
              resultSet.getDouble("equip_sales_referral") + 
              resultSet.getDouble("ach_referral"));
    }
    
    protected double extractResellerContract( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return(  resultSet.getDouble("ndr_reseller") +
               resultSet.getDouble("capture_reseller") +
               resultSet.getDouble("debit_reseller") +
               resultSet.getDouble("plan_reseller") +
               resultSet.getDouble("auth_reseller") +
               resultSet.getDouble("sys_gen_reseller") +
               resultSet.getDouble("equip_rental_reseller") +
               resultSet.getDouble("equip_sales_reseller") );
    }
    
    protected double extractVmcFees( ResultSet resultSet )
      throws java.sql.SQLException
    {
      return(  resultSet.getDouble("vmc_fees") );
    }
    
    public double getAdjustmentsExpense( )
    {
      return( Expense[ BankContractBean.BET_GROUP_ADJUSTMENTS ] );
    }
  
    public double getAdjustmentsIncome( )
    {
      return( Income[ BankContractBean.BET_GROUP_ADJUSTMENTS ] );
    }
    
    public double getAvgNetDiscount( )
    {
      double    revenue       = Income[BankContractBean.BET_GROUP_DISC_IC] - Expense[BankContractBean.BET_GROUP_DISC_IC];
      int       totalCount    = (VmcSalesCount + CashAdvanceCount);
      
      if ( totalCount != 0 )
      {
        revenue /= totalCount;
      }
      return( revenue );
    }
  
    public double getDiscIcExpense( )
    {
      return( Expense[ BankContractBean.BET_GROUP_DISC_IC ] );
    }
  
    public double getDiscIcIncome( )
    {
      return( Income[ BankContractBean.BET_GROUP_DISC_IC ] );
    }
    
    public double getEquipmentExpense( )
    {
      return( EquipmentExpense );
    }
    
    public double getEquipSalesTax( )
    {
      return( Income[ BankContractBean.BET_GROUP_TAX_COLLECTED ] );
    }
    
    public double getExcludedEquipExpense()
    {
      return( Expense[ BankContractBean.BET_GROUP_EXCLUDED_EQUIP ] );
    }
    
    public double getExcludedFeesExpense()
    {
      return( Expense[ BankContractBean.BET_GROUP_EXCLUDED_FEES ] );
    }
  
    public double getFeesTotalExpense( )
    {
      int           bankNumber      = getReportBankId();
      double        retVal          = 0.0;
      int           salesTaxGroup   = -1;     // none
    
      // sales tax is just another fee for all banks
      // except 3941.
      if ( bankNumber != mesConstants.BANK_ID_MES )
      {
        salesTaxGroup = BankContractBean.BET_GROUP_TAX_COLLECTED;
      }
      
      for ( int i = 0; i < BankContractBean.BET_GROUP_COUNT; ++i )
      {
        if ( ( i == BankContractBean.BET_GROUP_CAPTURE_FEES ) ||
             ( i == BankContractBean.BET_GROUP_DEBIT_FEES   ) ||
             ( i == BankContractBean.BET_GROUP_PLAN_FEES    ) ||
             ( i == BankContractBean.BET_GROUP_AUTH_FEES    ) ||
             ( i == BankContractBean.BET_GROUP_SYSTEM_FEES  ) ||
             ( i == BankContractBean.BET_GROUP_EQUIP_RENTAL ) ||
             ( i == BankContractBean.BET_GROUP_EQUIP_SALES  ) ||
             ( i == salesTaxGroup                           ) )
        {
          retVal += Expense[i];
        }
      }
      return( retVal );
    }
    
    public double getFeesTotalIncome( )
    {
      int           bankNumber      = getReportBankId();
      double        retVal  = 0.0;
      int           salesTaxGroup   = -1;     // none
    
      // sales tax is just another fee for all banks
      // except 3941.
      if ( bankNumber != mesConstants.BANK_ID_MES )
      {
        salesTaxGroup = BankContractBean.BET_GROUP_TAX_COLLECTED;
      }
    
      for ( int i = 0; i < BankContractBean.BET_GROUP_COUNT; ++i )
      {
        if ( ( i == BankContractBean.BET_GROUP_CAPTURE_FEES ) ||
             ( i == BankContractBean.BET_GROUP_DEBIT_FEES   ) ||
             ( i == BankContractBean.BET_GROUP_PLAN_FEES    ) ||
             ( i == BankContractBean.BET_GROUP_AUTH_FEES    ) ||
             ( i == BankContractBean.BET_GROUP_SYSTEM_FEES  ) ||
             ( i == BankContractBean.BET_GROUP_EQUIP_RENTAL ) ||
             ( i == BankContractBean.BET_GROUP_EQUIP_SALES  ) ||
             ( i == salesTaxGroup                           ) )
        {
          retVal += Income[i];
        }        
      }
      return( retVal );
    }
    
    public double getGRINExpense( )
    {
      return( GRINExpense[ BankContractBean.BET_GROUP_DISC_IC      ] +
              GRINExpense[ BankContractBean.BET_GROUP_AUTH_FEES    ] +
              GRINExpense[ BankContractBean.BET_GROUP_CAPTURE_FEES ] +
              GRINExpense[ BankContractBean.BET_GROUP_DEBIT_FEES   ] +
              GRINExpense[ BankContractBean.BET_GROUP_PLAN_FEES    ] +
              GRINExpense[ BankContractBean.BET_GROUP_SYSTEM_FEES  ] +
              GRINExpense[ BankContractBean.BET_GROUP_EQUIP_RENTAL ] +
              GRINExpense[ BankContractBean.BET_GROUP_EQUIP_SALES  ] );
    }
  
    public double getGRINGroupExpense( int cat )
    {
      return( GRINExpense[cat] );
    }
  
    public double getGroupExpense( int cat )
    {
      return( Expense[cat] );
    }
  
    public double getGroupIncome( int cat )
    {
      return( Income[cat] );
    }
    
    // get the merchant billing income
    public double getMEBillingIncome( )
    {
      return( MerchantIncome );
    }
    public double getMECOGS( )
    {
      return( COGS );
    }
    
    
    public double getNetIncomeExpense( )
    {
      return( ( getDiscIcIncome() + getFeesTotalIncome() + getPartnershipIncome() + getAdjustmentsIncome() ) - 
              ( getDiscIcExpense() + getFeesTotalExpense() + getPartnershipExpense() + getAdjustmentsExpense() ) );
    }
    
    public double getNetRevenue( )
    {
      return( NetRevenue );
    }
  
    public double getPartnershipExpense( )
    {
      return( Expense[BankContractBean.BET_GROUP_PARTNERSHIP] );
    }
  
    public double getPartnershipIncome( )
    {
      return( Income[BankContractBean.BET_GROUP_PARTNERSHIP] );
    }
    
    public double getProcessingFees( )
    {
      return( ProcessingFees );
    }
    
    public double getReferralContractExpense()
    {
      return( ReferralFees );
    }
    
    public void reset( )
    {
      for( int i = 0; i < Expense.length; ++i )
      {
        Expense[i] = 0.0;
      }
      for( int i = 0; i < Income.length; ++i )
      {
        Income[i] = 0.0;
      }      
    }
    /* RS Code Changes ACH PS - get ACH Discount & Fees */
    public double getAchDiscIncome() {
      return (AchDiscInc);
    }
    public double getAchFeesIncome() {
      return (Income[BankContractBean.BET_GROUP_ACH] - AchDiscInc);
    }	
    public double getAchDiscExpense() {
      return (AchDiscExp);
    }
    public double getAchFeesExpense() {
      return (Expense[BankContractBean.BET_GROUP_ACH]- AchDiscExp);
    }
    
  }
  
  public class RetentionAppData
  {
    public int                AppCountTotal         = 0;
    public double             AppNDR                = 0;
    public double             AppSalesAmount        = 0.0;
    public double             AverageAccountLife    = 0.0;
    public long               OrgId                 = 0L;
  
    public RetentionAppData( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ResultSetIterator     it    = null;
      ResultSet             rs    = null;
      
      OrgId           = resultSet.getLong("org_num");
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:954^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum( decode( at.date_completed, null, 0, 1 ) )      as app_count_total,
//                    sum( nvl(mr.merch_month_visa_mc_sales,0) )          as app_sales, 
//                    round( sum(
//                      (nvl(mr.MERCH_MONTH_VISA_MC_SALES,0)  *  nvl(mf.visa_disc_rate, 0)/1000 * 0.01) +
//                      (trunc(nvl(mr.MERCH_MONTH_VISA_MC_SALES,0)/ decode(mr.MERCH_AVERAGE_CC_TRAN,0,1,null,1,mr.merch_average_cc_tran)) * nvl(mf.visa_per_item, 0)/1000 )
//                    ),2)                                                as income,
//                    round( sum( decode( tc.tranchrg_discrate_type,
//                            3, 0,
//                            (
//                              (nvl(mr.MERCH_MONTH_VISA_MC_SALES,0) * (nvl(icr.rate,1.87) + nvl(icr.assessment_rate,0.089)) * 0.01) +
//                              (trunc(nvl(mr.MERCH_MONTH_VISA_MC_SALES,0)/ decode(mr.MERCH_AVERAGE_CC_TRAN,0,1,null,1,mr.merch_average_cc_tran)) * nvl(icr.per_item,0.10))
//                            ) ) ),2)                                    as expense
//            from    group_merchant              gm,
//                    mif                         mf,
//                    merchant                    mr,
//                    app_tracking                at,
//                    tranchrg                    tc,
//                    commission_ic_rates         icr
//            where   gm.org_num = :OrgId and
//                    mf.merchant_number = gm.merchant_number and
//                    trunc( mf.activation_date,'month' ) between :ReportDateBegin and :ReportDateEnd and
//                    mr.merch_number = mf.merchant_number and
//                    tc.app_seq_num(+) = mr.app_seq_num and
//                    tc.cardtype_code(+) = 1 and
//                    icr.grid_type(+) = mr.pricing_grid and
//                    mr.date_activated between icr.valid_date_begin(+) and icr.valid_date_end(+) and
//                    at.app_seq_num(+) = mr.app_seq_num and
//                    at.dept_code(+)   = :QueueConstants.DEPARTMENT_CREDIT and -- 100 and
//                    at.status_code(+) = :QueueConstants.DEPT_STATUS_CREDIT_APPROVED -- 102
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum( decode( at.date_completed, null, 0, 1 ) )      as app_count_total,\n                  sum( nvl(mr.merch_month_visa_mc_sales,0) )          as app_sales, \n                  round( sum(\n                    (nvl(mr.MERCH_MONTH_VISA_MC_SALES,0)  *  nvl(mf.visa_disc_rate, 0)/1000 * 0.01) +\n                    (trunc(nvl(mr.MERCH_MONTH_VISA_MC_SALES,0)/ decode(mr.MERCH_AVERAGE_CC_TRAN,0,1,null,1,mr.merch_average_cc_tran)) * nvl(mf.visa_per_item, 0)/1000 )\n                  ),2)                                                as income,\n                  round( sum( decode( tc.tranchrg_discrate_type,\n                          3, 0,\n                          (\n                            (nvl(mr.MERCH_MONTH_VISA_MC_SALES,0) * (nvl(icr.rate,1.87) + nvl(icr.assessment_rate,0.089)) * 0.01) +\n                            (trunc(nvl(mr.MERCH_MONTH_VISA_MC_SALES,0)/ decode(mr.MERCH_AVERAGE_CC_TRAN,0,1,null,1,mr.merch_average_cc_tran)) * nvl(icr.per_item,0.10))\n                          ) ) ),2)                                    as expense\n          from    group_merchant              gm,\n                  mif                         mf,\n                  merchant                    mr,\n                  app_tracking                at,\n                  tranchrg                    tc,\n                  commission_ic_rates         icr\n          where   gm.org_num =  :1   and\n                  mf.merchant_number = gm.merchant_number and\n                  trunc( mf.activation_date,'month' ) between  :2   and  :3   and\n                  mr.merch_number = mf.merchant_number and\n                  tc.app_seq_num(+) = mr.app_seq_num and\n                  tc.cardtype_code(+) = 1 and\n                  icr.grid_type(+) = mr.pricing_grid and\n                  mr.date_activated between icr.valid_date_begin(+) and icr.valid_date_end(+) and\n                  at.app_seq_num(+) = mr.app_seq_num and\n                  at.dept_code(+)   =  :4   and -- 100 and\n                  at.status_code(+) =  :5   -- 102";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,OrgId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   __sJT_st.setInt(4,QueueConstants.DEPARTMENT_CREDIT);
   __sJT_st.setInt(5,QueueConstants.DEPT_STATUS_CREDIT_APPROVED);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:985^9*/
        rs = it.getResultSet();
      
        if ( rs.next() )
        {
          AppCountTotal           = rs.getInt("app_count_total");
          AppSalesAmount          = rs.getDouble("app_sales");
          AppNDR                  = rs.getDouble("income") -
                                    rs.getDouble("expense");
        }
        rs.close();
        it.close();
        
        /*@lineinfo:generated-code*//*@lineinfo:998^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  round( avg( sm_act.months_active ), 2 )       as avg_life
//            from    group_merchant              gm,
//                    merchant                    mr,
//                    application                 app,
//                    (
//                      select  sm.merchant_number          as merchant_number,
//                              sum( decode( nvl(sm.vmc_sales_amount,0),0,0,1 ) ) 
//                                                          as months_active
//                      from    group_merchant            gm,
//                              mif                       mf,
//                              monthly_extract_summary   sm
//                      where   gm.org_num = :OrgId and
//                              mf.merchant_number = gm.merchant_number and
//                              mf.dmacctst is null and
//                              sm.merchant_number = mf.merchant_number and
//                              sm.active_date between :ReportDateBegin and trunc(sysdate,'month') 
//                      group by sm.merchant_number                            
//                    )                           sm_act,
//                    (
//                      select  sm.merchant_number          as merchant_number,
//                              count(sm.merchant_number)   as active_months
//                      from    group_merchant            gm,
//                              mif                       mf,
//                              monthly_extract_summary   sm
//                      where   gm.org_num = :OrgId and
//                              mf.merchant_number = gm.merchant_number and
//                              mf.dmacctst is null and
//                              sm.merchant_number = mf.merchant_number and
//                              sm.active_date between trunc( trunc(sysdate,'month')-75, 'month' ) and trunc(trunc(sysdate,'month')-1,'month')
//                      group by sm.merchant_number
//                    )                           sm_act2,
//                    monthly_extract_summary     sm                  
//            where   gm.org_num = :OrgId and
//                    mf.merchant_number = gm.merchant_number and
//                    trunc(mf.activation_date,'month') between  :ReportDateBegin and :ReportDateEnd and
//                    sm_act.merchant_number = mf.merchant_number and
//                    sm_act2.merchant_number = sm_act.merchant_number and
//                    sm_act2.active_months > 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  round( avg( sm_act.months_active ), 2 )       as avg_life\n          from    group_merchant              gm,\n                  merchant                    mr,\n                  application                 app,\n                  (\n                    select  sm.merchant_number          as merchant_number,\n                            sum( decode( nvl(sm.vmc_sales_amount,0),0,0,1 ) ) \n                                                        as months_active\n                    from    group_merchant            gm,\n                            mif                       mf,\n                            monthly_extract_summary   sm\n                    where   gm.org_num =  :1   and\n                            mf.merchant_number = gm.merchant_number and\n                            mf.dmacctst is null and\n                            sm.merchant_number = mf.merchant_number and\n                            sm.active_date between  :2   and trunc(sysdate,'month') \n                    group by sm.merchant_number                            \n                  )                           sm_act,\n                  (\n                    select  sm.merchant_number          as merchant_number,\n                            count(sm.merchant_number)   as active_months\n                    from    group_merchant            gm,\n                            mif                       mf,\n                            monthly_extract_summary   sm\n                    where   gm.org_num =  :3   and\n                            mf.merchant_number = gm.merchant_number and\n                            mf.dmacctst is null and\n                            sm.merchant_number = mf.merchant_number and\n                            sm.active_date between trunc( trunc(sysdate,'month')-75, 'month' ) and trunc(trunc(sysdate,'month')-1,'month')\n                    group by sm.merchant_number\n                  )                           sm_act2,\n                  monthly_extract_summary     sm                  \n          where   gm.org_num =  :4   and\n                  mf.merchant_number = gm.merchant_number and\n                  trunc(mf.activation_date,'month') between   :5   and  :6   and\n                  sm_act.merchant_number = mf.merchant_number and\n                  sm_act2.merchant_number = sm_act.merchant_number and\n                  sm_act2.active_months > 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,OrgId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setLong(3,OrgId);
   __sJT_st.setLong(4,OrgId);
   __sJT_st.setDate(5,ReportDateBegin);
   __sJT_st.setDate(6,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1038^9*/
        rs = it.getResultSet();
        if ( rs.next() )
        {
          AverageAccountLife = rs.getDouble("avg_life");
        }
        rs.close();
        it.close();
      }
      catch( Exception e )
      {
        logEntry("RetentionAppData()",e.toString());
      }        
      finally
      {
        try{ it.close(); } catch(Exception e){ }
      }
    }
  }
  
  public class RetentionSummaryData extends SummaryData
  {
    public int                ActivatedCount        = 0;
    public int                ActiveCount           = 0;
    public Date               ActiveDate            = null;
    public int                AppCount              = 0;
    public RetentionAppData   AppData               = null;
    public int                ApprovedAppCount      = 0;
    public int                ClosedCount           = 0;
    public double             CommissionAmount      = 0.0;
    public double             FeesIncome            = 0;
    public Date               MonthBeginDate        = null;
    public Date               MonthEndDate          = null;
    public double             NetDiscountRevenue    = 0;
    public int                SetupAppCount         = 0;
    
    public RetentionSummaryData( RetentionAppData appData, ResultSet resultSet, int userType )
      throws java.sql.SQLException
    {
      super( resultSet, userType );
      
      AppData = appData;
      
      // store the current month
      ActiveDate      = resultSet.getDate("active_date");
      
      MonthBeginDate  = resultSet.getDate("month_begin_date");
      MonthEndDate    = resultSet.getDate("month_end_date");
      
      /*@lineinfo:generated-code*//*@lineinfo:1087^7*/

//  ************************************************************
//  #sql [Ctx] { select  count( mf.merchant_number )     
//          from    group_merchant            gm,
//                  mif                       mf,
//                  (
//                    select  sm.merchant_number            as merchant_number,
//                            count(sm.merchant_number)     as month_count
//                    from    group_merchant            gm,
//                            mif                       mf,
//                            monthly_extract_summary   sm
//                    where   gm.org_num = :OrgId and
//                            mf.merchant_number = gm.merchant_number and
//                            trunc(mf.activation_date,'month') between :ReportDateBegin and :ReportDateEnd and
//                            sm.merchant_number = mf.merchant_number and
//                            sm.active_date between trunc( :ActiveDate-50, 'month' ) and :ActiveDate and
//                            (nvl(sm.vmc_sales_amount,0)+nvl(sm.cash_advance_vol_amount,0)) >= 10
//                    group by sm.merchant_number                          
//                  )                         act
//          where   gm.org_num = :OrgId and
//                  mf.merchant_number = gm.merchant_number and
//                  trunc(mf.activation_date,'month') between :ReportDateBegin and :ReportDateEnd and
//                  act.merchant_number = mf.merchant_number and
//                  act.month_count > 0 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( mf.merchant_number )      \n        from    group_merchant            gm,\n                mif                       mf,\n                (\n                  select  sm.merchant_number            as merchant_number,\n                          count(sm.merchant_number)     as month_count\n                  from    group_merchant            gm,\n                          mif                       mf,\n                          monthly_extract_summary   sm\n                  where   gm.org_num =  :1   and\n                          mf.merchant_number = gm.merchant_number and\n                          trunc(mf.activation_date,'month') between  :2   and  :3   and\n                          sm.merchant_number = mf.merchant_number and\n                          sm.active_date between trunc(  :4  -50, 'month' ) and  :5   and\n                          (nvl(sm.vmc_sales_amount,0)+nvl(sm.cash_advance_vol_amount,0)) >= 10\n                  group by sm.merchant_number                          \n                )                         act\n        where   gm.org_num =  :6   and\n                mf.merchant_number = gm.merchant_number and\n                trunc(mf.activation_date,'month') between  :7   and  :8   and\n                act.merchant_number = mf.merchant_number and\n                act.month_count > 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,OrgId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   __sJT_st.setDate(4,ActiveDate);
   __sJT_st.setDate(5,ActiveDate);
   __sJT_st.setLong(6,OrgId);
   __sJT_st.setDate(7,ReportDateBegin);
   __sJT_st.setDate(8,ReportDateEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   ActiveCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1111^7*/
    }
    
    public void addData( ResultSet resultSet )
      throws java.sql.SQLException 
    {
      super.addData(resultSet);
      
      // add the counts
//@      ActiveCount     += resultSet.getInt("active_count");
      ActivatedCount  += resultSet.getInt("activated_count");
      
      
      // extract the actual NDR for this month
      NetDiscountRevenue  +=  ( resultSet.getDouble("disc_ic_inc") -
                                ( resultSet.getDouble("ic_exp") + 
                                  resultSet.getDouble("assessment") )
                              );
                              
      FeesIncome      +=  ( resultSet.getDouble("capture_inc") +
                            resultSet.getDouble("debit_inc") +
                            resultSet.getDouble("plan_inc") +
                            resultSet.getDouble("auth_inc") +
                            resultSet.getDouble("sys_gen_inc") +
                            resultSet.getDouble("equip_rental_inc") +
                            resultSet.getDouble("equip_sales_inc") +
                            resultSet.getDouble("equip_sales_tax") );
    }
    
    public int compareTo( Object obj )
    {
      RetentionSummaryData  compareObj    = (RetentionSummaryData) obj;
      int                   retVal        = 0;
      
      if ( (retVal = OrgName.compareTo(compareObj.OrgName)) == 0 )
      {
        if ( (retVal = (int)(HierarchyNode - compareObj.HierarchyNode)) == 0 )
        {
          if ( (retVal = (int)(OrgId - compareObj.OrgId)) == 0 )
          {
            retVal = ActiveDate.compareTo( compareObj.ActiveDate );
          }
        }
      }
      return( retVal );
    }
    
    public double getAcquisitionCost( )
    {
      double            retVal             = 0.0;
      
      retVal  = NetRevenue;  // start with all merchant income
      retVal += CommissionAmount;     // add in direct commission paid
      retVal -= ProcessingFees;      // take out MES contract fees
      
      return( retVal );
    }
    
    public void loadAppStatsData( )
    {
      ResultSetIterator     it    = null;
      ResultSet             rs    = null;
    
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:1176^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum( decode(app_count.app_seq_num,null,0,1) )         as new_app_count,
//                    sum( decode( at.date_completed, null, 0, 1 ) )        as approved_apps,
//                    sum( decode( mf_open.date_opened,null,0,1 ) )         as setup_apps,
//                    sum( decode( mf_act.activation_date,null,0,1 ) )      as activated_apps,
//                    sum( decode( mf_close.date_stat_chgd_to_dcb,null,0,1 ) )  as closed_apps,
//                    sum( decode( ( nvl(sm.vmc_sales_amount,0) +
//                                   nvl(sm.cash_advance_vol_amount,0) -
//                                   least( (nvl(sm.vmc_sales_amount,0) + 
//                                           nvl(sm.cash_advance_vol_amount,0)),
//                                          9.99
//                                         ) 
//                                  ), 
//                                 0, 0, 1 ) )                              as active_count,
//                    sum( nvl( c.commission_amount,0 ) )                   as commission_amount
//            from    group_merchant              gm,
//                    merchant                    mr,
//                    application                 app,
//                    application                 app_count,
//                    mif                         mf,
//                    mif                         mf_act,
//                    mif                         mf_open, 
//                    mif                         mf_close,
//                    app_tracking                at,
//                    monthly_extract_summary     sm,
//                    (
//                      select  c.merchant_number           as merchant_number,
//                              sum( c.commission_amount )  as commission_amount
//                      from    group_merchant      gm,
//                              commissions         c
//                      where   gm.org_num = :OrgId and
//                              c.merchant_number = gm.merchant_number and
//                              c.commission_date between :MonthBeginDate and :MonthEndDate and
//                              c.transaction_type = 'I'
//                      group by c.merchant_number                            
//                    )                           c
//            where   gm.org_num = :OrgId and
//                    mr.merch_number = gm.merchant_number and
//                    app.app_seq_num = mr.app_seq_num and
//                    trunc( app.app_created_date,'month' ) between :ReportDateBegin and :ReportDateEnd and
//                    app_count.app_seq_num(+) = app.app_seq_num and
//                    trunc( app_count.app_created_date(+),'month' ) = :ActiveDate and
//                    mf.merchant_number = mr.merch_number and
//                    at.app_seq_num(+) = app.app_seq_num and
//                    at.dept_code(+) = 100 and
//                    at.status_code(+) = 102 and
//                    trunc(at.date_completed(+)) between :MonthBeginDate and :MonthEndDate and
//                    mf_act.merchant_number(+) = mf.merchant_number and
//                    mf_act.activation_date(+) between :MonthBeginDate+1 and :MonthEndDate+1 and
//                    mf_open.merchant_number(+) = mf.merchant_number and
//                    to_date(mf_open.date_opened(+),'mmddrr') between :MonthBeginDate and :MonthEndDate and
//                    mf_close.merchant_number(+) = mf.merchant_number and
//                    mf_close.date_stat_chgd_to_dcb(+) between :MonthBeginDate and :MonthEndDate and
//                    sm.merchant_number(+) = mf.merchant_number and
//                    sm.active_date(+) = :ActiveDate and
//                    c.merchant_number(+) = mf.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum( decode(app_count.app_seq_num,null,0,1) )         as new_app_count,\n                  sum( decode( at.date_completed, null, 0, 1 ) )        as approved_apps,\n                  sum( decode( mf_open.date_opened,null,0,1 ) )         as setup_apps,\n                  sum( decode( mf_act.activation_date,null,0,1 ) )      as activated_apps,\n                  sum( decode( mf_close.date_stat_chgd_to_dcb,null,0,1 ) )  as closed_apps,\n                  sum( decode( ( nvl(sm.vmc_sales_amount,0) +\n                                 nvl(sm.cash_advance_vol_amount,0) -\n                                 least( (nvl(sm.vmc_sales_amount,0) + \n                                         nvl(sm.cash_advance_vol_amount,0)),\n                                        9.99\n                                       ) \n                                ), \n                               0, 0, 1 ) )                              as active_count,\n                  sum( nvl( c.commission_amount,0 ) )                   as commission_amount\n          from    group_merchant              gm,\n                  merchant                    mr,\n                  application                 app,\n                  application                 app_count,\n                  mif                         mf,\n                  mif                         mf_act,\n                  mif                         mf_open, \n                  mif                         mf_close,\n                  app_tracking                at,\n                  monthly_extract_summary     sm,\n                  (\n                    select  c.merchant_number           as merchant_number,\n                            sum( c.commission_amount )  as commission_amount\n                    from    group_merchant      gm,\n                            commissions         c\n                    where   gm.org_num =  :1   and\n                            c.merchant_number = gm.merchant_number and\n                            c.commission_date between  :2   and  :3   and\n                            c.transaction_type = 'I'\n                    group by c.merchant_number                            \n                  )                           c\n          where   gm.org_num =  :4   and\n                  mr.merch_number = gm.merchant_number and\n                  app.app_seq_num = mr.app_seq_num and\n                  trunc( app.app_created_date,'month' ) between  :5   and  :6   and\n                  app_count.app_seq_num(+) = app.app_seq_num and\n                  trunc( app_count.app_created_date(+),'month' ) =  :7   and\n                  mf.merchant_number = mr.merch_number and\n                  at.app_seq_num(+) = app.app_seq_num and\n                  at.dept_code(+) = 100 and\n                  at.status_code(+) = 102 and\n                  trunc(at.date_completed(+)) between  :8   and  :9   and\n                  mf_act.merchant_number(+) = mf.merchant_number and\n                  mf_act.activation_date(+) between  :10  +1 and  :11  +1 and\n                  mf_open.merchant_number(+) = mf.merchant_number and\n                  to_date(mf_open.date_opened(+),'mmddrr') between  :12   and  :13   and\n                  mf_close.merchant_number(+) = mf.merchant_number and\n                  mf_close.date_stat_chgd_to_dcb(+) between  :14   and  :15   and\n                  sm.merchant_number(+) = mf.merchant_number and\n                  sm.active_date(+) =  :16   and\n                  c.merchant_number(+) = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,OrgId);
   __sJT_st.setDate(2,MonthBeginDate);
   __sJT_st.setDate(3,MonthEndDate);
   __sJT_st.setLong(4,OrgId);
   __sJT_st.setDate(5,ReportDateBegin);
   __sJT_st.setDate(6,ReportDateEnd);
   __sJT_st.setDate(7,ActiveDate);
   __sJT_st.setDate(8,MonthBeginDate);
   __sJT_st.setDate(9,MonthEndDate);
   __sJT_st.setDate(10,MonthBeginDate);
   __sJT_st.setDate(11,MonthEndDate);
   __sJT_st.setDate(12,MonthBeginDate);
   __sJT_st.setDate(13,MonthEndDate);
   __sJT_st.setDate(14,MonthBeginDate);
   __sJT_st.setDate(15,MonthEndDate);
   __sJT_st.setDate(16,ActiveDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1233^9*/
        rs = it.getResultSet();      
        if ( rs.next() )
        {
          AppCount                = rs.getInt("new_app_count");
          ApprovedAppCount        = rs.getInt("approved_apps");
          SetupAppCount           = rs.getInt("setup_apps");
          ActivatedCount          = rs.getInt("activated_apps");
          ClosedCount             = rs.getInt("closed_apps");
          ActiveCount             = rs.getInt("active_count");
          CommissionAmount        = rs.getDouble("commission_amount");
        }
        rs.close();
        it.close();
      }
      catch( Exception e )
      {
        logEntry("RetentionSummary()",e.toString());
      }        
      finally
      {
        try{ it.close(); } catch(Exception e){ }
      }
    }
    
    public double loadAppNDR( )
    {
      ResultSetIterator     it        = null;
      ResultSet             resultSet = null;
      double                retVal    = 0.0;
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:1266^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  round( sum(
//                    (nvl(mr.MERCH_MONTH_VISA_MC_SALES,0)  *  nvl(mf.visa_disc_rate, 0)/1000 * 0.01) +
//                    (trunc(nvl(mr.MERCH_MONTH_VISA_MC_SALES,0)/ decode(mr.MERCH_AVERAGE_CC_TRAN,0,1,null,1,mr.merch_average_cc_tran)) * nvl(mf.visa_per_item, 0)/1000 )
//                  ),2)                                                as income,
//                  round( sum( decode( tc.tranchrg_discrate_type,
//                          3, 0,
//                          (
//                            (nvl(mr.MERCH_MONTH_VISA_MC_SALES,0) * (nvl(icr.rate,1.87) + nvl(icr.assessment_rate,0.089)) * 0.01) +
//                            (trunc(nvl(mr.MERCH_MONTH_VISA_MC_SALES,0)/ decode(mr.MERCH_AVERAGE_CC_TRAN,0,1,null,1,mr.merch_average_cc_tran)) * nvl(icr.per_item,0.10))
//                          ) ) ),2)                                    as expense
//            from    group_merchant      gm,
//                    mif                 mf,
//                    merchant            mr,
//                    tranchrg            tc,
//                    commission_ic_rates icr
//            where   gm.org_num = :OrgId and
//                    mf.merchant_number = gm.merchant_number and
//                    trunc( mf.activation_date, 'month' ) between :ReportDateBegin and :ReportDateEnd and
//                    mr.merch_number = mf.merchant_number and
//                    tc.app_seq_num = mr.app_seq_num and
//                    tc.cardtype_code = 1 and
//                    icr.grid_type = mr.pricing_grid and
//                    mr.date_activated between icr.valid_date_begin(+) and icr.valid_date_end(+)        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  round( sum(\n                  (nvl(mr.MERCH_MONTH_VISA_MC_SALES,0)  *  nvl(mf.visa_disc_rate, 0)/1000 * 0.01) +\n                  (trunc(nvl(mr.MERCH_MONTH_VISA_MC_SALES,0)/ decode(mr.MERCH_AVERAGE_CC_TRAN,0,1,null,1,mr.merch_average_cc_tran)) * nvl(mf.visa_per_item, 0)/1000 )\n                ),2)                                                as income,\n                round( sum( decode( tc.tranchrg_discrate_type,\n                        3, 0,\n                        (\n                          (nvl(mr.MERCH_MONTH_VISA_MC_SALES,0) * (nvl(icr.rate,1.87) + nvl(icr.assessment_rate,0.089)) * 0.01) +\n                          (trunc(nvl(mr.MERCH_MONTH_VISA_MC_SALES,0)/ decode(mr.MERCH_AVERAGE_CC_TRAN,0,1,null,1,mr.merch_average_cc_tran)) * nvl(icr.per_item,0.10))\n                        ) ) ),2)                                    as expense\n          from    group_merchant      gm,\n                  mif                 mf,\n                  merchant            mr,\n                  tranchrg            tc,\n                  commission_ic_rates icr\n          where   gm.org_num =  :1   and\n                  mf.merchant_number = gm.merchant_number and\n                  trunc( mf.activation_date, 'month' ) between  :2   and  :3   and\n                  mr.merch_number = mf.merchant_number and\n                  tc.app_seq_num = mr.app_seq_num and\n                  tc.cardtype_code = 1 and\n                  icr.grid_type = mr.pricing_grid and\n                  mr.date_activated between icr.valid_date_begin(+) and icr.valid_date_end(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,OrgId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1291^9*/
        resultSet = it.getResultSet();
        
        if ( resultSet.next() )
        {
          retVal = resultSet.getDouble("income") -
                   resultSet.getDouble("expense");
        }
        resultSet.close();
        it.close();
      }
      catch( java.sql.SQLException e )
      {
        logEntry("loadAppNDR()",e.toString());
      }
      finally
      {
        try{ it.close(); } catch(Exception e){ }
      }
      
      return( retVal );
    }
    
    public void reset( )
    {
      super.reset();
      
      // reset internals
      ActiveCount           = 0;
      ClosedCount           = 0;
      AppCount              = 0;
      SetupAppCount         = 0;
    }
  }

  private   boolean     AuditSummaryMode              = false;  
  //private   String      ConnectionString              = DEFAULT_CONNECTION_STRING;  // see database/SQLJConnectionBase.sqlj
  private   boolean     HasCashAdvanceContractExpense = false;
  private   boolean     HasCashAdvanceContractIncome  = false;
  private   boolean     HasLiabilityContract          = false;
  private   boolean     HasResellerContract           = false;
  private static Date   NetSalesCutoffDate            = null;
  private   int         ProfReportType                = MERCH_PROF_TYPE_DEFAULT;
  private   int         RelationshipType              = -1;
  private   int         ReportBetGroup                = BankContractBean.BET_GROUP_ALL;
  private   int         ReportDataElementsIndex       = 0;
  private   int         ReportUserType                = UT_INVALID;
  private   boolean     ShowClosedAccounts            = true;
  
  /*
  ** CONSTRUCTOR
  */
  public BankContractDataBean( )
  {
  }
  
  protected void addContractElement( long childId, int dataType, int contractType, ContractTypes.PerItemRecord record )
  {
    try
    {
      ChildProfitabilitySummary  childData = findChildData( childId );
    
      if ( childData == null )
      {
        childData = new ChildProfitabilitySummary( Ctx, childId, ReportDateBegin, ReportDateEnd );
        ReportRows.add( childData );
      }
      childData.addContractElement( dataType, contractType, record );
    }
    catch( Exception e )
    {
      logEntry(  "addContractElement: ", e.toString());
      addError("addContractElement: " + e.toString());
    }
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long childNodeId, Date beginDate, Date endDate )
  {
    super.encodeNodeUrl(buffer,childNodeId,beginDate,endDate);
    buffer.append("&defaultOrgId=");
    buffer.append(getReportOrgIdDefault());
    buffer.append("&profReportType=");
    buffer.append(ProfReportType);
    if ( ProfReportType == MERCH_PROF_TYPE_UNDER_WATER )
    {
      try
      {
        double value = fields.getField("uwThreshold").asDouble();
        
        buffer.append("&uwThreshold=");
        buffer.append(value);
      }
      catch( Exception e )
      {
        // ignore
      }        
    }      
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    int               reportBankId      = getReportBankId();
  
    line.setLength(0);
    
    switch( getProfReportType() )
    {
      case MERCH_PROF_TYPE_SUMMARY:
      case MERCH_PROF_TYPE_UNDER_WATER:
        line.append("\"Organization\",");
        line.append("\"Org Id\",");
        line.append("\"Disc/IC Income\",");
        line.append("\"Disc/IC Expense\",");
        if ( reportBankId != mesConstants.BANK_ID_MES ) 
        {
          line.append("\"Net Disc/IC\",");
        }
        line.append("\"Fees Income\",");
        line.append("\"Fees Expense\",");
        if ( reportBankId != mesConstants.BANK_ID_MES ) 
        {
          line.append("\"Net Fees\",");
        }
        if( hasPartnershipContract() )
        {
          line.append("\"Partnership\",");
        }
        line.append("\"Adjustments\",");
        if ( reportBankId == mesConstants.BANK_ID_MES ) 
        {
          line.append("\"Net Processing P/L\",");
          line.append("\"Sales Tax\",");
          line.append("\"Product Fees\",");
          line.append("\"COGS\",");
        }          
        if ( reportBankId == mesConstants.BANK_ID_CBT ) 
        {
          line.append("\"Sales Volume\",");
          line.append("\"Spread\",");
          if( isNodeAssociation(getReportHierarchyNode()) )
          {
            line.append("\"Acct Status\",");
          }
        }          
        line.append("\"Net P/L\"");
        break;
        
      case MERCH_PROF_TYPE_NET_REVENUE_RETENTION:
        line.append("\"Org Id\",");
        line.append("\"Org Name\",");
        line.append("\"Billing Month\",");
        line.append("\"Activated\",");
        line.append("\"Active\",");
        line.append("\"VMC Sales Count\",");
        line.append("\"VMC Sales Amount\",");
        line.append("\"Net Revenue\"");
        break;
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    SummaryData       record            = (SummaryData)obj;
    int               reportBankId      = getReportBankId();
    
    // inc/exp values
    double            adjustmentsExp    = 0.0;
    double            adjustmentsInc    = 0.0;
    double            discIcExpense     = 0.0;
    double            discIcIncome      = 0.0;
    double            excludedEquipExp  = 0.0;
    double            excludedFeesExp   = 0.0;
    double            feesExpense       = 0.0;
    double            feesIncome        = 0.0;
    double            netProcPL         = 0.0;
    double            netProcTotal      = 0.0;
    double            netProfitLoss     = 0.0;
    double            netTotal          = 0.0;
    double            partnershipExp    = 0.0;
    double            partnershipInc    = 0.0;
    double            referralExp       = 0.0;
    double            salesAmount       = 0.0;
    double            salesTax          = 0.0;
    
    // extract the numbers from the record
    discIcIncome    = record.getDiscIcIncome();
    discIcExpense   = record.getDiscIcExpense();
    feesIncome      = record.getFeesTotalIncome();
    feesExpense     = record.getFeesTotalExpense();
    partnershipInc  = record.getPartnershipIncome();
    partnershipExp  = record.getPartnershipExpense();
    adjustmentsInc  = record.getAdjustmentsIncome();
    adjustmentsExp  = record.getAdjustmentsExpense();
    excludedEquipExp= record.getExcludedEquipExpense();
    excludedFeesExp = record.getExcludedFeesExpense();
    referralExp     = record.getReferralContractExpense();
    salesAmount     = record.VmcSalesAmount + record.CashAdvanceAmount;
    salesTax        = record.getEquipSalesTax();
  
    netProcPL       = ( ( discIcIncome + feesIncome + partnershipInc + adjustmentsInc ) - 
                        ( discIcExpense + feesExpense + partnershipExp + adjustmentsExp ) );
  
    netProfitLoss   = ( netProcPL - ( excludedEquipExp + excludedFeesExp ) );
    
    // 3941 excludes sales tax from the amount returned by 
    // SummaryData.getFeesTotalIncome() to allow it to appear
    // in a separate column on this report.  It needs to be 
    // added back in manually to the net P/L.
    if ( reportBankId == mesConstants.BANK_ID_MES )
    {
      netProfitLoss += salesTax;
    }
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    
    switch( getProfReportType() )
    {
      case MERCH_PROF_TYPE_SUMMARY:
      case MERCH_PROF_TYPE_UNDER_WATER:
        line.append("\"");
        line.append(record.OrgName);
        line.append("\",");
        line.append( encodeHierarchyNode(record.HierarchyNode) );
        line.append(",");
        line.append(discIcIncome);
        line.append(",");
        line.append(discIcExpense);
        line.append(",");
        if ( reportBankId != mesConstants.BANK_ID_MES ) 
        {
          line.append( discIcIncome - discIcExpense );
          line.append(",");
        }
        line.append(feesIncome);
        line.append(",");
        line.append(feesExpense);
        line.append(",");
        if ( reportBankId != mesConstants.BANK_ID_MES ) 
        {
          line.append(feesIncome - feesExpense);
          line.append(",");
        }
        if ( hasPartnershipContract() == true )
        {
          line.append(partnershipInc - partnershipExp);
          line.append(",");
        }
        line.append(adjustmentsInc - adjustmentsExp);
        line.append(",");
        if ( reportBankId == mesConstants.BANK_ID_MES ) 
        {
          line.append(netProcPL);
          line.append(",");
          line.append(salesTax);
          line.append(",");
          line.append(excludedFeesExp);
          line.append(",");
          line.append(excludedEquipExp);
          line.append(",");
        }
        if ( reportBankId == mesConstants.BANK_ID_CBT ) 
        {
          line.append(salesAmount);
          line.append(",");
          line.append( (salesAmount == 0.0) ? "" : String.valueOf(netProfitLoss/salesAmount) );
          line.append(",");
          if( isNodeAssociation(getReportHierarchyNode()) )
          {
            line.append(record.AccountStatus == null ? "" : record.AccountStatus);
            line.append(",");
          }
        }
        line.append(netProfitLoss);
        break;
        
      case MERCH_PROF_TYPE_NET_REVENUE_RETENTION:
        line.append(encodeHierarchyNode(record.HierarchyNode));
        line.append(",\"");
        line.append(record.OrgName);
        line.append("\",");
        line.append( DateTimeFormatter.getFormattedDate(record.ActiveDate,"MM/dd/yyyy") );
        line.append(",");
        line.append(record.ActivatedCount);
        line.append(",");
        line.append(record.ActiveCount);
        line.append(",");
        line.append(record.VmcSalesCount);
        line.append(",");
        line.append(record.VmcSalesAmount);
        line.append(",");
        line.append( String.valueOf((discIcIncome + feesIncome + partnershipInc) -
                     (discIcExpense + partnershipExp + referralExp)) );
        break;
    }
  }
  
  protected ChildProfitabilitySummary findChildData( long orgId )
  {
    ChildProfitabilitySummary       retVal = null;
    
    for ( int i = 0; i < ReportRows.size(); ++i )
    {
      if ( ((ChildProfitabilitySummary)ReportRows.elementAt(i)).getOrgId() == orgId )
      {
        retVal = (ChildProfitabilitySummary)ReportRows.elementAt(i);
        break;
      }
    }
    return( retVal );
  }
  
  public ChildProfitabilitySummary firstChild( )
  {
    ReportDataElementsIndex = 0;
    return( nextChild() );
  }
  
  public long getContractNode( long hierarchyNode, int contractType )
  {
    return( getContractNode(hierarchyNode,contractType,null) );
  }
  
  public long getContractNode( long hierarchyNode, int contractType, Date activeDate )
  {
    long        nodeId      = hierarchyNode;
    long        retVal      = 0L;
    int         rowCount    = 0;
    
    try
    {
      while( nodeId != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1625^9*/

//  ************************************************************
//  #sql [Ctx] { select  count( abc.hierarchy_node ) 
//            from    agent_bank_contract     abc
//            where   abc.hierarchy_node = :nodeId 
//                    and abc.contract_type = :contractType
//                    and nvl(:activeDate,trunc(trunc(sysdate,'month')-1,'month')) 
//                          between abc.valid_date_begin and abc.valid_date_end
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( abc.hierarchy_node )  \n          from    agent_bank_contract     abc\n          where   abc.hierarchy_node =  :1   \n                  and abc.contract_type =  :2  \n                  and nvl( :3  ,trunc(trunc(sysdate,'month')-1,'month')) \n                        between abc.valid_date_begin and abc.valid_date_end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,contractType);
   __sJT_st.setDate(3,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1633^9*/
      
        if ( rowCount > 0 )
        {
          // found a match, break from the loop
          retVal = nodeId;
          break;
        }
      
        // no match, get the parent node id
        nodeId = getParentNode(nodeId);
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "getContractNode()", e.toString() );
    }
    return( retVal );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    
    switch( getProfReportType() )
    {
      case MERCH_PROF_TYPE_NET_REVENUE_RETENTION:
        filename.append("_net_revenue_retention_");
        break;
        
      case MERCH_PROF_TYPE_SUMMARY:
        filename.append("_prof_summary_");
        break;
      
      case MERCH_PROF_TYPE_UNDER_WATER:
        filename.append("_under_water_summary_");
        break;
    }
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin,"MMMyyyy" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd,"MMMyyyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public static Date getNetSalesCutoffDate( )
  {
    if ( NetSalesCutoffDate == null )
    {
      Calendar        cal   = Calendar.getInstance();
    
      // 30-SEP-2004
      cal.set( Calendar.DAY_OF_MONTH, 30 );
      cal.set( Calendar.MONTH, Calendar.SEPTEMBER );
      cal.set( Calendar.YEAR, 2004 );
    
      NetSalesCutoffDate = new java.sql.Date( cal.getTime().getTime() );
    }      
    return( NetSalesCutoffDate ); 
  }
  
  public int getReportBetGroup( )
  {
    return( ReportBetGroup );
  }
  
  public String getReportTitle( )
  {
    String      retVal  = "No Title";
    
    switch( ProfReportType )
    {
      case MERCH_PROF_TYPE_UNDER_WATER:
        retVal = "Under Water Merchants";
        break;
        
      case MERCH_PROF_TYPE_SUMMARY:
        retVal = "Bank Income & Expense Summary";
        break;
        
      case MERCH_PROF_TYPE_NET_REVENUE:
        retVal = "Net Revenue Summary";
        break;
        
      case MERCH_PROF_TYPE_ME_REVENUE:
        retVal = "Monthly Revenue Summary";
        break;
        
      case MERCH_PROF_TYPE_RETENTION:
        retVal = "Attrition Analysis";
        break;
        
      case MERCH_PROF_TYPE_NET_REVENUE_RETENTION:
        retVal = "Net Revenue Attrition/Accretion Analysis";
        break;
        
      case MERCH_PROF_TYPE_REVENUE_PLUS_EQUIP:
        retVal = "Bank Income & Expense Plus Equipment";
        break;
        
      case MERCH_PROF_TYPE_AVG_NET_DISC:
        retVal = "Average Net Discount";
        break;
        
      case MERCH_PROF_TYPE_TPS_GRIN:
        retVal = "Transcom GRIN Summary";
        break;
    }
    return( retVal );
  }
  
  public long getParentNode( long nodeId )
  {
    ResultSetIterator       it          = null;
    ResultSet               resultSet   = null;
    long                    retVal      = 0L;
    
    // if report date begin is null, this is being called
    // from init code.  nothing can be done except use 
    // the default hierarchy
    if ( isNodeMerchant(nodeId) && ReportDateBegin != null )
    {
      // special handling on the profitability reporting.
      // accounts are locked to their association at month
      // end using the association in the extract.  if 
      // the association changes, we still want the merchant
      // to appear under the month end assocation.
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:1769^9*/

//  ************************************************************
//  #sql [Ctx] it = { -- default to MIF assoc if no m/e data available
//            select  nvl(sm.assoc_hierarchy_node,
//                        mf.association_node)    as assoc_node,
//                    sm.hh_load_sec      as hh_load_sec    
//            from    mif                       mf,
//                    monthly_extract_summary   sm
//            where   mf.merchant_number = :nodeId and
//                    sm.merchant_number(+) = mf.merchant_number and
//                    sm.active_date(+) between :ReportDateBegin and :ReportDateEnd
//            order by sm.active_date desc                
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "-- default to MIF assoc if no m/e data available\n          select  nvl(sm.assoc_hierarchy_node,\n                      mf.association_node)    as assoc_node,\n                  sm.hh_load_sec      as hh_load_sec    \n          from    mif                       mf,\n                  monthly_extract_summary   sm\n          where   mf.merchant_number =  :1   and\n                  sm.merchant_number(+) = mf.merchant_number and\n                  sm.active_date(+) between  :2   and  :3  \n          order by sm.active_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1781^9*/
        resultSet = it.getResultSet();
        if ( resultSet.next() )
        {
      if (getHHLoadSec() == 0) {
          setHHLoadSec(resultSet.getLong ("hh_load_sec"));
          }
          retVal = resultSet.getLong("assoc_node");
        }
        resultSet.close();
        it.close();
      }
      catch( Exception e )
      {
        logEntry( "getParentNode()", e.toString() );
      }        
      finally
      {
        try{ it.close(); } catch( Exception e ) {}
      }
    }
    else
    {
      // use normal hierarchy above the assoc level
      retVal = super.getParentNode(nodeId);
    }
    return( retVal );
  }
  
  public int getProfReportType()
  {
    return(ProfReportType);
  }
  
  public int getUserType( )
  {
    boolean       bankUser        = isBankUser();
    int           bankId          = getReportBankIdDefault();
    long          defaultNode     = getReportHierarchyNodeDefault();
    
    if ( ReportUserType == UT_INVALID )
    {
      if ( isUserUnderContract() )
      {
        if ( bankUser )   // "banks" have their own processing contract
        {
          ReportUserType = UT_VAR_BANK;       // we are a VAR to this bank
        }
        else
        {
          ReportUserType = UT_CONTRACT_BANK;  // bank piggy backs on our contract
        }   
      }
      else if ( getContractNode(defaultNode,ContractTypes.CONTRACT_SOURCE_RESELLER) != 0L )
      {
        // user is a reseller
        ReportUserType = UT_RESELLER;
      }
      else
      {
        if ( ( bankId == mesConstants.BANK_ID_MES ) || ( bankId == 9999 ) )
        {
          ReportUserType = UT_MES;
        }
        else
        {
          ReportUserType = UT_VAR_BANK;   // by default this is just a VAR bank
        }
      }
    }
    return( ReportUserType );
  }
  
  public boolean hasCashAdvanceReferralContract( )
  {
    return( hasCashAdvanceReferralContract( getReportHierarchyNodeDefault() ) );
  }
  
  public boolean hasCashAdvanceReferralContract( long nodeId )
  {
    int             rowCount      = 0;
    
    try
    {
      while( nodeId != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1867^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(abc.hierarchy_node) 
//            from    agent_bank_contract abc
//            where   abc.hierarchy_node = :nodeId and
//                    abc.contract_type = :ContractTypes.CONTRACT_SOURCE_REFERRAL and
//                    abc.billing_element_type = :BankContractBean.BET_CASH_ADV_TRANSACTION_FEE
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(abc.hierarchy_node)  \n          from    agent_bank_contract abc\n          where   abc.hierarchy_node =  :1   and\n                  abc.contract_type =  :2   and\n                  abc.billing_element_type =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,ContractTypes.CONTRACT_SOURCE_REFERRAL);
   __sJT_st.setInt(3,BankContractBean.BET_CASH_ADV_TRANSACTION_FEE);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1874^9*/
         
        if ( rowCount > 0 )
        {
          break;
        }
        nodeId = getParentNode(nodeId);
      }
    }
    catch( java.sql.SQLException e )
    {
    }
    return( nodeId != 0L );
  }
  
  public boolean hasCashAdvanceContractExpense( )
  {
    return( HasCashAdvanceContractExpense );
  }
  
  public boolean hasCashAdvanceContractIncome( )
  {
    return( HasCashAdvanceContractIncome );
  }
  
  public boolean hasParentLiabilityContract( )
  {
    long          nodeId      = getReportHierarchyNodeDefault();
    int           rowCount    = 0;
    
    
    try
    {
      while( nodeId != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1909^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(abc.hierarchy_node) 
//            from    agent_bank_contract abc
//            where   abc.hierarchy_node = :nodeId and
//                    abc.contract_type = :ContractTypes.CONTRACT_SOURCE_LIABILITY
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(abc.hierarchy_node)  \n          from    agent_bank_contract abc\n          where   abc.hierarchy_node =  :1   and\n                  abc.contract_type =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,ContractTypes.CONTRACT_SOURCE_LIABILITY);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1915^9*/
         
        if ( rowCount > 0 )
        {
          break;
        }
        nodeId = getParentNode(nodeId);
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("hasParentLiabilityContract()",e.toString());
    }
          
    return( (nodeId == 0L) ? false : true );
  }
  
  public boolean hasPartnershipContract( )
  {
    boolean            retVal  = false;
    SummaryData        row     = null;
    
    for( int i = 0; i < ReportRows.size(); ++i )
    {
      row = (SummaryData)ReportRows.elementAt(i);  
      if ( row.Expense[BankContractBean.BET_GROUP_PARTNERSHIP] != 0.0 || 
           row.Income[BankContractBean.BET_GROUP_PARTNERSHIP] != 0.0 )
      {
        retVal = true;
        break;
      }           
    }
    return( retVal );
  }
  
  public boolean includeSupplyExpense( )
  {
    return( (getUserType() == UT_VAR_BANK) );
  }
  
  public boolean includeVitalExpense( )
  {
    boolean     includeVitalExp     = false;
    
    switch( getReportBankId( ) )
    {
      case mesConstants.BANK_ID_MES:
      case mesConstants.BANK_ID_NBSC:
        // only show bet expense for non-GRIN bank 3941 
        // direct merchants and 3860 merchants
        includeVitalExp = ( !hasParentLiabilityContract() &&
                            getProfReportType() != MERCH_PROF_TYPE_TPS_GRIN );
        break;
        
      default:
        // load the vital expense data for all bank level users 
        // (i.e. 386000000) and for all sub-nodes that do not have a 
        // liability contract.
        includeVitalExp = ( !hasParentLiabilityContract() || isBankUser() ) ;
        break;
    }
    return( includeVitalExp );
  }
  
  protected boolean isBankUser( )
  {
    int         bankNumber    = getReportBankId();
    boolean     retVal        = false;
    
    // if the login node is the top level node or a bank level node,
    // then the user is considered a bank level user.
    if ( ( ( getReportHierarchyNodeDefault() == HierarchyTree.DEFAULT_HIERARCHY_NODE ) ||
           ( isOrgBank( getReportOrgIdDefault() ) == true ) ) &&
         ( bankNumber != mesConstants.BANK_ID_NBSC ) &&           
         ( bankNumber != mesConstants.BANK_ID_STERLING ) )           
    
    {
      retVal = true;
    }
    return( retVal );
  }
  
  public boolean isDirectMerchant()
  {
    return( isDirectMerchant( getReportMerchantId() ) );
  }
  
  public boolean isDirectMerchant( long merchantId )
  {
    boolean           retVal          = false;
    int               userType        = getUserType();
    
    try
    {
      // test if this merchant has a liability contract node above it
      HasLiabilityContract = ( getContractNode(merchantId,ContractTypes.CONTRACT_SOURCE_LIABILITY) != 0L );
      HasResellerContract  = ( getContractNode(merchantId,ContractTypes.CONTRACT_SOURCE_RESELLER) != 0L );
      
      switch( userType )
      {
        case UT_CONTRACT_BANK:
          if ( HasLiabilityContract )
          {
            retVal = true;
          }
          break;
          
        case UT_RESELLER:
          if ( !HasLiabilityContract )
          {
            retVal = true;
          }
          break;
          
        default:
          if ( ! HasLiabilityContract && ! HasResellerContract )
          {
            retVal = true;
          }
          break;
      }
    }
    catch( Exception e )
    {
      logEntry("isDirectMerchant()",e.toString());
    }
    return( retVal );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean       retVal    = false;
    
    switch( fileFormat )
    {
      case FF_CSV:
        switch ( getProfReportType() )
        {
          case MERCH_PROF_TYPE_UNDER_WATER:
          case MERCH_PROF_TYPE_SUMMARY:
          case MERCH_PROF_TYPE_NET_REVENUE_RETENTION:
            retVal = true;
            break;
        }
        break;
        
      default:
        break;
    }
    return( retVal );
  }
  
  public boolean isEquipmentExpIncluded( )
  {
    return( isEquipmentExpIncluded( getReportHierarchyNode() ) );
  }
  
  public boolean isEquipmentExpIncluded( long merchantId )
  {
    long          defaultNode   = getReportHierarchyNodeDefault();
    int           rowCount      = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2079^7*/

//  ************************************************************
//  #sql [Ctx] { select count(ep.hierarchy_node) 
//          from    mif                       mf,
//                  groups                    g,
//                  agent_bank_exclude_prof   ep,
//                  t_hierarchy               th
//          where   mf.merchant_number = :merchantId and
//                  g.assoc_number = mf.association_node and
//                  ep.hierarchy_node in
//                    ( g.group_1, g.group_2, g.group_3, g.group_4, g.group_5,
//                      g.group_6, g.group_7, g.group_8, g.group_9, g.group_10 ) and
//                  ep.expense_category in 
//                    ( :BankContractBean.BET_GROUP_EQUIP_RENTAL,
//                      :BankContractBean.BET_GROUP_EQUIP_SALES ) and -- ( 6,7 ) and
//                  th.ancestor = ep.hierarchy_node and
//                  th.hier_type = 1 and
//                  th.descendent = :defaultNode
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(ep.hierarchy_node)  \n        from    mif                       mf,\n                groups                    g,\n                agent_bank_exclude_prof   ep,\n                t_hierarchy               th\n        where   mf.merchant_number =  :1   and\n                g.assoc_number = mf.association_node and\n                ep.hierarchy_node in\n                  ( g.group_1, g.group_2, g.group_3, g.group_4, g.group_5,\n                    g.group_6, g.group_7, g.group_8, g.group_9, g.group_10 ) and\n                ep.expense_category in \n                  (  :2  ,\n                     :3   ) and -- ( 6,7 ) and\n                th.ancestor = ep.hierarchy_node and\n                th.hier_type = 1 and\n                th.descendent =  :4 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setInt(2,BankContractBean.BET_GROUP_EQUIP_RENTAL);
   __sJT_st.setInt(3,BankContractBean.BET_GROUP_EQUIP_SALES);
   __sJT_st.setLong(4,defaultNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2097^7*/
    }
    catch(Exception e)
    {
      logEntry("isEquipmentExcluded(" + merchantId + ")",e.toString());
    }
    return( rowCount == 0 );
  }
  
  protected boolean isMesOnlyRevenue( long merchantId, int betGroup )
  {
    int     recCount    = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2112^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(gm.merchant_number) 
//          from    agent_bank_revenue_mask   rm,
//                  organization              o,
//                  group_merchant            gm
//          where   rm.mask_type = 2 and
//                  rm.charge_rec_prefix is null and
//                  rm.income_category = :betGroup and
//                  o.org_group = rm.hierarchy_node and
//                  gm.org_num = o.org_num and
//                  gm.merchant_number = :merchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(gm.merchant_number)  \n        from    agent_bank_revenue_mask   rm,\n                organization              o,\n                group_merchant            gm\n        where   rm.mask_type = 2 and\n                rm.charge_rec_prefix is null and\n                rm.income_category =  :1   and\n                o.org_group = rm.hierarchy_node and\n                gm.org_num = o.org_num and\n                gm.merchant_number =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,betGroup);
   __sJT_st.setLong(2,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2124^7*/
    }
    catch(Exception e)
    {
      logEntry( "isMesOnlyRevenue()", e.toString() );
    }
    return( (recCount > 0) );
  }
  
  protected boolean isResellerUser( )
  {
    long          nodeId      = getReportHierarchyNodeDefault();
    int           rowCount    = 0;
    
    try
    {
      if ( isUserUnderContract() )
      {
        // abort, this node is under liab/refer contract
        nodeId = 0L;      
      }
      
      while( nodeId != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:2148^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(abc.hierarchy_node) 
//            from    agent_bank_contract abc
//            where   abc.hierarchy_node = :nodeId and
//                    abc.contract_type = :ContractTypes.CONTRACT_SOURCE_RESELLER -- 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(abc.hierarchy_node)  \n          from    agent_bank_contract abc\n          where   abc.hierarchy_node =  :1   and\n                  abc.contract_type =  :2   -- 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,ContractTypes.CONTRACT_SOURCE_RESELLER);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2154^9*/
         
        if ( rowCount > 0 )
        {
          break;
        }
        nodeId = getParentNode(nodeId);
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("isResellerUser()",e.toString());
    }
          
    return( (nodeId == 0L) ? false : true );
  }
  
  protected boolean isUserUnderContract( )
  {
    long          nodeId      = getReportHierarchyNodeDefault();
    int           rowCount    = 0;
    
    
    try
    {
      while( nodeId != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:2181^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(abc.hierarchy_node) 
//            from    agent_bank_contract abc
//            where   abc.hierarchy_node = :nodeId and
//                    abc.contract_type in 
//                      ( 
//                        :ContractTypes.CONTRACT_SOURCE_LIABILITY, -- 1,
//                        :ContractTypes.CONTRACT_SOURCE_REFERRAL -- 2
//                      )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(abc.hierarchy_node)  \n          from    agent_bank_contract abc\n          where   abc.hierarchy_node =  :1   and\n                  abc.contract_type in \n                    ( \n                       :2  , -- 1,\n                       :3   -- 2\n                    )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,ContractTypes.CONTRACT_SOURCE_LIABILITY);
   __sJT_st.setInt(3,ContractTypes.CONTRACT_SOURCE_REFERRAL);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2191^9*/
         
        if ( rowCount > 0 )
        {
          break;
        }
        nodeId = getParentNode(nodeId);
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("isUserUnderContract()",e.toString());
    }
          
    return( (nodeId == 0L) ? false : true );
  }
  
  public boolean isVitalData()
  {
    Date        beginDate   = getReportDateBegin();
    Date        endDate     = getReportDateEnd();
    long        orgId       = getReportOrgId();
    int         mbsCount    = 0;
    int         tsysCount   = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2218^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum( case when sm.load_filename like 'mbs_ext%' then 1 else 0 end ),0),
//                  nvl(sum( case when sm.load_filename like 'mon_ext%' then 1 else 0 end ),0)
//                          
//          from    monthly_extract_summary   sm
//          where   sm.merchant_number in
//                  (
//                    select  gm.merchant_number
//                    from    group_merchant    gm
//                    where   gm.org_num = :orgId
//                  )
//                  and sm.active_date between :beginDate and :endDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum( case when sm.load_filename like 'mbs_ext%' then 1 else 0 end ),0),\n                nvl(sum( case when sm.load_filename like 'mon_ext%' then 1 else 0 end ),0)\n                         \n        from    monthly_extract_summary   sm\n        where   sm.merchant_number in\n                (\n                  select  gm.merchant_number\n                  from    group_merchant    gm\n                  where   gm.org_num =  :1  \n                )\n                and sm.active_date between  :2   and  :3 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mbsCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   tsysCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2231^7*/
    }
    catch( Exception e )
    {
      logEntry("isVitalData(" + orgId + "," + beginDate + "," + endDate + ")", e.toString());
    }
    // return true if there is no MBS data and there is some TSYS data
    return( (mbsCount == 0) && (tsysCount > 0) );
  }
  
  public Vector loadChargeRecordRevenue( long nodeId )
  {
    return( loadChargeRecordRevenue( nodeId, ReportDateBegin, ReportDateEnd ) );
  }
  
  public Vector loadChargeRecordRevenue( long nodeId, Date beginDate, Date endDate )
  {
    ResultSetIterator         it          = null;
    ResultSet                 resultSet   = null;
    Vector                    retVal      = new Vector();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2254^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  st.st_statement_desc                        as statement_msg,
//                  nvl( sum( nvl(st.st_fee_amount,0) ), 0 )    as revenue
//          from    organization              o,
//                  group_merchant            gm,
//                  monthly_extract_summary   sm,
//                  monthly_extract_st        st
//          where   o.org_group = :nodeId and
//                  gm.org_num = o.org_num and
//                  sm.merchant_number = gm.merchant_number and
//                  sm.active_date between :beginDate and :endDate and
//                  st.hh_load_sec = sm.hh_load_sec 
//          group by st.st_statement_desc  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  st.st_statement_desc                        as statement_msg,\n                nvl( sum( nvl(st.st_fee_amount,0) ), 0 )    as revenue\n        from    organization              o,\n                group_merchant            gm,\n                monthly_extract_summary   sm,\n                monthly_extract_st        st\n        where   o.org_group =  :1   and\n                gm.org_num = o.org_num and\n                sm.merchant_number = gm.merchant_number and\n                sm.active_date between  :2   and  :3   and\n                st.hh_load_sec = sm.hh_load_sec \n        group by st.st_statement_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2268^7*/
      resultSet = it.getResultSet();
      while( resultSet.next() )
      {
        retVal.add( new ContractTypes.ChargeRecordRevenue( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadChargeRecRevenue()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return(retVal);
  }
  
  public Vector loadTsysSysGenerateIncome( String hhLoadSec )
  {
    ResultSetIterator         it          = null;
    ResultSet                 resultSet   = null;
    Vector                    retVal      = new Vector();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2296^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  st_statement_desc,
//                  st_fee_amount
//          from    monthly_extract_st
//          where   
//      item_category in ('MISC', 'ASSOC', 'CG-MIS', 'CG-IMP') and
//      st_number_of_items_on_stmt != 0 and
//      hh_load_sec = :hhLoadSec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  st_statement_desc,\n                st_fee_amount\n        from    monthly_extract_st\n        where   \n    item_category in ('MISC', 'ASSOC', 'CG-MIS', 'CG-IMP') and\n    st_number_of_items_on_stmt != 0 and\n    hh_load_sec =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,hhLoadSec);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2305^7*/
      resultSet = it.getResultSet();
      while( resultSet.next() )
      {
        retVal.add( new ContractTypes.TsysSysGenerateIncome( resultSet ) );
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("loadTsysSysGenerateIncome()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return(retVal);
  }  
  
  protected void loadContractData( long merchantId )
  {
    BankContractBean              contract            = null;  
    ResultSetIterator             it                  = null;
    long                          nodeId              = 0L;
    ResultSet                     resultSet           = null;
    int                           userType            = getUserType();
    
    try
    {
      contract = new BankContractBean();
      contract.connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:2338^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+
//                    INDEX (sm pk_monthly_extract_summary)
//                  */                  
//                  :merchantId                                     as merchant_number,
//                  sm.active_date                                  as active_date,
//                  decode(sm.cash_advance_account,'Y',1,0)         as cash_advance,
//                  decode(sm.mes_inventory,'Y',1,0)                as mes_inventory,
//                  decode(sm.moto_merchant,'Y',1,0)                as moto_merchant,
//                  sm.product_code                                 as product_code,
//                  /* RS changes for ACH PS include ACH Discount   */
//                  sum( sm.tot_inc_discount +
//                       sm.tot_inc_interchange +
//                       sm.tot_inc_authorization +
//                       sm.tot_inc_capture +
//                       sm.tot_inc_debit +
//                       sm.tot_inc_ind_plans +
//                       sm.tot_inc_sys_generated +
//                       nvl(sm.tot_inc_ach,0)+ nvl(sm.tot_inc_discount_ach) )                 as proc_inc,
//                  sum( nvl(sm.TOT_AUTHORIZATION_LIABILITY,0) +
//                       nvl(sm.TOT_CAPTURE_LIABILITY,0) +
//                       nvl(sm.TOT_DEBIT_LIABILITY,0) +
//                       nvl(sm.TOT_EQUIP_RENTAL_LIABILITY,0) +
//                       nvl(sm.TOT_EQUIP_SALES_LIABILITY,0) +
//                       nvl(sm.TOT_IND_PLANS_LIABILITY,0) +
//                       nvl(sm.TOT_NDR_LIABILITY,0) +
//                       nvl(sm.TOT_SYS_GENERATED_LIABILITY,0) +
//                       nvl(sm.tot_ach_liability,0) )    as liability,
//                  sum( nvl(sm.TOT_AUTHORIZATION_REFERRAL,0) +
//                       nvl(sm.TOT_CAPTURE_REFERRAL,0) +
//                       nvl(sm.TOT_DEBIT_REFERRAL,0) +
//                       nvl(sm.TOT_EQUIP_RENTAL_REFERRAL,0) +
//                       nvl(sm.TOT_EQUIP_SALES_REFERRAL,0) +
//                       nvl(sm.TOT_IND_PLANS_REFERRAL,0) +
//                       nvl(sm.TOT_NDR_REFERRAL,0) +
//                       nvl(sm.TOT_SYS_GENERATED_REFERRAL,0) +
//                       nvl(sm.tot_ach_referral_expense,0) )     as referral,
//                  /* RS changes for ACH PS exclude ACH Discount from VMC Discount/Ic Income  */
//                  sum( (sm.tot_inc_interchange              
//                       sm.tot_inc_discount) - nvl(sm.tot_inc_discount_ach,0) )                 as disc_ic_inc,
//                  sum( sm.tot_inc_authorization )                 as auth_inc,
//                  sum( sm.tot_inc_capture )                       as capture_inc,
//                  sum( sm.tot_inc_debit )                         as debit_inc,
//                  sum( sm.tot_inc_ind_plans )                     as plan_inc,
//                  sum( sm.tot_inc_sys_generated -
//                         ( sm.equip_rental_income +
//                           sm.equip_sales_income +
//                           nvl(sm.equip_sales_tax_collected,0) 
//                         )
//                      )                                           as sys_gen_inc,
//                  sum( nvl(sm.tot_inc_discount_ach,0) + nvl(sm.tot_inc_ach,0))                 as ach_inc,
//                  sum (nvl(sm.tot_ach_liability,0))               as ach_liability,    
//                  sum (nvl(sm.tot_ach_referral_expense,0))        as ach_referral,    
//                  sum (nvl(sm.tot_exp_ach_vendor,0))              as ps_exp,    
//                  sum( nvl(sm.mes_only_inc_disc_ic,0) )           as mes_only_disc_ic,
//                  sum( nvl(sm.mes_only_inc_authorization,0) )     as mes_only_auth,
//                  sum( nvl(sm.mes_only_inc_capture,0) )           as mes_only_capture,
//                  sum( nvl(sm.mes_only_inc_debit,0) )             as mes_only_debit,
//                  sum( nvl(sm.mes_only_inc_ind_plans,0) )         as mes_only_plan,
//                  sum( nvl(sm.mes_only_inc_sys_generated,0) )     as mes_only_sys_gen,
//                  sum( sm.equip_rental_income )                   as equip_rental_inc,
//                  sum( sm.equip_sales_income )                    as equip_sales_inc,
//                  sum( nvl(sm.equip_sales_tax_collected,0) )      as equip_sales_tax,
//                  sum( nvl(sm.equip_rental_base_cost,0) )         as equip_rental_base_cost,
//                  sum( nvl(sm.equip_sales_base_cost,0) )          as equip_sales_base_cost,
//                  sum( nvl(sm.equip_transfer_cost,0) )            as equip_transfer_cost,
//                  sum( sm.disc_ic_dce_adj_amount )                as disc_ic_adj_amount,
//                  sum( sm.fee_dce_adj_amount )                    as fee_adj_amount,
//                  sum( nvl(sm.interchange_expense,0) )            as ic_exp,
//                  sum( nvl(sm.vmc_assessment_expense,0) )         as assessment,                             
//                  sum( sm.cash_adv_interchange_expense )          as cash_adv_interchange_exp,
//                  sum( nvl(sm.vmc_fees,0) )                       as vmc_fees,
//                  sum( sm.visa_vol_count )                        as visa_vol_count,
//                  sum( sm.visa_vol_amount )                       as visa_vol_amount,
//                  sum( sm.visa_sales_count )                      as visa_sales_count,
//                  sum( sm.visa_sales_amount )                     as visa_sales_amount,
//                  sum( sm.visa_credits_count )                    as visa_credits_count,
//                  sum( sm.visa_credits_amount )                   as visa_credits_amount,
//                  sum( sm.mc_vol_count )                          as mc_vol_count,
//                  sum( sm.mc_vol_amount )                         as mc_vol_amount,
//                  sum( sm.mc_sales_count )                        as mc_sales_count,
//                  sum( sm.mc_sales_amount )                       as mc_sales_amount,
//                  sum( sm.mc_credits_count )                      as mc_credits_count,
//                  sum( sm.mc_credits_amount )                     as mc_credits_amount,
//                  sum( sm.mc_cross_border_vol_count )             as mc_cross_border_vol_count,
//                  sum( sm.mc_cross_border_vol_amount )            as mc_cross_border_vol_amount,
//                  sum( sm.mc_cross_border_sales_count )           as mc_cross_border_sales_count,
//                  sum( sm.mc_cross_border_sales_amount )          as mc_cross_border_sales_amount,
//                  sum( sm.mc_cross_border_credits_count )         as mc_cross_border_credits_count,
//                  sum( sm.mc_cross_border_credits_amount )        as mc_cross_border_credits_amount,
//                  sum( sm.mc_cross_border_sales_amount +
//                       sm.mc_cross_border_credits_amount )        as mc_cross_border_hash_amount,
//                  sum( sm.mc_foreign_std_vol_count )              as mc_foreign_std_vol_count,
//                  sum( sm.mc_foreign_std_vol_amount )             as mc_foreign_std_vol_amount,
//                  sum( sm.mc_foreign_std_sales_count )            as mc_foreign_std_sales_count,
//                  sum( sm.mc_foreign_std_sales_amount )           as mc_foreign_std_sales_amount,
//                  sum( sm.mc_foreign_std_credits_count )          as mc_foreign_std_credits_count,
//                  sum( sm.mc_foreign_std_credits_amount )         as mc_foreign_std_credits_amount,
//                  sum( sm.mc_foreign_std_sales_amount +
//                       sm.mc_foreign_std_credits_amount )         as mc_foreign_std_hash_amount,
//                  sum( sm.mc_foreign_elec_vol_count )             as mc_foreign_elec_vol_count,
//                  sum( sm.mc_foreign_elec_vol_amount )            as mc_foreign_elec_vol_amount,
//                  sum( sm.mc_foreign_elec_sales_count )           as mc_foreign_elec_sales_count,
//                  sum( sm.mc_foreign_elec_sales_amount )          as mc_foreign_elec_sales_amount,
//                  sum( sm.mc_foreign_elec_credits_count )         as mc_foreign_elec_credits_count,
//                  sum( sm.mc_foreign_elec_credits_amount )        as mc_foreign_elec_credits_amount,
//                  sum( sm.mc_foreign_elec_sales_amount +
//                       sm.mc_foreign_elec_credits_amount )        as mc_foreign_elec_hash_amount,
//                  /* RS changes for ACH PS Columns added  to calculate partner fees  */
//                  sum( nvl(sm.ach_tran_count,0))                  as ach_tran_count,
//                  sum( nvl(sm.ach_return_count,0))                as ach_return_count,
//                  sum( nvl(sm.ach_unauth_count,0))                as ach_unauth_count,
//                  sum( nvl(sm.mc_vol_count,0) - 
//                       nvl(mc_cross_border_vol_count,0) )         as mc_nabu_vol_count,
//                  sum(  sm.vmc_sales_count
//                      -- + nvl(sm.amex_ic_sales_count,0)    -- exclude per rory
//                      + nvl(sm.disc_ic_sales_count,0) )           as risk_sales_count,  
//                  sum(  sm.vmc_sales_amount
//                      -- + nvl(sm.amex_ic_sales_amount,0)   -- exclude per rory
//                      + nvl(sm.disc_ic_sales_amount,0) )          as risk_sales_amount,  
//                  sum(  sm.vmc_vol_count
//                      -- + nvl(sm.amex_ic_vol_count,0)      -- exclude per rory
//                      + nvl(sm.disc_ic_vol_count,0) )             as processing_vol_count,  
//                  sum(  sm.vmc_vol_amount
//                      -- + nvl(sm.amex_ic_vol_amount,0)     -- exclude per rory
//                      + nvl(sm.disc_ic_vol_amount,0) )            as processing_vol_amount,  
//                  sum(  sm.vmc_sales_count
//                      -- + nvl(sm.amex_ic_sales_count,0)    -- exclude per rory
//                      + nvl(sm.disc_ic_sales_count,0) )           as processing_sales_count,
//                  sum(  sm.vmc_sales_amount
//                      -- + nvl(sm.amex_ic_sales_amount,0)   -- exclude per rory
//                      + nvl(sm.disc_ic_sales_amount,0) )          as processing_sales_amount,  
//                  sum( sm.vmc_vol_count )                         as vmc_vol_count,
//                  sum( sm.vmc_vol_amount )                        as vmc_vol_amount,  
//                  sum( sm.vmc_sales_count )                       as vmc_sales_count,
//                  sum( sm.vmc_sales_amount )                      as vmc_sales_amount,
//                  sum( sm.vmc_credits_count )                     as vmc_credits_count,
//                  sum( sm.vmc_credits_amount )                    as vmc_credits_amount,
//                  sum( nvl(sm.amex_ic_vol_count,0) )              as am_ic_vol_count,
//                  sum( nvl(sm.amex_ic_vol_amount,0) )             as am_ic_vol_amount,  
//                  sum( nvl(sm.amex_ic_sales_count,0) )            as am_ic_sales_count,
//                  sum( nvl(sm.amex_ic_sales_amount,0) )           as am_ic_sales_amount,  
//                  sum( nvl(sm.amex_ic_credits_count,0) )          as am_ic_credits_count,
//                  sum( nvl(sm.amex_ic_credits_amount,0) )         as am_ic_credits_amount,  
//                  sum( nvl(sm.disc_ic_vol_count,0) )              as ds_ic_vol_count,
//                  sum( nvl(sm.disc_ic_vol_amount,0) )             as ds_ic_vol_amount,  
//                  sum( nvl(sm.disc_ic_sales_count,0) )            as ds_ic_sales_count,
//                  sum( nvl(sm.disc_ic_sales_amount,0) )           as ds_ic_sales_amount,  
//                  sum( nvl(sm.disc_ic_credits_count,0) )          as ds_ic_credits_count,
//                  sum( nvl(sm.disc_ic_credits_amount,0) )         as ds_ic_credits_amount,  
//                  sum( nvl(sm.amex_vol_count,0) )                 as amex_vol_count,
//                  sum( nvl(sm.amex_vol_amount,0) )                as amex_vol_amount,  
//                  sum( nvl(sm.amex_sales_count,0) )               as amex_sales_count,
//                  sum( nvl(sm.amex_sales_amount,0) )              as amex_sales_amount,  
//                  sum( nvl(sm.amex_credits_count,0) )             as amex_credits_count,
//                  sum( nvl(sm.amex_credits_amount,0) )            as amex_credits_amount,  
//                  sum( nvl(sm.dinr_vol_count,0) )                 as dinr_vol_count,
//                  sum( nvl(sm.dinr_vol_amount,0) )                as dinr_vol_amount,  
//                  sum( nvl(sm.dinr_sales_count,0) )               as dinr_sales_count,
//                  sum( nvl(sm.dinr_sales_amount,0) )              as dinr_sales_amount,  
//                  sum( nvl(sm.dinr_credits_count,0) )             as dinr_credits_count,
//                  sum( nvl(sm.dinr_credits_amount,0) )            as dinr_credits_amount,  
//                  sum( nvl(sm.disc_vol_count,0) )                 as disc_vol_count,
//                  sum( nvl(sm.disc_vol_amount,0) )                as disc_vol_amount,  
//                  sum( nvl(sm.disc_sales_count,0) )               as disc_sales_count,
//                  sum( nvl(sm.disc_sales_amount,0) )              as disc_sales_amount,  
//                  sum( nvl(sm.disc_credits_count,0) )             as disc_credits_count,
//                  sum( nvl(sm.disc_credits_amount,0) )            as disc_credits_amount,  
//                  sum( nvl(sm.jcb_vol_count,0) )                  as jcb_vol_count,
//                  sum( nvl(sm.jcb_vol_amount,0) )                 as jcb_vol_amount, 
//                  sum( nvl(sm.jcb_sales_count,0) )                as jcb_sales_count,
//                  sum( nvl(sm.jcb_sales_amount,0) )               as jcb_sales_amount,  
//                  sum( nvl(sm.jcb_credits_count,0) )              as jcb_credits_count,
//                  sum( nvl(sm.jcb_credits_amount,0) )             as jcb_credits_amount,  
//                  sum( nvl(sm.pl1_vol_count,0) )                  as pl1_vol_count,
//                  sum( nvl(sm.pl1_vol_amount,0) )                 as pl1_vol_amount, 
//                  sum( nvl(sm.pl1_sales_count,0) )                as pl1_sales_count,
//                  sum( nvl(sm.pl1_sales_amount,0) )               as pl1_sales_amount,  
//                  sum( nvl(sm.pl1_credits_count,0) )              as pl1_credits_count,
//                  sum( nvl(sm.pl1_credits_amount,0) )             as pl1_credits_amount,  
//                  sum( nvl(sm.pl2_vol_count,0) )                  as pl2_vol_count,
//                  sum( nvl(sm.pl2_vol_amount,0) )                 as pl2_vol_amount, 
//                  sum( nvl(sm.pl2_sales_count,0) )                as pl2_sales_count,
//                  sum( nvl(sm.pl2_sales_amount,0) )               as pl2_sales_amount,  
//                  sum( nvl(sm.pl2_credits_count,0) )              as pl2_credits_count,
//                  sum( nvl(sm.pl2_credits_amount,0) )             as pl2_credits_amount,  
//                  sum( nvl(sm.pl3_vol_count,0) )                  as pl3_vol_count,
//                  sum( nvl(sm.pl3_vol_amount,0) )                 as pl3_vol_amount, 
//                  sum( nvl(sm.pl3_sales_count,0) )                as pl3_sales_count,
//                  sum( nvl(sm.pl3_sales_amount,0) )               as pl3_sales_amount,  
//                  sum( nvl(sm.pl3_credits_count,0) )              as pl3_credits_count,
//                  sum( nvl(sm.pl3_credits_amount,0) )             as pl3_credits_amount,  
//                  sum( nvl(sm.debit_vol_count,0) )                as debit_vol_count,
//                  sum( nvl(sm.debit_vol_amount,0) )               as debit_vol_amount,
//                  sum( nvl(sm.debit_network_fees,0))              as debit_network_fees,
//                  sum( nvl(sm.debit_sales_count,0) )              as debit_sales_count,
//                  sum( nvl(sm.debit_sales_amount,0) )             as debit_sales_amount,  
//                  sum( nvl(sm.debit_credits_count,0) )            as debit_credits_count,
//                  sum( nvl(sm.debit_credits_amount,0) )           as debit_credits_amount,  
//                  sum( nvl(sm.ebt_vol_count,0) )                  as ebt_vol_count,
//                  sum( nvl(sm.achp_vol_count,0) )                 as achp_vol_count,
//                  sum( nvl(sm.achp_return_count,0) )              as achp_return_count,
//                  sum( nvl(dn.cat_01_total_count,0) )             as debit_count_cat_01,
//                  sum( nvl(dn.cat_02_total_count,0) )             as debit_count_cat_02,
//                  sum( nvl(dn.cat_03_total_count,0) )             as debit_count_cat_03,
//                  sum( nvl(dn.cat_04_total_count,0) )             as debit_count_cat_04,
//                  sum( nvl(dn.cat_05_total_count,0) )             as debit_count_cat_05,
//                  sum( nvl(dn.cat_06_total_count,0) )             as debit_count_cat_06,
//                  sum( nvl(dn.cat_07_total_count,0) )             as debit_count_cat_07,
//                  sum( nvl(dn.cat_08_total_count,0) )             as debit_count_cat_08,
//                  sum( nvl(dn.cat_09_total_count,0) )             as debit_count_cat_09,
//                  sum( nvl(dn.cat_10_total_count,0) )             as debit_count_cat_10,
//                  sum( sm.vmc_auth_count )                        as vmc_auth_count,
//                  sum( nvl(sm.visa_auth_count,0) )                as visa_auth_count,
//                  sum( nvl(sm.mc_auth_count,0) )                  as mc_auth_count,
//                  sum( sm.amex_auth_count )                       as amex_auth_count,
//                  sum( sm.dinr_auth_count )                       as dinr_auth_count,
//                  sum( sm.disc_auth_count )                       as disc_auth_count,
//                  sum( sm.jcb_auth_count )                        as jcb_auth_count,
//                  sum( sm.debit_auth_count )                      as debit_auth_count,
//                  sum( nvl(sm.auth_count_total,0) )               as auth_count_total,
//                  sum( nvl(sm.auth_count_total_ext,0) )           as auth_count_total_ext,
//                  sum( nvl(sm.auth_count_total_api,0) )           as auth_count_total_api,
//                  sum( nvl(sm.auth_count_total_trident,0) )       as auth_count_total_trident,
//                  sum( nvl(sm.vmc_auth_count_trident,0) )         as vmc_auth_count_trident,
//                  sum( nvl(sm.amex_auth_count_trident,0) )        as amex_auth_count_trident,
//                  sum( nvl(sm.amex_direct_auth_count_trident,0) ) as amex_direct_auth_count_trident,
//                  sum( nvl(sm.amex_direct_auth_count_vital,0) )   as amex_direct_auth_count_vital,               
//                  sum( nvl(sm.disc_auth_count_trident,0) )        as disc_auth_count_trident,
//                  sum( nvl(sm.dinr_auth_count_trident,0) )        as dinr_auth_count_trident,
//                  sum( nvl(sm.jcb_auth_count_trident,0) )         as jcb_auth_count_trident,
//                  sum( nvl(sm.debit_auth_count_trident,0) )       as debit_auth_count_trident,
//                  sum( nvl(sm.visa_auth_count,0) +
//                       nvl(dn.cat_03_total_count,0) )             as visa_interlink_auth_count,
//                  sum( nvl(sm.disc_auth_count,0) +
//                       ( nvl(sm.debit_auth_count,0) -
//                         nvl(dn.cat_03_total_count,0) ) )         as non_visa_auth_access_count,
//                  sum( sm.chargeback_vol_count )                  as chargeback_vol_count,
//                  sum( sm.chargeback_vol_amount )                 as chargeback_vol_amount,  
//                  sum( sm.incoming_chargeback_count )             as incoming_cb_count,
//                  sum( sm.incoming_chargeback_amount )            as incoming_cb_amount,
//                  sum( sm.incoming_ft_chargeback_count )          as incoming_ft_cb_count,
//                  sum( sm.incoming_ft_chargeback_amount )         as incoming_ft_cb_amount,
//                  sum( sm.retrieval_count )                       as retrieval_count,
//                  sum( sm.retrieval_amount )                      as retrieval_amount,
//                  sum( sm.service_call_count )                    as num_service_calls,
//                  sum( sm.cash_advance_vol_count )                as cash_advance_vol_count,
//                  sum( sm.cash_advance_vol_amount )               as cash_advance_vol_amount,
//                  sum( decode( sm.activated_merchant,'Y',1,0 ) )  as num_activated_merchants,
//                  sum( decode( sm.new_merchant,'Y',1,0 ) )        as num_new_merchants,
//                  sum( decode( sm.amex_setup,'Y',1,0 ) )          as num_amex_setups,
//                  sum( decode( sm.discover_setup,'Y',1,0 ) )      as num_disc_setups,
//                  sum( sm.terminal_rental_count )                 as num_rentals,
//                  sum( decode( sm.merchant_status,
//                               'C', 0,  -- closed
//                               'D', 0,  -- deleted
//                               'F', 0,  -- fraud
//                                1 ) )                             as num_active_merchants,
//                  sum( nvl(sm.voice_auth_count,0) +
//                       nvl(sm.aru_auth_count,0) )                 as dialpay_auth_count,
//                  sum( nvl(sm.avs_count,0) )                      as avs_auth_count,
//                  sum( nvl(sm.ach_reject_count,0) )               as ach_reject_count,
//                  sum( nvl(sm.ic_correction_count,0) )            as ic_correction_count,
//                  sum( nvl(sm.terminal_swap_count,0) )            as swap_count,
//                  sum( nvl(sm.terminal_deployment_count,0) )      as terminal_deployment_count,
//                  sum( decode( nvl(sm.print_statement, 'N'), 'Y', 1, 0 ) )  	
//                  												as print_statement_count,
//                  sum( nvl(sm.batch_fee_count, 0) )  	
//                  												as batch_fee_count
//          from    monthly_extract_summary   sm,
//                  monthly_extract_dn        dn
//          where   sm.merchant_number = :merchantId and
//                  sm.active_date between :ReportDateBegin and :ReportDateEnd and
//                  dn.hh_load_sec(+) = sm.hh_load_sec
//          group by sm.active_date, 
//                   decode(sm.cash_advance_account,'Y',1,0),
//                   decode(sm.mes_inventory,'Y',1,0),
//                   decode(sm.moto_merchant,'Y',1,0),
//                   sm.product_code                        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+\n                  INDEX (sm pk_monthly_extract_summary)\n                */                  \n                 :1                                       as merchant_number,\n                sm.active_date                                  as active_date,\n                decode(sm.cash_advance_account,'Y',1,0)         as cash_advance,\n                decode(sm.mes_inventory,'Y',1,0)                as mes_inventory,\n                decode(sm.moto_merchant,'Y',1,0)                as moto_merchant,\n                sm.product_code                                 as product_code,\n                /* RS changes for ACH PS include ACH Discount   */\n                sum( sm.tot_inc_discount +\n                     sm.tot_inc_interchange +\n                     sm.tot_inc_authorization +\n                     sm.tot_inc_capture +\n                     sm.tot_inc_debit +\n                     sm.tot_inc_ind_plans +\n                     sm.tot_inc_sys_generated +\n                     nvl(sm.tot_inc_ach,0)+ nvl(sm.tot_inc_discount_ach) )                 as proc_inc,\n                sum( nvl(sm.TOT_AUTHORIZATION_LIABILITY,0) +\n                     nvl(sm.TOT_CAPTURE_LIABILITY,0) +\n                     nvl(sm.TOT_DEBIT_LIABILITY,0) +\n                     nvl(sm.TOT_EQUIP_RENTAL_LIABILITY,0) +\n                     nvl(sm.TOT_EQUIP_SALES_LIABILITY,0) +\n                     nvl(sm.TOT_IND_PLANS_LIABILITY,0) +\n                     nvl(sm.TOT_NDR_LIABILITY,0) +\n                     nvl(sm.TOT_SYS_GENERATED_LIABILITY,0) +\n                     nvl(sm.tot_ach_liability,0) )    as liability,\n                sum( nvl(sm.TOT_AUTHORIZATION_REFERRAL,0) +\n                     nvl(sm.TOT_CAPTURE_REFERRAL,0) +\n                     nvl(sm.TOT_DEBIT_REFERRAL,0) +\n                     nvl(sm.TOT_EQUIP_RENTAL_REFERRAL,0) +\n                     nvl(sm.TOT_EQUIP_SALES_REFERRAL,0) +\n                     nvl(sm.TOT_IND_PLANS_REFERRAL,0) +\n                     nvl(sm.TOT_NDR_REFERRAL,0) +\n                     nvl(sm.TOT_SYS_GENERATED_REFERRAL,0) +\n                     nvl(sm.tot_ach_referral_expense,0) )     as referral,\n                /* RS changes for ACH PS exclude ACH Discount from VMC Discount/Ic Income  */\n                sum( (sm.tot_inc_interchange              \n                     sm.tot_inc_discount) - nvl(sm.tot_inc_discount_ach,0) )                 as disc_ic_inc,\n                sum( sm.tot_inc_authorization )                 as auth_inc,\n                sum( sm.tot_inc_capture )                       as capture_inc,\n                sum( sm.tot_inc_debit )                         as debit_inc,\n                sum( sm.tot_inc_ind_plans )                     as plan_inc,\n                sum( sm.tot_inc_sys_generated -\n                       ( sm.equip_rental_income +\n                         sm.equip_sales_income +\n                         nvl(sm.equip_sales_tax_collected,0) \n                       )\n                    )                                           as sys_gen_inc,\n                sum( nvl(sm.tot_inc_discount_ach,0) + nvl(sm.tot_inc_ach,0))                 as ach_inc,\n                sum (nvl(sm.tot_ach_liability,0))               as ach_liability,    \n                sum (nvl(sm.tot_ach_referral_expense,0))        as ach_referral,    \n                sum (nvl(sm.tot_exp_ach_vendor,0))              as ps_exp,    \n                sum( nvl(sm.mes_only_inc_disc_ic,0) )           as mes_only_disc_ic,\n                sum( nvl(sm.mes_only_inc_authorization,0) )     as mes_only_auth,\n                sum( nvl(sm.mes_only_inc_capture,0) )           as mes_only_capture,\n                sum( nvl(sm.mes_only_inc_debit,0) )             as mes_only_debit,\n                sum( nvl(sm.mes_only_inc_ind_plans,0) )         as mes_only_plan,\n                sum( nvl(sm.mes_only_inc_sys_generated,0) )     as mes_only_sys_gen,\n                sum( sm.equip_rental_income )                   as equip_rental_inc,\n                sum( sm.equip_sales_income )                    as equip_sales_inc,\n                sum( nvl(sm.equip_sales_tax_collected,0) )      as equip_sales_tax,\n                sum( nvl(sm.equip_rental_base_cost,0) )         as equip_rental_base_cost,\n                sum( nvl(sm.equip_sales_base_cost,0) )          as equip_sales_base_cost,\n                sum( nvl(sm.equip_transfer_cost,0) )            as equip_transfer_cost,\n                sum( sm.disc_ic_dce_adj_amount )                as disc_ic_adj_amount,\n                sum( sm.fee_dce_adj_amount )                    as fee_adj_amount,\n                sum( nvl(sm.interchange_expense,0) )            as ic_exp,\n                sum( nvl(sm.vmc_assessment_expense,0) )         as assessment,                             \n                sum( sm.cash_adv_interchange_expense )          as cash_adv_interchange_exp,\n                sum( nvl(sm.vmc_fees,0) )                       as vmc_fees,\n                sum( sm.visa_vol_count )                        as visa_vol_count,\n                sum( sm.visa_vol_amount )                       as visa_vol_amount,\n                sum( sm.visa_sales_count )                      as visa_sales_count,\n                sum( sm.visa_sales_amount )                     as visa_sales_amount,\n                sum( sm.visa_credits_count )                    as visa_credits_count,\n                sum( sm.visa_credits_amount )                   as visa_credits_amount,\n                sum( sm.mc_vol_count )                          as mc_vol_count,\n                sum( sm.mc_vol_amount )                         as mc_vol_amount,\n                sum( sm.mc_sales_count )                        as mc_sales_count,\n                sum( sm.mc_sales_amount )                       as mc_sales_amount,\n                sum( sm.mc_credits_count )                      as mc_credits_count,\n                sum( sm.mc_credits_amount )                     as mc_credits_amount,\n                sum( sm.mc_cross_border_vol_count )             as mc_cross_border_vol_count,\n                sum( sm.mc_cross_border_vol_amount )            as mc_cross_border_vol_amount,\n                sum( sm.mc_cross_border_sales_count )           as mc_cross_border_sales_count,\n                sum( sm.mc_cross_border_sales_amount )          as mc_cross_border_sales_amount,\n                sum( sm.mc_cross_border_credits_count )         as mc_cross_border_credits_count,\n                sum( sm.mc_cross_border_credits_amount )        as mc_cross_border_credits_amount,\n                sum( sm.mc_cross_border_sales_amount +\n                     sm.mc_cross_border_credits_amount )        as mc_cross_border_hash_amount,\n                sum( sm.mc_foreign_std_vol_count )              as mc_foreign_std_vol_count,\n                sum( sm.mc_foreign_std_vol_amount )             as mc_foreign_std_vol_amount,\n                sum( sm.mc_foreign_std_sales_count )            as mc_foreign_std_sales_count,\n                sum( sm.mc_foreign_std_sales_amount )           as mc_foreign_std_sales_amount,\n                sum( sm.mc_foreign_std_credits_count )          as mc_foreign_std_credits_count,\n                sum( sm.mc_foreign_std_credits_amount )         as mc_foreign_std_credits_amount,\n                sum( sm.mc_foreign_std_sales_amount +\n                     sm.mc_foreign_std_credits_amount )         as mc_foreign_std_hash_amount,\n                sum( sm.mc_foreign_elec_vol_count )             as mc_foreign_elec_vol_count,\n                sum( sm.mc_foreign_elec_vol_amount )            as mc_foreign_elec_vol_amount,\n                sum( sm.mc_foreign_elec_sales_count )           as mc_foreign_elec_sales_count,\n                sum( sm.mc_foreign_elec_sales_amount )          as mc_foreign_elec_sales_amount,\n                sum( sm.mc_foreign_elec_credits_count )         as mc_foreign_elec_credits_count,\n                sum( sm.mc_foreign_elec_credits_amount )        as mc_foreign_elec_credits_amount,\n                sum( sm.mc_foreign_elec_sales_amount +\n                     sm.mc_foreign_elec_credits_amount )        as mc_foreign_elec_hash_amount,\n                /* RS changes for ACH PS Columns added  to calculate partner fees  */\n                sum( nvl(sm.ach_tran_count,0))                  as ach_tran_count,\n                sum( nvl(sm.ach_return_count,0))                as ach_return_count,\n                sum( nvl(sm.ach_unauth_count,0))                as ach_unauth_count,\n                sum( nvl(sm.mc_vol_count,0) - \n                     nvl(mc_cross_border_vol_count,0) )         as mc_nabu_vol_count,\n                sum(  sm.vmc_sales_count\n                    -- + nvl(sm.amex_ic_sales_count,0)    -- exclude per rory\n                    + nvl(sm.disc_ic_sales_count,0) )           as risk_sales_count,  \n                sum(  sm.vmc_sales_amount\n                    -- + nvl(sm.amex_ic_sales_amount,0)   -- exclude per rory\n                    + nvl(sm.disc_ic_sales_amount,0) )          as risk_sales_amount,  \n                sum(  sm.vmc_vol_count\n                    -- + nvl(sm.amex_ic_vol_count,0)      -- exclude per rory\n                    + nvl(sm.disc_ic_vol_count,0) )             as processing_vol_count,  \n                sum(  sm.vmc_vol_amount\n                    -- + nvl(sm.amex_ic_vol_amount,0)     -- exclude per rory\n                    + nvl(sm.disc_ic_vol_amount,0) )            as processing_vol_amount,  \n                sum(  sm.vmc_sales_count\n                    -- + nvl(sm.amex_ic_sales_count,0)    -- exclude per rory\n                    + nvl(sm.disc_ic_sales_count,0) )           as processing_sales_count,\n                sum(  sm.vmc_sales_amount\n                    -- + nvl(sm.amex_ic_sales_amount,0)   -- exclude per rory\n                    + nvl(sm.disc_ic_sales_amount,0) )          as processing_sales_amount,  \n                sum( sm.vmc_vol_count )                         as vmc_vol_count,\n                sum( sm.vmc_vol_amount )                        as vmc_vol_amount,  \n                sum( sm.vmc_sales_count )                       as vmc_sales_count,\n                sum( sm.vmc_sales_amount )                      as vmc_sales_amount,\n                sum( sm.vmc_credits_count )                     as vmc_credits_count,\n                sum( sm.vmc_credits_amount )                    as vmc_credits_amount,\n                sum( nvl(sm.amex_ic_vol_count,0) )              as am_ic_vol_count,\n                sum( nvl(sm.amex_ic_vol_amount,0) )             as am_ic_vol_amount,  \n                sum( nvl(sm.amex_ic_sales_count,0) )            as am_ic_sales_count,\n                sum( nvl(sm.amex_ic_sales_amount,0) )           as am_ic_sales_amount,  \n                sum( nvl(sm.amex_ic_credits_count,0) )          as am_ic_credits_count,\n                sum( nvl(sm.amex_ic_credits_amount,0) )         as am_ic_credits_amount,  \n                sum( nvl(sm.disc_ic_vol_count,0) )              as ds_ic_vol_count,\n                sum( nvl(sm.disc_ic_vol_amount,0) )             as ds_ic_vol_amount,  \n                sum( nvl(sm.disc_ic_sales_count,0) )            as ds_ic_sales_count,\n                sum( nvl(sm.disc_ic_sales_amount,0) )           as ds_ic_sales_amount,  \n                sum( nvl(sm.disc_ic_credits_count,0) )          as ds_ic_credits_count,\n                sum( nvl(sm.disc_ic_credits_amount,0) )         as ds_ic_credits_amount,  \n                sum( nvl(sm.amex_vol_count,0) )                 as amex_vol_count,\n                sum( nvl(sm.amex_vol_amount,0) )                as amex_vol_amount,  \n                sum( nvl(sm.amex_sales_count,0) )               as amex_sales_count,\n                sum( nvl(sm.amex_sales_amount,0) )              as amex_sales_amount,  \n                sum( nvl(sm.amex_credits_count,0) )             as amex_credits_count,\n                sum( nvl(sm.amex_credits_amount,0) )            as amex_credits_amount,  \n                sum( nvl(sm.dinr_vol_count,0) )                 as dinr_vol_count,\n                sum( nvl(sm.dinr_vol_amount,0) )                as dinr_vol_amount,  \n                sum( nvl(sm.dinr_sales_count,0) )               as dinr_sales_count,\n                sum( nvl(sm.dinr_sales_amount,0) )              as dinr_sales_amount,  \n                sum( nvl(sm.dinr_credits_count,0) )             as dinr_credits_count,\n                sum( nvl(sm.dinr_credits_amount,0) )            as dinr_credits_amount,  \n                sum( nvl(sm.disc_vol_count,0) )                 as disc_vol_count,\n                sum( nvl(sm.disc_vol_amount,0) )                as disc_vol_amount,  \n                sum( nvl(sm.disc_sales_count,0) )               as disc_sales_count,\n                sum( nvl(sm.disc_sales_amount,0) )              as disc_sales_amount,  \n                sum( nvl(sm.disc_credits_count,0) )             as disc_credits_count,\n                sum( nvl(sm.disc_credits_amount,0) )            as disc_credits_amount,  \n                sum( nvl(sm.jcb_vol_count,0) )                  as jcb_vol_count,\n                sum( nvl(sm.jcb_vol_amount,0) )                 as jcb_vol_amount, \n                sum( nvl(sm.jcb_sales_count,0) )                as jcb_sales_count,\n                sum( nvl(sm.jcb_sales_amount,0) )               as jcb_sales_amount,  \n                sum( nvl(sm.jcb_credits_count,0) )              as jcb_credits_count,\n                sum( nvl(sm.jcb_credits_amount,0) )             as jcb_credits_amount,  \n                sum( nvl(sm.pl1_vol_count,0) )                  as pl1_vol_count,\n                sum( nvl(sm.pl1_vol_amount,0) )                 as pl1_vol_amount, \n                sum( nvl(sm.pl1_sales_count,0) )                as pl1_sales_count,\n                sum( nvl(sm.pl1_sales_amount,0) )               as pl1_sales_amount,  \n                sum( nvl(sm.pl1_credits_count,0) )              as pl1_credits_count,\n                sum( nvl(sm.pl1_credits_amount,0) )             as pl1_credits_amount,  \n                sum( nvl(sm.pl2_vol_count,0) )                  as pl2_vol_count,\n                sum( nvl(sm.pl2_vol_amount,0) )                 as pl2_vol_amount, \n                sum( nvl(sm.pl2_sales_count,0) )                as pl2_sales_count,\n                sum( nvl(sm.pl2_sales_amount,0) )               as pl2_sales_amount,  \n                sum( nvl(sm.pl2_credits_count,0) )              as pl2_credits_count,\n                sum( nvl(sm.pl2_credits_amount,0) )             as pl2_credits_amount,  \n                sum( nvl(sm.pl3_vol_count,0) )                  as pl3_vol_count,\n                sum( nvl(sm.pl3_vol_amount,0) )                 as pl3_vol_amount, \n                sum( nvl(sm.pl3_sales_count,0) )                as pl3_sales_count,\n                sum( nvl(sm.pl3_sales_amount,0) )               as pl3_sales_amount,  \n                sum( nvl(sm.pl3_credits_count,0) )              as pl3_credits_count,\n                sum( nvl(sm.pl3_credits_amount,0) )             as pl3_credits_amount,  \n                sum( nvl(sm.debit_vol_count,0) )                as debit_vol_count,\n                sum( nvl(sm.debit_vol_amount,0) )               as debit_vol_amount,\n                sum( nvl(sm.debit_network_fees,0))              as debit_network_fees,\n                sum( nvl(sm.debit_sales_count,0) )              as debit_sales_count,\n                sum( nvl(sm.debit_sales_amount,0) )             as debit_sales_amount,  \n                sum( nvl(sm.debit_credits_count,0) )            as debit_credits_count,\n                sum( nvl(sm.debit_credits_amount,0) )           as debit_credits_amount,  \n                sum( nvl(sm.ebt_vol_count,0) )                  as ebt_vol_count,\n                sum( nvl(sm.achp_vol_count,0) )                 as achp_vol_count,\n                sum( nvl(sm.achp_return_count,0) )              as achp_return_count,\n                sum( nvl(dn.cat_01_total_count,0) )             as debit_count_cat_01,\n                sum( nvl(dn.cat_02_total_count,0) )             as debit_count_cat_02,\n                sum( nvl(dn.cat_03_total_count,0) )             as debit_count_cat_03,\n                sum( nvl(dn.cat_04_total_count,0) )             as debit_count_cat_04,\n                sum( nvl(dn.cat_05_total_count,0) )             as debit_count_cat_05,\n                sum( nvl(dn.cat_06_total_count,0) )             as debit_count_cat_06,\n                sum( nvl(dn.cat_07_total_count,0) )             as debit_count_cat_07,\n                sum( nvl(dn.cat_08_total_count,0) )             as debit_count_cat_08,\n                sum( nvl(dn.cat_09_total_count,0) )             as debit_count_cat_09,\n                sum( nvl(dn.cat_10_total_count,0) )             as debit_count_cat_10,\n                sum( sm.vmc_auth_count )                        as vmc_auth_count,\n                sum( nvl(sm.visa_auth_count,0) )                as visa_auth_count,\n                sum( nvl(sm.mc_auth_count,0) )                  as mc_auth_count,\n                sum( sm.amex_auth_count )                       as amex_auth_count,\n                sum( sm.dinr_auth_count )                       as dinr_auth_count,\n                sum( sm.disc_auth_count )                       as disc_auth_count,\n                sum( sm.jcb_auth_count )                        as jcb_auth_count,\n                sum( sm.debit_auth_count )                      as debit_auth_count,\n                sum( nvl(sm.auth_count_total,0) )               as auth_count_total,\n                sum( nvl(sm.auth_count_total_ext,0) )           as auth_count_total_ext,\n                sum( nvl(sm.auth_count_total_api,0) )           as auth_count_total_api,\n                sum( nvl(sm.auth_count_total_trident,0) )       as auth_count_total_trident,\n                sum( nvl(sm.vmc_auth_count_trident,0) )         as vmc_auth_count_trident,\n                sum( nvl(sm.amex_auth_count_trident,0) )        as amex_auth_count_trident,\n                sum( nvl(sm.amex_direct_auth_count_trident,0) ) as amex_direct_auth_count_trident,\n                sum( nvl(sm.amex_direct_auth_count_vital,0) )   as amex_direct_auth_count_vital,               \n                sum( nvl(sm.disc_auth_count_trident,0) )        as disc_auth_count_trident,\n                sum( nvl(sm.dinr_auth_count_trident,0) )        as dinr_auth_count_trident,\n                sum( nvl(sm.jcb_auth_count_trident,0) )         as jcb_auth_count_trident,\n                sum( nvl(sm.debit_auth_count_trident,0) )       as debit_auth_count_trident,\n                sum( nvl(sm.visa_auth_count,0) +\n                     nvl(dn.cat_03_total_count,0) )             as visa_interlink_auth_count,\n                sum( nvl(sm.disc_auth_count,0) +\n                     ( nvl(sm.debit_auth_count,0) -\n                       nvl(dn.cat_03_total_count,0) ) )         as non_visa_auth_access_count,\n                sum( sm.chargeback_vol_count )                  as chargeback_vol_count,\n                sum( sm.chargeback_vol_amount )                 as chargeback_vol_amount,  \n                sum( sm.incoming_chargeback_count )             as incoming_cb_count,\n                sum( sm.incoming_chargeback_amount )            as incoming_cb_amount,\n                sum( sm.incoming_ft_chargeback_count )          as incoming_ft_cb_count,\n                sum( sm.incoming_ft_chargeback_amount )         as incoming_ft_cb_amount,\n                sum( sm.retrieval_count )                       as retrieval_count,\n                sum( sm.retrieval_amount )                      as retrieval_amount,\n                sum( sm.service_call_count )                    as num_service_calls,\n                sum( sm.cash_advance_vol_count )                as cash_advance_vol_count,\n                sum( sm.cash_advance_vol_amount )               as cash_advance_vol_amount,\n                sum( decode( sm.activated_merchant,'Y',1,0 ) )  as num_activated_merchants,\n                sum( decode( sm.new_merchant,'Y',1,0 ) )        as num_new_merchants,\n                sum( decode( sm.amex_setup,'Y',1,0 ) )          as num_amex_setups,\n                sum( decode( sm.discover_setup,'Y',1,0 ) )      as num_disc_setups,\n                sum( sm.terminal_rental_count )                 as num_rentals,\n                sum( decode( sm.merchant_status,\n                             'C', 0,  -- closed\n                             'D', 0,  -- deleted\n                             'F', 0,  -- fraud\n                              1 ) )                             as num_active_merchants,\n                sum( nvl(sm.voice_auth_count,0) +\n                     nvl(sm.aru_auth_count,0) )                 as dialpay_auth_count,\n                sum( nvl(sm.avs_count,0) )                      as avs_auth_count,\n                sum( nvl(sm.ach_reject_count,0) )               as ach_reject_count,\n                sum( nvl(sm.ic_correction_count,0) )            as ic_correction_count,\n                sum( nvl(sm.terminal_swap_count,0) )            as swap_count,\n                sum( nvl(sm.terminal_deployment_count,0) )      as terminal_deployment_count,\n                sum( decode( nvl(sm.print_statement, 'N'), 'Y', 1, 0 ) )  \t\n                \t\t\t\t\t\t\t\t\t\t\t\tas print_statement_count,\n                sum( nvl(sm.batch_fee_count, 0) )  \t\n                \t\t\t\t\t\t\t\t\t\t\t\tas batch_fee_count\n        from    monthly_extract_summary   sm,\n                monthly_extract_dn        dn\n        where   sm.merchant_number =  :2   and\n                sm.active_date between  :3   and  :4   and\n                dn.hh_load_sec(+) = sm.hh_load_sec\n        group by sm.active_date, \n                 decode(sm.cash_advance_account,'Y',1,0),\n                 decode(sm.mes_inventory,'Y',1,0),\n                 decode(sm.moto_merchant,'Y',1,0),\n                 sm.product_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setDate(3,ReportDateBegin);
   __sJT_st.setDate(4,ReportDateEnd);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2618^7*/
      resultSet = it.getResultSet();      
      while( resultSet.next() )
      {
        for( int ct = 0; ct < ContractTypes.CONTRACT_SOURCE_COUNT; ++ct )
        {
          switch( ct )
          {
            // only test the types we are interested in 
            // to optimize the loading process.
            case ContractTypes.CONTRACT_SOURCE_LIABILITY:
            case ContractTypes.CONTRACT_SOURCE_REFERRAL:
            case ContractTypes.CONTRACT_SOURCE_RESELLER:
            case ContractTypes.CONTRACT_SOURCE_THIRD_PARTY:
              if ( (nodeId = getContractNode(merchantId,ct,resultSet.getDate("active_date"))) != 0L )
              {
                contract.loadContract( hierarchyNodeToOrgId(nodeId), ct, resultSet.getDate("active_date") );
                break;
              }
              
            case ContractTypes.CONTRACT_SOURCE_GRIN:
              if ( ( getProfReportType() == MERCH_PROF_TYPE_TPS_GRIN ) &&
                   ( (nodeId = getContractNode(merchantId,ct,resultSet.getDate("active_date"))) != 0L ) )
              {
                contract.loadContract( hierarchyNodeToOrgId(nodeId), ct, resultSet.getDate("active_date") );
                break;
              }
              
            default:
              continue;
          }
          
          if ( userType == UT_RESELLER )
          {
            switch( ct )
            {
              case ContractTypes.CONTRACT_SOURCE_REFERRAL:
              case ContractTypes.CONTRACT_SOURCE_RESELLER:
                processContractSummaryData( contract, ContractTypes.CONTRACT_TYPE_EXPENSE, resultSet );
                break;
                
              case ContractTypes.CONTRACT_SOURCE_LIABILITY:
                processContractSummaryData( contract, ContractTypes.CONTRACT_TYPE_INCOME, resultSet );
                break;
            }
          }
          else if ( (userType == UT_CONTRACT_BANK) || (userType == UT_VAR_BANK) )
          {
            switch( ct )
            {
              case ContractTypes.CONTRACT_SOURCE_REFERRAL:
                processContractSummaryData( contract, ContractTypes.CONTRACT_TYPE_INCOME, resultSet );
                break;
                
              case ContractTypes.CONTRACT_SOURCE_LIABILITY:
              case ContractTypes.CONTRACT_SOURCE_THIRD_PARTY:
                processContractSummaryData( contract, ContractTypes.CONTRACT_TYPE_EXPENSE, resultSet );
                break;
            }
          }
          else    // MES user
          {
            switch( ct )
            {
              case ContractTypes.CONTRACT_SOURCE_RESELLER:
                processContractSummaryData( contract, ContractTypes.CONTRACT_TYPE_INCOME, resultSet );
                break;
                
              case ContractTypes.CONTRACT_SOURCE_REFERRAL:
                if ( HasResellerContract == false )
                {
                  processContractSummaryData( contract, ContractTypes.CONTRACT_TYPE_EXPENSE, resultSet );
                }                  
                break;
                
              case ContractTypes.CONTRACT_SOURCE_LIABILITY:
                if ( HasResellerContract == false )
                {
                  processContractSummaryData( contract, ContractTypes.CONTRACT_TYPE_INCOME, resultSet );
                }                  
                break;
                
              case ContractTypes.CONTRACT_SOURCE_GRIN:
                processContractSummaryData( contract, ContractTypes.CONTRACT_TYPE_EXPENSE, resultSet );
                break;
            }
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadContractData()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
      try{ contract.cleanUp(); } catch( Exception e ) { }
    }
  }
  
  public void loadData( )
  {
    switch( ProfReportType )
    {
      case MERCH_PROF_TYPE_RETENTION:
        if( getReportMerchantId() == 0L )
        {
          loadRetentionData();
          break;
        }
        else
        {
          loadDataDefault();
        }
        break;
    
      case MERCH_PROF_TYPE_UNDER_WATER:
        if( getReportMerchantId() == 0L )
        {
          loadUnderWaterMerchants();
          break;
        }
        else
        {
          loadDataDefault();
        }
        break;
        
      case MERCH_PROF_TYPE_NET_REVENUE_RETENTION:
        loadNetRevenueRetentionData();
        break;
        
        // if it is a merchant, then fall through.  under water
        // merchant report only has top level summary and then
        // merchant level summary/detail.  there is nothing 
        // different about the merchant level summary/detail,
        // so just treat the request as a standard profitability 
        // report.
      default:
        loadDataDefault();
        break;
    }
  }
        
  public void loadDataDefault( )
  {
    int                         bankNumber        = getBankNumberFromNodeId(ReportNodeIdDefault);
    ResultSetIterator           it                = null;
    long                        lastOrgId         = -1L;
    long                        nodeId            = getReportHierarchyNode();
    long                        orgId             = 0L;
    ResultSet                   resultSet         = null;
    SummaryData                 row               = null;
    int                         userType          = getUserType(); 
    
    try
    {
      // GRIN is only available from the MES perspective
      if ( ReportType == MERCH_PROF_TYPE_TPS_GRIN )
      {
        userType = UT_MES;
      }
      
      ReportRows.removeAllElements();
      
      if( isNodeMerchant( nodeId ) )
      {
        if ( ReportBetGroup != BankContractBean.BET_GROUP_ALL )
        {
          loadDetailData( nodeId );
        }
        else // summary
        {
          int hasPinDebit=0;
          /*@lineinfo:generated-code*//*@lineinfo:2793^11*/

//  ************************************************************
//  #sql [Ctx] { select count(1) 
//              from t_hierarchy     th,
//                    mif             m,
//                    agent_bank_contract     abc
//              where m.merchant_number = :nodeId and
//                m.association_node = th.descendent and
//                th.ancestor = abc.hierarchy_node and
//                :ReportDateBegin between abc.valid_date_begin and abc.valid_date_end and
//                billing_element_type = :BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(1)  \n            from t_hierarchy     th,\n                  mif             m,\n                  agent_bank_contract     abc\n            where m.merchant_number =  :1   and\n              m.association_node = th.descendent and\n              th.ancestor = abc.hierarchy_node and\n               :2   between abc.valid_date_begin and abc.valid_date_end and\n              billing_element_type =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setInt(3,BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   hasPinDebit = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2804^11*/

          /*@lineinfo:generated-code*//*@lineinfo:2806^11*/

//  ************************************************************
//  #sql [Ctx] it = { select    /*+
//                            index (sm pk_monthly_extract_summary)
//                         */
//                        o.org_num                                     as org_num,
//                        o.org_group                                   as hierarchy_node,
//                        o.org_name                                    as org_name,
//                        count(sm.merchant_number)                     as merchant_count,
//                        decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0)       as cash_advance,          
//                        decode(sm.mes_inventory,'Y',1,0)              as mes_inventory,
//                        decode(sm.liability_contract,'Y',1,0)         as liability_contract,
//                        decode(sm.referral_contract,'Y',1,0)          as referral_contract,                   
//                        decode(sm.reseller_contract,'Y',1,0)          as reseller_contract,                   
//                        decode(sm.cash_advance_referral,'Y',1,0)      as cash_advance_referral,                   
//                        decode(sm.moto_merchant,'Y',1,0)              as moto_merchant,
//                        sum( sm.vmc_sales_count )                     as vmc_sales_count,
//                        sum( sm.vmc_sales_amount )                    as vmc_sales_amount,
//                        sum( sm.vmc_credits_count )                   as vmc_credits_count,
//                        sum( sm.vmc_credits_amount )                  as vmc_credits_amount,
//                        sum( sm.cash_advance_vol_count )              as cash_advance_vol_count,
//                        sum( sm.cash_advance_vol_amount )             as cash_advance_vol_amount, 
//                        sum( sm.TOT_INC_DISCOUNT + 
//                             sm.tot_inc_interchange - 
//                             nvl(sm.tot_inc_discount_ach,0))          as disc_ic_inc,
//                        sum( sm.tot_inc_authorization )               as auth_inc,
//                        sum( sm.tot_inc_capture )                     as capture_inc,
//                        sum( sm.tot_inc_debit )                       as debit_inc,
//                        sum( sm.tot_inc_ind_plans )                   as plan_inc,
//                        sum( sm.tot_inc_sys_generated -
//                             (sm.equip_sales_income + 
//                              sm.equip_rental_income +
//                              nvl(sm.equip_sales_tax_collected,0)
//                             ) 
//                           )                                          as sys_gen_inc,
//                        sum(nvl(sm.tot_inc_discount_ach,0))           as ach_disc_inc,  
//                        sum(nvl(sm.tot_inc_discount_ach,0) + 
//                            nvl(sm.tot_inc_ach,0))                    as ach_inc,
//                        sum(nvl(sm.tot_ach_disc_liability,0))         as ach_disc_liability,
//                        sum(nvl(sm.tot_ach_disc_referral,0))          as ach_disc_referral,   
//                        sum(nvl(sm.tot_exp_ach_vendor_discount,0))    as ps_disc_exp,
//                        sum(nvl(sm.tot_exp_ach_vendor,0))             as ps_exp,
//                        sum( nvl(sm.mes_only_inc_disc_ic,0) )         as mes_only_disc_ic,
//                        sum( nvl(sm.mes_only_inc_authorization,0) )   as mes_only_auth,
//                        sum( nvl(sm.mes_only_inc_capture,0) )         as mes_only_capture,
//                        sum( nvl(sm.mes_only_inc_debit,0) )           as mes_only_debit,
//                        sum( nvl(sm.mes_only_inc_ind_plans,0) )       as mes_only_plan,
//                        sum( nvl(sm.mes_only_inc_sys_generated,0) )   as mes_only_sys_gen,
//                        sum( sm.equip_sales_income )                  as equip_sales_inc,
//                        sum( sm.equip_rental_income )                 as equip_rental_inc,
//                        sum( nvl(sm.equip_sales_tax_collected,0) )    as equip_sales_tax,
//                        sum( nvl(sm.interchange_expense,0) )          as ic_exp,
//                        sum( nvl(sm.vmc_assessment_expense,0) )       as assessment,                             
//                        sum( nvl(sm.vmc_fees,0) )                     as vmc_fees,
//                        sum( sm.tot_exp_authorization )               as auth_exp,
//                        sum( sm.tot_exp_capture )                     as capture_exp,
//                        sum( sm.tot_exp_debit )                       as debit_exp,
//                        sum( sm.tot_exp_ind_plans )                   as plan_exp,
//                        sum( sm.tot_exp_sys_generated -
//                             (nvl(sm.equip_sales_expense,0) + 
//                              nvl(sm.equip_rental_expense,0)) )       as sys_gen_exp,
//                        sum( nvl(sm.supply_cost,0) )                  as supply_cost,                            
//                        sum( nvl(sm.equip_sales_expense,0) )          as equip_sales_exp,
//                        sum( nvl(sm.equip_rental_expense,0) )         as equip_rental_exp,
//                        sum( nvl(sm.equip_rental_base_cost,0) )       as equip_rental_base_cost,
//                        sum( nvl(sm.equip_sales_base_cost,0) )        as equip_sales_base_cost,
//                        sum( nvl(sm.excluded_equip_rental_amount,0) ) as excluded_equip_rental,
//                        sum( nvl(sm.excluded_equip_sales_amount,0) )  as excluded_equip_sales,
//                        sum( nvl(sm.excluded_fees_amount,0) )         as excluded_fees,
//                        sum( nvl(sm.equip_transfer_cost,0) )          as equip_transfer_cost,
//                        sum( nvl(sm.DISC_IC_DCE_ADJ_AMOUNT,0) )       as disc_ic_adj,
//                        sum( nvl(sm.fee_dce_adj_amount,0) )           as fee_adj,
//                        sum( nvl(mcli.tot_ndr,0) )                    as ndr_liability,   
//                        sum( nvl(mcli.tot_authorization,0) )          as auth_liability,
//                        sum( nvl(mcli.tot_capture,0) )                as capture_liability,
//                        case when :hasPinDebit > 0 then sum( nvl(mcli.tot_debit,0) +
//                                    nvl(sm.debit_network_fees, 0) )
//                              else sum( nvl(mcli.tot_debit,0) )
//                              end                                     as debit_liability,
//                        sum( nvl(mcli.tot_ind_plans,0) )              as plan_liability,
//                        sum( nvl(mcli.tot_sys_generated,0) )          as sys_gen_liability,
//                        sum( nvl(mcli.tot_equip_rental,0) )           as equip_rental_liability,
//                        sum( nvl(mcli.tot_equip_sales,0) )            as equip_sales_liability,          
//                        sum( nvl(mcli.tot_ach,0) )                    as ach_liability,
//                        sum( nvl(mcrf.tot_ndr,0) )                    as ndr_referral,   
//                        sum( nvl(mcrf.tot_authorization,0) )          as auth_referral,
//                        sum( nvl(mcrf.tot_capture,0) )                as capture_referral,
//                        sum( nvl(mcrf.tot_debit,0) )                  as debit_referral,
//                        sum( nvl(mcrf.tot_ind_plans,0) )              as plan_referral,
//                        sum( nvl(mcrf.tot_sys_generated,0) )          as sys_gen_referral,          
//                        sum( nvl(mcrf.tot_equip_rental,0) )           as equip_rental_referral,          
//                        sum( nvl(mcrf.tot_equip_sales,0) )            as equip_sales_referral,          
//                        sum( nvl(mcrf.tot_ach,0) )                    as ach_referral,
//                        sum( nvl(mcrs.tot_ndr,0) )                    as ndr_reseller,   
//                        sum( nvl(mcrs.tot_authorization,0) )          as auth_reseller,
//                        sum( nvl(mcrs.tot_capture,0) )                as capture_reseller,
//                        sum( nvl(mcrs.tot_debit,0) )                  as debit_reseller,
//                        sum( nvl(mcrs.tot_ind_plans,0) )              as plan_reseller,
//                        sum( nvl(mcrs.tot_sys_generated,0) )          as sys_gen_reseller,          
//                        sum( nvl(mcrs.tot_equip_rental,0) )           as equip_rental_reseller,          
//                        sum( nvl(mcrs.tot_equip_sales,0) )            as equip_sales_reseller,          
//                        sum( nvl(mcex.tot_ndr,0) )                    as ndr_external,   
//                        sum( nvl(mcex.tot_authorization,0) )          as auth_external,
//                        sum( nvl(mcex.tot_capture,0) )                as capture_external,
//                        sum( nvl(mcex.tot_debit,0) )                  as debit_external,
//                        sum( nvl(mcex.tot_ind_plans,0) )              as plan_external,
//                        sum( nvl(mcex.tot_sys_generated,0) )          as sys_gen_external,
//                        sum( nvl(mcex.tot_equip_rental,0) )           as equip_rental_external,
//                        sum( nvl(mcex.tot_equip_sales,0) )            as equip_sales_external,          
//                        sum( nvl(mcgr.tot_ndr,0) )                    as ndr_grin,
//                        sum( nvl(mcgr.tot_authorization,0) )          as auth_grin,
//                        sum( nvl(mcgr.tot_capture,0) )                as capture_grin,
//                        sum( nvl(mcgr.tot_debit,0) )                  as debit_grin,
//                        sum( nvl(mcgr.tot_ind_plans,0) )              as plan_grin,
//                        sum( nvl(mcgr.tot_sys_generated,0) )          as sys_gen_grin,
//                        sum( nvl(mcgr.tot_equip_rental,0) )           as equip_rental_grin,
//                        sum( nvl(mcgr.tot_equip_sales,0) )            as equip_sales_grin,
//                        sum( nvl(sm.tot_partnership,0) )              as partnership                      
//              from      monthly_extract_summary     sm,
//                        mif                         mf,
//                        monthly_extract_contract    mcli,
//                        monthly_extract_contract    mcrf,
//                        monthly_extract_contract    mcrs,
//                        monthly_extract_contract    mcex,
//                        monthly_extract_contract    mcgr,
//                        organization                o
//              where     sm.merchant_number = :nodeId and
//                        sm.active_date between :ReportDateBegin and :ReportDateEnd and
//                        mf.merchant_number = sm.merchant_number and
//                        ( :ProfReportType != :MERCH_PROF_TYPE_TPS_GRIN or -- 7 or
//                          mf.activation_date > '28-NOV-2003' ) and
//                        mcli.hh_load_sec(+) = sm.hh_load_sec and
//                        mcli.merchant_number(+) = sm.merchant_number and
//                        mcli.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_LIABILITY and -- 1 and
//                        mcrf.hh_load_sec(+) = sm.hh_load_sec and
//                        mcrf.merchant_number(+) = sm.merchant_number and
//                        mcrf.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_REFERRAL and -- 2 and
//                        mcrs.hh_load_sec(+) = sm.hh_load_sec and
//                        mcrs.merchant_number(+) = sm.merchant_number and
//                        mcrs.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_RESELLER and -- 3 and
//                        mcex.hh_load_sec(+) = sm.hh_load_sec and
//                        mcex.merchant_number(+) = sm.merchant_number and
//                        mcex.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_THIRD_PARTY and -- 4 and
//                        mcgr.hh_load_sec(+) = sm.hh_load_sec and
//                        mcgr.merchant_number(+) = sm.merchant_number and
//                        mcgr.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_GRIN and -- 5 and
//                        o.org_group = sm.merchant_number
//              group by o.org_num, o.org_group, o.org_name,
//                       decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0),
//                       decode(sm.liability_contract,'Y',1,0),
//                       decode(sm.referral_contract,'Y',1,0),
//                       decode(sm.reseller_contract,'Y',1,0),
//                       decode(sm.cash_advance_referral,'Y',1,0),
//                       decode(sm.moto_merchant,'Y',1,0),
//                       decode(sm.mes_inventory,'Y',1,0)
//              order by o.org_name, o.org_num
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    /*+\n                          index (sm pk_monthly_extract_summary)\n                       */\n                      o.org_num                                     as org_num,\n                      o.org_group                                   as hierarchy_node,\n                      o.org_name                                    as org_name,\n                      count(sm.merchant_number)                     as merchant_count,\n                      decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0)       as cash_advance,          \n                      decode(sm.mes_inventory,'Y',1,0)              as mes_inventory,\n                      decode(sm.liability_contract,'Y',1,0)         as liability_contract,\n                      decode(sm.referral_contract,'Y',1,0)          as referral_contract,                   \n                      decode(sm.reseller_contract,'Y',1,0)          as reseller_contract,                   \n                      decode(sm.cash_advance_referral,'Y',1,0)      as cash_advance_referral,                   \n                      decode(sm.moto_merchant,'Y',1,0)              as moto_merchant,\n                      sum( sm.vmc_sales_count )                     as vmc_sales_count,\n                      sum( sm.vmc_sales_amount )                    as vmc_sales_amount,\n                      sum( sm.vmc_credits_count )                   as vmc_credits_count,\n                      sum( sm.vmc_credits_amount )                  as vmc_credits_amount,\n                      sum( sm.cash_advance_vol_count )              as cash_advance_vol_count,\n                      sum( sm.cash_advance_vol_amount )             as cash_advance_vol_amount, \n                      sum( sm.TOT_INC_DISCOUNT + \n                           sm.tot_inc_interchange - \n                           nvl(sm.tot_inc_discount_ach,0))          as disc_ic_inc,\n                      sum( sm.tot_inc_authorization )               as auth_inc,\n                      sum( sm.tot_inc_capture )                     as capture_inc,\n                      sum( sm.tot_inc_debit )                       as debit_inc,\n                      sum( sm.tot_inc_ind_plans )                   as plan_inc,\n                      sum( sm.tot_inc_sys_generated -\n                           (sm.equip_sales_income + \n                            sm.equip_rental_income +\n                            nvl(sm.equip_sales_tax_collected,0)\n                           ) \n                         )                                          as sys_gen_inc,\n                      sum(nvl(sm.tot_inc_discount_ach,0))           as ach_disc_inc,  \n                      sum(nvl(sm.tot_inc_discount_ach,0) + \n                          nvl(sm.tot_inc_ach,0))                    as ach_inc,\n                      sum(nvl(sm.tot_ach_disc_liability,0))         as ach_disc_liability,\n                      sum(nvl(sm.tot_ach_disc_referral,0))          as ach_disc_referral,   \n                      sum(nvl(sm.tot_exp_ach_vendor_discount,0))    as ps_disc_exp,\n                      sum(nvl(sm.tot_exp_ach_vendor,0))             as ps_exp,\n                      sum( nvl(sm.mes_only_inc_disc_ic,0) )         as mes_only_disc_ic,\n                      sum( nvl(sm.mes_only_inc_authorization,0) )   as mes_only_auth,\n                      sum( nvl(sm.mes_only_inc_capture,0) )         as mes_only_capture,\n                      sum( nvl(sm.mes_only_inc_debit,0) )           as mes_only_debit,\n                      sum( nvl(sm.mes_only_inc_ind_plans,0) )       as mes_only_plan,\n                      sum( nvl(sm.mes_only_inc_sys_generated,0) )   as mes_only_sys_gen,\n                      sum( sm.equip_sales_income )                  as equip_sales_inc,\n                      sum( sm.equip_rental_income )                 as equip_rental_inc,\n                      sum( nvl(sm.equip_sales_tax_collected,0) )    as equip_sales_tax,\n                      sum( nvl(sm.interchange_expense,0) )          as ic_exp,\n                      sum( nvl(sm.vmc_assessment_expense,0) )       as assessment,                             \n                      sum( nvl(sm.vmc_fees,0) )                     as vmc_fees,\n                      sum( sm.tot_exp_authorization )               as auth_exp,\n                      sum( sm.tot_exp_capture )                     as capture_exp,\n                      sum( sm.tot_exp_debit )                       as debit_exp,\n                      sum( sm.tot_exp_ind_plans )                   as plan_exp,\n                      sum( sm.tot_exp_sys_generated -\n                           (nvl(sm.equip_sales_expense,0) + \n                            nvl(sm.equip_rental_expense,0)) )       as sys_gen_exp,\n                      sum( nvl(sm.supply_cost,0) )                  as supply_cost,                            \n                      sum( nvl(sm.equip_sales_expense,0) )          as equip_sales_exp,\n                      sum( nvl(sm.equip_rental_expense,0) )         as equip_rental_exp,\n                      sum( nvl(sm.equip_rental_base_cost,0) )       as equip_rental_base_cost,\n                      sum( nvl(sm.equip_sales_base_cost,0) )        as equip_sales_base_cost,\n                      sum( nvl(sm.excluded_equip_rental_amount,0) ) as excluded_equip_rental,\n                      sum( nvl(sm.excluded_equip_sales_amount,0) )  as excluded_equip_sales,\n                      sum( nvl(sm.excluded_fees_amount,0) )         as excluded_fees,\n                      sum( nvl(sm.equip_transfer_cost,0) )          as equip_transfer_cost,\n                      sum( nvl(sm.DISC_IC_DCE_ADJ_AMOUNT,0) )       as disc_ic_adj,\n                      sum( nvl(sm.fee_dce_adj_amount,0) )           as fee_adj,\n                      sum( nvl(mcli.tot_ndr,0) )                    as ndr_liability,   \n                      sum( nvl(mcli.tot_authorization,0) )          as auth_liability,\n                      sum( nvl(mcli.tot_capture,0) )                as capture_liability,\n                      case when  :1   > 0 then sum( nvl(mcli.tot_debit,0) +\n                                  nvl(sm.debit_network_fees, 0) )\n                            else sum( nvl(mcli.tot_debit,0) )\n                            end                                     as debit_liability,\n                      sum( nvl(mcli.tot_ind_plans,0) )              as plan_liability,\n                      sum( nvl(mcli.tot_sys_generated,0) )          as sys_gen_liability,\n                      sum( nvl(mcli.tot_equip_rental,0) )           as equip_rental_liability,\n                      sum( nvl(mcli.tot_equip_sales,0) )            as equip_sales_liability,          \n                      sum( nvl(mcli.tot_ach,0) )                    as ach_liability,\n                      sum( nvl(mcrf.tot_ndr,0) )                    as ndr_referral,   \n                      sum( nvl(mcrf.tot_authorization,0) )          as auth_referral,\n                      sum( nvl(mcrf.tot_capture,0) )                as capture_referral,\n                      sum( nvl(mcrf.tot_debit,0) )                  as debit_referral,\n                      sum( nvl(mcrf.tot_ind_plans,0) )              as plan_referral,\n                      sum( nvl(mcrf.tot_sys_generated,0) )          as sys_gen_referral,          \n                      sum( nvl(mcrf.tot_equip_rental,0) )           as equip_rental_referral,          \n                      sum( nvl(mcrf.tot_equip_sales,0) )            as equip_sales_referral,          \n                      sum( nvl(mcrf.tot_ach,0) )                    as ach_referral,\n                      sum( nvl(mcrs.tot_ndr,0) )                    as ndr_reseller,   \n                      sum( nvl(mcrs.tot_authorization,0) )          as auth_reseller,\n                      sum( nvl(mcrs.tot_capture,0) )                as capture_reseller,\n                      sum( nvl(mcrs.tot_debit,0) )                  as debit_reseller,\n                      sum( nvl(mcrs.tot_ind_plans,0) )              as plan_reseller,\n                      sum( nvl(mcrs.tot_sys_generated,0) )          as sys_gen_reseller,          \n                      sum( nvl(mcrs.tot_equip_rental,0) )           as equip_rental_reseller,          \n                      sum( nvl(mcrs.tot_equip_sales,0) )            as equip_sales_reseller,          \n                      sum( nvl(mcex.tot_ndr,0) )                    as ndr_external,   \n                      sum( nvl(mcex.tot_authorization,0) )          as auth_external,\n                      sum( nvl(mcex.tot_capture,0) )                as capture_external,\n                      sum( nvl(mcex.tot_debit,0) )                  as debit_external,\n                      sum( nvl(mcex.tot_ind_plans,0) )              as plan_external,\n                      sum( nvl(mcex.tot_sys_generated,0) )          as sys_gen_external,\n                      sum( nvl(mcex.tot_equip_rental,0) )           as equip_rental_external,\n                      sum( nvl(mcex.tot_equip_sales,0) )            as equip_sales_external,          \n                      sum( nvl(mcgr.tot_ndr,0) )                    as ndr_grin,\n                      sum( nvl(mcgr.tot_authorization,0) )          as auth_grin,\n                      sum( nvl(mcgr.tot_capture,0) )                as capture_grin,\n                      sum( nvl(mcgr.tot_debit,0) )                  as debit_grin,\n                      sum( nvl(mcgr.tot_ind_plans,0) )              as plan_grin,\n                      sum( nvl(mcgr.tot_sys_generated,0) )          as sys_gen_grin,\n                      sum( nvl(mcgr.tot_equip_rental,0) )           as equip_rental_grin,\n                      sum( nvl(mcgr.tot_equip_sales,0) )            as equip_sales_grin,\n                      sum( nvl(sm.tot_partnership,0) )              as partnership                      \n            from      monthly_extract_summary     sm,\n                      mif                         mf,\n                      monthly_extract_contract    mcli,\n                      monthly_extract_contract    mcrf,\n                      monthly_extract_contract    mcrs,\n                      monthly_extract_contract    mcex,\n                      monthly_extract_contract    mcgr,\n                      organization                o\n            where     sm.merchant_number =  :2   and\n                      sm.active_date between  :3   and  :4   and\n                      mf.merchant_number = sm.merchant_number and\n                      (  :5   !=  :6   or -- 7 or\n                        mf.activation_date > '28-NOV-2003' ) and\n                      mcli.hh_load_sec(+) = sm.hh_load_sec and\n                      mcli.merchant_number(+) = sm.merchant_number and\n                      mcli.contract_type(+) =  :7   and -- 1 and\n                      mcrf.hh_load_sec(+) = sm.hh_load_sec and\n                      mcrf.merchant_number(+) = sm.merchant_number and\n                      mcrf.contract_type(+) =  :8   and -- 2 and\n                      mcrs.hh_load_sec(+) = sm.hh_load_sec and\n                      mcrs.merchant_number(+) = sm.merchant_number and\n                      mcrs.contract_type(+) =  :9   and -- 3 and\n                      mcex.hh_load_sec(+) = sm.hh_load_sec and\n                      mcex.merchant_number(+) = sm.merchant_number and\n                      mcex.contract_type(+) =  :10   and -- 4 and\n                      mcgr.hh_load_sec(+) = sm.hh_load_sec and\n                      mcgr.merchant_number(+) = sm.merchant_number and\n                      mcgr.contract_type(+) =  :11   and -- 5 and\n                      o.org_group = sm.merchant_number\n            group by o.org_num, o.org_group, o.org_name,\n                     decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0),\n                     decode(sm.liability_contract,'Y',1,0),\n                     decode(sm.referral_contract,'Y',1,0),\n                     decode(sm.reseller_contract,'Y',1,0),\n                     decode(sm.cash_advance_referral,'Y',1,0),\n                     decode(sm.moto_merchant,'Y',1,0),\n                     decode(sm.mes_inventory,'Y',1,0)\n            order by o.org_name, o.org_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,hasPinDebit);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setDate(3,ReportDateBegin);
   __sJT_st.setDate(4,ReportDateEnd);
   __sJT_st.setInt(5,ProfReportType);
   __sJT_st.setInt(6,MERCH_PROF_TYPE_TPS_GRIN);
   __sJT_st.setInt(7,ContractTypes.CONTRACT_SOURCE_LIABILITY);
   __sJT_st.setInt(8,ContractTypes.CONTRACT_SOURCE_REFERRAL);
   __sJT_st.setInt(9,ContractTypes.CONTRACT_SOURCE_RESELLER);
   __sJT_st.setInt(10,ContractTypes.CONTRACT_SOURCE_THIRD_PARTY);
   __sJT_st.setInt(11,ContractTypes.CONTRACT_SOURCE_GRIN);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2962^11*/
        }
      }
      else if ( isNodeAssociation( nodeId ) == true ) // assoc
      {
        if( ReportUserBean != null && ReportUserBean.hasRight(com.mes.constants.MesUsers.RIGHT_REPORT_SEE_ALL_CB_MERCHANTS) ) { 
          /*@lineinfo:generated-code*//*@lineinfo:2968^11*/

//  ************************************************************
//  #sql [Ctx] it = { select    /*+ index (sm idx_mon_ext_sum_ahn_ad) */
//                  o.org_num                                     as org_num,
//                  o.org_group                                   as hierarchy_node,
//                  o.org_name                                    as org_name,
//                  count(mids.merchant_number)                     as merchant_count,
//                  decode(mids.CASH_ADVANCE_ACCOUNT,'Y',1,0)       as cash_advance,
//                  decode(mids.mes_inventory,'Y',1,0)              as mes_inventory,
//                  decode(mids.liability_contract,'Y',1,0)         as liability_contract,
//                  decode(mids.referral_contract,'Y',1,0)          as referral_contract,
//                  decode(mids.reseller_contract,'Y',1,0)          as reseller_contract,
//                  decode(mids.cash_advance_referral,'Y',1,0)      as cash_advance_referral,
//                  decode(mids.moto_merchant,'Y',1,0)              as moto_merchant,
//                  nvl(mf.dmacctst, '')                          as account_status,
//                  sum( mids.vmc_sales_count )                     as vmc_sales_count,
//                  sum( mids.vmc_sales_amount )                    as vmc_sales_amount,
//                  sum( mids.vmc_credits_count )                   as vmc_credits_count,
//                  sum( mids.vmc_credits_amount )                  as vmc_credits_amount,
//                  sum( mids.cash_advance_vol_count )              as cash_advance_vol_count,
//                  sum( mids.cash_advance_vol_amount )             as cash_advance_vol_amount,
//                  sum( mids.TOT_INC_DISCOUNT + 
//                       mids.tot_inc_interchange - 
//                       nvl(mids.tot_inc_discount_ach,0))          as disc_ic_inc,
//                  sum( mids.tot_inc_authorization )               as auth_inc,
//                  sum( mids.tot_inc_capture )                     as capture_inc,
//                  sum( mids.tot_inc_debit )                       as debit_inc,
//                  sum( mids.tot_inc_ind_plans )                   as plan_inc,
//                  sum( mids.tot_inc_sys_generated -
//                          (mids.equip_sales_income +
//                              mids.equip_rental_income +
//                              nvl(mids.equip_sales_tax_collected,0))
//                  )                                               as sys_gen_inc,
//                  sum(nvl(mids.tot_inc_discount_ach,0))           as ach_disc_inc,  
//                  sum(nvl(mids.tot_inc_discount_ach,0) + 
//                      nvl(mids.tot_inc_ach,0))                    as ach_inc,
//                  sum(nvl(mids.tot_ach_disc_liability,0))         as ach_disc_liability,
//                  sum(nvl(mids.tot_ach_disc_referral,0))          as ach_disc_referral,   
//                  sum(nvl(mids.tot_exp_ach_vendor_discount,0))    as ps_disc_exp,
//                  sum(nvl(mids.tot_exp_ach_vendor,0))             as ps_exp,
//                  sum( nvl(mids.mes_only_inc_disc_ic,0) )         as mes_only_disc_ic,
//                  sum( nvl(mids.mes_only_inc_authorization,0) )   as mes_only_auth,
//                  sum( nvl(mids.mes_only_inc_capture,0) )         as mes_only_capture,
//                  sum( nvl(mids.mes_only_inc_debit,0) )           as mes_only_debit,
//                  sum( nvl(mids.mes_only_inc_ind_plans,0) )       as mes_only_plan,
//                  sum( nvl(mids.mes_only_inc_sys_generated,0) )   as mes_only_sys_gen,
//                  sum( mids.equip_sales_income )                  as equip_sales_inc,
//                  sum( mids.equip_rental_income )                 as equip_rental_inc,
//                  sum( nvl(mids.equip_sales_tax_collected,0) )    as equip_sales_tax,
//                  sum( nvl(mids.interchange_expense,0) )          as ic_exp,
//                  sum( nvl(mids.vmc_assessment_expense,0) )       as assessment,
//                  sum( nvl(mids.vmc_fees,0) )                     as vmc_fees,
//                  sum( mids.tot_exp_authorization )               as auth_exp,
//                  sum( mids.tot_exp_capture )                     as capture_exp,
//                  sum( mids.tot_exp_debit )                       as debit_exp,
//                  sum( mids.tot_exp_ind_plans )                   as plan_exp,
//                  sum( mids.tot_exp_sys_generated -
//                      (nvl(mids.equip_sales_expense,0) +
//                          nvl(mids.equip_rental_expense,0)) )       as sys_gen_exp,
//                  sum( nvl(mids.supply_cost,0) )                  as supply_cost,
//                  sum( nvl(mids.equip_sales_expense,0) )          as equip_sales_exp,
//                  sum( nvl(mids.equip_rental_expense,0) )         as equip_rental_exp,
//                  sum( nvl(mids.equip_rental_base_cost,0) )       as equip_rental_base_cost,
//                  sum( nvl(mids.equip_sales_base_cost,0) )        as equip_sales_base_cost,
//                  sum( nvl(mids.excluded_equip_rental_amount,0) ) as excluded_equip_rental,
//                  sum( nvl(mids.excluded_equip_sales_amount,0) )  as excluded_equip_sales,
//                  sum( nvl(mids.excluded_fees_amount,0) )         as excluded_fees,
//                  sum( nvl(mids.equip_transfer_cost,0) )          as equip_transfer_cost,
//                  sum( nvl(mids.DISC_IC_DCE_ADJ_AMOUNT,0) )       as disc_ic_adj,
//                  sum( nvl(mids.fee_dce_adj_amount,0) )           as fee_adj,
//                  sum( nvl(mcli.tot_ndr,0) )                    as ndr_liability,
//                  sum( nvl(mcli.tot_authorization,0) )          as auth_liability,
//                  sum( nvl(mcli.tot_capture,0) )                as capture_liability,
//                case when exists ( select 1
//                          from t_hierarchy     th,
//                            mif             m,
//                            agent_bank_contract     abc
//                          where m.merchant_number = o.org_group and
//                          m.association_node = th.descendent and
//                          th.ancestor = abc.hierarchy_node and
//                          :ReportDateBegin between abc.valid_date_begin and abc.valid_date_end and
//                          billing_element_type = :BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG)  
//                    then sum( nvl(mcli.tot_debit,0) + nvl(mids.debit_network_fees, 0) )
//                    else sum( nvl(mcli.tot_debit,0) )
//                  end                                           as debit_liability,
//                  sum( nvl(mcli.tot_ind_plans,0) )              as plan_liability,
//                  sum( nvl(mcli.tot_sys_generated,0) )          as sys_gen_liability,
//                  sum( nvl(mcli.tot_equip_rental,0) )           as equip_rental_liability,
//                  sum( nvl(mcli.tot_equip_sales,0) )            as equip_sales_liability,
//                  sum( nvl(mcli.tot_ach,0) )                    as ach_liability,
//                  sum( nvl(mcrf.tot_ndr,0) )                    as ndr_referral,
//                  sum( nvl(mcrf.tot_authorization,0) )          as auth_referral,
//                  sum( nvl(mcrf.tot_capture,0) )                as capture_referral,
//                  sum( nvl(mcrf.tot_debit,0) )                  as debit_referral,
//                  sum( nvl(mcrf.tot_ind_plans,0) )              as plan_referral,
//                  sum( nvl(mcrf.tot_sys_generated,0) )          as sys_gen_referral,
//                  sum( nvl(mcrf.tot_equip_rental,0) )           as equip_rental_referral,
//                  sum( nvl(mcrf.tot_equip_sales,0) )            as equip_sales_referral,
//                  sum( nvl(mcrf.tot_ach,0) )                    as ach_referral,
//                  sum( nvl(mcrs.tot_ndr,0) )                    as ndr_reseller,
//                  sum( nvl(mcrs.tot_authorization,0) )          as auth_reseller,
//                  sum( nvl(mcrs.tot_capture,0) )                as capture_reseller,
//                  sum( nvl(mcrs.tot_debit,0) )                  as debit_reseller,
//                  sum( nvl(mcrs.tot_ind_plans,0) )              as plan_reseller,
//                  sum( nvl(mcrs.tot_sys_generated,0) )          as sys_gen_reseller,
//                  sum( nvl(mcrs.tot_equip_rental,0) )           as equip_rental_reseller,
//                  sum( nvl(mcrs.tot_equip_sales,0) )            as equip_sales_reseller,
//                  sum( nvl(mcex.tot_ndr,0) )                    as ndr_external,
//                  sum( nvl(mcex.tot_authorization,0) )          as auth_external,
//                  sum( nvl(mcex.tot_capture,0) )                as capture_external,
//                  sum( nvl(mcex.tot_debit,0) )                  as debit_external,
//                  sum( nvl(mcex.tot_ind_plans,0) )              as plan_external,
//                  sum( nvl(mcex.tot_sys_generated,0) )          as sys_gen_external,
//                  sum( nvl(mcex.tot_equip_rental,0) )           as equip_rental_external,
//                  sum( nvl(mcex.tot_equip_sales,0) )            as equip_sales_external,
//                  sum( nvl(mcgr.tot_ndr,0) )                    as ndr_grin,
//                  sum( nvl(mcgr.tot_authorization,0) )          as auth_grin,
//                  sum( nvl(mcgr.tot_capture,0) )                as capture_grin,
//                  sum( nvl(mcgr.tot_debit,0) )                  as debit_grin,
//                  sum( nvl(mcgr.tot_ind_plans,0) )              as plan_grin,
//                  sum( nvl(mcgr.tot_sys_generated,0) )          as sys_gen_grin,
//                  sum( nvl(mcgr.tot_equip_rental,0) )           as equip_rental_grin,
//                  sum( nvl(mcgr.tot_equip_sales,0) )            as equip_sales_grin,
//                  sum( nvl(mids.tot_partnership,0) )            as partnership
//                from      organization                o,
//                  mif                         mf,
//                  monthly_extract_contract    mcli,
//                  monthly_extract_contract    mcrf,
//                  monthly_extract_contract    mcrs,
//                  monthly_extract_contract    mcex,
//                  monthly_extract_contract    mcgr,
//                  ( select *
//                    from monthly_extract_summary
//                    where assoc_hierarchy_node in
//                      (
//                        :nodeId,decode(:bankNumber,3943,('3941' || substr(:nodeId,5)),-1)
//                      )
//                      and
//                      active_date between :ReportDateBegin and :ReportDateEnd
//                  ) mids
//                where mf.association_node = :nodeId and
//                  ( :ProfReportType != 7 or -- 7 or
//                    mf.activation_date > '28-NOV-2003' ) and
//                  mids.merchant_number(+) = mf.merchant_number and
//                  o.org_group = mf.merchant_number and
//                  mcli.hh_load_sec(+) = mids.hh_load_sec and
//                  mcli.merchant_number(+) = mids.merchant_number and
//                  mcli.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_LIABILITY and -- 1 and
//                  mcrf.hh_load_sec(+) = mids.hh_load_sec and
//                  mcrf.merchant_number(+) = mids.merchant_number and
//                  mcrf.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_REFERRAL and -- 2 and
//                  mcrs.hh_load_sec(+) = mids.hh_load_sec and
//                  mcrs.merchant_number(+) = mids.merchant_number and
//                  mcrs.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_RESELLER and -- 3 and
//                  mcex.hh_load_sec(+) = mids.hh_load_sec and
//                  mcex.merchant_number(+) = mids.merchant_number and
//                  mcex.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_THIRD_PARTY and -- 4 and
//                  mcgr.hh_load_sec(+) = mids.hh_load_sec and
//                  mcgr.merchant_number(+) = mids.merchant_number and
//                  mcgr.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_GRIN -- 5
//                group by o.org_num, o.org_group, o.org_name,
//                  decode(mids.CASH_ADVANCE_ACCOUNT,'Y',1,0),
//                  decode(mids.liability_contract,'Y',1,0),
//                  decode(mids.referral_contract,'Y',1,0),
//                  decode(mids.reseller_contract,'Y',1,0),
//                  decode(mids.cash_advance_referral,'Y',1,0),
//                  decode(mids.moto_merchant,'Y',1,0),
//                  decode(mids.mes_inventory,'Y',1,0),
//                  mf.dmacctst
//                order by o.org_name, o.org_num
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    /*+ index (sm idx_mon_ext_sum_ahn_ad) */\n                o.org_num                                     as org_num,\n                o.org_group                                   as hierarchy_node,\n                o.org_name                                    as org_name,\n                count(mids.merchant_number)                     as merchant_count,\n                decode(mids.CASH_ADVANCE_ACCOUNT,'Y',1,0)       as cash_advance,\n                decode(mids.mes_inventory,'Y',1,0)              as mes_inventory,\n                decode(mids.liability_contract,'Y',1,0)         as liability_contract,\n                decode(mids.referral_contract,'Y',1,0)          as referral_contract,\n                decode(mids.reseller_contract,'Y',1,0)          as reseller_contract,\n                decode(mids.cash_advance_referral,'Y',1,0)      as cash_advance_referral,\n                decode(mids.moto_merchant,'Y',1,0)              as moto_merchant,\n                nvl(mf.dmacctst, '')                          as account_status,\n                sum( mids.vmc_sales_count )                     as vmc_sales_count,\n                sum( mids.vmc_sales_amount )                    as vmc_sales_amount,\n                sum( mids.vmc_credits_count )                   as vmc_credits_count,\n                sum( mids.vmc_credits_amount )                  as vmc_credits_amount,\n                sum( mids.cash_advance_vol_count )              as cash_advance_vol_count,\n                sum( mids.cash_advance_vol_amount )             as cash_advance_vol_amount,\n                sum( mids.TOT_INC_DISCOUNT + \n                     mids.tot_inc_interchange - \n                     nvl(mids.tot_inc_discount_ach,0))          as disc_ic_inc,\n                sum( mids.tot_inc_authorization )               as auth_inc,\n                sum( mids.tot_inc_capture )                     as capture_inc,\n                sum( mids.tot_inc_debit )                       as debit_inc,\n                sum( mids.tot_inc_ind_plans )                   as plan_inc,\n                sum( mids.tot_inc_sys_generated -\n                        (mids.equip_sales_income +\n                            mids.equip_rental_income +\n                            nvl(mids.equip_sales_tax_collected,0))\n                )                                               as sys_gen_inc,\n                sum(nvl(mids.tot_inc_discount_ach,0))           as ach_disc_inc,  \n                sum(nvl(mids.tot_inc_discount_ach,0) + \n                    nvl(mids.tot_inc_ach,0))                    as ach_inc,\n                sum(nvl(mids.tot_ach_disc_liability,0))         as ach_disc_liability,\n                sum(nvl(mids.tot_ach_disc_referral,0))          as ach_disc_referral,   \n                sum(nvl(mids.tot_exp_ach_vendor_discount,0))    as ps_disc_exp,\n                sum(nvl(mids.tot_exp_ach_vendor,0))             as ps_exp,\n                sum( nvl(mids.mes_only_inc_disc_ic,0) )         as mes_only_disc_ic,\n                sum( nvl(mids.mes_only_inc_authorization,0) )   as mes_only_auth,\n                sum( nvl(mids.mes_only_inc_capture,0) )         as mes_only_capture,\n                sum( nvl(mids.mes_only_inc_debit,0) )           as mes_only_debit,\n                sum( nvl(mids.mes_only_inc_ind_plans,0) )       as mes_only_plan,\n                sum( nvl(mids.mes_only_inc_sys_generated,0) )   as mes_only_sys_gen,\n                sum( mids.equip_sales_income )                  as equip_sales_inc,\n                sum( mids.equip_rental_income )                 as equip_rental_inc,\n                sum( nvl(mids.equip_sales_tax_collected,0) )    as equip_sales_tax,\n                sum( nvl(mids.interchange_expense,0) )          as ic_exp,\n                sum( nvl(mids.vmc_assessment_expense,0) )       as assessment,\n                sum( nvl(mids.vmc_fees,0) )                     as vmc_fees,\n                sum( mids.tot_exp_authorization )               as auth_exp,\n                sum( mids.tot_exp_capture )                     as capture_exp,\n                sum( mids.tot_exp_debit )                       as debit_exp,\n                sum( mids.tot_exp_ind_plans )                   as plan_exp,\n                sum( mids.tot_exp_sys_generated -\n                    (nvl(mids.equip_sales_expense,0) +\n                        nvl(mids.equip_rental_expense,0)) )       as sys_gen_exp,\n                sum( nvl(mids.supply_cost,0) )                  as supply_cost,\n                sum( nvl(mids.equip_sales_expense,0) )          as equip_sales_exp,\n                sum( nvl(mids.equip_rental_expense,0) )         as equip_rental_exp,\n                sum( nvl(mids.equip_rental_base_cost,0) )       as equip_rental_base_cost,\n                sum( nvl(mids.equip_sales_base_cost,0) )        as equip_sales_base_cost,\n                sum( nvl(mids.excluded_equip_rental_amount,0) ) as excluded_equip_rental,\n                sum( nvl(mids.excluded_equip_sales_amount,0) )  as excluded_equip_sales,\n                sum( nvl(mids.excluded_fees_amount,0) )         as excluded_fees,\n                sum( nvl(mids.equip_transfer_cost,0) )          as equip_transfer_cost,\n                sum( nvl(mids.DISC_IC_DCE_ADJ_AMOUNT,0) )       as disc_ic_adj,\n                sum( nvl(mids.fee_dce_adj_amount,0) )           as fee_adj,\n                sum( nvl(mcli.tot_ndr,0) )                    as ndr_liability,\n                sum( nvl(mcli.tot_authorization,0) )          as auth_liability,\n                sum( nvl(mcli.tot_capture,0) )                as capture_liability,\n              case when exists ( select 1\n                        from t_hierarchy     th,\n                          mif             m,\n                          agent_bank_contract     abc\n                        where m.merchant_number = o.org_group and\n                        m.association_node = th.descendent and\n                        th.ancestor = abc.hierarchy_node and\n                         :1   between abc.valid_date_begin and abc.valid_date_end and\n                        billing_element_type =  :2  )  \n                  then sum( nvl(mcli.tot_debit,0) + nvl(mids.debit_network_fees, 0) )\n                  else sum( nvl(mcli.tot_debit,0) )\n                end                                           as debit_liability,\n                sum( nvl(mcli.tot_ind_plans,0) )              as plan_liability,\n                sum( nvl(mcli.tot_sys_generated,0) )          as sys_gen_liability,\n                sum( nvl(mcli.tot_equip_rental,0) )           as equip_rental_liability,\n                sum( nvl(mcli.tot_equip_sales,0) )            as equip_sales_liability,\n                sum( nvl(mcli.tot_ach,0) )                    as ach_liability,\n                sum( nvl(mcrf.tot_ndr,0) )                    as ndr_referral,\n                sum( nvl(mcrf.tot_authorization,0) )          as auth_referral,\n                sum( nvl(mcrf.tot_capture,0) )                as capture_referral,\n                sum( nvl(mcrf.tot_debit,0) )                  as debit_referral,\n                sum( nvl(mcrf.tot_ind_plans,0) )              as plan_referral,\n                sum( nvl(mcrf.tot_sys_generated,0) )          as sys_gen_referral,\n                sum( nvl(mcrf.tot_equip_rental,0) )           as equip_rental_referral,\n                sum( nvl(mcrf.tot_equip_sales,0) )            as equip_sales_referral,\n                sum( nvl(mcrf.tot_ach,0) )                    as ach_referral,\n                sum( nvl(mcrs.tot_ndr,0) )                    as ndr_reseller,\n                sum( nvl(mcrs.tot_authorization,0) )          as auth_reseller,\n                sum( nvl(mcrs.tot_capture,0) )                as capture_reseller,\n                sum( nvl(mcrs.tot_debit,0) )                  as debit_reseller,\n                sum( nvl(mcrs.tot_ind_plans,0) )              as plan_reseller,\n                sum( nvl(mcrs.tot_sys_generated,0) )          as sys_gen_reseller,\n                sum( nvl(mcrs.tot_equip_rental,0) )           as equip_rental_reseller,\n                sum( nvl(mcrs.tot_equip_sales,0) )            as equip_sales_reseller,\n                sum( nvl(mcex.tot_ndr,0) )                    as ndr_external,\n                sum( nvl(mcex.tot_authorization,0) )          as auth_external,\n                sum( nvl(mcex.tot_capture,0) )                as capture_external,\n                sum( nvl(mcex.tot_debit,0) )                  as debit_external,\n                sum( nvl(mcex.tot_ind_plans,0) )              as plan_external,\n                sum( nvl(mcex.tot_sys_generated,0) )          as sys_gen_external,\n                sum( nvl(mcex.tot_equip_rental,0) )           as equip_rental_external,\n                sum( nvl(mcex.tot_equip_sales,0) )            as equip_sales_external,\n                sum( nvl(mcgr.tot_ndr,0) )                    as ndr_grin,\n                sum( nvl(mcgr.tot_authorization,0) )          as auth_grin,\n                sum( nvl(mcgr.tot_capture,0) )                as capture_grin,\n                sum( nvl(mcgr.tot_debit,0) )                  as debit_grin,\n                sum( nvl(mcgr.tot_ind_plans,0) )              as plan_grin,\n                sum( nvl(mcgr.tot_sys_generated,0) )          as sys_gen_grin,\n                sum( nvl(mcgr.tot_equip_rental,0) )           as equip_rental_grin,\n                sum( nvl(mcgr.tot_equip_sales,0) )            as equip_sales_grin,\n                sum( nvl(mids.tot_partnership,0) )            as partnership\n              from      organization                o,\n                mif                         mf,\n                monthly_extract_contract    mcli,\n                monthly_extract_contract    mcrf,\n                monthly_extract_contract    mcrs,\n                monthly_extract_contract    mcex,\n                monthly_extract_contract    mcgr,\n                ( select *\n                  from monthly_extract_summary\n                  where assoc_hierarchy_node in\n                    (\n                       :3  ,decode( :4  ,3943,('3941' || substr( :5  ,5)),-1)\n                    )\n                    and\n                    active_date between  :6   and  :7  \n                ) mids\n              where mf.association_node =  :8   and\n                (  :9   != 7 or -- 7 or\n                  mf.activation_date > '28-NOV-2003' ) and\n                mids.merchant_number(+) = mf.merchant_number and\n                o.org_group = mf.merchant_number and\n                mcli.hh_load_sec(+) = mids.hh_load_sec and\n                mcli.merchant_number(+) = mids.merchant_number and\n                mcli.contract_type(+) =  :10   and -- 1 and\n                mcrf.hh_load_sec(+) = mids.hh_load_sec and\n                mcrf.merchant_number(+) = mids.merchant_number and\n                mcrf.contract_type(+) =  :11   and -- 2 and\n                mcrs.hh_load_sec(+) = mids.hh_load_sec and\n                mcrs.merchant_number(+) = mids.merchant_number and\n                mcrs.contract_type(+) =  :12   and -- 3 and\n                mcex.hh_load_sec(+) = mids.hh_load_sec and\n                mcex.merchant_number(+) = mids.merchant_number and\n                mcex.contract_type(+) =  :13   and -- 4 and\n                mcgr.hh_load_sec(+) = mids.hh_load_sec and\n                mcgr.merchant_number(+) = mids.merchant_number and\n                mcgr.contract_type(+) =  :14   -- 5\n              group by o.org_num, o.org_group, o.org_name,\n                decode(mids.CASH_ADVANCE_ACCOUNT,'Y',1,0),\n                decode(mids.liability_contract,'Y',1,0),\n                decode(mids.referral_contract,'Y',1,0),\n                decode(mids.reseller_contract,'Y',1,0),\n                decode(mids.cash_advance_referral,'Y',1,0),\n                decode(mids.moto_merchant,'Y',1,0),\n                decode(mids.mes_inventory,'Y',1,0),\n                mf.dmacctst\n              order by o.org_name, o.org_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,ReportDateBegin);
   __sJT_st.setInt(2,BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG);
   __sJT_st.setLong(3,nodeId);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setLong(5,nodeId);
   __sJT_st.setDate(6,ReportDateBegin);
   __sJT_st.setDate(7,ReportDateEnd);
   __sJT_st.setLong(8,nodeId);
   __sJT_st.setInt(9,ProfReportType);
   __sJT_st.setInt(10,ContractTypes.CONTRACT_SOURCE_LIABILITY);
   __sJT_st.setInt(11,ContractTypes.CONTRACT_SOURCE_REFERRAL);
   __sJT_st.setInt(12,ContractTypes.CONTRACT_SOURCE_RESELLER);
   __sJT_st.setInt(13,ContractTypes.CONTRACT_SOURCE_THIRD_PARTY);
   __sJT_st.setInt(14,ContractTypes.CONTRACT_SOURCE_GRIN);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3138^11*/
        } else {
          /*@lineinfo:generated-code*//*@lineinfo:3140^11*/

//  ************************************************************
//  #sql [Ctx] it = { select    /*+ index (sm idx_mon_ext_sum_ahn_ad) */
//                    o.org_num as org_num,
//                    o.org_group as hierarchy_node,
//                    o.org_name as org_name,
//                    count(sm.merchant_number)as merchant_count,
//                    decode(sm.CASH_ADVANCE_ACCOUNT, 'Y', 1, 0)as cash_advance,
//                    decode(sm.mes_inventory, 'Y', 1, 0)as mes_inventory,
//                    decode(sm.liability_contract, 'Y', 1, 0)as liability_contract,
//                    decode(sm.referral_contract, 'Y', 1, 0)as referral_contract,
//                    decode(sm.reseller_contract, 'Y', 1, 0)as reseller_contract,
//                    decode(sm.cash_advance_referral, 'Y', 1, 0)as cash_advance_referral,
//                    decode(sm.moto_merchant, 'Y', 1, 0)as moto_merchant,
//                    nvl(mf.dmacctst, '')as account_status,
//                    sum(sm.vmc_sales_count)as vmc_sales_count,
//                    sum(sm.vmc_sales_amount)as vmc_sales_amount,
//                    sum(sm.vmc_credits_count)as vmc_credits_count,
//                    sum(sm.vmc_credits_amount)as vmc_credits_amount,
//                    sum(sm.cash_advance_vol_count)as cash_advance_vol_count,
//                    sum(sm.cash_advance_vol_amount)as cash_advance_vol_amount,
//                    sum( sm.TOT_INC_DISCOUNT + 
//                         sm.tot_inc_interchange - 
//                         nvl(sm.tot_inc_discount_ach,0))          as disc_ic_inc,
//                    sum(sm.tot_inc_authorization)as auth_inc,
//                    sum(sm.tot_inc_capture)as capture_inc,
//                    sum(sm.tot_inc_debit)as debit_inc,
//                    sum(sm.tot_inc_ind_plans)as plan_inc,
//                    sum(sm.tot_inc_sys_generated -
//                            (sm.equip_sales_income +
//                                sm.equip_rental_income +
//                                nvl(sm.equip_sales_tax_collected, 0))
//                    )as sys_gen_inc,
//                    sum(nvl(sm.tot_inc_discount_ach,0))               as ach_disc_inc,  
//                    sum(nvl(sm.tot_inc_discount_ach,0) + 
//                         nvl(sm.tot_inc_ach,0))                       as ach_inc,
//                    sum(nvl(sm.tot_ach_disc_liability,0))             as ach_disc_liability,
//                    sum(nvl(sm.tot_ach_disc_referral,0))              as ach_disc_referral,   
//                    sum (nvl(sm.tot_exp_ach_vendor_discount,0))       as ps_disc_exp,
//                    sum (nvl(sm.tot_exp_ach_vendor,0))                as ps_exp,
//                    sum(nvl(sm.mes_only_inc_disc_ic, 0))as mes_only_disc_ic,
//                    sum(nvl(sm.mes_only_inc_authorization, 0))as mes_only_auth,
//                    sum(nvl(sm.mes_only_inc_capture, 0))as mes_only_capture,
//                    sum(nvl(sm.mes_only_inc_debit, 0))as mes_only_debit,
//                    sum(nvl(sm.mes_only_inc_ind_plans, 0))as mes_only_plan,
//                    sum(nvl(sm.mes_only_inc_sys_generated, 0))as mes_only_sys_gen,
//                    sum(sm.equip_sales_income)as equip_sales_inc,
//                    sum(sm.equip_rental_income)as equip_rental_inc,
//                    sum(nvl(sm.equip_sales_tax_collected, 0))as equip_sales_tax,
//                    sum(nvl(sm.interchange_expense, 0))as ic_exp,
//                    sum(nvl(sm.vmc_assessment_expense, 0))as assessment,
//                    sum(nvl(sm.vmc_fees, 0))as vmc_fees,
//                    sum(sm.tot_exp_authorization)as auth_exp,
//                    sum(sm.tot_exp_capture)as capture_exp,
//                    sum(sm.tot_exp_debit)as debit_exp,
//                    sum(sm.tot_exp_ind_plans)as plan_exp,
//                    sum(sm.tot_exp_sys_generated -
//                        (nvl(sm.equip_sales_expense, 0) +
//                            nvl(sm.equip_rental_expense, 0)))as sys_gen_exp,
//                    sum(nvl(sm.supply_cost, 0))as supply_cost,
//                    sum(nvl(sm.equip_sales_expense, 0))as equip_sales_exp,
//                    sum(nvl(sm.equip_rental_expense, 0))as equip_rental_exp,
//                    sum(nvl(sm.equip_rental_base_cost, 0))as equip_rental_base_cost,
//                    sum(nvl(sm.equip_sales_base_cost, 0))as equip_sales_base_cost,
//                    sum(nvl(sm.excluded_equip_rental_amount, 0))as excluded_equip_rental,
//                    sum(nvl(sm.excluded_equip_sales_amount, 0))as excluded_equip_sales,
//                    sum(nvl(sm.excluded_fees_amount, 0))as excluded_fees,
//                    sum(nvl(sm.equip_transfer_cost, 0))as equip_transfer_cost,
//                    sum(nvl(sm.DISC_IC_DCE_ADJ_AMOUNT, 0))as disc_ic_adj,
//                    sum(nvl(sm.fee_dce_adj_amount, 0))as fee_adj,
//                    sum(nvl(mcli.tot_ndr, 0))as ndr_liability,
//                    sum(nvl(mcli.tot_authorization, 0))as auth_liability,
//                    sum(nvl(mcli.tot_capture, 0))as capture_liability,                  
//                    case when exists ( select 1
//                                from t_hierarchy     th,
//                                  mif             m,
//                                  agent_bank_contract     abc
//                                where m.merchant_number = o.org_group and
//                                m.association_node = th.descendent and
//                                th.ancestor = abc.hierarchy_node and
//                                :ReportDateBegin between abc.valid_date_begin and abc.valid_date_end and
//                                billing_element_type = :BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG)  
//                        then sum( nvl(mcli.tot_debit,0) + nvl(sm.debit_network_fees, 0) )
//                        else sum( nvl(mcli.tot_debit,0) )
//                    end                                           as debit_liability,
//                    sum(nvl(mcli.tot_ind_plans, 0))as plan_liability,
//                    sum(nvl(mcli.tot_sys_generated, 0))as sys_gen_liability,
//                    sum(nvl(mcli.tot_equip_rental, 0))as equip_rental_liability,
//                    sum(nvl(mcli.tot_equip_sales, 0))as equip_sales_liability,
//                    sum(nvl(mcli.tot_ach,0) )  as ach_liability,
//                    sum(nvl(mcrf.tot_ndr, 0))as ndr_referral,
//                    sum(nvl(mcrf.tot_authorization, 0))as auth_referral,
//                    sum(nvl(mcrf.tot_capture, 0))as capture_referral,
//                    sum(nvl(mcrf.tot_debit, 0))as debit_referral,
//                    sum(nvl(mcrf.tot_ind_plans, 0))as plan_referral,
//                    sum(nvl(mcrf.tot_sys_generated, 0))as sys_gen_referral,
//                    sum(nvl(mcrf.tot_equip_rental, 0))as equip_rental_referral,
//                    sum(nvl(mcrf.tot_equip_sales, 0))as equip_sales_referral,
//                    sum(nvl(mcrf.tot_ach,0) )    as ach_referral,
//                    sum(nvl(mcrs.tot_ndr, 0))as ndr_reseller,
//                    sum(nvl(mcrs.tot_authorization, 0))as auth_reseller,
//                    sum(nvl(mcrs.tot_capture, 0))as capture_reseller,
//                    sum(nvl(mcrs.tot_debit, 0))as debit_reseller,
//                    sum(nvl(mcrs.tot_ind_plans, 0))as plan_reseller,
//                    sum(nvl(mcrs.tot_sys_generated, 0))as sys_gen_reseller,
//                    sum(nvl(mcrs.tot_equip_rental, 0))as equip_rental_reseller,
//                    sum(nvl(mcrs.tot_equip_sales, 0))as equip_sales_reseller,
//                    sum(nvl(mcex.tot_ndr, 0))as ndr_external,
//                    sum(nvl(mcex.tot_authorization, 0))as auth_external,
//                    sum(nvl(mcex.tot_capture, 0))as capture_external,
//                    sum(nvl(mcex.tot_debit, 0))as debit_external,
//                    sum(nvl(mcex.tot_ind_plans, 0))as plan_external,
//                    sum(nvl(mcex.tot_sys_generated, 0))as sys_gen_external,
//                    sum(nvl(mcex.tot_equip_rental, 0))as equip_rental_external,
//                    sum(nvl(mcex.tot_equip_sales, 0))as equip_sales_external,
//                    sum(nvl(mcgr.tot_ndr, 0))as ndr_grin,
//                    sum(nvl(mcgr.tot_authorization, 0))as auth_grin,
//                    sum(nvl(mcgr.tot_capture, 0))as capture_grin,
//                    sum(nvl(mcgr.tot_debit, 0))as debit_grin,
//                    sum(nvl(mcgr.tot_ind_plans, 0))as plan_grin,
//                    sum(nvl(mcgr.tot_sys_generated, 0))as sys_gen_grin,
//                    sum(nvl(mcgr.tot_equip_rental, 0))as equip_rental_grin,
//                    sum(nvl(mcgr.tot_equip_sales, 0))as equip_sales_grin,
//                    sum(nvl(sm.tot_partnership, 0))as partnership
//            from organization o,
//                    monthly_extract_summary sm,
//                    mif mf,
//                    monthly_extract_contract mcli,
//                    monthly_extract_contract mcrf,
//                    monthly_extract_contract mcrs,
//                    monthly_extract_contract mcex,
//                    monthly_extract_contract mcgr
//                    where sm.assoc_hierarchy_node in
//                    (
//            :nodeId, decode(:bankNumber, 3943, ('3941' || substr(:nodeId, 5)),-1)
//            )
//            and
//            sm.active_date between:ReportDateBegin and:ReportDateEnd and
//            o.org_group = sm.merchant_number and
//            mf.merchant_number = sm.merchant_number and
//                (:ProfReportType !=:MERCH_PROF_TYPE_TPS_GRIN or --7 or
//            mf.activation_date > '28-NOV-2003')and
//            mcli.hh_load_sec(+) = sm.hh_load_sec and
//            mcli.merchant_number(+) = sm.merchant_number and
//            mcli.contract_type(+) =:ContractTypes.CONTRACT_SOURCE_LIABILITY and-- 1 and
//            mcrf.hh_load_sec(+) = sm.hh_load_sec and
//            mcrf.merchant_number(+) = sm.merchant_number and
//            mcrf.contract_type(+) =:ContractTypes.CONTRACT_SOURCE_REFERRAL and-- 2 and
//            mcrs.hh_load_sec(+) = sm.hh_load_sec and
//            mcrs.merchant_number(+) = sm.merchant_number and
//            mcrs.contract_type(+) =:ContractTypes.CONTRACT_SOURCE_RESELLER and-- 3 and
//            mcex.hh_load_sec(+) = sm.hh_load_sec and
//            mcex.merchant_number(+) = sm.merchant_number and
//            mcex.contract_type(+) =:ContractTypes.CONTRACT_SOURCE_THIRD_PARTY and-- 4 and
//            mcgr.hh_load_sec(+) = sm.hh_load_sec and
//            mcgr.merchant_number(+) = sm.merchant_number and
//            mcgr.contract_type(+) =:ContractTypes.CONTRACT_SOURCE_GRIN-- 5
//            group by o.org_num, o.org_group, o.org_name,
//                decode(sm.CASH_ADVANCE_ACCOUNT, 'Y', 1, 0),
//                decode(sm.liability_contract, 'Y', 1, 0),
//                decode(sm.referral_contract, 'Y', 1, 0),
//                decode(sm.reseller_contract, 'Y', 1, 0),
//                decode(sm.cash_advance_referral, 'Y', 1, 0),
//                decode(sm.moto_merchant, 'Y', 1, 0),
//                decode(sm.mes_inventory, 'Y', 1, 0),
//                mf.dmacctst
//            order by o.org_name, o.org_num
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    /*+ index (sm idx_mon_ext_sum_ahn_ad) */\n                  o.org_num as org_num,\n                  o.org_group as hierarchy_node,\n                  o.org_name as org_name,\n                  count(sm.merchant_number)as merchant_count,\n                  decode(sm.CASH_ADVANCE_ACCOUNT, 'Y', 1, 0)as cash_advance,\n                  decode(sm.mes_inventory, 'Y', 1, 0)as mes_inventory,\n                  decode(sm.liability_contract, 'Y', 1, 0)as liability_contract,\n                  decode(sm.referral_contract, 'Y', 1, 0)as referral_contract,\n                  decode(sm.reseller_contract, 'Y', 1, 0)as reseller_contract,\n                  decode(sm.cash_advance_referral, 'Y', 1, 0)as cash_advance_referral,\n                  decode(sm.moto_merchant, 'Y', 1, 0)as moto_merchant,\n                  nvl(mf.dmacctst, '')as account_status,\n                  sum(sm.vmc_sales_count)as vmc_sales_count,\n                  sum(sm.vmc_sales_amount)as vmc_sales_amount,\n                  sum(sm.vmc_credits_count)as vmc_credits_count,\n                  sum(sm.vmc_credits_amount)as vmc_credits_amount,\n                  sum(sm.cash_advance_vol_count)as cash_advance_vol_count,\n                  sum(sm.cash_advance_vol_amount)as cash_advance_vol_amount,\n                  sum( sm.TOT_INC_DISCOUNT + \n                       sm.tot_inc_interchange - \n                       nvl(sm.tot_inc_discount_ach,0))          as disc_ic_inc,\n                  sum(sm.tot_inc_authorization)as auth_inc,\n                  sum(sm.tot_inc_capture)as capture_inc,\n                  sum(sm.tot_inc_debit)as debit_inc,\n                  sum(sm.tot_inc_ind_plans)as plan_inc,\n                  sum(sm.tot_inc_sys_generated -\n                          (sm.equip_sales_income +\n                              sm.equip_rental_income +\n                              nvl(sm.equip_sales_tax_collected, 0))\n                  )as sys_gen_inc,\n                  sum(nvl(sm.tot_inc_discount_ach,0))               as ach_disc_inc,  \n                  sum(nvl(sm.tot_inc_discount_ach,0) + \n                       nvl(sm.tot_inc_ach,0))                       as ach_inc,\n                  sum(nvl(sm.tot_ach_disc_liability,0))             as ach_disc_liability,\n                  sum(nvl(sm.tot_ach_disc_referral,0))              as ach_disc_referral,   \n                  sum (nvl(sm.tot_exp_ach_vendor_discount,0))       as ps_disc_exp,\n                  sum (nvl(sm.tot_exp_ach_vendor,0))                as ps_exp,\n                  sum(nvl(sm.mes_only_inc_disc_ic, 0))as mes_only_disc_ic,\n                  sum(nvl(sm.mes_only_inc_authorization, 0))as mes_only_auth,\n                  sum(nvl(sm.mes_only_inc_capture, 0))as mes_only_capture,\n                  sum(nvl(sm.mes_only_inc_debit, 0))as mes_only_debit,\n                  sum(nvl(sm.mes_only_inc_ind_plans, 0))as mes_only_plan,\n                  sum(nvl(sm.mes_only_inc_sys_generated, 0))as mes_only_sys_gen,\n                  sum(sm.equip_sales_income)as equip_sales_inc,\n                  sum(sm.equip_rental_income)as equip_rental_inc,\n                  sum(nvl(sm.equip_sales_tax_collected, 0))as equip_sales_tax,\n                  sum(nvl(sm.interchange_expense, 0))as ic_exp,\n                  sum(nvl(sm.vmc_assessment_expense, 0))as assessment,\n                  sum(nvl(sm.vmc_fees, 0))as vmc_fees,\n                  sum(sm.tot_exp_authorization)as auth_exp,\n                  sum(sm.tot_exp_capture)as capture_exp,\n                  sum(sm.tot_exp_debit)as debit_exp,\n                  sum(sm.tot_exp_ind_plans)as plan_exp,\n                  sum(sm.tot_exp_sys_generated -\n                      (nvl(sm.equip_sales_expense, 0) +\n                          nvl(sm.equip_rental_expense, 0)))as sys_gen_exp,\n                  sum(nvl(sm.supply_cost, 0))as supply_cost,\n                  sum(nvl(sm.equip_sales_expense, 0))as equip_sales_exp,\n                  sum(nvl(sm.equip_rental_expense, 0))as equip_rental_exp,\n                  sum(nvl(sm.equip_rental_base_cost, 0))as equip_rental_base_cost,\n                  sum(nvl(sm.equip_sales_base_cost, 0))as equip_sales_base_cost,\n                  sum(nvl(sm.excluded_equip_rental_amount, 0))as excluded_equip_rental,\n                  sum(nvl(sm.excluded_equip_sales_amount, 0))as excluded_equip_sales,\n                  sum(nvl(sm.excluded_fees_amount, 0))as excluded_fees,\n                  sum(nvl(sm.equip_transfer_cost, 0))as equip_transfer_cost,\n                  sum(nvl(sm.DISC_IC_DCE_ADJ_AMOUNT, 0))as disc_ic_adj,\n                  sum(nvl(sm.fee_dce_adj_amount, 0))as fee_adj,\n                  sum(nvl(mcli.tot_ndr, 0))as ndr_liability,\n                  sum(nvl(mcli.tot_authorization, 0))as auth_liability,\n                  sum(nvl(mcli.tot_capture, 0))as capture_liability,                  \n                  case when exists ( select 1\n                              from t_hierarchy     th,\n                                mif             m,\n                                agent_bank_contract     abc\n                              where m.merchant_number = o.org_group and\n                              m.association_node = th.descendent and\n                              th.ancestor = abc.hierarchy_node and\n                               :1   between abc.valid_date_begin and abc.valid_date_end and\n                              billing_element_type =  :2  )  \n                      then sum( nvl(mcli.tot_debit,0) + nvl(sm.debit_network_fees, 0) )\n                      else sum( nvl(mcli.tot_debit,0) )\n                  end                                           as debit_liability,\n                  sum(nvl(mcli.tot_ind_plans, 0))as plan_liability,\n                  sum(nvl(mcli.tot_sys_generated, 0))as sys_gen_liability,\n                  sum(nvl(mcli.tot_equip_rental, 0))as equip_rental_liability,\n                  sum(nvl(mcli.tot_equip_sales, 0))as equip_sales_liability,\n                  sum(nvl(mcli.tot_ach,0) )  as ach_liability,\n                  sum(nvl(mcrf.tot_ndr, 0))as ndr_referral,\n                  sum(nvl(mcrf.tot_authorization, 0))as auth_referral,\n                  sum(nvl(mcrf.tot_capture, 0))as capture_referral,\n                  sum(nvl(mcrf.tot_debit, 0))as debit_referral,\n                  sum(nvl(mcrf.tot_ind_plans, 0))as plan_referral,\n                  sum(nvl(mcrf.tot_sys_generated, 0))as sys_gen_referral,\n                  sum(nvl(mcrf.tot_equip_rental, 0))as equip_rental_referral,\n                  sum(nvl(mcrf.tot_equip_sales, 0))as equip_sales_referral,\n                  sum(nvl(mcrf.tot_ach,0) )    as ach_referral,\n                  sum(nvl(mcrs.tot_ndr, 0))as ndr_reseller,\n                  sum(nvl(mcrs.tot_authorization, 0))as auth_reseller,\n                  sum(nvl(mcrs.tot_capture, 0))as capture_reseller,\n                  sum(nvl(mcrs.tot_debit, 0))as debit_reseller,\n                  sum(nvl(mcrs.tot_ind_plans, 0))as plan_reseller,\n                  sum(nvl(mcrs.tot_sys_generated, 0))as sys_gen_reseller,\n                  sum(nvl(mcrs.tot_equip_rental, 0))as equip_rental_reseller,\n                  sum(nvl(mcrs.tot_equip_sales, 0))as equip_sales_reseller,\n                  sum(nvl(mcex.tot_ndr, 0))as ndr_external,\n                  sum(nvl(mcex.tot_authorization, 0))as auth_external,\n                  sum(nvl(mcex.tot_capture, 0))as capture_external,\n                  sum(nvl(mcex.tot_debit, 0))as debit_external,\n                  sum(nvl(mcex.tot_ind_plans, 0))as plan_external,\n                  sum(nvl(mcex.tot_sys_generated, 0))as sys_gen_external,\n                  sum(nvl(mcex.tot_equip_rental, 0))as equip_rental_external,\n                  sum(nvl(mcex.tot_equip_sales, 0))as equip_sales_external,\n                  sum(nvl(mcgr.tot_ndr, 0))as ndr_grin,\n                  sum(nvl(mcgr.tot_authorization, 0))as auth_grin,\n                  sum(nvl(mcgr.tot_capture, 0))as capture_grin,\n                  sum(nvl(mcgr.tot_debit, 0))as debit_grin,\n                  sum(nvl(mcgr.tot_ind_plans, 0))as plan_grin,\n                  sum(nvl(mcgr.tot_sys_generated, 0))as sys_gen_grin,\n                  sum(nvl(mcgr.tot_equip_rental, 0))as equip_rental_grin,\n                  sum(nvl(mcgr.tot_equip_sales, 0))as equip_sales_grin,\n                  sum(nvl(sm.tot_partnership, 0))as partnership\n          from organization o,\n                  monthly_extract_summary sm,\n                  mif mf,\n                  monthly_extract_contract mcli,\n                  monthly_extract_contract mcrf,\n                  monthly_extract_contract mcrs,\n                  monthly_extract_contract mcex,\n                  monthly_extract_contract mcgr\n                  where sm.assoc_hierarchy_node in\n                  (\n           :3  , decode( :4  , 3943, ('3941' || substr( :5  , 5)),-1)\n          )\n          and\n          sm.active_date between :6   and :7   and\n          o.org_group = sm.merchant_number and\n          mf.merchant_number = sm.merchant_number and\n              ( :8   != :9   or --7 or\n          mf.activation_date > '28-NOV-2003')and\n          mcli.hh_load_sec(+) = sm.hh_load_sec and\n          mcli.merchant_number(+) = sm.merchant_number and\n          mcli.contract_type(+) = :10   and-- 1 and\n          mcrf.hh_load_sec(+) = sm.hh_load_sec and\n          mcrf.merchant_number(+) = sm.merchant_number and\n          mcrf.contract_type(+) = :11   and-- 2 and\n          mcrs.hh_load_sec(+) = sm.hh_load_sec and\n          mcrs.merchant_number(+) = sm.merchant_number and\n          mcrs.contract_type(+) = :12   and-- 3 and\n          mcex.hh_load_sec(+) = sm.hh_load_sec and\n          mcex.merchant_number(+) = sm.merchant_number and\n          mcex.contract_type(+) = :13   and-- 4 and\n          mcgr.hh_load_sec(+) = sm.hh_load_sec and\n          mcgr.merchant_number(+) = sm.merchant_number and\n          mcgr.contract_type(+) = :14  -- 5\n          group by o.org_num, o.org_group, o.org_name,\n              decode(sm.CASH_ADVANCE_ACCOUNT, 'Y', 1, 0),\n              decode(sm.liability_contract, 'Y', 1, 0),\n              decode(sm.referral_contract, 'Y', 1, 0),\n              decode(sm.reseller_contract, 'Y', 1, 0),\n              decode(sm.cash_advance_referral, 'Y', 1, 0),\n              decode(sm.moto_merchant, 'Y', 1, 0),\n              decode(sm.mes_inventory, 'Y', 1, 0),\n              mf.dmacctst\n          order by o.org_name, o.org_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,ReportDateBegin);
   __sJT_st.setInt(2,BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG);
   __sJT_st.setLong(3,nodeId);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setLong(5,nodeId);
   __sJT_st.setDate(6,ReportDateBegin);
   __sJT_st.setDate(7,ReportDateEnd);
   __sJT_st.setInt(8,ProfReportType);
   __sJT_st.setInt(9,MERCH_PROF_TYPE_TPS_GRIN);
   __sJT_st.setInt(10,ContractTypes.CONTRACT_SOURCE_LIABILITY);
   __sJT_st.setInt(11,ContractTypes.CONTRACT_SOURCE_REFERRAL);
   __sJT_st.setInt(12,ContractTypes.CONTRACT_SOURCE_RESELLER);
   __sJT_st.setInt(13,ContractTypes.CONTRACT_SOURCE_THIRD_PARTY);
   __sJT_st.setInt(14,ContractTypes.CONTRACT_SOURCE_GRIN);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3307^11*/
        }
      }
      else      // group
      {
        /*@lineinfo:generated-code*//*@lineinfo:3312^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    /*+ index (sm idx_mon_ext_sum_ahn_ad) */
//                      o.org_num                                     as org_num,
//                      o.org_group                                   as hierarchy_node,
//                      o.org_name                                    as org_name,
//                      count(sm.merchant_number)                     as merchant_count,
//                      decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0)       as cash_advance,          
//                      decode(sm.mes_inventory,'Y',1,0)              as mes_inventory,
//                      decode(sm.liability_contract,'Y',1,0)         as liability_contract,
//                      decode(sm.referral_contract,'Y',1,0)          as referral_contract,                   
//                      decode(sm.reseller_contract,'Y',1,0)          as reseller_contract,                   
//                      decode(sm.cash_advance_referral,'Y',1,0)      as cash_advance_referral,                   
//                      decode(sm.moto_merchant,'Y',1,0)              as moto_merchant,
//                      sum( sm.vmc_sales_count )                     as vmc_sales_count,
//                      sum( sm.vmc_sales_amount )                    as vmc_sales_amount,
//                      sum( sm.vmc_credits_count )                   as vmc_credits_count,
//                      sum( sm.vmc_credits_amount )                  as vmc_credits_amount,
//                      sum( nvl(sm.amex_ic_vol_count,0) )            as am_ic_vol_count,
//                      sum( nvl(sm.amex_ic_vol_amount,0) )           as am_ic_vol_amount,  
//                      sum( nvl(sm.amex_ic_sales_count,0) )          as am_ic_sales_count,
//                      sum( nvl(sm.amex_ic_sales_amount,0) )         as am_ic_sales_amount,  
//                      sum( nvl(sm.amex_ic_credits_count,0) )        as am_ic_credits_count,
//                      sum( nvl(sm.amex_ic_credits_amount,0) )       as am_ic_credits_amount,  
//                      sum( nvl(sm.disc_ic_vol_count,0) )            as ds_ic_vol_count,
//                      sum( nvl(sm.disc_ic_vol_amount,0) )           as ds_ic_vol_amount,  
//                      sum( nvl(sm.disc_ic_sales_count,0) )          as ds_ic_sales_count,
//                      sum( nvl(sm.disc_ic_sales_amount,0) )         as ds_ic_sales_amount,  
//                      sum( nvl(sm.disc_ic_credits_count,0) )        as ds_ic_credits_count,
//                      sum( nvl(sm.disc_ic_credits_amount,0) )       as ds_ic_credits_amount,  
//                      sum( sm.cash_advance_vol_count )              as cash_advance_vol_count,
//                      sum( sm.cash_advance_vol_amount )             as cash_advance_vol_amount,  
//                      sum(sm.TOT_INC_DISCOUNT + 
//                          sm.tot_inc_interchange - 
//                          nvl(sm.tot_inc_discount_ach,0))           as disc_ic_inc,                    
//                      sum( sm.tot_inc_authorization )               as auth_inc,
//                      sum( sm.tot_inc_capture )                     as capture_inc,
//                      sum( sm.tot_inc_debit )                       as debit_inc,
//                      sum( sm.tot_inc_ind_plans )                   as plan_inc,
//                      sum( sm.tot_inc_sys_generated -
//                           (sm.equip_sales_income + 
//                            sm.equip_rental_income +
//                            nvl(sm.equip_sales_tax_collected,0)) 
//                         )                                          as sys_gen_inc,
//                      sum(nvl(sm.tot_inc_discount_ach,0))           as ach_disc_inc,  
//                      sum(nvl(sm.tot_inc_discount_ach,0) + 
//                          nvl(sm.tot_inc_ach,0))                    as ach_inc,
//                      sum(nvl(tot_ach_disc_liability,0))            as ach_disc_liability,
//                      sum(nvl(tot_ach_disc_referral,0))             as ach_disc_referral,   
//                      sum(nvl(sm.tot_exp_ach_vendor_discount,0))    as ps_disc_exp,
//                      sum(nvl(sm.tot_exp_ach_vendor,0))             as ps_exp,
//                      sum( nvl(sm.mes_only_inc_disc_ic,0) )         as mes_only_disc_ic,
//                      sum( nvl(sm.mes_only_inc_authorization,0) )   as mes_only_auth,
//                      sum( nvl(sm.mes_only_inc_capture,0) )         as mes_only_capture,
//                      sum( nvl(sm.mes_only_inc_debit,0) )           as mes_only_debit,
//                      sum( nvl(sm.mes_only_inc_ind_plans,0) )       as mes_only_plan,
//                      sum( nvl(sm.mes_only_inc_sys_generated,0) )   as mes_only_sys_gen,
//                      sum( sm.equip_sales_income )                  as equip_sales_inc,
//                      sum( sm.equip_rental_income )                 as equip_rental_inc,
//                      sum( nvl(sm.equip_sales_tax_collected,0) )    as equip_sales_tax,
//                      sum( nvl(sm.interchange_expense,0) )          as ic_exp,
//                      sum( nvl(sm.vmc_assessment_expense,0) )       as assessment,                             
//                      sum( nvl(sm.vmc_fees,0) )                     as vmc_fees,
//                      sum( sm.tot_exp_authorization )               as auth_exp,
//                      sum( sm.tot_exp_capture )                     as capture_exp,
//                      sum( sm.tot_exp_debit )                       as debit_exp,
//                      sum( sm.tot_exp_ind_plans )                   as plan_exp,
//                      sum( sm.tot_exp_sys_generated -
//                           (nvl(sm.equip_sales_expense,0) + 
//                            nvl(sm.equip_rental_expense,0)) )       as sys_gen_exp,
//                      sum( nvl(sm.supply_cost,0) )                  as supply_cost,                            
//                      sum( nvl(sm.equip_sales_expense,0) )          as equip_sales_exp,
//                      sum( nvl(sm.equip_rental_expense,0) )         as equip_rental_exp,
//                      sum( nvl(sm.equip_rental_base_cost,0) )       as equip_rental_base_cost,
//                      sum( nvl(sm.equip_sales_base_cost,0) )        as equip_sales_base_cost,
//                      sum( nvl(sm.excluded_equip_rental_amount,0) ) as excluded_equip_rental,
//                      sum( nvl(sm.excluded_equip_sales_amount,0) )  as excluded_equip_sales,
//                      sum( nvl(sm.excluded_fees_amount,0) )         as excluded_fees,
//                      sum( nvl(sm.equip_transfer_cost,0) )          as equip_transfer_cost,
//                      sum( nvl(sm.DISC_IC_DCE_ADJ_AMOUNT,0) )       as disc_ic_adj,
//                      sum( nvl(sm.fee_dce_adj_amount,0) )           as fee_adj,
//                      sum( nvl(mcli.tot_ndr,0) )                    as ndr_liability,   
//                      sum( nvl(mcli.tot_authorization,0) )          as auth_liability,
//                      sum( nvl(mcli.tot_capture,0) )                as capture_liability,
//                      sum( nvl(mcli.tot_debit,0) + nvl (mids_dnf.debit_network_fees, 0) ) 
//                                              as debit_liability,
//                      sum( nvl(mcli.tot_ind_plans,0) )              as plan_liability,
//                      sum( nvl(mcli.tot_sys_generated,0) )          as sys_gen_liability,
//                      sum( nvl(mcli.tot_equip_rental,0) )           as equip_rental_liability,
//                      sum( nvl(mcli.tot_equip_sales,0) )            as equip_sales_liability,
//                      sum( nvl(mcli.tot_ach,0) )                    as ach_liability,          
//                      sum( nvl(mcrf.tot_ndr,0) )                    as ndr_referral,   
//                      sum( nvl(mcrf.tot_authorization,0) )          as auth_referral,
//                      sum( nvl(mcrf.tot_capture,0) )                as capture_referral,
//                      sum( nvl(mcrf.tot_debit,0) )                  as debit_referral,
//                      sum( nvl(mcrf.tot_ind_plans,0) )              as plan_referral,
//                      sum( nvl(mcrf.tot_sys_generated,0) )          as sys_gen_referral,          
//                      sum( nvl(mcrf.tot_equip_rental,0) )           as equip_rental_referral,          
//                      sum( nvl(mcrf.tot_equip_sales,0) )            as equip_sales_referral,
//                      sum( nvl(mcrf.tot_ach,0) )                    as ach_referral,          
//                      sum( nvl(mcrs.tot_ndr,0) )                    as ndr_reseller,   
//                      sum( nvl(mcrs.tot_authorization,0) )          as auth_reseller,
//                      sum( nvl(mcrs.tot_capture,0) )                as capture_reseller,
//                      sum( nvl(mcrs.tot_debit,0) )                  as debit_reseller,
//                      sum( nvl(mcrs.tot_ind_plans,0) )              as plan_reseller,
//                      sum( nvl(mcrs.tot_sys_generated,0) )          as sys_gen_reseller,          
//                      sum( nvl(mcrs.tot_equip_rental,0) )           as equip_rental_reseller,          
//                      sum( nvl(mcrs.tot_equip_sales,0) )            as equip_sales_reseller,          
//                      sum( nvl(mcex.tot_ndr,0) )                    as ndr_external,   
//                      sum( nvl(mcex.tot_authorization,0) )          as auth_external,
//                      sum( nvl(mcex.tot_capture,0) )                as capture_external,
//                      sum( nvl(mcex.tot_debit,0) )                  as debit_external,
//                      sum( nvl(mcex.tot_ind_plans,0) )              as plan_external,
//                      sum( nvl(mcex.tot_sys_generated,0) )          as sys_gen_external,
//                      sum( nvl(mcex.tot_equip_rental,0) )           as equip_rental_external,
//                      sum( nvl(mcex.tot_equip_sales,0) )            as equip_sales_external,          
//                      sum( nvl(mcgr.tot_ndr,0) )                    as ndr_grin,
//                      sum( nvl(mcgr.tot_authorization,0) )          as auth_grin,
//                      sum( nvl(mcgr.tot_capture,0) )                as capture_grin,
//                      sum( nvl(mcgr.tot_debit,0) )                  as debit_grin,
//                      sum( nvl(mcgr.tot_ind_plans,0) )              as plan_grin,
//                      sum( nvl(mcgr.tot_sys_generated,0) )          as sys_gen_grin,
//                      sum( nvl(mcgr.tot_equip_rental,0) )           as equip_rental_grin,
//                      sum( nvl(mcgr.tot_equip_sales,0) )            as equip_sales_grin,
//                      sum( nvl(sm.tot_partnership,0) )              as partnership
//            from      t_hierarchy                 th,
//                      t_hierarchy                 thc,
//                      organization                o,
//                      monthly_extract_summary     sm,
//                      mif                         mf,
//                      monthly_extract_contract    mcli,
//                      monthly_extract_contract    mcrf,
//                      monthly_extract_contract    mcrs,
//                      monthly_extract_contract    mcex,
//                      monthly_extract_contract    mcgr,
//                      (select sm.debit_network_fees, sm.merchant_number 
//            from t_hierarchy th, 
//                t_hierarchy thc, 
//                monthly_extract_summary sm, 
//                t_hierarchy mth, mif m, 
//                agent_bank_contract abc  
//            where th.ancestor = :nodeId 
//                and th.hier_type = 1 
//                and th.relation = 1 
//                and thc.hier_type = 1 
//                and thc.ancestor = th.descendent 
//                and thc.entity_type = 4 
//                and sm.assoc_hierarchy_node in (thc.descendent, decode(:bankNumber,3943,('3941' || substr(thc.descendent,5)),-1) ) 
//                and sm.active_date between :ReportDateBegin and :ReportDateEnd 
//                and m.merchant_number = sm.merchant_number 
//                and m.association_node = mth.descendent 
//                and mth.ancestor = abc.hierarchy_node 
//                and :ReportDateBegin between abc.valid_date_begin and abc.valid_date_end 
//                and billing_element_type = :BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG 
//                and sm.debit_network_fees > 0
//            group by sm.debit_network_fees, sm.merchant_number) mids_dnf
//            where     th.hier_type = 1 and
//                      th.ancestor = :nodeId and
//                      th.relation = 1 and  
//                      thc.hier_type = 1 and
//                      thc.ancestor = th.descendent and
//                      thc.entity_type = 4 and
//                      sm.assoc_hierarchy_node in 
//                      (
//                        thc.descendent,
//                        decode(:bankNumber,3943,('3941' || substr(thc.descendent,5)),-1)
//                      ) and
//                      sm.active_date between :ReportDateBegin and :ReportDateEnd and
//                      mf.merchant_number = sm.merchant_number and
//                      ( :ProfReportType != :MERCH_PROF_TYPE_TPS_GRIN or -- 7 or
//                        mf.activation_date > '28-NOV-2003' ) and
//                      mcli.hh_load_sec(+) = sm.hh_load_sec and
//                      mcli.merchant_number(+) = sm.merchant_number and
//                      mcli.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_LIABILITY and -- 1 and
//                      mcrf.hh_load_sec(+) = sm.hh_load_sec and
//                      mcrf.merchant_number(+) = sm.merchant_number and
//                      mcrf.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_REFERRAL and -- 2 and
//                      mcrs.hh_load_sec(+) = sm.hh_load_sec and
//                      mcrs.merchant_number(+) = sm.merchant_number and
//                      mcrs.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_RESELLER and -- 3 and
//                      mcex.hh_load_sec(+) = sm.hh_load_sec and
//                      mcex.merchant_number(+) = sm.merchant_number and
//                      mcex.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_THIRD_PARTY and -- 4 and
//                      mcgr.hh_load_sec(+) = sm.hh_load_sec and
//                      mcgr.merchant_number(+) = sm.merchant_number and
//                      mcgr.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_GRIN and -- 5 and
//                      o.org_group = thc.ancestor and
//                      sm.merchant_number = mids_dnf.merchant_number (+) 
//            group by o.org_num, o.org_group, o.org_name,
//                     decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0),
//                     decode(sm.liability_contract,'Y',1,0),
//                     decode(sm.referral_contract,'Y',1,0),
//                     decode(sm.reseller_contract,'Y',1,0),
//                     decode(sm.cash_advance_referral,'Y',1,0),
//                     decode(sm.moto_merchant,'Y',1,0),
//                     decode(sm.mes_inventory,'Y',1,0)
//            order by o.org_name, o.org_num        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = 
		   "  SELECT /*+ index (sm idx_mon_ext_sum_ahn_ad) */ " +
		   "    o.org_num                                       AS org_num, " +
		   "    o.org_group                                     AS hierarchy_node, " +
		   "    o.org_name                                      AS org_name, " +
		   "    COUNT( sm.merchant_number)                      AS merchant_count, " +
		   "    DECODE( sm.CASH_ADVANCE_ACCOUNT,'Y',1,0)        AS cash_advance, " +
		   "    DECODE( sm.mes_inventory,'Y',1,0)               AS mes_inventory, " +
		   "    DECODE( sm.liability_contract,'Y',1,0)          AS liability_contract, " +
		   "    DECODE( sm.referral_contract,'Y',1,0)           AS referral_contract, " +
		   "    DECODE( sm.reseller_contract,'Y',1,0)           AS reseller_contract, " +
		   "    DECODE( sm.cash_advance_referral,'Y',1,0)       AS cash_advance_referral, " +
		   "    DECODE( sm.moto_merchant,'Y',1,0)               AS moto_merchant, " +
		   "    SUM( sm.vmc_sales_count )                       AS vmc_sales_count, " +
		   "    SUM( sm.vmc_sales_amount )                      AS vmc_sales_amount, " +
		   "    SUM( sm.vmc_credits_count )                     AS vmc_credits_count, " +
		   "    SUM( sm.vmc_credits_amount )                    AS vmc_credits_amount, " +
		   "    SUM( NVL(sm.amex_ic_vol_count,0))               AS am_ic_vol_count, " +
		   "    SUM( NVL(sm.amex_ic_vol_amount,0))              AS am_ic_vol_amount, " +
		   "    SUM( NVL(sm.amex_ic_sales_count,0))             AS am_ic_sales_count, " +
		   "    SUM( NVL(sm.amex_ic_sales_amount,0) )           AS am_ic_sales_amount, " +
		   "    SUM( NVL(sm.amex_ic_credits_count,0) )          AS am_ic_credits_count, " +
		   "    SUM( NVL(sm.amex_ic_credits_amount,0) )         AS am_ic_credits_amount, " +
		   "    SUM( NVL(sm.disc_ic_vol_count,0) )              AS ds_ic_vol_count, " +
		   "    SUM( NVL(sm.disc_ic_vol_amount,0) )             AS ds_ic_vol_amount, " +
		   "    SUM( NVL(sm.disc_ic_sales_count,0) )            AS ds_ic_sales_count, " +
		   "    SUM( NVL(sm.disc_ic_sales_amount,0) )           AS ds_ic_sales_amount, " +
		   "    SUM( NVL(sm.disc_ic_credits_count,0) )          AS ds_ic_credits_count, " +
		   "    SUM( NVL(sm.disc_ic_credits_amount,0) )         AS ds_ic_credits_amount, " +
		   "    SUM( sm.cash_advance_vol_count )                AS cash_advance_vol_count, " +
		   "    SUM( sm.cash_advance_vol_amount )               AS cash_advance_vol_amount, " +
		   "    SUM( sm.TOT_INC_DISCOUNT + sm.tot_inc_interchange "+ 
		   "         - NVL(sm.tot_inc_discount_ach,0))          AS disc_ic_inc, " +
		   "    SUM( sm.tot_inc_authorization )                 AS auth_inc, " +
		   "    SUM( sm.tot_inc_capture )                       AS capture_inc, " +
		   "    SUM( sm.tot_inc_debit )                         AS debit_inc, " +
		   "    SUM( sm.tot_inc_ind_plans )                     AS plan_inc, " +
		   "    SUM( sm.tot_inc_sys_generated"+ 
		   "         - (sm.equip_sales_income "+
		   "         + sm.equip_rental_income"+
		   "         + NVL(sm.equip_sales_tax_collected,0)) )   AS sys_gen_inc, " +
		   "    SUM( NVL(sm.tot_inc_discount_ach,0))            AS ach_disc_inc, " +
		   "    SUM( NVL(sm.tot_inc_discount_ach,0)"+
		   "         + NVL(sm.tot_inc_ach,0))                   AS ach_inc, " +
		   "    SUM( NVL(tot_ach_disc_liability,0))             AS ach_disc_liability, " +
		   "    SUM( NVL(tot_ach_disc_referral,0))              AS ach_disc_referral, " +
		   "    SUM( NVL(sm.tot_exp_ach_vendor_discount,0))     AS ps_disc_exp, " +
		   "    SUM( NVL(sm.tot_exp_ach_vendor,0))              AS ps_exp, " +
		   "    SUM( NVL(sm.mes_only_inc_disc_ic,0) )           AS mes_only_disc_ic, " +
		   "    SUM( NVL(sm.mes_only_inc_authorization,0) )     AS mes_only_auth, " +
		   "    SUM( NVL(sm.mes_only_inc_capture,0) )           AS mes_only_capture, " +
		   "    SUM( NVL(sm.mes_only_inc_debit,0) )             AS mes_only_debit, " +
		   "    SUM( NVL(sm.mes_only_inc_ind_plans,0) )         AS mes_only_plan, " +
		   "    SUM( NVL(sm.mes_only_inc_sys_generated,0) )     AS mes_only_sys_gen, " +
		   "    SUM( sm.equip_sales_income )                    AS equip_sales_inc, " +
		   "    SUM( sm.equip_rental_income )                   AS equip_rental_inc, " +
		   "    SUM( NVL(sm.equip_sales_tax_collected,0) )      AS equip_sales_tax, " +
		   "    SUM( NVL(sm.interchange_expense,0) )            AS ic_exp, " +
		   "    SUM( NVL(sm.vmc_assessment_expense,0) )         AS assessment, " +
		   "    SUM( NVL(sm.vmc_fees,0) )                       AS vmc_fees, " +
		   "    SUM( sm.tot_exp_authorization )                 AS auth_exp, " +
		   "    SUM( sm.tot_exp_capture )                       AS capture_exp, " +
		   "    SUM( sm.tot_exp_debit )                         AS debit_exp, " +
		   "    SUM( sm.tot_exp_ind_plans )                     AS plan_exp, " +
		   "    SUM( sm.tot_exp_sys_generated "+
		   "         - (NVL(sm.equip_sales_expense,0) "+
		   "         + NVL( sm.equip_rental_expense,0)) )       AS sys_gen_exp, " +
		   "    SUM( NVL(sm.supply_cost,0) )                    AS supply_cost, " +
		   "    SUM( NVL(sm.equip_sales_expense,0) )            AS equip_sales_exp, " +
		   "    SUM( NVL(sm.equip_rental_expense,0) )           AS equip_rental_exp, " +
		   "    SUM( NVL(sm.equip_rental_base_cost,0) )         AS equip_rental_base_cost, " +
		   "    SUM( NVL(sm.equip_sales_base_cost,0) )          AS equip_sales_base_cost, " +
		   "    SUM( NVL(sm.excluded_equip_rental_amount,0) )   AS excluded_equip_rental, " +
		   "    SUM( NVL(sm.excluded_equip_sales_amount,0) )    AS excluded_equip_sales, " +
		   "    SUM( NVL(sm.excluded_fees_amount,0) )           AS excluded_fees, " +
		   "    SUM( NVL(sm.equip_transfer_cost,0) )            AS equip_transfer_cost, " +
		   "    SUM( NVL(sm.DISC_IC_DCE_ADJ_AMOUNT,0) )         AS disc_ic_adj, " +
		   "    SUM( NVL(sm.fee_dce_adj_amount,0) )             AS fee_adj, " +
		   "    SUM( NVL(mcli.tot_ndr,0) )                      AS ndr_liability, " +
		   "    SUM( NVL(mcli.tot_authorization,0) )            AS auth_liability, " +
		   "    SUM( NVL(mcli.tot_capture,0) )                  AS capture_liability, " +
		   "    SUM( NVL(mcli.tot_debit,0)"+
		   "         + NVL (mids_dnf.debit_network_fees, 0) )   AS debit_liability, " +
		   "    SUM( NVL(mcli.tot_ind_plans,0) )                AS plan_liability, " +
		   "    SUM( NVL(mcli.tot_sys_generated,0) )            AS sys_gen_liability, " +
		   "    SUM( NVL(mcli.tot_equip_rental,0) )             AS equip_rental_liability, " +
		   "    SUM( NVL(mcli.tot_equip_sales,0) )              AS equip_sales_liability, " +
		   "    SUM( NVL(mcli.tot_ach,0) )                      AS ach_liability, " +
		   "    SUM( NVL(mcrf.tot_ndr,0) )                      AS ndr_referral, " +
		   "    SUM( NVL(mcrf.tot_authorization,0) )            AS auth_referral, " +
		   "    SUM( NVL(mcrf.tot_capture,0) )                  AS capture_referral, " +
		   "    SUM( NVL(mcrf.tot_debit,0) )                    AS debit_referral, " +
		   "    SUM( NVL(mcrf.tot_ind_plans,0) )                AS plan_referral, " +
		   "    SUM( NVL(mcrf.tot_sys_generated,0) )            AS sys_gen_referral, " +
		   "    SUM( NVL(mcrf.tot_equip_rental,0) )             AS equip_rental_referral, " +
		   "    SUM( NVL(mcrf.tot_equip_sales,0) )              AS equip_sales_referral, " +
		   "    SUM( NVL(mcrf.tot_ach,0) )                      AS ach_referral, " +
		   "    SUM( NVL(mcrs.tot_ndr,0) )                      AS ndr_reseller, " +
		   "    SUM( NVL(mcrs.tot_authorization,0) )            AS auth_reseller, " +
		   "    SUM( NVL(mcrs.tot_capture,0) )                  AS capture_reseller, " +
		   "    SUM( NVL(mcrs.tot_debit,0) )                    AS debit_reseller, " +
		   "    SUM( NVL(mcrs.tot_ind_plans,0) )                AS plan_reseller, " +
		   "    SUM( NVL(mcrs.tot_sys_generated,0) )            AS sys_gen_reseller, " +
		   "    SUM( NVL(mcrs.tot_equip_rental,0) )             AS equip_rental_reseller, " +
		   "    SUM( NVL(mcrs.tot_equip_sales,0) )              AS equip_sales_reseller, " +
		   "    SUM( NVL(mcex.tot_ndr,0) )                      AS ndr_external, " +
		   "    SUM( NVL(mcex.tot_authorization,0) )            AS auth_external, " +
		   "    SUM( NVL(mcex.tot_capture,0) )                  AS capture_external, " +
		   "    SUM( NVL(mcex.tot_debit,0) )                    AS debit_external, " +
		   "    SUM( NVL(mcex.tot_ind_plans,0) )                AS plan_external, " +
		   "    SUM( NVL(mcex.tot_sys_generated,0) )            AS sys_gen_external, " +
		   "    SUM( NVL(mcex.tot_equip_rental,0) )             AS equip_rental_external, " +
		   "    SUM( NVL(mcex.tot_equip_sales,0) )              AS equip_sales_external, " +
		   "    SUM( NVL(mcgr.tot_ndr,0) )                      AS ndr_grin, " +
		   "    SUM( NVL(mcgr.tot_authorization,0) )            AS auth_grin, " +
		   "    SUM( NVL(mcgr.tot_capture,0) )                  AS capture_grin, " +
		   "    SUM( NVL(mcgr.tot_debit,0) )                    AS debit_grin, " +
		   "    SUM( NVL(mcgr.tot_ind_plans,0) )                AS plan_grin, " +
		   "    SUM( NVL(mcgr.tot_sys_generated,0) )            AS sys_gen_grin, " +
		   "    SUM( NVL(mcgr.tot_equip_rental,0) )             AS equip_rental_grin, " +
		   "    SUM( NVL(mcgr.tot_equip_sales,0) )              AS equip_sales_grin, " +
		   "    SUM( NVL(sm.TOT_INC_PINLESSDEBIT,0) )           AS lcr_income, " +
		   "    SUM( NVL(sm.TOT_EXP_PINLESSDEBIT,0) )           AS lcr_expense, " +
		   "    SUM( NVL(sm.tot_partnership,0) )                AS partnership " +
		   "  FROM " +
		   "    t_hierarchy th, " +
		   "    t_hierarchy thc, " +
		   "    organization o, " +
		   "    monthly_extract_summary sm, " +
		   "    mif mf, " +
		   "    monthly_extract_contract mcli, " +
		   "    monthly_extract_contract mcrf, " +
		   "    monthly_extract_contract mcrs, " +
		   "    monthly_extract_contract mcex, " +
		   "    monthly_extract_contract mcgr, " +
		   "    ("+
		   "      SELECT " +
		   "       sm.debit_network_fees, " +
		   "       sm.merchant_number, " +
		   "       sm.active_date " +
		   "      FROM " +
		   "       t_hierarchy th, " +
		   "       t_hierarchy thc, " +
		   "       monthly_extract_summary sm, " +
		   "       t_hierarchy mth, " +
		   "       mif m, " +
		   "       agent_bank_contract abc " +
		   "      WHERE " +
		   "       th.ancestor                = :1 " +
		   "       AND th.hier_type             = 1 " +
		   "       AND th.relation              = 1 " +
		   "       AND thc.hier_type            = 1 " +
		   "       AND thc.ancestor             = th.descendent " +
		   "       AND thc.entity_type          = 4 " +
		   "       AND sm.assoc_hierarchy_node IN (thc.descendent, DECODE( :2 ,3943,(  '3941' " +
		   "       || SUBSTR(thc.descendent,5)),-1) ) " +
		   "       AND sm.active_date BETWEEN :3 AND :4 " +
		   "       AND m.merchant_number  = sm.merchant_number " +
		   "       AND m.association_node = mth.descendent " +
		   "       AND mth.ancestor       = abc.hierarchy_node " +
		   "       AND :5 BETWEEN abc.valid_date_begin AND abc.valid_date_end " +
		   "       AND billing_element_type  = :6 " +
		   "       AND sm.debit_network_fees > 0 " +
		   "      GROUP BY " +
		   "       sm.debit_network_fees, " +
		   "       sm.merchant_number, " +
		   "       sm.active_date " +
		   "     ) mids_dnf " +
		   "  WHERE " +
		   "    th.hier_type                 = 1 " +
		   "    AND th.ancestor              = :7 " +
		   "    AND th.relation              = 1 " +
		   "    AND thc.hier_type            = 1 " +
		   "    AND thc.ancestor             = th.descendent " +
		   "    AND thc.entity_type          = 4 " +
		   "    AND sm.assoc_hierarchy_node   IN ( thc.descendent, DECODE( :8 ,3943,( '3941' " +
		   "    || SUBSTR(thc.descendent,5)),-1) ) " +
		   "    AND sm.active_date BETWEEN :9 AND :10 " +
		   "    AND mf.merchant_number = sm.merchant_number " +
		   "    AND mcli.hh_load_sec(+)     = sm.hh_load_sec " +
		   "    AND mcli.merchant_number(+) = sm.merchant_number " +
		   "    AND mcli.contract_type(+)   = :11 " +
		   "    AND mcrf.hh_load_sec(+)     = sm.hh_load_sec " +
		   "    AND mcrf.merchant_number(+) = sm.merchant_number " +
		   "    AND mcrf.contract_type(+)   = :12 " +
		   "    AND mcrs.hh_load_sec(+)     = sm.hh_load_sec " +
		   "    AND mcrs.merchant_number(+) = sm.merchant_number " +
		   "    AND mcrs.contract_type(+)   = :13 " +
		   "    AND mcex.hh_load_sec(+)     = sm.hh_load_sec " +
		   "    AND mcex.merchant_number(+) = sm.merchant_number " +
		   "    AND mcex.contract_type(+)   = :14 " +
		   "    AND mcgr.hh_load_sec(+)     = sm.hh_load_sec " +
		   "    AND mcgr.merchant_number(+) = sm.merchant_number " +
		   "    AND mcgr.contract_type(+)   = :15 " +
		   "    AND o.org_group             = thc.ancestor " +
		   "    AND sm.merchant_number      = mids_dnf.merchant_number (+) " +
		   "    AND sm.active_date          = mids_dnf.active_date(+) " +
		   "  GROUP BY " +
		   "    o.org_num, " +
		   "    o.org_group, " +
		   "    o.org_name, " +
		   "    DECODE(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0), " +
		   "    DECODE(sm.liability_contract,'Y',1,0), " +
		   "    DECODE( sm.referral_contract,'Y',1,0), " +
		   "    DECODE( sm.reseller_contract,'Y',1,0), " +
		   "    DECODE( sm.cash_advance_referral,'Y',1,0), " +
		   "    DECODE( sm.moto_merchant,'Y',1,0), " +
		   "    DECODE(sm.mes_inventory,'Y' ,1,0) " +
		   "  ORDER BY " +
		   "    o.org_name, " +
		   "    o.org_num";
		   
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setDate(3,ReportDateBegin);
   __sJT_st.setDate(4,ReportDateEnd);
   __sJT_st.setDate(5,ReportDateBegin);
   __sJT_st.setInt(6,BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG);
   __sJT_st.setLong(7,nodeId);
   __sJT_st.setInt(8,bankNumber);
   __sJT_st.setDate(9,ReportDateBegin);
   __sJT_st.setDate(10,ReportDateEnd);
   __sJT_st.setInt(11,ContractTypes.CONTRACT_SOURCE_LIABILITY);
   __sJT_st.setInt(12,ContractTypes.CONTRACT_SOURCE_REFERRAL);
   __sJT_st.setInt(13,ContractTypes.CONTRACT_SOURCE_RESELLER);
   __sJT_st.setInt(14,ContractTypes.CONTRACT_SOURCE_THIRD_PARTY);
   __sJT_st.setInt(15,ContractTypes.CONTRACT_SOURCE_GRIN);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3509^9*/
      }
    
      // it will be null when the node is a merchant
      // and the report is requesting category details
      if ( it != null )
      {
        resultSet = it.getResultSet();
    
        while( resultSet.next() )
        {
          orgId = resultSet.getLong("org_num");
      
          if ( orgId != lastOrgId )
          {
            row = new SummaryData( resultSet, userType );
            ReportRows.add(row);
            lastOrgId = orgId;
          }
          row.addData( resultSet );
        }
      }        
    }
    catch( Exception e )
    {
      logEntry( "loadData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  public void loadDetailData( long merchantId )
  {
    try
    {
      // make sure there is an entry even if there is no data for this merchant
      ReportRows.add( new ChildProfitabilitySummary( Ctx, getReportOrgId(), getReportOrgName(), getReportDateBegin(), getReportDateEnd() ) );
    
      // if the user is either a bank user or an agent bank user
      // then load the merchant inc/exp data if the contract settings
      // match the user type 
      if ( isDirectMerchant( merchantId ) )
      {
        if ( !isMesOnlyRevenue( merchantId, getReportBetGroup() ) )
        {
          loadDetailDataProcessor( merchantId );
        }
      }
      else    // not a direct merchant account, still need the MES equipment
      {
        loadDetailMesEquip( merchantId );
        
        // if this is MES only revenue, then load the detail for MES
        if ( isMesOnlyRevenue( merchantId, getReportBetGroup() ) )
        {
          loadDetailDataProcessor( merchantId );
        }
      }
      
      // load the MES contract data
      loadContractData( merchantId );
      
      // load the charge records that are 100% to MES
      loadMesOnlySysFees( merchantId );
    }      
    catch( Exception e )
    {
      logEntry("loadDetailData()",e.toString());
    }
  }
  
  protected void loadDetailDataProcessor( long merchantId )
  {
    Class       c         = null;
    Method      m         = null;
    Object      o         = null;
        
    try
    {
      // assumes that Report
      switch( getReportBankId() )
      {
        case mesConstants.BANK_ID_CERTEGY:
          c = Class.forName("com.mes.reports.ProfCertegyDataBean");
          break;
        
        default:      // default is vital
          c = Class.forName("com.mes.reports.ProfVitalDataBean");
          break;
      }
      
      // create an instance of the class
      o = c.newInstance();
      
      // initialize the new instance of the object with a reference
      // to this contract data bean.  The merchant direct data bean
      // will call methods in this class to populate the contract 
      // element vectors.
      m = c.getMethod( "loadData", new Class[]{ BankContractDataBean.class } );
      m.invoke(o, new Object[] { this } );
    }      
    catch( Exception e )
    {
      logEntry("loadDetailDataProcessor()",e.toString());
    }
  }
  
  protected void loadDetailMesEquip( long merchantId )
  {
    Class       c         = null;
    Method      m         = null;
    Object      o         = null;
        
    try
    {
      // assumes that Report
      switch( getReportBankId() )
      {
        case mesConstants.BANK_ID_CERTEGY:    // not supported
          break;
        
        default:      // default is vital
          c = Class.forName("com.mes.reports.ProfVitalDataBean");
          break;
      }
      
      if ( c != null )
      {
        // create an instance of the class
        o = c.newInstance();
      
        // initialize the new instance of the object with a reference
        // to this contract data bean.  The merchant direct data bean
        // will call methods in this class to populate the contract 
        // element vectors.
        m = c.getMethod( "loadDetailMesEquipIncome", new Class[]{ BankContractDataBean.class } );
        m.invoke(o, new Object[] { this } );
      }        
    }      
    catch( Exception e )
    {
      logEntry("loadDetailDataProcessor()",e.toString());
    }
  }
  
  public void loadNetRevenueRetentionData( )
  {
    Date                        activeDate        = null;
    boolean                     agentBankUser     = isUserUnderContract();
    ResultSetIterator           it                = null;
    Date                        lastActiveDate    = null;
    long                        lastRelId         = -1;
    long                        nodeId            = getReportHierarchyNode();
    long                        relId             = 0L;
    ResultSet                   resultSet         = null;
    SummaryData                 row               = null;
    int                         userType          = getUserType();
    
    try
    {
      ReportRows.removeAllElements();

      if ( SummaryType == ST_PARENT )
      {
        /*@lineinfo:generated-code*//*@lineinfo:3675^9*/

//  ************************************************************
//  #sql [Ctx] it = { select   0                                             as org_num,
//                      rt.relationship_type                          as hierarchy_node,
//                      rt.relationship_desc                          as org_name,
//                      sm.active_date                                as active_date,
//                      -- dates are offset (to TSYS) by one day to allow
//                      -- billing queries to get all items (i.e. service calls)
//                      -- adjust the dates to the TSYS billing dates
//                      min(nvl(sm.month_begin_date+1,sm.active_date))as month_begin_date,
//                      max(sm.month_end_date+1)                      as month_end_date,                  
//                      count(sm.merchant_number)                     as merchant_count,
//                      sum(decode( sm.activated_merchant,'Y',1,0 ))  as activated_count,
//                                   
//                      sum( decode( ( nvl(sm.vmc_sales_amount,0) +
//                                     nvl(sm.cash_advance_vol_amount,0) -
//                                     least( (nvl(sm.vmc_sales_amount,0) + 
//                                             nvl(sm.cash_advance_vol_amount,0)),
//                                            9.99
//                                           ) 
//                                    ), 
//                                   0, 0, 1 ) )                      as active_count,
//                      decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0)       as cash_advance,          
//                      decode(sm.mes_inventory,'Y',1,0)              as mes_inventory,
//                      decode(sm.liability_contract,'Y',1,0)         as liability_contract,
//                      decode(sm.referral_contract,'Y',1,0)          as referral_contract,                   
//                      decode(sm.reseller_contract,'Y',1,0)          as reseller_contract,                   
//                      decode(sm.cash_advance_referral,'Y',1,0)      as cash_advance_referral,                   
//                      decode(sm.moto_merchant,'Y',1,0)              as moto_merchant,
//                      sum( sm.vmc_sales_count )                     as vmc_sales_count,
//                      sum( sm.vmc_sales_amount )                    as vmc_sales_amount,
//                      sum( sm.vmc_credits_count )                   as vmc_credits_count,
//                      sum( sm.vmc_credits_amount )                  as vmc_credits_amount,
//                      sum( sm.cash_advance_vol_count )              as cash_advance_vol_count,
//                      sum( sm.cash_advance_vol_amount )             as cash_advance_vol_amount,  
//                      sum( sm.TOT_INC_DISCOUNT + 
//                           sm.tot_inc_interchange )                 as disc_ic_inc,
//                      sum( sm.tot_inc_authorization )               as auth_inc,
//                      sum( sm.tot_inc_capture )                     as capture_inc,
//                      sum( sm.tot_inc_debit )                       as debit_inc,
//                      sum( sm.tot_inc_ind_plans )                   as plan_inc,
//                      sum( sm.tot_inc_sys_generated -
//                           (sm.equip_sales_income + 
//                            sm.equip_rental_income +
//                            nvl(sm.equip_sales_tax_collected,0)) 
//                         )                                          as sys_gen_inc,
//                      sum( nvl(sm.mes_only_inc_disc_ic,0) )         as mes_only_disc_ic,
//                      sum( nvl(sm.mes_only_inc_authorization,0) )   as mes_only_auth,
//                      sum( nvl(sm.mes_only_inc_capture,0) )         as mes_only_capture,
//                      sum( nvl(sm.mes_only_inc_debit,0) )           as mes_only_debit,
//                      sum( nvl(sm.mes_only_inc_ind_plans,0) )       as mes_only_plan,
//                      sum( nvl(sm.mes_only_inc_sys_generated,0) )   as mes_only_sys_gen,
//                      sum( sm.equip_sales_income )                  as equip_sales_inc,
//                      sum( sm.equip_rental_income )                 as equip_rental_inc,
//                      sum( nvl(sm.equip_sales_tax_collected,0) )    as equip_sales_tax,
//                      sum( nvl(sm.interchange_expense,0) )          as ic_exp,
//                      sum( nvl(sm.vmc_assessment_expense,0) )       as assessment,                             
//                      sum( nvl(sm.vmc_fees,0) )                     as vmc_fees,
//                      sum( sm.tot_exp_authorization )               as auth_exp,
//                      sum( sm.tot_exp_capture )                     as capture_exp,
//                      sum( sm.tot_exp_debit )                       as debit_exp,
//                      sum( sm.tot_exp_ind_plans )                   as plan_exp,
//                      sum( sm.tot_exp_sys_generated -
//                           (nvl(sm.equip_sales_expense,0) + 
//                            nvl(sm.equip_rental_expense,0)) )       as sys_gen_exp,
//                      sum( nvl(sm.supply_cost,0) )                  as supply_cost,                            
//                      sum( nvl(sm.equip_sales_expense,0) )          as equip_sales_exp,
//                      sum( nvl(sm.equip_rental_expense,0) )         as equip_rental_exp,
//                      sum( nvl(sm.equip_rental_base_cost,0) )       as equip_rental_base_cost,
//                      sum( nvl(sm.equip_sales_base_cost,0) )        as equip_sales_base_cost,
//                      sum( nvl(sm.excluded_equip_rental_amount,0) ) as excluded_equip_rental,
//                      sum( nvl(sm.excluded_equip_sales_amount,0) )  as excluded_equip_sales,
//                      sum( nvl(sm.excluded_fees_amount,0) )         as excluded_fees,
//                      sum( nvl(sm.equip_transfer_cost,0) )          as equip_transfer_cost,
//                      sum( nvl(sm.DISC_IC_DCE_ADJ_AMOUNT,0) )       as disc_ic_adj,
//                      sum( nvl(sm.fee_dce_adj_amount,0) )           as fee_adj,
//                      sum( nvl(mcli.tot_ndr,0) )                    as ndr_liability,   
//                      sum( nvl(mcli.tot_authorization,0) )          as auth_liability,
//                      sum( nvl(mcli.tot_capture,0) )                as capture_liability,
//                      sum( nvl(mcli.tot_debit,0) )                  as debit_liability,
//                      sum( nvl(mcli.tot_ind_plans,0) )              as plan_liability,
//                      sum( nvl(mcli.tot_sys_generated,0) )          as sys_gen_liability,
//                      sum( nvl(mcli.tot_equip_rental,0) )           as equip_rental_liability,
//                      sum( nvl(mcli.tot_equip_sales,0) )            as equip_sales_liability,          
//                      sum( nvl(mcrf.tot_ndr,0) )                    as ndr_referral,   
//                      sum( nvl(mcrf.tot_authorization,0) )          as auth_referral,
//                      sum( nvl(mcrf.tot_capture,0) )                as capture_referral,
//                      sum( nvl(mcrf.tot_debit,0) )                  as debit_referral,
//                      sum( nvl(mcrf.tot_ind_plans,0) )              as plan_referral,
//                      sum( nvl(mcrf.tot_sys_generated,0) )          as sys_gen_referral,          
//                      sum( nvl(mcrf.tot_equip_rental,0) )           as equip_rental_referral,          
//                      sum( nvl(mcrf.tot_equip_sales,0) )            as equip_sales_referral,          
//                      sum( nvl(mcrs.tot_ndr,0) )                    as ndr_reseller,   
//                      sum( nvl(mcrs.tot_authorization,0) )          as auth_reseller,
//                      sum( nvl(mcrs.tot_capture,0) )                as capture_reseller,
//                      sum( nvl(mcrs.tot_debit,0) )                  as debit_reseller,
//                      sum( nvl(mcrs.tot_ind_plans,0) )              as plan_reseller,
//                      sum( nvl(mcrs.tot_sys_generated,0) )          as sys_gen_reseller,          
//                      sum( nvl(mcrs.tot_equip_rental,0) )           as equip_rental_reseller,          
//                      sum( nvl(mcrs.tot_equip_sales,0) )            as equip_sales_reseller,          
//                      sum( nvl(mcex.tot_ndr,0) )                    as ndr_external,   
//                      sum( nvl(mcex.tot_authorization,0) )          as auth_external,
//                      sum( nvl(mcex.tot_capture,0) )                as capture_external,
//                      sum( nvl(mcex.tot_debit,0) )                  as debit_external,
//                      sum( nvl(mcex.tot_ind_plans,0) )              as plan_external,
//                      sum( nvl(mcex.tot_sys_generated,0) )          as sys_gen_external,
//                      sum( nvl(mcex.tot_equip_rental,0) )           as equip_rental_external,
//                      sum( nvl(mcex.tot_equip_sales,0) )            as equip_sales_external,          
//                      sum( nvl(sm.tot_partnership,0) )              as partnership
//            from      organization                    o,
//                      group_merchant                  gm,
//                      mif                             mf,
//                      merch_prof_relationships        pr,
//                      merch_prof_relationship_type    rt,
//                      monthly_extract_summary         sm,
//                      monthly_extract_contract        mcli,
//                      monthly_extract_contract        mcrf,
//                      monthly_extract_contract        mcrs,
//                      monthly_extract_contract        mcex
//            where     o.org_group = 394100000 and   -- only works for 3941 accounts
//                      gm.org_num = o.org_num and
//                      mf.merchant_number = gm.merchant_number and
//                      mf.activation_date between '01-JAN-2000' and last_day(:ReportDateBegin) and
//                      pr.hierarchy_node = (mf.bank_number || mf.group_2_association) and
//                      -- exclude sabre because amex interchange is 
//                      -- not included on the expense side making this
//                      -- node appear to have huge revenue.
//                      not pr.hierarchy_node in ( 3941400059 ) and 
//                      rt.relationship_type = pr.relationship_type and
//                      sm.merchant_number = mf.merchant_number and
//                      sm.active_date between :ReportDateBegin and trunc( trunc(sysdate,'month')-1,'month' ) and
//                      mcli.hh_load_sec(+) = sm.hh_load_sec and
//                      mcli.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_LIABILITY and -- 1 and
//                      mcrf.hh_load_sec(+) = sm.hh_load_sec and
//                      mcrf.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_REFERRAL and -- 2 and
//                      mcrs.hh_load_sec(+) = sm.hh_load_sec and
//                      mcrs.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_RESELLER and -- 3 and
//                      mcex.hh_load_sec(+) = sm.hh_load_sec and
//                      mcex.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_THIRD_PARTY -- 4
//            group by rt.relationship_type,rt.relationship_desc, sm.active_date,
//                     decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0),
//                     decode(sm.liability_contract,'Y',1,0),
//                     decode(sm.referral_contract,'Y',1,0),
//                     decode(sm.reseller_contract,'Y',1,0),
//                     decode(sm.cash_advance_referral,'Y',1,0),
//                     decode(sm.moto_merchant,'Y',1,0),
//                     decode(sm.mes_inventory,'Y',1,0)
//            order by rt.relationship_type, rt.relationship_desc, sm.active_date        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   0                                             as org_num,\n                    rt.relationship_type                          as hierarchy_node,\n                    rt.relationship_desc                          as org_name,\n                    sm.active_date                                as active_date,\n                    -- dates are offset (to TSYS) by one day to allow\n                    -- billing queries to get all items (i.e. service calls)\n                    -- adjust the dates to the TSYS billing dates\n                    min(nvl(sm.month_begin_date+1,sm.active_date))as month_begin_date,\n                    max(sm.month_end_date+1)                      as month_end_date,                  \n                    count(sm.merchant_number)                     as merchant_count,\n                    sum(decode( sm.activated_merchant,'Y',1,0 ))  as activated_count,\n                                 \n                    sum( decode( ( nvl(sm.vmc_sales_amount,0) +\n                                   nvl(sm.cash_advance_vol_amount,0) -\n                                   least( (nvl(sm.vmc_sales_amount,0) + \n                                           nvl(sm.cash_advance_vol_amount,0)),\n                                          9.99\n                                         ) \n                                  ), \n                                 0, 0, 1 ) )                      as active_count,\n                    decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0)       as cash_advance,          \n                    decode(sm.mes_inventory,'Y',1,0)              as mes_inventory,\n                    decode(sm.liability_contract,'Y',1,0)         as liability_contract,\n                    decode(sm.referral_contract,'Y',1,0)          as referral_contract,                   \n                    decode(sm.reseller_contract,'Y',1,0)          as reseller_contract,                   \n                    decode(sm.cash_advance_referral,'Y',1,0)      as cash_advance_referral,                   \n                    decode(sm.moto_merchant,'Y',1,0)              as moto_merchant,\n                    sum( sm.vmc_sales_count )                     as vmc_sales_count,\n                    sum( sm.vmc_sales_amount )                    as vmc_sales_amount,\n                    sum( sm.vmc_credits_count )                   as vmc_credits_count,\n                    sum( sm.vmc_credits_amount )                  as vmc_credits_amount,\n                    sum( sm.cash_advance_vol_count )              as cash_advance_vol_count,\n                    sum( sm.cash_advance_vol_amount )             as cash_advance_vol_amount,  \n                    sum( sm.TOT_INC_DISCOUNT + \n                         sm.tot_inc_interchange )                 as disc_ic_inc,\n                    sum( sm.tot_inc_authorization )               as auth_inc,\n                    sum( sm.tot_inc_capture )                     as capture_inc,\n                    sum( sm.tot_inc_debit )                       as debit_inc,\n                    sum( sm.tot_inc_ind_plans )                   as plan_inc,\n                    sum( sm.tot_inc_sys_generated -\n                         (sm.equip_sales_income + \n                          sm.equip_rental_income +\n                          nvl(sm.equip_sales_tax_collected,0)) \n                       )                                          as sys_gen_inc,\n                    sum( nvl(sm.mes_only_inc_disc_ic,0) )         as mes_only_disc_ic,\n                    sum( nvl(sm.mes_only_inc_authorization,0) )   as mes_only_auth,\n                    sum( nvl(sm.mes_only_inc_capture,0) )         as mes_only_capture,\n                    sum( nvl(sm.mes_only_inc_debit,0) )           as mes_only_debit,\n                    sum( nvl(sm.mes_only_inc_ind_plans,0) )       as mes_only_plan,\n                    sum( nvl(sm.mes_only_inc_sys_generated,0) )   as mes_only_sys_gen,\n                    sum( sm.equip_sales_income )                  as equip_sales_inc,\n                    sum( sm.equip_rental_income )                 as equip_rental_inc,\n                    sum( nvl(sm.equip_sales_tax_collected,0) )    as equip_sales_tax,\n                    sum( nvl(sm.interchange_expense,0) )          as ic_exp,\n                    sum( nvl(sm.vmc_assessment_expense,0) )       as assessment,                             \n                    sum( nvl(sm.vmc_fees,0) )                     as vmc_fees,\n                    sum( sm.tot_exp_authorization )               as auth_exp,\n                    sum( sm.tot_exp_capture )                     as capture_exp,\n                    sum( sm.tot_exp_debit )                       as debit_exp,\n                    sum( sm.tot_exp_ind_plans )                   as plan_exp,\n                    sum( sm.tot_exp_sys_generated -\n                         (nvl(sm.equip_sales_expense,0) + \n                          nvl(sm.equip_rental_expense,0)) )       as sys_gen_exp,\n                    sum( nvl(sm.supply_cost,0) )                  as supply_cost,                            \n                    sum( nvl(sm.equip_sales_expense,0) )          as equip_sales_exp,\n                    sum( nvl(sm.equip_rental_expense,0) )         as equip_rental_exp,\n                    sum( nvl(sm.equip_rental_base_cost,0) )       as equip_rental_base_cost,\n                    sum( nvl(sm.equip_sales_base_cost,0) )        as equip_sales_base_cost,\n                    sum( nvl(sm.excluded_equip_rental_amount,0) ) as excluded_equip_rental,\n                    sum( nvl(sm.excluded_equip_sales_amount,0) )  as excluded_equip_sales,\n                    sum( nvl(sm.excluded_fees_amount,0) )         as excluded_fees,\n                    sum( nvl(sm.equip_transfer_cost,0) )          as equip_transfer_cost,\n                    sum( nvl(sm.DISC_IC_DCE_ADJ_AMOUNT,0) )       as disc_ic_adj,\n                    sum( nvl(sm.fee_dce_adj_amount,0) )           as fee_adj,\n                    sum( nvl(mcli.tot_ndr,0) )                    as ndr_liability,   \n                    sum( nvl(mcli.tot_authorization,0) )          as auth_liability,\n                    sum( nvl(mcli.tot_capture,0) )                as capture_liability,\n                    sum( nvl(mcli.tot_debit,0) )                  as debit_liability,\n                    sum( nvl(mcli.tot_ind_plans,0) )              as plan_liability,\n                    sum( nvl(mcli.tot_sys_generated,0) )          as sys_gen_liability,\n                    sum( nvl(mcli.tot_equip_rental,0) )           as equip_rental_liability,\n                    sum( nvl(mcli.tot_equip_sales,0) )            as equip_sales_liability,          \n                    sum( nvl(mcrf.tot_ndr,0) )                    as ndr_referral,   \n                    sum( nvl(mcrf.tot_authorization,0) )          as auth_referral,\n                    sum( nvl(mcrf.tot_capture,0) )                as capture_referral,\n                    sum( nvl(mcrf.tot_debit,0) )                  as debit_referral,\n                    sum( nvl(mcrf.tot_ind_plans,0) )              as plan_referral,\n                    sum( nvl(mcrf.tot_sys_generated,0) )          as sys_gen_referral,          \n                    sum( nvl(mcrf.tot_equip_rental,0) )           as equip_rental_referral,          \n                    sum( nvl(mcrf.tot_equip_sales,0) )            as equip_sales_referral,          \n                    sum( nvl(mcrs.tot_ndr,0) )                    as ndr_reseller,   \n                    sum( nvl(mcrs.tot_authorization,0) )          as auth_reseller,\n                    sum( nvl(mcrs.tot_capture,0) )                as capture_reseller,\n                    sum( nvl(mcrs.tot_debit,0) )                  as debit_reseller,\n                    sum( nvl(mcrs.tot_ind_plans,0) )              as plan_reseller,\n                    sum( nvl(mcrs.tot_sys_generated,0) )          as sys_gen_reseller,          \n                    sum( nvl(mcrs.tot_equip_rental,0) )           as equip_rental_reseller,          \n                    sum( nvl(mcrs.tot_equip_sales,0) )            as equip_sales_reseller,          \n                    sum( nvl(mcex.tot_ndr,0) )                    as ndr_external,   \n                    sum( nvl(mcex.tot_authorization,0) )          as auth_external,\n                    sum( nvl(mcex.tot_capture,0) )                as capture_external,\n                    sum( nvl(mcex.tot_debit,0) )                  as debit_external,\n                    sum( nvl(mcex.tot_ind_plans,0) )              as plan_external,\n                    sum( nvl(mcex.tot_sys_generated,0) )          as sys_gen_external,\n                    sum( nvl(mcex.tot_equip_rental,0) )           as equip_rental_external,\n                    sum( nvl(mcex.tot_equip_sales,0) )            as equip_sales_external,          \n                    sum( nvl(sm.tot_partnership,0) )              as partnership\n          from      organization                    o,\n                    group_merchant                  gm,\n                    mif                             mf,\n                    merch_prof_relationships        pr,\n                    merch_prof_relationship_type    rt,\n                    monthly_extract_summary         sm,\n                    monthly_extract_contract        mcli,\n                    monthly_extract_contract        mcrf,\n                    monthly_extract_contract        mcrs,\n                    monthly_extract_contract        mcex\n          where     o.org_group = 394100000 and   -- only works for 3941 accounts\n                    gm.org_num = o.org_num and\n                    mf.merchant_number = gm.merchant_number and\n                    mf.activation_date between '01-JAN-2000' and last_day( :1  ) and\n                    pr.hierarchy_node = (mf.bank_number || mf.group_2_association) and\n                    -- exclude sabre because amex interchange is \n                    -- not included on the expense side making this\n                    -- node appear to have huge revenue.\n                    not pr.hierarchy_node in ( 3941400059 ) and \n                    rt.relationship_type = pr.relationship_type and\n                    sm.merchant_number = mf.merchant_number and\n                    sm.active_date between  :2   and trunc( trunc(sysdate,'month')-1,'month' ) and\n                    mcli.hh_load_sec(+) = sm.hh_load_sec and\n                    mcli.contract_type(+) =  :3   and -- 1 and\n                    mcrf.hh_load_sec(+) = sm.hh_load_sec and\n                    mcrf.contract_type(+) =  :4   and -- 2 and\n                    mcrs.hh_load_sec(+) = sm.hh_load_sec and\n                    mcrs.contract_type(+) =  :5   and -- 3 and\n                    mcex.hh_load_sec(+) = sm.hh_load_sec and\n                    mcex.contract_type(+) =  :6   -- 4\n          group by rt.relationship_type,rt.relationship_desc, sm.active_date,\n                   decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0),\n                   decode(sm.liability_contract,'Y',1,0),\n                   decode(sm.referral_contract,'Y',1,0),\n                   decode(sm.reseller_contract,'Y',1,0),\n                   decode(sm.cash_advance_referral,'Y',1,0),\n                   decode(sm.moto_merchant,'Y',1,0),\n                   decode(sm.mes_inventory,'Y',1,0)\n          order by rt.relationship_type, rt.relationship_desc, sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,ReportDateBegin);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setInt(3,ContractTypes.CONTRACT_SOURCE_LIABILITY);
   __sJT_st.setInt(4,ContractTypes.CONTRACT_SOURCE_REFERRAL);
   __sJT_st.setInt(5,ContractTypes.CONTRACT_SOURCE_RESELLER);
   __sJT_st.setInt(6,ContractTypes.CONTRACT_SOURCE_THIRD_PARTY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3823^9*/
      }
      else    // child
      {
        String whereClause    = null;
        
        if ( RelationshipType == -1 )
        {
          if ( isNodeAssociation(nodeId) )
          {
            whereClause = "o.org_group in                                   " +
                          "(                                                " +
                          "  select  mf.merchant_number" + 
                          "  from    mif mf                                 " +
                          "  where   mf.association_node = " + String.valueOf(nodeId) +
                          ")";
          }
          else
          {
            whereClause = "o.org_group in                                   " +
                          "(                                                " +
                          "  select th.descendent                           " +
                          "  from    t_hierarchy th                         " +
                          "  where   th.hier_type = 1 and                   " +
                          "          th.ancestor = " + String.valueOf(nodeId) + " and " +
                          "          ( th.relation = 1 or                   " +
                          "            ( th.ancestor = th.descendent and    " +
                          "              th.entity_type = 4 )               " +
                          "           )                                     " +
                          ")                                                ";
          }                          
        }
        else
        {
          whereClause = "o.org_group in                                      " +
                        "(                                                   " +
                        "  select  pr.hierarchy_node                         " +
                        "  from    merch_prof_relationships    pr            " +
                        "  where   pr.relationship_type = " + String.valueOf(RelationshipType) +
                        ")                                                   ";
        }
        /*@lineinfo:generated-code*//*@lineinfo:3864^9*/

//  ************************************************************
//  #sql [Ctx] it = { select   o.org_num                                     as org_num,
//                      o.org_group                                   as hierarchy_node,
//                      o.org_name                                    as org_name,
//                      sm.active_date                                as active_date,
//                      -- dates are offset (to TSYS) by one day to allow
//                      -- billing queries to get all items (i.e. service calls)
//                      -- adjust the dates to the TSYS billing dates
//                      min(nvl(sm.month_begin_date+1,sm.active_date))as month_begin_date,
//                      max(sm.month_end_date+1)                      as month_end_date,                  
//                      count(sm.merchant_number)                     as merchant_count,
//                      sum( decode( trunc(mf.activation_date,'month'),
//                                   sm.active_date, 1, 0 ) )         as activated_count,
//                      sum( decode( ( nvl(sm.vmc_sales_amount,0) +
//                                     nvl(sm.cash_advance_vol_amount,0) -
//                                     least( (nvl(sm.vmc_sales_amount,0) + 
//                                             nvl(sm.cash_advance_vol_amount,0)),
//                                            9.99
//                                           ) 
//                                    ), 
//                                   0, 0, 1 ) )                      as active_count,
//                      decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0)       as cash_advance,          
//                      decode(sm.mes_inventory,'Y',1,0)              as mes_inventory,
//                      decode(sm.liability_contract,'Y',1,0)         as liability_contract,
//                      decode(sm.referral_contract,'Y',1,0)          as referral_contract,                   
//                      decode(sm.reseller_contract,'Y',1,0)          as reseller_contract,                   
//                      decode(sm.cash_advance_referral,'Y',1,0)      as cash_advance_referral,                   
//                      decode(sm.moto_merchant,'Y',1,0)              as moto_merchant,
//                      sum( sm.vmc_sales_count )                     as vmc_sales_count,
//                      sum( sm.vmc_sales_amount )                    as vmc_sales_amount,
//                      sum( sm.vmc_credits_count )                   as vmc_credits_count,
//                      sum( sm.vmc_credits_amount )                  as vmc_credits_amount,
//                      sum( sm.cash_advance_vol_count )              as cash_advance_vol_count,
//                      sum( sm.cash_advance_vol_amount )             as cash_advance_vol_amount,  
//                      sum( sm.TOT_INC_DISCOUNT + 
//                           sm.tot_inc_interchange )                 as disc_ic_inc,
//                      sum( sm.tot_inc_authorization )               as auth_inc,
//                      sum( sm.tot_inc_capture )                     as capture_inc,
//                      sum( sm.tot_inc_debit )                       as debit_inc,
//                      sum( sm.tot_inc_ind_plans )                   as plan_inc,
//                      sum( sm.tot_inc_sys_generated -
//                           (sm.equip_sales_income + 
//                            sm.equip_rental_income +
//                            nvl(sm.equip_sales_tax_collected,0)) 
//                         )                                          as sys_gen_inc,
//                      sum( nvl(sm.mes_only_inc_disc_ic,0) )         as mes_only_disc_ic,
//                      sum( nvl(sm.mes_only_inc_authorization,0) )   as mes_only_auth,
//                      sum( nvl(sm.mes_only_inc_capture,0) )         as mes_only_capture,
//                      sum( nvl(sm.mes_only_inc_debit,0) )           as mes_only_debit,
//                      sum( nvl(sm.mes_only_inc_ind_plans,0) )       as mes_only_plan,
//                      sum( nvl(sm.mes_only_inc_sys_generated,0) )   as mes_only_sys_gen,
//                      sum( sm.equip_sales_income )                  as equip_sales_inc,
//                      sum( sm.equip_rental_income )                 as equip_rental_inc,
//                      sum( nvl(sm.equip_sales_tax_collected,0) )    as equip_sales_tax,
//                      sum( nvl(sm.interchange_expense,0) )          as ic_exp,
//                      sum( nvl(sm.vmc_assessment_expense,0) )       as assessment,                             
//                      sum( nvl(sm.vmc_fees,0) )                     as vmc_fees,
//                      sum( sm.tot_exp_authorization )               as auth_exp,
//                      sum( sm.tot_exp_capture )                     as capture_exp,
//                      sum( sm.tot_exp_debit )                       as debit_exp,
//                      sum( sm.tot_exp_ind_plans )                   as plan_exp,
//                      sum( sm.tot_exp_sys_generated -
//                           (nvl(sm.equip_sales_expense,0) + 
//                            nvl(sm.equip_rental_expense,0)) )       as sys_gen_exp,
//                      sum( nvl(sm.supply_cost,0) )                  as supply_cost,                            
//                      sum( nvl(sm.equip_sales_expense,0) )          as equip_sales_exp,
//                      sum( nvl(sm.equip_rental_expense,0) )         as equip_rental_exp,
//                      sum( nvl(sm.equip_rental_base_cost,0) )       as equip_rental_base_cost,
//                      sum( nvl(sm.equip_sales_base_cost,0) )        as equip_sales_base_cost,
//                      sum( nvl(sm.excluded_equip_rental_amount,0) ) as excluded_equip_rental,
//                      sum( nvl(sm.excluded_equip_sales_amount,0) )  as excluded_equip_sales,
//                      sum( nvl(sm.excluded_fees_amount,0) )         as excluded_fees,
//                      sum( nvl(sm.equip_transfer_cost,0) )          as equip_transfer_cost,
//                      sum( nvl(sm.DISC_IC_DCE_ADJ_AMOUNT,0) )       as disc_ic_adj,
//                      sum( nvl(sm.fee_dce_adj_amount,0) )           as fee_adj,
//                      sum( nvl(mcli.tot_ndr,0) )                    as ndr_liability,   
//                      sum( nvl(mcli.tot_authorization,0) )          as auth_liability,
//                      sum( nvl(mcli.tot_capture,0) )                as capture_liability,
//                      sum( nvl(mcli.tot_debit,0) )                  as debit_liability,
//                      sum( nvl(mcli.tot_ind_plans,0) )              as plan_liability,
//                      sum( nvl(mcli.tot_sys_generated,0) )          as sys_gen_liability,
//                      sum( nvl(mcli.tot_equip_rental,0) )           as equip_rental_liability,
//                      sum( nvl(mcli.tot_equip_sales,0) )            as equip_sales_liability,          
//                      sum( nvl(mcrf.tot_ndr,0) )                    as ndr_referral,   
//                      sum( nvl(mcrf.tot_authorization,0) )          as auth_referral,
//                      sum( nvl(mcrf.tot_capture,0) )                as capture_referral,
//                      sum( nvl(mcrf.tot_debit,0) )                  as debit_referral,
//                      sum( nvl(mcrf.tot_ind_plans,0) )              as plan_referral,
//                      sum( nvl(mcrf.tot_sys_generated,0) )          as sys_gen_referral,          
//                      sum( nvl(mcrf.tot_equip_rental,0) )           as equip_rental_referral,          
//                      sum( nvl(mcrf.tot_equip_sales,0) )            as equip_sales_referral,          
//                      sum( nvl(mcrs.tot_ndr,0) )                    as ndr_reseller,   
//                      sum( nvl(mcrs.tot_authorization,0) )          as auth_reseller,
//                      sum( nvl(mcrs.tot_capture,0) )                as capture_reseller,
//                      sum( nvl(mcrs.tot_debit,0) )                  as debit_reseller,
//                      sum( nvl(mcrs.tot_ind_plans,0) )              as plan_reseller,
//                      sum( nvl(mcrs.tot_sys_generated,0) )          as sys_gen_reseller,          
//                      sum( nvl(mcrs.tot_equip_rental,0) )           as equip_rental_reseller,          
//                      sum( nvl(mcrs.tot_equip_sales,0) )            as equip_sales_reseller,          
//                      sum( nvl(mcex.tot_ndr,0) )                    as ndr_external,   
//                      sum( nvl(mcex.tot_authorization,0) )          as auth_external,
//                      sum( nvl(mcex.tot_capture,0) )                as capture_external,
//                      sum( nvl(mcex.tot_debit,0) )                  as debit_external,
//                      sum( nvl(mcex.tot_ind_plans,0) )              as plan_external,
//                      sum( nvl(mcex.tot_sys_generated,0) )          as sys_gen_external,
//                      sum( nvl(mcex.tot_equip_rental,0) )           as equip_rental_external,
//                      sum( nvl(mcex.tot_equip_sales,0) )            as equip_sales_external,          
//                      sum( nvl(sm.tot_partnership,0) )              as partnership
//            from      organization                o,
//                      group_merchant              gm,
//                      mif                         mf,
//                      monthly_extract_summary     sm,
//                      monthly_extract_contract    mcli,
//                      monthly_extract_contract    mcrf,
//                      monthly_extract_contract    mcrs,
//                      monthly_extract_contract    mcex
//            where     :whereClause and
//                      gm.org_num = o.org_num and
//                      mf.merchant_number = gm.merchant_number and
//                      mf.activation_date between '01-JAN-2000' and last_day(:ReportDateBegin) and
//                      sm.merchant_number = mf.merchant_number and
//                      sm.active_date between :ReportDateBegin and trunc( trunc(sysdate,'month')-1,'month' ) and
//                      mcli.hh_load_sec(+) = sm.hh_load_sec and
//                      mcli.merchant_number(+) = sm.merchant_number and
//                      mcli.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_LIABILITY and -- 1 and
//                      mcrf.hh_load_sec(+) = sm.hh_load_sec and
//                      mcrf.merchant_number(+) = sm.merchant_number and
//                      mcrf.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_REFERRAL and -- 2 and
//                      mcrs.hh_load_sec(+) = sm.hh_load_sec and
//                      mcrs.merchant_number(+) = sm.merchant_number and
//                      mcrs.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_RESELLER and -- 3 and
//                      mcex.hh_load_sec(+) = sm.hh_load_sec and
//                      mcex.merchant_number(+) = sm.merchant_number and
//                      mcex.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_THIRD_PARTY -- 4 
//            group by o.org_num, o.org_group, o.org_name, sm.active_date,
//                     decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0),
//                     decode(sm.liability_contract,'Y',1,0),
//                     decode(sm.referral_contract,'Y',1,0),
//                     decode(sm.reseller_contract,'Y',1,0),
//                     decode(sm.cash_advance_referral,'Y',1,0),
//                     decode(sm.moto_merchant,'Y',1,0),
//                     decode(sm.mes_inventory,'Y',1,0)
//            order by o.org_name, o.org_num, sm.active_date        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select   o.org_num                                     as org_num,\n                    o.org_group                                   as hierarchy_node,\n                    o.org_name                                    as org_name,\n                    sm.active_date                                as active_date,\n                    -- dates are offset (to TSYS) by one day to allow\n                    -- billing queries to get all items (i.e. service calls)\n                    -- adjust the dates to the TSYS billing dates\n                    min(nvl(sm.month_begin_date+1,sm.active_date))as month_begin_date,\n                    max(sm.month_end_date+1)                      as month_end_date,                  \n                    count(sm.merchant_number)                     as merchant_count,\n                    sum( decode( trunc(mf.activation_date,'month'),\n                                 sm.active_date, 1, 0 ) )         as activated_count,\n                    sum( decode( ( nvl(sm.vmc_sales_amount,0) +\n                                   nvl(sm.cash_advance_vol_amount,0) -\n                                   least( (nvl(sm.vmc_sales_amount,0) + \n                                           nvl(sm.cash_advance_vol_amount,0)),\n                                          9.99\n                                         ) \n                                  ), \n                                 0, 0, 1 ) )                      as active_count,\n                    decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0)       as cash_advance,          \n                    decode(sm.mes_inventory,'Y',1,0)              as mes_inventory,\n                    decode(sm.liability_contract,'Y',1,0)         as liability_contract,\n                    decode(sm.referral_contract,'Y',1,0)          as referral_contract,                   \n                    decode(sm.reseller_contract,'Y',1,0)          as reseller_contract,                   \n                    decode(sm.cash_advance_referral,'Y',1,0)      as cash_advance_referral,                   \n                    decode(sm.moto_merchant,'Y',1,0)              as moto_merchant,\n                    sum( sm.vmc_sales_count )                     as vmc_sales_count,\n                    sum( sm.vmc_sales_amount )                    as vmc_sales_amount,\n                    sum( sm.vmc_credits_count )                   as vmc_credits_count,\n                    sum( sm.vmc_credits_amount )                  as vmc_credits_amount,\n                    sum( sm.cash_advance_vol_count )              as cash_advance_vol_count,\n                    sum( sm.cash_advance_vol_amount )             as cash_advance_vol_amount,  \n                    sum( sm.TOT_INC_DISCOUNT + \n                         sm.tot_inc_interchange )                 as disc_ic_inc,\n                    sum( sm.tot_inc_authorization )               as auth_inc,\n                    sum( sm.tot_inc_capture )                     as capture_inc,\n                    sum( sm.tot_inc_debit )                       as debit_inc,\n                    sum( sm.tot_inc_ind_plans )                   as plan_inc,\n                    sum( sm.tot_inc_sys_generated -\n                         (sm.equip_sales_income + \n                          sm.equip_rental_income +\n                          nvl(sm.equip_sales_tax_collected,0)) \n                       )                                          as sys_gen_inc,\n                    sum( nvl(sm.mes_only_inc_disc_ic,0) )         as mes_only_disc_ic,\n                    sum( nvl(sm.mes_only_inc_authorization,0) )   as mes_only_auth,\n                    sum( nvl(sm.mes_only_inc_capture,0) )         as mes_only_capture,\n                    sum( nvl(sm.mes_only_inc_debit,0) )           as mes_only_debit,\n                    sum( nvl(sm.mes_only_inc_ind_plans,0) )       as mes_only_plan,\n                    sum( nvl(sm.mes_only_inc_sys_generated,0) )   as mes_only_sys_gen,\n                    sum( sm.equip_sales_income )                  as equip_sales_inc,\n                    sum( sm.equip_rental_income )                 as equip_rental_inc,\n                    sum( nvl(sm.equip_sales_tax_collected,0) )    as equip_sales_tax,\n                    sum( nvl(sm.interchange_expense,0) )          as ic_exp,\n                    sum( nvl(sm.vmc_assessment_expense,0) )       as assessment,                             \n                    sum( nvl(sm.vmc_fees,0) )                     as vmc_fees,\n                    sum( sm.tot_exp_authorization )               as auth_exp,\n                    sum( sm.tot_exp_capture )                     as capture_exp,\n                    sum( sm.tot_exp_debit )                       as debit_exp,\n                    sum( sm.tot_exp_ind_plans )                   as plan_exp,\n                    sum( sm.tot_exp_sys_generated -\n                         (nvl(sm.equip_sales_expense,0) + \n                          nvl(sm.equip_rental_expense,0)) )       as sys_gen_exp,\n                    sum( nvl(sm.supply_cost,0) )                  as supply_cost,                            \n                    sum( nvl(sm.equip_sales_expense,0) )          as equip_sales_exp,\n                    sum( nvl(sm.equip_rental_expense,0) )         as equip_rental_exp,\n                    sum( nvl(sm.equip_rental_base_cost,0) )       as equip_rental_base_cost,\n                    sum( nvl(sm.equip_sales_base_cost,0) )        as equip_sales_base_cost,\n                    sum( nvl(sm.excluded_equip_rental_amount,0) ) as excluded_equip_rental,\n                    sum( nvl(sm.excluded_equip_sales_amount,0) )  as excluded_equip_sales,\n                    sum( nvl(sm.excluded_fees_amount,0) )         as excluded_fees,\n                    sum( nvl(sm.equip_transfer_cost,0) )          as equip_transfer_cost,\n                    sum( nvl(sm.DISC_IC_DCE_ADJ_AMOUNT,0) )       as disc_ic_adj,\n                    sum( nvl(sm.fee_dce_adj_amount,0) )           as fee_adj,\n                    sum( nvl(mcli.tot_ndr,0) )                    as ndr_liability,   \n                    sum( nvl(mcli.tot_authorization,0) )          as auth_liability,\n                    sum( nvl(mcli.tot_capture,0) )                as capture_liability,\n                    sum( nvl(mcli.tot_debit,0) )                  as debit_liability,\n                    sum( nvl(mcli.tot_ind_plans,0) )              as plan_liability,\n                    sum( nvl(mcli.tot_sys_generated,0) )          as sys_gen_liability,\n                    sum( nvl(mcli.tot_equip_rental,0) )           as equip_rental_liability,\n                    sum( nvl(mcli.tot_equip_sales,0) )            as equip_sales_liability,          \n                    sum( nvl(mcrf.tot_ndr,0) )                    as ndr_referral,   \n                    sum( nvl(mcrf.tot_authorization,0) )          as auth_referral,\n                    sum( nvl(mcrf.tot_capture,0) )                as capture_referral,\n                    sum( nvl(mcrf.tot_debit,0) )                  as debit_referral,\n                    sum( nvl(mcrf.tot_ind_plans,0) )              as plan_referral,\n                    sum( nvl(mcrf.tot_sys_generated,0) )          as sys_gen_referral,          \n                    sum( nvl(mcrf.tot_equip_rental,0) )           as equip_rental_referral,          \n                    sum( nvl(mcrf.tot_equip_sales,0) )            as equip_sales_referral,          \n                    sum( nvl(mcrs.tot_ndr,0) )                    as ndr_reseller,   \n                    sum( nvl(mcrs.tot_authorization,0) )          as auth_reseller,\n                    sum( nvl(mcrs.tot_capture,0) )                as capture_reseller,\n                    sum( nvl(mcrs.tot_debit,0) )                  as debit_reseller,\n                    sum( nvl(mcrs.tot_ind_plans,0) )              as plan_reseller,\n                    sum( nvl(mcrs.tot_sys_generated,0) )          as sys_gen_reseller,          \n                    sum( nvl(mcrs.tot_equip_rental,0) )           as equip_rental_reseller,          \n                    sum( nvl(mcrs.tot_equip_sales,0) )            as equip_sales_reseller,          \n                    sum( nvl(mcex.tot_ndr,0) )                    as ndr_external,   \n                    sum( nvl(mcex.tot_authorization,0) )          as auth_external,\n                    sum( nvl(mcex.tot_capture,0) )                as capture_external,\n                    sum( nvl(mcex.tot_debit,0) )                  as debit_external,\n                    sum( nvl(mcex.tot_ind_plans,0) )              as plan_external,\n                    sum( nvl(mcex.tot_sys_generated,0) )          as sys_gen_external,\n                    sum( nvl(mcex.tot_equip_rental,0) )           as equip_rental_external,\n                    sum( nvl(mcex.tot_equip_sales,0) )            as equip_sales_external,          \n                    sum( nvl(sm.tot_partnership,0) )              as partnership\n          from      organization                o,\n                    group_merchant              gm,\n                    mif                         mf,\n                    monthly_extract_summary     sm,\n                    monthly_extract_contract    mcli,\n                    monthly_extract_contract    mcrf,\n                    monthly_extract_contract    mcrs,\n                    monthly_extract_contract    mcex\n          where      ");
   __sjT_sb.append(whereClause);
   __sjT_sb.append("  and\n                    gm.org_num = o.org_num and\n                    mf.merchant_number = gm.merchant_number and\n                    mf.activation_date between '01-JAN-2000' and last_day( ? ) and\n                    sm.merchant_number = mf.merchant_number and\n                    sm.active_date between  ?  and trunc( trunc(sysdate,'month')-1,'month' ) and\n                    mcli.hh_load_sec(+) = sm.hh_load_sec and\n                    mcli.merchant_number(+) = sm.merchant_number and\n                    mcli.contract_type(+) =  ?  and -- 1 and\n                    mcrf.hh_load_sec(+) = sm.hh_load_sec and\n                    mcrf.merchant_number(+) = sm.merchant_number and\n                    mcrf.contract_type(+) =  ?  and -- 2 and\n                    mcrs.hh_load_sec(+) = sm.hh_load_sec and\n                    mcrs.merchant_number(+) = sm.merchant_number and\n                    mcrs.contract_type(+) =  ?  and -- 3 and\n                    mcex.hh_load_sec(+) = sm.hh_load_sec and\n                    mcex.merchant_number(+) = sm.merchant_number and\n                    mcex.contract_type(+) =  ?  -- 4 \n          group by o.org_num, o.org_group, o.org_name, sm.active_date,\n                   decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0),\n                   decode(sm.liability_contract,'Y',1,0),\n                   decode(sm.referral_contract,'Y',1,0),\n                   decode(sm.reseller_contract,'Y',1,0),\n                   decode(sm.cash_advance_referral,'Y',1,0),\n                   decode(sm.moto_merchant,'Y',1,0),\n                   decode(sm.mes_inventory,'Y',1,0)\n          order by o.org_name, o.org_num, sm.active_date");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "23com.mes.reports.BankContractDataBean:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,ReportDateBegin);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setInt(3,ContractTypes.CONTRACT_SOURCE_LIABILITY);
   __sJT_st.setInt(4,ContractTypes.CONTRACT_SOURCE_REFERRAL);
   __sJT_st.setInt(5,ContractTypes.CONTRACT_SOURCE_RESELLER);
   __sJT_st.setInt(6,ContractTypes.CONTRACT_SOURCE_THIRD_PARTY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4008^9*/
      }        
      resultSet = it.getResultSet();
      while( resultSet.next() )
      {
        relId       = resultSet.getLong("hierarchy_node");
        activeDate  = resultSet.getDate("active_date");
    
        if ( relId != lastRelId || !activeDate.equals(lastActiveDate) )
        {
          System.out.print("creating summary data..");//@
          row = new SummaryData( resultSet, userType );
          ReportRows.add(row);
          
          // store the last values
          lastRelId      = relId;
          lastActiveDate = activeDate;
        }
        row.addData( resultSet );
      }        
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadNetRevenueRetentionData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  protected void loadMesOnlySysFees( long merchantId )
  {
    Date                            activeDate            = null;
    int                             betType;
    long                            contractNode;
    StringBuffer                    desc                  = new StringBuffer("");
    int                             incExpFlag            = -1;
    ResultSetIterator               it                    = null;
    ContractTypes.PerItemRecord     record                = null;
    ResultSet                       resultSet             = null;
    
    try
    {
      // setup a generic BET type for the any summary level items.
      betType = BankContractBean.groupIndexToBetType( BankContractBean.BET_GROUP_SYSTEM_FEES );
      
      // get the liability contract node to use when masking revenue
      contractNode = getContractNode( merchantId, ContractTypes.CONTRACT_SOURCE_LIABILITY );
    
      /*@lineinfo:generated-code*//*@lineinfo:4058^7*/

//  ************************************************************
//  #sql [Ctx] it = { select sm.active_date                             as active_date,
//                 decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, 
//                         0, 1,
//                         st.ST_NUMBER_OF_ITEMS_ON_STMT )    as item_count,
//                 decode( st.ST_AMOUNT_OF_ITEM_ON_STMT,
//                         0, ( decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, 
//                                      0, st.ST_FEE_AMOUNT,
//                                      st.ST_FEE_AMOUNT / st.ST_NUMBER_OF_ITEMS_ON_STMT ) ),
//                         st.ST_AMOUNT_OF_ITEM_ON_STMT )     as per_item,                       
//                 st.ST_STATEMENT_DESC                       as item_desc,
//                 st.ST_FEE_AMOUNT                           as total_amount,
//                 msk.charge_rec_prefix                      as masked
//          from   monthly_extract_summary  sm,
//                 monthly_extract_st       st,
//                 monthly_extract_cg       cg,
//                 agent_bank_revenue_mask  msk
//          where    sm.merchant_number = :merchantId and
//                   sm.active_date between :ReportDateBegin and last_day( :ReportDateEnd ) and
//                      /* take all the statement records that were 
//                         generated as a result of a charge record 
//                         except POS and IMP record types          */
//                   st.hh_load_sec = sm.hh_load_sec and
//                   st.st_fee_amount > 0 and
//                   cg.hh_load_sec(+) = st.hh_load_sec and
//                   cg.CG_MESSAGE_FOR_STMT(+) = st.ST_STATEMENT_DESC and
//                   ( 
//                      not cg.cg_charge_record_type in ('POS','IMP') or  
//                      st.st_statement_desc like 'CHARGEBACK FEE%' 
//                    ) and
//                    msk.hierarchy_node = :contractNode and
//                    msk.mask_type = 2 and         -- agent bank mask
//                    msk.income_category = 5 and   -- 5 is system fees
//                    not msk.charge_rec_prefix is null and
//                    st.st_statement_desc like ( msk.charge_rec_prefix || '%' )
//          order by st.ST_STATEMENT_DESC      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select sm.active_date                             as active_date,\n               decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, \n                       0, 1,\n                       st.ST_NUMBER_OF_ITEMS_ON_STMT )    as item_count,\n               decode( st.ST_AMOUNT_OF_ITEM_ON_STMT,\n                       0, ( decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, \n                                    0, st.ST_FEE_AMOUNT,\n                                    st.ST_FEE_AMOUNT / st.ST_NUMBER_OF_ITEMS_ON_STMT ) ),\n                       st.ST_AMOUNT_OF_ITEM_ON_STMT )     as per_item,                       \n               st.ST_STATEMENT_DESC                       as item_desc,\n               st.ST_FEE_AMOUNT                           as total_amount,\n               msk.charge_rec_prefix                      as masked\n        from   monthly_extract_summary  sm,\n               monthly_extract_st       st,\n               monthly_extract_cg       cg,\n               agent_bank_revenue_mask  msk\n        where    sm.merchant_number =  :1   and\n                 sm.active_date between  :2   and last_day(  :3   ) and\n                    /* take all the statement records that were \n                       generated as a result of a charge record \n                       except POS and IMP record types          */\n                 st.hh_load_sec = sm.hh_load_sec and\n                 st.st_fee_amount > 0 and\n                 cg.hh_load_sec(+) = st.hh_load_sec and\n                 cg.CG_MESSAGE_FOR_STMT(+) = st.ST_STATEMENT_DESC and\n                 ( \n                    not cg.cg_charge_record_type in ('POS','IMP') or  \n                    st.st_statement_desc like 'CHARGEBACK FEE%' \n                  ) and\n                  msk.hierarchy_node =  :4   and\n                  msk.mask_type = 2 and         -- agent bank mask\n                  msk.income_category = 5 and   -- 5 is system fees\n                  not msk.charge_rec_prefix is null and\n                  st.st_statement_desc like ( msk.charge_rec_prefix || '%' )\n        order by st.ST_STATEMENT_DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   __sJT_st.setLong(4,contractNode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"24com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4095^7*/
      resultSet = it.getResultSet();      
      while( resultSet.next() )
      {
        activeDate = resultSet.getDate("active_date");
        record = new ContractTypes.PerItemRecord( activeDate, ContractTypes.CONTRACT_SOURCE_VITAL, betType, resultSet.getInt( "ITEM_COUNT" ),
                                    resultSet.getDouble("PER_ITEM") );
        desc.setLength(0);
        desc.append("MES: ");
        desc.append( resultSet.getString("ITEM_DESC") );
        desc.append( " - " );
        desc.append( DateTimeFormatter.getFormattedDate( activeDate,"MMM yyyy" ) );
        record.setDescription( desc.toString() );
      
        switch( getUserType() )
        {
          case UT_MES:
            incExpFlag = ContractTypes.CONTRACT_TYPE_INCOME;
            break;
            
          case UT_CONTRACT_BANK:
            incExpFlag = ContractTypes.CONTRACT_TYPE_EXPENSE;
            break;
            
          case UT_RESELLER:
            if ( isDirectMerchant(merchantId) )
            {
              incExpFlag = ContractTypes.CONTRACT_TYPE_EXPENSE;
            }
            else
            {
              incExpFlag = -1;
            }
            break;
        }
       
        if ( incExpFlag != -1 )
        {
          addContractElement( getReportOrgId(), ContractTypes.CONTRACT_DATA_MERCH_DIRECT,
                              incExpFlag, record );
        }
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadMesOnlySysFees()", e.toString());
      addError("loadMesOnlySysFees: " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  public void loadRetentionData( )
  {
    Date                        activeDate        = null;
    boolean                     agentBankUser     = isUserUnderContract();
    RetentionAppData            appData           = null;
    ResultSetIterator           it                = null;
    Date                        lastActiveDate    = null;
    long                        lastOrgId         = -1L;
    long                        nodeId            = getReportHierarchyNode();
    long                        orgId             = 0L;
    ResultSet                   resultSet         = null;
    RetentionSummaryData        row               = null;
    int                         userType          = getUserType();
    
    try
    {
      ReportRows.removeAllElements();

      if ( SummaryType == ST_PARENT )
      {
        /*@lineinfo:generated-code*//*@lineinfo:4171^9*/

//  ************************************************************
//  #sql [Ctx] it = { select   o.org_num                                     as org_num,
//                      o.org_group                                   as hierarchy_node,
//                      o.org_name                                    as org_name,
//                      sm.active_date                                as active_date,
//                      -- dates are offset (to TSYS) by one day to allow
//                      -- billing queries to get all items (i.e. service calls)
//                      -- adjust the dates to the TSYS billing dates
//                      min(nvl(sm.month_begin_date+1,sm.active_date))as month_begin_date,
//                      max(sm.month_end_date+1)                      as month_end_date,                  
//                      count(sm.merchant_number)                     as merchant_count,
//                      sum( decode( trunc(mf.activation_date,'month'),
//                                   sm.active_date, 1, 0 ) )         as activated_count,
//                      sum( decode( ( nvl(sm.vmc_sales_amount,0) +
//                                     nvl(sm.cash_advance_vol_amount,0) -
//                                     least( (nvl(sm.vmc_sales_amount,0) + 
//                                             nvl(sm.cash_advance_vol_amount,0)),
//                                            9.99
//                                           ) 
//                                    ), 
//                                   0, 0, 1 ) )                      as active_count,
//                      decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0)       as cash_advance,          
//                      decode(sm.mes_inventory,'Y',1,0)              as mes_inventory,
//                      decode(sm.liability_contract,'Y',1,0)         as liability_contract,
//                      decode(sm.referral_contract,'Y',1,0)          as referral_contract,                   
//                      decode(sm.reseller_contract,'Y',1,0)          as reseller_contract,                   
//                      decode(sm.cash_advance_referral,'Y',1,0)      as cash_advance_referral,                   
//                      decode(sm.moto_merchant,'Y',1,0)              as moto_merchant,
//                      sum( sm.vmc_sales_count )                     as vmc_sales_count,
//                      sum( sm.vmc_sales_amount )                    as vmc_sales_amount,
//                      sum( sm.vmc_credits_count )                   as vmc_credits_count,
//                      sum( sm.vmc_credits_amount )                  as vmc_credits_amount,
//                      sum( sm.cash_advance_vol_count )              as cash_advance_vol_count,
//                      sum( sm.cash_advance_vol_amount )             as cash_advance_vol_amount,  
//                      sum( sm.TOT_INC_DISCOUNT + 
//                           sm.tot_inc_interchange )                 as disc_ic_inc,
//                      sum( sm.tot_inc_authorization )               as auth_inc,
//                      sum( sm.tot_inc_capture )                     as capture_inc,
//                      sum( sm.tot_inc_debit )                       as debit_inc,
//                      sum( sm.tot_inc_ind_plans )                   as plan_inc,
//                      sum( sm.tot_inc_sys_generated -
//                           (sm.equip_sales_income + 
//                            sm.equip_rental_income +
//                            nvl(sm.equip_sales_tax_collected,0)) 
//                         )                                          as sys_gen_inc,
//                      sum( nvl(sm.mes_only_inc_disc_ic,0) )         as mes_only_disc_ic,
//                      sum( nvl(sm.mes_only_inc_authorization,0) )   as mes_only_auth,
//                      sum( nvl(sm.mes_only_inc_capture,0) )         as mes_only_capture,
//                      sum( nvl(sm.mes_only_inc_debit,0) )           as mes_only_debit,
//                      sum( nvl(sm.mes_only_inc_ind_plans,0) )       as mes_only_plan,
//                      sum( nvl(sm.mes_only_inc_sys_generated,0) )   as mes_only_sys_gen,
//                      sum( sm.equip_sales_income )                  as equip_sales_inc,
//                      sum( sm.equip_rental_income )                 as equip_rental_inc,
//                      sum( nvl(sm.equip_sales_tax_collected,0) )    as equip_sales_tax,
//                      sum( nvl(sm.interchange_expense,0) )          as ic_exp,
//                      sum( nvl(sm.vmc_assessment_expense,0) )       as assessment,                             
//                      sum( nvl(sm.vmc_fees,0) )                     as vmc_fees,
//                      sum( sm.tot_exp_authorization )               as auth_exp,
//                      sum( sm.tot_exp_capture )                     as capture_exp,
//                      sum( sm.tot_exp_debit )                       as debit_exp,
//                      sum( sm.tot_exp_ind_plans )                   as plan_exp,
//                      sum( sm.tot_exp_sys_generated -
//                           (nvl(sm.equip_sales_expense,0) + 
//                            nvl(sm.equip_rental_expense,0)) )       as sys_gen_exp,
//                      sum( nvl(sm.supply_cost,0) )                  as supply_cost,                            
//                      sum( nvl(sm.equip_sales_expense,0) )          as equip_sales_exp,
//                      sum( nvl(sm.equip_rental_expense,0) )         as equip_rental_exp,
//                      sum( nvl(sm.equip_rental_base_cost,0) )       as equip_rental_base_cost,
//                      sum( nvl(sm.equip_sales_base_cost,0) )        as equip_sales_base_cost,
//                      sum( nvl(sm.excluded_equip_rental_amount,0) ) as excluded_equip_rental,
//                      sum( nvl(sm.excluded_equip_sales_amount,0) )  as excluded_equip_sales,
//                      sum( nvl(sm.excluded_fees_amount,0) )         as excluded_fees,
//                      sum( nvl(sm.equip_transfer_cost,0) )          as equip_transfer_cost,
//                      sum( nvl(sm.DISC_IC_DCE_ADJ_AMOUNT,0) )       as disc_ic_adj,
//                      sum( nvl(sm.fee_dce_adj_amount,0) )           as fee_adj,
//                      sum( nvl(mcli.tot_ndr,0) )                    as ndr_liability,   
//                      sum( nvl(mcli.tot_authorization,0) )          as auth_liability,
//                      sum( nvl(mcli.tot_capture,0) )                as capture_liability,
//                      sum( nvl(mcli.tot_debit,0) )                  as debit_liability,
//                      sum( nvl(mcli.tot_ind_plans,0) )              as plan_liability,
//                      sum( nvl(mcli.tot_sys_generated,0) )          as sys_gen_liability,
//                      sum( nvl(mcli.tot_equip_rental,0) )           as equip_rental_liability,
//                      sum( nvl(mcli.tot_equip_sales,0) )            as equip_sales_liability,          
//                      sum( nvl(mcrf.tot_ndr,0) )                    as ndr_referral,   
//                      sum( nvl(mcrf.tot_authorization,0) )          as auth_referral,
//                      sum( nvl(mcrf.tot_capture,0) )                as capture_referral,
//                      sum( nvl(mcrf.tot_debit,0) )                  as debit_referral,
//                      sum( nvl(mcrf.tot_ind_plans,0) )              as plan_referral,
//                      sum( nvl(mcrf.tot_sys_generated,0) )          as sys_gen_referral,          
//                      sum( nvl(mcrf.tot_equip_rental,0) )           as equip_rental_referral,          
//                      sum( nvl(mcrf.tot_equip_sales,0) )            as equip_sales_referral,          
//                      sum( nvl(mcrs.tot_ndr,0) )                    as ndr_reseller,   
//                      sum( nvl(mcrs.tot_authorization,0) )          as auth_reseller,
//                      sum( nvl(mcrs.tot_capture,0) )                as capture_reseller,
//                      sum( nvl(mcrs.tot_debit,0) )                  as debit_reseller,
//                      sum( nvl(mcrs.tot_ind_plans,0) )              as plan_reseller,
//                      sum( nvl(mcrs.tot_sys_generated,0) )          as sys_gen_reseller,          
//                      sum( nvl(mcrs.tot_equip_rental,0) )           as equip_rental_reseller,          
//                      sum( nvl(mcrs.tot_equip_sales,0) )            as equip_sales_reseller,          
//                      sum( nvl(mcex.tot_ndr,0) )                    as ndr_external,   
//                      sum( nvl(mcex.tot_authorization,0) )          as auth_external,
//                      sum( nvl(mcex.tot_capture,0) )                as capture_external,
//                      sum( nvl(mcex.tot_debit,0) )                  as debit_external,
//                      sum( nvl(mcex.tot_ind_plans,0) )              as plan_external,
//                      sum( nvl(mcex.tot_sys_generated,0) )          as sys_gen_external,
//                      sum( nvl(mcex.tot_equip_rental,0) )           as equip_rental_external,
//                      sum( nvl(mcex.tot_equip_sales,0) )            as equip_sales_external,          
//                      sum( nvl(sm.tot_partnership,0) )              as partnership
//            from      organization                o,
//                      group_merchant              gm,
//                      mif                         mf,
//                      monthly_extract_summary     sm,
//                      monthly_extract_contract    mcli,
//                      monthly_extract_contract    mcrf,
//                      monthly_extract_contract    mcrs,
//                      monthly_extract_contract    mcex
//            where     o.org_group = :nodeId and
//                      gm.org_num = o.org_num and
//                      mf.merchant_number = gm.merchant_number and
//                      mf.activation_date between :ReportDateBegin and last_day(:ReportDateEnd) and
//                      sm.merchant_number = mf.merchant_number and
//                      sm.active_date between :ReportDateBegin and trunc( trunc(sysdate,'month')-1,'month' ) and
//                      mcli.hh_load_sec(+) = sm.hh_load_sec and
//                      mcli.merchant_number(+) = sm.merchant_number and
//                      mcli.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_LIABILITY and -- 1 and
//                      mcrf.hh_load_sec(+) = sm.hh_load_sec and
//                      mcrf.merchant_number(+) = sm.merchant_number and
//                      mcrf.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_REFERRAL and -- 2 and
//                      mcrs.hh_load_sec(+) = sm.hh_load_sec and
//                      mcrs.merchant_number(+) = sm.merchant_number and
//                      mcrs.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_RESELLER and -- 3 and
//                      mcex.hh_load_sec(+) = sm.hh_load_sec and
//                      mcex.merchant_number(+) = sm.merchant_number and
//                      mcex.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_THIRD_PARTY -- 4 
//            group by o.org_num, o.org_group, o.org_name, sm.active_date,
//                     decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0),
//                     decode(sm.liability_contract,'Y',1,0),
//                     decode(sm.referral_contract,'Y',1,0),
//                     decode(sm.reseller_contract,'Y',1,0),
//                     decode(sm.cash_advance_referral,'Y',1,0),
//                     decode(sm.moto_merchant,'Y',1,0),
//                     decode(sm.mes_inventory,'Y',1,0)
//            order by o.org_name, o.org_num, sm.active_date        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   o.org_num                                     as org_num,\n                    o.org_group                                   as hierarchy_node,\n                    o.org_name                                    as org_name,\n                    sm.active_date                                as active_date,\n                    -- dates are offset (to TSYS) by one day to allow\n                    -- billing queries to get all items (i.e. service calls)\n                    -- adjust the dates to the TSYS billing dates\n                    min(nvl(sm.month_begin_date+1,sm.active_date))as month_begin_date,\n                    max(sm.month_end_date+1)                      as month_end_date,                  \n                    count(sm.merchant_number)                     as merchant_count,\n                    sum( decode( trunc(mf.activation_date,'month'),\n                                 sm.active_date, 1, 0 ) )         as activated_count,\n                    sum( decode( ( nvl(sm.vmc_sales_amount,0) +\n                                   nvl(sm.cash_advance_vol_amount,0) -\n                                   least( (nvl(sm.vmc_sales_amount,0) + \n                                           nvl(sm.cash_advance_vol_amount,0)),\n                                          9.99\n                                         ) \n                                  ), \n                                 0, 0, 1 ) )                      as active_count,\n                    decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0)       as cash_advance,          \n                    decode(sm.mes_inventory,'Y',1,0)              as mes_inventory,\n                    decode(sm.liability_contract,'Y',1,0)         as liability_contract,\n                    decode(sm.referral_contract,'Y',1,0)          as referral_contract,                   \n                    decode(sm.reseller_contract,'Y',1,0)          as reseller_contract,                   \n                    decode(sm.cash_advance_referral,'Y',1,0)      as cash_advance_referral,                   \n                    decode(sm.moto_merchant,'Y',1,0)              as moto_merchant,\n                    sum( sm.vmc_sales_count )                     as vmc_sales_count,\n                    sum( sm.vmc_sales_amount )                    as vmc_sales_amount,\n                    sum( sm.vmc_credits_count )                   as vmc_credits_count,\n                    sum( sm.vmc_credits_amount )                  as vmc_credits_amount,\n                    sum( sm.cash_advance_vol_count )              as cash_advance_vol_count,\n                    sum( sm.cash_advance_vol_amount )             as cash_advance_vol_amount,  \n                    sum( sm.TOT_INC_DISCOUNT + \n                         sm.tot_inc_interchange )                 as disc_ic_inc,\n                    sum( sm.tot_inc_authorization )               as auth_inc,\n                    sum( sm.tot_inc_capture )                     as capture_inc,\n                    sum( sm.tot_inc_debit )                       as debit_inc,\n                    sum( sm.tot_inc_ind_plans )                   as plan_inc,\n                    sum( sm.tot_inc_sys_generated -\n                         (sm.equip_sales_income + \n                          sm.equip_rental_income +\n                          nvl(sm.equip_sales_tax_collected,0)) \n                       )                                          as sys_gen_inc,\n                    sum( nvl(sm.mes_only_inc_disc_ic,0) )         as mes_only_disc_ic,\n                    sum( nvl(sm.mes_only_inc_authorization,0) )   as mes_only_auth,\n                    sum( nvl(sm.mes_only_inc_capture,0) )         as mes_only_capture,\n                    sum( nvl(sm.mes_only_inc_debit,0) )           as mes_only_debit,\n                    sum( nvl(sm.mes_only_inc_ind_plans,0) )       as mes_only_plan,\n                    sum( nvl(sm.mes_only_inc_sys_generated,0) )   as mes_only_sys_gen,\n                    sum( sm.equip_sales_income )                  as equip_sales_inc,\n                    sum( sm.equip_rental_income )                 as equip_rental_inc,\n                    sum( nvl(sm.equip_sales_tax_collected,0) )    as equip_sales_tax,\n                    sum( nvl(sm.interchange_expense,0) )          as ic_exp,\n                    sum( nvl(sm.vmc_assessment_expense,0) )       as assessment,                             \n                    sum( nvl(sm.vmc_fees,0) )                     as vmc_fees,\n                    sum( sm.tot_exp_authorization )               as auth_exp,\n                    sum( sm.tot_exp_capture )                     as capture_exp,\n                    sum( sm.tot_exp_debit )                       as debit_exp,\n                    sum( sm.tot_exp_ind_plans )                   as plan_exp,\n                    sum( sm.tot_exp_sys_generated -\n                         (nvl(sm.equip_sales_expense,0) + \n                          nvl(sm.equip_rental_expense,0)) )       as sys_gen_exp,\n                    sum( nvl(sm.supply_cost,0) )                  as supply_cost,                            \n                    sum( nvl(sm.equip_sales_expense,0) )          as equip_sales_exp,\n                    sum( nvl(sm.equip_rental_expense,0) )         as equip_rental_exp,\n                    sum( nvl(sm.equip_rental_base_cost,0) )       as equip_rental_base_cost,\n                    sum( nvl(sm.equip_sales_base_cost,0) )        as equip_sales_base_cost,\n                    sum( nvl(sm.excluded_equip_rental_amount,0) ) as excluded_equip_rental,\n                    sum( nvl(sm.excluded_equip_sales_amount,0) )  as excluded_equip_sales,\n                    sum( nvl(sm.excluded_fees_amount,0) )         as excluded_fees,\n                    sum( nvl(sm.equip_transfer_cost,0) )          as equip_transfer_cost,\n                    sum( nvl(sm.DISC_IC_DCE_ADJ_AMOUNT,0) )       as disc_ic_adj,\n                    sum( nvl(sm.fee_dce_adj_amount,0) )           as fee_adj,\n                    sum( nvl(mcli.tot_ndr,0) )                    as ndr_liability,   \n                    sum( nvl(mcli.tot_authorization,0) )          as auth_liability,\n                    sum( nvl(mcli.tot_capture,0) )                as capture_liability,\n                    sum( nvl(mcli.tot_debit,0) )                  as debit_liability,\n                    sum( nvl(mcli.tot_ind_plans,0) )              as plan_liability,\n                    sum( nvl(mcli.tot_sys_generated,0) )          as sys_gen_liability,\n                    sum( nvl(mcli.tot_equip_rental,0) )           as equip_rental_liability,\n                    sum( nvl(mcli.tot_equip_sales,0) )            as equip_sales_liability,          \n                    sum( nvl(mcrf.tot_ndr,0) )                    as ndr_referral,   \n                    sum( nvl(mcrf.tot_authorization,0) )          as auth_referral,\n                    sum( nvl(mcrf.tot_capture,0) )                as capture_referral,\n                    sum( nvl(mcrf.tot_debit,0) )                  as debit_referral,\n                    sum( nvl(mcrf.tot_ind_plans,0) )              as plan_referral,\n                    sum( nvl(mcrf.tot_sys_generated,0) )          as sys_gen_referral,          \n                    sum( nvl(mcrf.tot_equip_rental,0) )           as equip_rental_referral,          \n                    sum( nvl(mcrf.tot_equip_sales,0) )            as equip_sales_referral,          \n                    sum( nvl(mcrs.tot_ndr,0) )                    as ndr_reseller,   \n                    sum( nvl(mcrs.tot_authorization,0) )          as auth_reseller,\n                    sum( nvl(mcrs.tot_capture,0) )                as capture_reseller,\n                    sum( nvl(mcrs.tot_debit,0) )                  as debit_reseller,\n                    sum( nvl(mcrs.tot_ind_plans,0) )              as plan_reseller,\n                    sum( nvl(mcrs.tot_sys_generated,0) )          as sys_gen_reseller,          \n                    sum( nvl(mcrs.tot_equip_rental,0) )           as equip_rental_reseller,          \n                    sum( nvl(mcrs.tot_equip_sales,0) )            as equip_sales_reseller,          \n                    sum( nvl(mcex.tot_ndr,0) )                    as ndr_external,   \n                    sum( nvl(mcex.tot_authorization,0) )          as auth_external,\n                    sum( nvl(mcex.tot_capture,0) )                as capture_external,\n                    sum( nvl(mcex.tot_debit,0) )                  as debit_external,\n                    sum( nvl(mcex.tot_ind_plans,0) )              as plan_external,\n                    sum( nvl(mcex.tot_sys_generated,0) )          as sys_gen_external,\n                    sum( nvl(mcex.tot_equip_rental,0) )           as equip_rental_external,\n                    sum( nvl(mcex.tot_equip_sales,0) )            as equip_sales_external,          \n                    sum( nvl(sm.tot_partnership,0) )              as partnership\n          from      organization                o,\n                    group_merchant              gm,\n                    mif                         mf,\n                    monthly_extract_summary     sm,\n                    monthly_extract_contract    mcli,\n                    monthly_extract_contract    mcrf,\n                    monthly_extract_contract    mcrs,\n                    monthly_extract_contract    mcex\n          where     o.org_group =  :1   and\n                    gm.org_num = o.org_num and\n                    mf.merchant_number = gm.merchant_number and\n                    mf.activation_date between  :2   and last_day( :3  ) and\n                    sm.merchant_number = mf.merchant_number and\n                    sm.active_date between  :4   and trunc( trunc(sysdate,'month')-1,'month' ) and\n                    mcli.hh_load_sec(+) = sm.hh_load_sec and\n                    mcli.merchant_number(+) = sm.merchant_number and\n                    mcli.contract_type(+) =  :5   and -- 1 and\n                    mcrf.hh_load_sec(+) = sm.hh_load_sec and\n                    mcrf.merchant_number(+) = sm.merchant_number and\n                    mcrf.contract_type(+) =  :6   and -- 2 and\n                    mcrs.hh_load_sec(+) = sm.hh_load_sec and\n                    mcrs.merchant_number(+) = sm.merchant_number and\n                    mcrs.contract_type(+) =  :7   and -- 3 and\n                    mcex.hh_load_sec(+) = sm.hh_load_sec and\n                    mcex.merchant_number(+) = sm.merchant_number and\n                    mcex.contract_type(+) =  :8   -- 4 \n          group by o.org_num, o.org_group, o.org_name, sm.active_date,\n                   decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0),\n                   decode(sm.liability_contract,'Y',1,0),\n                   decode(sm.referral_contract,'Y',1,0),\n                   decode(sm.reseller_contract,'Y',1,0),\n                   decode(sm.cash_advance_referral,'Y',1,0),\n                   decode(sm.moto_merchant,'Y',1,0),\n                   decode(sm.mes_inventory,'Y',1,0)\n          order by o.org_name, o.org_num, sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   __sJT_st.setDate(4,ReportDateBegin);
   __sJT_st.setInt(5,ContractTypes.CONTRACT_SOURCE_LIABILITY);
   __sJT_st.setInt(6,ContractTypes.CONTRACT_SOURCE_REFERRAL);
   __sJT_st.setInt(7,ContractTypes.CONTRACT_SOURCE_RESELLER);
   __sJT_st.setInt(8,ContractTypes.CONTRACT_SOURCE_THIRD_PARTY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"25com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4315^9*/
      }
      else    // child
      {
        /*@lineinfo:generated-code*//*@lineinfo:4319^9*/

//  ************************************************************
//  #sql [Ctx] it = { select   o.org_num                                     as org_num,
//                      o.org_group                                   as hierarchy_node,
//                      o.org_name                                    as org_name,
//                      sm.active_date                                as active_date,
//                      -- dates are offset (to TSYS) by one day to allow
//                      -- billing queries to get all items (i.e. service calls)
//                      -- adjust the dates to the TSYS billing dates
//                      min(nvl(sm.month_begin_date+1,sm.active_date))as month_begin_date,
//                      max(sm.month_end_date+1)                      as month_end_date,                  
//                      count(sm.merchant_number)                     as merchant_count,
//                      sum( decode( trunc(mf.activation_date,'month'),
//                                   sm.active_date, 1, 0 ) )         as activated_count,
//                      sum( decode( ( nvl(sm.vmc_sales_amount,0) +
//                                     nvl(sm.cash_advance_vol_amount,0) -
//                                     least( (nvl(sm.vmc_sales_amount,0) + 
//                                             nvl(sm.cash_advance_vol_amount,0)),
//                                            9.99
//                                           ) 
//                                    ), 
//                                   0, 0, 1 ) )                      as active_count,
//                      decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0)       as cash_advance,          
//                      decode(sm.mes_inventory,'Y',1,0)              as mes_inventory,
//                      decode(sm.liability_contract,'Y',1,0)         as liability_contract,
//                      decode(sm.referral_contract,'Y',1,0)          as referral_contract,                   
//                      decode(sm.reseller_contract,'Y',1,0)          as reseller_contract,                   
//                      decode(sm.cash_advance_referral,'Y',1,0)      as cash_advance_referral,                   
//                      decode(sm.moto_merchant,'Y',1,0)              as moto_merchant,
//                      sum( sm.vmc_sales_count )                     as vmc_sales_count,
//                      sum( sm.vmc_sales_amount )                    as vmc_sales_amount,
//                      sum( sm.vmc_credits_count )                   as vmc_credits_count,
//                      sum( sm.vmc_credits_amount )                  as vmc_credits_amount,
//                      sum( sm.cash_advance_vol_count )              as cash_advance_vol_count,
//                      sum( sm.cash_advance_vol_amount )             as cash_advance_vol_amount,  
//                      sum( sm.TOT_INC_DISCOUNT + 
//                           sm.tot_inc_interchange )                 as disc_ic_inc,
//                      sum( sm.tot_inc_authorization )               as auth_inc,
//                      sum( sm.tot_inc_capture )                     as capture_inc,
//                      sum( sm.tot_inc_debit )                       as debit_inc,
//                      sum( sm.tot_inc_ind_plans )                   as plan_inc,
//                      sum( sm.tot_inc_sys_generated -
//                           (sm.equip_sales_income + 
//                            sm.equip_rental_income +
//                            nvl(sm.equip_sales_tax_collected,0)) 
//                         )                                          as sys_gen_inc,
//                      sum( nvl(sm.mes_only_inc_disc_ic,0) )         as mes_only_disc_ic,
//                      sum( nvl(sm.mes_only_inc_authorization,0) )   as mes_only_auth,
//                      sum( nvl(sm.mes_only_inc_capture,0) )         as mes_only_capture,
//                      sum( nvl(sm.mes_only_inc_debit,0) )           as mes_only_debit,
//                      sum( nvl(sm.mes_only_inc_ind_plans,0) )       as mes_only_plan,
//                      sum( nvl(sm.mes_only_inc_sys_generated,0) )   as mes_only_sys_gen,
//                      sum( sm.equip_sales_income )                  as equip_sales_inc,
//                      sum( sm.equip_rental_income )                 as equip_rental_inc,
//                      sum( nvl(sm.equip_sales_tax_collected,0) )    as equip_sales_tax,
//                      sum( nvl(sm.interchange_expense,0) )          as ic_exp,
//                      sum( nvl(sm.vmc_assessment_expense,0) )       as assessment,                             
//                      sum( nvl(sm.vmc_fees,0) )                     as vmc_fees,
//                      sum( sm.tot_exp_authorization )               as auth_exp,
//                      sum( sm.tot_exp_capture )                     as capture_exp,
//                      sum( sm.tot_exp_debit )                       as debit_exp,
//                      sum( sm.tot_exp_ind_plans )                   as plan_exp,
//                      sum( sm.tot_exp_sys_generated -
//                           (nvl(sm.equip_sales_expense,0) + 
//                            nvl(sm.equip_rental_expense,0)) )       as sys_gen_exp,
//                      sum( nvl(sm.supply_cost,0) )                  as supply_cost,                            
//                      sum( nvl(sm.equip_sales_expense,0) )          as equip_sales_exp,
//                      sum( nvl(sm.equip_rental_expense,0) )         as equip_rental_exp,
//                      sum( nvl(sm.equip_rental_base_cost,0) )       as equip_rental_base_cost,
//                      sum( nvl(sm.equip_sales_base_cost,0) )        as equip_sales_base_cost,
//                      sum( nvl(sm.excluded_equip_rental_amount,0) ) as excluded_equip_rental,
//                      sum( nvl(sm.excluded_equip_sales_amount,0) )  as excluded_equip_sales,
//                      sum( nvl(sm.excluded_fees_amount,0) )         as excluded_fees,
//                      sum( nvl(sm.equip_transfer_cost,0) )          as equip_transfer_cost,
//                      sum( nvl(sm.DISC_IC_DCE_ADJ_AMOUNT,0) )       as disc_ic_adj,
//                      sum( nvl(sm.fee_dce_adj_amount,0) )           as fee_adj,
//                      sum( nvl(mcli.tot_ndr,0) )                    as ndr_liability,   
//                      sum( nvl(mcli.tot_authorization,0) )          as auth_liability,
//                      sum( nvl(mcli.tot_capture,0) )                as capture_liability,
//                      sum( nvl(mcli.tot_debit,0) )                  as debit_liability,
//                      sum( nvl(mcli.tot_ind_plans,0) )              as plan_liability,
//                      sum( nvl(mcli.tot_sys_generated,0) )          as sys_gen_liability,
//                      sum( nvl(mcli.tot_equip_rental,0) )           as equip_rental_liability,
//                      sum( nvl(mcli.tot_equip_sales,0) )            as equip_sales_liability,          
//                      sum( nvl(mcrf.tot_ndr,0) )                    as ndr_referral,   
//                      sum( nvl(mcrf.tot_authorization,0) )          as auth_referral,
//                      sum( nvl(mcrf.tot_capture,0) )                as capture_referral,
//                      sum( nvl(mcrf.tot_debit,0) )                  as debit_referral,
//                      sum( nvl(mcrf.tot_ind_plans,0) )              as plan_referral,
//                      sum( nvl(mcrf.tot_sys_generated,0) )          as sys_gen_referral,          
//                      sum( nvl(mcrf.tot_equip_rental,0) )           as equip_rental_referral,          
//                      sum( nvl(mcrf.tot_equip_sales,0) )            as equip_sales_referral,          
//                      sum( nvl(mcrs.tot_ndr,0) )                    as ndr_reseller,   
//                      sum( nvl(mcrs.tot_authorization,0) )          as auth_reseller,
//                      sum( nvl(mcrs.tot_capture,0) )                as capture_reseller,
//                      sum( nvl(mcrs.tot_debit,0) )                  as debit_reseller,
//                      sum( nvl(mcrs.tot_ind_plans,0) )              as plan_reseller,
//                      sum( nvl(mcrs.tot_sys_generated,0) )          as sys_gen_reseller,          
//                      sum( nvl(mcrs.tot_equip_rental,0) )           as equip_rental_reseller,          
//                      sum( nvl(mcrs.tot_equip_sales,0) )            as equip_sales_reseller,          
//                      sum( nvl(mcex.tot_ndr,0) )                    as ndr_external,   
//                      sum( nvl(mcex.tot_authorization,0) )          as auth_external,
//                      sum( nvl(mcex.tot_capture,0) )                as capture_external,
//                      sum( nvl(mcex.tot_debit,0) )                  as debit_external,
//                      sum( nvl(mcex.tot_ind_plans,0) )              as plan_external,
//                      sum( nvl(mcex.tot_sys_generated,0) )          as sys_gen_external,
//                      sum( nvl(mcex.tot_equip_rental,0) )           as equip_rental_external,
//                      sum( nvl(mcex.tot_equip_sales,0) )            as equip_sales_external,          
//                      sum( nvl(sm.tot_partnership,0) )              as partnership
//            from      t_hierarchy                 th,
//                      organization                o,
//                      group_merchant              gm,
//                      mif                         mf,
//                      monthly_extract_summary     sm,
//                      monthly_extract_contract    mcli,
//                      monthly_extract_contract    mcrf,
//                      monthly_extract_contract    mcrs,
//                      monthly_extract_contract    mcex
//            where     th.hier_type = 1 and
//                      th.ancestor = :nodeId and
//                      ( th.relation = 1 or
//                        ( th.ancestor = th.descendent and
//                          th.entity_type = 4 ) 
//                      ) and 
//                      o.org_group = th.descendent and
//                      gm.org_num = o.org_num and
//                      mf.merchant_number = gm.merchant_number and
//                      mf.activation_date between :ReportDateBegin and last_day(:ReportDateEnd) and
//                      sm.merchant_number = mf.merchant_number and
//                      sm.active_date between :ReportDateBegin and trunc( trunc(sysdate,'month')-1,'month' ) and
//                      mcli.hh_load_sec(+) = sm.hh_load_sec and
//                      mcli.merchant_number(+) = sm.merchant_number and
//                      mcli.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_LIABILITY and -- 1 and
//                      mcrf.hh_load_sec(+) = sm.hh_load_sec and
//                      mcrf.merchant_number(+) = sm.merchant_number and
//                      mcrf.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_REFERRAL and -- 2 and
//                      mcrs.hh_load_sec(+) = sm.hh_load_sec and
//                      mcrs.merchant_number(+) = sm.merchant_number and
//                      mcrs.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_RESELLER and -- 3 and
//                      mcex.hh_load_sec(+) = sm.hh_load_sec and
//                      mcex.merchant_number(+) = sm.merchant_number and
//                      mcex.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_THIRD_PARTY -- 4 
//            group by o.org_num, o.org_group, o.org_name, sm.active_date,
//                     decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0),
//                     decode(sm.liability_contract,'Y',1,0),
//                     decode(sm.referral_contract,'Y',1,0),
//                     decode(sm.reseller_contract,'Y',1,0),
//                     decode(sm.cash_advance_referral,'Y',1,0),
//                     decode(sm.moto_merchant,'Y',1,0),
//                     decode(sm.mes_inventory,'Y',1,0)
//            order by o.org_name, o.org_num, sm.active_date        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select   o.org_num                                     as org_num,\n                    o.org_group                                   as hierarchy_node,\n                    o.org_name                                    as org_name,\n                    sm.active_date                                as active_date,\n                    -- dates are offset (to TSYS) by one day to allow\n                    -- billing queries to get all items (i.e. service calls)\n                    -- adjust the dates to the TSYS billing dates\n                    min(nvl(sm.month_begin_date+1,sm.active_date))as month_begin_date,\n                    max(sm.month_end_date+1)                      as month_end_date,                  \n                    count(sm.merchant_number)                     as merchant_count,\n                    sum( decode( trunc(mf.activation_date,'month'),\n                                 sm.active_date, 1, 0 ) )         as activated_count,\n                    sum( decode( ( nvl(sm.vmc_sales_amount,0) +\n                                   nvl(sm.cash_advance_vol_amount,0) -\n                                   least( (nvl(sm.vmc_sales_amount,0) + \n                                           nvl(sm.cash_advance_vol_amount,0)),\n                                          9.99\n                                         ) \n                                  ), \n                                 0, 0, 1 ) )                      as active_count,\n                    decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0)       as cash_advance,          \n                    decode(sm.mes_inventory,'Y',1,0)              as mes_inventory,\n                    decode(sm.liability_contract,'Y',1,0)         as liability_contract,\n                    decode(sm.referral_contract,'Y',1,0)          as referral_contract,                   \n                    decode(sm.reseller_contract,'Y',1,0)          as reseller_contract,                   \n                    decode(sm.cash_advance_referral,'Y',1,0)      as cash_advance_referral,                   \n                    decode(sm.moto_merchant,'Y',1,0)              as moto_merchant,\n                    sum( sm.vmc_sales_count )                     as vmc_sales_count,\n                    sum( sm.vmc_sales_amount )                    as vmc_sales_amount,\n                    sum( sm.vmc_credits_count )                   as vmc_credits_count,\n                    sum( sm.vmc_credits_amount )                  as vmc_credits_amount,\n                    sum( sm.cash_advance_vol_count )              as cash_advance_vol_count,\n                    sum( sm.cash_advance_vol_amount )             as cash_advance_vol_amount,  \n                    sum( sm.TOT_INC_DISCOUNT + \n                         sm.tot_inc_interchange )                 as disc_ic_inc,\n                    sum( sm.tot_inc_authorization )               as auth_inc,\n                    sum( sm.tot_inc_capture )                     as capture_inc,\n                    sum( sm.tot_inc_debit )                       as debit_inc,\n                    sum( sm.tot_inc_ind_plans )                   as plan_inc,\n                    sum( sm.tot_inc_sys_generated -\n                         (sm.equip_sales_income + \n                          sm.equip_rental_income +\n                          nvl(sm.equip_sales_tax_collected,0)) \n                       )                                          as sys_gen_inc,\n                    sum( nvl(sm.mes_only_inc_disc_ic,0) )         as mes_only_disc_ic,\n                    sum( nvl(sm.mes_only_inc_authorization,0) )   as mes_only_auth,\n                    sum( nvl(sm.mes_only_inc_capture,0) )         as mes_only_capture,\n                    sum( nvl(sm.mes_only_inc_debit,0) )           as mes_only_debit,\n                    sum( nvl(sm.mes_only_inc_ind_plans,0) )       as mes_only_plan,\n                    sum( nvl(sm.mes_only_inc_sys_generated,0) )   as mes_only_sys_gen,\n                    sum( sm.equip_sales_income )                  as equip_sales_inc,\n                    sum( sm.equip_rental_income )                 as equip_rental_inc,\n                    sum( nvl(sm.equip_sales_tax_collected,0) )    as equip_sales_tax,\n                    sum( nvl(sm.interchange_expense,0) )          as ic_exp,\n                    sum( nvl(sm.vmc_assessment_expense,0) )       as assessment,                             \n                    sum( nvl(sm.vmc_fees,0) )                     as vmc_fees,\n                    sum( sm.tot_exp_authorization )               as auth_exp,\n                    sum( sm.tot_exp_capture )                     as capture_exp,\n                    sum( sm.tot_exp_debit )                       as debit_exp,\n                    sum( sm.tot_exp_ind_plans )                   as plan_exp,\n                    sum( sm.tot_exp_sys_generated -\n                         (nvl(sm.equip_sales_expense,0) + \n                          nvl(sm.equip_rental_expense,0)) )       as sys_gen_exp,\n                    sum( nvl(sm.supply_cost,0) )                  as supply_cost,                            \n                    sum( nvl(sm.equip_sales_expense,0) )          as equip_sales_exp,\n                    sum( nvl(sm.equip_rental_expense,0) )         as equip_rental_exp,\n                    sum( nvl(sm.equip_rental_base_cost,0) )       as equip_rental_base_cost,\n                    sum( nvl(sm.equip_sales_base_cost,0) )        as equip_sales_base_cost,\n                    sum( nvl(sm.excluded_equip_rental_amount,0) ) as excluded_equip_rental,\n                    sum( nvl(sm.excluded_equip_sales_amount,0) )  as excluded_equip_sales,\n                    sum( nvl(sm.excluded_fees_amount,0) )         as excluded_fees,\n                    sum( nvl(sm.equip_transfer_cost,0) )          as equip_transfer_cost,\n                    sum( nvl(sm.DISC_IC_DCE_ADJ_AMOUNT,0) )       as disc_ic_adj,\n                    sum( nvl(sm.fee_dce_adj_amount,0) )           as fee_adj,\n                    sum( nvl(mcli.tot_ndr,0) )                    as ndr_liability,   \n                    sum( nvl(mcli.tot_authorization,0) )          as auth_liability,\n                    sum( nvl(mcli.tot_capture,0) )                as capture_liability,\n                    sum( nvl(mcli.tot_debit,0) )                  as debit_liability,\n                    sum( nvl(mcli.tot_ind_plans,0) )              as plan_liability,\n                    sum( nvl(mcli.tot_sys_generated,0) )          as sys_gen_liability,\n                    sum( nvl(mcli.tot_equip_rental,0) )           as equip_rental_liability,\n                    sum( nvl(mcli.tot_equip_sales,0) )            as equip_sales_liability,          \n                    sum( nvl(mcrf.tot_ndr,0) )                    as ndr_referral,   \n                    sum( nvl(mcrf.tot_authorization,0) )          as auth_referral,\n                    sum( nvl(mcrf.tot_capture,0) )                as capture_referral,\n                    sum( nvl(mcrf.tot_debit,0) )                  as debit_referral,\n                    sum( nvl(mcrf.tot_ind_plans,0) )              as plan_referral,\n                    sum( nvl(mcrf.tot_sys_generated,0) )          as sys_gen_referral,          \n                    sum( nvl(mcrf.tot_equip_rental,0) )           as equip_rental_referral,          \n                    sum( nvl(mcrf.tot_equip_sales,0) )            as equip_sales_referral,          \n                    sum( nvl(mcrs.tot_ndr,0) )                    as ndr_reseller,   \n                    sum( nvl(mcrs.tot_authorization,0) )          as auth_reseller,\n                    sum( nvl(mcrs.tot_capture,0) )                as capture_reseller,\n                    sum( nvl(mcrs.tot_debit,0) )                  as debit_reseller,\n                    sum( nvl(mcrs.tot_ind_plans,0) )              as plan_reseller,\n                    sum( nvl(mcrs.tot_sys_generated,0) )          as sys_gen_reseller,          \n                    sum( nvl(mcrs.tot_equip_rental,0) )           as equip_rental_reseller,          \n                    sum( nvl(mcrs.tot_equip_sales,0) )            as equip_sales_reseller,          \n                    sum( nvl(mcex.tot_ndr,0) )                    as ndr_external,   \n                    sum( nvl(mcex.tot_authorization,0) )          as auth_external,\n                    sum( nvl(mcex.tot_capture,0) )                as capture_external,\n                    sum( nvl(mcex.tot_debit,0) )                  as debit_external,\n                    sum( nvl(mcex.tot_ind_plans,0) )              as plan_external,\n                    sum( nvl(mcex.tot_sys_generated,0) )          as sys_gen_external,\n                    sum( nvl(mcex.tot_equip_rental,0) )           as equip_rental_external,\n                    sum( nvl(mcex.tot_equip_sales,0) )            as equip_sales_external,          \n                    sum( nvl(sm.tot_partnership,0) )              as partnership\n          from      t_hierarchy                 th,\n                    organization                o,\n                    group_merchant              gm,\n                    mif                         mf,\n                    monthly_extract_summary     sm,\n                    monthly_extract_contract    mcli,\n                    monthly_extract_contract    mcrf,\n                    monthly_extract_contract    mcrs,\n                    monthly_extract_contract    mcex\n          where     th.hier_type = 1 and\n                    th.ancestor =  :1   and\n                    ( th.relation = 1 or\n                      ( th.ancestor = th.descendent and\n                        th.entity_type = 4 ) \n                    ) and \n                    o.org_group = th.descendent and\n                    gm.org_num = o.org_num and\n                    mf.merchant_number = gm.merchant_number and\n                    mf.activation_date between  :2   and last_day( :3  ) and\n                    sm.merchant_number = mf.merchant_number and\n                    sm.active_date between  :4   and trunc( trunc(sysdate,'month')-1,'month' ) and\n                    mcli.hh_load_sec(+) = sm.hh_load_sec and\n                    mcli.merchant_number(+) = sm.merchant_number and\n                    mcli.contract_type(+) =  :5   and -- 1 and\n                    mcrf.hh_load_sec(+) = sm.hh_load_sec and\n                    mcrf.merchant_number(+) = sm.merchant_number and\n                    mcrf.contract_type(+) =  :6   and -- 2 and\n                    mcrs.hh_load_sec(+) = sm.hh_load_sec and\n                    mcrs.merchant_number(+) = sm.merchant_number and\n                    mcrs.contract_type(+) =  :7   and -- 3 and\n                    mcex.hh_load_sec(+) = sm.hh_load_sec and\n                    mcex.merchant_number(+) = sm.merchant_number and\n                    mcex.contract_type(+) =  :8   -- 4 \n          group by o.org_num, o.org_group, o.org_name, sm.active_date,\n                   decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0),\n                   decode(sm.liability_contract,'Y',1,0),\n                   decode(sm.referral_contract,'Y',1,0),\n                   decode(sm.reseller_contract,'Y',1,0),\n                   decode(sm.cash_advance_referral,'Y',1,0),\n                   decode(sm.moto_merchant,'Y',1,0),\n                   decode(sm.mes_inventory,'Y',1,0)\n          order by o.org_name, o.org_num, sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   __sJT_st.setDate(4,ReportDateBegin);
   __sJT_st.setInt(5,ContractTypes.CONTRACT_SOURCE_LIABILITY);
   __sJT_st.setInt(6,ContractTypes.CONTRACT_SOURCE_REFERRAL);
   __sJT_st.setInt(7,ContractTypes.CONTRACT_SOURCE_RESELLER);
   __sJT_st.setInt(8,ContractTypes.CONTRACT_SOURCE_THIRD_PARTY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"26com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4470^9*/
      }        
      resultSet = it.getResultSet();
      while( resultSet.next() )
      {
        orgId       = resultSet.getLong("org_num");
        activeDate  = resultSet.getDate("active_date");
    
        if ( orgId != lastOrgId || !activeDate.equals(lastActiveDate) )
        {
          if ( appData == null || appData.OrgId != orgId )
          {
            appData = new RetentionAppData( resultSet );
          }
          
          row = new RetentionSummaryData( appData, resultSet, userType );
          ReportRows.add(row);
          
          // store the last values
          lastOrgId = orgId;
          lastActiveDate = activeDate;
        }
        row.addData( resultSet );
      }        
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadRetentionData()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }

  public void loadUnderWaterMerchants( )
  {
    boolean                     agentBankUser     = isUserUnderContract();
    int                         isLiabilityBank   = 0;
    ResultSetIterator           it                = null;
    long                        lastOrgId         = -1L;
    long                        nodeId            = getReportHierarchyNode();
    long                        orgId             = 0L;
    ResultSet                   resultSet         = null;
    SummaryData                 row               = null;
    double                      threshold         = -(fields.getField("uwThreshold").asDouble());
    int                         userType          = getUserType();
    
    try
    {
      ReportRows.removeAllElements();
      
      if ( hasParentLiabilityContract() && ! isBankUser() )
      {
        isLiabilityBank = 1;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:4527^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    /*+
//                        index (po idx_parent_org_num)
//                        index (gm pkgroup_merchant)
//                        index (sm pk_monthly_extract_summary)
//                     */
//                    mo.org_num                                    as org_num,
//                    mo.org_group                                  as hierarchy_node,
//                    mo.org_name                                   as org_name,
//                    thc.ancestor                                  as portfolio_node,
//                    count(sm.merchant_number)                     as merchant_count,
//                    decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0)       as cash_advance,          
//                    decode(sm.mes_inventory,'Y',1,0)              as mes_inventory,
//                    decode(sm.liability_contract,'Y',1,0)         as liability_contract,
//                    decode(sm.referral_contract,'Y',1,0)          as referral_contract,                   
//                    decode(sm.reseller_contract,'Y',1,0)          as reseller_contract,                   
//                    decode(sm.cash_advance_referral,'Y',1,0)      as cash_advance_referral,                   
//                    decode(sm.moto_merchant,'Y',1,0)              as moto_merchant,
//                    sum( sm.vmc_sales_count )                     as vmc_sales_count,
//                    sum( sm.vmc_sales_amount )                    as vmc_sales_amount,
//                    sum( sm.vmc_credits_count )                   as vmc_credits_count,
//                    sum( sm.vmc_credits_amount )                  as vmc_credits_amount,
//                    sum( sm.cash_advance_vol_count )              as cash_advance_vol_count,
//                    sum( sm.cash_advance_vol_amount )             as cash_advance_vol_amount,  
//                    sum( sm.TOT_INC_DISCOUNT + 
//                         sm.tot_inc_interchange )                 as disc_ic_inc,
//                    sum( sm.tot_inc_authorization )               as auth_inc,
//                    sum( sm.tot_inc_capture )                     as capture_inc,
//                    sum( sm.tot_inc_debit )                       as debit_inc,
//                    sum( sm.tot_inc_ind_plans )                   as plan_inc,
//                    sum( sm.tot_inc_sys_generated -
//                         (sm.equip_sales_income + 
//                          sm.equip_rental_income +
//                          nvl(sm.equip_sales_tax_collected,0)) 
//                       )                                          as sys_gen_inc,
//                    sum( nvl(sm.mes_only_inc_disc_ic,0) )         as mes_only_disc_ic,
//                    sum( nvl(sm.mes_only_inc_authorization,0) )   as mes_only_auth,
//                    sum( nvl(sm.mes_only_inc_capture,0) )         as mes_only_capture,
//                    sum( nvl(sm.mes_only_inc_debit,0) )           as mes_only_debit,
//                    sum( nvl(sm.mes_only_inc_ind_plans,0) )       as mes_only_plan,
//                    sum( nvl(sm.mes_only_inc_sys_generated,0) )   as mes_only_sys_gen,
//                    sum( sm.equip_sales_income )                  as equip_sales_inc,
//                    sum( sm.equip_rental_income )                 as equip_rental_inc,
//                    sum( nvl(sm.equip_sales_tax_collected,0) )    as equip_sales_tax,
//                    sum( nvl(sm.interchange_expense,0) )          as ic_exp,
//                    sum( nvl(sm.vmc_assessment_expense,0) )       as assessment,                             
//                    sum( nvl(sm.vmc_fees,0) )                     as vmc_fees,
//                    sum( sm.tot_exp_authorization )               as auth_exp,
//                    sum( sm.tot_exp_capture )                     as capture_exp,
//                    sum( sm.tot_exp_debit )                       as debit_exp,
//                    sum( sm.tot_exp_ind_plans )                   as plan_exp,
//                    sum( sm.tot_exp_sys_generated -
//                         (nvl(sm.equip_sales_expense,0) + 
//                          nvl(sm.equip_rental_expense,0)) )       as sys_gen_exp,
//                    sum( nvl(sm.supply_cost,0) )                  as supply_cost,                            
//                    sum( sm.equip_sales_expense )                 as equip_sales_exp,
//                    sum( sm.equip_rental_expense )                as equip_rental_exp,
//                    sum( nvl(sm.equip_rental_base_cost,0) )       as equip_rental_base_cost,
//                    sum( nvl(sm.equip_sales_base_cost,0) )        as equip_sales_base_cost,
//                    sum( nvl(sm.excluded_equip_rental_amount,0) ) as excluded_equip_rental,
//                    sum( nvl(sm.excluded_equip_sales_amount,0) )  as excluded_equip_sales,
//                    sum( nvl(sm.excluded_fees_amount,0) )         as excluded_fees,
//                    sum( nvl(sm.equip_transfer_cost,0) )          as equip_transfer_cost,
//                    sum( sm.DISC_IC_DCE_ADJ_AMOUNT )              as disc_ic_adj,
//                    sum( sm.fee_dce_adj_amount )                  as fee_adj,
//                    sum( nvl(mcli.tot_ndr,0) )                    as ndr_liability,   
//                    sum( nvl(mcli.tot_authorization,0) )          as auth_liability,
//                    sum( nvl(mcli.tot_capture,0) )                as capture_liability,
//                    sum( nvl(mcli.tot_debit,0) )                  as debit_liability,
//                    sum( nvl(mcli.tot_ind_plans,0) )              as plan_liability,
//                    sum( nvl(mcli.tot_sys_generated,0) )          as sys_gen_liability,
//                    sum( nvl(mcli.tot_equip_rental,0) )           as equip_rental_liability,
//                    sum( nvl(mcli.tot_equip_sales,0) )            as equip_sales_liability,          
//                    sum( nvl(mcrf.tot_ndr,0) )                    as ndr_referral,   
//                    sum( nvl(mcrf.tot_authorization,0) )          as auth_referral,
//                    sum( nvl(mcrf.tot_capture,0) )                as capture_referral,
//                    sum( nvl(mcrf.tot_debit,0) )                  as debit_referral,
//                    sum( nvl(mcrf.tot_ind_plans,0) )              as plan_referral,
//                    sum( nvl(mcrf.tot_sys_generated,0) )          as sys_gen_referral,          
//                    sum( nvl(mcrf.tot_equip_rental,0) )           as equip_rental_referral,          
//                    sum( nvl(mcrf.tot_equip_sales,0) )            as equip_sales_referral,          
//                    sum( nvl(mcrs.tot_ndr,0) )                    as ndr_reseller,   
//                    sum( nvl(mcrs.tot_authorization,0) )          as auth_reseller,
//                    sum( nvl(mcrs.tot_capture,0) )                as capture_reseller,
//                    sum( nvl(mcrs.tot_debit,0) )                  as debit_reseller,
//                    sum( nvl(mcrs.tot_ind_plans,0) )              as plan_reseller,
//                    sum( nvl(mcrs.tot_sys_generated,0) )          as sys_gen_reseller,          
//                    sum( nvl(mcrs.tot_equip_rental,0) )           as equip_rental_reseller,          
//                    sum( nvl(mcrs.tot_equip_sales,0) )            as equip_sales_reseller,          
//                    sum( nvl(mcex.tot_ndr,0) )                    as ndr_external,   
//                    sum( nvl(mcex.tot_authorization,0) )          as auth_external,
//                    sum( nvl(mcex.tot_capture,0) )                as capture_external,
//                    sum( nvl(mcex.tot_debit,0) )                  as debit_external,
//                    sum( nvl(mcex.tot_ind_plans,0) )              as plan_external,
//                    sum( nvl(mcex.tot_sys_generated,0) )          as sys_gen_external,
//                    sum( nvl(mcex.tot_equip_rental,0) )           as equip_rental_external,
//                    sum( nvl(mcex.tot_equip_sales,0) )            as equip_sales_external,          
//                    sum(sm.tot_partnership)                       as partnership                                                      
//          from      t_hierarchy                 th,
//                    t_hierarchy                 thc,
//                    mif                         mf,
//                    monthly_extract_summary     sm,
//                    monthly_extract_contract    mcli,
//                    monthly_extract_contract    mcrf,
//                    monthly_extract_contract    mcrs,
//                    monthly_extract_contract    mcex,
//                    organization                mo
//          where     th.hier_type = 1 and
//                    th.ancestor = :nodeId and
//                    th.relation = 1 and
//                    thc.hier_type = 1 and
//                    thc.ancestor = th.descendent and
//                    thc.entity_type = 4 and
//                    mf.association_node = thc.descendent and
//                    sm.merchant_number = mf.merchant_number and
//                    sm.active_date = :ReportDateBegin and
//                    nvl(sm.cash_advance_account,'N') = 'N' and
//                    mcli.hh_load_sec(+) = sm.hh_load_sec and
//                    mcli.merchant_number(+) = sm.merchant_number and
//                    mcli.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_LIABILITY and -- 1 and
//                    mcrf.hh_load_sec(+) = sm.hh_load_sec and
//                    mcrf.merchant_number(+) = sm.merchant_number and
//                    mcrf.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_REFERRAL and -- 2 and
//                    mcrs.hh_load_sec(+) = sm.hh_load_sec and
//                    mcrs.merchant_number(+) = sm.merchant_number and
//                    mcrs.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_RESELLER and -- 3 and
//                    mcex.hh_load_sec(+) = sm.hh_load_sec and
//                    mcex.merchant_number(+) = sm.merchant_number and
//                    mcex.contract_type(+) = :ContractTypes.CONTRACT_SOURCE_THIRD_PARTY and -- 4 and
//                    (
//                      ( 
//                        :userType = :UT_RESELLER and -- 1 and
//                        ( (nvl(mcli.tot_ndr,0) +
//                           nvl(mcli.tot_authorization,0) +
//                           nvl(mcli.tot_capture,0) +
//                           nvl(mcli.tot_debit,0) +
//                           nvl(mcli.tot_ind_plans,0) +
//                           nvl(mcli.tot_sys_generated,0) +
//                           nvl(mcli.tot_equip_rental,0) +
//                           nvl(mcli.tot_equip_sales,0)) -
//                          (nvl(mcrs.tot_ndr,0) +
//                           nvl(mcrs.tot_authorization,0) +
//                           nvl(mcrs.tot_capture,0) +
//                           nvl(mcrs.tot_debit,0) +
//                           nvl(mcrs.tot_ind_plans,0) +
//                           nvl(mcrs.tot_sys_generated,0) +
//                           nvl(mcrs.tot_equip_sales,0) +
//                           nvl(mcrs.tot_equip_rental,0) +
//                           nvl(mcrf.tot_ndr,0) +
//                           nvl(mcrf.tot_authorization,0) +
//                           nvl(mcrf.tot_capture,0) +
//                           nvl(mcrf.tot_debit,0) +
//                           nvl(mcrf.tot_ind_plans,0) +
//                           nvl(mcrf.tot_sys_generated,0) +
//                           nvl(mcrf.tot_equip_sales,0) +
//                           nvl(mcrf.tot_equip_rental,0)) ) < :threshold
//                      )
//                      or
//                      ( 
//                        :userType = :UT_CONTRACT_BANK and -- 2 and
//                        ( (sm.tot_inc_discount +
//                           sm.tot_inc_interchange +
//                           sm.tot_inc_authorization +
//                           sm.tot_inc_capture +
//                           sm.tot_inc_debit +
//                           sm.tot_inc_ind_plans +
//                           sm.tot_inc_sys_generated +
//                           nvl(mcrf.tot_ndr,0) +
//                           nvl(mcrf.tot_authorization,0) +
//                           nvl(mcrf.tot_capture,0) +
//                           nvl(mcrf.tot_debit,0) +
//                           nvl(mcrf.tot_ind_plans,0) +
//                           nvl(mcrf.tot_sys_generated,0) +
//                           nvl(mcrf.tot_equip_rental,0) +
//                           nvl(mcrf.tot_equip_sales,0) +
//                           nvl(sm.disc_ic_dce_adj_amount,0) + 
//                           nvl(sm.fee_dce_adj_amount,0)) -
//                          (nvl(sm.interchange_expense,0) +
//                           sm.vmc_assessment_expense + 
//                           nvl(mcli.tot_ndr,0) +
//                           nvl(mcli.tot_authorization,0) +
//                           nvl(mcli.tot_capture,0) +
//                           nvl(mcli.tot_debit,0) +
//                           nvl(mcli.tot_ind_plans,0) +
//                           nvl(mcli.tot_sys_generated,0) +
//                           nvl(mcli.tot_equip_rental,0) +
//                           nvl(mcli.tot_equip_sales,0) +
//                           nvl(mcex.tot_ndr,0) +
//                           nvl(mcex.tot_authorization,0) +
//                           nvl(mcex.tot_capture,0) +
//                           nvl(mcex.tot_debit,0) +
//                           nvl(mcex.tot_ind_plans,0) +
//                           nvl(mcex.tot_sys_generated,0) +
//                           nvl(mcex.tot_equip_rental,0) +
//                           nvl(mcex.tot_equip_sales,0)) ) < :threshold
//                      )
//                      or
//                      ( 
//                        :userType = :UT_VAR_BANK and -- 3 and
//                        ( (sm.tot_inc_discount +
//                           sm.tot_inc_interchange +
//                           sm.tot_inc_authorization +
//                           sm.tot_inc_capture +
//                           sm.tot_inc_debit +
//                           sm.tot_inc_ind_plans +
//                           sm.tot_inc_sys_generated +
//                           nvl(sm.disc_ic_dce_adj_amount,0) + 
//                           nvl(sm.fee_dce_adj_amount,0) +
//                           nvl(mcrf.tot_ndr,0) +
//                           nvl(mcrf.tot_authorization,0) +
//                           nvl(mcrf.tot_capture,0) +
//                           nvl(mcrf.tot_debit,0) +
//                           nvl(mcrf.tot_ind_plans,0) +
//                           nvl(mcrf.tot_sys_generated,0) +
//                           nvl(mcrf.tot_equip_rental,0) +
//                           nvl(mcrf.tot_equip_sales,0) ) -
//                          (nvl(sm.interchange_expense,0) +
//                           sm.vmc_assessment_expense + 
//                           sm.tot_exp_authorization +
//                           sm.tot_exp_capture +
//                           sm.tot_exp_debit +
//                           sm.tot_exp_ind_plans +
//                           sm.tot_exp_sys_generated) ) < :threshold
//                      )
//                      or
//                      ( 
//                        -- only review direct accounts for mes
//                        :userType = :UT_MES and -- 0 and
//                        nvl(sm.liability_contract,'N') = 'N' and
//                        nvl(sm.reseller_contract,'N') = 'N' and
//                        ( (sm.tot_inc_discount +
//                           sm.tot_inc_interchange +
//                           sm.tot_inc_authorization +
//                           sm.tot_inc_capture +
//                           sm.tot_inc_debit +
//                           sm.tot_inc_ind_plans +
//                           sm.tot_inc_sys_generated +
//                           nvl(sm.disc_ic_dce_adj_amount,0) + 
//                           nvl(sm.fee_dce_adj_amount,0)) -
//                          (nvl(sm.interchange_expense,0) +
//                           sm.vmc_assessment_expense +
//                           sm.tot_exp_authorization +
//                           sm.tot_exp_capture +
//                           sm.tot_exp_debit +
//                           sm.tot_exp_ind_plans +
//                           sm.tot_exp_sys_generated +
//                           nvl(mcrf.tot_ndr,0) +
//                           nvl(mcrf.tot_authorization,0) +
//                           nvl(mcrf.tot_capture,0) +
//                           nvl(mcrf.tot_debit,0) +
//                           nvl(mcrf.tot_ind_plans,0) +
//                           nvl(mcrf.tot_sys_generated,0) +
//                           nvl(mcrf.tot_equip_rental,0) +
//                           nvl(mcrf.tot_equip_sales,0)) ) < :threshold
//                      ) 
//                    ) and
//                    mo.org_group = sm.merchant_number
//          group by  mo.org_num, mo.org_group, mo.org_name,
//                    thc.ancestor,
//                    decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0),
//                    decode(sm.liability_contract,'Y',1,0),
//                    decode(sm.referral_contract,'Y',1,0),
//                    decode(sm.reseller_contract,'Y',1,0),
//                    decode(sm.cash_advance_referral,'Y',1,0),
//                    decode(sm.moto_merchant,'Y',1,0),
//                    decode(sm.mes_inventory,'Y',1,0)
//          order by thc.ancestor, mo.org_name, mo.org_num
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    /*+\n                      index (po idx_parent_org_num)\n                      index (gm pkgroup_merchant)\n                      index (sm pk_monthly_extract_summary)\n                   */\n                  mo.org_num                                    as org_num,\n                  mo.org_group                                  as hierarchy_node,\n                  mo.org_name                                   as org_name,\n                  thc.ancestor                                  as portfolio_node,\n                  count(sm.merchant_number)                     as merchant_count,\n                  decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0)       as cash_advance,          \n                  decode(sm.mes_inventory,'Y',1,0)              as mes_inventory,\n                  decode(sm.liability_contract,'Y',1,0)         as liability_contract,\n                  decode(sm.referral_contract,'Y',1,0)          as referral_contract,                   \n                  decode(sm.reseller_contract,'Y',1,0)          as reseller_contract,                   \n                  decode(sm.cash_advance_referral,'Y',1,0)      as cash_advance_referral,                   \n                  decode(sm.moto_merchant,'Y',1,0)              as moto_merchant,\n                  sum( sm.vmc_sales_count )                     as vmc_sales_count,\n                  sum( sm.vmc_sales_amount )                    as vmc_sales_amount,\n                  sum( sm.vmc_credits_count )                   as vmc_credits_count,\n                  sum( sm.vmc_credits_amount )                  as vmc_credits_amount,\n                  sum( sm.cash_advance_vol_count )              as cash_advance_vol_count,\n                  sum( sm.cash_advance_vol_amount )             as cash_advance_vol_amount,  \n                  sum( sm.TOT_INC_DISCOUNT + \n                       sm.tot_inc_interchange )                 as disc_ic_inc,\n                  sum( sm.tot_inc_authorization )               as auth_inc,\n                  sum( sm.tot_inc_capture )                     as capture_inc,\n                  sum( sm.tot_inc_debit )                       as debit_inc,\n                  sum( sm.tot_inc_ind_plans )                   as plan_inc,\n                  sum( sm.tot_inc_sys_generated -\n                       (sm.equip_sales_income + \n                        sm.equip_rental_income +\n                        nvl(sm.equip_sales_tax_collected,0)) \n                     )                                          as sys_gen_inc,\n                  sum( nvl(sm.mes_only_inc_disc_ic,0) )         as mes_only_disc_ic,\n                  sum( nvl(sm.mes_only_inc_authorization,0) )   as mes_only_auth,\n                  sum( nvl(sm.mes_only_inc_capture,0) )         as mes_only_capture,\n                  sum( nvl(sm.mes_only_inc_debit,0) )           as mes_only_debit,\n                  sum( nvl(sm.mes_only_inc_ind_plans,0) )       as mes_only_plan,\n                  sum( nvl(sm.mes_only_inc_sys_generated,0) )   as mes_only_sys_gen,\n                  sum( sm.equip_sales_income )                  as equip_sales_inc,\n                  sum( sm.equip_rental_income )                 as equip_rental_inc,\n                  sum( nvl(sm.equip_sales_tax_collected,0) )    as equip_sales_tax,\n                  sum( nvl(sm.interchange_expense,0) )          as ic_exp,\n                  sum( nvl(sm.vmc_assessment_expense,0) )       as assessment,                             \n                  sum( nvl(sm.vmc_fees,0) )                     as vmc_fees,\n                  sum( sm.tot_exp_authorization )               as auth_exp,\n                  sum( sm.tot_exp_capture )                     as capture_exp,\n                  sum( sm.tot_exp_debit )                       as debit_exp,\n                  sum( sm.tot_exp_ind_plans )                   as plan_exp,\n                  sum( sm.tot_exp_sys_generated -\n                       (nvl(sm.equip_sales_expense,0) + \n                        nvl(sm.equip_rental_expense,0)) )       as sys_gen_exp,\n                  sum( nvl(sm.supply_cost,0) )                  as supply_cost,                            \n                  sum( sm.equip_sales_expense )                 as equip_sales_exp,\n                  sum( sm.equip_rental_expense )                as equip_rental_exp,\n                  sum( nvl(sm.equip_rental_base_cost,0) )       as equip_rental_base_cost,\n                  sum( nvl(sm.equip_sales_base_cost,0) )        as equip_sales_base_cost,\n                  sum( nvl(sm.excluded_equip_rental_amount,0) ) as excluded_equip_rental,\n                  sum( nvl(sm.excluded_equip_sales_amount,0) )  as excluded_equip_sales,\n                  sum( nvl(sm.excluded_fees_amount,0) )         as excluded_fees,\n                  sum( nvl(sm.equip_transfer_cost,0) )          as equip_transfer_cost,\n                  sum( sm.DISC_IC_DCE_ADJ_AMOUNT )              as disc_ic_adj,\n                  sum( sm.fee_dce_adj_amount )                  as fee_adj,\n                  sum( nvl(mcli.tot_ndr,0) )                    as ndr_liability,   \n                  sum( nvl(mcli.tot_authorization,0) )          as auth_liability,\n                  sum( nvl(mcli.tot_capture,0) )                as capture_liability,\n                  sum( nvl(mcli.tot_debit,0) )                  as debit_liability,\n                  sum( nvl(mcli.tot_ind_plans,0) )              as plan_liability,\n                  sum( nvl(mcli.tot_sys_generated,0) )          as sys_gen_liability,\n                  sum( nvl(mcli.tot_equip_rental,0) )           as equip_rental_liability,\n                  sum( nvl(mcli.tot_equip_sales,0) )            as equip_sales_liability,          \n                  sum( nvl(mcrf.tot_ndr,0) )                    as ndr_referral,   \n                  sum( nvl(mcrf.tot_authorization,0) )          as auth_referral,\n                  sum( nvl(mcrf.tot_capture,0) )                as capture_referral,\n                  sum( nvl(mcrf.tot_debit,0) )                  as debit_referral,\n                  sum( nvl(mcrf.tot_ind_plans,0) )              as plan_referral,\n                  sum( nvl(mcrf.tot_sys_generated,0) )          as sys_gen_referral,          \n                  sum( nvl(mcrf.tot_equip_rental,0) )           as equip_rental_referral,          \n                  sum( nvl(mcrf.tot_equip_sales,0) )            as equip_sales_referral,          \n                  sum( nvl(mcrs.tot_ndr,0) )                    as ndr_reseller,   \n                  sum( nvl(mcrs.tot_authorization,0) )          as auth_reseller,\n                  sum( nvl(mcrs.tot_capture,0) )                as capture_reseller,\n                  sum( nvl(mcrs.tot_debit,0) )                  as debit_reseller,\n                  sum( nvl(mcrs.tot_ind_plans,0) )              as plan_reseller,\n                  sum( nvl(mcrs.tot_sys_generated,0) )          as sys_gen_reseller,          \n                  sum( nvl(mcrs.tot_equip_rental,0) )           as equip_rental_reseller,          \n                  sum( nvl(mcrs.tot_equip_sales,0) )            as equip_sales_reseller,          \n                  sum( nvl(mcex.tot_ndr,0) )                    as ndr_external,   \n                  sum( nvl(mcex.tot_authorization,0) )          as auth_external,\n                  sum( nvl(mcex.tot_capture,0) )                as capture_external,\n                  sum( nvl(mcex.tot_debit,0) )                  as debit_external,\n                  sum( nvl(mcex.tot_ind_plans,0) )              as plan_external,\n                  sum( nvl(mcex.tot_sys_generated,0) )          as sys_gen_external,\n                  sum( nvl(mcex.tot_equip_rental,0) )           as equip_rental_external,\n                  sum( nvl(mcex.tot_equip_sales,0) )            as equip_sales_external,          \n                  sum(sm.tot_partnership)                       as partnership                                                      \n        from      t_hierarchy                 th,\n                  t_hierarchy                 thc,\n                  mif                         mf,\n                  monthly_extract_summary     sm,\n                  monthly_extract_contract    mcli,\n                  monthly_extract_contract    mcrf,\n                  monthly_extract_contract    mcrs,\n                  monthly_extract_contract    mcex,\n                  organization                mo\n        where     th.hier_type = 1 and\n                  th.ancestor =  :1   and\n                  th.relation = 1 and\n                  thc.hier_type = 1 and\n                  thc.ancestor = th.descendent and\n                  thc.entity_type = 4 and\n                  mf.association_node = thc.descendent and\n                  sm.merchant_number = mf.merchant_number and\n                  sm.active_date =  :2   and\n                  nvl(sm.cash_advance_account,'N') = 'N' and\n                  mcli.hh_load_sec(+) = sm.hh_load_sec and\n                  mcli.merchant_number(+) = sm.merchant_number and\n                  mcli.contract_type(+) =  :3   and -- 1 and\n                  mcrf.hh_load_sec(+) = sm.hh_load_sec and\n                  mcrf.merchant_number(+) = sm.merchant_number and\n                  mcrf.contract_type(+) =  :4   and -- 2 and\n                  mcrs.hh_load_sec(+) = sm.hh_load_sec and\n                  mcrs.merchant_number(+) = sm.merchant_number and\n                  mcrs.contract_type(+) =  :5   and -- 3 and\n                  mcex.hh_load_sec(+) = sm.hh_load_sec and\n                  mcex.merchant_number(+) = sm.merchant_number and\n                  mcex.contract_type(+) =  :6   and -- 4 and\n                  (\n                    ( \n                       :7   =  :8   and -- 1 and\n                      ( (nvl(mcli.tot_ndr,0) +\n                         nvl(mcli.tot_authorization,0) +\n                         nvl(mcli.tot_capture,0) +\n                         nvl(mcli.tot_debit,0) +\n                         nvl(mcli.tot_ind_plans,0) +\n                         nvl(mcli.tot_sys_generated,0) +\n                         nvl(mcli.tot_equip_rental,0) +\n                         nvl(mcli.tot_equip_sales,0)) -\n                        (nvl(mcrs.tot_ndr,0) +\n                         nvl(mcrs.tot_authorization,0) +\n                         nvl(mcrs.tot_capture,0) +\n                         nvl(mcrs.tot_debit,0) +\n                         nvl(mcrs.tot_ind_plans,0) +\n                         nvl(mcrs.tot_sys_generated,0) +\n                         nvl(mcrs.tot_equip_sales,0) +\n                         nvl(mcrs.tot_equip_rental,0) +\n                         nvl(mcrf.tot_ndr,0) +\n                         nvl(mcrf.tot_authorization,0) +\n                         nvl(mcrf.tot_capture,0) +\n                         nvl(mcrf.tot_debit,0) +\n                         nvl(mcrf.tot_ind_plans,0) +\n                         nvl(mcrf.tot_sys_generated,0) +\n                         nvl(mcrf.tot_equip_sales,0) +\n                         nvl(mcrf.tot_equip_rental,0)) ) <  :9  \n                    )\n                    or\n                    ( \n                       :10   =  :11   and -- 2 and\n                      ( (sm.tot_inc_discount +\n                         sm.tot_inc_interchange +\n                         sm.tot_inc_authorization +\n                         sm.tot_inc_capture +\n                         sm.tot_inc_debit +\n                         sm.tot_inc_ind_plans +\n                         sm.tot_inc_sys_generated +\n                         nvl(mcrf.tot_ndr,0) +\n                         nvl(mcrf.tot_authorization,0) +\n                         nvl(mcrf.tot_capture,0) +\n                         nvl(mcrf.tot_debit,0) +\n                         nvl(mcrf.tot_ind_plans,0) +\n                         nvl(mcrf.tot_sys_generated,0) +\n                         nvl(mcrf.tot_equip_rental,0) +\n                         nvl(mcrf.tot_equip_sales,0) +\n                         nvl(sm.disc_ic_dce_adj_amount,0) + \n                         nvl(sm.fee_dce_adj_amount,0)) -\n                        (nvl(sm.interchange_expense,0) +\n                         sm.vmc_assessment_expense + \n                         nvl(mcli.tot_ndr,0) +\n                         nvl(mcli.tot_authorization,0) +\n                         nvl(mcli.tot_capture,0) +\n                         nvl(mcli.tot_debit,0) +\n                         nvl(mcli.tot_ind_plans,0) +\n                         nvl(mcli.tot_sys_generated,0) +\n                         nvl(mcli.tot_equip_rental,0) +\n                         nvl(mcli.tot_equip_sales,0) +\n                         nvl(mcex.tot_ndr,0) +\n                         nvl(mcex.tot_authorization,0) +\n                         nvl(mcex.tot_capture,0) +\n                         nvl(mcex.tot_debit,0) +\n                         nvl(mcex.tot_ind_plans,0) +\n                         nvl(mcex.tot_sys_generated,0) +\n                         nvl(mcex.tot_equip_rental,0) +\n                         nvl(mcex.tot_equip_sales,0)) ) <  :12  \n                    )\n                    or\n                    ( \n                       :13   =  :14   and -- 3 and\n                      ( (sm.tot_inc_discount +\n                         sm.tot_inc_interchange +\n                         sm.tot_inc_authorization +\n                         sm.tot_inc_capture +\n                         sm.tot_inc_debit +\n                         sm.tot_inc_ind_plans +\n                         sm.tot_inc_sys_generated +\n                         nvl(sm.disc_ic_dce_adj_amount,0) + \n                         nvl(sm.fee_dce_adj_amount,0) +\n                         nvl(mcrf.tot_ndr,0) +\n                         nvl(mcrf.tot_authorization,0) +\n                         nvl(mcrf.tot_capture,0) +\n                         nvl(mcrf.tot_debit,0) +\n                         nvl(mcrf.tot_ind_plans,0) +\n                         nvl(mcrf.tot_sys_generated,0) +\n                         nvl(mcrf.tot_equip_rental,0) +\n                         nvl(mcrf.tot_equip_sales,0) ) -\n                        (nvl(sm.interchange_expense,0) +\n                         sm.vmc_assessment_expense + \n                         sm.tot_exp_authorization +\n                         sm.tot_exp_capture +\n                         sm.tot_exp_debit +\n                         sm.tot_exp_ind_plans +\n                         sm.tot_exp_sys_generated) ) <  :15  \n                    )\n                    or\n                    ( \n                      -- only review direct accounts for mes\n                       :16   =  :17   and -- 0 and\n                      nvl(sm.liability_contract,'N') = 'N' and\n                      nvl(sm.reseller_contract,'N') = 'N' and\n                      ( (sm.tot_inc_discount +\n                         sm.tot_inc_interchange +\n                         sm.tot_inc_authorization +\n                         sm.tot_inc_capture +\n                         sm.tot_inc_debit +\n                         sm.tot_inc_ind_plans +\n                         sm.tot_inc_sys_generated +\n                         nvl(sm.disc_ic_dce_adj_amount,0) + \n                         nvl(sm.fee_dce_adj_amount,0)) -\n                        (nvl(sm.interchange_expense,0) +\n                         sm.vmc_assessment_expense +\n                         sm.tot_exp_authorization +\n                         sm.tot_exp_capture +\n                         sm.tot_exp_debit +\n                         sm.tot_exp_ind_plans +\n                         sm.tot_exp_sys_generated +\n                         nvl(mcrf.tot_ndr,0) +\n                         nvl(mcrf.tot_authorization,0) +\n                         nvl(mcrf.tot_capture,0) +\n                         nvl(mcrf.tot_debit,0) +\n                         nvl(mcrf.tot_ind_plans,0) +\n                         nvl(mcrf.tot_sys_generated,0) +\n                         nvl(mcrf.tot_equip_rental,0) +\n                         nvl(mcrf.tot_equip_sales,0)) ) <  :18  \n                    ) \n                  ) and\n                  mo.org_group = sm.merchant_number\n        group by  mo.org_num, mo.org_group, mo.org_name,\n                  thc.ancestor,\n                  decode(sm.CASH_ADVANCE_ACCOUNT,'Y',1,0),\n                  decode(sm.liability_contract,'Y',1,0),\n                  decode(sm.referral_contract,'Y',1,0),\n                  decode(sm.reseller_contract,'Y',1,0),\n                  decode(sm.cash_advance_referral,'Y',1,0),\n                  decode(sm.moto_merchant,'Y',1,0),\n                  decode(sm.mes_inventory,'Y',1,0)\n        order by thc.ancestor, mo.org_name, mo.org_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.reports.BankContractDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setInt(3,ContractTypes.CONTRACT_SOURCE_LIABILITY);
   __sJT_st.setInt(4,ContractTypes.CONTRACT_SOURCE_REFERRAL);
   __sJT_st.setInt(5,ContractTypes.CONTRACT_SOURCE_RESELLER);
   __sJT_st.setInt(6,ContractTypes.CONTRACT_SOURCE_THIRD_PARTY);
   __sJT_st.setInt(7,userType);
   __sJT_st.setInt(8,UT_RESELLER);
   __sJT_st.setDouble(9,threshold);
   __sJT_st.setInt(10,userType);
   __sJT_st.setInt(11,UT_CONTRACT_BANK);
   __sJT_st.setDouble(12,threshold);
   __sJT_st.setInt(13,userType);
   __sJT_st.setInt(14,UT_VAR_BANK);
   __sJT_st.setDouble(15,threshold);
   __sJT_st.setInt(16,userType);
   __sJT_st.setInt(17,UT_MES);
   __sJT_st.setDouble(18,threshold);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"27com.mes.reports.BankContractDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4795^7*/
      resultSet = it.getResultSet();  
      while( resultSet.next() )
      {
        orgId = resultSet.getLong("org_num");
    
        if ( orgId != lastOrgId )
        {
          row = new SummaryData( resultSet, userType );
          ReportRows.add(row);
          lastOrgId = orgId;
        }
        row.addData( resultSet );
      }        
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadUnderWaterMerchants()", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }
  
  protected boolean maskNonCashAdvanceFees( long merchantId )
  {
    int           recCount    = 0;
    boolean       retVal      = false;
    
    // if the current account has a referral contract for
    // cash advances or it is under a node that only
    // charges for cash advance related contract items
    if ( hasCashAdvanceReferralContract(merchantId) )
    {
      retVal = true;
    }
    if ( retVal == false )
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:4836^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(hierarchy_node) 
//            from    agent_bank_ca_fee_exempt    fe,
//                    t_hierarchy                 th,
//                    mif                         mf
//            where   th.hier_type = 1 and
//                    th.ancestor = fe.hierarchy_node and
//                    th.entity_type = 4 and
//                    mf.association_node = th.descendent and
//                    mf.merchant_number = :merchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(hierarchy_node)  \n          from    agent_bank_ca_fee_exempt    fe,\n                  t_hierarchy                 th,\n                  mif                         mf\n          where   th.hier_type = 1 and\n                  th.ancestor = fe.hierarchy_node and\n                  th.entity_type = 4 and\n                  mf.association_node = th.descendent and\n                  mf.merchant_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4847^9*/
      }
      catch( Exception e )
      {
        logEntry("maskNonCashAdvanceFees(" + merchantId +")", e.toString() );
      }
      retVal = (recCount > 0);
    }
    return( retVal );
  }

  public ChildProfitabilitySummary nextChild( )
  {
    ChildProfitabilitySummary       childSummary = null;
    
    try
    {
      childSummary = 
        (ChildProfitabilitySummary) ReportRows.elementAt(ReportDataElementsIndex);
      ReportDataElementsIndex++;        
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
    }      
    return( childSummary );
  }
  
  protected void processContractSummaryData( BankContractBean contract, int contractType, ResultSet resultSet )
  {
    // default is details mode
    processContractSummaryData( contract, contractType, resultSet, null );
  }

  public void processContractSummaryData( BankContractBean contract, int contractType, ResultSet resultSet, double[][] contractTotals  )
  {
    Date                            activeDate        = null;
    String                          amountFieldName   = null;
    int                             betGroup          = BankContractBean.BET_GROUP_COUNT;
    int                             betType           = BankContractBean.BET_NONE;
    boolean                         cashAdvMerchData  = false;
    int                             contractSource    = 0;
    int                             count             = 0;
    String                          countFieldName    = null;
    String                          desc              = null;
    int                             discBetType       = BankContractBean.BET_NONE;
    boolean                         excludeFromProf   = false;
    int                             index             = 0;
    int                             perItemBetType    = BankContractBean.BET_NONE;
    int                             preProcCount      = 0;
    String                          productCode       = null;
    double                          rate              = 0.0;
    ContractTypes.PerItemRecord     record            = null;
    achDiscLiability                                  = 0.0;
    try
    {
      contractSource = contract.getContractSource();
      
      activeDate = resultSet.getDate("active_date");
    
      // process all the per item elements for this row
      if ( resultSet.getInt("cash_advance") == 1 &&
           maskNonCashAdvanceFees(resultSet.getLong("merchant_number")) )
      {
        cashAdvMerchData = true;
      }
      else
      {
        cashAdvMerchData = false;
      }
      productCode       = resultSet.getString("product_code");
      
      // add the contract data
      for( int i = 0; i < BankContractBean.BET_COUNT; ++i )
      {
         /*
         * RS Changes for ACHPS Partner billing
         * There are some range gap between 112 to 230 in database table. Hence setting value of i is 239. 
         */
        
        if(i == 107){
          i = 239;
        }
    
        // setup some defaults
        amountFieldName   = null;
        betType           = i;
        betGroup          = contract.getItemGroup(betType);
        count             = 0;
        countFieldName    = null;
        perItemBetType    = i;
        discBetType       = BankContractBean.BET_NONE;
        desc              = null;
        excludeFromProf   = false;
        preProcCount      = 0;
        excludeFromProf   = contract.isItemExcluded( betType, productCode );
        
        if ( (cashAdvMerchData == true) && !contract.isCashAdvanceBetType(betType) )
        {
          continue;
        }
        
        // when in details mode, only process entries for the current bet group
        if ( (contractTotals == null) && (AuditSummaryMode == false) )
        {
          if ( (excludeFromProf && (ReportBetGroup != BankContractBean.BET_GROUP_EXCLUDED_FEES)) ||
               !(excludeFromProf || BankContractBean.isItemInGroup(betType,ReportBetGroup)) )
          {               
            continue;
          }            
        }
        
        //
        // skip any entries that do not have a contract 
        // rate associated with them.  Note, BET_PROCESSING_PER_ITEM and BET_AMEX_PROCESSING_FEE
        // has two possible bet contract rates (per item and
        // percentage based), so always allow the software to
        // process that bet type.
        //
        if ( ( contract.getContractItemRate( i, productCode ) == 0.0 ) &&
             ( i != BankContractBean.BET_PROCESSING_PER_ITEM ) &&
             (i != BankContractBean.BET_AMEX_PROCESSING_FEE ) &&
             (i != BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG))
        {
          continue;
        }
        
        // 
        // if the contract element is not enabled for the month
        // then skip this element. Note, BET_PROCESSING_PER_ITEM
        // has two possible bet contract rates (per item and
        // percentage based), so check for either to be enabled.
        
        if ( !contract.isElementEnabled(i,activeDate,productCode) &&
             ( i != BankContractBean.BET_PROCESSING_PER_ITEM ||
               !contract.isElementEnabled(BankContractBean.BET_PROCESSING_RATE,activeDate,productCode) ) )
        {
          continue;
        }
        
       if ( (i == BankContractBean.BET_AMEX_PROCESSING_FEE) &&
            (contract.isElementEnabled(BankContractBean.BET_AMEX_PROCESSING_FEE,activeDate,productCode)  && 
               contract.isElementEnabled(BankContractBean.BET_AMEX_PROCESSING_RATE,activeDate,productCode)     )  ) 
       {
           continue;
       }
        
        switch(i)
        {
          case BankContractBean.BET_CASH_ADV_TRANSACTION_FEE:
            countFieldName  = "cash_advance_vol_count";
            
            // if this is a "pay $x per CashAdv to bank and keep
            // the interchange for ourselves" deal and we are loading
            // the merchant level details data
            if ( cashAdvMerchData == true && contractTotals == null )
            {
              if ( contractTotals == null )   // details mode
              {
                //
                // if the current node is paying another node for the cash
                // advance, then assume they are receiving the interchange
                // from the transaction.  
                //
                // if the current node is receiving payment from another node
                // for the cash advance, then assume that the other node is
                // collecting the negative interchange.
                //
                if ( contractType == ContractTypes.CONTRACT_TYPE_EXPENSE )
                {
                  // if the contract type is referral and the parent does not
                  // "own" the merchant (child liability contract also exists)
                  // then we need to add the cash advance negative interchange
                  // to the income contract data.
                  if ( ( contract.getContractSource() == ContractTypes.CONTRACT_SOURCE_REFERRAL ) )
                  {
                    // paying lower node a per item cash adv fee through
                    // a referral contract, so take the negative interchange,
                    // and skip all the other referral contract expenses.
                    record = new ContractTypes.PerItemRecord( activeDate,
                                                              ContractTypes.CONTRACT_SOURCE_VITAL,
                                                              BankContractBean.BET_CASH_ADVANCE_INTERCHANGE, 
                                                              1,
                                                              ( resultSet.getDouble("cash_adv_interchange_exp") +
                                                                resultSet.getDouble("assessment") ) );
                    addContractElement( getReportOrgId(), ContractTypes.CONTRACT_DATA_MES, contractType, record );
                    HasCashAdvanceContractExpense = true;
                  }
                }
                else    // income contract
                {
                  HasCashAdvanceContractIncome  = true;
                }
              }                
            }                
            break;
        
          case BankContractBean.BET_PROCESSING_PER_ITEM:  
            // do both per item and discount in one statement (ignore the other)
            betType         = contract.groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC );
            countFieldName  = "processing_vol_count";       // sales + credits
            if ( activeDate.after(getNetSalesCutoffDate()) )
            {
              amountFieldName = "processing_sales_amount";  // sales only, all acquired card types
            }
            else
            {
              amountFieldName = "vmc_vol_amount";           // legacy, vmc sales - credits
            }
            perItemBetType  = BankContractBean.BET_PROCESSING_PER_ITEM;
            discBetType     = BankContractBean.BET_PROCESSING_RATE;
            desc            = "VS/MC/DS Processing Fees " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;
           
          case BankContractBean.BET_AMEX_PROCESSING_RATE:
          case BankContractBean.BET_AMEX_PROCESSING_FEE:  
              betType         = contract.groupIndexToBetType( BankContractBean.BET_GROUP_DISC_IC );
              discBetType     = BankContractBean.BET_AMEX_PROCESSING_RATE;
              perItemBetType  = BankContractBean.BET_AMEX_PROCESSING_FEE;
              desc            = "American Express Processing Fees "+ DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
              amountFieldName = "am_ic_sales_amount";
              countFieldName  = "am_ic_vol_count";
              break;  
            
          case BankContractBean.BET_VS_SPONSORSHIP:
            discBetType     = betType;
            perItemBetType  = BankContractBean.BET_NONE;
            if ( activeDate.after(getNetSalesCutoffDate()) )
            {
              amountFieldName = "visa_sales_amount";    // sales only
            }
            else
            {
              amountFieldName = "visa_vol_amount";      // sales - credits
            }
            countFieldName  = "visa_vol_count";       // sales + credits
            desc            = "Visa Sponsorship " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;
            
          case BankContractBean.BET_MC_SPONSORSHIP:
            discBetType     = betType;
            perItemBetType  = BankContractBean.BET_NONE;
            if ( activeDate.after(getNetSalesCutoffDate()) )
            {
              amountFieldName = "mc_sales_amount";    // sales only
            }
            else
            {
              amountFieldName = "mc_vol_amount";      // sales - credits
            }
            countFieldName  = "mc_vol_count";         // sales + credits
            desc            = "MasterCard Sponsorship " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;
            
          case BankContractBean.BET_DS_SPONSORSHIP:
            discBetType     = betType;
            perItemBetType  = BankContractBean.BET_NONE;
            amountFieldName = "ds_ic_sales_amount";     // sales only
            countFieldName  = "ds_ic_vol_count";        // sales + credits
            desc            = "Discover Sponsorship " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;
            
          case BankContractBean.BET_AM_SPONSORSHIP:
            discBetType     = betType;
            perItemBetType  = BankContractBean.BET_NONE;
            amountFieldName = "am_ic_sales_amount";     // sales only
            countFieldName  = "am_ic_vol_count";        // sales + credits
            desc            = "American Express Sponsorship " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;

          case BankContractBean.BET_AMEX_GLOBAL_ACQ_FEE:
              discBetType     = betType;
              perItemBetType  = BankContractBean.BET_NONE;
              amountFieldName = "am_ic_sales_amount";     // sales only
              countFieldName  = "am_ic_vol_count";        // sales + credits
              desc            = "American Express Global Acquiring Fee " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
              break;
                       
          case BankContractBean.BET_MC_CROSS_BORDER_FEE:
            discBetType     = betType;
            perItemBetType  = BankContractBean.BET_NONE;
            amountFieldName = "mc_cross_border_hash_amount";  // sales + credits
            countFieldName  = "mc_cross_border_vol_count";    // ""
            desc            = "MasterCard Cross Border Fee " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;
            
          case BankContractBean.BET_MC_FOREIGN_STD_FEE:
            discBetType     = betType;
            perItemBetType  = BankContractBean.BET_NONE;
            amountFieldName = "mc_foreign_std_hash_amount";  // sales + credits
            countFieldName  = "mc_foreign_std_vol_count";    // ""
            desc            = "MasterCard Global Support Fee (Foreign Std) " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;
            
          case BankContractBean.BET_MC_FOREIGN_ELEC_FEE:
            discBetType     = betType;
            perItemBetType  = BankContractBean.BET_NONE;
            amountFieldName = "mc_foreign_elec_hash_amount";  // sales + credits
            countFieldName  = "mc_foreign_elec_vol_count";    // ""
            desc            = "MasterCard Global Support Fee (Foreign Elec) " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;
            
          case BankContractBean.BET_MC_NABU_FEE:
            countFieldName  = "mc_nabu_vol_count";            // sales + credits
            desc            = "MasterCard NABU Fee " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;
            
          case BankContractBean.BET_VMC_ASSESSMENT_FEE:
            discBetType     = betType;
            perItemBetType  = BankContractBean.BET_NONE;
            amountFieldName = "vmc_vol_amount";
            countFieldName  = "vmc_vol_count";
            desc            = "V/MC Assessment " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;
            
          case BankContractBean.BET_AMEX_INTERCHANGE:
            discBetType     = betType;
            perItemBetType  = BankContractBean.BET_NONE;
            amountFieldName = "amex_sales_amount";
            countFieldName  = "amex_sales_count";
            desc            = "Amex Interchange " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;
            
          case BankContractBean.BET_DINERS_INTERCHANGE:
            discBetType     = betType;
            perItemBetType  = BankContractBean.BET_NONE;
            amountFieldName = "dinr_sales_amount";
            countFieldName  = "dinr_sales_count";
            desc            = "Diners Interchange " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;
            
          case BankContractBean.BET_DISCOVER_INTERCHANGE:
            discBetType     = betType;
            perItemBetType  = BankContractBean.BET_NONE;
            amountFieldName = "disc_sales_amount";
            countFieldName  = "disc_sales_count";
            desc            = "Discover Interchange " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;
            
          case BankContractBean.BET_PL1_INTERCHANGE:
            discBetType     = betType;
            perItemBetType  = BankContractBean.BET_NONE;
            amountFieldName = "pl1_sales_amount";
            countFieldName  = "pl1_sales_count";
            desc            = "Private Label Interchange " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;
            
          case BankContractBean.BET_MC_SETTLE_ACCESS_FEE:
            countFieldName  = "mc_vol_count";
            break;
            
          case BankContractBean.BET_AMEX_TRANSACTION_FEE:
            countFieldName  = "amex_vol_count";
            break;
            
          case BankContractBean.BET_DINERS_TRANSACTION_FEE:
            countFieldName  = "dinr_vol_count";
            break;
            
          case BankContractBean.BET_DISC_TRANSACTION_FEE:
            countFieldName  = "disc_vol_count";
            break;
            
          case BankContractBean.BET_JCB_TRANSACTION_FEE:
            countFieldName  = "jcb_vol_count";
            break;
            
          case BankContractBean.BET_DEBIT_TRANSACTION_FEE:
            // go through and pre-process all the categories
            // of debit for overloaded pricing.
            for( int dcat = 0; dcat < BankContractBean.DEBIT_CAT_COUNT; ++dcat )
            {
              count = resultSet.getInt( "debit_count_cat_" + NumberFormatter.getPaddedInt((dcat+1),2) );
              if ( count != 0 )
              {
                rate = contract.getContractItemRate( perItemBetType, NumberFormatter.getPaddedInt((dcat+1),3) );
                if ( rate != 0.0 )
                {
                  if ( contractTotals == null ) // details mode
                  {
                    record = new ContractTypes.PerItemRecord( activeDate, contract.getContractSource(), betType, count, rate );
            
                    // append the appropriate category code                                           
                    record.setDescription("Debit Transaction Fee"); //(Cat " + (dcat+1) + ")");
                  
                    addContractElement( getReportOrgId(), ContractTypes.CONTRACT_DATA_MES, contractType, record );
                  }
                  else    // summary only
                  {
                    contractTotals[contractSource][betGroup] += (count * rate);
                  }                    
                  preProcCount += count;
                }
              }
            }

            // do both per item and discount in one statement (ignore the other)
            betType         = contract.groupIndexToBetType( BankContractBean.BET_GROUP_DEBIT_FEES );
            countFieldName  = "debit_vol_count";        // sales + credits
            amountFieldName = "debit_sales_amount";     // sales only
            perItemBetType  = BankContractBean.BET_DEBIT_TRANSACTION_FEE;
            discBetType     = BankContractBean.BET_DEBIT_TRANSACTION_RATE;
            desc            = "Debit Card Transaction Fees " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;

          case BankContractBean.BET_EBT_TRANSACTION_FEE:
            countFieldName  = "ebt_vol_count";
            break;
            
          case BankContractBean.BET_ACHP_TRANSACTION_FEE:
            countFieldName  = "achp_vol_count";
            break;
            
          case BankContractBean.BET_ACHP_RETURNED_ITEM_FEE:
            countFieldName  = "achp_return_count";
            break;
            
          case BankContractBean.BET_OUTGOING_CHARGEBACK_PER_ITEM:
            countFieldName  = "chargeback_vol_count";
            break;
            
          case BankContractBean.BET_INCOMING_CHARGEBACK_PER_ITEM:
            countFieldName  = "incoming_cb_count";
            break;
            
          case BankContractBean.BET_INCOMING_FT_CHARGEBACK_PER_ITEM:
            countFieldName  = "incoming_ft_cb_count";
            break;
            
          case BankContractBean.BET_INCOMING_RETRIEVAL_REQUEST:
            countFieldName  = "retrieval_count";
            break;
            
          case BankContractBean.BET_VMC_AUTHORIZATION_FEE:
            countFieldName  = "vmc_auth_count";
            break;
            
          case BankContractBean.BET_VISA_AUTH_ACCESS_FEE:
            countFieldName  = "visa_interlink_auth_count";
            break;
            
          case BankContractBean.BET_VISA_AUTH_ACCESS_FEE_NON_VISA:
            countFieldName  = "non_visa_auth_access_count";
            break;
            
          case BankContractBean.BET_AMEX_AUTHORIZATION_FEE:
            countFieldName  = "amex_auth_count";
            break;

          
          case BankContractBean.BET_AMEX_DIRECT_AUTH_FEE:
              countFieldName  = "amex_direct_auth_count_vital";
              break;
             
          case BankContractBean.BET_AMEX_DIRECT_AUTH_FEE_TRIDENT:
              countFieldName  = "amex_direct_auth_count_trident";
              break;   

          case BankContractBean.BET_DINERS_AUTHORIZATION_FEE:
            countFieldName  = "dinr_auth_count";
            break;
            
          case BankContractBean.BET_DISC_AUTHORIZATION_FEE:
            countFieldName  = "disc_auth_count";
            break;
            
          case BankContractBean.BET_JCB_AUTHORIZATION_FEE:
            countFieldName  = "jcb_auth_count";
            break;
            
          case BankContractBean.BET_EQUIPMENT_RENTAL_FEE:
            countFieldName  = "num_rentals";
            break;
            
          case BankContractBean.BET_CUSTOMER_SERVICE_CALLS:
            countFieldName  = "num_service_calls";
            break;
            
          case BankContractBean.BET_AMEX_SETUP_FEE:
            countFieldName  = "num_amex_setups";
            break;
            
          case BankContractBean.BET_DISCOVER_SETUP_FEE:
            countFieldName  = "num_disc_setups";
            break;
            
          case BankContractBean.BET_RISK_MONITORING_RETAIL_VOLUME:
          case BankContractBean.BET_RISK_MONITORING_MOTO_VOLUME:
            // if the account is moto and the billing element is retail
            // or vice versus, then skip this record.
            if( ( (resultSet.getInt("moto_merchant") == 1) &&
                  (i == BankContractBean.BET_RISK_MONITORING_RETAIL_VOLUME) ) ||
                ( (resultSet.getInt("moto_merchant") == 0) &&
                  (i == BankContractBean.BET_RISK_MONITORING_MOTO_VOLUME) ) )
            {
              continue;
            }
            
            // setup the contract element ids
            discBetType     = betType;
            perItemBetType  = BankContractBean.BET_NONE;
            amountFieldName = "risk_sales_amount";
            countFieldName  = "risk_sales_count";
            desc            = "Risk Monitoring " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
            break;
            
          case BankContractBean.BET_MONTHLY_SUPPLY_FEE:
          case BankContractBean.BET_MONTHLY_ACTIVE_MERCH_FEE:
          case BankContractBean.BET_STATEMENT_FEE:
          case BankContractBean.BET_WEB_REPORTING_FEE:
          case BankContractBean.BET_RISK_MONITORING:
          case BankContractBean.BET_RISK_MANAGEMENT_REPORTING:
          case BankContractBean.BET_HELP_DESK_FEE:
          case BankContractBean.BET_TWO_FACTOR_AUTHENTICATION_FEE:
          case BankContractBean.BET_PCI_ANNUAL_REG_FEE:
          case BankContractBean.BET_IRS_REPORTING_FEE: 
            countFieldName  = "num_active_merchants";
            break;
          case BankContractBean.BET_PAPER_STATEMENT_FEE:
            countFieldName  = "print_statement_count";
            break;
          case BankContractBean.BET_BATCH_FEE:
            countFieldName  = "batch_fee_count";
            break;
          case BankContractBean.BET_ACTIVATION_FEE:
            countFieldName  = "num_activated_merchants";
            break;
            
          case BankContractBean.BET_APPLICATION_FEE:
          case BankContractBean.BET_NEW_ACCOUNT_SETUP_FEE:
          case BankContractBean.BET_CREDIT_SCORING_FEE:
            countFieldName  = "num_new_merchants";
            break;
            
          case BankContractBean.BET_ECOMMERCE_SETUP_FEE:
          case BankContractBean.BET_ECOMMERCE_MONTHLY_FEE:
            if ( BankContractBean.isECommerceProduct( productCode ) == true )
            {
              if ( betType == BankContractBean.BET_ECOMMERCE_SETUP_FEE )
              {
                countFieldName = "num_new_merchants";
              }
              else    // monthly fee
              {
                countFieldName = "num_active_merchants";
              }                
            }
            break;
            
          case BankContractBean.BET_POS_PARTNER_SETUP_FEE:
          case BankContractBean.BET_VERISIGN_PAYLINK_SETUP_FEE:
          case BankContractBean.BET_VERISIGN_PAYFLOW_SETUP_FEE:
            // if this product code is associated with the current
            // bet type, then process the new merchant data.
            if ( BankContractBean.getSetupFeeBetType( productCode ) == betType ) 
            {
              countFieldName = "num_new_merchants";
            }
            break;
          
          case BankContractBean.BET_POS_PARTNER_FEE:
          case BankContractBean.BET_VERISIGN_PAYLINK_FEE:
          case BankContractBean.BET_VERISIGN_PAYFLOW_FEE:
            // if this product code is associated with the current
            // bet type, then process the active merchant data.
            if ( BankContractBean.getMonthlyFeeBetType( productCode ) == betType ) 
            {
              countFieldName = "num_active_merchants";
            }
            break;
            
          case BankContractBean.BET_SUPPLY_PER_AUTH_FEE:
            countFieldName = "auth_count_total";
            break;
            
          case BankContractBean.BET_PARTNERSHIP_RATE:
            if ( contractTotals != null || (AuditSummaryMode == true) )
            {
              continue;     // not used in summary mode
            }
            break;
            
          case BankContractBean.BET_PRODUCT_SETUP_FEE:
            countFieldName = "num_new_merchants";
            if ( contractTotals == null )
            {
              // only need the description in details mode
              desc = "Setup Fee - " + contract.loadProductDesc( productCode ); 
            }
            break;
            
          case BankContractBean.BET_MONTHLY_PRODUCT_FEE:
            countFieldName = "num_active_merchants";
            if ( contractTotals == null ) 
            {
              // only need the description in details mode
              desc = contract.loadProductDesc( productCode ) + DateTimeFormatter.getFormattedDate(activeDate," MMM yyyy");
            }              
            break;
            
          case BankContractBean.BET_DIAL_PAY_TRANSACTION_FEE:
            countFieldName = "dialpay_auth_count";
            break;
            
          case BankContractBean.BET_AVS_AUTHORIZATION_FEE:
            countFieldName = "avs_auth_count";
            break;
            
          case BankContractBean.BET_ACH_REJECT_FEE:
            countFieldName = "ach_reject_count";
            break;
            
          case BankContractBean.BET_TERMINAL_DEPLOYMENT:
            countFieldName = "terminal_deployment_count";
            break;

          case BankContractBean.BET_AUTHORIZATION_CREDIT:
            countFieldName = "auth_count_total";
            break;
            
          case BankContractBean.BET_GATEWAY_AUTH_FEE:
            countFieldName = "auth_count_total";
            if ( contractTotals == null ) 
            {
              // only need the description in details mode
              desc = "Gateway Authorization Fee - " + contract.loadProductDesc( productCode ) + DateTimeFormatter.getFormattedDate(activeDate," MMM yyyy");
            }              
            break;
            
          case BankContractBean.BET_THIRD_PARTY_AUTH_FEE:
            countFieldName = "auth_count_total_ext";
            break;
            
          case BankContractBean.BET_EQUIPMENT_SWAP:
            countFieldName = "swap_count";
            break;
            
          case BankContractBean.BET_IC_CORRECTION_FEE:
            countFieldName = "ic_correction_count";
            break;
            
          case BankContractBean.BET_NEW_ACCOUNT_REFERRAL_FEE:
            countFieldName = "num_activated_merchants";
            break;
            
          case BankContractBean.BET_REFERRAL_RESIDUAL:
            discBetType     = betType;
            perItemBetType  = BankContractBean.BET_NONE;
            amountFieldName = "vmc_vol_amount";
            countFieldName  = "vmc_vol_count";
            break;
            
          case BankContractBean.BET_AUTH_LOG_FEE:
            countFieldName = "auth_count_total";
            break;
            
          case BankContractBean.BET_TPG_AUTH_FEE:
            countFieldName = "auth_count_total_api";
            break;
            
          case BankContractBean.BET_TRIDENT_AUTH_FEE:
            countFieldName = "auth_count_total_trident";
            break;
            
          case BankContractBean.BET_VMC_AUTH_FEE_TRIDENT:
            countFieldName = "vmc_auth_count_trident";
            break;
            
          case BankContractBean.BET_AMEX_AUTH_FEE_TRIDENT:
            countFieldName = "amex_auth_count_trident";
            break;
            
          case BankContractBean.BET_DISCOVER_AUTH_FEE_TRIDENT:
            countFieldName = "disc_auth_count_trident";
            break;
            
          case BankContractBean.BET_DINERS_AUTH_FEE_TRIDENT:
            countFieldName = "dinr_auth_count_trident";
            break;
            
          case BankContractBean.BET_JCB_AUTH_FEE_TRIDENT:
            countFieldName = "jcb_auth_count_trident";
            break;
            
          case BankContractBean.BET_DEBIT_AUTH_FEE_TRIDENT:
            countFieldName = "debit_auth_count_trident";
            break;

          case BankContractBean.BET_DEBIT_AUTH_FEE:
            countFieldName = "debit_auth_count";
            break;
          /* Code Modified by RS  - Added ACH PS Specific Count field names  for agent bank fees calcuation*/  

          case BankContractBean.BET_ACH_TRANSACTION_FEE:
            countFieldName = "ach_tran_count";
            break;

          case BankContractBean.BET_ACH_RET_REJ_FEE:
            countFieldName = "ach_return_count";
            break;

          case BankContractBean.BET_ACH_UNAUTH_RETURN:
            countFieldName = "ach_unauth_count";
            break;

          case BankContractBean.BET_ACH_MONHLY_FEE:
            countFieldName = "";
            break;

          case BankContractBean.BET_ACH_SMRTPY_MONHLY_FEE:
            countFieldName = "";
            break;

          case BankContractBean.BET_ACH_HOSTED_PY_MONHLY_FEE:
            countFieldName = "";
            break;

          case BankContractBean.BET_ACH_SMRTPY_EXPR_SETUP_FEE:
            countFieldName = "";
            break;

          case BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG:
            int hasPinDebit=0;
            /*@lineinfo:generated-code*//*@lineinfo:5569^13*/

//  ************************************************************
//  #sql [Ctx] { select count(1) 
//                from t_hierarchy     th,
//                      mif             m,
//                      agent_bank_contract     abc
//                where m.merchant_number = :ReportNodeId and
//                  m.association_node = th.descendent and
//                  th.ancestor = abc.hierarchy_node and
//                  :ReportDateBegin between abc.valid_date_begin and abc.valid_date_end and
//                  billing_element_type = :BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(1)  \n              from t_hierarchy     th,\n                    mif             m,\n                    agent_bank_contract     abc\n              where m.merchant_number =  :1   and\n                m.association_node = th.descendent and\n                th.ancestor = abc.hierarchy_node and\n                 :2   between abc.valid_date_begin and abc.valid_date_end and\n                billing_element_type =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,ReportNodeId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setInt(3,BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   hasPinDebit = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:5580^13*/

            if(hasPinDebit > 0 && contractType == ContractTypes.CONTRACT_TYPE_EXPENSE) {
              count = resultSet.getInt("debit_vol_count");
              ContractTypes.SalesRecord record1 = new ContractTypes.SalesRecord(activeDate, contract.getContractSource(), betType, count, resultSet.getDouble("debit_vol_amount"), 0.0, 0.0);
              record1.setAmount(resultSet.getDouble("debit_network_fees"));
              record1.setVolumeAmount(resultSet.getDouble("debit_vol_amount"));
              record1.setDescription("Debit Network Fees");
              addContractElement(getReportOrgId(), ContractTypes.CONTRACT_DATA_MES, contractType, record1);
            }

            String icPassthrough = "N";
      try {
            /*@lineinfo:generated-code*//*@lineinfo:5593^13*/

//  ************************************************************
//  #sql [Ctx] { select  upper(nvl(mf.debit_pass_through,'N'))
//                
//                from    mif   mf
//                where   mf.merchant_number = :ReportNodeId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  upper(nvl(mf.debit_pass_through,'N'))\n               \n              from    mif   mf\n              where   mf.merchant_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,ReportNodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   icPassthrough = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:5599^13*/
      } catch(Exception ignore) {}

            if ( "Y".equals(icPassthrough) && contractType == ContractTypes.CONTRACT_TYPE_INCOME ) {
              double amount = 0.0;
        try {
              /*@lineinfo:generated-code*//*@lineinfo:5605^15*/

//  ************************************************************
//  #sql [Ctx] { select sum( case
//                                when st.st_statement_desc like 'DB%' and st.item_category = 'IC' then 1
//                                else 0
//                              end
//                              * st.st_fee_amount ) 
//                  from    monthly_extract_st st,
//                          monthly_extract_summary sm
//                  where   sm.merchant_number = :ReportNodeId and
//                          sm.active_date between :ReportDateBegin and :ReportDateEnd and
//                          st.hh_load_sec(+) = sm.hh_load_sec
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sum( case\n                              when st.st_statement_desc like 'DB%' and st.item_category = 'IC' then 1\n                              else 0\n                            end\n                            * st.st_fee_amount )  \n                from    monthly_extract_st st,\n                        monthly_extract_summary sm\n                where   sm.merchant_number =  :1   and\n                        sm.active_date between  :2   and  :3   and\n                        st.hh_load_sec(+) = sm.hh_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,ReportNodeId);
   __sJT_st.setDate(2,ReportDateBegin);
   __sJT_st.setDate(3,ReportDateEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amount = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:5617^15*/
        } catch(Exception ignore) {}
              count = resultSet.getInt("debit_vol_count");
              ContractTypes.SalesRecord record1 = new ContractTypes.SalesRecord(activeDate, contract.getContractSource(), betType, count, resultSet.getDouble("debit_vol_amount"), 0.0, 0.0);
              record1.setAmount(amount);
              record1.setVolumeAmount(resultSet.getDouble("debit_vol_amount"));
              record1.setDescription("Debit Network Fees");
              addContractElement(getReportOrgId(), ContractTypes.CONTRACT_DATA_MES, contractType, record1);
            }
            break;

          default:    // not supported
            continue;
        }
        
        if ( countFieldName != null )
        {
          // get the number of items minus any that were already
          // processed.  Currently this is only the debit items.
          if (countFieldName.trim().length() > 0)
          {
             count = resultSet.getInt(countFieldName) - preProcCount;
          }
          // For ACH PS Fixed charges count to be assumed as 1
          else
          {
             count = 1;
          }   
        }
        else    // do not have a valid count field
        {
          count = 0;
        }            
        
        if ( betType == BankContractBean.BET_PARTNERSHIP_RATE )
        {
          // only happens in detail mode
          record = new ContractTypes.PartnershipRecord( betType, contract.getContractItemRate( betType,productCode ),
                                                        contract.getRevenueMask(), 
                                                        loadChargeRecordRevenue(resultSet.getLong("merchant_number")),
                                                        resultSet );
        }
        else if ( amountFieldName != null && 
                  discBetType != BankContractBean.BET_NONE &&
                  ( count > 0 ||
                    resultSet.getDouble(amountFieldName) > 0 ) )
        {
          record = new ContractTypes.VolumeRecord( 
                                     activeDate,
                                     contract.getContractSource(),
                                     betType, 
                                     count,
                                     resultSet.getDouble(amountFieldName),
                                     contract.getContractItemRate( perItemBetType, productCode ),
                                     contract.getContractItemRate( discBetType, productCode ) );
        }
        else if ( countFieldName != null && 
                  perItemBetType != BankContractBean.BET_NONE &&
                  count > 0 )
        {
          record = new ContractTypes.PerItemRecord( activeDate,
                                                    contract.getContractSource(),
                                                    betType, 
                                                    count,
                                                    contract.getContractItemRate( perItemBetType,productCode ),
                                                    contract.getContractItemExemptCount( perItemBetType,productCode ) );
        }
        else
        {
          record = null;
        }
        
        if ( record != null )
        {
          // set the exclude status.  this allows some of the contract
          // fees to be separated from the income/expense calculations
          record.setExcludeStatus( excludeFromProf );
          
          if ( desc != null )
          {
            // store any overloaded item descriptions
            record.setDescription( desc );
          }
          if ( contractTotals == null ) // details mode
          {
            addContractElement( getReportOrgId(), ContractTypes.CONTRACT_DATA_MES, contractType, record );
          }
          else    // totals only
          {
            if ( excludeFromProf == true )
            {
              // add the special group "excluded"
              contractTotals[contractSource][BankContractBean.BET_GROUP_EXCLUDED_FEES] += record.getExcludedAmount();
            }
            else  // add the net (amount - exempt) to totals
            {
              contractTotals[contractSource][betGroup] += record.getNetAmount();
              /* RS Code Change for  ACH PS */
              if (i == BankContractBean.BET_ACH_TRANSACTION_FEE)
              {
                if (contractSource == ContractTypes.CONTRACT_SOURCE_LIABILITY)
                {
                   achDiscLiability = record.getNetAmount();
                } 
              }              
             }
          }            
        }
      }   // end for loop through row columns
    }
    catch( java.sql.SQLException e )
    {
      logEntry("processContractSummaryData( " + betType + " ): ", e.toString());
      if ( contractTotals == null )
      {
        addError("processData: " + e.toString());
      }        
    }
    finally
    {
    }
  }
  
  public void setAuditSummaryMode( boolean val )
  {
    AuditSummaryMode = val;
  }
  
  public void setProperties(HttpServletRequest request)
  {
    long                      newDefault    = 0L;
    
    // load the default report properties
    super.setProperties( request );
    
    ProfReportType      = HttpHelper.getInt( request, "profReportType",  MERCH_PROF_TYPE_DEFAULT );
    ReportBetGroup      = HttpHelper.getInt( request, "betGroup", BankContractBean.BET_GROUP_ALL );
    
    if ( ProfReportType == MERCH_PROF_TYPE_UNDER_WATER )
    {
      Vector        allFields = null;
      Field         field     = null;
    
      try
      {
        fields.add( new CurrencyField("uwThreshold",16,16,false) );
        fields.getField("uwThreshold").setData("50");
        
        // set the extra html data for each field
        allFields = fields.getFieldsVector();
    
        for( int i = 0; i < allFields.size(); ++i )
        {
          field = (Field)allFields.elementAt(i);
          field.addHtmlExtra("class=\"formFields\"");
        }
        
        // load the default form fields
        setFields( request );
      }       
      catch( Exception e )
      {
        logEntry( "setProperties(underWaterSetup)",e.toString() );
      }      
    }
    
    // overload the default (login) org id that the report uses if 
    // a new default org id was specified.
    newDefault = HttpHelper.getLong(request,"defaultOrgId", 0L );
    
    if ( newDefault != 0L )
    {
      // setup the new perspective org id
      setReportOrgIdDefault( newDefault );
      
      // if the current node is not a child of the new 
      // perspective node, then set the current node to
      // be the to of the perspective node hierarchy.
      if ( isOrgParentOfOrg( newDefault, getReportOrgId() ) == false )
      {
        StringBuffer      msg = new StringBuffer();
      
        msg.append( "Requested node, " );
        msg.append( getReportHierarchyNode() );
        msg.append( ", is not a child of " );
        msg.append( getReportHierarchyNodeDefault() );
        addError(msg.toString());
      
        setReportOrgId( newDefault );
      }
    }
    
    // if the from date is today, then set the default to rolling previous week
    if ( usingDefaultReportDates() )
    {
      Calendar    cal           = Calendar.getInstance();
      
      // setup the default date range
      cal.setTime( ReportDateBegin );
      cal.add( Calendar.MONTH, -1 );
      
      // since this report is for one month at a time,
      // make sure that the from date always starts with
      // the first day of the specified month.
      cal.set( Calendar.DAY_OF_MONTH, 1 );
      
      // adjust the retention report
      if ( ProfReportType == MERCH_PROF_TYPE_RETENTION )
      {
        cal.add(Calendar.MONTH,1);
        cal.add(Calendar.YEAR,-1);
        setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
        cal.add(Calendar.MONTH,2);        
        setReportDateEnd  ( new java.sql.Date( cal.getTime().getTime() ) );
      }
      else if ( ProfReportType == MERCH_PROF_TYPE_NET_REVENUE_RETENTION )
      {
        cal.set(Calendar.MONTH,Calendar.JANUARY);
        cal.set(Calendar.DAY_OF_MONTH,1);
        cal.add(Calendar.YEAR,-1);
        setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
      }
      else
      {
        // set both the begin and end dates
        setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
        setReportDateEnd  ( new java.sql.Date( cal.getTime().getTime() ) );
      }        
    }    
    
    if ( ProfReportType == MERCH_PROF_TYPE_NET_REVENUE_RETENTION )
    {
      // overload default
      SummaryType       = HttpHelper.getInt( request, "summaryType", ST_PARENT );
      RelationshipType  = HttpHelper.getInt( request,"relationship", -1);
      
      Calendar cal = Calendar.getInstance();
      cal.add(Calendar.MONTH,-1);
      cal.set(Calendar.DAY_OF_MONTH,1);
      setReportDateEnd( new java.sql.Date( cal.getTime().getTime() ) );
    }
    
    if ( ProfReportType == MERCH_PROF_TYPE_UNDER_WATER )
    {
      // if the report is the under water report and the
      // node is a non-merchant node, then force
      // the current node to match the login node.  this
      // report only shows merchants that are under water
//@      if ( getReportMerchantId() == 0L )
//@      {
//@        setReportOrgId( getReportOrgIdDefault() );
//@      }
  
      // if the current node is the top node in the tree 
      // then force the user down to the 3941 bank level
      if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
      {
        setReportHierarchyNode( 394100000 );
      }
    
      // under water merchant report is for a 
      // single month only so force the ending
      // report date to match the beginning
      // report date.
      setReportDateEnd( getReportDateBegin() );
    }
    
    // if the report is the TPS GRIN report and
    // the node is above the top TPS node then
    // force the user to the top TPS node
    if ( ProfReportType == MERCH_PROF_TYPE_TPS_GRIN )
    {
      int       matchCount  = 0;
      long      reportNode  = getReportHierarchyNode();
      long      topNode     = 0L;
      
      try
      {
        topNode = 3941000000L + MesDefaults.getLong(MesDefaults.DK_TRANSCOM_TOP_NODE);
        
        /*@lineinfo:generated-code*//*@lineinfo:5897^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(th.descendent) 
//            from    t_hierarchy   th
//            where   th.hier_type = 1 and
//                    th.ancestor = :reportNode and
//                    th.descendent = :topNode
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(th.descendent)  \n          from    t_hierarchy   th\n          where   th.hier_type = 1 and\n                  th.ancestor =  :1   and\n                  th.descendent =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,reportNode);
   __sJT_st.setLong(2,topNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   matchCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:5904^9*/
      }
      catch(Exception e)
      {
        // do nothing
      }        
      
      if ( matchCount > 0 )
      {
        setReportHierarchyNode( topNode );
      }
    }
    
    // setup the show closed flag
    if ( HttpHelper.getString(request,"showClosed",null) == null )
    {
      try
      {
        String    paramValue  = null;
        long      nodeId      = ReportUserBean.getHierarchyNode();
        
        /*@lineinfo:generated-code*//*@lineinfo:5925^9*/

//  ************************************************************
//  #sql [Ctx] { select  lower(bp.param_value) 
//            from    merch_prof_bank_params  bp
//            where   bp.hierarchy_node = :nodeId and
//                    bp.param_type = 2
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  lower(bp.param_value)  \n          from    merch_prof_bank_params  bp\n          where   bp.hierarchy_node =  :1   and\n                  bp.param_type = 2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.reports.BankContractDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   paramValue = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:5931^9*/
        ShowClosedAccounts = ( paramValue.equals("true") ||
                               paramValue.equals("y") );
      }
      catch( Exception e )
      {
        ShowClosedAccounts = true;    // show all by default
      }        
    }
    else
    {
      ShowClosedAccounts = HttpHelper.getBoolean(request,"showClosed",false);
    }      
  }
  
  public void setReportBetGroup( int betGroup )
  {
    ReportBetGroup = betGroup;
  }
  
  public void setReportHierarchyNodeDefault( long nodeId )
  {
    super.setReportHierarchyNodeDefault(nodeId);
    setUserType(UT_INVALID);    // invalidate the user type
  }
  
  public void setUserType( int userType )
  {
    ReportUserType = userType;
  }
  
  public boolean showClosed( )
  {
    return( ShowClosedAccounts );
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/