/*@lineinfo:filename=DiscoverEvergreenContract*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/DiscoverEvergreenContract.sqlj $

  Description:  


  Last Modified By   : $Author: Jduncan $
  Last Modified Date : $Date: 7/31/03 2:51p $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesUsers;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.MesMath;
import sqlj.runtime.ResultSetIterator;

public class DiscoverEvergreenContract extends ReportSQLJBean
{
  public class RowData
  {
    public int            DaysToActivate    = 0;
    public String         ElementDesc       = null;
    public int            ElementType       = 0;
    public long           HierarchyNode     = 0L;
    public String         OrgName           = null;
    public double         Rate              = 0.0;
    public String         RateType          = null;
    public long           RecId             = 0L;
    public Date           ValidDateBegin    = null;
    public Date           ValidDateEnd      = null;
    public String         VolumeColName     = null;
    
    public RowData( ResultSet       resultSet )
      throws java.sql.SQLException
    {
      DaysToActivate    = resultSet.getInt("days_to_activate");
      ElementDesc       = resultSet.getString("element_desc");
      ElementType       = resultSet.getInt("element_type");
      HierarchyNode     = resultSet.getLong("hierarchy_node");
      OrgName           = resultSet.getString("org_name");
      Rate              = resultSet.getDouble("rate");
      RateType          = resultSet.getString("rate_type");
      RecId             = resultSet.getLong("rec_id");
      ValidDateBegin    = resultSet.getDate("valid_date_begin");
      ValidDateEnd      = resultSet.getDate("valid_date_end");
      VolumeColName     = resultSet.getString("vol_col_name");
    }
    
    public boolean isActive( Date activeDate, int openToActiveDays )
    {
      boolean     retVal      = false;
      
      if ( !activeDate.before( ValidDateBegin ) &&
           !activeDate.after( ValidDateEnd ) &&
           ( (DaysToActivate == 0) || (openToActiveDays <= DaysToActivate) )
         )
      {
        retVal = true;
      }             
      
      return( retVal );
    }
  }
  
  public DiscoverEvergreenContract( )
  {
  }
  
  protected void deleteContractElement( long id )
  {
    try
    {
      setAutoCommit(false);
      
      /*@lineinfo:generated-code*//*@lineinfo:107^7*/

//  ************************************************************
//  #sql [Ctx] { delete  
//          from    discover_referral_contract rc
//          where   rc.rec_id = :id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  \n        from    discover_referral_contract rc\n        where   rc.rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.reports.DiscoverEvergreenContract",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:112^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:114^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:117^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("deleteContractElement()",e.toString());
      try{ /*@lineinfo:generated-code*//*@lineinfo:122^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:122^34*/ } catch( Exception ee ){}
    }
    finally
    {
    }
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Hierarchy Node\",");
    line.append("\"Contract Element\",");
    line.append("\"Effective Date\",");
    line.append("\"Expiration Date\",");
    line.append("\"Rate\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RowData         record    = (RowData)obj;
    
    // clear the line buffer and add 
    // the merchant specific data.
    line.setLength(0);
    line.append(encodeHierarchyNode(record.HierarchyNode));
    line.append(",\"");
    line.append(record.ElementDesc);
    line.append("\",");
    line.append(DateTimeFormatter.getFormattedDate(record.ValidDateBegin,"MM/dd/yyyy"));
    line.append(",");
    line.append(DateTimeFormatter.getFormattedDate(record.ValidDateEnd,"MM/dd/yyyy"));
    line.append(",");
    line.append(MesMath.toFractionalCurrency(record.Rate,5));
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append("discover_evergreen_contracts_");
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate(ReportDateBegin,"MMddyy") );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate(ReportDateEnd,"MMddyy") );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( )
  {
    loadData( getReportHierarchyNode(), ReportDateBegin, ReportDateEnd );
  }
  
  public void loadData( long nodeId, Date beginDate, Date endDate )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      /*@lineinfo:generated-code*//*@lineinfo:200^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rc.rec_id                             as rec_id,
//                  rc.hierarchy_node                     as hierarchy_node,
//                  o.org_name                            as org_name,
//                  rc.billing_element_type               as element_type,
//                  nvl(ce.element_desc,'Not Available (' || rc.billing_element_type || ')')
//                                                        as element_desc,
//                  rc.valid_date_begin                   as valid_date_begin,
//                  rc.valid_date_end                     as valid_date_end,
//                  rc.rate                               as rate,
//                  nvl(rc.days_to_activate,0)            as days_to_activate,
//                  ce.vol_col_name                       as vol_col_name,
//                  nvl(ce.rate_type,'P')                 as rate_type
//          from    t_hierarchy                   th,
//                  discover_referral_contract    rc,
//                  discover_contract_elements    ce,
//                  organization                  o
//          where   th.hier_type = 1 and
//                  th.ancestor = :nodeId and
//                  rc.hierarchy_node = th.descendent and
//                  ce.element_type(+) = rc.billing_element_type and
//                  o.org_group(+) = rc.hierarchy_node
//          order by o.org_name, rc.hierarchy_node, nvl(ce.element_desc,'Not Available (' || rc.billing_element_type || ')'),
//                    rc.valid_date_begin, rc.valid_date_end
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rc.rec_id                             as rec_id,\n                rc.hierarchy_node                     as hierarchy_node,\n                o.org_name                            as org_name,\n                rc.billing_element_type               as element_type,\n                nvl(ce.element_desc,'Not Available (' || rc.billing_element_type || ')')\n                                                      as element_desc,\n                rc.valid_date_begin                   as valid_date_begin,\n                rc.valid_date_end                     as valid_date_end,\n                rc.rate                               as rate,\n                nvl(rc.days_to_activate,0)            as days_to_activate,\n                ce.vol_col_name                       as vol_col_name,\n                nvl(ce.rate_type,'P')                 as rate_type\n        from    t_hierarchy                   th,\n                discover_referral_contract    rc,\n                discover_contract_elements    ce,\n                organization                  o\n        where   th.hier_type = 1 and\n                th.ancestor =  :1  and\n                rc.hierarchy_node = th.descendent and\n                ce.element_type(+) = rc.billing_element_type and\n                o.org_group(+) = rc.hierarchy_node\n        order by o.org_name, rc.hierarchy_node, nvl(ce.element_desc,'Not Available (' || rc.billing_element_type || ')'),\n                  rc.valid_date_begin, rc.valid_date_end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.DiscoverEvergreenContract",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.DiscoverEvergreenContract",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:225^7*/
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.addElement( new RowData( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",nodeId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    long        delId     = 0L;
  
    // load the default report properties
    super.setProperties( request );
    
    if ( ( (delId = HttpHelper.getLong(request,"deleteId",-1L)) != -1L ) &&
         ( ReportUserBean.hasRight( MesUsers.RIGHT_REPORT_DISC_EVERGREEN_ADMIN ) ) )
    {
      deleteContractElement(delId);
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/