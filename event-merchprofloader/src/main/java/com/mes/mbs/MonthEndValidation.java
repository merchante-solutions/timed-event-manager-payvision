/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/trunk/src/main/com/mes/mbs/BillingDb.java $

  Description:

  Last Modified By   : $Author: mduyck $
  Last Modified Date : $LastChangedDate: 2013-02-11 15:25:30 -0800 (Mon, 11 Feb 2013) $
  Version            : $Revision: 20913 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mbs;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;

public class MonthEndValidation extends SQLJConnectionBase	{
	
	private static final long serialVersionUID = 1L;
	private int bankNumber;
	private Date activeDate;
	
	private static final int IC_ACCEPTABLE_VARIANCE = 10000;
	static Logger log = Logger.getLogger(MonthEndValidation.class);
	
	public MonthEndValidation(int bankNumber, Date activeDate) {
		this.bankNumber = bankNumber;
		this.activeDate = activeDate;		
	}
	
	public boolean validate() {
		connect(true);
		
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		
		int paramIdx = 1;
		boolean isMonthEndGood = false;
		
		try {
			pStmt = getPreparedStatement(IC_DIFF_SQL);
			
			pStmt.setInt(paramIdx++, bankNumber); 
			pStmt.setDate(paramIdx++, activeDate);
			pStmt.setDate(paramIdx++, activeDate);
			pStmt.setDate(paramIdx++, activeDate);
			pStmt.setInt(paramIdx++, bankNumber); 
			pStmt.setDate(paramIdx++, activeDate);
			pStmt.setDate(paramIdx++, activeDate);
			pStmt.setInt(paramIdx++, bankNumber); 
			pStmt.setDate(paramIdx++, activeDate);
			pStmt.setDate(paramIdx++, activeDate);
			
			rs = pStmt.executeQuery();
			
			if(rs.next()) {				
				StringBuffer msgBody = new StringBuffer();
				
				isMonthEndGood = rs.getDouble("VISA_IC_DIFF") < IC_ACCEPTABLE_VARIANCE 
						&& rs.getDouble("VISA_IC_DIFF") * -1 < IC_ACCEPTABLE_VARIANCE 
						&& rs.getDouble("MC_IC_DIFF") < IC_ACCEPTABLE_VARIANCE
						&& rs.getDouble("MC_IC_DIFF") * -1 < IC_ACCEPTABLE_VARIANCE
						&& rs.getDouble("DISC_IC_DIFF") < IC_ACCEPTABLE_VARIANCE
						&& rs.getDouble("DISC_IC_DIFF") * -1 < IC_ACCEPTABLE_VARIANCE;
				
				msgBody.append("The IC differences between Daily Clearing report and Month End Summarization ");
				msgBody.append(isMonthEndGood ? "have" : "DO NOT have");
				msgBody.append(" acceptable variance:\n\n");
				msgBody.append("MONTH                        : " + rs.getDate("MONTH") + "\n");
			    msgBody.append("BANK_NUMBER       : " + rs.getInt("BANK_NUMBER") + "\n\n");
				msgBody.append("MONTHLY_VISA_IC   : " + rs.getDouble("MONTHLY_VISA_IC") + "\n");
				msgBody.append("CLEARING_VISA_IC  : " + rs.getDouble("CLEARING_VISA_IC") + "\n");
				msgBody.append("VISA_IC_DIFF              : " + rs.getDouble("VISA_IC_DIFF") + "\n\n");
				msgBody.append("MONTHLY_MC_IC     : " + rs.getDouble("MONTHLY_MC_IC") + "\n");
				msgBody.append("CLEARING_MC_IC    : " + rs.getDouble("CLEARING_MC_IC") + "\n");
				msgBody.append("MC_IC_DIFF                : " + rs.getDouble("MC_IC_DIFF") + "\n\n");
				msgBody.append("MONTHLY_DISC_IC   : " + rs.getDouble("MONTHLY_DISC_IC") + "\n");
				msgBody.append("CLEARING_DISC_IC  : " + rs.getDouble("CLEARING_DISC_IC") + "\n");
				msgBody.append("DISC_IC_DIFF              : " + rs.getDouble("DISC_IC_DIFF") + "\n");
				
				sendEmail(msgBody);
			}
		} catch(SQLException sqe) {
			log.error("validate()", sqe);
		} finally {
			try { 
				if(pStmt != null) 
					pStmt.close(); 
			} catch(Exception ignore) {}
			
		    try { 
		    	if(rs != null) 
					rs.close(); 
		    } catch(Exception ignore) {}
		}
		
		return isMonthEndGood;
	}
	
	
	private void sendEmail(StringBuffer msgBody) {
	    
		try {
			log.debug(msgBody);
			
			MailMessage msg = new MailMessage();
			msg.setAddresses(MesEmails.MSG_ADDRS_MONTH_END_VALIDATION_NOTIFY); // 176
			msg.setSubject("Month End Validation - " + bankNumber);
			msg.setText(msgBody.toString());
			msg.send();
	    } catch(Exception e) {
	    	log.error("validate()", e);
	    }
	}
	
	public static void main(String args[]) {
		if(args.length > 0) {
			try {
				MonthEndValidation mev = new MonthEndValidation(Integer.parseInt(args[0]), 
					new Date(DateTimeFormatter.parseDate(args[1],"MM/dd/yyyy").getTime()) );
				mev.validate();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private final static String NL = "\n";
	
	private String IC_DIFF_SQL = "select     sm.active_date                        as month, " + NL + 
			"            gn.hh_bank_number                     as bank_number, " + NL + 
			"            sum(sm.visa_interchange)              as monthly_visa_ic, " + NL + 
			"            clearing_data.visa_ic                 as clearing_visa_ic, " + NL + 
			"            sum(sm.visa_interchange) + " + NL + 
			"                clearing_data.visa_ic             as visa_ic_diff,         " + NL + 
			"            sum(sm.mc_interchange)                as monthly_mc_ic, " + NL + 
			"            clearing_data.mc_ic                   as clearing_mc_ic, " + NL + 
			"            sum(sm.mc_interchange) + " + NL + 
			"                clearing_data.mc_ic             as mc_ic_diff, " + NL + 
			"            sum( nvl(sm.disc_interchange,0) )     as monthly_disc_ic, " + NL + 
			"            clearing_data.disc_ic                 as clearing_disc_ic, " + NL + 
			"            sum(sm.disc_interchange) + " + NL + 
			"                clearing_data.disc_ic             as disc_ic_diff " + NL + 
			"from        monthly_extract_gn        gn, " + NL + 
			"            monthly_extract_summary   sm, " + NL + 
			"            mif                       mf, " + NL + 
			"            groups                    g, " + NL + 
			"            channel_link              cl, " + NL + 
			"            organization              o,     " + NL + 
			"            ( " + NL + 
			"            select  vf.merchant_number, " + NL + 
			"                    vf.active_date, " + NL + 
			"                    sum(nvl(vf.fee,0)) fee " + NL + 
			"            from    visa_fanf_summary vf " + NL + 
			"            where   vf.merchant_number in (select merchant_number from mif where bank_number in (?)) " + NL + 
			"                    and vf.active_date between ? and last_day(?) " + NL + 
			"            group by vf.merchant_number, vf.active_date                   " + NL + 
			"            ) fanf, " + NL + 
			"            (    " + NL + 
			"            select month,  " + NL + 
			"                    bank_number,  " + NL + 
			"                    sum(visa_ic)            as visa_ic,  " + NL + 
			"                    sum(mc_ic)              as mc_ic,  " + NL + 
			"                    sum(amex_ic)            as amex_ic,  " + NL + 
			"                    sum(disc_ic)            as disc_ic " + NL + 
			"            from (  " + NL + 
			"                select  ?                                  as month, " + NL + 
			"                        bank_number, " + NL + 
			"                        case when card_type = 'VS'  " + NL + 
			"                                then sum(reimbursement_fees)  " + NL + 
			"                                else 0   end                        as visa_ic,         " + NL + 
			"                        case when card_type = 'MC'  " + NL + 
			"                                then sum(reimbursement_fees)  " + NL + 
			"                                else 0   end                        as mc_ic, " + NL + 
			"                        case when card_type = 'AM'  " + NL + 
			"                                then sum(reimbursement_fees)  " + NL + 
			"                                else 0   end                        as amex_ic, " + NL + 
			"                        case when card_type = 'DS'  " + NL + 
			"                                then sum(reimbursement_fees)  " + NL + 
			"                                else 0   end                        as disc_ic " + NL + 
			"                from    daily_clearing_summary " + NL + 
			"                where   bank_number = ? " + NL + 
			"                        and clearing_date between ? and last_day(?) " + NL + 
			"                group by bank_number, card_type) " + NL + 
			"            group by month, bank_number ) clearing_data " + NL + 
			"where   gn.hh_bank_number in (?) and " + NL + 
			"        gn.hh_active_date between ? and last_day(?) and " + NL + 
			"        sm.merchant_number  = gn.hh_merchant_number and " + NL + 
			"        gn.hh_merchant_number = fanf.merchant_number(+) and " + NL + 
			"        fanf.active_date(+) = gn.hh_active_date and " + NL + 
			"        sm.active_date      = gn.hh_active_date and " + NL + 
			"        mf.merchant_number  = sm.merchant_number and " + NL + 
			"        g.assoc_number      = mf.association_node and " + NL + 
			"        o.org_group         = cl.hierarchy_node(+) and " + NL + 
			"        o.org_group         = g.group_2 and " + NL + 
			"        sm.active_date      = clearing_data.month and " + NL + 
			"        sm.bank_number      = clearing_data.bank_number  " + NL + 
			"group by  sm.active_date, gn.hh_bank_number, clearing_data.visa_ic, clearing_data.mc_ic, clearing_data.disc_ic  " + NL; 
  
}