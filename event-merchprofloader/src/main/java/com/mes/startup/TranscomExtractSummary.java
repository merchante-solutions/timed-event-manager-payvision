/*@lineinfo:filename=TranscomExtractSummary*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/TranscomExtractSummary.sqlj $

  Description:  
   Trigger to load Vital monthly extract data into the transcom summary table.


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.StringTokenizer;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.reports.BankContractBean;
import com.mes.reports.ContractTypes;
import sqlj.runtime.ResultSetIterator;

public class TranscomExtractSummary extends SQLJConnectionBase
{
  // Transcom charge types
  private static final int  TCT_DEFAULT_CHARGE_TYPE         = 9999;

  private static final int  CT_VS                           = 0;
  private static final int  CT_MC                           = 1;
  private static final int  CT_COUNT                        = 2;
  
  private static final int  VT_SALES                        = 0;
  private static final int  VT_CREDITS                      = 1;
  private static final int  VT_COUNT                        = 2;
  
  private static final int  MC_CAT_COUNT                    = 12;
  private static final int  VISA_CAT_COUNT                  = 25;
  
  private static final int  AUTH_FIELD_FIXED                = 0;
  private static final int  AUTH_FIELD_INTERSTATE_1         = 1;
  private static final int  AUTH_FIELD_INTERSTATE_2         = 2;
  private static final int  AUTH_FIELD_LOCAL                = 3;
  private static final int  AUTH_FIELD_950                  = 4;
  private static final int  AUTH_FIELD_SWITCHED             = 5;
  private static final int  AUTH_FIELD_ABOVE_FLOOR          = 6;
  private static final int  AUTH_FIELD_BELOW_FLOOR          = 7;
  private static final int  AUTH_FIELD_OTHER                = 8;
  private static final int  AUTH_FIELD_COUNT                = 9;
  
  private static final int  AUTH_VENDOR_VISANET             = 0;
  private static final int  AUTH_VENDOR_COUNT               = 1;
  
  private static final int  AUTH_TYPE_TERMINAL              = 0;
  private static final int  AUTH_TYPE_VOICE                 = 1;
  private static final int  AUTH_TYPE_ARU                   = 2;
  private static final int  AUTH_TYPE_ECR                   = 3;
  private static final int  AUTH_TYPE_BATCH                 = 4;
  private static final int  AUTH_TYPE_FIXED                 = 5;
  private static final int  AUTH_TYPE_COUNT                 = 6;
  
  private static final String[] AuthFieldNames = 
  {
    "num_fixed_auths",              // AUTH_FIELD_FIXED        
    "num_interstate1_auths",        // AUTH_FIELD_INTERSTATE_1 
    "num_interstate2_auths",        // AUTH_FIELD_INTERSTATE_2 
    "num_local_auths",              // AUTH_FIELD_LOCAL        
    "num_950_auths",                // AUTH_FIELD_950          
    "num_switched_auths",           // AUTH_FIELD_SWITCHED     
    "num_above_floor_auths",        // AUTH_FIELD_ABOVE_FLOOR  
    "num_below_floor_auths",        // AUTH_FIELD_BELOW_FLOOR  
    "num_other_auths",              // AUTH_FIELD_OTHER        
  };
    
  private static final String[] AuthTypeNames = 
  {
    "TE",         // AUTH_TYPE_TERMINAL
    "VO",         // AUTH_TYPE_VOICE
    "AR",         // AUTH_TYPE_ARU
    "EC",         // AUTH_TYPE_ECR
    "BA",         // AUTH_TYPE_BATCH
    "FX",         // AUTH_TYPE_FIXED
  };
  
  private static final String[] AuthVendorNames = 
  {
    "VI",         // AUTH_VENDOR_VISANET
  };
  
  private static final String[] DebitCatToTranscomCardType =
  {
    "10",         // cat 01  - All Other
    "11",         // cat 02  - Honor
    "26",         // cat 03  - Interlink
    "13",         // cat 04  - MAC
    "25",         // cat 05  - Explore-Star
    "18",         // cat 06  - Pulse
    "21",         // cat 07  - Yankee 24
    "01",         // cat 08  - Accel
    "10",         // cat 09  - ?
    "10",         // cat 10  - ?
  };
  
  // indexes into the ChargeRecDescToTranscomChargeType array
  private static final int        CG_CHARGE_CODE            = 0;
  private static final int        CG_CHARGE_DESC            = 1;
  private static final int        CG_TRANSCOM_TYPE          = 2;
  
  private static final String[][] TranscomContractElements =
  {
//    bet type                                                              field name                TPS description                      transcom   MES 
//                                                                                                                                         chg type prod code                      
    { Integer.toString(BankContractBean.BET_INCOMING_CHARGEBACK_PER_ITEM) , "cb_count"              , "Incoming Chargebacks"              , "1400",  null },
    { Integer.toString(BankContractBean.BET_NEW_ACCOUNT_SETUP_FEE       ) , "new_merchant_count"    , "New Account Setup Fee"             , "6000",  null },
    { Integer.toString(BankContractBean.BET_POS_PARTNER_SETUP_FEE       ) , "new_merchant_count"    , "POS Partner Setup Fee"             , "6000",  "P0" },
    { Integer.toString(BankContractBean.BET_POS_PARTNER_SETUP_FEE       ) , "new_merchant_count"    , "POS Partner Setup Fee"             , "6000",  "P2" },
    { Integer.toString(BankContractBean.BET_POS_PARTNER_FEE             ) , "active_merchant_count" , "POS Partner Monthly Fee"           , "6000",  null },
    { Integer.toString(BankContractBean.BET_INCOMING_RETRIEVAL_REQUEST  ) , "retr_count"            , "Retrieval Requests"                , "1411",  null },
    { Integer.toString(BankContractBean.BET_CUSTOMER_SERVICE_CALLS      ) , "service_call_count"    , "Help Desk Support"                 , "4101",  null },
    { Integer.toString(BankContractBean.BET_AMEX_SETUP_FEE              ) , "amex_setup_count"      , "Amex ESA Setup"                    , "6820",  null },
    { Integer.toString(BankContractBean.BET_DISCOVER_SETUP_FEE          ) , "disc_setup_count"      , "Discover RAP Setup"                , "6821",  null },
    { Integer.toString(BankContractBean.BET_VERISIGN_PAYLINK_FEE        ) , "active_merchant_count" , "Verisign Payflow Link Monthly Fee" , "6067",  "VL" },
    { Integer.toString(BankContractBean.BET_VERISIGN_PAYLINK_SETUP_FEE  ) , "new_merchant_count"    , "Verisign Payflow Link Setup Fee"   , "6068",  "VL" },
    { Integer.toString(BankContractBean.BET_VERISIGN_PAYFLOW_FEE        ) , "active_merchant_count" , "Verisign Payflow Pro Monthly Fee"  , "6069",  "VF" },
    { Integer.toString(BankContractBean.BET_VERISIGN_PAYFLOW_SETUP_FEE  ) , "new_merchant_count"    , "Verisign Payflow Pro Setup Fee"    , "6070",  "VF" },
    { Integer.toString(BankContractBean.BET_WEB_REPORTING_FEE           ) , "active_merchant_count" , "Web Reporting"                     , "6127",  null },
    { Integer.toString(BankContractBean.BET_RISK_MANAGEMENT_REPORTING   ) , "active_merchant_count" , "Risk Reporting"                    , "6128",  null },
    { Integer.toString(BankContractBean.BET_RISK_MONITORING             ) , "active_merchant_count" , "Risk Monitoring"                   , "6129",  null },
  };
  
  // indexes into the TranscomContractElements array element
  private static final int        CE_BET_TYPE               = 0;
  private static final int        CE_COUNT_FIELD_NAME       = 1;
  private static final int        CE_DESCRIPTION            = 2;
  private static final int        CE_TRANSCOM_CHARGE_TYPE   = 3;
  private static final int        CE_PRODUCT_CODE           = 4;
  
  private static final String[][] ChargeRecDescToTranscomChargeType = 
  { 
  //  code    desc                            transcom charge type
    { "IMP",  null                        ,   "4001" },   // all imprinter records
    { "POS",  "RENTAL"                    ,   "4007" },   // all POS rec with RENTAL in the description
    { null ,  "POSPTRSETUP"               ,   "4099" },   // all POS Partner setups
    { null ,  "EQUIP%SUP"                 ,   "4100" },   // all equipment support fees
    { null ,  "REPORTING"                 ,   "6039" },   // all reporting fees
    { null ,  "NEW ACCOUNT SETUP"         ,   "6064" },   // new account setup fees
    { null ,  "APPLICATION FEE"           ,   "6064" },   // application fees
    { null ,  "STATEMENT"                 ,   "6112" },   // statement fees
    { null ,  "SERVICE FEE"               ,   "6112" },   // statement fees
    { null ,  "%TRAINING%"                ,   "6420" },   // training fees
    { null ,  "DEBIT ACCESS FEE"          ,   "6031" },   // debit access fee
    { null ,  "TERMINAL%REPROGRAMMING%"   ,   "6406" },   // reprogramming fee
    { null ,  "TERMINAL%SUPPORT%"         ,   "4100" },   // equipment support fee
    { null ,  "%INTERNET%REPORT%FEE%"     ,   "6003" },   // internet report fee
    { null ,  "%PIN%PAD%FEE"              ,   "6208" },   // pin pad swap fee
    { null ,  "%MISC%FEE"                 ,   "6038" },   // misc fee
    { null ,  "%PCI%"                     ,   "6038" },   // pci compliance fee
    { "POS",  null                        ,   "9999" },   // all remaining POS records
    { null ,  null                        ,   "9999" },   // all remaining records 
  };
  
  private static final String[][] PlanSummaryFields = 
  {
//    field                                  transcom   transcom  transcom                  
//    prefix  vital plan type                card type  chg type  reference                 
    { "amex", mesConstants.VITAL_ME_PL_AMEX,   "30",     "1203",  "American Express Volume"   },
    { "disc", mesConstants.VITAL_ME_PL_DISC,   "32",     "1203",  "Discover Volume"           },
    { "dinr", mesConstants.VITAL_ME_PL_DINERS, "31",     "1203",  "Diners Volume"             },
    { "jcb",  mesConstants.VITAL_ME_PL_JCB,    "33",     "1203",  "JCB Volume"                },
    { "vmc",  mesConstants.VITAL_ME_PL_VMC,    null,     "2453",  "V/MC Plan Volume (MASP)"   },
  };
  
  // indexes into the PlanSummaryFields array element
  private static final int  PL_FIELD_PREFIX     = 0;
  private static final int  PL_PLAN_TYPE        = 1;
  private static final int  PL_TPS_CARD_TYPE    = 2;
  private static final int  PL_TPS_CHG_TYPE     = 3;
  private static final int  PL_TPS_REFERENCE    = 4;

  private static final String[][] TranscomAuthIncChargeTypes = 
  {
    { "AM", "2900" },     // amex
    { "DC", "2901" },     // diners
    { "CB", "2901" },     // carte blanc
    { "DS", "2902" },     // discover
  };
    
  private static final int[][][] TranscomAuthChargeTypes = 
  {
    {         // AUTH_VENDOR_VISANET
      {         // AUTH_TYPE_TERMINAL    
        2314,     // AUTH_FIELD_FIXED        
        2314,     // AUTH_FIELD_INTERSTATE_1 
        2314,     // AUTH_FIELD_INTERSTATE_2 
        2321,     // AUTH_FIELD_LOCAL        
        2324,     // AUTH_FIELD_950          
        2314,     // AUTH_FIELD_SWITCHED     
        2314,     // AUTH_FIELD_ABOVE_FLOOR  
        2314,     // AUTH_FIELD_BELOW_FLOOR  
        2314,     // AUTH_FIELD_OTHER        
      },
      {         // AUTH_TYPE_VOICE
        2314,     // AUTH_FIELD_FIXED        
        2314,     // AUTH_FIELD_INTERSTATE_1 
        2314,     // AUTH_FIELD_INTERSTATE_2 
        2321,     // AUTH_FIELD_LOCAL        
        2324,     // AUTH_FIELD_950          
        2314,     // AUTH_FIELD_SWITCHED     
        2314,     // AUTH_FIELD_ABOVE_FLOOR  
        2314,     // AUTH_FIELD_BELOW_FLOOR  
        2314,     // AUTH_FIELD_OTHER        
      },
      {         // AUTH_TYPE_ARU
        2314,     // AUTH_FIELD_FIXED        
        2314,     // AUTH_FIELD_INTERSTATE_1 
        2314,     // AUTH_FIELD_INTERSTATE_2 
        2321,     // AUTH_FIELD_LOCAL        
        2324,     // AUTH_FIELD_950          
        2314,     // AUTH_FIELD_SWITCHED     
        2314,     // AUTH_FIELD_ABOVE_FLOOR  
        2314,     // AUTH_FIELD_BELOW_FLOOR  
        2314,     // AUTH_FIELD_OTHER        
      },
      {         // AUTH_TYPE_ECR
        2314,     // AUTH_FIELD_FIXED        
        2314,     // AUTH_FIELD_INTERSTATE_1 
        2314,     // AUTH_FIELD_INTERSTATE_2 
        2321,     // AUTH_FIELD_LOCAL        
        2324,     // AUTH_FIELD_950          
        2314,     // AUTH_FIELD_SWITCHED     
        2314,     // AUTH_FIELD_ABOVE_FLOOR  
        2314,     // AUTH_FIELD_BELOW_FLOOR  
        2314,     // AUTH_FIELD_OTHER        
      },
      {         // AUTH_TYPE_BATCH
        2314,     // AUTH_FIELD_FIXED        
        2314,     // AUTH_FIELD_INTERSTATE_1 
        2314,     // AUTH_FIELD_INTERSTATE_2 
        2321,     // AUTH_FIELD_LOCAL        
        2324,     // AUTH_FIELD_950          
        2314,     // AUTH_FIELD_SWITCHED     
        2314,     // AUTH_FIELD_ABOVE_FLOOR  
        2314,     // AUTH_FIELD_BELOW_FLOOR  
        2314,     // AUTH_FIELD_OTHER        
      },
      {         // AUTH_TYPE_FIXED
        2314,     // AUTH_FIELD_FIXED        
        2314,     // AUTH_FIELD_INTERSTATE_1 
        2314,     // AUTH_FIELD_INTERSTATE_2 
        2321,     // AUTH_FIELD_LOCAL        
        2324,     // AUTH_FIELD_950          
        2314,     // AUTH_FIELD_SWITCHED     
        2314,     // AUTH_FIELD_ABOVE_FLOOR  
        2314,     // AUTH_FIELD_BELOW_FLOOR  
        2314,     // AUTH_FIELD_OTHER        
      },
    },
  };      
       
  private   BankContractBean     LiabilityContract = new BankContractBean();
  
  private String authIncTypeToTranscomChargeType( String authPlanType )
  {
    String    retVal        = "99";
    
    try
    {
      for( int i = 0; i < TranscomAuthIncChargeTypes.length; ++i )
      {
        if( TranscomAuthIncChargeTypes[i][0].equals(authPlanType) )
        {
          retVal = TranscomAuthIncChargeTypes[i][1];
          break;
        }
      }
    }
    catch( Exception e )
    {
      logEntry( "authIncTypeToTranscomChargeType( " + authPlanType + " )", e.toString() );
    }
    return( retVal );
  }
  
  private int authTypeToTranscomChargeType( String vitalVendor, String vitalAuthType, int fieldIdx )
  {
    int       authTypeIdx   = vitalAuthTypeToIndex( vitalAuthType );
    int       retVal        = 0;
    int       vendorIdx     = AUTH_VENDOR_VISANET; // always VisaNet
    
    try
    {
      retVal = TranscomAuthChargeTypes[vendorIdx][authTypeIdx][fieldIdx];
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
      logEntry( "authTypeToTranscomChargeType( " + vitalVendor + "," + vitalAuthType + "," + fieldIdx + " )", e.toString() );
    }
    return( retVal );
  }
  
  private Date getActivityBeginDate( long merchantId, Date activeDate )
  {
    Date          beginDate     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:323^7*/

//  ************************************************************
//  #sql [Ctx] { -- since the extract arrives on the last business day
//          -- the date range for the data is not necessarily the
//          -- first day to the last day of the month.  in order
//          -- to correctly summarize the chargebacks and retrievals
//          -- it is necessary to derive the date range using the
//          -- extract date ranges.
//          select decode(gnp.hh_curr_date,
//                        null, gn.hh_active_date,
//                       (gnp.hh_curr_date+1) )      
//          from   monthly_extract_gn      gn,
//                 monthly_extract_gn      gnp
//          where  gn.hh_merchant_number = :merchantId and
//                 gn.hh_active_date     = :activeDate and
//                 gnp.hh_merchant_number(+) = gn.hh_merchant_number and
//                 gnp.hh_active_date(+) = to_date(to_char((:activeDate-15),'mm/yyyy'),'mm/yyyy')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "-- since the extract arrives on the last business day\n        -- the date range for the data is not necessarily the\n        -- first day to the last day of the month.  in order\n        -- to correctly summarize the chargebacks and retrievals\n        -- it is necessary to derive the date range using the\n        -- extract date ranges.\n        select decode(gnp.hh_curr_date,\n                      null, gn.hh_active_date,\n                     (gnp.hh_curr_date+1) )       \n        from   monthly_extract_gn      gn,\n               monthly_extract_gn      gnp\n        where  gn.hh_merchant_number =  :1  and\n               gn.hh_active_date     =  :2  and\n               gnp.hh_merchant_number(+) = gn.hh_merchant_number and\n               gnp.hh_active_date(+) = to_date(to_char(( :3 -15),'mm/yyyy'),'mm/yyyy')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TranscomExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:340^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "getActivityBeginDate( " + merchantId + "," + activeDate + " )", e.toString() );
    }
    return( beginDate );
  }
  
  private Date getActivityEndDate( long merchantId, Date activeDate )
  {
    Date          endDate     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:355^7*/

//  ************************************************************
//  #sql [Ctx] { select gn.hh_curr_date    
//          from   monthly_extract_gn      gn
//          where  gn.hh_merchant_number = :merchantId and
//                 gn.hh_active_date     = :activeDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select gn.hh_curr_date     \n        from   monthly_extract_gn      gn\n        where  gn.hh_merchant_number =  :1  and\n               gn.hh_active_date     =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.TranscomExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   endDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:361^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "getActivityEndDate( " + merchantId + "," + activeDate + " )", e.toString() );
    }
    return( endDate );
  }
  
  private String getIcCatDesc( String cardType, int cat )
  {
    String      retVal = "";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:376^7*/

//  ************************************************************
//  #sql [Ctx] { select description 
//          from   ic_category_desc
//          where  card_type  = :cardType and
//                 category   = :cat
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select description  \n        from   ic_category_desc\n        where  card_type  =  :1  and\n               category   =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.TranscomExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,cardType);
   __sJT_st.setInt(2,cat);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:382^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "getIcCatDesc(" + cardType + "," + cat + ")", e.toString() );
    }
    return( retVal );
  }
  
  private String getTranscomIcCardType( String cardType, int cat )
  {
    String      retVal = "INVALID";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:397^7*/

//  ************************************************************
//  #sql [Ctx] { select transcom_card_type 
//          from   ic_category_desc
//          where  card_type  = :cardType and
//                 category   = :cat
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select transcom_card_type  \n        from   ic_category_desc\n        where  card_type  =  :1  and\n               category   =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.TranscomExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,cardType);
   __sJT_st.setInt(2,cat);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:403^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "getTranscomIcCardType(" + cardType + "," + cat + ")", e.toString() );
    }
    return( retVal );
  }
  
  private String getTranscomIcChargeType( String cardType, int cat )
  {
    String      retVal = "INVALID";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:418^7*/

//  ************************************************************
//  #sql [Ctx] { select transcom_charge_type 
//          from   ic_category_desc
//          where  card_type  = :cardType and
//                 category   = :cat
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select transcom_charge_type  \n        from   ic_category_desc\n        where  card_type  =  :1  and\n               category   =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.TranscomExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,cardType);
   __sJT_st.setInt(2,cat);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:424^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "getTranscomIcChargeType(" + cardType + "," + cat + ")", e.toString() );
    }
    return( retVal );
  }
  
  private long getTranscomHierarchyNode( String loadFilename )
  {
    String        groupNum  = "";
    long          retVal    = 0L;
    
    try
    {
      groupNum = MesDefaults.getString(MesDefaults.DK_TRANSCOM_TOP_NODE);
      /*@lineinfo:generated-code*//*@lineinfo:441^7*/

//  ************************************************************
//  #sql [Ctx] { select  get_file_bank_number(:loadFilename) || :groupNum
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  get_file_bank_number( :1 ) ||  :2 \n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.TranscomExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setString(2,groupNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:446^7*/
    }
    catch( Exception e )
    {
      logEntry( "getTranscomHierarchyNode(" + loadFilename + ")", e.toString() );
    }
    return( retVal );
  }
  
  public void loadAuthData( long merchantId, Date activeDate, String loadFilename )
  {
    int                   authCount         = 0;
    double                authExp           = 0.0;
    double                authInc           = 0.0;
    double                authIncPerItem    = 0.0;
    double                authPerItem       = 0.0;
    String                cardTypeString    = null;
    ResultSetIterator     it                = null;
    ResultSet             resultSet         = null;
    int                   tcChargeType      = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:469^7*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ INDEX (gn idx_mon_ext_gn_merch_date) */
//                 substr(gn.g1_user_data3,3)   product_code, 
//                 ap.a1_plan_type              plan_type,
//                 ap.a1_vendor_code            vendor,
//                 ap.a1_media_type             media_type,
//                 decode( ap.a1_plan_type,
//                         'VS', 40,
//                         'MC', 50,
//                         'DC', 31,
//                         'DS', 32,
//                         'AM', 30,
//                         'CB', 31,
//                         'JC', 33,
//                         'DB', 10,
//                         0 )                  transcom_card_type,                   
//                 ap.a1_number_fixed_auths     num_fixed_auths,
//                 ap.a1_interstate_number1     num_interstate1_auths,
//                 ap.a1_interstate_number2     num_interstate2_auths,
//                 ap.a1_local_number           num_local_auths,
//                 ap.A1_950_NUMBER             num_950_auths,
//                 ap.a1_switched_number        num_switched_auths,
//                 ap.a1_above_floor_number     num_above_floor_auths,
//                 ap.a1_below_floor_number     num_below_floor_auths,
//                 ap.a1_other_number           num_other_auths,
//                 ap.auth_income_total         total_auth_inc,
//                 (ap.auth_income_total/
//                  decode(ap.auth_count_total,0,1,
//                         ap.auth_count_total)) auth_inc_per_item
//          from   monthly_extract_gn gn,
//                 monthly_extract_ap ap
//          where  gn.HH_MERCHANT_NUMBER = :merchantId and
//                 gn.hh_active_date     = :activeDate and
//                 ap.HH_LOAD_SEC        = gn.HH_LOAD_SEC
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ INDEX (gn idx_mon_ext_gn_merch_date) */\n               substr(gn.g1_user_data3,3)   product_code, \n               ap.a1_plan_type              plan_type,\n               ap.a1_vendor_code            vendor,\n               ap.a1_media_type             media_type,\n               decode( ap.a1_plan_type,\n                       'VS', 40,\n                       'MC', 50,\n                       'DC', 31,\n                       'DS', 32,\n                       'AM', 30,\n                       'CB', 31,\n                       'JC', 33,\n                       'DB', 10,\n                       0 )                  transcom_card_type,                   \n               ap.a1_number_fixed_auths     num_fixed_auths,\n               ap.a1_interstate_number1     num_interstate1_auths,\n               ap.a1_interstate_number2     num_interstate2_auths,\n               ap.a1_local_number           num_local_auths,\n               ap.A1_950_NUMBER             num_950_auths,\n               ap.a1_switched_number        num_switched_auths,\n               ap.a1_above_floor_number     num_above_floor_auths,\n               ap.a1_below_floor_number     num_below_floor_auths,\n               ap.a1_other_number           num_other_auths,\n               ap.auth_income_total         total_auth_inc,\n               (ap.auth_income_total/\n                decode(ap.auth_count_total,0,1,\n                       ap.auth_count_total)) auth_inc_per_item\n        from   monthly_extract_gn gn,\n               monthly_extract_ap ap\n        where  gn.HH_MERCHANT_NUMBER =  :1  and\n               gn.hh_active_date     =  :2  and\n               ap.HH_LOAD_SEC        = gn.HH_LOAD_SEC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.startup.TranscomExtractSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:504^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        authIncPerItem = resultSet.getDouble("auth_inc_per_item");
        
        for( int field = 0; field < AuthFieldNames.length; ++field )
        {
          authCount       = resultSet.getInt(AuthFieldNames[field]);
          cardTypeString  = resultSet.getString("plan_type");
          authInc         = (authCount * authIncPerItem);
          
          // if there are no auths or the card type 
          // is invalid, then skip the row
          if ( (authCount == 0) || (resultSet.getInt("transcom_card_type") == 0) )
          {
            continue;
          }
          
          if ( authInc > 0.0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:526^13*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_extract_summary
//                  ( merchant_number,
//                    active_date,
//                    card_type,
//                    charge_type,
//                    disc_rate_revenue,
//                    disc_per_item_revenue,
//                    interchange_revenue,
//                    surcharge_revenue,
//                    surcharge_per_item_revenue,
//                    assessment_expense,
//                    interchange_expense,
//                    item_fee,
//                    item_count,
//                    equip_revenue,
//                    ticket_count,
//                    batch_count,
//                    auth_count,
//                    auth_per_item,
//                    misc_revenue,
//                    mtd_inc,
//                    description,
//                    load_filename
//                  )
//                values
//                  ( :merchantId,
//                    :activeDate,
//                    :resultSet.getInt("transcom_card_type"),
//                    :authIncTypeToTranscomChargeType( cardTypeString),
//                    0.0,    -- discount rate income
//                    round(:authInc,2),  -- discount per item income
//                    0.0,    -- interchange revenue
//                    0.0,    -- surcharge income
//                    0.0,    -- surcharge per item (not supported)
//                    0.0,    -- assessment expense
//                    0.0,    -- interchange expense
//                    0,      -- item fee (?)
//                    0,      -- item count
//                    0,      -- equip revenue
//                    0,      -- ticket count
//                    0,      -- batch count
//                    :authCount,         -- auth count
//                    0.0,                  -- auth per item expense
//                    0,                    -- misc revenue
//                    round(:authInc,2),  -- mtd total
//                    :mesConstants.getVitalExtractCardTypeDesc(cardTypeString) + " Authorization Income",  
//                    :loadFilename
//                  )    
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_4836 = resultSet.getInt("transcom_card_type");
 String __sJT_4837 = authIncTypeToTranscomChargeType( cardTypeString);
 String __sJT_4838 = mesConstants.getVitalExtractCardTypeDesc(cardTypeString) + " Authorization Income";
   String theSqlTS = "insert into transcom_extract_summary\n                ( merchant_number,\n                  active_date,\n                  card_type,\n                  charge_type,\n                  disc_rate_revenue,\n                  disc_per_item_revenue,\n                  interchange_revenue,\n                  surcharge_revenue,\n                  surcharge_per_item_revenue,\n                  assessment_expense,\n                  interchange_expense,\n                  item_fee,\n                  item_count,\n                  equip_revenue,\n                  ticket_count,\n                  batch_count,\n                  auth_count,\n                  auth_per_item,\n                  misc_revenue,\n                  mtd_inc,\n                  description,\n                  load_filename\n                )\n              values\n                (  :1 ,\n                   :2 ,\n                   :3 ,\n                   :4 ,\n                  0.0,    -- discount rate income\n                  round( :5 ,2),  -- discount per item income\n                  0.0,    -- interchange revenue\n                  0.0,    -- surcharge income\n                  0.0,    -- surcharge per item (not supported)\n                  0.0,    -- assessment expense\n                  0.0,    -- interchange expense\n                  0,      -- item fee (:6)\n                  0,      -- item count\n                  0,      -- equip revenue\n                  0,      -- ticket count\n                  0,      -- batch count\n                   :7 ,         -- auth count\n                  0.0,                  -- auth per item expense\n                  0,                    -- misc revenue\n                  round( :8 ,2),  -- mtd total\n                   :9 ,  \n                   :10 \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setInt(3,__sJT_4836);
   __sJT_st.setString(4,__sJT_4837);
   __sJT_st.setDouble(5,authInc);
   __sJT_st.setInt(6,authCount);
   __sJT_st.setDouble(7,authInc);
   __sJT_st.setString(8,__sJT_4838);
   __sJT_st.setString(9,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:576^13*/
          }
          
          authPerItem     = LiabilityContract.getContractItemRate( LiabilityContract.getAuthBetType(resultSet.getString("plan_type")), 
                                                                   resultSet.getString("product_code") );
          authExp         = (authPerItem * authCount);
          tcChargeType    = authTypeToTranscomChargeType( resultSet.getString("vendor"),
                                                          resultSet.getString("media_type"),
                                                          field );
          // if there are no auths, the card type or charge type
          // is invalid, then skip the record.
          if ( tcChargeType != 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:589^13*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_extract_summary
//                  ( merchant_number,
//                    active_date,
//                    card_type,
//                    charge_type,
//                    disc_rate_revenue,
//                    disc_per_item_revenue,
//                    interchange_revenue,
//                    surcharge_revenue,
//                    surcharge_per_item_revenue,
//                    assessment_expense,
//                    interchange_expense,
//                    item_fee,
//                    item_count,
//                    equip_revenue,
//                    ticket_count,
//                    batch_count,
//                    auth_count,
//                    auth_per_item,
//                    misc_revenue,
//                    mtd_inc,
//                    description,
//                    load_filename
//                  )
//                values
//                  ( :merchantId,
//                    :activeDate,
//                    :resultSet.getInt("transcom_card_type"),
//                    :tcChargeType,
//                    0.0,    -- discount rate income
//                    0.0,    -- discount per item income
//                    0.0,    -- interchange revenue
//                    0.0,    -- surcharge income
//                    0.0,    -- surcharge per item (not supported)
//                    0.0,    -- assessment expense
//                    0.0,    -- interchange expense
//                    0,      -- item fee (?)
//                    0,      -- item count
//                    0,      -- equip revenue
//                    0,      -- ticket count
//                    0,      -- batch count
//                    :authCount,         -- auth count
//                    round(:authExp,2),  -- auth per item expense
//                    0,                    -- misc revenue
//                    round(:-authExp,2), -- mtd total
//                    :mesConstants.getVitalExtractCardTypeDesc(cardTypeString) + " Authorizations",  
//                    :loadFilename
//                  )    
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_4839 = resultSet.getInt("transcom_card_type");
 double __sJT_4840 = -authExp;
 String __sJT_4841 = mesConstants.getVitalExtractCardTypeDesc(cardTypeString) + " Authorizations";
   String theSqlTS = "insert into transcom_extract_summary\n                ( merchant_number,\n                  active_date,\n                  card_type,\n                  charge_type,\n                  disc_rate_revenue,\n                  disc_per_item_revenue,\n                  interchange_revenue,\n                  surcharge_revenue,\n                  surcharge_per_item_revenue,\n                  assessment_expense,\n                  interchange_expense,\n                  item_fee,\n                  item_count,\n                  equip_revenue,\n                  ticket_count,\n                  batch_count,\n                  auth_count,\n                  auth_per_item,\n                  misc_revenue,\n                  mtd_inc,\n                  description,\n                  load_filename\n                )\n              values\n                (  :1 ,\n                   :2 ,\n                   :3 ,\n                   :4 ,\n                  0.0,    -- discount rate income\n                  0.0,    -- discount per item income\n                  0.0,    -- interchange revenue\n                  0.0,    -- surcharge income\n                  0.0,    -- surcharge per item (not supported)\n                  0.0,    -- assessment expense\n                  0.0,    -- interchange expense\n                  0,      -- item fee (:5)\n                  0,      -- item count\n                  0,      -- equip revenue\n                  0,      -- ticket count\n                  0,      -- batch count\n                   :6 ,         -- auth count\n                  round( :7 ,2),  -- auth per item expense\n                  0,                    -- misc revenue\n                  round( :8 ,2), -- mtd total\n                   :9 ,  \n                   :10 \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setInt(3,__sJT_4839);
   __sJT_st.setInt(4,tcChargeType);
   __sJT_st.setInt(5,authCount);
   __sJT_st.setDouble(6,authExp);
   __sJT_st.setDouble(7,__sJT_4840);
   __sJT_st.setString(8,__sJT_4841);
   __sJT_st.setString(9,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:639^13*/
          }
        }
      }
    }
    catch( Exception e )
    {
      logEntry( "loadAuthData("+merchantId+","+activeDate+","+loadFilename+")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  private void loadBatchHeaderData( long merchantId, Date activeDate, String loadFilename )
  {
    int       batchCount  = 0;
    Date      beginDate   = getActivityBeginDate( merchantId, activeDate );
    Date      endDate     = getActivityEndDate( merchantId, activeDate );
  
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:662^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(ts.batch_count),0) 
//          from    daily_detail_file_summary ts
//          where   ts.merchant_number = :merchantId and
//                  ts.batch_date between :beginDate and :endDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(ts.batch_count),0)  \n        from    daily_detail_file_summary ts\n        where   ts.merchant_number =  :1  and\n                ts.batch_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.TranscomExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   batchCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:668^7*/
      
      if ( batchCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:672^9*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_extract_summary
//              ( merchant_number,
//                active_date,
//                card_type,
//                charge_type,
//                disc_rate_revenue,
//                disc_per_item_revenue,
//                interchange_revenue,
//                surcharge_revenue,
//                surcharge_per_item_revenue,
//                assessment_expense,
//                interchange_expense,
//                item_fee,
//                item_count,
//                equip_revenue,
//                ticket_count,
//                batch_count,
//                auth_count,
//                auth_per_item,
//                misc_revenue,
//                mtd_inc,
//                description,
//                load_filename
//              )
//            values
//              ( :merchantId,
//                :activeDate,
//                null,
//                '6106',
//                0.0,    -- discount rate income
//                0.0,    -- discount per item income
//                0.0,    -- interchange revenue
//                0.0,    -- surcharge income
//                0.0,    -- surcharge per item (not supported)
//                0.0,    -- assessment expense
//                0.0,    -- interchange expense
//                0,      -- item fee (?)
//                0,      -- item count
//                0,      -- equip revenue
//                0,      -- ticket count
//                :batchCount,  -- batch count
//                0,            -- auth count
//                0,            -- auth per item expense
//                0,            -- misc revenue
//                0,            -- mtd total
//                'Batch Headers',
//                :loadFilename
//              )    
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into transcom_extract_summary\n            ( merchant_number,\n              active_date,\n              card_type,\n              charge_type,\n              disc_rate_revenue,\n              disc_per_item_revenue,\n              interchange_revenue,\n              surcharge_revenue,\n              surcharge_per_item_revenue,\n              assessment_expense,\n              interchange_expense,\n              item_fee,\n              item_count,\n              equip_revenue,\n              ticket_count,\n              batch_count,\n              auth_count,\n              auth_per_item,\n              misc_revenue,\n              mtd_inc,\n              description,\n              load_filename\n            )\n          values\n            (  :1 ,\n               :2 ,\n              null,\n              '6106',\n              0.0,    -- discount rate income\n              0.0,    -- discount per item income\n              0.0,    -- interchange revenue\n              0.0,    -- surcharge income\n              0.0,    -- surcharge per item (not supported)\n              0.0,    -- assessment expense\n              0.0,    -- interchange expense\n              0,      -- item fee (:3)\n              0,      -- item count\n              0,      -- equip revenue\n              0,      -- ticket count\n               :4 ,  -- batch count\n              0,            -- auth count\n              0,            -- auth per item expense\n              0,            -- misc revenue\n              0,            -- mtd total\n              'Batch Headers',\n               :5 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setInt(3,batchCount);
   __sJT_st.setString(4,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:722^9*/
      }        
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadBatchHeaderData("+merchantId+","+activeDate+","+loadFilename+")", e.toString() );
    }
    finally
    {
    }
  }
  
  private void loadDebitEBTData( long merchantId, Date activeDate, String loadFilename )
  {
    String                cardType      = null;
    String                chargeType    = null;
    double                creditsAmount = 0.0;
    int                   creditsCount  = 0;
    String                desc          = null;
    double                expense       = 0.0;
    ResultSetIterator     it            = null;
    double                perItem       = 0.0;
    ResultSet             resultSet     = null;
    double                salesAmount   = 0.0;
    int                   salesCount    = 0;
    double                totalInc      = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:751^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  gn.T1_TOT_INC_DEBIT_NETWORKS      as tot_debit_inc,
//                  pl.pl_plan_type                   as plan_type,
//                  pl.PL_NUMBER_OF_CREDITS           as credits_count,
//                  pl.pl_credits_amount              as credits_amount,
//                  pl.PL_NUMBER_OF_SALES             as sales_count,
//                  pl.pl_sales_amount                as sales_amount,
//                  pl.pl_correct_disc_amt            as drt_inc
//          from    monthly_extract_gn gn,
//                  monthly_extract_pl pl
//          where   gn.hh_merchant_number = :merchantId and
//                  gn.hh_active_date = :activeDate and
//                  pl.hh_load_sec = gn.hh_load_sec and
//                  pl.pl_plan_type in ( 'DEBC', 'EBT' )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  gn.T1_TOT_INC_DEBIT_NETWORKS      as tot_debit_inc,\n                pl.pl_plan_type                   as plan_type,\n                pl.PL_NUMBER_OF_CREDITS           as credits_count,\n                pl.pl_credits_amount              as credits_amount,\n                pl.PL_NUMBER_OF_SALES             as sales_count,\n                pl.pl_sales_amount                as sales_amount,\n                pl.pl_correct_disc_amt            as drt_inc\n        from    monthly_extract_gn gn,\n                monthly_extract_pl pl\n        where   gn.hh_merchant_number =  :1  and\n                gn.hh_active_date =  :2  and\n                pl.hh_load_sec = gn.hh_load_sec and\n                pl.pl_plan_type in ( 'DEBC', 'EBT' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.startup.TranscomExtractSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:766^7*/
      resultSet = it.getResultSet();
      
      while(resultSet.next())
      {
        salesCount    = resultSet.getInt("sales_count");
        salesAmount   = resultSet.getDouble("sales_amount");
        creditsCount  = resultSet.getInt("credits_count");
        creditsAmount = resultSet.getDouble("credits_amount");
      
        if ( resultSet.getString("plan_type").equals("DEBC") )
        {
          // first get the debit sponsor fee (if any)
          // this fee is generated by the debit network BET but
          // appears under the individual plan total in the extract
          totalInc = 0.0;
          /*@lineinfo:generated-code*//*@lineinfo:782^11*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(st.st_fee_amount),0) 
//              from    monthly_extract_gn gn,
//                      monthly_extract_st st
//              where   gn.hh_merchant_number = :merchantId and
//                      gn.hh_active_date = :activeDate and
//                      st.hh_load_sec = gn.hh_load_sec and
//                      st.st_statement_desc like 'PIN DEBIT NETWORK SPONSOR FEE%'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(st.st_fee_amount),0)  \n            from    monthly_extract_gn gn,\n                    monthly_extract_st st\n            where   gn.hh_merchant_number =  :1  and\n                    gn.hh_active_date =  :2  and\n                    st.hh_load_sec = gn.hh_load_sec and\n                    st.st_statement_desc like 'PIN DEBIT NETWORK SPONSOR FEE%'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.TranscomExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   totalInc = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:791^11*/
          
          // add in the MES contract expense
          perItem       = LiabilityContract.getContractItemRate( BankContractBean.BET_DEBIT_TRANSACTION_FEE );
          expense       = ((salesCount + creditsCount) * perItem);
          cardType      = "10";
          chargeType    = "1263";
          totalInc     += resultSet.getDouble("tot_debit_inc");
          desc          = "Debit Network Income/Expense";
        }
        else    // ebt
        {
          perItem       = LiabilityContract.getContractItemRate( BankContractBean.BET_EBT_TRANSACTION_FEE );
          expense       = ((salesCount + creditsCount) * perItem);
          cardType      = "08";
          chargeType    = "1203";
          totalInc      = resultSet.getDouble("drt_inc");   // income from DRT
          desc          = "EBT Income/Expense";
        }
      
        if ( (salesCount + creditsCount) > 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:813^11*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_extract_summary
//                ( merchant_number,
//                  active_date,
//                  card_type,
//                  charge_type,
//                  sales_vol_count,
//                  sales_vol_amount,
//                  credits_vol_count,
//                  credits_vol_amount,
//                  disc_rate_revenue,
//                  disc_per_item_revenue,
//                  interchange_revenue,
//                  surcharge_revenue,
//                  surcharge_per_item_revenue,
//                  assessment_expense,
//                  interchange_expense,
//                  item_fee,
//                  item_count,
//                  equip_revenue,
//                  ticket_count,
//                  batch_count,
//                  auth_count,
//                  auth_per_item,
//                  misc_revenue,
//                  mtd_inc,
//                  description,
//                  load_filename
//                )
//              values
//                ( :merchantId,
//                  :activeDate,
//                  :cardType,
//                  :chargeType,
//                  :salesCount,
//                  :salesAmount,
//                  :creditsCount,
//                  :creditsAmount,
//                  0.0,    -- discount rate income
//                  :totalInc,    -- discount per item income
//                  0.0,    -- interchange revenue
//                  0.0,    -- surcharge income
//                  0.0,    -- surcharge per item (not supported)
//                  0.0,    -- assessment expense
//                  0.0,    -- interchange expense
//                  round(:expense,2), -- item fee 
//                  0,      -- item count (quantity)
//                  0,      -- equip revenue
//                  0,      -- ticket count
//                  0,      -- batch count
//                  0,      -- auth count
//                  0.0,    -- auth per item expense
//                  0.0,    -- misc revenue
//                  round(:totalInc-expense,2), -- mtd total
//                  :desc,
//                  :loadFilename
//                )    
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4842 = totalInc-expense;
   String theSqlTS = "insert into transcom_extract_summary\n              ( merchant_number,\n                active_date,\n                card_type,\n                charge_type,\n                sales_vol_count,\n                sales_vol_amount,\n                credits_vol_count,\n                credits_vol_amount,\n                disc_rate_revenue,\n                disc_per_item_revenue,\n                interchange_revenue,\n                surcharge_revenue,\n                surcharge_per_item_revenue,\n                assessment_expense,\n                interchange_expense,\n                item_fee,\n                item_count,\n                equip_revenue,\n                ticket_count,\n                batch_count,\n                auth_count,\n                auth_per_item,\n                misc_revenue,\n                mtd_inc,\n                description,\n                load_filename\n              )\n            values\n              (  :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                 :5 ,\n                 :6 ,\n                 :7 ,\n                 :8 ,\n                0.0,    -- discount rate income\n                 :9 ,    -- discount per item income\n                0.0,    -- interchange revenue\n                0.0,    -- surcharge income\n                0.0,    -- surcharge per item (not supported)\n                0.0,    -- assessment expense\n                0.0,    -- interchange expense\n                round( :10 ,2), -- item fee \n                0,      -- item count (quantity)\n                0,      -- equip revenue\n                0,      -- ticket count\n                0,      -- batch count\n                0,      -- auth count\n                0.0,    -- auth per item expense\n                0.0,    -- misc revenue\n                round( :11 ,2), -- mtd total\n                 :12 ,\n                 :13 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setString(3,cardType);
   __sJT_st.setString(4,chargeType);
   __sJT_st.setInt(5,salesCount);
   __sJT_st.setDouble(6,salesAmount);
   __sJT_st.setInt(7,creditsCount);
   __sJT_st.setDouble(8,creditsAmount);
   __sJT_st.setDouble(9,totalInc);
   __sJT_st.setDouble(10,expense);
   __sJT_st.setDouble(11,__sJT_4842);
   __sJT_st.setString(12,desc);
   __sJT_st.setString(13,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:871^11*/
        }        
      }       // end while( resultSet.next() )
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry( "loadDebitEBTData("+merchantId+","+activeDate+","+loadFilename+")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  private void loadChargebackVolumeData( long merchantId, Date activeDate, double[] volAmount, int[] volCount )
  {
    Date                      beginDate         = null;
    Date                      currDate          = null;
    int                       index             = 0;
    ResultSetIterator         it                = null;
    ResultSet                 resultSet         = null;
    
    try
    {
      // reset the totals
      for( int i = 0; i < volCount.length; ++i )
      {
        volAmount[i]  = 0.0;
        volCount[i]   = 0;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:904^7*/

//  ************************************************************
//  #sql [Ctx] { select decode(gnp.hh_curr_date,
//                        null, gn.hh_active_date,
//                       (gnp.hh_curr_date+1) ),
//                 gn.hh_curr_date      
//          from   monthly_extract_gn      gn,
//                 monthly_extract_gn      gnp
//          where  gn.hh_merchant_number = :merchantId and
//                 gn.hh_active_date     = :activeDate and
//                 gnp.hh_merchant_number(+) = gn.hh_merchant_number and
//                 gnp.hh_active_date(+) = to_date(to_char((:activeDate-15),'mm/yyyy'),'mm/yyyy')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select decode(gnp.hh_curr_date,\n                      null, gn.hh_active_date,\n                     (gnp.hh_curr_date+1) ),\n               gn.hh_curr_date       \n        from   monthly_extract_gn      gn,\n               monthly_extract_gn      gnp\n        where  gn.hh_merchant_number =  :1  and\n               gn.hh_active_date     =  :2  and\n               gnp.hh_merchant_number(+) = gn.hh_merchant_number and\n               gnp.hh_active_date(+) = to_date(to_char(( :3 -15),'mm/yyyy'),'mm/yyyy')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.TranscomExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginDate = (java.sql.Date)__sJT_rs.getDate(1);
   currDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:916^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:918^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode( cb.TRAN_AMOUNT, abs(cb.tran_amount), 'D','C')     as debit_credit_indicator,
//                  count(cb.tran_amount)                                     as cb_count,
//                  sum(abs(cb.tran_amount))                                  as cb_amount
//          from    network_chargebacks cb
//          where   cb.merchant_number = :merchantId and
//                  cb.INCOMING_DATE between :beginDate and :currDate
//          group by decode( cb.TRAN_AMOUNT, abs(cb.tran_amount), 'D','C')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode( cb.TRAN_AMOUNT, abs(cb.tran_amount), 'D','C')     as debit_credit_indicator,\n                count(cb.tran_amount)                                     as cb_count,\n                sum(abs(cb.tran_amount))                                  as cb_amount\n        from    network_chargebacks cb\n        where   cb.merchant_number =  :1  and\n                cb.INCOMING_DATE between  :2  and  :3 \n        group by decode( cb.TRAN_AMOUNT, abs(cb.tran_amount), 'D','C')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.startup.TranscomExtractSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:927^7*/
      resultSet = it.getResultSet();
     
      while( resultSet.next() )
      {
        index = ((resultSet.getString("debit_credit_indicator").equals("C")) ? VT_CREDITS : VT_SALES);
        volCount[index]   += resultSet.getInt("cb_count");
        volAmount[index]  += resultSet.getDouble("cb_amount");
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadChargebackVolumeData("+merchantId+","+activeDate+")", e.toString() );
    }
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
  }
  
  private void loadChargeRecordData( long merchantId, Date activeDate, String loadFilename )
  {
    String                cgCode            = null;
    String                cgDesc            = null;
    int                   itemCount         = 0;
    String                itemDesc          = null;
    double                itemInc           = 0.0;
    ResultSetIterator     it                = null;
    boolean               match             = false;
    ResultSet             resultSet         = null;
    String                tcChargeType      = null;
    String                token             = null;
    StringTokenizer       tokenizer         = null;
    
    try
    {
      // start with equipment sales
      /*@lineinfo:generated-code*//*@lineinfo:964^7*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct gn.hh_active_date                 as active_date,
//                 cg.cg_charge_record_type                   as cg_type,
//                 decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, 
//                         0, 1,
//                         st.ST_NUMBER_OF_ITEMS_ON_STMT )    as item_count,
//                 decode( st.ST_AMOUNT_OF_ITEM_ON_STMT,
//                         0, ( decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, 
//                                      0, st.ST_FEE_AMOUNT,
//                                      st.ST_FEE_AMOUNT / st.ST_NUMBER_OF_ITEMS_ON_STMT ) ),
//                         st.ST_AMOUNT_OF_ITEM_ON_STMT )     as per_item,                       
//                 st.ST_STATEMENT_DESC                       as item_desc,
//                 st.ST_FEE_AMOUNT                           as total_amount            
//          from   monthly_extract_gn gn,
//                 monthly_extract_st st,
//                 monthly_extract_cg cg
//          where    gn.hh_merchant_number  = :merchantId and
//                   gn.hh_active_date      = :activeDate and
//                   st.hh_load_sec         = gn.hh_load_sec and
//                   st.st_fee_amount       > 0 and
//                   cg.hh_load_sec         = gn.hh_load_sec and
//                   cg.CG_MESSAGE_FOR_STMT = st.ST_STATEMENT_DESC
//          order by st.ST_STATEMENT_DESC
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct gn.hh_active_date                 as active_date,\n               cg.cg_charge_record_type                   as cg_type,\n               decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, \n                       0, 1,\n                       st.ST_NUMBER_OF_ITEMS_ON_STMT )    as item_count,\n               decode( st.ST_AMOUNT_OF_ITEM_ON_STMT,\n                       0, ( decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, \n                                    0, st.ST_FEE_AMOUNT,\n                                    st.ST_FEE_AMOUNT / st.ST_NUMBER_OF_ITEMS_ON_STMT ) ),\n                       st.ST_AMOUNT_OF_ITEM_ON_STMT )     as per_item,                       \n               st.ST_STATEMENT_DESC                       as item_desc,\n               st.ST_FEE_AMOUNT                           as total_amount            \n        from   monthly_extract_gn gn,\n               monthly_extract_st st,\n               monthly_extract_cg cg\n        where    gn.hh_merchant_number  =  :1  and\n                 gn.hh_active_date      =  :2  and\n                 st.hh_load_sec         = gn.hh_load_sec and\n                 st.st_fee_amount       > 0 and\n                 cg.hh_load_sec         = gn.hh_load_sec and\n                 cg.CG_MESSAGE_FOR_STMT = st.ST_STATEMENT_DESC\n        order by st.ST_STATEMENT_DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.startup.TranscomExtractSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:988^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        itemDesc  = resultSet.getString("item_desc");
        match     = false;
        
        for( int i = 0; i < ChargeRecDescToTranscomChargeType.length; ++i )
        {
          cgCode        = ChargeRecDescToTranscomChargeType[i][CG_CHARGE_CODE];
          cgDesc        = ChargeRecDescToTranscomChargeType[i][CG_CHARGE_DESC];
          tcChargeType  = ChargeRecDescToTranscomChargeType[i][CG_TRANSCOM_TYPE];
          
          if ( cgCode == null || cgCode.equals(resultSet.getString("cg_type")) )
          {
            // if the description in the lookup table is null
            // then all we are comparing is the charge record
            // type.  consider this a match and exit for loop
            if ( cgDesc == null )
            {
              match = true;
            }
            else if ( cgDesc.indexOf('%') != -1 )
            {
              // setup the tokenizer
              tokenizer = new StringTokenizer(cgDesc,"%");
              match     = true;
              
              while( tokenizer.hasMoreTokens() )
              {
                token = tokenizer.nextToken();
                if ( itemDesc.indexOf(token) == -1 )    // does not match
                {
                  match = false;
                  break;    // exit the while loop
                }
              }
            }
            else if ( itemDesc.indexOf(cgDesc) != -1 )
            {
              match = true;
            }
            if ( match == true )
            {
              break;      // exit the for loop
            }
          }
        }
        
        // put in as misc if this code did
        // not resolve correctly.  should never
        // happen because the last entry in the
        // decode table will fill this value 
        // automatically, but just in case...
        if ( match == false )
        {
          tcChargeType  = Integer.toString(TCT_DEFAULT_CHARGE_TYPE);
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:1048^9*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_extract_summary
//              ( merchant_number,
//                active_date,
//                charge_type,
//                disc_rate_revenue,
//                disc_per_item_revenue,
//                interchange_revenue,
//                surcharge_revenue,
//                surcharge_per_item_revenue,
//                assessment_expense,
//                interchange_expense,
//                item_fee,
//                item_count,
//                equip_revenue,
//                ticket_count,
//                batch_count,
//                auth_count,
//                auth_per_item,
//                misc_revenue,
//                mtd_inc,
//                description,
//                load_filename
//              )
//            values
//              ( :merchantId,
//                :activeDate,
//                :tcChargeType,
//                0.0,    -- discount rate income
//                0.0,    -- discount per item income
//                0.0,    -- interchange revenue
//                0.0,    -- surcharge income
//                0.0,    -- surcharge per item (not supported)
//                0.0,    -- assessment expense
//                0.0,    -- interchange expense
//                0,      -- item fee (?)
//                :resultSet.getInt("item_count"), -- item count (quantity)
//                0,      -- equip revenue
//                0,      -- ticket count
//                0,      -- batch count
//                0,                    -- auth count
//                0.0,                  -- auth per item expense
//                :resultSet.getDouble("total_amount"), -- misc revenue
//                :resultSet.getDouble("total_amount"), -- mtd total
//                :itemDesc,
//                :loadFilename
//              )    
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_4843 = resultSet.getInt("item_count");
 double __sJT_4844 = resultSet.getDouble("total_amount");
 double __sJT_4845 = resultSet.getDouble("total_amount");
   String theSqlTS = "insert into transcom_extract_summary\n            ( merchant_number,\n              active_date,\n              charge_type,\n              disc_rate_revenue,\n              disc_per_item_revenue,\n              interchange_revenue,\n              surcharge_revenue,\n              surcharge_per_item_revenue,\n              assessment_expense,\n              interchange_expense,\n              item_fee,\n              item_count,\n              equip_revenue,\n              ticket_count,\n              batch_count,\n              auth_count,\n              auth_per_item,\n              misc_revenue,\n              mtd_inc,\n              description,\n              load_filename\n            )\n          values\n            (  :1 ,\n               :2 ,\n               :3 ,\n              0.0,    -- discount rate income\n              0.0,    -- discount per item income\n              0.0,    -- interchange revenue\n              0.0,    -- surcharge income\n              0.0,    -- surcharge per item (not supported)\n              0.0,    -- assessment expense\n              0.0,    -- interchange expense\n              0,      -- item fee (:4)\n               :5 , -- item count (quantity)\n              0,      -- equip revenue\n              0,      -- ticket count\n              0,      -- batch count\n              0,                    -- auth count\n              0.0,                  -- auth per item expense\n               :6 , -- misc revenue\n               :7 , -- mtd total\n               :8 ,\n               :9 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"17com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setString(3,tcChargeType);
   __sJT_st.setInt(4,__sJT_4843);
   __sJT_st.setDouble(5,__sJT_4844);
   __sJT_st.setDouble(6,__sJT_4845);
   __sJT_st.setString(7,itemDesc);
   __sJT_st.setString(8,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1096^9*/
      }
    }
    catch( Exception e )
    {
      logEntry( "loadChargeRecordData("+merchantId+","+activeDate+","+loadFilename+")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  public void loadIcData( long merchantId, Date activeDate, String loadFilename )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1113^7*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_extract_summary
//          ( 
//            merchant_number,
//            active_date,
//            card_type,
//            charge_type,
//            sales_vol_count,
//            sales_vol_amount,
//            credits_vol_count,
//            credits_vol_amount,
//            disc_rate_revenue,
//            disc_per_item_revenue,
//            interchange_revenue,
//            surcharge_revenue,
//            surcharge_per_item_revenue,
//            assessment_expense,
//            interchange_expense,
//            item_fee,
//            item_count,
//            equip_revenue,
//            ticket_count,
//            batch_count,
//            auth_count,
//            auth_per_item,
//            misc_revenue,
//            mtd_inc,
//            description,
//            load_filename
//          )
//          select /*+ 
//                     ordered 
//                     use_nl(mf mr)
//                     use_nl(mr app)
//                     use_nl(app sr)                    
//                     use_nl(mf sm)
//                 */
//                  sm.merchant_number,
//                  sm.active_date,
//                  '40',     -- card type
//                  '1300',   -- charge type
//                  sm.vmc_sales_count,
//                  sm.vmc_sales_amount,
//                  sm.vmc_credits_count,
//                  sm.vmc_credits_amount,
//                  sm.tot_inc_discount,    -- disc rate income
//                  0,      -- disc per item income
//                  0.0,    -- interchange revenue
//                  (sm.tot_inc_interchange + nvl(st.vmc_fees_inc,0)),
//                  0.0,    -- surchage per item income
//                  0.0,    -- assessments
//                  (sm.interchange_expense + sm.vmc_assessment_expense), -- include assessments
//                  0,      -- item fee (?)
//                  0,      -- item count
//                  0,      -- equip revenue
//                  0,      -- ticket count
//                  0,      -- batch count
//                  0,      -- auth count
//                  0,      -- auth per item
//                  0,      -- misc revenue
//                  ((sm.tot_inc_discount + sm.tot_inc_interchange + nvl(st.vmc_fees_inc,0)) 
//                    - (sm.interchange_expense + sm.vmc_assessment_expense)),  -- mtd total
//                  'V/MC Interchange',
//                  sm.load_filename
//          from    monthly_extract_summary   sm,
//                  (
//                    select  st.hh_load_sec          as hh_load_sec,
//                            sum(st.st_fee_amount)   as vmc_fees_inc
//                    from    monthly_extract_st    st
//                    where   st.hh_load_sec in 
//                            (
//                              select  gn.hh_load_sec 
//                              from    monthly_extract_gn gn 
//                              where   gn.hh_merchant_number = :merchantId 
//                                      and gn.hh_active_date = :activeDate
//                            )
//                            and st.item_category = 'ASSOC'
//                    group by st.hh_load_sec                  
//                  )                         st
//          where   sm.merchant_number = :merchantId
//                  and sm.active_date = :activeDate
//                  and st.hh_load_sec(+) = sm.hh_load_sec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into transcom_extract_summary\n        ( \n          merchant_number,\n          active_date,\n          card_type,\n          charge_type,\n          sales_vol_count,\n          sales_vol_amount,\n          credits_vol_count,\n          credits_vol_amount,\n          disc_rate_revenue,\n          disc_per_item_revenue,\n          interchange_revenue,\n          surcharge_revenue,\n          surcharge_per_item_revenue,\n          assessment_expense,\n          interchange_expense,\n          item_fee,\n          item_count,\n          equip_revenue,\n          ticket_count,\n          batch_count,\n          auth_count,\n          auth_per_item,\n          misc_revenue,\n          mtd_inc,\n          description,\n          load_filename\n        )\n        select /*+ \n                   ordered \n                   use_nl(mf mr)\n                   use_nl(mr app)\n                   use_nl(app sr)                    \n                   use_nl(mf sm)\n               */\n                sm.merchant_number,\n                sm.active_date,\n                '40',     -- card type\n                '1300',   -- charge type\n                sm.vmc_sales_count,\n                sm.vmc_sales_amount,\n                sm.vmc_credits_count,\n                sm.vmc_credits_amount,\n                sm.tot_inc_discount,    -- disc rate income\n                0,      -- disc per item income\n                0.0,    -- interchange revenue\n                (sm.tot_inc_interchange + nvl(st.vmc_fees_inc,0)),\n                0.0,    -- surchage per item income\n                0.0,    -- assessments\n                (sm.interchange_expense + sm.vmc_assessment_expense), -- include assessments\n                0,      -- item fee (:1)\n                0,      -- item count\n                0,      -- equip revenue\n                0,      -- ticket count\n                0,      -- batch count\n                0,      -- auth count\n                0,      -- auth per item\n                0,      -- misc revenue\n                ((sm.tot_inc_discount + sm.tot_inc_interchange + nvl(st.vmc_fees_inc,0)) \n                  - (sm.interchange_expense + sm.vmc_assessment_expense)),  -- mtd total\n                'V/MC Interchange',\n                sm.load_filename\n        from    monthly_extract_summary   sm,\n                (\n                  select  st.hh_load_sec          as hh_load_sec,\n                          sum(st.st_fee_amount)   as vmc_fees_inc\n                  from    monthly_extract_st    st\n                  where   st.hh_load_sec in \n                          (\n                            select  gn.hh_load_sec \n                            from    monthly_extract_gn gn \n                            where   gn.hh_merchant_number =  :2  \n                                    and gn.hh_active_date =  :3 \n                          )\n                          and st.item_category = 'ASSOC'\n                  group by st.hh_load_sec                  \n                )                         st\n        where   sm.merchant_number =  :4 \n                and sm.active_date =  :5 \n                and st.hh_load_sec(+) = sm.hh_load_sec";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setDate(4,activeDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1196^7*/
    }
    catch( Exception e )
    {
      logEntry( "loadIcData("+merchantId+","+activeDate+","+loadFilename+")", e.toString() );
    }
    finally
    {
    }
  }
  
  public void loadPlanData( long merchantId, Date activeDate, String loadFilename )
  {
    double                creditsAmount     = 0.0;
    int                   creditsCount      = 0;
    double                expPerItem        = 0.0;
    ResultSetIterator     it                = null;
    ResultSet             resultSet         = null;
    double                salesAmount       = 0.0;
    int                   salesCount        = 0;
    double                totalExp          = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1220^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.product_code               as product_code,
//                  sm.vmc_sales_count            as vmc_sales_count,
//                  sm.vmc_sales_amount           as vmc_sales_amount,
//                  sm.vmc_credits_count          as vmc_credits_count,
//                  sm.vmc_credits_amount         as vmc_credits_amount,
//                  sm.amex_sales_count           as amex_sales_count,
//                  sm.amex_sales_amount          as amex_sales_amount,
//                  sm.amex_credits_count         as amex_credits_count,
//                  sm.amex_credits_amount        as amex_credits_amount,
//                  sm.disc_sales_count           as disc_sales_count,
//                  sm.disc_sales_amount          as disc_sales_amount,
//                  sm.disc_credits_count         as disc_credits_count,
//                  sm.disc_credits_amount        as disc_credits_amount,
//                  sm.dinr_sales_count           as dinr_sales_count,
//                  sm.dinr_sales_amount          as dinr_sales_amount,
//                  sm.dinr_credits_count         as dinr_credits_count,
//                  sm.dinr_credits_amount        as dinr_credits_amount,
//                  sm.jcb_sales_count            as jcb_sales_count,
//                  sm.jcb_sales_amount           as jcb_sales_amount,
//                  sm.jcb_credits_count          as jcb_credits_count,
//                  sm.jcb_credits_amount         as jcb_credits_amount
//          from    monthly_extract_summary sm
//          where   sm.merchant_number  = :merchantId and
//                  sm.active_date      = :activeDate                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.product_code               as product_code,\n                sm.vmc_sales_count            as vmc_sales_count,\n                sm.vmc_sales_amount           as vmc_sales_amount,\n                sm.vmc_credits_count          as vmc_credits_count,\n                sm.vmc_credits_amount         as vmc_credits_amount,\n                sm.amex_sales_count           as amex_sales_count,\n                sm.amex_sales_amount          as amex_sales_amount,\n                sm.amex_credits_count         as amex_credits_count,\n                sm.amex_credits_amount        as amex_credits_amount,\n                sm.disc_sales_count           as disc_sales_count,\n                sm.disc_sales_amount          as disc_sales_amount,\n                sm.disc_credits_count         as disc_credits_count,\n                sm.disc_credits_amount        as disc_credits_amount,\n                sm.dinr_sales_count           as dinr_sales_count,\n                sm.dinr_sales_amount          as dinr_sales_amount,\n                sm.dinr_credits_count         as dinr_credits_count,\n                sm.dinr_credits_amount        as dinr_credits_amount,\n                sm.jcb_sales_count            as jcb_sales_count,\n                sm.jcb_sales_amount           as jcb_sales_amount,\n                sm.jcb_credits_count          as jcb_credits_count,\n                sm.jcb_credits_amount         as jcb_credits_amount\n        from    monthly_extract_summary sm\n        where   sm.merchant_number  =  :1  and\n                sm.active_date      =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"19com.mes.startup.TranscomExtractSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1246^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        for( int i = 0; i < PlanSummaryFields.length; ++i )
        {
          salesCount    = resultSet.getInt( PlanSummaryFields[i][PL_FIELD_PREFIX] + "_sales_count" );
          salesAmount   = resultSet.getInt( PlanSummaryFields[i][PL_FIELD_PREFIX] + "_sales_amount" );
          creditsCount  = resultSet.getInt( PlanSummaryFields[i][PL_FIELD_PREFIX] + "_credits_count" );
          creditsAmount = resultSet.getInt( PlanSummaryFields[i][PL_FIELD_PREFIX] + "_credits_amount" );
          
          if ( (salesCount+creditsCount) > 0 )
          {
            expPerItem = LiabilityContract.getContractItemRate( LiabilityContract.getPlanBetType(PlanSummaryFields[i][PL_PLAN_TYPE]), 
                                                                resultSet.getString("product_code") );
            totalExp   = (expPerItem * (salesCount + creditsCount));
            
            /*@lineinfo:generated-code*//*@lineinfo:1264^13*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_extract_summary
//                  ( merchant_number,
//                    active_date,
//                    card_type,
//                    charge_type,
//                    sales_vol_count,
//                    sales_vol_amount,
//                    credits_vol_count,
//                    credits_vol_amount,
//                    disc_rate_revenue,
//                    disc_per_item_revenue,
//                    interchange_revenue,
//                    surcharge_revenue,
//                    surcharge_per_item_revenue,
//                    assessment_expense,
//                    interchange_expense,
//                    item_fee,
//                    item_count,
//                    equip_revenue,
//                    ticket_count,
//                    batch_count,
//                    auth_count,
//                    auth_per_item,
//                    misc_revenue,
//                    mtd_inc,
//                    description,
//                    load_filename
//                  )
//                values
//                  ( :merchantId,
//                    :activeDate,
//                    :PlanSummaryFields[i][PL_TPS_CARD_TYPE],
//                    :PlanSummaryFields[i][PL_TPS_CHG_TYPE],
//                    :salesCount,
//                    :salesAmount,
//                    :creditsCount,
//                    :creditsAmount,
//                    0.0,    -- discount rate income
//                    0.0,    -- discount per item income
//                    0.0,    -- interchange revenue
//                    0.0,    -- surcharge income
//                    0.0,    -- surcharge per item (not supported)
//                    0.0,    -- assessment expense
//                    0.0,    -- interchange expense
//                    :totalExp,       -- item fee
//                    0,      -- quantity
//                    0,      -- equip revenue
//                    0,      -- ticket count
//                    0,      -- batch count
//                    0,      -- auth count
//                    0,      -- auth fee
//                    0,                    -- misc revenue
//                    round(:-totalExp,2), -- mtd total
//                    :PlanSummaryFields[i][PL_TPS_REFERENCE],
//                    :loadFilename
//                  )    
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4846 = PlanSummaryFields[i][PL_TPS_CARD_TYPE];
 String __sJT_4847 = PlanSummaryFields[i][PL_TPS_CHG_TYPE];
 double __sJT_4848 = -totalExp;
 String __sJT_4849 = PlanSummaryFields[i][PL_TPS_REFERENCE];
   String theSqlTS = "insert into transcom_extract_summary\n                ( merchant_number,\n                  active_date,\n                  card_type,\n                  charge_type,\n                  sales_vol_count,\n                  sales_vol_amount,\n                  credits_vol_count,\n                  credits_vol_amount,\n                  disc_rate_revenue,\n                  disc_per_item_revenue,\n                  interchange_revenue,\n                  surcharge_revenue,\n                  surcharge_per_item_revenue,\n                  assessment_expense,\n                  interchange_expense,\n                  item_fee,\n                  item_count,\n                  equip_revenue,\n                  ticket_count,\n                  batch_count,\n                  auth_count,\n                  auth_per_item,\n                  misc_revenue,\n                  mtd_inc,\n                  description,\n                  load_filename\n                )\n              values\n                (  :1 ,\n                   :2 ,\n                   :3 ,\n                   :4 ,\n                   :5 ,\n                   :6 ,\n                   :7 ,\n                   :8 ,\n                  0.0,    -- discount rate income\n                  0.0,    -- discount per item income\n                  0.0,    -- interchange revenue\n                  0.0,    -- surcharge income\n                  0.0,    -- surcharge per item (not supported)\n                  0.0,    -- assessment expense\n                  0.0,    -- interchange expense\n                   :9 ,       -- item fee\n                  0,      -- quantity\n                  0,      -- equip revenue\n                  0,      -- ticket count\n                  0,      -- batch count\n                  0,      -- auth count\n                  0,      -- auth fee\n                  0,                    -- misc revenue\n                  round( :10 ,2), -- mtd total\n                   :11 ,\n                   :12 \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setString(3,__sJT_4846);
   __sJT_st.setString(4,__sJT_4847);
   __sJT_st.setInt(5,salesCount);
   __sJT_st.setDouble(6,salesAmount);
   __sJT_st.setInt(7,creditsCount);
   __sJT_st.setDouble(8,creditsAmount);
   __sJT_st.setDouble(9,totalExp);
   __sJT_st.setDouble(10,__sJT_4848);
   __sJT_st.setString(11,__sJT_4849);
   __sJT_st.setString(12,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1322^13*/
            
            
            
            
          }
        }
      }
    }
    catch( Exception e )
    {
      logEntry( "loadPlanData("+merchantId+","+activeDate+","+loadFilename+")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  private void loadMiscContractData( long merchantId, Date activeDate, String loadFilename )
  {
    int                   betType           = BankContractBean.BET_NONE;
    String[]              contractElement   = null;
    String                desc              = null;
    ResultSetIterator     it                = null;
    int                   itemCount         = 0;
    double                perItem           = 0.0;
    String                productCode       = null;
    ResultSet             resultSet         = null;
    double[]              volAmount         = new double[2];
    int[]                 volCount          = new int[2];
    double                totalExp          = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1357^7*/

//  ************************************************************
//  #sql [Ctx] it = { select sm.product_code                      as product_code,
//                 sm.incoming_chargeback_count         as cb_count,
//                 decode(sm.merchant_status,
//                        'C',0,
//                        'D',0,
//                        'F',0,
//                        1)                            as active_merchant_count,
//                 decode(sm.new_merchant,'Y',1,0)      as new_merchant_count,
//                 sm.RETRIEVAL_COUNT                   as retr_count,                      
//                 sm.service_call_count                as service_call_count,
//                 decode(sm.amex_setup,'Y',1,0)        as amex_setup_count,
//                 decode(sm.discover_setup,'Y',1,0)    as disc_setup_count
//          from   monthly_extract_summary sm
//          where  sm.MERCHANT_NUMBER = :merchantId and
//                 sm.active_date     = :activeDate      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select sm.product_code                      as product_code,\n               sm.incoming_chargeback_count         as cb_count,\n               decode(sm.merchant_status,\n                      'C',0,\n                      'D',0,\n                      'F',0,\n                      1)                            as active_merchant_count,\n               decode(sm.new_merchant,'Y',1,0)      as new_merchant_count,\n               sm.RETRIEVAL_COUNT                   as retr_count,                      \n               sm.service_call_count                as service_call_count,\n               decode(sm.amex_setup,'Y',1,0)        as amex_setup_count,\n               decode(sm.discover_setup,'Y',1,0)    as disc_setup_count\n        from   monthly_extract_summary sm\n        where  sm.MERCHANT_NUMBER =  :1  and\n               sm.active_date     =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"21com.mes.startup.TranscomExtractSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1374^7*/
      resultSet = it.getResultSet();
      
      while(resultSet.next())
      {
        productCode = resultSet.getString("product_code");
      
        for ( int i = 0; i < TranscomContractElements.length; ++i )
        {
          contractElement   = TranscomContractElements[i];
          
          // if the current element requires a specific product code
          // then the current row must match that product code to 
          // be processed, otherwise skip the current contract element.
          if ( (contractElement[CE_PRODUCT_CODE] != null) &&    
               ((productCode == null) || !(productCode.equals(contractElement[CE_PRODUCT_CODE]))) )
          {
            continue;
          }
          
          betType           = Integer.parseInt(contractElement[CE_BET_TYPE]);
          perItem           = LiabilityContract.getContractItemRate(betType,productCode);
          itemCount         = resultSet.getInt(contractElement[CE_COUNT_FIELD_NAME]);
          totalExp          = (itemCount * perItem);
          
          if ( itemCount > 0 )
          {
            switch(betType)
            {
              case BankContractBean.BET_INCOMING_CHARGEBACK_PER_ITEM:
                loadChargebackVolumeData(merchantId,activeDate,volAmount,volCount);
                break;
              
              default:
                for( int vol_i = 0; vol_i < volCount.length; ++vol_i )
                {
                  volAmount[vol_i]  = 0.0;
                  volCount[vol_i]   = 0;
                }
                break;
            }
          
            /*@lineinfo:generated-code*//*@lineinfo:1416^13*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_extract_summary
//                  ( merchant_number,
//                    active_date,
//                    card_type,
//                    charge_type,
//                    sales_vol_count,
//                    sales_vol_amount,
//                    credits_vol_count,
//                    credits_vol_amount,
//                    disc_rate_revenue,
//                    disc_per_item_revenue,
//                    interchange_revenue,
//                    surcharge_revenue,
//                    surcharge_per_item_revenue,
//                    assessment_expense,
//                    interchange_expense,
//                    item_fee,
//                    item_count,
//                    equip_revenue,
//                    ticket_count,
//                    batch_count,
//                    auth_count,
//                    auth_per_item,
//                    misc_revenue,
//                    mtd_inc,
//                    description,
//                    load_filename
//                  )
//                values
//                  ( :merchantId,
//                    :activeDate,
//                    '90',
//                    :contractElement[CE_TRANSCOM_CHARGE_TYPE],
//                    :volCount[VT_SALES],
//                    :volAmount[VT_SALES],
//                    :volCount[VT_CREDITS],
//                    :volAmount[VT_CREDITS],
//                    0.0,    -- discount rate income
//                    0.0,    -- discount per item income
//                    0.0,    -- interchange revenue
//                    0.0,    -- surcharge income
//                    0.0,    -- surcharge per item (not supported)
//                    0.0,    -- assessment expense
//                    0.0,    -- interchange expense
//                    0,      -- item fee (?)
//                    :itemCount,  -- item count (quantity)
//                    0,      -- equip revenue
//                    0,      -- ticket count
//                    0,      -- batch count
//                    0,      -- auth count
//                    0.0,    -- auth per item expense
//                    round(:-totalExp,2),  -- misc revenue
//                    round(:-totalExp,2),  -- mtd total
//                    :contractElement[CE_DESCRIPTION],
//                    :loadFilename
//                  )    
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4850 = contractElement[CE_TRANSCOM_CHARGE_TYPE];
 int __sJT_4851 = volCount[VT_SALES];
 double __sJT_4852 = volAmount[VT_SALES];
 int __sJT_4853 = volCount[VT_CREDITS];
 double __sJT_4854 = volAmount[VT_CREDITS];
 double __sJT_4855 = -totalExp;
 double __sJT_4856 = -totalExp;
 String __sJT_4857 = contractElement[CE_DESCRIPTION];
   String theSqlTS = "insert into transcom_extract_summary\n                ( merchant_number,\n                  active_date,\n                  card_type,\n                  charge_type,\n                  sales_vol_count,\n                  sales_vol_amount,\n                  credits_vol_count,\n                  credits_vol_amount,\n                  disc_rate_revenue,\n                  disc_per_item_revenue,\n                  interchange_revenue,\n                  surcharge_revenue,\n                  surcharge_per_item_revenue,\n                  assessment_expense,\n                  interchange_expense,\n                  item_fee,\n                  item_count,\n                  equip_revenue,\n                  ticket_count,\n                  batch_count,\n                  auth_count,\n                  auth_per_item,\n                  misc_revenue,\n                  mtd_inc,\n                  description,\n                  load_filename\n                )\n              values\n                (  :1 ,\n                   :2 ,\n                  '90',\n                   :3 ,\n                   :4 ,\n                   :5 ,\n                   :6 ,\n                   :7 ,\n                  0.0,    -- discount rate income\n                  0.0,    -- discount per item income\n                  0.0,    -- interchange revenue\n                  0.0,    -- surcharge income\n                  0.0,    -- surcharge per item (not supported)\n                  0.0,    -- assessment expense\n                  0.0,    -- interchange expense\n                  0,      -- item fee (:8)\n                   :9 ,  -- item count (quantity)\n                  0,      -- equip revenue\n                  0,      -- ticket count\n                  0,      -- batch count\n                  0,      -- auth count\n                  0.0,    -- auth per item expense\n                  round( :10 ,2),  -- misc revenue\n                  round( :11 ,2),  -- mtd total\n                   :12 ,\n                   :13 \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setString(3,__sJT_4850);
   __sJT_st.setInt(4,__sJT_4851);
   __sJT_st.setDouble(5,__sJT_4852);
   __sJT_st.setInt(6,__sJT_4853);
   __sJT_st.setDouble(7,__sJT_4854);
   __sJT_st.setInt(8,itemCount);
   __sJT_st.setDouble(9,__sJT_4855);
   __sJT_st.setDouble(10,__sJT_4856);
   __sJT_st.setString(11,__sJT_4857);
   __sJT_st.setString(12,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1474^13*/
          }
        }
      }          
    }
    catch( Exception e )
    {
      logEntry( "loadMiscContractData("+merchantId+","+activeDate+","+loadFilename+")", e.toString() );
    }
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
  }

  public boolean loadTranscomExtractSummary( String loadFilename )
  {
    Date                  activeDate        = null;
    double                assessmentExp     = 0.0;
    double                assessmentInc     = 0.0;
    double                assessmentExpRate = 0.0;
    double                assessmentIncRate = 0.0;
    double                calculatedDiscount= 0.0;
    String                cardTypeString    = "";
    int                   catCount          = 0;
    NumberFormat          catFormat         = NumberFormat.getNumberInstance();
    String                catString         = "00";
    String                chargeTypeString  = "";
    double                collectedDiscount = 0.0;
    Date                  contractDate      = null;
    double                creditsAmount     = 0.0;
    int                   creditsCount      = 0;
    String                desc              = null;
    long                  directNode        = 0L;
    double                discPerItemInc    = 0.0;
    double                discRateInc       = 0.0;
    boolean               fileProcessed     = false;
    double                icExp             = 0.0;
    ResultSetIterator     it                = null;
    int                   itemCount         = 0;
    long                  loadSec           = 0L;
    long                  merchantId        = 0L;
    int                   recCount          = 0;
    ResultSet             resultSet         = null;
    double                salesAmount       = 0.0;
    int                   salesCount        = 0;
    double                surchargeInc      = 0.0;
    long                  topNode           = getTranscomHierarchyNode(loadFilename);
    double                totalDiscount     = 0.0;
    
    String debug = "entry";//@
    
    try
    {
      catFormat.setMinimumIntegerDigits(2);
      
      LiabilityContract.connect(true);
      
      // get the active date for the m/e load filename
      /*@lineinfo:generated-code*//*@lineinfo:1533^7*/

//  ************************************************************
//  #sql [Ctx] { select  max(gn.hh_active_date),
//                  least(max(gn.hh_active_date),'01-NOV-2003'),
//                  to_number(get_file_bank_number(:loadFilename) || '500001')
//          
//          from    monthly_extract_gn  gn
//          where   gn.load_file_id = load_filename_to_load_file_id(:loadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max(gn.hh_active_date),\n                least(max(gn.hh_active_date),'01-NOV-2003'),\n                to_number(get_file_bank_number( :1 ) || '500001')\n         \n        from    monthly_extract_gn  gn\n        where   gn.load_file_id = load_filename_to_load_file_id( :2 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.startup.TranscomExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setString(2,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activeDate = (java.sql.Date)__sJT_rs.getDate(1);
   contractDate = (java.sql.Date)__sJT_rs.getDate(2);
   directNode = __sJT_rs.getLong(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1541^7*/
      
      // remove any existing entries for this load filename
      /*@lineinfo:generated-code*//*@lineinfo:1544^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from    transcom_extract_summary  tes
//          where   tes.merchant_number in 
//                  (
//                    select  gn.hh_merchant_number
//                    from    monthly_extract_gn  gn
//                    where   gn.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                  )
//                  and tes.active_date = :activeDate
//                  and tes.load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from    transcom_extract_summary  tes\n        where   tes.merchant_number in \n                (\n                  select  gn.hh_merchant_number\n                  from    monthly_extract_gn  gn\n                  where   gn.load_file_id = load_filename_to_load_file_id( :1 )\n                )\n                and tes.active_date =  :2 \n                and tes.load_filename =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setString(3,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1556^7*/
      
      // load the transcom contract
      LiabilityContract.loadContract( LiabilityContract.hierarchyNodeToOrgId(topNode), 
                                      ContractTypes.CONTRACT_SOURCE_LIABILITY,
                                      contractDate );
      
      // query all the data from the vital monthly extract files
      /*@lineinfo:generated-code*//*@lineinfo:1564^7*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ 
//                     ordered 
//                 */
//                 gn.hh_merchant_number                as merchant_number,
//                 gn.hh_active_date                    as active_date,
//                 gn.hh_load_sec                       as hh_load_sec,
//                 gn.t1_tot_calculated_discount        as total_disc_inc,
//                 vs.v1_income_assessment              as vs_assessment_inc,
//                 vs.v3_expense_assessment             as vs_assessment_exp,
//                 ( vs.V5_CAT_01_SALES_COUNT +
//                   vs.V5_CAT_01_CRED_COUNT  +
//                   vs.V5_CAT_02_SALES_COUNT +
//                   vs.V5_CAT_02_CRED_COUNT  +
//                   vs.V5_CAT_03_SALES_COUNT +
//                   vs.V5_CAT_03_CRED_COUNT  +
//                   vs.V5_CAT_04_SALES_COUNT +
//                   vs.V5_CAT_04_CRED_COUNT  +
//                   vs.V5_CAT_05_SALES_COUNT +
//                   vs.V5_CAT_05_CRED_COUNT  +
//                   vs.V5_CAT_06_SALES_COUNT +
//                   vs.V5_CAT_06_CRED_COUNT  +
//                   vs.V6_CAT_07_SALES_COUNT +
//                   vs.V6_CAT_07_CRED_COUNT  +
//                   vs.V6_CAT_08_SALES_COUNT +
//                   vs.V6_CAT_08_CRED_COUNT  +
//                   vs.V6_CAT_09_SALES_COUNT +
//                   vs.V6_CAT_09_CRED_COUNT  +
//                   vs.V6_CAT_10_SALES_COUNT +
//                   vs.V6_CAT_10_CRED_COUNT  +
//                   vs.V6_CAT_11_SALES_COUNT +
//                   vs.V6_CAT_11_CRED_COUNT  +
//                   vs.V6_CAT_12_SALES_COUNT +
//                   vs.V6_CAT_12_CRED_COUNT  +
//                   vs.V7_CAT_13_SALES_COUNT +
//                   vs.V7_CAT_13_CRED_COUNT  +
//                   vs.V7_CAT_14_SALES_COUNT +
//                   vs.V7_CAT_14_CRED_COUNT  +
//                   vs.V7_CAT_15_SALES_COUNT +
//                   vs.V7_CAT_15_CRED_COUNT  +
//                   vs.V7_CAT_16_SALES_COUNT +
//                   vs.V7_CAT_16_CRED_COUNT  +
//                   vs.V7_CAT_17_SALES_COUNT +
//                   vs.V7_CAT_17_CRED_COUNT  +
//                   vs.V7_CAT_18_SALES_COUNT +
//                   vs.V8_CAT_18_CRED_COUNT  +
//                   vs.V8_CAT_19_SALES_COUNT +
//                   vs.V8_CAT_19_CRED_COUNT  +
//                   vs.V8_CAT_20_SALES_COUNT +
//                   vs.V8_CAT_20_CRED_COUNT  +
//                   vs.V8_CAT_21_SALES_COUNT +
//                   vs.V8_CAT_21_CRED_COUNT  +
//                   vs.V8_CAT_22_SALES_COUNT +
//                   vs.V8_CAT_22_CRED_COUNT  +
//                   vs.V8_CAT_23_SALES_COUNT +
//                   vs.V8_CAT_23_CRED_COUNT  +
//                   vs.V9_CAT_24_SALES_COUNT +
//                   vs.V9_CAT_24_CRED_COUNT )          as vs_item_count,
//                 mc.m1_income_assessment              as mc_assessment_inc,
//                 mc.m2_expense_assessment             as mc_assessment_exp,
//                 ( mc.M3_CAT_01_SALES_COUNT +
//                   mc.M3_CAT_01_CRED_COUNT  +
//                   mc.M3_CAT_02_SALES_COUNT +
//                   mc.M3_CAT_02_CRED_COUNT  +
//                   mc.M3_CAT_03_SALES_COUNT +
//                   mc.M3_CAT_03_CRED_COUNT  +
//                   mc.M3_CAT_04_SALES_COUNT +
//                   mc.M3_CAT_04_CRED_COUNT  +
//                   mc.M3_CAT_05_SALES_COUNT +
//                   mc.M3_CAT_05_CRED_COUNT  +
//                   mc.M3_CAT_06_SALES_COUNT +
//                   mc.M3_CAT_06_CRED_COUNT  +
//                   mc.M4_CAT_07_SALES_COUNT +
//                   mc.M4_CAT_07_CRED_COUNT  +
//                   mc.M4_CAT_08_SALES_COUNT +
//                   mc.M4_CAT_08_CRED_COUNT  +
//                   mc.M4_CAT_09_SALES_COUNT +
//                   mc.M4_CAT_09_CRED_COUNT  +
//                   mc.M4_CAT_10_SALES_COUNT +
//                   mc.M4_CAT_10_CRED_COUNT  +
//                   mc.M4_CAT_11_SALES_COUNT +
//                   mc.M4_CAT_11_CRED_COUNT  +
//                   mc.M4_CAT_12_SALES_COUNT +
//                   mc.M4_CAT_12_CRED_COUNT  )         as mc_item_count,
//                 vs.V5_CAT_01_SALES_COUNT                                             as vs_sales_count_01,
//                 vs.V5_CAT_01_SALES_AMOUNT                                            as vs_sales_amount_01,
//                 vs.V5_CAT_01_CRED_COUNT                                              as vs_credits_count_01,
//                 vs.V5_CAT_01_CRED_AMOUNT                                             as vs_credits_amount_01,
//                 round( (vs.V5_CAT_01_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_01,
//                 round( (vs.V5_CAT_01_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_01,
//                 (vs.V1_INC_CAT_01_SALES + vs.V1_INC_CAT_01_FEES)                     as vs_surc_01,
//                 vs.V5_CAT_01_INTCH                                                   as vs_ic_01,
//                 vs.V5_CAT_02_SALES_COUNT                                             as vs_sales_count_02,
//                 vs.V5_CAT_02_SALES_AMOUNT                                            as vs_sales_amount_02,
//                 vs.V5_CAT_02_CRED_COUNT                                              as vs_credits_count_02,
//                 vs.V5_CAT_02_CRED_AMOUNT                                             as vs_credits_amount_02,
//                 round( (vs.V5_CAT_02_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_02,
//                 round( (vs.V5_CAT_02_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_02,
//                 (vs.V1_INC_CAT_02_SALES + vs.V1_INC_CAT_02_FEES)                     as vs_surc_02,
//                 vs.V5_CAT_02_INTCH                                                   as vs_ic_02,
//                 vs.V5_CAT_03_SALES_COUNT                                             as vs_sales_count_03,
//                 vs.V5_CAT_03_SALES_AMOUNT                                            as vs_sales_amount_03,
//                 vs.V5_CAT_03_CRED_COUNT                                              as vs_credits_count_03,
//                 vs.V5_CAT_03_CRED_AMOUNT                                             as vs_credits_amount_03,
//                 round( (vs.V5_CAT_03_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_03,
//                 round( (vs.V5_CAT_03_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_03,
//                 (vs.V1_INC_CAT_03_SALES + vs.V1_INC_CAT_03_FEES)                     as vs_surc_03,
//                 vs.V5_CAT_03_INTCH                                                   as vs_ic_03,
//                 vs.V5_CAT_04_SALES_COUNT                                             as vs_sales_count_04,
//                 vs.V5_CAT_04_SALES_AMOUNT                                            as vs_sales_amount_04,
//                 vs.V5_CAT_04_CRED_COUNT                                              as vs_credits_count_04,
//                 vs.V5_CAT_04_CRED_AMOUNT                                             as vs_credits_amount_04,
//                 round( (vs.V5_CAT_04_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_04,
//                 round( (vs.V5_CAT_04_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_04,
//                 (vs.V1_INC_CAT_04_SALES + vs.V1_INC_CAT_04_FEES)                     as vs_surc_04,
//                 vs.V5_CAT_04_INTCH                                                   as vs_ic_04,
//                 vs.V5_CAT_05_SALES_COUNT                                             as vs_sales_count_05,
//                 vs.V5_CAT_05_SALES_AMOUNT                                            as vs_sales_amount_05,
//                 vs.V5_CAT_05_CRED_COUNT                                              as vs_credits_count_05,
//                 vs.V5_CAT_05_CRED_AMOUNT                                             as vs_credits_amount_05,
//                 round( (vs.V5_CAT_05_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_05,
//                 round( (vs.V5_CAT_05_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_05,
//                 (vs.V1_INC_CAT_05_SALES + vs.V1_INC_CAT_05_FEES)                     as vs_surc_05,
//                 vs.V5_CAT_05_INTCH                                                   as vs_ic_05,
//                 vs.V5_CAT_06_SALES_COUNT                                             as vs_sales_count_06,
//                 vs.V5_CAT_06_SALES_AMOUNT                                            as vs_sales_amount_06,
//                 vs.V5_CAT_06_CRED_COUNT                                              as vs_credits_count_06,
//                 vs.V5_CAT_06_CRED_AMOUNT                                             as vs_credits_amount_06,
//                 round( (vs.V5_CAT_06_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_06,
//                 round( (vs.V5_CAT_06_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_06,
//                 (vs.V1_INC_CAT_06_SALES + vs.V1_INC_CAT_06_FEES)                     as vs_surc_06,
//                 vs.V6_CAT_06_INTCH                                                   as vs_ic_06,
//                 vs.V6_CAT_07_SALES_COUNT                                             as vs_sales_count_07,
//                 vs.V6_CAT_07_SALES_AMOUNT                                            as vs_sales_amount_07,
//                 vs.V6_CAT_07_CRED_COUNT                                              as vs_credits_count_07,
//                 vs.V6_CAT_07_CRED_AMOUNT                                             as vs_credits_amount_07,
//                 round( (vs.V6_CAT_07_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_07,
//                 round( (vs.V6_CAT_07_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_07,
//                 (vs.V1_INC_CAT_07_SALES + vs.V1_INC_CAT_07_FEES)                     as vs_surc_07,
//                 vs.V6_CAT_07_INTCH                                                   as vs_ic_07,
//                 vs.V6_CAT_08_SALES_COUNT                                             as vs_sales_count_08,
//                 vs.V6_CAT_08_SALES_AMOUNT                                            as vs_sales_amount_08,
//                 vs.V6_CAT_08_CRED_COUNT                                              as vs_credits_count_08,
//                 vs.V6_CAT_08_CRED_AMOUNT                                             as vs_credits_amount_08,
//                 round( (vs.V6_CAT_08_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_08,
//                 round( (vs.V6_CAT_08_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_08,
//                 (vs.V1_INC_CAT_08_SALES + vs.V1_INC_CAT_08_FEES)                     as vs_surc_08,
//                 vs.V6_CAT_08_INTCH                                                   as vs_ic_08,
//                 vs.V6_CAT_09_SALES_COUNT                                             as vs_sales_count_09,
//                 vs.V6_CAT_09_SALES_AMOUNT                                            as vs_sales_amount_09,
//                 vs.V6_CAT_09_CRED_COUNT                                              as vs_credits_count_09,
//                 vs.V6_CAT_09_CRED_AMOUNT                                             as vs_credits_amount_09,
//                 round( (vs.V6_CAT_09_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_09,
//                 round( (vs.V6_CAT_09_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_09,
//                 (vs.V1_INC_CAT_09_SALES + vs.V1_INC_CAT_09_FEES)                     as vs_surc_09,
//                 vs.V6_CAT_09_INTCH                                                   as vs_ic_09,
//                 vs.V6_CAT_10_SALES_COUNT                                             as vs_sales_count_10,
//                 vs.V6_CAT_10_SALES_AMOUNT                                            as vs_sales_amount_10,
//                 vs.V6_CAT_10_CRED_COUNT                                              as vs_credits_count_10,
//                 vs.V6_CAT_10_CRED_AMOUNT                                             as vs_credits_amount_10,
//                 round( (vs.V6_CAT_10_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_10,
//                 round( (vs.V6_CAT_10_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_10,
//                 (vs.V1_INC_CAT_10_SALES + vs.V1_INC_CAT_10_FEES)                     as vs_surc_10,
//                 vs.V6_CAT_10_INTCH                                                   as vs_ic_10,
//                 vs.V6_CAT_11_SALES_COUNT                                             as vs_sales_count_11,
//                 vs.V6_CAT_11_SALES_AMOUNT                                            as vs_sales_amount_11,
//                 vs.V6_CAT_11_CRED_COUNT                                              as vs_credits_count_11,
//                 vs.V6_CAT_11_CRED_AMOUNT                                             as vs_credits_amount_11,
//                 round( (vs.V6_CAT_11_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_11,
//                 round( (vs.V6_CAT_11_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_11,
//                 (vs.V1_INC_CAT_11_SALES + vs.V1_INC_CAT_11_FEES)                     as vs_surc_11,
//                 vs.V6_CAT_11_INTCH                                                   as vs_ic_11,
//                 vs.V6_CAT_12_SALES_COUNT                                             as vs_sales_count_12,
//                 vs.V6_CAT_12_SALES_AMOUNT                                            as vs_sales_amount_12,
//                 vs.V6_CAT_12_CRED_COUNT                                              as vs_credits_count_12,
//                 vs.V7_CAT_12_CRED_AMOUNT                                             as vs_credits_amount_12,
//                 round( (vs.V6_CAT_12_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_12,
//                 round( (vs.V6_CAT_12_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_12,
//                 (vs.V1_INC_CAT_12_SALES + vs.V1_INC_CAT_12_FEES)                     as vs_surc_12,
//                 vs.V7_CAT_12_INTCH                                                   as vs_ic_12,
//                 vs.V7_CAT_13_SALES_COUNT                                             as vs_sales_count_13,
//                 vs.V7_CAT_13_SALES_AMOUNT                                            as vs_sales_amount_13,
//                 vs.V7_CAT_13_CRED_COUNT                                              as vs_credits_count_13,
//                 vs.V7_CAT_13_CRED_AMOUNT                                             as vs_credits_amount_13,
//                 round( (vs.V7_CAT_13_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_13,
//                 round( (vs.V7_CAT_13_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_13,
//                 (vs.V1_INC_CAT_13_SALES + vs.V1_INC_CAT_13_FEES)                     as vs_surc_13,
//                 vs.V7_CAT_13_INTCH                                                   as vs_ic_13,
//                 vs.V7_CAT_14_SALES_COUNT                                             as vs_sales_count_14,
//                 vs.V7_CAT_14_SALES_AMOUNT                                            as vs_sales_amount_14,
//                 vs.V7_CAT_14_CRED_COUNT                                              as vs_credits_count_14,
//                 vs.V7_CAT_14_CRED_AMOUNT                                             as vs_credits_amount_14,
//                 round( (vs.V7_CAT_14_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_14,
//                 round( (vs.V7_CAT_14_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_14,
//                 (vs.V1_INC_CAT_14_SALES + vs.V1_INC_CAT_14_FEES)                     as vs_surc_14,
//                 vs.V7_CAT_14_INTCH                                                   as vs_ic_14,
//                 vs.V7_CAT_15_SALES_COUNT                                             as vs_sales_count_15,
//                 vs.V7_CAT_15_SALES_AMOUNT                                            as vs_sales_amount_15,
//                 vs.V7_CAT_15_CRED_COUNT                                              as vs_credits_count_15,
//                 vs.V7_CAT_15_CRED_AMOUNT                                             as vs_credits_amount_15,
//                 round( (vs.V7_CAT_15_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_15,
//                 round( (vs.V7_CAT_15_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_15,
//                 (vs.V1_INC_CAT_15_SALES + vs.V1_INC_CAT_15_FEES)                     as vs_surc_15,
//                 vs.V7_CAT_15_INTCH                                                   as vs_ic_15,
//                 vs.V7_CAT_16_SALES_COUNT                                             as vs_sales_count_16,
//                 vs.V7_CAT_16_SALES_AMOUNT                                            as vs_sales_amount_16,
//                 vs.V7_CAT_16_CRED_COUNT                                              as vs_credits_count_16,
//                 vs.V7_CAT_16_CRED_AMOUNT                                             as vs_credits_amount_16,
//                 round( (vs.V7_CAT_16_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_16,
//                 round( (vs.V7_CAT_16_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_16,
//                 (vs.V1_INC_CAT_16_SALES + vs.V1_INC_CAT_16_FEES)                     as vs_surc_16,
//                 vs.V7_CAT_16_INTCH                                                   as vs_ic_16,
//                 vs.V7_CAT_17_SALES_COUNT                                             as vs_sales_count_17,
//                 vs.V7_CAT_17_SALES_AMOUNT                                            as vs_sales_amount_17,
//                 vs.V7_CAT_17_CRED_COUNT                                              as vs_credits_count_17,
//                 vs.V7_CAT_17_CRED_AMOUNT                                             as vs_credits_amount_17,
//                 round( (vs.V7_CAT_17_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_17,
//                 round( (vs.V7_CAT_17_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_17,
//                 (vs.V1_INC_CAT_17_SALES + vs.V2_INC_CAT_17_FEES)                     as vs_surc_17,
//                 vs.V7_CAT_17_INTCH                                                   as vs_ic_17,
//                 vs.V7_CAT_18_SALES_COUNT                                             as vs_sales_count_18,
//                 vs.V8_CAT_18_SALES_AMOUNT                                            as vs_sales_amount_18,
//                 vs.V8_CAT_18_CRED_COUNT                                              as vs_credits_count_18,
//                 vs.V8_CAT_18_CRED_AMOUNT                                             as vs_credits_amount_18,
//                 round( (vs.V8_CAT_18_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_18,
//                 round( (vs.V7_CAT_18_SALES_COUNT  * vs_pl.pl_disc_rate_per_item),2 ) as vs_calc_per_item_18,
//                 (vs.V2_INC_CAT_18_SALES + vs.V2_INC_CAT_18_FEES)                     as vs_surc_18,
//                 vs.V8_CAT_18_INTCH                                                   as vs_ic_18,
//                 vs.V8_CAT_19_SALES_COUNT                                             as vs_sales_count_19,
//                 vs.V8_CAT_19_SALES_AMOUNT                                            as vs_sales_amount_19,
//                 vs.V8_CAT_19_CRED_COUNT                                              as vs_credits_count_19,
//                 vs.V8_CAT_19_CRED_AMOUNT                                             as vs_credits_amount_19,
//                 round( (vs.V8_CAT_19_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_19,
//                 round( (vs.V8_CAT_19_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_19,
//                 (vs.V2_INC_CAT_19_SALES + vs.V2_INC_CAT_19_FEES)                     as vs_surc_19,
//                 vs.V8_CAT_19_INTCH                                                   as vs_ic_19,
//                 vs.V8_CAT_20_SALES_COUNT                                             as vs_sales_count_20,
//                 vs.V8_CAT_20_SALES_AMOUNT                                            as vs_sales_amount_20,
//                 vs.V8_CAT_20_CRED_COUNT                                              as vs_credits_count_20,
//                 vs.V8_CAT_20_CRED_AMOUNT                                             as vs_credits_amount_20,
//                 round( (vs.V8_CAT_20_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_20,
//                 round( (vs.V8_CAT_20_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_20,
//                 (vs.V2_INC_CAT_20_SALES + vs.V2_INC_CAT_20_FEES)                     as vs_surc_20,
//                 vs.V8_CAT_20_INTCH                                                   as vs_ic_20,
//                 vs.V8_CAT_21_SALES_COUNT                                             as vs_sales_count_21,
//                 vs.V8_CAT_21_SALES_AMOUNT                                            as vs_sales_amount_21,
//                 vs.V8_CAT_21_CRED_COUNT                                              as vs_credits_count_21,
//                 vs.V8_CAT_21_CRED_AMOUNT                                             as vs_credits_amount_21,
//                 round( (vs.V8_CAT_21_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_21,
//                 round( (vs.V8_CAT_21_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_21,
//                 (vs.V2_INC_CAT_21_SALES + vs.V2_INC_CAT_21_FEES)                     as vs_surc_21,
//                 vs.V8_CAT_21_INTCH                                                   as vs_ic_21,
//                 vs.V8_CAT_22_SALES_COUNT                                             as vs_sales_count_22,
//                 vs.V8_CAT_22_SALES_AMOUNT                                            as vs_sales_amount_22,
//                 vs.V8_CAT_22_CRED_COUNT                                              as vs_credits_count_22,
//                 vs.V8_CAT_22_CRED_AMOUNT                                             as vs_credits_amount_22,
//                 round( (vs.V8_CAT_22_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_22,
//                 round( (vs.V8_CAT_22_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_22,
//                 (vs.V2_INC_CAT_22_SALES + vs.V2_INC_CAT_22_FEES)                     as vs_surc_22,
//                 vs.V8_CAT_22_INTCH                                                   as vs_ic_22,
//                 vs.V8_CAT_23_SALES_COUNT                                             as vs_sales_count_23,
//                 vs.V8_CAT_23_SALES_AMOUNT                                            as vs_sales_amount_23,
//                 vs.V8_CAT_23_CRED_COUNT                                              as vs_credits_count_23,
//                 vs.V8_CAT_23_CRED_AMOUNT                                             as vs_credits_amount_23,
//                 round( (vs.V8_CAT_23_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_23,
//                 round( (vs.V8_CAT_23_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_23,
//                 (vs.V2_INC_CAT_23_SALES + vs.V2_INC_CAT_23_FEES)                     as vs_surc_23,
//                 vs.V9_CAT_23_INTCH                                                   as vs_ic_23,
//                 vs.V9_CAT_24_SALES_COUNT                                             as vs_sales_count_24,
//                 vs.V9_CAT_24_SALES_AMOUNT                                            as vs_sales_amount_24,
//                 vs.V9_CAT_24_CRED_COUNT                                              as vs_credits_count_24,
//                 vs.V9_CAT_24_CRED_AMOUNT                                             as vs_credits_amount_24,
//                 round( (vs.V9_CAT_24_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_24,
//                 round( (vs.V9_CAT_24_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_24,
//                 (vs.V2_INC_CAT_24_SALES + vs.V2_INC_CAT_24_FEES)                     as vs_surc_24,
//                 vs.V9_CAT_24_INTCH                                                   as vs_ic_24,
//                 /* these empty columns added to simplify processing algorithm below */               
//                 0                                                                    as vs_sales_count_25,
//                 0.0                                                                  as vs_sales_amount_25,
//                 0                                                                    as vs_credits_count_25,
//                 0.0                                                                  as vs_credits_amount_25,
//                 0.0                                                                  as vs_calc_disc_25,
//                 0.0                                                                  as vs_calc_per_item_25,
//                 vs.V2_INC_CAT_25_FEES                                                as vs_surc_25,
//                 vs.V9_CAT_25_INTCH                                                   as vs_ic_25,
//                 mc.M3_CAT_01_SALES_COUNT                                             as mc_sales_count_01,
//                 mc.M3_CAT_01_SALES_AMOUNT                                            as mc_sales_amount_01,
//                 mc.M3_CAT_01_CRED_COUNT                                              as mc_credits_count_01,
//                 mc.M3_CAT_01_CRED_AMOUNT                                             as mc_credits_amount_01,
//                 round( (mc.M3_CAT_01_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_01,
//                 round( (mc.M3_CAT_01_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_01,
//                 (mc.M1_INC_CAT_01_SALES + mc.M1_INC_CAT_01_FEES)                     as mc_surc_01,
//                 mc.M3_CAT_01_INTCH                                                   as mc_ic_01,
//                 mc.M3_CAT_02_SALES_COUNT                                             as mc_sales_count_02,
//                 mc.M3_CAT_02_SALES_AMOUNT                                            as mc_sales_amount_02,
//                 mc.M3_CAT_02_CRED_COUNT                                              as mc_credits_count_02,
//                 mc.M3_CAT_02_CRED_AMOUNT                                             as mc_credits_amount_02,
//                 round( (mc.M3_CAT_02_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_02,
//                 round( (mc.M3_CAT_02_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_02,
//                 (mc.M1_INC_CAT_02_SALES + mc.M1_INC_CAT_02_FEES)                     as mc_surc_02,
//                 mc.M3_CAT_02_INTCH                                                   as mc_ic_02,
//                 mc.M3_CAT_03_SALES_COUNT                                             as mc_sales_count_03,
//                 mc.M3_CAT_03_SALES_AMOUNT                                            as mc_sales_amount_03,
//                 mc.M3_CAT_03_CRED_COUNT                                              as mc_credits_count_03,
//                 mc.M3_CAT_03_CRED_AMOUNT                                             as mc_credits_amount_03,
//                 round( (mc.M3_CAT_03_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_03,
//                 round( (mc.M3_CAT_03_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_03,
//                 (mc.M1_INC_CAT_03_SALES + mc.M1_INC_CAT_03_FEES)                     as mc_surc_03,
//                 mc.M3_CAT_03_INTCH                                                   as mc_ic_03,
//                 mc.M3_CAT_04_SALES_COUNT                                             as mc_sales_count_04,
//                 mc.M3_CAT_04_SALES_AMOUNT                                            as mc_sales_amount_04,
//                 mc.M3_CAT_04_CRED_COUNT                                              as mc_credits_count_04,
//                 mc.M3_CAT_04_CRED_AMOUNT                                             as mc_credits_amount_04,
//                 round( (mc.M3_CAT_04_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_04,
//                 round( (mc.M3_CAT_04_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_04,
//                 (mc.M1_INC_CAT_04_SALES + mc.M1_INC_CAT_04_FEES)                     as mc_surc_04,
//                 mc.M3_CAT_04_INTCH                                                   as mc_ic_04,
//                 mc.M3_CAT_05_SALES_COUNT                                             as mc_sales_count_05,
//                 mc.M3_CAT_05_SALES_AMOUNT                                            as mc_sales_amount_05,
//                 mc.M3_CAT_05_CRED_COUNT                                              as mc_credits_count_05,
//                 mc.M3_CAT_05_CRED_AMOUNT                                             as mc_credits_amount_05,
//                 round( (mc.M3_CAT_05_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_05,
//                 round( (mc.M3_CAT_05_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_05,
//                 (mc.M1_INC_CAT_05_SALES + mc.M1_INC_CAT_05_FEES)                     as mc_surc_05,
//                 mc.M3_CAT_05_INTCH                                                   as mc_ic_05,
//                 mc.M3_CAT_06_SALES_COUNT                                             as mc_sales_count_06,
//                 mc.M3_CAT_06_SALES_AMOUNT                                            as mc_sales_amount_06,
//                 mc.M3_CAT_06_CRED_COUNT                                              as mc_credits_count_06,
//                 mc.M3_CAT_06_CRED_AMOUNT                                             as mc_credits_amount_06,
//                 round( (mc.M3_CAT_06_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_06,
//                 round( (mc.M3_CAT_06_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_06,
//                 (mc.M1_INC_CAT_06_SALES + mc.M1_INC_CAT_06_FEES)                     as mc_surc_06,
//                 mc.M4_CAT_06_INTCH                                                   as mc_ic_06,
//                 mc.M4_CAT_07_SALES_COUNT                                             as mc_sales_count_07,
//                 mc.M4_CAT_07_SALES_AMOUNT                                            as mc_sales_amount_07,
//                 mc.M4_CAT_07_CRED_COUNT                                              as mc_credits_count_07,
//                 mc.M4_CAT_07_CRED_AMOUNT                                             as mc_credits_amount_07,
//                 round( (mc.M4_CAT_07_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_07,
//                 round( (mc.M4_CAT_07_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_07,
//                 (mc.M1_INC_CAT_07_SALES + mc.M1_INC_CAT_07_FEES)                     as mc_surc_07,
//                 mc.M4_CAT_07_INTCH                                                   as mc_ic_07,
//                 mc.M4_CAT_08_SALES_COUNT                                             as mc_sales_count_08,
//                 mc.M4_CAT_08_SALES_AMOUNT                                            as mc_sales_amount_08,
//                 mc.M4_CAT_08_CRED_COUNT                                              as mc_credits_count_08,
//                 mc.M4_CAT_08_CRED_AMOUNT                                             as mc_credits_amount_08,
//                 round( (mc.M4_CAT_08_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_08,
//                 round( (mc.M4_CAT_08_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_08,
//                 (mc.M1_INC_CAT_08_SALES + mc.M1_INC_CAT_08_FEES)                     as mc_surc_08,
//                 mc.M4_CAT_08_INTCH                                                   as mc_ic_08,
//                 mc.M4_CAT_09_SALES_COUNT                                             as mc_sales_count_09,
//                 mc.M4_CAT_09_SALES_AMOUNT                                            as mc_sales_amount_09,
//                 mc.M4_CAT_09_CRED_COUNT                                              as mc_credits_count_09,
//                 mc.M4_CAT_09_CRED_AMOUNT                                             as mc_credits_amount_09,
//                 round( (mc.M4_CAT_09_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_09,
//                 round( (mc.M4_CAT_09_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_09,
//                 (mc.M1_INC_CAT_09_SALES + mc.M1_INC_CAT_09_FEES)                     as mc_surc_09,
//                 mc.M4_CAT_09_INTCH                                                   as mc_ic_09,
//                 mc.M4_CAT_10_SALES_COUNT                                             as mc_sales_count_10,
//                 mc.M4_CAT_10_SALES_AMOUNT                                            as mc_sales_amount_10,
//                 mc.M4_CAT_10_CRED_COUNT                                              as mc_credits_count_10,
//                 mc.M4_CAT_10_CRED_AMOUNT                                             as mc_credits_amount_10,
//                 round( (mc.M4_CAT_10_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_10,
//                 round( (mc.M4_CAT_10_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_10,
//                 (mc.M1_INC_CAT_10_SALES + mc.M1_INC_CAT_10_FEES)                     as mc_surc_10,
//                 mc.M4_CAT_10_INTCH                                                   as mc_ic_10,
//                 mc.M4_CAT_11_SALES_COUNT                                             as mc_sales_count_11,
//                 mc.M4_CAT_11_SALES_AMOUNT                                            as mc_sales_amount_11,
//                 mc.M4_CAT_11_CRED_COUNT                                              as mc_credits_count_11,
//                 mc.M4_CAT_11_CRED_AMOUNT                                             as mc_credits_amount_11,
//                 round( (mc.M4_CAT_11_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_11,
//                 round( (mc.M4_CAT_11_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_11,
//                 (mc.M1_INC_CAT_11_SALES + mc.M1_INC_CAT_11_FEES)                     as mc_surc_11,
//                 mc.M4_CAT_11_INTCH                                                   as mc_ic_11,
//                 mc.M4_CAT_12_SALES_COUNT                                             as mc_sales_count_12,
//                 mc.M4_CAT_12_SALES_AMOUNT                                            as mc_sales_amount_12,
//                 mc.M4_CAT_12_CRED_COUNT                                              as mc_credits_count_12,
//                 mc.M5_CAT_12_CRED_AMOUNT                                             as mc_credits_amount_12,
//                 round( (mc.M4_CAT_12_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2)    as mc_calc_disc_12,
//                 round( (mc.M4_CAT_12_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2)  as mc_calc_per_item_12,
//                 (mc.M1_INC_CAT_12_SALES + mc.M1_INC_CAT_12_FEES)                     as mc_surc_12,
//                 mc.M5_CAT_12_INTCH                                                   as mc_ic_12
//          from  organization             o,
//                group_merchant           gm,
//                mif                      mf,
//                merchant                 mr,
//                application              app,
//                sales_rep                sr,
//                monthly_extract_gn       gn,
//                monthly_extract_pl       mc_pl,
//                monthly_extract_pl       vs_pl,
//                monthly_extract_visa     vs,
//                monthly_extract_mc       mc
//          where o.org_group in 
//                (
//                  select  :directNode
//                  from    dual
//                  union
//                  select  :topNode
//                  from    dual
//                  union 
//                  select hierarchy_node 
//                  from   transcom_extract_legacy_nodes
//                ) and              
//                gm.org_num = o.org_num and
//                mf.merchant_number = gm.merchant_number and
//                mr.merch_number(+) = mf.merchant_number and
//                app.app_seq_num(+) = mr.app_seq_num and
//                sr.user_id(+) = app.app_user_id and
//                (
//                  o.org_group = :topNode or
//                  nvl(app.app_type,-1) = 28 or -- mes new app
//                  (
//                    -- old direct mes app that past the 13 month audit cycle
//                    is_direct_commission_plan(sr.rep_plan_id) = 1 and   
//                    months_between((last_day(:activeDate)+1),mf.activation_date) > 13
//                  ) or
//                  exists   -- in legacy table
//                  (
//                    select  gmi.merchant_number
//                    from    transcom_extract_legacy_nodes  ln,
//                            organization                   oi,
//                            group_merchant                 gmi                            
//                    where   oi.org_group = ln.hierarchy_node and
//                            gmi.org_num = oi.org_num and
//                            gmi.merchant_number = mf.merchant_number
//                  )
//                ) and 
//                gn.hh_merchant_number = mf.merchant_number and
//                gn.hh_active_date = :activeDate and
//                mc_pl.hh_load_sec(+)  = gn.hh_load_sec and
//                mc_pl.pl_plan_type(+) = 'MC' and
//                vs_pl.hh_load_sec(+)  = gn.hh_load_sec and
//                vs_pl.pl_plan_type(+) = 'VISA' and
//                vs.hh_load_sec(+)  = gn.hh_load_sec and
//                mc.hh_load_sec(+)  = gn.hh_load_sec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select /*+ \n                   ordered \n               */\n               gn.hh_merchant_number                as merchant_number,\n               gn.hh_active_date                    as active_date,\n               gn.hh_load_sec                       as hh_load_sec,\n               gn.t1_tot_calculated_discount        as total_disc_inc,\n               vs.v1_income_assessment              as vs_assessment_inc,\n               vs.v3_expense_assessment             as vs_assessment_exp,\n               ( vs.V5_CAT_01_SALES_COUNT +\n                 vs.V5_CAT_01_CRED_COUNT  +\n                 vs.V5_CAT_02_SALES_COUNT +\n                 vs.V5_CAT_02_CRED_COUNT  +\n                 vs.V5_CAT_03_SALES_COUNT +\n                 vs.V5_CAT_03_CRED_COUNT  +\n                 vs.V5_CAT_04_SALES_COUNT +\n                 vs.V5_CAT_04_CRED_COUNT  +\n                 vs.V5_CAT_05_SALES_COUNT +\n                 vs.V5_CAT_05_CRED_COUNT  +\n                 vs.V5_CAT_06_SALES_COUNT +\n                 vs.V5_CAT_06_CRED_COUNT  +\n                 vs.V6_CAT_07_SALES_COUNT +\n                 vs.V6_CAT_07_CRED_COUNT  +\n                 vs.V6_CAT_08_SALES_COUNT +\n                 vs.V6_CAT_08_CRED_COUNT  +\n                 vs.V6_CAT_09_SALES_COUNT +\n                 vs.V6_CAT_09_CRED_COUNT  +\n                 vs.V6_CAT_10_SALES_COUNT +\n                 vs.V6_CAT_10_CRED_COUNT  +\n                 vs.V6_CAT_11_SALES_COUNT +\n                 vs.V6_CAT_11_CRED_COUNT  +\n                 vs.V6_CAT_12_SALES_COUNT +\n                 vs.V6_CAT_12_CRED_COUNT  +\n                 vs.V7_CAT_13_SALES_COUNT +\n                 vs.V7_CAT_13_CRED_COUNT  +\n                 vs.V7_CAT_14_SALES_COUNT +\n                 vs.V7_CAT_14_CRED_COUNT  +\n                 vs.V7_CAT_15_SALES_COUNT +\n                 vs.V7_CAT_15_CRED_COUNT  +\n                 vs.V7_CAT_16_SALES_COUNT +\n                 vs.V7_CAT_16_CRED_COUNT  +\n                 vs.V7_CAT_17_SALES_COUNT +\n                 vs.V7_CAT_17_CRED_COUNT  +\n                 vs.V7_CAT_18_SALES_COUNT +\n                 vs.V8_CAT_18_CRED_COUNT  +\n                 vs.V8_CAT_19_SALES_COUNT +\n                 vs.V8_CAT_19_CRED_COUNT  +\n                 vs.V8_CAT_20_SALES_COUNT +\n                 vs.V8_CAT_20_CRED_COUNT  +\n                 vs.V8_CAT_21_SALES_COUNT +\n                 vs.V8_CAT_21_CRED_COUNT  +\n                 vs.V8_CAT_22_SALES_COUNT +\n                 vs.V8_CAT_22_CRED_COUNT  +\n                 vs.V8_CAT_23_SALES_COUNT +\n                 vs.V8_CAT_23_CRED_COUNT  +\n                 vs.V9_CAT_24_SALES_COUNT +\n                 vs.V9_CAT_24_CRED_COUNT )          as vs_item_count,\n               mc.m1_income_assessment              as mc_assessment_inc,\n               mc.m2_expense_assessment             as mc_assessment_exp,\n               ( mc.M3_CAT_01_SALES_COUNT +\n                 mc.M3_CAT_01_CRED_COUNT  +\n                 mc.M3_CAT_02_SALES_COUNT +\n                 mc.M3_CAT_02_CRED_COUNT  +\n                 mc.M3_CAT_03_SALES_COUNT +\n                 mc.M3_CAT_03_CRED_COUNT  +\n                 mc.M3_CAT_04_SALES_COUNT +\n                 mc.M3_CAT_04_CRED_COUNT  +\n                 mc.M3_CAT_05_SALES_COUNT +\n                 mc.M3_CAT_05_CRED_COUNT  +\n                 mc.M3_CAT_06_SALES_COUNT +\n                 mc.M3_CAT_06_CRED_COUNT  +\n                 mc.M4_CAT_07_SALES_COUNT +\n                 mc.M4_CAT_07_CRED_COUNT  +\n                 mc.M4_CAT_08_SALES_COUNT +\n                 mc.M4_CAT_08_CRED_COUNT  +\n                 mc.M4_CAT_09_SALES_COUNT +\n                 mc.M4_CAT_09_CRED_COUNT  +\n                 mc.M4_CAT_10_SALES_COUNT +\n                 mc.M4_CAT_10_CRED_COUNT  +\n                 mc.M4_CAT_11_SALES_COUNT +\n                 mc.M4_CAT_11_CRED_COUNT  +\n                 mc.M4_CAT_12_SALES_COUNT +\n                 mc.M4_CAT_12_CRED_COUNT  )         as mc_item_count,\n               vs.V5_CAT_01_SALES_COUNT                                             as vs_sales_count_01,\n               vs.V5_CAT_01_SALES_AMOUNT                                            as vs_sales_amount_01,\n               vs.V5_CAT_01_CRED_COUNT                                              as vs_credits_count_01,\n               vs.V5_CAT_01_CRED_AMOUNT                                             as vs_credits_amount_01,\n               round( (vs.V5_CAT_01_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_01,\n               round( (vs.V5_CAT_01_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_01,\n               (vs.V1_INC_CAT_01_SALES + vs.V1_INC_CAT_01_FEES)                     as vs_surc_01,\n               vs.V5_CAT_01_INTCH                                                   as vs_ic_01,\n               vs.V5_CAT_02_SALES_COUNT                                             as vs_sales_count_02,\n               vs.V5_CAT_02_SALES_AMOUNT                                            as vs_sales_amount_02,\n               vs.V5_CAT_02_CRED_COUNT                                              as vs_credits_count_02,\n               vs.V5_CAT_02_CRED_AMOUNT                                             as vs_credits_amount_02,\n               round( (vs.V5_CAT_02_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_02,\n               round( (vs.V5_CAT_02_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_02,\n               (vs.V1_INC_CAT_02_SALES + vs.V1_INC_CAT_02_FEES)                     as vs_surc_02,\n               vs.V5_CAT_02_INTCH                                                   as vs_ic_02,\n               vs.V5_CAT_03_SALES_COUNT                                             as vs_sales_count_03,\n               vs.V5_CAT_03_SALES_AMOUNT                                            as vs_sales_amount_03,\n               vs.V5_CAT_03_CRED_COUNT                                              as vs_credits_count_03,\n               vs.V5_CAT_03_CRED_AMOUNT                                             as vs_credits_amount_03,\n               round( (vs.V5_CAT_03_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_03,\n               round( (vs.V5_CAT_03_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_03,\n               (vs.V1_INC_CAT_03_SALES + vs.V1_INC_CAT_03_FEES)                     as vs_surc_03,\n               vs.V5_CAT_03_INTCH                                                   as vs_ic_03,\n               vs.V5_CAT_04_SALES_COUNT                                             as vs_sales_count_04,\n               vs.V5_CAT_04_SALES_AMOUNT                                            as vs_sales_amount_04,\n               vs.V5_CAT_04_CRED_COUNT                                              as vs_credits_count_04,\n               vs.V5_CAT_04_CRED_AMOUNT                                             as vs_credits_amount_04,\n               round( (vs.V5_CAT_04_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_04,\n               round( (vs.V5_CAT_04_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_04,\n               (vs.V1_INC_CAT_04_SALES + vs.V1_INC_CAT_04_FEES)                     as vs_surc_04,\n               vs.V5_CAT_04_INTCH                                                   as vs_ic_04,\n               vs.V5_CAT_05_SALES_COUNT                                             as vs_sales_count_05,\n               vs.V5_CAT_05_SALES_AMOUNT                                            as vs_sales_amount_05,\n               vs.V5_CAT_05_CRED_COUNT                                              as vs_credits_count_05,\n               vs.V5_CAT_05_CRED_AMOUNT                                             as vs_credits_amount_05,\n               round( (vs.V5_CAT_05_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_05,\n               round( (vs.V5_CAT_05_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_05,\n               (vs.V1_INC_CAT_05_SALES + vs.V1_INC_CAT_05_FEES)                     as vs_surc_05,\n               vs.V5_CAT_05_INTCH                                                   as vs_ic_05,\n               vs.V5_CAT_06_SALES_COUNT                                             as vs_sales_count_06,\n               vs.V5_CAT_06_SALES_AMOUNT                                            as vs_sales_amount_06,\n               vs.V5_CAT_06_CRED_COUNT                                              as vs_credits_count_06,\n               vs.V5_CAT_06_CRED_AMOUNT                                             as vs_credits_amount_06,\n               round( (vs.V5_CAT_06_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_06,\n               round( (vs.V5_CAT_06_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_06,\n               (vs.V1_INC_CAT_06_SALES + vs.V1_INC_CAT_06_FEES)                     as vs_surc_06,\n               vs.V6_CAT_06_INTCH                                                   as vs_ic_06,\n               vs.V6_CAT_07_SALES_COUNT                                             as vs_sales_count_07,\n               vs.V6_CAT_07_SALES_AMOUNT                                            as vs_sales_amount_07,\n               vs.V6_CAT_07_CRED_COUNT                                              as vs_credits_count_07,\n               vs.V6_CAT_07_CRED_AMOUNT                                             as vs_credits_amount_07,\n               round( (vs.V6_CAT_07_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_07,\n               round( (vs.V6_CAT_07_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_07,\n               (vs.V1_INC_CAT_07_SALES + vs.V1_INC_CAT_07_FEES)                     as vs_surc_07,\n               vs.V6_CAT_07_INTCH                                                   as vs_ic_07,\n               vs.V6_CAT_08_SALES_COUNT                                             as vs_sales_count_08,\n               vs.V6_CAT_08_SALES_AMOUNT                                            as vs_sales_amount_08,\n               vs.V6_CAT_08_CRED_COUNT                                              as vs_credits_count_08,\n               vs.V6_CAT_08_CRED_AMOUNT                                             as vs_credits_amount_08,\n               round( (vs.V6_CAT_08_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_08,\n               round( (vs.V6_CAT_08_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_08,\n               (vs.V1_INC_CAT_08_SALES + vs.V1_INC_CAT_08_FEES)                     as vs_surc_08,\n               vs.V6_CAT_08_INTCH                                                   as vs_ic_08,\n               vs.V6_CAT_09_SALES_COUNT                                             as vs_sales_count_09,\n               vs.V6_CAT_09_SALES_AMOUNT                                            as vs_sales_amount_09,\n               vs.V6_CAT_09_CRED_COUNT                                              as vs_credits_count_09,\n               vs.V6_CAT_09_CRED_AMOUNT                                             as vs_credits_amount_09,\n               round( (vs.V6_CAT_09_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_09,\n               round( (vs.V6_CAT_09_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_09,\n               (vs.V1_INC_CAT_09_SALES + vs.V1_INC_CAT_09_FEES)                     as vs_surc_09,\n               vs.V6_CAT_09_INTCH                                                   as vs_ic_09,\n               vs.V6_CAT_10_SALES_COUNT                                             as vs_sales_count_10,\n               vs.V6_CAT_10_SALES_AMOUNT                                            as vs_sales_amount_10,\n               vs.V6_CAT_10_CRED_COUNT                                              as vs_credits_count_10,\n               vs.V6_CAT_10_CRED_AMOUNT                                             as vs_credits_amount_10,\n               round( (vs.V6_CAT_10_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_10,\n               round( (vs.V6_CAT_10_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_10,\n               (vs.V1_INC_CAT_10_SALES + vs.V1_INC_CAT_10_FEES)                     as vs_surc_10,\n               vs.V6_CAT_10_INTCH                                                   as vs_ic_10,\n               vs.V6_CAT_11_SALES_COUNT                                             as vs_sales_count_11,\n               vs.V6_CAT_11_SALES_AMOUNT                                            as vs_sales_amount_11,\n               vs.V6_CAT_11_CRED_COUNT                                              as vs_credits_count_11,\n               vs.V6_CAT_11_CRED_AMOUNT                                             as vs_credits_amount_11,\n               round( (vs.V6_CAT_11_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_11,\n               round( (vs.V6_CAT_11_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_11,\n               (vs.V1_INC_CAT_11_SALES + vs.V1_INC_CAT_11_FEES)                     as vs_surc_11,\n               vs.V6_CAT_11_INTCH                                                   as vs_ic_11,\n               vs.V6_CAT_12_SALES_COUNT                                             as vs_sales_count_12,\n               vs.V6_CAT_12_SALES_AMOUNT                                            as vs_sales_amount_12,\n               vs.V6_CAT_12_CRED_COUNT                                              as vs_credits_count_12,\n               vs.V7_CAT_12_CRED_AMOUNT                                             as vs_credits_amount_12,\n               round( (vs.V6_CAT_12_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_12,\n               round( (vs.V6_CAT_12_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_12,\n               (vs.V1_INC_CAT_12_SALES + vs.V1_INC_CAT_12_FEES)                     as vs_surc_12,\n               vs.V7_CAT_12_INTCH                                                   as vs_ic_12,\n               vs.V7_CAT_13_SALES_COUNT                                             as vs_sales_count_13,\n               vs.V7_CAT_13_SALES_AMOUNT                                            as vs_sales_amount_13,\n               vs.V7_CAT_13_CRED_COUNT                                              as vs_credits_count_13,\n               vs.V7_CAT_13_CRED_AMOUNT                                             as vs_credits_amount_13,\n               round( (vs.V7_CAT_13_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_13,\n               round( (vs.V7_CAT_13_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_13,\n               (vs.V1_INC_CAT_13_SALES + vs.V1_INC_CAT_13_FEES)                     as vs_surc_13,\n               vs.V7_CAT_13_INTCH                                                   as vs_ic_13,\n               vs.V7_CAT_14_SALES_COUNT                                             as vs_sales_count_14,\n               vs.V7_CAT_14_SALES_AMOUNT                                            as vs_sales_amount_14,\n               vs.V7_CAT_14_CRED_COUNT                                              as vs_credits_count_14,\n               vs.V7_CAT_14_CRED_AMOUNT                                             as vs_credits_amount_14,\n               round( (vs.V7_CAT_14_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_14,\n               round( (vs.V7_CAT_14_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_14,\n               (vs.V1_INC_CAT_14_SALES + vs.V1_INC_CAT_14_FEES)                     as vs_surc_14,\n               vs.V7_CAT_14_INTCH                                                   as vs_ic_14,\n               vs.V7_CAT_15_SALES_COUNT                                             as vs_sales_count_15,\n               vs.V7_CAT_15_SALES_AMOUNT                                            as vs_sales_amount_15,\n               vs.V7_CAT_15_CRED_COUNT                                              as vs_credits_count_15,\n               vs.V7_CAT_15_CRED_AMOUNT                                             as vs_credits_amount_15,\n               round( (vs.V7_CAT_15_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_15,\n               round( (vs.V7_CAT_15_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_15,\n               (vs.V1_INC_CAT_15_SALES + vs.V1_INC_CAT_15_FEES)                     as vs_surc_15,\n               vs.V7_CAT_15_INTCH                                                   as vs_ic_15,\n               vs.V7_CAT_16_SALES_COUNT                                             as vs_sales_count_16,\n               vs.V7_CAT_16_SALES_AMOUNT                                            as vs_sales_amount_16,\n               vs.V7_CAT_16_CRED_COUNT                                              as vs_credits_count_16,\n               vs.V7_CAT_16_CRED_AMOUNT                                             as vs_credits_amount_16,\n               round( (vs.V7_CAT_16_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_16,\n               round( (vs.V7_CAT_16_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_16,\n               (vs.V1_INC_CAT_16_SALES + vs.V1_INC_CAT_16_FEES)                     as vs_surc_16,\n               vs.V7_CAT_16_INTCH                                                   as vs_ic_16,\n               vs.V7_CAT_17_SALES_COUNT                                             as vs_sales_count_17,\n               vs.V7_CAT_17_SALES_AMOUNT                                            as vs_sales_amount_17,\n               vs.V7_CAT_17_CRED_COUNT                                              as vs_credits_count_17,\n               vs.V7_CAT_17_CRED_AMOUNT                                             as vs_credits_amount_17,\n               round( (vs.V7_CAT_17_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_17,\n               round( (vs.V7_CAT_17_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_17,\n               (vs.V1_INC_CAT_17_SALES + vs.V2_INC_CAT_17_FEES)                     as vs_surc_17,\n               vs.V7_CAT_17_INTCH                                                   as vs_ic_17,\n               vs.V7_CAT_18_SALES_COUNT                                             as vs_sales_count_18,\n               vs.V8_CAT_18_SALES_AMOUNT                                            as vs_sales_amount_18,\n               vs.V8_CAT_18_CRED_COUNT                                              as vs_credits_count_18,\n               vs.V8_CAT_18_CRED_AMOUNT                                             as vs_credits_amount_18,\n               round( (vs.V8_CAT_18_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_18,\n               round( (vs.V7_CAT_18_SALES_COUNT  * vs_pl.pl_disc_rate_per_item),2 ) as vs_calc_per_item_18,\n               (vs.V2_INC_CAT_18_SALES + vs.V2_INC_CAT_18_FEES)                     as vs_surc_18,\n               vs.V8_CAT_18_INTCH                                                   as vs_ic_18,\n               vs.V8_CAT_19_SALES_COUNT                                             as vs_sales_count_19,\n               vs.V8_CAT_19_SALES_AMOUNT                                            as vs_sales_amount_19,\n               vs.V8_CAT_19_CRED_COUNT                                              as vs_credits_count_19,\n               vs.V8_CAT_19_CRED_AMOUNT                                             as vs_credits_amount_19,\n               round( (vs.V8_CAT_19_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_19,\n               round( (vs.V8_CAT_19_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_19,\n               (vs.V2_INC_CAT_19_SALES + vs.V2_INC_CAT_19_FEES)                     as vs_surc_19,\n               vs.V8_CAT_19_INTCH                                                   as vs_ic_19,\n               vs.V8_CAT_20_SALES_COUNT                                             as vs_sales_count_20,\n               vs.V8_CAT_20_SALES_AMOUNT                                            as vs_sales_amount_20,\n               vs.V8_CAT_20_CRED_COUNT                                              as vs_credits_count_20,\n               vs.V8_CAT_20_CRED_AMOUNT                                             as vs_credits_amount_20,\n               round( (vs.V8_CAT_20_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_20,\n               round( (vs.V8_CAT_20_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_20,\n               (vs.V2_INC_CAT_20_SALES + vs.V2_INC_CAT_20_FEES)                     as vs_surc_20,\n               vs.V8_CAT_20_INTCH                                                   as vs_ic_20,\n               vs.V8_CAT_21_SALES_COUNT                                             as vs_sales_count_21,\n               vs.V8_CAT_21_SALES_AMOUNT                                            as vs_sales_amount_21,\n               vs.V8_CAT_21_CRED_COUNT                                              as vs_credits_count_21,\n               vs.V8_CAT_21_CRED_AMOUNT                                             as vs_credits_amount_21,\n               round( (vs.V8_CAT_21_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_21,\n               round( (vs.V8_CAT_21_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_21,\n               (vs.V2_INC_CAT_21_SALES + vs.V2_INC_CAT_21_FEES)                     as vs_surc_21,\n               vs.V8_CAT_21_INTCH                                                   as vs_ic_21,\n               vs.V8_CAT_22_SALES_COUNT                                             as vs_sales_count_22,\n               vs.V8_CAT_22_SALES_AMOUNT                                            as vs_sales_amount_22,\n               vs.V8_CAT_22_CRED_COUNT                                              as vs_credits_count_22,\n               vs.V8_CAT_22_CRED_AMOUNT                                             as vs_credits_amount_22,\n               round( (vs.V8_CAT_22_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_22,\n               round( (vs.V8_CAT_22_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_22,\n               (vs.V2_INC_CAT_22_SALES + vs.V2_INC_CAT_22_FEES)                     as vs_surc_22,\n               vs.V8_CAT_22_INTCH                                                   as vs_ic_22,\n               vs.V8_CAT_23_SALES_COUNT                                             as vs_sales_count_23,\n               vs.V8_CAT_23_SALES_AMOUNT                                            as vs_sales_amount_23,\n               vs.V8_CAT_23_CRED_COUNT                                              as vs_credits_count_23,\n               vs.V8_CAT_23_CRED_AMOUNT                                             as vs_credits_amount_23,\n               round( (vs.V8_CAT_23_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_23,\n               round( (vs.V8_CAT_23_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_23,\n               (vs.V2_INC_CAT_23_SALES + vs.V2_INC_CAT_23_FEES)                     as vs_surc_23,\n               vs.V9_CAT_23_INTCH                                                   as vs_ic_23,\n               vs.V9_CAT_24_SALES_COUNT                                             as vs_sales_count_24,\n               vs.V9_CAT_24_SALES_AMOUNT                                            as vs_sales_amount_24,\n               vs.V9_CAT_24_CRED_COUNT                                              as vs_credits_count_24,\n               vs.V9_CAT_24_CRED_AMOUNT                                             as vs_credits_amount_24,\n               round( (vs.V9_CAT_24_SALES_AMOUNT * vs_pl.PL_DISC_RATE * 0.01),2 )   as vs_calc_disc_24,\n               round( (vs.V9_CAT_24_SALES_COUNT  * vs_pl.PL_DISC_RATE_PER_ITEM),2 ) as vs_calc_per_item_24,\n               (vs.V2_INC_CAT_24_SALES + vs.V2_INC_CAT_24_FEES)                     as vs_surc_24,\n               vs.V9_CAT_24_INTCH                                                   as vs_ic_24,\n               /* these empty columns added to simplify processing algorithm below */               \n               0                                                                    as vs_sales_count_25,\n               0.0                                                                  as vs_sales_amount_25,\n               0                                                                    as vs_credits_count_25,\n               0.0                                                                  as vs_credits_amount_25,\n               0.0                                                                  as vs_calc_disc_25,\n               0.0                                                                  as vs_calc_per_item_25,\n               vs.V2_INC_CAT_25_FEES                                                as vs_surc_25,\n               vs.V9_CAT_25_INTCH                                                   as vs_ic_25,\n               mc.M3_CAT_01_SALES_COUNT                                             as mc_sales_count_01,\n               mc.M3_CAT_01_SALES_AMOUNT                                            as mc_sales_amount_01,\n               mc.M3_CAT_01_CRED_COUNT                                              as mc_credits_count_01,\n               mc.M3_CAT_01_CRED_AMOUNT                                             as mc_credits_amount_01,\n               round( (mc.M3_CAT_01_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_01,\n               round( (mc.M3_CAT_01_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_01,\n               (mc.M1_INC_CAT_01_SALES + mc.M1_INC_CAT_01_FEES)                     as mc_surc_01,\n               mc.M3_CAT_01_INTCH                                                   as mc_ic_01,\n               mc.M3_CAT_02_SALES_COUNT                                             as mc_sales_count_02,\n               mc.M3_CAT_02_SALES_AMOUNT                                            as mc_sales_amount_02,\n               mc.M3_CAT_02_CRED_COUNT                                              as mc_credits_count_02,\n               mc.M3_CAT_02_CRED_AMOUNT                                             as mc_credits_amount_02,\n               round( (mc.M3_CAT_02_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_02,\n               round( (mc.M3_CAT_02_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_02,\n               (mc.M1_INC_CAT_02_SALES + mc.M1_INC_CAT_02_FEES)                     as mc_surc_02,\n               mc.M3_CAT_02_INTCH                                                   as mc_ic_02,\n               mc.M3_CAT_03_SALES_COUNT                                             as mc_sales_count_03,\n               mc.M3_CAT_03_SALES_AMOUNT                                            as mc_sales_amount_03,\n               mc.M3_CAT_03_CRED_COUNT                                              as mc_credits_count_03,\n               mc.M3_CAT_03_CRED_AMOUNT                                             as mc_credits_amount_03,\n               round( (mc.M3_CAT_03_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_03,\n               round( (mc.M3_CAT_03_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_03,\n               (mc.M1_INC_CAT_03_SALES + mc.M1_INC_CAT_03_FEES)                     as mc_surc_03,\n               mc.M3_CAT_03_INTCH                                                   as mc_ic_03,\n               mc.M3_CAT_04_SALES_COUNT                                             as mc_sales_count_04,\n               mc.M3_CAT_04_SALES_AMOUNT                                            as mc_sales_amount_04,\n               mc.M3_CAT_04_CRED_COUNT                                              as mc_credits_count_04,\n               mc.M3_CAT_04_CRED_AMOUNT                                             as mc_credits_amount_04,\n               round( (mc.M3_CAT_04_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_04,\n               round( (mc.M3_CAT_04_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_04,\n               (mc.M1_INC_CAT_04_SALES + mc.M1_INC_CAT_04_FEES)                     as mc_surc_04,\n               mc.M3_CAT_04_INTCH                                                   as mc_ic_04,\n               mc.M3_CAT_05_SALES_COUNT                                             as mc_sales_count_05,\n               mc.M3_CAT_05_SALES_AMOUNT                                            as mc_sales_amount_05,\n               mc.M3_CAT_05_CRED_COUNT                                              as mc_credits_count_05,\n               mc.M3_CAT_05_CRED_AMOUNT                                             as mc_credits_amount_05,\n               round( (mc.M3_CAT_05_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_05,\n               round( (mc.M3_CAT_05_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_05,\n               (mc.M1_INC_CAT_05_SALES + mc.M1_INC_CAT_05_FEES)                     as mc_surc_05,\n               mc.M3_CAT_05_INTCH                                                   as mc_ic_05,\n               mc.M3_CAT_06_SALES_COUNT                                             as mc_sales_count_06,\n               mc.M3_CAT_06_SALES_AMOUNT                                            as mc_sales_amount_06,\n               mc.M3_CAT_06_CRED_COUNT                                              as mc_credits_count_06,\n               mc.M3_CAT_06_CRED_AMOUNT                                             as mc_credits_amount_06,\n               round( (mc.M3_CAT_06_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_06,\n               round( (mc.M3_CAT_06_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_06,\n               (mc.M1_INC_CAT_06_SALES + mc.M1_INC_CAT_06_FEES)                     as mc_surc_06,\n               mc.M4_CAT_06_INTCH                                                   as mc_ic_06,\n               mc.M4_CAT_07_SALES_COUNT                                             as mc_sales_count_07,\n               mc.M4_CAT_07_SALES_AMOUNT                                            as mc_sales_amount_07,\n               mc.M4_CAT_07_CRED_COUNT                                              as mc_credits_count_07,\n               mc.M4_CAT_07_CRED_AMOUNT                                             as mc_credits_amount_07,\n               round( (mc.M4_CAT_07_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_07,\n               round( (mc.M4_CAT_07_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_07,\n               (mc.M1_INC_CAT_07_SALES + mc.M1_INC_CAT_07_FEES)                     as mc_surc_07,\n               mc.M4_CAT_07_INTCH                                                   as mc_ic_07,\n               mc.M4_CAT_08_SALES_COUNT                                             as mc_sales_count_08,\n               mc.M4_CAT_08_SALES_AMOUNT                                            as mc_sales_amount_08,\n               mc.M4_CAT_08_CRED_COUNT                                              as mc_credits_count_08,\n               mc.M4_CAT_08_CRED_AMOUNT                                             as mc_credits_amount_08,\n               round( (mc.M4_CAT_08_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_08,\n               round( (mc.M4_CAT_08_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_08,\n               (mc.M1_INC_CAT_08_SALES + mc.M1_INC_CAT_08_FEES)                     as mc_surc_08,\n               mc.M4_CAT_08_INTCH                                                   as mc_ic_08,\n               mc.M4_CAT_09_SALES_COUNT                                             as mc_sales_count_09,\n               mc.M4_CAT_09_SALES_AMOUNT                                            as mc_sales_amount_09,\n               mc.M4_CAT_09_CRED_COUNT                                              as mc_credits_count_09,\n               mc.M4_CAT_09_CRED_AMOUNT                                ");
   __sjT_sb.append("             as mc_credits_amount_09,\n               round( (mc.M4_CAT_09_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_09,\n               round( (mc.M4_CAT_09_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_09,\n               (mc.M1_INC_CAT_09_SALES + mc.M1_INC_CAT_09_FEES)                     as mc_surc_09,\n               mc.M4_CAT_09_INTCH                                                   as mc_ic_09,\n               mc.M4_CAT_10_SALES_COUNT                                             as mc_sales_count_10,\n               mc.M4_CAT_10_SALES_AMOUNT                                            as mc_sales_amount_10,\n               mc.M4_CAT_10_CRED_COUNT                                              as mc_credits_count_10,\n               mc.M4_CAT_10_CRED_AMOUNT                                             as mc_credits_amount_10,\n               round( (mc.M4_CAT_10_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_10,\n               round( (mc.M4_CAT_10_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_10,\n               (mc.M1_INC_CAT_10_SALES + mc.M1_INC_CAT_10_FEES)                     as mc_surc_10,\n               mc.M4_CAT_10_INTCH                                                   as mc_ic_10,\n               mc.M4_CAT_11_SALES_COUNT                                             as mc_sales_count_11,\n               mc.M4_CAT_11_SALES_AMOUNT                                            as mc_sales_amount_11,\n               mc.M4_CAT_11_CRED_COUNT                                              as mc_credits_count_11,\n               mc.M4_CAT_11_CRED_AMOUNT                                             as mc_credits_amount_11,\n               round( (mc.M4_CAT_11_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2 )   as mc_calc_disc_11,\n               round( (mc.M4_CAT_11_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2 ) as mc_calc_per_item_11,\n               (mc.M1_INC_CAT_11_SALES + mc.M1_INC_CAT_11_FEES)                     as mc_surc_11,\n               mc.M4_CAT_11_INTCH                                                   as mc_ic_11,\n               mc.M4_CAT_12_SALES_COUNT                                             as mc_sales_count_12,\n               mc.M4_CAT_12_SALES_AMOUNT                                            as mc_sales_amount_12,\n               mc.M4_CAT_12_CRED_COUNT                                              as mc_credits_count_12,\n               mc.M5_CAT_12_CRED_AMOUNT                                             as mc_credits_amount_12,\n               round( (mc.M4_CAT_12_SALES_AMOUNT * mc_pl.PL_DISC_RATE * 0.01),2)    as mc_calc_disc_12,\n               round( (mc.M4_CAT_12_SALES_COUNT  * mc_pl.PL_DISC_RATE_PER_ITEM),2)  as mc_calc_per_item_12,\n               (mc.M1_INC_CAT_12_SALES + mc.M1_INC_CAT_12_FEES)                     as mc_surc_12,\n               mc.M5_CAT_12_INTCH                                                   as mc_ic_12\n        from  organization             o,\n              group_merchant           gm,\n              mif                      mf,\n              merchant                 mr,\n              application              app,\n              sales_rep                sr,\n              monthly_extract_gn       gn,\n              monthly_extract_pl       mc_pl,\n              monthly_extract_pl       vs_pl,\n              monthly_extract_visa     vs,\n              monthly_extract_mc       mc\n        where o.org_group in \n              (\n                select   :1 \n                from    dual\n                union\n                select   :2 \n                from    dual\n                union \n                select hierarchy_node \n                from   transcom_extract_legacy_nodes\n              ) and              \n              gm.org_num = o.org_num and\n              mf.merchant_number = gm.merchant_number and\n              mr.merch_number(+) = mf.merchant_number and\n              app.app_seq_num(+) = mr.app_seq_num and\n              sr.user_id(+) = app.app_user_id and\n              (\n                o.org_group =  :3  or\n                nvl(app.app_type,-1) = 28 or -- mes new app\n                (\n                  -- old direct mes app that past the 13 month audit cycle\n                  is_direct_commission_plan(sr.rep_plan_id) = 1 and   \n                  months_between((last_day( :4 )+1),mf.activation_date) > 13\n                ) or\n                exists   -- in legacy table\n                (\n                  select  gmi.merchant_number\n                  from    transcom_extract_legacy_nodes  ln,\n                          organization                   oi,\n                          group_merchant                 gmi                            \n                  where   oi.org_group = ln.hierarchy_node and\n                          gmi.org_num = oi.org_num and\n                          gmi.merchant_number = mf.merchant_number\n                )\n              ) and \n              gn.hh_merchant_number = mf.merchant_number and\n              gn.hh_active_date =  :5  and\n              mc_pl.hh_load_sec(+)  = gn.hh_load_sec and\n              mc_pl.pl_plan_type(+) = 'MC' and\n              vs_pl.hh_load_sec(+)  = gn.hh_load_sec and\n              vs_pl.pl_plan_type(+) = 'VISA' and\n              vs.hh_load_sec(+)  = gn.hh_load_sec and\n              mc.hh_load_sec(+)  = gn.hh_load_sec");
  try {
   String theSqlTS = __sjT_sb.toString();
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,directNode);
   __sJT_st.setLong(2,topNode);
   __sJT_st.setLong(3,topNode);
   __sJT_st.setDate(4,activeDate);
   __sJT_st.setDate(5,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"25com.mes.startup.TranscomExtractSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2000^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        loadSec           = resultSet.getLong("hh_load_sec");
        merchantId        = resultSet.getLong("merchant_number");
        collectedDiscount = resultSet.getDouble("total_disc_inc");
        totalDiscount = 0.0;
        
        /*@lineinfo:generated-code*//*@lineinfo:2010^9*/

//  ************************************************************
//  #sql [Ctx] { select  sum(pl.PL_CORRECT_DISC_AMT)     
//            from    monthly_extract_pl pl
//            where   pl.hh_load_sec = :loadSec
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum(pl.PL_CORRECT_DISC_AMT)      \n          from    monthly_extract_pl pl\n          where   pl.hh_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.startup.TranscomExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   calculatedDiscount = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2015^9*/
      
        for( int ct = 0; ct < CT_COUNT; ++ct )
        {
          switch( ct )
          {
            case CT_VS:
              cardTypeString  = "VS";
              catCount        = VISA_CAT_COUNT;
              break;
              
            case CT_MC:
              cardTypeString  = "MC";
              catCount        = MC_CAT_COUNT;
              break;
              
            default:
              continue;
          }
          
          // first do the visa cards
//@          assessmentIncRate  = resultSet.getDouble( cardTypeString + "_assessment_inc_per_item" );
//@          assessmentExpRate  = resultSet.getDouble( cardTypeString + "_assessment_per_item" );

          // prevent divide by zero errors
          itemCount = resultSet.getInt( cardTypeString + "_item_count" );
          if ( itemCount == 0 )
          {
            itemCount = 1;
          }
          assessmentIncRate  = ( resultSet.getDouble( cardTypeString + "_assessment_inc" ) / itemCount );
          assessmentExpRate  = ( resultSet.getDouble( cardTypeString + "_assessment_exp" ) / itemCount );
          

          for( int cat = 1; cat <= catCount; ++cat )
          {
            catString     = catFormat.format(cat);
            salesCount    = resultSet.getInt   ( cardTypeString + "_sales_count_" + catString );
            salesAmount   = resultSet.getDouble( cardTypeString + "_sales_amount_" + catString );
            creditsCount  = resultSet.getInt   ( cardTypeString + "_credits_count_" + catString );
            creditsAmount = resultSet.getDouble( cardTypeString + "_credits_amount_" + catString );
            surchargeInc  = resultSet.getDouble( cardTypeString + "_surc_" + catString);
            icExp         = resultSet.getDouble( cardTypeString + "_ic_" + catString);
            assessmentExp = (assessmentExpRate * (salesCount + creditsCount));
            assessmentInc = (assessmentIncRate * (salesCount + creditsCount));
            
            // if minimum discount was *not* paid then add the discount for
            // every row in the transcom summary table, otherwise there
            // will be a single row for the minimum discount charged
            if ( calculatedDiscount == collectedDiscount )
            {
              discRateInc     = resultSet.getDouble( cardTypeString + "_calc_disc_" + catString);
              discPerItemInc  = resultSet.getDouble( cardTypeString + "_calc_per_item_" + catString);
            }              
            else
            {
              discRateInc     = 0.0;
              discPerItemInc  = 0.0;
            }
            totalDiscount += (discRateInc + discPerItemInc);
            
            debug = "inserting ic data";//@
            if( (salesCount + creditsCount) > 0 )
            {
              /*@lineinfo:generated-code*//*@lineinfo:2079^15*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_extract_summary
//                    ( merchant_number,
//                      active_date,
//                      card_type,
//                      charge_type,
//                      sales_vol_count,
//                      sales_vol_amount,
//                      credits_vol_count,
//                      credits_vol_amount,
//                      disc_rate_revenue,
//                      disc_per_item_revenue,
//                      interchange_revenue,
//                      surcharge_revenue,
//                      surcharge_per_item_revenue,
//                      assessment_expense,
//                      interchange_expense,
//                      item_fee,
//                      item_count,
//                      equip_revenue,
//                      ticket_count,
//                      batch_count,
//                      auth_count,
//                      auth_per_item,
//                      misc_revenue,
//                      mtd_inc,
//                      description,
//                      load_filename
//                    )
//                  values
//                    ( :merchantId,
//                      :activeDate,
//                      :getTranscomIcCardType(cardTypeString,cat),
//                      :getTranscomIcChargeType(cardTypeString,cat),
//                      :salesCount,
//                      :salesAmount,
//                      :creditsCount,
//                      :creditsAmount,
//                      :discRateInc,
//                      :discPerItemInc,
//                      0.0,    -- interchange revenue
//                      round(:surchargeInc + assessmentInc,2),
//                      0.0,    -- surcharge per item (not supported)
//                      round( :assessmentExp, 2 ),
//                      :icExp,
//                      0,      -- item fee (?)
//                      0,      -- item count
//                      0,      -- equip revenue
//                      0,      -- ticket count
//                      0,      -- batch count
//                      0,      -- auth count
//                      0,      -- auth per item
//                      0,      -- misc revenue
//                      round( :(discRateInc + discPerItemInc + surchargeInc) - 
//                               (icExp + assessmentExp), 2 ),     -- mtd total
//                      :getIcCatDesc(cardTypeString,cat),  
//                      :loadFilename
//                    )    
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4858 = getTranscomIcCardType(cardTypeString,cat);
 String __sJT_4859 = getTranscomIcChargeType(cardTypeString,cat);
 double __sJT_4860 = surchargeInc + assessmentInc;
 double __sJT_4861 = (discRateInc + discPerItemInc + surchargeInc) - 
                             (icExp + assessmentExp);
 String __sJT_4862 = getIcCatDesc(cardTypeString,cat);
   String theSqlTS = "insert into transcom_extract_summary\n                  ( merchant_number,\n                    active_date,\n                    card_type,\n                    charge_type,\n                    sales_vol_count,\n                    sales_vol_amount,\n                    credits_vol_count,\n                    credits_vol_amount,\n                    disc_rate_revenue,\n                    disc_per_item_revenue,\n                    interchange_revenue,\n                    surcharge_revenue,\n                    surcharge_per_item_revenue,\n                    assessment_expense,\n                    interchange_expense,\n                    item_fee,\n                    item_count,\n                    equip_revenue,\n                    ticket_count,\n                    batch_count,\n                    auth_count,\n                    auth_per_item,\n                    misc_revenue,\n                    mtd_inc,\n                    description,\n                    load_filename\n                  )\n                values\n                  (  :1 ,\n                     :2 ,\n                     :3 ,\n                     :4 ,\n                     :5 ,\n                     :6 ,\n                     :7 ,\n                     :8 ,\n                     :9 ,\n                     :10 ,\n                    0.0,    -- interchange revenue\n                    round( :11 ,2),\n                    0.0,    -- surcharge per item (not supported)\n                    round(  :12 , 2 ),\n                     :13 ,\n                    0,      -- item fee (:14)\n                    0,      -- item count\n                    0,      -- equip revenue\n                    0,      -- ticket count\n                    0,      -- batch count\n                    0,      -- auth count\n                    0,      -- auth per item\n                    0,      -- misc revenue\n                    round(  :15 , 2 ),     -- mtd total\n                     :16 ,  \n                     :17 \n                  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setString(3,__sJT_4858);
   __sJT_st.setString(4,__sJT_4859);
   __sJT_st.setInt(5,salesCount);
   __sJT_st.setDouble(6,salesAmount);
   __sJT_st.setInt(7,creditsCount);
   __sJT_st.setDouble(8,creditsAmount);
   __sJT_st.setDouble(9,discRateInc);
   __sJT_st.setDouble(10,discPerItemInc);
   __sJT_st.setDouble(11,__sJT_4860);
   __sJT_st.setDouble(12,assessmentExp);
   __sJT_st.setDouble(13,icExp);
   __sJT_st.setDouble(14,__sJT_4861);
   __sJT_st.setString(15,__sJT_4862);
   __sJT_st.setString(16,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2138^15*/
            }   // end if ((salesCount+creditsCount)>0)              
          }   // end for( int cat = 1; cat <= catCount; ++cat )
        } // end for( int ct = 1; ct <= CT_COUNT; ++ct )
        
        // if the amount collected from the merchant does 
        // not match the amount we have put in the extract
        // at this point, then make an adjustment record
        // for the differene.
        if ( collectedDiscount != totalDiscount )
        {
          debug = "inserting min discount";//@
            
          // if the calculated discount is the same as the 
          // collected discount then the adjustment is done
          // to correct for rounding errors introduced by
          // doing the calculation by IC bucket instead of PL bucket.
          if ( calculatedDiscount == collectedDiscount )
          {
            cardTypeString    = "MIN";
            chargeTypeString  = "ADJ";
            desc              = "Discount Adjustment";
          }
          else    // the merchant paid minimum discount
          {
            cardTypeString    = "90";
            chargeTypeString  = "6049";
            desc              = "Minimum Discount";
          }
          /*@lineinfo:generated-code*//*@lineinfo:2167^11*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_extract_summary
//                ( merchant_number,
//                  active_date,
//                  card_type,
//                  charge_type,
//                  sales_vol_count,
//                  sales_vol_amount,
//                  credits_vol_count,
//                  credits_vol_amount,
//                  disc_rate_revenue,
//                  disc_per_item_revenue,
//                  interchange_revenue,
//                  surcharge_revenue,
//                  surcharge_per_item_revenue,
//                  assessment_expense,
//                  interchange_expense,
//                  item_fee,
//                  item_count,
//                  equip_revenue,
//                  ticket_count,
//                  batch_count,
//                  auth_count,
//                  auth_per_item,
//                  misc_revenue,
//                  mtd_inc,
//                  description,
//                  load_filename
//                )
//              values
//                ( :merchantId,
//                  :activeDate,
//                  :cardTypeString,   
//                  :chargeTypeString, 
//                  0,              -- sales count
//                  0,              -- sales amount
//                  0,              -- credits count
//                  0,              -- credits amount
//                  0,              -- discount amount
//                  0.0,            -- per item revenue
//                  0.0,            -- interchange revenue
//                  0.0,            -- surcharge revenue
//                  0.0,            -- surcharge per item (not supported)
//                  0.0,            -- assessment expense
//                  0.0,            -- interchange expense
//                  0,              -- item fee (?)
//                  0,              -- item count
//                  0,              -- equip revenue
//                  0,              -- ticket count
//                  0,              -- batch count
//                  0,              -- auth count
//                  0,              -- auth per item
//                  round( :collectedDiscount - totalDiscount, 2 ), -- misc revenue
//                  round( :collectedDiscount - totalDiscount, 2 ), -- mtd income
//                  :desc,
//                  :loadFilename
//                )    
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4863 = collectedDiscount - totalDiscount;
 double __sJT_4864 = collectedDiscount - totalDiscount;
   String theSqlTS = "insert into transcom_extract_summary\n              ( merchant_number,\n                active_date,\n                card_type,\n                charge_type,\n                sales_vol_count,\n                sales_vol_amount,\n                credits_vol_count,\n                credits_vol_amount,\n                disc_rate_revenue,\n                disc_per_item_revenue,\n                interchange_revenue,\n                surcharge_revenue,\n                surcharge_per_item_revenue,\n                assessment_expense,\n                interchange_expense,\n                item_fee,\n                item_count,\n                equip_revenue,\n                ticket_count,\n                batch_count,\n                auth_count,\n                auth_per_item,\n                misc_revenue,\n                mtd_inc,\n                description,\n                load_filename\n              )\n            values\n              (  :1 ,\n                 :2 ,\n                 :3 ,   \n                 :4 , \n                0,              -- sales count\n                0,              -- sales amount\n                0,              -- credits count\n                0,              -- credits amount\n                0,              -- discount amount\n                0.0,            -- per item revenue\n                0.0,            -- interchange revenue\n                0.0,            -- surcharge revenue\n                0.0,            -- surcharge per item (not supported)\n                0.0,            -- assessment expense\n                0.0,            -- interchange expense\n                0,              -- item fee (:5)\n                0,              -- item count\n                0,              -- equip revenue\n                0,              -- ticket count\n                0,              -- batch count\n                0,              -- auth count\n                0,              -- auth per item\n                round(  :6 , 2 ), -- misc revenue\n                round(  :7 , 2 ), -- mtd income\n                 :8 ,\n                 :9 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.startup.TranscomExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setString(3,cardTypeString);
   __sJT_st.setString(4,chargeTypeString);
   __sJT_st.setDouble(5,__sJT_4863);
   __sJT_st.setDouble(6,__sJT_4864);
   __sJT_st.setString(7,desc);
   __sJT_st.setString(8,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2225^11*/
        }
        
            debug = "inserting auth";//@
        // extract the authorizations for this merchant
        loadAuthData( merchantId, activeDate, loadFilename );
        
            debug = "inserting plan";//@
        // extract the plan data for this merchant
        loadPlanData( merchantId, activeDate, loadFilename );
        
            debug = "inserting debit/ebt";//@
        // load the debit networks and EBT data
        loadDebitEBTData( merchantId, activeDate, loadFilename );
        
            debug = "inserting charge records";//@
        // collect the charge records
        loadChargeRecordData( merchantId, activeDate, loadFilename );
        
            debug = "inserting batch header";//@
        // load the batch headers
        loadBatchHeaderData( merchantId, activeDate, loadFilename );
        
            debug = "inserting misc contract items";//@
        // load the remainder of the contract items
        loadMiscContractData( merchantId, activeDate, loadFilename );
        
        loadIcData( merchantId, activeDate, loadFilename );
        
        System.out.print("Processed " + ++recCount + "               \r");
      }   // end while( resultSet.next() )
      it.close();

      System.out.println();
      fileProcessed = true;
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadTranscomExtractSummary(" + loadFilename + ")", 
                debug + ": " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
      try{ LiabilityContract.cleanUp(); } catch( Exception e ) {}
    }
    return( fileProcessed );
  }
  
  private int vitalAuthTypeToIndex( String vitalAuthType )
  {
    int       retVal    = -1;
    
    try
    {
      for( int i = 0; i < AuthTypeNames.length; ++i )
      {
        if ( AuthTypeNames[i].equals(vitalAuthType) )
        {
          retVal = i;
          break;
        }
      }
    }
    catch( NullPointerException e )
    {
      logEntry( "vitalAuthTypeToIndex()",  e.toString() );
    }      
    
    return( retVal );
  }
  
  private int vitalVendorToIndex( String vitalVendor )
  {
    int       retVal    = -1;
    
    try
    {
      for( int i = 0; i < AuthVendorNames.length; ++i )
      {
        if ( AuthVendorNames[i].equals(vitalVendor) )
        {
          retVal = i;
          break;
        }
      }
    }
    catch( NullPointerException e )
    {
      logEntry( "vitalVendorToIndex()", e.toString() );
    }      
    return( retVal );
  }
}/*@lineinfo:generated-code*/