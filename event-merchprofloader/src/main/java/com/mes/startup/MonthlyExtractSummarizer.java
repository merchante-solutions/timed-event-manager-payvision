/*@lineinfo:filename=MonthlyExtractSummarizer*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/MonthlyExtractSummarizer.sqlj $

  Description:

  Last Modified By   : $Author: dsmith $
  Last Modified Date : $LastChangedDate: 2018-03-05 11:04:13 -0700 (Mon, 5 Mar 2018) $
  Version            : $Revision: 23932 $

  Change History:
     See Git database

  Copyright (C) 2000-2008,2009-2018 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.DateField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.FieldGroup;
import com.mes.forms.NumberField;
import com.mes.mbs.MonthEndValidation;
import com.mes.reports.BankContractBean;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import com.mes.support.StringUtilities;
import com.mes.support.ThreadPool;
import sqlj.runtime.ResultSetIterator;

public class MonthlyExtractSummarizer extends EventBase
{
  static Logger log = Logger.getLogger(MonthlyExtractSummarizer.class);
  
  static final String[] TeFieldPrefixes = 
  {
    "amex_vol",
    "amex_sales",
    "amex_credits",
    "dinr_vol",
    "dinr_sales",
    "dinr_credits",
    "disc_vol",
    "disc_sales",
    "disc_credits",
    "jcb_vol",
    "jcb_sales",
    "jcb_credits",
  };
  
  public class SummaryJob extends FieldBean
    implements Runnable
  {
  
    private   long        LoadSec               = 0L;
    
    private   double[]    amounts               = new double[12];
    private   int[]       counts                = new int[12];
  
    // variables from the original stored procedure
    //@ TODO: clean up run()
    private   String      amexFlag              = null;
    private   int         authCount             = 0;
    private   double      avgTicket             = 0.0;
    private   Date        beginDate             = null;
    private   double      cashAdvVolAmount      = 0.0;
    private   int         cashAdvVolCount       = 0;
    private   int         dcCount               = 0;
    private   int         delayCount            = 0;
    private   String      discFlag              = null;
    private   String      flagTemp              = null;
    private   int         keyedCount            = 0;
    private   long        node                  = 0L;
    private   Date        openDateBegin         = null;
    private   Date        openDateEnd           = null;
    private   String      planType              = null;

    private   int         salesCount            = 0;
    private   double      salesAmount           = 0.0;
    private   int         creditsCount          = 0;
    private   double      creditsAmount         = 0.0;
    private   int         volCount              = 0;
    private   double      volAmount             = 0.0;

    private   String      loadFilename          = null;
    private   long        loadFileId            = 0L;
    
    private   int         mcAuthCount           = 0;
    
    private double        vdSalesAmount         = 0.0;
    private double        vCSalesAmount         = 0.0;
    
  
    public SummaryJob( long loadSec )
    {
      LoadSec     = loadSec;
    }
  
    public void run()
    {
      int                 bankNumber      = 0;
      long                merchantId      = 0L;
      Date                activeDate      = null;
      Date                currDate        = null;
      String              merchantStatus  = null;
      double              incDisc         = 0.0;
      double              discPaid        = 0.0;
      String              prefix          = null;
      String              progress        = "Start of procedure";
      int                 amDirAuth       = 0;
      int                 amDirAuthTr     = 0;
      ResultSetIterator   it              = null;
      ResultSet           resultSet       = null;
      long                startTs         = System.currentTimeMillis();
      /* Code modified by RS for LCR merchant billing implementation   */      
      double              incPinlessDebit     = 0.0;
      double              expPinlessDebit     = 0.0;
      double              intchngFeePinlessDebit = 0.0;
      double              netwrkFeePinlessDebit = 0.0;
      int                 tranCountPinlessDebit =  0;
      double              ratePinlessDebit      = 0.0;
      double              totVpInterChange     = 0.0;
      double              totMpInterChange     = 0.0;
      double              totPinlessdebitInterChange     = 0.0;
    
      try
      {
        connect(true);

        //------------------------------------------------------------------------------------------------------------------------
        //                                fname                           label                            len  size   null ok?   extra...
        //------------------------------------------------------------------------------------------------------------------------
        fields.add( new Field       ("bank_number"                    , "bank_number"                    ,  4   ,  6  ,  true             ) );
        fields.add( new Field       ("assoc_hierarchy_node"           , "assoc_hierarchy_node"           , 16   , 18  ,  true             ) );
        fields.add( new Field       ("merchant_number"                , "merchant_number"                , 16   , 18  ,  true             ) );
        fields.add( new DateField   ("active_date"                    , "active_date"                                 ,  true             ) );
        fields.add( new NumberField ("terminal_rental_count"          , "terminal_rental_count"          ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("vmc_sales_count"                , "vmc_sales_count"                ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("vmc_sales_amount"               , "vmc_sales_amount"               , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("vmc_credits_count"              , "vmc_credits_count"              ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("vmc_credits_amount"             , "vmc_credits_amount"             , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("vmc_vol_count"                  , "vmc_vol_count"                  ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("vmc_vol_amount"                 , "vmc_vol_amount"                 , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("amex_sales_count"               , "amex_sales_count"               ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("amex_sales_amount"              , "amex_sales_amount"              , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("amex_credits_count"             , "amex_credits_count"             ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("amex_credits_amount"            , "amex_credits_amount"            , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("amex_vol_count"                 , "amex_vol_count"                 ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("amex_vol_amount"                , "amex_vol_amount"                , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("dinr_sales_count"               , "dinr_sales_count"               ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("dinr_sales_amount"              , "dinr_sales_amount"              , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("dinr_credits_count"             , "dinr_credits_count"             ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("dinr_credits_amount"            , "dinr_credits_amount"            , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("dinr_vol_count"                 , "dinr_vol_count"                 ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("dinr_vol_amount"                , "dinr_vol_amount"                , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("disc_sales_count"               , "disc_sales_count"               ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("disc_sales_amount"              , "disc_sales_amount"              , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("disc_credits_count"             , "disc_credits_count"             ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("disc_credits_amount"            , "disc_credits_amount"            , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("disc_vol_count"                 , "disc_vol_count"                 ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("disc_vol_amount"                , "disc_vol_amount"                , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("jcb_sales_count"                , "jcb_sales_count"                ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("jcb_sales_amount"               , "jcb_sales_amount"               , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("jcb_credits_count"              , "jcb_credits_count"              ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("jcb_credits_amount"             , "jcb_credits_amount"             , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("jcb_vol_count"                  , "jcb_vol_count"                  ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("jcb_vol_amount"                 , "jcb_vol_amount"                 , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("debit_sales_count"              , "debit_sales_count"              ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("debit_sales_amount"             , "debit_sales_amount"             , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("debit_credits_count"            , "debit_credits_count"            ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("debit_credits_amount"           , "debit_credits_amount"           , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("debit_vol_count"                , "debit_vol_count"                ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("debit_vol_amount"               , "debit_vol_amount"               , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("chargeback_vol_count"           , "chargeback_vol_count"           ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("chargeback_vol_amount"          , "chargeback_vol_amount"          , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("cash_advance_vol_count"         , "cash_advance_vol_count"         ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("cash_advance_vol_amount"        , "cash_advance_vol_amount"        , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("vmc_auth_count"                 , "vmc_auth_count"                 ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("amex_auth_count"                , "amex_auth_count"                ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("dinr_auth_count"                , "dinr_auth_count"                ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("disc_auth_count"                , "disc_auth_count"                ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("jcb_auth_count"                 , "jcb_auth_count"                 ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("debit_auth_count"               , "debit_auth_count"               ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("interchange_expense"            , "interchange_expense"            , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("bet_interchange_expense"        , "bet_interchange_expense"        , 10   , 12  ,  true      ,2     ) );
        fields.add( new NumberField ("cash_adv_interchange_expense"   , "cash_adv_interchange_expense"   , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("vmc_assessment_expense"         , "vmc_assessment_expense"         , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("equip_rental_income"            , "equip_rental_income"            , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("equip_sales_income"             , "equip_sales_income"             , 13   , 15  ,  true      ,2     ) );
        fields.add( new Field       ("product_code"                   , "product_code"                   ,  2   ,  4  ,  true             ) );
        fields.add( new Field       ("new_merchant"                   , "new_merchant"                   ,  1   ,  3  ,  true             ) );
        fields.add( new Field       ("load_filename"                  , "load_filename"                  , 32   , 34  ,  true             ) );
        fields.add( new Field       ("merchant_status"                , "merchant_status"                ,  1   ,  3  ,  true             ) );
        fields.add( new NumberField ("incoming_chargeback_count"      , "incoming_chargeback_count"      ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("incoming_chargeback_amount"     , "incoming_chargeback_amount"     , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("incoming_ft_chargeback_count"   , "incoming_ft_chargeback_count"   ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("incoming_ft_chargeback_amount"  , "incoming_ft_chargeback_amount"  , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("retrieval_count"                , "retrieval_count"                ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("retrieval_amount"               , "retrieval_amount"               , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("service_call_count"             , "service_call_count"             ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("discount_inc_calc"              , "discount_inc_calc"              , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("equip_rental_expense"           , "equip_rental_expense"           , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("equip_sales_expense"            , "equip_sales_expense"            , 13   , 15  ,  true      ,2     ) );
        fields.add( new Field       ("amex_setup"                     , "amex_setup"                     ,  1   ,  3  ,  true             ) );
        fields.add( new Field       ("discover_setup"                 , "discover_setup"                 ,  1   ,  3  ,  true             ) );
        fields.add( new NumberField ("vmc_cash_advance_count"         , "vmc_cash_advance_count"         ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("vmc_cash_advance_amount"        , "vmc_cash_advance_amount"        , 13   , 15  ,  true      ,2     ) );
        fields.add( new Field       ("activated_merchant"             , "activated_merchant"             ,  1   ,  3  ,  true             ) );
        fields.add( new NumberField ("vital_merchant_income"          , "vital_merchant_income"          , 13   , 15  ,  true      ,2     ) );
        fields.add( new Field       ("hh_load_sec"                    , "hh_load_sec"                    , 20   , 22  ,  true             ) );
        fields.add( new NumberField ("disc_ic_dce_adj_count"          , "disc_ic_dce_adj_count"          ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("disc_ic_dce_adj_amount"         , "disc_ic_dce_adj_amount"         , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("fee_dce_adj_count"              , "fee_dce_adj_count"              ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("fee_dce_adj_amount"             , "fee_dce_adj_amount"             , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("visa_interchange"               , "visa_interchange"               , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("mc_interchange"                 , "mc_interchange"                 , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("vmc_avg_ticket"                 , "vmc_avg_ticket"                 , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("avs_count"                      , "avs_count"                      ,  7   ,  9  ,  true      ,0     ) );
        fields.add( new NumberField ("voice_auth_count"               , "voice_auth_count"               ,  7   ,  9  ,  true      ,0     ) );
        fields.add( new Field       ("moto_merchant"                  , "moto_merchant"                  ,  1   ,  9  ,  true             ) );
        fields.add( new NumberField ("tot_inc_authorization"          , "tot_inc_authorization"          , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_inc_capture"                , "tot_inc_capture"                , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_inc_debit"                  , "tot_inc_debit"                  , 12   , 14  ,  true      ,2     ) );
        /* Code modified by RS for ACHPS populate ACH income & PS expense fields  */ 
        fields.add( new NumberField ("tot_inc_ach"                    , "tot_inc_ach"                    , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_inc_discount_ach"           , "tot_inc_discount_ach"           , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_inc_ind_plans"              , "tot_inc_ind_plans"              , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_inc_sys_generated"          , "tot_inc_sys_generated"          , 12   , 14  ,  true      ,2     ) );
        fields.add( new Field       ("cash_advance_account"           , "cash_advance_account"           ,  1   ,  2  ,  true             ) );
        fields.add( new NumberField ("tot_inc_discount"               , "tot_inc_discount"               , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_inc_interchange"            , "tot_inc_interchange"            , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_exp_authorization"          , "tot_exp_authorization"          , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_exp_capture"                , "tot_exp_capture"                , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_exp_debit"                  , "tot_exp_debit"                  , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_exp_ind_plans"              , "tot_exp_ind_plans"              , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_exp_sys_generated"          , "tot_exp_sys_generated"          , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_ndr_liability"              , "tot_ndr_liability"              , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_authorization_liability"    , "tot_authorization_liability"    , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_capture_liability"          , "tot_capture_liability"          , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_debit_liability"            , "tot_debit_liability"            , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_ind_plans_liability"        , "tot_ind_plans_liability"        , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_sys_generated_liability"    , "tot_sys_generated_liability"    , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_ndr_referral"               , "tot_ndr_referral"               , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_authorization_referral"     , "tot_authorization_referral"     , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_capture_referral"           , "tot_capture_referral"           , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_debit_referral"             , "tot_debit_referral"             , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_ind_plans_referral"         , "tot_ind_plans_referral"         , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_sys_generated_referral"     , "tot_sys_generated_referral"     , 12   , 14  ,  true      ,2     ) );
        fields.add( new DateField   ("month_end_date"                 , "month_end_date"                              ,  true             ) );
        fields.add( new Field       ("liability_contract"             , "liability_contract"             ,  1   ,  3  ,  true             ) );
        fields.add( new Field       ("referral_contract"              , "referral_contract"              ,  1   ,  3  ,  true             ) );
        fields.add( new NumberField ("tot_partnership"                , "tot_partnership"                , 12   , 14  ,  true      ,2     ) );
        fields.add( new DateField   ("month_begin_date"               , "month_begin_date"                            ,  true             ) );
        fields.add( new NumberField ("equip_rental_base_cost"         , "equip_rental_base_cost"         ,  9   , 11  ,  true      ,2     ) );
        fields.add( new NumberField ("equip_sales_base_cost"          , "equip_sales_base_cost"          ,  9   , 11  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_equip_rental_liability"     , "tot_equip_rental_liability"     , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_equip_sales_liability"      , "tot_equip_sales_liability"      , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_equip_rental_referral"      , "tot_equip_rental_referral"      , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_equip_sales_referral"       , "tot_equip_sales_referral"       , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("ebt_vol_count"                  , "ebt_vol_count"                  ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("ebt_vol_amount"                 , "ebt_vol_amount"                 , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("ebt_sales_count"                , "ebt_sales_count"                ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("ebt_sales_amount"               , "ebt_sales_amount"               , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("ebt_credits_count"              , "ebt_credits_count"              ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("ebt_credits_amount"             , "ebt_credits_amount"             , 13   , 15  ,  true      ,2     ) );
        fields.add( new NumberField ("visa_sales_count"               , "visa_sales_count"               ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("visa_sales_amount"              , "visa_sales_amount"              , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("visa_credits_count"             , "visa_credits_count"             ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("visa_credits_amount"            , "visa_credits_amount"            , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("visa_vol_count"                 , "visa_vol_count"                 ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("visa_vol_amount"                , "visa_vol_amount"                , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mc_sales_count"                 , "mc_sales_count"                 ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_sales_amount"                , "mc_sales_amount"                , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mc_credits_count"               , "mc_credits_count"               ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_credits_amount"              , "mc_credits_amount"              , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mc_vol_count"                   , "mc_vol_count"                   ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_vol_amount"                  , "mc_vol_amount"                  , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("aru_auth_count"                 , "aru_auth_count"                 ,  7   ,  9  ,  true      ,0     ) );
        fields.add( new NumberField ("imprinter_count"                , "imprinter_count"                ,  4   ,  6  ,  true      ,0     ) );
        fields.add( new NumberField ("visa_ic_bet_number"             , "visa_ic_bet_number"             ,  4   ,  6  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_ic_bet_number"               , "mc_ic_bet_number"               ,  4   ,  6  ,  true      ,0     ) );
        fields.add( new NumberField ("ach_reject_count"               , "ach_reject_count"               ,  7   ,  9  ,  true      ,0     ) );
        fields.add( new NumberField ("tot_inc_daily_discount"         , "tot_inc_daily_discount"         , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_inc_daily_interchange"      , "tot_inc_daily_interchange"      , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("visa_assessment"                , "visa_assessment"                , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mc_assessment"                  , "mc_assessment"                  , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("disc_assessment"                , "disc_assessment"                , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("terminal_deployment_count"      , "terminal_deployment_count"      ,  6   ,  8  ,  true      ,0     ) );
        fields.add( new NumberField ("terminal_swap_count"            , "terminal_swap_count"            ,  6   ,  8  ,  true      ,0     ) );
        fields.add( new NumberField ("auth_count_total"               , "auth_count_total"               ,  9   , 11  ,  true      ,0     ) );
        fields.add( new Field       ("mes_inventory"                  , "mes_inventory"                  ,  1   ,  3  ,  true             ) );
        fields.add( new NumberField ("vmc_auth_count_ext"             , "vmc_auth_count_ext"             ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("amex_auth_count_ext"            , "amex_auth_count_ext"            ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("dinr_auth_count_ext"            , "dinr_auth_count_ext"            ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("disc_auth_count_ext"            , "disc_auth_count_ext"            ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("jcb_auth_count_ext"             , "jcb_auth_count_ext"             ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("debit_auth_count_ext"           , "debit_auth_count_ext"           ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("auth_count_total_ext"           , "auth_count_total_ext"           ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("equip_transfer_cost"            , "equip_transfer_cost"            ,  9   , 11  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_ndr_reseller"               , "tot_ndr_reseller"               , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_authorization_reseller"     , "tot_authorization_reseller"     , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_capture_reseller"           , "tot_capture_reseller"           , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_debit_reseller"             , "tot_debit_reseller"             , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_ind_plans_reseller"         , "tot_ind_plans_reseller"         , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_sys_generated_reseller"     , "tot_sys_generated_reseller"     , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_equip_rental_reseller"      , "tot_equip_rental_reseller"      , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_equip_sales_reseller"       , "tot_equip_sales_reseller"       , 12   , 14  ,  true      ,2     ) );
        fields.add( new Field       ("reseller_contract"              , "reseller_contract"              ,  1   ,  3  ,  true             ) );
        fields.add( new NumberField ("excluded_fees_amount"           , "excluded_fees_amount"           , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("excluded_equip_rental_amount"   , "excluded_equip_rental_amount"   , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("excluded_equip_sales_amount"    , "excluded_equip_sales_amount"    , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("equip_sales_tax_collected"      , "equip_sales_tax_collected"      , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_ndr_external"               , "tot_ndr_external"               , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_authorization_external"     , "tot_authorization_external"     , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_capture_external"           , "tot_capture_external"           , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_debit_external"             , "tot_debit_external"             , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_ind_plans_external"         , "tot_ind_plans_external"         , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_sys_generated_external"     , "tot_sys_generated_external"     , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_equip_rental_external"      , "tot_equip_rental_external"      , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_equip_sales_external"       , "tot_equip_sales_external"       , 12   , 14  ,  true      ,2     ) );
        fields.add( new Field       ("external_contract"              , "external_contract"              ,  1   ,  3  ,  true             ) );
        fields.add( new Field       ("cash_advance_referral"          , "cash_advance_referral"          ,  1   ,  3  ,  true             ) );
        fields.add( new NumberField ("load_file_id"                   , "load_file_id"                   , 20   , 22  ,  true      ,0     ) );
        fields.add( new NumberField ("mes_only_inc_disc_ic"           , "mes_only_inc_disc_ic"           , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mes_only_inc_authorization"     , "mes_only_inc_authorization"     , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mes_only_inc_capture"           , "mes_only_inc_capture"           , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mes_only_inc_ind_plans"         , "mes_only_inc_ind_plans"         , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mes_only_inc_debit"             , "mes_only_inc_debit"             , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mes_only_inc_sys_generated"     , "mes_only_inc_sys_generated"     , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("pl1_sales_count"                , "pl1_sales_count"                ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("pl1_sales_amount"               , "pl1_sales_amount"               , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("pl1_credits_count"              , "pl1_credits_count"              ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("pl1_credits_amount"             , "pl1_credits_amount"             , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("pl1_vol_count"                  , "pl1_vol_count"                  ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("pl1_vol_amount"                 , "pl1_vol_amount"                 , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("pl2_sales_count"                , "pl2_sales_count"                ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("pl2_sales_amount"               , "pl2_sales_amount"               , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("pl2_credits_count"              , "pl2_credits_count"              ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("pl2_credits_amount"             , "pl2_credits_amount"             , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("pl2_vol_count"                  , "pl2_vol_count"                  ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("pl2_vol_amount"                 , "pl2_vol_amount"                 , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("pl3_sales_count"                , "pl3_sales_count"                ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("pl3_sales_amount"               , "pl3_sales_amount"               , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("pl3_credits_count"              , "pl3_credits_count"              ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("pl3_credits_amount"             , "pl3_credits_amount"             , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("pl3_vol_count"                  , "pl3_vol_count"                  ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("pl3_vol_amount"                 , "pl3_vol_amount"                 , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("visa_auth_count"                , "visa_auth_count"                ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("visa_apf_count"                 , "visa_apf_count"                 ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("visa_apf_db_count"              , "visa_apf_db_count"              ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("visa_tif_count"                 , "visa_tif_count"                 ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_auth_count"                  , "mc_auth_count"                  ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_tif_count"                   , "mc_tif_count"                   ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_cross_border_sales_count"    , "mc_cross_border_sales_count"    ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_cross_border_sales_amount"   , "mc_cross_border_sales_amount"   , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mc_cross_border_credits_count"  , "mc_cross_border_credits_count"  ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_cross_border_credits_amount" , "mc_cross_border_credits_amount" , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mc_cross_border_vol_count"      , "mc_cross_border_vol_count"      ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_cross_border_vol_amount"     , "mc_cross_border_vol_amount"     , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("supply_cost"                    , "supply_cost"                    ,  9   , 11  ,  true      ,2     ) );
        fields.add( new NumberField ("ic_correction_count"            , "ic_correction_count"            ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("auth_count_total_api"           , "auth_count_total_api"           ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("auth_count_total_trident"       , "auth_count_total_trident"       ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("amex_auth_count_trident"        , "amex_auth_count_trident"        ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("disc_auth_count_trident"        , "disc_auth_count_trident"        ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("jcb_auth_count_trident"         , "jcb_auth_count_trident"         ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("dinr_auth_count_trident"        , "dinr_auth_count_trident"        ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("vmc_auth_count_trident"         , "vmc_auth_count_trident"         ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("debit_auth_count_trident"       , "debit_auth_count_trident"       ,  9   , 11  ,  true      ,0     ) );
        fields.add( new Field       ("dda"                            , "dda"                            , 17   , 19  ,  true             ) );
        fields.add( new Field       ("transit_routing"                , "transit_routing"                ,  9   , 11  ,  true             ) );
        fields.add( new Field       ("mailing_name"                   , "mailing_name"                   , 32   , 34  ,  true             ) );
        fields.add( new Field       ("mailing_addr1"                  , "mailing_addr1"                  , 32   , 34  ,  true             ) );
        fields.add( new Field       ("mailing_addr2"                  , "mailing_addr2"                  , 32   , 34  ,  true             ) );
        fields.add( new Field       ("mailing_city"                   , "mailing_city"                   , 24   , 26  ,  true             ) );
        fields.add( new Field       ("mailing_state"                  , "mailing_state"                  ,  2   ,  4  ,  true             ) );
        fields.add( new Field       ("mailing_zip"                    , "mailing_zip"                    ,  9   , 11  ,  true             ) );
        fields.add( new NumberField ("mc_foreign_std_sales_count"     , "mc_foreign_std_sales_count"     ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_foreign_std_sales_amount"    , "mc_foreign_std_sales_amount"    , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mc_foreign_std_credits_count"   , "mc_foreign_std_credits_count"   ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_foreign_std_credits_amount"  , "mc_foreign_std_credits_amount"  , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mc_foreign_std_vol_count"       , "mc_foreign_std_vol_count"       ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_foreign_std_vol_amount"      , "mc_foreign_std_vol_amount"      , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mc_foreign_elec_sales_count"    , "mc_foreign_elec_sales_count"    ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_foreign_elec_sales_amount"   , "mc_foreign_elec_sales_amount"   , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mc_foreign_elec_credits_count"  , "mc_foreign_elec_credits_count"  ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_foreign_elec_credits_amount" , "mc_foreign_elec_credits_amount" , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mc_foreign_elec_vol_count"      , "mc_foreign_elec_vol_count"      ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_foreign_elec_vol_amount"     , "mc_foreign_elec_vol_amount"     , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("partnership_rate"               , "partnership_rate"               , 12   , 14  ,  true      ,6     ) );
        fields.add( new NumberField ("mc_intl_assessment_expense"     , "mc_intl_assessment_expense"     , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("visa_auth_count_trident"        , "visa_auth_count_trident"        ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_auth_count_trident"          , "mc_auth_count_trident"          ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("vmc_fees"                       , "vmc_fees"                       , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("visa_fees"                      , "visa_fees"                      , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mc_fees"                        , "mc_fees"                        , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("disc_fees"                      , "disc_fees"                      , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("debit_fees"                     , "debit_fees"                      , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("debit_network_fees"             , "debit_network_fees"             , 12   , 14  ,  true      ,2     ) );
        fields.add( new Field       ("debit_pass_through_flag"        , "debit_pass_through_flag"        ,  1   ,  3  ,  true             ) );
        fields.add( new NumberField ("visa_misuse_auth_count"         , "visa_misuse_auth_count"         ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("visa_zfl_auth_count"            , "visa_zfl_auth_count"            ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("amex_ic_sales_count"            , "amex_ic_sales_count"            ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("amex_ic_sales_amount"           , "amex_ic_sales_amount"           , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("amex_ic_credits_count"          , "amex_ic_credits_count"          ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("amex_ic_credits_amount"         , "amex_ic_credits_amount"         , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("amex_ic_vol_count"              , "amex_ic_vol_count"              ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("amex_ic_vol_amount"             , "amex_ic_vol_amount"             , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("amex_interchange"               , "amex_interchange"               , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("disc_ic_sales_count"            , "disc_ic_sales_count"            ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("disc_ic_sales_amount"           , "disc_ic_sales_amount"           , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("disc_ic_credits_count"          , "disc_ic_credits_count"          ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("disc_ic_credits_amount"         , "disc_ic_credits_amount"         , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("disc_ic_vol_count"              , "disc_ic_vol_count"              ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("disc_ic_vol_amount"             , "disc_ic_vol_amount"             , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("disc_interchange"               , "disc_interchange"               , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_inc_ic_disc"                , "tot_inc_ic_disc"                , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_inc_ic_amex"                , "tot_inc_ic_amex"                , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("interchange_expense_enhanced"   , "interchange_expense_enhanced"   , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tran_count_keyed"               , "tran_count_keyed"               ,  10   , 11  ,  true     ,0     ) );
        fields.add( new NumberField ("tran_count_swiped"              , "tran_count_swiped"              ,  10   , 11  ,  true     ,0     ) );
        fields.add( new NumberField ("amex_direct_auth_count_trident" , "amex_direct_auth_count_trident" ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("amex_direct_auth_count_vital"   , "amex_direct_auth_count_vital"   ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("tot_amount_keyed"               , "tot_amount_keyed"               , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_amount_swiped"              , "tot_amount_swiped"              , 12   , 14  ,  true      ,2     ) );        
        fields.add( new NumberField ("mc_cnp_digi_enablement_count"   , "mc_cnp_digi_enablement_count"   ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mc_cnp_digi_enablement_amount"  , "mc_cnp_digi_enablement_amount"  , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("vs_cr_assessment_count"         , "vs_cr_assessment_count"         ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("vs_cr_assessment_amount"        , "vs_cr_assessment_amount"        , 12   , 14  ,  true      ,2     ) );
        fields.add( new Field       ("print_statement"                , "print_statement"                ,  1   ,  3  ,  true             ) );
        fields.add( new NumberField ("batch_fee_count"                , "batch_fee_count"                ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("batch_fee_amount"               , "batch_fee_amount"               , 12   , 14  ,  true      ,2     ) );
      /* Code modified by RS for ACHPS populate count fields for Partner fees calculation  */ 
        fields.add( new NumberField ("ach_tran_count"                 , "ach_tran_count"                 ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("ach_tran_amount"                , "ach_tran__amount"               , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("ach_return_count"               , "ach_return_count"               ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("ach_return_amount"              , "ach_return__amount"             , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("ach_unauth_count"               , "ach_unauth_count"               ,  9   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("ach_unauth_amount"              , "ach_unauth__amount"             , 12   , 14  ,  true      ,2     ) );
        /* Code modified by RS for LCR implementation  */ 
        fields.add( new NumberField ("mp_sales_count"                , "mp_sales_count"                ,  10   , 11  ,  true     ,0     ) );
        fields.add( new NumberField ("mp_sales_amount"               , "mp_sales_amount"               , 10   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mp_credits_count"              , "mp_credits_count"              , 10   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("mp_credits_amount"             , "mp_credits_amount"             , 10   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("mp_vol_count"                  , "mp_vol_count"                  ,  10   , 11  ,  true     ,0     ) );
        fields.add( new NumberField ("mp_vol_amount"                 , "mp_vol_amount"                 , 10   , 14  ,  true      ,2     ) );
        /* Code modified by RS for LCR implementation  */ 
        fields.add( new NumberField ("vp_sales_count"                , "vp_sales_count"                ,  10   , 11  ,  true     ,0     ) );
        fields.add( new NumberField ("vp_sales_amount"               , "vp_sales_amount"               , 10   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("vp_credits_count"              , "vp_credits_count"              , 10   , 11  ,  true      ,0     ) );
        fields.add( new NumberField ("vp_credits_amount"             , "vp_credits_amount"             , 10   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("vp_vol_count"                  , "vp_vol_count"                  ,  10   , 11  ,  true     ,0     ) );
        fields.add( new NumberField ("vp_vol_amount"                 , "vp_vol_amount"                 , 10   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_inc_pinlessdebit"          , "tot_inc_pinlessdebit"              , 12   , 14  ,  true      ,2     ) );
        fields.add( new NumberField ("tot_exp_pinlessdebit"          , "tot_exp_pinlessdebit"              , 12   , 14  ,  true      ,2     ) );
        
        progress = "top of loop: " + merchantId;
        
        /*@lineinfo:generated-code*//*@lineinfo:477^9*/

//  ************************************************************
//  #sql [Ctx] { select  gn.hh_bank_number,
//                    gn.hh_merchant_number,
//                    gn.hh_active_date,
//                    gn.hh_curr_date,
//                    gn.g1_merchant_status,
//                    gn.t1_tot_calculated_discount,
//                    gn.t1_tot_discount_paid,
//                    gn.load_filename,gn.load_file_id
//            
//            from    monthly_extract_gn    gn
//            where   gn.hh_load_sec = :LoadSec
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  gn.hh_bank_number,\n                  gn.hh_merchant_number,\n                  gn.hh_active_date,\n                  gn.hh_curr_date,\n                  gn.g1_merchant_status,\n                  gn.t1_tot_calculated_discount,\n                  gn.t1_tot_discount_paid,\n                  gn.load_filename,gn.load_file_id\n           \n          from    monthly_extract_gn    gn\n          where   gn.hh_load_sec =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 9) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(9,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   merchantId = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   activeDate = (java.sql.Date)__sJT_rs.getDate(3);
   currDate = (java.sql.Date)__sJT_rs.getDate(4);
   merchantStatus = (String)__sJT_rs.getString(5);
   incDisc = __sJT_rs.getDouble(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   discPaid = __sJT_rs.getDouble(7); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   loadFilename = (String)__sJT_rs.getString(8);
   loadFileId = __sJT_rs.getLong(9); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:492^9*/          
        
        // setup the billing cycle begin date for this merchant/month combo
        progress = "get_extract_begin_date(" + merchantId + ")";
        /*@lineinfo:generated-code*//*@lineinfo:496^9*/

//  ************************************************************
//  #sql [Ctx] { select  max( decode(gnp.hh_curr_date,
//                                null, gn.hh_active_date,
//                                gnp.hh_curr_date ) )
//            
//            from    monthly_extract_gn      gn,
//                    monthly_extract_gn      gnp
//            where   gn.hh_load_sec = :LoadSec
//                    and gnp.hh_merchant_number(+) = gn.hh_merchant_number 
//                    and gnp.hh_active_date(+) = trunc((:activeDate-15),'month')
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max( decode(gnp.hh_curr_date,\n                              null, gn.hh_active_date,\n                              gnp.hh_curr_date ) )\n           \n          from    monthly_extract_gn      gn,\n                  monthly_extract_gn      gnp\n          where   gn.hh_load_sec =  :1  \n                  and gnp.hh_merchant_number(+) = gn.hh_merchant_number \n                  and gnp.hh_active_date(+) = trunc(( :2  -15),'month')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:507^9*/                  
      
        /*@lineinfo:generated-code*//*@lineinfo:509^9*/

//  ************************************************************
//  #sql [Ctx] { select    activity_to_open_date_begin( :merchantId, :activeDate, :beginDate ),
//                      activity_to_open_date_end( :merchantId, :activeDate, (:currDate-1) )
//            
//            from      dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select    activity_to_open_date_begin(  :1  ,  :2  ,  :3   ),\n                    activity_to_open_date_end(  :4  ,  :5  , ( :6  -1) )\n           \n          from      dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setLong(4,merchantId);
   __sJT_st.setDate(5,activeDate);
   __sJT_st.setDate(6,currDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   openDateBegin = (java.sql.Date)__sJT_rs.getDate(1);
   openDateEnd = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:515^9*/

        /* insert the basic record */
        progress = "inserting into summary table: " + merchantId;
        /*@lineinfo:generated-code*//*@lineinfo:519^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  gn.hh_bank_number                             as bank_number,
//                    ( gn.hh_bank_number ||
//                      lpad(gn.g1_association_number,6,'0') )      as assoc_hierarchy_node,
//                    gn.hh_merchant_number                         as merchant_number,
//                    gn.hh_active_date                             as active_date,
//                    decode(:MbsBackend,1,:activeDate,:beginDate)  as month_begin_date,
//                    decode(:MbsBackend,1,:currDate,(:currDate-1)) as month_end_date,
//                    gn.g1_merchant_status                         as merchant_status,
//                    substr( gn.G1_USER_DATA3, 3 )                 as product_code,
//                    gn.T1_TOT_NUMBER_OF_CHARGEBACK                as chargeback_vol_count,
//                    gn.t1_tot_amount_of_chargeback                as chargeback_vol_amount,
//                    gn.t1_tot_number_of_cash_advc                 as cash_advance_vol_count,
//                    gn.t1_tot_amount_of_cash_advc                 as cash_advance_vol_amount,
//                    ( gn.t1_tot_calculated_discount 
//                      + gn.t1_tot_inc_interchange 
//                      + gn.t1_tot_inc_ind_plans 
//                      + gn.t1_tot_inc_authorization 
//                      + gn.t1_tot_inc_capture 
//                      + gn.t1_tot_inc_sys_generated 
//                      + gn.t1_tot_inc_debit_networks
//                      + gn.t1_tot_inc_ach )               as vital_merchant_income,
//                    gn.t1_tot_calculated_discount                 as tot_inc_discount,
//                    gn.t1_tot_discount_paid                       as tot_inc_daily_discount,
//                    gn.t1_tot_interchange_paid                    as tot_inc_daily_interchange,
//                    gn.t1_tot_inc_interchange                     as tot_inc_interchange,
//                    gn.t1_tot_inc_authorization                   as tot_inc_authorization,
//                    gn.t1_tot_inc_capture                         as tot_inc_capture,
//                    gn.t1_tot_inc_debit_networks                  as tot_inc_debit,
//                    gn.t1_tot_inc_ind_plans                       as tot_inc_ind_plans,
//                    gn.t1_tot_inc_sys_generated                   as tot_inc_sys_generated,
//                    gn.t1_tot_exp_authorization                   as tot_exp_authorization,
//                    gn.t1_tot_exp_capture                         as tot_exp_capture,
//                    gn.t1_tot_exp_debit_networks                  as tot_exp_debit,
//                    gn.t1_tot_exp_ind_plans                       as tot_exp_ind_plans,
//                    gn.t1_tot_exp_sys_generated                   as tot_exp_sys_generated,
//                    /* Code modified by RS for ACHPS Add ACH fees & Discount separately  */ 
//                    gn.t1_tot_inc_ach                             as tot_inc_ach,
//                    gn.t1_tot_inc_discount_ach                    as tot_inc_discount_ach,
//                    decode(gn.g1_sic_code,6010,'Y',6011,'Y','N')  as cash_advance_account,
//                    nvl(visa.v1_bet_table_number,mf.ic_bet_visa)  as visa_ic_bet_number,
//                    nvl(mc.m1_bet_table_number,mf.ic_bet_mc)      as mc_ic_bet_number,
//                    -- data no longer provided by TSYS
//                    0                                             as interchange_expense,
//                    0                                             as visa_assessment,
//                    0                                             as visa_interchange,
//                    0                                             as mc_assessment,
//                    0                                             as mc_interchange,
//                    0                                             as cash_adv_interchange_expense,
//                    0                                             as vmc_assessment_expense,
//                    nvl(gn.t1_tot_exp_interchange,0)              as bet_interchange_expense,
//                    gn.hh_load_sec                                as hh_load_sec,
//                    gn.load_filename                              as load_filename,
//                    gn.load_file_id                               as load_file_id,
//                    mf.dda_num                                    as dda,
//                    mf.transit_routng_num                         as transit_routing,
//                    -- 3858 uses custom addresses
//                    -- until all banks are on the MES backend, do not
//                    -- use the mon_ext_gn table for mailing addresses
//                    case when mf.bank_number = 3858 then 
//                      decode(sa.merchant_number,null, mf.dba_name      ,sa.address_name)    else  
//                      decode(sa.merchant_number, null, mf.name1_line_1, sa.address_name)    end as ad_name_line1, 
//                    case when mf.bank_number = 3858 then 
//                      decode(sa.merchant_number,null, mf.dmaddr        ,sa.address_1)       else  
//                      decode(sa.merchant_number, null, mf.addr1_line_1, sa.address_1)       end as ad_address_line1, 
//                    case when mf.bank_number = 3858 then 
//                      decode(sa.merchant_number,null, mf.address_line_3,sa.address_2)       else  
//                      decode(sa.merchant_number, null, mf.addr1_line_2, sa.address_2)       end as ad_address_line2, 
//                    case when mf.bank_number = 3858 then 
//                      decode(sa.merchant_number,null, mf.dmcity        ,sa.address_city)    else  
//                      decode(sa.merchant_number, null, mf.city1_line_4, sa.address_city)    end as ad_city, 
//                    case when mf.bank_number = 3858 then 
//                      decode(sa.merchant_number,null,mf.dmstate       ,sa.address_state)    else  
//                      decode(sa.merchant_number, null, mf.state1_line_4, sa.address_state)  end as ad_state, 
//                    case when mf.bank_number = 3858 then 
//                      decode(sa.merchant_number,null,mf.dmzip         ,sa.address_zip)      else  
//                      decode(sa.merchant_number, null, mf.zip1_line_4, sa.address_zip)      end as ad_zip_code
//            from    monthly_extract_gn        gn,
//                    monthly_extract_visa      visa,
//                    monthly_extract_mc        mc,
//                    mif                       mf,
//                    statement_address         sa
//            where   gn.hh_load_sec = :LoadSec
//                    and visa.hh_load_sec(+) = gn.hh_load_sec 
//                    and mc.hh_load_sec(+) = gn.hh_load_sec 
//                    and mf.merchant_number = gn.hh_merchant_number
//                    and sa.merchant_number(+) = mf.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  gn.hh_bank_number                             as bank_number,\n                  ( gn.hh_bank_number ||\n                    lpad(gn.g1_association_number,6,'0') )      as assoc_hierarchy_node,\n                  gn.hh_merchant_number                         as merchant_number,\n                  gn.hh_active_date                             as active_date,\n                  decode( :1  ,1, :2  , :3  )  as month_begin_date,\n                  decode( :4  ,1, :5  ,( :6  -1)) as month_end_date,\n                  gn.g1_merchant_status                         as merchant_status,\n                  substr( gn.G1_USER_DATA3, 3 )                 as product_code,\n                  gn.T1_TOT_NUMBER_OF_CHARGEBACK                as chargeback_vol_count,\n                  gn.t1_tot_amount_of_chargeback                as chargeback_vol_amount,\n                  gn.t1_tot_number_of_cash_advc                 as cash_advance_vol_count,\n                  gn.t1_tot_amount_of_cash_advc                 as cash_advance_vol_amount,\n                  ( gn.t1_tot_calculated_discount \n                    + gn.t1_tot_inc_interchange \n                    + gn.t1_tot_inc_ind_plans \n                    + gn.t1_tot_inc_authorization \n                    + gn.t1_tot_inc_capture \n                    + gn.t1_tot_inc_sys_generated \n                    + gn.t1_tot_inc_debit_networks\n                    + gn.t1_tot_inc_ach )               as vital_merchant_income,\n                  gn.t1_tot_calculated_discount                 as tot_inc_discount,\n                  gn.t1_tot_discount_paid                       as tot_inc_daily_discount,\n                  gn.t1_tot_interchange_paid                    as tot_inc_daily_interchange,\n                  gn.t1_tot_inc_interchange                     as tot_inc_interchange,\n                  gn.t1_tot_inc_authorization                   as tot_inc_authorization,\n                  gn.t1_tot_inc_capture                         as tot_inc_capture,\n                  gn.t1_tot_inc_debit_networks                  as tot_inc_debit,\n                  gn.t1_tot_inc_ind_plans                       as tot_inc_ind_plans,\n                  gn.t1_tot_inc_sys_generated                   as tot_inc_sys_generated,\n                  gn.t1_tot_exp_authorization                   as tot_exp_authorization,\n                  gn.t1_tot_exp_capture                         as tot_exp_capture,\n                  gn.t1_tot_exp_debit_networks                  as tot_exp_debit,\n                  gn.t1_tot_exp_ind_plans                       as tot_exp_ind_plans,\n                  gn.t1_tot_exp_sys_generated                   as tot_exp_sys_generated,\n                  /* Code modified by RS for ACHPS Add ACH fees & Discount separately  */ \n                  gn.t1_tot_inc_ach                             as tot_inc_ach,\n                  gn.t1_tot_inc_discount_ach                    as tot_inc_discount_ach,\n                  decode(gn.g1_sic_code,6010,'Y',6011,'Y','N')  as cash_advance_account,\n                  nvl(visa.v1_bet_table_number,mf.ic_bet_visa)  as visa_ic_bet_number,\n                  nvl(mc.m1_bet_table_number,mf.ic_bet_mc)      as mc_ic_bet_number,\n                  -- data no longer provided by TSYS\n                  0                                             as interchange_expense,\n                  0                                             as visa_assessment,\n                  0                                             as visa_interchange,\n                  0                                             as mc_assessment,\n                  0                                             as mc_interchange,\n                  0                                             as cash_adv_interchange_expense,\n                  0                                             as vmc_assessment_expense,\n                  nvl(gn.t1_tot_exp_interchange,0)              as bet_interchange_expense,\n                  gn.hh_load_sec                                as hh_load_sec,\n                  gn.load_filename                              as load_filename,\n                  gn.load_file_id                               as load_file_id,\n                  mf.dda_num                                    as dda,\n                  mf.transit_routng_num                         as transit_routing,\n                  -- 3858 uses custom addresses\n                  -- until all banks are on the MES backend, do not\n                  -- use the mon_ext_gn table for mailing addresses\n                  case when mf.bank_number = 3858 then \n                    decode(sa.merchant_number,null, mf.dba_name      ,sa.address_name)    else  \n                    decode(sa.merchant_number, null, mf.name1_line_1, sa.address_name)    end as ad_name_line1, \n                  case when mf.bank_number = 3858 then \n                    decode(sa.merchant_number,null, mf.dmaddr        ,sa.address_1)       else  \n                    decode(sa.merchant_number, null, mf.addr1_line_1, sa.address_1)       end as ad_address_line1, \n                  case when mf.bank_number = 3858 then \n                    decode(sa.merchant_number,null, mf.address_line_3,sa.address_2)       else  \n                    decode(sa.merchant_number, null, mf.addr1_line_2, sa.address_2)       end as ad_address_line2, \n                  case when mf.bank_number = 3858 then \n                    decode(sa.merchant_number,null, mf.dmcity        ,sa.address_city)    else  \n                    decode(sa.merchant_number, null, mf.city1_line_4, sa.address_city)    end as ad_city, \n                  case when mf.bank_number = 3858 then \n                    decode(sa.merchant_number,null,mf.dmstate       ,sa.address_state)    else  \n                    decode(sa.merchant_number, null, mf.state1_line_4, sa.address_state)  end as ad_state, \n                  case when mf.bank_number = 3858 then \n                    decode(sa.merchant_number,null,mf.dmzip         ,sa.address_zip)      else  \n                    decode(sa.merchant_number, null, mf.zip1_line_4, sa.address_zip)      end as ad_zip_code\n          from    monthly_extract_gn        gn,\n                  monthly_extract_visa      visa,\n                  monthly_extract_mc        mc,\n                  mif                       mf,\n                  statement_address         sa\n          where   gn.hh_load_sec =  :7  \n                  and visa.hh_load_sec(+) = gn.hh_load_sec \n                  and mc.hh_load_sec(+) = gn.hh_load_sec \n                  and mf.merchant_number = gn.hh_merchant_number\n                  and sa.merchant_number(+) = mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MbsBackend);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,beginDate);
   __sJT_st.setInt(4,MbsBackend);
   __sJT_st.setDate(5,currDate);
   __sJT_st.setDate(6,currDate);
   __sJT_st.setLong(7,LoadSec);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.MonthlyExtractSummarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:607^9*/
        resultSet = it.getResultSet();
        resultSet.next();
        FieldBean.setFields(fields,resultSet,false,false);
        resultSet.close();
        it.close();

        // update the status in the MIF table
        progress = "updating merchant status in mif: " +  merchantId;
        /*@lineinfo:generated-code*//*@lineinfo:616^9*/

//  ************************************************************
//  #sql [Ctx] { update  mif
//            set     merchant_status = :merchantStatus
//            where   merchant_number = :merchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mif\n          set     merchant_status =  :1  \n          where   merchant_number =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantStatus);
   __sJT_st.setLong(2,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:621^9*/

        // set the daily discount flag in the application
        // only when there was some discount paid
        progress = "updating daily discount: " + merchantId;
        if ( incDisc > 0.0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:628^11*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//              set     merch_dly_discount_flag = decode(:discPaid, 0, 'N', 'D')
//              where   merch_number = :merchantId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merchant\n            set     merch_dly_discount_flag = decode( :1  , 0, 'N', 'D')\n            where   merch_number =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,discPaid);
   __sJT_st.setLong(2,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:633^11*/
        }
      
        progress = "disabling closed users: " + merchantId;
        if ( merchantStatus != null && "DCB".indexOf(merchantStatus) >= 0 )
        {
          // disable user account for closed, blocked or deleted accounts
          /*@lineinfo:generated-code*//*@lineinfo:640^11*/

//  ************************************************************
//  #sql [Ctx] { update  users
//              set     enabled = 'N'
//              where   hierarchy_node = :merchantId and
//                      (
//                        select  trunc(sysdate - mf.date_stat_chgd_to_dcb)
//                        from    mif   mf
//                        where   mf.merchant_number = :merchantId
//                      ) > 120
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  users\n            set     enabled = 'N'\n            where   hierarchy_node =  :1   and\n                    (\n                      select  trunc(sysdate - mf.date_stat_chgd_to_dcb)\n                      from    mif   mf\n                      where   mf.merchant_number =  :2  \n                    ) > 120";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:650^11*/
        }                    

        progress = "loading plan summaries: " + merchantId;
      
        // load all of the plan record summaries 
        /*@lineinfo:generated-code*//*@lineinfo:656^9*/

//  ************************************************************
//  #sql [Ctx] it = { select pl.pl_plan_type                                        as plan_type,
//                   sum( pl.PL_SALES_AMOUNT - pl.PL_CREDITS_AMOUNT )       as vol_amount,
//                   sum( pl.PL_NUMBER_OF_CREDITS + pl.PL_NUMBER_OF_SALES ) as vol_count,
//                   sum( pl.pl_sales_amount )                              as sales_amount,
//                   sum( pl.pl_number_of_sales )                           as sales_count,
//                   sum( pl.pl_credits_amount )                            as credits_amount,
//                   sum( pl.pl_number_of_credits )                         as credits_count
//            from   monthly_extract_gn gn,
//                   monthly_extract_pl pl
//            where  gn.hh_merchant_number = :merchantId and
//                   gn.hh_active_date     = :activeDate and
//                   pl.hh_load_sec        = gn.hh_load_sec
//            group by pl.pl_plan_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select pl.pl_plan_type                                        as plan_type,\n                 sum( pl.PL_SALES_AMOUNT - pl.PL_CREDITS_AMOUNT )       as vol_amount,\n                 sum( pl.PL_NUMBER_OF_CREDITS + pl.PL_NUMBER_OF_SALES ) as vol_count,\n                 sum( pl.pl_sales_amount )                              as sales_amount,\n                 sum( pl.pl_number_of_sales )                           as sales_count,\n                 sum( pl.pl_credits_amount )                            as credits_amount,\n                 sum( pl.pl_number_of_credits )                         as credits_count\n          from   monthly_extract_gn gn,\n                 monthly_extract_pl pl\n          where  gn.hh_merchant_number =  :1   and\n                 gn.hh_active_date     =  :2   and\n                 pl.hh_load_sec        = gn.hh_load_sec\n          group by pl.pl_plan_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.MonthlyExtractSummarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:671^9*/
        resultSet = it.getResultSet();

        // reset the summary variables
        for( int i = 0; i < counts.length; ++i ) { counts[i] = 0; }
        for( int i = 0; i < amounts.length; ++i ) { amounts[i] = 0; }
      
        while( resultSet.next() )
        {
          planType        = resultSet.getString("plan_type");
          volAmount       = resultSet.getDouble("vol_amount");
          volCount        = resultSet.getInt("vol_count");
          salesAmount     = resultSet.getDouble("sales_amount");
          salesCount      = resultSet.getInt("sales_count");
          creditsAmount   = resultSet.getDouble("credits_amount");
          creditsCount    = resultSet.getInt("credits_count");

          progress = "processing plan summaries " + planType + ": " + merchantId;
        
          if ( planType.equals("AMEX" ) || planType.equals("AM" ) )
          {
            fields.setData("amex_vol_amount"    ,volAmount      );
            fields.setData("amex_vol_count"     ,volCount       );
            fields.setData("amex_sales_amount"  ,salesAmount    );
            fields.setData("amex_sales_count"   ,salesCount     );
            fields.setData("amex_credits_amount",creditsAmount  );
            fields.setData("amex_credits_count" ,creditsCount   );
          }
          else if ( planType.equals("DC") ) 
          {
            fields.setData("dinr_vol_amount"    ,volAmount      );
            fields.setData("dinr_vol_count"     ,volCount       );
            fields.setData("dinr_sales_amount"  ,salesAmount    );
            fields.setData("dinr_sales_count"   ,salesCount     );
            fields.setData("dinr_credits_amount",creditsAmount  );
            fields.setData("dinr_credits_count" ,creditsCount   );
          }
          else if ( planType.equals("DS") )
          {
            fields.setData("disc_vol_amount"    ,volAmount      );
            fields.setData("disc_vol_count"     ,volCount       );
            fields.setData("disc_sales_amount"  ,salesAmount    );
            fields.setData("disc_sales_count"   ,salesCount     );
            fields.setData("disc_credits_amount",creditsAmount  );
            fields.setData("disc_credits_count" ,creditsCount   );
          }
          else if ( planType.equals("JCB") ) 
          {
            fields.setData("jcb_vol_amount"    ,volAmount      );
            fields.setData("jcb_vol_count"     ,volCount       );
            fields.setData("jcb_sales_amount"  ,salesAmount    );
            fields.setData("jcb_sales_count"   ,salesCount     );
            fields.setData("jcb_credits_amount",creditsAmount  );
            fields.setData("jcb_credits_count" ,creditsCount   );
          }
          else if ( planType.equals("DEBC") )
          {
            fields.setData("debit_vol_amount"    ,volAmount      );
            fields.setData("debit_vol_count"     ,volCount       );
            fields.setData("debit_sales_amount"  ,salesAmount    );
            fields.setData("debit_sales_count"   ,salesCount     );
            fields.setData("debit_credits_amount",creditsAmount  );
            fields.setData("debit_credits_count" ,creditsCount   );
          }
          else if ( planType.equals("EBT") ) 
          {
            fields.setData("ebt_vol_amount"    ,volAmount      );
            fields.setData("ebt_vol_count"     ,volCount       );
            fields.setData("ebt_sales_amount"  ,salesAmount    );
            fields.setData("ebt_sales_count"   ,salesCount     );
            fields.setData("ebt_credits_amount",creditsAmount  );
            fields.setData("ebt_credits_count" ,creditsCount   );
          }
          else if ( planType.equals("PL1") ) 
          {
            fields.setData("pl1_vol_amount"    ,volAmount      );
            fields.setData("pl1_vol_count"     ,volCount       );
            fields.setData("pl1_sales_amount"  ,salesAmount    );
            fields.setData("pl1_sales_count"   ,salesCount     );
            fields.setData("pl1_credits_amount",creditsAmount  );
            fields.setData("pl1_credits_count" ,creditsCount   );
          }
          else if ( planType.equals("PL2") ) 
          {
            fields.setData("pl2_vol_amount"    ,volAmount      );
            fields.setData("pl2_vol_count"     ,volCount       );
            fields.setData("pl2_sales_amount"  ,salesAmount    );
            fields.setData("pl2_sales_count"   ,salesCount     );
            fields.setData("pl2_credits_amount",creditsAmount  );
            fields.setData("pl2_credits_count" ,creditsCount   );
          }
          else if ( planType.equals("PL3") ) 
          {
            fields.setData("pl3_vol_amount"    ,volAmount      );
            fields.setData("pl3_vol_count"     ,volCount       );
            fields.setData("pl3_sales_amount"  ,salesAmount    );
            fields.setData("pl3_sales_count"   ,salesCount     );
            fields.setData("pl3_credits_amount",creditsAmount  );
            fields.setData("pl3_credits_count" ,creditsCount   );
          }
          else if ( planType.equals("VS$") || planType.equals("MC$") )
          {
            // treat cash advance items as special
            counts[9]   += volCount;
            amounts[9]  += volAmount;
          }
          /* Code modified by RS for LCR implementation  */
          else if ( planType.equals("VP") || planType.equals("MP") )
          {
          	amounts[0]  += volAmount;
            counts[0]   += volCount;
            amounts[1]  += salesAmount;
            counts[1]   += salesCount;
            amounts[2]  += creditsAmount;
            counts[2]   += creditsCount;
          if("VP".equals(planType))
          {
	    	fields.setData("vp_sales_amount"   ,salesAmount    );
	        fields.setData("vp_sales_count"    ,salesCount     );
	        fields.setData("vp_credits_amount" ,creditsAmount  );
	        fields.setData("vp_credits_count"  ,creditsCount   );
	        fields.setData("vp_vol_count"      ,volCount       );
	        fields.setData("vp_vol_amount"     ,volAmount      );
	          amounts[3]  += volAmount;
              counts[3]   += volCount;
              amounts[4]  += salesAmount;
              counts[4]   += salesCount;
              amounts[5]  += creditsAmount;
              counts[5]   += creditsCount;
           }
          else if("MP".equals(planType))
           {
			fields.setData("mp_sales_amount"   ,salesAmount    );
	        fields.setData("mp_sales_count"    ,salesCount     );
	        fields.setData("mp_credits_amount" ,creditsAmount  );
	        fields.setData("mp_credits_count"  ,creditsCount   );
	        fields.setData("mp_vol_count"      ,volCount       );
	        fields.setData("mp_vol_amount"     ,volAmount      );
	          amounts[6]  += volAmount;
              counts[6]   += volCount;
              amounts[7]  += salesAmount;
              counts[7]   += salesCount;
              amounts[8]  += creditsAmount;
              counts[8]   += creditsCount;
            }
          }
          /* Code modified by RS for LCR implementation  */
          else if ( planType.startsWith("V") || planType.startsWith("M") )
          {
            amounts[0]  += volAmount;
            counts[0]   += volCount;
            amounts[1]  += salesAmount;
            counts[1]   += salesCount;
            amounts[2]  += creditsAmount;
            counts[2]   += creditsCount;

            if ( planType.startsWith("V") ) 
            {
              log.debug("Visa plan type is  " + planType);
              if ( "VSDB".equals(planType) || "VD".equals(planType)) {
            	   vdSalesAmount += salesAmount;
            	   log.debug("Visa Debit Sale of " + salesAmount + " being added to vdSalesAmount...now making it : " + vdSalesAmount);
              } else {
                   vCSalesAmount += salesAmount;
            	   log.debug("Other Visa Sale of " + salesAmount + " being added to vCSalesAmount...now making it : " + vCSalesAmount);
              }
              amounts[3]  += volAmount;
              counts[3]   += volCount;
              amounts[4]  += salesAmount;
              counts[4]   += salesCount;
              amounts[5]  += creditsAmount;
              counts[5]   += creditsCount;
            }
            else    // assume MC
            {
              amounts[6]  += volAmount;
              counts[6]   += volCount;
              amounts[7]  += salesAmount;
              counts[7]   += salesCount;
              amounts[8]  += creditsAmount;
              counts[8]   += creditsCount;
            }
          }
        }
        resultSet.close();
        it.close();

        progress = "setting avg ticket: " + merchantId;
        if ( counts[1] > 0 )
        {
          avgTicket = MesMath.round( (amounts[1]/counts[1]), 2 );
        }          
        else
        {
          avgTicket = 0.0;
        }

        progress = "updating summary with sales: " + merchantId;
        fields.setData("vmc_vol_amount"         ,amounts[0]   );
        fields.setData("vmc_vol_count"          ,counts[0]    );
        fields.setData("vmc_sales_amount"       ,amounts[1]   );
        fields.setData("vmc_sales_count"        ,counts[1]    );
        fields.setData("vmc_credits_amount"     ,amounts[2]   );
        fields.setData("vmc_credits_count"      ,counts[2]    );
        
        fields.setData("visa_vol_amount"        ,amounts[3]   );
        fields.setData("visa_vol_count"         ,counts[3]    );
        fields.setData("visa_sales_amount"      ,amounts[4]   );
        fields.setData("visa_sales_count"       ,counts[4]    );
        fields.setData("visa_credits_amount"    ,amounts[5]   );
        fields.setData("visa_credits_count"     ,counts[5]    );
        
        fields.setData("mc_vol_amount"          ,amounts[6]   );
        fields.setData("mc_vol_count"           ,counts[6]    );
        fields.setData("mc_sales_amount"        ,amounts[7]   );
        fields.setData("mc_sales_count"         ,counts[7]    );
        fields.setData("mc_credits_amount"      ,amounts[8]   );
        fields.setData("mc_credits_count"       ,counts[8]    );
        
        fields.setData("vmc_cash_advance_amount",amounts[9]   );
        fields.setData("vmc_cash_advance_count" ,counts[9]    );
        fields.setData("vmc_avg_ticket"         ,avgTicket    );
      
        // load T&E data from the front end files starting 21-MAR-2003
        progress = "loading t&e data: " + merchantId;
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:890^11*/

//  ************************************************************
//  #sql [Ctx] { select  sum( nvl(sm.amex_sales_amount,0) - nvl(sm.amex_credits_amount,0) ),
//                      sum( nvl(sm.amex_sales_count,0)  + nvl(sm.amex_credits_count,0)  ),
//                      sum( nvl(sm.amex_sales_amount,0) ),
//                      sum( nvl(sm.amex_sales_count,0) ),
//                      sum( nvl(sm.amex_credits_amount,0) ),
//                      sum( nvl(sm.amex_credits_count,0) ),
//                      sum( nvl(sm.diners_sales_amount,0) - nvl(sm.diners_credits_amount,0) ),
//                      sum( nvl(sm.diners_sales_count,0)  + nvl(sm.diners_credits_count,0)  ),
//                      sum( nvl(sm.diners_sales_amount,0) ),
//                      sum( nvl(sm.diners_sales_count,0) ),
//                      sum( nvl(sm.diners_credits_amount,0) ),
//                      sum( nvl(sm.diners_credits_count,0) ),
//                      sum( nvl(sm.disc_sales_amount,0) - nvl(sm.disc_credits_amount,0) ),
//                      sum( nvl(sm.disc_sales_count,0)  + nvl(sm.disc_credits_count,0)  ),
//                      sum( nvl(sm.disc_sales_amount,0) ),
//                      sum( nvl(sm.disc_sales_count,0) ),
//                      sum( nvl(sm.disc_credits_amount,0) ),
//                      sum( nvl(sm.disc_credits_count,0) ),
//                      sum( nvl(sm.jcb_sales_amount,0) - nvl(sm.jcb_credits_amount,0) ),
//                      sum( nvl(sm.jcb_sales_count,0)  + nvl(sm.jcb_credits_count,0)  ),
//                      sum( nvl(sm.jcb_sales_amount,0) ),
//                      sum( nvl(sm.jcb_sales_count,0) ),
//                      sum( nvl(sm.jcb_credits_amount,0) ),
//                      sum( nvl(sm.jcb_credits_count,0) )
//              
//              from    daily_detail_file_ext_summary    sm
//              where   sm.merchant_number = :merchantId and
//                      sm.batch_date > '20-MAR-2003' and
//                      sm.batch_date between :beginDate and :currDate
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( nvl(sm.amex_sales_amount,0) - nvl(sm.amex_credits_amount,0) ),\n                    sum( nvl(sm.amex_sales_count,0)  + nvl(sm.amex_credits_count,0)  ),\n                    sum( nvl(sm.amex_sales_amount,0) ),\n                    sum( nvl(sm.amex_sales_count,0) ),\n                    sum( nvl(sm.amex_credits_amount,0) ),\n                    sum( nvl(sm.amex_credits_count,0) ),\n                    sum( nvl(sm.diners_sales_amount,0) - nvl(sm.diners_credits_amount,0) ),\n                    sum( nvl(sm.diners_sales_count,0)  + nvl(sm.diners_credits_count,0)  ),\n                    sum( nvl(sm.diners_sales_amount,0) ),\n                    sum( nvl(sm.diners_sales_count,0) ),\n                    sum( nvl(sm.diners_credits_amount,0) ),\n                    sum( nvl(sm.diners_credits_count,0) ),\n                    sum( nvl(sm.disc_sales_amount,0) - nvl(sm.disc_credits_amount,0) ),\n                    sum( nvl(sm.disc_sales_count,0)  + nvl(sm.disc_credits_count,0)  ),\n                    sum( nvl(sm.disc_sales_amount,0) ),\n                    sum( nvl(sm.disc_sales_count,0) ),\n                    sum( nvl(sm.disc_credits_amount,0) ),\n                    sum( nvl(sm.disc_credits_count,0) ),\n                    sum( nvl(sm.jcb_sales_amount,0) - nvl(sm.jcb_credits_amount,0) ),\n                    sum( nvl(sm.jcb_sales_count,0)  + nvl(sm.jcb_credits_count,0)  ),\n                    sum( nvl(sm.jcb_sales_amount,0) ),\n                    sum( nvl(sm.jcb_sales_count,0) ),\n                    sum( nvl(sm.jcb_credits_amount,0) ),\n                    sum( nvl(sm.jcb_credits_count,0) )\n             \n            from    daily_detail_file_ext_summary    sm\n            where   sm.merchant_number =  :1   and\n                    sm.batch_date > '20-MAR-2003' and\n                    sm.batch_date between  :2   and  :3 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 24) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(24,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amounts[0] = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[0] = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[1] = __sJT_rs.getDouble(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[1] = __sJT_rs.getInt(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[2] = __sJT_rs.getDouble(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[2] = __sJT_rs.getInt(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[3] = __sJT_rs.getDouble(7); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[3] = __sJT_rs.getInt(8); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[4] = __sJT_rs.getDouble(9); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[4] = __sJT_rs.getInt(10); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[5] = __sJT_rs.getDouble(11); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[5] = __sJT_rs.getInt(12); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[6] = __sJT_rs.getDouble(13); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[6] = __sJT_rs.getInt(14); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[7] = __sJT_rs.getDouble(15); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[7] = __sJT_rs.getInt(16); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[8] = __sJT_rs.getDouble(17); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[8] = __sJT_rs.getInt(18); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[9] = __sJT_rs.getDouble(19); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[9] = __sJT_rs.getInt(20); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[10] = __sJT_rs.getDouble(21); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[10] = __sJT_rs.getInt(22); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[11] = __sJT_rs.getDouble(23); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[11] = __sJT_rs.getInt(24); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:932^11*/                   

          progress = "updating t data: " + merchantId;
          for( int i = 0; i < TeFieldPrefixes.length; ++i )
          {
            prefix = TeFieldPrefixes[i];
            if ( fields.getField(prefix+"_amount").isBlank() )
            { 
              fields.setData(prefix+"_amount", amounts[i] ); 
              fields.setData   (prefix+"_count" , counts[i]  ); 
            }
          }            
        }
        catch ( Exception ee )
        {
          // ignore when there is no data found
        }

        progress = "calc discount income: " + merchantId;
      
        // add the calculated discount income
        /*@lineinfo:generated-code*//*@lineinfo:953^9*/

//  ************************************************************
//  #sql [Ctx] { select nvl(sum( pl.pl_correct_disc_amt ),0) 
//            from   monthly_extract_gn gn,
//                   monthly_extract_pl pl
//            where  gn.hh_merchant_number = :merchantId and
//                   gn.hh_active_date     = :activeDate and
//                   pl.hh_load_sec        = gn.hh_load_sec
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select nvl(sum( pl.pl_correct_disc_amt ),0)  \n          from   monthly_extract_gn gn,\n                 monthly_extract_pl pl\n          where  gn.hh_merchant_number =  :1   and\n                 gn.hh_active_date     =  :2   and\n                 pl.hh_load_sec        = gn.hh_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amounts[0] = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:961^9*/
        fields.setData("discount_inc_calc",amounts[0]);

        // MC Cross Border Fees
        progress = "MC cross border fees: " + merchantId;
      
        /*@lineinfo:generated-code*//*@lineinfo:967^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(ic.sales_amount)  ,0)                               as sales_amount, 
//                    nvl(sum(ic.sales_count)   ,0)                               as sales_count,
//                    nvl(sum(ic.credits_amount),0)                               as credits_amount,
//                    nvl(sum(ic.credits_count) ,0)                               as credits_count,
//                    nvl(sum(ic.sales_amount),0) - nvl(sum(ic.credits_amount),0) as vol_amount, 
//                    nvl(sum(ic.sales_count),0)  + nvl(sum(ic.credits_count),0)  as vol_count
//            
//            from    daily_detail_ic_summary   ic,
//                    daily_detail_file_ic_desc icd
//            where   ic.merchant_number  = :merchantId and
//                    ic.batch_date between :beginDate+1 and :currDate and
//                    icd.card_type = decode(substr(ic.card_type,1,1),'V','VS','M','MC','ER') and
//                    icd.ic_code = ic.ic_cat and
//                    ic.batch_date between icd.valid_date_begin and icd.valid_date_end and
//                    nvl(icd.cross_border_cat,'N') = 'Y'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(ic.sales_amount)  ,0)                               as sales_amount, \n                  nvl(sum(ic.sales_count)   ,0)                               as sales_count,\n                  nvl(sum(ic.credits_amount),0)                               as credits_amount,\n                  nvl(sum(ic.credits_count) ,0)                               as credits_count,\n                  nvl(sum(ic.sales_amount),0) - nvl(sum(ic.credits_amount),0) as vol_amount, \n                  nvl(sum(ic.sales_count),0)  + nvl(sum(ic.credits_count),0)  as vol_count\n           \n          from    daily_detail_ic_summary   ic,\n                  daily_detail_file_ic_desc icd\n          where   ic.merchant_number  =  :1   and\n                  ic.batch_date between  :2  +1 and  :3   and\n                  icd.card_type = decode(substr(ic.card_type,1,1),'V','VS','M','MC','ER') and\n                  icd.ic_code = ic.ic_cat and\n                  ic.batch_date between icd.valid_date_begin and icd.valid_date_end and\n                  nvl(icd.cross_border_cat,'N') = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 6) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(6,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amounts[0] = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[0] = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[1] = __sJT_rs.getDouble(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[1] = __sJT_rs.getInt(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[2] = __sJT_rs.getDouble(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[2] = __sJT_rs.getInt(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:984^9*/
        fields.setData("mc_cross_border_sales_count"    ,counts[0]  );
        fields.setData("mc_cross_border_sales_amount"   ,amounts[0] );
        fields.setData("mc_cross_border_credits_count"  ,counts[1]  );
        fields.setData("mc_cross_border_credits_amount" ,amounts[1] );
        fields.setData("mc_cross_border_vol_count"      ,counts[2]  );
        fields.setData("mc_cross_border_vol_amount"     ,amounts[2] );
          
        progress = "MC global support standard/electronic fees: " + merchantId;
        /*@lineinfo:generated-code*//*@lineinfo:993^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(decode(nvl(icd.foreign_standard,'N'),'Y',1,0) * ic.sales_amount),0)                         as std_sales_amount, 
//                    nvl(sum(decode(nvl(icd.foreign_standard,'N'),'Y',1,0) * ic.sales_count),0)                          as std_sales_count,
//                    nvl(sum(decode(nvl(icd.foreign_standard,'N'),'Y',1,0) * ic.credits_amount),0)                       as std_credits_amount,
//                    nvl(sum(decode(nvl(icd.foreign_standard,'N'),'Y',1,0) * ic.credits_count),0)                        as std_credits_count,
//                    nvl(sum(decode(nvl(icd.foreign_standard,'N'),'Y',1,0) * (ic.sales_amount - ic.credits_amount)),0)   as std_vol_amount, 
//                    nvl(sum(decode(nvl(icd.foreign_standard,'N'),'Y',1,0) * (ic.sales_count  + ic.credits_count)),0)    as std_vol_count,
//                    nvl(sum(decode(nvl(icd.foreign_electronic,'N'),'Y',1,0) * ic.sales_amount),0)                       as elec_sales_amount, 
//                    nvl(sum(decode(nvl(icd.foreign_electronic,'N'),'Y',1,0) * ic.sales_count),0)                        as elec_sales_count,
//                    nvl(sum(decode(nvl(icd.foreign_electronic,'N'),'Y',1,0) * ic.credits_amount),0)                     as elec_credits_amount,
//                    nvl(sum(decode(nvl(icd.foreign_electronic,'N'),'Y',1,0) * ic.credits_count),0)                      as elec_credits_count,
//                    nvl(sum(decode(nvl(icd.foreign_electronic,'N'),'Y',1,0) *(ic.sales_amount - ic.credits_amount)),0)  as elec_vol_amount, 
//                    nvl(sum(decode(nvl(icd.foreign_electronic,'N'),'Y',1,0) *(ic.sales_count  + ic.credits_count)),0)   as elec_vol_count        
//            
//            from    daily_detail_ic_summary   ic,
//                    daily_detail_file_ic_desc icd
//            where   ic.merchant_number  = :merchantId and
//                    ic.batch_date between :beginDate+1 and :currDate and
//                    icd.card_type = decode(substr(ic.card_type,1,1),'V','VS','M','MC','ER') and
//                    icd.ic_code = ic.ic_cat and
//                    ic.batch_date between icd.valid_date_begin and icd.valid_date_end and
//                    (nvl(icd.foreign_standard,'N') = 'Y' or nvl(icd.foreign_electronic,'N') = 'Y')
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(decode(nvl(icd.foreign_standard,'N'),'Y',1,0) * ic.sales_amount),0)                         as std_sales_amount, \n                  nvl(sum(decode(nvl(icd.foreign_standard,'N'),'Y',1,0) * ic.sales_count),0)                          as std_sales_count,\n                  nvl(sum(decode(nvl(icd.foreign_standard,'N'),'Y',1,0) * ic.credits_amount),0)                       as std_credits_amount,\n                  nvl(sum(decode(nvl(icd.foreign_standard,'N'),'Y',1,0) * ic.credits_count),0)                        as std_credits_count,\n                  nvl(sum(decode(nvl(icd.foreign_standard,'N'),'Y',1,0) * (ic.sales_amount - ic.credits_amount)),0)   as std_vol_amount, \n                  nvl(sum(decode(nvl(icd.foreign_standard,'N'),'Y',1,0) * (ic.sales_count  + ic.credits_count)),0)    as std_vol_count,\n                  nvl(sum(decode(nvl(icd.foreign_electronic,'N'),'Y',1,0) * ic.sales_amount),0)                       as elec_sales_amount, \n                  nvl(sum(decode(nvl(icd.foreign_electronic,'N'),'Y',1,0) * ic.sales_count),0)                        as elec_sales_count,\n                  nvl(sum(decode(nvl(icd.foreign_electronic,'N'),'Y',1,0) * ic.credits_amount),0)                     as elec_credits_amount,\n                  nvl(sum(decode(nvl(icd.foreign_electronic,'N'),'Y',1,0) * ic.credits_count),0)                      as elec_credits_count,\n                  nvl(sum(decode(nvl(icd.foreign_electronic,'N'),'Y',1,0) *(ic.sales_amount - ic.credits_amount)),0)  as elec_vol_amount, \n                  nvl(sum(decode(nvl(icd.foreign_electronic,'N'),'Y',1,0) *(ic.sales_count  + ic.credits_count)),0)   as elec_vol_count        \n           \n          from    daily_detail_ic_summary   ic,\n                  daily_detail_file_ic_desc icd\n          where   ic.merchant_number  =  :1   and\n                  ic.batch_date between  :2  +1 and  :3   and\n                  icd.card_type = decode(substr(ic.card_type,1,1),'V','VS','M','MC','ER') and\n                  icd.ic_code = ic.ic_cat and\n                  ic.batch_date between icd.valid_date_begin and icd.valid_date_end and\n                  (nvl(icd.foreign_standard,'N') = 'Y' or nvl(icd.foreign_electronic,'N') = 'Y')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 12) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(12,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amounts[0] = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[0] = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[1] = __sJT_rs.getDouble(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[1] = __sJT_rs.getInt(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[2] = __sJT_rs.getDouble(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[2] = __sJT_rs.getInt(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[3] = __sJT_rs.getDouble(7); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[3] = __sJT_rs.getInt(8); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[4] = __sJT_rs.getDouble(9); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[4] = __sJT_rs.getInt(10); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[5] = __sJT_rs.getDouble(11); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[5] = __sJT_rs.getInt(12); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1017^9*/
        fields.setData("mc_foreign_std_sales_count"     ,counts[0]  );
        fields.setData("mc_foreign_std_sales_amount"    ,amounts[0] );
        fields.setData("mc_foreign_std_credits_count"   ,counts[1]  );
        fields.setData("mc_foreign_std_credits_amount"  ,amounts[1] );
        fields.setData("mc_foreign_std_vol_count"       ,counts[2]  );
        fields.setData("mc_foreign_std_vol_amount"      ,amounts[2] );
        fields.setData("mc_foreign_elec_sales_count"    ,counts[3]  );
        fields.setData("mc_foreign_elec_sales_amount"   ,amounts[3] );
        fields.setData("mc_foreign_elec_credits_count"  ,counts[4]  );
        fields.setData("mc_foreign_elec_credits_amount" ,amounts[4] );
        fields.setData("mc_foreign_elec_vol_count"      ,counts[5]  );
        fields.setData("mc_foreign_elec_vol_amount"     ,amounts[5] );
        fields.setData("mc_intl_assessment_expense"     ,((amounts[0] + amounts[3]) * 0.0045) );

        progress = "getting auth counts: " + merchantId;
        /*@lineinfo:generated-code*//*@lineinfo:1033^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ INDEX (gn idx_mon_ext_gn_merch_date) */
//                   nvl(ap.a1_plan_type,'UK')  as plan_type,
//                   sum( ap.auth_count_total ) as auth_count
//            from   monthly_extract_gn gn,
//                   monthly_extract_ap ap
//            where  gn.hh_merchant_number = :merchantId and
//                   gn.hh_active_date     = :activeDate and
//                   ap.hh_load_sec        = gn.hh_load_sec and
//                   not ap.a1_media_type in ('VO','AR') -- ignore voice and ARU, bill as dialpay
//            group by ap.a1_plan_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ INDEX (gn idx_mon_ext_gn_merch_date) */\n                 nvl(ap.a1_plan_type,'UK')  as plan_type,\n                 sum( ap.auth_count_total ) as auth_count\n          from   monthly_extract_gn gn,\n                 monthly_extract_ap ap\n          where  gn.hh_merchant_number =  :1   and\n                 gn.hh_active_date     =  :2   and\n                 ap.hh_load_sec        = gn.hh_load_sec and\n                 not ap.a1_media_type in ('VO','AR') -- ignore voice and ARU, bill as dialpay\n          group by ap.a1_plan_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.startup.MonthlyExtractSummarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1045^9*/
        resultSet = it.getResultSet();
      
        // reset the auth summary variables
        for( int i = 0; i < counts.length; ++i ) { counts[i] = 0; }
        for( int i = 0; i < amounts.length; ++i ) { amounts[i] = 0; }

        progress = "updating summary auth counts: " + merchantId;
        while( resultSet.next() )
        {
          planType  = resultSet.getString("plan_type");
          counts[0] = resultSet.getInt("auth_count");
      
          if ( planType.equals("AM") ) 
          {
            fields.setData("amex_auth_count",counts[0]);
            amDirAuth = counts[0];
          }            
          else if ( planType.equals("DS") ) 
          {
            fields.setData("disc_auth_count",counts[0]);
          }            
          else if ( planType.equals("DB") ) 
          {
            fields.setData("debit_auth_count",counts[0]);
          }            
          else if ( planType.equals("JC") ) 
          {
            fields.setData("jcb_auth_count",counts[0]);
          }            
          else if ( planType.equals("DC") || planType.equals("CB") ) 
          {
            counts[4] += counts[0];
          }            
          else if ( planType.equals("VS") ) 
          {
            counts[1] += counts[0];
            counts[2] += counts[0];      
          }            
          else if ( planType.equals("MC") ) 
          {
            counts[1] += counts[0];
            counts[3] += counts[0];  
          }            
          // collect the total # of auths
          counts[5] += counts[0];
        }   // end result set loop          
        resultSet.close();
        it.close();

        progress = "updating bank auth counts: " + merchantId;
        fields.setData("vmc_auth_count"   ,counts[1]);
        fields.setData("visa_auth_count"  ,counts[2]);
        fields.setData("mc_auth_count"    ,counts[3]);
        fields.setData("dinr_auth_count"  ,counts[4]);
        fields.setData("auth_count_total" ,counts[5]);
        
        mcAuthCount = counts[3];
        
        progress = "updating nonbank auth counts: " + merchantId;
        // update the third party auth counts
        /*@lineinfo:generated-code*//*@lineinfo:1106^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(case when ap.a1_plan_type in ('VS','MC','VP','MP') then 1 else 0 end
//                        * case when ap.a1_vendor_code != 'VI' then 1 else 0 end
//                        * ap.auth_count_total),0)         as vmc_auth_count_ext,
//                    nvl(sum(case when ap.a1_plan_type in ('AM') then 1 else 0 end
//                        * case when ap.a1_vendor_code != 'VI' then 1 else 0 end
//                        * ap.auth_count_total),0)         as amex_auth_count_ext,
//                    nvl(sum(case when ap.a1_plan_type in ('DC','CB') then 1 else 0 end
//                        * case when ap.a1_vendor_code != 'VI' then 1 else 0 end
//                        * ap.auth_count_total),0)         as dinr_auth_count_ext,
//                    nvl(sum(case when ap.a1_plan_type in ('DS') then 1 else 0 end
//                        * case when ap.a1_vendor_code != 'VI' then 1 else 0 end
//                        * ap.auth_count_total),0)         as disc_auth_count_ext,
//                    nvl(sum(case when ap.a1_plan_type in ('JC') then 1 else 0 end
//                        * case when ap.a1_vendor_code != 'VI' then 1 else 0 end
//                        * ap.auth_count_total),0)         as jcb_auth_count_ext,
//                    nvl(sum(case when ap.a1_plan_type in ('DB') then 1 else 0 end
//                        * case when ap.a1_vendor_code != 'VI' then 1 else 0 end
//                        * ap.auth_count_total),0)         as debit_auth_count_ext,
//                    nvl(sum(case when ap.a1_vendor_code != 'VI' then 1 else 0 end
//                        * ap.auth_count_total),0)         as auth_count_total_ext,
//                    nvl(sum( nvl(ap.a1_avs_number,0) ),0) as avs_count,
//                    nvl(sum(case when ap.a1_media_type = 'VO' then 1 else 0 end
//                        * ap.auth_count_total),0)         as voice_auth_count,
//                    nvl(sum(case when ap.a1_media_type = 'AR' then 1 else 0 end
//                        * ap.auth_count_total),0)         as aru_count
//            
//            from    monthly_extract_ap ap
//            where   ap.hh_load_sec = :LoadSec 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(case when ap.a1_plan_type in ('VS','MC','VP','MP') then 1 else 0 end\n                      * case when ap.a1_vendor_code != 'VI' then 1 else 0 end\n                      * ap.auth_count_total),0)         as vmc_auth_count_ext,\n                  nvl(sum(case when ap.a1_plan_type in ('AM') then 1 else 0 end\n                      * case when ap.a1_vendor_code != 'VI' then 1 else 0 end\n                      * ap.auth_count_total),0)         as amex_auth_count_ext,\n                  nvl(sum(case when ap.a1_plan_type in ('DC','CB') then 1 else 0 end\n                      * case when ap.a1_vendor_code != 'VI' then 1 else 0 end\n                      * ap.auth_count_total),0)         as dinr_auth_count_ext,\n                  nvl(sum(case when ap.a1_plan_type in ('DS') then 1 else 0 end\n                      * case when ap.a1_vendor_code != 'VI' then 1 else 0 end\n                      * ap.auth_count_total),0)         as disc_auth_count_ext,\n                  nvl(sum(case when ap.a1_plan_type in ('JC') then 1 else 0 end\n                      * case when ap.a1_vendor_code != 'VI' then 1 else 0 end\n                      * ap.auth_count_total),0)         as jcb_auth_count_ext,\n                  nvl(sum(case when ap.a1_plan_type in ('DB') then 1 else 0 end\n                      * case when ap.a1_vendor_code != 'VI' then 1 else 0 end\n                      * ap.auth_count_total),0)         as debit_auth_count_ext,\n                  nvl(sum(case when ap.a1_vendor_code != 'VI' then 1 else 0 end\n                      * ap.auth_count_total),0)         as auth_count_total_ext,\n                  nvl(sum( nvl(ap.a1_avs_number,0) ),0) as avs_count,\n                  nvl(sum(case when ap.a1_media_type = 'VO' then 1 else 0 end\n                      * ap.auth_count_total),0)         as voice_auth_count,\n                  nvl(sum(case when ap.a1_media_type = 'AR' then 1 else 0 end\n                      * ap.auth_count_total),0)         as aru_count\n           \n          from    monthly_extract_ap ap\n          where   ap.hh_load_sec =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 10) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(10,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[1] = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[2] = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[3] = __sJT_rs.getInt(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[4] = __sJT_rs.getInt(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[5] = __sJT_rs.getInt(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[6] = __sJT_rs.getInt(7); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[7] = __sJT_rs.getInt(8); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[8] = __sJT_rs.getInt(9); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[9] = __sJT_rs.getInt(10); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1138^9*/                  
        fields.setData("vmc_auth_count_ext"   , counts[0] );
        fields.setData("amex_auth_count_ext"  , counts[1] );
        fields.setData("dinr_auth_count_ext"  , counts[2] );
        fields.setData("disc_auth_count_ext"  , counts[3] );
        fields.setData("jcb_auth_count_ext"   , counts[4] );
        fields.setData("debit_auth_count_ext" , counts[5] );
        fields.setData("auth_count_total_ext" , counts[6] );
        fields.setData("avs_count"            , counts[7] );
        fields.setData("voice_auth_count"     , counts[8] );
        fields.setData("aru_auth_count"       , counts[9] );
        
        // add the AVS auth count to the summary table
        progress = "getting trident auth counts: " + merchantId;
        /*@lineinfo:generated-code*//*@lineinfo:1152^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum( decode(nvl(tpc.trident_payment_gateway,'N'),'Y',1,0) ),0),
//                    count( auth.merchant_number ),
//                    nvl(sum( decode(nvl(auth.card_type,'ER'),'AM',1,0) ),0),
//                    nvl(sum( decode(nvl(auth.card_type,'ER'),'DC',1,0) ),0),
//                    nvl(sum( decode(nvl(auth.card_type,'ER'),'DS',1,0) ),0),
//                    nvl(sum( decode(nvl(auth.card_type,'ER'),'JC',1,0) ),0),
//                    nvl(sum( decode(nvl(auth.card_type,'ER'),'VS',1,'MC',1,0) ),0),
//                    nvl(sum( decode(nvl(auth.card_type,'ER'),'VS',1,0) ),0),
//                    nvl(sum( decode(nvl(auth.card_type,'ER'),'MC',1,0) ),0),
//                    nvl(sum( decode(nvl(auth.card_type,'ER'),'DB',1,'EB',1,0) ),0)
//            
//            from    tc33_trident            auth,
//                    trident_profile         tp,
//                    trident_product_codes   tpc    
//            where   auth.merchant_number = :merchantId and
//                    auth.transaction_date between :beginDate and (:currDate-1) and
//                    tp.terminal_id = auth.terminal_id and
//                    tpc.product_code = tp.product_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum( decode(nvl(tpc.trident_payment_gateway,'N'),'Y',1,0) ),0),\n                  count( auth.merchant_number ),\n                  nvl(sum( decode(nvl(auth.card_type,'ER'),'AM',1,0) ),0),\n                  nvl(sum( decode(nvl(auth.card_type,'ER'),'DC',1,0) ),0),\n                  nvl(sum( decode(nvl(auth.card_type,'ER'),'DS',1,0) ),0),\n                  nvl(sum( decode(nvl(auth.card_type,'ER'),'JC',1,0) ),0),\n                  nvl(sum( decode(nvl(auth.card_type,'ER'),'VS',1,'MC',1,0) ),0),\n                  nvl(sum( decode(nvl(auth.card_type,'ER'),'VS',1,0) ),0),\n                  nvl(sum( decode(nvl(auth.card_type,'ER'),'MC',1,0) ),0),\n                  nvl(sum( decode(nvl(auth.card_type,'ER'),'DB',1,'EB',1,0) ),0)\n           \n          from    tc33_trident            auth,\n                  trident_profile         tp,\n                  trident_product_codes   tpc    \n          where   auth.merchant_number =  :1   and\n                  auth.transaction_date between  :2   and ( :3  -1) and\n                  tp.terminal_id = auth.terminal_id and\n                  tpc.product_code = tp.product_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 10) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(10,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[1] = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[2] = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[3] = __sJT_rs.getInt(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[4] = __sJT_rs.getInt(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[5] = __sJT_rs.getInt(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[6] = __sJT_rs.getInt(7); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[7] = __sJT_rs.getInt(8); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[8] = __sJT_rs.getInt(9); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[9] = __sJT_rs.getInt(10); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1173^9*/                  
        fields.setData("auth_count_total_api"     , counts[0] );
        fields.setData("auth_count_total_trident" , counts[1] );
        fields.setData("amex_auth_count_trident"  , counts[2] );
        fields.setData("dinr_auth_count_trident"  , counts[3] );
        fields.setData("disc_auth_count_trident"  , counts[4] );
        fields.setData("jcb_auth_count_trident"   , counts[5] );
        fields.setData("vmc_auth_count_trident"   , counts[6] );
        fields.setData("visa_auth_count_trident"  , counts[7] );
        fields.setData("mc_auth_count_trident"    , counts[8] );
        fields.setData("debit_auth_count_trident" , counts[9] );
        amDirAuthTr = counts[2];
        /*
         * This part of code is added to support newly added 
         * columns amex_direct_auth_count_trident and amex_direct_auth_count_vital 
         * in monthly_extract_summary
         */
        progress = "getting Amex direct vital and trident count: " + merchantId;
        /*@lineinfo:generated-code*//*@lineinfo:1191^9*/

//  ************************************************************
//  #sql [Ctx] it = { select nvl(mif.amex_plan,'none') as amex_plan
//              from   mif
//              where merchant_number = :merchantId   
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select nvl(mif.amex_plan,'none') as amex_plan\n            from   mif\n            where merchant_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.startup.MonthlyExtractSummarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1196^9*/
        resultSet = it.getResultSet();
        if( resultSet.next() )
        {
          if ( ("DN").equals( resultSet.getString("amex_plan")) ) 
          {
              fields.setData("amex_direct_auth_count_trident"     , amDirAuthTr );
              fields.setData("amex_direct_auth_count_vital"       , amDirAuth );
          }
        }
        resultSet.close();
        it.close();
        progress = "getting moto retail ind: " + merchantId;
        // add the moto retail indicator
        /*@lineinfo:generated-code*//*@lineinfo:1210^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum( decode( nvl(dt.mo_to_indicator,' '),
//                             ' ', 0, 1 ) ),0)       as keyed_count,
//                    count(1)                        as total_count
//            
//            from    daily_detail_file_dt      dt
//            where   dt.merchant_account_number = :merchantId and
//                    dt.batch_date between :beginDate and (:currDate-1) and
//                    substr(dt.card_type,1,1) in ('V','M') and
//                    dt.debit_credit_indicator = 'D'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum( decode( nvl(dt.mo_to_indicator,' '),\n                           ' ', 0, 1 ) ),0)       as keyed_count,\n                  count(1)                        as total_count\n           \n          from    daily_detail_file_dt      dt\n          where   dt.merchant_account_number =  :1   and\n                  dt.batch_date between  :2   and ( :3  -1) and\n                  substr(dt.card_type,1,1) in ('V','M') and\n                  dt.debit_credit_indicator = 'D'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[1] = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1221^9*/
        fields.setData("moto_merchant",((counts[1] > 0 && ((double)counts[0]/(double)counts[1]) >= 0.50) ? "Y" : "N"));
        
        //add keyed and swiped count and amount
        progress = "getting keyed and swiped count/amount: " + merchantId;
        try{
        /*@lineinfo:generated-code*//*@lineinfo:1227^9*/

//  ************************************************************
//  #sql [Ctx] { select nvl(sum(decode (dt.pos_entry_mode, '00', 1, '01', 1, '04', 1, 'NA', 1, '81', 1, 0)), 0) as keyed_count,
//                  nvl(sum(decode (dt.pos_entry_mode, '02', 1, '03', 1, '04', 1, '05', 1, '06', 1, '90', 1, '95', 1, 0)), 0) as swiped_count,
//                  nvl(sum(decode(dt.pos_entry_mode,
//                  '00',dt.transaction_amount,  
//                  '01',dt.transaction_amount,
//                  '04',dt.transaction_amount,  
//                  '81',dt.transaction_amount,
//                  'NA',dt.transaction_amount,  
//                   0)), 0)                    as keyed_amount, 
//  
//                  nvl(sum(decode(dt.pos_entry_mode,
//                  '02',dt.transaction_amount,
//                  '03',dt.transaction_amount, 
//                  '04',dt.transaction_amount, 
//                  '05',dt.transaction_amount, 
//                  '06',dt.transaction_amount,
//                  '90',dt.transaction_amount,
//                  '95',dt.transaction_amount, 
//                   0)), 0)                    as swiped_amount
//              
//              from daily_detail_file_dt dt
//              where dt.batch_date between :beginDate and (:currDate-1)
//              and merchant_account_number =  :merchantId 
//             group by merchant_account_number
//            };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select nvl(sum(decode (dt.pos_entry_mode, '00', 1, '01', 1, '04', 1, 'NA', 1, '81', 1, 0)), 0) as keyed_count,\n                nvl(sum(decode (dt.pos_entry_mode, '02', 1, '03', 1, '04', 1, '05', 1, '06', 1, '90', 1, '95', 1, 0)), 0) as swiped_count,\n                nvl(sum(decode(dt.pos_entry_mode,\n                '00',dt.transaction_amount,  \n                '01',dt.transaction_amount,\n                '04',dt.transaction_amount,  \n                '81',dt.transaction_amount,\n                'NA',dt.transaction_amount,  \n                 0)), 0)                    as keyed_amount, \n\n                nvl(sum(decode(dt.pos_entry_mode,\n                '02',dt.transaction_amount,\n                '03',dt.transaction_amount, \n                '04',dt.transaction_amount, \n                '05',dt.transaction_amount, \n                '06',dt.transaction_amount,\n                '90',dt.transaction_amount,\n                '95',dt.transaction_amount, \n                 0)), 0)                    as swiped_amount\n             \n            from daily_detail_file_dt dt\n            where dt.batch_date between  :1   and ( :2  -1)\n            and merchant_account_number =   :3   \n           group by merchant_account_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,beginDate);
   __sJT_st.setDate(2,currDate);
   __sJT_st.setLong(3,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[1] = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[0] = __sJT_rs.getDouble(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[1] = __sJT_rs.getDouble(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1253^10*/
        }catch(Exception e){
            counts[0] = 0;
            counts[1] = 0;
            amounts[0] = 0;
            amounts[1] = 0;
            log.debug("Exception while calculating key and swipe count and amount for MID " + merchantId);
            logEntry("run(" + LoadSec + ")",e.toString());
        }
         //keyed and swiped count/amount
         fields.setData("tran_count_keyed",counts[0]);
         fields.setData("tran_count_swiped",counts[1]);
         fields.setData("tot_amount_keyed",amounts[0]);
         fields.setData("tot_amount_swiped",amounts[1]);
        //*************************************
        //*   chargebacks & retrievals        
        //*************************************
        progress = "getting retrievals: " + merchantId;
        /*@lineinfo:generated-code*//*@lineinfo:1271^9*/

//  ************************************************************
//  #sql [Ctx] { select  count( rt.tran_amount ),
//                    nvl(sum( rt.tran_amount ),0) 
//            
//            from    network_retrievals rt
//            where   rt.merchant_number = :merchantId and
//                    rt.incoming_date between :beginDate and (:currDate-1)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( rt.tran_amount ),\n                  nvl(sum( rt.tran_amount ),0) \n           \n          from    network_retrievals rt\n          where   rt.merchant_number =  :1   and\n                  rt.incoming_date between  :2   and ( :3  -1)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[0] = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1279^9*/
        fields.setData("retrieval_count",counts[0]);
        fields.setData("retrieval_amount",amounts[0]);

        progress = "getting chargebacks: " + merchantId;
        // insert the total number of incoming chargebacks
        /*@lineinfo:generated-code*//*@lineinfo:1285^9*/

//  ************************************************************
//  #sql [Ctx] { select count(1)                         as cb_count,
//                   nvl(sum( cb.tran_amount ),0)     as cb_amount,
//                   nvl( sum(decode(cb.first_time_chargeback,'Y',1,0)),0 )
//                                                    as ft_count,
//                   nvl(sum(decode(cb.first_time_chargeback,'Y',1,0) * cb.tran_amount),0) 
//                                                    as ft_amount
//            
//            from   network_chargebacks cb
//            where  cb.merchant_number = :merchantId and
//                   cb.incoming_date between :beginDate and (:currDate-1)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(1)                         as cb_count,\n                 nvl(sum( cb.tran_amount ),0)     as cb_amount,\n                 nvl( sum(decode(cb.first_time_chargeback,'Y',1,0)),0 )\n                                                  as ft_count,\n                 nvl(sum(decode(cb.first_time_chargeback,'Y',1,0) * cb.tran_amount),0) \n                                                  as ft_amount\n           \n          from   network_chargebacks cb\n          where  cb.merchant_number =  :1   and\n                 cb.incoming_date between  :2   and ( :3  -1)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[0] = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[1] = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[1] = __sJT_rs.getDouble(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1298^9*/                 
        fields.setData("incoming_chargeback_count"    , counts[0]  );
        fields.setData("incoming_chargeback_amount"   , amounts[0] );
        fields.setData("incoming_ft_chargeback_count" , counts[1]  );
        fields.setData("incoming_ft_chargeback_amount", amounts[1] );

        progress = "getting service call node: " + merchantId;
        // each agent bank can have their own set of excluded call types
        // several banks get the first 30 days (from activation) free
        /*@lineinfo:generated-code*//*@lineinfo:1307^9*/

//  ************************************************************
//  #sql [Ctx] { select  get_service_call_node( :merchantId ),
//                    delay_service_calls( :merchantId ) 
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  get_service_call_node(  :1   ),\n                  delay_service_calls(  :2   ) \n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   node = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   delayCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1313^9*/          

        progress = "counting merch calls: " + merchantId;
        /*@lineinfo:generated-code*//*@lineinfo:1316^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1) 
//            
//            from    mif                     mf,
//                    service_calls           sc,
//                    service_call_exemptions sce
//            where   mf.merchant_number = :merchantId 
//                    and sc.merchant_number = mf.merchant_number 
//                    and nvl(sc.client_generated,'N') = 'N' 
//                    and trunc(sc.CALL_DATE) between :beginDate and (:currDate-1) 
//                    and nvl(sc.billable,'Y') = 'Y' 
//                    and
//                    (
//                      :delayCount = 0 or
//                      trunc(sc.call_date) > (mf.activation_date + :delayCount)
//                    ) 
//                    and sce.hierarchy_node(+) = :node 
//                    and sce.call_type(+) = sc.type 
//                    and sce.call_type is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1) \n           \n          from    mif                     mf,\n                  service_calls           sc,\n                  service_call_exemptions sce\n          where   mf.merchant_number =  :1   \n                  and sc.merchant_number = mf.merchant_number \n                  and nvl(sc.client_generated,'N') = 'N' \n                  and trunc(sc.CALL_DATE) between  :2   and ( :3  -1) \n                  and nvl(sc.billable,'Y') = 'Y' \n                  and\n                  (\n                     :4   = 0 or\n                    trunc(sc.call_date) > (mf.activation_date +  :5  )\n                  ) \n                  and sce.hierarchy_node(+) =  :6   \n                  and sce.call_type(+) = sc.type \n                  and sce.call_type is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   __sJT_st.setInt(4,delayCount);
   __sJT_st.setInt(5,delayCount);
   __sJT_st.setLong(6,node);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1336^9*/
        fields.setData("service_call_count", counts[0] );

        progress = "getting ach rejects: " + merchantId;
        // add in ACH rejects
        /*@lineinfo:generated-code*//*@lineinfo:1341^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1) 
//            from    ach_rejects      ach
//            where   ach.merchant_number = :merchantId 
//                    and ach.settled_date between :beginDate and (:currDate-1) 
//                    and ( substr(ach.reason_code,1,1) = 'C' or ach.sec = 'COR' )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)  \n          from    ach_rejects      ach\n          where   ach.merchant_number =  :1   \n                  and ach.settled_date between  :2   and ( :3  -1) \n                  and ( substr(ach.reason_code,1,1) = 'C' or ach.sec = 'COR' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1348^9*/                   
        fields.setData("ach_reject_count", counts[0] );

        progress = "getting dcs adjustments: " + merchantId;
        // discount and interchange dce adjustments
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1355^11*/

//  ************************************************************
//  #sql [Ctx] { select count(1),
//                     sum( ( adj.adjustment_amount *
//                            decode(adj.debit_credit_ind,'C',-1,1)) )   
//              
//              from   daily_detail_file_adjustment  adj,
//                     bank_dce_ref_to_gl            gl
//              where  adj.merchant_account_number = :merchantId and
//                     adj.batch_date between :beginDate and (:currDate-1) and
//                     adj.transacction_code in (9072,9078) and
//    --                 ( adj.transacction_code in (9072,9078) or -- always take 9072 and 9078
//    --                   (adj.transacction_code = 9071 and       -- only take 9071 if the acct
//    --                    rec.sic_code in ( 6010,6011 )) )  and  -- is cash advance (6010,6011)
//                     ( gl.bank_number = :bankNumber or
//                       gl.bank_number is null ) and
//                     gl.DCE_REF_NUM = substr(adj.reference_number,length(adj.reference_number)-1,2) and
//                     gl.revenue_offset = 'D'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(1),\n                   sum( ( adj.adjustment_amount *\n                          decode(adj.debit_credit_ind,'C',-1,1)) )   \n             \n            from   daily_detail_file_adjustment  adj,\n                   bank_dce_ref_to_gl            gl\n            where  adj.merchant_account_number =  :1   and\n                   adj.batch_date between  :2   and ( :3  -1) and\n                   adj.transacction_code in (9072,9078) and\n  --                 ( adj.transacction_code in (9072,9078) or -- always take 9072 and 9078\n  --                   (adj.transacction_code = 9071 and       -- only take 9071 if the acct\n  --                    rec.sic_code in ( 6010,6011 )) )  and  -- is cash advance (6010,6011)\n                   ( gl.bank_number =  :4   or\n                     gl.bank_number is null ) and\n                   gl.DCE_REF_NUM = substr(adj.reference_number,length(adj.reference_number)-1,2) and\n                   gl.revenue_offset = 'D'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   __sJT_st.setInt(4,bankNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[0] = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1373^11*/                   
        }
        catch( Exception ee )
        {
          amounts[0] = 0;
          counts[0]  = 0;
        }
        fields.setData("disc_ic_dce_adj_count"  , counts[0] );
        fields.setData("disc_ic_dce_adj_amount" , amounts[0]);

        // fee dce adjustments
        progress = "getting fee dce adj: " + merchantId;
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1387^11*/

//  ************************************************************
//  #sql [Ctx] { select count( adj.adjustment_amount ),
//                     sum( ( adj.adjustment_amount *
//                            decode(adj.debit_credit_ind,'C',-1,1)) )  
//              
//              from   daily_detail_file_adjustment  adj,
//                     bank_dce_ref_to_gl            gl
//              where  adj.merchant_account_number = :merchantId and
//                     adj.batch_date between :beginDate and (:currDate-1) and
//                   ( adj.transacction_code in (9072,9078) or -- always take 9072 and 9078
//                     (adj.transacction_code = 9071 and       -- only take 9071 if the acct
//                      rec.sic_code in ( 6010,6011 )) )  and  -- is cash advance (6010,6011)
//                     ( gl.bank_number = :bankNumber or
//                       gl.bank_number is null ) and
//                     gl.DCE_REF_NUM = substr(adj.reference_number,length(adj.reference_number)-1,2) and
//                     gl.revenue_offset = 'F'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count( adj.adjustment_amount ),\n                   sum( ( adj.adjustment_amount *\n                          decode(adj.debit_credit_ind,'C',-1,1)) )  \n             \n            from   daily_detail_file_adjustment  adj,\n                   bank_dce_ref_to_gl            gl\n            where  adj.merchant_account_number =  :1   and\n                   adj.batch_date between  :2   and ( :3  -1) and\n                 ( adj.transacction_code in (9072,9078) or -- always take 9072 and 9078\n                   (adj.transacction_code = 9071 and       -- only take 9071 if the acct\n                    rec.sic_code in ( 6010,6011 )) )  and  -- is cash advance (6010,6011)\n                   ( gl.bank_number =  :4   or\n                     gl.bank_number is null ) and\n                   gl.DCE_REF_NUM = substr(adj.reference_number,length(adj.reference_number)-1,2) and\n                   gl.revenue_offset = 'F'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   __sJT_st.setInt(4,bankNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[0] = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1404^11*/
        }
        catch( Exception ee )
        {
          amounts[0] = 0;
          counts[0]  = 0;
        }          

        // bankserv fee adjustments
        progress = "bankserv fee adj: " + merchantId;
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1416^11*/

//  ************************************************************
//  #sql [Ctx] { select  count( adj.amount ),
//                      sum( ( adj.amount *
//                              decode(adj.credit_debit_ind,'C',-1,1)) )  
//              
//              from    bankserv_ach_detail          adj,
//                      bankserv_ach_descriptions    bad
//              where   adj.merchant_number = :merchantId and
//                      trunc(adj.date_transmitted) between :beginDate and (:currDate-1) and
//                      bad.description = adj.entry_description and
//                      bad.revenue_offset = 'F'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( adj.amount ),\n                    sum( ( adj.amount *\n                            decode(adj.credit_debit_ind,'C',-1,1)) )  \n             \n            from    bankserv_ach_detail          adj,\n                    bankserv_ach_descriptions    bad\n            where   adj.merchant_number =  :1   and\n                    trunc(adj.date_transmitted) between  :2   and ( :3  -1) and\n                    bad.description = adj.entry_description and\n                    bad.revenue_offset = 'F'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[1] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[1] = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1428^11*/
        }
        catch( Exception ee )
        {           
          amounts[1] = 0;
          counts[1]  = 0;
        }          
        
        progress = "ach rev adj " + merchantId;
        /*@lineinfo:generated-code*//*@lineinfo:1437^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(ar.merchant_number),
//                    nvl(sum(ar.amount),0) 
//            
//            from    ach_rejects               ar,
//                    ach_reject_status         ars
//            where   ar.merchant_number = :merchantId and
//                    ar.report_date between :beginDate and :currDate-1 and
//                    substr(ar.reason_code,1,1) != 'C' and
//                    ars.reject_seq_num = ar.reject_seq_num and
//                    ars.reject_status = 'REVERSAL_OF_REVENUE'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ar.merchant_number),\n                  nvl(sum(ar.amount),0) \n           \n          from    ach_rejects               ar,\n                  ach_reject_status         ars\n          where   ar.merchant_number =  :1   and\n                  ar.report_date between  :2   and  :3  -1 and\n                  substr(ar.reason_code,1,1) != 'C' and\n                  ars.reject_seq_num = ar.reject_seq_num and\n                  ars.reject_status = 'REVERSAL_OF_REVENUE'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[2] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[2] = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1449^9*/
        fields.setData("fee_dce_adj_count"  , counts[0] + counts[1] + counts[2] );
        fields.setData("fee_dce_adj_amount" , amounts[0] + amounts[1] + amounts[2]);
        
        progress = "getting activated: " + merchantId;
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1456^11*/

//  ************************************************************
//  #sql [Ctx] { select 'Y' 
//              from   mif      mf
//              where  mf.merchant_number = :merchantId and
//                    -- use the open dates to allow banks to delay activation expenses
//                    -- until they can pass through the charge to the merchant account
//                     mf.activation_date between :openDateBegin and :openDateEnd and
//                     nvl( substr( mf.user_data_4, 16),' ' ) <> 'C'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select 'Y'  \n            from   mif      mf\n            where  mf.merchant_number =  :1   and\n                  -- use the open dates to allow banks to delay activation expenses\n                  -- until they can pass through the charge to the merchant account\n                   mf.activation_date between  :2   and  :3   and\n                   nvl( substr( mf.user_data_4, 16),' ' ) <> 'C'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,openDateBegin);
   __sJT_st.setDate(3,openDateEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   flagTemp = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1465^11*/                   
        }
        catch( Exception ee )
        {                   
          flagTemp = "N";
        }
        fields.setData("activated_merchant" , flagTemp);

        // add the imprinter counts from the online application
        progress = "getting imprinter counts: " + merchantId;
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1477^11*/

//  ************************************************************
//  #sql [Ctx] { select  sum( decode( me.equiplendtype_code,
//                                   1, me.merchequip_equip_quantity,
//                                   2, me.merchequip_equip_quantity,
//                                   0 ) )      
//              
//              from    monthly_extract_gn  gn,
//                      merchant            mr,
//                      merchequipment      me
//              where   gn.hh_load_sec = :LoadSec and
//                      gn.g2_date_opened between :openDateBegin and :openDateEnd and
//                      mr.merch_number(+) = gn.hh_merchant_number and
//                      me.app_seq_num(+) = mr.app_seq_num and
//                      me.equiptype_code(+) = 4    -- imprinters only
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( decode( me.equiplendtype_code,\n                                 1, me.merchequip_equip_quantity,\n                                 2, me.merchequip_equip_quantity,\n                                 0 ) )      \n             \n            from    monthly_extract_gn  gn,\n                    merchant            mr,\n                    merchequipment      me\n            where   gn.hh_load_sec =  :1   and\n                    gn.g2_date_opened between  :2   and  :3   and\n                    mr.merch_number(+) = gn.hh_merchant_number and\n                    me.app_seq_num(+) = mr.app_seq_num and\n                    me.equiptype_code(+) = 4    -- imprinters only";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
   __sJT_st.setDate(2,openDateBegin);
   __sJT_st.setDate(3,openDateEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1492^11*/
        }
        catch( Exception ee )
        {                    
          counts[0] = 0;
        }
        fields.setData("imprinter_count" , counts[0]);


        progress = "getting terminal rentals: " + merchantId;
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1504^11*/

//  ************************************************************
//  #sql [Ctx] { select  gn.s1_num_terminals  
//              
//              from   monthly_extract_gn gn
//              where  gn.hh_merchant_number = :merchantId and
//                     gn.hh_active_date = :activeDate and
//                     gn.s1_num_terminals > 0 and
//                     exists
//                     (  select  st.hh_load_sec
//                        from    monthly_extract_st    st,
//                                monthly_extract_cg    cg
//                        where   st.hh_load_sec = gn.hh_load_sec and
//                                st.st_statement_desc like '%RENTAL%' and
//                                cg.hh_load_sec = st.hh_load_sec and
//                                cg.cg_message_for_stmt = st.st_statement_desc
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  gn.s1_num_terminals  \n             \n            from   monthly_extract_gn gn\n            where  gn.hh_merchant_number =  :1   and\n                   gn.hh_active_date =  :2   and\n                   gn.s1_num_terminals > 0 and\n                   exists\n                   (  select  st.hh_load_sec\n                      from    monthly_extract_st    st,\n                              monthly_extract_cg    cg\n                      where   st.hh_load_sec = gn.hh_load_sec and\n                              st.st_statement_desc like '%RENTAL%' and\n                              cg.hh_load_sec = st.hh_load_sec and\n                              cg.cg_message_for_stmt = st.st_statement_desc\n                    )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1521^11*/
        }
        catch( Exception ee )
        {                    
          counts[0] = 0;
        }
        fields.setData("terminal_rental_count" , counts[0]);

        progress = "getting sales tax etc: " + merchantId;
        try
        {
          // collect data for equipment sales tax, rentals and sales
          /*@lineinfo:generated-code*//*@lineinfo:1533^11*/

//  ************************************************************
//  #sql [Ctx] { select sum( decode( instr(st.st_statement_desc,'TAX'),
//                                  0, 0,
//                                  st.st_fee_amount ) ),                 -- tax collected
//                     sum( decode( instr(st.st_statement_desc,'TAX'),
//                                  0, decode( instr(st.st_statement_desc,'RENT'),
//                                             0, 0,
//                                             st.st_fee_amount ),
//                                  0 ) ),                 -- rental income
//                     sum( decode( instr(st.st_statement_desc,'TAX'),
//                                  0, decode( instr(st.st_statement_desc,'RENT'),
//                                             0, st.st_fee_amount,
//                                             0 ),                       -- sales income
//                                  0 ) )
//              
//              from   monthly_extract_gn gn,
//                     monthly_extract_st st
//              where  gn.hh_merchant_number = :merchantId and
//                     gn.hh_active_date = :activeDate and
//                     -- mask merchants that were not billed for any
//                     -- imprinter or pos equipment (i.e. merchants
//                     -- with gn.g1_merchant_status == 'D'eleted or
//                     --      gn.g1_merchant_status == 'I'nactive or
//                     --      gn.g1_merchant_status == 'C'losed etc.
//                     st.hh_load_sec = gn.hh_load_sec and
//                     st.st_fee_amount != 0 and
//                     exists
//                     (
//                       select cg.hh_load_sec
//                       from   monthly_extract_cg cg
//                       where  cg.hh_load_sec = st.hh_load_sec and
//                              cg.cg_charge_record_type in ( 'POS','IMP' ) and
//                              cg.cg_message_for_stmt = st.st_statement_desc
//                     )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sum( decode( instr(st.st_statement_desc,'TAX'),\n                                0, 0,\n                                st.st_fee_amount ) ),                 -- tax collected\n                   sum( decode( instr(st.st_statement_desc,'TAX'),\n                                0, decode( instr(st.st_statement_desc,'RENT'),\n                                           0, 0,\n                                           st.st_fee_amount ),\n                                0 ) ),                 -- rental income\n                   sum( decode( instr(st.st_statement_desc,'TAX'),\n                                0, decode( instr(st.st_statement_desc,'RENT'),\n                                           0, st.st_fee_amount,\n                                           0 ),                       -- sales income\n                                0 ) )\n             \n            from   monthly_extract_gn gn,\n                   monthly_extract_st st\n            where  gn.hh_merchant_number =  :1   and\n                   gn.hh_active_date =  :2   and\n                   -- mask merchants that were not billed for any\n                   -- imprinter or pos equipment (i.e. merchants\n                   -- with gn.g1_merchant_status == 'D'eleted or\n                   --      gn.g1_merchant_status == 'I'nactive or\n                   --      gn.g1_merchant_status == 'C'losed etc.\n                   st.hh_load_sec = gn.hh_load_sec and\n                   st.st_fee_amount != 0 and\n                   exists\n                   (\n                     select cg.hh_load_sec\n                     from   monthly_extract_cg cg\n                     where  cg.hh_load_sec = st.hh_load_sec and\n                            cg.cg_charge_record_type in ( 'POS','IMP' ) and\n                            cg.cg_message_for_stmt = st.st_statement_desc\n                   )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amounts[0] = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[1] = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[2] = __sJT_rs.getDouble(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1568^11*/
        }
        catch( Exception ee )
        {
          amounts[0] = 0;
          amounts[1] = 0;
          amounts[2] = 0;
        }          
        fields.setData("equip_sales_tax_collected", amounts[0]);
        fields.setData("equip_rental_income"      , amounts[1]);
        fields.setData("equip_sales_income"       , amounts[2]);

        //********************************************************************
        // note, equipment expense is now loaded by the timed event
        // see com.mes.startup.MerchProfLoader.loadEquipSummary(...)
        //
        // swap and deployment counts are still loaded here so that
        // the contract summary and equipment summary are not dependent
        // on each other and therefore can be run in any order or in parallel
        //********************************************************************
        progress = "setting swap and deploy counts: " + merchantId;
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1591^11*/

//  ************************************************************
//  #sql [Ctx] { select  sum( decode(start_code,'NEW',1,'ADD',1,0) ) as deployed_count,
//                      sum( decode(start_code,'SWAP',1,0) )        as swap_count
//              
//              from    tmg_deployment
//              where   start_code in ('NEW','ADD','SWAP')
//                      and start_ts between :openDateBegin and :openDateEnd
//                      and merch_num = :merchantId
//                      and end_code <> 'CANCEL'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( decode(start_code,'NEW',1,'ADD',1,0) ) as deployed_count,\n                    sum( decode(start_code,'SWAP',1,0) )        as swap_count\n             \n            from    tmg_deployment\n            where   start_code in ('NEW','ADD','SWAP')\n                    and start_ts between  :1   and  :2  \n                    and merch_num =  :3  \n                    and end_code <> 'CANCEL'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,openDateBegin);
   __sJT_st.setDate(2,openDateEnd);
   __sJT_st.setLong(3,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[1] = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1601^11*/ 
        }
        catch( Exception ee )
        {
          counts[0] = 0;
          counts[1] = 0;
        }
        fields.setData("terminal_deployment_count", counts[0]);
        fields.setData("terminal_swap_count"      , counts[1]);

        progress = "getting new merchants: " + merchantId;
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1614^11*/

//  ************************************************************
//  #sql [Ctx] { select 'Y'          
//              from   monthly_extract_gn gn
//              where  gn.hh_merchant_number = :merchantId and
//                     gn.hh_active_date = :activeDate and
//                     gn.g2_date_opened between :openDateBegin and :openDateEnd
//                     -- removed per roreilly 12/6/2007 
//                     /*and
//                     (
//                       nvl( substr( gn.g1_user_data4, 16),' ' ) <> 'C' or
//                       exists
//                       (
//                         select gm.merchant_number
//                         from   merch_prof_bank_params  bp,
//                                organization            o,
//                                group_merchant          gm
//                         where  bp.param_type = 3 and
//                                lower(nvl(bp.param_value,'n')) in ( 'y', 'true' ) and
//                                o.org_group = bp.hierarchy_node and
//                                gm.org_num = o.org_num and
//                                gm.merchant_number = :merchantId
//                       )
//                     )*/
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select 'Y'           \n            from   monthly_extract_gn gn\n            where  gn.hh_merchant_number =  :1   and\n                   gn.hh_active_date =  :2   and\n                   gn.g2_date_opened between  :3   and  :4  \n                   -- removed per roreilly 12/6/2007 \n                   /*and\n                   (\n                     nvl( substr( gn.g1_user_data4, 16),' ' ) <> 'C' or\n                     exists\n                     (\n                       select gm.merchant_number\n                       from   merch_prof_bank_params  bp,\n                              organization            o,\n                              group_merchant          gm\n                       where  bp.param_type = 3 and\n                              lower(nvl(bp.param_value,'n')) in ( 'y', 'true' ) and\n                              o.org_group = bp.hierarchy_node and\n                              gm.org_num = o.org_num and\n                              gm.merchant_number = :merchantId\n                     )\n                   )*/";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"32com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,openDateBegin);
   __sJT_st.setDate(4,openDateEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   flagTemp = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1638^11*/
        }
        catch( Exception ee )
        {
          flagTemp = "N";
        }
        fields.setData("new_merchant",flagTemp);

        progress = "getting mes inventory: " + merchantId;
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1649^11*/

//  ************************************************************
//  #sql [Ctx] { select  decode( at.inventory_owner_name,
//                              'MES','Y',
//                              decode(at.separate_inventory,'Y','N','Y')
//                             ) 
//              from    merchant        mr,
//                      application     app,
//                      app_type        at
//              where   mr.merch_number = :merchantId and
//                      app.app_seq_num = mr.app_seq_num and
//                      at.app_type_code = app.app_type
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode( at.inventory_owner_name,\n                            'MES','Y',\n                            decode(at.separate_inventory,'Y','N','Y')\n                           )  \n            from    merchant        mr,\n                    application     app,\n                    app_type        at\n            where   mr.merch_number =  :1   and\n                    app.app_seq_num = mr.app_seq_num and\n                    at.app_type_code = app.app_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   flagTemp = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1661^11*/
        }
        catch( Exception ee )
        {                    
          flagTemp = "N";
        }          
        fields.setData("mes_inventory",flagTemp);

        progress = "getting disc/amex flags: " + merchantId;
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1672^11*/

//  ************************************************************
//  #sql [Ctx] { select distinct decode(po_disc.merchpo_rate,null,'N','Y'),
//                     decode(po_amex.merchpo_rate,null,'N','Y') 
//              from   monthly_extract_gn gn,
//                     merchant           mr,
//                     merchpayoption     po_disc,
//                     merchpayoption     po_amex
//              where  gn.hh_merchant_number = :merchantId and
//                     gn.hh_active_date     = :activeDate and
//                     gn.g2_date_opened between :openDateBegin and :openDateEnd and
//                     mr.merch_number(+) = gn.hh_merchant_number and
//                     po_disc.app_seq_num(+) = mr.app_seq_num and
//                     po_disc.cardtype_code(+) = 14 and
//                     po_amex.app_seq_num(+) = mr.app_seq_num and
//                     po_amex.cardtype_code(+) = 16
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select distinct decode(po_disc.merchpo_rate,null,'N','Y'),\n                   decode(po_amex.merchpo_rate,null,'N','Y')  \n            from   monthly_extract_gn gn,\n                   merchant           mr,\n                   merchpayoption     po_disc,\n                   merchpayoption     po_amex\n            where  gn.hh_merchant_number =  :1   and\n                   gn.hh_active_date     =  :2   and\n                   gn.g2_date_opened between  :3   and  :4   and\n                   mr.merch_number(+) = gn.hh_merchant_number and\n                   po_disc.app_seq_num(+) = mr.app_seq_num and\n                   po_disc.cardtype_code(+) = 14 and\n                   po_amex.app_seq_num(+) = mr.app_seq_num and\n                   po_amex.cardtype_code(+) = 16";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,openDateBegin);
   __sJT_st.setDate(4,openDateEnd);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   discFlag = (String)__sJT_rs.getString(1);
   amexFlag = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1688^11*/
        }
        catch( Exception ee )
        {
          discFlag = "N";
          amexFlag = "N";
        }          
        fields.setData("amex_setup",amexFlag);
        fields.setData("discover_setup",discFlag);

        // supply cost
        progress = "getting supply: " + merchantId;    
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1702^11*/

//  ************************************************************
//  #sql [Ctx] { select  sum( round((so.tax_rate * tots.item_total), 2) + 
//                           round((tots.item_total/nvl(so.markup_multiplier,1) * 
//                                 (so.markup_multiplier - get_supply_bank_markup_amount(so.merch_num,so.order_date)) 
//                                 ),2) + 
//                           so.handling_cost + 
//                           supply_shipping_cost(so.supply_order_id) 
//                         )
//              
//              from    supply_club_config  scc, 
//                      supply_order        so, 
//                      ( 
//                        select  iso.supply_order_id           as supply_order_id,
//                                sum(soi.cost * soi.quantity)  as item_total 
//                        from    supply_order        iso,
//                                supply_order_item   soi
//                        where   iso.merch_num = :merchantId and
//                                iso.order_date between :beginDate and (:currDate-1) and
//                                iso.order_date >= '10-Mar-2005' and          
//                                soi.supply_order_id = iso.supply_order_id   
//                        group by iso.supply_order_id
//                      )                   tots 
//              where   scc.merchant_number = :merchantId and 
//                      nvl(scc.is_club,'n') = 'y' and
//                      so.merch_num = scc.merchant_number and 
//                      tots.supply_order_id = so.supply_order_id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( round((so.tax_rate * tots.item_total), 2) + \n                         round((tots.item_total/nvl(so.markup_multiplier,1) * \n                               (so.markup_multiplier - get_supply_bank_markup_amount(so.merch_num,so.order_date)) \n                               ),2) + \n                         so.handling_cost + \n                         supply_shipping_cost(so.supply_order_id) \n                       )\n             \n            from    supply_club_config  scc, \n                    supply_order        so, \n                    ( \n                      select  iso.supply_order_id           as supply_order_id,\n                              sum(soi.cost * soi.quantity)  as item_total \n                      from    supply_order        iso,\n                              supply_order_item   soi\n                      where   iso.merch_num =  :1   and\n                              iso.order_date between  :2   and ( :3  -1) and\n                              iso.order_date >= '10-Mar-2005' and          \n                              soi.supply_order_id = iso.supply_order_id   \n                      group by iso.supply_order_id\n                    )                   tots \n            where   scc.merchant_number =  :4   and \n                    nvl(scc.is_club,'n') = 'y' and\n                    so.merch_num = scc.merchant_number and \n                    tots.supply_order_id = so.supply_order_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"35com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   __sJT_st.setLong(4,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amounts[0] = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1729^11*/
        }
        catch( Exception ee )
        {
          amounts[0] = 0.0;
        }
        fields.setData("supply_cost",amounts[0]);
        
        // interchange correction count
        progress = "getting ic correction count: " + merchantId;  
        try
        {
          // count records between the 25th of previous month
          // and the 24th of the current month
          /*@lineinfo:generated-code*//*@lineinfo:1743^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//              
//              from    trident_capture_ic_corrections 
//              where   merchant_number = :merchantId and
//                      batch_date between (trunc((trunc(:activeDate,'month')-1),'month')+24) and
//                                         (trunc(:activeDate,'month')+23)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n             \n            from    trident_capture_ic_corrections \n            where   merchant_number =  :1   and\n                    batch_date between (trunc((trunc( :2  ,'month')-1),'month')+24) and\n                                       (trunc( :3  ,'month')+23)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"36com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1751^11*/
        }
        catch( Exception ee )
        {
          counts[0] = 0;
        }
        fields.setData("ic_correction_count",counts[0]);
      
      
        // add all the non-sense fees from the associations
        progress = "adding association fees: " + merchantId;  
        double debitNetworkFees = 0.0;
        int vsApfIntlFeeCount = 0;
        int vsApfdbIntlFeeCount = 0;
        double visa_isa_amount = 0.0;
        double visa_enh_isa_amount = 0.0;
        double mc_cross_border_amount = 0.0;
        double mc_enh_cross_border_amount = 0.0;
        int    mc_cnp_digi_enablement_count = 0;
        double mc_cnp_digi_enablement_amount = 0.0;
        int    vs_cr_assessment_count = 0;
        double vs_cr_assessment_amount = 0.0;
        int    batch_fee_count = 0;
        double batch_fee_amount = 0.0;
        int    vsCVPDebitCount = 0;
        int    vsCVPCreditCount = 0;
        int    vsCVPDebitCountIntl = 0;
        int    vsCVPCreditCountIntl = 0;        
        int    vsAccessFeeCount = 0;
        int    mcAccessFeeCount = 0;
        int    vsCardVerifyFeeCount = 0;
        int    mcCardVerifyFeeCount = 0;
        int    achTranCount = 0;
        int    achRetCount = 0;
        int    achUnauthCount = 0;
        double optBAssessmentAmount = 0;
        double optBInboundAmount = 0;
        double optBNonSwipedAmount = 0;
        double optBNonComplAmount = 0;
        double optBDataQualityAmount = 0;
        double achTranAmount = 0;
        double achRetAmount = 0;
        double achUnauthAmount = 0;
        double mcPreAuthAmount = 0.0;
        double mcFinalAuthAmount = 0.0;
        int    mcFinalAuthCount = 0;
        double mcPreAuthFeeAmount = 0.0;
        double mcFinalAuthFeeAmount = 0.0; 
        int    mcGlobalWholesaleTravelCount = 0;
        double mcGloablWholesaleTravelAmount = 0.0;
        int    mcGlobalAcquirerSupportCount = 0;
        double mcGlobalAcquirerSupportAmount = 0.0;
        int    mcNabuCount = 0;
        int    mcExcessiveAuthCount = 0;
        double mcExcessiveAuthAmount = 0.0;
        double mcExcessiveAuthFeeAmount = 0.0;
        int    mcFreightAssessmentCount = 0;
        double mcFreightAssessmentAmount = 0.0;
        int    mcNominalAmountAuthCount = 0;
        double mcNominalAmountAuthAmount = 0.0;
        double mcNominalAmountAuthFeeAmount = 0.0;


        if ( MbsBackend == 1 )
        {
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:1800^13*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(decode(ds.item_subclass,'VS',1,0) 
//                                * decode(ds.item_type,113,ds.item_count,0) ),0) as visa_count,
//                        nvl(sum(decode(ds.item_subclass,'VS',1,0) 
//                                * decode(ds.item_type,113,ds.item_amount,0) ),0) as visa_amount,
//                        nvl(sum(decode(ds.item_subclass,'MC',1,0) 
//                                * decode(ds.item_type,113,ds.item_count,0) ),0)  as mc_count,
//                        nvl(sum(decode(ds.item_subclass,'MC',1,0) 
//                                * decode(ds.item_type,113,ds.item_amount,0) ),0) as mc_amount,
//                        nvl(sum(decode(ds.item_subclass,'DS',1,0) 
//                                * decode(ds.item_type,113,ds.item_count,0) ),0)  as ds_count,
//                        nvl(sum(decode(ds.item_subclass,'DS',1,0) 
//                                * decode(ds.item_type,113,ds.item_amount,0) ),0) as ds_amount,
//                        nvl(sum( decode(ds.item_type,111,1,0) 
//                                 * decode(ds.item_subclass,'VS',1,0)
//                                 * ds.expense_actual
//                               ),0)                                             as visa_ic_exp,
//                        nvl(sum( decode(ds.item_type,111,1,0) 
//                                 * decode(ds.item_subclass,'MC',1,0)
//                                 * ds.expense_actual
//                               ),0)                                             as mc_ic_exp,
//                        nvl(sum( decode(ds.item_type,111,1,0)
//                                        * decode(ds.item_subclass,'DB',1,0)
//                                        * ds.expense_actual
//                        ),0)                                                    as debit_network_exp,
//                        nvl(sum( decode(ds.item_type,111,1,0) 
//                                 * decode(ds.item_subclass,'EB',0,'DB',0,1)
//                                 * ds.expense_actual
//                               ),0)                                             as ic_exp_enh,
//                        nvl(sum(decode(ds.item_subclass,'MC',1,0) 
//                                * decode(ds.item_type,117,ds.item_amount,0)),0) as mc_large_ticket_amount,
//                        nvl(sum(decode(ds.item_subclass,'VS',1,0) 
//                                * decode(ds.item_type,7,ds.item_count,0)),0)    as visa_apf_count,
//                        nvl(sum(decode(ds.item_subclass,'VS',1,0) 
//                                * decode(ds.item_type,14,ds.item_count,0)),0)   as visa_apf_db_count,
//                        nvl(sum(decode(ds.item_subclass,'VS',1,0) 
//                                * decode(ds.item_type,97,ds.item_count,0)),0)    as visa_apf_intl_count,
//                        nvl(sum(decode(ds.item_subclass,'VS',1,0) 
//                                * decode(ds.item_type,104,ds.item_count,0)),0)   as visa_apf_db_intl_count,
//                        nvl(sum(decode(ds.item_subclass,'VS',1,0) 
//                                * decode(ds.item_type,15,ds.item_count,0)),0)   as visa_tif_count,
//                        nvl(sum(decode(ds.item_subclass,'MC',1,0) 
//                                  * decode(ds.item_type,230,ds.item_count,0)),0)   as mc_pre_auth_count,
//                        nvl(sum(decode(ds.item_subclass,'MC',1,0) 
//                                * decode(ds.item_type,19,ds.item_count,0)),0)   as mc_cvc2_count,
//                        nvl(sum(decode(ds.item_subclass,'VS',1,0) 
//                                * decode(ds.item_type,115,ds.item_amount,0) ),0) as visa_isa_amount,
//                        nvl(sum(decode(ds.item_subclass,'VS',1,0) 
//                                * decode(ds.item_type,125,ds.item_amount,0) ),0) as visa_enh_isa_amount,
//                        nvl(sum(decode(ds.item_subclass,'MC',1,0) 
//                                * decode(ds.item_type,126,ds.item_amount,0) ),0) as mc_cross_border_amount,
//                        nvl(sum(decode(ds.item_subclass,'MC',1,0) 
//                                * decode(ds.item_type,127,ds.item_amount,0) ),0) as mc_enh_cross_border_amount,
//                        nvl(sum(decode(ds.item_type,109,ds.sales_count,0) ),0)   as mc_cnp_digi_enablement_count,
//                        nvl(sum(decode(ds.item_type,109,ds.sales_amount,0) ),0)  as mc_cnp_digi_enablement_amount,
//                        nvl(sum(decode(ds.item_type,110,ds.sales_count,0) ),0)   as vs_cr_assessment_count,
//                        nvl(sum(decode(ds.item_type,110,ds.sales_amount,0) ),0)  as vs_cr_assessment_amount,
//                        nvl(sum(decode(ds.item_type,8,ds.item_count,0) ),0)      as batch_fee_count,
//                        nvl(sum(decode(ds.item_type,8,ds.item_amount,0) ),0)     as batch_fee_amount,
//                        nvl(sum(decode(ds.item_subclass,'VD',1,0) 
//                                * decode(ds.item_type,209,ds.item_count,0)),0)   as vs_cvp_debit_count,
//                        nvl(sum(decode(ds.item_subclass,'VS',1,'VB',1,0) 
//                                * decode(ds.item_type,210,ds.item_count,0)),0)   as vs_cvp_credit_count,
//                        nvl(sum(decode(ds.item_subclass,'VD',1,0) 
//                                * decode(ds.item_type,238,ds.item_count,0)),0)   as vs_cvp_debit_count_intl,
//                        nvl(sum(decode(ds.item_subclass,'VS',1,'VB',1,0) 
//                                * decode(ds.item_type,239,ds.item_count,0)),0)   as vs_cvp_credit_count_intl,
//                        nvl(sum(decode(ds.item_subclass,'VS',1,0)
//                                * decode(ds.item_type,212,ds.item_count,0)),0)   as vs_access_fee_count,
//                        nvl(sum(decode(ds.item_subclass,'MC',1,0)
//                                * decode(ds.item_type,214,ds.item_count,0)),0)   as mc_access_fee_count,
//                        nvl(sum(decode(ds.item_subclass,'VS',1,0)
//                                * decode(ds.item_type,216,ds.item_count,0)),0)   as vs_card_verify_count,
//                        nvl(sum(decode(ds.item_subclass,'MC',1,0)
//                                * decode(ds.item_type,218,ds.item_count,0)),0)   as mc_card_vefify_count,
//                        nvl(sum(decode(ds.item_subclass,'AM',1,0)
//                                * decode(ds.item_type,128,ds.item_amount,0) ),0) as optb_assessment_fee_amount,
//                        nvl(sum(decode(ds.item_subclass,'AM',1,0)
//                                * decode(ds.item_type,215,ds.item_amount,0) ),0) as optb_inbound_fee_amount,
//                        nvl(sum(decode(ds.item_subclass,'AM',1,0)
//                                * decode(ds.item_type,220,ds.item_amount,0) ),0) as optb_non_swiped_fee_amount,
//                        nvl(sum(decode(ds.item_subclass,'AM',1,0)
//                                * decode(ds.item_type,221,ds.item_amount,0) ),0) as optb_non_compl_fee_amount,
//                        nvl(sum(decode(ds.item_subclass,'AM',1,0)
//                                * decode(ds.item_type,222,ds.item_amount,0) ),0) as optb_data_quality_fee_amount,
//                        /* Code modified by RS for ACHPS populate count & amount for ACH expense calculation  */ 
//                        nvl(sum(decode(ds.item_subclass,'AC',1,0)
//                                * decode(ds.item_type,224,ds.item_count,0) ),0) as ach_tran_count,
//                        nvl(sum(decode(ds.item_subclass,'AC',1,0)
//                                * decode(ds.item_type,224,ds.item_amount,0) ),0) as ach_tran_amount,
//                        nvl(sum(decode(ds.item_subclass,'AC',1,0)
//                                * decode(ds.item_type,225,ds.item_count,0) ),0) as ach_ret_count,
//                        nvl(sum(decode(ds.item_subclass,'AC',1,0)
//                                * decode(ds.item_type,225,ds.item_amount,0) ),0) as ach_ret_amount,
//                        nvl(sum(decode(ds.item_subclass,'AC',1,0)
//                                * decode(ds.item_type,226,ds.item_count,0) ),0) as ach_unauth_count,
//                        nvl(sum(decode(ds.item_subclass,'AC',1,0)
//                                * decode(ds.item_type,226,ds.item_amount,0) ),0) as ach_unauth_amount,
//                        nvl(sum(decode(ds.item_subclass,'MC',1,0) 
//                                * decode(ds.item_type,230,ds.item_amount,0)),0)  as mc_pre_auth_amount,
//                        nvl(sum(decode(ds.item_subclass,'MC',1,0) 
//                                * decode(ds.item_type,236,ds.item_amount,0)),0)  as mc_final_auth_amount,
//                        nvl(sum(decode(ds.item_subclass,'MC',1,0) 
//                                * decode(ds.item_type,236,ds.item_count,0)),0)   as mc_final_auth_count,
//                        nvl(sum(decode(ds.item_subclass,'MC',1,0) 
//              				  * decode(ds.item_type,236,ds.fees_due,0)),0)      as mc_final_auth_fee_amount,
//          	          nvl(sum(decode(ds.item_subclass,'MC',1,0) 
//  					          * decode(ds.item_type,230,ds.fees_due,0)),0)      as mc_pre_auth_fee_amount
//                
//                from    mbs_daily_summary   ds
//                where   ds.me_load_file_id = load_filename_to_load_file_id(:loadFilename)
//                        and ds.merchant_number = :merchantId
//                        and ds.item_type in ( 7, 14, 15, 111, 113, 117, 115, 125, 126, 127, 109, 110, 8, 209, 210, 212, 214, 216, 218,
//                                              128, 215, 220, 221, 222, 224, 225, 226, 230, 236, 19, 97, 104, 238, 239 )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(decode(ds.item_subclass,'VS',1,0)"
                   + "      * decode(ds.item_type,113,ds.item_count,0) ),0) as visa_count,"
                   + " nvl(sum(decode(ds.item_subclass,'VS',1,0)"
                   + "      * decode(ds.item_type,113,ds.item_amount,0) ),0) as visa_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,113,ds.item_count,0) ),0)  as mc_count,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,113,ds.item_amount,0) ),0) as mc_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'DS',1,0)"
                   + "      * decode(ds.item_type,113,ds.item_count,0) ),0)  as ds_count,"
                   + " nvl(sum(decode(ds.item_subclass,'DS',1,0)"
                   + "      * decode(ds.item_type,113,ds.item_amount,0) ),0) as ds_amount,"
                   + " nvl(sum( decode(ds.item_type,111,1,0)"
                   + "      * decode(ds.item_subclass,'VS',1,0)"
                   + "      * ds.expense_actual),0) as visa_ic_exp,"
                   + " nvl(sum( decode(ds.item_type,111,1,0)"
                   + "      * decode(ds.item_subclass,'MC',1,0)"
                   + "      * ds.expense_actual),0) as mc_ic_exp,"
                   + " nvl(sum( decode(ds.item_type,111,1,0)"
                   + "      * decode(ds.item_subclass,'DB',1,0)"
                   + "      * ds.expense_actual),0) as debit_network_exp,"
                   + " nvl(sum( decode(ds.item_type,111,1,0)"
                   + "      * decode(ds.item_subclass,'EB',0,'DB',0,1)"
                   + "      * ds.expense_actual),0) as ic_exp_enh,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,117,ds.item_amount,0)),0) as mc_large_ticket_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'VS',1,0)"
                   + "      * decode(ds.item_type,7,ds.item_count,0)),0)    as visa_apf_count,"
                   + " nvl(sum(decode(ds.item_subclass,'VS',1,0)"
                   + "      * decode(ds.item_type,14,ds.item_count,0)),0)   as visa_apf_db_count,"
                   + " nvl(sum(decode(ds.item_subclass,'VS',1,0) "
                   + "      * decode(ds.item_type,97,ds.item_count,0)),0)    as visa_apf_intl_count,"
                   + " nvl(sum(decode(ds.item_subclass,'VS',1,0)"
                   + "      * decode(ds.item_type,104,ds.item_count,0)),0)   as visa_apf_db_intl_count,"
                   + " nvl(sum(decode(ds.item_subclass,'VS',1,0)"
                   + "      * decode(ds.item_type,15,ds.item_count,0)),0)   as visa_tif_count,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,230,ds.item_count,0)),0)   as mc_pre_auth_count,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,19,ds.item_count,0)),0)   as mc_cvc2_count,"
                   + " nvl(sum(decode(ds.item_subclass,'VS',1,0)"
                   + "      * decode(ds.item_type,115,ds.item_amount,0) ),0) as visa_isa_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'VS',1,0)"
                   + "      * decode(ds.item_type,125,ds.item_amount,0) ),0) as visa_enh_isa_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,126,ds.item_amount,0) ),0) as mc_cross_border_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,127,ds.item_amount,0) ),0) as mc_enh_cross_border_amount,"
                   + " nvl(sum(decode(ds.item_type,109,ds.sales_count,0) ),0)   as mc_cnp_digi_enablement_count,"
                   + " nvl(sum(decode(ds.item_type,109,ds.sales_amount,0) ),0)  as mc_cnp_digi_enablement_amount,"
                   + " nvl(sum(decode(ds.item_type,110,ds.sales_count,0) ),0)   as vs_cr_assessment_count,"
                   + " nvl(sum(decode(ds.item_type,110,ds.sales_amount,0) ),0)  as vs_cr_assessment_amount,"
                   + " nvl(sum(decode(ds.item_type,8,ds.item_count,0) ),0)      as batch_fee_count,"
                   + " nvl(sum(decode(ds.item_type,8,ds.item_amount,0) ),0)     as batch_fee_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'VD',1,0)"
                   + "      * decode(ds.item_type,209,ds.item_count,0)),0)   as vs_cvp_debit_count,"
                   + " nvl(sum(decode(ds.item_subclass,'VS',1,'VB',1,0)"
                   + "      * decode(ds.item_type,210,ds.item_count,0)),0)   as vs_cvp_credit_count,"
                   + " nvl(sum(decode(ds.item_subclass,'VD',1,0) "
                   + "       * decode(ds.item_type,238,ds.item_count,0)),0)   as vs_cvp_debit_count_intl,"
                   + " nvl(sum(decode(ds.item_subclass,'VS',1,'VB',1,0) "
                   + "       * decode(ds.item_type,239,ds.item_count,0)),0)   as vs_cvp_credit_count_intl,"
                   + " nvl(sum(decode(ds.item_subclass,'VS',1,0)"
                   + "      * decode(ds.item_type,212,ds.item_count,0)),0)   as vs_access_fee_count,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,214,ds.item_count,0)),0)   as mc_access_fee_count,"
                   + " nvl(sum(decode(ds.item_subclass,'VS',1,0)"
                   + "      * decode(ds.item_type,216,ds.item_count,0)),0)   as vs_card_verify_count,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,218,ds.item_count,0)),0)   as mc_card_vefify_count,"
                   + " nvl(sum(decode(ds.item_subclass,'AM',1,0)"
                   + "      * decode(ds.item_type,128,ds.item_amount,0) ),0) as optb_assessment_fee_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'AM',1,0)"
                   + "      * decode(ds.item_type,215,ds.item_amount,0) ),0) as optb_inbound_fee_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'AM',1,0)"
                   + "      * decode(ds.item_type,220,ds.item_amount,0) ),0) as optb_non_swiped_fee_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'AM',1,0)"
                   + "      * decode(ds.item_type,221,ds.item_amount,0) ),0) as optb_non_compl_fee_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'AM',1,0)"
                   + "      * decode(ds.item_type,222,ds.item_amount,0) ),0) as optb_data_quality_fee_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'AC',1,0)"
                   + "      * decode(ds.item_type,224,ds.item_count,0) ),0) as ach_tran_count,"
                   + " nvl(sum(decode(ds.item_subclass,'AC',1,0)"
                   + "      * decode(ds.item_type,224,ds.item_amount,0) ),0) as ach_tran_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'AC',1,0)"
                   + "      * decode(ds.item_type,225,ds.item_count,0) ),0) as ach_ret_count,"
                   + " nvl(sum(decode(ds.item_subclass,'AC',1,0)"
                   + "      * decode(ds.item_type,225,ds.item_amount,0) ),0) as ach_ret_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'AC',1,0)"
                   + "      * decode(ds.item_type,226,ds.item_count,0) ),0) as ach_unauth_count,"
                   + " nvl(sum(decode(ds.item_subclass,'AC',1,0)"
                   + "      * decode(ds.item_type,226,ds.item_amount,0) ),0) as ach_unauth_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,230,ds.item_amount,0)),0)  as mc_pre_auth_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,236,ds.item_amount,0)),0)  as mc_final_auth_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,236,ds.item_count,0)),0)   as mc_final_auth_count,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,236,ds.fees_due,0)),0)     as mc_final_auth_fee_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,230,ds.fees_due,0)),0)     as mc_pre_auth_fee_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,129,ds.item_amount,0)),0)  as mc_global_wholesale_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,129,ds.item_count,0)),0)   as mc_global_wholesale_count,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,114,ds.item_amount,0)),0)  as mc_global_acquirer_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,114,ds.item_count,0)),0)   as mc_global_acquirer_count,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,240,ds.item_count,0)),0)   as mc_excessive_auth_count,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,240,ds.item_amount,0)),0)  as mc_excessive_auth_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,240,ds.fees_due,0)),0)     as mc_excessive_auth_fee_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,130,ds.item_amount,0)),0)  as mc_freight_assessment_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,130,ds.item_count,0)),0)   as mc_freight_assessment_count,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,241,ds.item_count,0)),0)   as mc_nominal_auth_count,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,241,ds.item_amount,0)),0)  as mc_nominal_auth_amount,"
                   + " nvl(sum(decode(ds.item_subclass,'MC',1,0)"
                   + "      * decode(ds.item_type,241,ds.fees_due,0)),0)     as mc_nominal_auth_fee_amount"
           + " from    mbs_daily_summary   ds"
           + " where   ds.me_load_file_id = load_filename_to_load_file_id( :1  )"
           + "     and ds.merchant_number =  :2"
           + "     and ds.item_type in ( 7, 14, 15, 111, 113, 117, 115, 125, 126, 127, 109, 110, 8, 209, 210, 212, 214, 216, 218,"
           + "                          128, 215, 220, 221, 222, 224, 225, 226, 230, 236, 19, 97, 104, 238, 239 ,114, 129, 240, 130, 241)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"37com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setLong(2,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 64) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(64,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[0] = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[1] = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[1] = __sJT_rs.getDouble(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[2] = __sJT_rs.getInt(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[2] = __sJT_rs.getDouble(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[3] = __sJT_rs.getDouble(7); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[4] = __sJT_rs.getDouble(8); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   debitNetworkFees = __sJT_rs.getDouble(9); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[5] = __sJT_rs.getDouble(10); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[6] = __sJT_rs.getDouble(11); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[3] = __sJT_rs.getInt(12); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[4] = __sJT_rs.getInt(13); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vsApfIntlFeeCount = __sJT_rs.getInt(14); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vsApfdbIntlFeeCount = __sJT_rs.getInt(15); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[5] = __sJT_rs.getInt(16); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[6] = __sJT_rs.getInt(17); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[7] = __sJT_rs.getInt(18); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   visa_isa_amount = __sJT_rs.getDouble(19); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   visa_enh_isa_amount = __sJT_rs.getDouble(20); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mc_cross_border_amount = __sJT_rs.getDouble(21); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mc_enh_cross_border_amount = __sJT_rs.getDouble(22); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mc_cnp_digi_enablement_count = __sJT_rs.getInt(23); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mc_cnp_digi_enablement_amount = __sJT_rs.getDouble(24); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vs_cr_assessment_count = __sJT_rs.getInt(25); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vs_cr_assessment_amount = __sJT_rs.getDouble(26); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   batch_fee_count = __sJT_rs.getInt(27); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   batch_fee_amount = __sJT_rs.getDouble(28); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vsCVPDebitCount = __sJT_rs.getInt(29); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vsCVPCreditCount = __sJT_rs.getInt(30); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vsCVPDebitCountIntl = __sJT_rs.getInt(31); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vsCVPCreditCountIntl = __sJT_rs.getInt(32); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vsAccessFeeCount = __sJT_rs.getInt(33); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcAccessFeeCount = __sJT_rs.getInt(34); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vsCardVerifyFeeCount = __sJT_rs.getInt(35); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcCardVerifyFeeCount = __sJT_rs.getInt(36); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   optBAssessmentAmount = __sJT_rs.getDouble(37); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   optBInboundAmount = __sJT_rs.getDouble(38); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   optBNonSwipedAmount = __sJT_rs.getDouble(39); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   optBNonComplAmount = __sJT_rs.getDouble(40); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   optBDataQualityAmount = __sJT_rs.getDouble(41); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   achTranCount = __sJT_rs.getInt(42); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   achTranAmount = __sJT_rs.getDouble(43); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   achRetCount = __sJT_rs.getInt(44); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   achRetAmount = __sJT_rs.getDouble(45); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   achUnauthCount = __sJT_rs.getInt(46); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   achUnauthAmount = __sJT_rs.getDouble(47); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcPreAuthAmount = __sJT_rs.getDouble(48); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcFinalAuthAmount = __sJT_rs.getDouble(49); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcFinalAuthCount = __sJT_rs.getInt(50); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcFinalAuthFeeAmount = __sJT_rs.getDouble(51); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcPreAuthFeeAmount = __sJT_rs.getDouble(52); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcGloablWholesaleTravelAmount = __sJT_rs.getDouble(53); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcGlobalWholesaleTravelCount = __sJT_rs.getInt(54); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcGlobalAcquirerSupportAmount = __sJT_rs.getDouble(55); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcGlobalAcquirerSupportCount = __sJT_rs.getInt(56); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcExcessiveAuthCount = __sJT_rs.getInt(57); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcExcessiveAuthAmount = __sJT_rs.getDouble(58); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcExcessiveAuthFeeAmount = __sJT_rs.getDouble(59); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcFreightAssessmentAmount = __sJT_rs.getDouble(60); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcFreightAssessmentCount = __sJT_rs.getInt(61); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcNominalAmountAuthCount = __sJT_rs.getInt(62); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcNominalAmountAuthAmount = __sJT_rs.getDouble(63); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcNominalAmountAuthFeeAmount = __sJT_rs.getDouble(64); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1920^13*/
          }
          catch( Exception ee )
          {
            // ignore, no data
            counts[0] = 0;  amounts[0]  = 0.0;  // visa
            counts[1] = 0;  amounts[1]  = 0.0;  // mc
            counts[2] = 0;  amounts[2]  = 0.0;  // discover
            counts[3] = 0;                      // visa apf
            counts[4] = 0;                      // visa apf db
            vsApfIntlFeeCount = 0;              // visa apf Intl
            vsApfdbIntlFeeCount = 0;            // visa apf db Intl
            counts[5] = 0;                      // visa tif
            counts[6] = 0;                      // mc tif
            counts[7] = 0;                      // mc cvc2
            amounts[3]  = 0.0;                  // visa ic
            amounts[4]  = 0.0;                  // mc ic
            amounts[5]  = 0.0;                  // ic enh
            amounts[6]  = 0.0;                  // mc large ticket
          }
         
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:1941^13*/

//  ************************************************************
//  #sql [Ctx] { select  sum( decode(vma.report_identifier,'FEE100S',vma.item_count,0) ),
//                        sum( decode(vma.report_identifier,'FEE200S',vma.item_count,0) )
//                
//                from    visa_auth_misuse_summary    vma
//                where   vma.merchant_number = :merchantId
//                        and vma.active_date = trunc(trunc(:activeDate,'month')-1,'month')
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( decode(vma.report_identifier,'FEE100S',vma.item_count,0) ),\n                      sum( decode(vma.report_identifier,'FEE200S',vma.item_count,0) )\n               \n              from    visa_auth_misuse_summary    vma\n              where   vma.merchant_number =  :1  \n                      and vma.active_date = trunc(trunc( :2  ,'month')-1,'month')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"38com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[8] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[9] = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1949^13*/
          }
          catch( Exception ee )
          {
            // ignore, no data
            counts[8] = 0; counts[9] = 0;       // visa misuse, visa zfl
          }
          
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:1959^13*/

//  ************************************************************
//  #sql [Ctx] { select  sum(decode(mf.sic_code,'8398',0,1) * fanf.fee)  as fee
//                
//                from    visa_fanf_summary   fanf,
//                        mif                 mf
//                where   fanf.merchant_number = :merchantId
//                        and fanf.active_date = :activeDate
//                        and mf.merchant_number = fanf.merchant_number
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum(decode(mf.sic_code,'8398',0,1) * fanf.fee)  as fee\n               \n              from    visa_fanf_summary   fanf,\n                      mif                 mf\n              where   fanf.merchant_number =  :1  \n                      and fanf.active_date =  :2  \n                      and mf.merchant_number = fanf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amounts[7] = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1968^13*/
          }
          catch( Exception ee )
          {
            // ignore, no data
            amounts[7] = 0.0; // visa fanf
          }
          fields.setData("interchange_expense", fields.getData("bet_interchange_expense"));
          fields.setData("visa_interchange",amounts[3]);
          fields.setData("mc_interchange",amounts[4]);
          fields.setData("interchange_expense_enhanced",amounts[5]);
          fields.setData("visa_apf_count",counts[3]+vsApfIntlFeeCount);
          fields.setData("visa_apf_db_count",counts[4]+vsApfdbIntlFeeCount);
          fields.setData("visa_tif_count",counts[5]);
          fields.setData("mc_tif_count",counts[6]+mcFinalAuthCount); // Pre Auth + Final Auth
          fields.setData("debit_network_fees",debitNetworkFees);
          fields.setData("mc_cnp_digi_enablement_count", mc_cnp_digi_enablement_count);
          fields.setData("mc_cnp_digi_enablement_amount", mc_cnp_digi_enablement_amount);
          fields.setData("vs_cr_assessment_count", vs_cr_assessment_count);
          fields.setData("vs_cr_assessment_amount", vs_cr_assessment_amount);
          fields.setData("batch_fee_count", batch_fee_count);
          fields.setData("batch_fee_amount", batch_fee_amount);
          /* Code modified by RS for ACHPS Partner billing and MOJAVE changes */ 
          fields.setData("ach_tran_count" , achTranCount);
          fields.setData("ach_tran_amount" , achTranAmount);
          fields.setData("ach_return_count" , achRetCount);
          fields.setData("ach_return_amount" , achRetAmount);
          fields.setData("ach_unauth_count" , achUnauthCount);
          fields.setData("ach_unauth_amount" , achUnauthAmount);
          
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:2000^13*/

//  ************************************************************
//  #sql [Ctx] { select case when count(1) > 0 then 'Y'
//                              else 'N'
//                          end 
//                  from t_hierarchy     th,
//                          mif             m,
//                          agent_bank_contract     abc
//                  where m.merchant_number = :merchantId and
//                          m.association_node = th.descendent and
//                          th.ancestor = abc.hierarchy_node and
//                          :activeDate between abc.valid_date_begin and abc.valid_date_end and
//                          abc.billing_element_type = :BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select case when count(1) > 0 then 'Y'\n                            else 'N'\n                        end  \n                from t_hierarchy     th,\n                        mif             m,\n                        agent_bank_contract     abc\n                where m.merchant_number =  :1   and\n                        m.association_node = th.descendent and\n                        th.ancestor = abc.hierarchy_node and\n                         :2   between abc.valid_date_begin and abc.valid_date_end and\n                        abc.billing_element_type =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"40com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setInt(3,BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   flagTemp = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2013^13*/
          }
          catch( Exception ee )
          {
            flagTemp = "N";
          }
          fields.setData("debit_pass_through_flag" , flagTemp);
          
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:2023^13*/

//  ************************************************************
//  #sql [Ctx] { select  case when mf.debit_plan like 'D%' then 1 else 0 end
//                
//                from    mif                         mf
//                where   mf.merchant_number = :merchantId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  case when mf.debit_plan like 'D%' then 1 else 0 end\n               \n              from    mif                         mf\n              where   mf.merchant_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"41com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[10] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2029^13*/
          }
          catch( Exception e )
          {
            // ignore, missing mid?
            counts[10] = 0;
          }
          
          // PROD-1621
          try {
            /*@lineinfo:generated-code*//*@lineinfo:2039^13*/

//  ************************************************************
//  #sql [Ctx] { select  decode( nvl(mf.print_statements, 'N'), 'Y', 'Y', 'N')
//                
//                from    mif                         mf
//                where   mf.merchant_number = :merchantId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode( nvl(mf.print_statements, 'N'), 'Y', 'Y', 'N')\n               \n              from    mif                         mf\n              where   mf.merchant_number =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"42com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   flagTemp = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2044^13*/
          }
          catch( Exception ignore ) {
              flagTemp = "N";
          }
          fields.setData("print_statement", flagTemp);
                    
          double  vsSalesAmount = getDouble("visa_sales_amount");
          double  mcSalesAmount = getDouble("mc_sales_amount");
          double  dsSalesAmount = getDouble("disc_sales_amount")
                                  + getDouble("jcb_sales_amount")
                                  + getDouble("dinr_sales_aount");
          int     vsVolCount    = getInt("visa_vol_count");
          int     mcVolCount    = getInt("mc_vol_count");
          int     dsAuthCount   = getInt("disc_auth_count")
                                  + getInt("jcb_auth_count")
                                  + getInt("dinr_auth_count");
          int     dsVolCount    = getInt("disc_vol_count")
                                  + getInt("jcb_vol_count")
                                  + getInt("dinr_vol_count");
          
          // Getting the MC Digital Enablement fee
          // 109    MC Digital Enablement Fee
          double    mcDigitalEnablementFees = 0.0;
          double    mc_digi_rate = 0.0;
          
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:2072^13*/

//  ************************************************************
//  #sql [Ctx] { select mp.rate
//                
//                from   mbs_pricing mp
//                where  mp.merchant_number = :merchantId
//                       and mp.item_type = 109
//                       and last_day(:activeDate) between mp.valid_date_begin and mp.valid_date_end
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select mp.rate\n               \n              from   mbs_pricing mp\n              where  mp.merchant_number =  :1  \n                     and mp.item_type = 109\n                     and last_day( :2  ) between mp.valid_date_begin and mp.valid_date_end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"43com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mc_digi_rate = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2080^13*/            
            mcDigitalEnablementFees = mc_cnp_digi_enablement_amount * mc_digi_rate * 0.01;
          }
          catch( Exception ignore ) { }

          log.debug("mcDigitalEnablementFees : " + mcDigitalEnablementFees);
          
          // Amex OptBlue Excessive Chargeback Program
          // Amex OptBlue Excessive Chargeback Fee
          // Amex OptBlue Existing ESA Merch Access Fee
          double optbECPFees  = 0.0;
          double optbExcCBFees = 0.0;
          double optbMerchAccessFee = 0.0;

          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:2096^13*/

//  ************************************************************
//  #sql [Ctx] { select st.st_fee_amount
//                
//                from   monthly_extract_gn gn, 
//                       monthly_extract_st st 
//                where  gn.hh_active_date = :activeDate
//                       and gn.hh_merchant_number = :merchantId
//                       and st.hh_load_sec = gn.hh_load_sec 
//                       and st.st_statement_desc like '%AM EXCESSIVE CHARGEBACK PROGRAM%'
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select st.st_fee_amount\n               \n              from   monthly_extract_gn gn, \n                     monthly_extract_st st \n              where  gn.hh_active_date =  :1  \n                     and gn.hh_merchant_number =  :2  \n                     and st.hh_load_sec = gn.hh_load_sec \n                     and st.st_statement_desc like '%AM EXCESSIVE CHARGEBACK PROGRAM%'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"44com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setLong(2,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   optbECPFees = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2106^13*/            
          }
          catch( Exception ignore ) { }

          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:2112^13*/

//  ************************************************************
//  #sql [Ctx] { select st.st_fee_amount
//                
//                from   monthly_extract_gn gn, 
//                       monthly_extract_st st 
//                where  gn.hh_active_date = :activeDate
//                       and gn.hh_merchant_number = :merchantId
//                       and st.hh_load_sec = gn.hh_load_sec 
//                       and st.st_statement_desc like '%AM EXCESSIVE CHARGEBACK FEE%'
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select st.st_fee_amount\n               \n              from   monthly_extract_gn gn, \n                     monthly_extract_st st \n              where  gn.hh_active_date =  :1  \n                     and gn.hh_merchant_number =  :2  \n                     and st.hh_load_sec = gn.hh_load_sec \n                     and st.st_statement_desc like '%AM EXCESSIVE CHARGEBACK FEE%'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"45com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setLong(2,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   optbExcCBFees = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2122^13*/            
          }
          catch( Exception ignore ) { }

          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:2128^13*/

//  ************************************************************
//  #sql [Ctx] { select st.st_fee_amount
//                
//                from   monthly_extract_gn gn, 
//                       monthly_extract_st st 
//                where  gn.hh_active_date = :activeDate
//                       and gn.hh_merchant_number = :merchantId
//                       and st.hh_load_sec = gn.hh_load_sec 
//                       and st.st_statement_desc like '%ESA MERCHANT ACCESS FEE%'
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select st.st_fee_amount\n               \n              from   monthly_extract_gn gn, \n                     monthly_extract_st st \n              where  gn.hh_active_date =  :1  \n                     and gn.hh_merchant_number =  :2  \n                     and st.hh_load_sec = gn.hh_load_sec \n                     and st.st_statement_desc like '%ESA MERCHANT ACCESS FEE%'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"46com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setLong(2,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   optbMerchAccessFee = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2138^13*/            
          }
          catch( Exception ignore ) { }

          // MC Monthly Location Fee
          double mcLocationFee  = 0.0;
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:2146^13*/

//  ************************************************************
//  #sql [Ctx] { select mcf.fee_amount
//                
//                from   mc_merch_location_fee mcf
//                where  mcf.active_date = :activeDate
//                       and mcf.merchant_number = :merchantId
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select mcf.fee_amount\n               \n              from   mc_merch_location_fee mcf\n              where  mcf.active_date =  :1  \n                     and mcf.merchant_number =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"47com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setLong(2,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mcLocationFee = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2153^13*/
          }
          catch( Exception ignore ) { }

          //vmc_assessment_fees components
          double mcSafetyNetFeeAmount = 0.0;
          double mcNabuFeeAmount = 0.0;
          double mcBCBFeeAmount = 0.0;
          double mcECBFeeAmount = 0.0;
          double mcGSFeeAmount = 0.0;
          double mcLTDFeeAmount = 0.0;
          double mcDEFeeAmount = 0.0;
          double mcALFeeAmount = 0.0;
          double mcTifFeeAmount = 0.0;
          double mcCvc2FeeAmount = 0.0;
          double mcDuesFeeAmount = 0.0;
          double mcAccessFeeAmount = 0.0;
          double mcCardVerifyFeeAmount = 0.0;
          double mcLocationFeeAmount = 0.0;
          double vsApfFeeAmount = 0.0;
          double vsApfdbFeeAmount = 0.0;
          double vsApfIntlFeeAmount = 0.0;
          double vsApfdbIntlFeeAmount = 0.0;
          double vsBISAFeeAmount = 0.0;
          double vsEISAFeeAmount = 0.0;
          double vsIafFeeAmount = 0.0;
          double vsTifFeeAmount = 0.0;
          double vsAuthMFeeAmount = 0.0;
          double vsZFLFeeAmount = 0.0;
          double vsFanfFeeAmount = 0.0;
          double vsDuesFeeAmount = 0.0;
          double vsDTFeeAmount = 0.0;
          double vsCVPDebitFee = 0.0;
          double vsCVPCreditFee = 0.0;
          double vsCVPDebitFeeIntl = 0.0;
          double vsCVPCreditFeeIntl = 0.0;
          double vsAccessFeeAmount = 0.0;
          double vsCardVerifyFeeAmount = 0.0;
          double dbNPFeeAmount = 0.0;
          double dsDUFeeAmount = 0.0;
          double dsARFeeAmount = 0.0;
          double dsDTFeeAmount = 0.0;
          double dsIpfFeeAmount = 0.0;
          double dsIsfFeeAmount = 0.0;
          double dsDuesFeeAmount = 0.0;
          double optbECPFeeAmount  = 0.0;
          double optbExcCBFeeAmount = 0.0;
          double optBAssessmentFeeAmount = 0;
          double optBInboundFeeAmount = 0;
          double optBNonSwipedFeeAmount = 0;
          double optBNonComplFeeAmount = 0;
          double optBDataQualityFeeAmount = 0;
          double optBMerchAccessFeeAmount = 0;
          double mcGlobalWholesaleTravelFeeAmount = 0.0;
          double mcFreightAssessmentFeeAmount = 0.0;


          /* 
           * Store the information that comprise the vmc_assessment_fee into a separate table
           * monthly_extract_exp.
          **/
          try{
              ArrayList<ExpenseRecord> exps = new ArrayList<ExpenseRecord>();
    
              // MasterCard SafetyNet and Fraud Dashboard
              mcSafetyNetFeeAmount = (mcAuthCount * 0.01);
              String mcSafetyNetDesc = "MasterCard SafetyNet and Fraud Dashboard";
              String mcSafetyNetFeeCalc = "mcAuthCount * 0.01";
              ExpenseRecord exprec21 = new ExpenseRecord();
              exprec21.setLoadSec(LoadSec);
              exprec21.setItemCat("EX");
              exprec21.setMerchantNumber(merchantId);
              exprec21.setActiveDate(activeDate);
              exprec21.setDescription(mcSafetyNetDesc);
              exprec21.setRateCalculation(mcSafetyNetFeeCalc);
              exprec21.setItemCount(mcAuthCount);
              exprec21.setItemAmount(0.0);
              exprec21.setTotFeeAmount(mcSafetyNetFeeAmount);
              exps.add(exprec21);
              
              // MC NABU
              mcNabuCount = mcAuthCount - (mcGlobalWholesaleTravelCount + mcFreightAssessmentCount);
              mcNabuFeeAmount = (mcNabuCount * 0.0195);
              String mcNabuDesc = "MC NABU";
              String mcNabuFeeCalc = "mcNabuCount * 0.0195";
              ExpenseRecord exprec1 = new ExpenseRecord();
              exprec1.setLoadSec(LoadSec);
              exprec1.setItemCat("EX");
              exprec1.setMerchantNumber(merchantId);
              exprec1.setActiveDate(activeDate);
              exprec1.setDescription(mcNabuDesc);
              exprec1.setRateCalculation(mcNabuFeeCalc);
              exprec1.setItemCount(mcNabuCount);
              exprec1.setItemAmount(0.0);
              exprec1.setTotFeeAmount(mcNabuFeeAmount);
              exps.add(exprec1);
      
                        
              // MC Base Cross Border
              mcBCBFeeAmount = (mc_cross_border_amount * 0.0060); 
              String mcBCBDesc = "MC Cross Border";
              String mcBCBFeeCalc = "mc_cross_border_amount * 0.0060";
              ExpenseRecord exprec2 = new ExpenseRecord();
              exprec2.setLoadSec(LoadSec);
              exprec2.setItemCat("EX");
              exprec2.setMerchantNumber(merchantId);
              exprec2.setActiveDate(activeDate);
              exprec2.setDescription(mcBCBDesc);
              exprec2.setRateCalculation(mcBCBFeeCalc);
              exprec2.setItemCount(0);
              exprec2.setItemAmount(mc_cross_border_amount);
              exprec2.setTotFeeAmount(mcBCBFeeAmount);
              exps.add(exprec2);
              
    
              // MC Enhanced Cross Border
              mcECBFeeAmount = (mc_enh_cross_border_amount * 0.0100); 
              String mcECBDesc = "MC Enhanced Cross Border";
              String mcECBFeeCalc = "mc_enh_cross_border_amount * 0.0100";
              ExpenseRecord exprec3 = new ExpenseRecord();
              exprec3.setLoadSec(LoadSec);
              exprec3.setItemCat("EX");
              exprec3.setMerchantNumber(merchantId);
              exprec3.setActiveDate(activeDate);
              exprec3.setDescription(mcECBDesc);
              exprec3.setRateCalculation(mcECBFeeCalc);
              exprec3.setItemCount(0);
              exprec3.setItemAmount(mc_enh_cross_border_amount);
              exprec3.setTotFeeAmount(mcECBFeeAmount);
              exps.add(exprec3);
              
              // MC Global Support
              mcGSFeeAmount = (mcGlobalAcquirerSupportAmount * 0.0085);
              String mcGSDesc = "MC Global Support";
              String mcGSFeeCalc = "mcGlobalAcquirerSupportAmount * 0.0085";
              ExpenseRecord exprec4 = new ExpenseRecord();
              exprec4.setLoadSec(LoadSec);
              exprec4.setItemCat("EX");
              exprec4.setMerchantNumber(merchantId);
              exprec4.setActiveDate(activeDate);
              exprec4.setDescription(mcGSDesc);
              exprec4.setRateCalculation(mcGSFeeCalc);
              exprec4.setItemCount(0);
              exprec4.setItemAmount(mcGlobalAcquirerSupportAmount);
              exprec4.setTotFeeAmount(mcGSFeeAmount);
              exps.add(exprec4);
              
              //MC Large ticket dues
              mcLTDFeeAmount = (MbsBackend * amounts[6] * 0.0001); 
              String mcLTDDesc = "MC large ticket dues";
              String mcLTDFeeCalc = "MbsBackend * amounts[6] * 0.0001";
              ExpenseRecord exprec5 = new ExpenseRecord();
              exprec5.setLoadSec(LoadSec);
              exprec5.setItemCat("EX");
              exprec5.setMerchantNumber(merchantId);
              exprec5.setActiveDate(activeDate);
              exprec5.setDescription(mcLTDDesc);
              exprec5.setRateCalculation(mcLTDFeeCalc);
              exprec5.setItemCount(0);
              exprec5.setItemAmount(amounts[6]);
              exprec5.setTotFeeAmount(mcLTDFeeAmount);
              exps.add(exprec5);
             
    
              //MC Digital Enablement Fee
              mcDEFeeAmount = mcDigitalEnablementFees;
              String mcDEDesc = "MC Digital Enablement Fee";
              String mcDEFeeCalc = "mcDigitalEnablementFees";
              ExpenseRecord exprec6 = new ExpenseRecord();
              exprec6.setLoadSec(LoadSec);
              exprec6.setItemCat("EX");
              exprec6.setMerchantNumber(merchantId);
              exprec6.setActiveDate(activeDate);
              exprec6.setDescription(mcDEDesc);
              exprec6.setRateCalculation(mcDEFeeCalc);
              exprec6.setItemCount(0);
              exprec6.setItemAmount(0.0);
              exprec6.setTotFeeAmount(mcDEFeeAmount);
              exps.add(exprec6);
              
              //MC Acquirer License Fee
              mcALFeeAmount = (MbsBackend * mcSalesAmount * 0.00015);
              String mcALDesc = "MC Acquirer License Fee";
              String mcALFeeCalc = "MbsBackend * mcSalesAmount * 0.00015";
              ExpenseRecord exprec7 = new ExpenseRecord();
              exprec7.setLoadSec(LoadSec);
              exprec7.setItemCat("EX");
              exprec7.setMerchantNumber(merchantId);
              exprec7.setActiveDate(activeDate);
              exprec7.setDescription(mcALDesc);
              exprec7.setRateCalculation(mcALFeeCalc);
              exprec7.setItemCount(0);
              exprec7.setItemAmount(mcSalesAmount);
              exprec7.setTotFeeAmount(mcALFeeAmount);
              exps.add(exprec7);
              
              /* commented as the MC TIF is not used any more //MC TIF
              mcTifFeeAmount = (counts[6] * 0.055);
              String mcTifDesc = "MC TIF";
              String mcTifFeeCalc = "counts[6] * 0.055";
              ExpenseRecord exprec8 = new ExpenseRecord();
              exprec8.setLoadSec(LoadSec);
              exprec8.setItemCat("EX");
              exprec8.setMerchantNumber(merchantId);
              exprec8.setActiveDate(activeDate);
              exprec8.setDescription(mcTifDesc);
              exprec8.setRateCalculation(mcTifFeeCalc);
              exprec8.setItemCount(counts[6]);
              exprec8.setItemAmount(0.0);
              exprec8.setTotFeeAmount(mcTifFeeAmount);
              exps.add(exprec8); */
                            
              //MC CVC2
              mcCvc2FeeAmount = (counts[7] * 0.0025);
              String mcCvc2Desc = "MC CVC2";
              String mcCvc2FeeCalc = "counts[7] * 0.0025";
              ExpenseRecord exprec9 = new ExpenseRecord();
              exprec9.setLoadSec(LoadSec);
              exprec9.setItemCat("EX");
              exprec9.setMerchantNumber(merchantId);
              exprec9.setActiveDate(activeDate);
              exprec9.setDescription(mcCvc2Desc);
              exprec9.setRateCalculation(mcCvc2FeeCalc);
              exprec9.setItemCount(counts[7]);
              exprec9.setItemAmount(0.0);
              exprec9.setTotFeeAmount(mcCvc2FeeAmount);
              exps.add(exprec9);
             
              //MC Dues
              mcDuesFeeAmount = (MbsBackend * mcSalesAmount * 0.0013);
              String mcDuesDesc = "MC Dues";
              String mcDuesFeeCalc = "MbsBackend * mcSalesAmount * 0.0013";
              ExpenseRecord exprec10 = new ExpenseRecord();
              exprec10.setLoadSec(LoadSec);
              exprec10.setItemCat("EX");
              exprec10.setMerchantNumber(merchantId);
              exprec10.setActiveDate(activeDate);
              exprec10.setDescription(mcDuesDesc);
              exprec10.setRateCalculation(mcDuesFeeCalc);
              exprec10.setItemCount(0);
              exprec10.setItemAmount(mcSalesAmount);
              exprec10.setTotFeeAmount(mcDuesFeeAmount);
              exps.add(exprec10);

              //MC Fixed Access Fee
              mcAccessFeeAmount = mcAccessFeeCount * 0.008;
              String mcAccessFeeDesc = "MC Fixed Access Fee";
              String mcAccessFeeCalc = "mcAccessFeeCount * 0.008";
              ExpenseRecord exprec33 = new ExpenseRecord();
              exprec33.setLoadSec(LoadSec);
              exprec33.setItemCat("EX");
              exprec33.setMerchantNumber(merchantId);
              exprec33.setActiveDate(activeDate);
              exprec33.setDescription(mcAccessFeeDesc);
              exprec33.setRateCalculation(mcAccessFeeCalc);
              exprec33.setItemCount(mcAccessFeeCount);
              exprec33.setItemAmount(0.0);
              exprec33.setTotFeeAmount(mcAccessFeeAmount);
              exps.add(exprec33);

              //MC Zero Dollar Verification Fee
              mcCardVerifyFeeAmount = mcCardVerifyFeeCount * 0.025;
              String mcCardVerifyFeeDesc = "MC Zero Dollar Verification Fee";
              String mcCardVerifyFeeCalc = "mcCardVerifyFeeCount * 0.025";
              ExpenseRecord exprec34 = new ExpenseRecord();
              exprec34.setLoadSec(LoadSec);
              exprec34.setItemCat("EX");
              exprec34.setMerchantNumber(merchantId);
              exprec34.setActiveDate(activeDate);
              exprec34.setDescription(mcCardVerifyFeeDesc);
              exprec34.setRateCalculation(mcCardVerifyFeeCalc);
              exprec34.setItemCount(mcCardVerifyFeeCount);
              exprec34.setItemAmount(0.0);
              exprec34.setTotFeeAmount(mcCardVerifyFeeAmount);
              exps.add(exprec34);

              //MC Monthly Location Fee
              mcLocationFeeAmount = mcLocationFee;
              String mcLocFeeDesc = "MC Monthly Location Fee";
              String mcLocFeeCalc = "mcLocationFee";
              ExpenseRecord exprec44 = new ExpenseRecord();
              exprec44.setLoadSec(LoadSec);
              exprec44.setItemCat("EX");
              exprec44.setMerchantNumber(merchantId);
              exprec44.setActiveDate(activeDate);
              exprec44.setDescription(mcLocFeeDesc);
              exprec44.setRateCalculation(mcLocFeeCalc);
              exprec44.setItemCount(0);
              exprec44.setItemAmount(0.0);
              exprec44.setTotFeeAmount(mcLocationFeeAmount);
              exps.add(exprec44);

              //Visa APF
              vsApfFeeAmount = ((counts[3]) * 0.0195);
              String vsApfDesc = "Visa APF";
              String vsApfFeeCalc = "(counts[3]) * 0.0195";
              ExpenseRecord exprec11 = new ExpenseRecord();
              exprec11.setLoadSec(LoadSec);
              exprec11.setItemCat("EX");
              exprec11.setMerchantNumber(merchantId);
              exprec11.setActiveDate(activeDate);
              exprec11.setDescription(vsApfDesc);
              exprec11.setRateCalculation(vsApfFeeCalc);
              exprec11.setItemCount(counts[3]);
              exprec11.setItemAmount(0.0);
              exprec11.setTotFeeAmount(vsApfFeeAmount);
              exps.add(exprec11);
              
              //VISA APF db
              vsApfdbFeeAmount = (counts[4] * 0.0155); 
              String vsADesc = "Visa APF db";
              String vsApfdbFeeCalc = "counts[4] * 0.0155";
              ExpenseRecord exprec12 = new ExpenseRecord();
              exprec12.setLoadSec(LoadSec);
              exprec12.setItemCat("EX");
              exprec12.setMerchantNumber(merchantId);
              exprec12.setActiveDate(activeDate);
              exprec12.setDescription(vsADesc);
              exprec12.setRateCalculation(vsApfdbFeeCalc);
              exprec12.setItemCount(counts[4]);
              exprec12.setItemAmount(0.0);
              exprec12.setTotFeeAmount(vsApfdbFeeAmount);
              exps.add(exprec12);

              //Visa APF Intl
              vsApfIntlFeeAmount = (vsApfIntlFeeCount * 0.0395);
              String vsApfIntlDesc = "Visa APF Intl";
              String vsApfIntlFeeCalc = "vsApfIntlFeeCount * 0.0395";
              ExpenseRecord exprec111 = new ExpenseRecord();
              exprec111.setLoadSec(LoadSec);
              exprec111.setItemCat("EX");
              exprec111.setMerchantNumber(merchantId);
              exprec111.setActiveDate(activeDate);
              exprec111.setDescription(vsApfIntlDesc);
              exprec111.setRateCalculation(vsApfIntlFeeCalc);
              exprec111.setItemCount(vsApfIntlFeeCount);
              exprec111.setItemAmount(0.0);
              exprec111.setTotFeeAmount(vsApfIntlFeeAmount);
              exps.add(exprec111);
              
              //VISA APF db Intl
              vsApfdbIntlFeeAmount = (vsApfdbIntlFeeCount * 0.0355); 
              String vsAIntlDesc = "Visa APF db Intl";
              String vsApfdbIntlFeeCalc = "vsApfdbIntlFeeCount * 0.0355";
              ExpenseRecord exprec112 = new ExpenseRecord();
              exprec112.setLoadSec(LoadSec);
              exprec112.setItemCat("EX");
              exprec112.setMerchantNumber(merchantId);
              exprec112.setActiveDate(activeDate);
              exprec112.setDescription(vsAIntlDesc);
              exprec112.setRateCalculation(vsApfdbIntlFeeCalc);
              exprec112.setItemCount(vsApfdbIntlFeeCount);
              exprec112.setItemAmount(0.0);
              exprec112.setTotFeeAmount(vsApfdbIntlFeeAmount);
              exps.add(exprec112);
              
              //VISA Base ISA
              vsBISAFeeAmount = (visa_isa_amount * 0.0100);
              String vsBISADesc = "Visa Base ISA";
              String vsBISAFeeCalc = "visa_isa_amount * 0.0100";
              ExpenseRecord exprec13 = new ExpenseRecord();   
              exprec13.setLoadSec(LoadSec);
              exprec13.setItemCat("EX");
              exprec13.setMerchantNumber(merchantId);
              exprec13.setActiveDate(activeDate);
              exprec13.setDescription(vsBISADesc);
              exprec13.setRateCalculation(vsBISAFeeCalc);
              exprec13.setItemCount(0);
              exprec13.setItemAmount(visa_isa_amount);
              exprec13.setTotFeeAmount(vsBISAFeeAmount);
              exps.add(exprec13);
              
              //Visa Enhanced ISA
              vsEISAFeeAmount = (visa_enh_isa_amount * 0.0140);
              String vsEISADesc = "Visa Enhanced ISA";
              String vsEISAFeeCalc = "visa_enh_isa_amount * 0.0140";
              ExpenseRecord exprec14 = new ExpenseRecord();   
              exprec14.setLoadSec(LoadSec);
              exprec14.setItemCat("EX");
              exprec14.setMerchantNumber(merchantId);
              exprec14.setActiveDate(activeDate);
              exprec14.setDescription(vsEISADesc);
              exprec14.setRateCalculation(vsEISAFeeCalc);
              exprec14.setItemCount(0);
              exprec14.setItemAmount(visa_enh_isa_amount);
              exprec14.setTotFeeAmount(vsEISAFeeAmount);
              exps.add(exprec14);
              
              //Visa IAf
              vsIafFeeAmount = (amounts[0] * 0.0045);
              String vsIafDesc = "Visa IAF";
              String vsIafFeeCalc = "amounts[0] * 0.0045";
              ExpenseRecord exprec15 = new ExpenseRecord();
              exprec15.setLoadSec(LoadSec);
              exprec15.setItemCat("EX");
              exprec15.setMerchantNumber(merchantId);
              exprec15.setActiveDate(activeDate);
              exprec15.setDescription(vsIafDesc);
              exprec15.setRateCalculation(vsIafFeeCalc);
              exprec15.setItemCount(0);
              exprec15.setItemAmount(amounts[0]);
              exprec15.setTotFeeAmount(vsIafFeeAmount);
              exps.add(exprec15);
              
              // Visa TIF
              vsTifFeeAmount = (counts[5] * 0.100);
              String vsTifDesc = "Visa TIF";
              String vsTifFeeCalc = "counts[5] * 0.100";
              ExpenseRecord exprec16 = new ExpenseRecord();
              exprec16.setLoadSec(LoadSec);
              exprec16.setItemCat("EX");
              exprec16.setMerchantNumber(merchantId);
              exprec16.setActiveDate(activeDate);
              exprec16.setDescription(vsTifDesc);
              exprec16.setRateCalculation(vsTifFeeCalc);
              exprec16.setItemCount(counts[5]);
              exprec16.setItemAmount(0.0);
              exprec16.setTotFeeAmount(vsTifFeeAmount);
              exps.add(exprec16);
              
              //Visa Misue of Auth
              vsAuthMFeeAmount = (counts[8] * 0.09);
              String vsAuthMDesc = "Visa Misuse of Auth";
              String vsAuthMFeeCalc = "counts[8] * 0.09";
              ExpenseRecord exprec17 = new ExpenseRecord();
              exprec17.setLoadSec(LoadSec);
              exprec17.setItemCat("EX");
              exprec17.setMerchantNumber(merchantId);
              exprec17.setActiveDate(activeDate);
              exprec17.setDescription(vsAuthMDesc);
              exprec17.setRateCalculation(vsAuthMFeeCalc);
              exprec17.setItemCount(counts[8]);
              exprec17.setItemAmount(0.0);
              exprec17.setTotFeeAmount(vsAuthMFeeAmount);
              exps.add(exprec17);
              
              //Visa Zero Floor Limit
              vsZFLFeeAmount = (counts[9] * 0.20);
              String vsZFLDesc  = "Visa Zero Floor Limit";
              String vsZFLFeeCalc = "counts[9] * 0.20";
              ExpenseRecord exprec18 = new ExpenseRecord();
              exprec18.setLoadSec(LoadSec);
              exprec18.setItemCat("EX");
              exprec18.setMerchantNumber(merchantId);
              exprec18.setActiveDate(activeDate);
              exprec18.setDescription(vsZFLDesc);
              exprec18.setRateCalculation(vsZFLFeeCalc);
              exprec18.setItemCount(counts[9]);
              exprec18.setItemAmount(0.0);
              exprec18.setTotFeeAmount(vsZFLFeeAmount);
              exps.add(exprec18);
              
              //Visa FANF
              vsFanfFeeAmount = amounts[7];  
              String vsFanfDesc = "Visa FANF";
              String vsFanfFeeCalc = "amounts[7]";
              ExpenseRecord exprec19 = new ExpenseRecord();
              exprec19.setLoadSec(LoadSec);
              exprec19.setItemCat("EX");
              exprec19.setMerchantNumber(merchantId);
              exprec19.setActiveDate(activeDate);
              exprec19.setDescription(vsFanfDesc);
              exprec19.setRateCalculation(vsFanfFeeCalc);
              exprec19.setItemCount(0);
              exprec19.setItemAmount(amounts[7]);
              exprec19.setTotFeeAmount(vsFanfFeeAmount);
              exps.add(exprec19);
              
              //Visa dues
              vsDuesFeeAmount = (MbsBackend * ((vCSalesAmount * 0.0014) + (vdSalesAmount * 0.0013)));
              log.debug("Visa Credit Sales Amount aka vsSalesAmount : " + vCSalesAmount);
              log.debug("Visa Debit Sales Amount aka vdSalesAmount  : " + vdSalesAmount);
              log.debug("Tot Fee Amount aka vsDuesFeeAmount  : " + vsDuesFeeAmount);
              String vsDuesDesc = "Visa dues";
              log.debug("DESCRIPTION: " + vsDuesDesc);
              String vsDuesFeeCalc = "MbsBackend * ((vCSalesAmount * 0.0014) + (vdSalesAmount * 0.0013))";
              log.debug("RATE CALCULATION: " + vsDuesFeeCalc);
              ExpenseRecord exprec20 = new ExpenseRecord();
              exprec20.setLoadSec(LoadSec);
              exprec20.setItemCat("EX");
              exprec20.setMerchantNumber(merchantId);
              exprec20.setActiveDate(activeDate);
              exprec20.setDescription(vsDuesDesc);
              exprec20.setRateCalculation(vsDuesFeeCalc);
              exprec20.setItemCount(0);
              exprec20.setItemAmount(vsSalesAmount);
              exprec20.setTotFeeAmount(vsDuesFeeAmount);
              exps.add(exprec20);
    
              //Visa Data Transmision Fee
              vsDTFeeAmount = vsVolCount * 0.0018;
              String vsDTDesc = "Visa Data Transmission Fee";
              String vsDTFeeCalc = "vsVolCount * 0.0018";
              ExpenseRecord exprec29 = new ExpenseRecord();
              exprec29.setLoadSec(LoadSec);
              exprec29.setItemCat("EX");
              exprec29.setMerchantNumber(merchantId);
              exprec29.setActiveDate(activeDate);
              exprec29.setDescription(vsDTDesc);
              exprec29.setRateCalculation(vsDTFeeCalc);
              exprec29.setItemCount(vsVolCount);
              exprec29.setItemAmount(0.0);
              exprec29.setTotFeeAmount(vsDTFeeAmount);
              exps.add(exprec29);

              //Visa Credit Voucher Processing Debit Fee
              vsCVPDebitFee = vsCVPDebitCount * 0.0155;
              String vsCVPDBDesc = "Visa CVP Debit Fee";
              String vsCVPDBFeeCalc = "vsCVPDebitCount * 0.0155";
              ExpenseRecord exprec30 = new ExpenseRecord();
              exprec30.setLoadSec(LoadSec);
              exprec30.setItemCat("EX");
              exprec30.setMerchantNumber(merchantId);
              exprec30.setActiveDate(activeDate);
              exprec30.setDescription(vsCVPDBDesc);
              exprec30.setRateCalculation(vsCVPDBFeeCalc);
              exprec30.setItemCount(vsCVPDebitCount);
              exprec30.setItemAmount(0.0);
              exprec30.setTotFeeAmount(vsCVPDebitFee);
              exps.add(exprec30);

              //Visa Credit Voucher Processing Credit Fee
              vsCVPCreditFee = vsCVPCreditCount * 0.0195;
              String vsCVPCRDesc = "Visa CVP Credit Fee";
              String vsCVPCRFeeCalc = "vsCVPCreditCount * 0.0195";
              ExpenseRecord exprec31 = new ExpenseRecord();
              exprec31.setLoadSec(LoadSec);
              exprec31.setItemCat("EX");
              exprec31.setMerchantNumber(merchantId);
              exprec31.setActiveDate(activeDate);
              exprec31.setDescription(vsCVPCRDesc);
              exprec31.setRateCalculation(vsCVPCRFeeCalc);
              exprec31.setItemCount(vsCVPCreditCount);
              exprec31.setItemAmount(0.0);
              exprec31.setTotFeeAmount(vsCVPCreditFee);
              exps.add(exprec31);

              //Visa Credit Voucher Processing Debit Fee - Intl
              vsCVPDebitFeeIntl = vsCVPDebitCountIntl * 0.0355;
              String vsCVPDBIntlDesc = "Visa CVP Debit Fee - Intl";
              String vsCVPDBInltFeeCalc = "vsCVPDebitCountIntl * 0.0355";
              ExpenseRecord exprec47 = new ExpenseRecord();
              exprec47.setLoadSec(LoadSec);
              exprec47.setItemCat("EX");
              exprec47.setMerchantNumber(merchantId);
              exprec47.setActiveDate(activeDate);
              exprec47.setDescription(vsCVPDBIntlDesc);
              exprec47.setRateCalculation(vsCVPDBInltFeeCalc);
              exprec47.setItemCount(vsCVPDebitCountIntl);
              exprec47.setItemAmount(0.0);
              exprec47.setTotFeeAmount(vsCVPDebitFeeIntl);
              exps.add(exprec47);

              //Visa Credit Voucher Processing Credit Fee - Intl
              vsCVPCreditFeeIntl = vsCVPCreditCountIntl * 0.0395;
              String vsCVPCRIntlDesc = "Visa CVP Credit Fee - Intl";
              String vsCVPCRIntlFeeCalc = "vsCVPCreditCountIntl * 0.0395";
              ExpenseRecord exprec48= new ExpenseRecord();
              exprec48.setLoadSec(LoadSec);
              exprec48.setItemCat("EX");
              exprec48.setMerchantNumber(merchantId);
              exprec48.setActiveDate(activeDate);
              exprec48.setDescription(vsCVPCRIntlDesc);
              exprec48.setRateCalculation(vsCVPCRIntlFeeCalc);
              exprec48.setItemCount(vsCVPCreditCountIntl);
              exprec48.setItemAmount(0.0);
              exprec48.setTotFeeAmount(vsCVPCreditFeeIntl);
              exps.add(exprec48);

              //Visa Fixed Access Fee
              vsAccessFeeAmount = vsAccessFeeCount * 0.008;
              String vsAccessFeeDesc = "Visa Fixed Access Fee";
              String vsAccessFeeCalc = "vsAccessFeeCount * 0.008";
              ExpenseRecord exprec32 = new ExpenseRecord();
              exprec32.setLoadSec(LoadSec);
              exprec32.setItemCat("EX");
              exprec32.setMerchantNumber(merchantId);
              exprec32.setActiveDate(activeDate);
              exprec32.setDescription(vsAccessFeeDesc);
              exprec32.setRateCalculation(vsAccessFeeCalc);
              exprec32.setItemCount(vsAccessFeeCount);
              exprec32.setItemAmount(0.0);
              exprec32.setTotFeeAmount(vsAccessFeeAmount);
              exps.add(exprec32);

              //VS Zero Dollar Verification Fee
              vsCardVerifyFeeAmount = vsCardVerifyFeeCount * 0.025;
              String vsCardVerifyFeeDesc = "VS Zero Dollar Verification Fee";
              String vsCardVerifyFeeCalc = "vsCardVerifyFeeCount * 0.025";
              ExpenseRecord exprec35 = new ExpenseRecord();
              exprec35.setLoadSec(LoadSec);
              exprec35.setItemCat("EX");
              exprec35.setMerchantNumber(merchantId);
              exprec35.setActiveDate(activeDate);
              exprec35.setDescription(vsCardVerifyFeeDesc);
              exprec35.setRateCalculation(vsCardVerifyFeeCalc);
              exprec35.setItemCount(vsCardVerifyFeeCount);
              exprec35.setItemAmount(0.0);
              exprec35.setTotFeeAmount(vsCardVerifyFeeAmount);
              exps.add(exprec35);

              //Debit Network Participation Fee
              dbNPFeeAmount = (MbsBackend * (counts[10]) * 4.50);
              String dbNPFeeDesc = "Debit Network Participation Fee";
              String dbNPFeeCalc = "MbsBackend * (counts[10]) * 4.50";
              ExpenseRecord exprec22 = new ExpenseRecord();
              exprec22.setLoadSec(LoadSec);
              exprec22.setItemCat("EX");
              exprec22.setMerchantNumber(merchantId);
              exprec22.setActiveDate(activeDate);
              exprec22.setDescription(dbNPFeeDesc);
              exprec22.setRateCalculation(dbNPFeeCalc);
              exprec22.setItemCount(counts[10]);
              exprec22.setItemAmount(0.0);
              exprec22.setTotFeeAmount(dbNPFeeAmount);
              exps.add(exprec22);
              
              //Discover Data Usage Fee
              dsDUFeeAmount = (dsAuthCount * 0.0195);
              String dsDUDesc = "Discover Data Usage Fee";
              String dsDuFeeCalc = "dsAuthCount * 0.0195";
              ExpenseRecord exprec23 = new ExpenseRecord();
              exprec23.setLoadSec(LoadSec);
              exprec23.setItemCat("EX");
              exprec23.setMerchantNumber(merchantId);
              exprec23.setActiveDate(activeDate);
              exprec23.setDescription(dsDUDesc);
              exprec23.setRateCalculation(dsDuFeeCalc);
              exprec23.setItemCount(dsAuthCount);
              exprec23.setItemAmount(0.0);
              exprec23.setTotFeeAmount(dsDUFeeAmount);
              exps.add(exprec23);
              
              //Discover Auth Request Fee
              dsARFeeAmount = (dsAuthCount * 0.0025); 
              String dsARDesc = "Discover Auth Request Fee"; 
              String dsARFeeCalc = "dsAuthCount * 0.0025";
              ExpenseRecord exprec24 = new ExpenseRecord();
              exprec24.setLoadSec(LoadSec);
              exprec24.setItemCat("EX");
              exprec24.setMerchantNumber(merchantId);
              exprec24.setActiveDate(activeDate);
              exprec24.setDescription(dsARDesc);
              exprec24.setRateCalculation(dsARFeeCalc);
              exprec24.setItemCount(dsAuthCount);
              exprec24.setItemAmount(0.0);
              exprec24.setTotFeeAmount(dsARFeeAmount);
              exps.add(exprec24);
             
              //Discover IPF
              dsIpfFeeAmount = (amounts[2] * 0.0050);
              String dsIpfDesc = "Discover IPF";
              String dsIpfFeeCalc = "amounts[2] * 0.0050";
              ExpenseRecord exprec26 = new ExpenseRecord();
              exprec26.setLoadSec(LoadSec);
              exprec26.setItemCat("EX");
              exprec26.setMerchantNumber(merchantId);
              exprec26.setActiveDate(activeDate);
              exprec26.setDescription(dsIpfDesc);
              exprec26.setRateCalculation(dsIpfFeeCalc);
              exprec26.setItemCount(0);
              exprec26.setItemAmount(amounts[2]);
              exprec26.setTotFeeAmount(dsIpfFeeAmount);
              exps.add(exprec26);
              
              //Discover ISF
              dsIsfFeeAmount = ((amounts[2]) * 0.0080);
              String dsIsfDesc = "Discover ISF";
              String dsIsfFeeCalc = "(amounts[2]) * 0.0080";
              ExpenseRecord exprec27 = new ExpenseRecord();
              exprec27.setLoadSec(LoadSec);
              exprec27.setItemCat("EX");
              exprec27.setMerchantNumber(merchantId);
              exprec27.setActiveDate(activeDate);
              exprec27.setDescription(dsIsfDesc);
              exprec27.setRateCalculation(dsIsfFeeCalc);
              exprec27.setItemCount(0);
              exprec27.setItemAmount(amounts[2]);
              exprec27.setTotFeeAmount(dsIsfFeeAmount);
              exps.add(exprec27);
              
              //Discover Dues
              dsDuesFeeAmount = (MbsBackend * dsSalesAmount * 0.0013);
              String dsDuesDesc = "Discover dues";
              String dsDuesFeeCalc = "MbsBackend * dsSalesAmount * 0.0013";
              ExpenseRecord exprec28 = new ExpenseRecord();
              exprec28.setLoadSec(LoadSec);
              exprec28.setItemCat("EX");
              exprec28.setMerchantNumber(merchantId);
              exprec28.setActiveDate(activeDate);
              exprec28.setDescription(dsDuesDesc);
              exprec28.setRateCalculation(dsDuesFeeCalc);
              exprec28.setItemCount(0);
              exprec28.setItemAmount(dsSalesAmount);
              exprec28.setTotFeeAmount(dsDuesFeeAmount);
              exps.add(exprec28);
              
              // Amex OptBlue Excessive Chargeback Program
              optbECPFeeAmount = optbECPFees;
              String optbECPDesc = "AM OptBlue Excessive Chargeback Program";
              String optbECPFeeCalc = "optbECPFeeAmount";
              ExpenseRecord exprec36 = new ExpenseRecord();
              exprec36.setLoadSec(LoadSec);
              exprec36.setItemCat("EX");
              exprec36.setMerchantNumber(merchantId);
              exprec36.setActiveDate(activeDate);
              exprec36.setDescription(optbECPDesc);
              exprec36.setRateCalculation(optbECPFeeCalc);
              exprec36.setItemCount(0);
              exprec36.setItemAmount(0.0);
              exprec36.setTotFeeAmount(optbECPFeeAmount);
              exps.add(exprec36);
              
              // Amex OptBlue Excessive Chargeback Fee
              optbExcCBFeeAmount = optbExcCBFees;
              String optbExcCBDesc = "AM OptBlue Excessive Chargeback Fee";
              String optbExcCBFeeCalc = "optbExcCBFeeAmount";
              ExpenseRecord exprec37 = new ExpenseRecord();
              exprec37.setLoadSec(LoadSec);
              exprec37.setItemCat("EX");
              exprec37.setMerchantNumber(merchantId);
              exprec37.setActiveDate(activeDate);
              exprec37.setDescription(optbExcCBDesc);
              exprec37.setRateCalculation(optbExcCBFeeCalc);
              exprec37.setItemCount(0);
              exprec37.setItemAmount(0.0);
              exprec37.setTotFeeAmount(optbExcCBFeeAmount);
              exps.add(exprec37);

              // Amex OptBlue Assessment Fee
              optBAssessmentFeeAmount =  (optBAssessmentAmount * 0.0015);
              String optBAssessmentFeeDesc = "AM OptBlue Assessment Fee";
              String optBAssessmentFeeCalc = "optBAssessmentAmount * 0.0015";
              ExpenseRecord exprec38 = new ExpenseRecord();
              exprec38.setLoadSec(LoadSec);
              exprec38.setItemCat("EX");
              exprec38.setMerchantNumber(merchantId);
              exprec38.setActiveDate(activeDate);
              exprec38.setDescription(optBAssessmentFeeDesc);
              exprec38.setRateCalculation(optBAssessmentFeeCalc);
              exprec38.setItemCount(0);
              exprec38.setItemAmount(0.0);
              exprec38.setTotFeeAmount(optBAssessmentFeeAmount);
              exps.add(exprec38);

              // Amex OptBlue Inbound Fee
              optBInboundFeeAmount =  (optBInboundAmount * 0.0040);
              String optBInboundFeeDesc = "AM OptBlue Inbound Fee";
              String optBInboundFeeCalc = "optBInboundAmount * 0.0040";
              ExpenseRecord exprec39 = new ExpenseRecord();
              exprec39.setLoadSec(LoadSec);
              exprec39.setItemCat("EX");
              exprec39.setMerchantNumber(merchantId);
              exprec39.setActiveDate(activeDate);
              exprec39.setDescription(optBInboundFeeDesc);
              exprec39.setRateCalculation(optBInboundFeeCalc);
              exprec39.setItemCount(0);
              exprec39.setItemAmount(0.0);
              exprec39.setTotFeeAmount(optBInboundFeeAmount);
              exps.add(exprec39);

              // Amex OptBlue Non Swiped Transaction Fee
              optBNonSwipedFeeAmount =  (optBNonSwipedAmount * 0.0030);
              String optBNonSwipedFeeDesc = "AM OptBlue Non Swiped Fee";
              String optBNonSwipedFeeCalc = "optBNonSwipedAmount * 0.0030";
              ExpenseRecord exprec40 = new ExpenseRecord();
              exprec40.setLoadSec(LoadSec);
              exprec40.setItemCat("EX");
              exprec40.setMerchantNumber(merchantId);
              exprec40.setActiveDate(activeDate);
              exprec40.setDescription(optBNonSwipedFeeDesc);
              exprec40.setRateCalculation(optBNonSwipedFeeCalc);
              exprec40.setItemCount(0);
              exprec40.setItemAmount(0.0);
              exprec40.setTotFeeAmount(optBNonSwipedFeeAmount);
              exps.add(exprec40);

              // Amex OptBlue Non Compliance Fee
              optBNonComplFeeAmount =  (optBNonComplAmount * 0.0075);
              String optBNonComplFeeDesc = "AM OptBlue Non Compliance Fee";
              String optBNonComplFeeCalc = "optBNonComplAmount * 0.0075";
              ExpenseRecord exprec41 = new ExpenseRecord();
              exprec41.setLoadSec(LoadSec);
              exprec41.setItemCat("EX");
              exprec41.setMerchantNumber(merchantId);
              exprec41.setActiveDate(activeDate);
              exprec41.setDescription(optBNonComplFeeDesc);
              exprec41.setRateCalculation(optBNonComplFeeCalc);
              exprec41.setItemCount(0);
              exprec41.setItemAmount(0.0);
              exprec41.setTotFeeAmount(optBNonComplFeeAmount);
              exps.add(exprec41);

              // Amex OptBlue Data Quality Fee
              optBDataQualityFeeAmount =  (optBDataQualityAmount * 0.0075);
              String optBDataQualityFeeDesc = "AM OptBlue Data Quality Fee";
              String optBDataQualityFeeCalc = "optBDataQualityAmount * 0.0075";
              ExpenseRecord exprec42 = new ExpenseRecord();
              exprec42.setLoadSec(LoadSec);
              exprec42.setItemCat("EX");
              exprec42.setMerchantNumber(merchantId);
              exprec42.setActiveDate(activeDate);
              exprec42.setDescription(optBDataQualityFeeDesc);
              exprec42.setRateCalculation(optBDataQualityFeeCalc);
              exprec42.setItemCount(0);
              exprec42.setItemAmount(0.0);
              exprec42.setTotFeeAmount(optBDataQualityFeeAmount);

              // Amex OptBlue Existing ESA Merchant Access Fee
              optBMerchAccessFeeAmount =  optbMerchAccessFee;
              String optBMerchAccessFeeDesc = "AM OptBlue Existing ESA Merchant Access Fee";
              String optBMerchAccessFeeCalc = "optBMerchAccessFeeAmount";
              ExpenseRecord exprec43 = new ExpenseRecord();
              exprec43.setLoadSec(LoadSec);
              exprec43.setItemCat("EX");
              exprec43.setMerchantNumber(merchantId);
              exprec43.setActiveDate(activeDate);
              exprec43.setDescription(optBMerchAccessFeeDesc);
              exprec43.setRateCalculation(optBMerchAccessFeeCalc);
              exprec43.setItemCount(0);
              exprec43.setItemAmount(0.0);
              exprec43.setTotFeeAmount(optBMerchAccessFeeAmount);
              exps.add(exprec43);

              //MC Process Integrity Fee Pre Auth
              String mcPreAuthDesc = "MC Process Integrity Fee Pre Auth";
              String mcPreAuthFeeCalc = "counts[6] * 0.060";
              ExpenseRecord exprec45 = new ExpenseRecord();
              exprec45.setLoadSec(LoadSec);
              exprec45.setItemCat("EX");
              exprec45.setMerchantNumber(merchantId);
              exprec45.setActiveDate(activeDate);
              exprec45.setDescription(mcPreAuthDesc);
              exprec45.setRateCalculation(mcPreAuthFeeCalc);
              exprec45.setItemCount(counts[6]);
              exprec45.setItemAmount(mcPreAuthAmount);
              exprec45.setTotFeeAmount(mcPreAuthFeeAmount);
              exps.add(exprec45);

              
              //MC Process Integrity Fee Final Auth
              String mcFinalAuthDesc = "MC Process Integrity Fee Final Auth";
              String mcFinalAuthFeeCalc ="(if (0.0025authAmount)>0.04 then (0.0025authAmount) else 0.04)+0.015";
              ExpenseRecord exprec46 = new ExpenseRecord();
              exprec46.setLoadSec(LoadSec);
              exprec46.setItemCat("EX");
              exprec46.setMerchantNumber(merchantId);
              exprec46.setActiveDate(activeDate);
              exprec46.setDescription(mcFinalAuthDesc);
              exprec46.setRateCalculation(mcFinalAuthFeeCalc);
              exprec46.setItemCount(mcFinalAuthCount);
              exprec46.setItemAmount(mcFinalAuthAmount);
              exprec46.setTotFeeAmount(mcFinalAuthFeeAmount);
              exps.add(exprec46);
              
              //MC Global Wholesale Travel Transaction fee
              mcGlobalWholesaleTravelFeeAmount = mcGloablWholesaleTravelAmount * 0.0157;
              String mcGlobalWholesale = "MC Global Wholesale Travel Transaction fee";
              String mcGlobalWholesaleFeeCalc ="mcGloablWholesaleTravelAmount * 0.0157";
              ExpenseRecord exprec49 = new ExpenseRecord();
              exprec49.setLoadSec(LoadSec);
              exprec49.setItemCat("EX");
              exprec49.setMerchantNumber(merchantId);
              exprec49.setActiveDate(activeDate);
              exprec49.setDescription(mcGlobalWholesale);
              exprec49.setRateCalculation(mcGlobalWholesaleFeeCalc);
              exprec49.setItemCount(0);
              exprec49.setItemAmount(mcGloablWholesaleTravelAmount);
              exprec49.setTotFeeAmount(mcGlobalWholesaleTravelFeeAmount);
              exps.add(exprec49);
                           
              //MC Excessive Authorization Fee
              String mcExcessiveAuthFee = "MC Excessive Authorization Fee";
              String mcExcessiveAuthFeeCalc ="mc_excessive_auth_count * 0.10";
              ExpenseRecord exprec50 = new ExpenseRecord();
              exprec50.setLoadSec(LoadSec);
              exprec50.setItemCat("EX");
              exprec50.setMerchantNumber(merchantId);
              exprec50.setActiveDate(activeDate);
              exprec50.setDescription(mcExcessiveAuthFee);
              exprec50.setRateCalculation(mcExcessiveAuthFeeCalc);
              exprec50.setItemCount(mcExcessiveAuthCount);
              exprec50.setItemAmount(mcExcessiveAuthAmount);
              exprec50.setTotFeeAmount(mcExcessiveAuthFeeAmount);
              exps.add(exprec50);
              
               //MC Freight Assessment fee
              mcFreightAssessmentFeeAmount = mcFreightAssessmentAmount * 0.0050;
              String mcFreightAssessment = "MC Freight Assessment fee";
              String mcFreightAssessmentFeeCalc ="mcFreightAssessmentAmount * 0.0050";
              ExpenseRecord exprec51 = new ExpenseRecord();
              exprec51.setLoadSec(LoadSec);
              exprec51.setItemCat("EX");
              exprec51.setMerchantNumber(merchantId);
              exprec51.setActiveDate(activeDate);
              exprec51.setDescription(mcFreightAssessment);
              exprec51.setRateCalculation(mcFreightAssessmentFeeCalc);
              exprec51.setItemCount(0);
              exprec51.setItemAmount(mcFreightAssessmentAmount);
              exprec51.setTotFeeAmount(mcFreightAssessmentFeeAmount);
              exps.add(exprec51);

               //MC Nominal Amount Authorization Fee
              String mcNominalAmountAuthFee = "MC Nominal Transaction Integrity Fee";
              String mcNominalAmountAuthFeeCalc ="mc_nominal_amount_auth_count * 0.045";
              ExpenseRecord exprec52 = new ExpenseRecord();
              exprec52.setLoadSec(LoadSec);
              exprec52.setItemCat("EX");
              exprec52.setMerchantNumber(merchantId);
              exprec52.setActiveDate(activeDate);
              exprec52.setDescription(mcNominalAmountAuthFee);
              exprec52.setRateCalculation(mcNominalAmountAuthFeeCalc);
              exprec52.setItemCount(mcNominalAmountAuthCount);
              exprec52.setItemAmount(mcNominalAmountAuthAmount);
              exprec52.setTotFeeAmount(mcNominalAmountAuthFeeAmount);
              exps.add(exprec52);


              storeExpenseRecord(exps); //Save the date to monthly_extract_exp table
          }
          catch(Exception e)
          {
            logEntry("vmc assessment object ", e.toString());
          }
           /*@lineinfo:generated-code*//*@lineinfo:2921^12*/

//  ************************************************************
//  #sql [Ctx] { select  (
//                        round(:mcNabuFeeAmount,2)                     +   -- MC NABU
//                        round(:mcBCBFeeAmount,2)                      +   -- MC Base Cross Border
//                        round(:mcECBFeeAmount,2)                      +   -- MC Enhanced Cross Border
//                        round(:mcGSFeeAmount,2)                       +   -- MC Global Support
//                        round(:mcLTDFeeAmount,2)                      +   -- MC large ticket dues
//                        round(:mcDEFeeAmount,2)                       +   -- MC Digital Enablement Fee
//                        round(:mcALFeeAmount,2)                       +   -- MC Acquirer License Fee
//                        round(:mcPreAuthFeeAmount,2)                  +   -- MC Pre Auth TIF
//                        round(:mcFinalAuthFeeAmount,2)                +   -- MC Final AuthTIF
//                        round(:mcCvc2FeeAmount,2)                     +   -- MC CVC2
//                        round(:mcDuesFeeAmount,2)                     +   -- MC dues
//                        round(:mcAccessFeeAmount,2)                   +   -- MC Fixed Access Fee
//                        round(:mcCardVerifyFeeAmount,2)               +   -- MC Zero Dollar Verification Fee
//                        round(:mcLocationFeeAmount,2)                 +   -- MC Monthly Location Fee
//                        round(:vsApfFeeAmount,2)                      +   -- Visa APF
//                        round(:vsApfdbFeeAmount,2)                    +   -- Visa APF db
//                        round(:vsApfIntlFeeAmount,2)                  +   -- Visa APF Intl
//                        round(:vsApfdbIntlFeeAmount,2)                +   -- Visa APF db Intl
//                        round(:vsBISAFeeAmount,2)                     +   -- Visa Base ISA
//                        round(:vsEISAFeeAmount,2)                     +   -- Visa Enhanced ISA
//                        round(:vsIafFeeAmount,2)                      +   -- Visa IAF
//                        round(:vsTifFeeAmount,2)                      +   -- Visa TIF
//                        round(:vsAuthMFeeAmount,2)                    +   -- Visa Misuse of Auth
//                        round(:vsZFLFeeAmount,2)                      +   -- Visa Zero Floor Limit
//                        :vsFanfFeeAmount                              +   -- Visa FANF
//                        round(:vsDuesFeeAmount,2)                     +   -- Visa dues
//                        round(:vsDTFeeAmount,2)                       +   -- Visa Data Transmission Fee
//                        round(:vsCVPDebitFee,2)                       +   -- Visa Credit Voucher Processing Debit Fee
//                        round(:vsCVPCreditFee,2)                      +   -- Visa Credit Voucher Processing Credit Fee
//                        round(:vsCVPDebitFeeIntl,2)                   +   -- Visa Credit Voucher Processing Debit Fee - Intl
//                        round(:vsCVPCreditFeeIntl,2)                  +   -- Visa Credit Voucher Processing Credit Fee - Intl
//                        round(:vsAccessFeeAmount,2)                   +   -- Visa Fixed Access Fee
//                        round(:vsCardVerifyFeeAmount,2)               +   -- Zero Dollar Verification Fee
//                        round(:dbNPFeeAmount,2)                       +   -- Debit Network Participation Fee
//                        round(:dsDUFeeAmount,2)                       +   -- Discover Data Usage Fee
//                        round(:dsARFeeAmount,2)                       +   -- Discover Auth Request Fee
//                        round(:dsIpfFeeAmount,2)                      +   -- Discover IPF
//                        round(:dsIsfFeeAmount,2)                      +   -- Discover ISF
//                        round(:dsDuesFeeAmount,2)                     +   -- Discover dues
//                        round(:optbECPFeeAmount,2)                    +   -- AM OptBlue Excessive Chargeback Program 
//                        round(:optbExcCBFeeAmount,2)                  +   -- AM OptBlue Excessive Chargeback Fee
//                        round(:optBAssessmentFeeAmount,2)             +   -- AM OptBlue Assessment Fee
//                        round(:optBInboundFeeAmount,2)                +   -- AM OptBlue Inbound Fee
//                        round(:optBNonSwipedFeeAmount,2)              +   -- AM OptBlue Non Swiped Transaction Fee
//                        round(:optBNonComplFeeAmount,2)               +   -- AM OptBlue Non Compliance Fee
//                        round(:optBDataQualityFeeAmount,2)            +   -- AM OptBlue Data Quality Fee
//                        round(:optBMerchAccessFeeAmount,2)                -- AM OptBlue Existing ESA Merchant Access Fee
//                      )                                                       as vmc_assessment_expense,
//                      (
//                        round((:counts[3] * 0.0195),2)                  +   -- Visa APF
//                        round((:counts[4] * 0.0155),2)                  +   -- Visa APF db
//                        round(:vsApfIntlFeeAmount,2)                    +   -- Visa APF Intl
//                        round(:vsApfdbIntlFeeAmount,2)                  +   -- Visa APF db Intl
//                        round(((:visa_isa_amount) * 0.0100),2)          +   -- Visa Base ISA
//                        round(((:visa_enh_isa_amount) * 0.0140),2)      +   -- Visa Enhanced ISA
//                        round((:amounts[0] * 0.0045),2)                 +   -- Visa IAF
//                        round((:counts[5] * 0.100),2)                   +   -- Visa TIF
//                        round(:vsAuthMFeeAmount,2)                      +   -- Visa Misuse of Auth
//                        round(:vsZFLFeeAmount,2)                        +   -- Visa Zero Floor Limit
//                        :amounts[7]                                     +   -- Visa FANF
//                        round(:vsDuesFeeAmount,2)                       +   -- Visa dues
//                        round(:vsDTFeeAmount,2)                         +   -- Visa Data Transmission Fee
//                        round(:vsCVPDebitFee,2)                         +   -- Visa Credit Voucher Processing Debit Fee
//                        round(:vsCVPCreditFee,2)                        +   -- Visa Credit Voucher Processing Credit Fee
//                        round(:vsCVPDebitFeeIntl,2)                     +   -- Visa Credit Voucher Processing Debit Fee - Intl
//                        round(:vsCVPCreditFeeIntl,2)                    +   -- Visa Credit Voucher Processing Credit Fee - Intl        
//                        round(:vsAccessFeeAmount,2)                     +   -- Visa Fixed Access Fee
//                        round(:vsCardVerifyFeeAmount,2)                     -- Zero Dollar Verification Fee
//                      )                                                       as visa_assessment,
//                      (
//                        round((:mcAuthCount * 0.0195),2)                   +   -- MC NABU
//                        round((:mc_cross_border_amount * 0.0060),2)       +   -- MC Base Cross Border
//                        round((:mc_enh_cross_border_amount * 0.0100),2)   +   -- MC Enhanced Cross Border
//                        round((:amounts[1] * 0.0085),2)                 +   -- MC Global Support
//                        round((:MbsBackend * :amounts[6] * 0.0001),2)   +   -- MC large ticket dues
//                        round(:mcDigitalEnablementFees,2)                 +   -- MC Digital Enablement Fee
//                        round((:MbsBackend * :mcSalesAmount * 0.00015),2) +   -- MC Acquirer License Fee
//                        round((:MbsBackend * :mcSalesAmount * 0.0013),2)  +   -- MC dues
//                        round(:mcAccessFeeAmount,2)                     +   -- MC Fixed Access Fee
//                        round(:mcCardVerifyFeeAmount,2)                 +   -- MC Zero Dollar Verification Fee
//                        round(:mcLocationFeeAmount,2)                       -- MC Monthly Location Fee
//                      )                                                       as mc_assessment,
//                      (
//                        round((:mcAuthCount * 0.0195),2)                   +   -- MC NABU
//                        round((:mc_cross_border_amount * 0.0060),2)       +   -- MC Base Cross Border
//                        round((:mc_enh_cross_border_amount * 0.0100),2)   +   -- MC Enhanced Cross Border
//                        round((:amounts[1] * 0.0085),2)                 +   -- MC Global Support
//                        round(:mcPreAuthFeeAmount,2)                    +   -- MC Pre Auth TIF
//                        round(:mcFinalAuthFeeAmount,2)                  +   -- MC Final AuthTIF
//                        round((:counts[7] * 0.0025),2)                  +   -- MC CVC2
//                        round(:mcAccessFeeAmount,2)                     +   -- MC Fixed Access Fee
//                        round(:mcCardVerifyFeeAmount,2)                 +   -- Zero Dollar Verification Fee
//                        round(:mcLocationFeeAmount,2)                   +   -- MC Monthly Location Fee
//                        round((:counts[3] * 0.0195),2)                  +   -- Visa APF
//                        round((:counts[4] * 0.0155),2)                  +   -- Visa APF db
//                        round(:vsApfIntlFeeAmount,2)                    +   -- Visa APF Intl
//                        round(:vsApfdbIntlFeeAmount,2)                  +   -- Visa APF db Intl          
//                        round(((:visa_isa_amount) * 0.0100),2)          +   -- Visa Base ISA
//                        round(((:visa_enh_isa_amount) * 0.0140),2)      +   -- Visa Enhanced ISA
//                        round((:amounts[0] * 0.0045),2)                 +   -- Visa IAF
//                        round((:counts[5] * 0.100),2)                   +   -- Visa TIF
//                        round(:vsAuthMFeeAmount,2)                      +   -- Visa Misuse of Auth
//                        round(:vsZFLFeeAmount,2)                        +   -- Visa Zero Floor Limit
//                        :amounts[7]                                     +   -- Visa FANF
//                        round(:vsDTFeeAmount,2)                         +   -- Visa Data Transmission Fee
//                        round(:vsCVPDebitFee,2)                         +   -- Visa Credit Voucher Processing Debit Fee
//                        round(:vsCVPCreditFee,2)                        +   -- Visa Credit Voucher Processing Credit Fee
//                        round(:vsCVPDebitFeeIntl,2)                     +   -- Visa Credit Voucher Processing Debit Fee - Intl
//                        round(:vsCVPCreditFeeIntl,2)                    +   -- Visa Credit Voucher Processing Credit Fee - Intl
//                        round(:vsAccessFeeAmount,2)                     +   -- Visa Fixed Access Fee
//                        round(:vsCardVerifyFeeAmount,2)                 +   -- Visa Zero Dollar Verification Fee
//                        round((:MbsBackend * :counts[10] * 4.17),2)     +   -- Debit Network Participation Fee
//                        round((:dsAuthCount * 0.0185),2)                  +   -- Discover Data Usage Fee
//                        round((:dsAuthCount * 0.0025),2)                  +   -- Discover Auth Request Fee
//                        round((:amounts[2] * 0.0050),2)                 +   -- Discover IPF
//                        round((:amounts[2] * 0.0080),2)                 +   -- Discover ISF
//                        round(:optbECPFeeAmount,2)                      +   -- AM OptBlue Excessive Chargeback Program
//                        round(:optbExcCBFeeAmount,2)                    +   -- AM OptBlue Excessive Chargeback Fee
//                        round(:optBAssessmentFeeAmount,2)               +   -- AM OptBlue Assessment Fee
//                        round(:optBInboundFeeAmount,2)                  +   -- AM OptBlue Inbound Fee
//                        round(:optBNonSwipedFeeAmount,2)                +   -- AM OptBlue Non Swiped Transaction Fee
//                        round(:optBNonComplFeeAmount,2)                 +   -- AM OptBlue Non Compliance Fee
//                        round(:optBDataQualityFeeAmount,2)              +   -- AM OptBlue Data Quality Fee
//                        round(:optBMerchAccessFeeAmount,2)                  -- AM OptBlue Existing ESA Merchant Access Fee
//                      )                                                       as vmc_fees,
//                      (
//                        round((:counts[3] * 0.0195),2)                  +   -- Visa APF
//                        round((:counts[4] * 0.0155),2)                  +   -- Visa APF db
//                        round(:vsApfIntlFeeAmount,2)                    +   -- Visa APF Intl
//                        round(:vsApfdbIntlFeeAmount,2)                  +   -- Visa APF db Intl
//                        round(((:visa_isa_amount) * 0.0100),2)          +   -- Visa Base ISA
//                        round(((:visa_enh_isa_amount) * 0.0140),2)      +   -- Visa Enhanced ISA
//                        round((:amounts[0] * 0.0045),2)                 +   -- Visa IAF
//                        round((:counts[5] * 0.100),2)                   +   -- Visa TIF
//                        round(:vsAuthMFeeAmount,2)                      +   -- Visa Misuse of Auth
//                        round(:vsZFLFeeAmount,2)                        +   -- Visa Zero Floor Limit
//                        :amounts[7]                                     +   -- Visa FANF
//                        round(:vsDTFeeAmount,2)                         +   -- Visa Data Transmission Fee
//                        round(:vsCVPDebitFee,2)                           +   -- Visa Credit Voucher Processing Debit Fee
//                        round(:vsCVPCreditFee,2)                          +   -- Visa Credit Voucher Processing Credit Fee
//                        round(:vsCVPDebitFeeIntl,2)                       +   -- Visa Credit Voucher Processing Debit Fee - Intl
//                        round(:vsCVPCreditFeeIntl,2)                      +   -- Visa Credit Voucher Processing Credit Fee - Intl
//                        round(:vsAccessFeeAmount,2)                     +   -- Visa Fixed Access Fee
//                        round(:vsCardVerifyFeeAmount,2)                     -- Visa Zero Dollar Verification Fee
//                      )                                                       as visa_fees,
//                      (
//                        round((:mcAuthCount * 0.0195),2)                   +   -- MC NABU
//                        round((:mc_cross_border_amount * 0.0060),2)       +   -- MC Base Cross Border
//                        round((:mc_enh_cross_border_amount * 0.0100),2)   +   -- MC Enhanced Cross Border
//                        round((:amounts[1] * 0.0085),2)                 +   -- MC Global Support
//                        round(:mcPreAuthFeeAmount,2)                    +   -- MC Pre Auth TIF
//                        round(:mcFinalAuthFeeAmount,2)                  +   -- MC Final AuthTIF
//                        round((:counts[7] * 0.0025),2)                  +   -- MC CVC2
//                        round(:mcAccessFeeAmount,2)                     +   -- MC Fixed Access Fee
//                        round(:mcCardVerifyFeeAmount,2)                 +   -- MC Zero Dollar Verification Fee
//                        round(:mcLocationFeeAmount,2)                       -- MC Monthly Location Fee
//                      )                                                       as mc_fees,
//                      (
//                        round((:dsAuthCount * 0.0195),2)                  +   -- Discover Data Usage Fee
//                        round((:dsAuthCount * 0.0025),2)                  +   -- Discover Auth Request Fee
//                        round((:amounts[2] * 0.0050),2)                 +   -- Discover IPF
//                        round((:amounts[2] * 0.0080),2)                 +   -- Discover ISF
//                        round((:MbsBackend * :dsSalesAmount * 0.0013),2)     -- Discover dues
//                      )                                                       as ds_assessment,
//                      (
//                        round((:dsAuthCount * 0.0195),2)                  +   -- Discover Data Usage Fee
//                        round((:dsAuthCount * 0.0025),2)                  +   -- Discover Auth Request Fee
//                        round((:amounts[2] * 0.0050),2)                 +   -- Discover IPF
//                        round((:amounts[2] * 0.0080),2)                     -- Discover ISF
//                      )                                                       as ds_fees,
//                      (
//                        round((:MbsBackend * :counts[10] * 4.17),2)         -- Debit Network Participation Fee
//                      )                                                       as db_fees
//              
//              from  dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_213 = counts[3];
 int __sJT_214 = counts[4];
 double __sJT_215 = amounts[0];
 int __sJT_216 = counts[5];
 double __sJT_217 = amounts[7];
 double __sJT_218 = mcGlobalAcquirerSupportAmount;
 double __sJT_219 = amounts[6];
 double __sJT_220 = mcGlobalAcquirerSupportAmount;
 int __sJT_221 = counts[7];
 int __sJT_222 = counts[3];
 int __sJT_223 = counts[4];
 double __sJT_224 = amounts[0];
 int __sJT_225 = counts[5];
 double __sJT_226 = amounts[7];
 int __sJT_227 = counts[10];
 double __sJT_228 = amounts[2];
 double __sJT_229 = amounts[2];
 int __sJT_230 = counts[3];
 int __sJT_231 = counts[4]; 
 double __sJT_232 = amounts[0];
 int __sJT_233 = counts[5];
 double __sJT_234 = amounts[7];
 double __sJT_235 = mcGlobalAcquirerSupportAmount;
 int __sJT_236 = counts[7];
 double __sJT_237 = amounts[2];
 double __sJT_238 = amounts[2];
 double __sJT_239 = amounts[2];
 double __sJT_240 = amounts[2];
 int __sJT_241 = counts[10];
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
	//@formatter:off				  
	  String theSqlTS = "select  ( "
       +"       round( :1  ,2)                     +   -- MC NABU\n   "
       +"       round( :2  ,2)                     +   -- MasterCard SafetyNet and Fraud Dashboard\n    "
       +"       round( :3  ,2)                     +   -- MC Base Cross Border\n      "
       +"       round( :4  ,2)                     +   -- MC Enhanced Cross Border\n     "
       +"       round( :5  ,2)                     +   -- MC Global Support\n     "
       +"       round( :6  ,2)                     +   -- MC large ticket dues\n     "
       +"       round( :7  ,2)                     +   -- MC Digital Enablement Fee\n    "
       +"       round( :8  ,2)                     +   -- MC Acquirer License Fee\n    "
       +"       round( :9  ,2)                     +   -- MC Pre Auth TIF\n     "
       +"       round( :10  ,2)                    +   -- MC Final AuthTIF\n     "
       +"       round( :11  ,2)                    +   -- MC CVC2\n     "
       +"       round( :12  ,2)                    +   -- MC dues\n       "
       +"       round( :13  ,2)                    +   -- MC Fixed Access Fee\n       "
       +"       round( :14  ,2)                    +   -- MC Zero Dollar Verification Fee\n      "
       +"       round( :15  ,2)                    +   -- MC Monthly Location Fee\n      "
       +"       round( :16  ,2)                    +   -- Visa APF\n     "
       +"       round( :17  ,2)                    +   -- Visa APF db\n     "
       +"       round( :18  ,2)                    +   -- Visa APF Intl\n     "
       +"       round( :19  ,2)                    +   -- Visa APF db Intl\n     "
       +"       round( :20  ,2)                    +   -- Visa Base ISA\n      "
       +"       round( :21  ,2)                    +   -- Visa Enhanced ISA\n     "
       +"       round( :22  ,2)                    +   -- Visa IAF\n       "
       +"       round( :23  ,2)                    +   -- Visa TIF\n     "
       +"       round( :24  ,2)                    +   -- Visa Misuse of Auth\n      "
       +"       round( :25  ,2)                    +   -- Visa Zero Floor Limit\n     "
       +"        :26                               +   -- Visa FANF\n     "
       +"       round( :27  ,2)                    +   -- Visa dues\n     "
       +"       round( :28  ,2)                    +   -- Visa Data Transmission Fee\n      "
       +"       round( :29  ,2)                    +   -- Visa Credit Voucher Processing Debit Fee\n     "
       +"       round( :30  ,2)                    +   -- Visa Credit Voucher Processing Credit Fee\n     "
       +"       round( :31  ,2)                    +   -- Visa Credit Voucher Processing Debit Fee - Intl\n     "
       +"       round( :32  ,2)                    +   -- Visa Credit Voucher Processing Credit Fee - Intl\n     "       
       +"       round( :33  ,2)                    +   -- Visa Fixed Access Fee\n        "
       +"       round( :34  ,2)                    +   -- Zero Dollar Verification Fee\n       "
       +"       round( :35  ,2)                    +   -- Debit Network Participation Fee\n       "
       +"       round( :36  ,2)                    +   -- Discover Data Usage Fee\n          "
       +"       round( :37  ,2)                    +   -- Discover Auth Request Fee\n       "
       +"       round( :38  ,2)                    +   -- Discover IPF\n               "
       +"       round( :39  ,2)                    +   -- Discover ISF\n             "
       +"       round( :40  ,2)                    +   -- Discover dues\n            "
       +"       round( :41  ,2)                    +   -- AM OptBlue Excessive Chargeback Program\n      "
       +"       round( :42  ,2)                    +   -- AM OptBlue Excessive Chargeback Fee\n         "
       +"       round( :43  ,2)                    +   -- AM OptBlue Assessment Fee\n             "
       +"       round( :44  ,2)                    +   -- AM OptBlue Inbound Fee\n        "
       +"       round( :45  ,2)                    +   -- AM OptBlue Non Swiped Transaction Fee\n      "
       +"       round( :46  ,2)                    +   -- AM OptBlue Non Compliance Fee\n      "
       +"       round( :47  ,2)                    +   -- AM OptBlue Data Quality Fee\n        "
       +"       round( :48  ,2)                    +   -- AM OptBlue Existing ESA Merchant Access Fee\n       "
       +"       round( :167  ,2)                   +   -- MC Global Wholesale Travel Fee\n       "
       +"       round( :171  ,2)                   +   -- MC Excessive Auth Fee\n       "
       +"       round( :175  ,2)                   +    -- MC Freight Assessment Fee\n       "
       +"       round( :179  ,2)                        -- MC Nominal Amount Auth Fee\n       "
     +")                                                       as vmc_assessment_expense,        "
     +"(                                 "
       +"       round( :49 * 0.0195 ,2)                 +   -- Visa APF\n             "
       +"       round( :50 * 0.0155 ,2)                 +   -- Visa APF db\n         "
       +"       round( :51  ,2)                         +   -- Visa APF Intl\n             "
       +"       round( :52  ,2)                         +   -- Visa APF db Intl\n         "
       +"       round((( :53  ) * 0.0100),2)            +   -- Visa Base ISA\n        "
       +"       round((( :54  ) * 0.0140),2)            +   -- Visa Enhanced ISA\n         "
       +"       round(( :55   * 0.0045),2)              +   -- Visa IAF\n                 "
       +"       round(( :56   * 0.100),2)               +   -- Visa TIF\n              "
       +"       round( :57  ,2)                         +   -- Visa Misuse of Auth\n            "
       +"       round( :58  ,2)                         +   -- Visa Zero Floor Limit\n         "
       +"        :59                                    +   -- Visa FANF\n                    "
       +"       round( :60  ,2)                         +   -- Visa dues\n                   "
       +"       round( :61  ,2)                         +   -- Visa Data Transmission Fee\n             "
       +"       round( :62  ,2)                         +   -- Visa Credit Voucher Processing Debit Fee\n        "
       +"       round( :63  ,2)                         +   -- Visa Credit Voucher Processing Credit Fe\n         "
       +"       round( :64  ,2)                         +   -- Visa Credit Voucher Processing Debit Fee - Intl\n        "
       +"       round( :65  ,2)                         +   -- Visa Credit Voucher Processing Credit Fee -Intl\n         "       
       +"       round( :66  ,2)                         +   -- Visa Fixed Access Fee\n                      "
       +"       round( :67  ,2)                             -- Zero Dollar Verification Fee\n                   "
     +")                                                       as visa_assessment,              "
     +"(                         "
       +"       round( :68 * 0.0195, 2)                 +   -- MC NABU\n          "
       +"       round(( :69   * 0.01),2)                +   -- MasterCard SafetyNet and Fraud Dashboard\n                    "
       +"       round(( :70   * 0.0060),2)              +   -- MC Base Cross Border\n             "
       +"       round(( :71   * 0.0100),2)              +   -- MC Enhanced Cross Border\n          "
       +"       round(( :72   * 0.0085),2)              +   -- MC Global Support\n              "
       +"       round(( :73   *  :74   * 0.0001),2)     +   -- MC large ticket dues\n             "
       +"       round( :75  ,2)                         +   -- MC Digital Enablement Fee\n            "
       +"       round(( :76   *  :77   * 0.00015),2)    +   -- MC Acquirer License Fee\n            "
       +"       round(( :78   *  :79   * 0.0013),2)     +   -- MC dues\n                  "
       +"       round( :80  ,2)                         +   -- MC Fixed Access Fee\n             "
       +"       round( :81  ,2)                         +   -- MC Zero Dollar Verification Fee\n          "
       +"       round( :82  ,2)                         +  -- MC Monthly Location Fee\n            "
       +"       round( :168  ,2)                        +   -- MC Global Wholesale Travel Fee\n       "
       +"       round( :172  ,2)                        +   -- MC Excessive Auth Fee\n       "
       +"       round( :176  ,2)                        +    -- MC Freight Assessment Fee\n       "
       +"       round( :180  ,2)                             -- MC Nominal Amount Auth Fee\n       "
     +")                                                       as mc_assessment,               "
     +"(                                "
       +"       round(( :83 * 0.0195), 2)               +   -- MC NABU\n             "
       +"       round(( :84   * 0.01),2)                +   -- MasterCard SafetyNet and Fraud Dashboard\n                   "
       +"       round(( :85   * 0.0060),2)              +   -- MC Base Cross Border\n              "
       +"       round(( :86   * 0.0100),2)              +   -- MC Enhanced Cross Border\n            "
       +"       round(( :87   * 0.0085),2)              +   -- MC Global Support\n                "
       +"       round( :88  ,2)                         +   -- MC Pre Auth TIF\n             "
       +"       round( :89  ,2)                         +   -- MC Final AuthTIF\n            "
       +"       round(( :90   * 0.0025),2)              +   -- MC CVC2\n             "
       +"       round( :91  ,2)                         +   -- MC Fixed Access Fee\n       "
       +"       round( :92  ,2)                         +   -- Zero Dollar Verification Fee\n    "
       +"       round( :93  ,2)                         +   -- MC Monthly Location Fee\n       "
       +"       round( :94 * 0.0195 ,2)                 +   -- Visa APF\n       "
       +"       round( :95 * 0.0155 ,2)                 +   -- Visa APF db\n      "
       +"       round( :96  ,2)                         +   -- Visa APF Intl\n       "
       +"       round( :97  ,2)                         +   -- Visa APF db Intl\n      "  
       +"       round((( :98  ) * 0.0100),2)            +   -- Visa Base ISA\n            "
       +"       round((( :99  ) * 0.0140),2)            +   -- Visa Enhanced ISA\n        "
       +"       round(( :100   * 0.0045),2)              +   -- Visa IAF\n                "
       +"       round(( :101   * 0.100),2)               +   -- Visa TIF\n              "
       +"       round( :102  ,2)                         +   -- Visa Misuse of Auth\n           "
       +"       round( :103  ,2)                         +   -- Visa Zero Floor Limit\n         "
       +"        :104                                   +   -- Visa FANF\n              "
       +"       round( :105 ,2)                         +   -- Visa Data Transmission Fee\n           "
       +"       round( :106  ,2)                        +   -- Visa Credit Voucher Processing Debit Fee\n     "
       +"       round( :107  ,2)                        +   -- Visa Credit Voucher Processing Credit Fee\n     "
       +"       round( :108  ,2)                        +   -- Visa Credit Voucher Processing Debit Fee - Intl\n     "
       +"       round( :109  ,2)                        +   -- Visa Credit Voucher Processing Credit Fee - Intl\n     "
       +"       round( :110  ,2)                        +   -- Visa Fixed Access Fee\n             "
       +"       round( :111  ,2)                        +   -- Visa Zero Dollar Verification Fee\n         "
       +"       round(( :112   *  :113   * 4.50),2)     +   -- Debit Network Participation Fee\n         "
       +"       round(( :114   * 0.0185),2)             +   -- Discover Data Usage Fee\n          "
       +"       round(( :115   * 0.0025),2)             +   -- Discover Auth Request Fee\n         "
       +"       round(( :116   * 0.0050),2)             +   -- Discover IPF\n             "
       +"       round(( :117   * 0.0080),2)             +   -- Discover ISF\n            "
       +"       round( :118  ,2)                        +   -- AM OptBlue Excessive Chargeback Program\n        "
       +"       round( :119  ,2)                        +   -- AM OptBlue Excessive Chargeback Fee\n        "
       +"       round( :120  ,2)                        +   -- AM OptBlue Assessment Fee\n          "
       +"       round( :121  ,2)                        +   -- AM OptBlue Inbound Fee\n            "
       +"       round( :122  ,2)                        +   -- AM OptBlue Non Swiped Transaction Fee\n         "
       +"       round( :123  ,2)                        +   -- AM OptBlue Non Compliance Fee\n           "
       +"       round( :124  ,2)                        +   -- AM OptBlue Data Quality Fee\n            "
       +"       round( :125  ,2)                        +  -- AM OptBlue Existing ESA Merchant Access Fee\n         "
       +"       round( :169  ,2)                        +   -- MC Global Wholesale Travel Fee\n       "
       +"       round( :173  ,2)                        +   -- MC Excessive Auth Fee\n       "
       +"       round( :177  ,2)                        +   -- MC Freight Assessment Fee\n       "
       +"       round( :181  ,2)                            -- MC Nominal Amount Auth Fee\n       "
     +")                                                       as vmc_fees,               "
     +"(                    "
       +"       round( :126 * 0.0195 ,2)                +   -- Visa APF\n              "
       +"       round( :127 * 0.0155 ,2)                +   -- Visa APF db\n            "
       +"       round( :128   ,2)                       +   -- Visa APF Intl\n              "
       +"       round( :129   ,2)                       +   -- Visa APF db Intl\n            "
       +"       round((( :130  ) * 0.0100),2)           +   -- Visa Base ISA\n         "
       +"       round((( :131  ) * 0.0140),2)           +   -- Visa Enhanced ISA\n           "
       +"       round(( :132   * 0.0045),2)             +   -- Visa IAF\n               "
       +"       round(( :133   * 0.100),2)              +   -- Visa TIF\n              "
       +"       round( :134  ,2)                        +   -- Visa Misuse of Auth\n            "
       +"       round( :135  ,2)                        +   -- Visa Zero Floor Limit\n           "
       +"        :136                                   +   -- Visa FANF\n           "
       +"       round( :137  ,2)                        +   -- Visa Data Transmission Fee\n         "
       +"       round( :138  ,2)                        +   -- Visa Credit Voucher Processing Debit Fee\n             "
       +"       round( :139  ,2)                        +   -- Visa Credit Voucher Processing Credit Fee\n         "
       +"       round( :140  ,2)                        +   -- Visa Credit Voucher Processing Debit Fee - Intl\n             "
       +"       round( :141  ,2)                        +   -- Visa Credit Voucher Processing Credit Fee - Intl\n         "
       +"       round( :142  ,2)                        +   -- Visa Fixed Access Fee\n         "
       +"       round( :143  ,2)                            -- Visa Zero Dollar Verification Fee\n       "
     +")                                                       as visa_fees,            "
     +"(           "
       +"       round(( :144   * 0.0195), 2)            +   -- MC NABU\n           "
       +"       round(( :145   * 0.01),2)               +   -- MasterCard SafetyNet and Fraud Dashboard\n             "
       +"       round(( :146   * 0.0060),2)             +   -- MC Base Cross Border\n           "
       +"       round(( :147   * 0.0100),2)             +   -- MC Enhanced Cross Border\n        "
       +"       round(( :148   * 0.0085),2)             +   -- MC Global Support\n            "
       +"       round( :149  ,2)                        +   -- MC Pre Auth TIF\n             "
       +"       round( :150  ,2)                        +   -- MC Final AuthTIF\n            "
       +"       round(( :151  * 0.0025),2)              +   -- MC CVC2\n                "
       +"       round( :152  ,2)                        +   -- MC Fixed Access Fee\n              "
       +"       round( :153  ,2)                        +   -- MC Zero Dollar Verification Fee\n        "
       +"       round( :154  ,2)                        +   -- MC Monthly Location Fee\n            "
       +"       round( :170  ,2)                        +   -- MC Global Wholesale Travel Fee\n       "
       +"       round( :174  ,2)                        +   -- MC Excessive Auth Fee\n       "
       +"       round( :178  ,2)                        +   -- MC Freight Assessment Fee\n       "
       +"       round( :182  ,2)                            -- MC Nominal Amount Auth Fee\n       "
     +")                                                       as mc_fees,             "
     +"(                     "
       +"       round(( :155   * 0.0195),2)             +   -- Discover Data Usage Fee\n      "
       +"       round(( :156   * 0.0025),2)             +   -- Discover Auth Request Fee\n     "
       +"       round(( :157   * 0.0050),2)             +   -- Discover IPF\n         "
       +"       round(( :158   * 0.0080),2)             +   -- Discover ISF\n         "
       +"       round(( :159   *  :160   * 0.0013),2)       -- Discover dues\n        "
     +")                                                       as ds_assessment,       "
     +"(           "
       +"       round(( :161   * 0.0195),2)             +   -- Discover Data Usage Fee\n     "
       +"       round(( :162   * 0.0025),2)             +   -- Discover Auth Request Fee\n     "
       +"       round(( :163   * 0.0050),2)             +   -- Discover IPF\n      "
       +"       round(( :164   * 0.0080),2)                 -- Discover ISF\n     "
     +")                                                       as ds_fees,     "
     +"(       "
       +"       round(( :165   *  :166   * 4.50),2)         -- Debit Network Participation Fee\n    "
     +")                                                       as db_fees    "
+"from  dual";
//@formatter:on
	  
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"48com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDoubleAtName("1", mcNabuFeeAmount);
   __sJT_st.setDoubleAtName("2", mcSafetyNetFeeAmount);
   __sJT_st.setDoubleAtName("3", mcBCBFeeAmount);
   __sJT_st.setDoubleAtName("4", mcECBFeeAmount);
   __sJT_st.setDoubleAtName("5", mcGSFeeAmount);
   __sJT_st.setDoubleAtName("6", mcLTDFeeAmount);
   __sJT_st.setDoubleAtName("7", mcDEFeeAmount);
   __sJT_st.setDoubleAtName("8", mcALFeeAmount);
   __sJT_st.setDoubleAtName("9", mcPreAuthFeeAmount);
   __sJT_st.setDoubleAtName("10", mcFinalAuthFeeAmount);
   __sJT_st.setDoubleAtName("11", mcCvc2FeeAmount);
   __sJT_st.setDoubleAtName("12", mcDuesFeeAmount);
   __sJT_st.setDoubleAtName("13", mcAccessFeeAmount);
   __sJT_st.setDoubleAtName("14", mcCardVerifyFeeAmount);
   __sJT_st.setDoubleAtName("15", mcLocationFeeAmount);
   __sJT_st.setDoubleAtName("16", vsApfFeeAmount);
   __sJT_st.setDoubleAtName("17", vsApfdbFeeAmount);
   __sJT_st.setDoubleAtName("18", vsApfIntlFeeAmount);
   __sJT_st.setDoubleAtName("19", vsApfdbIntlFeeAmount);
   __sJT_st.setDoubleAtName("20", vsBISAFeeAmount);
   __sJT_st.setDoubleAtName("21", vsEISAFeeAmount);
   __sJT_st.setDoubleAtName("22", vsIafFeeAmount);
   __sJT_st.setDoubleAtName("23", vsTifFeeAmount);
   __sJT_st.setDoubleAtName("24", vsAuthMFeeAmount);
   __sJT_st.setDoubleAtName("25", vsZFLFeeAmount);
   __sJT_st.setDoubleAtName("26", vsFanfFeeAmount);
   __sJT_st.setDoubleAtName("27", vsDuesFeeAmount);
   __sJT_st.setDoubleAtName("28", vsDTFeeAmount);
   __sJT_st.setDoubleAtName("29", vsCVPDebitFee);
   __sJT_st.setDoubleAtName("30", vsCVPCreditFee);
   __sJT_st.setDoubleAtName("31", vsCVPDebitFeeIntl);
   __sJT_st.setDoubleAtName("32", vsCVPCreditFeeIntl);
   __sJT_st.setDoubleAtName("33", vsAccessFeeAmount);
   __sJT_st.setDoubleAtName("34", vsCardVerifyFeeAmount);
   __sJT_st.setDoubleAtName("35", dbNPFeeAmount);
   __sJT_st.setDoubleAtName("36", dsDUFeeAmount);
   __sJT_st.setDoubleAtName("37", dsARFeeAmount);
   __sJT_st.setDoubleAtName("38", dsIpfFeeAmount);
   __sJT_st.setDoubleAtName("39", dsIsfFeeAmount);
   __sJT_st.setDoubleAtName("40", dsDuesFeeAmount);
   __sJT_st.setDoubleAtName("41", optbECPFeeAmount);
   __sJT_st.setDoubleAtName("42", optbExcCBFeeAmount);
   __sJT_st.setDoubleAtName("43", optBAssessmentFeeAmount);
   __sJT_st.setDoubleAtName("44", optBInboundFeeAmount);
   __sJT_st.setDoubleAtName("45", optBNonSwipedFeeAmount);
   __sJT_st.setDoubleAtName("46", optBNonComplFeeAmount);
   __sJT_st.setDoubleAtName("47", optBDataQualityFeeAmount);
   __sJT_st.setDoubleAtName("48", optBMerchAccessFeeAmount);
   __sJT_st.setIntAtName("49", __sJT_213);
   __sJT_st.setIntAtName("50", __sJT_214);
   __sJT_st.setDoubleAtName("51", vsApfIntlFeeAmount);
   __sJT_st.setDoubleAtName("52", vsApfdbIntlFeeAmount);
   __sJT_st.setDoubleAtName("53", visa_isa_amount);
   __sJT_st.setDoubleAtName("54", visa_enh_isa_amount);
   __sJT_st.setDoubleAtName("55", __sJT_215);
   __sJT_st.setIntAtName("56", __sJT_216);
   __sJT_st.setDoubleAtName("57", vsAuthMFeeAmount);
   __sJT_st.setDoubleAtName("58", vsZFLFeeAmount);
   __sJT_st.setDoubleAtName("59", __sJT_217);
   __sJT_st.setDoubleAtName("60", vsDuesFeeAmount);
   __sJT_st.setDoubleAtName("61", vsDTFeeAmount);
   __sJT_st.setDoubleAtName("62", vsCVPDebitFee);
   __sJT_st.setDoubleAtName("63", vsCVPCreditFee);
   __sJT_st.setDoubleAtName("64", vsCVPDebitFeeIntl);
   __sJT_st.setDoubleAtName("65", vsCVPCreditFeeIntl);
   __sJT_st.setDoubleAtName("66", vsAccessFeeAmount);
   __sJT_st.setDoubleAtName("67", vsCardVerifyFeeAmount);
   __sJT_st.setIntAtName("68", mcNabuCount);
   __sJT_st.setDoubleAtName("69", mcAuthCount);
   __sJT_st.setDoubleAtName("70", mc_cross_border_amount);
   __sJT_st.setDoubleAtName("71", mc_enh_cross_border_amount);
   __sJT_st.setDoubleAtName("72", __sJT_218);
   __sJT_st.setIntAtName("73", MbsBackend);
   __sJT_st.setDoubleAtName("74", __sJT_219);
   __sJT_st.setDoubleAtName("75", mcDigitalEnablementFees);
   __sJT_st.setIntAtName("76", MbsBackend);
   __sJT_st.setDoubleAtName("77", mcSalesAmount);
   __sJT_st.setIntAtName("78", MbsBackend);
   __sJT_st.setDoubleAtName("79", mcSalesAmount);
   __sJT_st.setDoubleAtName("80", mcAccessFeeAmount);
   __sJT_st.setDoubleAtName("81", mcCardVerifyFeeAmount);
   __sJT_st.setDoubleAtName("82", mcLocationFeeAmount);
   __sJT_st.setIntAtName("83", mcNabuCount);
   __sJT_st.setDoubleAtName("84", mcAuthCount);
   __sJT_st.setDoubleAtName("85", mc_cross_border_amount);
   __sJT_st.setDoubleAtName("86", mc_enh_cross_border_amount);
   __sJT_st.setDoubleAtName("87", __sJT_220);
   __sJT_st.setDoubleAtName("88", mcPreAuthFeeAmount);
   __sJT_st.setDoubleAtName("89", mcFinalAuthFeeAmount);
   __sJT_st.setIntAtName("90", __sJT_221);
   __sJT_st.setDoubleAtName("91", mcAccessFeeAmount);
   __sJT_st.setDoubleAtName("92", mcCardVerifyFeeAmount);
   __sJT_st.setDoubleAtName("93", mcLocationFeeAmount);
   __sJT_st.setIntAtName("94", __sJT_222);
   __sJT_st.setIntAtName("95", __sJT_223);
   __sJT_st.setDoubleAtName("96", vsApfIntlFeeAmount);
   __sJT_st.setDoubleAtName("97", vsApfdbIntlFeeAmount);
   __sJT_st.setDoubleAtName("98", visa_isa_amount);
   __sJT_st.setDoubleAtName("99", visa_enh_isa_amount);
   __sJT_st.setDoubleAtName("100", __sJT_224);
   __sJT_st.setIntAtName("101", __sJT_225);
   __sJT_st.setDoubleAtName("102", vsAuthMFeeAmount);
   __sJT_st.setDoubleAtName("103", vsZFLFeeAmount);
   __sJT_st.setDoubleAtName("104", __sJT_226);
   __sJT_st.setDoubleAtName("105", vsDTFeeAmount);
   __sJT_st.setDoubleAtName("106", vsCVPDebitFee);
   __sJT_st.setDoubleAtName("107", vsCVPCreditFee);
   __sJT_st.setDoubleAtName("108", vsCVPDebitFeeIntl);
   __sJT_st.setDoubleAtName("109", vsCVPCreditFeeIntl);
   __sJT_st.setDoubleAtName("110", vsAccessFeeAmount);
   __sJT_st.setDoubleAtName("111", vsCardVerifyFeeAmount);
   __sJT_st.setIntAtName("112", MbsBackend);
   __sJT_st.setIntAtName("113", __sJT_227);
   __sJT_st.setIntAtName("114", dsAuthCount);
   __sJT_st.setIntAtName("115", dsAuthCount);
   __sJT_st.setDoubleAtName("116", __sJT_228);
   __sJT_st.setDoubleAtName("117", __sJT_229);
   __sJT_st.setDoubleAtName("118", optbECPFeeAmount);
   __sJT_st.setDoubleAtName("119", optbExcCBFeeAmount);
   __sJT_st.setDoubleAtName("120", optBAssessmentFeeAmount);
   __sJT_st.setDoubleAtName("121", optBInboundFeeAmount);
   __sJT_st.setDoubleAtName("122", optBNonSwipedFeeAmount);
   __sJT_st.setDoubleAtName("123", optBNonComplFeeAmount);
   __sJT_st.setDoubleAtName("124", optBDataQualityFeeAmount);
   __sJT_st.setDoubleAtName("125", optBMerchAccessFeeAmount);
   __sJT_st.setIntAtName("126", __sJT_230);
   __sJT_st.setIntAtName("127", __sJT_231);
   __sJT_st.setDoubleAtName("128", vsApfIntlFeeAmount);
   __sJT_st.setDoubleAtName("129", vsApfdbIntlFeeAmount);
   __sJT_st.setDoubleAtName("130", visa_isa_amount);
   __sJT_st.setDoubleAtName("131", visa_enh_isa_amount);
   __sJT_st.setDoubleAtName("132", __sJT_232);
   __sJT_st.setIntAtName("133", __sJT_233);
   __sJT_st.setDoubleAtName("134", vsAuthMFeeAmount);
   __sJT_st.setDoubleAtName("135", vsZFLFeeAmount);
   __sJT_st.setDoubleAtName("136", __sJT_234);
   __sJT_st.setDoubleAtName("137", vsDTFeeAmount);
   __sJT_st.setDoubleAtName("138", vsCVPDebitFee);
   __sJT_st.setDoubleAtName("139", vsCVPCreditFee);
   __sJT_st.setDoubleAtName("140", vsCVPDebitFeeIntl);
   __sJT_st.setDoubleAtName("141", vsCVPCreditFeeIntl);
   __sJT_st.setDoubleAtName("142", vsAccessFeeAmount);
   __sJT_st.setDoubleAtName("143", vsCardVerifyFeeAmount);
   __sJT_st.setIntAtName("144", mcNabuCount);
   __sJT_st.setDoubleAtName("145", mcAuthCount);
   __sJT_st.setDoubleAtName("146", mc_cross_border_amount);
   __sJT_st.setDoubleAtName("147", mc_enh_cross_border_amount);
   __sJT_st.setDoubleAtName("148", __sJT_235);
   __sJT_st.setDoubleAtName("149", mcPreAuthFeeAmount);
   __sJT_st.setDoubleAtName("150", mcFinalAuthFeeAmount);
   __sJT_st.setIntAtName("151", __sJT_236);
   __sJT_st.setDoubleAtName("152", mcAccessFeeAmount);
   __sJT_st.setDoubleAtName("153", mcCardVerifyFeeAmount);
   __sJT_st.setDoubleAtName("154", mcLocationFeeAmount);
   __sJT_st.setIntAtName("155", dsAuthCount);
   __sJT_st.setIntAtName("156", dsAuthCount);
   __sJT_st.setDoubleAtName("157", __sJT_237);
   __sJT_st.setDoubleAtName("158", __sJT_238);
   __sJT_st.setIntAtName("159", MbsBackend);
   __sJT_st.setDoubleAtName("160", dsSalesAmount);
   __sJT_st.setIntAtName("161", dsAuthCount);
   __sJT_st.setIntAtName("162", dsAuthCount);
   __sJT_st.setDoubleAtName("163", __sJT_239);
   __sJT_st.setDoubleAtName("164", __sJT_240);
   __sJT_st.setIntAtName("165", MbsBackend);
   __sJT_st.setIntAtName("166", __sJT_241);
   __sJT_st.setDoubleAtName("167", mcGlobalWholesaleTravelFeeAmount);
   __sJT_st.setDoubleAtName("168", mcGlobalWholesaleTravelFeeAmount);
   __sJT_st.setDoubleAtName("169", mcGlobalWholesaleTravelFeeAmount);
   __sJT_st.setDoubleAtName("170", mcGlobalWholesaleTravelFeeAmount);
   __sJT_st.setDoubleAtName("171", mcExcessiveAuthFeeAmount);
   __sJT_st.setDoubleAtName("172", mcExcessiveAuthFeeAmount);
   __sJT_st.setDoubleAtName("173", mcExcessiveAuthFeeAmount);
   __sJT_st.setDoubleAtName("174", mcExcessiveAuthFeeAmount);
   __sJT_st.setDoubleAtName("175", mcFreightAssessmentFeeAmount);
   __sJT_st.setDoubleAtName("176", mcFreightAssessmentFeeAmount);
   __sJT_st.setDoubleAtName("177", mcFreightAssessmentFeeAmount);
   __sJT_st.setDoubleAtName("178", mcFreightAssessmentFeeAmount);
   __sJT_st.setDoubleAtName("179", mcNominalAmountAuthFeeAmount);
   __sJT_st.setDoubleAtName("180", mcNominalAmountAuthFeeAmount);
   __sJT_st.setDoubleAtName("181", mcNominalAmountAuthFeeAmount);
   __sJT_st.setDoubleAtName("182", mcNominalAmountAuthFeeAmount);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 9) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(9,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amounts[0] = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[1] = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[2] = __sJT_rs.getDouble(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[3] = __sJT_rs.getDouble(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[4] = __sJT_rs.getDouble(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[5] = __sJT_rs.getDouble(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[6] = __sJT_rs.getDouble(7); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[7] = __sJT_rs.getDouble(8); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[8] = __sJT_rs.getDouble(9); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   
   if ( log.isDebugEnabled() ) {
	   log.debug(String.format("[run()] - getting fees merchantId=%s, vmc_assessment_expense=%s visa_assessment=%s mc_assessment=%s vmc_fee=%s "
	     		+ "visa_fees=%s mc_fees=%s disc_assessment=%s disc_fees=%s debit_fees=%s",
	     		merchantId,amounts[0], amounts[1], amounts[2], amounts[3], amounts[4], amounts[5], amounts[6], amounts[7], amounts[8]));
   }
   
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3085^11*/
          fields.setData("vmc_assessment_expense" , amounts[0]);
          fields.setData("visa_assessment"        , amounts[1]);
          fields.setData("mc_assessment"          , amounts[2]);
          fields.setData("vmc_fees"               , amounts[3]);
          fields.setData("visa_fees"              , amounts[4]);
          fields.setData("mc_fees"                , amounts[5]);
          fields.setData("disc_assessment"        , amounts[6]);
          fields.setData("disc_fees"              , amounts[7]);
          fields.setData("debit_fees"             , amounts[8]);
          
          // amex interchange expense
          /*@lineinfo:generated-code*//*@lineinfo:3097^11*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(sm.sales_count),0)          as sales_count,
//                      nvl(sum(sm.sales_amount),0)         as sales_amount,
//                      nvl(sum(sm.credits_count),0)        as credits_count,
//                      nvl(sum(sm.credits_amount),0)       as credits_amount,
//                      nvl(sum(sm.sales_count 
//                              + sm.credits_count),0)      as vol_count,
//                      nvl(sum(sm.sales_amount 
//                              - sm.credits_amount),0)     as vol_amount,
//                      nvl(sum(sm.expense_actual),0)       as ic_exp
//              
//              from    mbs_daily_summary   sm
//              where   sm.me_load_file_id = load_filename_to_load_file_id(:loadFilename)
//                      and sm.merchant_number = :merchantId
//                      and sm.item_type = 111
//                      and sm.item_subclass = 'AM'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(sm.sales_count),0)          as sales_count,\n                    nvl(sum(sm.sales_amount),0)         as sales_amount,\n                    nvl(sum(sm.credits_count),0)        as credits_count,\n                    nvl(sum(sm.credits_amount),0)       as credits_amount,\n                    nvl(sum(sm.sales_count \n                            + sm.credits_count),0)      as vol_count,\n                    nvl(sum(sm.sales_amount \n                            - sm.credits_amount),0)     as vol_amount,\n                    nvl(sum(sm.expense_actual),0)       as ic_exp\n             \n            from    mbs_daily_summary   sm\n            where   sm.me_load_file_id = load_filename_to_load_file_id( :1  )\n                    and sm.merchant_number =  :2  \n                    and sm.item_type = 111\n                    and sm.item_subclass = 'AM'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"49com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setLong(2,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 7) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(7,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[0] = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[1] = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[1] = __sJT_rs.getDouble(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[2] = __sJT_rs.getInt(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[2] = __sJT_rs.getDouble(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[3] = __sJT_rs.getDouble(7); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3117^11*/
          fields.setData("amex_ic_sales_count"    , counts[0] );
          fields.setData("amex_ic_sales_amount"   , amounts[0]);
          fields.setData("amex_ic_credits_count"  , counts[1] );
          fields.setData("amex_ic_credits_amount" , amounts[1]);
          fields.setData("amex_ic_vol_count"      , counts[2] );
          fields.setData("amex_ic_vol_amount"     , amounts[2]);
          fields.setData("amex_interchange"       , amounts[3]);

          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:3128^13*/

//  ************************************************************
//  #sql [Ctx] { select sum(case
//                            when st.st_statement_desc like 'AM%TIER%' then 1
//                            else 0
//                           end
//                           * st.st_fee_amount)
//                
//                from   monthly_extract_st st
//                where  st.hh_load_sec = :LoadSec
//                       and st.item_category in ('IC','CG-MIS')
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sum(case\n                          when st.st_statement_desc like 'AM%TIER%' then 1\n                          else 0\n                         end\n                         * st.st_fee_amount)\n               \n              from   monthly_extract_st st\n              where  st.hh_load_sec =  :1  \n                     and st.item_category in ('IC','CG-MIS')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"50com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amounts[0] = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3139^13*/
          }
          catch( Exception ee )
          {
            amounts[0] = 0.0;
          }
          fields.setData("tot_inc_ic_amex", amounts[0]);

          // discover interchange expense
          /*@lineinfo:generated-code*//*@lineinfo:3148^11*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(sum(sm.sales_count),0)          as sales_count,
//                      nvl(sum(sm.sales_amount),0)         as sales_amount,
//                      nvl(sum(sm.credits_count),0)        as credits_count,
//                      nvl(sum(sm.credits_amount),0)       as credits_amount,
//                      nvl(sum(sm.sales_count 
//                              + sm.credits_count),0)      as vol_count,
//                      nvl(sum(sm.sales_amount 
//                              - sm.credits_amount),0)     as vol_amount,
//                      nvl(sum(sm.expense_actual),0)       as ic_exp
//              
//              from    mbs_daily_summary   sm
//              where   sm.me_load_file_id = load_filename_to_load_file_id(:loadFilename)
//                      and sm.merchant_number = :merchantId
//                      and sm.item_type = 111
//                      and sm.item_subclass = 'DS'
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(sum(sm.sales_count),0)          as sales_count,\n                    nvl(sum(sm.sales_amount),0)         as sales_amount,\n                    nvl(sum(sm.credits_count),0)        as credits_count,\n                    nvl(sum(sm.credits_amount),0)       as credits_amount,\n                    nvl(sum(sm.sales_count \n                            + sm.credits_count),0)      as vol_count,\n                    nvl(sum(sm.sales_amount \n                            - sm.credits_amount),0)     as vol_amount,\n                    nvl(sum(sm.expense_actual),0)       as ic_exp\n             \n            from    mbs_daily_summary   sm\n            where   sm.me_load_file_id = load_filename_to_load_file_id( :1  )\n                    and sm.merchant_number =  :2  \n                    and sm.item_type = 111\n                    and sm.item_subclass = 'DS'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"51com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setLong(2,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 7) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(7,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   counts[0] = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[0] = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[1] = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[1] = __sJT_rs.getDouble(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   counts[2] = __sJT_rs.getInt(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[2] = __sJT_rs.getDouble(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   amounts[3] = __sJT_rs.getDouble(7); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3168^11*/
          fields.setData("disc_ic_sales_count"    , counts[0] );
          fields.setData("disc_ic_sales_amount"   , amounts[0]);
          fields.setData("disc_ic_credits_count"  , counts[1] );
          fields.setData("disc_ic_credits_amount" , amounts[1]);
          fields.setData("disc_ic_vol_count"      , counts[2] );
          fields.setData("disc_ic_vol_amount"     , amounts[2]);
          fields.setData("disc_interchange"       , amounts[3]);
          
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:3179^13*/

//  ************************************************************
//  #sql [Ctx] { select sum(case 
//                            when st.st_statement_desc like 'DS%'        then 1
//                            when st.st_statement_desc like '%DSCVR%'    then 1
//                            when st.st_statement_desc like '%DISCOVER%' then 1
//                            else 0
//                          end
//                          * st.st_fee_amount)
//                
//                from    monthly_extract_st    st
//                where   st.hh_load_sec = :LoadSec
//                        and st.item_category = 'IC'          
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sum(case \n                          when st.st_statement_desc like 'DS%'        then 1\n                          when st.st_statement_desc like '%DSCVR%'    then 1\n                          when st.st_statement_desc like '%DISCOVER%' then 1\n                          else 0\n                        end\n                        * st.st_fee_amount)\n               \n              from    monthly_extract_st    st\n              where   st.hh_load_sec =  :1  \n                      and st.item_category = 'IC'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"52com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,LoadSec);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   amounts[0] = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3192^13*/
          }
          catch( Exception ee )
          {
            amounts[0] = 0.0;
          }
          fields.setData("tot_inc_ic_disc", amounts[0]);
        }   // if ( MbsBackend == 1 )
        
        //@showData(fields);//@
        //@compareData(fields);//@
        storeRecord(fields);
        long elapsed = System.currentTimeMillis() - startTs;
        incrementRecCount(elapsed);
        
        try{  
       	 
            /*
             * Code modified by RS for LCR Bank income and expense calculation.
             * 
             */
            
            
            /*@lineinfo:generated-code*//*@lineinfo:3215^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  sum(T1_TOT_INC_PINLESSDEBIT)  as tot_inc_pinlessdebit
//              	  from    monthly_extract_gn
//              	  where   hh_active_date = :activeDate
//              	  and     hh_merchant_number = :merchantId                
//              	 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sum(T1_TOT_INC_PINLESSDEBIT)  as tot_inc_pinlessdebit\n            \t  from    monthly_extract_gn\n            \t  where   hh_active_date =  :1  \n            \t  and     hh_merchant_number =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"53com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"53com.mes.startup.MonthlyExtractSummarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3221^14*/
            
            resultSet = it.getResultSet();

            if ( resultSet.next() )
            {
              incPinlessDebit = resultSet.getDouble("tot_inc_pinlessdebit");
            }
            
            //fields.setData("tot_inc_pinlessdebit"     , incPinlessDebit  );
            
            /*@lineinfo:generated-code*//*@lineinfo:3232^13*/

//  ************************************************************
//  #sql [Ctx] { update  monthly_extract_summary
//                    set     TOT_INC_PINLESSDEBIT     = :incPinlessDebit
//                    where   active_date = :activeDate
//                    and     merchant_number = :merchantId
//               	 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  monthly_extract_summary\n                  set     TOT_INC_PINLESSDEBIT     =  :1  \n                  where   active_date =  :2  \n                  and     merchant_number =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"54com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,incPinlessDebit);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setLong(3,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3238^15*/
             	
            resultSet.close();
            it.close();
            
            /*@lineinfo:generated-code*//*@lineinfo:3243^13*/

//  ************************************************************
//  #sql [Ctx] it = { select sum(expense_actual) as tot_vp_interchange
//        			  from mbs_daily_summary
//        			  where   me_load_file_id = :loadFileId and item_subclass in ('VP') 
//        			  and merchant_number = :merchantId  and item_type = '228'           
//              	 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select sum(expense_actual) as tot_vp_interchange\n      \t\t\t  from mbs_daily_summary\n      \t\t\t  where   me_load_file_id =  :1   and item_subclass in ('VP') \n      \t\t\t  and merchant_number =  :2    and item_type = '228'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"55com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"55com.mes.startup.MonthlyExtractSummarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3249^14*/
            
            resultSet = it.getResultSet();

            if ( resultSet.next() )
            {
              totVpInterChange = resultSet.getDouble("tot_vp_interchange");
            }
            
            resultSet.close();
            it.close();
            
            /*@lineinfo:generated-code*//*@lineinfo:3261^13*/

//  ************************************************************
//  #sql [Ctx] it = { select sum(expense_actual) as tot_mp_interchange
//        			  from mbs_daily_summary
//        			  where   me_load_file_id = :loadFileId and item_subclass in ('MP') 
//        			  and merchant_number = :merchantId  and item_type = '228'           
//              	 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select sum(expense_actual) as tot_mp_interchange\n      \t\t\t  from mbs_daily_summary\n      \t\t\t  where   me_load_file_id =  :1   and item_subclass in ('MP') \n      \t\t\t  and merchant_number =  :2    and item_type = '228'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"56com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"56com.mes.startup.MonthlyExtractSummarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3267^14*/
            
            resultSet = it.getResultSet();

            if ( resultSet.next() )
            {
              totMpInterChange = resultSet.getDouble("tot_mp_interchange");
            }
            
            resultSet.close();
            it.close();
            
            /*@lineinfo:generated-code*//*@lineinfo:3279^13*/

//  ************************************************************
//  #sql [Ctx] it = { select sum(expense_actual) as tot_pinlessdebit_interchange
//        			  from mbs_daily_summary
//        			  where   me_load_file_id = :loadFileId and item_subclass in ('VP','MP') 
//        			  and merchant_number = :merchantId  and item_type = '228'           
//              	 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select sum(expense_actual) as tot_pinlessdebit_interchange\n      \t\t\t  from mbs_daily_summary\n      \t\t\t  where   me_load_file_id =  :1   and item_subclass in ('VP','MP') \n      \t\t\t  and merchant_number =  :2    and item_type = '228'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"57com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"57com.mes.startup.MonthlyExtractSummarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3285^14*/
            
            resultSet = it.getResultSet();

            if ( resultSet.next() )
            {
              totPinlessdebitInterChange = resultSet.getDouble("tot_pinlessdebit_interchange");
            }
            
            resultSet.close();
            it.close();
            
            /*@lineinfo:generated-code*//*@lineinfo:3297^13*/

//  ************************************************************
//  #sql [Ctx] { update  monthly_extract_summary
//                    set     VISA_INTERCHANGE     = VISA_INTERCHANGE + :totVpInterChange,
//                    MC_INTERCHANGE     = MC_INTERCHANGE + :totMpInterChange,
//                    INTERCHANGE_EXPENSE_ENHANCED     = INTERCHANGE_EXPENSE_ENHANCED + :totPinlessdebitInterChange
//                    where   active_date = :activeDate
//                    and     merchant_number = :merchantId
//               	 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  monthly_extract_summary\n                  set     VISA_INTERCHANGE     = VISA_INTERCHANGE +  :1  ,\n                  MC_INTERCHANGE     = MC_INTERCHANGE +  :2  ,\n                  INTERCHANGE_EXPENSE_ENHANCED     = INTERCHANGE_EXPENSE_ENHANCED +  :3  \n                  where   active_date =  :4  \n                  and     merchant_number =  :5 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"58com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,totVpInterChange);
   __sJT_st.setDouble(2,totMpInterChange);
   __sJT_st.setDouble(3,totPinlessdebitInterChange);
   __sJT_st.setDate(4,activeDate);
   __sJT_st.setLong(5,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3305^15*/
             	
            
            /*@lineinfo:generated-code*//*@lineinfo:3308^13*/

//  ************************************************************
//  #sql [Ctx] it = { select sum(expense) as tot_exp_pinlessdebit
//        			from mbs_daily_summary
//        			where   me_load_file_id = :loadFileId and item_subclass in ('VP','MP') 
//        			and merchant_number = :merchantId
//        		   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select sum(expense) as tot_exp_pinlessdebit\n      \t\t\tfrom mbs_daily_summary\n      \t\t\twhere   me_load_file_id =  :1   and item_subclass in ('VP','MP') \n      \t\t\tand merchant_number =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"59com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setLong(2,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"59com.mes.startup.MonthlyExtractSummarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3314^11*/
      			
      	 resultSet = it.getResultSet();

          if ( resultSet.next() )
          {
            expPinlessDebit = resultSet.getDouble("tot_exp_pinlessdebit");
          }
          
          fields.setData("tot_exp_pinlessdebit"     , expPinlessDebit  );
          resultSet.close();
          it.close();
          
          
          /*@lineinfo:generated-code*//*@lineinfo:3328^11*/

//  ************************************************************
//  #sql [Ctx] { update  monthly_extract_summary
//                 set     TOT_EXP_PINLESSDEBIT     = :expPinlessDebit
//                 where   active_date = :activeDate
//                 and     merchant_number = :merchantId
//                };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  monthly_extract_summary\n               set     TOT_EXP_PINLESSDEBIT     =  :1  \n               where   active_date =  :2  \n               and     merchant_number =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"60com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,expPinlessDebit);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setLong(3,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3334^14*/
              
   }catch(Exception e){
  	
  	log.debug("Exception while calculating the partner billing components for MID " + merchantId);
      logEntry("run(" + LoadSec + ")",e.toString());
   }
      }
      catch( Exception e )
      {
        try{ con.rollback();  } catch( Exception ee ){}
        logEntry("run(" + LoadSec + ")",e.toString());
        try{ con.commit();    } catch( Exception ee ){}
      }  
      finally
      {
        cleanUp();
      }
    }
    
    public void compareData(FieldGroup fields)
    {
      ResultSetIterator     it          = null;
      ResultSet             resultSet   = null;
      String                sqlValue    = null;
      
      try
      {
        System.out.println("*********** Compare Begin ***********");
        /*@lineinfo:generated-code*//*@lineinfo:3363^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  * 
//            from    monthly_extract_summary
//            where   hh_load_sec = :getData("hh_load_sec")
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_242 = getData("hh_load_sec");
  try {
   String theSqlTS = "select  * \n          from    monthly_extract_summary\n          where   hh_load_sec =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"61com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_242);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"61com.mes.startup.MonthlyExtractSummarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3368^9*/
        resultSet = it.getResultSet();
        resultSet.next();
        
        Vector allFields = fields.getFieldsVector();

        for(int i=0; i<allFields.size(); ++i)
        {
          Field   f       = (Field)(allFields.elementAt(i));
          String  fname   = f.getName();
          
          sqlValue = resultSet.getString(fname);
          if ( sqlValue == null ) { sqlValue = ""; }
          if ( !sqlValue.equals(f.getData()) )
          {
            System.out.println(StringUtilities.leftJustify(f.getName(),30,' ') + " : " + sqlValue + " != " + f.getData());
          }
        }
        System.out.println("*********** Compare End   ***********");
      }
      catch(Exception e)
      {
        logEntry("compareData()", e.toString());
      }
    }
    
    /*
     * Store the vmc_assessment_expense breakdown data into monthly_extract_exp table categorize by EX
     */
    protected void storeExpenseRecord( ArrayList<ExpenseRecord> erec )
    {

       try
          {
                // loop through the array list and store the individual object data
                for (int i = 0; i < erec.size(); i++) {
                    ExpenseRecord exp = (ExpenseRecord) erec.get(i);
                           /*@lineinfo:generated-code*//*@lineinfo:3405^28*/

//  ************************************************************
//  #sql [Ctx] { insert into monthly_extract_exp  
//                                  (
//                                    hh_load_sec,
//                                    merchant_number,
//                                    active_date,
//                                    item_category,
//                                    description,
//                                    number_of_items,
//                                    amount_of_item,
//                                    tot_fee_amount,
//                                    rate_calculation
//                                    
//                                  )
//                                  values
//                                  (
//                                    :exp.getLoadSec(),
//                                    :exp.getMerchantNumber(),
//                                    :exp.getActiveDate(),
//                                    :exp.getItemCat(), 
//                                    :exp.getDescription(),
//                                    :exp.getItemCount(),
//                                    :exp.getItemAmount(),
//                                    :exp.getTotFeeAmount(),
//                                    :exp.getRateCalculation()
//                                  )
//                                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_243 = exp.getLoadSec();
 long __sJT_244 = exp.getMerchantNumber();
 java.sql.Date __sJT_245 = exp.getActiveDate();
 String __sJT_246 = exp.getItemCat();
 String __sJT_247 = exp.getDescription();
 int __sJT_248 = exp.getItemCount();
 double __sJT_249 = exp.getItemAmount();
 double __sJT_250 = exp.getTotFeeAmount();
 String __sJT_251 = exp.getRateCalculation();
   String theSqlTS = "insert into monthly_extract_exp  \n                                (\n                                  hh_load_sec,\n                                  merchant_number,\n                                  active_date,\n                                  item_category,\n                                  description,\n                                  number_of_items,\n                                  amount_of_item,\n                                  tot_fee_amount,\n                                  rate_calculation\n                                  \n                                )\n                                values\n                                (\n                                   :1  ,\n                                   :2  ,\n                                   :3  ,\n                                   :4  , \n                                   :5  ,\n                                   :6  ,\n                                   :7  ,\n                                   :8  ,\n                                   :9  \n                                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"62com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_243);
   __sJT_st.setLong(2,__sJT_244);
   __sJT_st.setDate(3,__sJT_245);
   __sJT_st.setString(4,__sJT_246);
   __sJT_st.setString(5,__sJT_247);
   __sJT_st.setInt(6,__sJT_248);
   __sJT_st.setDouble(7,__sJT_249);
   __sJT_st.setDouble(8,__sJT_250);
   __sJT_st.setString(9,__sJT_251);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
   
   if ( log.isDebugEnabled() ) {
	   log.debug("[storeExpenseRecord] insert expense record - "+exp);
   }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3432^31*/ 

                }
                /*@lineinfo:generated-code*//*@lineinfo:3435^17*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3435^37*/
          }
          catch( Exception e )
          {
            logEntry("storeExpenseRecord()",e.toString());
          }
         
    }


    public void storeRecord( FieldGroup fields )
    {
      PreparedStatement     ps          = null;
      String                sqlValue    = null;
      
      try
      {
        StringBuffer    sqlText   = new StringBuffer("insert into monthly_extract_summary (");
        
        Vector allFields = fields.getFieldsVector();

        for(int i=0; i<allFields.size(); ++i)
        {
          Field   f       = (Field)(allFields.elementAt(i));
          
          sqlText.append( (i ==0) ? "" : "," );
          sqlText.append( f.getName() );
        }
        sqlText.append(") values (");
        for(int i=0; i<allFields.size(); ++i)
        {
          Field   f       = (Field)(allFields.elementAt(i));
          
          sqlText.append( (i ==0) ? "" : "," );
          if ( f instanceof DateField )
          {
            sqlText.append( "'" + ((DateField)f).getSqlDateString() + "'" );
          }
          else if ( f instanceof NumberField )
          {
            sqlText.append( getDouble(f.getName()) );
          }
          else    // assume string
          {
            sqlText.append( "'" + f.getData().replaceAll("'","''") + "'" );
          }
        }
        sqlText.append(")");

        //@System.out.println(sqlText.toString());//@
        ps = getPreparedStatement(sqlText.toString());
        ps.executeUpdate();
        /*@lineinfo:generated-code*//*@lineinfo:3487^9*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3487^29*/
      }
      catch(Exception e)
      {
        logEntry("storeRecord(" + getData("merchant_number") + "," + getData("hh_load_sec") + ")", e.toString());
      }
      finally
      {
        try{ ps.close(); } catch( Exception ee ){}
      }
    }

    public void showData(FieldGroup fields)
    {
      try
      {
        Vector allFields = fields.getFieldsVector();

        for(int i=0; i<allFields.size(); ++i)
        {
          Field f = (Field)(allFields.elementAt(i));
          System.out.println(StringUtilities.leftJustify(f.getName(),30,' ') + " : " + f.getData());
        }
      }
      catch(Exception e)
      {
        logEntry("showData()", e.toString());
      }
    }
  }
  
  protected int               MbsBackend    = 0;
  protected int               RecCount      = 0;
  protected long              MinTime       = 999999999999L;
  protected long              MaxTime       = 0L;
  protected long              TestNodeId    = 0L;
  protected int               TotalCount    = 0;
  protected long              TotalTime     = 0L;
  
  public MonthlyExtractSummarizer()
  {
  }
  
  public synchronized void incrementRecCount(long elapsed)
  {
    ++RecCount;
    TotalTime += elapsed;
    MaxTime   = (elapsed > MaxTime ? elapsed : MaxTime);
    MinTime   = (elapsed < MinTime ? elapsed : MinTime);
    long avgTime = (long)((double)TotalTime/(double)RecCount);
    double percentComplete = ((double)RecCount/(double)TotalCount);
    System.out.print("\r  Records Processed: " + RecCount + " of " + TotalCount + " (" + NumberFormatter.getPercentString(percentComplete,2) + ")  Min Time: " + MinTime + "  MaxTime: " + MaxTime + "  Avg Time: " + avgTime + "            ");
  }

  /*
   * Object to hold the  expense record information for vmc_assessment_fee  
   */
  public  class ExpenseRecord
  {
      protected     long        MerchantNumber      = 0L;
      protected     long        LoadSec             = 0L;
      protected     Date        ActiveDate          = null;
      protected     String      Description         = null;
      protected     String      ItemCat             = null;
      protected     int         ItemCount           = 0;
      protected     double      ItemAmount          = 0.0;
      protected     double      TotFeeAmount        = 0.0;
      protected     String      RateCalculation     = null;
      
    
    public ExpenseRecord( )
    {
    }
  

    public ExpenseRecord( long lSec, long mNum, Date aDate )
    {
      setLoadSec(lSec);
      setMerchantNumber(mNum);
      setActiveDate(aDate);
    }
    
 
    /*
     * Store the expense data for vmc_assessment_fee  
     */
    public ExpenseRecord(long lSec, long mNum, Date aDate, String itmCat,
            String desc, int itmCount, double itmAmount, double totFeeAmt, String rateCalc) {
        this.TotFeeAmount = totFeeAmt;
        this.ItemAmount = itmAmount;
        this.ItemCount =  itmCount;
        this.MerchantNumber = mNum;
        this.ActiveDate = aDate;
        this.LoadSec = lSec;
        this.Description = desc;
        this.RateCalculation = rateCalc;
    }

    public long     getLoadSec()                      { return( LoadSec );    }
    public void     setLoadSec(long value)            { LoadSec = value;      }

    public long     getMerchantNumber()               { return( MerchantNumber ); }
    public void     setMerchantNumber(long   value)   { MerchantNumber = value;   }
   
    public Date     getActiveDate()                   { return( ActiveDate ); }
    public void     setActiveDate(Date value)         { ActiveDate = value;   }

    public String   getDescription()                  { return( Description );}
    public void     setDescription(String value)      { Description = value;  }
    
    public String   getRateCalculation()              { return( RateCalculation );}
    public void     setRateCalculation(String value)  { RateCalculation = value;  }

    public String   getItemCat()                      { return( ItemCat );    }
    public void     setItemCat(String value)          { ItemCat = value;      }
 
    public int      getItemCount()                    { return( ItemCount );  }
    public void     setItemCount(int value)           { ItemCount = value;    }

    public double   getItemAmount()                   { return( ItemAmount ); }
    public void     setItemAmount(double value)       { ItemAmount = value;   }

    public double   getTotFeeAmount()                 { return(TotFeeAmount); }
    public void     setTotFeeAmount(double value)     { TotFeeAmount = value; }

   
    public void clear()
    {
      TotFeeAmount        = 0.0;    
      ItemAmount          = 0.0;    
      ItemCount           = 0;    
      ItemCat             = null;
      MerchantNumber      = 0L;    
      LoadSec             = 0L;    
      Description         = null;  
      ActiveDate          = null;
      RateCalculation     = null;
    }


	@Override
	public String toString() {
		return String.format("ExpenseRecord [MerchantNumber=%s, LoadSec=%s, ActiveDate=%s, Description=%s, "
								+ "ItemCat=%s, ItemCount=%s, ItemAmount=%s, TotFeeAmount=%s, RateCalculation=%s]", 
							  MerchantNumber, LoadSec, ActiveDate, Description, 
							  	  ItemCat, ItemCount, ItemAmount, TotFeeAmount, RateCalculation);
	}
    
    
  }
  
  public void loadMonthlyExtractSummary( String loadFilename, boolean onlyMissingRecs )
  {
    int                 bankNumber          = 0;
    ResultSetIterator   it                  = null;
    ResultSet           resultSet           = null;
    int                 rowCount            = 0;
    ThreadPool          pool                = null;
    Vector              loadSecs            = new Vector();
    long                loadSec             = 0L;
    long                loadFileId          = 0L;

    try
    {
      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
    
      /*@lineinfo:generated-code*//*@lineinfo:3642^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init( :loadFilename )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init(  :1   )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"63com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3645^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:3647^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:loadFilename),
//                  get_file_bank_number(:loadFilename)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1  ),\n                get_file_bank_number( :2  )\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"64com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setString(2,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   bankNumber = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3653^7*/
      MbsBackend = ((loadFilename.indexOf("mbs_ext") >= 0) ? 1 : 0);
      
      // only delete if processing all records
      if ( onlyMissingRecs == false)
      {
        log.debug("deleting existing records");
      
        // delete any existing records for this file
        /*@lineinfo:generated-code*//*@lineinfo:3662^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    monthly_extract_summary sm
//            where   sm.load_file_id = :loadFileId
//                    and 
//                    (
//                      :TestNodeId = 0 
//                      or sm.merchant_number in 
//                          ( 
//                            select  gm.merchant_number 
//                            from    organization    o, 
//                                    group_merchant  gm 
//                            where   o.org_group = :TestNodeId 
//                                    and gm.org_num = o.org_num 
//                          )
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    monthly_extract_summary sm\n          where   sm.load_file_id =  :1  \n                  and \n                  (\n                     :2   = 0 \n                    or sm.merchant_number in \n                        ( \n                          select  gm.merchant_number \n                          from    organization    o, \n                                  group_merchant  gm \n                          where   o.org_group =  :3   \n                                  and gm.org_num = o.org_num \n                        )\n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"65com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setLong(2,TestNodeId);
   __sJT_st.setLong(3,TestNodeId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3679^9*/
      }

      log.debug("selecting data...");
      int missingRecsOnly = (onlyMissingRecs ? 1 : 0);
      /*@lineinfo:generated-code*//*@lineinfo:3684^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  gn.hh_load_sec                     as load_sec
//          from    monthly_extract_gn        gn, 
//                  mif                       mf
//          where   gn.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                  and mf.merchant_number = gn.hh_merchant_number
//                  and 
//                  (
//                    :missingRecsOnly = 0 or
//                    not exists
//                    (
//                      select  sm.merchant_number
//                      from    monthly_extract_summary   sm
//                      where   sm.merchant_number = gn.hh_merchant_number
//                              and sm.active_date = gn.hh_active_date
//                    )
//                  )
//                  and
//                  (
//                    :TestNodeId = 0 
//                    or gn.hh_merchant_number in 
//                        ( 
//                          select  gm.merchant_number 
//                          from    organization    o, 
//                                  group_merchant  gm 
//                          where   o.org_group = :TestNodeId 
//                                  and gm.org_num = o.org_num 
//                        )
//                  )                        
//                  and 
//                  (
//                    ( 
//                      :loadFilename like 'mon_ext%'
//                      and ( nvl(mf.processor_id,0) != 1
//                            or mf.conversion_date > gn.hh_active_date )
//                    )
//                    or
//                    ( 
//                      :loadFilename like 'mbs_ext%'
//                      and nvl(mf.processor_id,0) = 1
//                      and mf.conversion_date <= gn.hh_active_date
//                    )
//                  )
//          order by gn.hh_load_sec                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  gn.hh_load_sec                     as load_sec\n        from    monthly_extract_gn        gn, \n                mif                       mf\n        where   gn.load_file_id = load_filename_to_load_file_id( :1  )\n                and mf.merchant_number = gn.hh_merchant_number\n                and \n                (\n                   :2   = 0 or\n                  not exists\n                  (\n                    select  sm.merchant_number\n                    from    monthly_extract_summary   sm\n                    where   sm.merchant_number = gn.hh_merchant_number\n                            and sm.active_date = gn.hh_active_date\n                  )\n                )\n                and\n                (\n                   :3   = 0 \n                  or gn.hh_merchant_number in \n                      ( \n                        select  gm.merchant_number \n                        from    organization    o, \n                                group_merchant  gm \n                        where   o.org_group =  :4   \n                                and gm.org_num = o.org_num \n                      )\n                )                        \n                and \n                (\n                  ( \n                     :5   like 'mon_ext%'\n                    and ( nvl(mf.processor_id,0) != 1\n                          or mf.conversion_date > gn.hh_active_date )\n                  )\n                  or\n                  ( \n                     :6   like 'mbs_ext%'\n                    and nvl(mf.processor_id,0) = 1\n                    and mf.conversion_date <= gn.hh_active_date\n                  )\n                )\n        order by gn.hh_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"66com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setInt(2,missingRecsOnly);
   __sJT_st.setLong(3,TestNodeId);
   __sJT_st.setLong(4,TestNodeId);
   __sJT_st.setString(5,loadFilename);
   __sJT_st.setString(6,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"66com.mes.startup.MonthlyExtractSummarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3729^7*/
      resultSet = it.getResultSet();              
      
      log.debug("extracting records to process");
      while( resultSet.next() )
      {
        loadSecs.addElement(resultSet.getLong("load_sec"));
      }
      resultSet.close();
      it.close();
      
      TotalCount = loadSecs.size();
      
      pool = new ThreadPool(7);
      for( int i = 0; i < loadSecs.size(); ++i )
      {
        loadSec = ((Long)loadSecs.elementAt(i)).longValue();
        Thread thread = pool.getThread( new SummaryJob(loadSec) );
        thread.start();
      }
      pool.waitForAllThreads();
      
      // do not run the downstream jobs when a test node is specified
      if ( TestNodeId == 0L )
      {
        // queue the equipment cost summary
        /*@lineinfo:generated-code*//*@lineinfo:3755^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_prof_process
//              ( process_type, load_filename )
//            values
//              ( 9, :loadFilename )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_prof_process\n            ( process_type, load_filename )\n          values\n            ( 9,  :1   )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"67com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3761^9*/
        
        // queue the debit count summary
        /*@lineinfo:generated-code*//*@lineinfo:3764^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_prof_process
//              ( process_type, load_filename )
//            values
//              ( 2, :loadFilename )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_prof_process\n            ( process_type, load_filename )\n          values\n            ( 2,  :1   )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"68com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3770^9*/            
      
        // queue the loading of the contract summary
        /*@lineinfo:generated-code*//*@lineinfo:3773^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_prof_process
//              ( process_type, load_filename )
//            values
//              ( 5, :loadFilename )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_prof_process\n            ( process_type, load_filename )\n          values\n            ( 5,  :1   )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"69com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3779^9*/
    
        // queue the hierarchy snapshot
        /*@lineinfo:generated-code*//*@lineinfo:3782^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_prof_process
//              ( process_type, load_filename )
//            values
//              ( 12, :loadFilename )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_prof_process\n            ( process_type, load_filename )\n          values\n            ( 12,  :1   )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"70com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3788^9*/          

        if ( 3941 == bankNumber ||
             3943 == bankNumber )
        {
          // queue the transcom extract summary
          /*@lineinfo:generated-code*//*@lineinfo:3794^11*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_prof_process
//                ( process_type, load_filename )
//              values
//                ( 7, :loadFilename )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_prof_process\n              ( process_type, load_filename )\n            values\n              ( 7,  :1   )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"71com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3800^11*/            

          // queue the verisign over auth expense recovery
          // TSYS and MBS only (file prefix equals mon_ext/mbs_ext)
          if (    loadFilename.startsWith("mon_ext")
               || loadFilename.startsWith("mbs_ext") )
          {
            /*@lineinfo:generated-code*//*@lineinfo:3807^13*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_prof_process
//                  ( process_type, load_filename )
//                values
//                  ( 10, :loadFilename )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_prof_process\n                ( process_type, load_filename )\n              values\n                ( 10,  :1   )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"72com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3813^13*/
          }          

          // queue the month end commissions
          /*@lineinfo:generated-code*//*@lineinfo:3817^11*/

//  ************************************************************
//  #sql [Ctx] { insert into commission_process
//                ( process_type, load_filename )
//              values
//                ( 1, :loadFilename )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into commission_process\n              ( process_type, load_filename )\n            values\n              ( 1,  :1   )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"73com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3823^11*/
          
          // queue the TID summary (Digital River)
          /*@lineinfo:generated-code*//*@lineinfo:3826^11*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_prof_process
//                ( process_type, load_filename )
//              values
//                ( 13, :loadFilename )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_prof_process\n              ( process_type, load_filename )\n            values\n              ( 13,  :1   )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"74com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3832^11*/
        }            
        // Validate Month End
        Calendar cal = Calendar.getInstance();
        cal.add( Calendar.MONTH, -1 );
        cal.set( Calendar.DAY_OF_MONTH, 1 );
        java.sql.Date  activeDatep  = new java.sql.Date( cal.getTime().getTime() );
        MonthEndValidation mev = new MonthEndValidation(bankNumber, activeDatep);
        boolean isMonthEndGood = mev.validate();
        // initiate the monthly billing process if month end is good.
        // if finace feels that month end is good even if variances are big
        // then statement generation will be a simple matter of doing an insert into statement_process manually 
        if(isMonthEndGood) {   
            /*@lineinfo:generated-code*//*@lineinfo:3845^13*/

//  ************************************************************
//  #sql [Ctx] { insert into statement_process ( process_type,process_sequence,load_filename )
//                values (0,0,:loadFilename)
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into statement_process ( process_type,process_sequence,load_filename )\n              values (0,0, :1  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"75com.mes.startup.MonthlyExtractSummarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3849^13*/
        }
      }   // end if ( TestNodeId != 0L )

      // remove if called by a trigger
      /*@lineinfo:generated-code*//*@lineinfo:3854^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3857^7*/        
      
      Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      long elapsed = (endTime.getTime() - beginTime.getTime());
      
      System.out.println();
      System.out.println("Begin Time: " + String.valueOf(beginTime));
      System.out.println("End Time  : " + String.valueOf(endTime));
      System.out.println("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));
    }
    catch( Exception e )
    {
      logEntry("loadMESummary(" + loadFilename + ")",e.toString());
    }
  }
  
  protected void setTestNodeId( long testNodeId )
  {
    TestNodeId = testNodeId;
  }
  
  public static void main( String[] args )
  {
    MonthlyExtractSummarizer  bean          = null;
    boolean                   missingOnly   = false;
    
    try
    {
      SQLJConnectionBase.initStandalonePool("DEBUG");
      
      bean = new MonthlyExtractSummarizer();
      bean.connect(true);
      
      if( args.length > 1 )
      {
        if ( "true".equals(args[1]) )
        {
          missingOnly = true;
        }
        else
        {
          long testNodeId = 0L;
          try { testNodeId = Long.parseLong(args[1]); } catch( Exception ee ) { testNodeId = 0L; }
          bean.setTestNodeId(testNodeId);
        }
      }
      bean.loadMonthlyExtractSummary(args[0],missingOnly);
    }
    catch( Exception e )
    {
      log.error(e.toString());
    }
    finally
    {
      try{ bean.cleanUp(); } catch( Exception ee ){}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/