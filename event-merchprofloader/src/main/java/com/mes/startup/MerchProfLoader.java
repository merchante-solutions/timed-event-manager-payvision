/*@lineinfo:filename=MerchProfLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/MerchProfLoader.sqlj $

  Description:  
    Summarization methods for merchant profitability.


  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-07-09 13:20:22 -0700 (Thu, 09 Jul 2015) $
  Version            : $Revision: 23735 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.mbs.BillingDb;
import com.mes.mbs.MerchantBilling;
import com.mes.mbs.MerchantBillingElement;
import com.mes.reports.BankContractBean;
import com.mes.reports.BankContractDataBean;
import com.mes.reports.BankContractDataBean.SummaryData;
import com.mes.reports.ContractTypes;
import com.mes.reports.DiscoverEvergreenDataBean;
import com.mes.reports.ReportSQLJBean;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import com.mes.support.ThreadPool;
import sqlj.runtime.ResultSetIterator;

public class MerchProfLoader extends ExtractLoader
{
  static Logger log = Logger.getLogger(MerchProfLoader.class);

  public static final int     PT_EXTRACT_SUMMARY              = 1;
  public static final int     PT_DEBIT_COUNT_SUMMARY          = 2;
  public static final int     PT_DISCOVER_SALES_SUMMARY       = 3;
  public static final int     PT_ATTRITION_IMPACT             = 4;
  public static final int     PT_CONTRACT_SUMMARY             = 5;
  public static final int     PT_CERTEGY_EXT_SUMMARY          = 6;
  public static final int     PT_TRANSCOM_EXT_SUMMARY         = 7;
  public static final int     PT_DISCOVER_EVERGREEN_SUMMARY   = 8;
  public static final int     PT_EQUIP_SUMMARY                = 9;
  public static final int     PT_VERISIGN_OVER_AUTH_BILLING   = 10;  
  public static final int     PT_EPICOR_AP                    = 11;  
  public static final int     PT_HIERARCHY_SNAPSHOT           = 12;  
  public static final int     PT_TID_SUMMARY                  = 13;  
  public static final int     PT_MOJAVE_SUMMARY               = 14;
  
  // debit categories
  public static final int         DB_INVALID                = -1;
  public static final int         DB_SALES                  = 0;
  public static final int         DB_CREDITS                = 1;
  public static final int         DB_CASH_BACK              = 2;
  
  public static final int         AP_STAGING                = 0;
  public static final int         AR_STAGING                = 1;
  private double                  expAchBuyback             = 0.0;
  private double                  expAchBuybackDisc         = 0.0;
 
  public static final String[] RevenueColumnNames =
  {
    "disc_ic_inc",        // BankContractBean.BET_GROUP_DISC_IC
    "capture_inc",        // BankContractBean.BET_GROUP_CAPTURE_FEES
    "debit_inc",          // BankContractBean.BET_GROUP_DEBIT_FEES
    "plan_inc",           // BankContractBean.BET_GROUP_PLAN_FEES
    "auth_inc",           // BankContractBean.BET_GROUP_AUTH_FEES
    "sys_gen_inc",        // BankContractBean.BET_GROUP_SYSTEM_FEES
    "equip_rental_inc",   // BankContractBean.BET_GROUP_EQUIP_RENTAL
    "equip_sales_inc",    // BankContractBean.BET_GROUP_EQUIP_SALES
    "ach_inc",            // BankContractBean.BET_GROUP_ACH
    null,                 // BankContractBean.BET_GROUP_PARTNERSHIP
    null,                 // BankContractBean.BET_GROUP_ADJUSTMENTS
  };
  
  // container for the debit bet data
  private class DebitBetData
  {
    private Date                ActiveDate  = null;
    private int                 BankNumber  = 0;
    private int                 BetNumber   = 0;
    private Vector              Elements    = new Vector();
    
    private class DebitElement
    {
      public  int         Category      = 0;
      public  double      PerItemInc    = 0.0;
      public  int         CatType       = DB_INVALID;
      
      DebitElement( ResultSet resultSet )
        throws java.sql.SQLException
      {
        CatType = resultSet.getInt("seg_type");
        Category = resultSet.getInt("cat");
        PerItemInc = resultSet.getDouble("per_item_inc");
      }
    }
    
    DebitBetData( int bankNumber, int betNumber, Date activeDate, ResultSet resultSet )
      throws java.sql.SQLException
    {
      ActiveDate  = activeDate;
      BankNumber  = bankNumber;
      BetNumber   = betNumber;
      
      while( resultSet.next() )
      {
        Elements.addElement( new DebitElement(resultSet) );
      }
    }
    
    private DebitElement findElement( int cat, int type )
    {
      DebitElement      retVal      = null;
      DebitElement      temp        = null;
      
      for( int i = 0; i < Elements.size(); ++i )
      {
        temp = (DebitElement)Elements.elementAt(i);
        if ( temp.Category == cat && temp.CatType == type )
        {
          retVal = temp;
          break;
        }             
      }
      return( retVal );
    }
    
    public Date getActiveDate()
    {
      return(ActiveDate);
    }
    
    public int getBankNumber()
    {
      return(BankNumber);
    }
    
    public int getBetNumber()
    {
      return(BetNumber);
    }
    
    public double getIncomePerItem( int cat, int type )
    {
      DebitElement    element = findElement(cat,type);
      double          retVal  = 0.0;
      
      if ( element != null )
      {
        retVal = element.PerItemInc;
      }
      return( retVal );
    }
  }
  
  private static class MeByTid
  {
    public      Date            ActiveDate        = null;
    public      long            MerchantId        = 0L;
    public      Date            MonthBeginDate    = null;
    public      Date            MonthEndDate      = null;
    
    public MeByTid( ResultSet resultSet )
      throws java.sql.SQLException
    {
      ActiveDate        = resultSet.getDate("active_date");
      MerchantId        = resultSet.getLong("merchant_number");
      MonthBeginDate    = resultSet.getDate("month_begin_date");
      MonthEndDate      = resultSet.getDate("month_end_date");
    }
  }
  
  private static class LoadMeDataByTidWorker extends SQLJConnectionBase
    implements Runnable
  {
    private       Date              ActiveDate      = null;
    private       MerchantBilling   BillingData     = null;
    private       long              LoadFileId      = 0L;
    private       String            LoadFilename    = null;
    private       long              MerchantId      = 0L;
    private       int               MinTermNum      = 9999;
    private       Date              MonthBeginDate  = null;
    private       Date              MonthEndDate    = null;
    
    
    public LoadMeDataByTidWorker( String loadFilename, MeByTid meByTid )
    
    {
      ActiveDate      = meByTid.ActiveDate;
      MerchantId      = meByTid.MerchantId;
      LoadFilename    = loadFilename;
      MonthBeginDate  = meByTid.MonthBeginDate;
      MonthEndDate    = meByTid.MonthEndDate;
      
      BillingData = BillingDb.loadMerchantBilling(MerchantId,MonthEndDate);
    }
  
    public void run( )
    {
      try
      {
        connect(true);
        
        /*@lineinfo:generated-code*//*@lineinfo:243^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    mbs_tid_summary
//            where   merchant_number = :MerchantId
//                    and active_date = :ActiveDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    mbs_tid_summary\n          where   merchant_number =  :1  \n                  and active_date =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   __sJT_st.setDate(2,ActiveDate);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:249^9*/
        
        if ( LoadFilename != null )
        {
          /*@lineinfo:generated-code*//*@lineinfo:253^11*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:LoadFilename)
//              
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1  )\n             \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,LoadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   LoadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:258^11*/
        }
        else
        {
          LoadFileId = 0L;
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:265^9*/

//  ************************************************************
//  #sql [Ctx] { insert into mbs_tid_summary
//            (
//              terminal_id,
//              merchant_number,
//              active_date,
//              interchange_expense,
//              assessment_expense,
//              association_fees,
//              processing_fees,
//              authorization_fees,
//              misc_fees,
//              cb_fees,
//              cb_adj_amount
//            )
//            select  tp.terminal_id,
//                    :MerchantId,
//                    :ActiveDate,
//                    0,0,0,0,0,0,0,0
//            from    trident_profile   tp
//            where   tp.merchant_number = :MerchantId                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mbs_tid_summary\n          (\n            terminal_id,\n            merchant_number,\n            active_date,\n            interchange_expense,\n            assessment_expense,\n            association_fees,\n            processing_fees,\n            authorization_fees,\n            misc_fees,\n            cb_fees,\n            cb_adj_amount\n          )\n          select  tp.terminal_id,\n                   :1  ,\n                   :2  ,\n                  0,0,0,0,0,0,0,0\n          from    trident_profile   tp\n          where   tp.merchant_number =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   __sJT_st.setDate(2,ActiveDate);
   __sJT_st.setLong(3,MerchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:287^9*/
        
        loadAP();
        loadCB();
        loadPL();
        loadST();
      }
      catch( Exception e )
      {
        System.out.println( "run( " + MerchantId + "," + LoadFilename + ") - " + e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    
    private double extractData( ResultSet resultSet, HashMap columnMap, String ctColName, String nonDefaultCardTypes )
    {
      MerchantBillingElement  el        = null;
      double                  retVal    = 0.0;
      
      try
      {
        Iterator i = columnMap.keySet().iterator();
        while( i.hasNext() )
        {
          String    key       = (String)i.next();
          int       itemType  = Integer.parseInt(key);
          String    cardType  = ((ctColName == null) ? null : resultSet.getString(ctColName));
          String[]  colNames  = ((String)columnMap.get(key)).split("/");
          boolean   useDefault= ((nonDefaultCardTypes == null) ? true : (nonDefaultCardTypes.indexOf(cardType) < 0));
          
          el = BillingData.findItemByType(itemType,cardType,useDefault);
          if ( el != null )
          {
            int     count   = ("none".equals(colNames[0]) ? 0   : resultSet.getInt   (colNames[0])); 
            double  amount  = ("none".equals(colNames[1]) ? 0.0 : resultSet.getDouble(colNames[1])); 
            
            retVal += ((amount * el.getRate() * 0.01) + (count * el.getPerItem()));
          }
        }
      }
      catch( Exception e )
      {
        logEntry("extractData()",e.toString());
      }
      finally
      {
      }
      return( retVal );
    }
    
    private void loadAP()
    {
      HashMap                 assocColumnNames      = null;
      double                  assocFees             = 0.0;
      HashMap                 authColumnNames       = new HashMap();
      double                  authFees              = 0.0;
      ResultSetIterator       it                    = null;
      String                  lastTid               = null;
      ResultSet               resultSet             = null;
      String                  tid                   = null;
      
      try
      {
        assocColumnNames = 
          new HashMap()
          {
            {
              put(  "7"    ,"total_count/none"   );  // VS APF
              put( "13"    ,"total_count/none"   );  // DS Data Usage Fee
              put("123"    ,"total_count/none"   );  // MC Access Fee
            }
          };
      
        /*@lineinfo:generated-code*//*@lineinfo:363^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  item_type,
//                    (nvl(count_column_name,'none') || '/' || 
//                     nvl(amount_column_name,'none'))  as col_names
//            from    mbs_elements    
//            where   item_category = 'AUTH'                  
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  item_type,\n                  (nvl(count_column_name,'none') || '/' || \n                   nvl(amount_column_name,'none'))  as col_names\n          from    mbs_elements    \n          where   item_category = 'AUTH'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.MerchProfLoader",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:370^9*/
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          authColumnNames.put( resultSet.getString("item_type"),
                               resultSet.getString("col_names") );
        }
        resultSet.close();
        it.close();
      
        /*@lineinfo:generated-code*//*@lineinfo:381^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    /*+ 
//                          index(auth idx_tc33_merch_date) 
//                          no_query_transformation
//                      */
//                      auth.terminal_id                as tid,
//                      substr(terminal_id,-4)          as term_num,
//                      auth.card_type                  as card_type, 
//                      count(1)                        as total_count,
//                      sum( decode(auth.developer_id,
//                                  'TELPAY',0,
//                                  'VCAUTH',0,
//                                  1) )                as item_count,
//                      case 
//                        when sum(decode(auth.developer_id,'TELPAY',0,'VCAUTH',0,1) *
//                                 auth.authorized_amount) > 9999999999.99 then 9999999999.99 -- hack to deal with limitations of file format
//                        else sum(decode(auth.developer_id,'TELPAY',0,'VCAUTH',0,1) *
//                                 auth.authorized_amount) 
//                        end                           as item_amount,
//                      sum( decode(nvl(auth.avs_result,'0'),
//                                  '0',0,1) )          as avs_count,
//                      0                               as avs_amount,
//                      sum( decode(auth.developer_id,
//                                  'TELPAY',1,
//                                  'VCAUTH',1,
//                                  0) )                as aru_count,
//                      0                               as aru_amount                     
//            from      tc33_authorization        auth
//            where     auth.merchant_number = :MerchantId
//                      and auth.transaction_date between :MonthBeginDate and :MonthEndDate
//                      and auth.load_filename in
//                      (          
//                        select  ds.data_source
//                        from    mbs_daily_summary   ds
//                        where   ds.me_load_file_id = :LoadFileId
//                                and ds.merchant_number = :MerchantId
//                                and ds.activity_date between :MonthBeginDate-30 and :MonthEndDate+7
//                      )
//            group by auth.terminal_id,auth.card_type
//            order by auth.terminal_id,auth.card_type        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    /*+ \n                        index(auth idx_tc33_merch_date) \n                        no_query_transformation\n                    */\n                    auth.terminal_id                as tid,\n                    substr(terminal_id,-4)          as term_num,\n                    auth.card_type                  as card_type, \n                    count(1)                        as total_count,\n                    sum( decode(auth.developer_id,\n                                'TELPAY',0,\n                                'VCAUTH',0,\n                                1) )                as item_count,\n                    case \n                      when sum(decode(auth.developer_id,'TELPAY',0,'VCAUTH',0,1) *\n                               auth.authorized_amount) > 9999999999.99 then 9999999999.99 -- hack to deal with limitations of file format\n                      else sum(decode(auth.developer_id,'TELPAY',0,'VCAUTH',0,1) *\n                               auth.authorized_amount) \n                      end                           as item_amount,\n                    sum( decode(nvl(auth.avs_result,'0'),\n                                '0',0,1) )          as avs_count,\n                    0                               as avs_amount,\n                    sum( decode(auth.developer_id,\n                                'TELPAY',1,\n                                'VCAUTH',1,\n                                0) )                as aru_count,\n                    0                               as aru_amount                     \n          from      tc33_authorization        auth\n          where     auth.merchant_number =  :1  \n                    and auth.transaction_date between  :2   and  :3  \n                    and auth.load_filename in\n                    (          \n                      select  ds.data_source\n                      from    mbs_daily_summary   ds\n                      where   ds.me_load_file_id =  :4  \n                              and ds.merchant_number =  :5  \n                              and ds.activity_date between  :6  -30 and  :7  +7\n                    )\n          group by auth.terminal_id,auth.card_type\n          order by auth.terminal_id,auth.card_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   __sJT_st.setDate(2,MonthBeginDate);
   __sJT_st.setDate(3,MonthEndDate);
   __sJT_st.setLong(4,LoadFileId);
   __sJT_st.setLong(5,MerchantId);
   __sJT_st.setDate(6,MonthBeginDate);
   __sJT_st.setDate(7,MonthEndDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:422^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          tid = resultSet.getString("tid");
          
          // store the fees due when the tid changes
          if ( lastTid != null && !lastTid.equals(tid) )
          {
            /*@lineinfo:generated-code*//*@lineinfo:432^13*/

//  ************************************************************
//  #sql [Ctx] { update  mbs_tid_summary   
//                set     authorization_fees  = (authorization_fees + :authFees),
//                        association_fees    = (association_fees   + :assocFees)
//                where   terminal_id = :lastTid
//                        and active_date = :ActiveDate
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mbs_tid_summary   \n              set     authorization_fees  = (authorization_fees +  :1  ),\n                      association_fees    = (association_fees   +  :2  )\n              where   terminal_id =  :3  \n                      and active_date =  :4 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,authFees);
   __sJT_st.setDouble(2,assocFees);
   __sJT_st.setString(3,lastTid);
   __sJT_st.setDate(4,ActiveDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:439^13*/
            authFees  = 0.0;
            assocFees = 0.0;
          }
          assocFees += extractData(resultSet,assocColumnNames ,"card_type",null   );
          authFees  += extractData(resultSet,authColumnNames  ,"card_type","DB,EB");
          
          MinTermNum  = Math.min(MinTermNum,resultSet.getInt("term_num"));
          lastTid     = tid;
        }
        resultSet.close();
        it.close();
        
        /*@lineinfo:generated-code*//*@lineinfo:452^9*/

//  ************************************************************
//  #sql [Ctx] { update  mbs_tid_summary   
//            set     authorization_fees  = (authorization_fees + :authFees),
//                    association_fees    = (association_fees   + :assocFees)
//            where   terminal_id = :tid
//                    and active_date = :ActiveDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mbs_tid_summary   \n          set     authorization_fees  = (authorization_fees +  :1  ),\n                  association_fees    = (association_fees   +  :2  )\n          where   terminal_id =  :3  \n                  and active_date =  :4 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,authFees);
   __sJT_st.setDouble(2,assocFees);
   __sJT_st.setString(3,tid);
   __sJT_st.setDate(4,ActiveDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:459^9*/
      }
      catch( Exception e )
      {
        logEntry("loadAP(" + MerchantId + "," + ActiveDate + ")",e.toString());
      }
      finally
      {
        try{ it.close(); } catch( Exception ee ) {}
      }
    }
    
    private void loadCB()
    {
      HashMap                 cbColumnNames         = new HashMap();
      ResultSetIterator       it                    = null;
      ResultSet               resultSet             = null;
      
      try
      {
        cbColumnNames = 
          new HashMap()
          {
            {
              put(  "5"    ,"cb_count/none"     );  // chargeback fee
            }
          };
      
        /*@lineinfo:generated-code*//*@lineinfo:487^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                        ordered
//                        index(adj idx_daily_detail_file_adj_mabd)
//                        no_query_transformation
//                    */
//                    tp.terminal_id                              as tid,
//                    substr(tp.terminal_id,-4)                   as term_num,
//                    sum(decode(adj.transacction_code,9071,1,0)) as cb_count,
//                    sum(decode(adj.transacction_code,9071,-1,1)
//                        * adj.adjustment_amount)                as cb_amount
//            from    daily_detail_file_adjustment  adj,
//                    trident_profile               tp
//            where   adj.merchant_account_number = :MerchantId
//                    and adj.batch_date between (:MonthBeginDate-120) and :MonthEndDate+7
//                    and adj.load_filename in 
//                    (
//                        select  ds.data_source
//                        from    mbs_daily_summary   ds
//                        where   ds.me_load_file_id = :LoadFileId
//                                and ds.merchant_number = :MerchantId
//                                and ds.activity_date between :MonthBeginDate-30 and :MonthEndDate+7
//                    )
//                    and tp.merchant_number = adj.merchant_account_number
//                    and tp.catid = adj.catid
//            group by tp.terminal_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                      ordered\n                      index(adj idx_daily_detail_file_adj_mabd)\n                      no_query_transformation\n                  */\n                  tp.terminal_id                              as tid,\n                  substr(tp.terminal_id,-4)                   as term_num,\n                  sum(decode(adj.transacction_code,9071,1,0)) as cb_count,\n                  sum(decode(adj.transacction_code,9071,-1,1)\n                      * adj.adjustment_amount)                as cb_amount\n          from    daily_detail_file_adjustment  adj,\n                  trident_profile               tp\n          where   adj.merchant_account_number =  :1  \n                  and adj.batch_date between ( :2  -120) and  :3  +7\n                  and adj.load_filename in \n                  (\n                      select  ds.data_source\n                      from    mbs_daily_summary   ds\n                      where   ds.me_load_file_id =  :4  \n                              and ds.merchant_number =  :5  \n                              and ds.activity_date between  :6  -30 and  :7  +7\n                  )\n                  and tp.merchant_number = adj.merchant_account_number\n                  and tp.catid = adj.catid\n          group by tp.terminal_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   __sJT_st.setDate(2,MonthBeginDate);
   __sJT_st.setDate(3,MonthEndDate);
   __sJT_st.setLong(4,LoadFileId);
   __sJT_st.setLong(5,MerchantId);
   __sJT_st.setDate(6,MonthBeginDate);
   __sJT_st.setDate(7,MonthEndDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:514^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          String  tid       = resultSet.getString("tid");
          double  cbAmount  = resultSet.getDouble("cb_amount");
          double  cbFees    = extractData(resultSet,cbColumnNames,null,null);
          
          /*@lineinfo:generated-code*//*@lineinfo:523^11*/

//  ************************************************************
//  #sql [Ctx] { update  mbs_tid_summary   
//              set     cb_fees       = (cb_fees       + :cbFees),
//                      cb_adj_amount = (cb_adj_amount + :cbAmount)
//              where   terminal_id = :tid
//                      and active_date = :ActiveDate
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mbs_tid_summary   \n            set     cb_fees       = (cb_fees       +  :1  ),\n                    cb_adj_amount = (cb_adj_amount +  :2  )\n            where   terminal_id =  :3  \n                    and active_date =  :4 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,cbFees);
   __sJT_st.setDouble(2,cbAmount);
   __sJT_st.setString(3,tid);
   __sJT_st.setDate(4,ActiveDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:530^11*/
          
          MinTermNum = Math.min(MinTermNum,resultSet.getInt("term_num"));
        }
        resultSet.close();
        it.close();
      }
      catch( Exception e )
      {
        logEntry("loadCB(" + MerchantId + "," + ActiveDate + ")",e.toString());
      }
      finally
      {
        try{ it.close(); } catch( Exception ee ) {}
      }
    }
    
    private void loadPL()
    {
      double                  assessment            = 0.0;
      HashMap                 assessmentColumnNames = null;
      double                  assocFees             = 0.0;
      HashMap                 discountColumnNames   = null;
      HashMap                 feeColumnNames        = null;
      double                  icExp                 = 0.0;
      ResultSetIterator       it                    = null;
      String                  lastTid               = null;
      HashMap                 procFeeColumnNames    = null;
      double                  procFees              = 0.0;
      ResultSet               resultSet             = null;
      String                  tid                   = null;
      
      try
      {
        discountColumnNames = 
          new HashMap()
          {
            {
              put("101"    ,"sales_count/none"        );  // discount
            }
          };
      
        procFeeColumnNames = 
          new HashMap()
          {
            {
              put("200"    ,"credits_count/none"      );  // credit fee
              put("201"    ,"credits_count/none"      );  // settlement fee
              put("206"    ,"sales_count/none"        );  // capture fee
            }
          };
      
        feeColumnNames = 
          new HashMap()
          {
            {
              put("113"    ,"none/foreign_amount"     );  // MC Cross Border
              put("114"    ,"none/foreign_amount"     );  // MC Global Support
              put("115"    ,"none/foreign_amount"     );  // VS ISA
              put("116"    ,"none/foreign_amount"     );  // VS IAF
              put("118"    ,"none/foreign_amount"     );  // DS IPF
              put("119"    ,"none/foreign_amount"     );  // DS ISF
              put("117"    ,"none/lt_sales_amount"    );  // MC Large Ticket Assessment
              put("102"    ,"total_count/none"        );  // MC NABU fee
              put("205"    ,"sales_count/none"        );  // Base II fee
            }
          };
          
        assessmentColumnNames = 
          new HashMap()
          {
            {
              put("112"    ,"none/sales_amount"       );  
            }
          };
          
        /*@lineinfo:generated-code*//*@lineinfo:606^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    /*+ 
//                          ordered
//                          index(dt idx_ddf_dt_bdate_merch_acct) 
//                          no_query_transformation
//                      */
//                      tp.terminal_id                    as tid,
//                      substr(tp.terminal_id,-4)         as term_num,
//                      dt.card_type                      as plan_type, 
//                      case
//                        when dt.card_type in ('VS','VB','VD') then 'VS'
//                        when dt.card_type in ('MC','MB','MD') then 'MC'
//                        when dt.card_type in ('DS','JC','DN') then 'DS'
//                        else dt.card_type
//                      end                               as card_type, 
//                      count(1)                          as total_count,
//                      sum(decode(dt.debit_credit_indicator,'C',0,1))
//                                                        as sales_count,
//                      sum(decode(dt.debit_credit_indicator,'C',0,1)
//                          * dt.transaction_amount)      as sales_amount,
//                      sum(decode(dt.debit_credit_indicator,'C',1,0))
//                                                        as credits_count,
//                      sum(decode(dt.debit_credit_indicator,'C',1,0)
//                          * dt.transaction_amount)      as credits_amount,
//                      sum(case when dt.transaction_amount >= 1000 then 1 else 0 end
//                          * decode(dt.debit_credit_indicator,'C',0,1)
//                          * dt.transaction_amount)      as lt_sales_amount,
//                      sum(case when nvl(dt.foreign_card_indicator,'N') in ('F','Y') then 1 else 0 end)
//                                                        as foreign_count,
//                      sum(case when nvl(dt.foreign_card_indicator,'N') in ('F','Y') then 1 else 0 end
//                          * dt.transaction_amount)      as foreign_amount,
//                      sum(dt.ic_expense)                as ic_exp                                                  
//            from      daily_detail_file_dt      dt,
//                      trident_profile           tp
//            where     dt.merchant_account_number = :MerchantId
//                      and dt.batch_date between :MonthBeginDate and :MonthEndDate
//                      and dt.load_filename in
//                      (          
//                        select  ds.data_source
//                        from    mbs_daily_summary   ds
//                        where   ds.me_load_file_id = :LoadFileId
//                                and ds.merchant_number = :MerchantId
//                                and ds.activity_date between :MonthBeginDate-30 and :MonthEndDate+7
//                      )
//                      and dt.reject_reason is null
//                      and tp.merchant_number = dt.merchant_account_number
//                      and tp.catid = dt.merchant_id
//            group by tp.terminal_id,dt.card_type
//            order by tp.terminal_id,dt.card_type        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    /*+ \n                        ordered\n                        index(dt idx_ddf_dt_bdate_merch_acct) \n                        no_query_transformation\n                    */\n                    tp.terminal_id                    as tid,\n                    substr(tp.terminal_id,-4)         as term_num,\n                    dt.card_type                      as plan_type, \n                    case\n                      when dt.card_type in ('VS','VB','VD') then 'VS'\n                      when dt.card_type in ('MC','MB','MD') then 'MC'\n                      when dt.card_type in ('DS','JC','DN') then 'DS'\n                      else dt.card_type\n                    end                               as card_type, \n                    count(1)                          as total_count,\n                    sum(decode(dt.debit_credit_indicator,'C',0,1))\n                                                      as sales_count,\n                    sum(decode(dt.debit_credit_indicator,'C',0,1)\n                        * dt.transaction_amount)      as sales_amount,\n                    sum(decode(dt.debit_credit_indicator,'C',1,0))\n                                                      as credits_count,\n                    sum(decode(dt.debit_credit_indicator,'C',1,0)\n                        * dt.transaction_amount)      as credits_amount,\n                    sum(case when dt.transaction_amount >= 1000 then 1 else 0 end\n                        * decode(dt.debit_credit_indicator,'C',0,1)\n                        * dt.transaction_amount)      as lt_sales_amount,\n                    sum(case when nvl(dt.foreign_card_indicator,'N') in ('F','Y') then 1 else 0 end)\n                                                      as foreign_count,\n                    sum(case when nvl(dt.foreign_card_indicator,'N') in ('F','Y') then 1 else 0 end\n                        * dt.transaction_amount)      as foreign_amount,\n                    sum(dt.ic_expense)                as ic_exp                                                  \n          from      daily_detail_file_dt      dt,\n                    trident_profile           tp\n          where     dt.merchant_account_number =  :1  \n                    and dt.batch_date between  :2   and  :3  \n                    and dt.load_filename in\n                    (          \n                      select  ds.data_source\n                      from    mbs_daily_summary   ds\n                      where   ds.me_load_file_id =  :4  \n                              and ds.merchant_number =  :5  \n                              and ds.activity_date between  :6  -30 and  :7  +7\n                    )\n                    and dt.reject_reason is null\n                    and tp.merchant_number = dt.merchant_account_number\n                    and tp.catid = dt.merchant_id\n          group by tp.terminal_id,dt.card_type\n          order by tp.terminal_id,dt.card_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   __sJT_st.setDate(2,MonthBeginDate);
   __sJT_st.setDate(3,MonthEndDate);
   __sJT_st.setLong(4,LoadFileId);
   __sJT_st.setLong(5,MerchantId);
   __sJT_st.setDate(6,MonthBeginDate);
   __sJT_st.setDate(7,MonthEndDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:656^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          tid = resultSet.getString("tid");
          
          // store the fees due when the tid changes
          if ( lastTid != null && !lastTid.equals(tid) )
          {
            /*@lineinfo:generated-code*//*@lineinfo:666^13*/

//  ************************************************************
//  #sql [Ctx] { update  mbs_tid_summary   
//                set     processing_fees     = (processing_fees      + :procFees),
//                        association_fees    = (association_fees     + :assocFees),
//                        assessment_expense  = (assessment_expense   + :assessment),
//                        interchange_expense = (interchange_expense  + :icExp)
//                where   terminal_id = :lastTid
//                        and active_date = :ActiveDate
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mbs_tid_summary   \n              set     processing_fees     = (processing_fees      +  :1  ),\n                      association_fees    = (association_fees     +  :2  ),\n                      assessment_expense  = (assessment_expense   +  :3  ),\n                      interchange_expense = (interchange_expense  +  :4  )\n              where   terminal_id =  :5  \n                      and active_date =  :6 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,procFees);
   __sJT_st.setDouble(2,assocFees);
   __sJT_st.setDouble(3,assessment);
   __sJT_st.setDouble(4,icExp);
   __sJT_st.setString(5,lastTid);
   __sJT_st.setDate(6,ActiveDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:675^13*/
            
            // reset the totals and store last tid
            procFees    = 0.0;
            assocFees   = 0.0;
            assessment  = 0.0;
            icExp       = 0.0;
          }
          
          procFees    += extractData(resultSet,discountColumnNames  ,"plan_type",null);
          procFees    += extractData(resultSet,procFeeColumnNames   ,"card_type",null);
          assocFees   += extractData(resultSet,feeColumnNames       ,"card_type",null);
          assessment  += extractData(resultSet,assessmentColumnNames,"card_type",null);
          icExp       += resultSet.getDouble("ic_exp");
          
          MinTermNum  = Math.min(MinTermNum,resultSet.getInt("term_num"));
          lastTid     = tid;
        }
        resultSet.close();
        it.close();
        
        /*@lineinfo:generated-code*//*@lineinfo:696^9*/

//  ************************************************************
//  #sql [Ctx] { update  mbs_tid_summary   
//            set     processing_fees     = (processing_fees      + :procFees),
//                    association_fees    = (association_fees     + :assocFees),
//                    assessment_expense  = (assessment_expense   + :assessment),
//                    interchange_expense = (interchange_expense  + :icExp)
//            where   terminal_id = :tid
//                    and active_date = :ActiveDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mbs_tid_summary   \n          set     processing_fees     = (processing_fees      +  :1  ),\n                  association_fees    = (association_fees     +  :2  ),\n                  assessment_expense  = (assessment_expense   +  :3  ),\n                  interchange_expense = (interchange_expense  +  :4  )\n          where   terminal_id =  :5  \n                  and active_date =  :6 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,procFees);
   __sJT_st.setDouble(2,assocFees);
   __sJT_st.setDouble(3,assessment);
   __sJT_st.setDouble(4,icExp);
   __sJT_st.setString(5,tid);
   __sJT_st.setDate(6,ActiveDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:705^9*/
      }
      catch( Exception e )
      {
        logEntry("loadPL(" + MerchantId + "," + ActiveDate + ")",e.toString());
      }
      finally
      {
        try{ it.close(); } catch( Exception ee ) {}
      }
    }
    
    private void loadST()
    {
      ResultSetIterator       it                    = null;
      String                  lastTid               = null;
      HashMap                 miscColumnNames       = null;
      double                  miscFees              = 0.0;
      ResultSet               resultSet             = null;
      String                  tid                   = null;
      
      try
      {
        miscColumnNames = 
          new HashMap()
          {
            {
              put(  "8"    ,"batch_count/none"        );
            }
          };
          
        /*@lineinfo:generated-code*//*@lineinfo:736^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    /*+ 
//                          ordered
//                          index(dt idx_ddf_dt_bdate_merch_acct) 
//                          no_query_transformation
//                      */
//                      tp.terminal_id                    as tid,
//                      substr(tp.terminal_id,-4)         as term_num,
//                      count(distinct dt.batch_number)   as batch_count
//            from      daily_detail_file_dt      dt,
//                      trident_profile           tp
//            where     dt.merchant_account_number = :MerchantId
//                      and dt.batch_date between :MonthBeginDate and :MonthEndDate
//                      and dt.load_filename in
//                      (          
//                        select  ds.data_source
//                        from    mbs_daily_summary   ds
//                        where   ds.me_load_file_id = :LoadFileId
//                                and ds.merchant_number = :MerchantId
//                                and ds.activity_date between :MonthBeginDate-30 and :MonthEndDate+7
//                      )
//                      and dt.reject_reason is null
//                      and tp.merchant_number = dt.merchant_account_number
//                      and tp.catid = dt.merchant_id
//            group by tp.terminal_id
//            order by tp.terminal_id       
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    /*+ \n                        ordered\n                        index(dt idx_ddf_dt_bdate_merch_acct) \n                        no_query_transformation\n                    */\n                    tp.terminal_id                    as tid,\n                    substr(tp.terminal_id,-4)         as term_num,\n                    count(distinct dt.batch_number)   as batch_count\n          from      daily_detail_file_dt      dt,\n                    trident_profile           tp\n          where     dt.merchant_account_number =  :1  \n                    and dt.batch_date between  :2   and  :3  \n                    and dt.load_filename in\n                    (          \n                      select  ds.data_source\n                      from    mbs_daily_summary   ds\n                      where   ds.me_load_file_id =  :4  \n                              and ds.merchant_number =  :5  \n                              and ds.activity_date between  :6  -30 and  :7  +7\n                    )\n                    and dt.reject_reason is null\n                    and tp.merchant_number = dt.merchant_account_number\n                    and tp.catid = dt.merchant_id\n          group by tp.terminal_id\n          order by tp.terminal_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,MerchantId);
   __sJT_st.setDate(2,MonthBeginDate);
   __sJT_st.setDate(3,MonthEndDate);
   __sJT_st.setLong(4,LoadFileId);
   __sJT_st.setLong(5,MerchantId);
   __sJT_st.setDate(6,MonthBeginDate);
   __sJT_st.setDate(7,MonthEndDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:763^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          tid = resultSet.getString("tid");
          
          // store the fees due when the tid changes
          if ( lastTid != null && !lastTid.equals(tid) )
          {
            /*@lineinfo:generated-code*//*@lineinfo:773^13*/

//  ************************************************************
//  #sql [Ctx] { update  mbs_tid_summary   
//                set     misc_fees = (misc_fees + :miscFees)
//                where   terminal_id = :lastTid
//                        and active_date = :ActiveDate
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mbs_tid_summary   \n              set     misc_fees = (misc_fees +  :1  )\n              where   terminal_id =  :2  \n                      and active_date =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,miscFees);
   __sJT_st.setString(2,lastTid);
   __sJT_st.setDate(3,ActiveDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:779^13*/
            
            // reset the totals and store last tid
            miscFees    = 0.0;
          }
          
          miscFees    += extractData(resultSet,miscColumnNames,null,null);
          
          MinTermNum  = Math.min(MinTermNum,resultSet.getInt("term_num"));
          lastTid     = tid;
        }
        resultSet.close();
        it.close();
        
        /*@lineinfo:generated-code*//*@lineinfo:793^9*/

//  ************************************************************
//  #sql [Ctx] { update  mbs_tid_summary   
//            set     misc_fees = (misc_fees + :miscFees)
//            where   terminal_id = :tid
//                    and active_date = :ActiveDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mbs_tid_summary   \n          set     misc_fees = (misc_fees +  :1  )\n          where   terminal_id =  :2  \n                  and active_date =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,miscFees);
   __sJT_st.setString(2,tid);
   __sJT_st.setDate(3,ActiveDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:799^9*/
      }
      catch( Exception e )
      {
        logEntry("loadST(" + MerchantId + "," + ActiveDate + ")",e.toString());
      }
      finally
      {
        try{ it.close(); } catch( Exception ee ) {}
      }
    }
  }
  
  protected boolean               ForceDelete     = false;
  
  public MerchProfLoader()
  {
  }
  
  protected boolean addMeByTid( String loadFilename )
  {
    ResultSetIterator   it              = null;
    long                loadFileId      = 0L;
    ThreadPool          pool            = null;
    int                 recCount        = 0;
    Vector              recsToProcess   = new Vector();
    ResultSet           resultSet       = null;
    boolean             retVal          = false;
    int                 totalRecCount   = 0;
    
    try
    {
      pool = new ThreadPool(7);
      
      if ( loadFilename != null ) 
      {
        /*@lineinfo:generated-code*//*@lineinfo:835^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  gn.hh_merchant_number      as merchant_number,
//                    gn.hh_active_date          as active_date,
//                    gn.hh_active_date          as month_begin_date,
//                    gn.hh_curr_date            as month_end_date
//            from    monthly_extract_gn      gn
//            where   gn.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                    and gn.hh_merchant_number in 
//                    (
//                      select  gm.merchant_number
//                      from    organization      o,
//                              group_merchant    gm
//                      where   o.org_group = 3941400168
//                              and gm.org_num = o.org_num
//                    )
//            order by gn.hh_merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  gn.hh_merchant_number      as merchant_number,\n                  gn.hh_active_date          as active_date,\n                  gn.hh_active_date          as month_begin_date,\n                  gn.hh_curr_date            as month_end_date\n          from    monthly_extract_gn      gn\n          where   gn.load_file_id = load_filename_to_load_file_id( :1  )\n                  and gn.hh_merchant_number in \n                  (\n                    select  gm.merchant_number\n                    from    organization      o,\n                            group_merchant    gm\n                    where   o.org_group = 3941400168\n                            and gm.org_num = o.org_num\n                  )\n          order by gn.hh_merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:852^9*/
      }
      else    // use month to date data
      {
        /*@lineinfo:generated-code*//*@lineinfo:856^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  gm.merchant_number         as merchant_number,
//                    trunc(sysdate,'month')     as active_date,
//                    trunc(sysdate,'month')     as month_begin_date,
//                    trunc(last_day(sysdate))   as month_end_date
//            from    organization      o,
//                    group_merchant    gm
//            where   o.org_group = 3941400168
//                    and gm.org_num = o.org_num
//            order by gm.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  gm.merchant_number         as merchant_number,\n                  trunc(sysdate,'month')     as active_date,\n                  trunc(sysdate,'month')     as month_begin_date,\n                  trunc(last_day(sysdate))   as month_end_date\n          from    organization      o,\n                  group_merchant    gm\n          where   o.org_group = 3941400168\n                  and gm.org_num = o.org_num\n          order by gm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.MerchProfLoader",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:867^9*/
      }
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        recsToProcess.addElement( new MeByTid(resultSet) );
      }
      resultSet.close();
      it.close();
      
      totalRecCount = recsToProcess.size();
      
      for ( int i = 0; i < recsToProcess.size(); ++i )
      {
        MeByTid meByTid = (MeByTid)recsToProcess.elementAt(i);
        
        Thread thread = pool.getThread( new LoadMeDataByTidWorker(loadFilename,meByTid) );
        thread.start();
        
        ++recCount;
        System.out.print("Loading " + meByTid.MerchantId + " (" + recCount + " - " + NumberFormatter.getPercentString(((double)recCount/(double)totalRecCount),3) + ")            \r");
      }
      pool.waitForAllThreads();
      
      System.out.println();
      retVal = true;
    }
    catch( Exception e )
    {
      System.out.println("addMeByTid(" + loadFilename + ") - " + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ){}
    }
    return( retVal );
  }
  
  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    int                   ctype             = ContractTypes.CONTRACT_SOURCE_INVALID;
    boolean               fileProcessed     = false;
    ResultSetIterator     it                = null;
    int                   itemCount         = 0;
    String                loadFilename      = null;
    long                  nodeId            = 0L;
    int                   procType          = -1;
    long                  recId             = 0L;
    ResultSet             resultSet         = null;
    int                   sequenceId        = 0;
    
    try
    {
      // get an automated timeout exempt connection
      connect(true);
      
      /*@lineinfo:generated-code*//*@lineinfo:929^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(load_filename)  
//          from    merch_prof_process
//          where   process_sequence = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(load_filename)   \n        from    merch_prof_process\n        where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:934^7*/
      
      if ( itemCount > 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:938^9*/

//  ************************************************************
//  #sql [Ctx] { select  merch_prof_process_sequence.nextval 
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_prof_process_sequence.nextval  \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sequenceId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:942^9*/
      
        // PT_MOJAVE_SUMMARY is handled by a separate timed event - com.mes.startup.MojaveSummaryEvent
        /*@lineinfo:generated-code*//*@lineinfo:945^9*/

//  ************************************************************
//  #sql [Ctx] { update  merch_prof_process
//            set     process_sequence = :sequenceId
//            where   process_sequence = 0
//                    and process_type != :PT_MOJAVE_SUMMARY
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merch_prof_process\n          set     process_sequence =  :1  \n          where   process_sequence = 0\n                  and process_type !=  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sequenceId);
   __sJT_st.setInt(2,PT_MOJAVE_SUMMARY);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:951^9*/
      
        /*@lineinfo:generated-code*//*@lineinfo:953^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  rec_id                      as rec_id,
//                    load_filename               as load_filename,
//                    process_type                as process_type,
//                    nvl(hierarchy_node,0)       as hierarchy_node,
//                    nvl(contract_type,-1)       as contract_type
//            from    merch_prof_process 
//            where   process_sequence = :sequenceId
//            order by process_type,rec_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rec_id                      as rec_id,\n                  load_filename               as load_filename,\n                  process_type                as process_type,\n                  nvl(hierarchy_node,0)       as hierarchy_node,\n                  nvl(contract_type,-1)       as contract_type\n          from    merch_prof_process \n          where   process_sequence =  :1  \n          order by process_type,rec_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sequenceId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:963^9*/
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          loadFilename  = resultSet.getString("load_filename");
          procType      = resultSet.getInt("process_type");
          recId         = resultSet.getLong("rec_id");
          nodeId        = resultSet.getLong("hierarchy_node");
          ctype         = resultSet.getInt("contract_type");
        
          recordTimestampBegin( recId );
          
          fileProcessed = true;     // default 
        
          switch( procType )
          {
            case PT_EXTRACT_SUMMARY:
              fileProcessed = loadExtractSummary(loadFilename);
              break;
          
            case PT_CONTRACT_SUMMARY:
              fileProcessed = loadContractSummary(loadFilename,recId,nodeId,ctype);
              break;
              
            case PT_EQUIP_SUMMARY:
              fileProcessed = loadEquipSummary(loadFilename,nodeId);
              break;
              
            case PT_VERISIGN_OVER_AUTH_BILLING:
              fileProcessed = loadVerisignOverAuths(loadFilename,nodeId);
              break;
              
            case PT_DEBIT_COUNT_SUMMARY:
              fileProcessed = (loadFilename.startsWith("mbs_ext") ? true : loadDebitCountSummary(loadFilename,recId));
              break;
              
            case PT_DISCOVER_SALES_SUMMARY:
              loadDiscoverExtractSummary(loadFilename);
              break;
              
            case PT_CERTEGY_EXT_SUMMARY:
              loadCertegyExtractSummary(loadFilename);
              break;

            case PT_TRANSCOM_EXT_SUMMARY:
              fileProcessed = loadTranscomExtractSummary(loadFilename);
              break;
              
            case PT_DISCOVER_EVERGREEN_SUMMARY:   // evergreen contract loading
              fileProcessed = loadDiscoverEvergreenSummary(loadFilename);
              break;
              
            case PT_EPICOR_AP:
              fileProcessed = loadEpicorAP(loadFilename,nodeId);
              break;
              
            case PT_HIERARCHY_SNAPSHOT:
              fileProcessed = storeHierarchy(loadFilename);
              break;
              
            case PT_TID_SUMMARY:
              fileProcessed = addMeByTid(loadFilename);
              break;
              
//@            case PT_ATTRITION_IMPACT:
//@              loadAttritionImpactSummary(loadFilename);
//@              break;
            
            default:      // skip
              continue;
          }
          if ( fileProcessed == true )
          {
            recordTimestampEnd( recId );
            markStageComplete(loadFilename,procType);
          }            
        }
        resultSet.close();
        it.close();        
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
      cleanUp();
    }
    return( true );
  }
  
  private boolean loadCertegyExtractSummary( String loadFilename )
  {
    boolean         fileProcessed       = false;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1064^7*/

//  ************************************************************
//  #sql [Ctx] { call load_certegy_extract_summary( :loadFilename )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_certegy_extract_summary(  :1   )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1067^7*/
      fileProcessed = true;
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadCertegyExtractSummary()",e.toString());
    }
    finally
    {
    }
    return( fileProcessed );
  }
  
  private boolean loadContractSummary( String loadFilename, long recId, long maskNodeId, int contractSource )
  {
    Date                        activeDate            = null;
    int                         bankNumber            = 0;  
    BankContractBean            contract              = null;
    BankContractDataBean        contractDataBean      = null;
    double[][]                  contractTotals        = null;
    double                      excludedRental        = 0.0;
    double                      excludedSales         = 0.0;
    boolean                     fileProcessed         = false;
    boolean                     hasCashAdvRef         = false;
    boolean[]                   hasContract           = null;
    int                         insertCount           = 0;
    ResultSetIterator           it                    = null;
    long                        loadSec               = 0L;
    long                        loadFileId            = 0L;
    long                        merchantId            = 0L;
    double                      mesOnlyChargeRecs     = 0.0;
    double[]                    mesOnlyRevenue        = null;
    long                        nodeId                = 0L;
    long                        partnershipNode       = 0L;
    double                      partnershipRate       = 0.0;
    boolean                     processFile           = true;
    double                      rate                  = 0.0;
    int                         recCount              = 0;
    ResultSet                   resultSet             = null;
    double                      revenue               = 0.0;
    double                      achDiscLiability      = 0.0;
    double                      achIncome             = 0.0; 
    double                      expAchPartner         = 0.0;
    double                      expAchPS              = 0.0;
    int                         acTranCount           = 0;
    int                         acReturnCount         = 0;
    int                         acAuthCount           = 0;
    double                      acTranVol             = 0.0;
    double                      acReturnVol           = 0.0;
    double                      acAuthVol             = 0.0;
    
    
    try
    {
      contract = new BankContractBean();
      contract.connect(true);
      
      // create a bank contract data bean to process
      // the query result rows.
      contractDataBean = new BankContractDataBean();
      contractDataBean.connect(true);
      
      contractTotals = new double[ContractTypes.CONTRACT_SOURCE_COUNT][BankContractBean.BET_GROUP_COUNT];
      hasContract    = new boolean[ContractTypes.CONTRACT_SOURCE_COUNT];
      mesOnlyRevenue = new double[BankContractBean.BET_GROUP_COUNT_DEFAULT];
      
      setConsoleTrace(true);//@ debug trace
      trace("Loading bank number and active date for " + loadFilename );
      loadFileId = loadFilenameToLoadFileId(loadFilename);
      
      /*@lineinfo:generated-code*//*@lineinfo:1137^7*/

//  ************************************************************
//  #sql [Ctx] { select  distinct bank_number, active_date 
//          from    monthly_extract_summary sm
//          where   sm.load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct bank_number, active_date  \n        from    monthly_extract_summary sm\n        where   sm.load_file_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   activeDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1142^7*/
      
      // wait until the debit bet data and the equipment has been
      // loaded so that we have counts by debit network and equipment expense.
      // these are necessary to handle split debit pricing
      // and equipment expense when processing the contracts
      /*@lineinfo:generated-code*//*@lineinfo:1148^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(load_file_id) 
//          from    merch_prof_load_progress lp
//          where   lp.load_file_id = :loadFileId and
//                  nvl(lp.debit_summary_loaded,'N') = 'Y' and
//                  nvl(lp.equip_cost_loaded,'N') = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(load_file_id)  \n        from    merch_prof_load_progress lp\n        where   lp.load_file_id =  :1   and\n                nvl(lp.debit_summary_loaded,'N') = 'Y' and\n                nvl(lp.equip_cost_loaded,'N') = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1155^7*/
      processFile = (recCount > 0);
      
      if ( processFile == false )
      {
        // reset the entry to try again later
        /*@lineinfo:generated-code*//*@lineinfo:1161^9*/

//  ************************************************************
//  #sql [Ctx] { update merch_prof_process
//            set process_sequence = 0,
//                process_begin_date = null,
//                process_elapsed = null
//            where rec_id = :recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update merch_prof_process\n          set process_sequence = 0,\n              process_begin_date = null,\n              process_elapsed = null\n          where rec_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1168^9*/
      }
      else
      {
        // reset the record counter
        recCount = 0;
        
        String whereClause = "";
        if ( maskNodeId != 0 )
        {
          whereClause = " exists (select gm.merchant_number from organization o, group_merchant gm where o.org_group = " + maskNodeId + " and gm.org_num = o.org_num and gm.merchant_number = sm.merchant_number) and ";
        }
        
        trace("Selecting records to process");
        /*@lineinfo:generated-code*//*@lineinfo:1182^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                        ordered
//                        index (sm idx_mon_ext_sum_load_file_id) 
//                     */
//                    sm.hh_load_sec                          as hh_load_sec,
//                    sm.merchant_number                      as merchant_number,
//                    sm.active_date                          as active_date,
//                    decode(sm.cash_advance_account,'Y',1,0) as cash_advance,
//                    decode(sm.mes_inventory,'Y',1,0)        as mes_inventory,
//                    decode(sm.moto_merchant,'Y',1,0)        as moto_merchant,
//                    decode(nvl(rm.cat_count,0),0,0,1)       as mes_only_revenue,
//                    sm.product_code                         as product_code,
//                    /* RS changes for ACH PS - exclude ACH income */
//                    (( sm.tot_inc_interchange +             
//                      sm.tot_inc_discount ) - sm.tot_inc_discount_ach )   as disc_ic_inc,
//                      
//                    sm.tot_inc_authorization                as auth_inc,
//                    sm.tot_inc_capture                      as capture_inc,
//                    sm.tot_inc_debit                        as debit_inc,
//                    sm.tot_inc_ind_plans                    as plan_inc,
//                    ( sm.tot_inc_sys_generated -
//                     ( sm.equip_rental_income + sm.equip_sales_income ))     as sys_gen_inc,
//                    sm.equip_rental_income                  as equip_rental_inc,
//                    sm.equip_sales_income                   as equip_sales_inc,
//                    /* RS changes for ACH PS - include ACH income  */
//                    nvl(sm.tot_inc_discount_ach,0) + nvl(sm.tot_inc_ach,0)  
//                                                            as ach_inc,
//                    nvl(sm.equip_rental_base_cost,0)        as equip_rental_base_cost,
//                    nvl(sm.equip_sales_base_cost,0)         as equip_sales_base_cost,
//                    nvl(sm.equip_transfer_cost,0)           as equip_transfer_cost,
//                    sm.disc_ic_dce_adj_amount               as disc_ic_adj_amount,
//                    sm.fee_dce_adj_amount                   as fee_adj_amount,
//                    nvl(sm.interchange_expense,0)           as ic_exp,
//                    sm.vmc_assessment_expense               as assessment,
//                    sm.cash_adv_interchange_expense         as cash_adv_interchange_exp,
//                    sm.visa_vol_count                       as visa_vol_count,
//                    sm.visa_vol_amount                      as visa_vol_amount,
//                    sm.visa_sales_count                     as visa_sales_count,
//                    sm.visa_sales_amount                    as visa_sales_amount,
//                    sm.visa_credits_count                   as visa_credits_count,
//                    sm.visa_credits_amount                  as visa_credits_amount,
//                    sm.mc_vol_count                         as mc_vol_count,
//                    sm.mc_vol_amount                        as mc_vol_amount,
//                    sm.mc_sales_count                       as mc_sales_count,
//                    sm.mc_sales_amount                      as mc_sales_amount,
//                    sm.mc_credits_count                     as mc_credits_count,
//                    sm.mc_credits_amount                    as mc_credits_amount,
//                    sm.mc_cross_border_vol_count            as mc_cross_border_vol_count,
//                    sm.mc_cross_border_vol_amount           as mc_cross_border_vol_amount,
//                    sm.mc_cross_border_sales_count          as mc_cross_border_sales_count,
//                    sm.mc_cross_border_sales_amount         as mc_cross_border_sales_amount,
//                    sm.mc_cross_border_credits_count        as mc_cross_border_credits_count,
//                    sm.mc_cross_border_credits_amount       as mc_cross_border_credits_amount,
//                    ( sm.mc_cross_border_sales_amount +
//                      sm.mc_cross_border_credits_amount )   as mc_cross_border_hash_amount,
//                    sm.mc_foreign_std_vol_count             as mc_foreign_std_vol_count,
//                    sm.mc_foreign_std_vol_amount            as mc_foreign_std_vol_amount,
//                    sm.mc_foreign_std_sales_count           as mc_foreign_std_sales_count,
//                    sm.mc_foreign_std_sales_amount          as mc_foreign_std_sales_amount,
//                    sm.mc_foreign_std_credits_count         as mc_foreign_std_credits_count,
//                    sm.mc_foreign_std_credits_amount        as mc_foreign_std_credits_amount,
//                    ( sm.mc_foreign_std_sales_amount +
//                      sm.mc_foreign_std_credits_amount )    as mc_foreign_std_hash_amount,
//                    sm.mc_foreign_elec_vol_count            as mc_foreign_elec_vol_count,
//                    sm.mc_foreign_elec_vol_amount           as mc_foreign_elec_vol_amount,
//                    sm.mc_foreign_elec_sales_count          as mc_foreign_elec_sales_count,
//                    sm.mc_foreign_elec_sales_amount         as mc_foreign_elec_sales_amount,
//                    sm.mc_foreign_elec_credits_count        as mc_foreign_elec_credits_count,
//                    sm.mc_foreign_elec_credits_amount       as mc_foreign_elec_credits_amount,
//                    ( sm.mc_foreign_elec_sales_amount +
//                      sm.mc_foreign_elec_credits_amount )   as mc_foreign_elec_hash_amount,
//                    ( nvl(sm.mc_vol_count,0) - 
//                      nvl(mc_cross_border_vol_count,0) )    as mc_nabu_vol_count,
//                    ( sm.vmc_sales_count
//                      -- + nvl(sm.amex_ic_sales_count,0)  -- exclude per rory
//                      + nvl(sm.disc_ic_sales_count,0) )     as risk_sales_count,  
//                    ( sm.vmc_sales_amount
//                      -- + nvl(sm.amex_ic_sales_amount,0) -- exclude per rory
//                      + nvl(sm.disc_ic_sales_amount,0) )    as risk_sales_amount,  
//                    ( sm.vmc_vol_count
//                      -- + nvl(sm.amex_ic_vol_count,0)    -- exclude per rory
//                      + nvl(sm.disc_ic_vol_count,0) )       as processing_vol_count,  
//                    ( sm.vmc_vol_amount
//                      -- + nvl(sm.amex_ic_vol_amount,0)   -- exclude per rory
//                      + nvl(sm.disc_ic_vol_amount,0) )      as processing_vol_amount,  
//                    ( sm.vmc_sales_count
//                      -- + nvl(sm.amex_ic_sales_count,0)  -- exclude per rory
//                      + nvl(sm.disc_ic_sales_count,0) )     as processing_sales_count,
//                    ( sm.vmc_sales_amount
//                      -- + nvl(sm.amex_ic_sales_amount,0) -- exclude per rory
//                      + nvl(sm.disc_ic_sales_amount,0) )    as processing_sales_amount,  
//                    sm.vmc_vol_count                        as vmc_vol_count,
//                    sm.vmc_vol_amount                       as vmc_vol_amount,  
//                    sm.vmc_sales_count                      as vmc_sales_count,
//                    sm.vmc_sales_amount                     as vmc_sales_amount,
//                    sm.vmc_credits_count                    as vmc_credits_count,
//                    sm.vmc_credits_amount                   as vmc_credits_amount,
//                    nvl(sm.amex_ic_vol_count,0)             as am_ic_vol_count,
//                    nvl(sm.amex_ic_vol_amount,0)            as am_ic_vol_amount,  
//                    nvl(sm.amex_ic_sales_count,0)           as am_ic_sales_count,
//                    nvl(sm.amex_ic_sales_amount,0)          as am_ic_sales_amount,  
//                    nvl(sm.amex_ic_credits_count,0)         as am_ic_credits_count,
//                    nvl(sm.amex_ic_credits_amount,0)        as am_ic_credits_amount,  
//                    nvl(sm.disc_ic_vol_count,0)             as ds_ic_vol_count,
//                    nvl(sm.disc_ic_vol_amount,0)            as ds_ic_vol_amount,  
//                    nvl(sm.disc_ic_sales_count,0)           as ds_ic_sales_count,
//                    nvl(sm.disc_ic_sales_amount,0)          as ds_ic_sales_amount,  
//                    nvl(sm.disc_ic_credits_count,0)         as ds_ic_credits_count,
//                    nvl(sm.disc_ic_credits_amount,0)        as ds_ic_credits_amount,  
//                    nvl(sm.amex_vol_count,0)                as amex_vol_count,
//                    nvl(sm.amex_vol_amount,0)               as amex_vol_amount,  
//                    nvl(sm.amex_sales_count,0)              as amex_sales_count,
//                    nvl(sm.amex_sales_amount,0)             as amex_sales_amount,  
//                    nvl(sm.amex_credits_count,0)            as amex_credits_count,
//                    nvl(sm.amex_credits_amount,0)           as amex_credits_amount,  
//                    nvl(sm.dinr_vol_count,0)                as dinr_vol_count,
//                    nvl(sm.dinr_vol_amount,0)               as dinr_vol_amount,  
//                    nvl(sm.dinr_sales_count,0)              as dinr_sales_count,
//                    nvl(sm.dinr_sales_amount,0)             as dinr_sales_amount,  
//                    nvl(sm.dinr_credits_count,0)            as dinr_credits_count,
//                    nvl(sm.dinr_credits_amount,0)           as dinr_credits_amount,  
//                    nvl(sm.disc_vol_count,0)                as disc_vol_count,
//                    nvl(sm.disc_vol_amount,0)               as disc_vol_amount,  
//                    nvl(sm.disc_sales_count,0)              as disc_sales_count,
//                    nvl(sm.disc_sales_amount,0)             as disc_sales_amount,  
//                    nvl(sm.disc_credits_count,0)            as disc_credits_count,
//                    nvl(sm.disc_credits_amount,0)           as disc_credits_amount,  
//                    nvl(sm.jcb_vol_count,0)                 as jcb_vol_count,
//                    nvl(sm.jcb_vol_amount,0)                as jcb_vol_amount, 
//                    nvl(sm.jcb_sales_count,0)               as jcb_sales_count,
//                    nvl(sm.jcb_sales_amount,0)              as jcb_sales_amount,  
//                    nvl(sm.jcb_credits_count,0)             as jcb_credits_count,
//                    nvl(sm.jcb_credits_amount,0)            as jcb_credits_amount,  
//                    nvl(sm.pl1_vol_count,0)                 as pl1_vol_count,
//                    nvl(sm.pl1_vol_amount,0)                as pl1_vol_amount, 
//                    nvl(sm.pl1_sales_count,0)               as pl1_sales_count,
//                    nvl(sm.pl1_sales_amount,0)              as pl1_sales_amount,  
//                    nvl(sm.pl1_credits_count,0)             as pl1_credits_count,
//                    nvl(sm.pl1_credits_amount,0)            as pl1_credits_amount,  
//                    nvl(sm.pl2_vol_count,0)                 as pl2_vol_count,
//                    nvl(sm.pl2_vol_amount,0)                as pl2_vol_amount, 
//                    nvl(sm.pl2_sales_count,0)               as pl2_sales_count,
//                    nvl(sm.pl2_sales_amount,0)              as pl2_sales_amount,  
//                    nvl(sm.pl2_credits_count,0)             as pl2_credits_count,
//                    nvl(sm.pl2_credits_amount,0)            as pl2_credits_amount,  
//                    nvl(sm.pl3_vol_count,0)                 as pl3_vol_count,
//                    nvl(sm.pl3_vol_amount,0)                as pl3_vol_amount, 
//                    nvl(sm.pl3_sales_count,0)               as pl3_sales_count,
//                    nvl(sm.pl3_sales_amount,0)              as pl3_sales_amount,  
//                    nvl(sm.pl3_credits_count,0)             as pl3_credits_count,
//                    nvl(sm.pl3_credits_amount,0)            as pl3_credits_amount,  
//                    nvl(sm.debit_vol_count,0)               as debit_vol_count,
//                    nvl(sm.debit_vol_amount,0)              as debit_vol_amount,  
//                    nvl(sm.debit_sales_count,0)             as debit_sales_count,
//                    nvl(sm.debit_sales_amount,0)            as debit_sales_amount,  
//                    nvl(sm.debit_credits_count,0)           as debit_credits_count,
//                    nvl(sm.debit_credits_amount,0)          as debit_credits_amount,  
//                    nvl(sm.ebt_vol_count,0)                 as ebt_vol_count,
//                    nvl(sm.achp_vol_count,0)                as achp_vol_count,
//                    nvl(sm.achp_return_count,0)             as achp_return_count,
//                    nvl(dn.cat_01_total_count,0)            as debit_count_cat_01,
//                    nvl(dn.cat_02_total_count,0)            as debit_count_cat_02,
//                    nvl(dn.cat_03_total_count,0)            as debit_count_cat_03,
//                    nvl(dn.cat_04_total_count,0)            as debit_count_cat_04,
//                    nvl(dn.cat_05_total_count,0)            as debit_count_cat_05,
//                    nvl(dn.cat_06_total_count,0)            as debit_count_cat_06,
//                    nvl(dn.cat_07_total_count,0)            as debit_count_cat_07,
//                    nvl(dn.cat_08_total_count,0)            as debit_count_cat_08,
//                    nvl(dn.cat_09_total_count,0)            as debit_count_cat_09,
//                    nvl(dn.cat_10_total_count,0)            as debit_count_cat_10,
//                    sm.vmc_auth_count                       as vmc_auth_count,
//                    nvl(sm.visa_auth_count,0)               as visa_auth_count,
//                    nvl(sm.mc_auth_count,0)                 as mc_auth_count,
//                    sm.amex_auth_count                      as amex_auth_count,
//                    sm.dinr_auth_count                      as dinr_auth_count,
//                    sm.disc_auth_count                      as disc_auth_count,
//                    sm.jcb_auth_count                       as jcb_auth_count,
//                    sm.debit_auth_count                     as debit_auth_count,
//                    nvl(sm.auth_count_total,0)              as auth_count_total,
//                    nvl(sm.auth_count_total_ext,0)          as auth_count_total_ext,
//                    nvl(sm.auth_count_total_api,0)          as auth_count_total_api,
//                    nvl(sm.auth_count_total_trident,0)      as auth_count_total_trident,
//                    nvl(sm.vmc_auth_count_trident,0)        as vmc_auth_count_trident,
//                    nvl(sm.amex_auth_count_trident,0)       as amex_auth_count_trident,
//                    nvl(sm.disc_auth_count_trident,0)       as disc_auth_count_trident,
//                    nvl(sm.dinr_auth_count_trident,0)       as dinr_auth_count_trident,
//                    nvl(sm.jcb_auth_count_trident,0)        as jcb_auth_count_trident,
//                    nvl(sm.debit_auth_count_trident,0)      as debit_auth_count_trident,
//                    ( nvl(sm.visa_auth_count,0) +
//                      nvl(dn.cat_03_total_count,0) )        as visa_interlink_auth_count,
//                    ( nvl(sm.disc_auth_count,0) +
//                     ( nvl(sm.debit_auth_count,0) -
//                       nvl(dn.cat_03_total_count,0) ) )     as non_visa_auth_access_count,
//                    sm.chargeback_vol_count                 as chargeback_vol_count,
//                    sm.chargeback_vol_amount                as chargeback_vol_amount,  
//                    sm.incoming_chargeback_count            as incoming_cb_count,
//                    sm.incoming_chargeback_amount           as incoming_cb_amount,
//                    sm.incoming_ft_chargeback_count         as incoming_ft_cb_count,
//                    sm.incoming_ft_chargeback_amount        as incoming_ft_cb_amount,
//                    sm.retrieval_count                      as retrieval_count,
//                    sm.retrieval_amount                     as retrieval_amount,
//                    sm.service_call_count                   as num_service_calls,
//                    sm.cash_advance_vol_count               as cash_advance_vol_count,
//                    sm.cash_advance_vol_amount              as cash_advance_vol_amount,  
//                    /* RS changes for ACH PS Columns added  to calculate partner fees  */
//                    nvl(sm.ach_tran_count,0)                as ach_tran_count,
//                    nvl(sm.ach_tran_amount,0)                as ach_tran_amount,
//                    nvl(sm.ach_return_count,0)              as ach_return_count,
//                    nvl(sm.ach_return_amount,0)              as ach_return_amount,
//                    nvl(sm.ach_unauth_count,0)              as ach_unauth_count,
//                    nvl(sm.ach_unauth_amount,0)              as ach_unauth_amount,
//                    decode( sm.activated_merchant,'Y',1,0 ) as num_activated_merchants,
//                    decode( sm.new_merchant,'Y',1,0 )       as num_new_merchants,
//                    decode( sm.amex_setup,'Y',1,0 )         as num_amex_setups,
//                    decode( sm.discover_setup,'Y',1,0 )     as num_disc_setups,
//                    sm.terminal_rental_count                as num_rentals,
//                    decode( sm.merchant_status,
//                            'C', 0,  -- closed
//                            'D', 0,  -- deleted
//                            'F', 0,  -- fraud
//                             1 )                            as num_active_merchants,
//                    ( nvl(sm.voice_auth_count,0) +
//                      nvl(sm.aru_auth_count,0) )            as dialpay_auth_count,
//                    nvl(sm.avs_count,0)                     as avs_auth_count,
//                    nvl(sm.ach_reject_count,0)              as ach_reject_count,
//                    nvl(sm.ic_correction_count,0)           as ic_correction_count,
//                    nvl(sm.terminal_swap_count,0)           as swap_count,
//                    nvl(sm.terminal_deployment_count,0)     as terminal_deployment_count,
//          		  case when exists(select (1) 
//          		  					from t_hierarchy     th,
//          		  						mif             m,
//          		  						agent_bank_contract     abc
//          		  					where m.merchant_number = sm.merchant_number and
//          		  						m.association_node = th.descendent and
//          		  						th.ancestor = abc.hierarchy_node and
//          		  						:activeDate between abc.valid_date_begin and abc.valid_date_end and
//          		  						billing_element_type = :BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG) 
//            				then sm.debit_network_fees 
//            				else 0 end 							as debit_network_fees,
//            			decode( nvl(sm.print_statement, 'N'),'Y', 1, 0 )    
//  						  									as print_statement_count,
//  					nvl(sm.batch_fee_count, 0)   	
//  															as batch_fee_count
//            from    monthly_extract_summary   sm,
//                    monthly_extract_dn        dn,
//                    (
//                      select  gm.merchant_number            as merchant_number,
//                              count(rmask.income_category)  as cat_count
//                      from    agent_bank_revenue_mask   rmask,
//                              organization              o,
//                              group_merchant            gm
//                      where   rmask.mask_type = 2 and
//                              rmask.charge_rec_prefix is null and
//                              o.org_group = rmask.hierarchy_node and
//                              gm.org_num  = o.org_num
//                      group by gm.merchant_number
//                    )                         rm
//            where   sm.load_file_id = :loadFileId and
//                    sm.active_date = :activeDate and
//                    :whereClause
//                    dn.hh_load_sec(+) = sm.hh_load_sec and
//                    exists
//                    (
//                      select  abc.hierarchy_node
//                      from    agent_bank_contract   abc,
//                              organization          o,
//                              group_merchant        gm
//                      where   o.org_group = abc.hierarchy_node and
//                              gm.org_num = o.org_num and
//                              gm.merchant_number = sm.merchant_number and
//                              :activeDate between abc.valid_date_begin and
//                                                  abc.valid_date_end
//                    ) and
//                    rm.merchant_number(+) = sm.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  /*+ \n                      ordered\n                      index (sm idx_mon_ext_sum_load_file_id) \n                   */\n                  sm.hh_load_sec                          as hh_load_sec,\n                  sm.merchant_number                      as merchant_number,\n                  sm.active_date                          as active_date,\n                  decode(sm.cash_advance_account,'Y',1,0) as cash_advance,\n                  decode(sm.mes_inventory,'Y',1,0)        as mes_inventory,\n                  decode(sm.moto_merchant,'Y',1,0)        as moto_merchant,\n                  decode(nvl(rm.cat_count,0),0,0,1)       as mes_only_revenue,\n                  sm.product_code                         as product_code,\n                  /* RS changes for ACH PS - exclude ACH income */\n                  (( sm.tot_inc_interchange +             \n                    sm.tot_inc_discount ) - sm.tot_inc_discount_ach )   as disc_ic_inc,\n                    \n                  sm.tot_inc_authorization                as auth_inc,\n                  sm.tot_inc_capture                      as capture_inc,\n                  sm.tot_inc_debit                        as debit_inc,\n                  sm.tot_inc_ind_plans                    as plan_inc,\n                  ( sm.tot_inc_sys_generated -\n                   ( sm.equip_rental_income + sm.equip_sales_income ))     as sys_gen_inc,\n                  sm.equip_rental_income                  as equip_rental_inc,\n                  sm.equip_sales_income                   as equip_sales_inc,\n                  /* RS changes for ACH PS - include ACH income  */\n                  nvl(sm.tot_inc_discount_ach,0) + nvl(sm.tot_inc_ach,0)  \n                                                          as ach_inc,\n                  nvl(sm.equip_rental_base_cost,0)        as equip_rental_base_cost,\n                  nvl(sm.equip_sales_base_cost,0)         as equip_sales_base_cost,\n                  nvl(sm.equip_transfer_cost,0)           as equip_transfer_cost,\n                  sm.disc_ic_dce_adj_amount               as disc_ic_adj_amount,\n                  sm.fee_dce_adj_amount                   as fee_adj_amount,\n                  nvl(sm.interchange_expense,0)           as ic_exp,\n                  sm.vmc_assessment_expense               as assessment,\n                  sm.cash_adv_interchange_expense         as cash_adv_interchange_exp,\n                  sm.visa_vol_count                       as visa_vol_count,\n                  sm.visa_vol_amount                      as visa_vol_amount,\n                  sm.visa_sales_count                     as visa_sales_count,\n                  sm.visa_sales_amount                    as visa_sales_amount,\n                  sm.visa_credits_count                   as visa_credits_count,\n                  sm.visa_credits_amount                  as visa_credits_amount,\n                  sm.mc_vol_count                         as mc_vol_count,\n                  sm.mc_vol_amount                        as mc_vol_amount,\n                  sm.mc_sales_count                       as mc_sales_count,\n                  sm.mc_sales_amount                      as mc_sales_amount,\n                  sm.mc_credits_count                     as mc_credits_count,\n                  sm.mc_credits_amount                    as mc_credits_amount,\n                  sm.mc_cross_border_vol_count            as mc_cross_border_vol_count,\n                  sm.mc_cross_border_vol_amount           as mc_cross_border_vol_amount,\n                  sm.mc_cross_border_sales_count          as mc_cross_border_sales_count,\n                  sm.mc_cross_border_sales_amount         as mc_cross_border_sales_amount,\n                  sm.mc_cross_border_credits_count        as mc_cross_border_credits_count,\n                  sm.mc_cross_border_credits_amount       as mc_cross_border_credits_amount,\n                  ( sm.mc_cross_border_sales_amount +\n                    sm.mc_cross_border_credits_amount )   as mc_cross_border_hash_amount,\n                  sm.mc_foreign_std_vol_count             as mc_foreign_std_vol_count,\n                  sm.mc_foreign_std_vol_amount            as mc_foreign_std_vol_amount,\n                  sm.mc_foreign_std_sales_count           as mc_foreign_std_sales_count,\n                  sm.mc_foreign_std_sales_amount          as mc_foreign_std_sales_amount,\n                  sm.mc_foreign_std_credits_count         as mc_foreign_std_credits_count,\n                  sm.mc_foreign_std_credits_amount        as mc_foreign_std_credits_amount,\n                  ( sm.mc_foreign_std_sales_amount +\n                    sm.mc_foreign_std_credits_amount )    as mc_foreign_std_hash_amount,\n                  sm.mc_foreign_elec_vol_count            as mc_foreign_elec_vol_count,\n                  sm.mc_foreign_elec_vol_amount           as mc_foreign_elec_vol_amount,\n                  sm.mc_foreign_elec_sales_count          as mc_foreign_elec_sales_count,\n                  sm.mc_foreign_elec_sales_amount         as mc_foreign_elec_sales_amount,\n                  sm.mc_foreign_elec_credits_count        as mc_foreign_elec_credits_count,\n                  sm.mc_foreign_elec_credits_amount       as mc_foreign_elec_credits_amount,\n                  ( sm.mc_foreign_elec_sales_amount +\n                    sm.mc_foreign_elec_credits_amount )   as mc_foreign_elec_hash_amount,\n                  ( nvl(sm.mc_vol_count,0) - \n                    nvl(mc_cross_border_vol_count,0) )    as mc_nabu_vol_count,\n                  ( sm.vmc_sales_count\n                    -- + nvl(sm.amex_ic_sales_count,0)  -- exclude per rory\n                    + nvl(sm.disc_ic_sales_count,0) )     as risk_sales_count,  \n                  ( sm.vmc_sales_amount\n                    -- + nvl(sm.amex_ic_sales_amount,0) -- exclude per rory\n                    + nvl(sm.disc_ic_sales_amount,0) )    as risk_sales_amount,  \n                  ( sm.vmc_vol_count\n                    -- + nvl(sm.amex_ic_vol_count,0)    -- exclude per rory\n                    + nvl(sm.disc_ic_vol_count,0) )       as processing_vol_count,  \n                  ( sm.vmc_vol_amount\n                    -- + nvl(sm.amex_ic_vol_amount,0)   -- exclude per rory\n                    + nvl(sm.disc_ic_vol_amount,0) )      as processing_vol_amount,  \n                  ( sm.vmc_sales_count\n                    -- + nvl(sm.amex_ic_sales_count,0)  -- exclude per rory\n                    + nvl(sm.disc_ic_sales_count,0) )     as processing_sales_count,\n                  ( sm.vmc_sales_amount\n                    -- + nvl(sm.amex_ic_sales_amount,0) -- exclude per rory\n                    + nvl(sm.disc_ic_sales_amount,0) )    as processing_sales_amount,  \n                  sm.vmc_vol_count                        as vmc_vol_count,\n                  sm.vmc_vol_amount                       as vmc_vol_amount,  \n                  sm.vmc_sales_count                      as vmc_sales_count,\n                  sm.vmc_sales_amount                     as vmc_sales_amount,\n                  sm.vmc_credits_count                    as vmc_credits_count,\n                  sm.vmc_credits_amount                   as vmc_credits_amount,\n                  nvl(sm.amex_ic_vol_count,0)             as am_ic_vol_count,\n                  nvl(sm.amex_ic_vol_amount,0)            as am_ic_vol_amount,  \n                  nvl(sm.amex_ic_sales_count,0)           as am_ic_sales_count,\n                  nvl(sm.amex_ic_sales_amount,0)          as am_ic_sales_amount,  \n                  nvl(sm.amex_ic_credits_count,0)         as am_ic_credits_count,\n                  nvl(sm.amex_ic_credits_amount,0)        as am_ic_credits_amount,  \n                  nvl(sm.disc_ic_vol_count,0)             as ds_ic_vol_count,\n                  nvl(sm.disc_ic_vol_amount,0)            as ds_ic_vol_amount,  \n                  nvl(sm.disc_ic_sales_count,0)           as ds_ic_sales_count,\n                  nvl(sm.disc_ic_sales_amount,0)          as ds_ic_sales_amount,  \n                  nvl(sm.disc_ic_credits_count,0)         as ds_ic_credits_count,\n                  nvl(sm.disc_ic_credits_amount,0)        as ds_ic_credits_amount,  \n                  nvl(sm.amex_vol_count,0)                as amex_vol_count,\n                  nvl(sm.amex_vol_amount,0)               as amex_vol_amount,  \n                  nvl(sm.amex_sales_count,0)              as amex_sales_count,\n                  nvl(sm.amex_sales_amount,0)             as amex_sales_amount,  \n                  nvl(sm.amex_credits_count,0)            as amex_credits_count,\n                  nvl(sm.amex_credits_amount,0)           as amex_credits_amount,  \n                  nvl(sm.dinr_vol_count,0)                as dinr_vol_count,\n                  nvl(sm.dinr_vol_amount,0)               as dinr_vol_amount,  \n                  nvl(sm.dinr_sales_count,0)              as dinr_sales_count,\n                  nvl(sm.dinr_sales_amount,0)             as dinr_sales_amount,  \n                  nvl(sm.dinr_credits_count,0)            as dinr_credits_count,\n                  nvl(sm.dinr_credits_amount,0)           as dinr_credits_amount,  \n                  nvl(sm.disc_vol_count,0)                as disc_vol_count,\n                  nvl(sm.disc_vol_amount,0)               as disc_vol_amount,  \n                  nvl(sm.disc_sales_count,0)              as disc_sales_count,\n                  nvl(sm.disc_sales_amount,0)             as disc_sales_amount,  \n                  nvl(sm.disc_credits_count,0)            as disc_credits_count,\n                  nvl(sm.disc_credits_amount,0)           as disc_credits_amount,  \n                  nvl(sm.jcb_vol_count,0)                 as jcb_vol_count,\n                  nvl(sm.jcb_vol_amount,0)                as jcb_vol_amount, \n                  nvl(sm.jcb_sales_count,0)               as jcb_sales_count,\n                  nvl(sm.jcb_sales_amount,0)              as jcb_sales_amount,  \n                  nvl(sm.jcb_credits_count,0)             as jcb_credits_count,\n                  nvl(sm.jcb_credits_amount,0)            as jcb_credits_amount,  \n                  nvl(sm.pl1_vol_count,0)                 as pl1_vol_count,\n                  nvl(sm.pl1_vol_amount,0)                as pl1_vol_amount, \n                  nvl(sm.pl1_sales_count,0)               as pl1_sales_count,\n                  nvl(sm.pl1_sales_amount,0)              as pl1_sales_amount,  \n                  nvl(sm.pl1_credits_count,0)             as pl1_credits_count,\n                  nvl(sm.pl1_credits_amount,0)            as pl1_credits_amount,  \n                  nvl(sm.pl2_vol_count,0)                 as pl2_vol_count,\n                  nvl(sm.pl2_vol_amount,0)                as pl2_vol_amount, \n                  nvl(sm.pl2_sales_count,0)               as pl2_sales_count,\n                  nvl(sm.pl2_sales_amount,0)              as pl2_sales_amount,  \n                  nvl(sm.pl2_credits_count,0)             as pl2_credits_count,\n                  nvl(sm.pl2_credits_amount,0)            as pl2_credits_amount,  \n                  nvl(sm.pl3_vol_count,0)                 as pl3_vol_count,\n                  nvl(sm.pl3_vol_amount,0)                as pl3_vol_amount, \n                  nvl(sm.pl3_sales_count,0)               as pl3_sales_count,\n                  nvl(sm.pl3_sales_amount,0)              as pl3_sales_amount,  \n                  nvl(sm.pl3_credits_count,0)             as pl3_credits_count,\n                  nvl(sm.pl3_credits_amount,0)            as pl3_credits_amount,  \n                  nvl(sm.debit_vol_count,0)               as debit_vol_count,\n                  nvl(sm.debit_vol_amount,0)              as debit_vol_amount,  \n                  nvl(sm.debit_sales_count,0)             as debit_sales_count,\n                  nvl(sm.debit_sales_amount,0)            as debit_sales_amount,  \n                  nvl(sm.debit_credits_count,0)           as debit_credits_count,\n                  nvl(sm.debit_credits_amount,0)          as debit_credits_amount,  \n                  nvl(sm.ebt_vol_count,0)                 as ebt_vol_count,\n                  nvl(sm.achp_vol_count,0)                as achp_vol_count,\n                  nvl(sm.achp_return_count,0)             as achp_return_count,\n                  nvl(dn.cat_01_total_count,0)            as debit_count_cat_01,\n                  nvl(dn.cat_02_total_count,0)            as debit_count_cat_02,\n                  nvl(dn.cat_03_total_count,0)            as debit_count_cat_03,\n                  nvl(dn.cat_04_total_count,0)            as debit_count_cat_04,\n                  nvl(dn.cat_05_total_count,0)            as debit_count_cat_05,\n                  nvl(dn.cat_06_total_count,0)            as debit_count_cat_06,\n                  nvl(dn.cat_07_total_count,0)            as debit_count_cat_07,\n                  nvl(dn.cat_08_total_count,0)            as debit_count_cat_08,\n                  nvl(dn.cat_09_total_count,0)            as debit_count_cat_09,\n                  nvl(dn.cat_10_total_count,0)            as debit_count_cat_10,\n                  sm.vmc_auth_count                       as vmc_auth_count,\n                  nvl(sm.visa_auth_count,0)               as visa_auth_count,\n                  nvl(sm.mc_auth_count,0)                 as mc_auth_count,\n                  sm.amex_auth_count                      as amex_auth_count,\n                  sm.dinr_auth_count                      as dinr_auth_count,\n                  sm.disc_auth_count                      as disc_auth_count,\n                  sm.jcb_auth_count                       as jcb_auth_count,\n                  sm.debit_auth_count                     as debit_auth_count,\n                  nvl(sm.auth_count_total,0)              as auth_count_total,\n                  nvl(sm.auth_count_total_ext,0)          as auth_count_total_ext,\n                  nvl(sm.auth_count_total_api,0)          as auth_count_total_api,\n                  nvl(sm.auth_count_total_trident,0)      as auth_count_total_trident,\n                  nvl(sm.vmc_auth_count_trident,0)        as vmc_auth_count_trident,\n                  nvl(sm.amex_auth_count_trident,0)       as amex_auth_count_trident,\n                  nvl(sm.disc_auth_count_trident,0)       as disc_auth_count_trident,\n                  nvl(sm.dinr_auth_count_trident,0)       as dinr_auth_count_trident,\n                  nvl(sm.jcb_auth_count_trident,0)        as jcb_auth_count_trident,\n                  nvl(sm.debit_auth_count_trident,0)      as debit_auth_count_trident,\n                  ( nvl(sm.visa_auth_count,0) +\n                    nvl(dn.cat_03_total_count,0) )        as visa_interlink_auth_count,\n                  ( nvl(sm.disc_auth_count,0) +\n                   ( nvl(sm.debit_auth_count,0) -\n                     nvl(dn.cat_03_total_count,0) ) )     as non_visa_auth_access_count,\n                  sm.chargeback_vol_count                 as chargeback_vol_count,\n                  sm.chargeback_vol_amount                as chargeback_vol_amount,  \n                  sm.incoming_chargeback_count            as incoming_cb_count,\n                  sm.incoming_chargeback_amount           as incoming_cb_amount,\n                  sm.incoming_ft_chargeback_count         as incoming_ft_cb_count,\n                  sm.incoming_ft_chargeback_amount        as incoming_ft_cb_amount,\n                  sm.retrieval_count                      as retrieval_count,\n                  sm.retrieval_amount                     as retrieval_amount,\n                  sm.service_call_count                   as num_service_calls,\n                  sm.cash_advance_vol_count               as cash_advance_vol_count,\n                  sm.cash_advance_vol_amount              as cash_advance_vol_amount,  \n                  /* RS changes for ACH PS Columns added  to calculate partner fees  */\n                  nvl(sm.ach_tran_count,0)                as ach_tran_count,\n                  nvl(sm.ach_tran_amount,0)                as ach_tran_amount,\n                  nvl(sm.ach_return_count,0)              as ach_return_count,\n                  nvl(sm.ach_return_amount,0)              as ach_return_amount,\n                  nvl(sm.ach_unauth_count,0)              as ach_unauth_count,\n                  nvl(sm.ach_unauth_amount,0)              as ach_unauth_amount,\n                  decode( sm.activated_merchant,'Y',1,0 ) as num_activated_merchants,\n                  decode( sm.new_merchant,'Y',1,0 )       as num_new_merchants,\n                  decode( sm.amex_setup,'Y',1,0 )         as num_amex_setups,\n                  decode( sm.discover_setup,'Y',1,0 )     as num_disc_setups,\n                  sm.terminal_rental_count                as num_rentals,\n                  decode( sm.merchant_status,\n                          'C', 0,  -- closed\n                          'D', 0,  -- deleted\n                          'F', 0,  -- fraud\n                           1 )                            as num_active_merchants,\n                  ( nvl(sm.voice_auth_count,0) +\n                    nvl(sm.aru_auth_count,0) )            as dialpay_auth_count,\n                  nvl(sm.avs_count,0)                     as avs_auth_count,\n                  nvl(sm.ach_reject_count,0)              as ach_reject_count,\n                  nvl(sm.ic_correction_count,0)           as ic_correction_count,\n                  nvl(sm.terminal_swap_count,0)           as swap_count,\n                  nvl(sm.terminal_deployment_count,0)     as terminal_deployment_count,\n        \t\t  case when exists(select (1) \n        \t\t  \t\t\t\t\tfrom t_hierarchy     th,\n        \t\t  \t\t\t\t\t\tmif             m,\n        \t\t  \t\t\t\t\t\tagent_bank_contract     abc\n        \t\t  \t\t\t\t\twhere m.merchant_number = sm.merchant_number and\n        \t\t  \t\t\t\t\t\tm.association_node = th.descendent and\n        \t\t  \t\t\t\t\t\tth.ancestor = abc.hierarchy_node and\n        \t\t  \t\t\t\t\t\t ?  between abc.valid_date_begin and abc.valid_date_end and\n        \t\t  \t\t\t\t\t\tbilling_element_type =  ? ) \n          \t\t\t\tthen sm.debit_network_fees \n          \t\t\t\telse 0 end \t\t\t\t\t\t\tas debit_network_fees,\n          \t\t\tdecode( nvl(sm.print_statement, 'N'),'Y', 1, 0 )    \n\t\t\t\t\t\t  \t\t\t\t\t\t\t\t\tas print_statement_count,\n\t\t\t\t\tnvl(sm.batch_fee_count, 0)   \t\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tas batch_fee_count\n          from    monthly_extract_summary   sm,\n                  monthly_extract_dn        dn,\n                  (\n                    select  gm.merchant_number            as merchant_number,\n                            count(rmask.income_category)  as cat_count\n                    from    agent_bank_revenue_mask   rmask,\n                            organization              o,\n                            group_merchant            gm\n                    where   rmask.mask_type = 2 and\n                            rmask.charge_rec_prefix is null and\n                            o.org_group = rmask.hierarchy_node and\n                            gm.org_num  = o.org_num\n                    group by gm.merchant_number\n                  )                         rm\n          where   sm.load_file_id =  ?  and\n                  sm.active_date =  ?  and\n                   ");
   __sjT_sb.append(whereClause);
   __sjT_sb.append(" \n                  dn.hh_load_sec(+) = sm.hh_load_sec and\n                  exists\n                  (\n                    select  abc.hierarchy_node\n                    from    agent_bank_contract   abc,\n                            organization          o,\n                            group_merchant        gm\n                    where   o.org_group = abc.hierarchy_node and\n                            gm.org_num = o.org_num and\n                            gm.merchant_number = sm.merchant_number and\n                             ?  between abc.valid_date_begin and\n                                                abc.valid_date_end\n                  ) and\n                  rm.merchant_number(+) = sm.merchant_number");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "25com.mes.startup.MerchProfLoader:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setInt(2,BankContractBean.BET_PIN_DEBIT_PASS_THROUGH_FLAG);
   __sJT_st.setLong(3,loadFileId);
   __sJT_st.setDate(4,activeDate);
   __sJT_st.setDate(5,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1458^9*/
        resultSet = it.getResultSet();
      
        while( resultSet.next() )
        {
          merchantId  = resultSet.getLong("merchant_number");
          loadSec     = resultSet.getLong("hh_load_sec");
          achIncome   = resultSet.getDouble("ach_inc");
          
          expAchBuyback         = 0.0;
          expAchBuybackDisc     = 0.0;
          processBuyBack(resultSet.getLong("ach_tran_count"),resultSet.getDouble("ach_tran_amount"),resultSet.getLong("ach_return_count"),resultSet.getDouble("ach_return_amount"),resultSet.getLong("ach_unauth_count"),resultSet.getDouble("ach_unauth_amount"),loadSec,activeDate);
          
          
          
          achDiscLiability  = 0.0;     
          // reset the contract totals
          for( int i = 0; i < contractTotals.length; ++i )
          {
            for( int j = 0; j < contractTotals[i].length; ++j )
            {
              contractTotals[i][j] = 0.0;
            }
          }
        
          // reset the contract flags
          for( int i = 0; i < hasContract.length; ++i )
          {
            hasContract[i] = false;
          }
          
          // reset the mes only revenue 
          for( int i = 0; i < mesOnlyRevenue.length; ++i )
          {
            mesOnlyRevenue[i] = 0.0;
          }
        
          // reset the partnership rate
          mesOnlyChargeRecs = 0.0;
          partnershipRate   = 0.0;
          partnershipNode   = 0L;
          revenue           = 0.0;
          excludedSales     = 0.0;
          excludedRental    = 0.0;
          achDiscLiability  = 0.0;
          hasCashAdvRef     = false;
        
          // load the new totals for each contract type
          for( int ct = 0; ct < ContractTypes.CONTRACT_SOURCE_COUNT; ++ct )
          {
            // sometimes it is desirable to load a single contract type
            if ( ( contractSource != ContractTypes.CONTRACT_SOURCE_INVALID ) &&
                 ( ct != contractSource ) )
            {
              continue;
            }
            
            switch( ct )
            {
              // only test the types we are interested in 
              // to optimize the loading process.
              case ContractTypes.CONTRACT_SOURCE_REFERRAL:
              case ContractTypes.CONTRACT_SOURCE_LIABILITY:
              case ContractTypes.CONTRACT_SOURCE_RESELLER:
              case ContractTypes.CONTRACT_SOURCE_THIRD_PARTY:
              case ContractTypes.CONTRACT_SOURCE_GRIN:
                if ( (nodeId = getContractNode(merchantId,ct,activeDate)) != 0L )
                {
                  hasContract[ct] = true;
                  
                  /* Removing the logic to add contract only if ACH Fees is billed */
                  contract.loadContract( hierarchyNodeToOrgId(nodeId), ct, activeDate,loadSec );
           
                  contractDataBean.processContractSummaryData( contract, ct, resultSet, contractTotals );
                  /* Code Changes for ACH PS - Capture Partner expense for Discount */
                  if ( ct == ContractTypes.CONTRACT_SOURCE_LIABILITY  )
                  {	  
                      achDiscLiability = contractDataBean.getAchDiscLiability();
                  }                  
                  // update the partnership rate if necessary
                  if ( (rate = contract.getContractItemRate( BankContractBean.BET_PARTNERSHIP_RATE )) != 0.0 )
                  {
                    partnershipRate = rate;
                    partnershipNode = nodeId;
                  }
                }
                break;
              
              default:
                continue;
            }
          }
          
          if ( resultSet.getInt("mes_only_revenue") == 1 )
          {
            // back out any MES only revenue.  This is a result of
            // taking over the CB&T contracts where the agent bank
            // was not receiving any of the system generated fees
            // from the merchant billing as revenue.  Once these
            // were converted to the MES portfolio it became necessary
            // to support this type of category revenue masking
            /*@lineinfo:generated-code*//*@lineinfo:1564^13*/

//  ************************************************************
//  #sql [Ctx] { select  sum( decode( rm.income_category,
//                                     0, (sm.tot_inc_discount + sm.tot_inc_interchange),
//                                     0 ) ),
//                        sum( decode( rm.income_category,
//                                     1, sm.tot_inc_capture,
//                                     0 ) ),
//                        sum( decode( rm.income_category,
//                                     2, sm.tot_inc_debit,
//                                     0 ) ),
//                        sum( decode( rm.income_category,
//                                     3, sm.tot_inc_ind_plans,
//                                     0 ) ),
//                        sum( decode( rm.income_category,
//                                     4, sm.tot_inc_authorization,
//                                     0 ) ),
//                        sum( decode( rm.income_category,
//                                     5, sm.tot_inc_sys_generated,
//                                     0 ) )
//                    -- BET_GROUP_SYSTEM_FEES 
//                from    agent_bank_revenue_mask   rm,
//                        organization              o,
//                        group_merchant            gm,
//                        monthly_extract_summary   sm
//                where   rm.mask_type = 2 and
//                        rm.charge_rec_prefix is null and
//                        o.org_group = rm.hierarchy_node and
//                        gm.org_num = o.org_num and
//                        sm.merchant_number = gm.merchant_number and
//                        sm.merchant_number = :merchantId and
//                        sm.active_date = :activeDate
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( decode( rm.income_category,\n                                   0, (sm.tot_inc_discount + sm.tot_inc_interchange),\n                                   0 ) ),\n                      sum( decode( rm.income_category,\n                                   1, sm.tot_inc_capture,\n                                   0 ) ),\n                      sum( decode( rm.income_category,\n                                   2, sm.tot_inc_debit,\n                                   0 ) ),\n                      sum( decode( rm.income_category,\n                                   3, sm.tot_inc_ind_plans,\n                                   0 ) ),\n                      sum( decode( rm.income_category,\n                                   4, sm.tot_inc_authorization,\n                                   0 ) ),\n                      sum( decode( rm.income_category,\n                                   5, sm.tot_inc_sys_generated,\n                                   0 ) )\n                   -- BET_GROUP_SYSTEM_FEES \n              from    agent_bank_revenue_mask   rm,\n                      organization              o,\n                      group_merchant            gm,\n                      monthly_extract_summary   sm\n              where   rm.mask_type = 2 and\n                      rm.charge_rec_prefix is null and\n                      o.org_group = rm.hierarchy_node and\n                      gm.org_num = o.org_num and\n                      sm.merchant_number = gm.merchant_number and\n                      sm.merchant_number =  :1   and\n                      sm.active_date =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 6) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(6,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mesOnlyRevenue[0] = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mesOnlyRevenue[1] = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mesOnlyRevenue[2] = __sJT_rs.getDouble(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mesOnlyRevenue[3] = __sJT_rs.getDouble(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mesOnlyRevenue[4] = __sJT_rs.getDouble(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mesOnlyRevenue[5] = __sJT_rs.getDouble(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1601^13*/
          }
          
          if ( partnershipRate != 0.0 )
          {
            // add the applicable merchant income
            for( int revenueCat = 0; revenueCat < BankContractBean.BET_GROUP_COUNT; ++revenueCat )
            {
              if( contract.isGroupInPartnership( revenueCat ) )
              {
                try
                {
                  revenue += resultSet.getDouble( RevenueColumnNames[revenueCat] );
                }
                catch( Exception e )
                {
                  // ignore null and index boundary exceptions
                }
              }
            }
            
            // back out any charge records that are MES only revenue
            try
            {
              // select the amount of the masked values
              /*@lineinfo:generated-code*//*@lineinfo:1626^15*/

//  ************************************************************
//  #sql [Ctx] { select  nvl( sum( st.st_fee_amount ), 0 ) 
//                  from    monthly_extract_summary     sm,
//                          agent_bank_revenue_mask     rm,
//                          monthly_extract_st          st
//                  where   sm.merchant_number = :merchantId and
//                          sm.active_date = :activeDate and
//                          rm.hierarchy_node = :partnershipNode and
//                          not rm.charge_rec_prefix is null and
//                          st.hh_load_sec = sm.hh_load_sec and
//                          st.st_statement_desc like (rm.charge_rec_prefix || '%')
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl( sum( st.st_fee_amount ), 0 )  \n                from    monthly_extract_summary     sm,\n                        agent_bank_revenue_mask     rm,\n                        monthly_extract_st          st\n                where   sm.merchant_number =  :1   and\n                        sm.active_date =  :2   and\n                        rm.hierarchy_node =  :3   and\n                        not rm.charge_rec_prefix is null and\n                        st.hh_load_sec = sm.hh_load_sec and\n                        st.st_statement_desc like (rm.charge_rec_prefix || '%')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setLong(3,partnershipNode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mesOnlyChargeRecs = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1638^15*/
            }
            catch(java.sql.SQLException sqe)
            {
              mesOnlyChargeRecs = 0.0;
            }
            
            // add MES only fees to the system fees liability
            contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_SYSTEM_FEES] += mesOnlyChargeRecs;
            
            // add any referral contract income
            for( int i = 0; i < contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL].length; ++i )
            {
              revenue += contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][i];
            }
          
            // add any revenue adjustments
            revenue += ( resultSet.getDouble("disc_ic_adj_amount") +
                         resultSet.getDouble("fee_adj_amount") );
                         
            // subtract out interchange and assessment fees
            revenue -= resultSet.getDouble("ic_exp");
            revenue -= resultSet.getDouble("assessment");
            
            // subtract out the equipment costs
            if ( resultSet.getInt("mes_inventory") == 1 )
            {
              revenue -= (resultSet.getDouble("equip_sales_base_cost") +
                          resultSet.getDouble("equip_rental_base_cost"));
            }
            else
            {
              revenue -= resultSet.getDouble("equip_transfer_cost");
            }
          
            // subtract out the liability contract
            for( int i = 0; i < contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY].length; ++i )
            {
              revenue -= contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][i];
            }
            
            // add any pin debit network fees
            revenue -= resultSet.getDouble("debit_network_fees");
            
          }   // end if partnership rate != 0
          
          // setup the contract fee exclusions
          if ( contract.isGroupExcluded( BankContractBean.BET_GROUP_EQUIP_SALES ) )
          {
            if ( resultSet.getInt("mes_inventory") == 0 )
            {
              // equipment owned by client, only cost is the JIT transfers
              excludedSales = resultSet.getDouble("equip_transfer_cost");
            }
            else
            {
              // equipment is owned by MES, cost is the base cost
              excludedSales = resultSet.getDouble("equip_sales_base_cost");
            }
          }
          if ( contract.isGroupExcluded( BankContractBean.BET_GROUP_EQUIP_RENTAL ) )
          {
            excludedRental = resultSet.getDouble("equip_rental_base_cost");
          }
          
          hasCashAdvRef = ( resultSet.getInt("cash_advance") != 0 &&
                            contractDataBean.hasCashAdvanceReferralContract( merchantId ) );
                
          // store the totals in the summary table
          /*@lineinfo:generated-code*//*@lineinfo:1707^11*/

//  ************************************************************
//  #sql [Ctx] { update  monthly_extract_summary
//              set     tot_ndr_liability           = :contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_DISC_IC],
//                      tot_authorization_liability = :contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_AUTH_FEES],
//                      tot_capture_liability       = :contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_CAPTURE_FEES],
//                      tot_debit_liability         = :contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_DEBIT_FEES],
//                      tot_ind_plans_liability     = :contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_PLAN_FEES],
//                      tot_sys_generated_liability = :contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_SYSTEM_FEES],
//                      tot_equip_rental_liability  = :contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_EQUIP_RENTAL],
//                      tot_equip_sales_liability   = :contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_EQUIP_SALES],
//                      /* RS changes for ACH PS - Include ACH Schd A Liability  */
//                      tot_ach_liability           = :contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_ACH],
//                      tot_ndr_referral            = :contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_DISC_IC],
//                      tot_authorization_referral  = :contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_AUTH_FEES],
//                      tot_capture_referral        = :contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_CAPTURE_FEES],
//                      tot_debit_referral          = :contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_DEBIT_FEES],
//                      tot_ind_plans_referral      = :contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_PLAN_FEES],
//                      tot_sys_generated_referral  = :contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_SYSTEM_FEES],
//                      tot_equip_rental_referral   = :contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_EQUIP_RENTAL],
//                      tot_equip_sales_referral    = :contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_EQUIP_SALES],
//                      /* RS changes for ACH PS - Include ACH Schd A Referral */
//                      tot_ach_referral_expense    = :contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_ACH],
//                      tot_ndr_reseller            = :contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_DISC_IC],
//                      tot_authorization_reseller  = :contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_AUTH_FEES],
//                      tot_capture_reseller        = :contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_CAPTURE_FEES],
//                      tot_debit_reseller          = :contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_DEBIT_FEES],
//                      tot_ind_plans_reseller      = :contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_PLAN_FEES],
//                      tot_sys_generated_reseller  = :contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_SYSTEM_FEES],
//                      tot_equip_rental_reseller   = :contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_EQUIP_RENTAL],
//                      tot_equip_sales_reseller    = :contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_EQUIP_SALES],
//                      tot_ndr_external            = :contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_DISC_IC],
//                      tot_authorization_external  = :contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_AUTH_FEES],
//                      tot_capture_external        = :contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_CAPTURE_FEES],
//                      tot_debit_external          = :contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_DEBIT_FEES],
//                      tot_ind_plans_external      = :contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_PLAN_FEES],
//                      tot_sys_generated_external  = :contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_SYSTEM_FEES],
//                      tot_equip_rental_external   = :contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_EQUIP_RENTAL],
//                      tot_equip_sales_external    = :contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_EQUIP_SALES],
//                      external_contract           = :(hasContract[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY] == true) ? "Y" : "N",
//                      reseller_contract           = :(hasContract[ContractTypes.CONTRACT_SOURCE_RESELLER] == true) ? "Y" : "N",
//                      referral_contract           = :(hasContract[ContractTypes.CONTRACT_SOURCE_REFERRAL] == true) ? "Y" : "N",
//                      liability_contract          = :(hasContract[ContractTypes.CONTRACT_SOURCE_LIABILITY] == true) ? "Y" : "N",
//                      tot_partnership             = (:revenue * :partnershipRate * 0.01),
//                      partnership_rate            = decode(:partnershipRate,0,null,:partnershipRate),
//                      /* RS changes for ACH PS - Include ACH Schd A Discount & PS Buy back Expense */
//                      tot_ach_disc_liability      = :achDiscLiability,
//                      tot_exp_ach_vendor          = :expAchBuyback,
//                      tot_exp_ach_vendor_discount = :expAchBuybackDisc, 
//                      excluded_equip_sales_amount = :excludedSales,
//                      excluded_equip_rental_amount= :excludedRental,
//                      excluded_fees_amount        = :contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_EXCLUDED_FEES],
//                      cash_advance_referral       = :hasCashAdvRef ? "Y" : "N",
//                      mes_only_inc_disc_ic        = :mesOnlyRevenue[BankContractBean.BET_GROUP_DISC_IC],
//                      mes_only_inc_authorization  = :mesOnlyRevenue[BankContractBean.BET_GROUP_AUTH_FEES],
//                      mes_only_inc_capture        = :mesOnlyRevenue[BankContractBean.BET_GROUP_CAPTURE_FEES],
//                      mes_only_inc_ind_plans      = :mesOnlyRevenue[BankContractBean.BET_GROUP_PLAN_FEES],
//                      mes_only_inc_debit          = :mesOnlyRevenue[BankContractBean.BET_GROUP_DEBIT_FEES],
//                      mes_only_inc_sys_generated  = :mesOnlyRevenue[BankContractBean.BET_GROUP_SYSTEM_FEES]
//              where   merchant_number = :merchantId and
//                      active_date     = :activeDate
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_75 = contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_DISC_IC];
 double __sJT_76 = contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_AUTH_FEES];
 double __sJT_77 = contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_CAPTURE_FEES];
 double __sJT_78 = contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_DEBIT_FEES];
 double __sJT_79 = contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_PLAN_FEES];
 double __sJT_80 = contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_SYSTEM_FEES];
 double __sJT_81 = contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_EQUIP_RENTAL];
 double __sJT_82 = contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_EQUIP_SALES];
 double __sJT_83 = contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_ACH];
 double __sJT_84 = contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_DISC_IC];
 double __sJT_85 = contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_AUTH_FEES];
 double __sJT_86 = contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_CAPTURE_FEES];
 double __sJT_87 = contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_DEBIT_FEES];
 double __sJT_88 = contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_PLAN_FEES];
 double __sJT_89 = contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_SYSTEM_FEES];
 double __sJT_90 = contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_EQUIP_RENTAL];
 double __sJT_91 = contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_EQUIP_SALES];
 double __sJT_92 = contractTotals[ContractTypes.CONTRACT_SOURCE_REFERRAL][BankContractBean.BET_GROUP_ACH];
 double __sJT_93 = contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_DISC_IC];
 double __sJT_94 = contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_AUTH_FEES];
 double __sJT_95 = contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_CAPTURE_FEES];
 double __sJT_96 = contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_DEBIT_FEES];
 double __sJT_97 = contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_PLAN_FEES];
 double __sJT_98 = contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_SYSTEM_FEES];
 double __sJT_99 = contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_EQUIP_RENTAL];
 double __sJT_100 = contractTotals[ContractTypes.CONTRACT_SOURCE_RESELLER][BankContractBean.BET_GROUP_EQUIP_SALES];
 double __sJT_101 = contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_DISC_IC];
 double __sJT_102 = contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_AUTH_FEES];
 double __sJT_103 = contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_CAPTURE_FEES];
 double __sJT_104 = contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_DEBIT_FEES];
 double __sJT_105 = contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_PLAN_FEES];
 double __sJT_106 = contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_SYSTEM_FEES];
 double __sJT_107 = contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_EQUIP_RENTAL];
 double __sJT_108 = contractTotals[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY][BankContractBean.BET_GROUP_EQUIP_SALES];
 String __sJT_109 = (hasContract[ContractTypes.CONTRACT_SOURCE_THIRD_PARTY] == true) ? "Y" : "N";
 String __sJT_110 = (hasContract[ContractTypes.CONTRACT_SOURCE_RESELLER] == true) ? "Y" : "N";
 String __sJT_111 = (hasContract[ContractTypes.CONTRACT_SOURCE_REFERRAL] == true) ? "Y" : "N";
 String __sJT_112 = (hasContract[ContractTypes.CONTRACT_SOURCE_LIABILITY] == true) ? "Y" : "N";
 double __sJT_113 = contractTotals[ContractTypes.CONTRACT_SOURCE_LIABILITY][BankContractBean.BET_GROUP_EXCLUDED_FEES];
 String __sJT_114 = hasCashAdvRef ? "Y" : "N";
 double __sJT_115 = mesOnlyRevenue[BankContractBean.BET_GROUP_DISC_IC];
 double __sJT_116 = mesOnlyRevenue[BankContractBean.BET_GROUP_AUTH_FEES];
 double __sJT_117 = mesOnlyRevenue[BankContractBean.BET_GROUP_CAPTURE_FEES];
 double __sJT_118 = mesOnlyRevenue[BankContractBean.BET_GROUP_PLAN_FEES];
 double __sJT_119 = mesOnlyRevenue[BankContractBean.BET_GROUP_DEBIT_FEES];
 double __sJT_120 = mesOnlyRevenue[BankContractBean.BET_GROUP_SYSTEM_FEES];
   String theSqlTS = "update  monthly_extract_summary\n            set     tot_ndr_liability           =  :1  ,\n                    tot_authorization_liability =  :2  ,\n                    tot_capture_liability       =  :3  ,\n                    tot_debit_liability         =  :4  ,\n                    tot_ind_plans_liability     =  :5  ,\n                    tot_sys_generated_liability =  :6  ,\n                    tot_equip_rental_liability  =  :7  ,\n                    tot_equip_sales_liability   =  :8  ,\n                    /* RS changes for ACH PS - Include ACH Schd A Liability  */\n                    tot_ach_liability           =  :9  ,\n                    tot_ndr_referral            =  :10  ,\n                    tot_authorization_referral  =  :11  ,\n                    tot_capture_referral        =  :12  ,\n                    tot_debit_referral          =  :13  ,\n                    tot_ind_plans_referral      =  :14  ,\n                    tot_sys_generated_referral  =  :15  ,\n                    tot_equip_rental_referral   =  :16  ,\n                    tot_equip_sales_referral    =  :17  ,\n                    /* RS changes for ACH PS - Include ACH Schd A Referral */\n                    tot_ach_referral_expense    =  :18  ,\n                    tot_ndr_reseller            =  :19  ,\n                    tot_authorization_reseller  =  :20  ,\n                    tot_capture_reseller        =  :21  ,\n                    tot_debit_reseller          =  :22  ,\n                    tot_ind_plans_reseller      =  :23  ,\n                    tot_sys_generated_reseller  =  :24  ,\n                    tot_equip_rental_reseller   =  :25  ,\n                    tot_equip_sales_reseller    =  :26  ,\n                    tot_ndr_external            =  :27  ,\n                    tot_authorization_external  =  :28  ,\n                    tot_capture_external        =  :29  ,\n                    tot_debit_external          =  :30  ,\n                    tot_ind_plans_external      =  :31  ,\n                    tot_sys_generated_external  =  :32  ,\n                    tot_equip_rental_external   =  :33  ,\n                    tot_equip_sales_external    =  :34  ,\n                    external_contract           =  :35  ,\n                    reseller_contract           =  :36  ,\n                    referral_contract           =  :37  ,\n                    liability_contract          =  :38  ,\n                    tot_partnership             = ( :39   *  :40   * 0.01),\n                    partnership_rate            = decode( :41  ,0,null, :42  ),\n                    /* RS changes for ACH PS - Include ACH Schd A Discount & PS Buy back Expense */\n                    tot_ach_disc_liability      =  :43  ,\n                    tot_exp_ach_vendor          =  :44  ,\n                    tot_exp_ach_vendor_discount =  :45  , \n                    excluded_equip_sales_amount =  :46  ,\n                    excluded_equip_rental_amount=  :47  ,\n                    excluded_fees_amount        =  :48  ,\n                    cash_advance_referral       =  :49  ,\n                    mes_only_inc_disc_ic        =  :50  ,\n                    mes_only_inc_authorization  =  :51  ,\n                    mes_only_inc_capture        =  :52  ,\n                    mes_only_inc_ind_plans      =  :53  ,\n                    mes_only_inc_debit          =  :54  ,\n                    mes_only_inc_sys_generated  =  :55  \n            where   merchant_number =  :56   and\n                    active_date     =  :57 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_75);
   __sJT_st.setDouble(2,__sJT_76);
   __sJT_st.setDouble(3,__sJT_77);
   __sJT_st.setDouble(4,__sJT_78);
   __sJT_st.setDouble(5,__sJT_79);
   __sJT_st.setDouble(6,__sJT_80);
   __sJT_st.setDouble(7,__sJT_81);
   __sJT_st.setDouble(8,__sJT_82);
   __sJT_st.setDouble(9,__sJT_83);
   __sJT_st.setDouble(10,__sJT_84);
   __sJT_st.setDouble(11,__sJT_85);
   __sJT_st.setDouble(12,__sJT_86);
   __sJT_st.setDouble(13,__sJT_87);
   __sJT_st.setDouble(14,__sJT_88);
   __sJT_st.setDouble(15,__sJT_89);
   __sJT_st.setDouble(16,__sJT_90);
   __sJT_st.setDouble(17,__sJT_91);
   __sJT_st.setDouble(18,__sJT_92);
   __sJT_st.setDouble(19,__sJT_93);
   __sJT_st.setDouble(20,__sJT_94);
   __sJT_st.setDouble(21,__sJT_95);
   __sJT_st.setDouble(22,__sJT_96);
   __sJT_st.setDouble(23,__sJT_97);
   __sJT_st.setDouble(24,__sJT_98);
   __sJT_st.setDouble(25,__sJT_99);
   __sJT_st.setDouble(26,__sJT_100);
   __sJT_st.setDouble(27,__sJT_101);
   __sJT_st.setDouble(28,__sJT_102);
   __sJT_st.setDouble(29,__sJT_103);
   __sJT_st.setDouble(30,__sJT_104);
   __sJT_st.setDouble(31,__sJT_105);
   __sJT_st.setDouble(32,__sJT_106);
   __sJT_st.setDouble(33,__sJT_107);
   __sJT_st.setDouble(34,__sJT_108);
   __sJT_st.setString(35,__sJT_109);
   __sJT_st.setString(36,__sJT_110);
   __sJT_st.setString(37,__sJT_111);
   __sJT_st.setString(38,__sJT_112);
   __sJT_st.setDouble(39,revenue);
   __sJT_st.setDouble(40,partnershipRate);
   __sJT_st.setDouble(41,partnershipRate);
   __sJT_st.setDouble(42,partnershipRate);
   __sJT_st.setDouble(43,achDiscLiability);
   __sJT_st.setDouble(44,expAchBuyback);
   __sJT_st.setDouble(45,expAchBuybackDisc);
   __sJT_st.setDouble(46,excludedSales);
   __sJT_st.setDouble(47,excludedRental);
   __sJT_st.setDouble(48,__sJT_113);
   __sJT_st.setString(49,__sJT_114);
   __sJT_st.setDouble(50,__sJT_115);
   __sJT_st.setDouble(51,__sJT_116);
   __sJT_st.setDouble(52,__sJT_117);
   __sJT_st.setDouble(53,__sJT_118);
   __sJT_st.setDouble(54,__sJT_119);
   __sJT_st.setDouble(55,__sJT_120);
   __sJT_st.setLong(56,merchantId);
   __sJT_st.setDate(57,activeDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1768^11*/
        
          // remove any existing GRIN entries 
          /*@lineinfo:generated-code*//*@lineinfo:1771^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    monthly_extract_contract
//              where   hh_load_sec = :loadSec and
//                      ( :contractSource = :ContractTypes.CONTRACT_SOURCE_INVALID or -- -1 or
//                        contract_type = :contractSource )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    monthly_extract_contract\n            where   hh_load_sec =  :1   and\n                    (  :2   =  :3   or -- -1 or\n                      contract_type =  :4   )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setInt(2,contractSource);
   __sJT_st.setInt(3,ContractTypes.CONTRACT_SOURCE_INVALID);
   __sJT_st.setInt(4,contractSource);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1778^11*/
            
          for( int ctype = 0; ctype < ContractTypes.CONTRACT_SOURCE_COUNT; ++ctype )
          {
            if ( hasContract[ctype] == true )
            {
              /*@lineinfo:generated-code*//*@lineinfo:1784^15*/

//  ************************************************************
//  #sql [Ctx] { insert into monthly_extract_contract
//                  (
//                    hh_load_sec,
//                    merchant_number,
//                    active_date,
//                    contract_type,
//                    tot_ndr,
//                    tot_authorization,
//                    tot_capture,
//                    tot_debit,
//                    tot_ind_plans,
//                    tot_sys_generated,
//                    tot_equip_rental,
//                    tot_equip_sales,
//                    /* RS changes for ACH PS - populate Sch A Expense  */
//                    tot_ach,
//                    load_file_id
//                  )
//                  values
//                  (
//                    :loadSec,
//                    :merchantId,
//                    :activeDate,
//                    :ctype,
//                    :contractTotals[ctype][BankContractBean.BET_GROUP_DISC_IC],
//                    :contractTotals[ctype][BankContractBean.BET_GROUP_AUTH_FEES],
//                    :contractTotals[ctype][BankContractBean.BET_GROUP_CAPTURE_FEES],
//                    :contractTotals[ctype][BankContractBean.BET_GROUP_DEBIT_FEES],
//                    :contractTotals[ctype][BankContractBean.BET_GROUP_PLAN_FEES],
//                    :contractTotals[ctype][BankContractBean.BET_GROUP_SYSTEM_FEES],
//                    :contractTotals[ctype][BankContractBean.BET_GROUP_EQUIP_RENTAL],
//                    :contractTotals[ctype][BankContractBean.BET_GROUP_EQUIP_SALES],
//                    /* RS changes for ACH PS - populate Sch A Expense  */
//                    :contractTotals[ctype][BankContractBean.BET_GROUP_ACH],
//                    :loadFileId
//                  )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_121 = contractTotals[ctype][BankContractBean.BET_GROUP_DISC_IC];
 double __sJT_122 = contractTotals[ctype][BankContractBean.BET_GROUP_AUTH_FEES];
 double __sJT_123 = contractTotals[ctype][BankContractBean.BET_GROUP_CAPTURE_FEES];
 double __sJT_124 = contractTotals[ctype][BankContractBean.BET_GROUP_DEBIT_FEES];
 double __sJT_125 = contractTotals[ctype][BankContractBean.BET_GROUP_PLAN_FEES];
 double __sJT_126 = contractTotals[ctype][BankContractBean.BET_GROUP_SYSTEM_FEES];
 double __sJT_127 = contractTotals[ctype][BankContractBean.BET_GROUP_EQUIP_RENTAL];
 double __sJT_128 = contractTotals[ctype][BankContractBean.BET_GROUP_EQUIP_SALES];
 double __sJT_129 = contractTotals[ctype][BankContractBean.BET_GROUP_ACH];
   String theSqlTS = "insert into monthly_extract_contract\n                (\n                  hh_load_sec,\n                  merchant_number,\n                  active_date,\n                  contract_type,\n                  tot_ndr,\n                  tot_authorization,\n                  tot_capture,\n                  tot_debit,\n                  tot_ind_plans,\n                  tot_sys_generated,\n                  tot_equip_rental,\n                  tot_equip_sales,\n                  /* RS changes for ACH PS - populate Sch A Expense  */\n                  tot_ach,\n                  load_file_id\n                )\n                values\n                (\n                   :1  ,\n                   :2  ,\n                   :3  ,\n                   :4  ,\n                   :5  ,\n                   :6  ,\n                   :7  ,\n                   :8  ,\n                   :9  ,\n                   :10  ,\n                   :11  ,\n                   :12  ,\n                  /* RS changes for ACH PS - populate Sch A Expense  */\n                   :13  ,\n                   :14  \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"30com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setDate(3,activeDate);
   __sJT_st.setInt(4,ctype);
   __sJT_st.setDouble(5,__sJT_121);
   __sJT_st.setDouble(6,__sJT_122);
   __sJT_st.setDouble(7,__sJT_123);
   __sJT_st.setDouble(8,__sJT_124);
   __sJT_st.setDouble(9,__sJT_125);
   __sJT_st.setDouble(10,__sJT_126);
   __sJT_st.setDouble(11,__sJT_127);
   __sJT_st.setDouble(12,__sJT_128);
   __sJT_st.setDouble(13,__sJT_129);
   __sJT_st.setLong(14,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1822^15*/
            }              
          }            
            
          // commit after every 100 inserts
          if ( ++insertCount >= 100 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:1829^13*/

//  ************************************************************
//  #sql [Ctx] { commit
//               };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1832^13*/
            insertCount = 0;
          }          
        
          //@+
          ++recCount;
          //@-
        }
        
        /* RS Code Change for ACH PS - Populate PS buy back expense for ACH PS Direct Merchant */
        trace("Selecting records to process");
		/*@lineinfo:generated-code*//*@lineinfo:1843^3*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//  		              ordered
//  		              index (sm idx_mon_ext_sum_load_file_id) 
//  		          */
//  		          sm.hh_load_sec                          as hh_load_sec,
//  		          sm.merchant_number                      as merchant_number,
//  		          sm.active_date                          as active_date,
//  		          nvl(sm.ach_tran_count,0)                as ach_tran_count,
//  		          nvl(sm.ach_tran_amount,0)                as ach_tran_amount,
//  		          nvl(sm.ach_return_count,0)              as ach_return_count,
//  		          nvl(sm.ach_return_amount,0)              as ach_return_amount,
//  		          nvl(sm.ach_unauth_count,0)              as ach_unauth_count,
//  		          nvl(sm.ach_unauth_amount,0)              as ach_unauth_amount
//  		  from    monthly_extract_summary   sm,
//  		          monthly_extract_dn        dn
//  		  where   sm.load_file_id = :loadFileId and
//  		          sm.active_date = :activeDate and
//  		          dn.hh_load_sec(+) = sm.hh_load_sec and
//  		          not exists
//  		          (
//  		            select  ' '
//  		            from    agent_bank_contract   abc,
//  		                    organization          o,
//  		                    group_merchant        gm
//  		            where   o.org_group = abc.hierarchy_node and
//  		                    gm.org_num = o.org_num and
//  		                    gm.merchant_number = sm.merchant_number and
//  		                    :activeDate between abc.valid_date_begin and
//  		                                        abc.valid_date_end
//  		          ) 
//  		 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n\t\t              ordered\n\t\t              index (sm idx_mon_ext_sum_load_file_id) \n\t\t          */\n\t\t          sm.hh_load_sec                          as hh_load_sec,\n\t\t          sm.merchant_number                      as merchant_number,\n\t\t          sm.active_date                          as active_date,\n\t\t          nvl(sm.ach_tran_count,0)                as ach_tran_count,\n\t\t          nvl(sm.ach_tran_amount,0)                as ach_tran_amount,\n\t\t          nvl(sm.ach_return_count,0)              as ach_return_count,\n\t\t          nvl(sm.ach_return_amount,0)              as ach_return_amount,\n\t\t          nvl(sm.ach_unauth_count,0)              as ach_unauth_count,\n\t\t          nvl(sm.ach_unauth_amount,0)              as ach_unauth_amount\n\t\t  from    monthly_extract_summary   sm,\n\t\t          monthly_extract_dn        dn\n\t\t  where   sm.load_file_id =  :1   and\n\t\t          sm.active_date =  :2   and\n\t\t          dn.hh_load_sec(+) = sm.hh_load_sec and\n\t\t          not exists\n\t\t          (\n\t\t            select  ' '\n\t\t            from    agent_bank_contract   abc,\n\t\t                    organization          o,\n\t\t                    group_merchant        gm\n\t\t            where   o.org_group = abc.hierarchy_node and\n\t\t                    gm.org_num = o.org_num and\n\t\t                    gm.merchant_number = sm.merchant_number and\n\t\t                     :3   between abc.valid_date_begin and\n\t\t                                        abc.valid_date_end\n\t\t          )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"31com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1875^3*/
		resultSet = it.getResultSet();
		
		while( resultSet.next() )
		{
		loadSec     = resultSet.getLong("hh_load_sec");
		merchantId  = resultSet.getLong("merchant_number");
		
		expAchBuyback         = 0.0;
		expAchBuybackDisc     = 0.0;
		processBuyBack(resultSet.getLong("ach_tran_count"),resultSet.getDouble("ach_tran_amount"),resultSet.getLong("ach_return_count"),resultSet.getDouble("ach_return_amount"),resultSet.getLong("ach_unauth_count"),resultSet.getDouble("ach_unauth_amount"),loadSec,activeDate);
		
		// store the ACH Buy back expense in summary table
		/*@lineinfo:generated-code*//*@lineinfo:1888^3*/

//  ************************************************************
//  #sql [Ctx] { update  monthly_extract_summary
//  		    set   tot_exp_ach_vendor          = :expAchBuyback,
//  		          tot_exp_ach_vendor_discount = :expAchBuybackDisc
//  		  where   merchant_number = :merchantId and
//  		          active_date     = :activeDate
//  		 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  monthly_extract_summary\n\t\t    set   tot_exp_ach_vendor          =  :1  ,\n\t\t          tot_exp_ach_vendor_discount =  :2  \n\t\t  where   merchant_number =  :3   and\n\t\t          active_date     =  :4 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"32com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,expAchBuyback);
   __sJT_st.setDouble(2,expAchBuybackDisc);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setDate(4,activeDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1895^3*/ 
		}  
		//
        
        /*@lineinfo:generated-code*//*@lineinfo:1899^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1902^9*/

        log.debug("Records Processed: " + recCount);
      
        resultSet.close();
        it.close();
        
        // queue the mojave summary
        /*@lineinfo:generated-code*//*@lineinfo:1910^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_prof_process
//              ( process_type, load_filename )
//            values
//              ( :PT_MOJAVE_SUMMARY, :loadFilename )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_prof_process\n            ( process_type, load_filename )\n          values\n            (  :1  ,  :2   )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"33com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,PT_MOJAVE_SUMMARY);
   __sJT_st.setString(2,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1916^9*/            
      
        commit();
        
        fileProcessed = true;
      }     // end if ( processFile == true )        
    }
    catch( Exception e )
    {
      logEntry("loadContractSummary(" + merchantId + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      try{ contract.cleanUp(); } catch( Exception e ) {}
      try{ contractDataBean.cleanUp(); } catch( Exception e ) {}
    }
    
    return( fileProcessed );
  }
  
    /* RS Code Change for ACH PS - PS Buy back Expense Generation */
    private void processBuyBack(long tranCount, double tranVol, long retCount, double retVol, long unAuthCount, double unAuthVol, long loadSec, Date activeDate) {
     ResultSet                   resultSet             = null;
     ResultSetIterator           it                    = null;
     String achCharge1 = mesConstants.ACH_MONTHLY_SERVICE_FEE;
     String achCharge2 = mesConstants.ACH_SMARTPAY_EXP_MONTHLY_FEE;
     String achCharge3 = mesConstants.ACH_HOST_PAY_MONTHLY_FEE;
     String achCharge4 = mesConstants.ACH_SMARTPAY_EXP_SETUP_FEE;
     
      try
      {
      /*@lineinfo:generated-code*//*@lineinfo:1948^7*/

//  ************************************************************
//  #sql [Ctx] it = { select nvl(decode(psprc.item_type, 224,1 ,0) * psprc.rate,0)  as ach_tran_rate,
//                 nvl(decode(psprc.item_type, 224, 1, 0) * psprc.per_item,0) as ach_tran_per_item,
//                 nvl(decode(psprc.item_type, 225, 1, 0) * psprc.rate,0)     as ach_return_rate,
//                 nvl(decode(psprc.item_type, 225, 1, 0) * psprc.per_item,0) as ach_return_per_item,
//                 nvl(decode(psprc.item_type, 226, 1, 0) * psprc.rate,0)     as ach_unauth_rate,
//                 nvl(decode(psprc.item_type, 226, 1, 0) * psprc.per_item,0) as ach_unauth_per_item,
//                 nvl(decode(psprc.item_type, 231, 1, 0) * psprc.per_item,0) as ach_monthly_service_fee,
//                 nvl(decode(psprc.item_type, 232, 1, 0) * psprc.per_item,0) as ach_smartpay_exp_mth_fee,
//                 nvl(decode(psprc.item_type, 233, 1, 0) * psprc.per_item,0) as ach_hostedpay_mth_fee,
//                 nvl(decode(psprc.item_type, 234, 1, 0) * psprc.per_item,0) as ach_smartpay_exp_setup_fee               
//                 from ach_pricing  psprc
//                     ,mbs_elements mbe
//                where item_subclass = 'AC'
//                  and :activeDate between psprc.valid_date_begin and psprc.valid_date_end 
//                  and mbe.item_type = psprc.item_type
//                  and (
//                        (psprc.item_type between 224 and 226 ) 
//                         or
//                        ( 
//                          psprc.item_type between 231 and 234 
//                          and exists
//                          (select ''  
//                            from   monthly_extract_st st
//                           where st.hh_load_sec = :loadSec and
//                                upper(st.st_statement_desc) = upper(mbe.statement_msg_default)
//                         )  
//                      )
//                    )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select nvl(decode(psprc.item_type, 224,1 ,0) * psprc.rate,0)  as ach_tran_rate,\n               nvl(decode(psprc.item_type, 224, 1, 0) * psprc.per_item,0) as ach_tran_per_item,\n               nvl(decode(psprc.item_type, 225, 1, 0) * psprc.rate,0)     as ach_return_rate,\n               nvl(decode(psprc.item_type, 225, 1, 0) * psprc.per_item,0) as ach_return_per_item,\n               nvl(decode(psprc.item_type, 226, 1, 0) * psprc.rate,0)     as ach_unauth_rate,\n               nvl(decode(psprc.item_type, 226, 1, 0) * psprc.per_item,0) as ach_unauth_per_item,\n               nvl(decode(psprc.item_type, 231, 1, 0) * psprc.per_item,0) as ach_monthly_service_fee,\n               nvl(decode(psprc.item_type, 232, 1, 0) * psprc.per_item,0) as ach_smartpay_exp_mth_fee,\n               nvl(decode(psprc.item_type, 233, 1, 0) * psprc.per_item,0) as ach_hostedpay_mth_fee,\n               nvl(decode(psprc.item_type, 234, 1, 0) * psprc.per_item,0) as ach_smartpay_exp_setup_fee               \n               from ach_pricing  psprc\n                   ,mbs_elements mbe\n              where item_subclass = 'AC'\n                and  :1   between psprc.valid_date_begin and psprc.valid_date_end \n                and mbe.item_type = psprc.item_type\n                and (\n                      (psprc.item_type between 224 and 226 ) \n                       or\n                      ( \n                        psprc.item_type between 231 and 234 \n                        and exists\n                        (select ''  \n                          from   monthly_extract_st st\n                         where st.hh_load_sec =  :2   and\n                              upper(st.st_statement_desc) = upper(mbe.statement_msg_default)\n                       )  \n                    )\n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setLong(2,loadSec);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"34com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1978^11*/
	  
		  resultSet = it.getResultSet();
		  while ( resultSet.next() )
          {
			  expAchBuyback  += resultSet.getDouble("ach_tran_per_item") * tranCount
                + resultSet.getDouble("ach_tran_rate") * 0.01   * tranVol
                + resultSet.getDouble("ach_return_per_item")    * retCount
                + resultSet.getDouble("ach_return_rate") * 0.01 * retVol
                + resultSet.getDouble("ach_unauth_per_item")    * unAuthCount
                + resultSet.getDouble("ach_unauth_rate") * 0.01 * unAuthVol
                + resultSet.getDouble("ach_monthly_service_fee")
                + resultSet.getDouble("ach_smartpay_exp_mth_fee")
                + resultSet.getDouble("ach_smartpay_exp_setup_fee")
                + resultSet.getDouble("ach_hostedpay_mth_fee");
			  expAchBuybackDisc += resultSet.getDouble("ach_tran_per_item") * tranCount
		                + resultSet.getDouble("ach_tran_rate") * 0.01   * tranVol;
          }
		  it.close();
		  resultSet.close();
		  
	  }
      catch (Exception e)
	  {
    	  expAchBuyback     = 0.0;
    	  expAchBuybackDisc = 0.0;
	  }
	  finally
	  {
       try{ it.close(); } catch(Exception e){}
	  }
}
  
  
  private DebitBetData loadDebitBet( int bankNumber, int betNumber, Date activeDate )
  {
    ResultSetIterator   it        = null;
    DebitBetData        retVal    = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2019^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bet.income_per_item               as per_item_inc,
//                  bet.segment_id                    as cat,
//                  decode( bet.segment_type,
//                          '01', :DB_SALES,
//                          '02', :DB_CREDITS,
//                          '03', :DB_CASH_BACK,
//                          :DB_INVALID )             as seg_type
//          from tsys_bet_file    bet
//          where bet.bank_number = :bankNumber and
//                bet.bet_number = :betNumber and
//                bet.active_date = :activeDate
//          order by cat, seg_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bet.income_per_item               as per_item_inc,\n                bet.segment_id                    as cat,\n                decode( bet.segment_type,\n                        '01',  :1  ,\n                        '02',  :2  ,\n                        '03',  :3  ,\n                         :4   )             as seg_type\n        from tsys_bet_file    bet\n        where bet.bank_number =  :5   and\n              bet.bet_number =  :6   and\n              bet.active_date =  :7  \n        order by cat, seg_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"35com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,DB_SALES);
   __sJT_st.setInt(2,DB_CREDITS);
   __sJT_st.setInt(3,DB_CASH_BACK);
   __sJT_st.setInt(4,DB_INVALID);
   __sJT_st.setInt(5,bankNumber);
   __sJT_st.setInt(6,betNumber);
   __sJT_st.setDate(7,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"35com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2033^7*/
      retVal = new DebitBetData( bankNumber, betNumber, activeDate, 
                                  it.getResultSet() );
      it.close();
    }
    catch( Exception e )
    {
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
    return( retVal );
  }
  
  private synchronized boolean loadDebitCountSummary( String loadFilename, long recId )
  {
    Date                  activeDate      = null;
    int                   bankNumber      = 0;
    DebitBetData          bet             = null;
    NumberFormat          catFormat       = NumberFormat.getNumberInstance();
    int[]                 creditsCounts   = new int[10];
    long                  incCredits      = 0;
    long                  incSales        = 0;
    ResultSetIterator     it              = null;
    int                   itemCount       = 0;
    long                  loadFileId      = 0L;
    long                  perItemCredits  = 0;
    long                  perItemSales    = 0;
    ResultSet             resultSet       = null;
    boolean               retVal          = false;
    int[]                 salesCounts     = new int[10];
    
    try
    {
      loadFileId = loadFilenameToLoadFileId(loadFilename);
      
      /*@lineinfo:generated-code*//*@lineinfo:2070^7*/

//  ************************************************************
//  #sql [Ctx] { select  distinct active_date,bank_number 
//          from    monthly_extract_summary sm
//          where   sm.load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct active_date,bank_number  \n        from    monthly_extract_summary sm\n        where   sm.load_file_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"36com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activeDate = (java.sql.Date)__sJT_rs.getDate(1);
   bankNumber = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2075^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:2077^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(bet.bank_number)    
//          from    tsys_bet_file     bet
//          where   bet.bank_number = :bankNumber and
//                  bet.active_date = :activeDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(bet.bank_number)     \n        from    tsys_bet_file     bet\n        where   bet.bank_number =  :1   and\n                bet.active_date =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"37com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2083^7*/
      
      if ( itemCount == 0 )
      {
        // reset the entry to load next time
        /*@lineinfo:generated-code*//*@lineinfo:2088^9*/

//  ************************************************************
//  #sql [Ctx] { update merch_prof_process
//            set process_sequence = 0,
//                process_begin_date = null
//            where rec_id = :recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update merch_prof_process\n          set process_sequence = 0,\n              process_begin_date = null\n          where rec_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"38com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2094^9*/
      }
      else
      {
        catFormat.setMinimumIntegerDigits(2);
      
        //
        // process the update
        //
        // notes:
        //
        // * debit table is oriented horizontally (one col per cat/type)
        // * bet table is oriented vertically (one row per cat/type)
        //
        // Because of this it was easier to have a stored function
        // perform the task of looking up the per item rate for the
        // particular cat/type combination.  
        //
        /*@lineinfo:generated-code*//*@lineinfo:2112^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  dn.hh_load_sec                      as load_sec,
//                    dn.n1_bet_table_number              as bet_number,
//                    dn.n1_inc_cat_01_sales              as inc_sales_01,
//                    dn.n1_inc_cat_01_credit             as inc_credits_01,
//                    dn.n1_inc_cat_02_sales              as inc_sales_02,
//                    dn.n1_inc_cat_02_credit             as inc_credits_02,
//                    dn.n1_inc_cat_03_sales              as inc_sales_03,
//                    dn.n1_inc_cat_03_credit             as inc_credits_03,
//                    dn.n1_inc_cat_04_sales              as inc_sales_04,
//                    dn.n1_inc_cat_04_credit             as inc_credits_04,
//                    dn.n1_inc_cat_05_sales              as inc_sales_05,
//                    dn.n1_inc_cat_05_credit             as inc_credits_05,
//                    dn.n1_inc_cat_06_sales              as inc_sales_06,
//                    dn.n1_inc_cat_06_credit             as inc_credits_06,
//                    dn.n1_inc_cat_07_sales              as inc_sales_07,
//                    dn.n1_inc_cat_07_credit             as inc_credits_07,
//                    dn.n1_inc_cat_08_sales              as inc_sales_08,
//                    dn.n1_inc_cat_08_credit             as inc_credits_08,
//                    dn.n1_inc_cat_09_sales              as inc_sales_09,
//                    dn.n1_inc_cat_09_credit             as inc_credits_09,
//                    dn.n1_inc_cat_10_sales              as inc_sales_10,
//                    dn.n1_inc_cat_10_credit             as inc_credits_10
//            from    monthly_extract_summary   sm,
//                    monthly_extract_dn        dn
//            where   sm.load_file_id = :loadFileId and
//                    dn.hh_load_sec = sm.hh_load_sec 
//            order by dn.n1_bet_table_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dn.hh_load_sec                      as load_sec,\n                  dn.n1_bet_table_number              as bet_number,\n                  dn.n1_inc_cat_01_sales              as inc_sales_01,\n                  dn.n1_inc_cat_01_credit             as inc_credits_01,\n                  dn.n1_inc_cat_02_sales              as inc_sales_02,\n                  dn.n1_inc_cat_02_credit             as inc_credits_02,\n                  dn.n1_inc_cat_03_sales              as inc_sales_03,\n                  dn.n1_inc_cat_03_credit             as inc_credits_03,\n                  dn.n1_inc_cat_04_sales              as inc_sales_04,\n                  dn.n1_inc_cat_04_credit             as inc_credits_04,\n                  dn.n1_inc_cat_05_sales              as inc_sales_05,\n                  dn.n1_inc_cat_05_credit             as inc_credits_05,\n                  dn.n1_inc_cat_06_sales              as inc_sales_06,\n                  dn.n1_inc_cat_06_credit             as inc_credits_06,\n                  dn.n1_inc_cat_07_sales              as inc_sales_07,\n                  dn.n1_inc_cat_07_credit             as inc_credits_07,\n                  dn.n1_inc_cat_08_sales              as inc_sales_08,\n                  dn.n1_inc_cat_08_credit             as inc_credits_08,\n                  dn.n1_inc_cat_09_sales              as inc_sales_09,\n                  dn.n1_inc_cat_09_credit             as inc_credits_09,\n                  dn.n1_inc_cat_10_sales              as inc_sales_10,\n                  dn.n1_inc_cat_10_credit             as inc_credits_10\n          from    monthly_extract_summary   sm,\n                  monthly_extract_dn        dn\n          where   sm.load_file_id =  :1   and\n                  dn.hh_load_sec = sm.hh_load_sec \n          order by dn.n1_bet_table_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"39com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2141^9*/
        resultSet = it.getResultSet();
        
        while( resultSet.next() )
        {
          if ( bet == null || resultSet.getInt("bet_number") != bet.getBetNumber() )
          {
            bet = loadDebitBet(bankNumber,resultSet.getInt("bet_number"),activeDate);
          }
          if ( bet == null )
          {
            logEntry( "loadDebitCountSummary()", 
                      "Failed to load bet " + resultSet.getInt("bet_number") +
                      " for bank " + bankNumber );
            continue;
          }
          
          // calculate all the data
          for( int cat = 0; cat < 10; ++cat )
          {
            incSales        = MesMath.doubleToLong(resultSet.getDouble("inc_sales_" + catFormat.format(cat+1)));
            incCredits      = MesMath.doubleToLong(resultSet.getDouble("inc_credits_" + catFormat.format(cat+1)));
            perItemSales    = MesMath.doubleToLong(bet.getIncomePerItem(cat+1,DB_SALES));
            perItemCredits  = MesMath.doubleToLong(bet.getIncomePerItem(cat+1,DB_CREDITS));
            
            if( perItemSales == 0 )
            {
              salesCounts[cat] = 0;
            }
            else
            {
              salesCounts[cat] = (int)(incSales/perItemSales);
            }
            if( perItemCredits == 0 )
            {
              creditsCounts[cat] = 0;
            }
            else
            {
              creditsCounts[cat] = (int)(incCredits/perItemCredits);
            }
          }
          
          /*@lineinfo:generated-code*//*@lineinfo:2184^11*/

//  ************************************************************
//  #sql [Ctx] { update monthly_extract_dn
//              set cat_01_sales_count = :salesCounts[0],
//                  cat_02_sales_count = :salesCounts[1],
//                  cat_03_sales_count = :salesCounts[2],
//                  cat_04_sales_count = :salesCounts[3],
//                  cat_05_sales_count = :salesCounts[4],
//                  cat_06_sales_count = :salesCounts[5],
//                  cat_07_sales_count = :salesCounts[6],
//                  cat_08_sales_count = :salesCounts[7],
//                  cat_09_sales_count = :salesCounts[8],
//                  cat_10_sales_count = :salesCounts[9],
//                  cat_01_credits_count = :creditsCounts[0],
//                  cat_02_credits_count = :creditsCounts[1],
//                  cat_03_credits_count = :creditsCounts[2],
//                  cat_04_credits_count = :creditsCounts[3],
//                  cat_05_credits_count = :creditsCounts[4],
//                  cat_06_credits_count = :creditsCounts[5],
//                  cat_07_credits_count = :creditsCounts[6],
//                  cat_08_credits_count = :creditsCounts[7],
//                  cat_09_credits_count = :creditsCounts[8],
//                  cat_10_credits_count = :creditsCounts[9],
//                  cat_01_total_count = :salesCounts[0] + creditsCounts[0],
//                  cat_02_total_count = :salesCounts[1] + creditsCounts[1],
//                  cat_03_total_count = :salesCounts[2] + creditsCounts[2],
//                  cat_04_total_count = :salesCounts[3] + creditsCounts[3],
//                  cat_05_total_count = :salesCounts[4] + creditsCounts[4],
//                  cat_06_total_count = :salesCounts[5] + creditsCounts[5],
//                  cat_07_total_count = :salesCounts[6] + creditsCounts[6],
//                  cat_08_total_count = :salesCounts[7] + creditsCounts[7],
//                  cat_09_total_count = :salesCounts[8] + creditsCounts[8],
//                  cat_10_total_count = :salesCounts[9] + creditsCounts[9]
//              where hh_load_sec = :resultSet.getLong("load_sec")                
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_130 = salesCounts[0];
 int __sJT_131 = salesCounts[1];
 int __sJT_132 = salesCounts[2];
 int __sJT_133 = salesCounts[3];
 int __sJT_134 = salesCounts[4];
 int __sJT_135 = salesCounts[5];
 int __sJT_136 = salesCounts[6];
 int __sJT_137 = salesCounts[7];
 int __sJT_138 = salesCounts[8];
 int __sJT_139 = salesCounts[9];
 int __sJT_140 = creditsCounts[0];
 int __sJT_141 = creditsCounts[1];
 int __sJT_142 = creditsCounts[2];
 int __sJT_143 = creditsCounts[3];
 int __sJT_144 = creditsCounts[4];
 int __sJT_145 = creditsCounts[5];
 int __sJT_146 = creditsCounts[6];
 int __sJT_147 = creditsCounts[7];
 int __sJT_148 = creditsCounts[8];
 int __sJT_149 = creditsCounts[9];
 int __sJT_150 = salesCounts[0] + creditsCounts[0];
 int __sJT_151 = salesCounts[1] + creditsCounts[1];
 int __sJT_152 = salesCounts[2] + creditsCounts[2];
 int __sJT_153 = salesCounts[3] + creditsCounts[3];
 int __sJT_154 = salesCounts[4] + creditsCounts[4];
 int __sJT_155 = salesCounts[5] + creditsCounts[5];
 int __sJT_156 = salesCounts[6] + creditsCounts[6];
 int __sJT_157 = salesCounts[7] + creditsCounts[7];
 int __sJT_158 = salesCounts[8] + creditsCounts[8];
 int __sJT_159 = salesCounts[9] + creditsCounts[9];
 long __sJT_160 = resultSet.getLong("load_sec");
   String theSqlTS = "update monthly_extract_dn\n            set cat_01_sales_count =  :1  ,\n                cat_02_sales_count =  :2  ,\n                cat_03_sales_count =  :3  ,\n                cat_04_sales_count =  :4  ,\n                cat_05_sales_count =  :5  ,\n                cat_06_sales_count =  :6  ,\n                cat_07_sales_count =  :7  ,\n                cat_08_sales_count =  :8  ,\n                cat_09_sales_count =  :9  ,\n                cat_10_sales_count =  :10  ,\n                cat_01_credits_count =  :11  ,\n                cat_02_credits_count =  :12  ,\n                cat_03_credits_count =  :13  ,\n                cat_04_credits_count =  :14  ,\n                cat_05_credits_count =  :15  ,\n                cat_06_credits_count =  :16  ,\n                cat_07_credits_count =  :17  ,\n                cat_08_credits_count =  :18  ,\n                cat_09_credits_count =  :19  ,\n                cat_10_credits_count =  :20  ,\n                cat_01_total_count =  :21  ,\n                cat_02_total_count =  :22  ,\n                cat_03_total_count =  :23  ,\n                cat_04_total_count =  :24  ,\n                cat_05_total_count =  :25  ,\n                cat_06_total_count =  :26  ,\n                cat_07_total_count =  :27  ,\n                cat_08_total_count =  :28  ,\n                cat_09_total_count =  :29  ,\n                cat_10_total_count =  :30  \n            where hh_load_sec =  :31 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"40com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_130);
   __sJT_st.setInt(2,__sJT_131);
   __sJT_st.setInt(3,__sJT_132);
   __sJT_st.setInt(4,__sJT_133);
   __sJT_st.setInt(5,__sJT_134);
   __sJT_st.setInt(6,__sJT_135);
   __sJT_st.setInt(7,__sJT_136);
   __sJT_st.setInt(8,__sJT_137);
   __sJT_st.setInt(9,__sJT_138);
   __sJT_st.setInt(10,__sJT_139);
   __sJT_st.setInt(11,__sJT_140);
   __sJT_st.setInt(12,__sJT_141);
   __sJT_st.setInt(13,__sJT_142);
   __sJT_st.setInt(14,__sJT_143);
   __sJT_st.setInt(15,__sJT_144);
   __sJT_st.setInt(16,__sJT_145);
   __sJT_st.setInt(17,__sJT_146);
   __sJT_st.setInt(18,__sJT_147);
   __sJT_st.setInt(19,__sJT_148);
   __sJT_st.setInt(20,__sJT_149);
   __sJT_st.setInt(21,__sJT_150);
   __sJT_st.setInt(22,__sJT_151);
   __sJT_st.setInt(23,__sJT_152);
   __sJT_st.setInt(24,__sJT_153);
   __sJT_st.setInt(25,__sJT_154);
   __sJT_st.setInt(26,__sJT_155);
   __sJT_st.setInt(27,__sJT_156);
   __sJT_st.setInt(28,__sJT_157);
   __sJT_st.setInt(29,__sJT_158);
   __sJT_st.setInt(30,__sJT_159);
   __sJT_st.setLong(31,__sJT_160);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2218^11*/
        }
        retVal = true;
      }
    }
    catch( Exception e )
    {
      int loadSec = 0;
      try{ loadSec = resultSet.getInt("load_sec"); } catch(Exception ee){}
      logEntry( "loadDebitCountSummary(" + loadFilename + " )", "load_sec: " + loadSec + "; " + e.toString() );
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
    return( retVal );
  }
  
  private boolean loadDiscoverEvergreenSummary( String loadFilename )
  {
    Date                                      activeDate      = null;
    DiscoverEvergreenDataBean                 dataBean        = null;
    long                                      loadFileId      = 0L;
    long                                      nodeId          = 0L;
    boolean                                   retVal          = false;
    DiscoverEvergreenDataBean.RowDetailData   row             = null;
    Vector                                    rows            = null;
    
    try
    {
      nodeId = MesDefaults.getLong(MesDefaults.DK_DISCOVER_TOP_NODE);
//      #sql [Ctx]
//      {
//        select value into :nodeId
//        from   mes_defaults
//        where  key = :(MesDefaults.DK_DISCOVER_TOP_NODE.intValue())
//      };
      
      /*@lineinfo:generated-code*//*@lineinfo:2256^7*/

//  ************************************************************
//  #sql [Ctx] { select  distinct active_date  
//          from    monthly_extract_summary sm
//          where   sm.load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct active_date   \n        from    monthly_extract_summary sm\n        where   sm.load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"41com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activeDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2261^7*/

      // make sure there is an index for this filename
      /*@lineinfo:generated-code*//*@lineinfo:2264^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init( :loadFilename )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init(  :1   )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"42com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2267^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:2269^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:loadFilename) 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1  )  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"43com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2273^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:2275^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from    discover_evergreen_summary    es
//          where   es.load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from    discover_evergreen_summary    es\n        where   es.load_file_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"44com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2280^7*/
      
      dataBean = new DiscoverEvergreenDataBean();
      
      dataBean.connect(true);
      dataBean.setReportType(ReportSQLJBean.RT_DETAILS);
      dataBean.loadData( nodeId, activeDate );
      rows = dataBean.getReportRows();
      
      for( int i = 0; i < rows.size(); ++i )
      {
        row = (DiscoverEvergreenDataBean.RowDetailData)rows.elementAt(i);
        
        /*@lineinfo:generated-code*//*@lineinfo:2293^9*/

//  ************************************************************
//  #sql [Ctx] { insert into discover_evergreen_summary
//            (
//              merchant_number,
//              active_date,
//              association_node,
//              vmc_vol_amount,
//              vmc_vol_amount_qtd,
//              vmc_vol_amount_ytd,
//              referral_per_item_amount,
//              referral_volume_amount,
//              load_filename,
//              load_file_id
//            )
//            values
//            (
//              :row.MerchantId,
//              :activeDate,
//              :row.AssociationNode,
//              :row.VmcVolAmount,
//              :row.VmcVolAmountQTD,
//              :row.VmcVolAmountYTD,
//              :row.RebatePerItemAmount,
//              :row.RebateVolumeAmount,
//              :loadFilename,
//              :loadFileId
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into discover_evergreen_summary\n          (\n            merchant_number,\n            active_date,\n            association_node,\n            vmc_vol_amount,\n            vmc_vol_amount_qtd,\n            vmc_vol_amount_ytd,\n            referral_per_item_amount,\n            referral_volume_amount,\n            load_filename,\n            load_file_id\n          )\n          values\n          (\n             :1  ,\n             :2  ,\n             :3  ,\n             :4  ,\n             :5  ,\n             :6  ,\n             :7  ,\n             :8  ,\n             :9  ,\n             :10  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"45com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,row.MerchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setLong(3,row.AssociationNode);
   __sJT_st.setDouble(4,row.VmcVolAmount);
   __sJT_st.setDouble(5,row.VmcVolAmountQTD);
   __sJT_st.setDouble(6,row.VmcVolAmountYTD);
   __sJT_st.setDouble(7,row.RebatePerItemAmount);
   __sJT_st.setDouble(8,row.RebateVolumeAmount);
   __sJT_st.setString(9,loadFilename);
   __sJT_st.setLong(10,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2321^9*/
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:2324^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2327^7*/
      retVal = true;
    }
    catch( Exception e )
    {
      logEntry("loadDiscoverEvergreenSummary(" + loadFilename + ")", e.toString() );
    }
    finally
    {
      try{ dataBean.cleanUp(); } catch(Exception e){}
    }
    return( retVal );
  }
  
  private boolean loadDiscoverExtractSummary( String loadFilename )
  {
    DiscoverExtractSummary          des             = null;
    boolean                         fileProcessed   = false;

    try
    {
      des = new DiscoverExtractSummary();  

      des.connect(true);
      if ( des.loadDiscoverExtractSummary(loadFilename) == true )
      {
        fileProcessed = true;
      }
    }
    catch( Exception e )
    {
      logEntry("loadDiscoverExtractSummary()",e.toString());
    }
    finally
    {
      try{ des.cleanUp(); } catch(Exception e){}
      des = null;
    }
    return( fileProcessed );
  }
  
  private void loadEpicorAP( int bankNumber, Date activeDate )
  {
    ResultSetIterator         it          = null;
    ResultSet                 resultSet   = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:2375^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct load_filename
//          from    monthly_extract_summary   sm
//          where   sm.bank_number = :bankNumber
//                  and sm.active_date = :activeDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct load_filename\n        from    monthly_extract_summary   sm\n        where   sm.bank_number =  :1  \n                and sm.active_date =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"46com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,activeDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"46com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2381^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        String loadFilename = resultSet.getString("load_filename");
        System.out.println("Loading Epicor entries for '" + loadFilename + "'");
        loadEpicorAP(loadFilename,0L);
      }
      resultSet.close();
    }
    catch( Exception e )
    {
      logEntry("loadEpicorAP(" + bankNumber + "," + activeDate + ")", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception ee){}
    }
  }
  
  private boolean loadEpicorAP( String loadFilename, long reqNodeId )
  {
    Date                  activeDate      = null;
    double                amount          = 0.0;
    BankContractDataBean  dataBean        = null;
    boolean               fileProcessed   = false;
    ResultSetIterator     it              = null;
    long                  loadFileId      = 0L;
    String                monthString     = null;
    double                netRevenue      = 0.0;
    long                  nodeId          = 0L;
    ResultSet             nodeSet         = null;
    double                partnership     = 0.0;
    double                procFees        = 0.0;
    int                   recCount        = 0;
    double                referralFees    = 0.0;
    ResultSet             resultSet       = null;
    SummaryData           row             = null;
    Vector                rows            = null;
    long                  seqNum          = 0L;
    String                sqlText         = null;
    int                   userType        = -1;
    String                vendorName      = null;
    String                vendorNode      = null;
    Date                  voucherDate     = null;
    String                voucherNumber   = null;
    
    try
    {
      dataBean = new BankContractDataBean();
      dataBean.connect(true);
      
      // extract the active date for this file
      /*@lineinfo:generated-code*//*@lineinfo:2438^7*/

//  ************************************************************
//  #sql [Ctx] { select  distinct sm.active_date,
//                  last_day(sm.active_date)
//          
//          from    load_file_index         lfi,
//                  monthly_extract_summary sm
//          where   lfi.load_filename = :loadFilename and
//                  sm.load_file_id = lfi.load_file_id                  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct sm.active_date,\n                last_day(sm.active_date)\n         \n        from    load_file_index         lfi,\n                monthly_extract_summary sm\n        where   lfi.load_filename =  :1   and\n                sm.load_file_id = lfi.load_file_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"47com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activeDate = (java.sql.Date)__sJT_rs.getDate(1);
   voucherDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2447^7*/
      
      // set the bean dates
      dataBean.setReportDateBegin(activeDate);
      dataBean.setReportDateEnd(activeDate);
      
      {
    	  // declare temps
    	  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
    	  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
    	  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
    	  try {
    	   String theSqlTS = "select distinct addr_sort1 from mes.apmaster";
    	   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"201com.mes.startup.MerchProfLoader",theSqlTS);
    	   // execute query
    	   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"201com.mes.startup.MerchProfLoader",null));
    	  } finally { __sJT_ec.oracleCloseQuery(); }
      }
      
      nodeSet = it.getResultSet();
      
      while( nodeSet.next() )
      {
        try 
        {
          nodeId = Long.parseLong( nodeSet.getString("addr_sort1") );
          
          /*@lineinfo:generated-code*//*@lineinfo:2468^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(o.org_group)  
//              from    organization    o
//              where   o.org_group = :nodeId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(o.org_group)   \n            from    organization    o\n            where   o.org_group =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"48com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2473^11*/
        }
        catch( Exception ee )
        {
          // non-numeric 
          recCount = 0;
        }
        
        if ( ( recCount == 0 ) ||
             ( (reqNodeId != 0) && (reqNodeId != nodeId) ) )
        {
          continue;   // skip this record, node does not exist
        }
        
        log.debug("Exporting data for " + nodeId);//@
        
        // reset values
        netRevenue    = 0.0;
        procFees      = 0.0;
        referralFees  = 0.0;
        partnership   = 0.0;
        
        // set bean values and load new data
        dataBean.setReportHierarchyNodeDefault(nodeId);
        dataBean.setReportHierarchyNode(nodeId);
        dataBean.loadData();
        
        rows      = dataBean.getReportRows();
        userType  = dataBean.getUserType();
        
        // accumulate the data for all child rows (output of report)
        for( int rowIdx = 0; rowIdx < rows.size(); ++rowIdx )
        {
          row = (SummaryData)rows.elementAt(rowIdx);
          
          netRevenue    += row.getNetRevenue();
          procFees      += ( row.getProcessingFees() + 
                             row.getEquipmentExpense() );
          referralFees  += row.getReferralContractExpense();
          partnership   += row.getPartnershipExpense();
        }
        
        log.debug("netRevenue  = " + netRevenue);
        log.debug("procFees    = " + procFees);
        log.debug("referralFees= " + referralFees);
        log.debug("partnership = " + partnership);
        
        // if this is a partnership relationship then adjust
        // the numbers to allow for a single entry.
        if ( partnership != 0.0 )
        {
          partnership   = ( ( netRevenue + referralFees ) - (procFees + partnership) );
          netRevenue    = 0.0;
          procFees      = 0.0;
          referralFees  = 0.0;
          
          log.debug("netRevenue  = " + netRevenue);
          log.debug("procFees    = " + procFees);
          log.debug("referralFees= " + referralFees);
        }
        
        if ( sqlText == null )
        {
          sqlText = "   delete                                          "
                  + "   from    mes.esc_voucher_staging  "
                  + "   where   voucher_date = :1                        ";

          if ( ForceDelete == false )    
          {
            // only dump existing records that have not been processed
            sqlText += " and process_status_code = 0 ";
          }
          {
        	  // declare temps
        	  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
        	  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
        	  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
        	   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"202com.mes.startup.MerchProfLoader", sqlText);
        	   // set IN parameters
        	   __sJT_st.setDate(1,voucherDate);
        	  // execute statement
        	   __sJT_ec.oracleExecuteBatchableUpdate();
          }
          
          // setup the insert statement
          sqlText = "   insert into mes.esc_voucher_staging  " 
                  + "   (                                                   " 
                  + "     Import_ID,                                        " 
                  + "     Import_Date,                                      " 
                  + "     Process_Status_Code,                              " 
                  + "     Vendor_code,                                      " 
                  + "     Voucher_Number,                                   " 
                  + "     Voucher_Date,                                     " 
                  + "     Voucher_Detail_Amount,                            " 
                  + "     Voucher_Detail_Description                        " 
                  + "   )                                                   " 
                  + "   values                                              " 
                  + "   ( :1,sysdate,0,:2,:3,:4,:5,:6 )                         ";
        }
        monthString = DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy");
        
        /*@lineinfo:generated-code*//*@lineinfo:2569^9*/

//  ************************************************************
//  #sql [Ctx] { select  o.org_group                     as vendor_node,
//                    o.org_name                      as vendor_name,
//                    (
//                      o.org_group || ' ' ||
//                      to_char(:activeDate,'MONyy')
//                    )                               as voucher_number
//                              
//            from    organization                  o
//            where   o.org_group = :nodeId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  o.org_group                     as vendor_node,\n                  o.org_name                      as vendor_name,\n                  (\n                    o.org_group || ' ' ||\n                    to_char( :1  ,'MONyy')\n                  )                               as voucher_number\n                             \n          from    organization                  o\n          where   o.org_group =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"49com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setLong(2,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vendorNode = (String)__sJT_rs.getString(1);
   vendorName = (String)__sJT_rs.getString(2);
   voucherNumber = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2580^9*/
        // post net revenue for contract banks
        if ( netRevenue != 0.0 )
        {
          // insert the net revenue line
          /*@lineinfo:generated-code*//*@lineinfo:2591^11*/

//  ************************************************************
//  #sql [Ctx] { select  epicor_import_sequence.nextval 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  epicor_import_sequence.nextval  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"50com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   seqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

	/*@lineinfo:user-code*//*@lineinfo:2595^11*/
	{
		// declare temps
		oracle.jdbc.OraclePreparedStatement __sJT_st = null;
		sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
		sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
		__sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"203com.mes.startup.MerchProfLoader", sqlText);
		// set IN parameters
		__sJT_st.setLong  (1, seqNum);
		__sJT_st.setString(2, vendorNode );
		__sJT_st.setString(3, voucherNumber );
		__sJT_st.setDate  (4, voucherDate );
		__sJT_st.setDouble(5, netRevenue );
		__sJT_st.setString(6, (monthString + " Net Revenue") );
		// execute statement
		__sJT_ec.oracleExecuteBatchableUpdate();
	}

        }
        
        if ( partnership != 0.0 ) // MES partnership
        {
          /*@lineinfo:generated-code*//*@lineinfo:2604^11*/

//  ************************************************************
//  #sql [Ctx] { select  epicor_import_sequence.nextval 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  epicor_import_sequence.nextval  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"51com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   seqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2608^11*/
{
	  // declare temps
	  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
	  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
	  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
	   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"204com.mes.startup.MerchProfLoader", sqlText);
	   // set IN parameters
	   __sJT_st.setLong  (1, seqNum);
	   __sJT_st.setString(2, vendorNode );
	   __sJT_st.setString(3, voucherNumber );
	   __sJT_st.setDate  (4, voucherDate );
	   __sJT_st.setDouble(5, partnership );
	   __sJT_st.setString(6, (monthString + " Partnership Fees") );
	  // execute statement
	   __sJT_ec.oracleExecuteBatchableUpdate();
}

        }
        else if ( procFees != 0.0 )  // liability contract
        {          
          // insert the liability bank offset line
          /*@lineinfo:generated-code*//*@lineinfo:2617^11*/

//  ************************************************************
//  #sql [Ctx] { select  epicor_import_sequence.nextval 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  epicor_import_sequence.nextval  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"52com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   seqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2621^11*/
          {
        	  // declare temps
        	  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
        	  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
        	  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
        	   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"205com.mes.startup.MerchProfLoader", sqlText);
        	   // set IN parameters
        	   __sJT_st.setLong  (1, seqNum);
        	   __sJT_st.setString(2, vendorNode );
        	   __sJT_st.setString(3, voucherNumber );
        	   __sJT_st.setDate  (4, voucherDate );
        	   __sJT_st.setDouble(5, (-1 * (procFees - referralFees))  );
        	   __sJT_st.setString(6, (monthString + " Processing Fees"));
        	  // execute statement
        	   __sJT_ec.oracleExecuteBatchableUpdate();
          }
        }    
        else if ( referralFees != 0.0 ) // no liability, referral only
        {
          /*@lineinfo:generated-code*//*@lineinfo:2632^11*/

//  ************************************************************
//  #sql [Ctx] { select  epicor_import_sequence.nextval 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  epicor_import_sequence.nextval  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"53com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   seqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2636^11*/
{
	  // declare temps
	  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
	  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
	  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
	   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"206com.mes.startup.MerchProfLoader", sqlText);
	   // set IN parameters
	   __sJT_st.setLong  (1, seqNum);
	   __sJT_st.setString(2, vendorNode );
	   __sJT_st.setString(3, voucherNumber );
	   __sJT_st.setDate  (4, voucherDate );
	   __sJT_st.setDouble(5, referralFees  );
	   __sJT_st.setString(6, (monthString + " Referral Fees"));
	  // execute statement
	   __sJT_ec.oracleExecuteBatchableUpdate();
}
        }
      }        
      nodeSet.close();
      
      fileProcessed = true;
    }
    catch( Exception e )
    {
      logEntry( "loadEpicorAP( " + loadFilename + ", " + nodeId + " )",
                e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
    return( fileProcessed );
  }
  
  private boolean loadEquipSummary( String loadFilename, long nodeId )
  {
    Date                  activeDate        = null;
    int                   bankNumber        = 0;
    ResultSetIterator     it                = null;
    long                  loadFileId        = 0L;
    Date                  monthBeginDate    = null;
    Date                  monthEndDate      = null;
    String                progress          = null;
    ResultSet             resultSet         = null;
    boolean               retVal            = true;
    
    try
    {
      setConsoleTrace(true);//@ debug trace
      trace("Loading bank number and active date for " + loadFilename );
      
      /*@lineinfo:generated-code*//*@lineinfo:2994^7*/

//  ************************************************************
//  #sql [Ctx] { select  lfi.load_file_id    
//          from    load_file_index         lfi
//          where   lfi.load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  lfi.load_file_id     \n        from    load_file_index         lfi\n        where   lfi.load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"61com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2999^7*/
      
      // get bank number and active date to use in the queries
      // to make them easier to debug if necessary.
      /*@lineinfo:generated-code*//*@lineinfo:3003^7*/

//  ************************************************************
//  #sql [Ctx] { select  sm.bank_number, sm.active_date,
//                  min(month_begin_date), max(month_end_date)
//          
//          from    monthly_extract_summary sm
//          where   sm.load_file_id = :loadFileId
//          group by sm.bank_number, sm.active_date      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sm.bank_number, sm.active_date,\n                min(month_begin_date), max(month_end_date)\n         \n        from    monthly_extract_summary sm\n        where   sm.load_file_id =  :1  \n        group by sm.bank_number, sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"62com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   activeDate = (java.sql.Date)__sJT_rs.getDate(2);
   monthBeginDate = (java.sql.Date)__sJT_rs.getDate(3);
   monthEndDate = (java.sql.Date)__sJT_rs.getDate(4);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3011^7*/
      
      //********************************************************************
      // Equipment Rental base cost for MES
      //********************************************************************
      /*@lineinfo:generated-code*//*@lineinfo:3016^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                              as merchant_number,
//                  round(sum(pp.part_price/36),2)                  as rent_cost
//          from    monthly_extract_summary sm,
//                  tmg_deployment          dp,
//                  tmg_orders              o,
//                  tmg_part_states         ps,
//                  tmg_part_prices         pp
//          where   sm.load_file_id = :loadFileId and
//                  ( :nodeId = 0 or 
//                    exists 
//                      ( select gm.merchant_number
//                        from    organization    o,
//                                group_merchant  gm
//                        where   o.org_group = :nodeId and
//                                gm.org_num = o.org_num and
//                                gm.merchant_number = sm.merchant_number
//                       )
//                  ) and
//                  dp.merch_num = sm.merchant_number and
//                  trunc(dp.start_ts) between :monthBeginDate and :monthEndDate and
//                  dp.start_code in ( 'ADD','NEW' ) and
//                  o.ord_id = dp.start_ord_id and
//                  o.ord_stat_code = 'DONE' and
//                  ps.deploy_id = dp.deploy_id and
//                  ps.disp_code = 'RENT' and
//                  ps.state_id =
//                  (
//                    select      min( psin.state_id ) 
//                    from        tmg_part_states psin 
//                    where       psin.deploy_id = dp.deploy_id
//                  ) and
//                  pp.part_id = ps.part_id
//          group by sm.merchant_number      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                              as merchant_number,\n                round(sum(pp.part_price/36),2)                  as rent_cost\n        from    monthly_extract_summary sm,\n                tmg_deployment          dp,\n                tmg_orders              o,\n                tmg_part_states         ps,\n                tmg_part_prices         pp\n        where   sm.load_file_id =  :1   and\n                (  :2   = 0 or \n                  exists \n                    ( select gm.merchant_number\n                      from    organization    o,\n                              group_merchant  gm\n                      where   o.org_group =  :3   and\n                              gm.org_num = o.org_num and\n                              gm.merchant_number = sm.merchant_number\n                     )\n                ) and\n                dp.merch_num = sm.merchant_number and\n                trunc(dp.start_ts) between  :4   and  :5   and\n                dp.start_code in ( 'ADD','NEW' ) and\n                o.ord_id = dp.start_ord_id and\n                o.ord_stat_code = 'DONE' and\n                ps.deploy_id = dp.deploy_id and\n                ps.disp_code = 'RENT' and\n                ps.state_id =\n                (\n                  select      min( psin.state_id ) \n                  from        tmg_part_states psin \n                  where       psin.deploy_id = dp.deploy_id\n                ) and\n                pp.part_id = ps.part_id\n        group by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"63com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setLong(3,nodeId);
   __sJT_st.setDate(4,monthBeginDate);
   __sJT_st.setDate(5,monthEndDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"63com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3051^7*/
      resultSet = it.getResultSet();
      
      while(resultSet.next())
      {
        /*@lineinfo:generated-code*//*@lineinfo:3056^9*/

//  ************************************************************
//  #sql [Ctx] { update monthly_extract_summary
//            set    equip_rental_expense = :resultSet.getDouble("rent_cost")
//            where  merchant_number = :resultSet.getLong("merchant_number") and
//                   active_date = :activeDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_161 = resultSet.getDouble("rent_cost");
 long __sJT_162 = resultSet.getLong("merchant_number");
   String theSqlTS = "update monthly_extract_summary\n          set    equip_rental_expense =  :1  \n          where  merchant_number =  :2   and\n                 active_date =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"64com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_161);
   __sJT_st.setLong(2,__sJT_162);
   __sJT_st.setDate(3,activeDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3062^9*/
      }
      resultSet.close();
      it.close();
      
      //********************************************************************
      // Equipment Sales base cost for MES 
      //********************************************************************
      /*@lineinfo:generated-code*//*@lineinfo:3070^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number            as merchant_number,
//                  round(sum(pp.part_price),2)   as sales_cost
//          from    monthly_extract_summary sm,
//                  tmg_deployment          dp,
//                  tmg_orders              o,
//                  tmg_part_states         ps,
//                  tmg_part_prices         pp
//          where   sm.load_file_id = :loadFileId and
//                  ( :nodeId = 0 or 
//                    exists 
//                      ( select gm.merchant_number
//                        from    organization    o,
//                                group_merchant  gm
//                        where   o.org_group = :nodeId and
//                                gm.org_num = o.org_num and
//                                gm.merchant_number = sm.merchant_number
//                       )
//                  ) and
//                  dp.merch_num = sm.merchant_number and
//                  trunc(dp.start_ts) between :monthBeginDate and :monthEndDate and
//                  dp.start_code in ( 'ADD','NEW' ) and
//                  o.ord_id = dp.start_ord_id and
//                  o.ord_stat_code = 'DONE' and
//                  ps.deploy_id = dp.deploy_id and
//                  ps.disp_code = 'SOLD' and
//                  ps.state_id =
//                  (
//                    select      min( psin.state_id ) 
//                    from        tmg_part_states psin 
//                    where       psin.deploy_id = dp.deploy_id
//                  ) and
//                  pp.part_id = ps.part_id
//          group by sm.merchant_number      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number            as merchant_number,\n                round(sum(pp.part_price),2)   as sales_cost\n        from    monthly_extract_summary sm,\n                tmg_deployment          dp,\n                tmg_orders              o,\n                tmg_part_states         ps,\n                tmg_part_prices         pp\n        where   sm.load_file_id =  :1   and\n                (  :2   = 0 or \n                  exists \n                    ( select gm.merchant_number\n                      from    organization    o,\n                              group_merchant  gm\n                      where   o.org_group =  :3   and\n                              gm.org_num = o.org_num and\n                              gm.merchant_number = sm.merchant_number\n                     )\n                ) and\n                dp.merch_num = sm.merchant_number and\n                trunc(dp.start_ts) between  :4   and  :5   and\n                dp.start_code in ( 'ADD','NEW' ) and\n                o.ord_id = dp.start_ord_id and\n                o.ord_stat_code = 'DONE' and\n                ps.deploy_id = dp.deploy_id and\n                ps.disp_code = 'SOLD' and\n                ps.state_id =\n                (\n                  select      min( psin.state_id ) \n                  from        tmg_part_states psin \n                  where       psin.deploy_id = dp.deploy_id\n                ) and\n                pp.part_id = ps.part_id\n        group by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"65com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setLong(3,nodeId);
   __sJT_st.setDate(4,monthBeginDate);
   __sJT_st.setDate(5,monthEndDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"65com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3105^7*/
      resultSet = it.getResultSet();
      
      while(resultSet.next())
      {
        /*@lineinfo:generated-code*//*@lineinfo:3110^9*/

//  ************************************************************
//  #sql [Ctx] { update monthly_extract_summary
//            set    equip_sales_expense = :resultSet.getDouble("sales_cost")
//            where  merchant_number = :resultSet.getLong("merchant_number") and
//                   active_date = :activeDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_163 = resultSet.getDouble("sales_cost");
 long __sJT_164 = resultSet.getLong("merchant_number");
   String theSqlTS = "update monthly_extract_summary\n          set    equip_sales_expense =  :1  \n          where  merchant_number =  :2   and\n                 active_date =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"66com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_163);
   __sJT_st.setLong(2,__sJT_164);
   __sJT_st.setDate(3,activeDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3116^9*/
      }
      resultSet.close();
      it.close();
      
      //********************************************************************
      // determine the base cost of the equipment for sales commission
      // and banks who do not own equipment but get receive the mark-up
      // over base cost in their m/e payments.
      //********************************************************************
      /*@lineinfo:generated-code*//*@lineinfo:3126^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                    as merchant_number,
//                  sum( decode( ps.disp_code, 
//                               'RENT', 0,       -- exclude rentals 
//                               decode( ps.pc_code,
//                                       'NEW', nvl(ep.price_base,nvl(pp.part_price,0)),
//                                       nvl(ep.price_used,nvl(pp.part_price,0)) 
//                                     )  
//                             ) )                        as sales_cost,
//                  round(
//                  sum( decode( ps.disp_code, 
//                               'RENT', decode( ps.pc_code, -- only rentals      
//                                               'NEW', nvl(ep.price_rental,(nvl(pp.part_price,0)/36)),
//                                               nvl(ep.price_rental_used,(nvl(pp.part_price,0)/36)) 
//                                             ),
//                               0   
//                             ) ), 2 )                   as rental_cost
//          from    monthly_extract_summary sm,
//                  tmg_deployment          dp,
//                  tmg_orders              o,
//                  tmg_part_states         ps,
//                  tmg_part_types          pt,
//                  tmg_part_prices         pp,
//                  equip_price             ep
//          where   sm.load_file_id = :loadFileId and
//                  ( :nodeId = 0 or 
//                    exists 
//                      ( select gm.merchant_number
//                        from    organization    o,
//                                group_merchant  gm
//                        where   o.org_group = :nodeId and
//                                gm.org_num = o.org_num and
//                                gm.merchant_number = sm.merchant_number
//                       )
//                  ) and
//                  dp.merch_num = sm.merchant_number and
//                  dp.start_code in ( 'ADD','NEW' ) and
//                  ( 
//                    ( trunc(dp.start_ts) between :monthBeginDate and :monthEndDate and
//                      ps.disp_code = 'SOLD' ) or
//                    ( ps.disp_code = 'RENT' and
//                      trunc(dp.start_ts) <= :monthEndDate and
//                      dp.end_ts is null ) -- still deployed
//                  ) and
//                  o.ord_id = dp.start_ord_id and
//                  o.ord_stat_code = 'DONE' and
//                  ps.deploy_id = dp.deploy_id and
//                  ps.state_id =
//                  (
//                    select      min( psin.state_id ) 
//                    from        tmg_part_states psin 
//                    where       psin.deploy_id = dp.deploy_id
//                  ) and
//                  pt.pt_id = ps.pt_id and
//                  pp.part_id = ps.part_id and
//                  ep.rec_id = get_equip_price_rec_id(sm.merchant_number,pt.model_code,trunc(dp.start_ts))
//          group by sm.merchant_number      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                    as merchant_number,\n                sum( decode( ps.disp_code, \n                             'RENT', 0,       -- exclude rentals \n                             decode( ps.pc_code,\n                                     'NEW', nvl(ep.price_base,nvl(pp.part_price,0)),\n                                     nvl(ep.price_used,nvl(pp.part_price,0)) \n                                   )  \n                           ) )                        as sales_cost,\n                round(\n                sum( decode( ps.disp_code, \n                             'RENT', decode( ps.pc_code, -- only rentals      \n                                             'NEW', nvl(ep.price_rental,(nvl(pp.part_price,0)/36)),\n                                             nvl(ep.price_rental_used,(nvl(pp.part_price,0)/36)) \n                                           ),\n                             0   \n                           ) ), 2 )                   as rental_cost\n        from    monthly_extract_summary sm,\n                tmg_deployment          dp,\n                tmg_orders              o,\n                tmg_part_states         ps,\n                tmg_part_types          pt,\n                tmg_part_prices         pp,\n                equip_price             ep\n        where   sm.load_file_id =  :1   and\n                (  :2   = 0 or \n                  exists \n                    ( select gm.merchant_number\n                      from    organization    o,\n                              group_merchant  gm\n                      where   o.org_group =  :3   and\n                              gm.org_num = o.org_num and\n                              gm.merchant_number = sm.merchant_number\n                     )\n                ) and\n                dp.merch_num = sm.merchant_number and\n                dp.start_code in ( 'ADD','NEW' ) and\n                ( \n                  ( trunc(dp.start_ts) between  :4   and  :5   and\n                    ps.disp_code = 'SOLD' ) or\n                  ( ps.disp_code = 'RENT' and\n                    trunc(dp.start_ts) <=  :6   and\n                    dp.end_ts is null ) -- still deployed\n                ) and\n                o.ord_id = dp.start_ord_id and\n                o.ord_stat_code = 'DONE' and\n                ps.deploy_id = dp.deploy_id and\n                ps.state_id =\n                (\n                  select      min( psin.state_id ) \n                  from        tmg_part_states psin \n                  where       psin.deploy_id = dp.deploy_id\n                ) and\n                pt.pt_id = ps.pt_id and\n                pp.part_id = ps.part_id and\n                ep.rec_id = get_equip_price_rec_id(sm.merchant_number,pt.model_code,trunc(dp.start_ts))\n        group by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"67com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setLong(3,nodeId);
   __sJT_st.setDate(4,monthBeginDate);
   __sJT_st.setDate(5,monthEndDate);
   __sJT_st.setDate(6,monthEndDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"67com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3184^7*/
      resultSet = it.getResultSet();
      
      while(resultSet.next())
      {
        /*@lineinfo:generated-code*//*@lineinfo:3189^9*/

//  ************************************************************
//  #sql [Ctx] { update monthly_extract_summary
//            set    equip_sales_base_cost  = :resultSet.getDouble("sales_cost"),
//                   equip_rental_base_cost = :resultSet.getDouble("rental_cost")
//            where  merchant_number = :resultSet.getLong("merchant_number") and
//                   active_date = :activeDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_165 = resultSet.getDouble("sales_cost");
 double __sJT_166 = resultSet.getDouble("rental_cost");
 long __sJT_167 = resultSet.getLong("merchant_number");
   String theSqlTS = "update monthly_extract_summary\n          set    equip_sales_base_cost  =  :1  ,\n                 equip_rental_base_cost =  :2  \n          where  merchant_number =  :3   and\n                 active_date =  :4 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"68com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_165);
   __sJT_st.setDouble(2,__sJT_166);
   __sJT_st.setLong(3,__sJT_167);
   __sJT_st.setDate(4,activeDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3196^9*/
      }
      resultSet.close();
      it.close();
      
      //********************************************************************
      // determine the base cost when inventory was transferred
      // i.e. Just in time sale by MES to Cedars bank
      //********************************************************************
      /*@lineinfo:generated-code*//*@lineinfo:3205^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ use_nl(sm dp) */
//                  sm.merchant_number                  as merchant_number,
//                  sum( decode( ps.pc_code,
//                               'NEW', nvl(ep.price_base,0),
//                                nvl(ep.price_used,0) 
//                              ) )                     as transfer_cost
//          from    monthly_extract_summary         sm,
//                  tmg_deployment                  dp,
//                  tmg_orders                      o,
//                  tmg_part_states                 ps,
//                  tmg_part_types                  pt,
//                  equip_price                     ep
//          where   sm.load_file_id = :loadFileId and
//                  ( :nodeId = 0 or 
//                    exists 
//                      ( select gm.merchant_number
//                        from    organization    o,
//                                group_merchant  gm
//                        where   o.org_group = :nodeId and
//                                gm.org_num = o.org_num and
//                                gm.merchant_number = sm.merchant_number
//                       )
//                  ) and
//                  dp.merch_num = sm.merchant_number and
//                  trunc(dp.start_ts) between :monthBeginDate and :monthEndDate and
//                  dp.start_code in ( 'ADD','NEW' ) and
//                  o.ord_id = dp.start_ord_id and
//                  o.ord_stat_code = 'DONE' and
//                  ps.deploy_id = dp.deploy_id and
//                  ps.state_id =
//                  (
//                    select      min( psin.state_id ) 
//                    from        tmg_part_states psin 
//                    where       psin.deploy_id = dp.deploy_id
//                  ) and
//                  exists
//                  (
//                    select  x.create_ts
//                    from    tmg_part_states   x_ps,
//                            tmg_op_log        x_ol,
//                            tmg_transfers     x
//                    where   x_ps.prior_id = ps.state_id and
//                            x_ol.op_id    = x_ps.op_id and
//                            x_ol.op_code  = 'PART_XFER' and
//                            x.op_id       = x_ps.op_id
//                  ) and
//                  pt.pt_id = ps.pt_id and
//                  ep.rec_id = get_equip_price_rec_id(sm.merchant_number,pt.model_code,trunc(dp.start_ts))
//          group by sm.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ use_nl(sm dp) */\n                sm.merchant_number                  as merchant_number,\n                sum( decode( ps.pc_code,\n                             'NEW', nvl(ep.price_base,0),\n                              nvl(ep.price_used,0) \n                            ) )                     as transfer_cost\n        from    monthly_extract_summary         sm,\n                tmg_deployment                  dp,\n                tmg_orders                      o,\n                tmg_part_states                 ps,\n                tmg_part_types                  pt,\n                equip_price                     ep\n        where   sm.load_file_id =  :1   and\n                (  :2   = 0 or \n                  exists \n                    ( select gm.merchant_number\n                      from    organization    o,\n                              group_merchant  gm\n                      where   o.org_group =  :3   and\n                              gm.org_num = o.org_num and\n                              gm.merchant_number = sm.merchant_number\n                     )\n                ) and\n                dp.merch_num = sm.merchant_number and\n                trunc(dp.start_ts) between  :4   and  :5   and\n                dp.start_code in ( 'ADD','NEW' ) and\n                o.ord_id = dp.start_ord_id and\n                o.ord_stat_code = 'DONE' and\n                ps.deploy_id = dp.deploy_id and\n                ps.state_id =\n                (\n                  select      min( psin.state_id ) \n                  from        tmg_part_states psin \n                  where       psin.deploy_id = dp.deploy_id\n                ) and\n                exists\n                (\n                  select  x.create_ts\n                  from    tmg_part_states   x_ps,\n                          tmg_op_log        x_ol,\n                          tmg_transfers     x\n                  where   x_ps.prior_id = ps.state_id and\n                          x_ol.op_id    = x_ps.op_id and\n                          x_ol.op_code  = 'PART_XFER' and\n                          x.op_id       = x_ps.op_id\n                ) and\n                pt.pt_id = ps.pt_id and\n                ep.rec_id = get_equip_price_rec_id(sm.merchant_number,pt.model_code,trunc(dp.start_ts))\n        group by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"69com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setLong(3,nodeId);
   __sJT_st.setDate(4,monthBeginDate);
   __sJT_st.setDate(5,monthEndDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"69com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3256^7*/
      resultSet = it.getResultSet();
      
      while(resultSet.next())
      {
        /*@lineinfo:generated-code*//*@lineinfo:3261^9*/

//  ************************************************************
//  #sql [Ctx] { update monthly_extract_summary
//            set    equip_transfer_cost = :resultSet.getDouble("transfer_cost")
//            where  merchant_number = :resultSet.getLong("merchant_number") and
//                   active_date = :activeDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_168 = resultSet.getDouble("transfer_cost");
 long __sJT_169 = resultSet.getLong("merchant_number");
   String theSqlTS = "update monthly_extract_summary\n          set    equip_transfer_cost =  :1  \n          where  merchant_number =  :2   and\n                 active_date =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"70com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_168);
   __sJT_st.setLong(2,__sJT_169);
   __sJT_st.setDate(3,activeDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3267^9*/
      }
      resultSet.close();
      it.close();
      
      // 0 fill all remaining null values
      /*@lineinfo:generated-code*//*@lineinfo:3273^7*/

//  ************************************************************
//  #sql [Ctx] { update  monthly_extract_summary
//          set     equip_rental_expense = nvl(equip_rental_expense,0),
//                  equip_sales_expense = nvl(equip_sales_expense,0),
//                  equip_rental_base_cost = nvl(equip_rental_base_cost,0),
//                  equip_sales_base_cost = nvl(equip_sales_base_cost,0),
//                  equip_transfer_cost = nvl(equip_transfer_cost,0)
//          where   load_file_id = :loadFileId and
//                  ( 
//                    equip_rental_expense is null or
//                    equip_sales_expense is null or
//                    equip_rental_base_cost is null or
//                    equip_sales_base_cost is null or
//                    equip_transfer_cost is null 
//                  ) 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  monthly_extract_summary\n        set     equip_rental_expense = nvl(equip_rental_expense,0),\n                equip_sales_expense = nvl(equip_sales_expense,0),\n                equip_rental_base_cost = nvl(equip_rental_base_cost,0),\n                equip_sales_base_cost = nvl(equip_sales_base_cost,0),\n                equip_transfer_cost = nvl(equip_transfer_cost,0)\n        where   load_file_id =  :1   and\n                ( \n                  equip_rental_expense is null or\n                  equip_sales_expense is null or\n                  equip_rental_base_cost is null or\n                  equip_sales_base_cost is null or\n                  equip_transfer_cost is null \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"71com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3289^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:3291^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3294^7*/
    }
    catch(Exception e)
    {
      logEntry("loadEquipSummary("+loadFilename+")",e.toString());
    }
    finally
    {
      try{ it.close(); }catch(Exception ee){}
    }      
    return( retVal );
  }
  
  private boolean loadEquipSummaryLegacy( String loadFilename, long nodeId )
  {
    Date                  activeDate        = null;
    int                   bankNumber        = 0;
    ResultSetIterator     it                = null;
    long                  loadFileId        = 0L;
    Date                  monthBeginDate    = null;
    Date                  monthEndDate      = null;
    String                progress          = null;
    ResultSet             resultSet         = null;
    boolean               retVal            = true;
    
    try
    {
      setConsoleTrace(true);//@ debug trace
      trace("Loading bank number and active date for " + loadFilename );
      
      /*@lineinfo:generated-code*//*@lineinfo:3324^7*/

//  ************************************************************
//  #sql [Ctx] { select  lfi.load_file_id    
//          from    load_file_index         lfi
//          where   lfi.load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  lfi.load_file_id     \n        from    load_file_index         lfi\n        where   lfi.load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"72com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3329^7*/
      
      // get bank number and active date to use in the queries
      // to make them easier to debug if necessary.
      /*@lineinfo:generated-code*//*@lineinfo:3333^7*/

//  ************************************************************
//  #sql [Ctx] { select  sm.bank_number, sm.active_date,
//                  min(month_begin_date), max(month_end_date)
//          
//          from    monthly_extract_summary sm
//          where   sm.load_file_id = :loadFileId
//          group by sm.bank_number, sm.active_date      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sm.bank_number, sm.active_date,\n                min(month_begin_date), max(month_end_date)\n         \n        from    monthly_extract_summary sm\n        where   sm.load_file_id =  :1  \n        group by sm.bank_number, sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"73com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   activeDate = (java.sql.Date)__sJT_rs.getDate(2);
   monthBeginDate = (java.sql.Date)__sJT_rs.getDate(3);
   monthEndDate = (java.sql.Date)__sJT_rs.getDate(4);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3341^7*/
      
      //********************************************************************
      // Equipment Rental base cost for MES
      //********************************************************************
      /*@lineinfo:generated-code*//*@lineinfo:3346^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                              as merchant_number,
//                  round(sum( (nvl(ei.ei_unit_cost,
//                                  nvl(eq.equip_rent,0))/36) ),2)  as rent_cost
//          from    monthly_extract_summary sm,
//                  equip_merchant_tracking emt,
//                  equip_inventory         ei,
//                  equipment               eq
//          where   sm.bank_number = :bankNumber and
//                  sm.active_date = :activeDate and
//                  ( :nodeId = 0 or 
//                    exists 
//                      ( select gm.merchant_number
//                        from    organization    o,
//                                group_merchant  gm
//                        where   o.org_group = :nodeId and
//                                gm.org_num = o.org_num and
//                                gm.merchant_number = sm.merchant_number
//                       )
//                  ) and
//                  emt.merchant_number = sm.merchant_number and
//                  emt.action in ( 1, 4 ) and -- acct setup deployment, additional equip deployment
//                  emt.cancel_date is null and
//                  emt.action_desc = 2 and  -- rent
//                  trunc(emt.action_date) between (:monthEndDate - (365*3)) and :monthEndDate and
//                  ei.ei_serial_number = emt.ref_num_serial_num and
//                  eq.equip_model(+) = ei.ei_part_number
//          group by sm.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                              as merchant_number,\n                round(sum( (nvl(ei.ei_unit_cost,\n                                nvl(eq.equip_rent,0))/36) ),2)  as rent_cost\n        from    monthly_extract_summary sm,\n                equip_merchant_tracking emt,\n                equip_inventory         ei,\n                equipment               eq\n        where   sm.bank_number =  :1   and\n                sm.active_date =  :2   and\n                (  :3   = 0 or \n                  exists \n                    ( select gm.merchant_number\n                      from    organization    o,\n                              group_merchant  gm\n                      where   o.org_group =  :4   and\n                              gm.org_num = o.org_num and\n                              gm.merchant_number = sm.merchant_number\n                     )\n                ) and\n                emt.merchant_number = sm.merchant_number and\n                emt.action in ( 1, 4 ) and -- acct setup deployment, additional equip deployment\n                emt.cancel_date is null and\n                emt.action_desc = 2 and  -- rent\n                trunc(emt.action_date) between ( :5   - (365*3)) and  :6   and\n                ei.ei_serial_number = emt.ref_num_serial_num and\n                eq.equip_model(+) = ei.ei_part_number\n        group by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"74com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setLong(3,nodeId);
   __sJT_st.setLong(4,nodeId);
   __sJT_st.setDate(5,monthEndDate);
   __sJT_st.setDate(6,monthEndDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"74com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3375^7*/
      resultSet = it.getResultSet();
      
      while(resultSet.next())
      {
        /*@lineinfo:generated-code*//*@lineinfo:3380^9*/

//  ************************************************************
//  #sql [Ctx] { update monthly_extract_summary
//            set    equip_rental_expense = :resultSet.getDouble("rent_cost")
//            where  merchant_number = :resultSet.getLong("merchant_number") and
//                   active_date = :activeDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_170 = resultSet.getDouble("rent_cost");
 long __sJT_171 = resultSet.getLong("merchant_number");
   String theSqlTS = "update monthly_extract_summary\n          set    equip_rental_expense =  :1  \n          where  merchant_number =  :2   and\n                 active_date =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"75com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_170);
   __sJT_st.setLong(2,__sJT_171);
   __sJT_st.setDate(3,activeDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3386^9*/
      }
      resultSet.close();
      it.close();
      
      //********************************************************************
      // Equipment Sales base cost for MES 
      //********************************************************************
      /*@lineinfo:generated-code*//*@lineinfo:3394^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                        as merchant_number,
//                  sum( nvl(ei.ei_unit_cost,nvl(eq.equip_price,0)) ) as sales_cost
//          from    monthly_extract_summary sm,
//                  equip_merchant_tracking emt,
//                  equip_inventory         ei,
//                  equipment               eq
//          where   sm.bank_number = :bankNumber and
//                  sm.active_date = :activeDate and
//                  ( :nodeId = 0 or 
//                    exists 
//                      ( select gm.merchant_number
//                        from    organization    o,
//                                group_merchant  gm
//                        where   o.org_group = :nodeId and
//                                gm.org_num = o.org_num and
//                                gm.merchant_number = sm.merchant_number
//                       )
//                  ) and
//                  emt.merchant_number = sm.merchant_number and
//                  trunc(emt.action_date) between :monthBeginDate and :monthEndDate and
//                  emt.cancel_date is null and
//                  emt.action in ( 1, 4 ) and  -- acct setup deployment, additional equip deployment
//                  emt.action_desc in ( 1, 4 ) and  -- buy, buy refurbished
//                  ei.ei_serial_number = emt.ref_num_serial_num and
//                  eq.equip_model = ei.ei_part_number
//          group by sm.merchant_number                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                        as merchant_number,\n                sum( nvl(ei.ei_unit_cost,nvl(eq.equip_price,0)) ) as sales_cost\n        from    monthly_extract_summary sm,\n                equip_merchant_tracking emt,\n                equip_inventory         ei,\n                equipment               eq\n        where   sm.bank_number =  :1   and\n                sm.active_date =  :2   and\n                (  :3   = 0 or \n                  exists \n                    ( select gm.merchant_number\n                      from    organization    o,\n                              group_merchant  gm\n                      where   o.org_group =  :4   and\n                              gm.org_num = o.org_num and\n                              gm.merchant_number = sm.merchant_number\n                     )\n                ) and\n                emt.merchant_number = sm.merchant_number and\n                trunc(emt.action_date) between  :5   and  :6   and\n                emt.cancel_date is null and\n                emt.action in ( 1, 4 ) and  -- acct setup deployment, additional equip deployment\n                emt.action_desc in ( 1, 4 ) and  -- buy, buy refurbished\n                ei.ei_serial_number = emt.ref_num_serial_num and\n                eq.equip_model = ei.ei_part_number\n        group by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"76com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setLong(3,nodeId);
   __sJT_st.setLong(4,nodeId);
   __sJT_st.setDate(5,monthBeginDate);
   __sJT_st.setDate(6,monthEndDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"76com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3422^7*/
      resultSet = it.getResultSet();
      
      while(resultSet.next())
      {
        /*@lineinfo:generated-code*//*@lineinfo:3427^9*/

//  ************************************************************
//  #sql [Ctx] { update monthly_extract_summary
//            set    equip_sales_expense = :resultSet.getDouble("sales_cost")
//            where  merchant_number = :resultSet.getLong("merchant_number") and
//                   active_date = :activeDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_172 = resultSet.getDouble("sales_cost");
 long __sJT_173 = resultSet.getLong("merchant_number");
   String theSqlTS = "update monthly_extract_summary\n          set    equip_sales_expense =  :1  \n          where  merchant_number =  :2   and\n                 active_date =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"77com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_172);
   __sJT_st.setLong(2,__sJT_173);
   __sJT_st.setDate(3,activeDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3433^9*/
      }
      resultSet.close();
      it.close();
      
      //********************************************************************
      // determine the base cost of the equipment for sales commission
      // and banks who do not own equipment but get receive the mark-up
      // over base cost in their m/e payments.
      //********************************************************************
      /*@lineinfo:generated-code*//*@lineinfo:3443^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                    as merchant_number,
//                  sum( decode( emt.action_desc, 
//                               2, 0,            -- exclude rentals 
//                               decode( ei.ei_class,
//                                       1, nvl(ep.price_base,nvl(ei.ei_unit_cost,0)),
//                                       nvl(ep.price_used,nvl(ei.ei_unit_cost,0)) 
//                                     )  
//                             ) )                        as sales_cost,
//                  round(                           
//                  sum( decode( emt.action_desc, 
//                               2, decode( ei.ei_class, -- only rentals (2 = rentals)      
//                                          1, nvl(ep.price_rental,(nvl(ei.ei_unit_cost,0)/36)),
//                                          nvl(ep.price_rental_used,(nvl(ei.ei_unit_cost,0)/36)) 
//                                        ),
//                               0   
//                             ) ), 2 )                   as rental_cost
//          from    monthly_extract_summary sm,
//                  equip_merchant_tracking emt,
//                  equip_inventory         ei,
//                  equip_price             ep,
//                  mif                     mf
//          where   sm.bank_number = :bankNumber and
//                  sm.active_date = :activeDate and
//                  ( :nodeId = 0 or 
//                    exists 
//                      ( select gm.merchant_number
//                        from    organization    o,
//                                group_merchant  gm
//                        where   o.org_group = :nodeId and
//                                gm.org_num = o.org_num and
//                                gm.merchant_number = sm.merchant_number
//                       )
//                  ) and
//                  emt.merchant_number = sm.merchant_number and
//                  mf.merchant_number = emt.merchant_number and
//                  emt.action in ( 1, 4 ) and -- acct setup deployment, additional equip deployment              
//                  ( ( trunc(emt.action_date) between :monthBeginDate and :monthEndDate and
//                      emt.action_desc in (1,4) ) or -- buy, buy refurbished
//                    ( emt.action_desc = 2 and
//                      trunc(emt.action_date) <= :monthEndDate and
//                      ei.ei_merchant_number = emt.merchant_number ) ) and
//                  emt.cancel_date is null and
//                  ei.ei_serial_number = emt.ref_num_serial_num and
//                  ep.rec_id = get_equip_price_rec_id(emt.merchant_number,ei.ei_part_number,trunc(emt.action_date))
//          group by sm.merchant_number              
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                    as merchant_number,\n                sum( decode( emt.action_desc, \n                             2, 0,            -- exclude rentals \n                             decode( ei.ei_class,\n                                     1, nvl(ep.price_base,nvl(ei.ei_unit_cost,0)),\n                                     nvl(ep.price_used,nvl(ei.ei_unit_cost,0)) \n                                   )  \n                           ) )                        as sales_cost,\n                round(                           \n                sum( decode( emt.action_desc, \n                             2, decode( ei.ei_class, -- only rentals (2 = rentals)      \n                                        1, nvl(ep.price_rental,(nvl(ei.ei_unit_cost,0)/36)),\n                                        nvl(ep.price_rental_used,(nvl(ei.ei_unit_cost,0)/36)) \n                                      ),\n                             0   \n                           ) ), 2 )                   as rental_cost\n        from    monthly_extract_summary sm,\n                equip_merchant_tracking emt,\n                equip_inventory         ei,\n                equip_price             ep,\n                mif                     mf\n        where   sm.bank_number =  :1   and\n                sm.active_date =  :2   and\n                (  :3   = 0 or \n                  exists \n                    ( select gm.merchant_number\n                      from    organization    o,\n                              group_merchant  gm\n                      where   o.org_group =  :4   and\n                              gm.org_num = o.org_num and\n                              gm.merchant_number = sm.merchant_number\n                     )\n                ) and\n                emt.merchant_number = sm.merchant_number and\n                mf.merchant_number = emt.merchant_number and\n                emt.action in ( 1, 4 ) and -- acct setup deployment, additional equip deployment              \n                ( ( trunc(emt.action_date) between  :5   and  :6   and\n                    emt.action_desc in (1,4) ) or -- buy, buy refurbished\n                  ( emt.action_desc = 2 and\n                    trunc(emt.action_date) <=  :7   and\n                    ei.ei_merchant_number = emt.merchant_number ) ) and\n                emt.cancel_date is null and\n                ei.ei_serial_number = emt.ref_num_serial_num and\n                ep.rec_id = get_equip_price_rec_id(emt.merchant_number,ei.ei_part_number,trunc(emt.action_date))\n        group by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"78com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setLong(3,nodeId);
   __sJT_st.setLong(4,nodeId);
   __sJT_st.setDate(5,monthBeginDate);
   __sJT_st.setDate(6,monthEndDate);
   __sJT_st.setDate(7,monthEndDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"78com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3490^7*/
      resultSet = it.getResultSet();
      
      while(resultSet.next())
      {
        /*@lineinfo:generated-code*//*@lineinfo:3495^9*/

//  ************************************************************
//  #sql [Ctx] { update monthly_extract_summary
//            set    equip_sales_base_cost  = :resultSet.getDouble("sales_cost"),
//                   equip_rental_base_cost = :resultSet.getDouble("rental_cost")
//            where  merchant_number = :resultSet.getLong("merchant_number") and
//                   active_date = :activeDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_174 = resultSet.getDouble("sales_cost");
 double __sJT_175 = resultSet.getDouble("rental_cost");
 long __sJT_176 = resultSet.getLong("merchant_number");
   String theSqlTS = "update monthly_extract_summary\n          set    equip_sales_base_cost  =  :1  ,\n                 equip_rental_base_cost =  :2  \n          where  merchant_number =  :3   and\n                 active_date =  :4 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"79com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_174);
   __sJT_st.setDouble(2,__sJT_175);
   __sJT_st.setLong(3,__sJT_176);
   __sJT_st.setDate(4,activeDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3502^9*/
      }
      resultSet.close();
      it.close();
      
      //********************************************************************
      // determine the base cost when inventory was transferred
      // i.e. Just in time sale by MES to Cedars bank
      //********************************************************************
      /*@lineinfo:generated-code*//*@lineinfo:3511^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number            as merchant_number,
//                  sum ( decode( ecd.condition,
//                                1, nvl(ep.price_base,nvl(ei.ei_unit_cost,0)),
//                                nvl(ep.price_used,nvl(ei.ei_unit_cost,0)) )  
//                      )                         as transfer_cost
//          from    monthly_extract_summary         sm,
//                  mif                             mf,
//                  equip_merchant_tracking         emt,
//                  equip_inventory                 ei,
//                  equip_client_deployment         ecd,
//                  equip_price                     ep
//          where   sm.bank_number = :bankNumber and
//                  sm.active_date = :activeDate and
//                  ( :nodeId = 0 or 
//                    exists 
//                      ( select gm.merchant_number
//                        from    organization    o,
//                                group_merchant  gm
//                        where   o.org_group = :nodeId and
//                                gm.org_num = o.org_num and
//                                gm.merchant_number = sm.merchant_number
//                       )
//                  ) and
//                  mf.merchant_number = sm.merchant_number and
//                  emt.merchant_number = mf.merchant_number and
//                  trunc(emt.action_date) between :monthBeginDate and :monthEndDate and
//                  emt.action in ( 1, 4 ) and  -- acct setup deployment, additional equip deployment
//                  emt.action_desc in ( 1, 2, 4 ) and  -- buy, rent, buy refurbished              
//                  emt.cancel_date is null and
//                  ei.ei_serial_number = emt.ref_num_serial_num and                            
//                  ecd.serial_number = ei.ei_serial_number and
//                  ecd.from_client = 0 and -- from MES
//                  ecd.to_client = ei.ei_owner and -- to the current owner
//                  trunc(ecd.move_date) between trunc(emt.action_date)-7 and trunc(emt.action_date) and
//                  ep.rec_id = get_equip_price_rec_id(emt.merchant_number,ei.ei_part_number,trunc(ecd.move_date))
//          group by sm.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number            as merchant_number,\n                sum ( decode( ecd.condition,\n                              1, nvl(ep.price_base,nvl(ei.ei_unit_cost,0)),\n                              nvl(ep.price_used,nvl(ei.ei_unit_cost,0)) )  \n                    )                         as transfer_cost\n        from    monthly_extract_summary         sm,\n                mif                             mf,\n                equip_merchant_tracking         emt,\n                equip_inventory                 ei,\n                equip_client_deployment         ecd,\n                equip_price                     ep\n        where   sm.bank_number =  :1   and\n                sm.active_date =  :2   and\n                (  :3   = 0 or \n                  exists \n                    ( select gm.merchant_number\n                      from    organization    o,\n                              group_merchant  gm\n                      where   o.org_group =  :4   and\n                              gm.org_num = o.org_num and\n                              gm.merchant_number = sm.merchant_number\n                     )\n                ) and\n                mf.merchant_number = sm.merchant_number and\n                emt.merchant_number = mf.merchant_number and\n                trunc(emt.action_date) between  :5   and  :6   and\n                emt.action in ( 1, 4 ) and  -- acct setup deployment, additional equip deployment\n                emt.action_desc in ( 1, 2, 4 ) and  -- buy, rent, buy refurbished              \n                emt.cancel_date is null and\n                ei.ei_serial_number = emt.ref_num_serial_num and                            \n                ecd.serial_number = ei.ei_serial_number and\n                ecd.from_client = 0 and -- from MES\n                ecd.to_client = ei.ei_owner and -- to the current owner\n                trunc(ecd.move_date) between trunc(emt.action_date)-7 and trunc(emt.action_date) and\n                ep.rec_id = get_equip_price_rec_id(emt.merchant_number,ei.ei_part_number,trunc(ecd.move_date))\n        group by sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"80com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setLong(3,nodeId);
   __sJT_st.setLong(4,nodeId);
   __sJT_st.setDate(5,monthBeginDate);
   __sJT_st.setDate(6,monthEndDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"80com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3549^7*/
      resultSet = it.getResultSet();
      
      while(resultSet.next())
      {
        /*@lineinfo:generated-code*//*@lineinfo:3554^9*/

//  ************************************************************
//  #sql [Ctx] { update monthly_extract_summary
//            set    equip_transfer_cost = :resultSet.getDouble("transfer_cost")
//            where  merchant_number = :resultSet.getLong("merchant_number") and
//                   active_date = :activeDate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_177 = resultSet.getDouble("transfer_cost");
 long __sJT_178 = resultSet.getLong("merchant_number");
   String theSqlTS = "update monthly_extract_summary\n          set    equip_transfer_cost =  :1  \n          where  merchant_number =  :2   and\n                 active_date =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"81com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_177);
   __sJT_st.setLong(2,__sJT_178);
   __sJT_st.setDate(3,activeDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3560^9*/
      }
      resultSet.close();
      it.close();
      
      // 0 fill all remaining null values
      /*@lineinfo:generated-code*//*@lineinfo:3566^7*/

//  ************************************************************
//  #sql [Ctx] { update  monthly_extract_summary
//          set     equip_rental_expense = nvl(equip_rental_expense,0),
//                  equip_sales_expense = nvl(equip_sales_expense,0),
//                  equip_rental_base_cost = nvl(equip_rental_base_cost,0),
//                  equip_sales_base_cost = nvl(equip_sales_base_cost,0),
//                  equip_transfer_cost = nvl(equip_transfer_cost,0)
//          where   load_file_id = :loadFileId and
//                  ( 
//                    equip_rental_expense is null or
//                    equip_sales_expense is null or
//                    equip_rental_base_cost is null or
//                    equip_sales_base_cost is null or
//                    equip_transfer_cost is null 
//                  ) 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  monthly_extract_summary\n        set     equip_rental_expense = nvl(equip_rental_expense,0),\n                equip_sales_expense = nvl(equip_sales_expense,0),\n                equip_rental_base_cost = nvl(equip_rental_base_cost,0),\n                equip_sales_base_cost = nvl(equip_sales_base_cost,0),\n                equip_transfer_cost = nvl(equip_transfer_cost,0)\n        where   load_file_id =  :1   and\n                ( \n                  equip_rental_expense is null or\n                  equip_sales_expense is null or\n                  equip_rental_base_cost is null or\n                  equip_sales_base_cost is null or\n                  equip_transfer_cost is null \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"82com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3582^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:3584^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3587^7*/
    }
    catch(Exception e)
    {
      logEntry("loadEquipSummaryLegacy("+loadFilename+")",e.toString());
    }
    finally
    {
      try{ it.close(); }catch(Exception ee){}
    }      
    return( retVal );
  }
  
  private boolean loadExtractSummary( String loadFilename )
  {
    boolean                   autoCommit      = false;
    boolean                   fileProcessed   = false;
    MonthlyExtractSummarizer  summarizer      = null;
    
    try
    {
      autoCommit = getAutoCommit();   // store current value
      setAutoCommit(false);           // disable auto commit
      
      // flush any transaction data
      /*@lineinfo:generated-code*//*@lineinfo:3612^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3615^7*/
      
      // reset all the completion flags
      /*@lineinfo:generated-code*//*@lineinfo:3618^7*/

//  ************************************************************
//  #sql [Ctx] { update  merch_prof_load_progress lp
//          set     lp.debit_summary_loaded     = 'N',
//                  lp.equip_cost_loaded        = 'N',
//                  lp.contract_summary_loaded  = 'N'
//          where   lp.load_file_id = load_filename_to_load_file_id(:loadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merch_prof_load_progress lp\n        set     lp.debit_summary_loaded     = 'N',\n                lp.equip_cost_loaded        = 'N',\n                lp.contract_summary_loaded  = 'N'\n        where   lp.load_file_id = load_filename_to_load_file_id( :1  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"83com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3625^7*/
      
      // commit change to tOracle
      /*@lineinfo:generated-code*//*@lineinfo:3628^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3631^7*/
      
      // use the large rollback segment
      /*@lineinfo:generated-code*//*@lineinfo:3634^7*/

//  ************************************************************
//  #sql [Ctx] { set transaction use rollback segment LARGE_RBS
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "set transaction use rollback segment LARGE_RBS";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"84com.mes.startup.MerchProfLoader",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3637^7*/
      
      // load the extract data
      summarizer = new MonthlyExtractSummarizer();
      summarizer.connect(true);
      summarizer.loadMonthlyExtractSummary(loadFilename,false);
      
      /*@lineinfo:generated-code*//*@lineinfo:3644^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3647^7*/
      
      if( getFileBankNumber( loadFilename ) == mesConstants.BANK_ID_CBT ) 
      {
        //
        //  CB&T is the only bank that uses the rebate reporting.
        //
        //  Bank Rebate
        //  CB&T Affiliate Rebate
        //  CB&T Agent Bank Rebate
        //  CB&T Management Affiliate Rebate
        //
        /*@lineinfo:generated-code*//*@lineinfo:3659^9*/

//  ************************************************************
//  #sql [Ctx] { call load_cbt_rebate_summary( :loadFilename ) -- load for cb&t specific
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_cbt_rebate_summary(  :1   ) -- load for cb&t specific\n        \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"85com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3662^9*/

        /*@lineinfo:generated-code*//*@lineinfo:3664^9*/

//  ************************************************************
//  #sql [Ctx] { call load_rebate_summary( :loadFilename )     -- load generic rebate
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_rebate_summary(  :1   )     -- load generic rebate\n        \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"86com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3667^9*/
      }
      /*@lineinfo:generated-code*//*@lineinfo:3669^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3672^7*/
      setAutoCommit(autoCommit);  // restore value
      fileProcessed = true;
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadExtractSummary()",e.toString());
    }
    finally
    {
      try{ summarizer.cleanUp(); } catch( Exception ee ) {}
    }
    return( fileProcessed );
  }
  
  private boolean loadTranscomExtractSummary( String loadFilename )
  {
    boolean                         fileProcessed   = false;
    TranscomExtractSummary          tes             = null;

    try
    {
      tes = new TranscomExtractSummary();  

      tes.connect(true);
      if ( tes.loadTranscomExtractSummary(loadFilename) == true )
      {
        /*@lineinfo:generated-code*//*@lineinfo:3699^9*/

//  ************************************************************
//  #sql [Ctx] { insert into transcom_process
//              ( file_type, load_filename )
//            values
//              ( 2, :loadFilename )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into transcom_process\n            ( file_type, load_filename )\n          values\n            ( 2,  :1   )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"87com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3705^9*/

        fileProcessed = true;
      }
      
      
    }
    catch( Exception e )
    {
      logEntry("loadTranscomExtractSummary()",e.toString());
    }
    finally
    {
      try{ tes.cleanUp(); } catch(Exception e){}
      tes = null;
    }
    return( fileProcessed );
  }
  
  private boolean loadVerisignOverAuths( String loadFilename, long nodeId )
  {
    Date                  activeDate        = null;
    int                   bankNumber        = 0;
    ResultSetIterator     it                = null;
    long                  lastMerchantId    = 0L;
    long                  loadFileId        = 0L;
    long                  merchantId        = 0L;
    String                progress          = null;
    ResultSet             resultSet         = null;
    boolean               retVal            = true;
    
    try
    {
      setConsoleTrace(false);//@ debug trace
      trace("Loading bank number and active date for " + loadFilename );
      
      /*@lineinfo:generated-code*//*@lineinfo:3741^7*/

//  ************************************************************
//  #sql [Ctx] { select  lfi.load_file_id    
//          from    load_file_index         lfi
//          where   lfi.load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  lfi.load_file_id     \n        from    load_file_index         lfi\n        where   lfi.load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"88com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3746^7*/
      
      // get bank number and active date to use in the queries
      // to make them easier to debug if necessary.
      /*@lineinfo:generated-code*//*@lineinfo:3750^7*/

//  ************************************************************
//  #sql [Ctx] { select  sm.bank_number, sm.active_date
//          
//          from    monthly_extract_summary sm
//          where   sm.load_file_id = :loadFileId
//          group by sm.bank_number, sm.active_date      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sm.bank_number, sm.active_date\n         \n        from    monthly_extract_summary sm\n        where   sm.load_file_id =  :1  \n        group by sm.bank_number, sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"89com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   activeDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3757^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:3759^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                                  as merchant_number,
//                  --mf.dba_name                                         as dba_name,
//                  get_node_relation(voa.hierarchy_node,
//                                    sm.merchant_number)               as relation,
//                  round( ((nvl(sm.auth_count_total,0) - decode(sm.product_code,'VL',500,'VF',1000,0)) *
//                         voa.per_auth_fee),2 )                        as charge_amount,
//                  substr(
//                  ( 
//                    ( nvl(sm.auth_count_total,0) -
//                      decode(sm.product_code,'VL',500,'VF',1000,0) ) || ' VERISIGN AUTHS @' ||
//                      to_char(voa.per_auth_fee,'$0.00') || ' EA ' ||
//                      to_char(sm.active_date,'MON YYYY')
//                  ),1,74)                                             as statement_message
//          from    billing_verisign_over_auth  voa,
//                  organization                o,
//                  group_merchant              gm,
//                  monthly_extract_summary     sm,
//                  mif                         mf
//          where   ( :nodeId = 0 or voa.hierarchy_node = :nodeId ) and
//                  o.org_group = voa.hierarchy_node and
//                  gm.org_num = o.org_num and 
//                  sm.merchant_number = gm.merchant_number and
//                  sm.active_date = :activeDate and
//                  sm.bank_number = :bankNumber and
//                  sm.active_date between voa.valid_date_begin and voa.valid_date_end and
//                  sm.product_code in ( 'VL','VF' ) and
//                  ( nvl(sm.auth_count_total,0) -
//                    decode(sm.product_code,'VL',500,'VF',1000,0) ) > 0 and
//                  mf.merchant_number = sm.merchant_number
//          order by merchant_number, relation                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                                  as merchant_number,\n                --mf.dba_name                                         as dba_name,\n                get_node_relation(voa.hierarchy_node,\n                                  sm.merchant_number)               as relation,\n                round( ((nvl(sm.auth_count_total,0) - decode(sm.product_code,'VL',500,'VF',1000,0)) *\n                       voa.per_auth_fee),2 )                        as charge_amount,\n                substr(\n                ( \n                  ( nvl(sm.auth_count_total,0) -\n                    decode(sm.product_code,'VL',500,'VF',1000,0) ) || ' VERISIGN AUTHS @' ||\n                    to_char(voa.per_auth_fee,'$0.00') || ' EA ' ||\n                    to_char(sm.active_date,'MON YYYY')\n                ),1,74)                                             as statement_message\n        from    billing_verisign_over_auth  voa,\n                organization                o,\n                group_merchant              gm,\n                monthly_extract_summary     sm,\n                mif                         mf\n        where   (  :1   = 0 or voa.hierarchy_node =  :2   ) and\n                o.org_group = voa.hierarchy_node and\n                gm.org_num = o.org_num and \n                sm.merchant_number = gm.merchant_number and\n                sm.active_date =  :3   and\n                sm.bank_number =  :4   and\n                sm.active_date between voa.valid_date_begin and voa.valid_date_end and\n                sm.product_code in ( 'VL','VF' ) and\n                ( nvl(sm.auth_count_total,0) -\n                  decode(sm.product_code,'VL',500,'VF',1000,0) ) > 0 and\n                mf.merchant_number = sm.merchant_number\n        order by merchant_number, relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"90com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setLong(2,nodeId);
   __sJT_st.setDate(3,activeDate);
   __sJT_st.setInt(4,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"90com.mes.startup.MerchProfLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3791^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        merchantId = resultSet.getLong("merchant_number");
        
        if ( merchantId == lastMerchantId )
        {
          continue;
        }
      
        /*@lineinfo:generated-code*//*@lineinfo:3803^9*/

//  ************************************************************
//  #sql [Ctx] { insert into billing_auto_cg
//            (
//              merchant_number,
//              charge_amount,
//              statement_message,
//              billing_months,
//              date_start,
//              date_expiration,
//              active_date
//            )
//            values
//            (
//              :merchantId,
//              :resultSet.getDouble("charge_amount"),
//              :resultSet.getString("statement_message"),
//              (
//                substr('NNNNNNNNNNNN',1,to_number(to_char((last_day(:activeDate)+15),'mm'))-1) ||
//                'Y' ||
//                substr('NNNNNNNNNNNN',to_number(to_char((last_day(:activeDate)+15),'mm'))+1)
//              ),
//              (last_day(:activeDate) + 15),
//              (last_day((last_day(:activeDate)+15))+15),
//              trunc(sysdate)
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_179 = resultSet.getDouble("charge_amount");
 String __sJT_180 = resultSet.getString("statement_message");
   String theSqlTS = "insert into billing_auto_cg\n          (\n            merchant_number,\n            charge_amount,\n            statement_message,\n            billing_months,\n            date_start,\n            date_expiration,\n            active_date\n          )\n          values\n          (\n             :1  ,\n             :2  ,\n             :3  ,\n            (\n              substr('NNNNNNNNNNNN',1,to_number(to_char((last_day( :4  )+15),'mm'))-1) ||\n              'Y' ||\n              substr('NNNNNNNNNNNN',to_number(to_char((last_day( :5  )+15),'mm'))+1)\n            ),\n            (last_day( :6  ) + 15),\n            (last_day((last_day( :7  )+15))+15),\n            trunc(sysdate)\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"91com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDouble(2,__sJT_179);
   __sJT_st.setString(3,__sJT_180);
   __sJT_st.setDate(4,activeDate);
   __sJT_st.setDate(5,activeDate);
   __sJT_st.setDate(6,activeDate);
   __sJT_st.setDate(7,activeDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3829^9*/
        lastMerchantId = merchantId;
      }
      resultSet.close();
      it.close();
      
      /*@lineinfo:generated-code*//*@lineinfo:3835^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3838^7*/
    }
    catch(Exception e)
    {
      logEntry("loadVerisignOverAuths("+loadFilename+")",e.toString());
    }
    finally
    {
      try{ it.close(); }catch(Exception ee){}
    }      
    return( retVal );
  }
  
  protected void markStageComplete( String loadFilename, int stageId )
  {
    long              loadFileId  = 0L;
    int               recCount    = 0;
    PreparedStatement s           = null;
    StringBuffer      sqlText     = new StringBuffer();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:3860^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init( :loadFilename )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init(  :1   )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"92com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3863^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:3865^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id(:loadFilename) 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id( :1  )  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"93com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3869^7*/

      /*@lineinfo:generated-code*//*@lineinfo:3871^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(load_file_id) 
//          from    merch_prof_load_progress lp
//          where   lp.load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(load_file_id)  \n        from    merch_prof_load_progress lp\n        where   lp.load_file_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"94com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3876^7*/    
      
      if ( recCount == 0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:3880^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_prof_load_progress
//            (
//              load_filename,
//              load_file_id
//            )
//            values
//            (
//              :loadFilename,
//              :loadFileId
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_prof_load_progress\n          (\n            load_filename,\n            load_file_id\n          )\n          values\n          (\n             :1  ,\n             :2  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"95com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setLong(2,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3892^9*/
      }
      
      
      sqlText.append("update merch_prof_load_progress set ");
      switch( stageId )
      {
        case PT_EXTRACT_SUMMARY:
          sqlText.append("summary_loaded = 'Y' ");
          break;
          
        case PT_DEBIT_COUNT_SUMMARY:
          sqlText.append("debit_summary_loaded = 'Y' ");
          break;
          
        case PT_CONTRACT_SUMMARY:
          sqlText.append("contract_summary_loaded = 'Y' ");
          break;
          
        case PT_EQUIP_SUMMARY:
          sqlText.append("equip_cost_loaded = 'Y' ");
          break;
          
        default:
          sqlText.setLength(0);
          break;
      }
      
      if ( sqlText.length() > 0 )
      {
        sqlText.append(" where load_file_id = ?" );
      
        s = getPreparedStatement(sqlText.toString());
        s.setLong( 1, loadFileId );
        s.executeUpdate();
        s.close();
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:3930^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3933^7*/
    }
    catch( Exception e )
    {
      logEntry("markStageComplete(" + loadFilename + "," + stageId + ")", e.toString());
    }
  }
  
  protected void recordTimestampBegin( long recId )
  {
    Timestamp     beginTime   = null;
    
    try
    {
      beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
    
      /*@lineinfo:generated-code*//*@lineinfo:3949^7*/

//  ************************************************************
//  #sql [Ctx] { update  merch_prof_process
//          set     process_begin_date = :beginTime
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  merch_prof_process\n        set     process_begin_date =  :1  \n        where   rec_id =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"96com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,beginTime);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3954^7*/
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "recordTimestampBegin()", e.toString());
      setError("recordTimestampBegin(): " + e.toString());
      logEntry("recordTimestampBegin()", e.toString());
    }
  }
  
  protected void recordTimestampEnd( long recId )
  {
    Timestamp     beginTime   = null;
    long          elapsed     = 0L; 
    Timestamp     endTime     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:3972^7*/

//  ************************************************************
//  #sql [Ctx] { select  process_begin_date 
//          from    merch_prof_process 
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  process_begin_date  \n        from    merch_prof_process \n        where   rec_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"97com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3977^7*/
      endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      elapsed = (endTime.getTime() - beginTime.getTime());
    
      /*@lineinfo:generated-code*//*@lineinfo:3981^7*/

//  ************************************************************
//  #sql [Ctx] { update  merch_prof_process
//          set     process_end_date = :endTime,
//                  process_elapsed = :DateTimeFormatter.getFormattedTimestamp(elapsed)
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_181 = DateTimeFormatter.getFormattedTimestamp(elapsed);
   String theSqlTS = "update  merch_prof_process\n        set     process_end_date =  :1  ,\n                process_elapsed =  :2  \n        where   rec_id =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"98com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,endTime);
   __sJT_st.setString(2,__sJT_181);
   __sJT_st.setLong(3,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3987^7*/
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "recordTimestampEnd()", e.toString());
      setError("recordTimestampEnd(): " + e.toString());
      logEntry("recordTimestampEnd()", e.toString());
    }
  }
  
  private boolean storeHierarchy( String loadFilename )
  {
    Date        activeDate      = null;
    int         bankNumber      = 0;
    int         recCount        = 0;
    boolean     retVal          = true;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:4006^7*/

//  ************************************************************
//  #sql [Ctx] { select  distinct 
//                  sm.bank_number,
//                  sm.active_date
//                          
//          from    monthly_extract_summary   sm
//          where   sm.load_file_id = load_filename_to_load_file_id(:loadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct \n                sm.bank_number,\n                sm.active_date\n                         \n        from    monthly_extract_summary   sm\n        where   sm.load_file_id = load_filename_to_load_file_id( :1  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"99com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   activeDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4014^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:4016^7*/

//  ************************************************************
//  #sql [Ctx] { select  /*+ index (th idx_th_me_descendent_date) */ 
//                  count(th.ancestor)  
//          
//          from    t_hierarchy_me th
//          where   descendent like (to_char(:bankNumber) || '%') and
//                  active_date = :activeDate      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  /*+ index (th idx_th_me_descendent_date) */ \n                count(th.ancestor)  \n         \n        from    t_hierarchy_me th\n        where   descendent like (to_char( :1  ) || '%') and\n                active_date =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"100com.mes.startup.MerchProfLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4024^7*/
      
      if ( (recCount > 0) && (ForceDelete == false) )
      {
        StringBuffer errorMsg   = new StringBuffer();
        errorMsg.append( "Bank " );
        errorMsg.append( String.valueOf(bankNumber) );
        errorMsg.append( " month end hierarchy for " );
        errorMsg.append( DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
        errorMsg.append( " already loaded!" );
        logEntry("storeHierarchy(" + loadFilename + ")", errorMsg.toString() );
      }
      else  // either table is empty or ForceDelete is enabled
      {        
        if ( ForceDelete )
        {
          /*@lineinfo:generated-code*//*@lineinfo:4040^11*/

//  ************************************************************
//  #sql [Ctx] { delete  /*+ index (th idx_th_me_descendent_date) */ 
//              from    t_hierarchy_me th
//              where   descendent like (to_char(:bankNumber) || '%') and
//                      active_date = :activeDate      
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete  /*+ index (th idx_th_me_descendent_date) */ \n            from    t_hierarchy_me th\n            where   descendent like (to_char( :1  ) || '%') and\n                    active_date =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"101com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setDate(2,activeDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4046^11*/
        }
      
        /*@lineinfo:generated-code*//*@lineinfo:4049^9*/

//  ************************************************************
//  #sql [Ctx] { insert into t_hierarchy_me 
//            ( 
//              active_date, 
//              ancestor, 
//              descendent, 
//              relation, 
//              entity_type 
//            ) 
//            select  :activeDate, 
//                    th.ancestor, 
//                    th.descendent, 
//                    th.relation, 
//                    th.entity_type 
//            from    t_hierarchy th 
//            where   th.hier_type = 1 and 
//                    substr(th.descendent,1,4) = :bankNumber
//            order by ancestor, relation, descendent        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into t_hierarchy_me \n          ( \n            active_date, \n            ancestor, \n            descendent, \n            relation, \n            entity_type \n          ) \n          select   :1  , \n                  th.ancestor, \n                  th.descendent, \n                  th.relation, \n                  th.entity_type \n          from    t_hierarchy th \n          where   th.hier_type = 1 and \n                  substr(th.descendent,1,4) =  :2  \n          order by ancestor, relation, descendent";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"102com.mes.startup.MerchProfLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activeDate);
   __sJT_st.setInt(2,bankNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4068^9*/
      }        
      
      /*@lineinfo:generated-code*//*@lineinfo:4071^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4074^7*/
    }
    catch( Exception e )
    {
      logEntry("storeHierarchy(" + loadFilename + ")", e.toString());
      retVal = false;
    }
    finally
    {
    }
    return( retVal );
  }
  
  public static void main( String[] args )
  {
    Date     beginDate       = null;
    Date     endDate         = null;
    MerchProfLoader   obj             = null;
    long              requestedNode   = 0L;
    
    SQLJConnectionBase.initStandalonePool("DEBUG");
    
    try
    {
        if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{
                    MesDefaults.DK_DISCOVER_TOP_NODE
            });
        }

      SQLJConnectionBase.initStandalonePool("DEBUG");
    
      obj = new MerchProfLoader();
      obj.connect(true);
    
      if ( args[0].equals("loadEpicorAP") )
      {
        obj.ForceDelete = true;
        
        java.util.Date  javaDate = DateTimeFormatter.parseDate(args[1],"MM/dd/yyyy");
        if ( javaDate == null ) // load by filename
        {
          obj.loadEpicorAP(args[1],Long.parseLong(args[2]));
        }
        else                    // load by date
        {
          obj.loadEpicorAP(Integer.parseInt(args[2]),new Date(javaDate.getTime()));
        }
      }        
      else if ( args[0].equals("loadContractSummary") )
      {
        System.out.println("Loading Contact Summary...");
        obj.loadContractSummary(args[1], -1L, Long.parseLong(args[2]),-1);
      }
      else if ( args[0].equals("loadDebitCountSummary") )
      {
        System.out.println("Loading Debit Count Summary...");
        obj.loadDebitCountSummary(args[1], -1L);
      }
      else if ( args[0].equals("loadEquipSummary") )
      {
        System.out.println("Loading Equip Summary...");
        obj.loadEquipSummary(args[1], Long.parseLong(args[2]));
      }
      else if ( args[0].equals("loadSummary") )
      {
        obj.loadExtractSummary(args[1]);
      }
      else if ( args[0].equals("storeHierarchy") )
      {
        log.debug("Storing hierarchy...");
        obj.ForceDelete = true;
        obj.storeHierarchy(args[1]);
      }
      else if ( args[0].equals("addMeByTid") ) 
      { 
        obj.addMeByTid(args.length < 2 ? null : args[1]);
      }
      else if ( args[0].equals("execute") )
      {
        obj.execute();
      }
    }
    catch( Exception e )
    {
      log.error(e.toString());
    }
    finally
    {
      try{ obj.cleanUp(); } catch(Exception ee){}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/