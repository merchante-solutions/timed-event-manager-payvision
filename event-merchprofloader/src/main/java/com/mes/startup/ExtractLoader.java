/*@lineinfo:filename=ExtractLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/ExtractLoader.sqlj $

  Description:  
    Summarization methods for merchant profitability.


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import com.mes.reports.BankContractBean;
import com.mes.reports.ContractTypes;

public class ExtractLoader extends EventBase
{
  public ExtractLoader()
  {
    super(true);    // enable auto-commit
  }
  
  public Date getActivityBeginDate( long merchantId, Date activeDate )
  {
    Date          beginDate     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:45^7*/

//  ************************************************************
//  #sql [Ctx] { -- since the extract arrives on the last business day
//          -- the date range for the data is not necessarily the
//          -- first day to the last day of the month.  in order
//          -- to correctly summarize the chargebacks and retrievals
//          -- it is necessary to derive the date range using the
//          -- extract date ranges.
//          select decode(smp.hh_curr_date,
//                        null, sm.active_date,
//                       (smp.month_end_date+1) )      
//          from   monthly_extract_summary      sm,
//                 monthly_extract_summary      smp
//          where  sm.merchant_number = :merchantId and
//                 sm.active_date     = :activeDate and
//                 smp.merchant_number(+) = sm.merchant_number and
//                 smp.active_date(+) = trunc(trunc(sm.active_date,'month')-1,'month')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "-- since the extract arrives on the last business day\n        -- the date range for the data is not necessarily the\n        -- first day to the last day of the month.  in order\n        -- to correctly summarize the chargebacks and retrievals\n        -- it is necessary to derive the date range using the\n        -- extract date ranges.\n        select decode(smp.hh_curr_date,\n                      null, sm.active_date,\n                     (smp.month_end_date+1) )       \n        from   monthly_extract_summary      sm,\n               monthly_extract_summary      smp\n        where  sm.merchant_number =  :1  and\n               sm.active_date     =  :2  and\n               smp.merchant_number(+) = sm.merchant_number and\n               smp.active_date(+) = trunc(trunc(sm.active_date,'month')-1,'month')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.ExtractLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:62^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "getActivityBeginDate( " + merchantId + "," + activeDate + " )", e.toString() );
    }
    return( beginDate );
  }
  
  public Date getActivityEndDate( long merchantId, Date activeDate )
  {
    Date          endDate     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:77^7*/

//  ************************************************************
//  #sql [Ctx] { select sm.month_end_date    
//          from   monthly_extract_summary      sm
//          where  sm.merchant_number = :merchantId and
//                 sm.active_date     = :activeDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sm.month_end_date     \n        from   monthly_extract_summary      sm\n        where  sm.merchant_number =  :1  and\n               sm.active_date     =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.ExtractLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   endDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:83^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "getActivityEndDate( " + merchantId + "," + activeDate + " )", e.toString() );
    }
    return( endDate );
  }
  
  public long getContractNode( long merchantId, int contractType, Date activeDate )
  {
    long        nodeId      = merchantId;
    long        retVal      = 0L;
    int         rowCount    = 0;
    
    try
    {
      while( nodeId != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:102^9*/

//  ************************************************************
//  #sql [Ctx] { select  count( abc.hierarchy_node ) 
//            from    agent_bank_contract     abc
//            where   abc.hierarchy_node = :nodeId and
//                    abc.contract_type = :contractType and
//                    :activeDate between abc.valid_date_begin and abc.valid_date_end
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( abc.hierarchy_node )  \n          from    agent_bank_contract     abc\n          where   abc.hierarchy_node =  :1  and\n                  abc.contract_type =  :2  and\n                   :3  between abc.valid_date_begin and abc.valid_date_end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.ExtractLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,contractType);
   __sJT_st.setDate(3,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:109^9*/
      
        if ( rowCount > 0 )
        {
          // found a match, break from the loop
          retVal = nodeId;
          break;
        }
      
        // no match, get the parent node id
        nodeId = getParentNode(nodeId);
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "getContractNode()", e.toString() );
    }
    return( retVal );
  }
  
  public long getLiabilityNode( long merchantId, Date activeDate )
  {
    return( getContractNode( merchantId, ContractTypes.CONTRACT_SOURCE_LIABILITY, activeDate ) );
  }
  
  public long getReferralNode( long merchantId, Date activeDate )
  {
    return( getContractNode( merchantId, ContractTypes.CONTRACT_SOURCE_REFERRAL, activeDate ) );
  }
  
  public boolean hasAppData( long merchantId )
  {
    int       rowCount      = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:145^7*/

//  ************************************************************
//  #sql [Ctx] { select  count( merch_number ) 
//          from    merchant      mr
//          where   mr.merch_number = :merchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count( merch_number )  \n        from    merchant      mr\n        where   mr.merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.ExtractLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:150^7*/
    }      
    catch( java.sql.SQLException e )
    {
      // ignore, no app data
    }
    
    return( rowCount != 0 );
  }
  
  public boolean hasCashAdvanceReferralContract( long nodeId )
  {
    int             rowCount      = 0;
    
    try
    {
      while( nodeId != 0L )
      {
        /*@lineinfo:generated-code*//*@lineinfo:168^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(abc.hierarchy_node) 
//            from    agent_bank_contract abc
//            where   abc.hierarchy_node = :nodeId and
//                    abc.contract_type = :ContractTypes.CONTRACT_SOURCE_REFERRAL and
//                    abc.billing_element_type = :BankContractBean.BET_CASH_ADV_TRANSACTION_FEE
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(abc.hierarchy_node)  \n          from    agent_bank_contract abc\n          where   abc.hierarchy_node =  :1  and\n                  abc.contract_type =  :2  and\n                  abc.billing_element_type =  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.ExtractLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,ContractTypes.CONTRACT_SOURCE_REFERRAL);
   __sJT_st.setInt(3,BankContractBean.BET_CASH_ADV_TRANSACTION_FEE);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:175^9*/
         
        if ( rowCount > 0 )
        {
          break;
        }
        nodeId = getParentNode(nodeId);
      }
    }
    catch( java.sql.SQLException e )
    {
    }
    return( nodeId != 0L );
  }
  
  public long hierarchyNodeToOrgId( long nodeId )
  {
    long        retVal      = 0L;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:196^7*/

//  ************************************************************
//  #sql [Ctx] { select  o.org_num 
//          from    organization      o
//          where   o.org_group = :nodeId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  o.org_num  \n        from    organization      o\n        where   o.org_group =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.ExtractLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:201^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "hierarchyNodeToOrgId()", e.toString() );
    }
    return( retVal );
  }
}/*@lineinfo:generated-code*/