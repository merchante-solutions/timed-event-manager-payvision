/*@lineinfo:filename=DiscoverExtractSummary*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/DiscoverExtractSummary.sqlj $

  Description:  
   Trigger to load Vital monthly extract data into the Discover summary table.


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.StringTokenizer;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.ops.QueueConstants;
import com.mes.reports.BankContractBean;
import com.mes.reports.ContractTypes;
import sqlj.runtime.ResultSetIterator;

class DiscoverExtractSummary extends SQLJConnectionBase
{
  private   static final int      CT_MC                   = 0;
  private   static final int      CT_VISA                 = 1;
  private   static final int      CT_DISC                 = 2;
  private   static final int      CT_AMEX                 = 3;
  private   static final int      CT_DINERS               = 4;
  private   static final int      CT_JCB                  = 5;
  private   static final int      CT_DEBIT                = 6;
  private   static final int      CT_CHECK                = 7;
  private   static final int      CT_COUNT                = 8;

  private static final int        CG_CHARGE_CODE          = 0;
  private static final int        CG_CHARGE_DESC          = 1;
  private static final int        CG_DISCOVER_TYPE        = 2;
  
  private static final int        FEE_INC_STATEMENT       = 0;
  private static final int        FEE_INC_APPLICATION     = 1;

  private static final String[][] ChargeRecDescToDiscoverChargeType = 
  { 
  //  code    desc                            Discover charge type
    { null ,  "STATEMENT"                 ,   Integer.toString(FEE_INC_STATEMENT)     },   // statement fees
    { null ,  "APPLICATION FEE"           ,   Integer.toString(FEE_INC_APPLICATION)   },   // application fees
  };
  
  // members
  private BankContractBean      LiabilityContract = null;
  
  private long getDiscoverHierarchyNode( )
  {
    long        retVal = 0L;
    try
    {
      retVal = MesDefaults.getLong(MesDefaults.DK_DISCOVER_TOP_NODE);
//      #sql [Ctx]
//      {
//        select value into :retVal
//        from   mes_defaults
//        where  key = :(MesDefaults.DK_DISCOVER_TOP_NODE.intValue())
//      };
    }
    catch( Exception e )
    {
      logEntry( "getDiscoverHierarchyNode()", e.toString() );
    }
    return( retVal );
  }
  
  private void loadDiscoverChargebackVolumeData( long merchantId, Date activeDate, long loadSec )
  {
    Date                      beginDate         = null;
    String                    cardType          = null;
    Date                      currDate          = null;
    ResultSetIterator         it                = null;
    ResultSet                 resultSet         = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:101^7*/

//  ************************************************************
//  #sql [Ctx] { select decode(gnp.hh_curr_date,
//                        null, gn.hh_active_date,
//                       (gnp.hh_curr_date+1) ),
//                 gn.hh_curr_date      
//          from   monthly_extract_gn      gn,
//                 monthly_extract_gn      gnp
//          where  gn.hh_load_sec        = :loadSec and
//                 gnp.hh_merchant_number(+) = gn.hh_merchant_number and
//                 gnp.hh_active_date(+) = to_date(to_char((:activeDate-15),'mm/yyyy'),'mm/yyyy')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select decode(gnp.hh_curr_date,\n                      null, gn.hh_active_date,\n                     (gnp.hh_curr_date+1) ),\n               gn.hh_curr_date       \n        from   monthly_extract_gn      gn,\n               monthly_extract_gn      gnp\n        where  gn.hh_load_sec        =  :1  and\n               gnp.hh_merchant_number(+) = gn.hh_merchant_number and\n               gnp.hh_active_date(+) = to_date(to_char(( :2 -15),'mm/yyyy'),'mm/yyyy')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.DiscoverExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setDate(2,activeDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginDate = (java.sql.Date)__sJT_rs.getDate(1);
   currDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:112^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:114^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(substr(cb.card_number,1,1),
//                                4,'VS',
//                                5,'MC,',
//                                'UK')               as card_type,
//                  count(cb.tran_amount)             as cb_count,
//                  sum(cb.tran_amount)               as cb_amount
//          from    network_chargebacks cb
//          where   cb.merchant_number = :merchantId and
//                  cb.INCOMING_DATE between :beginDate and :currDate and
//                  cb.first_time_chargeback = 'Y'
//          group by decode(substr(cb.card_number,1,1),4,'VS',5,'MC,','UK')
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(substr(cb.card_number,1,1),\n                              4,'VS',\n                              5,'MC,',\n                              'UK')               as card_type,\n                count(cb.tran_amount)             as cb_count,\n                sum(cb.tran_amount)               as cb_amount\n        from    network_chargebacks cb\n        where   cb.merchant_number =  :1  and\n                cb.INCOMING_DATE between  :2  and  :3  and\n                cb.first_time_chargeback = 'Y'\n        group by decode(substr(cb.card_number,1,1),4,'VS',5,'MC,','UK')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,currDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.DiscoverExtractSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:127^7*/
      resultSet = it.getResultSet();
     
      while( resultSet.next() )
      {
        cardType = resultSet.getString("card_type");
        if ( cardType.equals("VS") ) 
        {
          /*@lineinfo:generated-code*//*@lineinfo:135^11*/

//  ************************************************************
//  #sql [Ctx] { update discover_extract_summary
//              set visa_cb_count   = :resultSet.getInt("cb_count"),
//                  visa_cb_amount  = :resultSet.getDouble("cb_amount")
//              where load_sec = :loadSec              
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_4159 = resultSet.getInt("cb_count");
 double __sJT_4160 = resultSet.getDouble("cb_amount");
   String theSqlTS = "update discover_extract_summary\n            set visa_cb_count   =  :1 ,\n                visa_cb_amount  =  :2 \n            where load_sec =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_4159);
   __sJT_st.setDouble(2,__sJT_4160);
   __sJT_st.setLong(3,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:141^11*/
        }
        else if ( cardType.equals("MC") )
        {
          /*@lineinfo:generated-code*//*@lineinfo:145^11*/

//  ************************************************************
//  #sql [Ctx] { update discover_extract_summary
//              set mc_cb_count     = :resultSet.getInt("cb_count"),
//                  mc_cb_amount    = :resultSet.getDouble("cb_amount")
//              where load_sec = :loadSec              
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_4161 = resultSet.getInt("cb_count");
 double __sJT_4162 = resultSet.getDouble("cb_amount");
   String theSqlTS = "update discover_extract_summary\n            set mc_cb_count     =  :1 ,\n                mc_cb_amount    =  :2 \n            where load_sec =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,__sJT_4161);
   __sJT_st.setDouble(2,__sJT_4162);
   __sJT_st.setLong(3,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:151^11*/
        }
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadDiscoverChargebackVolumeData("+merchantId+","+activeDate+")",e.toString() );
    }
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
  }
  
  private void loadDiscoverChargeRecordData( long loadSec )
  {
    String                cgCode            = null;
    String                cgDesc            = null;
    double                itemAmount        = 0;
    String                itemDesc          = null;
    double                itemInc           = 0.0;
    ResultSetIterator     it                = null;
    boolean               match             = false;
    ResultSet             resultSet         = null;
    int                   incType           = -1;
    String                token             = null;
    StringTokenizer       tokenizer         = null;
    
    try
    {
      // start with equipment sales
      /*@lineinfo:generated-code*//*@lineinfo:182^7*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct gn.hh_active_date                 as active_date,
//                 cg.cg_charge_record_type                   as cg_type,
//                 decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, 
//                         0, 1,
//                         st.ST_NUMBER_OF_ITEMS_ON_STMT )    as item_count,
//                 decode( st.ST_AMOUNT_OF_ITEM_ON_STMT,
//                         0, ( decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, 
//                                      0, st.ST_FEE_AMOUNT,
//                                      st.ST_FEE_AMOUNT / st.ST_NUMBER_OF_ITEMS_ON_STMT ) ),
//                         st.ST_AMOUNT_OF_ITEM_ON_STMT )     as per_item,                       
//                 st.ST_STATEMENT_DESC                       as item_desc,
//                 st.ST_FEE_AMOUNT                           as total_amount            
//          from   monthly_extract_gn gn,
//                 monthly_extract_st st,
//                 monthly_extract_cg cg
//          where  gn.hh_load_sec         = :loadSec and
//                 st.hh_load_sec         = gn.hh_load_sec and
//                 st.st_fee_amount       > 0 and
//                 cg.hh_load_sec         = gn.hh_load_sec and
//                 cg.CG_MESSAGE_FOR_STMT = st.ST_STATEMENT_DESC
//          order by st.ST_STATEMENT_DESC
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct gn.hh_active_date                 as active_date,\n               cg.cg_charge_record_type                   as cg_type,\n               decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, \n                       0, 1,\n                       st.ST_NUMBER_OF_ITEMS_ON_STMT )    as item_count,\n               decode( st.ST_AMOUNT_OF_ITEM_ON_STMT,\n                       0, ( decode( st.ST_NUMBER_OF_ITEMS_ON_STMT, \n                                    0, st.ST_FEE_AMOUNT,\n                                    st.ST_FEE_AMOUNT / st.ST_NUMBER_OF_ITEMS_ON_STMT ) ),\n                       st.ST_AMOUNT_OF_ITEM_ON_STMT )     as per_item,                       \n               st.ST_STATEMENT_DESC                       as item_desc,\n               st.ST_FEE_AMOUNT                           as total_amount            \n        from   monthly_extract_gn gn,\n               monthly_extract_st st,\n               monthly_extract_cg cg\n        where  gn.hh_load_sec         =  :1  and\n               st.hh_load_sec         = gn.hh_load_sec and\n               st.st_fee_amount       > 0 and\n               cg.hh_load_sec         = gn.hh_load_sec and\n               cg.CG_MESSAGE_FOR_STMT = st.ST_STATEMENT_DESC\n        order by st.ST_STATEMENT_DESC";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.DiscoverExtractSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:205^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        itemDesc  = resultSet.getString("item_desc");
        match     = false;
        
        for( int i = 0; i < ChargeRecDescToDiscoverChargeType.length; ++i )
        {
          cgCode    = ChargeRecDescToDiscoverChargeType[i][CG_CHARGE_CODE];
          cgDesc    = ChargeRecDescToDiscoverChargeType[i][CG_CHARGE_DESC];
          incType   = Integer.parseInt(ChargeRecDescToDiscoverChargeType[i][CG_DISCOVER_TYPE]);
          
          if ( cgCode == null || cgCode.equals(resultSet.getString("cg_type")) )
          {
            // if the description in the lookup table is null
            // then all we are comparing is the charge record
            // type.  consider this a match and exit for loop
            if ( cgDesc == null )
            {
              match = true;
            }
            else if ( cgDesc.indexOf('%') != -1 )
            {
              // setup the tokenizer
              tokenizer = new StringTokenizer(cgDesc,"%");
              match     = true;
              
              while( tokenizer.hasMoreTokens() )
              {
                token = tokenizer.nextToken();
                if ( itemDesc.indexOf(token) == -1 )    // does not match
                {
                  match = false;
                  break;    // exit the while loop
                }
              }
            }
            else if ( itemDesc.indexOf(cgDesc) != -1 )
            {
              match = true;
            }
            if ( match == true )
            {
              break;      // exit the for loop
            }
          }
        }
        
        // ignore any charge records that do not match
        if ( match == false )
        {
          continue;
        }
        
        itemInc = resultSet.getDouble("total_amount");
        
        switch( incType )
        {
          case FEE_INC_STATEMENT:
            /*@lineinfo:generated-code*//*@lineinfo:266^13*/

//  ************************************************************
//  #sql [Ctx] { update discover_extract_summary
//                set fee_inc_statement = (nvl(fee_inc_statement,0) + :itemInc)  
//                where load_sec = :loadSec              
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update discover_extract_summary\n              set fee_inc_statement = (nvl(fee_inc_statement,0) +  :1 )  \n              where load_sec =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,itemInc);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:271^13*/
            break;
            
          case FEE_INC_APPLICATION:
            /*@lineinfo:generated-code*//*@lineinfo:275^13*/

//  ************************************************************
//  #sql [Ctx] { update discover_extract_summary
//                set fee_inc_application = (nvl(fee_inc_application,0) + :itemInc)
//                where load_sec = :loadSec              
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update discover_extract_summary\n              set fee_inc_application = (nvl(fee_inc_application,0) +  :1 )\n              where load_sec =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,itemInc);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:280^13*/
            break;
        }
      }
    }
    catch( Exception e )
    {
      logEntry( "loadDiscoverChargeRecordData("+loadSec+")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  private void loadDiscoverContractExpense( ResultSet resultSet, long loadSec )
  {
    double[]        expense   = new double[CT_COUNT];
    double          itemRate  = 0.0;
    int             mcCount   = 0;
    int             visaCount = 0;
    
    try
    {
      for( int i = 0; i < BankContractBean.BET_COUNT; ++i )
      {
        itemRate = LiabilityContract.getContractItemRate(i);
        
        switch(i)
        {
          case BankContractBean.BET_PROCESSING_PER_ITEM:  
            itemRate = LiabilityContract.getContractItemRate( BankContractBean.BET_PROCESSING_PER_ITEM );
            expense[CT_MC] += ( ( resultSet.getInt("mc_sales_count") +
                         resultSet.getInt("mc_credits_count") ) *
                       itemRate );
            expense[CT_VISA] += ( ( resultSet.getInt("visa_sales_count") +
                         resultSet.getInt("visa_credits_count") ) *
                         itemRate );
                         
            itemRate = LiabilityContract.getContractItemRate( BankContractBean.BET_PROCESSING_RATE );
            expense[CT_MC] += ( ( resultSet.getInt("mc_sales_amount") +
                         resultSet.getInt("mc_credits_amount") ) *
                       itemRate );
            expense[CT_VISA] += ( ( resultSet.getInt("visa_sales_amount") +
                            resultSet.getInt("visa_credits_amount") ) *
                            itemRate );
            break;
            
          case BankContractBean.BET_AMEX_TRANSACTION_FEE:
            expense[CT_AMEX] += ( itemRate * 
                            ( resultSet.getInt("amex_sales_count") +
                              resultSet.getInt("amex_credits_count") ) );
            break;
            
          case BankContractBean.BET_DINERS_TRANSACTION_FEE:
            expense[CT_DINERS] += ( itemRate * 
                                ( resultSet.getInt("diners_sales_count") +
                                  resultSet.getInt("diners_credits_count") ) );
            break;
            
          case BankContractBean.BET_JCB_TRANSACTION_FEE:
            expense[CT_JCB] += ( itemRate * 
                            ( resultSet.getInt("jcb_sales_count") +
                              resultSet.getInt("jcb_credits_count") ) );
            break;
            
          case BankContractBean.BET_DEBIT_TRANSACTION_FEE:
            expense[CT_DEBIT] += ( itemRate * 
                            ( resultSet.getInt("debit_sales_count") +
                              resultSet.getInt("debit_credits_count") ) );
            break;
            
          case BankContractBean.BET_VMC_AUTHORIZATION_FEE:
            /*@lineinfo:generated-code*//*@lineinfo:353^13*/

//  ************************************************************
//  #sql [Ctx] { select  nvl( sum( decode(ap.a1_plan_type,
//                                         'VS',ap.auth_count_total,
//                                          0) ), 0 ),
//                        nvl( sum( decode(ap.a1_plan_type,
//                                         'MC',ap.auth_count_total,
//                                          0) ), 0 ) 
//                from    monthly_extract_ap    ap
//                where   ap.hh_load_sec = :loadSec                                  
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl( sum( decode(ap.a1_plan_type,\n                                       'VS',ap.auth_count_total,\n                                        0) ), 0 ),\n                      nvl( sum( decode(ap.a1_plan_type,\n                                       'MC',ap.auth_count_total,\n                                        0) ), 0 )  \n              from    monthly_extract_ap    ap\n              where   ap.hh_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.DiscoverExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   visaCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:363^13*/
            expense[CT_MC] += ( itemRate * mcCount );
            expense[CT_VISA] += ( itemRate * visaCount );
            break;
            
          case BankContractBean.BET_AMEX_AUTHORIZATION_FEE:
            expense[CT_AMEX] += ( itemRate * resultSet.getInt("amex_auth_count") );
            break;
            
          case BankContractBean.BET_DINERS_AUTHORIZATION_FEE:
            expense[CT_DINERS] += ( itemRate * resultSet.getInt("diners_auth_count") );
            break;
            
          case BankContractBean.BET_JCB_AUTHORIZATION_FEE:
            expense[CT_JCB] += ( itemRate * resultSet.getInt("jcb_auth_count") );
            break;
            
          default:    // not supported
            continue;
        }
      }
      
      // insert the data
      /*@lineinfo:generated-code*//*@lineinfo:386^7*/

//  ************************************************************
//  #sql [Ctx] { update discover_extract_summary
//          set     visa_contract_expense   = :expense[CT_VISA],
//                  mc_contract_expense     = :expense[CT_MC],
//                  amex_contract_expense   = :expense[CT_AMEX],
//                  diners_contract_expense = :expense[CT_DINERS],
//                  jcb_contract_expense    = :expense[CT_JCB],
//                  debit_contract_expense  = :expense[CT_DEBIT]
//          where   load_sec = :loadSec                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4163 = expense[CT_VISA];
 double __sJT_4164 = expense[CT_MC];
 double __sJT_4165 = expense[CT_AMEX];
 double __sJT_4166 = expense[CT_DINERS];
 double __sJT_4167 = expense[CT_JCB];
 double __sJT_4168 = expense[CT_DEBIT];
   String theSqlTS = "update discover_extract_summary\n        set     visa_contract_expense   =  :1 ,\n                mc_contract_expense     =  :2 ,\n                amex_contract_expense   =  :3 ,\n                diners_contract_expense =  :4 ,\n                jcb_contract_expense    =  :5 ,\n                debit_contract_expense  =  :6 \n        where   load_sec =  :7";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_4163);
   __sJT_st.setDouble(2,__sJT_4164);
   __sJT_st.setDouble(3,__sJT_4165);
   __sJT_st.setDouble(4,__sJT_4166);
   __sJT_st.setDouble(5,__sJT_4167);
   __sJT_st.setDouble(6,__sJT_4168);
   __sJT_st.setLong(7,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:396^7*/
    }
    catch( Exception e )
    {
      logEntry( "loadDiscoverContractExpense()", e.toString() );
    }
    finally
    {
    }
  }
  
  public synchronized boolean loadDiscoverExtractSummary( String loadFilename )
  {
    Date                  activeDate        = null;
    ResultSetIterator     it                = null;
    boolean               fileProcessed     = false;
    long                  loadSec           = 0L;
    long                  merchantId        = 0L;
    ResultSet             resultSet         = null;
    long                  topNode           = getDiscoverHierarchyNode();
    
    try
    {
      LiabilityContract = new BankContractBean();
      LiabilityContract.connect(true);
      
      // remove any existing entries for this load filename
      /*@lineinfo:generated-code*//*@lineinfo:423^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from    discover_extract_summary
//          where   load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from    discover_extract_summary\n        where   load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:428^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:430^7*/

//  ************************************************************
//  #sql [Ctx] { select  distinct sm.active_date 
//          from  organization                o,
//                group_merchant              gm,
//                monthly_extract_summary     sm
//          where o.org_group         = :topNode and
//                gm.org_num          = o.org_num and 
//                sm.merchant_number  = gm.merchant_number and
//                sm.load_filename    = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct sm.active_date  \n        from  organization                o,\n              group_merchant              gm,\n              monthly_extract_summary     sm\n        where o.org_group         =  :1  and\n              gm.org_num          = o.org_num and \n              sm.merchant_number  = gm.merchant_number and\n              sm.load_filename    =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.DiscoverExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,topNode);
   __sJT_st.setString(2,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activeDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:440^7*/
      
      // load the Discover contract
      LiabilityContract.loadContract( LiabilityContract.hierarchyNodeToOrgId(topNode),
                                      ContractTypes.CONTRACT_SOURCE_LIABILITY,
                                      activeDate );
                                  
      // query all the data from the vital monthly extract files
      /*@lineinfo:generated-code*//*@lineinfo:448^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.hh_load_sec                                      as load_sec,
//                  sm.merchant_number                                  as merchant_number,
//                  nvl( mf.dmdsnum,
//                       mpo.merchpo_card_merch_number )                as discover_merchant_number,
//                  sm.active_date                                      as active_date,
//                  decode( at.DATE_COMPLETED,
//                          null, 
//                          to_date(decode(mf.date_opened,
//                                           '000000','010100',
//                                           mf.date_opened),'mmddyy'),
//                          trunc(at.date_completed))                   as approval_date,
//                  mf.date_stat_chgd_to_dcb                            as cancel_date,
//                  decode(substr(mf.user_data_4,16),
//                         'C','Y',null)                                as converted,
//                  nvl(mr.discover_rep_id, u.third_party_user_id)      as disc_rep_id,
//                  pl_volume_sales_amount_query('M%',sm.hh_load_sec)   as mc_sales_amount,
//                  pl_volume_sales_count_query('M%',sm.hh_load_sec)    as mc_sales_count,
//                  pl_volume_credits_amount_query('M%',sm.hh_load_sec) as mc_credits_amount,
//                  pl_volume_credits_count_query('M%',sm.hh_load_sec)  as mc_credits_count,
//                  pl_volume_sales_amount_query('V%',sm.hh_load_sec)   as visa_sales_amount,
//                  pl_volume_sales_count_query('V%',sm.hh_load_sec)    as visa_sales_count,
//                  pl_volume_credits_amount_query('V%',sm.hh_load_sec) as visa_credits_amount,
//                  pl_volume_credits_count_query('V%',sm.hh_load_sec)  as visa_credits_count,
//                  nvl(sm.amex_sales_amount,0)                         as amex_sales_amount,
//                  nvl(sm.amex_sales_count,0 )                         as amex_sales_count,
//                  nvl(sm.amex_credits_amount,0)                       as amex_credits_amount,
//                  nvl(sm.amex_credits_count,0 )                       as amex_credits_count,
//                  nvl(sm.disc_sales_amount,0)                         as disc_sales_amount,
//                  nvl(sm.disc_sales_count,0 )                         as disc_sales_count,
//                  nvl(sm.disc_credits_amount,0)                       as disc_credits_amount,
//                  nvl(sm.disc_credits_count,0 )                       as disc_credits_count,
//                  nvl(sm.dinr_sales_amount,0)                         as diners_sales_amount,
//                  nvl(sm.dinr_sales_count,0 )                         as diners_sales_count,
//                  nvl(sm.dinr_credits_amount,0)                       as diners_credits_amount,
//                  nvl(sm.dinr_credits_count,0 )                       as diners_credits_count,
//                  nvl(sm.jcb_sales_amount,0)                          as jcb_sales_amount,
//                  nvl(sm.jcb_sales_count,0 )                          as jcb_sales_count,
//                  nvl(sm.jcb_credits_amount,0)                        as jcb_credits_amount,
//                  nvl(sm.jcb_credits_count,0 )                        as jcb_credits_count,
//                  nvl(sm.debit_sales_amount,0)                        as debit_sales_amount,
//                  nvl(sm.debit_sales_count,0 )                        as debit_sales_count,
//                  nvl(sm.debit_credits_amount,0)                      as debit_credits_amount,
//                  nvl(sm.debit_credits_count,0 )                      as debit_credits_count,
//                  nvl(sm.visa_interchange,0)                          as visa_ic_expense,
//                  nvl(sm.mc_interchange,0)                            as mc_ic_expense,
//                  nvl(sm.amex_auth_count,0)                           as amex_auth_count,
//                  nvl(sm.dinr_auth_count,0)                           as diners_auth_count,
//                  nvl(sm.jcb_auth_count,0)                            as jcb_auth_count
//          from  organization                o,
//                group_merchant              gm,
//                monthly_extract_summary     sm,
//                mif                         mf,
//                merchant                    mr,
//                application                 ap,
//                app_tracking                at,
//                merchpayoption              mpo,
//                users                       u
//          where o.org_group         = :topNode and
//                gm.org_num          = o.org_num and 
//                sm.merchant_number  = gm.merchant_number and
//                sm.load_filename    = :loadFilename and
//                mf.merchant_number  = sm.merchant_number and
//                mr.merch_number     = mf.merchant_number and
//                ap.app_seq_num      = mr.app_seq_num and
//                u.user_id           = ap.app_user_id and
//                at.app_seq_num(+)   = mr.app_seq_num and
//                at.dept_code(+)     = :QueueConstants.DEPARTMENT_CREDIT and -- 100 and
//                at.status_code(+)   = :QueueConstants.DEPT_STATUS_CREDIT_APPROVED and -- 102 and
//                mpo.app_seq_num(+)  = mr.app_seq_num and
//                mpo.cardtype_code(+)= :mesConstants.APP_CT_DISCOVER -- 14
//          order by mf.dba_name, sm.active_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.hh_load_sec                                      as load_sec,\n                sm.merchant_number                                  as merchant_number,\n                nvl( mf.dmdsnum,\n                     mpo.merchpo_card_merch_number )                as discover_merchant_number,\n                sm.active_date                                      as active_date,\n                decode( at.DATE_COMPLETED,\n                        null, \n                        to_date(decode(mf.date_opened,\n                                         '000000','010100',\n                                         mf.date_opened),'mmddyy'),\n                        trunc(at.date_completed))                   as approval_date,\n                mf.date_stat_chgd_to_dcb                            as cancel_date,\n                decode(substr(mf.user_data_4,16),\n                       'C','Y',null)                                as converted,\n                nvl(mr.discover_rep_id, u.third_party_user_id)      as disc_rep_id,\n                pl_volume_sales_amount_query('M%',sm.hh_load_sec)   as mc_sales_amount,\n                pl_volume_sales_count_query('M%',sm.hh_load_sec)    as mc_sales_count,\n                pl_volume_credits_amount_query('M%',sm.hh_load_sec) as mc_credits_amount,\n                pl_volume_credits_count_query('M%',sm.hh_load_sec)  as mc_credits_count,\n                pl_volume_sales_amount_query('V%',sm.hh_load_sec)   as visa_sales_amount,\n                pl_volume_sales_count_query('V%',sm.hh_load_sec)    as visa_sales_count,\n                pl_volume_credits_amount_query('V%',sm.hh_load_sec) as visa_credits_amount,\n                pl_volume_credits_count_query('V%',sm.hh_load_sec)  as visa_credits_count,\n                nvl(sm.amex_sales_amount,0)                         as amex_sales_amount,\n                nvl(sm.amex_sales_count,0 )                         as amex_sales_count,\n                nvl(sm.amex_credits_amount,0)                       as amex_credits_amount,\n                nvl(sm.amex_credits_count,0 )                       as amex_credits_count,\n                nvl(sm.disc_sales_amount,0)                         as disc_sales_amount,\n                nvl(sm.disc_sales_count,0 )                         as disc_sales_count,\n                nvl(sm.disc_credits_amount,0)                       as disc_credits_amount,\n                nvl(sm.disc_credits_count,0 )                       as disc_credits_count,\n                nvl(sm.dinr_sales_amount,0)                         as diners_sales_amount,\n                nvl(sm.dinr_sales_count,0 )                         as diners_sales_count,\n                nvl(sm.dinr_credits_amount,0)                       as diners_credits_amount,\n                nvl(sm.dinr_credits_count,0 )                       as diners_credits_count,\n                nvl(sm.jcb_sales_amount,0)                          as jcb_sales_amount,\n                nvl(sm.jcb_sales_count,0 )                          as jcb_sales_count,\n                nvl(sm.jcb_credits_amount,0)                        as jcb_credits_amount,\n                nvl(sm.jcb_credits_count,0 )                        as jcb_credits_count,\n                nvl(sm.debit_sales_amount,0)                        as debit_sales_amount,\n                nvl(sm.debit_sales_count,0 )                        as debit_sales_count,\n                nvl(sm.debit_credits_amount,0)                      as debit_credits_amount,\n                nvl(sm.debit_credits_count,0 )                      as debit_credits_count,\n                nvl(sm.visa_interchange,0)                          as visa_ic_expense,\n                nvl(sm.mc_interchange,0)                            as mc_ic_expense,\n                nvl(sm.amex_auth_count,0)                           as amex_auth_count,\n                nvl(sm.dinr_auth_count,0)                           as diners_auth_count,\n                nvl(sm.jcb_auth_count,0)                            as jcb_auth_count\n        from  organization                o,\n              group_merchant              gm,\n              monthly_extract_summary     sm,\n              mif                         mf,\n              merchant                    mr,\n              application                 ap,\n              app_tracking                at,\n              merchpayoption              mpo,\n              users                       u\n        where o.org_group         =  :1  and\n              gm.org_num          = o.org_num and \n              sm.merchant_number  = gm.merchant_number and\n              sm.load_filename    =  :2  and\n              mf.merchant_number  = sm.merchant_number and\n              mr.merch_number     = mf.merchant_number and\n              ap.app_seq_num      = mr.app_seq_num and\n              u.user_id           = ap.app_user_id and\n              at.app_seq_num(+)   = mr.app_seq_num and\n              at.dept_code(+)     =  :3  and -- 100 and\n              at.status_code(+)   =  :4  and -- 102 and\n              mpo.app_seq_num(+)  = mr.app_seq_num and\n              mpo.cardtype_code(+)=  :5  -- 14\n        order by mf.dba_name, sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,topNode);
   __sJT_st.setString(2,loadFilename);
   __sJT_st.setInt(3,QueueConstants.DEPARTMENT_CREDIT);
   __sJT_st.setInt(4,QueueConstants.DEPT_STATUS_CREDIT_APPROVED);
   __sJT_st.setInt(5,mesConstants.APP_CT_DISCOVER);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.startup.DiscoverExtractSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:521^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        loadSec     = resultSet.getLong("load_sec");
        merchantId  = resultSet.getLong("merchant_number");
        
        /*@lineinfo:generated-code*//*@lineinfo:529^9*/

//  ************************************************************
//  #sql [Ctx] { insert into discover_extract_summary
//            (
//              load_sec,      
//              merchant_number,           
//              discover_merchant_number,  
//              active_date,               
//              converted,                 
//              approval_date,             
//              cancel_date,               
//              rep_id,                    
//              visa_ic_expense,           
//              visa_sales_count,      
//              visa_sales_amount,     
//              visa_credits_count,    
//              visa_credits_amount,   
//              mc_ic_expense,             
//              mc_sales_count,        
//              mc_sales_amount,       
//              mc_credits_count,      
//              mc_credits_amount,     
//              amex_sales_count,      
//              amex_sales_amount,     
//              amex_credits_count,    
//              amex_credits_amount,   
//              diners_sales_count,
//              diners_sales_amount,
//              diners_credits_count,
//              diners_credits_amount,
//              jcb_sales_count,
//              jcb_sales_amount,
//              jcb_credits_count,
//              jcb_credits_amount,
//              debit_sales_count,
//              debit_sales_amount,
//              debit_credits_count,
//              debit_credits_amount,
//              load_filename
//            )
//            values
//            (
//              :loadSec,
//              :resultSet.getLong("merchant_number"),
//              :resultSet.getLong("discover_merchant_number"),
//              :resultSet.getDate("active_date"),
//              :resultSet.getString("converted"),
//              :resultSet.getDate("approval_date"),
//              :resultSet.getDate("cancel_date"),
//              :resultSet.getString("disc_rep_id"),
//              :resultSet.getDouble("visa_ic_expense"),
//              :resultSet.getInt("visa_sales_count"),
//              :resultSet.getDouble("visa_sales_amount"),
//              :resultSet.getInt("visa_credits_count"),
//              :resultSet.getDouble("visa_credits_amount"),
//              :resultSet.getDouble("mc_ic_expense"),
//              :resultSet.getInt("mc_sales_count"),
//              :resultSet.getDouble("mc_sales_amount"),
//              :resultSet.getInt("mc_credits_count"),
//              :resultSet.getDouble("mc_credits_amount"),
//              :resultSet.getInt("amex_sales_count"),
//              :resultSet.getDouble("amex_sales_amount"),
//              :resultSet.getInt("amex_credits_count"),
//              :resultSet.getDouble("amex_credits_amount"),
//              :resultSet.getInt("diners_sales_count"),
//              :resultSet.getDouble("diners_sales_amount"),
//              :resultSet.getInt("diners_credits_count"),
//              :resultSet.getDouble("diners_credits_amount"),
//              :resultSet.getInt("jcb_sales_count"),
//              :resultSet.getDouble("jcb_sales_amount"),
//              :resultSet.getInt("jcb_credits_count"),
//              :resultSet.getDouble("jcb_credits_amount"),
//              :resultSet.getInt("debit_sales_count"),
//              :resultSet.getDouble("debit_sales_amount"),
//              :resultSet.getInt("debit_credits_count"),
//              :resultSet.getDouble("debit_credits_amount"),
//              :loadFilename
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4169 = resultSet.getLong("merchant_number");
 long __sJT_4170 = resultSet.getLong("discover_merchant_number");
 java.sql.Date __sJT_4171 = resultSet.getDate("active_date");
 String __sJT_4172 = resultSet.getString("converted");
 java.sql.Date __sJT_4173 = resultSet.getDate("approval_date");
 java.sql.Date __sJT_4174 = resultSet.getDate("cancel_date");
 String __sJT_4175 = resultSet.getString("disc_rep_id");
 double __sJT_4176 = resultSet.getDouble("visa_ic_expense");
 int __sJT_4177 = resultSet.getInt("visa_sales_count");
 double __sJT_4178 = resultSet.getDouble("visa_sales_amount");
 int __sJT_4179 = resultSet.getInt("visa_credits_count");
 double __sJT_4180 = resultSet.getDouble("visa_credits_amount");
 double __sJT_4181 = resultSet.getDouble("mc_ic_expense");
 int __sJT_4182 = resultSet.getInt("mc_sales_count");
 double __sJT_4183 = resultSet.getDouble("mc_sales_amount");
 int __sJT_4184 = resultSet.getInt("mc_credits_count");
 double __sJT_4185 = resultSet.getDouble("mc_credits_amount");
 int __sJT_4186 = resultSet.getInt("amex_sales_count");
 double __sJT_4187 = resultSet.getDouble("amex_sales_amount");
 int __sJT_4188 = resultSet.getInt("amex_credits_count");
 double __sJT_4189 = resultSet.getDouble("amex_credits_amount");
 int __sJT_4190 = resultSet.getInt("diners_sales_count");
 double __sJT_4191 = resultSet.getDouble("diners_sales_amount");
 int __sJT_4192 = resultSet.getInt("diners_credits_count");
 double __sJT_4193 = resultSet.getDouble("diners_credits_amount");
 int __sJT_4194 = resultSet.getInt("jcb_sales_count");
 double __sJT_4195 = resultSet.getDouble("jcb_sales_amount");
 int __sJT_4196 = resultSet.getInt("jcb_credits_count");
 double __sJT_4197 = resultSet.getDouble("jcb_credits_amount");
 int __sJT_4198 = resultSet.getInt("debit_sales_count");
 double __sJT_4199 = resultSet.getDouble("debit_sales_amount");
 int __sJT_4200 = resultSet.getInt("debit_credits_count");
 double __sJT_4201 = resultSet.getDouble("debit_credits_amount");
   String theSqlTS = "insert into discover_extract_summary\n          (\n            load_sec,      \n            merchant_number,           \n            discover_merchant_number,  \n            active_date,               \n            converted,                 \n            approval_date,             \n            cancel_date,               \n            rep_id,                    \n            visa_ic_expense,           \n            visa_sales_count,      \n            visa_sales_amount,     \n            visa_credits_count,    \n            visa_credits_amount,   \n            mc_ic_expense,             \n            mc_sales_count,        \n            mc_sales_amount,       \n            mc_credits_count,      \n            mc_credits_amount,     \n            amex_sales_count,      \n            amex_sales_amount,     \n            amex_credits_count,    \n            amex_credits_amount,   \n            diners_sales_count,\n            diners_sales_amount,\n            diners_credits_count,\n            diners_credits_amount,\n            jcb_sales_count,\n            jcb_sales_amount,\n            jcb_credits_count,\n            jcb_credits_amount,\n            debit_sales_count,\n            debit_sales_amount,\n            debit_credits_count,\n            debit_credits_amount,\n            load_filename\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10 ,\n             :11 ,\n             :12 ,\n             :13 ,\n             :14 ,\n             :15 ,\n             :16 ,\n             :17 ,\n             :18 ,\n             :19 ,\n             :20 ,\n             :21 ,\n             :22 ,\n             :23 ,\n             :24 ,\n             :25 ,\n             :26 ,\n             :27 ,\n             :28 ,\n             :29 ,\n             :30 ,\n             :31 ,\n             :32 ,\n             :33 ,\n             :34 ,\n             :35 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setLong(2,__sJT_4169);
   __sJT_st.setLong(3,__sJT_4170);
   __sJT_st.setDate(4,__sJT_4171);
   __sJT_st.setString(5,__sJT_4172);
   __sJT_st.setDate(6,__sJT_4173);
   __sJT_st.setDate(7,__sJT_4174);
   __sJT_st.setString(8,__sJT_4175);
   __sJT_st.setDouble(9,__sJT_4176);
   __sJT_st.setInt(10,__sJT_4177);
   __sJT_st.setDouble(11,__sJT_4178);
   __sJT_st.setInt(12,__sJT_4179);
   __sJT_st.setDouble(13,__sJT_4180);
   __sJT_st.setDouble(14,__sJT_4181);
   __sJT_st.setInt(15,__sJT_4182);
   __sJT_st.setDouble(16,__sJT_4183);
   __sJT_st.setInt(17,__sJT_4184);
   __sJT_st.setDouble(18,__sJT_4185);
   __sJT_st.setInt(19,__sJT_4186);
   __sJT_st.setDouble(20,__sJT_4187);
   __sJT_st.setInt(21,__sJT_4188);
   __sJT_st.setDouble(22,__sJT_4189);
   __sJT_st.setInt(23,__sJT_4190);
   __sJT_st.setDouble(24,__sJT_4191);
   __sJT_st.setInt(25,__sJT_4192);
   __sJT_st.setDouble(26,__sJT_4193);
   __sJT_st.setInt(27,__sJT_4194);
   __sJT_st.setDouble(28,__sJT_4195);
   __sJT_st.setInt(29,__sJT_4196);
   __sJT_st.setDouble(30,__sJT_4197);
   __sJT_st.setInt(31,__sJT_4198);
   __sJT_st.setDouble(32,__sJT_4199);
   __sJT_st.setInt(33,__sJT_4200);
   __sJT_st.setDouble(34,__sJT_4201);
   __sJT_st.setString(35,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:607^9*/
        
        loadDiscoverDiscIcIncome(loadSec);
        loadDiscoverFeeIncome(loadSec);
        loadDiscoverChargebackVolumeData(merchantId, activeDate, loadSec);
        loadDiscoverChargeRecordData(loadSec);
        loadDiscoverContractExpense( resultSet, loadSec );
        
        /*@lineinfo:generated-code*//*@lineinfo:615^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:618^9*/
      }   // end while( resultSet.next() )
      it.close();
      
      // load the month end discover equipment entries
      loadDiscoverEquipmentSummary( loadFilename );
      
      fileProcessed = true;
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadDiscoverExtractSummary(" + loadFilename + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
      try{ LiabilityContract.cleanUp(); } catch( Exception e ) {}
      LiabilityContract = null;
    }
    return( fileProcessed );
  }
  
  private void loadDiscoverDiscIcIncome( long loadSec )
  {
    double                calcDiscount      = 0.0;
    double                discount          = 0.0;
    ResultSetIterator     it                = null;
    double                mcIcInc           = 0.0;
    ResultSet             resultSet         = null;
    double                totalDiscount     = 0.0;
    double                visaIcInc         = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:652^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  gn.t1_tot_calculated_discount     as calc_disc,
//                  substr(pl.pl_plan_type,1,1)       as plan_type,
//                  sum(pl.PL_CORRECT_DISC_AMT)       as disc_amount
//          from    monthly_extract_gn                gn,
//                  monthly_extract_pl                pl
//          where   gn.hh_load_sec = :loadSec and
//                  pl.hh_load_sec = gn.hh_load_sec and
//                  substr(pl.pl_plan_type,1,1) in ('V','M')
//          group by gn.t1_tot_calculated_discount,
//                   substr(pl.pl_plan_type,1,1)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  gn.t1_tot_calculated_discount     as calc_disc,\n                substr(pl.pl_plan_type,1,1)       as plan_type,\n                sum(pl.PL_CORRECT_DISC_AMT)       as disc_amount\n        from    monthly_extract_gn                gn,\n                monthly_extract_pl                pl\n        where   gn.hh_load_sec =  :1  and\n                pl.hh_load_sec = gn.hh_load_sec and\n                substr(pl.pl_plan_type,1,1) in ('V','M')\n        group by gn.t1_tot_calculated_discount,\n                 substr(pl.pl_plan_type,1,1)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"13com.mes.startup.DiscoverExtractSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:664^7*/
      resultSet = it.getResultSet();
      
      if ( resultSet.next() )
      {
        totalDiscount = resultSet.getDouble("calc_disc");
        do
        {
          discount      = resultSet.getDouble("disc_amount");
          calcDiscount += discount;
          
          if ( resultSet.getString("plan_type").equals("V") )
          {
            /*@lineinfo:generated-code*//*@lineinfo:677^13*/

//  ************************************************************
//  #sql [Ctx] { update  discover_extract_summary
//                set     visa_discount = :discount
//                where   load_sec = :loadSec
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  discover_extract_summary\n              set     visa_discount =  :1 \n              where   load_sec =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,discount);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:682^13*/
          }
          else    // MC
          {
            /*@lineinfo:generated-code*//*@lineinfo:686^13*/

//  ************************************************************
//  #sql [Ctx] { update  discover_extract_summary
//                set     mc_discount   = :discount
//                where   load_sec = :loadSec
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  discover_extract_summary\n              set     mc_discount   =  :1 \n              where   load_sec =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"15com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,discount);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:691^13*/
          }
        }
        while( resultSet.next() );
        
        if ( calcDiscount != totalDiscount )
        {
          /*@lineinfo:generated-code*//*@lineinfo:698^11*/

//  ************************************************************
//  #sql [Ctx] { update  discover_extract_summary
//              set     fee_inc_min_discount = round(:totalDiscount - calcDiscount,0)
//              where   load_sec = :loadSec
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4202 = totalDiscount - calcDiscount;
   String theSqlTS = "update  discover_extract_summary\n            set     fee_inc_min_discount = round( :1 ,0)\n            where   load_sec =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"16com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_4202);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:703^11*/
        }
      }
      it.close();
      
      // Interchange
      /*@lineinfo:generated-code*//*@lineinfo:709^7*/

//  ************************************************************
//  #sql [Ctx] { select nvl((vs.V1_INC_CAT_01_FEES +
//                  vs.V1_INC_CAT_01_SALES +
//                  vs.V1_INC_CAT_02_FEES +
//                  vs.V1_INC_CAT_02_SALES +
//                  vs.V1_INC_CAT_03_FEES +
//                  vs.V1_INC_CAT_03_SALES +
//                  vs.V1_INC_CAT_04_FEES +
//                  vs.V1_INC_CAT_04_SALES +
//                  vs.V1_INC_CAT_05_FEES +
//                  vs.V1_INC_CAT_05_SALES +
//                  vs.V1_INC_CAT_06_FEES +
//                  vs.V1_INC_CAT_06_SALES +
//                  vs.V1_INC_CAT_07_FEES +
//                  vs.V1_INC_CAT_07_SALES +
//                  vs.V1_INC_CAT_08_FEES +
//                  vs.V1_INC_CAT_08_SALES +
//                  vs.V1_INC_CAT_09_FEES +
//                  vs.V1_INC_CAT_09_SALES +
//                  vs.V1_INC_CAT_10_FEES +
//                  vs.V1_INC_CAT_10_SALES +
//                  vs.V1_INC_CAT_11_FEES +
//                  vs.V1_INC_CAT_11_SALES +
//                  vs.V1_INC_CAT_12_FEES +
//                  vs.V1_INC_CAT_12_SALES +
//                  vs.V1_INC_CAT_13_FEES +
//                  vs.V1_INC_CAT_13_SALES +
//                  vs.V1_INC_CAT_14_FEES +
//                  vs.V1_INC_CAT_14_SALES +
//                  vs.V1_INC_CAT_15_FEES +
//                  vs.V1_INC_CAT_15_SALES +
//                  vs.V1_INC_CAT_16_FEES +
//                  vs.V1_INC_CAT_16_SALES +
//                  vs.V1_INC_CAT_17_SALES +
//                  vs.V1_INCOME_ASSESSMENT +
//                  vs.V1_INCOME_BULLETIN +
//                  vs.V1_INCOME_INTRA_CHANGE +
//                  vs.V2_INC_CAT_17_FEES +
//                  vs.V2_INC_CAT_18_FEES +
//                  vs.V2_INC_CAT_18_SALES +
//                  vs.V2_INC_CAT_19_FEES +
//                  vs.V2_INC_CAT_19_SALES +
//                  vs.V2_INC_CAT_20_FEES +
//                  vs.V2_INC_CAT_20_SALES +
//                  vs.V2_INC_CAT_21_FEES +
//                  vs.V2_INC_CAT_21_SALES +
//                  vs.V2_INC_CAT_22_FEES +
//                  vs.V2_INC_CAT_22_SALES +
//                  vs.V2_INC_CAT_23_FEES +
//                  vs.V2_INC_CAT_23_SALES +
//                  vs.V2_INC_CAT_24_FEES +
//                  vs.V2_INC_CAT_24_SALES +
//                  vs.V2_INC_CAT_25_FEES),0),
//                 nvl((mc.M1_INC_AID_FOR_INTERCHANGE +
//                  mc.M1_INC_AID_FOR_QUAL +
//                  mc.M1_INC_CAT_01_FEES +
//                  mc.M1_INC_CAT_01_SALES +
//                  mc.M1_INC_CAT_02_FEES +
//                  mc.M1_INC_CAT_02_SALES +
//                  mc.M1_INC_CAT_03_FEES +
//                  mc.M1_INC_CAT_03_SALES +
//                  mc.M1_INC_CAT_04_FEES +
//                  mc.M1_INC_CAT_04_SALES +
//                  mc.M1_INC_CAT_05_FEES +
//                  mc.M1_INC_CAT_05_SALES +
//                  mc.M1_INC_CAT_06_FEES +
//                  mc.M1_INC_CAT_06_SALES +
//                  mc.M1_INC_CAT_07_FEES +
//                  mc.M1_INC_CAT_07_SALES +
//                  mc.M1_INC_CAT_08_FEES +
//                  mc.M1_INC_CAT_08_SALES +
//                  mc.M1_INC_CAT_09_FEES +
//                  mc.M1_INC_CAT_09_SALES +
//                  mc.M1_INC_CAT_10_FEES +
//                  mc.M1_INC_CAT_10_SALES +
//                  mc.M1_INC_CAT_11_FEES +
//                  mc.M1_INC_CAT_11_SALES +
//                  mc.M1_INC_CAT_12_FEES +
//                  mc.M1_INC_CAT_12_SALES +
//                  mc.M1_INCOME_ASSESSMENT +
//                  mc.M1_INCOME_BULLETIN +
//                  mc.M1_INCOME_INTRA_CHANGE),0) 
//          from    monthly_extract_visa    vs,
//                  monthly_extract_mc      mc
//          where   vs.hh_load_sec(+) = :loadSec and
//                  mc.hh_load_sec(+) = :loadSec 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select nvl((vs.V1_INC_CAT_01_FEES +\n                vs.V1_INC_CAT_01_SALES +\n                vs.V1_INC_CAT_02_FEES +\n                vs.V1_INC_CAT_02_SALES +\n                vs.V1_INC_CAT_03_FEES +\n                vs.V1_INC_CAT_03_SALES +\n                vs.V1_INC_CAT_04_FEES +\n                vs.V1_INC_CAT_04_SALES +\n                vs.V1_INC_CAT_05_FEES +\n                vs.V1_INC_CAT_05_SALES +\n                vs.V1_INC_CAT_06_FEES +\n                vs.V1_INC_CAT_06_SALES +\n                vs.V1_INC_CAT_07_FEES +\n                vs.V1_INC_CAT_07_SALES +\n                vs.V1_INC_CAT_08_FEES +\n                vs.V1_INC_CAT_08_SALES +\n                vs.V1_INC_CAT_09_FEES +\n                vs.V1_INC_CAT_09_SALES +\n                vs.V1_INC_CAT_10_FEES +\n                vs.V1_INC_CAT_10_SALES +\n                vs.V1_INC_CAT_11_FEES +\n                vs.V1_INC_CAT_11_SALES +\n                vs.V1_INC_CAT_12_FEES +\n                vs.V1_INC_CAT_12_SALES +\n                vs.V1_INC_CAT_13_FEES +\n                vs.V1_INC_CAT_13_SALES +\n                vs.V1_INC_CAT_14_FEES +\n                vs.V1_INC_CAT_14_SALES +\n                vs.V1_INC_CAT_15_FEES +\n                vs.V1_INC_CAT_15_SALES +\n                vs.V1_INC_CAT_16_FEES +\n                vs.V1_INC_CAT_16_SALES +\n                vs.V1_INC_CAT_17_SALES +\n                vs.V1_INCOME_ASSESSMENT +\n                vs.V1_INCOME_BULLETIN +\n                vs.V1_INCOME_INTRA_CHANGE +\n                vs.V2_INC_CAT_17_FEES +\n                vs.V2_INC_CAT_18_FEES +\n                vs.V2_INC_CAT_18_SALES +\n                vs.V2_INC_CAT_19_FEES +\n                vs.V2_INC_CAT_19_SALES +\n                vs.V2_INC_CAT_20_FEES +\n                vs.V2_INC_CAT_20_SALES +\n                vs.V2_INC_CAT_21_FEES +\n                vs.V2_INC_CAT_21_SALES +\n                vs.V2_INC_CAT_22_FEES +\n                vs.V2_INC_CAT_22_SALES +\n                vs.V2_INC_CAT_23_FEES +\n                vs.V2_INC_CAT_23_SALES +\n                vs.V2_INC_CAT_24_FEES +\n                vs.V2_INC_CAT_24_SALES +\n                vs.V2_INC_CAT_25_FEES),0),\n               nvl((mc.M1_INC_AID_FOR_INTERCHANGE +\n                mc.M1_INC_AID_FOR_QUAL +\n                mc.M1_INC_CAT_01_FEES +\n                mc.M1_INC_CAT_01_SALES +\n                mc.M1_INC_CAT_02_FEES +\n                mc.M1_INC_CAT_02_SALES +\n                mc.M1_INC_CAT_03_FEES +\n                mc.M1_INC_CAT_03_SALES +\n                mc.M1_INC_CAT_04_FEES +\n                mc.M1_INC_CAT_04_SALES +\n                mc.M1_INC_CAT_05_FEES +\n                mc.M1_INC_CAT_05_SALES +\n                mc.M1_INC_CAT_06_FEES +\n                mc.M1_INC_CAT_06_SALES +\n                mc.M1_INC_CAT_07_FEES +\n                mc.M1_INC_CAT_07_SALES +\n                mc.M1_INC_CAT_08_FEES +\n                mc.M1_INC_CAT_08_SALES +\n                mc.M1_INC_CAT_09_FEES +\n                mc.M1_INC_CAT_09_SALES +\n                mc.M1_INC_CAT_10_FEES +\n                mc.M1_INC_CAT_10_SALES +\n                mc.M1_INC_CAT_11_FEES +\n                mc.M1_INC_CAT_11_SALES +\n                mc.M1_INC_CAT_12_FEES +\n                mc.M1_INC_CAT_12_SALES +\n                mc.M1_INCOME_ASSESSMENT +\n                mc.M1_INCOME_BULLETIN +\n                mc.M1_INCOME_INTRA_CHANGE),0)  \n        from    monthly_extract_visa    vs,\n                monthly_extract_mc      mc\n        where   vs.hh_load_sec(+) =  :1  and\n                mc.hh_load_sec(+) =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.DiscoverExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setLong(2,loadSec);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   visaIcInc = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mcIcInc = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:796^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:798^7*/

//  ************************************************************
//  #sql [Ctx] { update  discover_extract_summary
//          set     visa_ic_income  = :visaIcInc,
//                  mc_ic_income    = :mcIcInc
//          where   load_sec = :loadSec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  discover_extract_summary\n        set     visa_ic_income  =  :1 ,\n                mc_ic_income    =  :2 \n        where   load_sec =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"18com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,visaIcInc);
   __sJT_st.setDouble(2,mcIcInc);
   __sJT_st.setLong(3,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:804^7*/
    }
    catch( Exception e )
    {
      logEntry( "loadDiscoverDiscIcIncome(" + loadSec + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
  
  private void loadDiscoverEquipmentSummary( String loadFilename )
  {
    Date                        activeDate  = null;
    Date                        beginDate   = null;
    Date                        endDate     = null;
    ResultSetIterator           it          = null;
    ResultSet                   resultSet   = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:826^7*/

//  ************************************************************
//  #sql [Ctx] { select  sm.active_date, min(sm.month_begin_date), max(sm.month_end_date) 
//                    
//          from    monthly_extract_summary    sm
//          where   sm.load_filename = :loadFilename
//          group by sm.active_date       
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sm.active_date, min(sm.month_begin_date), max(sm.month_end_date) \n                   \n        from    monthly_extract_summary    sm\n        where   sm.load_filename =  :1 \n        group by sm.active_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.startup.DiscoverExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activeDate = (java.sql.Date)__sJT_rs.getDate(1);
   beginDate = (java.sql.Date)__sJT_rs.getDate(2);
   endDate = (java.sql.Date)__sJT_rs.getDate(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:833^7*/
      
/*      
      #sql [Ctx]
      {
        select  mf.merchant_number                              as merchant_number,
                mf.DMDSNUM                                      as discover_merchant_number,
                nvl(mr.discover_rep_id,u.THIRD_PARTY_USER_ID)   as rep_id,
                mf.DDSACCPT                                     as accepts_discover,
                mf.DVSACCPT                                     as accepts_visa
                mf.DMCACCPT                                     as accepts_mc,
                mf.DAMACCPT                                     as accepts_amex,
                mf.DDCACCPT                                     as accepts_diners, 
                mf.DJCACCPT                                     as accepts_jcb,
                decode( mf.debit_plan,null,'N','Y' )            as accepts_debit,
                
                decode( at.DATE_COMPLETED,
                        null,mmddyy_to_date(mf.date_opened),
                        trunc(at.date_completed))               as order_date,
                trunc(ei.EI_DEPLOYED_DATE)                      as shipped_date,
                        
                decode( ei.ei_lrb,     
                        :(mesConstants.APP_EQUIP_PURCHASE),'P',         -- 1,'P',
                        :(mesConstants.APP_EQUIP_BUY_REFURBISHED),'P',  -- 4,'P',
                        :(mesConstants.APP_EQUIP_RENT),'R',             -- 2,'R',
                        :(mesConstants.APP_EQUIP_LEASE),'L',            -- 5,'L',
                        '?' )                                   as discover_disposition,
                        
                decode( ei.ei_lrb,
                        1,'New',    -- purchase
                        2,'New',    -- rent
                        5,'New',    -- lease
                        4,'Ref',    -- buy refurbished
                        '?' )                                   as new_equipment,
                        
                nvl(lt.equiplendtype_description,
                    'Status Code: ' || ei.ei_lrb )              as disposition,
                ei.ei_part_number                               as part_number,
                nvl(eq.equip_descriptor,'Not Available')        as equip_desc,
                ei.ei_unit_cost                                 as base_cost,
                'INV'                                           as data_source,
                mr.app_seq_num                                  as app_seq_num
        from    
                
                group_merchant            gm,
                mif                       mf,
--                ips_file                  ips,
                equip_inventory           ei,
                equiplendtype             lt,
                equipment                 eq,
                merchant                  mr,
                application               app,
                app_tracking              at,
                users                     u
        where   po.parent_org_num = :orgId and
                o.org_num   = po.org_num and
                gm.org_num  = o.org_num and
                mf.merchant_number = gm.merchant_number and
                nvl(ei.EI_MERCHANT_NUMBER,0) = mf.merchant_number and
                nvl(ei.EI_DEPLOYED_DATE,'31-DEC-9999') between :beginDate and last_day(:endDate) and
                mr.merch_number(+)  = mf.merchant_number and
                app.app_seq_num(+)  = mr.app_seq_num and
                at.app_seq_num(+)   = mr.app_seq_num and
                at.dept_code(+)     = :(QueueConstants.DEPARTMENT_CREDIT) and -- 100 and
                at.status_code(+)   = :(QueueConstants.DEPT_STATUS_CREDIT_APPROVED) and -- 102 and
                u.user_id(+) = app.app_user_id and
                eq.EQUIP_MODEL(+) = ei.EI_PART_NUMBER and
                lt.equiplendtype_code(+) = ei.ei_lrb
        order by o.org_name, dba_name, merchant_number, part_number, discover_disposition
      };
*/      
    }
    catch( Exception e ) 
    {
      logEntry( "loadDiscoverEquipmentSummary( " + loadFilename + " )", e.toString() );
    }
    finally
    {
    }
  }
  
  private void loadDiscoverFeeIncome( long loadSec )
  {
    double                income            = 0.0;
    ResultSetIterator     it                = null;
    ResultSet             resultSet         = null;
    
    try
    {
      // load authorizations
      /*@lineinfo:generated-code*//*@lineinfo:923^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode( ap.a1_plan_type,
//                          'VS',:CT_VISA,
//                          'MC',:CT_MC,
//                          'AM',:CT_AMEX,
//                          'DC',:CT_DINERS,
//                          'JC',:CT_JCB,
//                          'DB',:CT_DEBIT,
//                          :CT_COUNT )       as ct_id,
//                  ap.auth_income_total      as auth_income
//          from    monthly_extract_ap    ap
//          where   ap.hh_load_sec = :loadSec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode( ap.a1_plan_type,\n                        'VS', :1 ,\n                        'MC', :2 ,\n                        'AM', :3 ,\n                        'DC', :4 ,\n                        'JC', :5 ,\n                        'DB', :6 ,\n                         :7  )       as ct_id,\n                ap.auth_income_total      as auth_income\n        from    monthly_extract_ap    ap\n        where   ap.hh_load_sec =  :8";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,CT_VISA);
   __sJT_st.setInt(2,CT_MC);
   __sJT_st.setInt(3,CT_AMEX);
   __sJT_st.setInt(4,CT_DINERS);
   __sJT_st.setInt(5,CT_JCB);
   __sJT_st.setInt(6,CT_DEBIT);
   __sJT_st.setInt(7,CT_COUNT);
   __sJT_st.setLong(8,loadSec);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.startup.DiscoverExtractSummary",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:936^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        switch( resultSet.getInt("ct_id") )
        {
          case CT_VISA:
            /*@lineinfo:generated-code*//*@lineinfo:944^13*/

//  ************************************************************
//  #sql [Ctx] { update discover_extract_summary
//                set visa_income = ( nvl(visa_income,0) + 
//                                    :resultSet.getDouble("auth_income") )
//                where load_sec = :loadSec                                  
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4203 = resultSet.getDouble("auth_income");
   String theSqlTS = "update discover_extract_summary\n              set visa_income = ( nvl(visa_income,0) + \n                                   :1  )\n              where load_sec =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_4203);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:950^13*/
            break;
            
          case CT_MC:
            /*@lineinfo:generated-code*//*@lineinfo:954^13*/

//  ************************************************************
//  #sql [Ctx] { update discover_extract_summary
//                set mc_income = ( nvl(mc_income,0) + 
//                                    :resultSet.getDouble("auth_income") )
//                where load_sec = :loadSec                                  
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4204 = resultSet.getDouble("auth_income");
   String theSqlTS = "update discover_extract_summary\n              set mc_income = ( nvl(mc_income,0) + \n                                   :1  )\n              where load_sec =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_4204);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:960^13*/
            break;
            
          case CT_AMEX:
            /*@lineinfo:generated-code*//*@lineinfo:964^13*/

//  ************************************************************
//  #sql [Ctx] { update discover_extract_summary
//                set amex_income = ( nvl(amex_income,0) + 
//                                    :resultSet.getDouble("auth_income") )
//                where load_sec = :loadSec                                  
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4205 = resultSet.getDouble("auth_income");
   String theSqlTS = "update discover_extract_summary\n              set amex_income = ( nvl(amex_income,0) + \n                                   :1  )\n              where load_sec =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_4205);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:970^13*/
            break;
            
          case CT_DINERS:
            /*@lineinfo:generated-code*//*@lineinfo:974^13*/

//  ************************************************************
//  #sql [Ctx] { update discover_extract_summary
//                set diners_income = ( nvl(diners_income,0) + 
//                                    :resultSet.getDouble("auth_income") )
//                where load_sec = :loadSec                                  
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4206 = resultSet.getDouble("auth_income");
   String theSqlTS = "update discover_extract_summary\n              set diners_income = ( nvl(diners_income,0) + \n                                   :1  )\n              where load_sec =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_4206);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:980^13*/
            break;
            
          case CT_JCB:
            /*@lineinfo:generated-code*//*@lineinfo:984^13*/

//  ************************************************************
//  #sql [Ctx] { update discover_extract_summary
//                set jcb_income = ( nvl(jcb_income,0) + 
//                                    :resultSet.getDouble("auth_income") )
//                where load_sec = :loadSec                                  
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4207 = resultSet.getDouble("auth_income");
   String theSqlTS = "update discover_extract_summary\n              set jcb_income = ( nvl(jcb_income,0) + \n                                   :1  )\n              where load_sec =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_4207);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:990^13*/
            break;
            
          case CT_DEBIT:
            /*@lineinfo:generated-code*//*@lineinfo:994^13*/

//  ************************************************************
//  #sql [Ctx] { update discover_extract_summary
//                set debit_income = ( nvl(debit_income,0) + 
//                                    :resultSet.getDouble("auth_income") )
//                where load_sec = :loadSec                                  
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4208 = resultSet.getDouble("auth_income");
   String theSqlTS = "update discover_extract_summary\n              set debit_income = ( nvl(debit_income,0) + \n                                   :1  )\n              where load_sec =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,__sJT_4208);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1000^13*/
            break;
        }
      }
      resultSet.close();
      it.close();
      
      // load debit networks
      /*@lineinfo:generated-code*//*@lineinfo:1008^7*/

//  ************************************************************
//  #sql [Ctx] { select  gn.t1_tot_inc_debit_networks   
//          from    monthly_extract_gn      gn
//          where   gn.hh_load_sec = :loadSec
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  gn.t1_tot_inc_debit_networks    \n        from    monthly_extract_gn      gn\n        where   gn.hh_load_sec =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.startup.DiscoverExtractSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   income = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1013^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:1015^7*/

//  ************************************************************
//  #sql [Ctx] { update discover_extract_summary
//          set debit_income = ( nvl(debit_income,0) + :income )
//          where load_sec = :loadSec 
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update discover_extract_summary\n        set debit_income = ( nvl(debit_income,0) +  :1  )\n        where load_sec =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.startup.DiscoverExtractSummary",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,income);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1020^7*/
    }
    catch( Exception e )
    {
      logEntry( "loadDiscoverFeeIncome(" + loadSec + ")", e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
    }
  }
}/*@lineinfo:generated-code*/