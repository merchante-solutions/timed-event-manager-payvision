/*@lineinfo:filename=DiscoverSettlement*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/DiscoverSettlement.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.settlement.SettlementRecord;
import sqlj.runtime.ResultSetIterator;

public class DiscoverSettlement extends DiscoverFileBase
{
  public DiscoverSettlement( )
  {
    PropertiesFilename          = "discover-settlement.properties";

    CardTypeTable               = "discover_settlement";
    CardTypeTableActivity       = "discover_settlement_activity";
    CardTypePostTotals          = "ds_settle";
    CardTypeFileDesc            = "Discover Settlement";
    DkOutgoingHost              = MesDefaults.DK_DISCOVER_OUTGOING_HOST;
    DkOutgoingUser              = MesDefaults.DK_DISCOVER_OUTGOING_USER;
    DkOutgoingPassword          = MesDefaults.DK_DISCOVER_OUTGOING_PASSWORD;
    DkOutgoingPath              = MesDefaults.DK_DISCOVER_MAP_OUTGOING_PATH;
    DkOutgoingUseBinary         = false;
    DkOutgoingSendFlagFile      = true;
    SettlementAddrsNotify       = MesEmails.MSG_ADDRS_OUTGOING_DISCOVER_NOTIFY;
    SettlementAddrsFailure      = MesEmails.MSG_ADDRS_OUTGOING_DISCOVER_FAILURE;
    SettlementRecMT             = SettlementRecord.MT_FIRST_PRESENTMENT;
  }
  
  protected boolean processTransactions( )
  {
    int                       bankNumber        = Integer.parseInt( getEventArg(0) );
    String                    bankNumberClause  = (bankNumber == 9999) ? "" : " and ds.bank_number = " + bankNumber + " ";
    String                    cardTypeClause    = " and ds.acquirer_id is not null ";
    Date                      cpd               = null;
    ResultSetIterator         it                = null;
    int                       recCount          = 0;
    long                      workFileId        = 0L;
    String                    workFilename      = null;

    try
    {
     for( int idx = 0; idx <= 1; ++idx )   // 0 is MAP (or rebuild of MAP/iMAP); 1 is Interim MAP
     {
      if ( TestFilename == null )
      {
        // second time through, change to use Interim MAP values and reset all counters
        if ( idx != 0 )
        {
          FileProcessorId       = 0;
          FileVersionInd        = MesDefaults.getString(MesDefaults.DISC_SETTLEMENT_FILE_VERSION_IND);
          DkOutgoingPath        = MesDefaults.DK_DISCOVER_OUTGOING_PATH;
          cardTypeClause        = " and ds.acquirer_id is null ";

          recCount              = 0;
          BatchCreditsAmount    = 0.0;
          BatchCreditsCount     = 0;
          BatchDebitsAmount     = 0.0;
          BatchDebitsCount      = 0;
          BatchRecordCount      = 0;
          BatchTranCount        = 0;
          FileBatchCount        = 0;
          FileCreditsAmount     = 0.0;
          FileCreditsCount      = 0;
          FileDebitsAmount      = 0.0;
          FileDebitsCount       = 0;
          FileRecordCount       = 0;
          FileTranCount         = 0;
        }
    
        FileTimestamp   = new Timestamp(Calendar.getInstance().getTime().getTime());

        cpd             = getSettlementDate();

        workFilename    = generateFilename(( "R".equals(ActionCode) ? "ds_reversals" : "ds_settle" )    // base name
                                          + String.valueOf(bankNumber),                                 // bank number
                                          (idx == 0) ? ".map" : ".imap");                               // file extension
        workFileId      = loadFilenameToLoadFileId(workFilename);
        
        if( ActionCode == null )
        {
          /*@lineinfo:generated-code*//*@lineinfo:108^11*/

//  ************************************************************
//  #sql [Ctx] { update  discover_settlement   ds
//              set     ds.output_filename  = :workFilename,
//                      ds.output_file_id   = :workFileId,
//                      ds.settlement_date  = :cpd
//              where   ds.output_file_id = 0
//                      and ds.output_filename is null
//                      and ds.test_flag = 'N'
//                      :bankNumberClause :cardTypeClause
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update  discover_settlement   ds\n            set     ds.output_filename  =  ? ,\n                    ds.output_file_id   =  ? ,\n                    ds.settlement_date  =  ? \n            where   ds.output_file_id = 0\n                    and ds.output_filename is null\n                    and ds.test_flag = 'N'\n                     ");
   __sjT_sb.append(bankNumberClause);
   __sjT_sb.append("   ");
   __sjT_sb.append(cardTypeClause);
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "0com.mes.startup.DiscoverSettlement:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setLong(2,workFileId);
   __sJT_st.setDate(3,cpd);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:118^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:122^11*/

//  ************************************************************
//  #sql [Ctx] { update  discover_settlement_activity   dsa
//              set     dsa.load_filename  = :workFilename,
//                      dsa.load_file_id   = :workFileId,
//                      dsa.settlement_date  = :cpd
//              where   dsa.load_file_id = 0
//                      and dsa.load_filename is null
//                      and dsa.action_code = :ActionCode
//                      and exists
//                      (
//                        select  ds.rec_id
//                        from    discover_settlement ds
//                        where   ds.rec_id = dsa.rec_id
//                                :bankNumberClause :cardTypeClause
//                      )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update  discover_settlement_activity   dsa\n            set     dsa.load_filename  =  ? ,\n                    dsa.load_file_id   =  ? ,\n                    dsa.settlement_date  =  ? \n            where   dsa.load_file_id = 0\n                    and dsa.load_filename is null\n                    and dsa.action_code =  ? \n                    and exists\n                    (\n                      select  ds.rec_id\n                      from    discover_settlement ds\n                      where   ds.rec_id = dsa.rec_id\n                               ");
   __sjT_sb.append(bankNumberClause);
   __sjT_sb.append("   ");
   __sjT_sb.append(cardTypeClause);
   __sjT_sb.append(" \n                    )");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "1com.mes.startup.DiscoverSettlement:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setLong(2,workFileId);
   __sJT_st.setDate(3,cpd);
   __sJT_st.setString(4,ActionCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:138^11*/
        }
        // only process if there were records updated 
        recCount = Ctx.getExecutionContext().getUpdateCount();
      }
      else    // re-build a previous file
      {
        idx = 1;    // do not go through loop again.

        // if imap (filename ends in .imap OR filename ends in ".dat" and is bank #9999), then change variables
        if( (TestFilename.indexOf(".imap") >= 0) || (TestFilename.indexOf(".dat") >= 0 && TestFilename.indexOf("9999") >= 0 ) )
        {
                //  before Aug 2011, 3941.dat was imap; those will not be caught by the if and will be built as map
          FileProcessorId = 0;
          FileVersionInd  = MesDefaults.getString(MesDefaults.DISC_SETTLEMENT_FILE_VERSION_IND);
          DkOutgoingPath  = MesDefaults.DK_DISCOVER_OUTGOING_PATH;
        }

        FileTimestamp   = new Timestamp( getFileDate(TestFilename).getTime() );
        workFilename    = TestFilename;
        workFileId      = loadFilenameToLoadFileId(workFilename);
        recCount        = 1;   // always re-build
      }

      if ( recCount > 0 )
      {
        // select the transactions to process
        if( ActionCode == null )
        {
          /*@lineinfo:generated-code*//*@lineinfo:167^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  ds.*
//              from    discover_settlement           ds
//              where   ds.output_file_id = :workFileId
//                      and ds.test_flag = 'N'
//              order by ds.batch_id, ds.batch_record_id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ds.*\n            from    discover_settlement           ds\n            where   ds.output_file_id =  :1 \n                    and ds.test_flag = 'N'\n            order by ds.batch_id, ds.batch_record_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.DiscoverSettlement",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,workFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.DiscoverSettlement",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:174^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:178^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  ds.*
//              from    discover_settlement_activity  dsa,
//                      discover_settlement           ds
//              where   dsa.load_file_id = :workFileId
//                      and dsa.action_code = :ActionCode
//                      and ds.rec_id = dsa.rec_id
//              order by ds.batch_id, ds.batch_record_id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   //@formatter:off
   String theSqlTS = " SELECT ds.* ,dsa.reproc external_reject " + 
   		             " FROM discover_settlement_activity dsa,  " + 
   		             "      discover_settlement ds " + 
   		             " WHERE dsa.load_file_id = :1  " + 
   		             "   AND dsa.action_code    = :2  " + 
   		             "   AND ds.rec_id          = dsa.rec_id" + 
   		             " ORDER BY ds.batch_id, ds.batch_record_id";
  //@formatter:on
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.DiscoverSettlement",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,workFileId);
   __sJT_st.setString(2,ActionCode);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.DiscoverSettlement",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:187^11*/
        }

        FileReferenceId = String.valueOf(workFileId);
        handleSelectedRecords( it, workFilename,workFileId );
      }
     }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "processTransactions()", e.toString());
      logEntry("processTransactions()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return( true );
  }

  public static void main( String[] args )
  {
    DiscoverSettlement                test          = null;
    
    try
    { 
        if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{
                    MesDefaults.DK_DISCOVER_OUTGOING_HOST,
                    MesDefaults.DK_DISCOVER_OUTGOING_USER,
                    MesDefaults.DK_DISCOVER_OUTGOING_PASSWORD,
                    MesDefaults.DK_DISCOVER_MAP_OUTGOING_PATH,
                    MesDefaults.DK_DISCOVER_OUTGOING_PATH,
                    MesDefaults.DISC_SETTLEMENT_FILE_VERSION_IND,
                    MesDefaults.DISC_SETTLEMENT_FILE_VERSION_IND,
                    MesDefaults.DK_DISCOVER_OUTGOING_PATH
            });
        }


      test = new DiscoverSettlement();
      test.executeCommandLine( args );
    }
    catch( Exception e )
    {
      log.debug(e.toString());
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}/*@lineinfo:generated-code*/