package com.mes.startup;

import org.apache.log4j.Logger;
import com.mes.ops.cbt.TierScoreQueueOps;

public final class CbtAutoCreditMonitorEvent extends EventBase
{
  static Logger log = Logger.getLogger(CbtAutoCreditMonitorEvent.class);

  /**
   * Calls the monitor method in com.mes.ops.cbtTierQueueOps.  This will 
   * monitor cb&t apps in the process of getting their credit scores pulled,
   * and completed or failed scores are routed accordingly within the review
   * queue system there.
   */
  public boolean execute()
  {
    try
    {
      TierScoreQueueOps.doAutoScoreMonitoring();
      return true;
    }
    catch (Exception e)
    {
      log.error("Error during cbt auto credit scoring monitor event: " + e);
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    
    return false;
  }
}
