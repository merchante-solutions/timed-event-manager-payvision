/*@lineinfo:filename=StatementFileServer*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/StatementFileServer.sqlj $

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-07-29 13:51:34 -0700 (Wed, 29 Jul 2015) $
  Version            : $Revision: 23755 $

  Change History:
     See SVN database

  Copyright (C) 2008-2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
//Java
import java.util.Vector;
//FOP
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.render.pdf.PDFRenderer;
// log4j
import org.apache.log4j.Logger;
//MES
import com.mes.database.SQLJConnectionBase;
import com.mes.reports.OrgStatementDetail;
import com.mes.support.DateTimeFormatter;


public class StatementFileServer extends EventBase
{
  static Logger log = Logger.getLogger(StatementFileServer.class);
  
  // process types
  public static final int   PT_BUILD_STATEMENTS       = 0;
  
  public static final String[][]  BankContactData =
  {
    //   Bank Name                 Address                 City/State/Zip                        Help Info                         Help Name   
    // ----------------------------------------------------------------------------------------------------------------------------------------------------
    {  "MERCHANT E-SOLUTIONS"   , "P.O. BOX 13305"      , "SPOKANE, WA 99213-3305"    , "24 HR HELP DESK 888/288-2692"   , "MERCHANT E-SOLUTIONS" },
    {  "STERLING SAVINGS BANK"  , ""                    , ""                          , "24 HR HELP DESK 888/288-2692"   , "MERCHANT E-SOLUTIONS" },
  };

  protected     Date              TestActiveDate    = null;
  protected     long              TestNodeId        = 0L;
  
  protected static final int[] bankNums = new int[] { 3943, 3942, 3003, 3941, 3858 };

  public StatementFileServer()
  {
    PropertiesFilename  = "merch-statements.properties";
  }
  
  protected void buildStatements( String loadFilename )
  {
    Date        activeDate        = null;
    int         bankNumber        = 0;
    String      monthEndFilename  = loadFilename;
    long        nodeId            = 0L;
    
    try
    {
	  if ( inTestMode() )
      {
        activeDate        = TestActiveDate;
        nodeId            = TestNodeId; // set default node, usually a mid
        bankNumber        = ((monthEndFilename == null) ? 0 : getFileBankNumber(monthEndFilename));
      }
      else    // load statements for all accounts in the m/e file
      {
        /*@lineinfo:generated-code*//*@lineinfo:106^9*/

//  ************************************************************
//  #sql [Ctx] { select  distinct 
//                    gn.hh_bank_number                        as bank_number,
//                    to_number(gn.hh_bank_number || '00000')  as node_id,
//                    gn.hh_active_date                        as active_date
//            
//            from    monthly_extract_gn    gn
//            where   gn.load_file_id = load_filename_to_load_file_id(:loadFilename)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct \n                  gn.hh_bank_number                        as bank_number,\n                  to_number(gn.hh_bank_number || '00000')  as node_id,\n                  gn.hh_active_date                        as active_date\n           \n          from    monthly_extract_gn    gn\n          where   gn.load_file_id = load_filename_to_load_file_id( :1  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.StatementFileServer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   bankNumber = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   nodeId = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   activeDate = (java.sql.Date)__sJT_rs.getDate(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:115^9*/
      }
      
      System.out.println("Loading statements...");
      System.out.println("  Statement Date: " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
      System.out.println("  ME Filename   : " + monthEndFilename );
      System.out.println("  Node ID       : " + nodeId );
      System.out.println("  Bank Number   : " + bankNumber );
      
      // configure fopFactory as desired
      FopFactory fopFactory = FopFactory.newInstance();

      // configure foUserAgent as desired
      FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
            
      // 02/26/2014: The following four lines of code were added to prempt the renderer class load. 
      // Without this the following exception was thrown when running the StatementFileServer with the Timed Event Manager.
      // java.io.IOException: Don't know how to handle "application/pdf" as an output format. Neither an FOEventHandler, nor a Renderer could be found for this o utput format.
      PDFRenderer pdfrenderer = new PDFRenderer();
      pdfrenderer.setUserAgent(foUserAgent);
      foUserAgent.setRendererOverride(pdfrenderer);
      fopFactory.addElementMapping(new org.apache.fop.fo.FOElementMapping());

      // allocate OrgStatementDetail
      OrgStatementDetail orgDetail = new OrgStatementDetail(nodeId,activeDate, fopFactory, foUserAgent);
      orgDetail.setBankNum(bankNumber);
      //generate the list of MerchantStatementDetails
      // orgDetail.setMonthEndFilename(monthEndFilename);
      // orgDetail.generateMerchantList();
      
      // set the bank contact information for the statement header
      if ( bankNumber == 3942 )
      {
        orgDetail.name      = BankContactData[1][0];
        orgDetail.orgAddr   = BankContactData[1][1];
        orgDetail.orgCsz    = BankContactData[1][2];
        orgDetail.helpInfo  = BankContactData[1][3];
        orgDetail.helpName  = BankContactData[1][4];
      }
      else  // default to 3941 data
      {
        orgDetail.name      = BankContactData[0][0];
        orgDetail.orgAddr   = BankContactData[0][1];
        orgDetail.orgCsz    = BankContactData[0][2];
        orgDetail.helpInfo  = BankContactData[0][3];
        orgDetail.helpName  = BankContactData[0][4];
      }
      
      // generate a statement load filename for use in the oracle table
      String statementFilename = generateFilename( "mstmt" + bankNumber );

      //generate the list of MerchantStatementDetails
      orgDetail.setMonthEndFilename(monthEndFilename);

      // Generate Merchant Object and convertToFile
      orgDetail.generateMerchantList();
      orgDetail.getMerchantObjectAndConvertToFile(statementFilename);

      // orgDetail.generateMerchantList();
      
      //convert each OrgStatementDetail into its file group
      //for now, we'll be saving hard copy
      // orgDetail.convertToFiles(fopFactory, foUserAgent, statementFilename);
    }
    catch( Exception e )
    {
      logEntry("buildStatements(" + loadFilename + ")",e.toString());
    }
  }

protected void buildStatements(int bankNo,String printArg, String loadFileName)
  {
    Date        activeDate        = null;
    int         bankNumber        = 0;
    String      monthEndFilename  = null;
    long        nodeId            = 0L;
    
    try
    {
    if ("print".equals(printArg)) 
      {
      	    	System.out.println("Inside StatementFileServer::buildStatements(bankNo,printArg)");
	    		System.out.println("Paperstatement: generate XML in 3rd Party Print Mode");
	    		activeDate = this.TestActiveDate;
	    		nodeId = 0L;
	    		bankNumber = bankNo;
	    		monthEndFilename = loadFileName;
	  } 
	  else if ( inTestMode() )
      {
        activeDate        = TestActiveDate;
        nodeId            = TestNodeId; // set default node, usually a mid
        bankNumber        = getFileBankNumber(monthEndFilename);
      }
      /*else    // load statements for all accounts in the m/e file
      {
        #sql [Ctx]
        {
          select  distinct 
                  gn.hh_bank_number                        as bank_number,
                  to_number(gn.hh_bank_number || '00000')  as node_id,
                  gn.hh_active_date                        as active_date
          into    :bankNumber, :nodeId, :activeDate
          from    monthly_extract_gn    gn
          where   gn.load_file_id = load_filename_to_load_file_id(:loadFilename)
        };
      }*/
      
      System.out.println("Loading statements...");
      System.out.println("  Statement Date: " + DateTimeFormatter.getFormattedDate(activeDate,"MMM yyyy") );
      System.out.println("  ME Filename   : " + monthEndFilename );
      System.out.println("  Node ID       : " + nodeId );
      System.out.println("  Bank Number   : " + bankNumber );
      
      // configure fopFactory as desired
      FopFactory fopFactory = FopFactory.newInstance();

      // configure foUserAgent as desired
      FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
            
      // 02/26/2014: The following four lines of code were added to prempt the renderer class load. 
      // Without this the following exception was thrown when running the StatementFileServer with the Timed Event Manager.
      // java.io.IOException: Don't know how to handle "application/pdf" as an output format. Neither an FOEventHandler, nor a Renderer could be found for this o utput format.
      PDFRenderer pdfrenderer = new PDFRenderer();
      pdfrenderer.setUserAgent(foUserAgent);
      foUserAgent.setRendererOverride(pdfrenderer);
      fopFactory.addElementMapping(new org.apache.fop.fo.FOElementMapping());

      // allocate OrgStatementDetail
      OrgStatementDetail orgDetail = new OrgStatementDetail(nodeId,activeDate, fopFactory, foUserAgent);
      orgDetail.setBankNum(bankNumber);
      if ("print".equals(printArg)) {
	    		orgDetail.setGeneratePaperStatement(true);
	  }

      //generate the list of MerchantStatementDetails
      // orgDetail.setMonthEndFilename(monthEndFilename);
      // orgDetail.generateMerchantList();
      
      // set the bank contact information for the statement header
      if ( bankNumber == 3942 )
      {
        orgDetail.name      = BankContactData[1][0];
        orgDetail.orgAddr   = BankContactData[1][1];
        orgDetail.orgCsz    = BankContactData[1][2];
        orgDetail.helpInfo  = BankContactData[1][3];
        orgDetail.helpName  = BankContactData[1][4];
      }
      else  // default to 3941 data
      {
        orgDetail.name      = BankContactData[0][0];
        orgDetail.orgAddr   = BankContactData[0][1];
        orgDetail.orgCsz    = BankContactData[0][2];
        orgDetail.helpInfo  = BankContactData[0][3];
        orgDetail.helpName  = BankContactData[0][4];
      }
      
      // generate a statement load filename for use in the oracle table
      String statementFilename = generateFilename( "mstmt" + bankNumber );

      //generate the list of MerchantStatementDetails
      orgDetail.setMonthEndFilename(monthEndFilename);

      // Generate Merchant Object and convertToFile
      if ("print".equals(printArg)) {
				orgDetail.setPaperBankNum(bankNumber);
	    		orgDetail.generatePaperMerchantList();
	    	    orgDetail.getMerchantObjectListAndConvertToFile(statementFilename);
	  } else {
		    	orgDetail.generateMerchantList();
		        orgDetail.getMerchantObjectAndConvertToFile(statementFilename);
	  }

      // orgDetail.generateMerchantList();
      // orgDetail.getMerchantObjectAndConvertToFile(statementFilename);
      
      //convert each OrgStatementDetail into its file group
      //for now, we'll be saving hard copy
      // orgDetail.convertToFiles(fopFactory, foUserAgent, statementFilename);
    }
    catch( Exception e )
    {
      logEntry("buildStatements()",e.toString());
    }
  }

  /**
   * Main method.
   */
  public boolean execute()
  {
    ProcessTable.ProcessTableEntry  entry         = null;
    ProcessTable                    pt            = null;
    boolean                         retVal        = false;
  
    try
    {
      connect(true);
      if(getEventArgCount() > 2) {
        setTestActiveDate(new Date(DateTimeFormatter.parseDate(getEventArg(2), "MM/dd/yyyy").getTime()));
        buildStatements(Integer.parseInt(getEventArg(0)),"print", getEventArg(1));
      }
      else {
        pt = new ProcessTable("statement_process","statement_process_sequence",ProcessTable.PT_ALL);
        if ( pt.hasPendingEntries() )
        {
          pt.preparePendingEntries();
          Vector entries = pt.getEntryVector();
          for( int i = 0; i < entries.size(); ++i )
            {
              entry = (ProcessTable.ProcessTableEntry)entries.elementAt(i);
              switch( entry.getProcessType() )
                {
                  case PT_BUILD_STATEMENTS:       // build statements
                  entry.recordTimestampBegin();
                  buildStatements( entry.getLoadFilename() );
                  entry.recordTimestampEnd();
                  break;
                }
            }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("execute(): ", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return( true );
  }
  
  public boolean inTestMode()
  {
    return( TestNodeId != 0L );
  }
  
  public void setTestActiveDate( Date activeDate )
  {
    TestActiveDate = activeDate;
  }

  public void setTestNodeId( long nodeId )
  {
    TestNodeId = nodeId;
  }

  /**
   * Main method.
   * @param args command-line arguments
   */
  public static void main(String[] args)
  {
    StatementFileServer     app     = null;
    
    try
    {
      SQLJConnectionBase.initStandalone("ERROR");
      
      log.debug("FOP Example\n");
      log.debug("Preparing...");

      app = new StatementFileServer();
      app.connect(true);
      
      if( args.length > 0 )
      {
        if("buildStatements".equals(args[0])) {
          log.debug("Loading statement for " + args[1] + " " + args[2] + "...");
          app.setTestActiveDate(new Date(DateTimeFormatter.parseDate(args[2], "MM/dd/yyyy").getTime()));
          app.setTestNodeId( Long.parseLong(args[1]) );
          app.buildStatements(args[3]);
    	} 
    	else if ("buildPaperStatements".equals(args[0])) { // paperStatement input parameter example : buildPaperStatements 3941 mbs_ext3003_071417_001.dat 06/01/2017
    	  log.debug("Loading statement for " + args[1] + " " + args[2] + "...");
    	  app.setTestActiveDate(new Date(DateTimeFormatter.parseDate(args[3], "MM/dd/yyyy").getTime()));
    	  app.buildStatements(Integer.parseInt(args[1]), "print", args[2]);
    	}
      } 
      else    // default is to execute the TE 
      {     
        if(app.execute())
        {
          log.debug("Success!");
        }
        else
        {
          log.debug("Failure!");
        }
      }
    }
    catch( Exception e )
    {
      try{ app.cleanUp(); } catch( Exception ee ) {}
      e.printStackTrace(System.err);
    }
    finally
    {
      Runtime.getRuntime().exit(0);
    }
  }
}/*@lineinfo:generated-code*/