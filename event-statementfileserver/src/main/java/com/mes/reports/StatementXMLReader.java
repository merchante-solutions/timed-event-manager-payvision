/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/trunk/src/main/com/mes/reports/StatementXMLReader.java $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2014-02-26 15:40:38 -0800 (Wed, 26 Feb 2014) $
  Version            : $Revision: 22180 $

  Change History:
     See SVN database

  Copyright (C) 2008-2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
//Java
import java.util.Iterator;
import java.util.TreeSet;
//SAX
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * XMLReader implementation, used to
 * generate SAX events from the MerchantStatementDetail class.
 */
public class StatementXMLReader extends AbstractObjectReader {


  private DecimalFormat pFormat = new DecimalFormat("#,###.00##");
  private DecimalFormat dFormat = new DecimalFormat("#,###.00");
  private NumberFormat nFormat  = NumberFormat.getInstance();

  /**
   * @see org.xml.sax.XMLReader#parse(InputSource)
   */
  public void parse(InputSource input)
  throws IOException, SAXException
  {
      if (input instanceof OrgStatementInputSource)
      {
          parse( ((OrgStatementInputSource)input).getOrgStatementDetail() );
      }
      else if (input instanceof MerchStatementInputSource)
      {
        parse( ((MerchStatementInputSource)input).getMerchantStatementDetail() );
      }
      else
      {
          throw new SAXException("Unsupported InputSource specified. "
                  + "Must be a StatementInputSource");
      }
  }


  /**
   * Starts parsing the OrgStatementDetail object.
   * @param OrgStatementDetail The object to parse
   * @throws SAXException In case of a problem during SAX event generation
   */
  //public void parse(MerchantStatementDetail statementDetail) throws SAXException {
  public void parse(OrgStatementDetail orgDetail)
  throws SAXException
  {
      if (orgDetail == null)
      {
          throw new NullPointerException("Parameter orgDetail must not be null");
      }
      if (handler == null)
      {
          throw new IllegalStateException("ContentHandler not set");
      }

      //Start the document
      handler.startDocument();

      //generage root element
      generateFor(orgDetail);

      //End the document
      handler.endDocument();
  }

  /**
   * Starts parsing the MerchantStatementDetail object.
   * @param MerchantStatementDetail The object to parse
   * @throws SAXException In case of a problem during SAX event generation
   */
  public void parse(MerchantStatementDetail statementDetail )
  throws SAXException
  {
      if (statementDetail == null)
      {
          throw new NullPointerException("Parameter statementDetail must not be null");
      }
      if (handler == null)
      {
          throw new IllegalStateException("ContentHandler not set");
      }

      //Start the document
      handler.startDocument();

      handler.startElement("orgdetail");

      //generage root element
      generateFor(statementDetail);

      handler.endElement("orgdetail");

      //End the document
      handler.endDocument();
  }


  /**
   * Generates SAX events for a OrgStatementDetail object.
   * @param projectMember ProjectMember object to use
   * @throws SAXException In case of a problem during SAX event generation
   */
  protected void generateFor(OrgStatementDetail orgDetail)
  throws SAXException
  {
    if (orgDetail == null)
    {
        throw new NullPointerException("Parameter orgDetail must not be null");
    }
    if (handler == null)
    {
        throw new IllegalStateException("ContentHandler not set");
    }

    handler.startElement("orgdetail");

    MerchantStatementDetail detail;

    int count = 0;
    int ecount = 0;

    long[] merchs = orgDetail.getMerchantDetails();
    for(int idx=0; idx < merchs.length; idx++)
    {
      detail = orgDetail.getMerchantDetail(merchs[idx]);

      //Generate SAX events for each statementDetail
      try
      {
        count++;
        System.out.println("count = "+ count);

        generateFor(detail);
      }
      catch(Exception e)
      {
        ecount++;

        if(ecount==1)
        {
          e.printStackTrace();
        }
      }

    }

/*
    handler.element("getId", ""+orgDetail.getId());
    handler.element("getXSLTFilename", orgDetail.getXSLTFilename());
    handler.element("getUsername", orgDetail.getUsername());
    handler.element("getPassword", orgDetail.getPassword());
    handler.element("getEmailAddress", orgDetail.getEmailAddress());
    handler.element("getIP", orgDetail.getIP());
    handler.element("getDocType", orgDetail.getDocType());
*/
    handler.endElement("orgdetail");
  }

  /**
   * Generates SAX events for a MerchantStatementDetail object.
   * @param MerchantStatementDetail object to use
   * @throws SAXException In case of a problem during SAX event generation
   */
  protected void generateFor(MerchantStatementDetail statementDetail )
  throws SAXException
  {
    if (statementDetail == null)
    {
        throw new NullPointerException("Parameter statementDetail must not be null");
    }

    if (handler == null)
    {
        throw new IllegalStateException("ContentHandler not set");
    }

    //start top element
    handler.startElement("statementdetail");

    //Generate SAX events for the orgDetail
    OrgStatementDetail orgDetail = statementDetail.getOrgDetail();
    //generateFor(orgDetail);

    handler.element("orgName",     statementDetail.merchDetails.returnAddrName);
    handler.element("returnAddrLine1",  statementDetail.merchDetails.returnAddrLines[0]);
    handler.element("returnAddrLine2",  statementDetail.merchDetails.returnAddrLines[1]);
    handler.element("returnAddrLine3",  statementDetail.merchDetails.returnAddrLines[2]);
    handler.element("returnAddrLine4",  statementDetail.merchDetails.returnAddrLines[3]);
    handler.element("orgHelpInfo", statementDetail.merchDetails.returnAddrPhone);
    handler.element("orgHelpName", statementDetail.merchDetails.returnAddrHelpName);

    //merchant details
    handler.element("processMonth", statementDetail.merchDetails.processMonth);
    handler.element("merchantNumber", statementDetail.merchDetails.merchantNumber);
    handler.element("association", statementDetail.merchDetails.association);
    handler.element("merchName", statementDetail.merchDetails.addyName);
    handler.element("address1", statementDetail.merchDetails.addyLine1);
    handler.element("address2", statementDetail.merchDetails.addyLine2);
    handler.element("addressCsz", statementDetail.merchDetails.addyCity+", "+statementDetail.merchDetails.addyState+" "+statementDetail.merchDetails.addyZip);
    
    handler.element("hasAddress1", String.valueOf(statementDetail.merchDetails.hasAddyLine1()));
    handler.element("hasAddress2", String.valueOf(statementDetail.merchDetails.hasAddyLine2()));
    
    //TEMP for sample doc
    //handler.element("merchName","ABC Co - Test Merchant Stmt Sterling 39");
    //handler.element("address1","1XX First Street");
    //handler.element("address2","Spokane, WA XXXXX");

    //sum amount discount due (from plan summary)
    //sum amount fees due     (from fees)
    //sum amount deducted     (from sum of above 2)
    handler.element("labelDiscountDue",     statementDetail.labelDiscountDue.toUpperCase());
    handler.element("totalDiscountDue",     dFormat.format(statementDetail.totalDiscountDue));
    handler.element("totalAmountDeducted",  dFormat.format(statementDetail.totalAmountDeducted));

    //determine remittal
    handler.element("isRemittal", statementDetail.merchDetails.remitStatus);


    //all LISTS
    Iterator i;

    //PLAN SUMMARY
    TreeSet sortedSet = new TreeSet(statementDetail.plans.values());
    i = sortedSet.iterator();
    while (i.hasNext())
    {
      MerchantStatementDetail.InfoLoadable obj = (MerchantStatementDetail.InfoPlan)i.next();
      generateFor(obj);
    }

    handler.element("pSalesNumberTotal",  nFormat.format(statementDetail.pSalesNumberTotal));
    handler.element("pSalesDollarTotal",  dFormat.format(statementDetail.pSalesDollarTotal));
    handler.element("pCreditNumberTotal", nFormat.format(statementDetail.pCreditNumberTotal));
    handler.element("pCreditDollarTotal", dFormat.format(statementDetail.pCreditDollarTotal));
    handler.element("pNetSalesTotal",     dFormat.format(statementDetail.pNetSalesTotal));
    handler.element("pAveTicketTotal",    dFormat.format(statementDetail.pAveTicketTotal));
    handler.element("pDiscountTotal",     dFormat.format(statementDetail.pDiscountTotal));

    //FEES
    i = statementDetail.fees.iterator();
    while (i.hasNext())
    {
        MerchantStatementDetail.InfoLoadable obj = (MerchantStatementDetail.InfoFee)i.next();
        generateFor(obj);
    }
    handler.element("fFeeTotal",      dFormat.format(statementDetail.fFeeTotal      ));


    //DEPOSITS
    i = statementDetail.deposits.iterator();
    while (i.hasNext())
    {
        MerchantStatementDetail.InfoLoadable obj = (MerchantStatementDetail.InfoTransaction)i.next();
        generateFor(obj);
    }
    handler.element("dSalesNumberTotal",  nFormat.format(statementDetail.dSalesNumberTotal  ));
    handler.element("dSalesDollarTotal",  dFormat.format(statementDetail.dSalesDollarTotal  ));
    handler.element("dCreditNumberTotal", nFormat.format(statementDetail.dCreditNumberTotal  ));
    handler.element("dCreditDollarTotal", dFormat.format(statementDetail.dCreditDollarTotal ));
    handler.element("dDiscountTotal",     dFormat.format(statementDetail.dDiscountPaid      ));
    handler.element("dSettledTotal",      dFormat.format(statementDetail.dSettledTotal      ));


    //ADJUSTMENTS
    if(statementDetail.adjustments.size()>0)
    {
      i = statementDetail.adjustments.iterator();
      while (i.hasNext())
      {
          MerchantStatementDetail.InfoLoadable obj = (MerchantStatementDetail.InfoTransaction)i.next();
          generateFor(obj);
      }
      handler.element("aSalesNumberTotal",  nFormat.format(statementDetail.aSalesNumberTotal  ));
      handler.element("aSalesDollarTotal",  dFormat.format(statementDetail.aSalesDollarTotal  ));
      handler.element("aCreditNumberTotal", nFormat.format(statementDetail.aCreditNumberTotal ));
      handler.element("aCreditDollarTotal", dFormat.format(statementDetail.aCreditDollarTotal ));
      handler.element("aDiscountTotal",     dFormat.format(statementDetail.aDiscountTotal     ));
      handler.element("aSettledTotal",      dFormat.format(statementDetail.aSettledTotal      ));
      handler.element("hasAdjustments","true");
    }
    else
    {
      handler.element("hasAdjustments","false");
    }

    //CHARGEBACKS
    if(statementDetail.chargebacks.size()>0)
    {
      i = statementDetail.chargebacks.iterator();

      while (i.hasNext())
      {
          MerchantStatementDetail.InfoLoadable obj = (MerchantStatementDetail.InfoTransaction)i.next();
          generateFor(obj);
      }
      handler.element("cSalesNumberTotal",  nFormat.format(statementDetail.cSalesNumberTotal  ));
      handler.element("cSalesDollarTotal",  dFormat.format(statementDetail.cSalesDollarTotal  ));
      handler.element("cCreditNumberTotal", nFormat.format(statementDetail.cCreditNumberTotal ));
      handler.element("cCreditDollarTotal", dFormat.format(statementDetail.cCreditDollarTotal ));
      handler.element("cDiscountTotal",     dFormat.format(statementDetail.cDiscountTotal     ));
      handler.element("cSettledTotal",      dFormat.format(statementDetail.cSettledTotal      ));
      handler.element("hasChargebacks","true");
    }
    else
    {
      handler.element("hasChargebacks","false");
    }

    //add statement message
    handler.element("stmtMsg", statementDetail.stmtMsg );


    /*
    //Generate SAX events for the statementDetail
    BASICALLY we need these segments:

    1.  Image (hmmmm.... web or internal file?)
    2a. org detail summary (left) containing same info on all merchants
    2b. org-merchant details (right)
    3.  merchant information (left) CLASS: InfoMerchant and amount deducted (right)
    4.  plan summary (list) CLASS: InfoPlan
    5.  deposits (list)     CLASS: InfoTransaction
    6.  adjustments (list)  CLASS: InfoTransaction
    7.  chargebacks (list)  CLASS: InfoTransaction
    8.  fees (list)         CLASS: InfoFee
    9.  discount and totals CLASS: ?
    10. summary footer (STATIC)

    LISTS - contain InfoTransaction Objects
    plans
    deposits
    adjustments
    chargebacks
    fees

    */

    //footer - can be altered via the xsl
    //or should this be database?
    handler.startElement("st_footer");
    handler.endElement("st_footer");

    //end top element
    handler.endElement("statementdetail");

  }

  protected void generateFor(MerchantStatementDetail.InfoLoadable obj)
  throws SAXException
  {
    if (obj == null)
    {
        System.out.println("InfoLoadable is NULL");
        throw new NullPointerException("Parameter obj must not be null");
    }
    if (handler == null)
    {
        throw new IllegalStateException("ContentHandler not set");
    }

    if(obj instanceof MerchantStatementDetail.InfoPlan)
    {
      MerchantStatementDetail.InfoPlan plan = (MerchantStatementDetail.InfoPlan)obj;

      handler.startElement("plansummary");

      handler.element("planType",       plan.planType);
      handler.element("salesCount",     nFormat.format(plan.salesCount));
      handler.element("saleAmount",     dFormat.format(plan.saleAmount));
      handler.element("creditCount",    nFormat.format(plan.creditCount));
      handler.element("creditAmount",   dFormat.format(plan.creditAmount));
      handler.element("netSalesAmount", dFormat.format(plan.netSalesAmount));
      handler.element("discountRate",   pFormat.format(plan.discountRate));
      handler.element("perItem",        pFormat.format(plan.perItem));
      handler.element("aveTicket",      dFormat.format(plan.aveTicket));
      handler.element("discountDue",    dFormat.format(plan.discountDue));

      handler.endElement("plansummary");
    }

    if(obj instanceof MerchantStatementDetail.InfoFee)
    {
      MerchantStatementDetail.InfoFee fee = (MerchantStatementDetail.InfoFee)obj;

      handler.startElement("fees");

      handler.element("itemNumber",     (fee.itemNumber == 0)   ? "" : nFormat.format(fee.itemNumber));
      handler.element("itemAmount",     (fee.itemAmount == 0.0) ? "" : dFormat.format(fee.itemAmount));
      handler.element("itemDescription",fee.itemDescription);
      handler.element("itemTotal",      dFormat.format(fee.itemTotal));

      handler.endElement("fees");
    }

    //uses trans.xmlTitle as XML name - so this HAS to be the same on the xsl
    if(obj instanceof MerchantStatementDetail.InfoTransaction)
    {
      MerchantStatementDetail.InfoTransaction trans = (MerchantStatementDetail.InfoTransaction)obj;

      handler.startElement(trans.xmlTitle);

      handler.element("depositDay",     trans.depositDay);
      handler.element("refNumber",      trans.refNumber);
      handler.element("transType",      trans.transType);
      handler.element("depositPl",      trans.depositPl);
      handler.element("salesCount",     nFormat.format(trans.salesCount));
      handler.element("salesAmount",    dFormat.format(trans.salesAmount));
      handler.element("creditsCount",   nFormat.format(trans.creditsCount));
      handler.element("creditsAmount",  dFormat.format(trans.creditsAmount));
      handler.element("discountPaid",   dFormat.format(trans.discountPaid));
      handler.element("settled",        dFormat.format(trans.getSettledAmount()));
      handler.endElement(trans.xmlTitle);
    }
  }

}
