/*@lineinfo:filename=OrgStatementDetail*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/trunk/src/main/com/mes/reports/OrgStatementDetail.sqlj $

  Last Modified By   : $Author: rmehta $
  Last Modified Date : $Date: 2014-03-03 15:34:22 -0800 (Mon, 03 Mar 2014) $
  Version            : $Revision: 22221 $

  Change History:
     See SVN database

  Copyright (C) 2008-2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
//JAXP
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;
import org.apache.fop.apps.FOPException;
//FOP
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;


public class OrgStatementDetail extends SQLJConnectionBase
{

  static Logger log = Logger.getLogger(OrgStatementDetail.class);

  //EXTERNAL info
  public String name      = "STERLING SAVINGS BANK";
  public String orgAddr   = "P.O. BOX 13305";
  public String orgCsz    = "SPOKANE, WA 99213-3305";
  public String helpInfo  = "24 HR HELP DESK 888/288-2692";
  public String helpName  = "MERCHANT E-SOLUTIONS";

  //INTERNAL info
  private long    id;
  private String  xsltFilename;
  private String  username;
  private String  password;
  private String  emailAddress;
  private String  ip;
  private boolean generatePrintStatement;
  private boolean generateWebStatement;
  private boolean generatePaperStatement;
  private String  docType;  //corresponds to MimeConstants MIME types
  private String  pdfDocType;
  private int     transType;
  private boolean isActive;

  private Date        statementDate     = null;
  private long[]      merchants         = null;
  private String      MonthEndFilename  = null;
  private Transformer transformer       = null;
  private FopFactory  fopFactory        = null;
  private FOUserAgent foUserAgent       = null;
  private ArrayList<Long>  paperMerchants    = null;
  private int paperBankNum;
  private int bankNum;

  public static String OUTPUT_DIR = "out";

  public OrgStatementDetail(long id, Date statementDate, FopFactory fopFactory,
      FOUserAgent foUserAgent)
  {
    this.id             = id;
    this.statementDate  = statementDate;
    this.fopFactory     = fopFactory;
    this.foUserAgent    = foUserAgent;

    //this gets all the merchants up and loaded
    generateOrgInfo();

  }

  //generates Source for translation
  public Source getSource()
  {
    return new SAXSource(new StatementXMLReader(),
                         new OrgStatementInputSource(this));
  }

  public void generateOrgInfo()
  {
    /*
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    try
    {
      connect();

      #sql [Ctx] it =
      {
        SELECT
          th.ancestor,
          sc.xslt_filename,
          sc.transmission_type,
          sc.doc_type,
          sc.ip,
          sc.username,
          sc.password,
          sc.email_address,
          sc.is_active
        FROM
          t_hierarchy th,
          mif m,
          statement_config sc
        WHERE
          m.merchant_number = :(id)
        AND
          m.association_node = th.descendent
        AND
          th.ancestor = sc.association_node
        ORDER BY
          th.relation asc
      };

      rs = it.getResultSet();

      if(rs.next())
      {
        xsltFilename            = rs.getString("xslt_filename");
        ip                      = rs.getString("ip");
        username                = rs.getString("username");
        password                = rs.getString("password");
        emailAddress            = rs.getString("email_address");
        generatePrintStatement  = false;
        generateWebStatement    = true;
        pdfDocType              = MimeConstants.MIME_PDF;
        docType                 = rs.getString("doc_type");
        transType               = rs.getInt("transmission_type");;
        isActive                = rs.getBoolean("node_id");;
      }

    }
    catch (Exception e)
    {
      System.out.println(e.getMessage());
    }
    finally
    {
      //cleanUp();
    }
*/
    //for test purposes
    //xsltFilename            = "statement.xsl";
    xsltFilename            = "resources/statement.xsl";
    ip                      = "test ip";
    username                = "test username";
    password                = "test password";
    emailAddress            = "test emailAddress";
    generatePrintStatement  = false;//getId()==StatementFileServer.STERLING?true:false;;
    generateWebStatement    = true;
    pdfDocType              = MimeConstants.MIME_PDF;
    docType                 = MimeConstants.MIME_PCL;
    transType               = 0;
    isActive                = true;
    
    paperMerchants          = new ArrayList<Long>();
    generatePaperStatement  = false;
    
    try {
      //File xsltfile = new File(resolvePath(xsltFilename));
      File xsltfile = new File(xsltFilename);
      Source xsltSource = new StreamSource(xsltfile);
      TransformerFactory factory = TransformerFactory.newInstance();
      Templates cachedXSLT = factory.newTemplates(xsltSource);
    
      transformer = cachedXSLT.newTransformer();
    } catch(TransformerException te) {}
  }

  public void getMerchantObjectAndConvertToFile(String loadFilename) {

    //heavy on the memory, but simple for now
    java.sql.Date activeDate = new java.sql.Date(statementDate.getTime());

    MerchantStatementDetail deet = null;
    int count = 0;

    for(int idx=0; idx < merchants.length; idx++) {
      log.info("generateWebStatement count = "+ idx);
      deet = getMerchantDetail(merchants[idx]);

      //valid merchant is one that has active merchant info
      //the rest can be blank
      if(deet.isValid()) {
        try {
          this.convertToFiles(loadFilename, deet, idx+1);
        } catch(IOException ioe) {
          log.error(ioe);
        } catch(Exception e) {
          log.error(e);
        }
      }
    }
  }
  
  public void getMerchantObjectListAndConvertToFile(String loadFilename) {

	    //heavy on the memory, but simple for now
	    java.sql.Date activeDate = new java.sql.Date(statementDate.getTime());

	    List<MerchantStatementDetail> detailList = new ArrayList<MerchantStatementDetail>();
	    MerchantStatementDetail detail = null;
	    int count = 0;

	    for(int idx=0; idx < merchants.length; idx++) {
	      log.info("generatePaperStatement count = "+ idx);
	      detail = getMerchantDetail(merchants[idx]);

	      //valid merchant is one that has active merchant info
	      //the rest can be blank
	      if(detail.isValid()) {
	    	  detailList.add(detail);
	      }
	    }
	    
	    if(!detailList.isEmpty()){
	    	  try {
		          this.convertToFiles(loadFilename, detailList, count);
		        } catch(IOException ioe) {
		          log.error(ioe);
		        } catch(Exception e) {
		          log.error(e);
		        }
	      }
	  }
  

  public MerchantStatementDetail getMerchantDetail(long merchantId) {
    return new MerchantStatementDetail(merchantId,
                            statementDate,
                            MonthEndFilename,
                            this);
  }
  
  public void generatePaperMerchantList()
  {

    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    //heavy on the memory, but simple for now
    java.sql.Date activeDate = new java.sql.Date(statementDate.getTime());

    try
    {
    if (this.paperBankNum > 0) {
    
    }
      connect(true);
      
      // Get total merchant count
      if ( id == 0L )   // load by filename only
      {
        /*@lineinfo:generated-code*//*@lineinfo:292^9*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT mf.merchant_number
//  			FROM mif mf, monthly_extract_gn gn
//  			WHERE mf.bank_number             =:paperBankNum
//  			AND NVL(mf.print_statements,'N') = 'Y'
//  			AND mf.date_stat_chgd_to_dcb    IS NULL
//  			AND   gn.hh_merchant_number = mf.merchant_number
//  			AND   gn.hh_active_date     = TRUNC( :activeDate , 'MONTH') 
//  			AND   gn.load_file_id       = load_filename_to_load_file_id( :MonthEndFilename )
//  			ORDER BY mf.merchant_number                  
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT mf.merchant_number\n\t\t\tFROM mif mf, monthly_extract_gn gn\n\t\t\tWHERE mf.bank_number             = :1  \n\t\t\tAND NVL(mf.print_statements,'N') = 'Y'\n\t\t\tAND mf.date_stat_chgd_to_dcb    IS NULL\n\t\t\tAND   gn.hh_merchant_number = mf.merchant_number\n\t\t\tAND   gn.hh_active_date     = TRUNC(  :2   , 'MONTH') \n\t\t\tAND   gn.load_file_id       = load_filename_to_load_file_id(  :3   )\n\t\t\tORDER BY mf.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.OrgStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,paperBankNum);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setString(3,MonthEndFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.OrgStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:303^11*/
      }
      else    // pull accounts by hierarchy node
      {
        System.out.println("loading data for " + id + " " + activeDate);
        /*@lineinfo:generated-code*//*@lineinfo:308^9*/

//  ************************************************************
//  #sql [Ctx] it = { SELECT gn.hh_merchant_number                   AS merchant_number,
//  			gn.hh_dba_name                                 AS dba_name,
//  			(gn.t1_tot_income - gn.t1_tot_discount_paid)   AS statement_fees
//  			FROM organization o,
//  			     group_merchant gm,
//  			     monthly_extract_gn gn
//  			WHERE o.org_group           = :id
//  			AND   gm.org_num            = o.org_num
//  			AND   gn.hh_merchant_number = gm.merchant_number
//  			AND   gn.hh_active_date     = TRUNC( :activeDate , 'MONTH') 
//  			-- and gn.t1_tot_income != 0
//  			AND   gn.load_file_id       = load_filename_to_load_file_id( :MonthEndFilename )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "SELECT gn.hh_merchant_number                   AS merchant_number,\n\t\t\tgn.hh_dba_name                                 AS dba_name,\n\t\t\t(gn.t1_tot_income - gn.t1_tot_discount_paid)   AS statement_fees\n\t\t\tFROM organization o,\n\t\t\t     group_merchant gm,\n\t\t\t     monthly_extract_gn gn\n\t\t\tWHERE o.org_group           =  :1  \n\t\t\tAND   gm.org_num            = o.org_num\n\t\t\tAND   gn.hh_merchant_number = gm.merchant_number\n\t\t\tAND   gn.hh_active_date     = TRUNC(  :2   , 'MONTH') \n\t\t\t-- and gn.t1_tot_income != 0\n\t\t\tAND   gn.load_file_id       = load_filename_to_load_file_id(  :3   )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.OrgStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setString(3,MonthEndFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.OrgStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:322^11*/
      }
      rs = it.getResultSet();
      
      while(rs.next()) {
	     paperMerchants.add(rs.getLong("merchant_number"));
	  }

	  log.info("Total merchant count = "+ paperMerchants.size());
	      
	  int merchantCount = paperMerchants.size();
	  merchants = new long[merchantCount];
	  int count = 0;
	  for(Long mId:paperMerchants){
	    	  merchants[count++]= mId;
	  }
    }
    catch (Exception e)
    {
        logEntry("OrgStatementDetail::generateMerchantList()",e.toString());
    }
    finally
    {
      try
      {
        cleanUp();
      }
      catch (Exception e)
      {
        //TODO?
      }
    }
  }

  public void generateMerchantList()
  {

    ResultSetIterator   it      = null;
    ResultSet           rs      = null;

    //heavy on the memory, but simple for now
    java.sql.Date activeDate = new java.sql.Date(statementDate.getTime());

    try
    {
      connect(true);
      
      // Get total merchant count
      if ( id == 0L )   // load by filename only
      {
        /*@lineinfo:generated-code*//*@lineinfo:372^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index(gn idx_mon_ext_gn_load_file_id) */
//              count(distinct gn.hh_merchant_number)
//                  from    monthly_extract_gn        gn
//                  where   gn.load_file_id = load_filename_to_load_file_id(:MonthEndFilename)
//                  --and gn.t1_tot_income != 0     
//                  -- and gn.hh_merchant_number = 490300045350 
//                  -- and rownum <= 5000
//                  order by gn.hh_merchant_number                  
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index(gn idx_mon_ext_gn_load_file_id) */\n            count(distinct gn.hh_merchant_number)\n                from    monthly_extract_gn        gn\n                where   gn.load_file_id = load_filename_to_load_file_id( :1  )\n                --and gn.t1_tot_income != 0     \n                -- and gn.hh_merchant_number = 490300045350 \n                -- and rownum <= 5000\n                order by gn.hh_merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.OrgStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,MonthEndFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.OrgStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:382^11*/
      }
      else    // pull accounts by hierarchy node
      {
        System.out.println("loading data for " + id + " " + activeDate);
        /*@lineinfo:generated-code*//*@lineinfo:387^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  count(distinct gn.hh_merchant_number)
//                  from    organization              o,
//                  group_merchant            gm,
//                  monthly_extract_gn        gn
//                  where   o.org_group = :id
//                  and gm.org_num = o.org_num
//                  and gn.hh_merchant_number = gm.merchant_number
//                  and gn.hh_active_date = trunc(:activeDate, 'MONTH')
//                  -- and gn.t1_tot_income != 0
//                  and gn.load_file_id = load_filename_to_load_file_id(:MonthEndFilename)
//                  -- and gn.hh_merchant_number = 490300045350 
//                  -- and rownum <= 5000
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  count(distinct gn.hh_merchant_number)\n                from    organization              o,\n                group_merchant            gm,\n                monthly_extract_gn        gn\n                where   o.org_group =  :1  \n                and gm.org_num = o.org_num\n                and gn.hh_merchant_number = gm.merchant_number\n                and gn.hh_active_date = trunc( :2  , 'MONTH')\n                -- and gn.t1_tot_income != 0\n                and gn.load_file_id = load_filename_to_load_file_id( :3  )\n                -- and gn.hh_merchant_number = 490300045350 \n                -- and rownum <= 5000";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.OrgStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setString(3,MonthEndFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.OrgStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:401^11*/
      }
      rs = it.getResultSet();
      
      if(rs.next()) {
        int merchantCount = rs.getInt(1);
        merchants = new long[merchantCount];
      } else {
        merchants = new long[]{};
      }
      
      if ( id == 0L )   // load by filename only
      {
        /*@lineinfo:generated-code*//*@lineinfo:414^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index(gn idx_mon_ext_gn_load_file_id) */
//              gn.hh_merchant_number         as merchant_number,
//              gn.hh_dba_name                as dba_name,
//              (gn.t1_tot_income - 
//                  gn.t1_tot_discount_paid)     as statement_fees
//                  from    monthly_extract_gn        gn
//                  where   gn.load_file_id = load_filename_to_load_file_id(:MonthEndFilename)
//                  --and gn.t1_tot_income != 0     
//                  -- and gn.hh_merchant_number = 490300045350 
//                  -- and rownum <= 5000
//                  order by gn.hh_merchant_number                  
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index(gn idx_mon_ext_gn_load_file_id) */\n            gn.hh_merchant_number         as merchant_number,\n            gn.hh_dba_name                as dba_name,\n            (gn.t1_tot_income - \n                gn.t1_tot_discount_paid)     as statement_fees\n                from    monthly_extract_gn        gn\n                where   gn.load_file_id = load_filename_to_load_file_id( :1  )\n                --and gn.t1_tot_income != 0     \n                -- and gn.hh_merchant_number = 490300045350 \n                -- and rownum <= 5000\n                order by gn.hh_merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.OrgStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,MonthEndFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.OrgStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:427^11*/
      }
      else    // pull accounts by hierarchy node
      {
        System.out.println("loading data for " + id + " " + activeDate);
        /*@lineinfo:generated-code*//*@lineinfo:432^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  gn.hh_merchant_number       as merchant_number,
//              gn.hh_dba_name              as dba_name,
//              (gn.t1_tot_income - 
//                  gn.t1_tot_discount_paid)   as statement_fees
//                  from    organization              o,
//                  group_merchant            gm,
//                  monthly_extract_gn        gn
//                  where   o.org_group = :id
//                  and gm.org_num = o.org_num
//                  and gn.hh_merchant_number = gm.merchant_number
//                  and gn.hh_active_date = trunc(:activeDate, 'MONTH')
//                  -- and gn.t1_tot_income != 0
//                  and gn.load_file_id = load_filename_to_load_file_id(:MonthEndFilename)
//                  -- and gn.hh_merchant_number = 490300045350 
//                  -- and rownum <= 5000
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  gn.hh_merchant_number       as merchant_number,\n            gn.hh_dba_name              as dba_name,\n            (gn.t1_tot_income - \n                gn.t1_tot_discount_paid)   as statement_fees\n                from    organization              o,\n                group_merchant            gm,\n                monthly_extract_gn        gn\n                where   o.org_group =  :1  \n                and gm.org_num = o.org_num\n                and gn.hh_merchant_number = gm.merchant_number\n                and gn.hh_active_date = trunc( :2  , 'MONTH')\n                -- and gn.t1_tot_income != 0\n                and gn.load_file_id = load_filename_to_load_file_id( :3  )\n                -- and gn.hh_merchant_number = 490300045350 \n                -- and rownum <= 5000";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.OrgStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,id);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setString(3,MonthEndFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.OrgStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:449^11*/
      }
      rs = it.getResultSet();

      int count = 0;
      while(rs.next()) {
        
        //valid merchant is one that has active merchant info
        //the rest can be blank
        merchants[count++] = rs.getLong("merchant_number");
      }

      log.info("Total merchant count = "+ count);
    }
    catch (Exception e)
    {
        logEntry("OrgStatementDetail::generateMerchantList()",e.toString());
    }
    finally
    {
      try
      {
        cleanUp();
      }
      catch (Exception e)
      {
        //TODO?
      }
    }
  }

  //contains MerchantStatementDetail
  public long[] getMerchantDetails()
  {
    return merchants;
  }

  public void setBankNum(int bankNum) {
	  this.bankNum = bankNum;
  }
  public int getBankNum(){
    return this.bankNum;
  }

  /**
   * converts the objects to statement files
   * @param FopFactory, FOUserAgent
   * This is the big daddy, as it basically translates the Obj into XML
   * and then runs it through the FOP engine to create the doc type requested
   */
  public void convertToFiles(
      String loadFilename,
      MerchantStatementDetail detail,
      int counter)
          throws IOException, FOPException, TransformerException
          {
    //Setup XSLT - cache it to improve performance
    //but note that since this is part of our Timed Event server code
    //no effort has been made to make this thread-safe nor runtime modification savvy
    
    //MerchantStatementDetail detail;
    //int counter = 0;
    String resolvedFileName = null;
    Fop fop;
    Source src;
    Result res;

    // Setup output to appropriate file
    OutputStream out = null;

    //PART 1
    //FOR print generation
    if(generatePrintStatement)
    {
      try
      {
        resolvedFileName     = resolvePath(""+getId()+".pcl",OUTPUT_DIR);
        //String resolvedFileName   = resolvePath(""+getId()+".pdf",OUTPUT_DIR);

        System.out.println("GENERATING PCL ..... " + resolvedFileName);

        out = new java.io.FileOutputStream(resolvedFileName);
        out = new java.io.BufferedOutputStream(out);

        // Construct fop with desired output format
        fop = fopFactory.newFop(docType, foUserAgent, out);

        //use cached transformer
        // transformer = cachedXSLT.newTransformer();

        // Setup input source for XSLT transformation
        //Source src = detail.getSource();
        src = getSource();

        // Resulting SAX events (the generated FO) must be piped through to FOP
        res = new SAXResult(fop.getDefaultHandler());

        // Start XSLT transformation and FOP processing
        transformer.transform(src, res);

      }
      catch(Exception e)
      {
        logEntry("OrgStatementDetail::convertToFiles()",e.toString());
      }
      finally
      {
          out.close();
      }
    }

    //PART 2
    //FOR data save and PDF generation...
    if(generateWebStatement)
    {
      System.out.println("saving details for web based statments...");

      try
      {
        //counter = 0;

        //for(Iterator it = merchants.iterator(); it.hasNext();)
        //{
          //counter ++;
          //System.out.println("generateWebStatement count = "+ counter);
          //detail = (MerchantStatementDetail)it.next();

          //resolvedFileName   = resolvePath("mstmt"+ detail.getFileName() + ".pdf",OUTPUT_DIR);

          // change the output stream
          out = new java.io.ByteArrayOutputStream();

          // Construct fop with desired output format
          fop = fopFactory.newFop(pdfDocType, foUserAgent, out);
          
          // Setup input source for XSLT transformation
          src = detail.getSource();

          // Resulting SAX events (the generated FO) must be piped through to FOP
          res = new SAXResult(fop.getDefaultHandler());

          // Start XSLT transformation and FOP processing
          transformer.transform(src, res);

          //generate the byte array for the detail
          byte[] statementData = ((ByteArrayOutputStream)out).toByteArray();

//@          boolean retVal = detail.saveToFile(statementData);
          boolean retVal = detail.saveForWeb(counter,
                                             loadFilename,
                                             statementData);
                                             
          
          //not necessary for ByteArrayOutputStream, but never hurts
          out.close();

          if(!retVal)
          {
            throw new Exception("Unable to completely process web docs.");
          }
          
          statementData = null;
          res           = null;
          src           = null;
          out           = null;
          
        //}  // end for
      }
      catch( Exception e )
      {
        throw new IOException(e.getMessage());
        //logEntry("OrgStatementDetail::convertToFiles()",e.toString());
      }
      finally
      {
        try{ out.close(); } catch( Exception e ) {}
      }
    }
  }
  
    /**
   * converts the object list to statement files
   * @param FopFactory, FOUserAgent
   * This is the big daddy, as it basically translates the Obj into XML
   * and then runs it through the FOP engine to create the doc type requested
   */
  public void convertToFiles(
      String loadFilename,
      List<MerchantStatementDetail> detailList,
      int counter)
          throws IOException, FOPException, TransformerException
          {

    OutputStream out = null;

    if (generatePaperStatement) {
        System.out.println("saving xml for 3rd party print statements...");
        int n2 = 0;
        try {
            String string = "mes_" + this.paperBankNum + "_" + this.statementDate.toString() + ".xml";
            OrgStatementDetail.log.error((Object)("GENERATING file ..... " + string));
            out = new FileOutputStream("statement/"+string);
            out = new BufferedOutputStream(out);
            out.write(this.generateXMLHeader().getBytes());
            Iterator<MerchantStatementDetail> merchantListIterator = detailList.iterator();
            while (merchantListIterator.hasNext()) {
                ++n2;
                System.out.println("count = " + n2);
                MerchantStatementDetail currentDetail = merchantListIterator.next();
                if (currentDetail != null) {
                    try {
                    	out.write(currentDetail.toXML().getBytes());
                    }
                    catch (Exception ex3) {
                        System.out.println("problem with MID: " + currentDetail.getMerchantNumber());
                        ex3.printStackTrace();
                    }
                }
                else {
                    OrgStatementDetail.log.error((Object)"Detail is null");
                }
            }
            out.write(this.generateXMLFooter().getBytes());
        }
        catch (Exception ex4) {
            ex4.printStackTrace();
        }
        finally {
            out.close();
        }
    }
  }

  private String resolvePath(String fileName)
  {
    return resolvePath(fileName, null);
  }

  /**
   * TODO
   * work on making this better, more dynamic
   * only use as test for now
   */

  public String resolvePath(String fileName, String subDirectory)
  {

    StringBuffer sb = new StringBuffer();

    sb.append("c:").append(File.separator).append("temp").append(File.separator);
    if(subDirectory != null)
    {
      sb.append(subDirectory).append(File.separator);
    }
    sb.append(fileName);

    return sb.toString();

  }
  
  private String generateXMLHeader() {
        final StringBuffer sb = new StringBuffer();
        sb.append("<mes-print-statements>");
        sb.append("<statementDate>").append(this.statementDate.toString()).append("</statementDate>");
        sb.append("<statementMsg>").append("").append("</statementMsg>");
        sb.append("<codeLegend><category><title>Transaction Codes</title><item><code>D</code><name>DEPOSIT</name></item><item><code>C</code><name>CHARGEBACK</name></item><item><code>A</code><name>ADJUSTMENT</name></item><item><code>R</code><name>CHARGEBACK REVERSAL</name></item></category></codeLegend>");
        return sb.toString();
    }
    
    private String generateXMLFooter() {
        return "</mes-print-statements>";
    }

//accessors
public long    getId()            { return id;}
public void    setId(long value)  { id = value; }
public String  getXSLTFilename()  { return xsltFilename;}
public String  getUsername()      { return username;}
public String  getPassword()      { return password;}
public String  getEmailAddress()  { return emailAddress;}
public String  getIP()            { return ip;}
public String  getDocType()       { return docType;}
public int     getTransType()     { return transType;}
public boolean getIsActive()      { return isActive;}

public String  getMonthEndFilename()                { return( MonthEndFilename ); }
public void    setMonthEndFilename( String value )  { MonthEndFilename = value;   }

public boolean getGeneratePaperStatement()            { return generatePaperStatement;}
public void    setGeneratePaperStatement(boolean value)  { generatePaperStatement = value; }
public int     getPaperBankNum() {return this.paperBankNum;}
public void    setPaperBankNum(int bankNum) { this.paperBankNum = bankNum;}

}/*@lineinfo:generated-code*/