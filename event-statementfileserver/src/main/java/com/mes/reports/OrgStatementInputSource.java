package com.mes.reports;

import org.xml.sax.InputSource;

/**
 * This class is a special InputSource descendent for using OrgStatementDetail
 * instances as XML sources.
 */
public class OrgStatementInputSource extends InputSource {

    private OrgStatementDetail statementDetail;

    /**
     * Constructor for the StatementInputSource
     * @param statementDetail The OrgStatementDetail object to use
     */
    public OrgStatementInputSource(OrgStatementDetail statementDetail)
    {
      this.statementDetail = statementDetail;
    }

    /**
     * Returns the statementDetail.
     * @return OrgStatementDetail
     */
    public OrgStatementDetail getOrgStatementDetail()
    {
      return statementDetail;
    }

    /**
     * Sets the statementDetail.
     * @param OrgStatementDetail
     */
    public void setOrgStatementDetail(OrgStatementDetail statementDetail)
    {
      this.statementDetail = statementDetail;
    }

}
