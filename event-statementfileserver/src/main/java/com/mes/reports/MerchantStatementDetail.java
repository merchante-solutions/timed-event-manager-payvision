/*@lineinfo:filename=MerchantStatementDetail*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/trunk/src/main/com/mes/reports/MerchantStatementDetail.sqlj $

  Last Modified By   : $Author: rmehta $
  Last Modified Date : $Date: 2014-08-04 11:02:19 -0700 (Mon, 04 Aug 2014) $
  Version            : $Revision: 22985 $

  Change History:
     See SVN database

  Copyright (C) 2008-2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
// XML libraries
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import org.apache.log4j.Logger;
import com.mes.ach.AchEntryData;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import oracle.sql.BLOB;
import sqlj.runtime.ResultSetIterator;


//queries to generate monthly statement details
public class MerchantStatementDetail extends SQLJConnectionBase
{

  static Logger log = Logger.getLogger(MerchantStatementDetail.class);

  //hold a ref to the parent org info
  private long merchantNumber;
  private java.sql.Date statementDate;
  private OrgStatementDetail orgDetail;
  public InfoMerchant merchDetails = null;
  private String      MonthEndFilename  = null;

  private boolean isValid          = false;
  private StringBuffer xmlSB       = null;
  public DecimalFormat df          = new DecimalFormat("##0.00##");


  //PLAN vars
  public HashMap    plans             = new HashMap();
  public int    pSalesNumberTotal        = 0;
  public double pSalesDollarTotal     = 0.0d;
  public int    pCreditNumberTotal       = 0;
  public double pCreditDollarTotal    = 0.0d;
  public double pNetSalesTotal        = 0.0d;
  public double pAveTicketTotal       = 0.0d;
  public double pDiscountTotal        = 0.0d;

  //DEPOSIT vars
  public LinkedList deposits    = new LinkedList();
  public int    dSalesNumberTotal     = 0;
  public double dSalesDollarTotal     = 0.0d;
  public int    dCreditNumberTotal    = 0;
  public double dCreditDollarTotal    = 0.0d;
  public double dDiscountPaid         = 0.0d;
  public double dSettledTotal         = 0.0d;

  //ADJUSTMENT vars
  public LinkedList adjustments = new LinkedList();
  public int    aSalesNumberTotal     = 0;
  public double aSalesDollarTotal     = 0.0d;
  public int    aCreditNumberTotal    = 0;
  public double aCreditDollarTotal    = 0.0d;
  public double aDiscountTotal        = 0.0d;
  public double aSettledTotal         = 0.0d;

  //CHARGEBACK vars
  public LinkedList chargebacks = new LinkedList();
  public int    cSalesNumberTotal     = 0;
  public double cSalesDollarTotal     = 0.0d;
  public int    cCreditNumberTotal    = 0;
  public double cCreditDollarTotal    = 0.0d;
  public double cDiscountTotal        = 0.0d;
  public double cSettledTotal         = 0.0d;

  //FEE vars
  public LinkedList fees              = new LinkedList();
  public double fFeeTotal             = 0.0d;

  //TOTAL DEDUCTION
  public double totalDiscountDue      = 0.0d;
  public double totalAmountDeducted   = 0.0d;

  // labels
  public String labelDiscountDue      = "DISCOUNT DUE";


  public final static int INFO_MERCHANT       = 1;
  public final static int INFO_PLAN           = 2;
  public final static int INFO_FEE            = 3;
  public final static int INFO_TRANSACTION    = 4;
  public final static int INFO_RETURN_ADDRESS = 5;
  public final static int INFO_STMT_MSG       = 6;

  //for insertion into merch_statements
  public final static String LOAD_FILENAME  = "stmt_testing";

  //for return address
  public String name      = "";
  public String orgAddr   = "";
  public String orgCsz    = "";
  public String helpInfo  = "";
  public String helpName  = "";

  public String imgName   = "";

  //JAN   = "THANK YOU TO ALL WHO VALIDATED THE COMPLIANCE OF YOUR BUSINESS WITH THE PAYMENT CARD INDUSTRY DATA SECURITY STANDARD (PCI DSS). YOU HAVE TAKEN AN IMPORTANT STEP IN HELPING TO PREVENT THE LOSS OF VITAL CARDHOLDER INFORMATION. IF YOU HAVE NOT YET VALIDATED THE PCI COMPLIANCE OF YOUR BUSINESS, PLEASE SIGN INTO THE MES WEBSITE AT WWW.MERCHANTE-SOLUTIONS.COM AND COMPLETE THE PCI QUESTIONNAIRE. IF YOU NEED HELP GETTING STARTED, PLEASE CALL THE MES HELP DESK AT 888-288-2692.";
  //FEB   = "Visa & MasterCard have announced rate changes as of April 1, 2010 that impact your business as follows. Discount rates will go up by .02% (2 cents per $100 transaction). Rates for non-qualified transactions will go up by .05%. Pass-through changes from MC include an association increase of .015% and new rate categories for Enhanced, World, World Elite & Small Business Enhanced Value business cards. Visa has increased the per item for retail debit $.05. Lastly, 0 is no longer an eligible value for sales tax in MC transactions. We have strived to control internal costs in order to keep rate increases to a minimum. If you have questions, please call 888-288-2692.";
  //MAR   = "Many states require that card numbers and expiration dates be truncated or masked on customer and merchant copies of receipts. The Visa/MC rules also require that card numbers be truncated on customer receipts.  To ensure your business complies with various State and Federal laws, as well as Visa/MC rules, please check your customer and merchant receipts to confirm they do not display card numbers or expiration dates.  If you find this information is appearing on receipts, please call our Help Desk at 888-288-2692 and we'll work with you to eliminate this information from receipts.";
  public String stmtMsg   = "";


 // card details
  int     sViCnt = 0;   int     sMcCnt = 0;   int     sDiCnt = 0;
  double  sViAmt = 0;   double  sMcAmt = 0;   double  sDiAmt = 0;
  int     cViCnt = 0;   int     cMcCnt = 0;   int     cDiCnt = 0;
  double  cViAmt = 0;   double  cMcAmt = 0;   double  cDiAmt = 0;
  double  dViAmt = 0;   double  dMcAmt = 0;   double  dDiAmt = 0;

  int     sAxCnt = 0;   int     sJcCnt = 0;   int     sDbCnt = 0;
  double  sAxAmt = 0;   double  sJcAmt = 0;   double  sDbAmt = 0;
  int     cAxCnt = 0;   int     cJcCnt = 0;   int     cDbCnt = 0;
  double  cAxAmt = 0;   double  cJcAmt = 0;   double  cDbAmt = 0;
  double  dAxAmt = 0;   double  dJcAmt = 0;   double  dDbAmt = 0;

  int     sDsCnt = 0;   int     sEbCnt = 0;   int     sP1Cnt = 0;
  double  sDsAmt = 0;   double  sEbAmt = 0;   double  sP1Amt = 0;
  int     cDsCnt = 0;   int     cEbCnt = 0;   int     cP1Cnt = 0;
  double  cDsAmt = 0;   double  cEbAmt = 0;   double  cP1Amt = 0;
  double  dDsAmt = 0;   double  dEbAmt = 0;   double  dP1Amt = 0;
  											  /* Code modified by RS for ACHPS implementation */
  int     sP2Cnt = 0;   int     sP3Cnt = 0;    int  sAchCnt     =  0; 
  double  sP2Amt = 0;   double  sP3Amt = 0;    double  sAchAmt  =  0;
  int     cP2Cnt = 0;   int     cP3Cnt = 0;    int  cAchCnt    =  0;
  double  cP2Amt = 0;   double  cP3Amt = 0;    double  cAchAmt =  0;
  double  dP2Amt = 0;   double  dP3Amt = 0;    double  dAchAmt   =  0;
  /* Code modified by RS for LCR implementation  */
  int mpSalesCnt = 0;    double mpSalesAmt = 0;  int mpCreditCnt = 0;
  double mpCreditAmt = 0; double mpDiscAmt = 0; int vpSalesCnt = 0;
  double vpSalesAmt = 0;  int vpCreditCnt = 0;  double vpCreditAmt = 0;
  double vpDiscAmt = 0;

  public MerchantStatementDetail(long merchantNumber, Date _statementDate, String meFilename, OrgStatementDetail orgDetail)
  {

    this.merchantNumber = merchantNumber;
    //wrap for db
    this.statementDate = new java.sql.Date(_statementDate.getTime());
    this.orgDetail = orgDetail;
    this.MonthEndFilename = meFilename;

    init();
  }

  private void init()
  {
    try
    {
      System.out.println("building merchant number..." + merchantNumber + " " + statementDate);


      connect(true);

      //for this list, generate all the applicable information
      generateMerchantInfo();
      
   // As merchants who are not valid are not considered for statement generation the subsequent building of this object is not required
      if(!isValid) {
        log.debug("building terminated... Invalid");
        return;
      }
      
      generatePlanInfo();
      generateFeeInfo();
      generateTransactionInfo();
      generateCardInsertInfo();
      generateReturnAddress();
      generateStatementMsg();

      double minDiscount = 0.0;
      /*@lineinfo:generated-code*//*@lineinfo:204^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(gn.t1_tot_calculated_discount,0)
//          
//          from    monthly_extract_gn  gn
//          where   gn.hh_merchant_number = :merchantNumber
//                  and gn.hh_active_date = :statementDate
//                  and gn.load_filename = nvl(:MonthEndFilename,gn.load_filename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(gn.t1_tot_calculated_discount,0)\n         \n        from    monthly_extract_gn  gn\n        where   gn.hh_merchant_number =  :1  \n                and gn.hh_active_date =  :2  \n                and gn.load_filename = nvl( :3  ,gn.load_filename)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.MerchantStatementDetail",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDate(2,statementDate);
   __sJT_st.setString(3,MonthEndFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   minDiscount = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:212^7*/
      if ( MesMath.round(pDiscountTotal,2) < minDiscount )
      {
        labelDiscountDue  = "Minimum Discount Due";
        totalDiscountDue  = minDiscount;
      }
      else
      {
        labelDiscountDue  = "Discount Due";
        totalDiscountDue  = pDiscountTotal;
      }

      //set the total
      totalAmountDeducted = totalDiscountDue + fFeeTotal - dDiscountPaid;

      System.out.println("building complete... SUCCESS");
    }
    catch(Exception e)
    {
      logEntry("init(" + merchantNumber + "," + statementDate + ")",e.toString());
      System.out.println("building complete... FAILURE: " + e.toString());
      //currently have catch-all here,
      //need to determine how this should load if one part fails
      //since all parts are needed to have a realistic statement
      //maybe RESET components and add to orgDetail as a problem account?
    }
    finally
    {
      cleanUp();
    }

  }

  public boolean isValid()
  {
    return isValid;
  }

  public void setOrgDetail(OrgStatementDetail orgDetail)
  {
    this.orgDetail = orgDetail;
  }

  public OrgStatementDetail getOrgDetail()
  {
    return orgDetail;
  }

  public java.sql.Date getStatementDate()
  {
    return statementDate;
  }

  //Address/Merchant information
  //(top of statement regarding merchant display info
  private void generateMerchantInfo()
    throws Exception
  {
    System.out.println("generateMerchantInfo...");
    ResultSetIterator it = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:274^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_char(gn.hh_active_date, 'yyyymm')  year_month,
//                  to_char(gn.hh_active_date, 'mm-yyyy') processing_month,
//                  gn.hh_merchant_number                 merchant_number,
//                  gn.g1_association_number              association,
//                  gn.g1_dba_name                        dba_name,
//                  mf.dda_num                            dda,
//                  mf.transit_routng_num                 transit_routing,
//                  decode(nvl(mf.remit_fees, 'N'),'Y','true','false')
//                                                        remit_status,
//                  nvl(sm.mailing_name, decode(mf.bank_number, 3942, mf.dba_name,gn.ad_name_line1))
//                                                        address_name,
//                  nvl(sm.mailing_addr1,decode(mf.bank_number, 3942,  mf.addr1_line_1,gn.ad_address_line1))
//                                                        address_line_1,
//                  nvl(sm.mailing_addr2,decode(mf.bank_number, 3942, nvl(mf.addr1_line_2,''), nvl(gn.ad_address_line2,'')))
//                                                        address_line_2,
//                  nvl(sm.mailing_city,decode(mf.bank_number, 3942, mf.city1_line_4,gn.ad_city))
//                                                        address_city,
//                  nvl(sm.mailing_state,decode(mf.bank_number, 3942, mf.state1_line_4,gn.ad_state))
//                                                        address_state,
//                  substr(nvl(sm.mailing_zip,decode(mf.bank_number, 3942, mf.zip1_line_4, gn.ad_zip_code)),1,5)||'-'
//                    ||substr(nvl(sm.mailing_zip,decode(mf.bank_number, 3942, mf.zip1_line_4,gn.ad_zip_code)),6,4)
//                                                        address_zip
//          from    monthly_extract_gn        gn,
//                  mif                       mf,
//                  monthly_extract_summary   sm
//          where   gn.hh_merchant_number = :merchantNumber
//                  and gn.hh_active_date = :statementDate
//                  and gn.load_filename = nvl(:MonthEndFilename,gn.load_filename)
//                  and mf.merchant_number = gn.hh_merchant_number
//                  and gn.hh_load_sec = sm.hh_load_sec(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  to_char(gn.hh_active_date, 'yyyymm')  year_month,\n                to_char(gn.hh_active_date, 'mm-yyyy') processing_month,\n                gn.hh_merchant_number                 merchant_number,\n                gn.g1_association_number              association,\n                gn.g1_dba_name                        dba_name,\n                mf.dda_num                            dda,\n                mf.transit_routng_num                 transit_routing,\n                decode(nvl(mf.remit_fees, 'N'),'Y','true','false')\n                                                      remit_status,\n                nvl(sm.mailing_name, decode(mf.bank_number, 3942, mf.dba_name,gn.ad_name_line1))\n                                                      address_name,\n                nvl(sm.mailing_addr1,decode(mf.bank_number, 3942,  mf.addr1_line_1,gn.ad_address_line1))\n                                                      address_line_1,\n                nvl(sm.mailing_addr2,decode(mf.bank_number, 3942, nvl(mf.addr1_line_2,''), nvl(gn.ad_address_line2,'')))\n                                                      address_line_2,\n                nvl(sm.mailing_city,decode(mf.bank_number, 3942, mf.city1_line_4,gn.ad_city))\n                                                      address_city,\n                nvl(sm.mailing_state,decode(mf.bank_number, 3942, mf.state1_line_4,gn.ad_state))\n                                                      address_state,\n                substr(nvl(sm.mailing_zip,decode(mf.bank_number, 3942, mf.zip1_line_4, gn.ad_zip_code)),1,5)||'-'\n                  ||substr(nvl(sm.mailing_zip,decode(mf.bank_number, 3942, mf.zip1_line_4,gn.ad_zip_code)),6,4)\n                                                      address_zip\n        from    monthly_extract_gn        gn,\n                mif                       mf,\n                monthly_extract_summary   sm\n        where   gn.hh_merchant_number =  :1  \n                and gn.hh_active_date =  :2  \n                and gn.load_filename = nvl( :3  ,gn.load_filename)\n                and mf.merchant_number = gn.hh_merchant_number\n                and gn.hh_load_sec = sm.hh_load_sec(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.MerchantStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDate(2,statementDate);
   __sJT_st.setString(3,MonthEndFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.MerchantStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:306^7*/

      loadData(it, INFO_MERCHANT);
    }
    catch(Exception e)
    {
      throw e;
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
  }

  private void generateReturnAddress()
    throws Exception
  {
    System.out.println("generateReturnAddress...");
    ResultSetIterator it = null;

    try
    {
      if( merchDetails != null &&
          "true".equals(merchDetails.remitStatus))
      {
        // remittance return addresses are stored in a separate table
        /*@lineinfo:generated-code*//*@lineinfo:332^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  ra.hierarchy_node,
//                    nvl(ra.address_name,'none')      as return_addr_name,
//                    ra.address_1         as return_addr_1,
//                    ra.address_2         as return_addr_2,
//                    (ra.address_city || ', ' || ra.address_state || '  ' || ra.address_zip)     
//                                         as return_addr_3,
//                    null                 as return_addr_4,
//                    ra.phone_number      as return_addr_phone,
//                    ra.help_info         as return_addr_help_name,
//                    case when ra.hierarchy_node = :merchantNumber then -1 else thr.relation end
//                                         as relation
//            from    monthly_extract_remit_address ra,
//                    organization                  o,
//                    group_merchant                gm,
//                    (
//                      select  ra.hierarchy_node,
//                              th.relation
//                      from    mif                           mf,
//                              t_hierarchy                   th,
//                              monthly_extract_remit_address ra
//                      where   mf.merchant_number = :merchantNumber
//                              and th.hier_type = 1
//                              and th.ancestor = ra.hierarchy_node
//                              and th.descendent = mf.association_node                                               
//                    )                             thr                  
//            where   o.org_group = ra.hierarchy_node
//                    and gm.org_num = o.org_num
//                    and gm.merchant_number = :merchantNumber
//                    and thr.hierarchy_node(+) = ra.hierarchy_node
//            order by relation
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ra.hierarchy_node,\n                  nvl(ra.address_name,'none')      as return_addr_name,\n                  ra.address_1         as return_addr_1,\n                  ra.address_2         as return_addr_2,\n                  (ra.address_city || ', ' || ra.address_state || '  ' || ra.address_zip)     \n                                       as return_addr_3,\n                  null                 as return_addr_4,\n                  ra.phone_number      as return_addr_phone,\n                  ra.help_info         as return_addr_help_name,\n                  case when ra.hierarchy_node =  :1   then -1 else thr.relation end\n                                       as relation\n          from    monthly_extract_remit_address ra,\n                  organization                  o,\n                  group_merchant                gm,\n                  (\n                    select  ra.hierarchy_node,\n                            th.relation\n                    from    mif                           mf,\n                            t_hierarchy                   th,\n                            monthly_extract_remit_address ra\n                    where   mf.merchant_number =  :2  \n                            and th.hier_type = 1\n                            and th.ancestor = ra.hierarchy_node\n                            and th.descendent = mf.association_node                                               \n                  )                             thr                  \n          where   o.org_group = ra.hierarchy_node\n                  and gm.org_num = o.org_num\n                  and gm.merchant_number =  :3  \n                  and thr.hierarchy_node(+) = ra.hierarchy_node\n          order by relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.MerchantStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setLong(2,merchantNumber);
   __sJT_st.setLong(3,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.MerchantStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:364^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:368^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mba.name                as return_addr_name,
//                    nvl(mba.address_1,'')   as return_addr_1,
//                    nvl(mba.address_2,'')   as return_addr_2,
//                    nvl(mba.address_3,'')   as return_addr_3,
//                    nvl(mba.address_4,'')   as return_addr_4,
//                    mba.phone               as return_addr_phone,
//                    'MERCHANT E-SOLUTIONS'  as return_addr_help_name
//            from    mif mf,
//                    t_hierarchy th,
//                    mbs_associations mba
//            where   mf.merchant_number = :merchantNumber
//                    and mf.association_node = th.descendent
//                    and th.ancestor = mba.association_node
//                    and mba.address_enabled = 'Y'
//            order by th.relation
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mba.name                as return_addr_name,\n                  nvl(mba.address_1,'')   as return_addr_1,\n                  nvl(mba.address_2,'')   as return_addr_2,\n                  nvl(mba.address_3,'')   as return_addr_3,\n                  nvl(mba.address_4,'')   as return_addr_4,\n                  mba.phone               as return_addr_phone,\n                  'MERCHANT E-SOLUTIONS'  as return_addr_help_name\n          from    mif mf,\n                  t_hierarchy th,\n                  mbs_associations mba\n          where   mf.merchant_number =  :1  \n                  and mf.association_node = th.descendent\n                  and th.ancestor = mba.association_node\n                  and mba.address_enabled = 'Y'\n          order by th.relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.MerchantStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.MerchantStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:385^9*/
      }
      loadData( it, INFO_RETURN_ADDRESS );
    }
    catch(Exception e)
    {
      throw e;
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
    }
    
    
  }

  private void generateStatementMsg()
  {

    System.out.println("generateStatementMsg...");
    ResultSetIterator it = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:409^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct
//                  case 
//                    when msm.association_node = mf.merchant_number then -1 
//                    else th.relation 
//                  end                        as relation,
//                  nvl(msg.statement_msg,' ') as statement_msg
//          from    mif mf,
//                  t_hierarchy th,
//                  mbs_statement_map msm,
//                  mbs_statement_message msg
//          where   mf.merchant_number = :merchantNumber
//                  and th.descendent = mf.association_node 
//                  and msm.association_node in (th.ancestor,mf.merchant_number) 
//                  and msm.enabled = 'Y'
//                  and msm.msg_date = :statementDate
//                  and msm.statement_id = msg.id
//          order by case when msm.association_node = mf.merchant_number then -1 else th.relation end
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct\n                case \n                  when msm.association_node = mf.merchant_number then -1 \n                  else th.relation \n                end                        as relation,\n                nvl(msg.statement_msg,' ') as statement_msg\n        from    mif mf,\n                t_hierarchy th,\n                mbs_statement_map msm,\n                mbs_statement_message msg\n        where   mf.merchant_number =  :1  \n                and th.descendent = mf.association_node \n                and msm.association_node in (th.ancestor,mf.merchant_number) \n                and msm.enabled = 'Y'\n                and msm.msg_date =  :2  \n                and msm.statement_id = msg.id\n        order by case when msm.association_node = mf.merchant_number then -1 else th.relation end";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.MerchantStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDate(2,statementDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.MerchantStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:428^7*/

      loadData( it, INFO_STMT_MSG );
    }
    catch(Exception e)
    {
      //not a fatal issue here - we'll leave the statement blank and keep going
      logEntry("generateStatementMsg(" + merchantNumber + "," + statementDate + ")",e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
    }
  }

  //plan types
  //(top of statement regarding card types, discount rate, and discount due
  //Code modified by RS for ACHPS implementation, added card type 'AC' and Plantype 'ACH'
  //Code modified by RS for LCR implementation. Added plan type VP and MP
  private void generatePlanInfo()
  throws Exception
  {
    //System.out.println("generatePlanInfo...");
    ResultSetIterator it = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:455^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode( pl.pl_plan_type,
//                          'VISA',1,'VSBS',2,'VSDB',3,
//                          'MC'  ,4,'MCBS',5,'MCDB',6,
//                          'DEBC',7,'EBT',8,'AMEX',9,'DISC',10,'DS',11,'AC',12,13 )            as sort_order,
//                  decode( pl.pl_plan_type,
//                          'MC'  ,'MC',
//                          'MCBS','MC',
//                          'MCDB','MC',
//                          'VISA','VS',
//                          'VSBS','VS',
//                          'VSDB','VS',
//                          'DEBC','DB',
//                          'EBT' ,'EB',
//                          'AMEX','AM',
//                          'DISC','DS',
//                          pl.pl_plan_type )       as combine_plan,
//                  decode( pl.pl_plan_type,
//                          'MC'  ,'MC',
//                          'MCBS','MB',
//                          'MCDB','MD',
//                          'VISA','VS',
//                          'VSBS','VB',
//                          'VSDB','VD',
//                          'DEBC','DB',
//                          'EBT' ,'EB',
//                          'AMEX','AM',
//                          'DISC','DS',
//                          'AC','ACH',
//                          'VP','VP',
//                          'MP','MP',
//                          pl.pl_plan_type )       as plan_type,
//                  pl.pl_disc_rate                 as discount_rate,
//                  pl.pl_disc_rate_per_item        as per_item,
//                  sum(pl.pl_number_of_sales)      as sales_count,
//                  sum(pl.pl_sales_amount)         as sales_amount,
//                  sum(pl.pl_number_of_credits)    as credits_count,
//                  sum(pl.pl_credits_amount)       as credits_amount,
//                  sum( pl.pl_sales_amount -
//                       pl.pl_credits_amount )     as net_sales_amount,
//                  sum(pl.pl_correct_disc_amt)     as discount_due,
//                  sum(pl.pl_sales_amount)/decode(sum(pl.pl_number_of_sales),0,1,sum(pl.pl_number_of_sales))
//                                                  as average_ticket
//          from    monthly_extract_gn gn,
//                  monthly_extract_pl pl
//          where   gn.hh_merchant_number = :merchantNumber
//                  and gn.hh_active_date = :statementDate
//                  and gn.load_filename = nvl(:MonthEndFilename,gn.load_filename)
//                  and gn.hh_load_sec = pl.hh_load_sec
//          group by  pl.pl_plan_type,
//                    pl.pl_disc_rate,
//                    pl.pl_disc_rate_per_item
//          order by sort_order, plan_type
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode( pl.pl_plan_type,\n                        'VISA',1,'VSBS',2,'VSDB',3,\n                        'MC'  ,4,'MCBS',5,'MCDB',6,\n                        'DEBC',7,'EBT',8,'AMEX',9,'DISC',10,'DS',11,'AC',12,13 )            as sort_order,\n                decode( pl.pl_plan_type,\n                        'MC'  ,'MC',\n                        'MCBS','MC',\n                        'MCDB','MC',\n                        'VISA','VS',\n                        'VSBS','VS',\n                        'VSDB','VS',\n                        'DEBC','DB',\n                        'EBT' ,'EB',\n                        'AMEX','AM',\n                        'DISC','DS',\n                        pl.pl_plan_type )       as combine_plan,\n                decode( pl.pl_plan_type,\n                        'MC'  ,'MC',\n                        'MCBS','MB',\n                        'MCDB','MD',\n                        'VISA','VS',\n                        'VSBS','VB',\n                        'VSDB','VD',\n                        'DEBC','DB',\n                        'EBT' ,'EB',\n                        'AMEX','AM',\n                        'DISC','DS',\n                        'AC','ACH',\n                        'VP','VP',\n                        'MP','MP',\n                        pl.pl_plan_type )       as plan_type,\n                pl.pl_disc_rate                 as discount_rate,\n                pl.pl_disc_rate_per_item        as per_item,\n                sum(pl.pl_number_of_sales)      as sales_count,\n                sum(pl.pl_sales_amount)         as sales_amount,\n                sum(pl.pl_number_of_credits)    as credits_count,\n                sum(pl.pl_credits_amount)       as credits_amount,\n                sum( pl.pl_sales_amount -\n                     pl.pl_credits_amount )     as net_sales_amount,\n                sum(pl.pl_correct_disc_amt)     as discount_due,\n                sum(pl.pl_sales_amount)/decode(sum(pl.pl_number_of_sales),0,1,sum(pl.pl_number_of_sales))\n                                                as average_ticket\n        from    monthly_extract_gn gn,\n                monthly_extract_pl pl\n        where   gn.hh_merchant_number =  :1  \n                and gn.hh_active_date =  :2  \n                and gn.load_filename = nvl( :3  ,gn.load_filename)\n                and gn.hh_load_sec = pl.hh_load_sec\n        group by  pl.pl_plan_type,\n                  pl.pl_disc_rate,\n                  pl.pl_disc_rate_per_item\n        order by sort_order, plan_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.MerchantStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDate(2,statementDate);
   __sJT_st.setString(3,MonthEndFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.MerchantStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:509^7*/
      loadData(it, INFO_PLAN);
    }
    catch(Exception e)
    {
      throw e;
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
    }
  }

  //fees
  private void generateFeeInfo()
  throws Exception
  {

    //System.out.println("generateFeeInfo...");
    ResultSetIterator it = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:532^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  st.st_number_of_items_on_stmt   item_number,
//                  st.st_amount_of_item_on_stmt    item_amount,
//                  st.st_statement_desc            item_description,
//                  st.st_fee_amount                item_total
//          from    monthly_extract_gn gn,
//                  monthly_extract_st st
//          where   gn.hh_merchant_number = :merchantNumber
//                  and gn.hh_active_date = :statementDate
//                  and gn.load_filename = nvl(:MonthEndFilename,gn.load_filename)
//                  and gn.hh_load_sec = st.hh_load_sec
//          order by nvl(st.item_category,'ALL'), st.st_statement_desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  st.st_number_of_items_on_stmt   item_number,\n                st.st_amount_of_item_on_stmt    item_amount,\n                st.st_statement_desc            item_description,\n                st.st_fee_amount                item_total\n        from    monthly_extract_gn gn,\n                monthly_extract_st st\n        where   gn.hh_merchant_number =  :1  \n                and gn.hh_active_date =  :2  \n                and gn.load_filename = nvl( :3  ,gn.load_filename)\n                and gn.hh_load_sec = st.hh_load_sec\n        order by nvl(st.item_category,'ALL'), st.st_statement_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.MerchantStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDate(2,statementDate);
   __sJT_st.setString(3,MonthEndFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.MerchantStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:545^7*/

      loadData(it, INFO_FEE);
    }
    catch(Exception e)
    {
      throw e;
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
    }
  }

/**
 * 3 Types of transaction here: Deposits, Adjusmtents and Chargebacks
 * Sort query based on translation of ats.entry_description and set
 * deposit_type accordingly
 **/
  private void generateTransactionInfo()
  throws Exception
  {
    java.sql.Date         conversionDate    = null;
    int                   converted         = 0;
    ResultSetIterator     it                = null;
    int                   processorId       = 0;

    System.out.println("generateTransactionInfo...");

    try
    {
      // check the back end setting for this merchant
      /*@lineinfo:generated-code*//*@lineinfo:577^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(mf.processor_id,0)      as mbs_merchant,
//                  case
//                    when nvl(mf.conversion_date,'31-dec-9999') <= :statementDate then 1
//                    else 0
//                  end                         as converted,
//                  mf.conversion_date          as conversion_date
//          
//          from    mif               mf
//          where   mf.merchant_number = :merchantNumber
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(mf.processor_id,0)      as mbs_merchant,\n                case\n                  when nvl(mf.conversion_date,'31-dec-9999') <=  :1   then 1\n                  else 0\n                end                         as converted,\n                mf.conversion_date          as conversion_date\n         \n        from    mif               mf\n        where   mf.merchant_number =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.MerchantStatementDetail",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,statementDate);
   __sJT_st.setLong(2,merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   processorId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   converted = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   conversionDate = (java.sql.Date)__sJT_rs.getDate(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:588^7*/

      // has been coverted for the current month
      // then use the MBS table data
      if ( processorId == 1 && converted == 1 )
      {
        String whereClause = (conversionDate.before(statementDate) ? "" : " and ds.merchant_number = gn.hh_merchant_number ");

        /*@lineinfo:generated-code*//*@lineinfo:596^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_char(adj.batch_date, 'Mon DD')         as deposit_day,
//                    decode(adj.transacction_code,9071,'C','B')as tran_type,
//                    adj.reference_number                      as reference_number,
//                    'T'                                       as deposit_pl,
//                    decode(adj.transacction_code,9071,1,0)    as sales_count,
//                    decode(adj.transacction_code,9071,adj.adjustment_amount,0)
//                                                              as sales_amount,
//                    decode(adj.transacction_code,9077,1,0)    as credits_count,
//                    decode(adj.transacction_code,9077,adj.adjustment_amount,0)
//                                                              as credits_amount,
//                    0                                         as discount_paid,
//                    (adj.adjustment_amount * decode(adj.debit_credit_ind,'D',-1,1))
//                                                              as settled,
//                    :AchEntryData.ED_CHARGEBACK             as entry_description
//            from    monthly_extract_gn            gn,
//                    daily_detail_file_adjustment  adj
//            where   gn.hh_merchant_number = :merchantNumber
//                    and gn.hh_active_date = :statementDate
//                    and gn.load_filename = nvl(:MonthEndFilename,gn.load_filename)
//                    and adj.merchant_account_number = gn.hh_merchant_number
//                    and adj.batch_date between (gn.hh_active_date-120) and gn.hh_curr_date+7
//                    and exists
//                    (
//                      select  /*+ index (ds idx_bds_load_file_id_mid) */
//                              ds.data_source
//                      from    mbs_daily_summary   ds
//                      where   ds.me_load_file_id = gn.load_file_id
//                              and ds.merchant_number = gn.hh_merchant_number
//                              and ds.activity_date between adj.batch_date-30 and adj.batch_date+7
//                              and ds.item_type = 5
//                              and ds.data_source = adj.load_filename
//                    )                  
//            order by adj.batch_date, adj.reference_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  to_char(adj.batch_date, 'Mon DD')         as deposit_day,\n                  decode(adj.transacction_code,9071,'C','B')as tran_type,\n                  adj.reference_number                      as reference_number,\n                  'T'                                       as deposit_pl,\n                  decode(adj.transacction_code,9071,1,0)    as sales_count,\n                  decode(adj.transacction_code,9071,adj.adjustment_amount,0)\n                                                            as sales_amount,\n                  decode(adj.transacction_code,9077,1,0)    as credits_count,\n                  decode(adj.transacction_code,9077,adj.adjustment_amount,0)\n                                                            as credits_amount,\n                  0                                         as discount_paid,\n                  (adj.adjustment_amount * decode(adj.debit_credit_ind,'D',-1,1))\n                                                            as settled,\n                   :1               as entry_description\n          from    monthly_extract_gn            gn,\n                  daily_detail_file_adjustment  adj\n          where   gn.hh_merchant_number =  :2  \n                  and gn.hh_active_date =  :3  \n                  and gn.load_filename = nvl( :4  ,gn.load_filename)\n                  and adj.merchant_account_number = gn.hh_merchant_number\n                  and adj.batch_date between (gn.hh_active_date-120) and gn.hh_curr_date+7\n                  and exists\n                  (\n                    select  /*+ index (ds idx_bds_load_file_id_mid) */\n                            ds.data_source\n                    from    mbs_daily_summary   ds\n                    where   ds.me_load_file_id = gn.load_file_id\n                            and ds.merchant_number = gn.hh_merchant_number\n                            and ds.activity_date between adj.batch_date-30 and adj.batch_date+7\n                            and ds.item_type = 5\n                            and ds.data_source = adj.load_filename\n                  )                  \n          order by adj.batch_date, adj.reference_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.MerchantStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,AchEntryData.ED_CHARGEBACK);
   __sJT_st.setLong(2,merchantNumber);
   __sJT_st.setDate(3,statementDate);
   __sJT_st.setString(4,MonthEndFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.reports.MerchantStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:631^9*/
        loadData(it, INFO_TRANSACTION);
        it.close();
        
        /*@lineinfo:generated-code*//*@lineinfo:635^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_char(ats.post_date_actual, 'Mon DD')  
//                                                      deposit_day,
//                    ats.trace_number                  reference_number,
//                    'T'                               deposit_pl,
//                    nvl(ats.sales_count,0)            sales_count,
//                    nvl(ats.sales_amount,0)           sales_amount,
//                    nvl(ats.credits_count,0)          credits_count,
//                    nvl(ats.credits_amount,0)         credits_amount,
//                    nvl(ats.daily_discount_amount,0)  discount_paid,
//                    ats.ach_amount*
//                      decode(tc.debit_credit_indicator,
//                      'D', -1,
//                      1)                              settled,
//                    nvl(ats.entry_description,'NULL') entry_description
//            from    monthly_extract_gn          gn,
//                    ach_trident_statement       ats,
//                    ach_detail_tran_code        tc
//            where   gn.hh_merchant_number = :merchantNumber
//                    and gn.hh_active_date = :statementDate
//                    and gn.load_filename = nvl(:MonthEndFilename,gn.load_filename)
//                    and ats.merchant_number = gn.hh_merchant_number
//                    and ats.post_date_actual between gn.hh_active_date and gn.hh_curr_date
//                    and ats.post_date >= (gn.hh_active_date-120)
//                    and ats.transaction_code = tc.ach_detail_trans_code(+)
//                    and nvl(ats.entry_description,'NULL') = 'MERCH ADJ'
//            order by
//                    ats.entry_description desc, ats.trace_number, ats.post_date asc        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  to_char(ats.post_date_actual, 'Mon DD')  \n                                                    deposit_day,\n                  ats.trace_number                  reference_number,\n                  'T'                               deposit_pl,\n                  nvl(ats.sales_count,0)            sales_count,\n                  nvl(ats.sales_amount,0)           sales_amount,\n                  nvl(ats.credits_count,0)          credits_count,\n                  nvl(ats.credits_amount,0)         credits_amount,\n                  nvl(ats.daily_discount_amount,0)  discount_paid,\n                  ats.ach_amount*\n                    decode(tc.debit_credit_indicator,\n                    'D', -1,\n                    1)                              settled,\n                  nvl(ats.entry_description,'NULL') entry_description\n          from    monthly_extract_gn          gn,\n                  ach_trident_statement       ats,\n                  ach_detail_tran_code        tc\n          where   gn.hh_merchant_number =  :1  \n                  and gn.hh_active_date =  :2  \n                  and gn.load_filename = nvl( :3  ,gn.load_filename)\n                  and ats.merchant_number = gn.hh_merchant_number\n                  and ats.post_date_actual between gn.hh_active_date and gn.hh_curr_date\n                  and ats.post_date >= (gn.hh_active_date-120)\n                  and ats.transaction_code = tc.ach_detail_trans_code(+)\n                  and nvl(ats.entry_description,'NULL') = 'MERCH ADJ'\n          order by\n                  ats.entry_description desc, ats.trace_number, ats.post_date asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.MerchantStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDate(2,statementDate);
   __sJT_st.setString(3,MonthEndFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.MerchantStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:664^9*/
        loadData(it, INFO_TRANSACTION);
        it.close();

        //******************************************************************
        //@+  Fix to display manual billing adjustments - April 2010 m/e
        //@+  Fix to display manual billing adjustments - July 2014 m/e
        Calendar cal = Calendar.getInstance();
        cal.set(2014, Calendar.JULY, 1);
        java.sql.Date  jul2014  = new java.sql.Date( cal.getTime().getTime() );
      
        if(statementDate.equals(jul2014)) {
        /*@lineinfo:generated-code*//*@lineinfo:676^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_char(bad.date_transmitted, 'Mon DD')     as deposit_day,
//                    'A'                                     		as tran_type,
//                    bad.trace_number                        		as reference_number,
//                    'T'                                     		as deposit_pl,
//                    decode(bad.credit_debit_ind,'C',1,0)    		as sales_count,
//                    ( decode(bad.credit_debit_ind,'C',1,0) *
//                      amount )                              		as sales_amount,
//                    decode(bad.credit_debit_ind,'C',0,1)    		as credits_count,
//                    ( decode(bad.credit_debit_ind,'C',0,1) *
//                      amount )                              		as credits_amount,
//                    0.0                                     		as discount_paid,
//                    ( decode(bad.credit_debit_ind,'C',1,-1) *
//                      abs(bad.amount) )                     		as settled,
//                      decode(entry_description, 'DUP RVRS D', 'MERCH DEP', 
//                                    'DUP RVRS C', 'MERCH DEP', entry_description)
//                                                            		as entry_description
//            from    bankserv_ach_detail     bad
//            where   bad.merchant_number = :merchantNumber
//                      -- and bad.created_by = 'jduncan'
//                      -- and trunc(bad.date_created) between :statementDate and last_day(:statementDate)
//                      and source_id between 71214 and 80114 
//                      and trunc(bad.date_created) between '12 jul 2014' and '31 jul 2014' 
//                      and bad.entry_description in ('DUP RVRS D', 'DUP RVRS C', 'MERCH CHBK')  
//              order by bad.date_transmitted, bad.trace_number
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  to_char(bad.date_transmitted, 'Mon DD')     as deposit_day,\n                  'A'                                     \t\tas tran_type,\n                  bad.trace_number                        \t\tas reference_number,\n                  'T'                                     \t\tas deposit_pl,\n                  decode(bad.credit_debit_ind,'C',1,0)    \t\tas sales_count,\n                  ( decode(bad.credit_debit_ind,'C',1,0) *\n                    amount )                              \t\tas sales_amount,\n                  decode(bad.credit_debit_ind,'C',0,1)    \t\tas credits_count,\n                  ( decode(bad.credit_debit_ind,'C',0,1) *\n                    amount )                              \t\tas credits_amount,\n                  0.0                                     \t\tas discount_paid,\n                  ( decode(bad.credit_debit_ind,'C',1,-1) *\n                    abs(bad.amount) )                     \t\tas settled,\n                    decode(entry_description, 'DUP RVRS D', 'MERCH DEP', \n                                  'DUP RVRS C', 'MERCH DEP', entry_description)\n                                                          \t\tas entry_description\n          from    bankserv_ach_detail     bad\n          where   bad.merchant_number =  :1  \n                    -- and bad.created_by = 'jduncan'\n                    -- and trunc(bad.date_created) between :statementDate and last_day(:statementDate)\n                    and source_id between 71214 and 80114 \n                    and trunc(bad.date_created) between '12 jul 2014' and '31 jul 2014' \n                    and bad.entry_description in ('DUP RVRS D', 'DUP RVRS C', 'MERCH CHBK')  \n            order by bad.date_transmitted, bad.trace_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.MerchantStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.MerchantStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:702^11*/
      } else {
        /*@lineinfo:generated-code*//*@lineinfo:704^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_char(bad.date_transmitted, 'Mon DD') as deposit_day,
//                      'A'                                     as tran_type,
//                      bad.trace_number                        as reference_number,
//                      'T'                                     as deposit_pl,
//                      decode(bad.credit_debit_ind,'C',1,0)    as sales_count,
//                      ( decode(bad.credit_debit_ind,'C',1,0) *
//                        amount )                              as sales_amount,
//                      decode(bad.credit_debit_ind,'C',0,1)    as credits_count,
//                      ( decode(bad.credit_debit_ind,'C',0,1) *
//                        amount )                              as credits_amount,
//                      0.0                                     as discount_paid,
//                      ( decode(bad.credit_debit_ind,'C',1,-1) *
//                        abs(bad.amount) )                     as settled,
//                      decode(entry_description, 'TRS-DB', 'MERCH DEP', 
//                                    'TRS-CR', 'MERCH DEP', entry_description)
//                                                              as entry_description
//              from    bankserv_ach_detail     bad
//              where   bad.merchant_number = :merchantNumber
//                      -- and bad.created_by = 'jduncan'
//                    and trunc(bad.date_created) between :statementDate and last_day(:statementDate)
//                      and bad.entry_description in ('TRS-CR', 'TRS-DB' )  
//            order by bad.date_transmitted, bad.trace_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  to_char(bad.date_transmitted, 'Mon DD') as deposit_day,\n                    'A'                                     as tran_type,\n                    bad.trace_number                        as reference_number,\n                    'T'                                     as deposit_pl,\n                    decode(bad.credit_debit_ind,'C',1,0)    as sales_count,\n                    ( decode(bad.credit_debit_ind,'C',1,0) *\n                      amount )                              as sales_amount,\n                    decode(bad.credit_debit_ind,'C',0,1)    as credits_count,\n                    ( decode(bad.credit_debit_ind,'C',0,1) *\n                      amount )                              as credits_amount,\n                    0.0                                     as discount_paid,\n                    ( decode(bad.credit_debit_ind,'C',1,-1) *\n                      abs(bad.amount) )                     as settled,\n                    decode(entry_description, 'TRS-DB', 'MERCH DEP', \n                                  'TRS-CR', 'MERCH DEP', entry_description)\n                                                            as entry_description\n            from    bankserv_ach_detail     bad\n            where   bad.merchant_number =  :1  \n                    -- and bad.created_by = 'jduncan'\n                  and trunc(bad.date_created) between  :2   and last_day( :3  )\n                    and bad.entry_description in ('TRS-CR', 'TRS-DB' )  \n          order by bad.date_transmitted, bad.trace_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.reports.MerchantStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDate(2,statementDate);
   __sJT_st.setDate(3,statementDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.reports.MerchantStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:728^9*/
      }
        loadData(it, INFO_TRANSACTION);
        it.close();
        //@-

        /*@lineinfo:generated-code*//*@lineinfo:734^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_char(ats.batch_date, 'Mon DD') deposit_day,
//                    ats.trace_number                  reference_number,
//                    'T'                               deposit_pl,
//                    nvl(ats.sales_count,0)            sales_count,
//                    nvl(ats.sales_amount,0)           sales_amount,
//                    nvl(ats.credits_count,0)          credits_count,
//                    nvl(ats.credits_amount,0)         credits_amount,
//                    ( nvl(ats.daily_discount_amount,0)
//                      + nvl(ats.daily_ic_amount,0) )  discount_paid,
//                    ats.ach_amount*
//                      decode(tc.debit_credit_indicator,
//                      'D', -1,
//                      1)                              settled,
//                    nvl(ats.entry_description,'NULL') entry_description
//            from    monthly_extract_gn          gn,
//                    ach_trident_statement       ats,
//                    ach_detail_tran_code        tc
//            where   gn.hh_merchant_number = :merchantNumber
//                    and gn.hh_active_date = :statementDate
//                    and gn.load_filename = nvl(:MonthEndFilename,gn.load_filename)
//                    and ats.merchant_number = gn.hh_merchant_number
//                    and ats.batch_date between (gn.hh_active_date-120) and (gn.hh_curr_date+30)
//                    and ats.post_date >= (gn.hh_active_date-120)
//                    and ats.transaction_code = tc.ach_detail_trans_code(+)
//                    and nvl(ats.entry_description,'NULL') in (:AchEntryData.ED_MERCH_DEP, :AchEntryData.ED_AMEX_DEP)
//                    and exists
//                    (
//                      select  /*+ index (ds idx_bds_load_file_id_mid) */
//                              ds.data_source
//                      from    mbs_daily_summary   ds
//                      where   ds.me_load_file_id = gn.load_file_id
//                              and ds.merchant_number = gn.hh_merchant_number
//                              and ds.activity_date = ats.batch_date
//                              and ds.data_source = ats.load_filename
//                    )                  
//            order by
//                    ats.batch_date, ats.trace_number, ats.entry_description desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  to_char(ats.batch_date, 'Mon DD') deposit_day,\n                  ats.trace_number                  reference_number,\n                  'T'                               deposit_pl,\n                  nvl(ats.sales_count,0)            sales_count,\n                  nvl(ats.sales_amount,0)           sales_amount,\n                  nvl(ats.credits_count,0)          credits_count,\n                  nvl(ats.credits_amount,0)         credits_amount,\n                  ( nvl(ats.daily_discount_amount,0)\n                    + nvl(ats.daily_ic_amount,0) )  discount_paid,\n                  ats.ach_amount*\n                    decode(tc.debit_credit_indicator,\n                    'D', -1,\n                    1)                              settled,\n                  nvl(ats.entry_description,'NULL') entry_description\n          from    monthly_extract_gn          gn,\n                  ach_trident_statement       ats,\n                  ach_detail_tran_code        tc\n          where   gn.hh_merchant_number =  :1  \n                  and gn.hh_active_date =  :2  \n                  and gn.load_filename = nvl( :3  ,gn.load_filename)\n                  and ats.merchant_number = gn.hh_merchant_number\n                  and ats.batch_date between (gn.hh_active_date-120) and (gn.hh_curr_date+30)\n                  and ats.post_date >= (gn.hh_active_date-120)\n                  and ats.transaction_code = tc.ach_detail_trans_code(+)\n                  and nvl(ats.entry_description,'NULL') in ( :4  ,  :5  )\n                  and exists\n                  (\n                    select  /*+ index (ds idx_bds_load_file_id_mid) */\n                            ds.data_source\n                    from    mbs_daily_summary   ds\n                    where   ds.me_load_file_id = gn.load_file_id\n                            and ds.merchant_number = gn.hh_merchant_number\n                            and ds.activity_date = ats.batch_date\n                            and ds.data_source = ats.load_filename\n                  )                  \n          order by\n                  ats.batch_date, ats.trace_number, ats.entry_description desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.MerchantStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDate(2,statementDate);
   __sJT_st.setString(3,MonthEndFilename);
   __sJT_st.setString(4,AchEntryData.ED_MERCH_DEP);
   __sJT_st.setString(5,AchEntryData.ED_AMEX_DEP);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.reports.MerchantStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:773^9*/
      }
      else    // legacy method, use dates only
      {
        java.sql.Date beginDate = null;
        /*@lineinfo:generated-code*//*@lineinfo:778^9*/

//  ************************************************************
//  #sql [Ctx] { select  decode(gnp.hh_curr_date,
//                           null, gn.hh_active_date,
//                           gnp.hh_curr_date )
//            
//            from    monthly_extract_gn      gn,
//                    monthly_extract_gn      gnp
//            where   gn.hh_merchant_number = :merchantNumber
//                    and gn.hh_active_date     = :statementDate
//                    and gn.load_filename = nvl(:MonthEndFilename,gn.load_filename)
//                    and gnp.hh_merchant_number(+) = gn.hh_merchant_number
//                    and gnp.hh_active_date(+) = trunc((:statementDate-15),'month')
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode(gnp.hh_curr_date,\n                         null, gn.hh_active_date,\n                         gnp.hh_curr_date )\n           \n          from    monthly_extract_gn      gn,\n                  monthly_extract_gn      gnp\n          where   gn.hh_merchant_number =  :1  \n                  and gn.hh_active_date     =  :2  \n                  and gn.load_filename = nvl( :3  ,gn.load_filename)\n                  and gnp.hh_merchant_number(+) = gn.hh_merchant_number\n                  and gnp.hh_active_date(+) = trunc(( :4  -15),'month')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.reports.MerchantStatementDetail",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDate(2,statementDate);
   __sJT_st.setString(3,MonthEndFilename);
   __sJT_st.setDate(4,statementDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:791^9*/

        /*@lineinfo:generated-code*//*@lineinfo:793^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  to_char(ats.post_date, 'DD')      deposit_day,
//                    ats.trace_number                  reference_number,
//                    'T'                               deposit_pl,
//                    nvl(ats.sales_count,0)            sales_count,
//                    nvl(ats.sales_amount,0)           sales_amount,
//                    nvl(ats.credits_count,0)          credits_count,
//                    nvl(ats.credits_amount,0)         credits_amount,
//                    nvl(ats.daily_discount_amount,0)  discount_paid,
//                    ats.ach_amount*
//                      decode(tc.debit_credit_indicator,
//                      'D', -1,
//                      1)                              settled,
//                    nvl(ats.entry_description,'NULL') entry_description
//            from    monthly_extract_gn        gn,
//                    ach_trident_statement     ats,
//                    ach_detail_tran_code      tc
//            where   gn.hh_merchant_number = :merchantNumber
//                    and gn.hh_active_date = :statementDate
//                    and gn.load_filename = nvl(:MonthEndFilename,gn.load_filename)
//                    and ats.merchant_number = gn.hh_merchant_number
//                    and
//                    (
//                      ats.post_date_actual is not null
//                      and
//                      ats.post_date_actual between (:beginDate+1) and gn.hh_curr_date
//                    )
//                    and ats.transaction_code = tc.ach_detail_trans_code(+)
//            order by
//                    ats.entry_description desc, ats.post_date asc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  to_char(ats.post_date, 'DD')      deposit_day,\n                  ats.trace_number                  reference_number,\n                  'T'                               deposit_pl,\n                  nvl(ats.sales_count,0)            sales_count,\n                  nvl(ats.sales_amount,0)           sales_amount,\n                  nvl(ats.credits_count,0)          credits_count,\n                  nvl(ats.credits_amount,0)         credits_amount,\n                  nvl(ats.daily_discount_amount,0)  discount_paid,\n                  ats.ach_amount*\n                    decode(tc.debit_credit_indicator,\n                    'D', -1,\n                    1)                              settled,\n                  nvl(ats.entry_description,'NULL') entry_description\n          from    monthly_extract_gn        gn,\n                  ach_trident_statement     ats,\n                  ach_detail_tran_code      tc\n          where   gn.hh_merchant_number =  :1  \n                  and gn.hh_active_date =  :2  \n                  and gn.load_filename = nvl( :3  ,gn.load_filename)\n                  and ats.merchant_number = gn.hh_merchant_number\n                  and\n                  (\n                    ats.post_date_actual is not null\n                    and\n                    ats.post_date_actual between ( :4  +1) and gn.hh_curr_date\n                  )\n                  and ats.transaction_code = tc.ach_detail_trans_code(+)\n          order by\n                  ats.entry_description desc, ats.post_date asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.reports.MerchantStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDate(2,statementDate);
   __sJT_st.setString(3,MonthEndFilename);
   __sJT_st.setDate(4,beginDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"14com.mes.reports.MerchantStatementDetail",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:824^9*/
      }

      loadData(it, INFO_TRANSACTION);
    }
    catch(Exception e)
    {
      throw e;
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
    }
  }

  private void generateCardInsertInfo()
  throws Exception
  {
    /*@lineinfo:generated-code*//*@lineinfo:842^5*/

//  ************************************************************
//  #sql [Ctx] { select
//               nvl( sum( decode(pl.pl_plan_type,'VISA',1,'VSBS',1,'VSDB',1,0) *
//                         pl.pl_number_of_sales ), 0 )                   as visa_sales_count,
//               nvl( sum( decode(pl.pl_plan_type,'VISA',1,'VSBS',1,'VSDB',1,0) *
//                         pl.pl_sales_amount ), 0 )                      as visa_sales_amount,
//               nvl( sum( decode(pl.pl_plan_type,'VISA',1,'VSBS',1,'VSDB',1,0) *
//                         pl.pl_number_of_credits ), 0 )                 as visa_credits_count,
//               nvl( sum( decode(pl.pl_plan_type,'VISA',1,'VSBS',1,'VSDB',1,0) *
//                         pl.pl_credits_amount ), 0 )                    as visa_credits_amount,
//               nvl( sum( decode(pl.pl_plan_type,'MC',1,'MCBS',1,'MCDB',1,0) *
//                         pl.pl_number_of_sales ), 0 )                   as mc_sales_count,
//               nvl( sum( decode(pl.pl_plan_type,'MC',1,'MCBS',1,'MCDB',1,0) *
//                         pl.pl_sales_amount ), 0 )                      as mc_sales_amount,
//               nvl( sum( decode(pl.pl_plan_type,'MC',1,'MCBS',1,'MCDB',1,0) *
//                         pl.pl_number_of_credits ), 0 )                 as mc_credits_count,
//               nvl( sum( decode(pl.pl_plan_type,'MC',1,'MCBS',1,'MCDB',1,0) *
//                         pl.pl_credits_amount ), 0 )                    as mc_credits_amount,
//               nvl( sum( decode(pl.pl_plan_type,'AM',1,0) *
//                         pl.pl_number_of_sales ), 0 )                   as amex_sales_count,
//               nvl( sum( decode(pl.pl_plan_type,'AMEX',1,0) *
//                         pl.pl_sales_amount ), 0 )                      as amex_sales_amount,
//               nvl( sum( decode(pl.pl_plan_type,'AMEX',1,0) *
//                         pl.pl_number_of_credits ), 0 )                 as amex_credits_count,
//               nvl( sum( decode(pl.pl_plan_type,'AMEX',1,0) *
//                         pl.pl_credits_amount ), 0 )                    as amex_credits_amount,
//               nvl( sum( decode(pl.pl_plan_type,'DC',1,0) *
//                         pl.pl_number_of_sales ), 0 )                   as dinr_sales_count,
//               nvl( sum( decode(pl.pl_plan_type,'DC',1,0) *
//                         pl.pl_sales_amount ), 0 )                      as dinr_sales_amount,
//               nvl( sum( decode(pl.pl_plan_type,'DC',1,0) *
//                         pl.pl_number_of_credits ), 0 )                 as dinr_credits_count,
//               nvl( sum( decode(pl.pl_plan_type,'DC',1,0) *
//                         pl.pl_credits_amount ), 0 )                    as dinr_credits_amount,
//               nvl( sum( decode(pl.pl_plan_type,'DS',1,0) *
//                         pl.pl_number_of_sales ), 0 )                   as disc_sales_count,
//               nvl( sum( decode(pl.pl_plan_type,'DS',1,0) *
//                         pl.pl_sales_amount ), 0 )                      as disc_sales_amount,
//               nvl( sum( decode(pl.pl_plan_type,'DS',1,0) *
//                         pl.pl_number_of_credits ), 0 )                 as disc_credits_count,
//               nvl( sum( decode(pl.pl_plan_type,'DS',1,0) *
//                         pl.pl_credits_amount ), 0 )                    as disc_credits_amount,
//               nvl( sum( decode(pl.pl_plan_type,'JCB',1,0) *
//                         pl.pl_number_of_sales ), 0 )                   as jcb_sales_count,
//               nvl( sum( decode(pl.pl_plan_type,'JCB',1,0) *
//                         pl.pl_sales_amount ), 0 )                      as jcb_sales_amount,
//               nvl( sum( decode(pl.pl_plan_type,'JCB',1,0) *
//                         pl.pl_number_of_credits ), 0 )                 as jcb_credits_count,
//               nvl( sum( decode(pl.pl_plan_type,'JCB',1,0) *
//                         pl.pl_credits_amount ), 0 )                    as jcb_credits_amount,
//               nvl( sum( decode(pl.pl_plan_type,'DEBC',1,0) *
//                         pl.pl_number_of_sales ), 0 )                   as debit_sales_count,
//               nvl( sum( decode(pl.pl_plan_type,'DEBC',1,0) *
//                         pl.pl_sales_amount ), 0 )                      as debit_sales_amount,
//               nvl( sum( decode(pl.pl_plan_type,'DEBC',1,0) *
//                         pl.pl_number_of_credits ), 0 )                 as debit_credits_count,
//               nvl( sum( decode(pl.pl_plan_type,'DEBC',1,0) *
//                         pl.pl_credits_amount ), 0 )                    as debit_credits_amount,
//               nvl( sum( decode(pl.pl_plan_type,'EBT',1,0) *
//                         pl.pl_number_of_sales ), 0 )                   as ebt_sales_count,
//               nvl( sum( decode(pl.pl_plan_type,'EBT',1,0) *
//                         pl.pl_sales_amount ), 0 )                      as ebt_sales_amount,
//               nvl( sum( decode(pl.pl_plan_type,'EBT',1,0) *
//                         pl.pl_number_of_credits ), 0 )                 as ebt_credits_count,
//               nvl( sum( decode(pl.pl_plan_type,'EBT',1,0) *
//                         pl.pl_credits_amount ), 0 )                    as ebt_credits_amount,
//               nvl( sum( decode(pl.pl_plan_type,'PL1',1,0) *
//                         pl.pl_number_of_sales ), 0 )                   as pl1_sales_count,
//               nvl( sum( decode(pl.pl_plan_type,'PL1',1,0) *
//                         pl.pl_sales_amount ), 0 )                      as pl1_sales_amount,
//               nvl( sum( decode(pl.pl_plan_type,'PL1',1,0) *
//                         pl.pl_number_of_credits ), 0 )                 as pl1_credits_count,
//               nvl( sum( decode(pl.pl_plan_type,'PL1',1,0) *
//                         pl.pl_credits_amount ), 0 )                    as pl1_credits_amount,
//               nvl( sum( decode(pl.pl_plan_type,'PL2',1,0) *
//                         pl.pl_number_of_sales ), 0 )                   as pl2_sales_count,
//               nvl( sum( decode(pl.pl_plan_type,'PL2',1,0) *
//                         pl.pl_sales_amount ), 0 )                      as pl2_sales_amount,
//               nvl( sum( decode(pl.pl_plan_type,'PL2',1,0) *
//                         pl.pl_number_of_credits ), 0 )                 as pl2_credits_count,
//               nvl( sum( decode(pl.pl_plan_type,'PL2',1,0) *
//                         pl.pl_credits_amount ), 0 )                    as pl2_credits_amount,
//               nvl( sum( decode(pl.pl_plan_type,'PL3',1,0) *
//                         pl.pl_number_of_sales ), 0 )                   as pl3_sales_count,
//               nvl( sum( decode(pl.pl_plan_type,'PL3',1,0) *
//                         pl.pl_sales_amount ), 0 )                      as pl3_sales_amount,
//               nvl( sum( decode(pl.pl_plan_type,'PL3',1,0) *
//                         pl.pl_number_of_credits ), 0 )                 as pl3_credits_count,
//               nvl( sum( decode(pl.pl_plan_type,'PL3',1,0) *
//                         pl.pl_credits_amount ), 0 )                    as pl3_credits_amount,
//               /*RS changes for LCR */
//                nvl( sum( decode(pl.pl_plan_type,'VP',1,0) *
//                       pl.pl_number_of_sales ), 0 )                     as vp_sales_count,   
//                nvl( sum( decode(pl.pl_plan_type,'VP',1,0) *
//                       pl.pl_sales_amount ), 0 )                       as vp_sales_amount, 
//                nvl( sum( decode(pl.pl_plan_type,'VP',1,0) *
//                       pl.pl_number_of_credits ), 0 )                  as vp_credits_count, 
//                nvl( sum( decode(pl.pl_plan_type,'VP',1,0) *
//                       pl.pl_credits_amount ), 0 )                     as vp_credits_amount,
//                nvl( sum( decode(pl.pl_plan_type,'MP',1,0) *
//                       pl.pl_number_of_sales ), 0 )                    as mp_sales_count,   
//                nvl( sum( decode(pl.pl_plan_type,'MP',1,0) *
//                       pl.pl_sales_amount ), 0 )                       as mp_sales_amount, 
//                nvl( sum( decode(pl.pl_plan_type,'MP',1,0) *
//                       pl.pl_number_of_credits ), 0 )                  as mp_credits_count, 
//                nvl( sum( decode(pl.pl_plan_type,'MP',1,0) *
//                       pl.pl_credits_amount ), 0 )                     as mp_credits_amount,                          
//               /*RS changes for ACH PS*/
//               nvl( sum( decode(pl.pl_plan_type,'AC',1,0) *
//                       pl.pl_number_of_sales ), 0 )                     as ac_sales_count,
//               nvl( sum( decode(pl.pl_plan_type,'AC',1,0) *
//                       pl.pl_sales_amount ), 0 )                        as ac_sales_amount,
//               nvl( sum( decode(pl.pl_plan_type,'AC',1,0) *
//                       pl.pl_number_of_credits ), 0 )                   as ac_credits_count,
//               nvl( sum( decode(pl.pl_plan_type,'AC',1,0) *
//                       pl.pl_credits_amount ), 0 )                      as ac_credits_amount
//         -- ACH
//        from    monthly_extract_gn gn,
//                monthly_extract_pl pl
//        where   gn.hh_merchant_number = :merchantNumber
//                and gn.hh_active_date = :statementDate
//                and gn.load_filename  = nvl(:MonthEndFilename,gn.load_filename)
//                and pl.hh_load_sec    = gn.hh_load_sec
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select\n             nvl( sum( decode(pl.pl_plan_type,'VISA',1,'VSBS',1,'VSDB',1,0) *\n                       pl.pl_number_of_sales ), 0 )                   as visa_sales_count,\n             nvl( sum( decode(pl.pl_plan_type,'VISA',1,'VSBS',1,'VSDB',1,0) *\n                       pl.pl_sales_amount ), 0 )                      as visa_sales_amount,\n             nvl( sum( decode(pl.pl_plan_type,'VISA',1,'VSBS',1,'VSDB',1,0) *\n                       pl.pl_number_of_credits ), 0 )                 as visa_credits_count,\n             nvl( sum( decode(pl.pl_plan_type,'VISA',1,'VSBS',1,'VSDB',1,0) *\n                       pl.pl_credits_amount ), 0 )                    as visa_credits_amount,\n             nvl( sum( decode(pl.pl_plan_type,'MC',1,'MCBS',1,'MCDB',1,0) *\n                       pl.pl_number_of_sales ), 0 )                   as mc_sales_count,\n             nvl( sum( decode(pl.pl_plan_type,'MC',1,'MCBS',1,'MCDB',1,0) *\n                       pl.pl_sales_amount ), 0 )                      as mc_sales_amount,\n             nvl( sum( decode(pl.pl_plan_type,'MC',1,'MCBS',1,'MCDB',1,0) *\n                       pl.pl_number_of_credits ), 0 )                 as mc_credits_count,\n             nvl( sum( decode(pl.pl_plan_type,'MC',1,'MCBS',1,'MCDB',1,0) *\n                       pl.pl_credits_amount ), 0 )                    as mc_credits_amount,\n             nvl( sum( decode(pl.pl_plan_type,'AM',1,0) *\n                       pl.pl_number_of_sales ), 0 )                   as amex_sales_count,\n             nvl( sum( decode(pl.pl_plan_type,'AMEX',1,0) *\n                       pl.pl_sales_amount ), 0 )                      as amex_sales_amount,\n             nvl( sum( decode(pl.pl_plan_type,'AMEX',1,0) *\n                       pl.pl_number_of_credits ), 0 )                 as amex_credits_count,\n             nvl( sum( decode(pl.pl_plan_type,'AMEX',1,0) *\n                       pl.pl_credits_amount ), 0 )                    as amex_credits_amount,\n             nvl( sum( decode(pl.pl_plan_type,'DC',1,0) *\n                       pl.pl_number_of_sales ), 0 )                   as dinr_sales_count,\n             nvl( sum( decode(pl.pl_plan_type,'DC',1,0) *\n                       pl.pl_sales_amount ), 0 )                      as dinr_sales_amount,\n             nvl( sum( decode(pl.pl_plan_type,'DC',1,0) *\n                       pl.pl_number_of_credits ), 0 )                 as dinr_credits_count,\n             nvl( sum( decode(pl.pl_plan_type,'DC',1,0) *\n                       pl.pl_credits_amount ), 0 )                    as dinr_credits_amount,\n             nvl( sum( decode(pl.pl_plan_type,'DS',1,0) *\n                       pl.pl_number_of_sales ), 0 )                   as disc_sales_count,\n             nvl( sum( decode(pl.pl_plan_type,'DS',1,0) *\n                       pl.pl_sales_amount ), 0 )                      as disc_sales_amount,\n             nvl( sum( decode(pl.pl_plan_type,'DS',1,0) *\n                       pl.pl_number_of_credits ), 0 )                 as disc_credits_count,\n             nvl( sum( decode(pl.pl_plan_type,'DS',1,0) *\n                       pl.pl_credits_amount ), 0 )                    as disc_credits_amount,\n             nvl( sum( decode(pl.pl_plan_type,'JCB',1,0) *\n                       pl.pl_number_of_sales ), 0 )                   as jcb_sales_count,\n             nvl( sum( decode(pl.pl_plan_type,'JCB',1,0) *\n                       pl.pl_sales_amount ), 0 )                      as jcb_sales_amount,\n             nvl( sum( decode(pl.pl_plan_type,'JCB',1,0) *\n                       pl.pl_number_of_credits ), 0 )                 as jcb_credits_count,\n             nvl( sum( decode(pl.pl_plan_type,'JCB',1,0) *\n                       pl.pl_credits_amount ), 0 )                    as jcb_credits_amount,\n             nvl( sum( decode(pl.pl_plan_type,'DEBC',1,0) *\n                       pl.pl_number_of_sales ), 0 )                   as debit_sales_count,\n             nvl( sum( decode(pl.pl_plan_type,'DEBC',1,0) *\n                       pl.pl_sales_amount ), 0 )                      as debit_sales_amount,\n             nvl( sum( decode(pl.pl_plan_type,'DEBC',1,0) *\n                       pl.pl_number_of_credits ), 0 )                 as debit_credits_count,\n             nvl( sum( decode(pl.pl_plan_type,'DEBC',1,0) *\n                       pl.pl_credits_amount ), 0 )                    as debit_credits_amount,\n             nvl( sum( decode(pl.pl_plan_type,'EBT',1,0) *\n                       pl.pl_number_of_sales ), 0 )                   as ebt_sales_count,\n             nvl( sum( decode(pl.pl_plan_type,'EBT',1,0) *\n                       pl.pl_sales_amount ), 0 )                      as ebt_sales_amount,\n             nvl( sum( decode(pl.pl_plan_type,'EBT',1,0) *\n                       pl.pl_number_of_credits ), 0 )                 as ebt_credits_count,\n             nvl( sum( decode(pl.pl_plan_type,'EBT',1,0) *\n                       pl.pl_credits_amount ), 0 )                    as ebt_credits_amount,\n             nvl( sum( decode(pl.pl_plan_type,'PL1',1,0) *\n                       pl.pl_number_of_sales ), 0 )                   as pl1_sales_count,\n             nvl( sum( decode(pl.pl_plan_type,'PL1',1,0) *\n                       pl.pl_sales_amount ), 0 )                      as pl1_sales_amount,\n             nvl( sum( decode(pl.pl_plan_type,'PL1',1,0) *\n                       pl.pl_number_of_credits ), 0 )                 as pl1_credits_count,\n             nvl( sum( decode(pl.pl_plan_type,'PL1',1,0) *\n                       pl.pl_credits_amount ), 0 )                    as pl1_credits_amount,\n             nvl( sum( decode(pl.pl_plan_type,'PL2',1,0) *\n                       pl.pl_number_of_sales ), 0 )                   as pl2_sales_count,\n             nvl( sum( decode(pl.pl_plan_type,'PL2',1,0) *\n                       pl.pl_sales_amount ), 0 )                      as pl2_sales_amount,\n             nvl( sum( decode(pl.pl_plan_type,'PL2',1,0) *\n                       pl.pl_number_of_credits ), 0 )                 as pl2_credits_count,\n             nvl( sum( decode(pl.pl_plan_type,'PL2',1,0) *\n                       pl.pl_credits_amount ), 0 )                    as pl2_credits_amount,\n             nvl( sum( decode(pl.pl_plan_type,'PL3',1,0) *\n                       pl.pl_number_of_sales ), 0 )                   as pl3_sales_count,\n             nvl( sum( decode(pl.pl_plan_type,'PL3',1,0) *\n                       pl.pl_sales_amount ), 0 )                      as pl3_sales_amount,\n             nvl( sum( decode(pl.pl_plan_type,'PL3',1,0) *\n                       pl.pl_number_of_credits ), 0 )                 as pl3_credits_count,\n             nvl( sum( decode(pl.pl_plan_type,'PL3',1,0) *\n                       pl.pl_credits_amount ), 0 )                    as pl3_credits_amount,\n             /*RS changes for LCR */\n              nvl( sum( decode(pl.pl_plan_type,'VP',1,0) *\n                     pl.pl_number_of_sales ), 0 )                     as vp_sales_count,   \n              nvl( sum( decode(pl.pl_plan_type,'VP',1,0) *\n                     pl.pl_sales_amount ), 0 )                       as vp_sales_amount, \n              nvl( sum( decode(pl.pl_plan_type,'VP',1,0) *\n                     pl.pl_number_of_credits ), 0 )                  as vp_credits_count, \n              nvl( sum( decode(pl.pl_plan_type,'VP',1,0) *\n                     pl.pl_credits_amount ), 0 )                     as vp_credits_amount,\n              nvl( sum( decode(pl.pl_plan_type,'MP',1,0) *\n                     pl.pl_number_of_sales ), 0 )                    as mp_sales_count,   \n              nvl( sum( decode(pl.pl_plan_type,'MP',1,0) *\n                     pl.pl_sales_amount ), 0 )                       as mp_sales_amount, \n              nvl( sum( decode(pl.pl_plan_type,'MP',1,0) *\n                     pl.pl_number_of_credits ), 0 )                  as mp_credits_count, \n              nvl( sum( decode(pl.pl_plan_type,'MP',1,0) *\n                     pl.pl_credits_amount ), 0 )                     as mp_credits_amount,                          \n             /*RS changes for ACH PS*/\n             nvl( sum( decode(pl.pl_plan_type,'AC',1,0) *\n                     pl.pl_number_of_sales ), 0 )                     as ac_sales_count,\n             nvl( sum( decode(pl.pl_plan_type,'AC',1,0) *\n                     pl.pl_sales_amount ), 0 )                        as ac_sales_amount,\n             nvl( sum( decode(pl.pl_plan_type,'AC',1,0) *\n                     pl.pl_number_of_credits ), 0 )                   as ac_credits_count,\n             nvl( sum( decode(pl.pl_plan_type,'AC',1,0) *\n                     pl.pl_credits_amount ), 0 )                      as ac_credits_amount\n        -- ACH\n      from    monthly_extract_gn gn,\n              monthly_extract_pl pl\n      where   gn.hh_merchant_number =  :1  \n              and gn.hh_active_date =  :2  \n              and gn.load_filename  = nvl( :3  ,gn.load_filename)\n              and pl.hh_load_sec    = gn.hh_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.reports.MerchantStatementDetail",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDate(2,statementDate);
   __sJT_st.setString(3,MonthEndFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 56) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(56,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sViCnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sViAmt = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cViCnt = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cViAmt = __sJT_rs.getDouble(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sMcCnt = __sJT_rs.getInt(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sMcAmt = __sJT_rs.getDouble(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cMcCnt = __sJT_rs.getInt(7); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cMcAmt = __sJT_rs.getDouble(8); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sAxCnt = __sJT_rs.getInt(9); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sAxAmt = __sJT_rs.getDouble(10); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cAxCnt = __sJT_rs.getInt(11); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cAxAmt = __sJT_rs.getDouble(12); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDiCnt = __sJT_rs.getInt(13); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDiAmt = __sJT_rs.getDouble(14); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDiCnt = __sJT_rs.getInt(15); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDiAmt = __sJT_rs.getDouble(16); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDsCnt = __sJT_rs.getInt(17); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDsAmt = __sJT_rs.getDouble(18); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDsCnt = __sJT_rs.getInt(19); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDsAmt = __sJT_rs.getDouble(20); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sJcCnt = __sJT_rs.getInt(21); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sJcAmt = __sJT_rs.getDouble(22); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cJcCnt = __sJT_rs.getInt(23); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cJcAmt = __sJT_rs.getDouble(24); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDbCnt = __sJT_rs.getInt(25); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDbAmt = __sJT_rs.getDouble(26); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDbCnt = __sJT_rs.getInt(27); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDbAmt = __sJT_rs.getDouble(28); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sEbCnt = __sJT_rs.getInt(29); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sEbAmt = __sJT_rs.getDouble(30); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cEbCnt = __sJT_rs.getInt(31); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cEbAmt = __sJT_rs.getDouble(32); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sP1Cnt = __sJT_rs.getInt(33); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sP1Amt = __sJT_rs.getDouble(34); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cP1Cnt = __sJT_rs.getInt(35); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cP1Amt = __sJT_rs.getDouble(36); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sP2Cnt = __sJT_rs.getInt(37); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sP2Amt = __sJT_rs.getDouble(38); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cP2Cnt = __sJT_rs.getInt(39); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cP2Amt = __sJT_rs.getDouble(40); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sP3Cnt = __sJT_rs.getInt(41); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sP3Amt = __sJT_rs.getDouble(42); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cP3Cnt = __sJT_rs.getInt(43); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cP3Amt = __sJT_rs.getDouble(44); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vpSalesCnt = __sJT_rs.getInt(45); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vpSalesAmt = __sJT_rs.getDouble(46); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vpCreditCnt = __sJT_rs.getInt(47); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   vpCreditAmt = __sJT_rs.getDouble(48); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mpSalesCnt = __sJT_rs.getInt(49); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mpSalesAmt = __sJT_rs.getDouble(50); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mpCreditCnt = __sJT_rs.getInt(51); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   mpCreditAmt = __sJT_rs.getDouble(52); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sAchCnt = __sJT_rs.getInt(53); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sAchAmt = __sJT_rs.getDouble(54); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cAchCnt = __sJT_rs.getInt(55); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cAchAmt = __sJT_rs.getDouble(56); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:980^5*/

    // Amex, Diners, Discover and JCB are no longer included
    // in the PL table.  Need to pull the totals from the batch
    // summary report data because it has the T&E card types
    /*@lineinfo:generated-code*//*@lineinfo:985^5*/

//  ************************************************************
//  #sql [Ctx] { select  nvl( sum( nvl(sm.amex_sales_count     ,0) ),0 ) as amex_sales_count,
//                nvl( sum( nvl(sm.amex_sales_amount    ,0) ),0 ) as amex_sales_amount,
//                nvl( sum( nvl(sm.amex_credits_count   ,0) ),0 ) as amex_credits_count,
//                nvl( sum( nvl(sm.amex_credits_amount  ,0) ),0 ) as amex_credits_amount,
//                nvl( sum( nvl(sm.diners_sales_count   ,0) ),0 ) as dinr_sales_count,
//                nvl( sum( nvl(sm.diners_sales_amount  ,0) ),0 ) as dinr_sales_amount,
//                nvl( sum( nvl(sm.diners_credits_count ,0) ),0 ) as dinr_credits_count,
//                nvl( sum( nvl(sm.diners_credits_amount,0) ),0 ) as dinr_credits_amount,
//                nvl( sum( nvl(sm.disc_sales_count     ,0) ),0 ) as disc_sales_count,
//                nvl( sum( nvl(sm.disc_sales_amount    ,0) ),0 ) as disc_sales_amount,
//                nvl( sum( nvl(sm.disc_credits_count   ,0) ),0 ) as disc_credits_count,
//                nvl( sum( nvl(sm.disc_credits_amount  ,0) ),0 ) as disc_credits_amount,
//                nvl( sum( nvl(sm.jcb_sales_count      ,0) ),0 ) as jcb_sales_count,
//                nvl( sum( nvl(sm.jcb_sales_amount     ,0) ),0 ) as jcb_sales_amount,
//                nvl( sum( nvl(sm.jcb_credits_count    ,0) ),0 ) as jcb_credits_count,
//                nvl( sum( nvl(sm.jcb_credits_amount   ,0) ),0 ) as jcb_credits_amount
//            -- JCB
//        from    daily_detail_file_ext_summary    sm
//        where   sm.merchant_number = :merchantNumber and
//                sm.batch_date between :statementDate and last_day(:statementDate)
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl( sum( nvl(sm.amex_sales_count     ,0) ),0 ) as amex_sales_count,\n              nvl( sum( nvl(sm.amex_sales_amount    ,0) ),0 ) as amex_sales_amount,\n              nvl( sum( nvl(sm.amex_credits_count   ,0) ),0 ) as amex_credits_count,\n              nvl( sum( nvl(sm.amex_credits_amount  ,0) ),0 ) as amex_credits_amount,\n              nvl( sum( nvl(sm.diners_sales_count   ,0) ),0 ) as dinr_sales_count,\n              nvl( sum( nvl(sm.diners_sales_amount  ,0) ),0 ) as dinr_sales_amount,\n              nvl( sum( nvl(sm.diners_credits_count ,0) ),0 ) as dinr_credits_count,\n              nvl( sum( nvl(sm.diners_credits_amount,0) ),0 ) as dinr_credits_amount,\n              nvl( sum( nvl(sm.disc_sales_count     ,0) ),0 ) as disc_sales_count,\n              nvl( sum( nvl(sm.disc_sales_amount    ,0) ),0 ) as disc_sales_amount,\n              nvl( sum( nvl(sm.disc_credits_count   ,0) ),0 ) as disc_credits_count,\n              nvl( sum( nvl(sm.disc_credits_amount  ,0) ),0 ) as disc_credits_amount,\n              nvl( sum( nvl(sm.jcb_sales_count      ,0) ),0 ) as jcb_sales_count,\n              nvl( sum( nvl(sm.jcb_sales_amount     ,0) ),0 ) as jcb_sales_amount,\n              nvl( sum( nvl(sm.jcb_credits_count    ,0) ),0 ) as jcb_credits_count,\n              nvl( sum( nvl(sm.jcb_credits_amount   ,0) ),0 ) as jcb_credits_amount\n           -- JCB\n      from    daily_detail_file_ext_summary    sm\n      where   sm.merchant_number =  :1   and\n              sm.batch_date between  :2   and last_day( :3  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.reports.MerchantStatementDetail",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDate(2,statementDate);
   __sJT_st.setDate(3,statementDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 16) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(16,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sAxCnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sAxAmt = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cAxCnt = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cAxAmt = __sJT_rs.getDouble(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDiCnt = __sJT_rs.getInt(5); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDiAmt = __sJT_rs.getDouble(6); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDiCnt = __sJT_rs.getInt(7); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDiAmt = __sJT_rs.getDouble(8); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDsCnt = __sJT_rs.getInt(9); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sDsAmt = __sJT_rs.getDouble(10); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDsCnt = __sJT_rs.getInt(11); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cDsAmt = __sJT_rs.getDouble(12); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sJcCnt = __sJT_rs.getInt(13); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sJcAmt = __sJT_rs.getDouble(14); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cJcCnt = __sJT_rs.getInt(15); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cJcAmt = __sJT_rs.getDouble(16); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1010^5*/
    
    // PROD-781
    /*@lineinfo:generated-code*//*@lineinfo:1013^5*/

//  ************************************************************
//  #sql [Ctx] { select  nvl( sum( nvl(sm.amex_sales_count     ,0) ),0 ) as amex_sales_count,
//                nvl( sum( nvl(sm.amex_sales_amount    ,0) ),0 ) as amex_sales_amount,
//                nvl( sum( nvl(sm.amex_credits_count   ,0) ),0 ) as amex_credits_count,
//                nvl( sum( nvl(sm.amex_credits_amount  ,0) ),0 ) as amex_credits_amount              
//           -- Amex      
//        from    daily_detail_file_ext_summary    sm,
//                mif mf
//        where   sm.merchant_number = :merchantNumber and
//                mf.merchant_number = sm.merchant_number and
//                sm.batch_date between (:statementDate - mes_amex_suspended_days) and (last_day(:statementDate) - mes_amex_suspended_days)              
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl( sum( nvl(sm.amex_sales_count     ,0) ),0 ) as amex_sales_count,\n              nvl( sum( nvl(sm.amex_sales_amount    ,0) ),0 ) as amex_sales_amount,\n              nvl( sum( nvl(sm.amex_credits_count   ,0) ),0 ) as amex_credits_count,\n              nvl( sum( nvl(sm.amex_credits_amount  ,0) ),0 ) as amex_credits_amount              \n          -- Amex      \n      from    daily_detail_file_ext_summary    sm,\n              mif mf\n      where   sm.merchant_number =  :1   and\n              mf.merchant_number = sm.merchant_number and\n              sm.batch_date between ( :2   - mes_amex_suspended_days) and (last_day( :3  ) - mes_amex_suspended_days)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.reports.MerchantStatementDetail",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDate(2,statementDate);
   __sJT_st.setDate(3,statementDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 4) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(4,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sAxCnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   sAxAmt = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cAxCnt = __sJT_rs.getInt(3); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   cAxAmt = __sJT_rs.getDouble(4); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1025^5*/
  }

  private void decodeAndAdd(InfoTransaction trans)
  {

    String entryDesc = trans.entryDescription;

    //deposit
    if( entryDesc.equals(AchEntryData.ED_MERCH_DEP) ||
        entryDesc.equals(AchEntryData.ED_AMEX_DEP) ||
        entryDesc.equals(AchEntryData.ED_DAILY_DISCOUNT) ||
        entryDesc.equals(AchEntryData.ED_DAILY_IC)
      )
    {
      trans.transType = "D";
      trans.xmlTitle = "deposit";
      deposits.add(trans);

      dSalesNumberTotal     += trans.salesCount;
      dSalesDollarTotal     += trans.salesAmount;
      dCreditNumberTotal    += trans.creditsCount;
      dCreditDollarTotal    += trans.creditsAmount;
      dDiscountPaid         += trans.discountPaid;
      dSettledTotal         += (trans.salesAmount - trans.creditsAmount - trans.discountPaid);

    }

    //chargeback
    if( (AchEntryData.ED_CHARGEBACK).equals(entryDesc) )
    {
      trans.xmlTitle = "chargeback";
      chargebacks.add(trans);

      cSalesNumberTotal     += trans.salesCount;
      cSalesDollarTotal     += trans.salesAmount;
      cCreditNumberTotal    += trans.creditsCount;
      cCreditDollarTotal    += trans.creditsAmount;
      cDiscountTotal        += trans.discountPaid;
      cSettledTotal         += (-trans.salesAmount + trans.creditsAmount - trans.discountPaid);
    }

    //adjustment
    if( (AchEntryData.ED_ADJUSTMENTS).equals(entryDesc) )
    {
      trans.transType = "A";
      trans.xmlTitle = "adjustment";
      adjustments.add(trans);

      aSalesNumberTotal     += trans.salesCount;
      aSalesDollarTotal     += trans.salesAmount;
      aCreditNumberTotal    += trans.creditsCount;
      aCreditDollarTotal    += trans.creditsAmount;
      aDiscountTotal        += trans.discountPaid;
      aSettledTotal         += (trans.salesAmount - trans.creditsAmount - trans.discountPaid);
    }

    //TODO
    //not found?? NULL - separate list?

  }

  private void decodeAndAdd(InfoPlan plan)
  {

    String pType = plan.planType;
    /*
    [16:02] bobswansen: PL_PLAN_TYPEs unaccounted for
    MC$
    MLGT
    VLGT
    VS$
    */
    if (pType.equals("VISA") ||
        pType.equals("VSBS") ||
        pType.equals("VSDB"))
    {

      dViAmt += plan.discountDue;
    }
    else if(pType.equals("MC") ||
            pType.equals("MCBS") ||
            pType.equals("MCDB" ))
    {
      dMcAmt += plan.discountDue;
    }
    else if(pType.equals("AMEX"))
    {
      dAxAmt += plan.discountDue;
    }
    else if(pType.equals("EBT"))
    {
      dEbAmt += plan.discountDue;
    }
    else if(pType.equals("JCB"))
    {
      dJcAmt += plan.discountDue;
    }
    else if(pType.equals("DEBC"))
    {
      dDbAmt += plan.discountDue;
    }
    else if(pType.equals("DC"))
    {
      dDiAmt += plan.discountDue;
    }
    else if(pType.equals("DS"))
    {
      dDsAmt += plan.discountDue;
    }
    else if(pType.equals("PL1"))
    {
      dP1Amt += plan.discountDue;
    }
    else if(pType.equals("PL2"))
    {
      dP2Amt += plan.discountDue;
    }
    else if(pType.equals("PL3"))
    {
      dP3Amt += plan.discountDue;
    }
    /* Code modified by RS for ACHPS implementation */
    else if(pType.equals("ACH")){
      dAchAmt +=  plan.discountDue;
    }
    /* Code modified by RS for LCR implementation */
	else if (pType.equals("MP")) {
	  mpDiscAmt += plan.discountDue;
	}
	/* Code modified by RS for LCR implementation */
	else if (pType.equals("VP")) {
	  vpDiscAmt += plan.discountDue;
	}
  }

  private void loadData (ResultSetIterator it, int type)
    throws Exception
  {
    //get result set
    ResultSet rs = it.getResultSet();

    try
    {
      //TODO
      switch(type)
      {
        case INFO_MERCHANT:

          //if there is valid rs data, it affects the whole Detail
          //otherwise, this merchant is invalid, and won't be included
          if(rs.next())
          {
            merchDetails = new InfoMerchant();
            merchDetails.load(rs);
            isValid = true;
          }

          break;

        case INFO_PLAN:
          while(rs.next())
          {
            InfoPlan plan = new InfoPlan();
            plan.load(rs);

            String    cPlanType = rs.getString("combine_plan");
            InfoPlan  cPlan     = (InfoPlan)plans.get(cPlanType);

            if ( cPlan == null )
            {
              plans.put(cPlanType,plan);
            }
            else    // combine code exists
            {
              // if the rate changed, add a new entry under the full plan type
              if ( plan.discountRate != cPlan.discountRate ||
                   plan.perItem      != cPlan.perItem      )
              {
                plans.put((rs.getString("plan_type") + "/" + plan.discountRate + "/" + plan.perItem),plan);
              }
              else
              {
                cPlan.addVolume(plan);
              }
            }

            //update running totals
            pSalesNumberTotal += plan.salesCount;
            pSalesDollarTotal += plan.saleAmount;
            pCreditNumberTotal+= plan.creditCount;
            pCreditDollarTotal+= plan.creditAmount;
            pNetSalesTotal    += plan.netSalesAmount;
            pDiscountTotal    += plan.discountDue;

            decodeAndAdd(plan);
          }

          pAveTicketTotal = pSalesDollarTotal / (pSalesNumberTotal!=0?pSalesNumberTotal:1);
          break;

        case INFO_FEE:
          while(rs.next())
          {
            InfoFee fee = new InfoFee();
            fee.load(rs);
            fees.add(fee);

            //update running total
            fFeeTotal += fee.itemTotal;
          }
          break;

        case INFO_TRANSACTION:
          while(rs.next())
          {
            InfoTransaction trans = new InfoTransaction();
            trans.load(rs);
            //this method defines the transType and adds the InfoTransaction
            //to the appropriate list
            decodeAndAdd(trans);
          }
          break;

        case INFO_RETURN_ADDRESS:
          if( rs.next() )
          {
            merchDetails.loadReturnAddr( rs );
          }
          break;

        case INFO_STMT_MSG:
          if( rs.next() )
          {
            //grab first available statement message
            stmtMsg = rs.getString("statement_msg");
          }
          log.debug(stmtMsg);
          break;

        default:
          throw new Exception("Data type not found - cannot process merchant");
      }
    }
    catch(Exception e)
    {
      throw e;
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
    }
  }

  public String getMonthEndFilename()
  {
    return( MonthEndFilename );
  }

  public void setMonthEndFilename( String value )
  {
    MonthEndFilename = value;
  }

  public String getFileName()
  {
    return ""+merchantNumber;
  }

  //For INFO_MERCHANT
  public class InfoMerchant implements InfoLoadable
  {
    public String yearMonth;      //year_month
    public String processMonth;   //processing_month
    public String merchantNumber; //merchant_number,
    public String association;    //association,
    public int assocNode;
    public String dbaName;        //dba_name,
    public String dda;            //dda,
    public String transitRouting; //transit_routing,
    public String addyName;       //address_name,
    public String addyLine1;      //address_line_1,
    public String addyLine2;      //address_line_2,
    public String addyCity;       //address_city,
    public String addyState;      //address_state,
    public String addyZip;        //address_zip

    public String remitStatus           = "false";    //remit_status

    public String   returnAddrName      = "";
    public String[] returnAddrLines     = new String[4];
    public String   returnAddrPhone     = "";
    public String   returnAddrHelpName  = "";


    public String getProcessMonth() {  return       processMonth;}
    public String getMerchantNumber() {  return     merchantNumber;}
    public String getAssociation() {  return        association;}
    public String getDbaName() {  return            dbaName;}
    public String getDda() {  return                dda;}
    public String getTransitRouting() {  return     transitRouting;}
    public String getAddyName() {  return           addyName;}
    public String getAddyLine1() {  return          addyLine1;}
    public String getAddyLine2() {  return          addyLine2;}
    public String getAddyCity() {  return           addyCity;}
    public String getAddyState() {  return          addyState;}
    public String getAddyZip() {  return            addyZip;}
    
    public boolean hasAddyLine1() { return ( addyLine1 != null && !"".equals(addyLine1.trim()) ); }
    public boolean hasAddyLine2() { return ( addyLine2 != null && !"".equals(addyLine2.trim()) ); }

    public void load(ResultSet rs)
      throws Exception
    {
      yearMonth=       rs.getString("year_month");
      processMonth=    rs.getString("processing_month");
      merchantNumber=  rs.getString("merchant_number");
      association=     rs.getString("association");
      assocNode=       rs.getInt("association");
      dbaName=         rs.getString("dba_name");
      dda=             rs.getString("dda");
      transitRouting=  rs.getString("transit_routing");
      addyName=        rs.getString("address_name");
      addyLine1=       rs.getString("address_line_1");
      addyLine2=       rs.getString("address_line_2");
      addyCity=        rs.getString("address_city");
      addyState=       rs.getString("address_state");
      addyZip=         rs.getString("address_zip");
      remitStatus=     rs.getString("remit_status");

      if( addyLine1==null || addyLine1.equals("null"))
      {
        addyLine1 = "";
      }
      if( addyLine2==null || addyLine2.equals("null"))
      {
        addyLine2 = "";
      }
    }

    public void loadReturnAddr( ResultSet rs )
      throws Exception
    {
      returnAddrName      = rs.getString("return_addr_name");
      for( int i = 0; i < returnAddrLines.length; ++i )
      {
        returnAddrLines[i] = rs.getString("return_addr_" + (i+1));
      }
      returnAddrPhone     = rs.getString("return_addr_phone");
      returnAddrHelpName  = rs.getString("return_addr_help_name");
    }

    public void genXML(XMLGen gen)
    {
      gen.openTag("merchantInfo");
      gen.genXML("processMonth",processMonth                );
      gen.genXML("merchantNumber",merchantNumber            );
      gen.genXML("association",association                  );
      gen.genXML("dbaName",dbaName                          );
      gen.genXML("dda",dda                                  );
      gen.genXML("transitRouting",transitRouting            );
      gen.genXML("addyName",addyName                        );
      gen.genXML("addyLine1",addyLine1==null?"":addyLine1   );
      gen.genXML("addyLine2",addyLine2==null?"":addyLine2   );
      gen.genXML("addyCity",addyCity                        );
      gen.genXML("addyState",addyState                      );
      gen.genXML("addyZip",addyZip                          );
      gen.genXML("remitStatus",remitStatus                  );
      gen.closeTag("merchantInfo");
    }
  }

  //For INFO_PLAN
  public class InfoPlan
    implements InfoLoadable, Comparable
  {
    private int sortOrder = 0;
    public String planType;       //plan_type,
    public int    salesCount;        //sales_count,
    public double saleAmount;     //sales_amount,
    public int    creditCount;       //credits_count,
    public double creditAmount;   //credits_amount,
    public double netSalesAmount; //net_sales_amount,
    public double discountRate;   //discount_rate,
    public double perItem;        //per_item,
    public double aveTicket;      //average_ticket,
    public double discountDue;    //discount_due
/*
    public String getPlanType() {  return           planType;}
    public int    getSalesCount() {  return         salesCount;}
    public double getSaleAmount() {  return         saleAmount;}
    public int    getCreditCount() {  return        creditCount;}
    public double getCreditAmount() {  return       creditAmount;}
    public double getNetSalesAmount() {  return     netSalesAmount;}
    public double getDiscountRate() {  return       discountRate;}
    public double getPerItem() {  return            perItem;}
    public double getAveTicket() {  return          aveTicket;}
    public double getDiscountDue() {  return        discountDue;}
*/

    public void load(ResultSet rs)
      throws Exception
    {
      sortOrder=      rs.getInt("sort_order");
      planType=       rs.getString("plan_type");
      salesCount=     rs.getInt("sales_count");
      saleAmount=     rs.getDouble("sales_amount");
      creditCount=    rs.getInt("credits_count");
      creditAmount=   rs.getDouble("credits_amount");
      netSalesAmount= rs.getDouble("net_sales_amount");
      discountRate=   rs.getDouble("discount_rate");
      perItem=        rs.getDouble("per_item");
      discountDue=    rs.getDouble("discount_due");
      aveTicket =     rs.getDouble("average_ticket");
    }

    public void genXML(XMLGen gen)
    {
      gen.openTag("infoPlan");
      gen.genXML("planType",planType      );
      gen.genXML("salesCount",salesCount    );
      gen.genXML("saleAmount",format(saleAmount    ));
      gen.genXML("creditCount",creditCount   );
      gen.genXML("creditAmount",format(creditAmount  ));
      gen.genXML("netSalesAmount",format(netSalesAmount));
      gen.genXML("discountRate",format(discountRate  ));
      gen.genXML("perItem",  format(perItem       ));
      gen.genXML("aveTicket",format(aveTicket     ));
      gen.genXML("discountDue",format(discountDue ));
      gen.closeTag("infoPlan");
    }

    public void addVolume(InfoPlan plan)
      throws Exception
    {
      salesCount      += plan.salesCount;
      saleAmount      += plan.saleAmount;
      creditCount     += plan.creditCount;
      creditAmount    += plan.creditAmount;
      netSalesAmount  += plan.netSalesAmount;
      discountDue     += plan.discountDue;
      aveTicket       =  ((salesCount == 0) ? 0 : (saleAmount/salesCount));
    }

    public int compareTo( Object compareObj )
    {
      InfoPlan          compare = (InfoPlan) compareObj;
      int               retVal  = 0;

      if ( (retVal = (int)(sortOrder - compare.sortOrder)) == 0 )
      {
        if ( (retVal = planType.compareTo(compare.planType)) == 0 )
        {
          if ( (retVal = (int)((discountRate - compare.discountRate)*10000)) == 0 )
          {
            retVal = (int)((perItem - compare.perItem)*10000);
          }
        }
      }
      return(retVal);
    }

  }

  //For INFO_FEE
  public class InfoFee implements InfoLoadable
  {
    public int    itemNumber;     //item_number,
    public double itemAmount;     //item_amount,
    public String itemDescription;//item_description,
    public double itemTotal;      //item_total

    public void load(ResultSet rs)
      throws Exception
    {
      itemNumber=       rs.getInt("item_number");
      itemAmount=       rs.getDouble("item_amount");
      itemDescription=  rs.getString("item_description");
      itemTotal=        rs.getDouble("item_total");
    }
    public void genXML(XMLGen gen)
    {
      gen.openTag("infoFee");
      gen.genXML("itemNumber",itemNumber     );
      gen.genXML("itemAmount",format(itemAmount     ));
      gen.genXML("itemDescription",itemDescription);
      gen.genXML("itemTotal",format(itemTotal      ));
      gen.closeTag("infoFee");
    }
  }

  //For INFO_TRANSACTION
  public class InfoTransaction implements InfoLoadable
  {
    public String depositDay;         //deposit_day,
    public String refNumber;          //reference_number,
    public String transType = null;
    public String entryDescription;   //entry_description
    public String depositPl;          //deposit_pl,
    public int    salesCount;         //sales_count,
    public double salesAmount;        //sales_amount,
    public int    creditsCount;       //credits_count,
    public double creditsAmount;      //credits_amount,
    public double discountPaid;       //discount_paid,

    public String xmlTitle = "void";
/*
    public String getDepositDay() {  return           depositDay;}
    public String getRefNumber() {  return            refNumber;}
    public String getTransType() {  return            transType;}
    public String getEntryDescription() {  return     entryDescription;}
    public String getDepositPl() {  return            depositPl;}
    public int    getSalesCount() {  return           salesCount;}
    public double getSalesAmount() {  return          salesAmount;}
    public int    getCreditsCount() {  return         creditsCount;}
    public double getCreditsAmount() {  return        creditsAmount;}
    public double getDiscountPaid() {  return         discountPaid;}

    public String getXmlTitle() {  return             xmlTitle;}
*/
    public double getSettledAmount()
    {
      double  retVal    = 0.0;

      if ( (AchEntryData.ED_CHARGEBACK).equals(this.entryDescription) )
      {
        // for chargebacks "sales" are debits to merchant and "credits" are credits to merchant
        retVal = (-this.salesAmount + this.creditsAmount - this.discountPaid);
      }
      else    // default is sales - credits - discount
      {
        retVal = (this.salesAmount - this.creditsAmount - this.discountPaid);
      }
      return( retVal );
    }

    public void load(ResultSet rs)
      throws Exception
    {
      depositDay=       rs.getString("deposit_day");
      refNumber=        rs.getString("reference_number");
      entryDescription= rs.getString("entry_description");
      depositPl=        rs.getString("deposit_pl");
      salesCount=       rs.getInt("sales_count");
      salesAmount=      rs.getDouble("sales_amount");
      creditsCount=     rs.getInt("credits_count");
      creditsAmount=    rs.getDouble("credits_amount");
      discountPaid=     rs.getDouble("discount_paid");

      try
      {
        transType = rs.getString("tran_type");
      }
      catch( Exception ee )
      {
        // ignore, transType will be set based on entry desc in decodeAndAdd(..)
      }
    }

    public void genXML(XMLGen gen)
    {
      gen.openTag(xmlTitle);
      gen.genXML("depositDay",depositDay      );
      gen.genXML("refNumber",refNumber       );
      gen.genXML("transType",transType);
      gen.genXML("entryDescription",entryDescription);
      gen.genXML("depositPl",depositPl       );
      gen.genXML("salesCount",salesCount      );
      gen.genXML("salesAmount",format(salesAmount     ));
      gen.genXML("creditsCount",creditsCount    );
      gen.genXML("creditsAmount",format(creditsAmount   ));
      gen.genXML("discountPaid",format(discountPaid    ));
      gen.closeTag(xmlTitle);
    }
  }

  public interface InfoLoadable
  {
    public void load(ResultSet rs) throws Exception;
    public void genXML(XMLGen gen);
  }

  //generates Source for translation
  public Source getSource()
  {
    return new SAXSource(new StatementXMLReader(),
                         new MerchStatementInputSource(this));
  }

  public boolean saveToFile( byte[] statementData )
  {
    String    filename  = null;
    boolean   retVal    = false;
    String    yearMonth = "";

    try
    {
      yearMonth = DateTimeFormatter.getFormattedDate(statementDate,"MMMyyyy");
      filename  = merchantNumber + "_" + yearMonth + ".pdf";

      System.out.println("Outputting statement to file " + filename);
      BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(filename,false));
      out.write(statementData,0,statementData.length);
      out.close();

      retVal = true;
    }
    catch( Exception e )
    {
      logEntry("saveToFile(" + merchantNumber + "," + yearMonth + ")",e.toString());
    }
    finally
    {
    }
    return( retVal );
  }

  //public boolean saveForWeb (int recNum, String loadFilename, OutputStream statementData )
  public boolean saveForWeb (int recNum, String loadFilename, byte[] statementData )
  //public OutputStream saveForWeb()
  {
    boolean retVal = false;

    //for testing
    /*
    for(Iterator it = plans.iterator(); it.hasNext();)
    {
      InfoPlan plan = (InfoPlan)it.next();
      System.out.println("planType = "+plan.planType+" :: discount = "+plan.discountDue);
    }

    System.out.println("merchantNumber = "+merchantNumber);
    System.out.println("mystery id = "+"0");
    System.out.println("processMonth = "+merchDetails.processMonth );
    System.out.println("dda = "+merchDetails.dda);
    System.out.println("transitRouting = "+merchDetails.transitRouting);
    System.out.println("LOAD_FILENAME = "+ loadFilename);
    System.out.println("orgDetail.getId() = "+orgDetail.getId());
    System.out.println("pDiscountTotal = "+pDiscountTotal);
    System.out.println("fFeeTotal = "+fFeeTotal);
    System.out.println("statementData = "+ statementData.length);
    System.out.println("-----------------------------------");
    */

    System.out.println("merchantNumber = "+merchantNumber);
    System.out.println("statementData = "+ statementData.length);
    System.out.println("-----------------------------------");

    OutputStream bOut = null;

    int bankNum = orgDetail.getBankNum();
    int count = 0;


    //while(!retVal && count < 3)
    //{
      try
      {
        connect(true);

        setAutoCommit(false);

      //get rid of existing information
      /*@lineinfo:generated-code*//*@lineinfo:1688^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    merch_statements
//          where   year_month      = :merchDetails.yearMonth
//                  and merch_num   = :merchantNumber
//                  and load_file_name like 'mstmt%'
//                  --@+ hack to prevent deletion of existing statement
//                  and not
//                  (
//                    year_month = 201001
//                    and merch_num in (941000091213,941000095453,941000076853)
//                  )
//                  --@- hack to prevent deletion of existing statement
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    merch_statements\n        where   year_month      =  :1  \n                and merch_num   =  :2  \n                and load_file_name like 'mstmt%'\n                --@+ hack to prevent deletion of existing statement\n                and not\n                (\n                  year_month = 201001\n                  and merch_num in (941000091213,941000095453,941000076853)\n                )\n                --@- hack to prevent deletion of existing statement";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.reports.MerchantStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchDetails.yearMonth);
   __sJT_st.setLong(2,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1702^7*/
      
      /* Code modified by RS for ACHPS implementation, Added ach fields to insert  */
      // insert the column data along with an empty blob
      /* Code modified by RS for LCR implementation, Added VP and MP fields to insert  */
        /*@lineinfo:generated-code*//*@lineinfo:1707^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_statements
//            (
//              merch_num,
//              rec_num,
//              mystery_id,
//              year_month,
//              dda,
//              transit_routing,
//              load_file_name,
//              bank_num,
//              discount,
//              fees,
//  
//              visa_sales_cnt,
//              visa_sales_amt,
//              visa_credit_cnt,
//              visa_credit_amt,
//              visa_disc_amt,
//  
//              mc_sales_cnt,
//              mc_sales_amt,
//              mc_credit_cnt,
//              mc_credit_amt,
//              mc_disc_amt,
//  
//              dinr_sales_cnt,
//              dinr_sales_amt,
//              dinr_credit_cnt,
//              dinr_credit_amt,
//              dinr_disc_amt,
//  
//              disc_sales_cnt,
//              disc_sales_amt,
//              disc_credit_cnt,
//              disc_credit_amt,
//              disc_disc_amt,
//  
//              amex_sales_cnt,
//              amex_sales_amt,
//              amex_credit_cnt,
//              amex_credit_amt,
//              amex_disc_amt,
//  
//              jcb_sales_cnt,
//              jcb_sales_amt,
//              jcb_credit_cnt,
//              jcb_credit_amt,
//              jcb_disc_amt,
//  
//              debit_sales_cnt,
//              debit_sales_amt,
//              debit_credit_cnt,
//              debit_credit_amt,
//              debit_disc_amt,
//  
//              --ebt_sales_cnt,
//              --ebt_sales_amt,
//              --ebt_credit_cnt,
//              --ebt_credit_amt,
//              --ebt_disc_amt,
//  
//              p1_sales_cnt,
//              p1_sales_amt,
//              p1_credit_cnt,
//              p1_credit_amt,
//              p1_disc_amt,
//  
//              p2_sales_cnt,
//              p2_sales_amt,
//              p2_credit_cnt,
//              p2_credit_amt,
//              p2_disc_amt,
//  
//              p3_sales_cnt,
//              p3_sales_amt,
//              p3_credit_cnt,
//              p3_credit_amt,
//              p3_disc_amt,
//              
//              ach_sales_cnt,
//              ach_sales_amt,
//              ach_credit_cnt,
//              ach_credit_amt,
//              ach_disc_amt,
//              
//              mp_sales_cnt,
//              mp_sales_amt,
//              mp_credit_cnt,
//              mp_credit_amt,
//              mp_disc_amt,
//              
//              vp_sales_cnt,
//              vp_sales_amt,
//              vp_credit_cnt,
//              vp_credit_amt,
//              vp_disc_amt,
//              statement_data
//          )
//          values
//          (
//            :merchantNumber,
//            :recNum,   --not sure about this
//            0, --ignore this
//            :merchDetails.yearMonth,
//            :merchDetails.dda,
//            :merchDetails.transitRouting,
//            :loadFilename,     --LOAD_FILENAME,--temp assignment, need to figure this out
//            :bankNum,
//            :totalDiscountDue,
//            :fFeeTotal,
//  
//              :sViCnt,
//              :sViAmt,
//              :cViCnt,
//              :cViAmt,
//              :dViAmt,
//  
//              :sMcCnt,
//              :sMcAmt,
//              :cMcCnt,
//              :cMcAmt,
//              :dMcAmt,
//  
//              :sDiCnt,
//              :sDiAmt,
//              :cDiCnt,
//              :cDiAmt,
//              :dDiAmt,
//  
//              :sDsCnt,
//              :sDsAmt,
//              :cDsCnt,
//              :cDsAmt,
//              :dDsAmt,
//  
//              :sAxCnt,
//              :sAxAmt,
//              :cAxCnt,
//              :cAxAmt,
//              :dAxAmt,
//  
//              :sJcCnt,
//              :sJcAmt,
//              :cJcCnt,
//              :cJcAmt,
//              :dJcAmt,
//  
//              :sDbCnt,
//              :sDbAmt,
//              :cDbCnt,
//              :cDbAmt,
//              :dDbAmt,
//  
//              --:sEbCnt,
//              --:sEbAmt,
//              --:cEbCnt,
//              --:cEbAmt,
//              --:dEbAmt,
//  
//              :sP1Cnt,
//              :sP1Amt,
//              :cP1Cnt,
//              :cP1Amt,
//              :dP1Amt,
//  
//              :sP2Cnt,
//              :sP2Amt,
//              :cP2Cnt,
//              :cP2Amt,
//              :dP2Amt,
//  
//              :sP3Cnt,
//              :sP3Amt,
//              :cP3Cnt,
//              :cP3Amt,
//              :dP3Amt,
//              
//              :sAchCnt,
//              :sAchAmt,
//              :cAchCnt,
//              :cAchAmt,
//              :dAchAmt,
//              
//              :mpSalesCnt,
//              :mpSalesAmt,
//              :mpCreditCnt,
//              :mpCreditAmt,
//              :mpDiscAmt,
//              
//              :vpSalesCnt,
//              :vpSalesAmt,
//              :vpCreditCnt,
//              :vpCreditAmt,
//              :vpDiscAmt,
//              empty_blob()
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into merch_statements\n          (\n            merch_num,\n            rec_num,\n            mystery_id,\n            year_month,\n            dda,\n            transit_routing,\n            load_file_name,\n            bank_num,\n            discount,\n            fees,\n\n            visa_sales_cnt,\n            visa_sales_amt,\n            visa_credit_cnt,\n            visa_credit_amt,\n            visa_disc_amt,\n\n            mc_sales_cnt,\n            mc_sales_amt,\n            mc_credit_cnt,\n            mc_credit_amt,\n            mc_disc_amt,\n\n            dinr_sales_cnt,\n            dinr_sales_amt,\n            dinr_credit_cnt,\n            dinr_credit_amt,\n            dinr_disc_amt,\n\n            disc_sales_cnt,\n            disc_sales_amt,\n            disc_credit_cnt,\n            disc_credit_amt,\n            disc_disc_amt,\n\n            amex_sales_cnt,\n            amex_sales_amt,\n            amex_credit_cnt,\n            amex_credit_amt,\n            amex_disc_amt,\n\n            jcb_sales_cnt,\n            jcb_sales_amt,\n            jcb_credit_cnt,\n            jcb_credit_amt,\n            jcb_disc_amt,\n\n            debit_sales_cnt,\n            debit_sales_amt,\n            debit_credit_cnt,\n            debit_credit_amt,\n            debit_disc_amt,\n\n            --ebt_sales_cnt,\n            --ebt_sales_amt,\n            --ebt_credit_cnt,\n            --ebt_credit_amt,\n            --ebt_disc_amt,\n\n            p1_sales_cnt,\n            p1_sales_amt,\n            p1_credit_cnt,\n            p1_credit_amt,\n            p1_disc_amt,\n\n            p2_sales_cnt,\n            p2_sales_amt,\n            p2_credit_cnt,\n            p2_credit_amt,\n            p2_disc_amt,\n\n            p3_sales_cnt,\n            p3_sales_amt,\n            p3_credit_cnt,\n            p3_credit_amt,\n            p3_disc_amt,\n            \n            ach_sales_cnt,\n            ach_sales_amt,\n            ach_credit_cnt,\n            ach_credit_amt,\n            ach_disc_amt,\n            \n            mp_sales_cnt,\n            mp_sales_amt,\n            mp_credit_cnt,\n            mp_credit_amt,\n            mp_disc_amt,\n            \n            vp_sales_cnt,\n            vp_sales_amt,\n            vp_credit_cnt,\n            vp_credit_amt,\n            vp_disc_amt,\n            statement_data\n        )\n        values\n        (\n           :1  ,\n           :2  ,   --not sure about this\n          0, --ignore this\n           :3  ,\n           :4  ,\n           :5  ,\n           :6  ,     --LOAD_FILENAME,--temp assignment, need to figure this out\n           :7  ,\n           :8  ,\n           :9  ,\n\n             :10  ,\n             :11  ,\n             :12  ,\n             :13  ,\n             :14  ,\n\n             :15  ,\n             :16  ,\n             :17  ,\n             :18  ,\n             :19  ,\n\n             :20  ,\n             :21  ,\n             :22  ,\n             :23  ,\n             :24  ,\n\n             :25  ,\n             :26  ,\n             :27  ,\n             :28  ,\n             :29  ,\n\n             :30  ,\n             :31  ,\n             :32  ,\n             :33  ,\n             :34  ,\n\n             :35  ,\n             :36  ,\n             :37  ,\n             :38  ,\n             :39  ,\n\n             :40  ,\n             :41  ,\n             :42  ,\n             :43  ,\n             :44  ,\n\n            --:sEbCnt,\n            --:sEbAmt,\n            --:cEbCnt,\n            --:cEbAmt,\n            --:dEbAmt,\n\n             :45  ,\n             :46  ,\n             :47  ,\n             :48  ,\n             :49  ,\n\n             :50  ,\n             :51  ,\n             :52  ,\n             :53  ,\n             :54  ,\n\n             :55  ,\n             :56  ,\n             :57  ,\n             :58  ,\n             :59  ,\n            \n             :60  ,\n             :61  ,\n             :62  ,\n             :63  ,\n             :64  ,\n            \n             :65  ,\n             :66  ,\n             :67  ,\n             :68  ,\n             :69  ,\n            \n             :70  ,\n             :71  ,\n             :72  ,\n             :73  ,\n             :74  ,\n            empty_blob()\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.reports.MerchantStatementDetail",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setInt(2,recNum);
   __sJT_st.setString(3,merchDetails.yearMonth);
   __sJT_st.setString(4,merchDetails.dda);
   __sJT_st.setString(5,merchDetails.transitRouting);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setInt(7,bankNum);
   __sJT_st.setDouble(8,totalDiscountDue);
   __sJT_st.setDouble(9,fFeeTotal);
   __sJT_st.setInt(10,sViCnt);
   __sJT_st.setDouble(11,sViAmt);
   __sJT_st.setInt(12,cViCnt);
   __sJT_st.setDouble(13,cViAmt);
   __sJT_st.setDouble(14,dViAmt);
   __sJT_st.setInt(15,sMcCnt);
   __sJT_st.setDouble(16,sMcAmt);
   __sJT_st.setInt(17,cMcCnt);
   __sJT_st.setDouble(18,cMcAmt);
   __sJT_st.setDouble(19,dMcAmt);
   __sJT_st.setInt(20,sDiCnt);
   __sJT_st.setDouble(21,sDiAmt);
   __sJT_st.setInt(22,cDiCnt);
   __sJT_st.setDouble(23,cDiAmt);
   __sJT_st.setDouble(24,dDiAmt);
   __sJT_st.setInt(25,sDsCnt);
   __sJT_st.setDouble(26,sDsAmt);
   __sJT_st.setInt(27,cDsCnt);
   __sJT_st.setDouble(28,cDsAmt);
   __sJT_st.setDouble(29,dDsAmt);
   __sJT_st.setInt(30,sAxCnt);
   __sJT_st.setDouble(31,sAxAmt);
   __sJT_st.setInt(32,cAxCnt);
   __sJT_st.setDouble(33,cAxAmt);
   __sJT_st.setDouble(34,dAxAmt);
   __sJT_st.setInt(35,sJcCnt);
   __sJT_st.setDouble(36,sJcAmt);
   __sJT_st.setInt(37,cJcCnt);
   __sJT_st.setDouble(38,cJcAmt);
   __sJT_st.setDouble(39,dJcAmt);
   __sJT_st.setInt(40,sDbCnt);
   __sJT_st.setDouble(41,sDbAmt);
   __sJT_st.setInt(42,cDbCnt);
   __sJT_st.setDouble(43,cDbAmt);
   __sJT_st.setDouble(44,dDbAmt);
   __sJT_st.setInt(45,sP1Cnt);
   __sJT_st.setDouble(46,sP1Amt);
   __sJT_st.setInt(47,cP1Cnt);
   __sJT_st.setDouble(48,cP1Amt);
   __sJT_st.setDouble(49,dP1Amt);
   __sJT_st.setInt(50,sP2Cnt);
   __sJT_st.setDouble(51,sP2Amt);
   __sJT_st.setInt(52,cP2Cnt);
   __sJT_st.setDouble(53,cP2Amt);
   __sJT_st.setDouble(54,dP2Amt);
   __sJT_st.setInt(55,sP3Cnt);
   __sJT_st.setDouble(56,sP3Amt);
   __sJT_st.setInt(57,cP3Cnt);
   __sJT_st.setDouble(58,cP3Amt);
   __sJT_st.setDouble(59,dP3Amt);
   __sJT_st.setInt(60,sAchCnt);
   __sJT_st.setDouble(61,sAchAmt);
   __sJT_st.setInt(62,cAchCnt);
   __sJT_st.setDouble(63,cAchAmt);
   __sJT_st.setDouble(64,dAchAmt);
   __sJT_st.setInt(65,mpSalesCnt);
   __sJT_st.setDouble(66,mpSalesAmt);
   __sJT_st.setInt(67,mpCreditCnt);
   __sJT_st.setDouble(68,mpCreditAmt);
   __sJT_st.setDouble(69,mpDiscAmt);
   __sJT_st.setInt(70,vpSalesCnt);
   __sJT_st.setDouble(71,vpSalesAmt);
   __sJT_st.setInt(72,vpCreditCnt);
   __sJT_st.setDouble(73,vpCreditAmt);
   __sJT_st.setDouble(74,vpDiscAmt);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1905^9*/
       
        // get a stream handler to the blob
        BLOB b;

        /*@lineinfo:generated-code*//*@lineinfo:1910^9*/

//  ************************************************************
//  #sql [Ctx] { select  statement_data 
//            from    merch_statements
//            where   load_file_name = :loadFilename  -- LOAD_FILENAME
//                    and merch_num  = :merchantNumber
//                    and rec_num    = :recNum
//            for update
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  statement_data  \n          from    merch_statements\n          where   load_file_name =  :1    -- LOAD_FILENAME\n                  and merch_num  =  :2  \n                  and rec_num    =  :3  \n          for update";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.reports.MerchantStatementDetail",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setLong(2,merchantNumber);
   __sJT_st.setInt(3,recNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   b = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1918^9*/

        //deprecated
        //OutputStream bOut = b.getBinaryOutputStream();
        //Use, as of 9.0.2:
        bOut = b.setBinaryStream(1L);

        bOut.write(statementData,0,statementData.length);
        bOut.flush(); 
        bOut.close(); 

        commit();

        retVal = true;

      }
      catch (Exception e)
      {
        e.printStackTrace();
        System.out.println("insert error = "+e.getMessage());
        rollback();
        count++;
        //logEntry("MerchantStatementDetail::saveForWeb",e.toString());
      }
      finally
      {
        cleanUp();
      }

    //}

    return (retVal);
  }

  public long getMerchantNumber()
  {
    return( merchantNumber);
  }

  public String format(double amt)
  {
    String fAmt;
    try
    {
      fAmt = df.format(amt);
    }
    catch(Exception e)
    {
      fAmt = ""+amt;
    }
    return fAmt;
  }

  public String toXML()
  {

    xmlSB = new StringBuffer();

    xmlSB.append("<merchantDetail>");

    XMLGen xmlGen = new XMLGen(xmlSB);

    //return address information
    xmlGen.openTag("returnAddress");
    xmlGen.genXML("name",         merchDetails.returnAddrName);
    xmlGen.genXML("address1",     merchDetails.returnAddrLines[0]);
    xmlGen.genXML("address2",     merchDetails.returnAddrLines[1]);
    xmlGen.genXML("address3",     merchDetails.returnAddrLines[2]);
    xmlGen.genXML("address4",     merchDetails.returnAddrLines[3]);
    xmlGen.genXML("helpInfo",     merchDetails.returnAddrPhone);
    xmlGen.closeTag("returnAddress");

    xmlGen.genXML("statementMsg",stmtMsg);

    //xmlGen.genXML("imageName",imgName);
    xmlGen.genXML("imageName","NA");

    //each Loadable object can generate their own XML
    merchDetails.genXML(xmlGen);

    //plans
    xmlGen.openTag("planInfo");
    xmlGen.genXML("SalesNumberTotal", pSalesNumberTotal);
    xmlGen.genXML("SalesDollarTotal", format(pSalesDollarTotal ));
    xmlGen.genXML("CreditNumberTotal",pCreditNumberTotal);
    xmlGen.genXML("CreditDollarTotal",format(pCreditDollarTotal));
    xmlGen.genXML("NetSalesTotal",    format(pNetSalesTotal    ));
    xmlGen.genXML("AveTicketTotal",   format(pAveTicketTotal   ));
    xmlGen.genXML("DiscountTotal",    format(pDiscountTotal    ));
    xmlGen.genXML(null,plans);
    xmlGen.closeTag("planInfo");

    //deposits
    xmlGen.openTag("depositInfo");
    xmlGen.genXML("SalesNumberTotal", dSalesNumberTotal );
    xmlGen.genXML("SalesDollarTotal", format(dSalesDollarTotal ));
    xmlGen.genXML("CreditNumberTotal",dCreditNumberTotal );
    xmlGen.genXML("CreditDollarTotal",format(dCreditDollarTotal));
    xmlGen.genXML("DiscountPaid",     format(dDiscountPaid     ));
    xmlGen.genXML("SettledTotal",     format(dSettledTotal     ));
    xmlGen.genXML(null, deposits);
    xmlGen.closeTag("depositInfo");


    //adjustments
    xmlGen.openTag("adjustmentInfo");
    xmlGen.genXML("SalesNumberTotal", aSalesNumberTotal );
    xmlGen.genXML("SalesDollarTotal", format(aSalesDollarTotal ));
    xmlGen.genXML("CreditNumberTotal",aCreditNumberTotal );
    xmlGen.genXML("CreditDollarTotal",format(aCreditDollarTotal));
    xmlGen.genXML("DiscountTotal",    format(aDiscountTotal    ));
    xmlGen.genXML("SettledTotal",     format(aSettledTotal     ));
    xmlGen.genXML(null, adjustments);
    xmlGen.closeTag("adjustmentInfo");


    //chargebacks
    xmlGen.openTag("chargebackInfo");
    xmlGen.genXML("SalesNumberTotal", cSalesNumberTotal );
    xmlGen.genXML("SalesDollarTotal", format(cSalesDollarTotal ));
    xmlGen.genXML("CreditNumberTotal",cCreditNumberTotal );
    xmlGen.genXML("CreditDollarTotal",format(cCreditDollarTotal));
    xmlGen.genXML("DiscountTotal",    format(cDiscountTotal    ));
    xmlGen.genXML("SettledTotal",     format(cSettledTotal     ));
    xmlGen.genXML(null, chargebacks);
    xmlGen.closeTag("chargebackInfo");

    //fees
    xmlGen.openTag("feeInfo");
    xmlGen.genXML("FeeTotal",format(fFeeTotal));
    xmlGen.genXML(null, fees);
    xmlGen.closeTag("feeInfo");

    xmlGen.openTag("totalDiscountDue");
    xmlGen.genXML("label", labelDiscountDue );
    xmlGen.genXML("amount",format(totalDiscountDue   ));
    xmlGen.closeTag("totalDiscountDue");

    xmlGen.openTag("totalDiscountPaid");
    xmlGen.genXML("label", "DISCOUNT PAID" );
    xmlGen.genXML("amount",format(dDiscountPaid   ));
    xmlGen.closeTag("totalDiscountPaid");

    xmlGen.openTag("totalFeesDue");
    xmlGen.genXML("label", "FEES DUE" );
    xmlGen.genXML("amount",format(fFeeTotal   ));
    xmlGen.closeTag("totalFeesDue");

    xmlGen.openTag("totalDeductions");
    xmlGen.genXML("label", "AMOUNT DEDUCTED" );
    xmlGen.genXML("amount",format(totalAmountDeducted   ));
    xmlGen.closeTag("totalDeductions");

/*
    //may need to move legend in here - currently in orgDetail
    xmlGen.openTag("legend");
    xmlGen.openTag("category");
    xmlGen.genXML("name","Transaction Codes");
    xmlGen.openTag("item");
    xmlGen.genXML("
    xmlGen.closeTag("item");
    xmlGen.closeTag("category");
    xmlGen.closeTag("legend");
*/

    xmlSB.append("</merchantDetail>");

    return xmlClean(xmlSB.toString());
  }

  public String xmlClean(String dirtyString)
  {
    return  dirtyString.replaceAll("&","&amp;");
    //dirtyString = dirtyString.replace("'","&apos;");
    //dirtyString = dirtyString.replace("\"","&quot;");
    //dirtyString = dirtyString.replace("<","&lt;");
    //dirtyString = dirtyString.replace(">","&gt;");
  }

  public HashMap getPlanInfo()
  {
    return( plans );
  }

  public InfoMerchant getMerchantInfo()
  {
    return( merchDetails );
  }

  public class XMLGen
  {
    StringBuffer xmlSB;

    public XMLGen()
    {
      xmlSB = new StringBuffer();
    }

    public XMLGen(StringBuffer xmlSB)
    {
      this.xmlSB = xmlSB;
    }

    public void openTag(String tag)
    {
      xmlSB.append("<").append(tag).append(">");
    }

    public void closeTag(String tag)
    {
      xmlSB.append("</").append(tag).append(">");
    }

    public void genXML(String title, String value)
    {
      openTag(title);
      xmlSB.append(value);
      closeTag(title);
    }

    public void genXML(String title, int value)
    {
      genXML(title, ""+value);
    }

    public void genXML(String title, double value)
    {
      genXML(title, ""+value);
    }

    public void genXML(String title, Date value)
    {
      genXML(title, value.toString());
    }

    public void genXML(String title, List value)
    {
      Object obj;

      if(value != null)
      {
        if(title !=null)
        {
          openTag(title);
        }

        for(Iterator it = value.iterator(); it.hasNext();)
        {
          obj = it.next();
          if(obj instanceof InfoLoadable)
          {
            ((InfoLoadable)obj).genXML(this);
          }
          else if(obj instanceof String)
          {
            xmlSB.append((String)obj);
          }
        }

        if(title !=null)
        {
          closeTag(title);
        }

      }
    }

    public void genXML(String title, Map value)
    {
      Object obj;

      if(value != null)
      {
        if(title !=null)
        {
          openTag(title);
        }

        for(Iterator it = value.values().iterator(); it.hasNext();)
        {
          obj = it.next();
          if(obj instanceof InfoLoadable)
          {
            ((InfoLoadable)obj).genXML(this);
          }
          else if(obj instanceof String)
          {
            xmlSB.append((String)obj);
          }
        }

        if(title !=null)
        {
          closeTag(title);
        }

      }
    }

  }

}/*@lineinfo:generated-code*/