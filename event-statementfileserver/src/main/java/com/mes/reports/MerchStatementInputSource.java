package com.mes.reports;

import org.xml.sax.InputSource;

/**
 * This class is a special InputSource descendent for using MerchantStatementDetail
 * instances as XML sources.
 */
public class MerchStatementInputSource extends InputSource {

    private MerchantStatementDetail statementDetail;

    /**
     * Constructor for the StatementInputSource
     * @param statementDetail The MerchantStatementDetail object to use
     */
    //public StatementInputSource(MerchantStatementDetail statementDetail)
    public MerchStatementInputSource(MerchantStatementDetail statementDetail)
    {
      this.statementDetail = statementDetail;
    }

    /**
     * Returns the statementDetail.
     * @return MerchantStatementDetail
     */
    public MerchantStatementDetail getMerchantStatementDetail()
    {
      return statementDetail;
    }

    /**
     * Sets the statementDetail.
     * @param statementDetail
     */
    public void setMerchantStatementDetail(MerchantStatementDetail statementDetail)
    {
      this.statementDetail = statementDetail;
    }

}
