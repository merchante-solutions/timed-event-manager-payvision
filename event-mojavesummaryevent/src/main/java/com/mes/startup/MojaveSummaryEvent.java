/*@lineinfo:filename=MojaveSummaryEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/startup/MojaveSummaryEvent.sqlj $

  Description:

  Last Modified By   : $Author: jduncan $
  Last Modified Date : $Date: 2012-11-08 11:09:10 -0800 (Thu, 08 Nov 2012) $
  Version            : $Revision: 20687 $

  Change History:
     See SVN database

  Copyright (C) 2000-2012,2013 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.clearing.utils.ClearingConstants;
import com.mes.startup.ProcessTable.ProcessTableEntry;
import com.mes.support.DateTimeFormatter;

public class MojaveSummaryEvent extends EventBase
{
  static Logger log = Logger.getLogger(MojaveSummaryEvent.class);
  
  public boolean execute( )
  {
    ProcessTableEntry   entry         = null;
    boolean             retVal        = false;
  
    try
    {
      connect(true);
      setAutoCommit(false);
      
      //@log.debug("creating new process table");
      ProcessTable pt = new ProcessTable( "merch_prof_process", "merch_prof_process_sequence", ClearingConstants.PT_MOJAVE_SUMMARY.getValue());
      
      //@log.debug("Checking for pending entries");
      if ( pt.hasPendingEntries() )
      {
        //@log.debug("preparing pending entries");
        pt.preparePendingEntries();
        
        //@log.debug("getting entries");
        Vector entries = pt.getEntryVector();
        
        for( int i = 0; i < entries.size(); ++i )
        {
          //@log.debug("processing entry (" + i + ")");
          entry = (ProcessTable.ProcessTableEntry) entries.elementAt(i);
          
          //@log.debug("recording timestamp begin");
          entry.recordTimestampBegin();
          log.debug("loading data (" + entry.getLoadFilename() + ")");
          loadData(entry);
          log.debug("done loading data");
          entry.recordTimestampEnd();
        }
      }
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  private void loadMainData( Date activityDate, String loadFilename )
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:92^7*/

//  ************************************************************
//  #sql [Ctx] { insert into mojave(
//            activity_month,
//            merchant_number,  
//            vmc_sales_count,
//            vmc_sales_amount,
//            vmc_credits_count,
//            vmc_credits_amount,
//            amex_sales_count,
//            amex_sales_amount,
//            amex_credits_count,
//            amex_credits_amount,
//            disc_sales_count,
//            disc_sales_amount,
//            disc_credits_count,
//            disc_credits_amount,
//            total_income,
//            interchange_expense,
//            vmc_fees,
//            interchange_repair,
//            net_revenue,
//            discover_net_revenue,
//            amex_net_revenue,
//            debit_count,
//            debit_amount,
//            ebt_count,
//            ebt_amount,
//            cash_advance_count,
//            cash_advance_amount,
//            pci_penalties,
//            partnership_payment,
//            load_file_id,
//            load_filename
//          )
//          select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */
//                  sm.active_date                          as activity_month,
//                  sm.merchant_number                      as merchant_number,
//                  nvl(sm.vmc_sales_count,0)          as vmc_sales_count,
//                  nvl(sm.vmc_sales_amount,0)         as vmc_sales_amount,
//                  nvl(sm.vmc_credits_count,0)        as vmc_credits_count,
//                  nvl(sm.vmc_credits_amount,0)       as vmc_credits_amount,
//                  nvl(pl_am.pl_number_of_sales,0)    as amex_sales_count,
//                  nvl(pl_am.pl_sales_amount,0)       as amex_sales_amount,
//                  nvl(pl_am.pl_number_of_credits,0)  as amex_credits_count,
//                  nvl(pl_am.pl_credits_amount,0)     as amex_credits_amount,
//                  nvl(pl_ds.pl_number_of_sales,0)    as disc_sales_count,
//                  nvl(pl_ds.pl_sales_amount,0)       as disc_sales_amount,
//                  nvl(pl_ds.pl_number_of_credits,0)  as disc_credits_count,
//                  nvl(pl_ds.pl_credits_amount,0)     as disc_credits_amount,
//  		      case
//  			      when sm.bank_number in (select bank_number from mbs_banks where owner != 1) 
//  			        then 0
//  			      else nvl(gn.t1_tot_income,0)
//  			    end   + nvl(sm.fee_dce_adj_amount,0) + nvl(sm.disc_ic_dce_adj_amount,0) 
//  			                                as total_income,
//  		      case
//  			      when sm.bank_number in (select bank_number from mbs_banks where owner != 1)
//  			        then 0
//  			      else
//  			        nvl(sm.interchange_expense_enhanced,nvl(sm.interchange_expense,0)) +
//  			          (
//  			            round(nvl(sm.visa_sales_amount,0) * 
//  			                case when sm.active_date >= '01-Jan-2015' then .0011
//  			                    when sm.active_date between '01-Jul-2010' and '31-Dec-2014' then .0011
//  			                    else .000925                        
//  			                end, 2) +
//  			            round(nvl(sm.mc_sales_amount,0) *  
//  			                case when sm.active_date >= '01-Jan-2015' then 0.0012
//  			                    when sm.active_date between '01-Apr-2010' and '31-Dec-2014' then .0011
//  			                    else .00095
//  			                end, 2)  +
//  			            round(nvl(sm.disc_ic_sales_amount,0) *
//  			            	case when sm.active_date >= '01-Jun-2016' then 0.0013
//  			            		when sm.active_date between '01-Apr-2015' and '31-May-2016' then 0.0011  
//  			                    when sm.active_date between '01-Apr-2012' and '31-Mar-2015' then .00105
//  			                else .0010
//  			                end,2)  +
//  			            round( nvl(sm.vs_cr_assessment_amount, 0) * 0.01 *
//  			            		nvl( (select mp.rate from mbs_pricing mp where sm.merchant_number = mp.merchant_number and mp.item_type = 110 and :activityDate between mp.valid_date_begin and mp.valid_date_end), 0) , 2)                  
//  			          ) + nvl(sm.tot_exp_ach_vendor,0)           
//  			      end                                 as interchange_expense, 
//                  case 
//                    when sm.bank_number in (select bank_number from mbs_banks where owner != 1)
//                      then 0
//                    else nvl(sm.vmc_fees,0)
//                    end                                  as vmc_fees,
//                  case
//                    when sm.bank_number in (select bank_number from mbs_banks where owner != 1)
//                      then 0
//                    else
//                      nvl(sm.interchange_expense,0)-nvl(sm.interchange_expense_enhanced,nvl(sm.interchange_expense,0)) 
//                    end                                  as interchange_repair,
//                  case 
//  	                when sm.bank_number in (select bank_number from mbs_banks where owner != 1)
//  	                  then 0
//  	                else nvl(gn.t1_tot_income,0) + nvl(sm.fee_dce_adj_amount,0) + nvl(sm.disc_ic_dce_adj_amount,0) -  
//  	                  (nvl(sm.interchange_expense_enhanced,nvl(sm.interchange_expense,0)) +
//  	                    (
//  	                      round(nvl(sm.visa_sales_amount,0) * 
//  	                          case when sm.active_date >= '01-Jan-2015' then .0011
//  	                              when sm.active_date between '01-Jul-2010' and '31-Dec-2014' then .0011
//  	                              else .000925   
//  	                              end,2) +
//  	                      round(nvl(sm.mc_sales_amount,0) * 
//  	                          case when sm.active_date >= '01-Jan-2015' then 0.0012
//  	                              when sm.active_date between '01-Apr-2010' and '31-Dec-2014' then .0011
//  	                              else .00095
//  	                              end,2) +
//  	                      round(nvl(sm.disc_ic_sales_amount,0) *  
//  	                      	  case when sm.active_date >= '01-Jun-2016' then 0.0013 
//                      	  		  when sm.active_date between '01-Apr-2015' and '31-May-2016' then 0.0011   
//  	                              when sm.active_date between '01-Apr-2012' and '31-Mar-2015' then .00105
//  	                          else .0010
//  	                          end,2)  +
//  	                      round( nvl(sm.vs_cr_assessment_amount, 0) * 0.01 *
//                                       nvl( (select mp.rate from mbs_pricing mp where sm.merchant_number = mp.merchant_number and mp.item_type = 110 and :activityDate between mp.valid_date_begin and mp.valid_date_end), 0) , 2)
//  	                    )
//  	                  )            
//  	                end                                 as net_revenue,
//                  case 
//                    when sm.bank_number in (select bank_number from mbs_banks where owner != 1)
//                      then 0
//                    else 
//                      nvl(pl_ds.pl_correct_disc_amt,0) +
//                        nvl(sm.tot_inc_ic_disc,0) -
//                        nvl(sm.disc_interchange,0)
//                    end                                  as discover_net_revenue,
//                  case
//                    when sm.bank_number in (select bank_number from mbs_banks where owner != 1)
//                      then 0
//                    when nvl(pl_am.pl_correct_disc_amt,0) = 0
//                      then 0            
//                    else
//                      nvl(pl_am.pl_correct_disc_amt,0) + nvl(sm.tot_inc_ic_amex,0) -
//                        case when mf.bank_number = 3941 and mf.group_2_association = '400059' and nvl(sm.amex_sales_amount,0) > 0 and nvl(sm.amex_interchange,0) = 0
//                          then round(sm.amex_sales_amount * 0.0235,2)
//                        else nvl(sm.amex_interchange,0)
//                        end
//                    end                                  as amex_net_revenue,
//                  nvl(sm.debit_sales_count,0),
//                  nvl(sm.debit_sales_amount,0),
//                  nvl(sm.ebt_sales_count,0),
//                  nvl(sm.ebt_sales_amount,0),
//                  nvl(sm.cash_advance_vol_count,0)   as cash_advance_count,
//                  nvl(sm.cash_advance_vol_amount,0)  as cash_advance_amount,
//                  nvl(penalty.st_fee_amount,0)       as pci_penalties,
//                  nvl(sm.tot_partnership,0)          as partnership_payment,
//                  sm.load_file_id                    as load_file_id,
//                  sm.load_filename                   as load_filename
//          from    monthly_extract_summary sm,
//                  mif mf,
//                  monthly_extract_pl pl_ds,
//                  (
//                    select  pl.hh_load_sec hh_load_sec,
//                            nvl(pl.pl_number_of_sales,0) pl_number_of_sales,
//                            nvl(pl.pl_sales_amount,0)    pl_sales_amount,
//                            nvl(pl.pl_number_of_credits,0) pl_number_of_credits,
//                            nvl(pl.pl_credits_amount,0)    pl_credits_amount,
//                            nvl(pl.pl_correct_disc_amt,0) pl_correct_disc_amt
//                    from    mif mf,
//                            monthly_extract_summary sm,
//                            monthly_extract_pl pl
//                    where   mf.bank_number in (select bank_number from mbs_banks)
//                            and mf.merchant_number = sm.merchant_number
//                            and sm.active_date = :activityDate
//                            and sm.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                            and sm.hh_load_sec = pl.hh_load_sec
//                            and pl.pl_plan_type like 'AM%'
//                    order by pl.hh_load_sec
//                  ) pl_am,
//                  monthly_extract_gn gn,
//                  monthly_extract_st penalty,
//                  (
//                      select merch_num, max(visa_rate) as visa_rate, max(ds_rate) as ds_rate, max(mc_rate) as mc_rate from (
//                          select mf.merchant_number as merch_num, 
//                              case when mp.item_subclass = 'VS' then mp.rate else 0 end as visa_rate,
//                              case when mp.item_subclass = 'DS' then mp.rate else 0 end as ds_rate,  
//                              case when mp.item_subclass = 'MC' then mp.rate else 0 end as mc_rate
//                          from  mif mf,
//                                monthly_extract_summary sm,
//                                mbs_pricing mp
//                          where mf.bank_number in (select bank_number from mbs_banks)
//                                and mf.merchant_number = sm.merchant_number
//                                and sm.active_date = :activityDate
//                                and sm.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                                and mp.merchant_number = (select nvl(mb.trait_01,mb.bank_number) 
//                                                      from mbs_banks mb 
//                                                      where mb.bank_number = mf.bank_number ) || lpad(mf.ic_bet_visa,4,'0')  
//                                and :activityDate between mp.valid_date_begin and mp.valid_date_end
//                                and mp.item_type = 112
//                          )
//                      group by merch_num
//                  ) assoc_pricing
//          where   sm.bank_number = substr(:loadFilename, 8, 4)
//                  and sm.active_date = :activityDate
//                  and sm.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                  and sm.merchant_number = mf.merchant_number
//                  and sm.hh_load_sec = gn.hh_load_sec
//                  and sm.hh_load_sec = pl_ds.hh_load_sec(+)
//                  and pl_ds.pl_plan_type(+) = 'DS'
//                  and sm.hh_load_sec = pl_am.hh_load_sec(+)
//                  and sm.hh_load_sec = penalty.hh_load_sec(+)
//                  and penalty.st_statement_desc(+) = 'QUARTERLY PCI VALIDATION PENALTY FEE'
//                  and mf.merchant_number = assoc_pricing.merch_num (+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into mojave(\n          activity_month,\n          merchant_number,  \n          vmc_sales_count,\n          vmc_sales_amount,\n          vmc_credits_count,\n          vmc_credits_amount,\n          amex_sales_count,\n          amex_sales_amount,\n          amex_credits_count,\n          amex_credits_amount,\n          disc_sales_count,\n          disc_sales_amount,\n          disc_credits_count,\n          disc_credits_amount,\n          total_income,\n          interchange_expense,\n          vmc_fees,\n          interchange_repair,\n          net_revenue,\n          discover_net_revenue,\n          amex_net_revenue,\n          debit_count,\n          debit_amount,\n          ebt_count,\n          ebt_amount,\n          cash_advance_count,\n          cash_advance_amount,\n          pci_penalties,\n          partnership_payment,\n          load_file_id,\n          load_filename\n        )\n        select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */\n                sm.active_date                          as activity_month,\n                sm.merchant_number                      as merchant_number,\n                nvl(sm.vmc_sales_count,0)          as vmc_sales_count,\n                nvl(sm.vmc_sales_amount,0)         as vmc_sales_amount,\n                nvl(sm.vmc_credits_count,0)        as vmc_credits_count,\n                nvl(sm.vmc_credits_amount,0)       as vmc_credits_amount,\n                nvl(pl_am.pl_number_of_sales,0)    as amex_sales_count,\n                nvl(pl_am.pl_sales_amount,0)       as amex_sales_amount,\n                nvl(pl_am.pl_number_of_credits,0)  as amex_credits_count,\n                nvl(pl_am.pl_credits_amount,0)     as amex_credits_amount,\n                nvl(pl_ds.pl_number_of_sales,0)    as disc_sales_count,\n                nvl(pl_ds.pl_sales_amount,0)       as disc_sales_amount,\n                nvl(pl_ds.pl_number_of_credits,0)  as disc_credits_count,\n                nvl(pl_ds.pl_credits_amount,0)     as disc_credits_amount,\n\t\t      case\n\t\t\t      when sm.bank_number in (select bank_number from mbs_banks where owner != 1) \n\t\t\t        then 0\n\t\t\t      else nvl(gn.t1_tot_income,0)\n\t\t\t    end   + nvl(sm.fee_dce_adj_amount,0) + nvl(sm.disc_ic_dce_adj_amount,0) \n\t\t\t                                as total_income,\n\t\t      case\n\t\t\t      when sm.bank_number in (select bank_number from mbs_banks where owner != 1)\n\t\t\t        then 0\n\t\t\t      else\n\t\t\t        nvl(sm.interchange_expense_enhanced,nvl(sm.interchange_expense,0)) +\n\t\t\t          (\n\t\t\t            round(nvl(sm.visa_sales_amount,0) * \n\t\t\t                case when sm.active_date >= '01-Jan-2015' then .0011\n\t\t\t                    when sm.active_date between '01-Jul-2010' and '31-Dec-2014' then .0011\n\t\t\t                    else .000925                        \n\t\t\t                end, 2) +\n\t\t\t            round(nvl(sm.mc_sales_amount,0) *  \n\t\t\t                case when sm.active_date >= '01-Jan-2015' then 0.0012\n\t\t\t                    when sm.active_date between '01-Apr-2010' and '31-Dec-2014' then .0011\n\t\t\t                    else .00095\n\t\t\t                end, 2)  +\n\t\t\t            round(nvl(sm.disc_ic_sales_amount,0) *\n\t\t\t            \tcase when sm.active_date >= '01-Jun-2016' then 0.0013\n\t\t\t            \t\twhen sm.active_date between '01-Apr-2015' and '31-May-2016' then 0.0011  \n\t\t\t                    when sm.active_date between '01-Apr-2012' and '31-Mar-2015' then .00105\n\t\t\t                else .0010\n\t\t\t                end,2)  +\n\t\t\t            round( nvl(sm.vs_cr_assessment_amount, 0) * 0.01 *\n\t\t\t            \t\tnvl( (select mp.rate from mbs_pricing mp where sm.merchant_number = mp.merchant_number and mp.item_type = 110 and  :1   between mp.valid_date_begin and mp.valid_date_end), 0) , 2)                  \n\t\t\t          ) + nvl(sm.tot_exp_ach_vendor,0)           \n\t\t\t      end                                 as interchange_expense, \n                case \n                  when sm.bank_number in (select bank_number from mbs_banks where owner != 1)\n                    then 0\n                  else nvl(sm.vmc_fees,0)\n                  end                                  as vmc_fees,\n                case\n                  when sm.bank_number in (select bank_number from mbs_banks where owner != 1)\n                    then 0\n                  else\n                    nvl(sm.interchange_expense,0)-nvl(sm.interchange_expense_enhanced,nvl(sm.interchange_expense,0)) \n                  end                                  as interchange_repair,\n                case \n\t                when sm.bank_number in (select bank_number from mbs_banks where owner != 1)\n\t                  then 0\n\t                else nvl(gn.t1_tot_income,0) + nvl(sm.fee_dce_adj_amount,0) + nvl(sm.disc_ic_dce_adj_amount,0) -  \n\t                  (nvl(sm.interchange_expense_enhanced,nvl(sm.interchange_expense,0)) +\n\t                    (\n\t                      round(nvl(sm.visa_sales_amount,0) * \n\t                          case when sm.active_date >= '01-Jan-2015' then .0011\n\t                              when sm.active_date between '01-Jul-2010' and '31-Dec-2014' then .0011\n\t                              else .000925   \n\t                              end,2) +\n\t                      round(nvl(sm.mc_sales_amount,0) * \n\t                          case when sm.active_date >= '01-Jan-2015' then 0.0012\n\t                              when sm.active_date between '01-Apr-2010' and '31-Dec-2014' then .0011\n\t                              else .00095\n\t                              end,2) +\n\t                      round(nvl(sm.disc_ic_sales_amount,0) *  \n\t                      \t  case when sm.active_date >= '01-Jun-2016' then 0.0013 \n                    \t  \t\t  when sm.active_date between '01-Apr-2015' and '31-May-2016' then 0.0011   \n\t                              when sm.active_date between '01-Apr-2012' and '31-Mar-2015' then .00105\n\t                          else .0010\n\t                          end,2)  +\n\t                      round( nvl(sm.vs_cr_assessment_amount, 0) * 0.01 *\n                                     nvl( (select mp.rate from mbs_pricing mp where sm.merchant_number = mp.merchant_number and mp.item_type = 110 and  :2   between mp.valid_date_begin and mp.valid_date_end), 0) , 2)\n\t                    )\n\t                  )            \n\t                end                                 as net_revenue,\n                case \n                  when sm.bank_number in (select bank_number from mbs_banks where owner != 1)\n                    then 0\n                  else \n                    nvl(pl_ds.pl_correct_disc_amt,0) +\n                      nvl(sm.tot_inc_ic_disc,0) -\n                      nvl(sm.disc_interchange,0)\n                  end                                  as discover_net_revenue,\n                case\n                  when sm.bank_number in (select bank_number from mbs_banks where owner != 1)\n                    then 0\n                  when nvl(pl_am.pl_correct_disc_amt,0) = 0\n                    then 0            \n                  else\n                    nvl(pl_am.pl_correct_disc_amt,0) + nvl(sm.tot_inc_ic_amex,0) -\n                      case when mf.bank_number = 3941 and mf.group_2_association = '400059' and nvl(sm.amex_sales_amount,0) > 0 and nvl(sm.amex_interchange,0) = 0\n                        then round(sm.amex_sales_amount * 0.0235,2)\n                      else nvl(sm.amex_interchange,0)\n                      end\n                  end                                  as amex_net_revenue,\n                nvl(sm.debit_sales_count,0),\n                nvl(sm.debit_sales_amount,0),\n                nvl(sm.ebt_sales_count,0),\n                nvl(sm.ebt_sales_amount,0),\n                nvl(sm.cash_advance_vol_count,0)   as cash_advance_count,\n                nvl(sm.cash_advance_vol_amount,0)  as cash_advance_amount,\n                nvl(penalty.st_fee_amount,0)       as pci_penalties,\n                nvl(sm.tot_partnership,0)          as partnership_payment,\n                sm.load_file_id                    as load_file_id,\n                sm.load_filename                   as load_filename\n        from    monthly_extract_summary sm,\n                mif mf,\n                monthly_extract_pl pl_ds,\n                (\n                  select  pl.hh_load_sec hh_load_sec,\n                          nvl(pl.pl_number_of_sales,0) pl_number_of_sales,\n                          nvl(pl.pl_sales_amount,0)    pl_sales_amount,\n                          nvl(pl.pl_number_of_credits,0) pl_number_of_credits,\n                          nvl(pl.pl_credits_amount,0)    pl_credits_amount,\n                          nvl(pl.pl_correct_disc_amt,0) pl_correct_disc_amt\n                  from    mif mf,\n                          monthly_extract_summary sm,\n                          monthly_extract_pl pl\n                  where   mf.bank_number in (select bank_number from mbs_banks)\n                          and mf.merchant_number = sm.merchant_number\n                          and sm.active_date =  :3  \n                          and sm.load_file_id = load_filename_to_load_file_id( :4  )\n                          and sm.hh_load_sec = pl.hh_load_sec\n                          and pl.pl_plan_type like 'AM%'\n                  order by pl.hh_load_sec\n                ) pl_am,\n                monthly_extract_gn gn,\n                monthly_extract_st penalty,\n                (\n                    select merch_num, max(visa_rate) as visa_rate, max(ds_rate) as ds_rate, max(mc_rate) as mc_rate from (\n                        select mf.merchant_number as merch_num, \n                            case when mp.item_subclass = 'VS' then mp.rate else 0 end as visa_rate,\n                            case when mp.item_subclass = 'DS' then mp.rate else 0 end as ds_rate,  \n                            case when mp.item_subclass = 'MC' then mp.rate else 0 end as mc_rate\n                        from  mif mf,\n                              monthly_extract_summary sm,\n                              mbs_pricing mp\n                        where mf.bank_number in (select bank_number from mbs_banks)\n                              and mf.merchant_number = sm.merchant_number\n                              and sm.active_date =  :5  \n                              and sm.load_file_id = load_filename_to_load_file_id( :6  )\n                              and mp.merchant_number = (select nvl(mb.trait_01,mb.bank_number) \n                                                    from mbs_banks mb \n                                                    where mb.bank_number = mf.bank_number ) || lpad(mf.ic_bet_visa,4,'0')  \n                              and  :7   between mp.valid_date_begin and mp.valid_date_end\n                              and mp.item_type = 112\n                        )\n                    group by merch_num\n                ) assoc_pricing\n        where   sm.bank_number = substr( :8  , 8, 4)\n                and sm.active_date =  :9  \n                and sm.load_file_id = load_filename_to_load_file_id( :10  )\n                and sm.merchant_number = mf.merchant_number\n                and sm.hh_load_sec = gn.hh_load_sec\n                and sm.hh_load_sec = pl_ds.hh_load_sec(+)\n                and pl_ds.pl_plan_type(+) = 'DS'\n                and sm.hh_load_sec = pl_am.hh_load_sec(+)\n                and sm.hh_load_sec = penalty.hh_load_sec(+)\n                and penalty.st_statement_desc(+) = 'QUARTERLY PCI VALIDATION PENALTY FEE'\n                and mf.merchant_number = assoc_pricing.merch_num (+)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.MojaveSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activityDate);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setDate(3,activityDate);
   __sJT_st.setString(4,loadFilename);
   __sJT_st.setDate(5,activityDate);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setDate(7,activityDate);
   __sJT_st.setString(8,loadFilename);
   __sJT_st.setDate(9,activityDate);
   __sJT_st.setString(10,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:297^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("loadMainData(" + activityDate.toString() + ")", e.toString());
      throw e;
    }
  }
  
  private void loadPCIData( Date activityDate, String loadFilename )
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:313^7*/

//  ************************************************************
//  #sql [Ctx] { merge into mojave mj
//          using 
//          (
//            select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */
//                    sm.active_date              as activity_month,
//                    sm.merchant_number          as merchant_number,
//                    sum(st.st_fee_amount)       as pci_compliance
//            from    monthly_extract_summary sm,
//                    monthly_extract_st st        
//            where   sm.bank_number = substr(:loadFilename, 8, 4) 
//                    and sm.active_date = :activityDate
//                    and sm.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                    and sm.hh_load_sec = st.hh_load_sec
//                    and st.item_category = 'CG-PCI'
//            group by sm.active_date, sm.merchant_number
//          ) pci
//          on
//          (
//            mj.merchant_number = pci.merchant_number
//            and mj.activity_month = pci.activity_month
//          )                
//          when matched then
//          update
//            set mj.pci_compliance = pci.pci_compliance
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "merge into mojave mj\n        using \n        (\n          select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */\n                  sm.active_date              as activity_month,\n                  sm.merchant_number          as merchant_number,\n                  sum(st.st_fee_amount)       as pci_compliance\n          from    monthly_extract_summary sm,\n                  monthly_extract_st st        \n          where   sm.bank_number = substr( :1  , 8, 4) \n                  and sm.active_date =  :2  \n                  and sm.load_file_id = load_filename_to_load_file_id( :3  )\n                  and sm.hh_load_sec = st.hh_load_sec\n                  and st.item_category = 'CG-PCI'\n          group by sm.active_date, sm.merchant_number\n        ) pci\n        on\n        (\n          mj.merchant_number = pci.merchant_number\n          and mj.activity_month = pci.activity_month\n        )                \n        when matched then\n        update\n          set mj.pci_compliance = pci.pci_compliance";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.MojaveSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setString(3,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:339^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("loadPCIData(" + activityDate.toString() + ")", e.toString());
      throw e;
    }
  }
  
  private void loadDurbinData( Date activityDate, String loadFilename )
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:355^7*/

//  ************************************************************
//  #sql [Ctx] { merge into mojave mj
//          using 
//          (
//            select  /*+ index (mes idx_mon_ext_summary_bnum_adate) */
//                    mes.merchant_number,
//                    trunc(sm.batch_date,'mm') activity_month,
//                    (
//                      sum(round((sm.sales_amount * icd2.ic_rate * 0.01),2)
//                          + (sm.sales_count * icd2.ic_per_item))
//                      - sum(round((sm.sales_amount * icd.ic_rate * 0.01),2)
//                            + (sm.sales_count * icd.ic_per_item))
//                    )                                           as cost_savings                                
//            from    monthly_extract_summary     mes,
//                    --mif                         mf,
//                    daily_detail_ic_summary     sm,
//                    daily_detail_file_ic_desc   icd,
//                    daily_detail_file_ic_desc   icd2
//            where   mes.bank_number = substr(:loadFilename, 8, 4)
//                    and mes.active_date = :activityDate
//                    and mes.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                    and sm.merchant_number = mes.merchant_number
//                    and sm.batch_date between :activityDate and last_day(:activityDate)
//                    and icd.card_type = decode(substr(sm.card_type,1,1),'V','VS','M','MC',sm.card_type)
//                    and icd.ic_code = sm.ic_cat
//                    and sm.batch_date between icd.valid_date_begin and icd.valid_date_end
//                    and icd.issuer_ic_level like '%-reg'
//                    and icd2.card_type = icd.card_type
//                    and icd2.ic_code = nvl(icd.reg_ic_code,'none')
//                    and sm.batch_date between icd2.valid_date_begin and icd2.valid_date_end
//            group by mes.merchant_number, trunc(sm.batch_date,'mm')
//          ) durbin
//          on
//          (
//            mj.merchant_number = durbin.merchant_number
//            and mj.activity_month = durbin.activity_month
//          )                
//          when matched then
//          update
//            set mj.durbin_net_revenue = durbin.cost_savings
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "merge into mojave mj\n        using \n        (\n          select  /*+ index (mes idx_mon_ext_summary_bnum_adate) */\n                  mes.merchant_number,\n                  trunc(sm.batch_date,'mm') activity_month,\n                  (\n                    sum(round((sm.sales_amount * icd2.ic_rate * 0.01),2)\n                        + (sm.sales_count * icd2.ic_per_item))\n                    - sum(round((sm.sales_amount * icd.ic_rate * 0.01),2)\n                          + (sm.sales_count * icd.ic_per_item))\n                  )                                           as cost_savings                                \n          from    monthly_extract_summary     mes,\n                  --mif                         mf,\n                  daily_detail_ic_summary     sm,\n                  daily_detail_file_ic_desc   icd,\n                  daily_detail_file_ic_desc   icd2\n          where   mes.bank_number = substr( :1  , 8, 4)\n                  and mes.active_date =  :2  \n                  and mes.load_file_id = load_filename_to_load_file_id( :3  )\n                  and sm.merchant_number = mes.merchant_number\n                  and sm.batch_date between  :4   and last_day( :5  )\n                  and icd.card_type = decode(substr(sm.card_type,1,1),'V','VS','M','MC',sm.card_type)\n                  and icd.ic_code = sm.ic_cat\n                  and sm.batch_date between icd.valid_date_begin and icd.valid_date_end\n                  and icd.issuer_ic_level like '%-reg'\n                  and icd2.card_type = icd.card_type\n                  and icd2.ic_code = nvl(icd.reg_ic_code,'none')\n                  and sm.batch_date between icd2.valid_date_begin and icd2.valid_date_end\n          group by mes.merchant_number, trunc(sm.batch_date,'mm')\n        ) durbin\n        on\n        (\n          mj.merchant_number = durbin.merchant_number\n          and mj.activity_month = durbin.activity_month\n        )                \n        when matched then\n        update\n          set mj.durbin_net_revenue = durbin.cost_savings";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.MojaveSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setString(3,loadFilename);
   __sJT_st.setDate(4,activityDate);
   __sJT_st.setDate(5,activityDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:396^7*/

      commit();
    }
    catch(Exception e)
    {
      logEntry("loadDurbinData(" + activityDate.toString() + ")", e.toString());
      throw e;
    }
  }
  
  private void loadComplianceData( Date activityDate, String loadFilename )
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:412^7*/

//  ************************************************************
//  #sql [Ctx] { merge into mojave mj
//          using 
//          (
//            select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */
//                    sm.active_date              as activity_month,
//                    sm.merchant_number          as merchant_number,
//                    sum(st.st_fee_amount)       as compliance_reporting
//            from    monthly_extract_summary sm,
//                    monthly_extract_st st        
//            where   sm.bank_number = substr(:loadFilename, 8, 4)
//                    and sm.active_date = :activityDate
//                    and sm.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                    and sm.hh_load_sec = st.hh_load_sec
//                    and st.st_statement_desc in 
//                    (
//                      'ANNUAL COMPLIANCE REPORTING FE',
//                      'IRS COMPLIANCE REPORTING (ANNUAL)',
//                      'ANNUAL COMPLIANCE REPORTING FEE',
//                      'COMPLIANCE REPORTING',
//                      'IRS COMPLIANCE REPORTING',
//                      'ANUAL COMPLIANCE REPORTING FEE',
//                      'ANNUAL IRS COMPLIANCE  REPORTING FEE',
//                      'ANNUAL COMPLIANCE REPORTING',
//                      'IRS COMPLIANCE REPORTING FEE',
//                      'IRS  COMPLIANCE REPORTING FEE',
//                      'COMPLIANCE REPORTING FEE',
//                      'ANNUAL ADMINISTRATIVE MAINTENANCE'
//                    ) 
//            group by sm.active_date, sm.merchant_number
//          ) irs
//          on
//          (
//            mj.merchant_number = irs.merchant_number
//            and mj.activity_month = irs.activity_month
//          )                
//          when matched then
//          update
//            set mj.compliance_reporting = irs.compliance_reporting
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "merge into mojave mj\n        using \n        (\n          select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */\n                  sm.active_date              as activity_month,\n                  sm.merchant_number          as merchant_number,\n                  sum(st.st_fee_amount)       as compliance_reporting\n          from    monthly_extract_summary sm,\n                  monthly_extract_st st        \n          where   sm.bank_number = substr( :1  , 8, 4)\n                  and sm.active_date =  :2  \n                  and sm.load_file_id = load_filename_to_load_file_id( :3  )\n                  and sm.hh_load_sec = st.hh_load_sec\n                  and st.st_statement_desc in \n                  (\n                    'ANNUAL COMPLIANCE REPORTING FE',\n                    'IRS COMPLIANCE REPORTING (ANNUAL)',\n                    'ANNUAL COMPLIANCE REPORTING FEE',\n                    'COMPLIANCE REPORTING',\n                    'IRS COMPLIANCE REPORTING',\n                    'ANUAL COMPLIANCE REPORTING FEE',\n                    'ANNUAL IRS COMPLIANCE  REPORTING FEE',\n                    'ANNUAL COMPLIANCE REPORTING',\n                    'IRS COMPLIANCE REPORTING FEE',\n                    'IRS  COMPLIANCE REPORTING FEE',\n                    'COMPLIANCE REPORTING FEE',\n                    'ANNUAL ADMINISTRATIVE MAINTENANCE'\n                  ) \n          group by sm.active_date, sm.merchant_number\n        ) irs\n        on\n        (\n          mj.merchant_number = irs.merchant_number\n          and mj.activity_month = irs.activity_month\n        )                \n        when matched then\n        update\n          set mj.compliance_reporting = irs.compliance_reporting";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.MojaveSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setString(3,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:452^7*/
    
      commit();
    }
    catch(Exception e)
    {
      logEntry("loadComplianceData(" + activityDate.toString() + ")", e.toString());
      throw e;
    }
  }
  
  private void loadDebitEBT( Date activityDate, String loadFilename )
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:468^7*/

//  ************************************************************
//  #sql [Ctx] { merge into mojave mj
//          using 
//          (
//            select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */
//                    sm.active_date as activity_month,
//                    sm.merchant_number as merchant_number,
//                    nvl(sm.debit_sales_count,0) as debit_count,
//                    nvl(sm.debit_sales_amount,0) as debit_amount,
//                    nvl(sm.ebt_sales_count,0) as ebt_count,
//                    nvl(sm.ebt_sales_amount,0) as ebt_amount
//            from    monthly_extract_summary sm
//            where   sm.bank_number = substr(:loadFilename, 8, 4)
//                    and sm.active_date = :activityDate
//                    and sm.load_file_id = load_filename_to_load_file_id(:loadFilename)
//          ) dbt
//          on
//          (
//            mj.merchant_number = dbt.merchant_number
//            and mj.activity_month = dbt.activity_month
//          )  
//          when matched then
//          update
//            set mj.debit_count = dbt.debit_count,
//                mj.debit_amount = dbt.debit_amount,
//                mj.ebt_count = dbt.ebt_count,
//                mj.ebt_amount = dbt.ebt_count
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "merge into mojave mj\n        using \n        (\n          select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */\n                  sm.active_date as activity_month,\n                  sm.merchant_number as merchant_number,\n                  nvl(sm.debit_sales_count,0) as debit_count,\n                  nvl(sm.debit_sales_amount,0) as debit_amount,\n                  nvl(sm.ebt_sales_count,0) as ebt_count,\n                  nvl(sm.ebt_sales_amount,0) as ebt_amount\n          from    monthly_extract_summary sm\n          where   sm.bank_number = substr( :1  , 8, 4)\n                  and sm.active_date =  :2  \n                  and sm.load_file_id = load_filename_to_load_file_id( :3  )\n        ) dbt\n        on\n        (\n          mj.merchant_number = dbt.merchant_number\n          and mj.activity_month = dbt.activity_month\n        )  \n        when matched then\n        update\n          set mj.debit_count = dbt.debit_count,\n              mj.debit_amount = dbt.debit_amount,\n              mj.ebt_count = dbt.ebt_count,\n              mj.ebt_amount = dbt.ebt_count";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.MojaveSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setString(3,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:496^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("loadDebitEBT(" + activityDate.toString() + ")", e.toString());
      throw e;
    }
  }
  
  private void loadICRepair( Date activityDate, String loadFilename )
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:512^7*/

//  ************************************************************
//  #sql [Ctx] { merge into mojave mj
//          using 
//          (
//             select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */
//                    sm.active_date as activity_month,
//                    sm.merchant_number as merchant_number,
//                    case
//                      when sm.bank_number in (select bank_number from mbs_banks where owner != 1)
//                        then 0
//                      when sm.load_filename like 'mon_ext%'
//                        then 0
//                      else
//                        nvl(sm.interchange_expense,0)-nvl(sm.interchange_expense_enhanced,nvl(sm.interchange_expense,0)) 
//                      end                                  as interchange_repair
//            from    monthly_extract_summary sm
//            where   sm.bank_number = substr(:loadFilename, 8, 4)
//                    and sm.active_date = :activityDate
//                    and sm.load_file_id = load_filename_to_load_file_id(:loadFilename)
//         ) icrep
//          on
//          (
//            mj.merchant_number = icrep.merchant_number
//            and mj.activity_month = icrep.activity_month
//          )
//          when matched then
//          update
//            set mj.interchange_repair = icrep.interchange_repair
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "merge into mojave mj\n        using \n        (\n           select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */\n                  sm.active_date as activity_month,\n                  sm.merchant_number as merchant_number,\n                  case\n                    when sm.bank_number in (select bank_number from mbs_banks where owner != 1)\n                      then 0\n                    when sm.load_filename like 'mon_ext%'\n                      then 0\n                    else\n                      nvl(sm.interchange_expense,0)-nvl(sm.interchange_expense_enhanced,nvl(sm.interchange_expense,0)) \n                    end                                  as interchange_repair\n          from    monthly_extract_summary sm\n          where   sm.bank_number = substr( :1  , 8, 4)\n                  and sm.active_date =  :2  \n                  and sm.load_file_id = load_filename_to_load_file_id( :3  )\n       ) icrep\n        on\n        (\n          mj.merchant_number = icrep.merchant_number\n          and mj.activity_month = icrep.activity_month\n        )\n        when matched then\n        update\n          set mj.interchange_repair = icrep.interchange_repair";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.MojaveSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setString(3,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:541^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("loadICRepair(" + activityDate.toString() + ")", e.toString());
      throw e;
    }
  }
  
  private void loadAuthCounts( Date activityDate, String loadFilename )
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:557^7*/

//  ************************************************************
//  #sql [Ctx] { merge into mojave mj
//          using 
//          (
//            select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */
//                    sm.active_date as activity_month,
//                    sm.merchant_number as merchant_number,
//                    nvl(sm.vmc_auth_count,0)+nvl(sm.amex_auth_count,0)+nvl(sm.disc_auth_count,0) auth_count
//            from    monthly_extract_summary sm
//            where   sm.bank_number = substr(:loadFilename, 8, 4)
//                    and sm.active_date = :activityDate
//                    and sm.load_file_id = load_filename_to_load_file_id(:loadFilename)
//          ) auth
//          on
//          (
//            mj.merchant_number = auth.merchant_number
//            and mj.activity_month = auth.activity_month
//          )                
//          when matched then
//          update
//            set mj.auth_count = auth.auth_count
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "merge into mojave mj\n        using \n        (\n          select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */\n                  sm.active_date as activity_month,\n                  sm.merchant_number as merchant_number,\n                  nvl(sm.vmc_auth_count,0)+nvl(sm.amex_auth_count,0)+nvl(sm.disc_auth_count,0) auth_count\n          from    monthly_extract_summary sm\n          where   sm.bank_number = substr( :1  , 8, 4)\n                  and sm.active_date =  :2  \n                  and sm.load_file_id = load_filename_to_load_file_id( :3  )\n        ) auth\n        on\n        (\n          mj.merchant_number = auth.merchant_number\n          and mj.activity_month = auth.activity_month\n        )                \n        when matched then\n        update\n          set mj.auth_count = auth.auth_count";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.MojaveSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setString(3,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:579^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("loadAuthCounts(" + activityDate.toString() + ")", e.toString());
      throw e;
    }
  }
  
  private void loadChargebacks( Date activityDate, String loadFilename )
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:595^7*/

//  ************************************************************
//  #sql [Ctx] { merge into mojave mj
//          using 
//          (
//            select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */
//                    sm.active_date as activity_month,
//                    sm.merchant_number as merchant_number,
//                    sum(nvl(st.st_fee_amount,0)) as chargeback_fees
//            from    monthly_extract_summary sm,
//                    monthly_extract_st st
//            where   sm.bank_number = substr(:loadFilename, 8, 4)
//                    and sm.active_date = :activityDate
//                    and sm.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                    and sm.hh_load_sec = st.hh_load_sec
//                    and st.st_statement_desc like '%CHARGEBACK FEE%'
//            group by sm.active_date, sm.merchant_number
//          ) cb
//          on
//          (
//            mj.merchant_number = cb.merchant_number
//            and mj.activity_month = cb.activity_month
//          )
//          when matched then
//          update
//            set mj.chargeback_fees = cb.chargeback_fees                        
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "merge into mojave mj\n        using \n        (\n          select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */\n                  sm.active_date as activity_month,\n                  sm.merchant_number as merchant_number,\n                  sum(nvl(st.st_fee_amount,0)) as chargeback_fees\n          from    monthly_extract_summary sm,\n                  monthly_extract_st st\n          where   sm.bank_number = substr( :1  , 8, 4)\n                  and sm.active_date =  :2  \n                  and sm.load_file_id = load_filename_to_load_file_id( :3  )\n                  and sm.hh_load_sec = st.hh_load_sec\n                  and st.st_statement_desc like '%CHARGEBACK FEE%'\n          group by sm.active_date, sm.merchant_number\n        ) cb\n        on\n        (\n          mj.merchant_number = cb.merchant_number\n          and mj.activity_month = cb.activity_month\n        )\n        when matched then\n        update\n          set mj.chargeback_fees = cb.chargeback_fees";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.MojaveSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setString(3,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:621^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("loadChargebacks(" + activityDate.toString() + ")", e.toString());
      throw e;
    }
  }
  
  private void loadMonthlyFees( Date activityDate, String loadFilename )
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:637^7*/

//  ************************************************************
//  #sql [Ctx] { merge into mojave mj
//          using 
//          (
//            select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */
//                    sm.active_date,
//                    sm.merchant_number,
//                    sum(nvl(st.st_fee_amount,0)) monthly_fees
//            from    monthly_extract_summary sm,
//                    monthly_extract_st st
//            where   sm.bank_number = substr(:loadFilename, 8, 4)
//                    and sm.active_date = :activityDate
//                    and sm.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                    and sm.hh_load_sec = st.hh_load_Sec
//                    and st.item_category like 'CG-%'
//            group by sm.active_date, sm.merchant_number
//          ) mon
//          on
//          (
//            mj.merchant_number = mon.merchant_number
//            and mj.activity_month = mon.active_date
//          )          
//          when matched then
//          update 
//            set mj.monthly_fees = mon.monthly_fees
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "merge into mojave mj\n        using \n        (\n          select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */\n                  sm.active_date,\n                  sm.merchant_number,\n                  sum(nvl(st.st_fee_amount,0)) monthly_fees\n          from    monthly_extract_summary sm,\n                  monthly_extract_st st\n          where   sm.bank_number = substr( :1  , 8, 4)\n                  and sm.active_date =  :2  \n                  and sm.load_file_id = load_filename_to_load_file_id( :3  )\n                  and sm.hh_load_sec = st.hh_load_Sec\n                  and st.item_category like 'CG-%'\n          group by sm.active_date, sm.merchant_number\n        ) mon\n        on\n        (\n          mj.merchant_number = mon.merchant_number\n          and mj.activity_month = mon.active_date\n        )          \n        when matched then\n        update \n          set mj.monthly_fees = mon.monthly_fees";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.MojaveSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setString(3,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:663^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("loadMonthlyFees(" + activityDate.toString() + ")", e.toString());
      throw e;
    }
  }
  
  private void loadOtherCharges( Date activityDate, String loadFilename )
    throws Exception
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:679^7*/

//  ************************************************************
//  #sql [Ctx] { merge into mojave mj
//          using 
//          (
//            select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */
//                    sm.active_date  as activity_month,
//                    sm.merchant_number as merchant_number,
//                    sum(nvl(st.st_fee_amount,0)) as other_charge_records
//            from    monthly_extract_summary sm,
//                    monthly_extract_st st
//            where   sm.bank_number = substr(:loadFilename, 8, 4)
//                    and sm.active_date = :activityDate
//                    and sm.load_file_id = load_filename_to_load_file_id(:loadFilename)
//                    and sm.hh_load_sec = st.hh_load_sec
//                    and st.item_category like 'CG-%'
//                    and st.st_statement_desc not in
//                    (
//                      'ANNUAL COMPLIANCE REPORTING FE',
//                      'IRS COMPLIANCE REPORTING (ANNUAL)',
//                      'ANNUAL COMPLIANCE REPORTING FEE',
//                      'COMPLIANCE REPORTING',
//                      'IRS COMPLIANCE REPORTING',
//                      'ANUAL COMPLIANCE REPORTING FEE',
//                      'ANNUAL IRS COMPLIANCE  REPORTING FEE',
//                      'ANNUAL COMPLIANCE REPORTING',
//                      'IRS COMPLIANCE REPORTING FEE',
//                      'IRS  COMPLIANCE REPORTING FEE',
//                      'COMPLIANCE REPORTING FEE',
//                      'ANNUAL PCI COMPLIANCE FEE-NOV.',
//                      'MONTHLY  PCI COMPLIANCE FEE',
//                      'MONTHLY PCI COMPLIANCE FEE - JUNE',
//                      'ANNUAL PCI COMPLIANCE',
//                      'PCI COMPLIANCE MONTHLY FEE',
//                      'ANNUAL PCI COMPLIANCE FEE',
//                      'MONTHLY PCI COMPLIANCE FEE',
//                      'MONTHLY PCI COMPLIANCE_FEE',
//                      'PCI COMPLIANCE FEE (ANNUAL)',
//                      'ANNUAL PCI COMPLIANCE REPORTING FEE',
//                      'ANNUAL PCI COMPLIANCE '||'&'||' REPORTING FEE',
//                      'PCI COMPLIANCE FEE',
//                      'QUARTERLY PCI VALIDATION PENALTY FEE',
//                      'ANNUAL ADMINISTRATIVE MAINTENANCE'
//                    )
//                    and st.st_statement_desc not like 'VISA MISUSE OF AUTH FEE%'
//                    and st.st_statement_desc not like 'VISA ZERO FLOOR%'
//            group by sm.active_date, sm.merchant_number  
//          ) other
//          on
//          (
//            mj.merchant_number = other.merchant_number
//            and mj.activity_month = other.activity_month
//          )          
//          when matched then
//          update 
//            set mj.other_charge_records = other.other_charge_records
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "merge into mojave mj\n        using \n        (\n          select  /*+ index (sm idx_mon_ext_summary_bnum_adate) */\n                  sm.active_date  as activity_month,\n                  sm.merchant_number as merchant_number,\n                  sum(nvl(st.st_fee_amount,0)) as other_charge_records\n          from    monthly_extract_summary sm,\n                  monthly_extract_st st\n          where   sm.bank_number = substr( :1  , 8, 4)\n                  and sm.active_date =  :2  \n                  and sm.load_file_id = load_filename_to_load_file_id( :3  )\n                  and sm.hh_load_sec = st.hh_load_sec\n                  and st.item_category like 'CG-%'\n                  and st.st_statement_desc not in\n                  (\n                    'ANNUAL COMPLIANCE REPORTING FE',\n                    'IRS COMPLIANCE REPORTING (ANNUAL)',\n                    'ANNUAL COMPLIANCE REPORTING FEE',\n                    'COMPLIANCE REPORTING',\n                    'IRS COMPLIANCE REPORTING',\n                    'ANUAL COMPLIANCE REPORTING FEE',\n                    'ANNUAL IRS COMPLIANCE  REPORTING FEE',\n                    'ANNUAL COMPLIANCE REPORTING',\n                    'IRS COMPLIANCE REPORTING FEE',\n                    'IRS  COMPLIANCE REPORTING FEE',\n                    'COMPLIANCE REPORTING FEE',\n                    'ANNUAL PCI COMPLIANCE FEE-NOV.',\n                    'MONTHLY  PCI COMPLIANCE FEE',\n                    'MONTHLY PCI COMPLIANCE FEE - JUNE',\n                    'ANNUAL PCI COMPLIANCE',\n                    'PCI COMPLIANCE MONTHLY FEE',\n                    'ANNUAL PCI COMPLIANCE FEE',\n                    'MONTHLY PCI COMPLIANCE FEE',\n                    'MONTHLY PCI COMPLIANCE_FEE',\n                    'PCI COMPLIANCE FEE (ANNUAL)',\n                    'ANNUAL PCI COMPLIANCE REPORTING FEE',\n                    'ANNUAL PCI COMPLIANCE '||'&'||' REPORTING FEE',\n                    'PCI COMPLIANCE FEE',\n                    'QUARTERLY PCI VALIDATION PENALTY FEE',\n                    'ANNUAL ADMINISTRATIVE MAINTENANCE'\n                  )\n                  and st.st_statement_desc not like 'VISA MISUSE OF AUTH FEE%'\n                  and st.st_statement_desc not like 'VISA ZERO FLOOR%'\n          group by sm.active_date, sm.merchant_number  \n        ) other\n        on\n        (\n          mj.merchant_number = other.merchant_number\n          and mj.activity_month = other.activity_month\n        )          \n        when matched then\n        update \n          set mj.other_charge_records = other.other_charge_records";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.MojaveSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setString(3,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:735^7*/

      commit();
    }
    catch(Exception e)
    {
      logEntry("loadOtherCharges(" + activityDate.toString() + ")", e.toString());
      throw e;
    }
  }
  
  public void loadData( ProcessTableEntry entry )
  {
    Date      activityDate    = null;
  
    try
    {
      log.debug("loading file " + entry.getLoadFilename());
      
      log.debug("getting active date");
      /*@lineinfo:generated-code*//*@lineinfo:755^7*/

//  ************************************************************
//  #sql [Ctx] { select  distinct active_date
//          
//          from    monthly_extract_summary   sm
//          where   sm.load_file_id = load_filename_to_load_file_id(:entry.getLoadFilename())
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_182 = entry.getLoadFilename();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  distinct active_date\n         \n        from    monthly_extract_summary   sm\n        where   sm.load_file_id = load_filename_to_load_file_id( :1  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.MojaveSummaryEvent",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_182);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:761^7*/
      
      // remove existing entries for this date and load_filename
      log.debug("removing old data");
      /*@lineinfo:generated-code*//*@lineinfo:765^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    mojave
//          where   activity_month = :activityDate
//                  and load_file_id = load_filename_to_load_file_id(:entry.LoadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    mojave\n        where   activity_month =  :1  \n                and load_file_id = load_filename_to_load_file_id( :2  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.MojaveSummaryEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,activityDate);
   __sJT_st.setString(2,entry.LoadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:771^7*/
    
      log.debug("Loading Main Data (" + activityDate.toString() + ")");
      loadMainData( activityDate, entry.getLoadFilename() );
      log.debug("Adding PCI Data");
      loadPCIData( activityDate, entry.getLoadFilename() );
      log.debug("Adding Durbin Data");
      loadDurbinData( activityDate, entry.getLoadFilename() );
      log.debug("Adding Compliance Data");
      loadComplianceData( activityDate, entry.getLoadFilename() );
      log.debug("Adding Debit Data");
      loadDebitEBT( activityDate, entry.getLoadFilename() );
      log.debug("Adding IC Repair");
      loadICRepair( activityDate, entry.getLoadFilename() );
      log.debug("Adding Auth Counts");
      loadAuthCounts( activityDate, entry.getLoadFilename() );
      log.debug("Adding Chargebacks");
      loadChargebacks( activityDate, entry.getLoadFilename() );
      log.debug("Adding Monthly Fees");
      loadMonthlyFees( activityDate, entry.getLoadFilename() );
      log.debug("Adding Other Charges");
      loadOtherCharges( activityDate, entry.getLoadFilename() );
      log.debug("DONE");
    }
    catch(Exception e)
    {
      logEntry("loadData(" + activityDate.toString() + ")", e.toString());
    }
  }
  
  public static void main(String[] args)
  {
    MojaveSummaryEvent         testEvent   = null;

    try
    {
    	testEvent = new MojaveSummaryEvent();
    	if(args.length > 0) {
    	
    		testEvent.connect(true);
    		
    		String loadFilename = args[0];
    		
    		Calendar cal = Calendar.getInstance();
        	cal.setTime( DateTimeFormatter.parseDate(args[1],"MM/dd/yyyy") );        
			Date activityDate = new java.sql.Date( cal.getTime().getTime() ); 
			    		
    		testEvent.loadMainData( activityDate, loadFilename );
      		log.debug("Adding PCI Data");
      		
      		testEvent.loadPCIData( activityDate, loadFilename );
		    log.debug("Adding Durbin Data");
		    
		    testEvent.loadDurbinData( activityDate, loadFilename );
		    log.debug("Adding Compliance Data");
		    
		    testEvent.loadComplianceData( activityDate, loadFilename );
		    log.debug("Adding Debit Data");
		    
		    testEvent.loadDebitEBT( activityDate, loadFilename );
		    log.debug("Adding IC Repair");
		    
		    testEvent.loadICRepair( activityDate, loadFilename );
		    log.debug("Adding Auth Counts");
		    
		    testEvent.loadAuthCounts( activityDate, loadFilename );
		    log.debug("Adding Chargebacks");
		    
		    testEvent.loadChargebacks( activityDate, loadFilename );
		    log.debug("Adding Monthly Fees");
		    
		    testEvent.loadMonthlyFees( activityDate, loadFilename );
		    log.debug("Adding Other Charges");
		    
		    testEvent.loadOtherCharges( activityDate, loadFilename );
    	}
    	else {
		      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
		      
		      testEvent.execute();
		}
      
      //String loadFilename = args[0];
      
      //ProcessTableEntry entry = new ProcessTable.ProcessTableEntry();
      //entry.setLoadFilename(loadFilename);
      //test.loadData(entry);
    }
    catch(Exception e)
    {
    }
    finally
    {
      try{ testEvent.cleanUp(); } catch(Exception e){}
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/