/*@lineinfo:filename=GlobalEventManager*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

 FILE: $Archive: /Java/beans/com/mes/events/GlobalEventManager.sqlj $

 Description:


 Last Modified By   : $Author: vbannikov $
 Last Modified Date : $Date: 2015-07-29 13:51:34 -0700 (Wed, 29 Jul 2015) $
 Version            : $Revision: 23755 $

 Change History:
 See VSS database

 Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
 All rights reserved, Unauthorized distribution prohibited.

 This document contains information which is the proprietary
 property of Merchant e-Solutions, Inc.  This document is received in
 confidence and its contents may not be disclosed without the
 prior written consent of Merchant e-Solutions, Inc.

 **************************************************************************/
package com.mes.events;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.config.ConfigurationManager;
import com.mes.config.DbProperties;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.support.LoggingConfigurator;
import com.mes.support.PropertiesFile;
import kong.unirest.Unirest;
import sqlj.runtime.ResultSetIterator;

public class GlobalEventManager extends SQLJConnectionBase
{
  static { LoggingConfigurator.requireConfiguration(); } // initialize log4j

  static Logger log = Logger.getLogger(GlobalEventManager.class);

  static { DbProperties.requireConfiguration(); } // initialize database properties

  public static final String  EVENT_PROPERTIES_FILENAME   = "event.properties";

  private long                creationTS    = System.currentTimeMillis();
  private Vector              events        = new Vector();
  private boolean             eventsChanged = false;
  private boolean             shutDown      = false;
  private long                sleepTime     = TimedEvent.MINUTE_MILLIS;
  private String              serverIp      = "";
  private ServerListenThread  listenThread  = null;
  private PropertiesFile      eventProps    = null;

  private Hashtable           WeedsNotifications = new Hashtable();

  public GlobalEventManager() {
  }

  /**
   * Load event properties file.  Includes properties such as the control port number.
   */
  private void loadEventManagerProperties() {

    try {
      // open main events properties file
      eventProps = new PropertiesFile(EVENT_PROPERTIES_FILENAME);
      log.info("Event properties file loaded: " + EVENT_PROPERTIES_FILENAME);
    }
    catch(Exception e) {
      log.error("Event properties file " + EVENT_PROPERTIES_FILENAME + " failed to load",e);
      // throw runtime exception to halt event manager
      throw new RuntimeException(e);
    }

  }

  /**
   * Load event properties file.  Includes properties such as the control port number.
   */
  private void loadGlobalProperties() {

    try {
      // open main events properties file
      ConfigurationManager.getInstance().loadTEMConfiguration(ConfigurationManager.TEM_PROPERTY_FILE_DEFAULT);
      log.info("Global properties file loaded: " + ConfigurationManager.TEM_PROPERTY_FILE_DEFAULT);
    }
    catch(Exception e) {
      log.error("Global properties file " + ConfigurationManager.TEM_PROPERTY_FILE_DEFAULT + " failed to load",e);
      // throw runtime exception to halt event manager
      throw new RuntimeException(e);
    }

  }

  public class ServerListenThread extends Thread
  {
    private ServerSocket        serverSocket  = null;
    private GlobalEventManager  manager       = null;
    private boolean             listening     = true;

    public ServerListenThread(GlobalEventManager manager, PropertiesFile eventProps)
    {
      this.manager = manager;
    }

    public void setListening(boolean listening)
    {
      this.listening = listening;
    }

    public void run()
    {
      try
      {
        int serverPort = eventProps.getInt("com.mes.listen.port", MesDefaults.getInt(MesDefaults.DK_EVENT_SERVER_LISTEN_PORT));
        //int serverPort = MesDefaults.getInt(MesDefaults.DK_EVENT_SERVER_LISTEN_PORT);

        System.out.println("Creating serversocket on port " + serverPort);
        serverSocket = new ServerSocket(serverPort);

        serverSocket.setSoTimeout(1000);

        while(listening)
        {
          try
          {
            new ServerThread(manager, serverSocket.accept()).start();
          }
          catch(java.io.InterruptedIOException inte)
          {
            // just interrupting but continue listening
          }
        }

        System.out.println("ServerListenThread.run(): shutting down");
        serverSocket.close();
      }
      catch(Exception e)
      {
        System.out.println("ServerListenThread.run(): " + e.toString());
        //logEntry("startListening()", e.toString());
      }
    }
  }

  /**
   * Start listener thread to monitor incoming messages on control port.
   */
  private void initializeControlPort() {

    try {
      listenThread = new ServerListenThread(this, eventProps);
      listenThread.start();
    }
    catch(Exception e) {
      log.error("Error starting control port listener thread",e);
      // throw runtime exception to halt event manager
      throw new RuntimeException(e);
    }
  }

  private void start()
  {
    boolean active = true;

    try
    {

      // loop until the system is shutdown and
      // there are no longer active events running
      while( !shutDown || active )
      {
        try
        {
          active = false;   // reset events active flag

          synchronized(events)
          {
            for(int i = 0; i < events.size(); ++i)
            {
              TimedEvent te = (TimedEvent)events.elementAt(i);

              if( te.status == TimedEvent.TE_STATUS_IDLE )
              {
                if ( te.eventModified() )
                {
                  reloadEvent(te);
                }

                // skip disabled events or all events if the
                // manager is shutting down.
                if ( !te.enabled || shutDown )
                {
                  continue;
                }

                // only start an event processing if we're not in the process
                // of shutting down
                if( te.timeToProcess() )
                {
                  // create a thread to run this process
                  //System.out.println("Processing " + te.name);
                  te.process();
                  active = true;    // an event is running
                }
              }
              else // the event is already running
              {
                active = true;

                // check to see if this event is in the weeds
                if( te.eventMonitored == true )
                {
                  Calendar curDate = te.pastTimeToProcess();
                  if( curDate != null )
                  {
                    notifyInTheWeeds(te, curDate);
                  }
                }
              }
            }
          }

          // sleep 100ms while pending shutdown
          Thread.sleep( (shutDown ? 100 : sleepTime) );
        }
        catch(InterruptedException inte)
        {
          System.out.println("InterruptedException: " + inte.toString());
          shutDown = true;
        }
      }
      log.info("Exiting event manager loop");
    }
    catch(Exception e)
    {
      logEntry("start()", e.toString());
    }
    finally
    {
      if(!isConnectionStale())
      {
        cleanUp();
      }

      // stop server thread
      listenThread.setListening(false);
    }
  }

  private void notifyInTheWeeds( TimedEvent te, Calendar curDate )
  {
    boolean notify = false;

    try
    {
      Integer curId = te.id;

      // get current timestamp
      long curTime = System.currentTimeMillis();

      // get last time this notification happened
      Long lastNotified = (Long)(WeedsNotifications.get(curId));

      if( lastNotified == null || curTime - lastNotified.longValue() > 900000L ) // 15 minutes
      {
        MailMessage   msg     = new MailMessage();
        StringBuffer  body    = new StringBuffer();

        body.append("Timed Event " + te.name + " appears to be stalled.\n\n");
        body.append("Last successful trigger: ");
        body.append(DateTimeFormatter.getFormattedDate(te.lastTriggered, "MM/dd/yy hh:mm:ss a"));
        body.append("\n");
        body.append("Current Date/Time      : ");
        body.append(DateTimeFormatter.getFormattedDate(curDate.getTime(), "MM/dd/yy hh:mm:ss a"));
        body.append("\n");

        msg.setAddresses(MesEmails.MSG_ADDRS_TIMED_EVENT_MGR);
        msg.setSubject("Timed Event " + te.name + " is stalled");
        msg.setText(body.toString());
        msg.send();

        // add latest notify time to table
        WeedsNotifications.put(curId, curTime);
      }
    }
    catch(Exception e)
    {
      logEntry("notifyInTheWeeds()", e.toString());
    }
  }

  public void unirestDestroyed() {
    try {
      log.info("Uniresst Destroying ");
      Unirest.shutDown();
    }
    catch (Exception e) {
      log.error("Error Uniresst Destroying", e);
    }
  }

  public void shutdown() {
    // stop server loop
    shutDown = true;
    log.info("Shutdown initiated.");
    unirestDestroyed();
  }

  // TODO:  this is next to useless with multiple timed event managers running,
  //        need to remove or update to store useful info for multiple managers
  private void configure() {

    log.debug("Configuring...");

    try {
      connect();

      sleepTime = TimedEvent.MINUTE_MILLIS;

      /*@lineinfo:generated-code*//*@lineinfo:328^7*/

//  ************************************************************
//  #sql [Ctx] { select  sleep_time
//          
//          from    timed_event_config
//          where   id = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sleep_time\n         \n        from    timed_event_config\n        where   id = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.events.GlobalEventManager",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sleepTime = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:334^7*/

      // update timed_event_config to show the local server's ip address
      if (HttpHelper.isProdServer(null)) {
        /*@lineinfo:generated-code*//*@lineinfo:338^9*/

//  ************************************************************
//  #sql [Ctx] { update  timed_event_config
//            set     server_address = :InetAddress.getLocalHost().getHostAddress()
//            where   id = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3637 = InetAddress.getLocalHost().getHostAddress();
   String theSqlTS = "update  timed_event_config\n          set     server_address =  :1 \n          where   id = 1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.events.GlobalEventManager",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3637);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:343^9*/

        commit();
      }

      log.info("Configuration complete.");
    }
    catch(Exception e) {
      log.error("Error in configure()",e);
    }
    finally {
      cleanUp();
    }
  }

  public long getUpTime()
  {
    return( ( System.currentTimeMillis() - creationTS ) );
  }

  public boolean isShutdown()
  {
    return(shutDown);
  }

  private String getEventDescriptor(TimedEvent te) {

    StringBuffer buf = new StringBuffer();

    buf.append(te.name);
    for(int j = 0; j < 20 - te.name.length(); ++j) buf.append(" ");

    buf.append(DateTimeFormatter.getFormattedDate(te.lastTriggered,
            DateTimeFormatter.DEFAULT_DATE_TIME_FORMAT));

    buf.append(": triggers ");

    // build info about timing
    switch(te.type) {

      case TimedEvent.EVENT_FREQUENTLY:
        buf.append("every ");
        buf.append(te.frequencyMinutes);
        buf.append(" minutes on these days (SMTWTFSS): ");
        buf.append(te.daysOfWeek);
        buf.append(" between the hours of ");
        buf.append(te.startTime);
        buf.append(" and ");
        buf.append(te.endTime);
        break;

      case TimedEvent.EVENT_DAILY:
        buf.append("daily, ");
        break;

      case TimedEvent.EVENT_MONTHLY:
        buf.append("monthly, on day ");
        buf.append(te.dayOfMonth);
        buf.append(" of months: ");
        buf.append(te.monthsOfYear);
        break;
    }

    return ""+buf;
  }

  private void logEventsInfo() {

    try {
      StringBuffer buf = new StringBuffer();
      synchronized(events) {
        for (Iterator i = events.iterator(); i.hasNext();) {
          TimedEvent te = (TimedEvent)i.next();
          buf.append("\n" + getEventDescriptor(te));
        }
      }
      log.info("Managed Events:" + buf);
    }
    catch(Exception e) {
      log.error("Error logging event info",e);
      e.printStackTrace();
    }
  }

  private void loadTimedEvents() {

    ResultSetIterator   it    = null;
    ResultSet           rs    = null;

    try  {
      connect();
      events.removeAllElements();   // reset events vector

      // get current list of events from database
      /*@lineinfo:generated-code*//*@lineinfo:443^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  event_id                  as event_id,
//                event_type                as event_type,
//                event_name                as event_name,
//                event_enabled             as event_enabled,
//                event_classname           as event_classname,
//                nvl(last_triggered,
//                        to_timestamp('01/01/2000','mm/dd/yyyy'))
//                as last_triggered,
//                start_time                as start_time,
//                end_time                  as end_time,
//                frequency_minutes         as frequency_minutes,
//                days_of_week              as days_of_week,
//                day_of_month              as day_of_month,
//                months_of_year            as months_of_year,
//                dev_name                  as dev_name,
//                event_parameters          as event_args,
//                monitor                   as event_monitored,
//                nvl(timeout_minutes,0)    as timeout_minutes,
//                event_desc				  as event_desc
//                from    timed_events
//                where   dev_name = :getHostName()
//        order by event_id
//  
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3638 = getHostName();
  try {
   String theSqlTS = "select  event_id                  as event_id,\n              event_type                as event_type,\n              event_name                as event_name,\n              event_enabled             as event_enabled,\n              event_classname           as event_classname,\n              nvl(last_triggered,\n                      to_timestamp('01/01/2000','mm/dd/yyyy'))\n              as last_triggered,\n              start_time                as start_time,\n              end_time                  as end_time,\n              frequency_minutes         as frequency_minutes,\n              days_of_week              as days_of_week,\n              day_of_month              as day_of_month,\n              months_of_year            as months_of_year,\n              dev_name                  as dev_name,\n              event_parameters          as event_args,\n              monitor                   as event_monitored,\n              nvl(timeout_minutes,0)    as timeout_minutes,\n              event_desc\t\t\t\t  as event_desc\n              from    timed_events\n              where   dev_name =  :1 \n      order by event_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.events.GlobalEventManager",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3638);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.events.GlobalEventManager",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:468^7*/

      rs = it.getResultSet();

      while (rs.next()) {
        events.add(new TimedEvent(rs, eventProps));
      }

      rs.close();
      it.close();

      logEventsInfo();
    }
    catch(Exception e) {
      log.error("Failed to load timed events",e);
      throw new RuntimeException(e);
    }
    finally {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  private void reloadEvent( TimedEvent te )
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;

    try
    {
      connect();

      System.out.println("Reloading event " + te.id);

      // get current list of events from database
      /*@lineinfo:generated-code*//*@lineinfo:504^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  event_id                  as event_id,
//                      event_type                as event_type,
//                      event_name                as event_name,
//                      event_enabled             as event_enabled,
//                      event_classname           as event_classname,
//                      nvl(last_triggered,
//                              to_timestamp('01/01/2000','mm/dd/yyyy'))
//                      as last_triggered,
//                      start_time                as start_time,
//                      end_time                  as end_time,
//                      frequency_minutes         as frequency_minutes,
//                      days_of_week              as days_of_week,
//                      day_of_month              as day_of_month,
//                      months_of_year            as months_of_year,
//                      dev_name                  as dev_name,
//                      monitor                   as event_monitored,
//                      nvl(timeout_minutes,0)    as timeout_minutes,
//                      event_desc				  as event_desc
//                      from    timed_events    te
//                      where   te.event_id = :te.id and
//              (
//        :HttpHelper.isDevServer(null) ? "Y" : "N" = 'N' and
//        dev_name is null
//        ) or
//              (
//        :HttpHelper.isDevServer(null) ? "Y" : "N" = 'Y' and
//        dev_name = :getHostName()
//        )
//        order by event_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3639 = HttpHelper.isDevServer(null) ? "Y" : "N";
 String __sJT_3640 = HttpHelper.isDevServer(null) ? "Y" : "N";
 String __sJT_3641 = getHostName();
  try {
   String theSqlTS = "select  event_id                  as event_id,\n                    event_type                as event_type,\n                    event_name                as event_name,\n                    event_enabled             as event_enabled,\n                    event_classname           as event_classname,\n                    nvl(last_triggered,\n                            to_timestamp('01/01/2000','mm/dd/yyyy'))\n                    as last_triggered,\n                    start_time                as start_time,\n                    end_time                  as end_time,\n                    frequency_minutes         as frequency_minutes,\n                    days_of_week              as days_of_week,\n                    day_of_month              as day_of_month,\n                    months_of_year            as months_of_year,\n                    dev_name                  as dev_name,\n                    monitor                   as event_monitored,\n                    nvl(timeout_minutes,0)    as timeout_minutes,\n                    event_desc\t\t\t\t  as event_desc\n                    from    timed_events    te\n                    where   te.event_id =  :1  and\n            (\n       :2  = 'N' and\n      dev_name is null\n      ) or\n            (\n       :3  = 'Y' and\n      dev_name =  :4 \n      )\n      order by event_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.events.GlobalEventManager",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,te.id);
   __sJT_st.setString(2,__sJT_3639);
   __sJT_st.setString(3,__sJT_3640);
   __sJT_st.setString(4,__sJT_3641);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.events.GlobalEventManager",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:535^7*/
      rs = it.getResultSet();

      if( rs.next() )
      {
        te.setData(rs);
        te.setEventModified(false);   // reset modified flag
      }
      rs.close();
      it.close();
    }
    catch(Exception e)
    {
      log.error("Failed to reload event",e);
    }
    finally
    {
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
  }

  public void setEventEnableFlag( EventMessage.EventEnableMessage.EventEnableData data )
  {
    int       eventId     = data.EventId;
    String    eventFlag   = (data.Enabled ? "Y" : "N");

    try
    {
      connect();

      System.out.println("Setting enable flag to " + eventFlag + " for event " + eventId);
      /*@lineinfo:generated-code*//*@lineinfo:567^7*/

//  ************************************************************
//  #sql [Ctx] { update  timed_events
//          set     event_enabled = :eventFlag
//          where   event_id = :eventId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  timed_events\n        set     event_enabled =  :1 \n        where   event_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.events.GlobalEventManager",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,eventFlag);
   __sJT_st.setInt(2,eventId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:572^7*/

      /*@lineinfo:generated-code*//*@lineinfo:574^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:577^7*/

      // since this is running in a thread other than the
      // main manager thread, just mark this event as
      // changed and allow the main thread to update the
      // event data from the database when the event goes idle
      for(int i = 0; i < events.size(); ++i)
      {
        TimedEvent te = (TimedEvent)events.elementAt(i);

        if ( te.id == eventId )
        {
          te.setEventModified(true);
          break;
        }
      }
    }
    catch( Exception e )
    {
      logEntry("setEventEnableFlag()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }

  public class ServerThread extends Thread
  {
    private Socket                  socket    = null;
    private GlobalEventManager      manager   = null;

    public ServerThread(GlobalEventManager manager, Socket socket)
    {
      super("EventServerThread");
      this.manager = manager;
      this.socket = socket;
    }

    public void run()
    {
      StringBuffer    inputData   = new StringBuffer("");

      try
      {
        // get message
        EventMessage  em = new EventMessage(socket);

        // perform requested action
        performAction(em.action, em.data);
      }
      catch(Exception e)
      {
        System.out.println("ServerThread.run(): " + e.toString());
      }
      finally
      {
        try{ socket.close(); } catch(Exception e) {}
      }
    }

    private void performAction(int action, Object data)
    {
      EventMessage    reply   = null;

      try
      {
        switch(action)
        {
          case EventMessage.ACTION_SHUTDOWN:
            // tell manager to shut everything down
            manager.shutdown();

            // acknowledge request to shutdown
            reply = new EventMessage.AckEventMessage();

            reply.send(socket);
            break;

          case EventMessage.ACTION_STATUS_REQUEST:
            // get events vector from manager and send it back
            synchronized(manager.events)
            {
              reply = new EventMessage(EventMessage.ACTION_STATUS_RESULT, manager.events);
              reply.send(socket);
            }
            break;

          case EventMessage.ACTION_UP_TIME:
            reply = new EventMessage(EventMessage.ACTION_UP_TIME_RESP, manager.getUpTime());
            reply.send(socket);
            break;

          case EventMessage.ACTION_SHUTDOWN_STATUS:
            reply = new EventMessage(EventMessage.ACTION_SHUTDOWN_STATUS_RESP, manager.isShutdown());
            reply.send(socket);
            break;

          case EventMessage.ACTION_SET_ENABLE_FLAG:
            manager.setEventEnableFlag((EventMessage.EventEnableMessage.EventEnableData)data);

            // acknowledge request to update enable flag
            reply = new EventMessage.AckEventMessage();
            reply.send(socket);
            break;

          default:
            break;
        }
      }
      catch(Exception e)
      {
        System.out.println("ServerThread.performAction(" + action + "): " + e.toString());
      }
    }
  }

  /**
   * Perform initial startup configuration.
   */
  private void initialize() {
    loadGlobalProperties();
    loadEventManagerProperties();
    // TODO: replace useless configure() with 
    //registerEventManager();
    configure();
    initializeControlPort();
    loadTimedEvents();
  }

  public static void main(String args[]) {

    try {

      log.info("GlobalEventManager starting");

      /*
       * legacy command line argument scheme:
       *
       * 0 arguments
       *
       *  default error level of ERROR (no longer supported)
       *
       * 1 arguments
       *
       *  parm 1 = logging error level (no longer supported)
       *
       * 2+ arguments
       *
       *  parm 1 = Y/N connection pool indicator,
       *  parm 2 = logging error level (no longer supported)
       *
       * New logging configuration obsoletes this.  Now if
       * 1 or more arguments exist use first argument as
       * pool indicator: Y means use connection pool.
       */

      // initialize db connection configuration
      boolean poolFlag = false;
      if(args.length >= 1 )
      {
        poolFlag = "Y".equals(args[0]);
      }
      SQLJConnectionBase.initStandalone(poolFlag);

      // instantiate a manager
      GlobalEventManager  manager = new GlobalEventManager();

      // perform startup configuration and initialization
      manager.initialize();

      // start the engine running
      manager.start();
    }
    catch(Throwable t) {
      log.error("Error or exception thrown in GlobalEventManager",t);
    }
    finally {
      log.info("GlobalEventManager stopping");
    }

    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/