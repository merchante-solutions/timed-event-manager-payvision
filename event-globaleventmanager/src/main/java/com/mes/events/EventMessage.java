/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/events/EventMessage.java $

  Description:
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/31/02 1:20p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.events;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;

public class EventMessage
  implements java.io.Serializable
{
  public static final int   ACTION_SHUTDOWN               = 1;
  public static final int   ACTION_STATUS_REQUEST         = 2;
  public static final int   ACTION_STATUS_RESULT          = 3;
  public static final int   ACTION_RESTART                = 4;
  public static final int   ACTION_SET_ENABLE_FLAG        = 5;
  public static final int   ACTION_SHUTDOWN_STATUS        = 6;
  public static final int   ACTION_SHUTDOWN_STATUS_RESP   = 7;
  public static final int   ACTION_UP_TIME                = 8;
  public static final int   ACTION_UP_TIME_RESP           = 9;
  
  public static final int   ACTION_ACK                    = 99;
  
  protected int       action;
  protected Object    data;
  
  public EventMessage()
  {
  }
  
  public EventMessage(int action, Object data)
  {
    this.action = action;
    this.data = data;
  }
  
  public EventMessage(Socket s)
  {
    try
    {
      // retreives message from the socket
      ObjectInputStream socketis = new ObjectInputStream(s.getInputStream());
    
      EventMessage em = new EventMessage();
    
      em = (EventMessage)(socketis.readObject());
    
      this.action = em.action;
      this.data = em.data;
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("EventMessage(Socket s)", e.toString());
    }
  }
  
  public void send(Socket s)
  {
    try
    {
      // make sure data is not null
      if(data == null)
      {
        data = new String("");
      }
      
      ObjectOutputStream socketos = new ObjectOutputStream(s.getOutputStream());
      
      socketos.writeObject(this);
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("EventMessage.send()", e.toString());
    }
  }
  
  public static class AckEventMessage extends EventMessage
  {
    public AckEventMessage()
    {
      super(ACTION_ACK, null);
    }
  }
  
  public static class ShutdownEventMessage extends EventMessage
  {
    public ShutdownEventMessage()
    {
      super(ACTION_SHUTDOWN, null);
    }
  }
  
  public static class StatusEventMessage extends EventMessage
  {
    public StatusEventMessage()
    {
      super(ACTION_STATUS_REQUEST, null);
    }
  }
  
  public static class RestartEventMessage extends EventMessage
  {
    public RestartEventMessage()
    {
      super(ACTION_RESTART, null);
    }
  }
  
  public static class ShutdownStatusMessage extends EventMessage
  {
    public ShutdownStatusMessage()
    {
      super(ACTION_SHUTDOWN_STATUS, null);
    }
  }
  
  public static class EventEnableMessage extends EventMessage
  {
    public static class EventEnableData implements Serializable
    {
      public boolean        Enabled     = false;
      public int            EventId     = -1;
  
      public EventEnableData()
      {
      }
  
      public EventEnableData(int eventId, boolean enabled)
      {
        EventId = eventId;
        Enabled = enabled;
      }
    }
  
    public EventEnableMessage( int eventId, boolean enabled )
    {
      super(ACTION_SET_ENABLE_FLAG, new EventEnableData(eventId,enabled));
    }
  }
}
