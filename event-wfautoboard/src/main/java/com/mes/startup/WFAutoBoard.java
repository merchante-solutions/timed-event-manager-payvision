/*@lineinfo:filename=WFAutoBoard*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.Vector;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import com.mes.api.TridentApiTranBase;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.net.MailMessage;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;


public class WFAutoBoard extends TsysFileBase
{
  static Logger log = Logger.getLogger(WFAutoBoard.class);
  
  private static String WF_NAMESPACE_URI = "http://service.wellsfargo.com/provider/MerchantServices/GPG/postRegistrations/2008/";
  
  private boolean TestMode = false;

  private Document  doc             = null;  
  
  private String    OutFileName     = "";
  private String    EntityId        = "";
  private String    AgentId         = "";
  private String    CurDateString   = "";
  private String    CurTimestamp    = "";
  private String    FileDateString  = "";
  private String    BatchId         = "";
  
  private class WFAutoBoardItem
  {
    public String merchantNumber;
    public String action;
    public String dbaName;
    public String appType;
    
    public WFAutoBoardItem(ResultSet rs)
    {
      try
      {
        merchantNumber  = rs.getString("merchant_number");
        action          = rs.getString("action");
        dbaName         = rs.getString("dba_name");
        appType         = rs.getString("appsrctype_code");
      }
      catch(Exception e)
      {
        logEntry("WFAutoBoardItem() constructor", e.toString());
      }
    }
    
    public WFAutoBoardItem(String merchantNumber, String action)
    {
      this.merchantNumber = merchantNumber;
      this.action = action;
    }
  }
  
  public WFAutoBoard( )
  {
    this( false );
  }
  
  public WFAutoBoard( boolean testMode )
  {
    TestMode = testMode;
    
    PropertiesFilename = "wellsfargo.properties";
    EmailGroupSuccess = MesEmails.MSG_ADDRS_OUTGOING_WFAB_NOTIFY;   
    EmailGroupFailure = MesEmails.MSG_ADDRS_OUTGOING_WFAB_FAILURE;  
  }
  
  /*
  ** String generateFilename()
  **
  ** Generates filename for outgoing XML file to Wells Fargo. Format is:
  **
  ** xxxx_reg_ccyymmdd_bbb.xml
  **
  ** xxxx = Entity Id
  ** ccyymmdd = Date
  ** bbb = daily sequence number, starting with 001 each day
  */
  private String generateFilename()
  {
    StringBuffer result = new StringBuffer("");
    
    try
    {
      // establish entity id based on test mode
      if( TestMode == true )
      {
        EntityId  = MesDefaults.getString(MesDefaults.DK_WF_ENTITY_ID_UAT);
        AgentId   = MesDefaults.getString(MesDefaults.DK_WF_AGENT_ID_UAT);
      }
      else
      {
        EntityId  = MesDefaults.getString(MesDefaults.DK_WF_ENTITY_ID_PROD);
        AgentId   = MesDefaults.getString(MesDefaults.DK_WF_AGENT_ID_PROD);
      }
              
      // establish current date string in format expected by Wells Fargo
      /*@lineinfo:generated-code*//*@lineinfo:117^7*/

//  ************************************************************
//  #sql [Ctx] { select  to_char(sysdate, 'yyyymmdd'),
//                  to_char(systimestamp, 'yyyymmddhh24missff3'),
//                  to_char(sysdate, 'mmddyy')
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  to_char(sysdate, 'yyyymmdd'),\n                to_char(systimestamp, 'yyyymmddhh24missff3'),\n                to_char(sysdate, 'mmddyy')\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.WFAutoBoard",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   CurDateString = (String)__sJT_rs.getString(1);
   CurTimestamp = (String)__sJT_rs.getString(2);
   FileDateString = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:126^7*/

      BatchId = NumberFormatter.getPaddedInt( getFileId( FileDateString ), 3 );
              
      result.append(EntityId);
      result.append("_reg_");
      result.append(CurDateString);
      result.append("_");
      result.append( BatchId );
      result.append(".xml");
    }
    catch(Exception e)
    {
      logEntry("generateFilename()", e.toString());
    }              
              
    return( result.toString() );              
  }
  
  private Element buildHeader()
  {
    Element header = new Element("Header");
    
    header.addContent(new Element( "MessageVersion").setText("1.0") );
    header.addContent(new Element( "ServiceProviderID").setText(EntityId) );
    header.addContent(new Element( "SubmissionBatchID").setText(CurDateString+BatchId) );
    header.addContent(new Element( "CreationTimeStamp").setText(CurTimestamp) );
    
    return( header );
  }
  
  private Element buildXMLChunk(String chunkName, ResultSet rs, boolean ignoreEmpty )
    throws Exception
  {
    StringBuffer elementName    = new StringBuffer();
    ResultSetMetaData metaData  = rs.getMetaData();
  
    Element chunk = new Element(chunkName);
    
    // convert one_two_three to OneTwoThree
    for( int col = 1; col <= metaData.getColumnCount(); ++col )
    {
      // first character is upper case by default
      boolean upper = true;
      
      // start with an all-lower-case version of the column name
      String columnName = metaData.getColumnName(col).toLowerCase();
      
      elementName.setLength(0);
      for( int i=0; i < columnName.length(); ++i )
      {
        if( columnName.charAt(i) == '_' )
        {
          // uppercase the next letter and continue
          upper = true;
          continue;
        }
        
        if( upper == true )
        {
          elementName.append( columnName.toUpperCase().charAt(i) );
          upper = false;
        }
        else
        {
          elementName.append( columnName.charAt(i) );
        }
      }
      
      // add element
      if( ignoreEmpty && TridentApiTranBase.isBlank( rs.getString(columnName) ) )
      {
        // skip this element
        continue;
      }

      chunk.addContent( new Element(elementName.toString()).setText(rs.getString(columnName)) );
    }
    
    return( chunk );
  }
  
//  private Element buildEntitlementList(String merchantNumber) throws Exception
//  {
//    ResultSetIterator it = null;
//    ResultSet         rs = null;
//    
//    Element entitlementList = new Element("EntitlementList");
//    
//    try
//    {
//      #sql[Ctx] it =
//      {
//        select  'VS'                      as entitlement_type,
//                '0'                       as entitlement_s_e_number                   
//        from    trident_profile 
//        where   merchant_number = :merchantNumber
//                and vmc_accept = 'Y'
//        union
//        select  'MC'                      as entitlement_type,
//                '0'                       as entitlement_s_e_number                   
//        from    trident_profile 
//        where   merchant_number = :merchantNumber
//                and vmc_accept = 'Y'
//        union
//        select  'DISC'                    as entitlement_type,
//                '0'                       as entitlement_s_e_number                   
//        from    trident_profile 
//        where   merchant_number = :merchantNumber
//                and disc_accept = 'Y'
//        union
//        select  'AMEX'                    as entitlement_type,
//                substr(amex_caid, 1, 15)  as entitlement_s_e_number                   
//        from    trident_profile 
//        where   merchant_number = :merchantNumber
//                and amex_accept = 'Y'
//      };
//      
//      rs = it.getResultSet();
//      
//      while (rs.next())
//      {
//        entitlementList.addContent( buildXMLChunk("Entitlement", rs, true));
//      }
//    }
//    catch(Exception e)
//    {
//      logEntry("buildEntitlementList(" + merchantNumber + ")", e.toString());
//      throw( e );
//    }
//    finally
//    {
//      try{ rs.close(); } catch(Exception e) {}
//      try{ it.close(); } catch(Exception e) {}
//    }
//    
//    return( entitlementList);
//  }
  
//  private Element buildRetailRiskInfo(String merchantNumber)
//    throws Exception
//  {
//    ResultSetIterator it = null;
//    ResultSet         rs = null;
//    
//    Element retailRiskInfo = null;
//    
//    try
//    {
//      #sql[Ctx] it =
//      {
//        select  nvl(si.siteinsp_name_flag,'N')                  as signage_match_d_b_a,
//                nvl(si.siteinsp_inv_sign_flag,'N')              as permanent_signage,
//                substr(nvl(si.floor_occupied,'112'),1,3)        as floor_occupied,
//                substr(decode(si.floor_occupied,
//                  '113', si.floor_occupied_other,
//                  'Ground Floor'),1,60)                         as floor_occupied_others,
//                substr(nvl(si.completed_by,'????'),1,25)        as onsite_completed_name,
//                substr(nvl(si.completed_by_title,'????'),1,25)  as onsite_completed_title,
//                to_char(
//                  nvl(si.completed_date,app.app_created_date), 
//                  'yyyymmdd')                                   as onsite_completed_date                   
//        from    merchant mr,
//                siteinspection si,
//                application app
//        where   mr.merch_number = :merchantNumber
//                and mr.app_seq_num = si.app_seq_num(+)
//                and mr.app_seq_num = app.app_seq_num
//      };
//      
//      rs = it.getResultSet();
//      
//      if(rs.next())
//      {
//        retailRiskInfo = buildXMLChunk("RetailRiskInfo", rs, true);
//      }
//    }
//    catch(Exception e)
//    {
//      logEntry("buidRetailRiskInfo(" + merchantNumber + ")", e.toString());
//      throw( e );
//    }
//    finally
//    {
//      try{ rs.close(); } catch(Exception e) {}
//      try{ it.close(); } catch(Exception e) {}
//    }
//    
//    return( retailRiskInfo );
//  }
  
  private Element buildBusinessList(String merchantNumber)
    throws Exception
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    Element businessInfo = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:327^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  substr(mf.dba_name,1,24)                        as d_b_a_name,
//                  substr(nvl(mf.fdr_corp_name,mf.dba_name),1,24)  as legal_name,
//                  lpad(mf.federal_tax_id,9,'0')                   as fed_tax_i_d,
//                  decode(cwi.irs_tax_type,
//                      0,    3,
//                      null, 3,
//                      1,    2,
//                      2,    1,
//                      cwi.irs_tax_type)                           as t_i_n_type,
//                  substr(mc.merchcont_prim_first_name,1,20)       as d_b_a_contact_f_name,
//                  nvl(substr(ltrim(rtrim(mc.merchcont_prim_last_name)),1,20),
//                  substr(mc.merchcont_prim_first_name,1,20))      as d_b_a_contact_l_name,
//                  substr(nvl(mc.title,'93'),1,3)                  as d_b_a_contact_title,
//                  substr(nvl(mr.merch_email_address,
//                    'none@none.com'),1,70)                        as d_b_a_email_address,
//                  substr(mf.phone_1,1,20)                         as d_b_a_phone_number,
//                  --substr(mf.phone_2_fax,1,20)                     as d_b_a_fax_number,
//                  substr(mf.dmaddr,1,24)                          as d_b_a_street,                  
//                  substr(mf.dmcity,1,21)                          as d_b_a_city,
//                  substr(mf.dmstate,1,21)                         as d_b_a_state,
//                  lpad(to_char(substr(mf.dmzip,1,12)),12,'0')     as d_b_a_zip,
//                  substr(nvl(addr.country_code,'US'),1,3)         as d_b_a_country,
//                  substr(bo.busowner_first_name,1,20)             as bill_to_contact_f_name,
//                  substr(bo.busowner_last_name,1,20)              as bill_to_contact_l_name,
//                  case when bo.busowner_title in
//                    (
//                      '84','85','86','87','88','89','90','91','92','93','94','95','114','135','136'
//                    ) then
//                    bo.busowner_title
//                  else '84' end                                   as bill_to_contact_title,
//                  substr(boa.email_address,1,70)                  as bill_to_email_address,
//                  substr(boa.address_phone,1,20)                  as bill_to_phone_number,
//                  --substr(boa.address_fax,1,20)                    as bill_to_fax_number,
//                  substr(mf.dmaddr,1,24)                          as bill_to_street,
//                  substr(mf.dmcity,1,21)                          as bill_to_city,
//                  substr(mf.dmstate,1,21)                         as bill_to_state,
//                  lpad(to_char(substr(mf.dmzip,1,12)),12,'0')     as bill_to_zip,
//                  substr(nvl(addr.country_code,'US'),1,3)         as bill_to_country,
//                  substr(mr.state_of_incorporation,1,12)          as state_incorporated,
//                  to_char(app.app_created_date,
//                    'yyyymmdd')                                   as customer_sign_date,
//                  nvl(substr(ltrim(rtrim(mf.customer_service_phone)),1,20),
//                    substr(mf.phone_1,1,20))                      as cust_service_phone,
//                  substr(nvl(mr.business_web_address,
//                    mr.merch_web_url),1,70)                       as web_site_address,
//                  REPLACE(REPLACE(substr(mr.merch_busgoodserv_descr,1,100), chr(10), ''), chr(13),  ' ')
//                                                                  as product_service_description,
//                  substr(mf.sic_code,1,4)                         as m_c_c_code,
//                  round(to_number(substr(nvl(mr.annual_vmc_sales,
//                      mr.merch_month_visa_mc_sales*12),1,10)))    as outlet_volume,
//                  substr(round(mr.merch_average_cc_tran,0),1,10)  as average_tkt,
//                  substr(nvl(mr.days_to_ship, '07'),1,3)          as ship_to_days,
//                  decode(mr.seasonal_flag,
//                     'Y', '1',
//                     '0')                                         as seasonal_ind,
//                  decode(mr.loctype_code,
//                    '3', '76', -- office suite
//                    '1', '77', -- retail storefront
//                    '4', '79', -- private residence
//                    '80'       -- others
//                    )                                             as business_operates_from_type,
//                  decode(mr.deposits_accepted,
//                    'Y', '1',
//                    '0')                                          as acc_dep_del_in_future,
//                  substr(nvl(mr.deposits_percent,0),1,3)          as percent_deposits,
//                  substr(nvl(mr.deposit_timeframe, 14),1,3)       as sales_deposited_on_type,
//                  decode(mr.merch_prior_cc_accp_flag,
//                    'Y', '1',
//                    '0')                                          as previous_processing_ind,
//                  decode(mr.refundtype_code,
//                    '5', 'CR', -- credit
//                    '1', '30', -- 30 days or less
//                    '6', 'MR', -- merchandise
//                    '2', 'NR', -- no refund
//                    '3', 'SC', -- store credit
//                    '4', 'NR', -- No Refund
//                    'MR')                                         as refund_policy,
//                  decode(mr.bustype_code,
//                    '1', '70',    -- sole proprietorship
//                    '3', '71',    -- partnership
//                    '2', '72',    -- publicly traded corporation
//                    '6', '73',    -- not for profit
//                    '8', '74',    -- limited liability company
//                    '4', '75',    -- privately held corporation
//                    '5', '126',   -- government
//                    '7', '126',   -- government
//                    '80', '151',  -- international organization
//                    '70')                                         as business_type,
//                  decode(mr.bustype_code,
//                    '6', '1',
//                    '0')                                          as tax_exempt,
//                  --'G'                                             as equip_app_type,
//                  decode(mr.merch_business_establ_date,
//                    null, to_char(to_date(nvl(mr.merch_business_establ_month,'01')||'/'||nvl(mr.merch_business_establ_year,'00'), 'mm/rr'), 'yyyymmdd'),
//                    to_char(mr.merch_business_establ_date, 'yyyymmdd')
//                    )                                             as business_start_date,
//                  substr(nvl(mr.merch_manual_keyed_sales,
//                    nvl(mr.percent_card_not_present,'0')),1,3)    as percent_keyed,
//                  --'M'                                             as communication_type,
//                  substr(nvl(mr.merch_b2b_sales,
//                    '0'),1,3)                                     as b_2_b_percent,
//                  substr(nvl(mr.percent_internet,
//                    '0'),1,3)                                     as trans_source_percent_internet,
//                  decode(mf.deposit_acct_type,
//                    'SV', 'S',
//                    'C')                                          as d_d_a_3_account_type,
//                  lpad(to_char(substr(mf.dda_num,1,17)),17,'0')   as d_d_a_3,
//                  lpad(to_char(mf.transit_routng_num),9,'0')      as d_d_a_3_a_b_a,             
//                  decode(mf.discount_acct_type,
//                    'SV', 'S',
//                    'C')                                          as d_d_a_4_account_type,
//                  lpad(to_char(substr(mf.discount_dda,1,17)),17,'0')   as d_d_a_4,
//                  lpad(to_char(substr(mf.discount_routing,1,9)),9,'0') as d_d_a_4_a_b_a
//                  --'000'                                           as clearing_bank,
//                  --'000'                                           as funding_bank,
//                  --'MeS'                                           as rep_code,
//                  --'MeS'                                           as r_e_l_m
//          from    mif mf,
//                  merchant mr,
//                  merchcontact mc,
//                  address addr,
//                  businessowner bo,
//                  address boa,
//                  application app,
//                  credit_worksheet_info cwi
//          where   mf.merchant_number = :merchantNumber
//                  and mf.merchant_number = mr.merch_number
//                  and mr.app_seq_num = mc.app_seq_num(+)
//                  and mr.app_seq_num = addr.app_seq_num(+)
//                  and addr.addresstype_code(+) = 1
//                  and mr.app_seq_num = bo.app_seq_num
//                  and bo.busowner_num = 1                        
//                  and mr.app_seq_num = boa.app_seq_num
//                  and boa.addresstype_code = 4
//                  and mr.app_seq_num = app.app_seq_num
//                  and mr.app_seq_num = cwi.app_seq_num(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  substr(mf.dba_name,1,24)                        as d_b_a_name,\n                substr(nvl(mf.fdr_corp_name,mf.dba_name),1,24)  as legal_name,\n                lpad(mf.federal_tax_id,9,'0')                   as fed_tax_i_d,\n                decode(cwi.irs_tax_type,\n                    0,    3,\n                    null, 3,\n                    1,    2,\n                    2,    1,\n                    cwi.irs_tax_type)                           as t_i_n_type,\n                substr(mc.merchcont_prim_first_name,1,20)       as d_b_a_contact_f_name,\n                nvl(substr(ltrim(rtrim(mc.merchcont_prim_last_name)),1,20),\n                substr(mc.merchcont_prim_first_name,1,20))      as d_b_a_contact_l_name,\n                substr(nvl(mc.title,'93'),1,3)                  as d_b_a_contact_title,\n                substr(nvl(mr.merch_email_address,\n                  'none@none.com'),1,70)                        as d_b_a_email_address,\n                substr(mf.phone_1,1,20)                         as d_b_a_phone_number,\n                --substr(mf.phone_2_fax,1,20)                     as d_b_a_fax_number,\n                substr(mf.dmaddr,1,24)                          as d_b_a_street,                  \n                substr(mf.dmcity,1,21)                          as d_b_a_city,\n                substr(mf.dmstate,1,21)                         as d_b_a_state,\n                lpad(to_char(substr(mf.dmzip,1,12)),12,'0')     as d_b_a_zip,\n                substr(nvl(addr.country_code,'US'),1,3)         as d_b_a_country,\n                substr(bo.busowner_first_name,1,20)             as bill_to_contact_f_name,\n                substr(bo.busowner_last_name,1,20)              as bill_to_contact_l_name,\n                case when bo.busowner_title in\n                  (\n                    '84','85','86','87','88','89','90','91','92','93','94','95','114','135','136'\n                  ) then\n                  bo.busowner_title\n                else '84' end                                   as bill_to_contact_title,\n                substr(boa.email_address,1,70)                  as bill_to_email_address,\n                substr(boa.address_phone,1,20)                  as bill_to_phone_number,\n                --substr(boa.address_fax,1,20)                    as bill_to_fax_number,\n                substr(mf.dmaddr,1,24)                          as bill_to_street,\n                substr(mf.dmcity,1,21)                          as bill_to_city,\n                substr(mf.dmstate,1,21)                         as bill_to_state,\n                lpad(to_char(substr(mf.dmzip,1,12)),12,'0')     as bill_to_zip,\n                substr(nvl(addr.country_code,'US'),1,3)         as bill_to_country,\n                substr(mr.state_of_incorporation,1,12)          as state_incorporated,\n                to_char(app.app_created_date,\n                  'yyyymmdd')                                   as customer_sign_date,\n                nvl(substr(ltrim(rtrim(mf.customer_service_phone)),1,20),\n                  substr(mf.phone_1,1,20))                      as cust_service_phone,\n                substr(nvl(mr.business_web_address,\n                  mr.merch_web_url),1,70)                       as web_site_address,\n                REPLACE(REPLACE(substr(mr.merch_busgoodserv_descr,1,100), chr(10), ''), chr(13),  ' ')\n                                                                as product_service_description,\n                substr(mf.sic_code,1,4)                         as m_c_c_code,\n                round(to_number(substr(nvl(mr.annual_vmc_sales,\n                    mr.merch_month_visa_mc_sales*12),1,10)))    as outlet_volume,\n                substr(round(mr.merch_average_cc_tran,0),1,10)  as average_tkt,\n                substr(nvl(mr.days_to_ship, '07'),1,3)          as ship_to_days,\n                decode(mr.seasonal_flag,\n                   'Y', '1',\n                   '0')                                         as seasonal_ind,\n                decode(mr.loctype_code,\n                  '3', '76', -- office suite\n                  '1', '77', -- retail storefront\n                  '4', '79', -- private residence\n                  '80'       -- others\n                  )                                             as business_operates_from_type,\n                decode(mr.deposits_accepted,\n                  'Y', '1',\n                  '0')                                          as acc_dep_del_in_future,\n                substr(nvl(mr.deposits_percent,0),1,3)          as percent_deposits,\n                substr(nvl(mr.deposit_timeframe, 14),1,3)       as sales_deposited_on_type,\n                decode(mr.merch_prior_cc_accp_flag,\n                  'Y', '1',\n                  '0')                                          as previous_processing_ind,\n                decode(mr.refundtype_code,\n                  '5', 'CR', -- credit\n                  '1', '30', -- 30 days or less\n                  '6', 'MR', -- merchandise\n                  '2', 'NR', -- no refund\n                  '3', 'SC', -- store credit\n                  '4', 'NR', -- No Refund\n                  'MR')                                         as refund_policy,\n                decode(mr.bustype_code,\n                  '1', '70',    -- sole proprietorship\n                  '3', '71',    -- partnership\n                  '2', '72',    -- publicly traded corporation\n                  '6', '73',    -- not for profit\n                  '8', '74',    -- limited liability company\n                  '4', '75',    -- privately held corporation\n                  '5', '126',   -- government\n                  '7', '126',   -- government\n                  '80', '151',  -- international organization\n                  '70')                                         as business_type,\n                decode(mr.bustype_code,\n                  '6', '1',\n                  '0')                                          as tax_exempt,\n                --'G'                                             as equip_app_type,\n                decode(mr.merch_business_establ_date,\n                  null, to_char(to_date(nvl(mr.merch_business_establ_month,'01')||'/'||nvl(mr.merch_business_establ_year,'00'), 'mm/rr'), 'yyyymmdd'),\n                  to_char(mr.merch_business_establ_date, 'yyyymmdd')\n                  )                                             as business_start_date,\n                substr(nvl(mr.merch_manual_keyed_sales,\n                  nvl(mr.percent_card_not_present,'0')),1,3)    as percent_keyed,\n                --'M'                                             as communication_type,\n                substr(nvl(mr.merch_b2b_sales,\n                  '0'),1,3)                                     as b_2_b_percent,\n                substr(nvl(mr.percent_internet,\n                  '0'),1,3)                                     as trans_source_percent_internet,\n                decode(mf.deposit_acct_type,\n                  'SV', 'S',\n                  'C')                                          as d_d_a_3_account_type,\n                lpad(to_char(substr(mf.dda_num,1,17)),17,'0')   as d_d_a_3,\n                lpad(to_char(mf.transit_routng_num),9,'0')      as d_d_a_3_a_b_a,             \n                decode(mf.discount_acct_type,\n                  'SV', 'S',\n                  'C')                                          as d_d_a_4_account_type,\n                lpad(to_char(substr(mf.discount_dda,1,17)),17,'0')   as d_d_a_4,\n                lpad(to_char(substr(mf.discount_routing,1,9)),9,'0') as d_d_a_4_a_b_a\n                --'000'                                           as clearing_bank,\n                --'000'                                           as funding_bank,\n                --'MeS'                                           as rep_code,\n                --'MeS'                                           as r_e_l_m\n        from    mif mf,\n                merchant mr,\n                merchcontact mc,\n                address addr,\n                businessowner bo,\n                address boa,\n                application app,\n                credit_worksheet_info cwi\n        where   mf.merchant_number =  :1  \n                and mf.merchant_number = mr.merch_number\n                and mr.app_seq_num = mc.app_seq_num(+)\n                and mr.app_seq_num = addr.app_seq_num(+)\n                and addr.addresstype_code(+) = 1\n                and mr.app_seq_num = bo.app_seq_num\n                and bo.busowner_num = 1                        \n                and mr.app_seq_num = boa.app_seq_num\n                and boa.addresstype_code = 4\n                and mr.app_seq_num = app.app_seq_num\n                and mr.app_seq_num = cwi.app_seq_num(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.WFAutoBoard",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.WFAutoBoard",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:465^7*/
      
      rs = it.getResultSet();
      
      if(rs.next())
      {
        businessInfo = buildXMLChunk("BusinessInfo", rs, true);
      }
    }
    catch(Exception e)
    {
      logEntry("buildBusinessList(" + merchantNumber + ")", e.toString());
      throw( e );
    }
    finally
    {
      try{ rs.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
    
    return( businessInfo );
  }
  
  private Element buildOwnerList(String merchantNumber)
    throws Exception
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    Element ownerList = new Element("OwnerInfoList");
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:498^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  39+bo.busowner_num                          as owner_type,
//                  substr(bo.busowner_first_name,1,20)         as owner_f_name,
//                  substr(bo.busowner_last_name,1,20)          as owner_l_name,
//                  substr(addr.email_address,1,70)             as owner_email_address,
//                  substr(addr.address_phone,1,20)             as owner_phone_number,
//                  --substr(addr.address_fax,1,20)               as owner_fax_number,
//                  substr(addr.address_line1,1,24)             as owner_street,
//                  substr(addr.address_city,1,21)              as owner_city,
//                  substr(addr.countrystate_code,1,21)         as owner_state,
//                  lpad(to_char(substr(addr.address_zip,1,5)),12,'0')               as owner_zip,
//                  substr(nvl(addr.country_code,'US'),1,3)     as owner_country,
//                  to_char(bo.owner_since,
//                    'yyyymmdd')                               as owner_since,
//                  --bo.busowner_owner_perc                      as owner_percentage,
//                  to_char(bo.busowner_dl_dob,
//                    'yyyymmdd')                               as owner_d_o_b,
//                  substr(nvl(bo.owner_citizenship,'US'),1,3)  as owner_citizenship,
//                  CASE when REGEXP_LIKE(lpad(SUBSTR (bo.busowner_ssn, 1, 9),9,'0'),
//                       '^(?(666|000|9[0-9][0-9]){3})|(?219099999|078051120|[0-9][0-9][0-9]00[0-9][0-9][0-9][0-9]|[0-9][0-9][0-9][0-9][0-9]0000|123456789)$')
//  		            then lpad(mf.federal_tax_id,9,'0')
//  			        else lpad(SUBSTR (bo.busowner_ssn, 1, 9),9,'0') end      as owner_s_s_n,
//                  decode(bo.personal_guarantee,
//                    'Y', '1',
//                    '0')                                      as owner_guarantee
//          from    merchant mr,
//                  businessowner bo,
//                  address addr,
//                  mif mf
//          where   mr.merch_number = :merchantNumber
//                  and mf.merchant_number = mr.merch_number
//                  and mr.app_seq_num = bo.app_seq_num
//                  and bo.busowner_first_name is not null
//                  and bo.app_seq_num = addr.app_seq_num
//                  and bo.busowner_num+3 = addr.addresstype_code
//          order by bo.busowner_num asc                
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  39+bo.busowner_num                          as owner_type,\n                substr(bo.busowner_first_name,1,20)         as owner_f_name,\n                substr(bo.busowner_last_name,1,20)          as owner_l_name,\n                substr(addr.email_address,1,70)             as owner_email_address,\n                substr(addr.address_phone,1,20)             as owner_phone_number,\n                --substr(addr.address_fax,1,20)               as owner_fax_number,\n                substr(addr.address_line1,1,24)             as owner_street,\n                substr(addr.address_city,1,21)              as owner_city,\n                substr(addr.countrystate_code,1,21)         as owner_state,\n                lpad(to_char(substr(addr.address_zip,1,5)),12,'0')               as owner_zip,\n                substr(nvl(addr.country_code,'US'),1,3)     as owner_country,\n                to_char(bo.owner_since,\n                  'yyyymmdd')                               as owner_since,\n                --bo.busowner_owner_perc                      as owner_percentage,\n                to_char(bo.busowner_dl_dob,\n                  'yyyymmdd')                               as owner_d_o_b,\n                substr(nvl(bo.owner_citizenship,'US'),1,3)  as owner_citizenship,\n                CASE when REGEXP_LIKE(lpad(SUBSTR (bo.busowner_ssn, 1, 9),9,'0'),\n                     '^(?(666|000|9[0-9][0-9]){3})|(?219099999|078051120|[0-9][0-9][0-9]00[0-9][0-9][0-9][0-9]|[0-9][0-9][0-9][0-9][0-9]0000|123456789)$')\n\t\t            then lpad(mf.federal_tax_id,9,'0')\n\t\t\t        else lpad(SUBSTR (bo.busowner_ssn, 1, 9),9,'0') end      as owner_s_s_n,\n                decode(bo.personal_guarantee,\n                  'Y', '1',\n                  '0')                                      as owner_guarantee\n        from    merchant mr,\n                businessowner bo,\n                address addr,\n                mif mf\n        where   mr.merch_number =  :1  \n                and mf.merchant_number = mr.merch_number\n                and mr.app_seq_num = bo.app_seq_num\n                and bo.busowner_first_name is not null\n                and bo.app_seq_num = addr.app_seq_num\n                and bo.busowner_num+3 = addr.addresstype_code\n        order by bo.busowner_num asc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.WFAutoBoard",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,merchantNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.WFAutoBoard",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:535^7*/
      
      rs = it.getResultSet();
      
      while( rs.next() )
      {
        ownerList.addContent( buildXMLChunk("OwnerInfo", rs, true) );
      }
    }
    catch(Exception e)
    {
      logEntry("buildOwnerList(" + merchantNumber + ")", e.toString());
      throw( e );
    }
    finally
    {
      try{ rs.close(); } catch( Exception e ) {}
      try{ it.close(); } catch( Exception e ) {}
    }
    
    return( ownerList );
  }
  
  private Element buildRegistration(WFAutoBoardItem item)
    throws Exception
  {
    Element registration = new Element("Registration");
    
    registration.addContent( new Element("AgentID").setText(AgentId) );
    registration.addContent( new Element("RegistrationID").setText(item.merchantNumber) );
    registration.addContent( new Element("RegistrationType").setText(item.action) );
    
    String accountStatus = "O";
    String closeReason = "Other";
    /*@lineinfo:generated-code*//*@lineinfo:569^5*/

//  ************************************************************
//  #sql [Ctx] { select  decode(mf.date_stat_chgd_to_dcb, null, 'O', 'C'),
//                mr.merch_term_reason
//        
//        from    mif mf,
//                merchant mr
//        where   mf.merchant_number = :item.merchantNumber
//            and mf.merchant_number = mr.merch_number              
//       };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  decode(mf.date_stat_chgd_to_dcb, null, 'O', 'C'),\n              mr.merch_term_reason\n       \n      from    mif mf,\n              merchant mr\n      where   mf.merchant_number =  :1  \n          and mf.merchant_number = mr.merch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.WFAutoBoard",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,item.merchantNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   accountStatus = (String)__sJT_rs.getString(1);
   closeReason = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:579^5*/
    
    registration.addContent( new Element("AccountStatus").setText(accountStatus) );
    if (accountStatus.equals("C")) { // C = close , O = open  for US842
    	if (closeReason == null || closeReason.trim().equals("") ) {
    		registration.addContent( new Element("AccountCloseReason").setText("Other") );
    	} else {
    	    registration.addContent( new Element("AccountCloseReason").setText(closeReason) );
    	}
    }
    
    
    registration.addContent( buildOwnerList(item.merchantNumber) );
    registration.addContent( buildBusinessList(item.merchantNumber) );
    //registration.addContent( buildEntitlementList(item.merchantNumber) );
    //registration.addContent( buildRetailRiskInfo(item.merchantNumber) );
    
    return( registration );
  }

  private Element buildRegistrationList(Vector items)
    throws Exception
  {
    Element regList = new Element("RegistrationList");
    WFAutoBoardItem item=null;
    
    for( int i=0; i<items.size(); ++i )
    {
      item = (WFAutoBoardItem)items.elementAt(i);
      try
      {  
        if(buildRegistration(item) != null) 
        {
          Element registration = buildRegistration(item);
          regList.addContent(registration);  
        }
      }
      catch(Exception e)
      {
        logEntry("buildRegistrationList(" + item.merchantNumber + ")", e.toString());
      }
    }
    
    return( regList );
  }
  
  private Element buildTrailer(Vector items)
  {
    Element trailer = new Element("Trailer");
    
    trailer.addContent(new Element("ServiceProviderID").setText(EntityId) );
    trailer.addContent(new Element("SubmissionBatchID").setText(CurDateString+BatchId) );
    trailer.addContent(new Element("RecordCount").setText(Integer.toString(items.size())) );
    
    return( trailer );
  }
  
  private boolean validateXML()
  {
    return(true);
  }
  
  private boolean createFile(Vector items)
  {
    boolean result = false;
    
    try
    {
      // generate filename
      OutFileName = generateFilename();
      
      //Element registrationContainer = new Element("n1:RegistrationMessage");
      //registrationContainer.setAttribute("xmlns:n1", WF_NAMESPACE);
      Element root = new Element("RegistrationMessage", "n1", WF_NAMESPACE_URI);
      
      // this may not be necessary -- this attribute is in the example but not the spec
      Namespace xsiNS = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
      root.addNamespaceDeclaration(xsiNS);
      
      root.addContent( buildHeader() );
      root.addContent( buildRegistrationList(items) );
      root.addContent( buildTrailer(items) );
      
      doc = new Document( root );
      
      // save document to filesystem
      File  outFile = new File(OutFileName);
      FileOutputStream fo = new FileOutputStream(outFile);
      
      XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
      outputter.output(doc, fo);
      
      fo.flush();
      fo.close();
      
      // send file
      if( sendDataFile(OutFileName,
            MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_HOST),     
            MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_USER),     
            MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_PASSWORD), 
            MesDefaults.getString(MesDefaults.DK_WF_OUTGOING_AB_PATH),  
            false, true, ".xml.ready") )
      {
        // send success email
        sendEmail(true, items);
        
        log.debug("archiving file: " + OutFileName);
        archiveDailyFile(OutFileName);
      }
      else
      {
        // send failure email
        sendEmail(false, items);
      }
        
      result = true;
    }
    catch(Exception e)
    {
      logEntry("createFile()", e.toString());
      result = false;
    }
    
    return( result );
  }
  
  private void sendEmail(boolean success, Vector items)
  {
    StringBuffer  msgBody = new StringBuffer("Wells Fargo Auto Boarding File ");
    StringBuffer  subject = new StringBuffer("WF Auto Boarding ");
    
    try
    {
      MailMessage msg = new MailMessage();
      
      // create subject and messsage body header
      if( success )
      {
        msgBody.append("SUCCESS ");
        subject.append("SUCCESS ");
        msg.setAddresses(EmailGroupSuccess);
      }
      else
      {
        msgBody.append("FAILURE ");
        subject.append("FAILURE ");
        msg.setAddresses(EmailGroupFailure);
      }
    
      String curTime = com.mes.support.DateTimeFormatter.getCurDateTimeString();
      msgBody.append( curTime );
      subject.append( curTime );
    
      msgBody.append("\n\n");
      
      msgBody.append(" Merchant #  Type Action    DBA Name\n");
      msgBody.append("------------ ---- ------ -------------------------\n");
      
      for( int i=0; i<items.size(); ++i )
      {
        WFAutoBoardItem item = (WFAutoBoardItem)(items.elementAt(i));
        msgBody.append(item.merchantNumber);
        msgBody.append(" ");
        msgBody.append(item.appType);
        msgBody.append("   ");
        msgBody.append(item.action);
        msgBody.append("    ");
        msgBody.append(item.dbaName);
        msgBody.append("\n");
      }
      
      msg.setSubject(subject.toString());
      msg.setText(msgBody.toString());
      msg.send();
    }      
    catch(Exception e)
    {
      logEntry("sendEmail()", e.toString());
    }
  }
  
  private boolean createFile(String testMerchantNumber)
  {
    // creates simple items vector for testing purposes
    Vector items = new Vector();
    
    items.add(new WFAutoBoardItem(testMerchantNumber, "N"));
    
    return( createFile(items) );
  }
  
  public boolean execute( )
  {
    boolean result = false;
    ResultSetIterator it = null;
    ResultSet         rs = null;
    
    try
    {
      connect();
      
      // mark items to process if present
      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:784^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    wf_auto_boarding
//          where   process_sequence = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    wf_auto_boarding\n        where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.WFAutoBoard",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:790^7*/
      
      if( recCount > 0 )
      {
        // mark items
        int procSeq = 0;
        /*@lineinfo:generated-code*//*@lineinfo:796^9*/

//  ************************************************************
//  #sql [Ctx] { select  wf_auto_board_seq.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  wf_auto_board_seq.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.WFAutoBoard",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procSeq = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:801^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:803^9*/

//  ************************************************************
//  #sql [Ctx] { update  wf_auto_boarding
//            set     process_sequence = :procSeq
//            where   process_sequence = 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  wf_auto_boarding\n          set     process_sequence =  :1  \n          where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.WFAutoBoard",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSeq);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:808^9*/
        
        
        // get items
        /*@lineinfo:generated-code*//*@lineinfo:812^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  wab.merchant_number,
//                    mf.dba_name,
//                    app.appsrctype_code,
//                    wab.action
//            from    wf_auto_boarding wab,
//                    mif mf,
//                    merchant mr,
//                    application app
//            where   wab.process_sequence = :procSeq
//                    and wab.merchant_number = mf.merchant_number
//                    and mf.merchant_number = mr.merch_number(+)
//                    and mr.app_seq_num = app.app_seq_num(+)
//            order by wab.merchant_number            
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  wab.merchant_number,\n                  mf.dba_name,\n                  app.appsrctype_code,\n                  wab.action\n          from    wf_auto_boarding wab,\n                  mif mf,\n                  merchant mr,\n                  application app\n          where   wab.process_sequence =  :1  \n                  and wab.merchant_number = mf.merchant_number\n                  and mf.merchant_number = mr.merch_number(+)\n                  and mr.app_seq_num = app.app_seq_num(+)\n          order by wab.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.WFAutoBoard",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSeq);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.WFAutoBoard",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:827^9*/
        
        Vector items = new Vector();
        
        rs = it.getResultSet();
        
        while(rs.next())
        {
          items.add( new WFAutoBoardItem(rs) );
        }
        
        rs.close();
        it.close();
        
        result = createFile(items);
        
        if( result == true )
        {
          // successful creation of file
          /*@lineinfo:generated-code*//*@lineinfo:846^11*/

//  ************************************************************
//  #sql [Ctx] { update  wf_auto_boarding
//              set     load_filename = :OutFileName
//              where   process_sequence = :procSeq
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  wf_auto_boarding\n            set     load_filename =  :1  \n            where   process_sequence =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.WFAutoBoard",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,OutFileName);
   __sJT_st.setInt(2,procSeq);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:851^11*/
        }
        else
        {
          // no success - un-mark these items
          /*@lineinfo:generated-code*//*@lineinfo:856^11*/

//  ************************************************************
//  #sql [Ctx] { update  wf_auto_boarding
//              set     process_sequence = 0
//              where   process_sequence = :procSeq
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  wf_auto_boarding\n            set     process_sequence = 0\n            where   process_sequence =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.WFAutoBoard",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSeq);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:861^11*/
        }
        
        commit();
      }
      else
      {
        // nothing to do
        result = true;
      }
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return( result );
  }
  
  private void displayDocument() throws Exception
  {
    // outputs the XML document to the console
    XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
    outputter.output(doc, System.out);
    
    System.out.flush();
  }
  
  public static void main( String[] args )
  {
    WFAutoBoard worker  = null;
    boolean     success = false;
    
    try {

      if (args.length > 0 && args[0].equals("testproperties")) {
          EventBase.printKeyListStatus(new String[]{
                  MesDefaults.DK_WF_ENTITY_ID_UAT,
                  MesDefaults.DK_WF_AGENT_ID_UAT,
                  MesDefaults.DK_WF_ENTITY_ID_PROD,
                  MesDefaults.DK_WF_OUTGOING_HOST,
                  MesDefaults.DK_WF_OUTGOING_USER,
                  MesDefaults.DK_WF_OUTGOING_PASSWORD,
                  MesDefaults.DK_WF_OUTGOING_AB_PATH
          });
      }
    } catch (Exception e) {
        System.out.println(e.toString());
    }

    
    try
    {
      com.mes.database.SQLJConnectionBase.initStandalone("DEBUG");
      
      // set test mode true
      worker = new WFAutoBoard(false);
      
      worker.connect(false);
      
      worker.setAutoCommit(false);
      
      String testMerchantNumber = "";
      if( args.length == 1 )
      {
      	
        testMerchantNumber = args[0];
        success = worker.createFile(testMerchantNumber);
      }                                               
      else
      {
        success = worker.execute();
      }
      
      if( success )
      {
        System.out.println("SUCCESS");
        //worker.displayDocument();
      }
    }
    catch(Exception e)
    {
      System.out.println( "main: " + e.toString() );
    }
    finally
    {
      worker.cleanUp();
    }
  }
}/*@lineinfo:generated-code*/