/*@lineinfo:filename=RejectQueueProcessor*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/RejectQueueProcessor.sqlj $

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-06-06 12:17:55 -0700 (Mon, 06 Jun 2011) $
  Version            : $Revision: 18891 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.database.SQLJConnectionBase;
import com.mes.queues.QueueTools;
import sqlj.runtime.ResultSetIterator;

public class RejectQueueProcessor extends EventBase
{
  static Logger log = Logger.getLogger(RejectQueueProcessor.class);

  public final int CLEAR    = 1;
  public final int KILL     = 2;

  public final int MC       = 1;
  public final int VS       = 2;

  private List clearList    = new ArrayList();
  private List killList     = new ArrayList();

  public boolean execute()
  {
    boolean result = false;
    try
    {
      connect();
      setAutoCommit(false);

      //build the list snapshot
      generatePendLists();

      //process the lists
      processLists();

      commit();

      result = true;
    }
    catch(Exception e)
    {
      rollback();
      logEntry("execute()",e.getMessage());
    }
    finally
    {
      cleanUp();
    }

    return result;

  }

  private void generatePendLists()
  {

    log.debug("running... generatePendLists");
    ResultSetIterator it  = null;

    //select items from pend complete - for settlement and queue move
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:89^7*/

//  ************************************************************
//  #sql [Ctx] it = { select
//            distinct q.id       as id,
//            q.last_user         as last_user,
//            rr.reject_type      as card_type
//          from
//            q_data            q,
//            reject_record     rr
//          where
//            q.type          = :MesQueues.Q_REJECTS_PENDING_CLEAR      -- 372
//            and q.item_type = :MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS  -- 44
//            and rr.rec_id   = q.id
//            and rr.status   = 2
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS =  "SELECT  " 
		   +"  CASE " 
		   +"    WHEN NVL(rr.presentment,0) = 2 " 
		   +"    AND rr.reject_type         = 'MC' " 
		   +"    THEN (q.id ) " 
		   +"    ELSE(0) " 
		   +"  END AS cb_id, " 
		   +"  CASE " 
		   +"    WHEN NVL(rr.presentment,0) = 2 " 
		   +"    AND rr.reject_type         = 'MC' " 
		   +"    THEN " 
		   +"      ( " 
		   +"        SELECT " 
		   +"          mc.rec_id " 
		   +"        FROM " 
		   +"          network_chargeback_mc cb , " 
		   +"          mc_settlement mc " 
		   +"        WHERE " 
		   +"          cb.load_sec           = q.id " 
		   +"        AND mc.merchant_number  = cb.merchant_account_number " 
		   +"        AND mc.reference_number = cb.reference_number " 
		   +"        AND yddd_to_date(SUBSTR( cb.reference_number,8,4)) BETWEEN " 
		   +"          mc.batch_date - 90 AND mc.batch_date + 90 " 
		   +"      ) " 
		   +"    ELSE(q.id) " 
		   +"  END            AS id, " 
		   +"  q.last_user    AS last_user, " 
		   +"  rr.reject_type AS card_type, "
		   +"  rr.external    AS external_reject "
		   +"FROM " 
		   +"  q_data q, " 
		   +"  (select rej.external,rej.rec_id, rej.status, rej.reject_type, rej.presentment from reject_record rej where rej.status = 2  group by rej.rec_id, rej.status, rej.reject_type, rej.presentment, rej.external) rr " 
		   +" WHERE " 
		   +"  q.type       = :1 " 
		   +"AND q.item_type= :2 " 
		   +"AND rr.rec_id  = q.id " ;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.RejectQueueProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_REJECTS_PENDING_CLEAR);
   __sJT_st.setInt(2,MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.RejectQueueProcessor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:103^7*/
      buildList(CLEAR, it.getResultSet());
    }
    catch(Exception e)
    {
      log.debug("No CLEAR rejects found.");
    }

    it  = null;

    //select items from pend kill - for queue move
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:116^7*/

//  ************************************************************
//  #sql [Ctx] it = { select
//            q.id                as id,
//            q.last_user         as last_user,
//            rr.reject_type      as card_type
//          from
//            q_data          q,
//            reject_record   rr
//          where
//            q.type          = :MesQueues.Q_REJECTS_PENDING_KILL       -- 373
//            and q.item_type = :MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS  -- 44
//            and rr.rec_id   = q.id
//            and rr.status   = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
	  String theSqlTS =  "SELECT " 
			  +"  CASE " 
			  +"    WHEN NVL(rr.presentment,0) = 2 " 
			  +"    AND rr.reject_type         = 'MC' " 
			  +"    THEN (q.id ) " 
			  +"    ELSE(0) " 
			  +"  END AS cb_id, " 
			  +"  CASE " 
			  +"    WHEN NVL(rr.presentment,0) = 2 " 
			  +"    AND rr.reject_type         = 'MC' " 
			  +"    THEN " 
			  +"      ( " 
			  +"        SELECT " 
			  +"          mc.rec_id " 
			  +"        FROM " 
			  +"          network_chargeback_mc cb , " 
			  +"          mc_settlement mc " 
			  +"        WHERE " 
			  +"          cb.load_sec           = q.id " 
			  +"        AND mc.merchant_number  = cb.merchant_account_number " 
			  +"        AND mc.reference_number = cb.reference_number " 
			  +"        AND yddd_to_date(SUBSTR( cb.reference_number,8,4)) BETWEEN " 
			  +"          mc.batch_date - 90 AND mc.batch_date + 90 " 
			  +"      ) " 
			  +"    ELSE(q.id) END AS id, " 
			  +"      q.last_user AS last_user, " 
			  +"      rr.reject_type AS card_type, "
			  +"      rr.external    AS external_reject "
			  +"    FROM " 
			  +"      q_data q, " 
			  +"      (select rej.external,rej.rec_id, rej.status, rej.reject_type, rej.presentment from reject_record rej where rej.status = 0  group by rej.rec_id, rej.status, rej.reject_type, rej.presentment,rej.external) rr " 
			  +"    WHERE " 
			  +"      q.type       = :1 " 
			  +"    AND q.item_type= :2 " 
			  +"    AND rr.rec_id  = q.id "; 
			  
	  __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.RejectQueueProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_REJECTS_PENDING_KILL);
   __sJT_st.setInt(2,MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.RejectQueueProcessor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:130^7*/
      buildList(KILL, it.getResultSet());
    }
    catch(Exception e)
    {
      log.debug("No KILL rejects found.");
    }
  }

  private void buildList(int type, ResultSet rs)
  throws Exception
  {
    log.debug("running... buildList");

    List list = getList(type);

    while(rs.next())
    {
      list.add(new _Item(rs));
    }

  }

  private List getList(int type)
  throws Exception
  {
    log.debug("running... getList");
    List list = null;

    switch(type)
    {
      case CLEAR:
        list = clearList;
        break;

      case KILL:
        list = killList;
        break;

      default:
        throw new Exception("Unable to find requested list.");
    };

    return list;
  }

  private void processLists()
  throws Exception
  {
    processClear();
    processKill();
  }

  private void processClear()
  throws Exception
  {
    log.debug("running... processClear");
    _Item item;

    if(clearList.size() > 0)
    {
      for(int i = 0; i < clearList.size(); i++)
      {
        item = (_Item)clearList.get(i);

        //first add item to trigger reproc
        insertSettlementRecord(item);

        updateRejectStatus((item.cardType.equals("MC") && item.cbRecId != 0 ? item.cbRecId : item.recId), 3);

        //then move item
        QueueTools.moveQueueItem((item.cardType.equals("MC") && item.cbRecId != 0 ? item.cbRecId : item.recId),MesQueues.Q_REJECTS_PENDING_CLEAR, MesQueues.Q_REJECTS_COMPLETED,null,null);
      }
    }
  }

  private void processKill()
  throws Exception
  {
    log.debug("running... processKill");
    _Item item;

    if(killList.size() > 0)
    {
      for(int i = 0; i < killList.size(); i++)
      {
        item = (_Item)killList.get(i);

        updateRejectStatus((item.cardType.equals("MC") && item.cbRecId != 0 ? item.cbRecId : item.recId), 1);

        //only need to clear pend Q here
        QueueTools.moveQueueItem((item.cardType.equals("MC") && item.cbRecId != 0 ? item.cbRecId : item.recId), MesQueues.Q_REJECTS_PENDING_KILL, MesQueues.Q_REJECTS_KILL,null,null);
      }
    }
  }

  private void updateRejectStatus(long id, int status)
  throws Exception
  {
    try
    {

      /*@lineinfo:generated-code*//*@lineinfo:232^7*/

//  ************************************************************
//  #sql [Ctx] { update reject_record
//          set
//          status=:status
//          where
//          rec_id = :id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update reject_record\n        set\n        status= :1 \n        where\n        rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.RejectQueueProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,status);
   __sJT_st.setLong(2,id);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:239^7*/

    }
    catch(Exception e)
    {
      log.debug("updateRejectStatus(): " + e.toString() );
      throw e;
    }
  }

  private void insertSettlementRecord(_Item item)
  throws Exception
  {
    int     count         = 0;
    String  tableName     = null;
    
    try
    {
      log.debug("running... insertSettlementRecord");
    
           if ( "MC".equals(item.cardType) ) { tableName = "mc_settlement_activity";    }
      else if ( "VS".equals(item.cardType) ) { tableName = "visa_settlement_activity";  }
      else if ( "AM".equals(item.cardType) ) { tableName = "amex_settlement_activity";  }
      else if ( "DS".equals(item.cardType) ) { tableName = "discover_settlement_activity";  }
      else                                   { logEntry("insertSettlementRecord(" + item.cardType + ")","Invalid card type"); }
    
      if ( tableName != null )
      {
        /*@lineinfo:generated-code*//*@lineinfo:267^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(tbl.rec_id)
//            
//            from    :tableName    tbl
//            where   tbl.rec_id = :item.recId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  count(tbl.rec_id)\n           \n          from     ");
   __sjT_sb.append(tableName);
   __sjT_sb.append("     tbl\n          where   tbl.rec_id =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "3com.mes.startup.RejectQueueProcessor:" + __sjT_sql;
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,item.recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:273^9*/
        count++;

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("insert into  ");
   __sjT_sb.append(tableName);
   __sjT_sb.append("( rec_id, action_rec_id, action_code, action_date, action_source, user_login, load_file_id, reproc ) VALUES ( ? , ? , 'X', trunc(sysdate), 2, ? , 0, ? )");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "4com.mes.startup.RejectQueueProcessor:" + __sjT_sql;
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,item.recId);
   __sJT_st.setInt(2,count);
   __sJT_st.setString(3,item.username);
   __sJT_st.setString(4,item.externalReject);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:298^9*/
      }        
    }
    catch( Exception e )
    {
      logEntry("insertSettlementRecord(" + ((item == null) ? "" : String.valueOf(item.recId)) + ")",e.toString());
    }
  }

  public class _Item
  {
    public String     cardType    = null;
    public long       recId       = -1L;
    public String     username    = null;
    public long       cbRecId       = -1L;
    public String     externalReject = null;


    public _Item()
    {
    }

    public _Item(ResultSet rs)
      throws java.sql.SQLException
    {
      recId     = rs.getLong("id");
      cardType  = rs.getString("card_type");
      username  = rs.getString("last_user");
      externalReject = rs.getString("external_reject");
      try {
      	cbRecId  = rs.getLong("cb_id");
      }catch(Exception e) {
          log.debug("cb_id: " + e.toString() );
      }
    }
    
  }


  public static void main( String[] args )
  {
    RejectQueueProcessor        test          = null;

    try
    {
      SQLJConnectionBase.initStandalone();
      test = new RejectQueueProcessor();
      test.execute();
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    finally
    {
    }
  }


}/*@lineinfo:generated-code*/