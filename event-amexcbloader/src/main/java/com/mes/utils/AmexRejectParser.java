/*@lineinfo:filename=AmexRejectParser*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

 FILE: $URL: http://10.1.71.21/svn/mesweb/trunk/src/main/com/mes/utils/AmexRejectParser.sqlj $

 Description:

 Last Modified By   : $Author: jduncan $
 Last Modified Date : $Date: 2012-02-21 10:17:17 -0800 (Tue, 21 Feb 2012) $
 Version            : $Revision: 19843 $

 Change History:
   See SVN database

 Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
 All rights reserved, Unauthorized distribution prohibited.

 This document contains information which is the proprietary
 property of Merchant e-Solutions, Inc.  This document is received in
 confidence and its contents may not be disclosed without the
 prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.ResultSet;
import java.util.HashMap;
import org.apache.log4j.Logger;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.database.SQLJConnectionDirect;
import com.mes.tools.FileUtils;
import sqlj.runtime.ResultSetIterator;

public class AmexRejectParser extends SQLJConnectionDirect
{
  private static Logger log = Logger.getLogger(AmexRejectParser.class);
  
  public static final int FT_UNKNOWN                  = 0;
  public static final int FT_FIRST_PRESENTMENTS       = 1;
  public static final int FT_REPROC_REJECTS           = 2;

  private int           FileType          = FT_UNKNOWN;
  private String        hashAmount        = null;
  private String        debitHashAmount   = null;
  private String        creditHashAmount  = null;
  private HashMap       recordErrorMap    = new HashMap();
  private long          WorkFileId        = 0L;
  private String        WorkFilename      = null;

  public HashMap getRecordErrorMap()
  {
    return recordErrorMap;
  }

  private void parse( String filename )
  {
    StringBuffer    buf           = null;
    BufferedReader  in            = null;
    int             index         = -1;
    String          line          = null;
    String          loadFilename  = null;
    int             recCount      = 0;
    
    try
    {
      loadFilename  = FileUtils.getNameFromFullPath(filename).toLowerCase();

      in = new BufferedReader( new FileReader(filename) );

      while( (line = in.readLine()) != null )
      {
        buf = new StringBuffer(line);

        // extract work file id from the report file
        index = buf.indexOf("Submitter File Ref Number");
        if( index != -1 )
        {
          // delete characters from beginning of line to the
          // ':' after the "Submitter File Ref Number" string
          index = buf.indexOf(":", index);
          buf = buf.delete(0, index+1);
          buf = new StringBuffer( buf.toString().trim() );

          WorkFileId = Long.parseLong(buf.toString());
          log.debug("WorkFileId: " + WorkFileId);
          
          /*@lineinfo:generated-code*//*@lineinfo:93^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//              
//              from    amex_settlement_activity    sa
//              where   load_file_id = :WorkFileId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n             \n            from    amex_settlement_activity    sa\n            where   load_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.utils.AmexRejectParser",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,WorkFileId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:99^11*/
          FileType = ((recCount == 0) ? FT_FIRST_PRESENTMENTS : FT_REPROC_REJECTS);
          log.debug("FileType: " + FileType);
        }

        //get debit hash amount
        index = buf.indexOf("Debit Hash Total Amount");
        if( index != -1 )
        {
          //delete characters from beginning of line to the
          //':' after the string searched for
          index = buf.indexOf(":", index);
          buf = buf.delete(0, index+1);
          debitHashAmount = buf.toString().trim();

          log.debug("Debit Hash Amount: " + debitHashAmount);
        }

        //get credit hash amount
        index = buf.indexOf("Credit Hash Total Amount");
        if( index != -1 )
        {
          //delete characters from beginning of line to the
          //':' after the string searched for
          index = buf.indexOf(":", index);
          buf = buf.delete(0, index+1);
          creditHashAmount = buf.toString().trim();

          log.debug("Credit Hash Amount: " + creditHashAmount);
        }

        //get hash amount
        index = buf.indexOf("Hash Total Amount");
        if( index != -1 )
        {
          //delete characters from beginning of line to the
          //':' after the string searched for
          index = buf.indexOf(":", index);
          buf = buf.delete(0, index+1);
          hashAmount = buf.toString().trim();

          log.debug("Hash Amount: " + hashAmount);
        }

        //get errors requiring record rejections
        index = buf.indexOf("Errors Requiring Record Rejection");
        if( index != -1 )
        {
          //skip headers lines
          while( (line = in.readLine()) != null && !line.startsWith("Error Code") )
          {
            //skip line
          }

          String errorCode = null;
          String recordNums = null;
          while( (line = in.readLine()) != null && line.length() > 0 && Character.isDigit(line.charAt(0)) )
          {
            index = line.indexOf(" ");
            errorCode = line.substring(0, index);
            
            if( !"8000".equals(errorCode) )
            {
              recordNums = line.substring(index).trim();
              String[] recordNumArray = recordNums.split(",");
              for(int i=0; i<recordNumArray.length; i++)
              {
                String lineNumber = String.valueOf(Integer.parseInt(recordNumArray[i]));
                String errorCodes = (String)recordErrorMap.get(lineNumber);
                errorCodes = ((errorCodes == null) ? errorCode : (errorCodes + "," + errorCode));
                log.debug("Storing record num " + lineNumber + " with error code " + errorCodes);
                recordErrorMap.put(lineNumber,errorCodes);
              }
            }
          }
          break;    // stop parsing file validation report
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadRejectFile("+loadFilename+")",e.toString());
    }
    finally
    {
      try{ in.close(); } catch( Exception e ) {}
    }
  }
  
  public int getFileType()
  {
    return( FileType );
  }
  
  public long getWorkFileId()
  {
    return( WorkFileId );
  }

  public String getWorkFilename()
  {
    return WorkFilename;
  }
  
  public boolean isReprocessedRejectsFile()
  {
    return( getFileType() == FT_REPROC_REJECTS );
  }

  public boolean verifyFile(String inputFilename)
  {
    boolean             fileVerified  = false;
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
    
    try
    {
      connect(true);
      parse(inputFilename);
      
      if ( isReprocessedRejectsFile() )
      {
        /*@lineinfo:generated-code*//*@lineinfo:221^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  sa.load_filename                  as work_filename,
//                    sum(s.transaction_amount)*100     as hash_amount,
//                    sum(decode(s.debit_credit_indicator,'D',1,0)
//                        * s.transaction_amount)*100   as debit_hash_amount,
//                    sum(decode(s.debit_credit_indicator,'C',1,0)
//                        * s.transaction_amount)*100   as credit_hash_amount
//            from    amex_settlement_activity  sa,
//                    amex_settlement           s
//            where   sa.load_file_id = :WorkFileId
//                    and s.rec_id = sa.rec_id
//            group by sa.load_filename
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sa.load_filename                  as work_filename,\n                  sum(s.transaction_amount)*100     as hash_amount,\n                  sum(decode(s.debit_credit_indicator,'D',1,0)\n                      * s.transaction_amount)*100   as debit_hash_amount,\n                  sum(decode(s.debit_credit_indicator,'C',1,0)\n                      * s.transaction_amount)*100   as credit_hash_amount\n          from    amex_settlement_activity  sa,\n                  amex_settlement           s\n          where   sa.load_file_id =  :1 \n                  and s.rec_id = sa.rec_id\n          group by sa.load_filename";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.utils.AmexRejectParser",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,WorkFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.utils.AmexRejectParser",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:234^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:238^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  s.output_filename                 as work_filename,
//                    sum(s.transaction_amount)*100     as hash_amount,
//                    sum(decode(s.debit_credit_indicator,'D',1,0)
//                        * s.transaction_amount)*100   as debit_hash_amount,
//                    sum(decode(s.debit_credit_indicator,'C',1,0)
//                        * s.transaction_amount)*100   as credit_hash_amount
//            from    amex_settlement   s
//            where   s.output_file_id = :WorkFileId
//            group by s.output_filename
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  s.output_filename                 as work_filename,\n                  sum(s.transaction_amount)*100     as hash_amount,\n                  sum(decode(s.debit_credit_indicator,'D',1,0)\n                      * s.transaction_amount)*100   as debit_hash_amount,\n                  sum(decode(s.debit_credit_indicator,'C',1,0)\n                      * s.transaction_amount)*100   as credit_hash_amount\n          from    amex_settlement   s\n          where   s.output_file_id =  :1 \n          group by s.output_filename";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.utils.AmexRejectParser",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,WorkFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.utils.AmexRejectParser",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:249^9*/
      }
      resultSet = it.getResultSet();
      
      if( resultSet.next() )
      {
        WorkFilename = resultSet.getString("work_filename");
        
        log.debug("hash_amount: " + resultSet.getString("hash_amount"));
        log.debug("debit_hash_amount: " + resultSet.getString("debit_hash_amount"));
        log.debug("credit_hash_amount: " + resultSet.getString("credit_hash_amount"));

        if( hashAmount.equals(resultSet.getString("hash_amount")) && 
            debitHashAmount.equals(resultSet.getString("debit_hash_amount")) && 
            creditHashAmount.equals(resultSet.getString("credit_hash_amount")))
        {
          fileVerified = true;
        }
        log.debug("File Verified: " + fileVerified);
      }
      resultSet.close();
    }
    catch(Exception e)
    {
      logEntry("verifyFile(" + WorkFilename + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      cleanUp();
    }
    return fileVerified;
  }

  public long getRecIdForRefNum(String inputFilename, long refNum)
  {
    ResultSetIterator   it            = null;
    ResultSet           resultSet     = null;
    long                rec_id        = 0L;
    
    try
    {
      connect(true);
      parse(inputFilename);
    
      /*@lineinfo:generated-code*//*@lineinfo:294^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  *
//          from    amex_settlement
//          where   output_file_id = :WorkFileId
//                  and reference_number = :refNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  *\n        from    amex_settlement\n        where   output_file_id =  :1 \n                and reference_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.utils.AmexRejectParser",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,WorkFileId);
   __sJT_st.setLong(2,refNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.utils.AmexRejectParser",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:300^7*/
      resultSet = it.getResultSet();
      if( resultSet.next() )
      {
        rec_id = resultSet.getLong("rec_id" );
        log.debug("rec_id for ref num " + refNum + ": " + rec_id);
      }
    }
    catch(Exception e)
    {
      log.error("Error getting rec_id for ref num: " + refNum + "\n" + e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      cleanUp();
    }
    
    return( rec_id );
  }

  public static void main(String[] args)
  {
    AmexRejectParser parser = null;

    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      parser = new AmexRejectParser();

      if ( args[0].equals("getRecIdForRefNum") )
      {
        parser.getRecIdForRefNum(args[1], Long.parseLong(args[2]));
      }
      else if ( args[0].equals("verifyFile") )
      {
         parser.verifyFile(args[1]);
      }
      else
      {
        log.error("Invalid option!");
      }
    }
    catch(Exception e)
    {
      log.error("Couldn't run option " + args[0]);
    }
    finally
    {
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
    
  }
}/*@lineinfo:generated-code*/