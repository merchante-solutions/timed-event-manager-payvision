/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/database/SQLJConnectionDirect.java $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2011-01-31 11:13:04 -0800 (Mon, 31 Jan 2011) $
  Version            : $Revision: 18354 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.database;

public class SQLJConnectionDirect extends SQLJConnectionBase
{
  public SQLJConnectionDirect( )
  {
    super();
    
    initialize( SQLJConnectionBase.getDirectConnectString() );
  }
  
  public SQLJConnectionDirect( boolean autoCommit)
  {
    super( autoCommit );
    
    initialize( getDirectConnectString() );
  }
}
