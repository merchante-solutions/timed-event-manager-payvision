/*@lineinfo:filename=AmexCBLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/AmexCBLoader.sqlj $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-09-14 09:45:26 -0700 (Mon, 14 Sep 2015) $
  Version            : $Revision: 23826 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.jscape.inet.sftp.SftpException;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.MesQueues;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.flatfile.FlatFileRecord;
import com.mes.net.MailMessage;
import com.mes.queues.QueueNotes;
import com.mes.queues.QueueTools;
import com.mes.settlement.SettlementDb;
import com.mes.settlement.SettlementRecord;
import com.mes.settlement.SettlementRecordAmex;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.PropertiesFile;
import com.mes.support.StringUtilities;
import com.mes.support.TridentTools;
import com.mes.tools.ChargebackTools;
import com.mes.tools.FileUtils;
import com.mes.utils.AmexRejectParser;
import sqlj.runtime.ResultSetIterator;

public class AmexCBLoader extends EventBase
{
  static Logger log = Logger.getLogger(AmexCBLoader.class);

  public static final int     FT_INVALID            = -1;
  public static final int     FT_CHARGEBACKS_SABRE  = 0;
  public static final int     FT_RETRIEVALS_SABRE   = 1;
  public static final int     FT_RETRIEVALS_MES     = 2;

  public static final String  AMEX_SE_UNKNOWN     = "9999999999";
  public static final String  AMEX_SE_SABRE       = "5049892912";
  public static final String  AMEX_SE_MES         = "1046000048";
  
  //optblue reject file types
  public static final String  AMEX_OPTB_REJECT_FILE          = "FVALOB";
  public static final String  AMEX_OPTB_REJECT_FILE1         = "FVALOB1";
  public static final String  AMEX_OPTB_REJECT_FILE2         = "FVALOB2";

  protected int         FileType        = FT_INVALID;

  private   boolean     success         = false;

  private static final HashMap  FilePrefixes  =
    new HashMap()
    {
      {
        put("CBNOT"   , "amex_cb"      );
        put("INQ02"   , "amex_retr"    );
        put("EPRAW"   , "amex_recon"   );
        put("FVAL"    , "amex_fvr"     );
        put("SPORP"   , "amex_spmresp" );
        put("OBSRP"   , "amex_obsresp" );
        put("FVALOB"  , "amex_ob_fvr"  );
        
      }
    };

  private class DuplicateCase
  {
    public String   CardNumber  = null;
    public String   CaseNumber  = null;
    public String   RefNum      = null;
    public String   SeNumber    = null;
    public double   TranAmount  = 0.0;

    public DuplicateCase( String seNumber, String caseNumber, String cardNumber, String refNum, double tranAmount )
      throws java.sql.SQLException
    {
      SeNumber    = seNumber;
      CaseNumber  = caseNumber;
      CardNumber  = TridentTools.encodeCardNumber(cardNumber);
      RefNum      = refNum;
      TranAmount  = tranAmount;
    }
  }

  public AmexCBLoader( )
  {
    PropertiesFilename = "amex-cb-loader.properties";
  }
  
  protected String buildChargebackNote( FlatFileRecord ffd )
  {
    StringBuffer      retVal    = new StringBuffer("");
    
    for( int i = 1; i < 8; ++i )
    {
      String noteFragment = ffd.getFieldData("note"+i).trim();
      if( !"".equals(noteFragment) )
      {
        retVal.append( noteFragment );
        if ( noteFragment.length() < ffd.getFieldLength("note"+i) )
        {
          retVal.append(" \n");
        }
      }
    }
    return( retVal.toString() );
  }

  protected void createProcessTableEntries( String loadFilename )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:161^7*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_process
//          (
//            process_sequence,
//            process_type,
//            load_filename
//          )
//          values
//          (
//            0,
//            1,
//            :loadFilename
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_process\n        (\n          process_sequence,\n          process_type,\n          load_filename\n        )\n        values\n        (\n          0,\n          1,\n           :1  \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:175^7*/
    }
    catch( Exception e )
    {
      logEntry("createProcessTableEntries(" + loadFilename + ")",e.toString());
    }
    finally
    {
    }
  }

  public boolean execute()
  {
    try
    {
      // get an automated timeout exempt connection
      connect(true);
      loadFiles(getEventArg(0));
      
      // Retry loading the files one more time in case of SFTPException / EOFException for Inquiries (PROD-1313)
      if(!success && "INQ02".equals(getEventArg(0))) {
          logError("loadFiles(" + getEventArg(0) + ")", "Retry loadFiles() again as success = " + success + " for initial load.");
          loadFiles(getEventArg(0));
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( true );
  }
  
  protected long findChargebackLoadSec( FlatFileRecord ffd )
  {
    String    adjRefNumColumnName = null;
    String    amountColumnName    = null;
    String    dateColumnName      = null;
    long      loadSec             = 0L;
    
    try
    {
      Date paymentDate = null;
      /*@lineinfo:generated-code*//*@lineinfo:222^7*/

//  ************************************************************
//  #sql [Ctx] { select  to_date( (:ffd.getFieldData("payment_year").trim() ||
//                            :ffd.getFieldData("payment_number").trim().substring(0,3)),
//                           'yyyyddd')
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3664 = ffd.getFieldData("payment_year").trim();
 String __sJT_3665 = ffd.getFieldData("payment_number").trim().substring(0,3);
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  to_date( ( :1   ||\n                           :2  ),\n                         'yyyyddd')\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.AmexCBLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3664);
   __sJT_st.setString(2,__sJT_3665);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   paymentDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:229^7*/
    
      for( int i = 0; i < 2 && loadSec == 0L; ++i )
      {
        dateColumnName      = (i == 0) ? "incoming_date"      : "reversal_date";
        amountColumnName    = (i == 0) ? "chargeback_amount"  : "reversal_amount";
        adjRefNumColumnName = (i == 0) ? "adjustment_ref_num" : "reversal_ref_num";
      
        /*@lineinfo:generated-code*//*@lineinfo:237^9*/

//  ************************************************************
//  #sql [Ctx] { select  cbam.load_sec
//            
//            from    network_chargeback_amex   cbam
//            where   cbam.se_number = :ffd.getFieldData("amex_se_number").trim()
//                    and :dateColumnName between :paymentDate-3 and :paymentDate+3
//                    and :adjRefNumColumnName = :ffd.getFieldData("adjustment_number").trim()
//                    and :amountColumnName = :TridentTools.decodeCobolAmount(ffd.getFieldData("adjustment_amount").trim(),2)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3666 = ffd.getFieldData("amex_se_number").trim();
 String __sJT_3667 = ffd.getFieldData("adjustment_number").trim();
 double __sJT_3668 = TridentTools.decodeCobolAmount(ffd.getFieldData("adjustment_amount").trim(),2);
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  cbam.load_sec\n           \n          from    network_chargeback_amex   cbam\n          where   cbam.se_number =  ? \n                  and  ");
   __sjT_sb.append(dateColumnName);
   __sjT_sb.append("  between  ? -3 and  ? +3\n                  and  ");
   __sjT_sb.append(adjRefNumColumnName);
   __sjT_sb.append("  =  ? \n                  and  ");
   __sjT_sb.append(amountColumnName);
   __sjT_sb.append("  =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "2com.mes.startup.AmexCBLoader:" + __sjT_sql;
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3666);
   __sJT_st.setDate(2,paymentDate);
   __sJT_st.setDate(3,paymentDate);
   __sJT_st.setString(4,__sJT_3667);
   __sJT_st.setDouble(5,__sJT_3668);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadSec = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:246^9*/
      }
    }
    catch( java.sql.SQLException sqle )
    {
      // ignore, assume not a chargeback adjustment
    }
    return( loadSec );
  }

  protected void insertRejectRecord( long recId, String reasonCode, String workFilename )
  {
    try
    {
      connect();
      
      // get a round number for this reject
      int round = 0;
      /*@lineinfo:generated-code*//*@lineinfo:264^7*/

//  ************************************************************
//  #sql [Ctx] { select  nvl (max(round), 0)+1
//          
//          from    reject_record rr
//          where   rr.rec_id = :recId
//                  and rr.reject_type = 'AM'
//                  and rr.presentment = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl (max(round), 0)+1\n         \n        from    reject_record rr\n        where   rr.rec_id =  :1  \n                and rr.reject_type = 'AM'\n                and rr.presentment = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.AmexCBLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   round = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:272^7*/

      /*@lineinfo:generated-code*//*@lineinfo:274^7*/

//  ************************************************************
//  #sql [Ctx] { insert into reject_record
//          (
//            rec_id,
//            reject_id,
//            reject_type,
//            status,
//            round,
//            creation_date,
//            reject_date,
//            presentment,
//            load_filename,
//            load_file_id
//          )
//          values
//          (
//            :recId,
//            :reasonCode,
//            'AM',
//            -1,
//            :round,
//            sysdate,
//            get_file_date(:workFilename),
//            0,
//            :workFilename,
//            load_filename_to_load_file_id(:workFilename)
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into reject_record\n        (\n          rec_id,\n          reject_id,\n          reject_type,\n          status,\n          round,\n          creation_date,\n          reject_date,\n          presentment,\n          load_filename,\n          load_file_id\n        )\n        values\n        (\n           :1  ,\n           :2  ,\n          'AM',\n          -1,\n           :3  ,\n          sysdate,\n          get_file_date( :4  ),\n          0,\n           :5  ,\n          load_filename_to_load_file_id( :6  )\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,recId);
   __sJT_st.setString(2,reasonCode);
   __sJT_st.setInt(3,round);
   __sJT_st.setString(4,workFilename);
   __sJT_st.setString(5,workFilename);
   __sJT_st.setString(6,workFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:302^7*/
      
      if ( QueueTools.hasItemEnteredQueue(recId, MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS) )
      {
        QueueTools.moveQueueItem(recId,
                                 QueueTools.getCurrentQueueType(recId, MesQueues.Q_ITEM_TYPE_PACKAGE_REJECTS),
                                 MesQueues.Q_REJECTS_UNWORKED,
                                 null,
                                 null);
      }
      else  // reject does not exist, add recId to the unworked queue
      {
        QueueTools.insertQueue(recId, MesQueues.Q_REJECTS_UNWORKED);
      }
    } 
    catch (Exception e) 
    {
      logEntry("insertRejectRecord(" + recId + "," + reasonCode + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  private HashMap rejectBatchIfTrailerIsRejected(long batchId,int fileRecCount,HashMap rejectMap, Vector batchRecIds,String reasonCode,HashMap recordErrorMap,boolean aggregatorSE )
  {
    try{
        // if the TBT record caused the reject, then insert a 
        // reject entry for each rec_id in the batch
        if ( (reasonCode = (String)recordErrorMap.get(String.valueOf(fileRecCount))) != null )
        {
          for( int i = 0; i < batchRecIds.size(); ++i )
          {
            String recId        = (String)batchRecIds.elementAt(i);
            String reasonCodes  = (String)rejectMap.get(recId);
            reasonCodes = ((reasonCodes == null) ? reasonCode : (reasonCodes + "," + reasonCode));
            reasonCodes += (aggregatorSE ? "A" : "");
            rejectMap.put(recId,reasonCodes);
          }
        }
      }catch(Exception e){
        log.error("inside method rejectBatchIfTrailerIsRejected(): Exception occured while rejecting batch : " + batchId, e);
	  }
    return rejectMap;
  }
  
  public boolean isAggregatorSE( int bankNumber, long seNumber )
  {
    int       recCount    = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:333^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    mbs_banks
//          where   bank_number = :bankNumber
//                  and instr(am_aggregators,:seNumber) > 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    mbs_banks\n        where   bank_number =  :1  \n                and instr(am_aggregators, :2  ) > 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.AmexCBLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setLong(2,seNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:340^7*/
    }
    catch( Exception e )
    {
      logEntry("isAggregatorSE(" + bankNumber + "," + seNumber + ")",e.toString());
    }
    finally
    {
    }
    return( recCount > 0 );
  }

  protected void loadFiles( String fileType )
  {
    String    fileMask          = null;
    String    filePrefix        = null;
    String    loadFilename      = null;
    String    remoteFilename    = null;
    Sftp      sftp              = null;

    try
    {
      if ( "FVAL".equals(fileType) )
      {
        fileMask = "SUB002.098535.C#.*";
      }
      else if(AMEX_OPTB_REJECT_FILE.equals(fileType)){
        loadFiles(AMEX_OPTB_REJECT_FILE1);
        loadFiles(AMEX_OPTB_REJECT_FILE2);
      }
      else if(AMEX_OPTB_REJECT_FILE1.equals(fileType)){
        //optblue rejects for sub id - 110885
    	fileMask = "SUB002.110885.C#.*";
    	fileType = AMEX_OPTB_REJECT_FILE;
      }
      else if(AMEX_OPTB_REJECT_FILE2.equals(fileType)){
    	//optblue rejects for sub id - 110886
      	fileMask = "SUB002.110886.C#.*";
      	fileType = AMEX_OPTB_REJECT_FILE;
      } 
      else if("SPORP".equals(fileType)){
        //fileMask = "MERCHANTESPSPTST." + fileType + ".*" ; // test
          fileMask = "MERCHANTESPSPPRD." + fileType + ".*" ; // prod
      }
      //OptBlue response
      else if("OBSPORP".equals(fileType))
      {
    	  //fileMask = "MERCHOBSPSPTST." + fileType + ".*" ; // test
          fileMask = "MERCHOBSPSPPRD." + fileType + ".*" ; // prod
    	  
      }
      else  // use generic
      {
        fileMask = "MERCHANTE." + fileType + "#.*";
      }
      
      //get the Sftp connection
      sftp = getSftp( MesDefaults.getString(MesDefaults.DK_AMEX_HOST),
                      MesDefaults.getString(MesDefaults.DK_AMEX_USER),
                      MesDefaults.getString(MesDefaults.DK_AMEX_PASSWORD),
                      MesDefaults.getString(MesDefaults.DK_AMEX_INCOMING_PATH),
                      false   // !binary
                    );

      EnumerationIterator enumi = new EnumerationIterator();
      Iterator it = enumi.iterator(sftp.getNameListing(fileMask));

      while( it.hasNext() )
      {
        remoteFilename  = (String)it.next();
        filePrefix      = (String)FilePrefixes.get(fileType);
        loadFilename    = generateFilename(filePrefix + "3941");

        if (!sftp.isConnected()) {
        	sftp = getSftp( MesDefaults.getString(MesDefaults.DK_AMEX_HOST),
                    MesDefaults.getString(MesDefaults.DK_AMEX_USER),
                    MesDefaults.getString(MesDefaults.DK_AMEX_PASSWORD),
                    MesDefaults.getString(MesDefaults.DK_AMEX_INCOMING_PATH),
                    false   // !binary
                  );
        }
        sftp.download(loadFilename,remoteFilename);

        if ( "CBNOT".equals(fileType) )
        {
          loadChargebackFile  ( loadFilename );
          loadChargebackSystem( loadFilename );
        }
        else if ( "INQ02".equals(fileType) )
        {
          loadRetrievalFile   ( loadFilename );
          loadRetrievalSystem ( loadFilename );
        }
        else if ( "EPRAW".equals(fileType) )
        {
          loadReconFile(loadFilename);
        }
        else if ( "FVAL".equals(fileType) ||
                   AMEX_OPTB_REJECT_FILE.equals(fileType) )
        {
          loadRejectFile(loadFilename);
          notifyRejectsDept(fileType,loadFilename);
        }
        else if ( "SPORP".equals(fileType) )
        {
          loadSPMResponseFile(loadFilename,"WEEKLYSPM");
        }
        else if ( "OBSPORP".equals(fileType) )
        {
        	loadSPMResponseFile(loadFilename,"DAILYSPM");
        }
        notifyChargebackDept( loadFilename );
        archiveDailyFile(loadFilename);
      }
      
      success = true;
    }
    catch( EOFException ioe ) {
        logEntry("loadFiles(" + fileType + "," + remoteFilename + ")", ioe.toString());
        success = false;
    }
    catch( SftpException sftpe ) {
        logEntry("loadFiles(" + fileType + "," + remoteFilename + ")", sftpe.toString());
        success = false;
    }
    catch( Exception e )
    {
      logEntry("loadFiles(" + fileType + "," + remoteFilename + ")",e.toString());
      // success not being set to false deliberately here as we do not want to retry in case of any exception other than IO or SFTP
    }
    finally
    {
      try{ sftp.disconnect(); } catch( Exception ee ) {}
    }
  }

  protected void loadChargebackFile( String inputFilename )
  {
    int               adjRefNum       = 0;
    FlatFileRecord    ffd             = null;
    HashMap           ffds            = new HashMap();
    BufferedReader    in              = null;
    Date              incomingDate    = null;
    ResultSetIterator it              = null;
    String            line            = null;
    long              loadFileId      = 0L;
    String            loadFilename    = null;
    long              loadSec         = 0L;
    String            note            = null;
    char              recType         = '\0';
    ResultSet         resultSet       = null;

    try
    {
      ffds.put("H",new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_CBNOT_HDR));
      ffds.put("D",new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_CBNOT_DTL));
      ffds.put("T",new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_CBNOT_TRL));

      loadFilename  = FileUtils.getNameFromFullPath(inputFilename).toLowerCase();
      loadFileId    = loadFilenameToLoadFileId(loadFilename);

      in = new BufferedReader( new FileReader(inputFilename) );

      while( (line = in.readLine()) != null )
      {
        recType = line.charAt(0);

        ffd = (FlatFileRecord)ffds.get(String.valueOf(recType));

        if ( ffd != null )
        {
          ffd.resetAllFields();
          ffd.suck(line);

          if ( recType == 'H' )
          {
            /*@lineinfo:generated-code*//*@lineinfo:516^13*/

//  ************************************************************
//  #sql [Ctx] { select  to_date(:ffd.getFieldData("transmission_date"),'yyyyddd')
//                
//                from    dual
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3669 = ffd.getFieldData("transmission_date");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  to_date( :1  ,'yyyyddd')\n               \n              from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.AmexCBLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3669);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   incomingDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:521^13*/
          }
          else if ( recType == 'D' )
          {
            adjRefNum = ffd.getFieldAsInt("cb_adj_number");
            note      = buildChargebackNote(ffd);
            
            // look for a previous chargeback entry with this 
            // adjustment reference number
            /*@lineinfo:generated-code*//*@lineinfo:530^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  nvl(qd.type,0)                          as qtype,
//                        cbam.load_sec                           as load_sec,
//                        cbam.case_number                        as case_number,
//                        cbam.chargeback_amount                  as cb_amount,
//                        decode(cbam.reversal_date,null,'N','Y') as reversed,
//                        to_number(:ffd.getFieldData("cb_amount").replace(' ','+'),'S0000000000000.00') 
//                                                                as reversal_amount,
//                        case when (cbam.chargeback_amount + to_number(:ffd.getFieldData("cb_amount").replace(' ','+'),'S0000000000000.00')) = 0 then 'Y' else 'N' end
//                                                                as offset_adjustment
//                from    network_chargeback_amex cbam,
//                        q_data                  qd
//                where   cbam.se_number = :ffd.getFieldData("se_number").trim()
//                        and cbam.incoming_date >= (:incomingDate-180)
//                        and cbam.adjustment_ref_num = :adjRefNum
//                        and qd.item_type(+) = 18
//                        and qd.id(+) = cbam.load_sec
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3670 = ffd.getFieldData("cb_amount").replace(' ','+');
 String __sJT_3671 = ffd.getFieldData("cb_amount").replace(' ','+');
 String __sJT_3672 = ffd.getFieldData("se_number").trim();
  try {
   String theSqlTS = "select  nvl(qd.type,0)                          as qtype,\n                      cbam.load_sec                           as load_sec,\n                      cbam.case_number                        as case_number,\n                      cbam.chargeback_amount                  as cb_amount,\n                      decode(cbam.reversal_date,null,'N','Y') as reversed,\n                      to_number( :1  ,'S0000000000000.00') \n                                                              as reversal_amount,\n                      case when (cbam.chargeback_amount + to_number( :2  ,'S0000000000000.00')) = 0 then 'Y' else 'N' end\n                                                              as offset_adjustment\n              from    network_chargeback_amex cbam,\n                      q_data                  qd\n              where   cbam.se_number =  :3  \n                      and cbam.incoming_date >= ( :4  -180)\n                      and cbam.adjustment_ref_num =  :5  \n                      and qd.item_type(+) = 18\n                      and qd.id(+) = cbam.load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3670);
   __sJT_st.setString(2,__sJT_3671);
   __sJT_st.setString(3,__sJT_3672);
   __sJT_st.setDate(4,incomingDate);
   __sJT_st.setInt(5,adjRefNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.AmexCBLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:548^13*/
            resultSet = it.getResultSet();
            
            if ( resultSet.next() )   // found a previous adjusted entry
            {
              loadSec = resultSet.getLong("load_sec");
              int  reversalRefNum = ffd.getFieldAsInt("cb_resolution_adj_number");
              
              // sometimes amex uses the same adjustment number with an offset
              // amount rather than issuing a new reversal adjustment number.
              // when this is the case, use the original adjustment number
              // as the reversal adjustment number
              if ( reversalRefNum == 0 && "Y".equals(resultSet.getString("offset_adjustment")) )
              {
                reversalRefNum = adjRefNum;
              }
              
              // if this is a reversal and the previous entry has not already
              // been reversed, then issue a reversal for the original
              if ( reversalRefNum != 0 && "N".equals(resultSet.getString("reversed")) )
              {
                ChargebackTools.reverseChargeback(Ctx,"AM",loadSec,incomingDate,resultSet.getDouble("reversal_amount"));
                
                // add the reversal reference number to the chargeback
                /*@lineinfo:generated-code*//*@lineinfo:572^17*/

//  ************************************************************
//  #sql [Ctx] { update  network_chargeback_amex
//                    set     reversal_ref_num = :reversalRefNum
//                    where   load_sec = :loadSec
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  network_chargeback_amex\n                  set     reversal_ref_num =  :1  \n                  where   load_sec =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,reversalRefNum);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:577^17*/                
              }
              
              // if amex changed the case number on this entry, add
              // a note to the chargeback item with the new case number
              String  oldCaseNum  = resultSet.getString("case_number");
              int     qtype       = resultSet.getInt("qtype");
              
              // use default qtype when selected qtype is invalid
              qtype = ((qtype == 0) ? MesQueues.Q_CHARGEBACKS_AMEX : qtype);
              if( !oldCaseNum.equals(ffd.getFieldData("case_number").trim()) )
              {
                QueueNotes.addNote(loadSec,qtype,"system","Case number updated to " + ffd.getFieldData("case_number").trim());
              }
              
              // if there was note data for this chargeback, add note
              if ( !"".equals(note) )
              {
                QueueNotes.addNote(loadSec,qtype,"system",note);
              }
            }
            else if ( adjRefNum != 0 )  // if this caused an adjustment, create a new chargeback
            {
              loadSec = ChargebackTools.getNewLoadSec();

              /*@lineinfo:generated-code*//*@lineinfo:602^15*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargeback_amex
//                  (
//                    load_sec,
//                    case_number,
//                    status,
//                    card_number,
//                    cardholder_name,
//                    transaction_date,
//                    transaction_amount,
//                    incoming_date,
//                    chargeback_amount,
//                    se_number,
//                    adjustment_ref_num,
//                    chargeback_type,
//                    reason_code,
//                    load_filename,
//                    load_file_id
//                  )
//                  values
//                  (
//                    :loadSec,
//                    :ffd.getFieldData("case_number").trim(),    -- case #
//                    null,                                         -- status
//                    :ffd.getFieldData("card_number").trim(),    -- card #
//                    :ffd.getFieldData("cm_name1").trim(),       -- cardholder name
//                    to_date(:ffd.getFieldData("transaction_date"),'yyyymmdd'),   
//                                                                  -- tran date
//                    to_number(:ffd.getFieldData("billed_amount").replace(' ','+'),'S0000000000000.00'),
//                                                                  -- tran amount
//                    :incomingDate,                                -- cb date
//                    to_number(:ffd.getFieldData("cb_amount").replace(' ','+'),'S0000000000000.00'),
//                                                                  -- cb amount
//                    :ffd.getFieldData("se_number").trim(),      -- se number
//                    :ffd.getFieldData("cb_adj_number").trim(),  -- adj #
//                    :ffd.getFieldData("case_type").trim(),      -- case type
//                    :ffd.getFieldData("reason_code").trim(),    -- reason code
//                    :loadFilename,
//                    :loadFileId
//                  )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3673 = ffd.getFieldData("case_number").trim();
 String __sJT_3674 = ffd.getFieldData("card_number").trim();
 String __sJT_3675 = ffd.getFieldData("cm_name1").trim();
 String __sJT_3676 = ffd.getFieldData("transaction_date");
 String __sJT_3677 = ffd.getFieldData("billed_amount").replace(' ','+');
 String __sJT_3678 = ffd.getFieldData("cb_amount").replace(' ','+');
 String __sJT_3679 = ffd.getFieldData("se_number").trim();
 String __sJT_3680 = ffd.getFieldData("cb_adj_number").trim();
 String __sJT_3681 = ffd.getFieldData("case_type").trim();
 String __sJT_3682 = ffd.getFieldData("reason_code").trim();
   String theSqlTS = "insert into network_chargeback_amex\n                (\n                  load_sec,\n                  case_number,\n                  status,\n                  card_number,\n                  cardholder_name,\n                  transaction_date,\n                  transaction_amount,\n                  incoming_date,\n                  chargeback_amount,\n                  se_number,\n                  adjustment_ref_num,\n                  chargeback_type,\n                  reason_code,\n                  load_filename,\n                  load_file_id\n                )\n                values\n                (\n                   :1  ,\n                   :2  ,    -- case #\n                  null,                                         -- status\n                   :3  ,    -- card #\n                   :4  ,       -- cardholder name\n                  to_date( :5  ,'yyyymmdd'),   \n                                                                -- tran date\n                  to_number( :6  ,'S0000000000000.00'),\n                                                                -- tran amount\n                   :7  ,                                -- cb date\n                  to_number( :8  ,'S0000000000000.00'),\n                                                                -- cb amount\n                   :9  ,      -- se number\n                   :10  ,  -- adj #\n                   :11  ,      -- case type\n                   :12  ,    -- reason code\n                   :13  ,\n                   :14  \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,__sJT_3673);
   __sJT_st.setString(3,__sJT_3674);
   __sJT_st.setString(4,__sJT_3675);
   __sJT_st.setString(5,__sJT_3676);
   __sJT_st.setString(6,__sJT_3677);
   __sJT_st.setDate(7,incomingDate);
   __sJT_st.setString(8,__sJT_3678);
   __sJT_st.setString(9,__sJT_3679);
   __sJT_st.setString(10,__sJT_3680);
   __sJT_st.setString(11,__sJT_3681);
   __sJT_st.setString(12,__sJT_3682);
   __sJT_st.setString(13,loadFilename);
   __sJT_st.setLong(14,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:643^15*/
              
              // if there were notes with this CBNOT entry, add
              // them to the notes for this queue item
              if ( !"".equals(note) )
              {
                QueueNotes.addNote(loadSec,MesQueues.Q_CHARGEBACKS_AMEX,"system",note);
              }
            }
            else
            {
              logEntry("loadChargebackData(" + loadFilename + "," + ffd.getFieldData("case_number").trim() + ")","WARNING:  Unable to resolve CBNOT entry");
            }
            resultSet.close();
            it.close();
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadChargebackData("+loadFilename+")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      try{ in.close(); } catch( Exception e ) {}
    }
  }

  public void loadChargebackSystem( String loadFilename )
  {
    boolean     autoCommit      = getAutoCommit();

    try
    {
      setAutoCommit(false);

      /*@lineinfo:generated-code*//*@lineinfo:681^7*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargebacks
//          (
//            cb_load_sec,
//            cb_ref_num,
//            merchant_number,
//            bank_number,
//            incoming_date,
//            reference_number,
//            card_number,
//            card_type,
//            tran_date,
//            tran_amount,
//            merchant_name,
//            reason_code,
//            load_filename,
//            first_time_chargeback,
//            mes_ref_num,
//            debit_credit_ind,
//            tran_date_missing,
//            card_number_enc
//          )
//          select  cb.load_sec               as load_sec,
//                  cb.case_number            as cb_ref_num,
//                  nvl(cb.merchant_number,0) as merchant_number,
//                  nvl(mf.bank_number,3941)  as bank_number,
//                  cb.incoming_date          as incoming_date,
//                  cb.acq_reference_number   as reference_number,
//                  cb.card_number            as card_number,
//                  'AM'                      as card_type,
//                  cb.transaction_date       as tran_date,
//                  -- amex raw data shows cb as a credit (to issuer)
//                  (-1 * cb.chargeback_amount) as tran_amount,
//                  mf.dba_name               as merchant_name,
//                  cb.reason_code            as reason_code,
//                  cb.load_filename          as load_filename,
//                  'Y'                       as first_time,
//                  cb.mes_ref_num            as mes_ref_num,
//                  decode( (cb.transaction_amount/abs(cb.transaction_amount)),
//                          -1, 'C',
//                          'D' )           as debit_credit_ind,
//                  'N'                     as tran_date_missing,
//                  cb.card_number_enc      as card_number_enc
//          from    network_chargeback_amex cb,
//                  mif                     mf
//          where   cb.load_file_id = load_filename_to_load_file_id(:loadFilename) and
//                  mf.merchant_number(+) = cb.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into network_chargebacks\n        (\n          cb_load_sec,\n          cb_ref_num,\n          merchant_number,\n          bank_number,\n          incoming_date,\n          reference_number,\n          card_number,\n          card_type,\n          tran_date,\n          tran_amount,\n          merchant_name,\n          reason_code,\n          load_filename,\n          first_time_chargeback,\n          mes_ref_num,\n          debit_credit_ind,\n          tran_date_missing,\n          card_number_enc\n        )\n        select  cb.load_sec               as load_sec,\n                cb.case_number            as cb_ref_num,\n                nvl(cb.merchant_number,0) as merchant_number,\n                nvl(mf.bank_number,3941)  as bank_number,\n                cb.incoming_date          as incoming_date,\n                cb.acq_reference_number   as reference_number,\n                cb.card_number            as card_number,\n                'AM'                      as card_type,\n                cb.transaction_date       as tran_date,\n                -- amex raw data shows cb as a credit (to issuer)\n                (-1 * cb.chargeback_amount) as tran_amount,\n                mf.dba_name               as merchant_name,\n                cb.reason_code            as reason_code,\n                cb.load_filename          as load_filename,\n                'Y'                       as first_time,\n                cb.mes_ref_num            as mes_ref_num,\n                decode( (cb.transaction_amount/abs(cb.transaction_amount)),\n                        -1, 'C',\n                        'D' )           as debit_credit_ind,\n                'N'                     as tran_date_missing,\n                cb.card_number_enc      as card_number_enc\n        from    network_chargeback_amex cb,\n                mif                     mf\n        where   cb.load_file_id = load_filename_to_load_file_id( :1  ) and\n                mf.merchant_number(+) = cb.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:729^7*/

      /*@lineinfo:generated-code*//*@lineinfo:731^7*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargeback_activity
//          (
//            cb_load_sec,
//            file_load_sec,
//            action_code,
//            action_date,
//            user_message,
//            action_source,
//            user_login
//          )
//          select  cb.load_sec,
//                  1,
//                  'D',
//                  trunc(sysdate),
//                  'Auto Action - Amex',
//                  8,    -- auto amex
//                  'system'
//          from    network_chargeback_amex cb
//          where   cb.load_file_id = load_filename_to_load_file_id(:loadFilename) and
//                  not cb.settlement_rec_id is null    -- only work resolved items
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into network_chargeback_activity\n        (\n          cb_load_sec,\n          file_load_sec,\n          action_code,\n          action_date,\n          user_message,\n          action_source,\n          user_login\n        )\n        select  cb.load_sec,\n                1,\n                'D',\n                trunc(sysdate),\n                'Auto Action - Amex',\n                8,    -- auto amex\n                'system'\n        from    network_chargeback_amex cb\n        where   cb.load_file_id = load_filename_to_load_file_id( :1  ) and\n                not cb.settlement_rec_id is null    -- only work resolved items";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:753^7*/

      /*@lineinfo:generated-code*//*@lineinfo:755^7*/

//  ************************************************************
//  #sql [Ctx] { insert into q_data
//          (
//            id,
//            type,
//            item_type,
//            owner,
//            date_created,
//            source,
//            affiliate,
//            last_changed,
//            last_user
//          )
//          select  cb.load_sec                     as id,
//                  decode(cb.settlement_rec_id,
//                         null,:MesQueues.Q_CHARGEBACKS_AMEX,  -- 1787,
//                         :MesQueues.Q_CHARGEBACKS_MCHB        -- 1763
//                        )                         as type,
//                  18                              as item_type, -- chargeback
//                  1                               as owner,
//                  cb.incoming_date                as date_created,
//                  'amex'                          as source,
//                  to_char(nvl(mf.bank_number,3941))
//                                                  as affiliate,
//                  sysdate                         as last_changed,
//                  'system'                        as last_user
//          from    network_chargeback_amex   cb,
//                  mif                       mf
//          where   cb.load_file_id = load_filename_to_load_file_id(:loadFilename) and
//                  mf.merchant_number(+) = cb.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into q_data\n        (\n          id,\n          type,\n          item_type,\n          owner,\n          date_created,\n          source,\n          affiliate,\n          last_changed,\n          last_user\n        )\n        select  cb.load_sec                     as id,\n                decode(cb.settlement_rec_id,\n                       null, :1  ,  -- 1787,\n                        :2          -- 1763\n                      )                         as type,\n                18                              as item_type, -- chargeback\n                1                               as owner,\n                cb.incoming_date                as date_created,\n                'amex'                          as source,\n                to_char(nvl(mf.bank_number,3941))\n                                                as affiliate,\n                sysdate                         as last_changed,\n                'system'                        as last_user\n        from    network_chargeback_amex   cb,\n                mif                       mf\n        where   cb.load_file_id = load_filename_to_load_file_id( :3  ) and\n                mf.merchant_number(+) = cb.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"12com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_CHARGEBACKS_AMEX);
   __sJT_st.setInt(2,MesQueues.Q_CHARGEBACKS_MCHB);
   __sJT_st.setString(3,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:786^7*/

      // queue the loading of chargeback risk points
      createProcessTableEntries(loadFilename);

      /*@lineinfo:generated-code*//*@lineinfo:791^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:794^7*/
    }
    catch(Exception e)
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:798^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:798^34*/ } catch( Exception sqe ) {}
      logEntry("loadChargebackSystem(" + loadFilename + ")",e.toString());
    }
    finally
    {
      setAutoCommit(autoCommit);
    }
  }

  protected void loadMesSystem( String loadFilename )
  {
    switch( FileType )
    {
      case FT_RETRIEVALS_SABRE:
      case FT_RETRIEVALS_MES:
        loadRetrievalSystem(loadFilename);
        break;

      case FT_CHARGEBACKS_SABRE:
        loadChargebackSystem(loadFilename);
        break;

      default:
        logEntry("loadMesSystem("+loadFilename+")",("Invalid file type " + FileType));
        break;
    }
  }

  public void loadReconFile( String inputFilename )
  {
    Vector            duplicateCases  = new Vector();
    FlatFileRecord    ffd             = null;
    HashMap           ffds            = new HashMap();
    BufferedReader    in              = null;
    String            line            = null;
    long              loadFileId      = 0L;
    String            loadFilename    = null;
    long              loadSec         = 0L;
    Date              paymentDate     = null;
    String            recKey          = null;
    char              recType         = '\0';

    try
    {
      ffds.put("DFHDR", new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_EPRAW_HDR       ) );
      ffds.put("00"   , new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_EPRAW_SUMMARY   ) );
      ffds.put("10"   , new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_EPRAW_DTL_SOC   ) );
      ffds.put("20"   , new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_EPRAW_DTL_CB    ) );
      ffds.put("30"   , new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_EPRAW_DTL_ADJ   ) );
      ffds.put("DFTRL", new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_EPRAW_TRL       ) );

      // multipe other fee types
      ffd = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_EPRAW_DTL_OTHER );
      ffds.put("40"   , ffd );    // assets billing
      ffds.put("41"   , ffd );    // take-one commissions
      ffds.put("50"   , ffd );    // other fees

      loadFilename  = FileUtils.getNameFromFullPath(inputFilename).toLowerCase();
      loadFileId    = loadFilenameToLoadFileId(loadFilename);

      /*@lineinfo:generated-code*//*@lineinfo:858^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    amex_settlement_recon
//          where   load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    amex_settlement_recon\n        where   load_file_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:863^7*/

      in = new BufferedReader( new FileReader(inputFilename) );

      while( (line = in.readLine()) != null )
      {
        recType = line.charAt(0);
        recKey  = (recType == 'D' ? line.substring(0,5) : line.substring(43,45));

        ffd = (FlatFileRecord)ffds.get(recKey);

        if ( ffd != null )
        {
          ffd.resetAllFields();
          ffd.suck(line);

          if ( recType != 'D' )
          {
            loadSec = 0;    // reset the chargeback control number

            switch( ffd.getDefType() )
            {
              case MesFlatFiles.DEF_TYPE_AMEX_EPRAW_DTL_ADJ:
                loadSec = findChargebackLoadSec(ffd);
                break;
                
              case MesFlatFiles.DEF_TYPE_AMEX_EPRAW_SUMMARY:
                paymentDate = ffd.getFieldAsSqlDate("payment_date");
                break;
            }

            /*@lineinfo:generated-code*//*@lineinfo:894^13*/

//  ************************************************************
//  #sql [Ctx] { insert into amex_settlement_recon
//                (
//                  amex_se_number,
//                  amex_payee_number,
//                  se_unit_number,
//                  cb_load_sec,
//                  record_type,
//                  detail_record_type,
//                  payment_year,
//                  payment_number,
//                  payment_date,
//                  payment_amount,
//                  debit_balance_amount,
//                  transit_routing,
//                  dda_number,
//                  business_date,
//                  amex_process_date,
//                  amex_sort_field_1,
//                  amex_sort_field_2,
//                  amex_gross_amount,
//                  amex_roc_count,
//                  soc_invoice_number,
//                  soc_amount,
//                  discount_amount,
//                  service_fee_amount,
//                  net_soc_amount,
//                  discount_rate,
//                  service_fee_rate,
//                  tracking_id,
//                  cpc_indicator,
//                  amex_roc_count_poa,
//                  chargeback_amount,
//                  net_chargeback_amount,
//                  adjustment_description,
//                  adjustment_number,
//                  adjustment_amount,
//                  net_adjustment_amount,
//                  card_number,
//                  asset_billing_amount,
//                  asset_billing_description,
//                  take_one_commission_amount,
//                  take_one_description,
//                  other_fee_amount,
//                  other_fee_description,
//                  asset_billing_tax,
//                  pay_in_gross_indicator,
//                  load_filename,
//                  load_file_id
//                )
//                values
//                (
//                  :ffd.getFieldData("amex_se_number").trim(),
//                  :ffd.getFieldData("amex_payee_number").trim(),
//                  :ffd.getFieldData("se_unit_number").trim(),
//                  :loadSec,
//                  :ffd.getFieldData("record_type").trim(),
//                  :ffd.getFieldData("detail_record_type").trim(),
//                  :ffd.getFieldData("payment_year").trim(),
//                  :ffd.getFieldData("payment_number").trim(),
//                  :paymentDate,
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("payment_amount").trim(),2),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("debit_balance_amount").trim(),2),
//                  :ffd.getFieldData("transit_routing").trim(),
//                  :ffd.getFieldData("dda_number").trim(),
//                  :ffd.getFieldData("business_date").trim(),
//                  to_date(:ffd.getFieldData("amex_process_date").trim(),'yyyyddd'),
//                  :ffd.getFieldData("amex_sort_field_1").trim(),
//                  :ffd.getFieldData("amex_sort_field_2").trim(),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("amex_gross_amount").trim(),2),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("amex_roc_count").trim(),0),
//                  :ffd.getFieldData("soc_invoice_number").trim(),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("soc_amount").trim(),2),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("discount_amount").trim(),2),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("service_fee_amount").trim(),2),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("net_soc_amount").trim(),2),
//                  :ffd.getFieldData("discount_rate").trim(),
//                  :ffd.getFieldData("service_fee_rate").trim(),
//                  :ffd.getFieldData("tracking_id").trim(),
//                  :ffd.getFieldData("cpc_indicator").trim(),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("amex_roc_count_poa").trim(),0),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("chargeback_amount").trim(),2),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("net_chargeback_amount").trim(),2),
//                  :ffd.getFieldData("adjustment_description").trim(),
//                  :ffd.getFieldData("adjustment_number").trim(),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("adjustment_amount").trim(),2),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("net_adjustment_amount").trim(),2),
//                  :ffd.getFieldData("card_number").trim(),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("asset_billing_amount").trim(),2),
//                  :ffd.getFieldData("asset_billing_description").trim(),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("take_one_commission_amount").trim(),2),
//                  :ffd.getFieldData("take_one_description").trim(),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("other_fee_amount").trim(),2),
//                  :ffd.getFieldData("other_fee_description").trim(),
//                  :TridentTools.decodeCobolAmount(ffd.getFieldData("asset_billing_tax").trim(),2),
//                  :ffd.getFieldData("pay_in_gross_indicator").trim(),
//                  :loadFilename,
//                  :loadFileId
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3683 = ffd.getFieldData("amex_se_number").trim();
 String __sJT_3684 = ffd.getFieldData("amex_payee_number").trim();
 String __sJT_3685 = ffd.getFieldData("se_unit_number").trim();
 String __sJT_3686 = ffd.getFieldData("record_type").trim();
 String __sJT_3687 = ffd.getFieldData("detail_record_type").trim();
 String __sJT_3688 = ffd.getFieldData("payment_year").trim();
 String __sJT_3689 = ffd.getFieldData("payment_number").trim();
 double __sJT_3690 = TridentTools.decodeCobolAmount(ffd.getFieldData("payment_amount").trim(),2);
 double __sJT_3691 = TridentTools.decodeCobolAmount(ffd.getFieldData("debit_balance_amount").trim(),2);
 String __sJT_3692 = ffd.getFieldData("transit_routing").trim();
 String __sJT_3693 = ffd.getFieldData("dda_number").trim();
 String __sJT_3694 = ffd.getFieldData("business_date").trim();
 String __sJT_3695 = ffd.getFieldData("amex_process_date").trim();
 String __sJT_3696 = ffd.getFieldData("amex_sort_field_1").trim();
 String __sJT_3697 = ffd.getFieldData("amex_sort_field_2").trim();
 double __sJT_3698 = TridentTools.decodeCobolAmount(ffd.getFieldData("amex_gross_amount").trim(),2);
 double __sJT_3699 = TridentTools.decodeCobolAmount(ffd.getFieldData("amex_roc_count").trim(),0);
 String __sJT_3700 = ffd.getFieldData("soc_invoice_number").trim();
 double __sJT_3701 = TridentTools.decodeCobolAmount(ffd.getFieldData("soc_amount").trim(),2);
 double __sJT_3702 = TridentTools.decodeCobolAmount(ffd.getFieldData("discount_amount").trim(),2);
 double __sJT_3703 = TridentTools.decodeCobolAmount(ffd.getFieldData("service_fee_amount").trim(),2);
 double __sJT_3704 = TridentTools.decodeCobolAmount(ffd.getFieldData("net_soc_amount").trim(),2);
 String __sJT_3705 = ffd.getFieldData("discount_rate").trim();
 String __sJT_3706 = ffd.getFieldData("service_fee_rate").trim();
 String __sJT_3707 = ffd.getFieldData("tracking_id").trim();
 String __sJT_3708 = ffd.getFieldData("cpc_indicator").trim();
 double __sJT_3709 = TridentTools.decodeCobolAmount(ffd.getFieldData("amex_roc_count_poa").trim(),0);
 double __sJT_3710 = TridentTools.decodeCobolAmount(ffd.getFieldData("chargeback_amount").trim(),2);
 double __sJT_3711 = TridentTools.decodeCobolAmount(ffd.getFieldData("net_chargeback_amount").trim(),2);
 String __sJT_3712 = ffd.getFieldData("adjustment_description").trim();
 String __sJT_3713 = ffd.getFieldData("adjustment_number").trim();
 double __sJT_3714 = TridentTools.decodeCobolAmount(ffd.getFieldData("adjustment_amount").trim(),2);
 double __sJT_3715 = TridentTools.decodeCobolAmount(ffd.getFieldData("net_adjustment_amount").trim(),2);
 String __sJT_3716 = ffd.getFieldData("card_number").trim();
 double __sJT_3717 = TridentTools.decodeCobolAmount(ffd.getFieldData("asset_billing_amount").trim(),2);
 String __sJT_3718 = ffd.getFieldData("asset_billing_description").trim();
 double __sJT_3719 = TridentTools.decodeCobolAmount(ffd.getFieldData("take_one_commission_amount").trim(),2);
 String __sJT_3720 = ffd.getFieldData("take_one_description").trim();
 double __sJT_3721 = TridentTools.decodeCobolAmount(ffd.getFieldData("other_fee_amount").trim(),2);
 String __sJT_3722 = ffd.getFieldData("other_fee_description").trim();
 double __sJT_3723 = TridentTools.decodeCobolAmount(ffd.getFieldData("asset_billing_tax").trim(),2);
 String __sJT_3724 = ffd.getFieldData("pay_in_gross_indicator").trim();
   String theSqlTS = "insert into amex_settlement_recon\n              (\n                amex_se_number,\n                amex_payee_number,\n                se_unit_number,\n                cb_load_sec,\n                record_type,\n                detail_record_type,\n                payment_year,\n                payment_number,\n                payment_date,\n                payment_amount,\n                debit_balance_amount,\n                transit_routing,\n                dda_number,\n                business_date,\n                amex_process_date,\n                amex_sort_field_1,\n                amex_sort_field_2,\n                amex_gross_amount,\n                amex_roc_count,\n                soc_invoice_number,\n                soc_amount,\n                discount_amount,\n                service_fee_amount,\n                net_soc_amount,\n                discount_rate,\n                service_fee_rate,\n                tracking_id,\n                cpc_indicator,\n                amex_roc_count_poa,\n                chargeback_amount,\n                net_chargeback_amount,\n                adjustment_description,\n                adjustment_number,\n                adjustment_amount,\n                net_adjustment_amount,\n                card_number,\n                asset_billing_amount,\n                asset_billing_description,\n                take_one_commission_amount,\n                take_one_description,\n                other_fee_amount,\n                other_fee_description,\n                asset_billing_tax,\n                pay_in_gross_indicator,\n                load_filename,\n                load_file_id\n              )\n              values\n              (\n                 :1  ,\n                 :2  ,\n                 :3  ,\n                 :4  ,\n                 :5  ,\n                 :6  ,\n                 :7  ,\n                 :8  ,\n                 :9  ,\n                 :10  ,\n                 :11  ,\n                 :12  ,\n                 :13  ,\n                 :14  ,\n                to_date( :15  ,'yyyyddd'),\n                 :16  ,\n                 :17  ,\n                 :18  ,\n                 :19  ,\n                 :20  ,\n                 :21  ,\n                 :22  ,\n                 :23  ,\n                 :24  ,\n                 :25  ,\n                 :26  ,\n                 :27  ,\n                 :28  ,\n                 :29  ,\n                 :30  ,\n                 :31  ,\n                 :32  ,\n                 :33  ,\n                 :34  ,\n                 :35  ,\n                 :36  ,\n                 :37  ,\n                 :38  ,\n                 :39  ,\n                 :40  ,\n                 :41  ,\n                 :42  ,\n                 :43  ,\n                 :44  ,\n                 :45  ,\n                 :46  \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3683);
   __sJT_st.setString(2,__sJT_3684);
   __sJT_st.setString(3,__sJT_3685);
   __sJT_st.setLong(4,loadSec);
   __sJT_st.setString(5,__sJT_3686);
   __sJT_st.setString(6,__sJT_3687);
   __sJT_st.setString(7,__sJT_3688);
   __sJT_st.setString(8,__sJT_3689);
   __sJT_st.setDate(9,paymentDate);
   __sJT_st.setDouble(10,__sJT_3690);
   __sJT_st.setDouble(11,__sJT_3691);
   __sJT_st.setString(12,__sJT_3692);
   __sJT_st.setString(13,__sJT_3693);
   __sJT_st.setString(14,__sJT_3694);
   __sJT_st.setString(15,__sJT_3695);
   __sJT_st.setString(16,__sJT_3696);
   __sJT_st.setString(17,__sJT_3697);
   __sJT_st.setDouble(18,__sJT_3698);
   __sJT_st.setDouble(19,__sJT_3699);
   __sJT_st.setString(20,__sJT_3700);
   __sJT_st.setDouble(21,__sJT_3701);
   __sJT_st.setDouble(22,__sJT_3702);
   __sJT_st.setDouble(23,__sJT_3703);
   __sJT_st.setDouble(24,__sJT_3704);
   __sJT_st.setString(25,__sJT_3705);
   __sJT_st.setString(26,__sJT_3706);
   __sJT_st.setString(27,__sJT_3707);
   __sJT_st.setString(28,__sJT_3708);
   __sJT_st.setDouble(29,__sJT_3709);
   __sJT_st.setDouble(30,__sJT_3710);
   __sJT_st.setDouble(31,__sJT_3711);
   __sJT_st.setString(32,__sJT_3712);
   __sJT_st.setString(33,__sJT_3713);
   __sJT_st.setDouble(34,__sJT_3714);
   __sJT_st.setDouble(35,__sJT_3715);
   __sJT_st.setString(36,__sJT_3716);
   __sJT_st.setDouble(37,__sJT_3717);
   __sJT_st.setString(38,__sJT_3718);
   __sJT_st.setDouble(39,__sJT_3719);
   __sJT_st.setString(40,__sJT_3720);
   __sJT_st.setDouble(41,__sJT_3721);
   __sJT_st.setString(42,__sJT_3722);
   __sJT_st.setDouble(43,__sJT_3723);
   __sJT_st.setString(44,__sJT_3724);
   __sJT_st.setString(45,loadFilename);
   __sJT_st.setLong(46,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:994^13*/
          }
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadReconFile("+recKey+","+loadFilename+")",e.toString());
    }
    finally
    {
      try{ in.close(); } catch( Exception e ) {}
    }
  }

  protected void loadRejectFile( String inputFilename ) 
  {
    boolean             aggregatorSE    = false;
    Vector              batchRecIds     = new Vector();
    boolean             fileVerified    = false;
    BufferedReader      in              = null;
    ResultSetIterator   it              = null;
    ResultSet           resultSet       = null;
    String              reasonCode      = null;
    String              workFilename    = null;

    try 
    {
      AmexRejectParser parser = new AmexRejectParser();
      fileVerified = parser.verifyFile(inputFilename);
      workFilename = parser.getWorkFilename();
    
      HashMap recordErrorMap  = parser.getRecordErrorMap();
      
      // if file is verified and there are rejects, scan the file data
      if ( fileVerified && recordErrorMap.size() > 0 ) 
      {
        SettlementRecordAmex  rec           = new SettlementRecordAmex();
        SettlementDb          db            = new SettlementDb();
        List                  ffds          = null;
        long                  batchId       = 0L;
        int                   fileRecCount  = 1; // 1 for file header
        HashMap               rejectMap     = new HashMap();
        int                   lastLine      = 0;
        
        // determine the last bad line number so we can 
        // stop scanning once we have found all the rejects
        Iterator iter = recordErrorMap.keySet().iterator();
        while( iter.hasNext() )
        {
          int lineNumber = Integer.parseInt((String)iter.next());
          lastLine = Math.max(lastLine,lineNumber);
        }
        log.debug("Last Line: " + lastLine);
      
        if ( parser.isReprocessedRejectsFile() )
        {
          /*@lineinfo:generated-code*//*@lineinfo:1051^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  s.*
//              from    amex_settlement_activity  sa,
//                      amex_settlement           s
//              where   sa.load_file_id = load_filename_to_load_file_id(:workFilename)
//                      and s.rec_id = sa.rec_id
//                      and s.test_flag = 'N'
//              order by s.batch_id, s.batch_record_id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  s.*\n            from    amex_settlement_activity  sa,\n                    amex_settlement           s\n            where   sa.load_file_id = load_filename_to_load_file_id( :1  )\n                    and s.rec_id = sa.rec_id\n                    and s.test_flag = 'N'\n            order by s.batch_id, s.batch_record_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"15com.mes.startup.AmexCBLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1060^11*/
        }
        else    // assume first presentments
        {
          /*@lineinfo:generated-code*//*@lineinfo:1064^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  s.*
//              from    amex_settlement   s
//              where   s.output_file_id = load_filename_to_load_file_id(:workFilename)
//                      and s.test_flag = 'N'
//              order by s.batch_id, s.batch_record_id
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  s.*\n            from    amex_settlement   s\n            where   s.output_file_id = load_filename_to_load_file_id( :1  )\n                    and s.test_flag = 'N'\n            order by s.batch_id, s.batch_record_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"16com.mes.startup.AmexCBLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1071^11*/
        }
        resultSet = it.getResultSet();
        
        boolean emvEnabled = false;
        
        try{
            PropertiesFile pf = new PropertiesFile("emv.properties");
            emvEnabled = "yes".equalsIgnoreCase(pf.getString("amex")) ? true : false;
        }catch(Exception e){
            log.error("EmvEnabled property not found for AmexCBLoader", e);
        }
      
        while (resultSet.next()) 
        {
          if ( batchId != resultSet.getLong("batch_id") ) 
          {
            if ( batchId != 0L )
            {
              ++fileRecCount;   // add one for the batch trailer record (TBT)
        	  rejectMap = rejectBatchIfTrailerIsRejected(batchId, fileRecCount, rejectMap, batchRecIds, reasonCode, recordErrorMap, aggregatorSE);
            }
            batchId = resultSet.getLong("batch_id");
            batchRecIds.removeAllElements();
            aggregatorSE = isAggregatorSE(resultSet.getInt("bank_number"),
                                          resultSet.getLong("amex_se_number"));
          }
          
          rec.clear();
          rec.setIsEmvEnabled( emvEnabled );
          rec.setFields(resultSet, false, false);

          if (rec.hasLevelIIIData()) 
          {
            rec.setLineItems(db._loadSettlementLineItemDetailRecords(rec.getLong("rec_id"), SettlementRecord.SETTLE_REC_AMEX));
          }
          ffds = rec.buildNetworkRecords(SettlementRecord.MT_FIRST_PRESENTMENT);
          batchRecIds.addElement(rec.getString("rec_id"));
          
          for( int i = 0; i < ffds.size(); ++i )
          {
            ++fileRecCount;
          
            // if this record rejected, then add the reject reason code
            // to the list of reject reason codes for this rec id
            if ( (reasonCode = (String)recordErrorMap.get(String.valueOf(fileRecCount))) != null )
            {
              String recId        = rec.getString("rec_id");
              String reasonCodes  = (String)rejectMap.get(recId);
              reasonCodes = ((reasonCodes == null) ? reasonCode : (reasonCodes + "," + reasonCode));
              reasonCodes += (aggregatorSE ? "A" : "");
              rejectMap.put(recId,reasonCodes);
            }
          }
          
          if ( fileRecCount > lastLine )
          {
            break;
          }
        }
        
        if ( batchId != 0L )
        {
          ++fileRecCount;   // add one for the FINAL batch trailer record (TBT)
    	  rejectMap = rejectBatchIfTrailerIsRejected(batchId, fileRecCount, rejectMap, batchRecIds, reasonCode, recordErrorMap, aggregatorSE);
        }
        
        resultSet.close();
        it.close();
        
        // store the rejected records into the database
        iter = rejectMap.keySet().iterator();
        while( iter.hasNext() )
        {
          long recId = Long.parseLong((String)iter.next());
          String[] reasonCodes = ((String)rejectMap.get(String.valueOf(recId))).split(",");
          for( int i = 0; i < reasonCodes.length; ++i )
          {
            insertRejectRecord(recId, reasonCodes[i], workFilename);
          }
        }
      }   // end if (fileVerified)
    } 
    catch (Exception e) 
    {
      logEntry("loadRejectFile(" + workFilename + ")", e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      try { in.close(); } catch (Exception e) {}
    }
  }

  public void loadRetrievalFile( String inputFilename )
  {
    FlatFileRecord    ffd             = null;
    HashMap           ffds            = new HashMap();
    BufferedReader    in              = null;
    Date              incomingDate    = null;
    ResultSetIterator it              = null;
    String            line            = null;
    long              loadFileId      = 0L;
    String            loadFilename    = null;
    long              loadSecOrig     = 0L;
    int               recCount        = 0;
    String            recKey          = null;
    char              recType         = '\0';
    int               typeOrig        = 0;

    try
    {
      ffds.put("H"      , new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_INQ02_HDR       ) );
      ffds.put("D-AIRDS", new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_INQ02_DTL_AIRDS ) );
      ffds.put("D-AIRLT", new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_INQ02_DTL_AIRLT ) );
      ffds.put("D-AIRRT", new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_INQ02_DTL_AIRRT ) );
      ffds.put("D-AIRTB", new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_INQ02_DTL_AIRTB ) );
      ffds.put("D-AREXS", new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_INQ02_DTL_AREXS ) );
      ffds.put("D-CARRD", new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_INQ02_DTL_CARRD ) );
      ffds.put("D-GSDIS", new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_INQ02_DTL_GSDIS ) );
      ffds.put("D-NAXMG", new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_INQ02_DTL_NAXMG ) );
      ffds.put("D-NAXMR", new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_INQ02_DTL_NAXMR ) );
      ffds.put("D-SEDIS", new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_INQ02_DTL_SEDIS ) );
      ffds.put("D-FRAUD", new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_INQ02_DTL_SEDIS ) );
      ffds.put("T"      , new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_INQ02_TRL       ) );

      loadFilename  = FileUtils.getNameFromFullPath(inputFilename).toLowerCase();
      loadFileId    = loadFilenameToLoadFileId(loadFilename);

      in = new BufferedReader( new FileReader(inputFilename) );

      while( (line = in.readLine()) != null )
      {
        recType = line.charAt(0);
        recKey  = (recType == 'D' ? (recType + "-" + line.substring(60,65)) : String.valueOf(recType));

        ffd = (FlatFileRecord)ffds.get(recKey);

        if ( ffd != null )
        {
          ffd.resetAllFields();
          ffd.suck(line);

          if ( recType == 'H' )
          {
            /*@lineinfo:generated-code*//*@lineinfo:1213^13*/

//  ************************************************************
//  #sql [Ctx] { select  to_date(:ffd.getFieldData("transmission_date"),'yyyyddd')
//                
//                from    dual
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3725 = ffd.getFieldData("transmission_date");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  to_date( :1  ,'yyyyddd')\n               \n              from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.AmexCBLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,__sJT_3725);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   incomingDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1218^13*/
          }
          else if ( recType == 'D' )
          {
            long loadSec = ChargebackTools.getNewLoadSec();

            /*@lineinfo:generated-code*//*@lineinfo:1224^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  nra.load_sec                                          as load_sec,
//                        nvl(qd.type,:MesQueues.Q_RETRIEVALS_AMEX_INCOMING)  as queue_type
//                from    network_retrieval_amex    nra,
//                        q_data                    qd
//                where   case_number = :ffd.getFieldData("case_number").trim()
//                        and nvl(nra.inquiry_seq_num,0) = :ffd.getFieldAsInt("inquiry_seq_num",0)
//                        and qd.id(+) = nra.load_sec
//                        and qd.item_type(+) = :MesQueues.Q_ITEM_TYPE_RETRIEVAL
//                order by nra.load_sec
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3726 = ffd.getFieldData("case_number").trim();
 int __sJT_3727 = ffd.getFieldAsInt("inquiry_seq_num",0);
  try {
   String theSqlTS = "select  nra.load_sec                                          as load_sec,\n                      nvl(qd.type, :1  )  as queue_type\n              from    network_retrieval_amex    nra,\n                      q_data                    qd\n              where   case_number =  :2  \n                      and nvl(nra.inquiry_seq_num,0) =  :3  \n                      and qd.id(+) = nra.load_sec\n                      and qd.item_type(+) =  :4  \n              order by nra.load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,MesQueues.Q_RETRIEVALS_AMEX_INCOMING);
   __sJT_st.setString(2,__sJT_3726);
   __sJT_st.setInt(3,__sJT_3727);
   __sJT_st.setInt(4,MesQueues.Q_ITEM_TYPE_RETRIEVAL);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.startup.AmexCBLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1235^13*/
            ResultSet resultSet = it.getResultSet();

            if ( resultSet.next() )
            {
              loadSecOrig = resultSet.getLong("load_sec");
              typeOrig    = resultSet.getInt("queue_type");
            }
            else
            {
              loadSecOrig = 0L;
            }
            resultSet.close();
            it.close();

            // extract the foreign currency amount from free text
            long    foreignAmount = 0L;
            String  numberStr     = "";
            String  tempStr       = ffd.getFieldData("foreign_amount").trim();
            for( int i = 0; i < tempStr.length(); ++i )
            {
              if ( Character.isDigit( tempStr.charAt(i) ) )
              {
                numberStr += tempStr.charAt(i);
              }
            }
            if ( numberStr.length() > 0 )
            {
              foreignAmount = Long.parseLong(numberStr);
            }

            /*@lineinfo:generated-code*//*@lineinfo:1266^13*/

//  ************************************************************
//  #sql [Ctx] { insert into network_retrieval_amex
//                (
//                  load_sec,
//                  case_number,
//                  reply_by_date,
//                  card_number,
//                  transaction_date,
//                  transaction_amount,
//                  retrieval_amount,
//                  cardholder_name,
//                  charge_ref_num,
//                  se_number,
//                  retrieval_type,
//                  reason_code,
//                  incoming_date,
//                  follow_up_reason_code,
//                  inquiry_source,
//                  inquiry_seq_num,
//                  inquiry_action_number,
//                  document_indicator,
//                  amex_id,
//                  foreign_amount,
//                  airline_ticket_number,
//                  inquiry_note_1,
//                  inquiry_note_2,
//                  inquiry_note_3,
//                  inquiry_note_4,
//                  inquiry_note_5,
//                  inquiry_note_6,
//                  inquiry_note_7,
//                  load_filename,
//                  load_file_id
//                )
//                values
//                (
//                  :loadSec,
//                  :ffd.getFieldData("case_number").trim(),      -- case #
//                  :ffd.getFieldAsSqlDate("reply_by_date"),      -- reply-by
//                  :ffd.getFieldData("card_number").trim(),      -- card #
//                  :ffd.getFieldAsSqlDate("tran_date"),          -- tran date
//                  :ffd.getFieldAsDouble("tran_amount"),         -- tran amount
//                  :ffd.getFieldAsDouble("disputed_amount"),     -- disputed amount
//                  :ffd.getFieldData("cardmember_name_1").trim(),-- cardmember name
//                  :ffd.getFieldData("reference_number").trim(), -- ref number
//                  :ffd.getFieldData("se_number").trim(),        -- se number
//                  :ffd.getFieldData("case_type").trim(),        -- case type
//                  :ffd.getFieldData("reason_code").trim(),      -- reason code
//                  :incomingDate,
//                  :ffd.getFieldData("follow_up_reason_code").trim(),
//                  :ffd.getFieldData("inquiry_source").trim(),
//                  :ffd.getFieldAsInt("inquiry_seq_num",0),
//                  :ffd.getFieldData("inquiry_action_number").trim(),
//                  :ffd.getFieldData("document_indicator").trim(),
//                  :ffd.getFieldData("amex_id").trim(),
//                  decode(:foreignAmount,0,null,(:foreignAmount * 0.01)),  -- foreign amount
//                  :ffd.getFieldData("airline_ticket_number").trim(),
//                  :ffd.getFieldData("inquiry_note_1").trim(),
//                  :ffd.getFieldData("inquiry_note_2").trim(),
//                  :ffd.getFieldData("inquiry_note_3").trim(),
//                  :ffd.getFieldData("inquiry_note_4").trim(),
//                  :ffd.getFieldData("inquiry_note_5").trim(),
//                  :ffd.getFieldData("inquiry_note_6").trim(),
//                  :ffd.getFieldData("inquiry_note_7").trim(),
//                  :loadFilename,
//                  :loadFileId
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3728 = ffd.getFieldData("case_number").trim();
 java.sql.Date __sJT_3729 = ffd.getFieldAsSqlDate("reply_by_date");
 String __sJT_3730 = ffd.getFieldData("card_number").trim();
 java.sql.Date __sJT_3731 = ffd.getFieldAsSqlDate("tran_date");
 double __sJT_3732 = ffd.getFieldAsDouble("tran_amount");
 double __sJT_3733 = ffd.getFieldAsDouble("disputed_amount");
 String __sJT_3734 = ffd.getFieldData("cardmember_name_1").trim();
 String __sJT_3735 = ffd.getFieldData("reference_number").trim();
 String __sJT_3736 = ffd.getFieldData("se_number").trim();
 String __sJT_3737 = ffd.getFieldData("case_type").trim();
 String __sJT_3738 = ffd.getFieldData("reason_code").trim();
 String __sJT_3739 = ffd.getFieldData("follow_up_reason_code").trim();
 String __sJT_3740 = ffd.getFieldData("inquiry_source").trim();
 int __sJT_3741 = ffd.getFieldAsInt("inquiry_seq_num",0);
 String __sJT_3742 = ffd.getFieldData("inquiry_action_number").trim();
 String __sJT_3743 = ffd.getFieldData("document_indicator").trim();
 String __sJT_3744 = ffd.getFieldData("amex_id").trim();
 String __sJT_3745 = ffd.getFieldData("airline_ticket_number").trim();
 String __sJT_3746 = ffd.getFieldData("inquiry_note_1").trim();
 String __sJT_3747 = ffd.getFieldData("inquiry_note_2").trim();
 String __sJT_3748 = ffd.getFieldData("inquiry_note_3").trim();
 String __sJT_3749 = ffd.getFieldData("inquiry_note_4").trim();
 String __sJT_3750 = ffd.getFieldData("inquiry_note_5").trim();
 String __sJT_3751 = ffd.getFieldData("inquiry_note_6").trim();
 String __sJT_3752 = ffd.getFieldData("inquiry_note_7").trim();
   String theSqlTS = "insert into network_retrieval_amex\n              (\n                load_sec,\n                case_number,\n                reply_by_date,\n                card_number,\n                transaction_date,\n                transaction_amount,\n                retrieval_amount,\n                cardholder_name,\n                charge_ref_num,\n                se_number,\n                retrieval_type,\n                reason_code,\n                incoming_date,\n                follow_up_reason_code,\n                inquiry_source,\n                inquiry_seq_num,\n                inquiry_action_number,\n                document_indicator,\n                amex_id,\n                foreign_amount,\n                airline_ticket_number,\n                inquiry_note_1,\n                inquiry_note_2,\n                inquiry_note_3,\n                inquiry_note_4,\n                inquiry_note_5,\n                inquiry_note_6,\n                inquiry_note_7,\n                load_filename,\n                load_file_id\n              )\n              values\n              (\n                 :1  ,\n                 :2  ,      -- case #\n                 :3  ,      -- reply-by\n                 :4  ,      -- card #\n                 :5  ,          -- tran date\n                 :6  ,         -- tran amount\n                 :7  ,     -- disputed amount\n                 :8  ,-- cardmember name\n                 :9  , -- ref number\n                 :10  ,        -- se number\n                 :11  ,        -- case type\n                 :12  ,      -- reason code\n                 :13  ,\n                 :14  ,\n                 :15  ,\n                 :16  ,\n                 :17  ,\n                 :18  ,\n                 :19  ,\n                decode( :20  ,0,null,( :21   * 0.01)),  -- foreign amount\n                 :22  ,\n                 :23  ,\n                 :24  ,\n                 :25  ,\n                 :26  ,\n                 :27  ,\n                 :28  ,\n                 :29  ,\n                 :30  ,\n                 :31  \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"19com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,__sJT_3728);
   __sJT_st.setDate(3,__sJT_3729);
   __sJT_st.setString(4,__sJT_3730);
   __sJT_st.setDate(5,__sJT_3731);
   __sJT_st.setDouble(6,__sJT_3732);
   __sJT_st.setDouble(7,__sJT_3733);
   __sJT_st.setString(8,__sJT_3734);
   __sJT_st.setString(9,__sJT_3735);
   __sJT_st.setString(10,__sJT_3736);
   __sJT_st.setString(11,__sJT_3737);
   __sJT_st.setString(12,__sJT_3738);
   __sJT_st.setDate(13,incomingDate);
   __sJT_st.setString(14,__sJT_3739);
   __sJT_st.setString(15,__sJT_3740);
   __sJT_st.setInt(16,__sJT_3741);
   __sJT_st.setString(17,__sJT_3742);
   __sJT_st.setString(18,__sJT_3743);
   __sJT_st.setString(19,__sJT_3744);
   __sJT_st.setLong(20,foreignAmount);
   __sJT_st.setLong(21,foreignAmount);
   __sJT_st.setString(22,__sJT_3745);
   __sJT_st.setString(23,__sJT_3746);
   __sJT_st.setString(24,__sJT_3747);
   __sJT_st.setString(25,__sJT_3748);
   __sJT_st.setString(26,__sJT_3749);
   __sJT_st.setString(27,__sJT_3750);
   __sJT_st.setString(28,__sJT_3751);
   __sJT_st.setString(29,__sJT_3752);
   __sJT_st.setString(30,loadFilename);
   __sJT_st.setLong(31,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1334^13*/

            if ( loadSecOrig != 0L )
            {
              // copy the notes from the old to the new
              QueueNotes.copyNotes(loadSecOrig,MesQueues.Q_RETRIEVALS_AMEX_INCOMING,loadSec);

              // add note indicating the second entry to both original and new
              QueueNotes.addNote(loadSecOrig,typeOrig,"system","Second request received, new control number is " + loadSec);
              QueueNotes.addNote(loadSec,MesQueues.Q_RETRIEVALS_AMEX_INCOMING,"system","Second request, original control number is " + loadSecOrig);
            }
          }   // end else if ( recType == 'D' )
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadRetrievalFile("+loadFilename+")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) {}
      try{ in.close(); } catch( Exception e ) {}
    }
  }

  protected void loadRetrievalSystem( String loadFilename )
  {
    boolean     autoCommit      = getAutoCommit();

    try
    {
      setAutoCommit(false);

      /*@lineinfo:generated-code*//*@lineinfo:1368^7*/

//  ************************************************************
//  #sql [Ctx] { insert into network_retrievals
//          (
//            retr_load_sec,
//            merchant_number,
//            incoming_date,
//            reference_number,
//            card_number,
//            tran_date,
//            tran_amount,
//            merchant_name,
//            reason_code,
//            load_filename,
//            user_message,
//            bank_number,
//            mes_ref_num,
//            card_number_enc,
//            retrieval_request_id,
//            merchant_number_missing,
//            tran_date_missing
//          )
//          select  retr.load_sec                     as load_sec,
//                  nvl(retr.merchant_number,0)       as merchant_number,
//                  retr.incoming_date                as incoming_date,
//                  nvl(retr.acq_reference_number,0)  as ref_num,
//                  retr.card_number                  as card_number,
//                  retr.transaction_date             as tran_date,
//                  retr.retrieval_amount             as tran_amount,
//                  mf.dba_name                       as merchant_name,
//                  retr.reason_code                  as reason_code,
//                  retr.load_filename                as load_filename,
//                  retr.charge_ref_num               as user_message,
//                  mf.bank_number                    as bank_number,
//                  retr.mes_ref_num                  as mes_ref_num,
//                  retr.card_number_enc              as card_number_enc,
//                  retr.case_number                  as retrieval_request_id,
//                  decode(retr.merchant_number,
//                         null,'Y','N')              as merchant_number_missing,
//                  'N'                               as tran_date_missing
//          from    network_retrieval_amex  retr,
//                  mif                     mf
//          where   retr.load_file_id = load_filename_to_load_file_id(:loadFilename) and
//                  mf.merchant_number(+) = retr.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into network_retrievals\n        (\n          retr_load_sec,\n          merchant_number,\n          incoming_date,\n          reference_number,\n          card_number,\n          tran_date,\n          tran_amount,\n          merchant_name,\n          reason_code,\n          load_filename,\n          user_message,\n          bank_number,\n          mes_ref_num,\n          card_number_enc,\n          retrieval_request_id,\n          merchant_number_missing,\n          tran_date_missing\n        )\n        select  retr.load_sec                     as load_sec,\n                nvl(retr.merchant_number,0)       as merchant_number,\n                retr.incoming_date                as incoming_date,\n                nvl(retr.acq_reference_number,0)  as ref_num,\n                retr.card_number                  as card_number,\n                retr.transaction_date             as tran_date,\n                retr.retrieval_amount             as tran_amount,\n                mf.dba_name                       as merchant_name,\n                retr.reason_code                  as reason_code,\n                retr.load_filename                as load_filename,\n                retr.charge_ref_num               as user_message,\n                mf.bank_number                    as bank_number,\n                retr.mes_ref_num                  as mes_ref_num,\n                retr.card_number_enc              as card_number_enc,\n                retr.case_number                  as retrieval_request_id,\n                decode(retr.merchant_number,\n                       null,'Y','N')              as merchant_number_missing,\n                'N'                               as tran_date_missing\n        from    network_retrieval_amex  retr,\n                mif                     mf\n        where   retr.load_file_id = load_filename_to_load_file_id( :1  ) and\n                mf.merchant_number(+) = retr.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"20com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1412^7*/

      // retrievals are put into the MES queue system
      // by a trigger on the network_retrievals table

      // queue the retrieval points and the linking of transaction and credits
      createProcessTableEntries(loadFilename);

      /*@lineinfo:generated-code*//*@lineinfo:1420^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1423^7*/
    }
    catch(Exception e)
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:1427^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1427^34*/ } catch( Exception sqe ) {}
      logEntry("loadRetrievalSystem(" + loadFilename + ")",e.toString());
    }
    finally
    {
      setAutoCommit(autoCommit);
    }
  }

  public void loadCsvData( String inputFilename )
  {
    char              ch;
    int               idx             = -1;
    BufferedReader    in              = null;
    Date              incomingDate    = null;
    String            line            = null;
    long              loadFileId      = 0L;
    String            loadFilename    = null;
    String            reasonCode      = null;
    String            seNumber        = AMEX_SE_SABRE;
    boolean           stringStarted   = false;
    StringBuffer      token           = new StringBuffer();
    int               valIdx          = 0;
    int               valueCount      = 0;
    String[]          values          = null;

    try
    {
           if ( FileType == FT_RETRIEVALS_SABRE )   { valueCount = 15;  }
      else if ( FileType == FT_CHARGEBACKS_SABRE )  { valueCount = 14;  }
      else if ( FileType == FT_RETRIEVALS_MES   )   { valueCount = 23;  }

      // create the values array for this data type
      values = new String[valueCount];

      log.debug("Processing '" + inputFilename + "'...");

      //*********************************************************************
      // FT_CHARGEBACKS_SABRE fields -
      //    [0]   Case #
      //    [1]   Cardmember #
      //    [2]   Adjustment #
      //    [3]   Charge Date
      //    [4]   Charge Amount
      //    [5]   Chargeback Amount
      //    [6]   Chargeback Date
      //    [7]   Charge Ref #
      //    [8]   Type
      //    [9]   Reason Category With Code
      //    [10]  Resolution
      //
      // FT_RETRIEVALS_SABRE fields -
      //    [0]   Case #
      //    [1]   Reply-by Date � Days Left
      //    [2]   Cardmember #
      //    [3]   Charge Date
      //    [4]   Charge Amount
      //    [5]   Disputed Amount
      //    [6]   Charge Ref #
      //    [7]   Type
      //    [8]   Reason Category With Code
      //    [9]   Resolution
      //    [10]  Status
      //
      // FT_RETRIEVALS_MES fields -
      //    [0]   Merchant #
      //    [1]   Cardmember #
      //    [2]   Case Number
      //    [3]   Status
      //    [4]   Disputed Amt.
      //    [5]   Charge Amt.
      //    [6]   Charge Date
      //    [7]   Received Date
      //    [8]   Respond By
      //    [9]   Days Left
      //    [10]  Cardmember Name
      //    [11]  Cardmember # at Transaction
      //    [12]  Location ID
      //    [13]  Case Type
      //    [14]  Dept. Code
      //    [15]  Reference #
      //    [16]  Merchandise Type
      //    [17]  Charge Reference Number:
      //    [18]  Merchandise Order Number
      //    [19]  RoC Invoice Number
      //    [20]  Reference Number
      //    [21]  Industry Location Number
      //    [22]  Reason
      //    [23]  Additional Information
      //
      //*********************************************************************

      // set default incoming date to current oracle date
      /*@lineinfo:generated-code*//*@lineinfo:1520^7*/

//  ************************************************************
//  #sql [Ctx] { select  sysdate 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sysdate  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.startup.AmexCBLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   incomingDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1524^7*/

      int offset = inputFilename.lastIndexOf( System.getProperty("file.separator") );

      if ( offset >= 0 )
      {
        loadFilename = inputFilename.substring(offset+1);
      }
      else
      {
        loadFilename = inputFilename;
      }
      loadFileId = loadFilenameToLoadFileId( loadFilename );

      in = new BufferedReader( new FileReader( inputFilename ) );

      while( ( line = in.readLine() ) != null )
      {
        try
        {
          if ( FileType == FT_RETRIEVALS_SABRE )
          {
            idx = line.indexOf("Date Range:");

            String dateString = line.substring(idx+12,line.indexOf("-"));
            incomingDate = new java.sql.Date( DateTimeFormatter.parseDate(dateString,"MMM dd yyyy").getTime());
            log.debug(dateString);
            log.debug(incomingDate);
          }
        }
        catch( Exception e )
        {
        }

        try
        {
          ch = line.charAt(0);
          if ( ch != '\'' && ch != '"' )
          {
            // skip non-data records
            continue;
          }
        }
        catch( Exception e )
        {
          continue; // skip this line
        }

        // reset indicators
        valIdx        = 0;
        stringStarted = false;
        token.setLength(0);
        for( int i = 0; i < values.length; ++i )
        {
          values[i] = "";
        }

        // create lookup tables for the parser
        // to use to extract the values correctly
        String[] currencyFields =
        {
          // strings must end with a ','!
          "4,5,",     // FT_CHARGEBACKS_SABRE
          "4,5,",     // FT_RETRIEVALS_SABRE
          "4,5,",     // FT_RETRIEVALS_MES
        };

        for( int i = 0; i < line.length() && valIdx < values.length; ++i )
        {
          ch = line.charAt(i);

          switch(ch)
          {
            case '$':
              if ( currencyFields[FileType].indexOf((String.valueOf(valIdx)+",")) >= 0 )
              {
                continue;
              }
              else
              {
                token.append(ch);
              }
              break;

            case ' ':       // skip leading spaces
            case '\'':      // skip leading ' marks
              if ( token.length() == 0 )
              {
                continue;
              }
              else
              {
                token.append(ch);
              }
              break;

            case '(':
              if ( currencyFields[FileType].indexOf((String.valueOf(valIdx)+",")) >= 0 )
              {
                token.append('-');
              }
              else
              {
                token.append(ch);
              }
              break;

            case ')':
              if ( currencyFields[FileType].indexOf((String.valueOf(valIdx)+",")) >= 0 )
              {
                continue;
              }
              else
              {
                token.append(ch);
              }
              break;

            case '"':
              stringStarted = !stringStarted;
              break;

            case ',':
              if ( !stringStarted )
              {
                values[valIdx++] = token.toString();
                token.setLength(0);
              }
              else  // comma inside a string value
              {
                // if it is *not* one of the currency fields, then append value
                if ( currencyFields[FileType].indexOf((String.valueOf(valIdx)+",")) < 0 )
                {
                  token.append(ch);
                }
              }
              break;

            default:
              token.append(ch);
              break;
          }
        }

        // index tests prevents legacy code
        // from throwing an exception if Amex adds
        // columns to the CSV output
        if ( valIdx < values.length )
        {
          values[valIdx] = token.toString();    // boundary condition
        }

        // remove any leading or trailing whitespace
        for( int i = 0; i < values.length; ++i )
        {
          values[i] = values[i].trim();
          log.debug("value[" + i + "]: " + values[i]);//@
        }
        log.debug(loadFilename);//@

        long newLoadSec = ChargebackTools.getNewLoadSec();

        if ( FileType == FT_RETRIEVALS_SABRE )
        {
          java.util.Date javaDate = DateTimeFormatter.parseDate(values[4],"MM/dd/yyyy");
          java.sql.Date  tranDate = ( (javaDate == null) ? null : new Date(javaDate.getTime()) );

          javaDate      = DateTimeFormatter.parseDate(values[3],"MM/dd/yyyy");
          incomingDate  = ( (javaDate == null) ? null : new Date(javaDate.getTime()) );

          idx = values[8].indexOf("-");
          reasonCode = values[8].substring(idx+1);

          /*@lineinfo:generated-code*//*@lineinfo:1697^11*/

//  ************************************************************
//  #sql [Ctx] { insert into network_retrieval_amex
//              (
//                load_sec,
//                case_number,
//                status,
//                reply_by_date,
//                card_number,
//                transaction_date,
//                transaction_amount,
//                retrieval_amount,
//                charge_ref_num,
//                se_number,
//                retrieval_type,
//                reason_code,
//                incoming_date,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :newLoadSec,
//                :values[0],     -- case #
//                :values[10],    -- status
//                to_date(substr(:values[1],1,10),'mm/dd/yyyy'), -- reply-by
//                :values[2],     -- card #
//                :tranDate,        -- tran date
//                :values[4],     -- tran amount
//                :values[5],     -- disputed amount
//                :values[6],     -- ref number
//                :AMEX_SE_UNKNOWN, -- se number
//                :values[7],     -- type
//                :reasonCode,      -- reason code
//                :incomingDate,
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3753 = values[0];
 String __sJT_3754 = values[10];
 String __sJT_3755 = values[1];
 String __sJT_3756 = values[2];
 String __sJT_3757 = values[4];
 String __sJT_3758 = values[5];
 String __sJT_3759 = values[6];
 String __sJT_3760 = values[7];
   String theSqlTS = "insert into network_retrieval_amex\n            (\n              load_sec,\n              case_number,\n              status,\n              reply_by_date,\n              card_number,\n              transaction_date,\n              transaction_amount,\n              retrieval_amount,\n              charge_ref_num,\n              se_number,\n              retrieval_type,\n              reason_code,\n              incoming_date,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,     -- case #\n               :3  ,    -- status\n              to_date(substr( :4  ,1,10),'mm/dd/yyyy'), -- reply-by\n               :5  ,     -- card #\n               :6  ,        -- tran date\n               :7  ,     -- tran amount\n               :8  ,     -- disputed amount\n               :9  ,     -- ref number\n               :10  , -- se number\n               :11  ,     -- type\n               :12  ,      -- reason code\n               :13  ,\n               :14  ,\n               :15  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"22com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,newLoadSec);
   __sJT_st.setString(2,__sJT_3753);
   __sJT_st.setString(3,__sJT_3754);
   __sJT_st.setString(4,__sJT_3755);
   __sJT_st.setString(5,__sJT_3756);
   __sJT_st.setDate(6,tranDate);
   __sJT_st.setString(7,__sJT_3757);
   __sJT_st.setString(8,__sJT_3758);
   __sJT_st.setString(9,__sJT_3759);
   __sJT_st.setString(10,AMEX_SE_UNKNOWN);
   __sJT_st.setString(11,__sJT_3760);
   __sJT_st.setString(12,reasonCode);
   __sJT_st.setDate(13,incomingDate);
   __sJT_st.setString(14,loadFilename);
   __sJT_st.setLong(15,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1735^11*/
        }
        else if ( FileType == FT_RETRIEVALS_MES   )
        {
          java.util.Date javaDate = DateTimeFormatter.parseDate(values[7],"MM/dd/yyyy");
          incomingDate = ((javaDate == null) ? null : new java.sql.Date(javaDate.getTime()));

          javaDate = DateTimeFormatter.parseDate(values[6],"MM/dd/yyyy");
          java.sql.Date  tranDate = ( (javaDate == null) ? null : new Date(javaDate.getTime()) );

          idx = values[22].lastIndexOf("(");
          reasonCode = values[22].substring(idx+1,idx+4);

          /*@lineinfo:generated-code*//*@lineinfo:1748^11*/

//  ************************************************************
//  #sql [Ctx] { insert into network_retrieval_amex
//              (
//                load_sec,
//                case_number,
//                status,
//                reply_by_date,
//                card_number,
//                transaction_date,
//                transaction_amount,
//                retrieval_amount,
//                cardholder_name,
//                charge_ref_num,
//                se_number,
//                retrieval_type,
//                reason_code,
//                incoming_date,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :newLoadSec,
//                :values[2],   -- case #
//                :values[3],   -- status
//                to_date(substr(:values[8],1,10),'mm/dd/yyyy'), -- reply-by
//                :values[11],  -- card # at transaction
//                :tranDate,      -- tran date
//                :values[5],   -- tran amount
//                :values[4],   -- disputed amount
//                :values[10],  -- cardholder name
//                :values[15],  -- ref number
//                :values[0],   -- se number
//                :values[13],  -- type
//                :reasonCode,    -- reason code
//                nvl(:incomingDate,trunc(sysdate)),  -- incoming date
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3761 = values[2];
 String __sJT_3762 = values[3];
 String __sJT_3763 = values[8];
 String __sJT_3764 = values[11];
 String __sJT_3765 = values[5];
 String __sJT_3766 = values[4];
 String __sJT_3767 = values[10];
 String __sJT_3768 = values[15];
 String __sJT_3769 = values[0];
 String __sJT_3770 = values[13];
   String theSqlTS = "insert into network_retrieval_amex\n            (\n              load_sec,\n              case_number,\n              status,\n              reply_by_date,\n              card_number,\n              transaction_date,\n              transaction_amount,\n              retrieval_amount,\n              cardholder_name,\n              charge_ref_num,\n              se_number,\n              retrieval_type,\n              reason_code,\n              incoming_date,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,   -- case #\n               :3  ,   -- status\n              to_date(substr( :4  ,1,10),'mm/dd/yyyy'), -- reply-by\n               :5  ,  -- card # at transaction\n               :6  ,      -- tran date\n               :7  ,   -- tran amount\n               :8  ,   -- disputed amount\n               :9  ,  -- cardholder name\n               :10  ,  -- ref number\n               :11  ,   -- se number\n               :12  ,  -- type\n               :13  ,    -- reason code\n              nvl( :14  ,trunc(sysdate)),  -- incoming date\n               :15  ,\n               :16  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,newLoadSec);
   __sJT_st.setString(2,__sJT_3761);
   __sJT_st.setString(3,__sJT_3762);
   __sJT_st.setString(4,__sJT_3763);
   __sJT_st.setString(5,__sJT_3764);
   __sJT_st.setDate(6,tranDate);
   __sJT_st.setString(7,__sJT_3765);
   __sJT_st.setString(8,__sJT_3766);
   __sJT_st.setString(9,__sJT_3767);
   __sJT_st.setString(10,__sJT_3768);
   __sJT_st.setString(11,__sJT_3769);
   __sJT_st.setString(12,__sJT_3770);
   __sJT_st.setString(13,reasonCode);
   __sJT_st.setDate(14,incomingDate);
   __sJT_st.setString(15,loadFilename);
   __sJT_st.setLong(16,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1788^11*/
        }
        else    // chargeback file
        {
          java.util.Date javaDate = DateTimeFormatter.parseDate(values[6],"MM/dd/yyyy");
          incomingDate = ((javaDate == null) ? null : new java.sql.Date(javaDate.getTime()));

          idx = values[9].indexOf("-");
          reasonCode = values[9].substring(idx+1);

          /*@lineinfo:generated-code*//*@lineinfo:1798^11*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargeback_amex
//              (
//                load_sec,
//                case_number,
//                status,
//                card_number,
//                transaction_date,
//                transaction_amount,
//                incoming_date,
//                chargeback_amount,
//                se_number,
//                adjustment_ref_num,
//                chargeback_type,
//                reason_code,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :newLoadSec,
//                :values[0],   -- case #
//                null,           -- status
//                :values[1],   -- card #
//                to_date(:values[3],'mm/dd/yyyy'), -- tran date
//                :values[4],   -- tran amount
//                nvl(:incomingDate,trunc(sysdate)),  -- cb date
//                :values[5],   -- cb amount
//                :seNumber,      -- se number
//                decode(:values[2],'N/A',null,:values[2]),   -- adj #
//                :values[8],   -- type
//                :reasonCode,    -- reason code
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3771 = values[0];
 String __sJT_3772 = values[1];
 String __sJT_3773 = values[3];
 String __sJT_3774 = values[4];
 String __sJT_3775 = values[5];
 String __sJT_3776 = values[2];
 String __sJT_3777 = values[2];
 String __sJT_3778 = values[8];
   String theSqlTS = "insert into network_chargeback_amex\n            (\n              load_sec,\n              case_number,\n              status,\n              card_number,\n              transaction_date,\n              transaction_amount,\n              incoming_date,\n              chargeback_amount,\n              se_number,\n              adjustment_ref_num,\n              chargeback_type,\n              reason_code,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,   -- case #\n              null,           -- status\n               :3  ,   -- card #\n              to_date( :4  ,'mm/dd/yyyy'), -- tran date\n               :5  ,   -- tran amount\n              nvl( :6  ,trunc(sysdate)),  -- cb date\n               :7  ,   -- cb amount\n               :8  ,      -- se number\n              decode( :9  ,'N/A',null, :10  ),   -- adj #\n               :11  ,   -- type\n               :12  ,    -- reason code\n               :13  ,\n               :14  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.startup.AmexCBLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,newLoadSec);
   __sJT_st.setString(2,__sJT_3771);
   __sJT_st.setString(3,__sJT_3772);
   __sJT_st.setString(4,__sJT_3773);
   __sJT_st.setString(5,__sJT_3774);
   __sJT_st.setDate(6,incomingDate);
   __sJT_st.setString(7,__sJT_3775);
   __sJT_st.setString(8,seNumber);
   __sJT_st.setString(9,__sJT_3776);
   __sJT_st.setString(10,__sJT_3777);
   __sJT_st.setString(11,__sJT_3778);
   __sJT_st.setString(12,reasonCode);
   __sJT_st.setString(13,loadFilename);
   __sJT_st.setLong(14,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1834^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("loadCsvData("+inputFilename+")",e.toString());
    }
    finally
    {
      try{ in.close(); } catch( Exception e ) {}
    }
  }

  public void loadSPMResponseFile( String loadFilename , String spmfileFrequency){	  
    BufferedReader in    = null;
    BufferedWriter out   = null;
    String csvfile       = null;   
    String fields[]      = null;
    String line          = "";
    
    FlatFileRecord ffd  = null;
    boolean notify      = false;
    log.debug("loading SPM response file");
    try{
    	if(spmfileFrequency.equals("DAILYSPM"))
    	{
    		ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_SPONSOR_MERCHANT_DETAIL);
    	}
    	else
    	{
    		ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_SPONSOR_MERCHANT_DETAIL);
    	}
        loadFilenameToLoadFileId(loadFilename);
        in =  new BufferedReader( new FileReader(loadFilename) );
        csvfile = loadFilename.replace(".dat", ".csv");
        out = new BufferedWriter(new FileWriter(csvfile, true)); 
        line = csvSPMHeader(spmfileFrequency);
        log.debug(line);
        fields = line.split(",");
        out.write(line);
        out.newLine();
        while( (line = in.readLine()) != null ){
            if(line.startsWith("DTL")){
                if(line.substring(1463).contains("N")){
                    notify = true;
                    ffd.suck(line);
                    for(int i=0; i<fields.length; i++) {
                        out.write((ffd.getFieldData(fields[i])).replaceAll(",", " ") + ",");
                    }
                    out.newLine();
                  }
               }
           }
        out.flush();
        log.debug("completed SPM response file processing");
       if(notify)
            notifyACRDept(csvfile); 
        
    }catch(Exception e){
        logEntry("loadSPMResponseFile("+loadFilename+")",e.toString());
    }
  }
  
  
  public String csvSPMHeader(String spmFile){
    ResultSetIterator it   = null;
    ResultSet resultSet    = null;
    StringBuffer line = new StringBuffer();
    log.debug("generating SPM header of csv file");
    // DEF_TYPE_AMEX_SPONSOR_MERCHANT_DETAIL          = 1014;
    // DEF_TYPE_AMEX_OPTBLUE_SPONSOR_MERCHANT_DETAIL  = 1017;
    // changing the below 1014 to 1017
    try{
    	if(spmFile.equals("DAILYSPM"))
    	{
        /*@lineinfo:generated-code*//*@lineinfo:1910^9*/

//  ************************************************************
//  #sql [Ctx] it = { select name as name
//                  from flat_file_Def 
//                  where def_Type = 1017
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select name as name\n                from flat_file_Def \n                where def_Type = 1017";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"25com.mes.startup.AmexCBLoader",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"25com.mes.startup.AmexCBLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1915^13*/
    	}
    	else
    	{
    		/*@lineinfo:generated-code*//*@lineinfo:1919^7*/

//  ************************************************************
//  #sql [Ctx] it = { select name as name
//                      from flat_file_Def 
//                      where def_Type = 1014
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select name as name\n                    from flat_file_Def \n                    where def_Type = 1014";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.startup.AmexCBLoader",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"26com.mes.startup.AmexCBLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1924^17*/
    		
    	}
        resultSet = it.getResultSet();
        line.setLength(0);
        while(resultSet.next()){
            if(resultSet.getString("name").equals("filler"))
                continue;
            else
                line.append(resultSet.getString("name") + ",");
        }
        line.deleteCharAt(line.lastIndexOf(","));
    } 
    catch(Exception e){
        logEntry("csvSPMHeader",e.toString());
    }
    finally{
        try{ it.close(); } catch( Exception e ) {}
    }
    log.debug("generating header for SPM file is completed");
    return line.toString();
  }
  
  
  protected void notifyACRDept( String loadFilename ) {
    log.debug("sending mail to ACR team");
    try{
          MailMessage msg = new MailMessage();
          msg.setAddresses(MesEmails.MSG_ADDRS_AMEX_SPM_RESP);
          msg.setSubject("Amex Sponsored Merchant file rejects");
          msg.setText("Please refer to this week Amex Sponsored Merchant file Rejects");
          msg.addFile(loadFilename);
          msg.send();
    }catch(Exception e){
        logEntry("notifyACRDept("+ loadFilename +")",e.toString());
    }
    log.debug("mail has been sent to ACR Team");
  }
  
  protected void notifyChargebackDept( String loadFilename )
  {
    int                   addrId        = -1;
    ResultSetIterator     it            = null;
    ResultSet             resultSet     = null;
    String                subject       = null;
    String                tableName     = null;

    try
    {
      if ( loadFilename.startsWith("amex_cb") )
      {
        addrId    = MesEmails.MSG_ADDRS_AMEX_CB_NOTIFY;
        subject   = "Incoming Amex Chargebacks";
        tableName = "network_chargeback_amex";
      }
      else if ( loadFilename.startsWith("amex_retr") )
      {
        addrId    = MesEmails.MSG_ADDRS_AMEX_RETR_NOTIFY;
        subject   = "Incoming Amex Retrievals";
        tableName = "network_retrieval_amex";
      }

      if ( tableName != null )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1988^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  se_number                 as se_number,
//                    count(1)                  as total_count,
//                    sum(transaction_amount)   as total_amount
//            from    :tableName
//            where   load_file_id = load_filename_to_load_file_id(:loadFilename)
//            group by se_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  se_number                 as se_number,\n                  count(1)                  as total_count,\n                  sum(transaction_amount)   as total_amount\n          from     ");
   __sjT_sb.append(tableName);
   __sjT_sb.append(" \n          where   load_file_id = load_filename_to_load_file_id( ? )\n          group by se_number");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "27com.mes.startup.AmexCBLoader:" + __sjT_sql;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,__sjT_tag,null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1996^9*/
        resultSet = it.getResultSet();

        StringBuffer buffer = new StringBuffer();
        buffer.append( StringUtilities.leftJustify("SE Number", 16,' ') );
        buffer.append( StringUtilities.rightJustify("Count"    , 9,' ') );
        buffer.append( StringUtilities.rightJustify("Amount"   ,15,' ') );
        buffer.append("\n");
        buffer.append( StringUtilities.leftJustify(""         , 40,'=') );
        buffer.append("\n");

        while( resultSet.next() )
        {
          buffer.append( StringUtilities.leftJustify  (resultSet.getString("se_number")   ,16,' ') );
          buffer.append( StringUtilities.rightJustify (resultSet.getString("total_count") , 9,' ') );
          buffer.append( StringUtilities.rightJustify (MesMath.toCurrency(resultSet.getDouble("total_amount")), 15,' ') );
          buffer.append("\n");
        }
        resultSet.close();
        it.close();

        MailMessage msg = new MailMessage();
        msg.setAddresses(addrId);
        msg.setSubject(subject + " - " + loadFilename);
        msg.setText(buffer.toString());
        msg.send();
      }
    }
    catch( Exception e )
    {
      logEntry("notifyChargebackDept(" + loadFilename + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ){}
    }
  }

  protected void notifyDuplicateCases( String loadFilename, Vector duplicateCases )
  {
    int                   addrId        = -1;
    String                subject       = null;

    try
    {
      if ( loadFilename.startsWith("amex_cb") )
      {
        addrId    = MesEmails.MSG_ADDRS_AMEX_CB_NOTIFY;
        subject   = "Duplicate Amex Chargebacks";
      }
      else if ( loadFilename.startsWith("amex_retr") )
      {
        addrId    = MesEmails.MSG_ADDRS_AMEX_RETR_NOTIFY;
        subject   = "Duplicate Amex Retrievals";
      }

      StringBuffer buffer = new StringBuffer();
      buffer.append( "The following inquiries were not loaded because the case number already exists in the MES system:\n\n" );
      buffer.append( StringUtilities.leftJustify("SE Number"  , 16,' ') );
      buffer.append( StringUtilities.leftJustify("Case Number", 15,' ') );
      buffer.append( StringUtilities.leftJustify("Reference #", 14,' ') );
      buffer.append( StringUtilities.leftJustify("Card Number", 18,' ') );
      buffer.append( StringUtilities.rightJustify("Amount"    , 15,' ') );
      buffer.append("\n");
      buffer.append( StringUtilities.leftJustify("",78,'=') );
      buffer.append("\n");

      for( int i = 0; i < duplicateCases.size(); ++i )
      {
        DuplicateCase dup = (DuplicateCase)duplicateCases.elementAt(i);
        buffer.append( StringUtilities.leftJustify  (dup.SeNumber   ,16,' ') );
        buffer.append( StringUtilities.leftJustify  (dup.CaseNumber ,15,' ') );
        buffer.append( StringUtilities.leftJustify  (dup.RefNum     ,14,' ') );
        buffer.append( StringUtilities.leftJustify  (dup.CardNumber ,18,' ') );
        buffer.append( StringUtilities.rightJustify (MesMath.toCurrency(dup.TranAmount), 15,' ') );
        buffer.append("\n");
      }

      MailMessage msg = new MailMessage();
      msg.setAddresses(addrId);
      msg.setSubject(subject + " - " + loadFilename);
      msg.setText(buffer.toString());
      msg.send();
    }
    catch( Exception e )
    {
      logEntry("notifyDuplicateCases(" + loadFilename + ")",e.toString());
    }
    finally
    {
    }
  }
  
  protected void notifyRejectsDept( String fileType, String loadFilename )
  {
    BufferedReader        in            = null;
    String                line          = null;

    try
    {
      in = new BufferedReader( new FileReader(loadFilename) );
      StringBuffer buffer = new StringBuffer();
      while( (line = in.readLine()) != null )
      {
        buffer.append( line );
        buffer.append("\n");
      }
      in.close();

      MailMessage msg = new MailMessage();
      msg.setAddresses(MesEmails.MSG_ADDRS_AMEX_FILE_VAL_REPORT_NOTIFY);
      if("FVAL".equals(fileType)){
          msg.setSubject("Amex File Validation Report - " + loadFilename);
      }
      else if( AMEX_OPTB_REJECT_FILE.equals(fileType)){
          msg.setSubject("Amex Optblue File Validation Report - " + loadFilename);        
      }
      msg.setText(buffer.toString());
      msg.send();
    }
    catch( Exception e )
    {
      logEntry("notifyRejectsDept(" + loadFilename + ")",e.toString());
    }
    finally
    {
      try{ in.close(); } catch( Exception ee ){}
    }
  }

  public String processUploadFile( String fileContent )
  {
    int                 offset        = 0;
    BufferedWriter      out           = null;
    String              workFilename  = null;

    try
    {
      // build the work filename
      if ( fileContent.indexOf("Inquiry Search Results") >= 0 )
      {
        FileType      = FT_RETRIEVALS_SABRE;
        workFilename  = buildFilename("amex_retr3941");
      }
      else if ( fileContent.indexOf("Industry Location Number") >= 0 )
      {
        FileType      = FT_RETRIEVALS_MES;
        workFilename  = buildFilename("amex_retr3941");
      }
      else    // assume chargeback
      {
        FileType      = FT_CHARGEBACKS_SABRE;
        workFilename  = buildFilename("amex_cb3941");
      }
      out   = new BufferedWriter( new FileWriter(workFilename, true) );
      out.write(fileContent);
      out.close();
    }
    catch(Exception e)
    {
      logEntry("processUploadFile()", e.toString());
    }
    return( workFilename.toString() );
  }
  
  public void reloadChargebacksFile( String loadFilename )
  {
    loadChargebackFile  ( loadFilename );
    loadChargebackSystem( loadFilename );
    notifyChargebackDept( loadFilename );
  }
  
  public void reloadRetrievalsFile( String loadFilename )
  {
    loadRetrievalFile   ( loadFilename );
    loadRetrievalSystem ( loadFilename );
    notifyChargebackDept( loadFilename );
  }

  public boolean submitData( HttpServletRequest request )
  {
    ServletFileUpload  fileUpload      = null;
    boolean            retVal          = false;
    String             workFilename    = null;

    try
    {
      fileUpload = new ServletFileUpload(new DiskFileItemFactory(1048576, new File("./")));
      ArrayList items = (ArrayList)(fileUpload.parseRequest(request));

      for( int i = 0; i < items.size(); ++i )
      {
        DiskFileItem fi = (DiskFileItem)items.get(i);

        if( fi.getFieldName().equals("amexCBFile") )
        {
          // process the data file to generate a work file
          workFilename = processUploadFile( fi.getString() );

          // load the work file into the database
          loadCsvData( workFilename );

          // integrate the raw data into the chargeback system
          loadMesSystem( workFilename );

          // place the file on the archive server
          archiveDailyFile( workFilename );
        }
      }
      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("submitData()", e.toString());
    }
    return( retVal );
  }

  public static void main(String[] args)
  {
    AmexCBLoader         t     = null;

    try
    {
        if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{MesDefaults.DK_AMEX_HOST, MesDefaults.DK_AMEX_USER,
                    MesDefaults.DK_AMEX_PASSWORD, MesDefaults.DK_AMEX_INCOMING_PATH,
                    MesDefaults.DK_ACH_CUTOFF, MesDefaults.DK_AMEX_CLEARING_CUTOFF, MesDefaults.DK_DISCOVER_CLEARING_CUTOFF,
                    MesDefaults.DK_MC_GCMS_CUTOFF, MesDefaults.DK_MODBII_CLEARING_CUTOFF, MesDefaults.DK_VISA_BASE_II_CUTOFF});
        }

      SQLJConnectionBase.initStandalone("DEBUG");


      t = new AmexCBLoader();
      t.connect();

      if ( args[0].equals("loadCsvData") )
      {
        BufferedReader    in = new BufferedReader( new FileReader(args[1]) );
        StringBuffer fileContent = new StringBuffer();
        String line;

        while( (line = in.readLine()) != null )
        {
          fileContent.append(line);
          fileContent.append("\n");
        }
        in.close();

        String workFilename = t.processUploadFile(fileContent.toString());
        t.loadCsvData(workFilename);
        t.loadMesSystem(workFilename);
      }
      else if ( "archiveDailyFile".equals(args[0]) )
      {
        t.archiveDailyFile(args[1]);
      }
      else if ( "loadChargebackFile".equals(args[0]) )
      {
        t.loadChargebackFile(args[1]);
      }
      else if ( "loadChargebackSystem".equals(args[0]) )
      {
        t.loadChargebackSystem(args[1]);
      }
      else if ( "loadReconFile".equals(args[0]) )
      {
        t.loadReconFile(args[1]);
      }
      else if ( "loadRejectFile".equals(args[0]) )
      {
        t.loadRejectFile(args[1]);
      }
      else if ( "loadRetrievalFile".equals(args[0]) )
      {
        t.loadRetrievalFile(args[1]);
      }
      else if ( "loadRetrievalSystem".equals(args[0]) )
      {
        t.loadRetrievalSystem(args[1]);
      }
      else if ( "notifyChargebackDept".equals(args[0]) )
      {
        t.notifyChargebackDept(args[1]);
      }
      else if ( "reloadChargebacksFile".equals(args[0]) )
      {
        t.reloadChargebacksFile(args[1]);
      }
      else if ( "reloadRetrievalsFile".equals(args[0]) )
      {
        t.reloadRetrievalsFile(args[1]);
      }
      /*else if ( "loadspmfile".equals(args[0]) )
      {
    	  //String filePrefix      = (String)FilePrefixes.get(fileType);
    	  //String ladFilename    = t.generateFilename("amex_obsresp" + "3942");
    	  //t.loadSPMResponseFile(ladFilename);
            t.loadSPMResponseFile(args[1]);
      }*/
      else if ( "execute".equals(args[0]) )
      {
        t.setEventArgs(args[1]);
        t.execute();
      }
    }
    catch(Exception e)
    {
    }
    finally
    {
      try{ t.cleanUp(); } catch(Exception e){}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/