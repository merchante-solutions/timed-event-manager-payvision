package com.mes.api;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mes.support.PropertiesFile;
import dto.LevelDto;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

public class IOMicroService {

	static Logger log = Logger.getLogger(IOMicroService.class);

	protected static final String JSON = "application/json";

	protected static String ioHostUrl;
	protected static String ioAuthToken = "Bearer ";
	protected static boolean isIOEnable;
	protected static int ioReadTimeOut;
	protected static int ioConnTimeOut;
	protected static String ioFindAllActiveMerchant;
	protected static String ioLatestDetail;

	protected static JSONArray ioActiveMerchantList = new JSONArray();

	static {
		log.info("Loading IO Configurations From Static Block.....");
		try {
			loadIOServiceCommDetails();
			setUniRestTimeOutConfig();

			if (isIOEnable)
				ioActiveMerchantList = loadIOActiveMerchantList();
		}
		catch (Exception e) {
			log.error("IOMicroService Static Block :",e);
		}
	}

	private LevelDto responseParser(HttpResponse<String> serviceResponse) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		return isSuccess(serviceResponse.getStatus()) ? objectMapper.readValue(serviceResponse.getBody(), LevelDto.class) : null;
	}

	public static boolean isSuccess(int responseCode) {
		return responseCode == HttpStatus.SC_OK;
	}

	public static LevelDto getIODetails(String merchantID) {

		try {
			return new IOMicroService().responseParser(sendToIoService(ioLatestDetail + merchantID));
		}
		catch (Exception e) {
			log.error("getIODetails(" + merchantID + ") : ", e);
		}

		return null;
	}

	private static JSONArray loadIOActiveMerchantList() {

		log.info("Connecting IO Service To Get Active Merchant List........");

		try {
			HttpResponse<String> response = sendToIoService(ioFindAllActiveMerchant);
			return new JSONArray(isSuccess(response.getStatus()) ? response.getBody() : "[]");
		}
		catch (Exception e) {
			log.error("loadIOActiveMerchantList() : ", e);
		}

		return new JSONArray();
	}

	private static HttpResponse<String> sendToIoService(String apiPath) throws Exception {
		long startTime = System.currentTimeMillis();
		
		HttpResponse<String> serviceResponse = Unirest.get(ioHostUrl + apiPath).header("Authorization", ioAuthToken).asString();

		log.info("IO serviceResponse.getStatus() ["+apiPath +"]  : " + serviceResponse.getStatus());
		log.info("IO serviceResponse.getBody() ["+apiPath +"]  " + serviceResponse.getBody());
		
		log.debug("IO Service Time Taken For API ["+apiPath +"] : " + (System.currentTimeMillis() - startTime));

		return serviceResponse;
	}
	
	

	public static boolean isIOActiveMerchant(String merchantID) {

		return ioActiveMerchantList.toString().contains(merchantID);
	}

	public static boolean isIOEnable() {
		return isIOEnable;
	}

	public static boolean isIOActiveMerchantListEmpty() {

		return ioActiveMerchantList.length() == 0;
	}

	public static void loadIOServiceCommDetails() {

		log.info(" Loading IO Service Comm Details.... ");

		try {
			PropertiesFile pf = new PropertiesFile();
			pf.load("IOService.properties");

			ioHostUrl = pf.getString("io.service.hosturl");
			ioLatestDetail = pf.getString("io.service.latest");
			ioFindAllActiveMerchant = pf.getString("io.service.findallmerchant");

			ioReadTimeOut = pf.getInt("io.service.readtimeout");
			ioConnTimeOut = pf.getInt("io.service.conntimeout");
			ioAuthToken = String.format("%s%s", ioAuthToken, pf.getString("io.service.authtoken"));
			isIOEnable = pf.getBoolean("io.service.enable");
		}
		catch (Exception e) {
			log.error("Failed To Load IOService Properties File :", e);
		}
		finally {
			log.info("isIOEnable : " + isIOEnable);
			log.info("ioHostUrl : " + ioHostUrl);
			log.info("ioFindAllActiveMerchant : " + ioFindAllActiveMerchant);
			log.info("ioLatestDetail : " + ioLatestDetail);
			log.info("readTimeOut : " + ioReadTimeOut);
			log.info("connTimeOut : " + ioConnTimeOut);
		}
	}

	public static void setUniRestTimeOutConfig() {
		Unirest.config().connectTimeout(ioConnTimeOut);
		Unirest.config().socketTimeout(ioReadTimeOut);

	}
}