package com.mes.extraction.utils;

public enum ExtractionConstants {
	PT_DISCOUNT_IC_CALC(1);
	
	private int value;	
	
	public int getValue() {
		return value;
	}
	
	private ExtractionConstants(int value){
		this.value = value;
	}

}
