/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/InterchangeReport.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $LastChangedDate: 2014-03-04 16:04:02 -0800 (Tue, 04 Mar 2014) $
  Version            : $Revision: 22226 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.util.List;
import com.mes.support.SyncLog;

public class InterchangeReport  extends InterchangeBase
{
  public static final String      MY_CARD_TYPE          = "RP";
  public static final String      MY_IC_CLASS_PREFIX    = "com.mes.settlement.InterchangeReport$";

  public static final int     LEN_ISSUER_IC_LVL = 4;
  public static final String  MeS_FAILED_TO_QUALIFY   = "UNKN";


  // ********************************************************************************
  // ********************************************************************************
  public static          class IC_Report                                                            extends IC_BaseClass
  {
    public                      IC_Report()                       { }
  }

  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************

  // constructor
  public InterchangeReport()
  {
    super( MY_CARD_TYPE, MY_IC_CLASS_PREFIX, LEN_ISSUER_IC_LVL );
  }

  public boolean getBestClass( SettlementRecord tranInfo, List listOfRuleSets )
  {
    boolean       retVal          = false;
    try
    {
      String  irfInfoList = null;
      String  cardType    = tranInfo.getData("card_type") + "  ";

      switch( (cardType.charAt(0) * 0x100) + cardType.charAt(1) )
      {
        case (('P' * 0x100) + 'L')  : // PL = Private Label
          if( "256s".equals( tranInfo.getData("load_filename").substring(0,4) ) )
            irfInfoList = "UATP";
          else
            irfInfoList = MeS_FAILED_TO_QUALIFY;
          break;

        case (('E' * 0x100) + 'B')  : // EB = EBT
          irfInfoList = "EBT-";
          break;

        case (('D' * 0x100) + 'B')  : // DB = Debit
          String regionIssuer = tranInfo.getData("region_issuer");
          String networkChar  = "-";

          switch( (regionIssuer.charAt(0) * 0x100) + regionIssuer.charAt(1) )
          {
            case (('B' * 0x100) + 'R')  : // BR = Brazil
//            if( prepaid )
//            {
//                    if(                                                 ) networkChar = "1";  // consumer
//              else  if(                                                 ) networkChar = "2";  // commercial
//              else  if(                                                 ) networkChar = "F";  // food
//              else  if(                                                 ) networkChar = "M";  // meal
//              else  if(                                                 ) networkChar = "V";  // vale-pedagio
//            }
//            else
              {
                IC_BaseClass  interchangeClass  = getInterchangeClass( "IC_Report" );

                      String        tran_sic_code   = tranInfo.getString("sic_code");
                      String        sicGroup        = "~OTH";     // default to "other"
                final String[][]    sicGroups       =
                {
                //  0     , 1
                //  group , sic list
                  { "~AIR", "3000-3299,4511"                                                                                            },  // Airlines
                  { "~CPN", "6532,6533"                                                                                                 },  // Coupon Booklets (CARNE)
                  { "~DLY", "5422,5462,5912"                                                                                            },  // Daily Usage
                  { "~DPT", "5311,5399,5611,5621,5631,5641,5651,5655,5661,5691,5722,5732,5733,5734"                                     },  // Department Stores
                  { "~FST", "4011,4111,4112,4121,4131,4215,4784,5499,5814,5994,7523,7995,7996,7998"                                     },  // Fast Transactions
                  { "~GAS", "5541,5542"                                                                                                 },  // Petroleum
                  { "~GOV", "9211,9222,9223,9311,9399,9402,9405"                                                                        },  // Government Services
                  { "~MPH", "4814"                                                                                                      },  // Mobile Phone Top-Up
                  { "~MTH", "4813,4816,4821,4899,5192,5960,5968,6513,7997,8211,8220,8241,8244,8249,8299,8351,8398,8641,8661,8675,8699"  },  // Monthly
                  { "~PRO", "0742,1520,1711,1731,1740,1750,1761,1771,1799,7210,7211,7216,7217,7230,7251,7261,7273,7277,7297,7298,7299,7343,7349,7361,7393,8011,8021,8031,8041,8042,8049,8050,8062,8071,8099,8111,8911" },  // Professional
                  { "~SMK", "5411"                                                                                                      },  // Supermarket
                  { "~T&E", "3351-3441,3501-3999,4722,5812,5813,7011,7012,7032,7033,7512,7519"                                          },  // T & E
                  { "~UTL", "4900"                                                                                                      },  // Utility
                  { "~WHL", "5300"                                                                                                      },  // Wholesalers
                };
                for( int i = 0; i < sicGroups.length; ++i )
                {
                  if( interchangeClass.testDataIsInList( 4, tran_sic_code, sicGroups[i][1] ) )
                  {
                    sicGroup = sicGroups[i][0];
                    break;
                  }
                }
                tranInfo.setData( "sic_group" , sicGroup );
              }
              break;

            case (('L' * 0x100) + 'C')  : // LC = LAC Intra-Regional
              break;

//          case (('U' * 0x100) + 'S')  : // US = USA
//          case (('I' * 0x100) + 'R')  : // IR = Inter-Regional
            default:
              String networkID = tranInfo.getData("debit_network_id") + "  ";
              switch( (networkID.charAt(0) * 0x100) + networkID.charAt(1) )
              {
                case (('M' * 0x100) + 'E')  : // ME = Maestro
                  if( "IR".equals(regionIssuer) )
                  {
                    String posDataCode = tranInfo.getData("pos_data_code").trim();
                          if( posDataCode.length()                        != 12 ) networkChar = "F";  // face-to-face; don't know, guess middle cost
                    else  if( posDataCode.charAt(6)                       == 'S') networkChar = "E";   // e-commerce
                    else  if( "5CDEM".indexOf(posDataCode.substring(0,1)) >= 0  ) networkChar = "C";   // emv chip
                    else                                                          networkChar = "F";   // face-to-face
                  }
                  break;

// IL -- what about supermarket cap? is "RA = Y" still a requirement? ('Y'/'y' in issuer_ic_level prevents select of the cap rows)
                case (('I' * 0x100) + 'L')  : // IL = InterLink
                case (('V' * 0x100) + 'I')  : // VI = Visa 0002
                  if( InterchangeVisa.CardTypeVisa.isVisaCommercialProduct(tranInfo.getData("product_id")) )
                  {
                    if( "P".equals(tranInfo.getData("funding_source")) )        networkChar = "x";    // visa prepaid business
                    else                                                        networkChar = "b";    // visa business
                  }
                  else if( "P".equals(tranInfo.getData("funding_source")) )     networkChar = "p";    // visa prepaid
                  break;

                case (('X' * 0x100) + 'L')  : // XL = Accel
                  networkChar = tranInfo.getData("cardholder_id_method");
                  break;
              }
              break;
          }

                        // 2 char issuer region + 1 char Debit-or-Credit                      + 1 char depending on network
          irfInfoList   = regionIssuer          + tranInfo.getData("debit_credit_indicator")  + networkChar;
          break;
      }

      if( irfInfoList != null )
      {
        int debitType = tranInfo.getInt("debit_type");
        getBestIcInfoClause = " and nvl(icd.debit_type," + debitType + ") = " + debitType;
        IcInfo  icInfo      = getBestIrfInfoFromInfoList( tranInfo, SettlementRecord.DEF_ENHANCED_IND + irfInfoList, cardType );
        if( "---".equals(icInfo.getIcCode()) )    // if no row found, get unknown
        {
          getBestIcInfoClause = "";
                  icInfo      = getBestIrfInfoFromInfoList( tranInfo, SettlementRecord.DEF_ENHANCED_IND + MeS_FAILED_TO_QUALIFY, cardType );
        }
        if( icInfo != null )
        {
          tranInfo.setData( "ic_cat"              , icInfo.getIcCode()        );
          tranInfo.setData( "ic_cat_billing"      , icInfo.getIcCodeBilling() );
          retVal = true;
        }
      }

/*
System.out.println("clause        :" + getBestIcInfoClause);
System.out.println("card type     :" + tranInfo.getData("card_type"));
System.out.println("card type enh :" + tranInfo.getData("card_type_enhanced"));
System.out.println("card type enh :" + tranInfo.getCardTypeEnhanced());
System.out.println("funding source:" + tranInfo.getData("funding_source"));
System.out.println("debit network :" + tranInfo.getData("debit_network_id"));
System.out.println("debit type    :" + tranInfo.getData("debit_type"));
System.out.println("product_id    :" + tranInfo.getData("product_id"));
System.out.println("e flags       :" + tranInfo.getData("eligibility_flags"));
System.out.println("irf info list :" + irfInfoList);
System.out.println("ic-cat        :" + tranInfo.getData("ic_cat"));
//System.out.println("ic-cat-billing:" + tranInfo.getData("ic_cat_billing"));
//System.out.println("auth rec id   :" + tranInfo.getData("auth_rec_id"));
System.out.println("auth fpi      :" + tranInfo.getData("auth_fpi"));
//System.out.println("acq ref num   :" + tranInfo.getData("acq_reference_number"));
//System.out.println("ic interventio:" + tranInfo.getData("ic_intervention"));
//System.out.println("bank  number  :" + tranInfo.getData("bank_number"));
*/

//    tranInfo.fixBadDataAfterIcAssignment();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.InterchangeReport.getBestClass()", e.toString());
      System.out.println(e.toString());
    }
    return( retVal );
  }
}
