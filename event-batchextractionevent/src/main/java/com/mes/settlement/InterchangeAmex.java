/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/InterchangeAmex.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $LastChangedDate: 2011-07-30 08:28:27 -0700 (Sat, 30 Jul 2011) $
  Version            : $Revision: 19080 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.util.List;
import com.mes.support.PropertiesFile;
import com.mes.support.SyncLog;

public class InterchangeAmex  extends InterchangeBase
{
  public static final String      MY_CARD_TYPE          = "AM";
  public static final String      MY_IC_CLASS_PREFIX    = "com.mes.settlement.InterchangeAmex$";

  public static final int     LEN_ISSUER_IC_LVL = 14;   // "RTN"/"SWP"/"KEY" then "-" then 10 digit SE #
  public PropertiesFile props = new PropertiesFile("optblue.properties");

  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************

  // constructor
  public InterchangeAmex()
  {
    super( MY_CARD_TYPE, MY_IC_CLASS_PREFIX, LEN_ISSUER_IC_LVL );
  }

  /*
   * renamed this method from getBestClass to getBestClassOld and wrote a new getBestClass method with optblue logic
   * Keeping this for method for future reference
   */
  
  public boolean getBestClassOld( SettlementRecord tranInfo, List listOfRuleSets )
  {
    
	boolean       retVal          = false;
    try
    {
      String  irfInfoList;
            if(                          "C".equals(  tranInfo.getData("debit_credit_indicator") )        ) irfInfoList = "RTN";
      else  if( "02,03,05,06,07,84,90,91,95".indexOf( tranInfo.getPaddedString("pos_entry_mode",2) ) >= 0 ) irfInfoList = "SWP";
      else                                                                                                  irfInfoList = "KEY";

      IcInfo  icInfo      = getBestIrfInfoFromInfoList( tranInfo, SettlementRecord.DEF_ENHANCED_IND + irfInfoList 
                                                                        + "-" + tranInfo.getData("amex_se_number") );
      if( icInfo != null )
      {
        tranInfo.setData( "ic_cat"              , icInfo.getIcCode()        );
        tranInfo.setData( "ic_cat_billing"      , icInfo.getIcCodeBilling() );
        retVal = true;
      }

      tranInfo.fixBadDataAfterIcAssignment();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.InterchangeAmex.getBestClass()", e.toString());
      System.out.println(e.toString());
    }
    return( retVal );
  }
  
  public boolean getBestClass( SettlementRecord tranInfo, List listOfRuleSets )
  {
    
	boolean       retVal          = false;
    try
    {
      String  irfInfoList;

      if(isOptblueMerchant(tranInfo.getData("amex_se_number"))){

    	  if("C".equals(  tranInfo.getData("debit_credit_indicator") ))
    		  irfInfoList = "RTN";
    	  else if("PR".equals(tranInfo.getData("dba_state")))
    		  irfInfoList = "OBP";
    	  else 
    		  irfInfoList = "OBU";

    	  String cardNumber=tranInfo.getCardNumberFull();
    	  String productCode = SettlementDb.getOptBlueProductCode(cardNumber.substring(0,6));
    	  if (AmexOptBlueBinRange.isPrePaidCard(productCode)){
    		  if("PR".equals(tranInfo.getData("dba_state")))
    			  irfInfoList = "PPP";
    		  else
    			  irfInfoList = "PPU";
    	  } else if(AmexOptBlueBinRange.isVpayment(productCode)) {
    		  irfInfoList = "OBV";
    	  }
    	  
    	  //"MES*" prefix is not required for optblue trasactions
    	  if(tranInfo.getData("dba_name").startsWith("MES*")  )
    		  tranInfo.setData( "dba_name", tranInfo.getData("dba_name").replace("MES*", ""));
      } else {
    	  if( "C".equals(  tranInfo.getData("debit_credit_indicator") ) ) 
    		  irfInfoList = "RTN";
          else  if( "02,03,05,06,07,84,90,91,95".indexOf( tranInfo.getPaddedString("pos_entry_mode",2) ) >= 0 ) 
        	  irfInfoList = "SWP";
          else
        	  irfInfoList = "KEY";
      }

      IcInfo  icInfo      = getBestIrfInfoFromInfoList( tranInfo, SettlementRecord.DEF_ENHANCED_IND + irfInfoList 
                                                                        + "-" + tranInfo.getData("amex_se_number"));
      if( icInfo != null )
      {
        tranInfo.setData( "ic_cat"              , icInfo.getIcCode()        );
        tranInfo.setData( "ic_cat_billing"      , icInfo.getIcCodeBilling() );
        retVal = true;
      }

      tranInfo.fixBadDataAfterIcAssignment();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.InterchangeAmex.getBestClass()", e.toString());
      System.out.println(e.toString());
    }
    return( retVal );
  }
  

  
  // Check if transaction is ESA or optblue.
  boolean isOptblueMerchant(String seNum){
	  boolean result = false;
	  String seList = props.getString("se_list_3943") + "," + props.getString("se_list_9999");
	  if(seList.indexOf(seNum) > 0)
		  result = true;
	  return result;
  }
  
  
}
