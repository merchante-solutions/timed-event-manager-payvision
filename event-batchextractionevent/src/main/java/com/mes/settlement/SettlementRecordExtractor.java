/*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/settlement/SettlementRecordExtractor.java $

  Description:  


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.api.IOMicroService;
import com.mes.constants.MesEmails;
import com.mes.constants.mesConstants;
import com.mes.data.merchant.ProcessForcedPostsService;
import com.mes.database.OracleConnectionPool;
import com.mes.net.MailMessage;
import com.mes.support.DateTimeFormatter;
import com.mes.support.SyncLog;
import com.mes.support.TridentTools;
import masthead.formats.visak.DetailRecord;

public class SettlementRecordExtractor
{
    private Logger log = Logger.getLogger(SettlementRecordExtractor.class);
    protected class BatchExtractThread 
    extends Thread 
  {
    protected     HashMap                     AchFlags            = null;
    protected     long                        BatchId             = -1L;
    protected     long                        BatchRecordId       = -1L;
    protected     int                         BatchType           = mesConstants.MBS_BT_UNKNOWN;
    protected     SettlementDb                Db                  = null;
    protected     String                      LastAci             = null;
    protected     String                      LastSicCode         = null;
    protected     Object                      LockedBy            = null;
    
    public BatchExtractThread( SettlementDb db )
    {
      Db = ((db == null) ? new SettlementDb() : db);
    }
    
    protected void alertAdmin( String source, String message, Exception ee )
    {
      try
      {
        String    stackTrace    = null;
        try
        {
          Writer result = new StringWriter();
          PrintWriter printWriter = new PrintWriter(result);
          ee.printStackTrace(printWriter);
          stackTrace = result.toString();
        }
        catch( Exception stackTraceException )
        {
          stackTrace = stackTraceException.toString();
        }          
      
        source = source + "\n" + "Stack Trace: " + "\n" + stackTrace;
        logEntry(source,message);
      }
      catch(Exception e)
      {
      }
    }
    
    protected void disqualMapDump( InterchangeBase baseIc, SettlementRecord rec )
    {
      if ( Verbose )
      {
        // dump the disqual reasons to the console
        HashMap dmap = baseIc.getDisqualMap();
        for ( Iterator it = dmap.keySet().iterator(); it.hasNext();  )
        {
          String className = (String)it.next();
          System.out.println(className + " failed to qualify");
        
          switch( rec.RecSettleRecType )
          {
            case SettlementRecord.SETTLE_REC_VISA   :
              System.out.println( "  ACI: "           + rec.getString("auth_returned_aci")    +
                                  "  MOTO/Ecomm: "    + rec.getString("moto_ecommerce_ind")   +
                                  "  Trident ID: "    + rec.getString("trident_tran_id")      );
              break;

            case SettlementRecord.SETTLE_REC_MC     :
              System.out.println( "  CPI: "           + rec.getString("card_program_id")      +
                                  "  GPID: "          + rec.getString("gcms_product_id")      +
                                  "  BSA: "           + rec.getString("bsa_type") + rec.getString("bsa_id_code") +
                                  "  MOTO/Ecomm: "    + rec.getString("moto_ecommerce_ind")   +
                                  "  Trident ID: "    + rec.getString("trident_tran_id")      );
              break;

//@         case SettlementRecord.SETTLE_REC_AMEX   : -- doesn't set any disqual reasons

            case SettlementRecord.SETTLE_REC_DISC   :
              System.out.println( "  Entry Mode: "    + rec.getString("pos_entry_mode")       +
                                  "  Proc Code:  "    + rec.getString("process_code")         +
                                  "  Trident ID: "    + rec.getString("trident_tran_id")      );
              break;

//@         case SettlementRecord.SETTLE_REC_MODBII : -- doesn't set any disqual reasons
          }
                  
          Vector disqualReasons = (Vector)dmap.get(className); 
          for( int i = 0; i < disqualReasons.size(); ++i )
          {
            System.out.println("  " + (String)disqualReasons.elementAt(i));
          }
          System.out.println();
        }
      }
    }
    
    protected void dumpClassList( List classList, int recType )
    {
      if ( classList != null )
      {
        for( Iterator it = classList.iterator(); it.hasNext(); )
        {
          switch( recType )
          {
            case SettlementRecord.SETTLE_REC_VISA   : System.out.println("Visa Class: " + ((String)it.next()) );  break;
            case SettlementRecord.SETTLE_REC_MC     : ((IcProgramRulesMC)it.next()).showData();                   break;
//          case SettlementRecord.SETTLE_REC_AMEX   :                                                             break;
            case SettlementRecord.SETTLE_REC_DISC   : ((IcProgramRulesDiscover)it.next()).showData();             break;
//          case SettlementRecord.SETTLE_REC_MODBII :                                                             break;
            default:                                                                                              break;
          }
        }
      }
    }
    
    public void dumpRecord( SettlementRecord rec )
    {
      rec.isValid();    // force validation
      
      Vector errors = rec.getErrors();
      System.out.println("Error Count: " + errors.size());
      for( int i = 0; i < errors.size(); ++i )
      {
        System.out.println(errors.elementAt(i));
      }                
      
      Vector        allFields = rec.getFields().getFieldsVector();
      com.mes.forms.Field         f         = null;

      for( int i = 0; i < allFields.size(); ++i )
      {
        f = (com.mes.forms.Field)allFields.elementAt(i);
        System.out.println(f.getName() + " : " + f.getData());
      }
      
      if ( rec.hasLevelIIIData() )
      {
        Iterator  it = rec.getLineItems().iterator();
    
        while( it.hasNext() )
        {
          LineItem item = (LineItem)it.next();

          // output line item data
          allFields = item.getFields().getFieldsVector();
          for( int i = 0; i < allFields.size(); ++i )
          {
            f = (com.mes.forms.Field)allFields.elementAt(i);
            System.out.println(f.getName() + " : " + f.getData());
          }
        }
      
      }
    }
    
    public boolean extractBatchData( )
    {
      VisakBatchData        batchData           = null;
      String                cardType            = null;
      DetailRecord          detailRec           = null;
      List                  classList           = null;
      List                  detailList          = null;
      long                  recId               = 0L;
      int                   recIdx              = 0;
      int                   recIdxStop          = 0;
      boolean               retVal              = false;
      String                testFlag            = (TestOnly) ? "Y" : "N";
      
      // Settlement Records
      SettlementRecord                    rec   = null;

      InterchangeBase                  baseIc   = null;
      InterchangeVisa                  visaIc   = new InterchangeVisa();        visaIc.setCentralProcessingDate(SettlementDates[0]);
      InterchangeMasterCard              mcIc   = new InterchangeMasterCard();    mcIc.setCentralProcessingDate(SettlementDates[1]);
      InterchangeAmex                  amexIc   = new InterchangeAmex();        amexIc.setCentralProcessingDate(SettlementDates[2]);
      InterchangeDiscover              discIc   = new InterchangeDiscover();    discIc.setCentralProcessingDate(SettlementDates[3]);
      InterchangeModBII              modbiiIc   = new InterchangeModBII();    modbiiIc.setCentralProcessingDate(SettlementDates[0]);
      InterchangeReport              reportIc   = new InterchangeReport();    reportIc.setCentralProcessingDate(SettlementDates[4]);
      
      try
      {
    	  if ( Db == null ) {
    		  Db = new SettlementDb();
    	  }
        Db.connect(true);
        
        if ( Verbose ) 
        {
          System.out.println("loading batch " + BatchId);
        }
        if ( log.isDebugEnabled()) {
        	log.debug(String.format("[extractBatchData()] - loading batch, batchId:%s",BatchId));
        }
      
        // load the batch data for this batch from the database
        batchData   = Db._loadMerchantAndBatchData( BatchId, BatchType );
        if ( BatchType == mesConstants.MBS_BT_VISAK )
        {
          // load the raw visak batch data
          batchData.setVisakBatch(Db._loadRawBatchDataVisak(BatchId) );
          // detailList = list of batch detail records
          detailList  = batchData.getDetailRecords();

          if ( BatchRecordId >= 0 )
          {
            recIdx      = (int)BatchRecordId;
            recIdxStop  = (int)BatchRecordId+1;
          }
          else
          {
            recIdxStop  = detailList.size();
          }
        }
        else
        {
          // detailList = list of batch detail record IDs
          if ( BatchRecordId <= 0L )
          {
		  log.info("[extractBatchData()] - Loading batch record IDs.");
		  detailList  = Db._loadBatchRecIds( BatchType, BatchId );
		  recIdxStop  = detailList.size();
          }
          else
          {
            detailList  = new ArrayList(1);
            detailList.add(BatchRecordId);
            recIdxStop  = 1;
          }
        }


        // Z status merchant account: send email and continue as usual
        if( batchData.getAccountStatus().charAt(0) == 'Z' && "N".equals(testFlag) )
        {
          try
          {
            MailMessage msg = new MailMessage();
            msg.setAddresses( MesEmails.MSG_ADDRS_RESEARCH_BATCH_HOLD_NOTIFY );
            msg.setSubject( "status Z: "    + batchData.getMerchantId() 
                          + " ~ batch ID "  + BatchId );
            msg.setText( "Note: account status Z only sends e-mail, does not hold the batch." );
            msg.send();
          }
          catch( Exception e )
          {
            alertAdmin("statusZ email exception", e.toString(), e);
          }
        }
        // test merchant account: send email and ignore batch
        if( batchData.getTestAccount() && "N".equals(testFlag) )
        {
          try
          {
            MailMessage msg = new MailMessage();
            msg.setAddresses( MesEmails.MSG_ADDRS_CERTIFICATION_NOTIFY );
            msg.setSubject( "test merch: "  + batchData.getMerchantId() 
                          + " ~ batch ID "  + BatchId );
            msg.setText( "Note: " + (recIdxStop-recIdx) + " transaction(s) ignored -- NOT extracted/cleared!" );
            msg.send();
          }
          catch( Exception e )
          {
            alertAdmin("test merchant email exception", e.toString(), e);
          }

          recIdxStop = recIdx;
        }

        if(IOMicroService.isIOEnable())	
        {
        	String merchantId = String.valueOf(batchData.getMerchantId());
        	log.debug("IO Enabled For BatchId :"+batchData.getBatchId() + ",	MID :" +merchantId );
        	
        	batchData.setLevelDto(IOMicroService.isIOActiveMerchant(merchantId) || IOMicroService.isIOActiveMerchantListEmpty() ? IOMicroService.getIODetails(merchantId) : null);
        }

        for( ; recIdx < recIdxStop; ++recIdx )
        {
          try
          {
            switch( BatchType )
            {
              case mesConstants.MBS_BT_VISAK:
                recId   = recIdx; // visa k only uses recId for error logging
                rec     = Db._extractTranVisak( batchData, TridentTools.decodeVisakCardType((DetailRecord)detailList.get(recIdx)), recIdx);
                break;

              case mesConstants.MBS_BT_PG:
              case mesConstants.MBS_BT_VITAL:
              case mesConstants.MBS_BT_CIELO:
                recId   = ((Long)detailList.get(recIdx)).longValue();
                rec     = Db._extractTran( BatchType, batchData, recId );
                break;
            }
          
            if ( rec != null )
            {
              rec.setData("test_flag",testFlag );
              rec.setData("batch_date",SettlementDates[4]); // set the batch date
              rec.setData("reference_number",SettlementTools.generateReferenceNumber(rec));
              rec.setLevelDataFromIO(batchData.getLevelDto());
              rec.fixBadData();
              rec.setForeignExchangeFields();
              
              rec.ioVersion = batchData.getLevelDto() != null ? String.valueOf(batchData.getLevelDto().getVersion()) : null;
      
              String rejectReason = null;
              if( rec.getString("reject_reason") != null )
              {
                rejectReason  = rec.getString("reject_reason");
              }
              else if( rec.getCardTypeEnhanced() == null || !rec.isCardNumberValid() )
              {
                rejectReason  = "0003";   // "INVALID ACCOUNT NUMBER"
              }
              else if( !rec.isCurrencyCodeAcceptable(batchData) )
              {
                rejectReason  = "0167";   // "INVALID CURRENCY CODE"
              }
              else if("C".equals(rec.getData("debit_credit_indicator")) &&
                      "F".equals(rec.getData("foreign_card_indicator")) &&
                      rec.getDouble("transaction_amount") > 700.00 &&
                      BatchType == mesConstants.MBS_BT_VITAL &&
                        ( rec.RecSettleRecType != SettlementRecord.SETTLE_REC_AMEX  || 
                            (rec.RecSettleRecType == SettlementRecord.SETTLE_REC_AMEX  && rec.isMerchantProcessedByMes()))
                      )
              {
            	rejectReason  = "9998";   // "POTENTIAL FRAUD SITUATION 0314"
              }
              //else if( rec.getDouble("transaction_amount") >= 500000.00 )
              //{
              //  rejectReason  = "1000";   // "TRANSACTION AMOUNT EXCEEDS LIMIT"
              //}
              else if( !rec.isMerchantNumberValid() )
              {
                rejectReason  = "0136";   // "INVALID AMEX/DISC/JCB MERCHANT NUMBER"
              }
              else if( rec.getData("account_status").charAt(0) == 'S'       &&
                       !"C".equals(rec.getData("debit_credit_indicator"))   )
              {
                rejectReason  = "0067";   // "USER OPTION"
              }
              else if(BatchType==mesConstants.MBS_BT_VITAL &&
           	       !("C".equals(rec.getData("debit_credit_indicator"))) && rec.isForcePostTransaction()) //if offline transaction then get forcepost flag for TSYS transactions
    		  {  
            	  try{
                    	 ProcessForcedPostsService processForcedPostsService =   new ProcessForcedPostsService();
                    	 if(!("PL".equals(rec.getCardTypeEnhanced())) && !(processForcedPostsService.isProcessForcedPostsEnabled(batchData.getMerchantId())))
                         {
                        	 rejectReason  = "0199"; 
                         }	 
            	  }
            	  catch(Exception e)
                  {
            		  log.error("Exception in invoking ProcessForcedPostsService", e);
            		  rejectReason  = "0199"; 
                  } 
              }
              
              if( rejectReason == null )
              {
                switch( rec.RecSettleRecType )
                {
                  case SettlementRecord.SETTLE_REC_VISA   :
                	  baseIc =   visaIc;
	                  classList = SettlementTools.getClassListVisa(rec);
 	                  boolean customFlag = classList.stream().anyMatch(p -> ((VisaProgram) p).getName().equalsIgnoreCase("IC_Visa_CPS_SmallTicket"));
	                  baseIc.setCpsSmallTicketFlag(customFlag);
	                  break;
                  case SettlementRecord.SETTLE_REC_MC     : baseIc =     mcIc;  classList = SettlementTools.getIrdListMC(rec);      break;
                  case SettlementRecord.SETTLE_REC_AMEX   : baseIc =   amexIc;  classList = new ArrayList();                        break;
                  case SettlementRecord.SETTLE_REC_DISC   : baseIc =   discIc;  classList = Db._loadIcpListDiscover( rec );         break;
                  case SettlementRecord.SETTLE_REC_MODBII : baseIc = modbiiIc;  classList = new ArrayList();                        break;
                  case SettlementRecord.SETTLE_REC_REPORT : baseIc = reportIc;  classList = new ArrayList();                        break;
                  default                                 : baseIc =     null;  classList = null;                                   break;
                }

                if ( baseIc != null && classList != null )
                {
//                  dumpClassList( classList, rec.RecSettleRecType );
                  baseIc.getBestClass(rec,classList);
                  disqualMapDump(baseIc,rec);

/********
//@ do not assign reject reason for these because they will probably be fixed and we will want to ach,
    but there is no code at present to do the ach later.
                  if( "000".equals(rec.getData("ic_cat")) )
                  {
                    rejectReason  = "9999";   // "UNKNOWN PROBLEM, IC CAT NOT ASSIGNED"
                  }
*/
                }
              }

              if ( rejectReason != null )
              {
                rec.setData( "reject_reason"    ,rejectReason               );
                rec.setData( "ach_flag"         ,"N"                        );
                rec.setData( "ic_cat"           ,"000"                      );
                rec.setData( "ic_cat_billing"   ,"000"                      );
                rec.setData( "output_filename"  ,"hold-mbs-" + rejectReason );
                rec.setData( "external_reject"  ,"N"                        );
              }

              if ( StoreRecord )
              {
                //@ dumpRecord(rec)
                Db._insertSettlementRecord(rec);
            
                if ( rec.hasLevelIIIData() )
                {
                  Db._insertLineItemDetail(rec);
                }

                if ( rec.hasLevelIIIData() && null != batchData.getLevelDto())
                {
                	Db._insertMerchantLevelDataUsage(rec);
                }
                
                if ( rejectReason != null && "N".equals(testFlag) )
                {
                  // don't call this until after establish rec.rec_id (via insertSettlementRecord);
                  // card types not inserted in xxxSettlement do not get a rec_id, and should not get reject entry
                  if( rec.getLong("rec_id") != 0L )
                  {
                    int idx;
                    switch( rec.RecSettleRecType )
                    {
                      case SettlementRecord.SETTLE_REC_VISA   : idx = 0;  break;
                      case SettlementRecord.SETTLE_REC_MC     : idx = 1;  break;
                      case SettlementRecord.SETTLE_REC_AMEX   : idx = 2;  break;
                      case SettlementRecord.SETTLE_REC_DISC   : idx = 3;  break;
                      case SettlementRecord.SETTLE_REC_MODBII : idx = 0;  break;
                      default                                 : idx = 4;  break;
                    }
                    rec.setData( "settlement_date"  ,SettlementDates[idx]       );

                    Db._insertRejectRecord(rec);
                  }
                }
              }
              
              if ( StoreReportRecords )
              {
                storeReportData(rec);
              }
              
              if ( BatchType == mesConstants.MBS_BT_CIELO && !rec.isBlank(rec.getData("installment_amount")) )
              {
                storeInstallments(rec);
              }
            }
          }
          catch(Exception e)
          {
		  log.error("Error.", e);
		  alertAdmin("extractTranData(" + BatchId + "," + recId + ")", e.toString(), e);
          }
        }
        retVal = true;
      }
      catch(Exception e)
      {
        alertAdmin("extractBatchData(" + BatchId + "," + recId + ")", e.toString(), e);
      }
      finally
      {
        try { Db.cleanUp(); } catch(Exception eee) {}
        Db = null;
      }
      return( retVal );
    }
    
    public int getBatchType()
    {
      return( BatchType );
    }
    
    protected void storeInstallments( SettlementRecord rec )
      throws Exception
    {
      Calendar  cal         = Calendar.getInstance();
      Date      batchDate   = rec.getDate("batch_date");    // save batch date
      String    tranAmount  = rec.getData("transaction_amount");

      // use the installment amount for the future installments
      rec.setData("transaction_amount",rec.getData("installment_amount"));
      if ( !rec.isBlank(rec.getData("funding_amount")) )
        rec.setData("funding_amount", rec.getData("installment_amount"));

      for( int i = 1; i < rec.getInt("multiple_clearing_seq_count"); ++i )
      {
        cal.setTime(batchDate);
        if ( rec instanceof SettlementRecordVisa || rec instanceof SettlementRecordModBII )
        {
          cal.add(Calendar.MONTH,i);
        }
        else  // assume mc
        {
          cal.add(Calendar.DAY_OF_MONTH,i*30);
        }
        rec.setData("multiple_clearing_seq_num",i+1);
        rec.setData("batch_date",new java.sql.Date(cal.getTime().getTime()));
        if ( rec instanceof SettlementRecordMC )
        {
          rec.setData("reference_number",SettlementTools.generateReferenceNumber(rec));
        }
        Db._insertInstallmentRecord(rec);

        if ( StoreReportRecords )
        {
          Db._insertDTRecord(rec);
        }
      }
      rec.setData("multiple_clearing_seq_num",1);   // restore to original seq # (always 1)
      rec.setData("batch_date",batchDate);          // restore to original batch date
      rec.setData("transaction_amount",tranAmount); // restore tran amount
      if ( !rec.isBlank(rec.getData("funding_amount")) )
        rec.setData("funding_amount", tranAmount);
    }
    
    protected void storeReportData( SettlementRecord rec )
    {
      try
      {
        if( rec.isMerchantProcessedByMes() )
        {
          // if the account is not set up to deposit for the card type,
          //    do not ACH and set reject reason
          String  planAccepted  = "N";
          if ( getBatchType() == mesConstants.MBS_BT_CIELO )
          {
            if ( AchFlags == null ) 
            {
              AchFlags = Db._loadCieloProducts( rec.getLong("merchant_number") );
            }
            CieloProductData product = (CieloProductData)AchFlags.get(rec.getString("cielo_product"));
            if ( product != null && product.acceptsProduct(rec.getInt("multiple_clearing_seq_count",1)) )
            {
              planAccepted = "Y";
            }
          }
          else
          {
            // if the ach flags have not been loaded for this batch, load them
            if ( AchFlags == null ) 
            {
              AchFlags = Db._loadMerchantAchFlags( rec.getLong("merchant_number") );
            }
            planAccepted  = (String)AchFlags.get(rec.getPlanType());
          }
          if ( planAccepted == null || "N".equals(planAccepted) )
          {
            try
            {
              MailMessage msg = new MailMessage();
              msg.setAddresses( MesEmails.MSG_ADDRS_RESEARCH_BATCH_HOLD_NOTIFY );
              msg.setSubject( "mbs0004: " + rec.getData("merchant_number") 
                            + " ~ "       + rec.getData("card_type")
                            + (rec.isBlank(rec.getData("cielo_product")) ? "" : (" ~ " + rec.getString("cielo_product")))
                            + " ~ $"      + rec.getDouble("transaction_amount") );
              msg.setText( "Transaction cleared but not funded." );
              msg.send();
            }
            catch( Exception e )
            {
              alertAdmin("mbs0004 email exception", e.toString(), e);
            }

            rec.setData( "ach_flag"       , "N"       );
            rec.setData( "reject_reason"  , "0004"    );
          }

          // insert all card types into daily_detail_file_dt
          Db._insertDTRecord(rec);
        }
        
        switch( BatchType )
        {
          case mesConstants.MBS_BT_VISAK:
          case mesConstants.MBS_BT_PG:
            if( "840".equals(rec.getData("funding_currency_code"))  &&
                "840".equals(rec.getData("currency_code"))          )
            {
              // insert all card types into daily_detail_file_ext_dt for use by batch reports
              Db._insertExtDTRecord(rec);
            }
            break;

          case mesConstants.MBS_BT_VITAL:
          case mesConstants.MBS_BT_CIELO:
            break;
        }
      }
      catch( Exception e )
      {
        logEntry( "storeReportData(" + rec.getData("batch_id") + "," + rec.getData("batch_record_id") + ")", e.toString());
      }
    }
    
    public synchronized boolean lock(Object lockObj) 
    { 
      boolean   retVal    = false;
      if ( LockedBy == null )
      {
        LockedBy = lockObj;
        retVal = true;
      }
      return( retVal );
    }
      
    public synchronized boolean unlock(Object lockObj)
    { 
      boolean retVal = false;
      if ( LockedBy == lockObj )
      {
        LockedBy = null;
        retVal = true;
      }
      return( retVal );
    }
    
    public synchronized boolean isLocked()        
    { 
      return( LockedBy != null ); 
    }
    
		public void run() {
			try {
				if (extractBatchData() == true) {
				}
			} catch (Exception e) {
				// ignore all exceptions
				logEntry("run()", e.toString());
			} finally {
				Db = null;
			}
		}
    
    public void setBatchId( long value )
    {
      BatchId = value;
    }
    
    public void setBatchRecordId( long value )
    {
      BatchRecordId = value;
    }

    public void setBatchType( int value )
    {
      BatchType = value;
    }
  }

  private   int                         RecordCount         = 0;
  protected Date[]                      SettlementDates     = null;
  protected boolean                     StoreRecord         = true;
  protected boolean                     StoreReportRecords  = false;
  private   int                         ThreadCount         = 0;
  private   int                         ThreadCountMax      = 7;
  protected boolean                     TestOnly            = false;
  protected boolean                     Verbose             = false;
  private   Vector                      Workers             = new Vector(ThreadCountMax);
  
  public SettlementRecordExtractor()
  {
    Calendar cal = Calendar.getInstance();
    
    // clear the time component 
    cal.set(Calendar.HOUR_OF_DAY,0);
    cal.set(Calendar.MINUTE,0);
    cal.set(Calendar.SECOND,0);
    cal.set(Calendar.MILLISECOND,0);
    
    Date today = cal.getTime();
    SettlementDates = new Date[]
      {
        today,today,today,today,today   // visa, mc, amex, discover and batch date default to current date
      };
  }
  
  public SettlementRecordExtractor( boolean isProduction )
  {
    InterchangeVisa.loadConfiguration();

    if( isProduction )
    {
      SettlementDates = new Date[]
        {
          SettlementTools.getSettlementDateVisa(),
          SettlementTools.getSettlementDateMC(),
          SettlementTools.getSettlementDateAmex(),
          SettlementTools.getSettlementDateDiscover(),
          SettlementTools.getSettlementBatchDate()
        };

      setStoreReportRecords(true);  // load ext_dt and dt tables
    }
    else
    {
      Calendar cal = Calendar.getInstance();
    
      // clear the time component 
      cal.set(Calendar.HOUR_OF_DAY,0);
      cal.set(Calendar.MINUTE,0);
      cal.set(Calendar.SECOND,0);
      cal.set(Calendar.MILLISECOND,0);
    
      Date today = cal.getTime();
      SettlementDates = new Date[]
        {
          today,today,today,today,today   // visa, mc, amex, discover and batch date default to current date
        };

      setVerbose(true);     // max output
      setTestOnly(true);    // force test flag to Y
    }
  }
  
  private synchronized void adjustThreadCount( int value )
  {
    ThreadCount += value;
  }
  
  public void extractDetailRecords( int batchType, String loadFilename )
  {
    long                batchId         = 0L;
    Vector              batchList       = null;
    BatchExtractThread  extractThread   = null;
    
    log.info("[extractDetailRecords()] - Extract Settlement records for batch="+batchType+", loadFilename="+loadFilename);
    
    try
    {  	
      batchList = SettlementDb.loadBatchesToExtract(batchType, loadFilename);
      
      RecordCount = 0;    // reset the record count
      
      if ( Verbose )
      {
        System.out.println("batch count : " + batchList.size());
      }
      
      for ( int i = 0; i < batchList.size(); ++i )
      {
        String[]  batchInfo = ((String)batchList.elementAt(i)).split(",");
        batchType = Long.valueOf( batchInfo[0].trim() ).intValue();
        batchId   = Long.valueOf( batchInfo[1].trim() ).longValue();
        
        extractThread = getExtractThread();
        extractThread.setBatchType(batchType);
        extractThread.setBatchId(batchId);
        extractThread.start();
      }
      
       //wait for all extraction threads to complete
      for ( int i = 0; i < Workers.size(); ++i )
      {
        BatchExtractThread worker = (BatchExtractThread)Workers.elementAt(i);
        worker.join();
      }

      log.info("[extractDetailRecords()] - Settlement record extraction complete.");
    }
    catch( Exception e )
    {
	    log.error("Error (" + loadFilename + ")", e);
	    logEntry("extractDetailRecords(" + loadFilename + ")", e.toString());
    }
    finally
    {
    }
  }
  
  public void extractDetailRecords( int batchType, long batchId, long recId )
  {
    BatchExtractThread  extractThread   = null;

    try
    { 	
      extractThread = new BatchExtractThread(new SettlementDb());
      extractThread.setBatchType(batchType);
      extractThread.setBatchId(batchId);
      extractThread.setBatchRecordId(recId);
      extractThread.start();
      extractThread.join();
    }
    catch( Exception e )
    {
      logEntry("extractDetailRecords(" + batchId + "," + recId + ")", e.toString());
    }      
  }
  
  public void extractDetailRecordsCielo( long batchId, long recId )
  {
    extractDetailRecords(mesConstants.MBS_BT_CIELO,batchId,recId);
  }
  
  public void extractDetailRecordsPG( long batchId, long recId )
  {
    extractDetailRecords(mesConstants.MBS_BT_PG,batchId,recId);
  }
  
  public void extractDetailRecordsVisak( long batchId, long recId )
  {
    extractDetailRecords(mesConstants.MBS_BT_VISAK,batchId,recId);
  }

  public void extractDetailRecordsVital( long batchId, long recId )
  {
    extractDetailRecords(mesConstants.MBS_BT_VITAL,batchId,recId);
  }
  
  protected BatchExtractThread getExtractThread()
  {
    BatchExtractThread     retVal    = null;
    BatchExtractThread     worker    = null;
    
    while( retVal == null )
    {
      if ( Workers.size() < ThreadCountMax )
      {
        retVal = new BatchExtractThread(null);
        Workers.add(retVal);
      }
      else
      {
        for( int i = 0; i < Workers.size(); ++i )
        {
          worker = (BatchExtractThread)Workers.elementAt(i);
          if ( !worker.isAlive() )
          {
            Workers.removeElementAt(i);
            retVal = new BatchExtractThread(null);
            Workers.add(retVal);
            break;
          }
        }
        
        if ( retVal == null )
        {
          try
          {
            Thread.sleep(100);    // wait 10ms
          }
          catch( java.lang.InterruptedException ie )
          {
            // thread killed, exit loop
            break;
          }
        }
      }
    }
    return( retVal );
  }
  
  public void logEntry( String source, String message )
  {
    try
    {
      MailMessage.sendSystemErrorEmail(this.getClass().getName() + " failed", source + "\n" + message);
    }
    catch(Exception e)
    {
    }
    SyncLog.LogEntry(getClass().getName() + "::" + source, message);
  }
  
  public void setSettlementDates( Date cpdVisa, Date cpdMC, Date cpdAmex, Date cpdDisc, Date cpdBatch )
  {
    SettlementDates[0] = cpdVisa;
    SettlementDates[1] = cpdMC;
    SettlementDates[2] = cpdBatch;
    SettlementDates[3] = cpdDisc;
    SettlementDates[4] = cpdBatch;
  }
  
  public void setStoreRecord(boolean value)         { StoreRecord = value;        }
  public void setStoreReportRecords(boolean value)  { StoreReportRecords = value; }
  public void setTestOnly(boolean value)            { TestOnly = value;           }
  public void setVerbose(boolean value)             { Verbose = value;            }
  
  public static void main( String[] args )
  {
    SettlementRecordExtractor     dumper    = null;
    
    try
    {
      String  cmd         = args[0];
      long    recId       = 0L;
      
      dumper = new SettlementRecordExtractor( false );
      
      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      
      if ( "loadFile".equals(cmd) )
      {
        dumper.extractDetailRecords( mesConstants.MBS_BT_UNKNOWN, args[1] );
      }
      else if (    "loadCieloFile".equals(cmd) ||
                "loadCieloReports".equals(cmd) ||
                   "loadCieloProd".equals(cmd) )
      {
        dumper.setStoreReportRecords( "loadCieloReports".equals(cmd) ||
                                      "loadCieloProd".equals(cmd) );
        dumper.setTestOnly( !"loadCieloProd".equals(cmd) );
        dumper.extractDetailRecords( mesConstants.MBS_BT_CIELO  , args[1] );
      }
      else if ( "loadCieloBatch".equals(cmd) )
      {
        if ( args.length > 2 ) { recId = Long.parseLong(args[2]); }
        if ( args.length > 3 ) { dumper.setStoreReportRecords( "true".equals(args[3]) ); }
        dumper.extractDetailRecords( mesConstants.MBS_BT_CIELO  , Long.parseLong(args[1]),recId );
      }
      else if ( "loadPGFile".equals(cmd) )
      {
        dumper.extractDetailRecords( mesConstants.MBS_BT_PG     , args[1] );
      }
      else if ( "loadPGBatch".equals(cmd) )
      {
        if ( args.length > 2 ) { recId = Long.parseLong(args[2]); }
        if ( args.length > 3 ) { dumper.setStoreReportRecords( "true".equals(args[3]) ); }
        dumper.extractDetailRecords( mesConstants.MBS_BT_PG     , Long.parseLong(args[1]),recId );
      }
      else if ( "loadVisakFile".equals(cmd) )
      {
        dumper.extractDetailRecords( mesConstants.MBS_BT_VISAK  , args[1] );
      }
      else if ( "loadVisakBatch".equals(cmd) )
      {
        if ( args.length > 2 ) { recId = Long.parseLong(args[2]); } else { recId = -1; }
        if ( args.length > 3 ) { dumper.setStoreReportRecords( "true".equals(args[3]) ); }
        dumper.extractDetailRecords( mesConstants.MBS_BT_VISAK  , Long.parseLong(args[1]),recId );
      }
      else if (    "loadVitalFile".equals(cmd) ||
                "loadVitalReports".equals(cmd) ||
                   "loadVitalProd".equals(cmd) )
      {
        dumper.setStoreReportRecords( "loadVitalReports".equals(cmd) ||
                                      "loadVitalProd".equals(cmd) );
        dumper.setTestOnly( !"loadVitalProd".equals(cmd) );
        dumper.extractDetailRecords( mesConstants.MBS_BT_VITAL  , args[1] );
      }
      else if (     "loadVitalBatch".equals(cmd) ||
                "loadVitalBatchProd".equals(cmd) )
      {
        if ( args.length > 2 ) { recId = Long.parseLong(args[2]); } else { recId = -1; }
        if ( args.length > 3 ) { dumper.setStoreReportRecords( "true".equals(args[3]) ); }
        dumper.setTestOnly( !"loadVitalBatchProd".equals(cmd) );
		dumper.extractDetailRecords( mesConstants.MBS_BT_VITAL  , Long.parseLong(args[1]),recId );
      }
      else if (    "loadTridentFile".equals(cmd) ||
                "loadTridentReports".equals(cmd) ||
                   "loadReportsOnly".equals(cmd) )
      {
        dumper.setStoreRecord( !"loadReportsOnly".equals(cmd) );
        dumper.setStoreReportRecords( "loadTridentReports".equals(cmd) ||
                                         "loadReportsOnly".equals(cmd) );
        dumper.extractDetailRecords( mesConstants.MBS_BT_UNKNOWN, args[1] );
      }
      else
      {
        System.out.println("Invalid command: " + cmd);
      }
      
      Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      long elapsed = (endTime.getTime() - beginTime.getTime());
      
      System.out.println("Begin Time: " + String.valueOf(beginTime));
      System.out.println("End Time  : " + String.valueOf(endTime));
      System.out.println("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}
