/*************************************************************************

  FILE: $URL: http://10.1.61.151/svn/mesweb/branches/te/src/main/com/mes/settlement/InterchangeModBII.java $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $LastChangedDate: 2014-01-23 15:10:34 -0800 (Thu, 23 Jan 2014) $
  Version            : $Revision: 22205 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.settlement;

import java.util.List;
import com.mes.support.SyncLog;

public class InterchangeModBII        extends InterchangeBase
{
  public static final String      MY_CARD_TYPE          = "B2";
  public static final String      MY_IC_CLASS_PREFIX    = "com.mes.settlement.InterchangeModBII$";


  public static final int     LEN_ISSUER_IC_LVL = 3;  //"-B2"
  public static final String  MeS_TRYING_TO_QUALIFY   = "-B2";
  public static final String  MeS_FAILED_TO_QUALIFY   = "UNK";


  // ********************************************************************************
  // ********************************************************************************
  public static          class IC_ModBII                                                            extends IC_BaseClass
  {
    public                      IC_ModBII()                       { }
  }

  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************
  // ********************************************************************************

  // constructor
  public InterchangeModBII()
  {
    super( MY_CARD_TYPE, MY_IC_CLASS_PREFIX, LEN_ISSUER_IC_LVL );
  }

  public boolean getBestClass( SettlementRecord tranInfo, List nullList )
  {
    boolean       retVal          = false;
    
    try
    {
      IC_BaseClass  interchangeClass  = getInterchangeClass( "IC_ModBII" );

            String        tran_sic_code   = tranInfo.getString("sic_code");
            String        sicGroup        = "~OTH";     // default to "other"
      final String[][][]  sicGroups       =             // [][][] = [card association] [sic group name] [sic list]
      {
        { // 0 = unknown
          {   "~UNK", "0000-9999"                                                                                                               },  // no groups for this association
        },
        { // 1 = ELO
          {   "~GAS", "5122,5172,5309,5311,5541,5542,5912,5983"                                                                                 },  // Petroleum, Pharmacy, Dept Store
          {   "~SMK", "5411,5422,5441,5451,5462,5499,5921"                                                                                      },  // Supermarket
          {   "~TVL", "3357,3389,3393,3501,3502,3503,3505,3509,3512,3518,3520,3533,3535,3548,3549,3579,3631,3641,3642,3691,3700,3742,3771,4411,4723,5021,5094,5712,5714,5718,5719,5932,5937,5944,5950,5962-5969,5971,7011,7512,7513,7519,7641"  },  // Travel, Jewelry, Telemarketing
          {   "~EM1", "0763,4784,4814,4899,5039,5051,5072,5074,5198,5300,5960,6211,6300,8211,8220,8244,8249,8299"                               },  // Emerging 1
          {   "~EM2", "7276,8351,9399,9402,9405"                                                                                                },  // Emerging 2
          {   "~EM3", "4900"                                                                                                                    },  // Emerging 3
        },
        { // 2 = Diners
          {   "~GAS", "5541,5542,5983"                                                                                                          },  // Petroleum
          {   "~SMK", "5411"                                                                                                                    },  // Supermarket
          {   "~TVL", "3357,3366,3387,3389,3393,3501,3502,3503,3505,3509,3512,3518,3533,3535,3548,3549,3579,3631,3641,3642,3700,3742,3750,3771,4411,4722,4723,5309,5511,7011,7032,7033"   },  // Travel
          {   "~AIR", "3000,3001,3005,3006,3007,3008,3010,3012,3013,3015,3017,3018,3023,3030,3032,3035,3039,3052,3058,3061,3076,3102,3161,3171,3219,3247,3248,4511,4582"                  },  // Airline
          {   "~WHL", "5300"                                                                                                                    },  // Wholesalers
          {   "~MPY", "4814,4816,4899,4900,5192,5968,6300,6513,7997,8211,8220,8241,8244,8249,8299,8351,8398,8641,8661,8675,8699"                },  // Monthly Payments
          {   "~FST", "4011,4111,4112,4121,4131,4215,4784,4789,4829,5499,5814,5994,7523"                                                        },  // Fast Transactions
          {   "~ENT", "5812,5813,7832,7922,7929,7932,7991-7996,7998,7999"                                                                       },  // Entertainment
          {   "~GOV", "8651,9211,9222,9223,9311,9399,9402,9405"                                                                                 },  // Government Services
        },
      };
      int sicType;
      switch( tranInfo.getInt("card_type_enhanced") )
      {
        default    :  sicType = 0;  break;  // Unknown
        case      7:  sicType = 1;  break;  // ELO
        case      9:  sicType = 2;  break;  // Diners
      }
      for( int i = 0; i < sicGroups[sicType].length; ++i )
      {
        if( interchangeClass.testDataIsInList( 4, tran_sic_code, (sicGroups[sicType][i][1]) ) )
        {
          sicGroup = sicGroups[sicType][i][0];
          break;
        }
      }
      tranInfo.setData( "sic_group" , sicGroup );


      IcInfo  icInfo  = getBestIrfInfoFromInfoList( tranInfo, SettlementRecord.DEF_ENHANCED_IND + MeS_TRYING_TO_QUALIFY );
      if( "---".equals(icInfo.getIcCode()) )    // if no row found, get unknown
      {
              icInfo  = getBestIrfInfoFromInfoList( tranInfo, SettlementRecord.DEF_ENHANCED_IND + MeS_FAILED_TO_QUALIFY );
      }
      if( icInfo != null )
      {
        tranInfo.setData( "ic_cat"              , icInfo.getIcCode()        );
        tranInfo.setData( "ic_cat_billing"      , icInfo.getIcCodeBilling() );
        tranInfo.setData( "ic_expense"          , icInfo.getIcExpense()     );
        retVal = true;
      }

//    tranInfo.fixBadDataAfterIcAssignment();
    }
    catch( Exception e )
    {
      SyncLog.LogEntry("com.mes.settlement.InterchangeModBII.getBestClass()", e.toString());
      System.out.println(e.toString());
    }
    return( retVal );
  }
}
