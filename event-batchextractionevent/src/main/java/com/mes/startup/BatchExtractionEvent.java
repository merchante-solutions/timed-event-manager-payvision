/*@lineinfo:filename=BatchExtractionEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/BatchExtractionEvent.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-07-25 09:15:41 -0700 (Thu, 25 Jul 2013) $
  Version            : $Revision: 21336 $

  Change History:
     See SVN database

  Copyright (C) 2009,2010 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.constants.mesConstants;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.extraction.utils.ExtractionConstants;
import com.mes.settlement.SettlementRecordExtractor;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class BatchExtractionEvent extends EventBase
{
  static Logger log = Logger.getLogger(BatchExtractionEvent.class);

  protected int               BatchType             = mesConstants.MBS_BT_UNKNOWN;
  protected int               FileBankNumber        = 0;
  protected boolean           SelectDccBatches      = false;
  protected String            TestFilename          = null;
  protected boolean           fasterFundingEnabled  = false;
  public  static final String FASTER_FUNDING_ENABLED = " and mb.merchant_number in (select merchant_number from ff_merchant where faster_funding_enabled = 'Y') ";
  public  static final String FASTER_FUNDING_DISABLED = " and mb.merchant_number not in (select merchant_number from ff_merchant where faster_funding_enabled = 'Y') ";
  protected String  fasterFundingClause = null;
  

  public BatchExtractionEvent( )
  {
    PropertiesFilename = "batch-extraction.properties";
  }
  
  protected void createProcessTableEntries( String workFilename, boolean summarizeExtDt )
  {
    try
    {
      // queue the discount and interchange 
      // summarization queries
      ResultSet resultSet = null;
      ResultSetIterator it = null;
      Vector fileRecs = new Vector();
      /*@lineinfo:generated-code*//*@lineinfo:64^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct
//                  dt.batch_date,
//                  dt.load_filename
//          from    daily_detail_file_dt    dt
//          where   dt.load_filename = :workFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct\n                dt.batch_date,\n                dt.load_filename\n        from    daily_detail_file_dt    dt\n        where   dt.load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.BatchExtractionEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.BatchExtractionEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:71^7*/
      resultSet = it.getResultSet();
      
      while( resultSet.next() )
      {
        fileRecs.addElement( new FileRec(resultSet) );
      }
      resultSet.close();
      it.close();
      
      for( int i = 0; i < fileRecs.size(); ++i )
      {
        FileRec fr = (FileRec)fileRecs.elementAt(i);

        /*@lineinfo:generated-code*//*@lineinfo:85^9*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_file_dt_process
//            (
//              process_type,
//              process_sequence,
//              batch_date,
//              load_filename
//            )
//            values
//            (
//              :DailyDetailFileProcessEvent.PT_DISCOUNT_IC_CALC,
//              0,              -- not processed
//              :fr.BatchDate,
//              :fr.LoadFilename
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into daily_detail_file_dt_process\n          (\n            process_type,\n            process_sequence,\n            batch_date,\n            load_filename\n          )\n          values\n          (\n             :1 ,\n            0,              -- not processed\n             :2 ,\n             :3 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.BatchExtractionEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,ExtractionConstants.PT_DISCOUNT_IC_CALC.getValue());
   __sJT_st.setDate(2,fr.getBatchDate());
   __sJT_st.setString(3,fr.getLoadFilename());
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:101^9*/
      }
      
      // queue bml payments
      /*@lineinfo:generated-code*//*@lineinfo:105^7*/

//  ************************************************************
//  #sql [Ctx] { insert into bml_payment_process
//          (
//            process_type,
//            process_sequence,
//            load_filename
//          )
//          values
//          (
//            0,
//            0,
//            :workFilename
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into bml_payment_process\n        (\n          process_type,\n          process_sequence,\n          load_filename\n        )\n        values\n        (\n          0,\n          0,\n           :1 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.BatchExtractionEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:119^7*/
    }
    catch( Exception e )
    {
      logEntry("createProcessTableEntries(" + workFilename + ")",e.toString());
    }
    finally
    {
    }      
  }
  
  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    boolean       retVal     = false;
    
    try
    {
      // get an automated timeout exempt connection
      connect(true);

      // get the arguments
      FileBankNumber  = Integer.parseInt( getEventArg(0) );
      BatchType       = Integer.parseInt( getEventArg(1) );
      SelectDccBatches= "Y".equals( getEventArg(2).toUpperCase() );
      fasterFundingEnabled = "Y".equals(getEventArg(3));
      
      switch( BatchType )
      {
        case mesConstants.MBS_BT_UNKNOWN:
        case mesConstants.MBS_BT_VISAK:
        case mesConstants.MBS_BT_PG:
        case mesConstants.MBS_BT_CIELO:
          retVal = extractMbsBatches();
          break;

        case mesConstants.MBS_BT_VITAL:
          retVal = extractVitalBatches();
          break;
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  protected boolean extractMbsBatches()
  {
    final String                  crlf            = "\r\n";
    SettlementRecordExtractor     extractor       = null;
    String                        tempStr         = null;
    String                        whereClause     = null;
    long                          workFileId      = 0L;
    String                        workFilename    = null;
    
    try
    {
      extractor = new SettlementRecordExtractor( true );


      whereClause   = "     mb.merchant_batch_date >= trunc(sysdate-30) " + crlf
                   +  " and mb.load_file_id = 0                         " + crlf
                   +  " and mb.load_filename is null                    " + crlf
                   +  " and mb.response_code = 0                        " + crlf    // only GB's
                   +  " and mb.test_flag = 'N'                          " + crlf;   // only prod batches

      tempStr       = ( FileBankNumber == 9999 ) ? ( " is not null " ) : ( " = " + FileBankNumber );
      whereClause  += " and mb.bank_number " + tempStr + crlf;

      tempStr       = SelectDccBatches ? " != " : " = ";
      whereClause  += " and nvl(mb.currency_code,'840') " + tempStr + crlf
                   +  " ( select nvl(mf.funding_currency_code,'840') from mif mf " + crlf
                   +  "    where mf.merchant_number = mb.merchant_number ) " + crlf;
      
      fasterFundingClause = fasterFundingEnabled ? FASTER_FUNDING_ENABLED : 
                                                   FASTER_FUNDING_DISABLED ;
      
      if( BatchType != mesConstants.MBS_BT_UNKNOWN )
        whereClause  += " and mb.mbs_batch_type = " + BatchType + crlf;
      

      int recCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:208^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)    
//          from    mbs_batches   mb
//          where   :whereClause
//                  :fasterFundingClause      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  count(1)     \n        from    mbs_batches   mb\n        where    ");
   __sjT_sb.append(whereClause);
   __sjT_sb.append(fasterFundingClause);
   String __sjT_sql = __sjT_sb.toString();
   
   String __sjT_tag = "3com.mes.startup.BatchExtractionEvent:" + __sjT_sql;
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  log.debug("[extractMbsBatches() - "+__sjT_tag+" : "+__sjT_sql);
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:213^7*/

      //@ System.out.println( crlf + whereClause + crlf + "   rec count = " + recCount + crlf );

	log.info("[extractMbsBatches()] - Number of batches to extract: " + recCount);
      if( recCount != 0 )
      {
        workFilename    = buildFilename("mddf" + FileBankNumber);
        workFileId      = loadFilenameToLoadFileId( workFilename );

        // mark the MBS_BATCHES batches for extraction
        /*@lineinfo:generated-code*//*@lineinfo:223^9*/

//  ************************************************************
//  #sql [Ctx] { update  mbs_batches   mb
//            set     mb.load_filename  = :workFilename,
//                    mb.load_file_id   = :workFileId
//            where   :whereClause
//                    :fasterFundingClause                      
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("update  mbs_batches   mb\n          set     mb.load_filename  =  ? ,\n                  mb.load_file_id   =  ? \n          where    ");
   __sjT_sb.append(whereClause);
   __sjT_sb.append(fasterFundingClause);
   String __sjT_sql = __sjT_sb.toString();

   String __sjT_tag = "4com.mes.startup.BatchExtractionEvent:" + __sjT_sql;
   log.debug("[extractMbsBatches()] - "+__sjT_tag+":"+__sjT_sql);
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,__sjT_tag,theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setLong(2,workFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:229^9*/

        extractor.extractDetailRecords( BatchType, workFilename );
        createProcessTableEntries(workFilename,true);
      }
    }
    catch( Exception e )
    {
	    log.error("Error.", e);
	    logEvent(this.getClass().getName(), "extractMbsBatches()", e.toString());
	    logEntry("extractMbsBatches()", e.toString());
    }
    finally
    {
    }
    return( true );
  }
  
  protected boolean extractVitalBatches()
  {
    ProcessTable.ProcessTableEntry  entry         = null;
    SettlementRecordExtractor       extractor     = null;
    ProcessTable                    pt            = null;
    
    try
    {
      pt = new ProcessTable("settlement_process","settlement_process_sequence",ProcessTable.PT_ALL);
      
      if ( pt.hasPendingEntries() )
      {
        extractor = new SettlementRecordExtractor( true );

        pt.preparePendingEntries();
        
        Vector entries = pt.getEntryVector();
        for( int i = 0; i < entries.size(); ++i )
        {
          entry = (ProcessTable.ProcessTableEntry)entries.elementAt(i);
          
          switch( entry.getProcessType() )
          {
            case 0:   // extract vital transactions
              entry.recordTimestampBegin();
              extractor.extractDetailRecords( mesConstants.MBS_BT_VITAL , entry.getLoadFilename() );
              entry.recordTimestampEnd();
              createProcessTableEntries( entry.getLoadFilename(), false );
              break;
          }
        }
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "extractVitalBatches()", e.toString());
      logEntry("extractVitalBatches()", e.toString());
    }
    finally
    {
    }
    return( true );
  }
  
  public static void main( String[] args )
  {
    BatchExtractionEvent        test          = null;
    
    try
    {
      SQLJConnectionBase.initStandalonePool();
      
      test = new BatchExtractionEvent();
      
      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      
      if ( "createProcessTableEntries".equals(args[0]) )
      {
        test.connect();
        test.createProcessTableEntries( args[1],"true".equals(args[2]) );
        test.cleanUp();
      }
      else if ( "execute".equals(args[0]) && args.length == 5)
      {
          test.setEventArgs(args[1] + "," + args[2] + "," + args[3] + "," + args[4]);   // bank number , batch type, select DCC batches (Y/N), faster funding
          test.execute();
      }
      else if ( "execute".equals(args[0]) )
      {
        test.setEventArgs(args[1] + "," + args[2] + "," + args[3]);   // bank number , batch type, select DCC batches (Y/N)
        test.execute();
      } 
      
      Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      long elapsed = (endTime.getTime() - beginTime.getTime());
      
      log.debug("Begin Time: " + String.valueOf(beginTime));
      log.debug("End Time  : " + String.valueOf(endTime));
      log.debug("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));
    }
    finally
    {
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}/*@lineinfo:generated-code*/
