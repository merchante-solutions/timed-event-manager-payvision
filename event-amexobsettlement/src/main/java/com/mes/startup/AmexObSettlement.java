package com.mes.startup;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.settlement.SettlementRecord;
import com.mes.support.PropertiesFile;

public class AmexObSettlement extends AmexFileBase {
	
	
	public AmexObSettlement(){
// Need to adjust these values for optblue
		PropertiesFilename          = "amex-settlement.properties";
	    CardTypeTable               = "amex_settlement";
	    CardTypeTableActivity       = "amex_settlement_activity";
	    CardTypePostTotals          = "am_ob_settle";
	    CardTypeFileDesc            = "Amex Optblue Settlement";
	    DkOutgoingHost              = MesDefaults.DK_AMEX_OUTGOING_HOST;
	    DkOutgoingUser              = MesDefaults.DK_AMEX_OUTGOING_USER;
	    DkOutgoingPassword          = MesDefaults.DK_AMEX_OUTGOING_PASSWORD;
	    DkOutgoingPath              = MesDefaults.DK_AMEX_OPTB_OUTGOING_PATH; 
	    DkOutgoingUseBinary         = false;
	    DkOutgoingSendFlagFile      = true;
	    SettlementAddrsNotify       = MesEmails.MSG_ADDRS_OUTGOING_AMEX_NOTIFY;
	    SettlementAddrsFailure      = MesEmails.MSG_ADDRS_OUTGOING_AMEX_FAILURE;
	    SettlementRecMT             = SettlementRecord.MT_FIRST_PRESENTMENT;

	}
	
	protected boolean processTransactions( ){
		log.debug("process transactions....");
	    String workFilename = null;
	    int bankNumber        = Integer.parseInt( getEventArg(0) );
		try{
			FileTimestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
	        workFilename    = generateFilename(  ( "R".equals(ActionCode) ? "am_ob_rev" : "am_ob_settle" ) + bankNumber ,   ".dat");                                                    
	        System.out.println("workFilename:" + workFilename);
	        if(ActionCode == null){
	        	generateSettlement(workFilename);
	        } else {
	        	generateRevSettlement(workFilename);
	        }
	        
		}catch(Exception e){
	    	logEvent(this.getClass().getName(), "processTransactions()", e.toString());
	        logEntry("processTransactions()", e.toString());
		}
		
		return true;
	}
	
	/*To-Do add Reverse/ rejects clearing logic.
	 * 
	 */
	protected void generateRevSettlement(String workFilename){
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		long workFileId = 0L; 
		int recCount = 0;
		Date cpd = null;
		
		String updateQ = " update amex_settlement_activity ama " +
				" set ama.load_filename = ? , ama.load_file_id = ? , ama.settlement_date = ? " +
				" where ama.load_file_id = 0 " +
				" and ama.load_filename is null " +
				" and ama.action_code = ? " +
				" and exists (" +
				" select  am.rec_id " +
				" from amex_settlement am" +
				" where am.rec_id = ama.rec_id " +
				" and am.test_flag = 'N' " +
				" and am.amex_se_number in (SE_LIST) )";

		String selectQ = "select am.*, "
				+ " ama.reproc as external_reject " +
				" from amex_settlement_activity ama, amex_settlement am" +
				" where ama.load_file_id = ?" +
				" and ama.action_code = ? " +
				" and am.rec_id = ama.rec_id " +
				" order by am.batch_id, am.batch_record_id";
		
		log.debug("generating Reverse/Reject file for " + workFilename);

		try{
			workFileId = loadFilenameToLoadFileId(workFilename);
			cpd = getSettlementDate();
			PropertiesFile props = new PropertiesFile("optblue.properties");
		    String sid = props.getString("se_list_"+getEventArg(0));
		    updateQ = updateQ.replace("SE_LIST", sid);
		    
			String actionCode = ActionCode == null ? "R": ActionCode;
		    ps = con.prepareStatement(updateQ);
			ps.setString(1, workFilename);
			ps.setLong(2, workFileId);
			ps.setDate(3, cpd);
			ps.setString(4, actionCode);
			recCount =  ps.executeUpdate();

			log.debug("recCount:"+recCount);
			ps.close();
			
			if(recCount > 0){
				ps = con.prepareStatement(selectQ);
				ps.setLong(1, workFileId);
				ps.setString(2, actionCode);
				rs = ps.executeQuery();
				FileReferenceId = String.valueOf(workFileId);
				handleSelectedOBRecords( rs, workFilename,workFileId);
			 } else {
				 log.debug("no rev or reject records found");
			 }
		}catch(Exception e){
			logEntry("generateRevSettlement("+ workFilename +")", e.toString());
		} finally{
			try{
				if(ps != null) ps.close();
				if(rs != null) rs.close();
			}catch(Exception ignore){}
		}
				
	}
	
	protected void generateSettlement(String workFilename){
		long workFileId        = 0L; 
		PreparedStatement ps = null;
		ResultSet rs = null;
		int recCount = 0;
		Date cpd = null;
		String updateQ = 	" update amex_settlement am set am.output_filename = ?," +
 						" am.output_file_id = ?, am.settlement_date = ? " +
 						" where am.output_file_id = 0 and am.output_filename is null " +
 						" and am.test_flag = 'N' and am.amex_se_number in (SE_LIST) ";
		
		String selectQ = " select " +
				        " am.* from amex_settlement am where am.output_file_id = ? and " +
				        " am.test_flag = 'N' order by am.batch_id, am.batch_record_id ";
		log.debug("generating settlement file for " + workFilename);
		
		try{
			 workFileId = loadFilenameToLoadFileId(workFilename);
			 cpd = getSettlementDate();
			 PropertiesFile props = new PropertiesFile("optblue.properties");
		     String sid = props.getString("se_list_"+getEventArg(0));
		     updateQ = updateQ.replace("SE_LIST", sid);
			 ps = con.prepareStatement(updateQ);
			 ps.setString(1,workFilename);
			 ps.setLong(2,workFileId);
			 ps.setDate(3, cpd);
			 recCount =  ps.executeUpdate();
			 log.debug("recCount:"+recCount);
			 
			 if(recCount > 0){
				 ps = con.prepareStatement(selectQ);
				 ps.setLong(1, workFileId);
				 rs = ps.executeQuery();
				 FileReferenceId = String.valueOf(workFileId);
				 handleSelectedOBRecords( rs, workFilename );
			 }

		} catch(Exception e){
			logEntry("generateSettlement("+ workFilename +")", e.toString());
		}finally{
			try{ ps.close(); rs.close(); }catch(Exception e){}
		}
	}
	
	public static void main(String args[]){
		AmexObSettlement amob = null;
	    try
	    {
	        if (args.length > 0 && args[0].equals("testproperties")) {
	            EventBase.printKeyListStatus(new String[]{
	                    MesDefaults.DK_AMEX_OUTGOING_HOST, MesDefaults.DK_AMEX_OUTGOING_USER,
	                    MesDefaults.DK_AMEX_OUTGOING_PASSWORD, MesDefaults.DK_AMEX_OUTGOING_PATH
	            });
	        }
	      amob = new AmexObSettlement();
	      amob.executeCommandLine( args );
	    
	    }
	    catch( Exception e )
	    {
	      log.debug(e.toString());
	    }
	    finally
	    {
	      try{ amob.cleanUp(); } catch( Exception e ) {}
	      Runtime.getRuntime().exit(0);
	    }		
	}

}
