/*@lineinfo:filename=VirtualAppSubmitter*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/VirtualAppSubmitter.sqlj $

  Description:

    AccountLookup

    Support bean for account lookup page in account maintenance.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 8/24/04 2:27p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Vector;
import org.jdom.Document;
import com.mes.config.MesDefaults;
import com.mes.virtualapp.VirtualAppProcessor;
import com.mes.virtualapp.VirtualAppXML;
import sqlj.runtime.ResultSetIterator;

public class VirtualAppSubmitter extends EventBase
{
  private Vector  apps            = null;
  private int     processSequence = 0;
  
  private String submitApp(String vSeq)
  {
    String result = "attempted";
    try
    {
      // see if app might be a duplicate
      int appCount = 0;
      
      String      vendorId = "";
      Timestamp   postDate = null;
      
      /*@lineinfo:generated-code*//*@lineinfo:57^7*/

//  ************************************************************
//  #sql [Ctx] { select  vendor_id,
//                  post_date
//          
//          from    vapp_xml_post
//          where   vapp_seq_num = :vSeq
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  vendor_id,\n                post_date\n         \n        from    vapp_xml_post\n        where   vapp_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.VirtualAppSubmitter",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,vSeq);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vendorId = (String)__sJT_rs.getString(1);
   postDate = (java.sql.Timestamp)__sJT_rs.getTimestamp(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:65^7*/
      
      /*@lineinfo:generated-code*//*@lineinfo:67^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(vendor_id)
//          
//          from    vapp_xml_post
//          where   vendor_id = :vendorId and
//                  post_date < :postDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(vendor_id)\n         \n        from    vapp_xml_post\n        where   vendor_id =  :1  and\n                post_date <  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.VirtualAppSubmitter",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,vendorId);
   __sJT_st.setTimestamp(2,postDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:74^7*/
      
      if(appCount > 0)
      {
        // more than one instance means a duplicate
        result = "duplicate - not added";
      }
      else
      {
        // see if this app already exists
        /*@lineinfo:generated-code*//*@lineinfo:84^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(vapp_seq_num)
//            
//            from    vapp_xml_post
//            where   vapp_seq_num = :vSeq and
//                    app_seq_num is not null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(vapp_seq_num)\n           \n          from    vapp_xml_post\n          where   vapp_seq_num =  :1  and\n                  app_seq_num is not null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.VirtualAppSubmitter",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,vSeq);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:91^9*/
        
        if(appCount > 0)
        {
          result = "app already created";
        }
        else
        {
          // get xml from database
          VirtualAppProcessor processor = new VirtualAppProcessor(Ctx);
      
          Document xmlAppDoc = processor.getXMLDoc(Long.parseLong(vSeq));
      
          VirtualAppXML xmlApp = new VirtualAppXML(Ctx);
      
          xmlApp.addAppManually(xmlAppDoc, Long.parseLong(vSeq));
        
          result = "success";
        }
      }
    }
    catch(Exception e)
    {
      logEntry("submitApp(" + vSeq + ")", e.toString());
    }
    
    return result;
  }
  
  public boolean execute()
  {
    boolean             result  = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try
    {
      connect();
      
      // see if there are apps to process
      int appCount;
      /*@lineinfo:generated-code*//*@lineinfo:132^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(vapp_seq_num)
//          
//          from    vapp_xml_process
//          where   process_sequence is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(vapp_seq_num)\n         \n        from    vapp_xml_process\n        where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.VirtualAppSubmitter",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:138^7*/
      
      if(appCount > 0)
      {
        // get new process sequence
        /*@lineinfo:generated-code*//*@lineinfo:143^9*/

//  ************************************************************
//  #sql [Ctx] { select  vapp_process_sequence.nextval
//            
//            from    dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  vapp_process_sequence.nextval\n           \n          from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.VirtualAppSubmitter",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   processSequence = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:148^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:150^9*/

//  ************************************************************
//  #sql [Ctx] { update  vapp_xml_process
//            set     process_sequence = :processSequence
//            where   process_sequence is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  vapp_xml_process\n          set     process_sequence =  :1 \n          where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.VirtualAppSubmitter",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,processSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:155^9*/
        
        commit();
        
        // get vector of items
        /*@lineinfo:generated-code*//*@lineinfo:160^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  vapp_seq_num
//            from    vapp_xml_process
//            where   process_sequence = :processSequence
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  vapp_seq_num\n          from    vapp_xml_process\n          where   process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.VirtualAppSubmitter",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,processSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.startup.VirtualAppSubmitter",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:165^9*/
        
        rs = it.getResultSet();
        
        apps = new Vector();
        while(rs.next())
        {
          apps.add(rs.getString("vapp_seq_num"));
        }
        
        rs.close();
        it.close();
        
        // process apps
        for(int i=0; i<apps.size(); ++i)
        {
          String vappSeqString = (String)(apps.elementAt(i));
          String action = submitApp(vappSeqString);
          
          /*@lineinfo:generated-code*//*@lineinfo:184^11*/

//  ************************************************************
//  #sql [Ctx] { update  vapp_xml_process
//              set     date_processed = sysdate,
//                      result = :action
//              where   process_sequence = :processSequence and
//                      vapp_seq_num = :vappSeqString
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  vapp_xml_process\n            set     date_processed = sysdate,\n                    result =  :1 \n            where   process_sequence =  :2  and\n                    vapp_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.VirtualAppSubmitter",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,action);
   __sJT_st.setInt(2,processSequence);
   __sJT_st.setString(3,vappSeqString);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:191^11*/
        }
      }
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }

  public static void main( String[] args )
    {
        try
        {

            if (args.length > 0 && args[0].equals("testproperties")) {
                EventBase.printKeyListStatus(new String[]{
                        MesDefaults.DK_SYN_XML_SUBSCRIBER_ID,
                        MesDefaults.DK_SYN_XML_USER_ID,
                        MesDefaults.DK_SYN_XML_PASSWORD,
                        MesDefaults.DK_SYN_XML_REQUEST_TYPE,
                        MesDefaults.DK_SYN_XML_PROVIDER_ID,
                        MesDefaults.DK_SYN_XML_PRODUCT_ID
                });
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }


    }
}/*@lineinfo:generated-code*/