/*@lineinfo:filename=AppBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/AppBase.sqlj $

  AppBase

  Base class for application beans. Extends forms FieldBean. Manages
  AppSequence object and handles creation of new applications.

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-07 15:59:41 -0700 (Mon, 07 Jul 2008) $
  Version            : $Revision: 15046 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.constants.MesQueues;
import com.mes.constants.MesUsers;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.forms.ButtonField;
import com.mes.forms.Field;
import com.mes.forms.FieldBean;
import com.mes.forms.HiddenField;
import com.mes.forms.NumberField;
import com.mes.forms.Validation;
import com.mes.tools.AppNotifyBean;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class AppBase extends FieldBean
{
  static Logger log = Logger.getLogger(AppBase.class);
  
  protected AppSequence appSeq = null;
  
  protected int         appType;
  protected int         curScreenId;
  protected boolean     submitPartial = false;
  
  // warning ids
  public static final int WARN_TRAN_NUM_NOT_FOUND = 1;

  /*************************************************************************
  **
  **   Drop Down Tables
  **
  **************************************************************************/

  protected class YesNoTable extends DropDownTable
  {
    public YesNoTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("Y","Yes");
      addElement("N","No");
    }
  }

  protected class MonthTable extends DropDownTable
  {
    public MonthTable()
    {
      // value/name pairs
      addElement("","select");
      addElement("1","Jan");
      addElement("2","Feb");
      addElement("3","Mar");
      addElement("4","Apr");
      addElement("5","May");
      addElement("6","Jun");
      addElement("7","Jul");
      addElement("8","Aug");
      addElement("9","Sep");
      addElement("10","Oct");
      addElement("11","Nov");
      addElement("12","Dec");
    }
  }

  /*************************************************************************
  **
  **   Validations
  **
  **************************************************************************/
  /*
  ** public class TransitRoutingValidation extends SQLJConnectionBase
  **
  ** Validates transit routing numbers.  Makes sure the number is valid by
  ** checking it's length and then trying to look it up in rap_app_bank_aba,
  ** a table containing known trn's.  If not found and a prior warning has
  ** not been set, a warning is set and the field is invalid.  If a prior
  ** warning has been given then the field is considered valid.
  */
  public class TransitRoutingValidation extends SQLJConnectionBase
    implements Validation
  {
    private String errorText = null;

    public String getErrorText()
    {
      return errorText;
    }

    public boolean validate(String fdata)
    {
      // never try to validate blank fields, let required validation handle this
      if (fdata == null || fdata.equals(""))
      {
        return true;
      }

      // make sure transit num has 9 digits
      if (countDigits(fdata) != 9)
      {
        errorText = "Transit routing number must be nine digits";
        return false;
      }

      boolean isValid = false;

      try
      {
        connect();

        // lookup the transit routing number
        int itemCount = 0;
        int fraudCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:149^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(transit_routing_num),
//                    nvl(sum(decode(fraud_flag,'Y',1,0)),0)
//            
//            from    rap_app_bank_aba
//            where   transit_routing_num = :fdata
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(transit_routing_num),\n                  nvl(sum(decode(fraud_flag,'Y',1,0)),0)\n           \n          from    rap_app_bank_aba\n          where   transit_routing_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.AppBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,fdata);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   fraudCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:157^9*/

        // if the transit routing number has been flagged as fraudulent
        // don't allow it to be submitted
        if (fraudCount > 0)
        {
          errorText = "Transit routing number not allowed";
        }
        // if user hasn't already been warned and transit routing number
        // is not found in our table, then add warning about unknown
        // transit routing num and flag field as invalid
        else if (itemCount == 0 && !hasWarning(WARN_TRAN_NUM_NOT_FOUND))
        {
          errorText = "Transit routing number not found, please confirm that "
                      + "it is correct and resubmit";
          addWarning(WARN_TRAN_NUM_NOT_FOUND);
        }
        // else clear any warning
        else
        {
          removeWarning(WARN_TRAN_NUM_NOT_FOUND);
          isValid = true;
        }
      }
      catch(java.sql.SQLException e)
      {
        logEntry("validate()",e.toString());
        errorText = "Transit routing number validation failed: " + e.toString();
      }
      finally
      {
        cleanUp();
      }

      return isValid;
    }
  }

  /*
  ** public class CardAcceptedValidation implements Validation
  **
  ** Validates card type acceptance check boxes.  If checked then one and only
  ** one field in the given list must be non-blank. If not checked no fields
  ** in the list may be checked.  Card acceptance will usually require a
  ** pre-existing merchant account no. with the card issuing entity, although
  ** some card types may be setup by mes if we are given a quoted rate with
  ** which to apply to the issuer for account setup on behalf of the merchant.
  */
  public class CardAcceptedValidation implements Validation
  {
    protected String  errorText = null;
    protected Vector  fieldList = new Vector();

    public void addField(Field field)
    {
      fieldList.add(field);
    }

    private String getFieldLabels()
    {
      StringBuffer fieldLabels = new StringBuffer();
      for (Iterator i = fieldList.iterator(); i.hasNext();)
      {
        Field field = (Field)i.next();
        fieldLabels.append(field.getLabel());
        if (i.hasNext())
        {
          fieldLabels.append(", ");
        }
      }
      return fieldLabels.toString();
    }

    public boolean validate(String fdata)
    {
      String clearVerbiage = "";
      String provideVerbiage = "";

      switch (fieldList.size())
      {
        case 0:
        case 1:
          clearVerbiage = "clear";
          provideVerbiage = "provide";
          break;

        default:
          clearVerbiage = "make sure these fields are cleared:";
          provideVerbiage = "provide only one of the following:";
          break;
      }

      // if field is blank make sure no dependent fields are non-blank
      if (fdata == null || !fdata.equals("y"))
      {
        for (Iterator i = fieldList.iterator(); i.hasNext();)
        {
          Field field = (Field)i.next();
          if (!field.isBlank())
          {
            // found a non-blank field, unchecked
            // acceptance field is not valid
            errorText = "If merchant is not to accept this card type please "
                        + clearVerbiage + " "
                        + getFieldLabels();
            return false;
          }
        }
        // no non-blank found, unchecked acceptance field is valid
        return true;
      }

      // card accepted is checked so require only one non-blank field
      int nonBlankCount = 0;
      for (Iterator i = fieldList.iterator(); i.hasNext();)
      {
        Field field = (Field)i.next();
        if (!field.isBlank())
        {
          ++nonBlankCount;
        }
      }

      // look for checked acceptance with all blanks or too many non-blanks
      if (nonBlankCount != 1)
      {
        errorText = "If merchant is to accept this card type please "
                    + provideVerbiage + " "
                    + getFieldLabels();
        return false;
      }

      // checked acceptance and only one non-blank: valid
      return true;
    }

    public String getErrorText()
    {
      return errorText;
    }
  }


  /*************************************************************************
  **
  **   Custom Fields
  **
  **************************************************************************/

  protected class YearField extends NumberField
  {
    public YearField(String fname, boolean nullAllowed)
    {
      super(fname,4,4,nullAllowed,0);
      addValidation(new YearValidation("Invalid year",nullAllowed));
    }

    protected String processData(String rawData)
    {
      if( rawData == null || rawData.equals("0") )
      {
        rawData = "";
      }
      return(rawData);
    }
  }

  /*************************************************************************
  **
  **   Field Bean Methods
  **
  **************************************************************************/

  public AppBase()
  {
  }
  
  public AppBase(DefaultContext defCtx)
  {
    super(defCtx);
  }
  
  public void initFields( )
  {
    createFields(null);
  }
  
  protected boolean applyingForAmex()
  {
    int         recCount  = 0;
    boolean     retVal    = false;
    
    try
    {
      connect();
      
      long appSeqNum = fields.getField("appSeqNum").asLong();
      if (appSeqNum != 0L)
      {
        /*@lineinfo:generated-code*//*@lineinfo:356^9*/

//  ************************************************************
//  #sql [Ctx] { select count(mpo.app_seq_num) 
//            from    merchpayoption    mpo
//            where   mpo.app_seq_num = :appSeqNum and
//                    mpo.cardtype_code = :mesConstants.APP_CT_AMEX and -- 16 and
//                    not mpo.merchpo_rate is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(mpo.app_seq_num)  \n          from    merchpayoption    mpo\n          where   mpo.app_seq_num =  :1  and\n                  mpo.cardtype_code =  :2  and -- 16 and\n                  not mpo.merchpo_rate is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.AppBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_AMEX);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:363^9*/
        retVal = (recCount > 0);
      }        
    }
    catch( Exception e )
    {
      logEntry("applyingForAmex()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  /*
  ** protected boolean appCompleted()
  **
  ** Returns TRUE if the app has been submitted to completion; false otherwise
  */
  protected boolean appCompleted()
  {
    boolean retVal    = false;
    long    appSeqNum = -1;
    try
    {
      connect();
      
      appSeqNum = fields.getField("appSeqNum").asLong();
      
      int recCount = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:395^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    xml_applications
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    xml_applications\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.AppBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:401^7*/
      
      retVal = (recCount > 0);
    }
    catch(Exception e)
    {
      logEntry("appCompleted(" +appSeqNum+ ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return retVal;
  }

  /*
  ** protected void createFields(HttpServletRequest request)
  **
  ** Creates fields common to all applications.
  */
  protected void createFields(HttpServletRequest request)
  {
    // fields common to all apps
    super.createFields(request);
    
    fields.add(new HiddenField("appSeqNum"));
    fields.addAlias("appSeqNum","primaryKey");
    fields.add(new HiddenField("appType"));
    fields.add(new HiddenField("controlNum"));
    fields.add(new HiddenField("curScreenId"));
    fields.add(new ButtonField("submit","Submit"));
    fields.add(new WarningField());
    
    // attempt to set app seq num based early so that
    // beans like pricing can use it when generating
    // various fields
    // also relied on by AppDone bean now...
    if(request != null)
    {
      String appSeqNum;
      if ((appSeqNum = request.getParameter("appSeqNum")) != null
          || (appSeqNum = request.getParameter("primaryKey")) != null)
      {
        fields.setData("appSeqNum",appSeqNum);
      }
    }
    
    // set the app type and screen id; the child class that is
    // extending this class is expected to have initialized these
    fields.setData("appType",Integer.toString(appType));
    fields.setData("curScreenId",Integer.toString(curScreenId));
  }
  
  /*
  ** public void postHandleRequest(HttpServletRequest request)
  **
  ** This method is responsible for making sure that a valid app sequence is
  ** setup.  That means that the appType must be determined, either by having
  ** been set by the child class or by using an appSeqNum to determine an
  ** existing app's appType.  If no appSeqNum exists, then it will be up to
  ** the auto submit method to create a new app when a successful submit has
  ** occured.
  */
  protected void postHandleRequest(HttpServletRequest request)
  {
    try
    {
      // create the application screen sequence
      if(parent == null || !(parent instanceof AppBase))
      {
        if(appSeq == null)
        {
          appSeq = new AppSequence();
        }
      }
      else
      {
        // make sure parent is of the correct type
        if(((AppBase)parent).appSeq == null)
        {
          ((AppBase)parent).appSeq = new AppSequence();
        }
        
        appSeq = ((AppBase)parent).appSeq;
      }

      // get app type from existing application if appSeqNum
      // is defined (also makes sure control num is loaded)
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      if (appSeqNum != 0L)
      {
        try
        {
          connect();

          int appType = 0;
          long controlNum = 0L;
          /*@lineinfo:generated-code*//*@lineinfo:499^11*/

//  ************************************************************
//  #sql [Ctx] { select  a.app_type,
//                      m.merc_cntrl_number
//              
//              from    application a,
//                      merchant    m
//              where   a.app_seq_num = :appSeqNum
//                      and m.app_seq_num = a.app_seq_num
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  a.app_type,\n                    m.merc_cntrl_number\n             \n            from    application a,\n                    merchant    m\n            where   a.app_seq_num =  :1 \n                    and m.app_seq_num = a.app_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.AppBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   controlNum = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:509^11*/

          fields.setData("appType",Integer.toString(appType));
          fields.setData("controlNum",Long.toString(controlNum));
        }
        catch (Exception e)
        {
          System.out.println(this.getClass().getName() + "::postHandleRequest(): "
            + e.toString());
          logEntry("postHandleRequest()",e.toString());
        }
        finally
        {
          cleanUp();
        }
      }

      // set the app type in the sequence (this will cause seq id and other
      // org_app fields to load as well within the sequence object, which is
      // especially important in createNewApp())
      appSeq.setAppType(fields.getField("appType").asInteger(), submitPartial);

      // sets the current screen based on child classes setting of curScreenId
      appSeq.setCurScreenId(fields.getField("curScreenId").asInteger());
      
      // if app seq num is present, set it in the sequence
      if (appSeqNum != 0L)
      {
        appSeq.setAppSeqNum(fields.getField("appSeqNum").asLong());
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::postHandleRequest(): "
        + e.toString());
      logEntry("postHandleRequest()",e.toString());
    }
  }
  
  /*
  ** protected void createNewApp(HttpServletRequest request)
  **
  ** Creates a new application.  To do so, the appType must have been set in
  ** appSeq and a user object is needed.  This base class assumes that child
  ** classes will have made sure the appType has been established.  The new
  ** app's appSeqNum is set in the appSeq object.
  */
  protected void createNewApp(HttpServletRequest request)
  {
    // these need to be created
    long    controlNum    = 0L;
    long    appSeqNum     = 0L;

    try
    {
      connect();
      
      // generate new app seq num and control number
      AppSeqNumGenerator  numGen = new AppSeqNumGenerator(Ctx);
      
      appSeqNum   = numGen.getAppSeqNum();
      controlNum  = numGen.getControlNumber();
      
      fields.setData("appSeqNum",Long.toString(appSeqNum));
      fields.setData("controlNum",Long.toString(controlNum));

      // create application record for the new app
      /*@lineinfo:generated-code*//*@lineinfo:576^7*/

//  ************************************************************
//  #sql [Ctx] { insert into application
//          ( app_seq_num,
//            appsrctype_code,
//            app_created_date,
//            app_scrn_code,
//            app_user_id,
//            app_user_login,
//            app_type,
//            screen_sequence_id )
//          values
//          ( :appSeqNum,
//            :appSeq.getAppSourceType(),
//            sysdate,
//            null,
//            :user.getUserId(),
//            :user.getLoginName(),
//            :appSeq.getAppType(),
//            :appSeq.getSeqId() )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_1930 = appSeq.getAppSourceType();
 long __sJT_1931 = user.getUserId();
 String __sJT_1932 = user.getLoginName();
 int __sJT_1933 = appSeq.getAppType();
 int __sJT_1934 = appSeq.getSeqId();
   String theSqlTS = "insert into application\n        ( app_seq_num,\n          appsrctype_code,\n          app_created_date,\n          app_scrn_code,\n          app_user_id,\n          app_user_login,\n          app_type,\n          screen_sequence_id )\n        values\n        (  :1 ,\n           :2 ,\n          sysdate,\n          null,\n           :3 ,\n           :4 ,\n           :5 ,\n           :6  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.app.AppBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_1930);
   __sJT_st.setLong(3,__sJT_1931);
   __sJT_st.setString(4,__sJT_1932);
   __sJT_st.setInt(5,__sJT_1933);
   __sJT_st.setInt(6,__sJT_1934);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:596^7*/

      // create merchant record for the new app
      /*@lineinfo:generated-code*//*@lineinfo:599^7*/

//  ************************************************************
//  #sql [Ctx] { insert into merchant
//          ( app_seq_num,
//            merc_cntrl_number,
//            merch_bank_number )
//          values
//          ( :appSeqNum,
//            :controlNum,
//            :appSeq.getBankNum() )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 int __sJT_1935 = appSeq.getBankNum();
   String theSqlTS = "insert into merchant\n        ( app_seq_num,\n          merc_cntrl_number,\n          merch_bank_number )\n        values\n        (  :1 ,\n           :2 ,\n           :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.app.AppBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,controlNum);
   __sJT_st.setInt(3,__sJT_1935);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:609^7*/

      // set the appSeqNum in appSeq
      appSeq.setAppSeqNum(appSeqNum);

      // do new app creation notification
      (new AppNotifyBean(Ctx)).notifyStatus(appSeqNum,
        mesConstants.APP_STATUS_INCOMPLETE,appSeq.getAppType());
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::createNewApp(): "
        + e.toString());
      logEntry("createNewApp()",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*
  ** protected boolean submitAppData()
  **
  ** Should be overriden by child classes.  This needs to store all page
  ** specific data.
  */
  protected boolean submitAppData()
  {
    return( true );
  }
  
  /*
  ** public boolean storeData()
  **
  ** publicly accessible method to persist app data
  */
  public boolean storeData()
  {
    return( submitAppData() );
  }

  /*
  ** protected boolean autoSubmit()
  **
  ** If there is no appSeqNum, a new app is created.  Then submitAppData()
  ** is called (assumed to be overriden by child class).
  **
  ** RETURNS: result of submitAppData(), true if successful, else false.
  */
  protected boolean autoSubmit()
  {
    // if appSeqNum isn't defined then create a new app now
    long appSeqNum = fields.getField("appSeqNum").asLong();
    if (appSeqNum == 0L)
    {
      createNewApp(request);
    }
    
    // store app data in the database
    return submitAppData();
  }
  
  /*
  ** protected boolean loadAppData()
  **
  ** Should be overriden by child classes.  This needs to load all page
  ** specific data.
  */
  protected boolean loadAppData()
  {
    return true;
  }

  /*
  ** protected boolean autoLoad()
  **
  ** Calls loadAppData().  This is assumed to be overriden by a child class.
  **
  ** RETURNS: result of loadAppData(), true if successful, else false.
  */
  protected boolean autoLoad()
  {
    return loadAppData();
  }

  /*
  ** public AppSequence getAppSequence()
  **
  ** RETURNS: appSeq.
  */
  public AppSequence getAppSequence()
  {
    return appSeq;
  }
  
  public void setAppSequence( AppSequence as )
  {
    this.appSeq = as;
  }

  /*
  ** public boolean isAppComplete()
  **
  ** RETURNS: true if at least one incomplete screen exists, else false.
  */
  public boolean isAppComplete()
  {
    try
    {
      return (appSeq.getFirstIncompleteScreen() == null);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::isAppComplete(): "
        + e.toString());
      logEntry("isAppComplete()",e.toString());
    }
    return false;
  }
  
  /*
  ** public boolean isNewApp()
  **
  ** RETURNS: true if the app has not been created in the database yet.
  */
  public boolean isNewApp()
  {
    // return true if app seq num has not been set 
    return (fields.getField("appSeqNum").asInteger() == 0L);
  }
  
  public boolean isUsingProductType( int productType )
  {
    return( isUsingProductType( new int[] { productType } ) );
  }
  
  public boolean isUsingProductType( int[] productTypes )
  {
    int           productType = -1;
    boolean       retVal      = false;
    
    try
    {
      connect();

      if ( !isNewApp() )
      {
        long appSeqNum = fields.getField("appSeqNum").asInteger();
        /*@lineinfo:generated-code*//*@lineinfo:758^9*/

//  ************************************************************
//  #sql [Ctx] { select  pc.pos_type         
//                    
//            from    merch_pos     mp,
//                    pos_category  pc
//            where   mp.app_seq_num = :appSeqNum and
//                    pc.pos_code = mp.pos_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pc.pos_type         \n                   \n          from    merch_pos     mp,\n                  pos_category  pc\n          where   mp.app_seq_num =  :1  and\n                  pc.pos_code = mp.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.AppBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   productType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:766^9*/
                
        for ( int i = 0; i < productTypes.length; ++i )
        {
          if ( productTypes[i] == productType )
          {
            retVal = true;
            break;
          }
        }
      }        
    }
    catch(Exception e)
    {
      logEntry("isUsingProduct()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  /*
  ** public boolean userCanSubmit()
  **
  ** Determines if the user viewing an app page can submit updates.  This is
  ** controlled by whether or not a submit button is placed on the form.
  ** If the application is not complete (merchant.merch_credit_status is
  ** checked for this) or the user has the right to edit applications then
  ** the user is allowed to submit.
  **
  ** RETURNS: true if app is incomplete or user has submit rights.
  */
  public boolean userCanSubmit()
  {
    boolean canSubmit = false;
    try
    {
      connect();
      
      int creditStatus = 0;
      
      /*@lineinfo:generated-code*//*@lineinfo:809^7*/

//  ************************************************************
//  #sql [Ctx] { select  merch_credit_status
//          
//          from    merchant
//          where   app_seq_num = :appSeq.getAppSeqNum()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1936 = appSeq.getAppSeqNum();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_credit_status\n         \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.AppBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1936);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   creditStatus = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:815^7*/
      
      canSubmit = (creditStatus == 0 ||
                   user.hasRight(MesUsers.RIGHT_APPLICATION_EDIT));
                   
      if( ! canSubmit && user.hasRight(MesUsers.RIGHT_CLIENT_REVIEW_QUEUES))
      {
        // if app is in a client review queue then it's ok for this user to edit it
        int recCount = 0;
        /*@lineinfo:generated-code*//*@lineinfo:824^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(id)
//            
//            from    q_data
//            where   id = :appSeq.getAppSeqNum() and
//                    type in 
//                    (
//                      :MesQueues.Q_CLIENT_REVIEW_NEW,
//                      :MesQueues.Q_CLIENT_REVIEW_PEND
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1937 = appSeq.getAppSeqNum();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(id)\n           \n          from    q_data\n          where   id =  :1  and\n                  type in \n                  (\n                     :2 ,\n                     :3 \n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.AppBase",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_1937);
   __sJT_st.setInt(2,MesQueues.Q_CLIENT_REVIEW_NEW);
   __sJT_st.setInt(3,MesQueues.Q_CLIENT_REVIEW_PEND);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:835^9*/
        
        canSubmit = (recCount > 0);
      }
    }
    catch (Exception e)
    {
      canSubmit = true;
    }
    finally
    {
      cleanUp();
    }
    return canSubmit;
  }
    
  /*************************************************************************
  **
  **   Warnings
  **
  **************************************************************************/

  protected Vector warnings = new Vector();

  /*
  ** public class WarningField extends Field
  **
  ** A field class that, when rendered, encodes any warning codes in the bean's
  ** warnings vector into an html hidden field.  When the field is set with posted
  ** data, the warning id's are decoded and loaded back into the bean's warnings
  ** vector.
  */
  public class WarningField extends Field
  {
    /*
    ** public WarningField()
    **
    ** Constructor.
    **
    ** Always named warnings.
    */
    public WarningField()
    {
      super("warnings","Warning codes",0,0,true);
    }

    /*
    ** protected String processData(String rawData)
    **
    ** Decodes warning id's, creates Warning objects and adds them to the warnings
    ** vector.
    **
    ** RETURNS: rawData String.
    */
    protected String processData(String rawData)
    {
      StringTokenizer tok = new StringTokenizer(rawData," ");
      while (tok.hasMoreTokens())
      {
        String idStr = tok.nextToken();
        try
        {
          int warningId = Integer.parseInt(idStr);
          addWarning(new Warning(warningId));
        }
        catch (NumberFormatException ne) {}
      }
      return rawData;
    }

    /*
    ** protected String renderHtmlField()
    **
    ** Renders a hidden field with a value containing all warning id's separated
    ** by spaces.
    **
    ** RETURNS: a String containing the html representing a hidden html form field.
    */
    protected String renderHtmlField()
    {
      StringBuffer html = new StringBuffer();
      html.append("<input type=\"hidden\" ");
      html.append("name=\"" + fname + "\" ");
      html.append("value=\"");
      for (Iterator i = warnings.iterator(); i.hasNext();)
      {
        Warning warning = (Warning)i.next();
        html.append(Integer.toString(warning.getWarningId()));
        if (i.hasNext())
        {
          html.append(" ");
        }
      }
      html.append("\" ");
      html.append(">");
      return html.toString();
    }
  }

  /*
  ** public class Warning
  **
  ** Basically just a wrapper around an int warning id.  These objects are placed
  ** in the warnings vector and represent a warning about the validity of some
  ** element of the application.
  */
  public class Warning
  {
    private int warningId;

    public Warning(int warningId)
    {
      this.warningId = warningId;
    }

    public int getWarningId()
    {
      return warningId;
    }

    public boolean equals(Object o)
    {
      return (o instanceof Warning && ((Warning)o).getWarningId() == warningId);
    }
  }

  /*
  ** public void addWarning(int warningId)
  **
  ** Adds a warning to the warning vector using the warningId.
  */
  public void addWarning(int warningId)
  {
    warnings.add(new Warning(warningId));
  }

  /*
  ** public void addWarning(Warning warning)
  **
  ** Adds a warning to the warning vector.
  */
  public void addWarning(Warning warning)
  {
    warnings.add(warning);
  }

  /*
  ** public boolean hasWarning(Warning warning)
  **
  ** RETURNS: true if the warning is present in the warnings vector.
  */
  public boolean hasWarning(Warning warning)
  {
    for (Iterator i = warnings.iterator(); i.hasNext();)
    {
      Warning w = (Warning)i.next();
      if (w.equals(warning))
      {
        return true;
      }
    }
    return false;
  }

  /*
  ** public boolean hasWarning(int warningId)
  **
  ** RETURNS: true if warning with warningId is present in the warnings vector.
  */
  public boolean hasWarning(int warningId)
  {
    for (Iterator i = warnings.iterator(); i.hasNext();)
    {
      Warning warning = (Warning)i.next();
      if (warning.getWarningId() == warningId)
      {
        return true;
      }
    }
    return false;
  }

  /*
  ** public void removeWarning(int warningId)
  **
  ** Removes warning with warningId if present from the warnings vector.
  */
  public void removeWarning(int warningId)
  {
    for (Iterator i = warnings.iterator(); i.hasNext();)
    {
      Warning warning = (Warning)i.next();
      if (warning.getWarningId() == warningId)
      {
        i.remove();
        return;
      }
    }
  }

  /*************************************************************************
  **
  **   Utility Methods
  **
  **************************************************************************/

  /*
  ** public int countDigits(String numStr)
  **
  ** Counts number of numeric digits present in a string.
  **
  ** RETURNS: the number of numeric digits.
  */
  public int countDigits(String numStr)
  {
    int result = 0;
    for (int i = 0; i < numStr.length(); ++i)
    {
      if(Character.isDigit(numStr.charAt(i)))
      {
        ++result;
      }
    }
    return result;
  }

  // pos type (set on page 1) determines if certain fee options are present
  protected int posType = -1;

  /*
  ** protected boolean loadPosType()
  **
  ** Loads the pos type from the pos_category/merch_pos.  This is an option set
  ** on page 1 of the app.
  **
  ** RETURNS: true if load successful, else false.
  */
  protected boolean loadPosType(long appSeqNum)
  {
    boolean loadOk = false;
    ResultSetIterator it = null;
    ResultSet         rs = null;
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:1081^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  pc.pos_type   pos_type
//          from    pos_category  pc,
//                  merch_pos     mp
//          where   mp.app_seq_num = :appSeqNum
//                  and pc.pos_code = mp.pos_code
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  pc.pos_type   pos_type\n        from    pos_category  pc,\n                merch_pos     mp\n        where   mp.app_seq_num =  :1 \n                and pc.pos_code = mp.pos_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.AppBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.app.AppBase",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1088^7*/
      rs = it.getResultSet();
      if (rs.next())
      {
        posType = rs.getInt("pos_type");
        loadOk = true;
      }
    }
    catch (Exception e)
    {
      logEntry("loadPosType()",e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
    return loadOk;
  }

  /*
  ** protected int getPosType()
  **
  ** Loads pos type if not loaded already.
  **
  ** RETURN: loaded pos type.
  */
  protected int getPosType()
  {
    long appSeqNum = fields.getField("appSeqNum").asLong();
    if (posType == -1 && appSeqNum != 0L)
    {
      loadPosType(appSeqNum);
    }
    return posType;
  }

  /*
  ** protected boolean isPosType(int checkPosType)
  **
  ** Checks the pos type given against the app's pos type.
  **
  ** RETURNS: true if app's pos type matches the given pos type.
  */
  protected boolean isPosType(int checkPosType)
  {
    return checkPosType == getPosType();
  }
}/*@lineinfo:generated-code*/