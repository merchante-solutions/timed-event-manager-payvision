/*@lineinfo:filename=AppSequence*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/AppSequence.sqlj $

  AppSequence
  
  Wraps the forms Sequence class to provide application specific sequence
  functionality.  This includes the creation of new applications within the
  database.  AppSequence uses appType to determine the seqId used by parent
  class Sequence.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-07 15:59:41 -0700 (Mon, 07 Jul 2008) $
  Version            : $Revision: 15046 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app;

import java.sql.ResultSet;
import com.mes.forms.Sequence;
import com.mes.forms.SequenceException;
import com.mes.forms.SequenceScreen;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class AppSequence extends Sequence
{
  private int     appType             = ID_NOT_SET;
  private String  appSourceType       = "--";
  private int     bankNum             = ID_NOT_SET;
  private long    controlNum          = ID_NOT_SET;
  private long    merchNum            = ID_NOT_SET;
  private String  merchName           = "--";
  
  private String  appTypeDescription  = "--";
  
  private String  sicCode             = "";
  private String  investigator        = "";
  private String  metTable            = "";
  private String  creditScore         = "";
  private String  tier2               = "N";
  private String  matchData           = "";
  private boolean submitPartial       = false;
  
  public AppSequence()
  {
  }
  
  public AppSequence(DefaultContext defCtx)
  {
    super(defCtx);
  }
  
  public void setAppType(int appType, boolean submitPartial) 
    throws SequenceException
  {
    this.submitPartial = submitPartial;
    
    setAppType(appType);
  }
  
  public boolean isAppComplete(long appSeqNum)
  {
    int recCount = 0;
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:81^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(sp.screen_pk)
//          
//          from    application app,
//                  screen_progress sp,
//                  (
//                    select  power(2,max(ss.screen_id))-1  cur_progress
//                    from    screen_sequence ss,
//                            application app
//                    where   app.app_seq_num = :appSeqNum
//                            and app.screen_sequence_id = ss.screen_sequence_id
//                  ) prog
//          where   app.app_seq_num = :appSeqNum
//                  and app.screen_sequence_id = sp.screen_sequence_id
//                  and app.app_seq_num = sp.screen_pk
//                  and sp.screen_complete = prog.cur_progress
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(sp.screen_pk)\n         \n        from    application app,\n                screen_progress sp,\n                (\n                  select  power(2,max(ss.screen_id))-1  cur_progress\n                  from    screen_sequence ss,\n                          application app\n                  where   app.app_seq_num =  :1 \n                          and app.screen_sequence_id = ss.screen_sequence_id\n                ) prog\n        where   app.app_seq_num =  :2 \n                and app.screen_sequence_id = sp.screen_sequence_id\n                and app.app_seq_num = sp.screen_pk\n                and sp.screen_complete = prog.cur_progress";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.AppSequence",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setLong(2,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:98^7*/
    }
    catch(Exception e)
    {
    }
    finally
    {
      cleanUp();
    }
    
    return( recCount > 0 );
  }
  
  /*
  ** public void setAppType(int appType) throws SequenceException
  **
  ** Sets the app type.  Sets the seq id, app source type and bank num, looking 
  ** them up in the org_app table using the app type.
  */
  public void setAppType(int appType) throws SequenceException
  {
    // store the app type
    this.appType = appType;
    
    // load the app source type and seq id
    String            appSourceType = "";
    int               seqId         = ID_NOT_SET;
    ResultSetIterator it            = null;
    ResultSet         rs            = null;
    boolean           wasStale      = false;
    try
    {
      connect();

      if( submitPartial )      
      {
        /*@lineinfo:generated-code*//*@lineinfo:134^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.screen_sequence_id + decode(nvl(vcp.submit_partial,'N'),'Y',8000,0) screen_sequence_id,
//                    o.appsrctype_code,
//                    o.app_bank_number,
//                    at.app_description
//            from    org_app   o,
//                    app_type  at,
//                    vapp_channel_pricing vcp
//            where   o.app_type = :appType
//                    and o.app_type = at.app_type_code and
//                    o.app_type = vcp.app_type(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.screen_sequence_id + decode(nvl(vcp.submit_partial,'N'),'Y',8000,0) screen_sequence_id,\n                  o.appsrctype_code,\n                  o.app_bank_number,\n                  at.app_description\n          from    org_app   o,\n                  app_type  at,\n                  vapp_channel_pricing vcp\n          where   o.app_type =  :1 \n                  and o.app_type = at.app_type_code and\n                  o.app_type = vcp.app_type(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.AppSequence",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.AppSequence",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:146^9*/
      }
      else
      {
        /*@lineinfo:generated-code*//*@lineinfo:150^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  o.screen_sequence_id,
//                    o.appsrctype_code,
//                    o.app_bank_number,
//                    at.app_description
//            from    org_app   o,
//                    app_type  at
//            where   o.app_type = :appType
//                    and o.app_type = at.app_type_code
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  o.screen_sequence_id,\n                  o.appsrctype_code,\n                  o.app_bank_number,\n                  at.app_description\n          from    org_app   o,\n                  app_type  at\n          where   o.app_type =  :1 \n                  and o.app_type = at.app_type_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.AppSequence",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,appType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.AppSequence",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:160^9*/
      }
      rs = it.getResultSet();
      
      if (rs.next())
      {
        seqId = rs.getInt("screen_sequence_id");
        appSourceType = rs.getString("appsrctype_code");
        bankNum = rs.getInt("app_bank_number");
        appTypeDescription = rs.getString("app_description");
      }
    }
    catch (Exception e)
    {
      System.out.println("getSeqId(): " + e.toString());
      logEntry("getSeqid()",e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
    
    // throw exception if no valid seq id was obtained
    if (seqId == ID_NOT_SET)
    {
      throw new SequenceException("getSeqId(appType=" + appType + "): "
        + "no seq id found");
    }
    
    // store the seq id and the source type string
    setSeqId(seqId);
    this.appSourceType = appSourceType;
    this.bankNum = bankNum;
  }
  
  public int getAppType()
  {
    return appType;
  }
  
  public String getAppTypeDescription()
  {
    return appTypeDescription;
  }
  
  /*
  ** public long getAppSeqNum() throws SequenceException
  **
  ** RETURNS: inst id.
  */
  public long getAppSeqNum() throws SequenceException
  {
    return super.getInstId();
  }

  /*
  ** public void setAppSeqNum(long appSeqNum) throws SequenceException
  **
  ** Sets inst id in parent.  Loads some application specific fields.
  */
  public void setAppSeqNum(long appSeqNum) throws SequenceException
  {
    super.setInstId(appSeqNum);
    
    
    ResultSetIterator it = null;
    ResultSet         rs = null;
    boolean           wasStale = false;

    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }

      // load app info
      int appCount = 0;
      /*@lineinfo:generated-code*//*@lineinfo:241^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(app_seq_num)
//          
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(app_seq_num)\n         \n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.app.AppSequence",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:247^7*/
      
      if(appCount > 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:251^9*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(merch_number, -1),
//                    nvl(merc_cntrl_number, -1),
//                    nvl(merch_business_name,'--')
//            
//            from    merchant
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(merch_number, -1),\n                  nvl(merc_cntrl_number, -1),\n                  nvl(merch_business_name,'--')\n           \n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.AppSequence",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 3) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(3,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   controlNum = __sJT_rs.getLong(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   merchName = (String)__sJT_rs.getString(3);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:261^9*/
      
        // load credit info
        /*@lineinfo:generated-code*//*@lineinfo:264^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.sic_code                    sic_code,
//                    m.merch_met_table_number      met_table,
//                    m.merch_invg_code             inv_code,
//                    nvl(acad.tier_2,'--')         tier2,
//                    nvl(mr.response_action,'--')  match_resp
//            from    merchant                    m,
//                    app_credit_additional_data  acad,
//                    match_requests              mr
//            where   m.app_seq_num = :appSeqNum
//                    and m.app_seq_num = acad.app_seq_num(+)
//                    and m.app_seq_num = mr.app_seq_num(+)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.sic_code                    sic_code,\n                  m.merch_met_table_number      met_table,\n                  m.merch_invg_code             inv_code,\n                  nvl(acad.tier_2,'--')         tier2,\n                  nvl(mr.response_action,'--')  match_resp\n          from    merchant                    m,\n                  app_credit_additional_data  acad,\n                  match_requests              mr\n          where   m.app_seq_num =  :1 \n                  and m.app_seq_num = acad.app_seq_num(+)\n                  and m.app_seq_num = mr.app_seq_num(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.AppSequence",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.app.AppSequence",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:277^9*/
        rs = it.getResultSet();
        if (rs.next())
        {
          sicCode      = rs.getString("sic_code");
          investigator = rs.getString("inv_code");
          metTable     = rs.getString("met_table");
          tier2        = rs.getString("tier2");
          matchData    = rs.getString("match_resp");
        }
        it.close();
      
        // load credit scores
        StringBuffer scores = new StringBuffer("");
        /*@lineinfo:generated-code*//*@lineinfo:291^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode(score,null,'NONE',to_char(score)) score
//            from    credit_scores
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode(score,null,'NONE',to_char(score)) score\n          from    credit_scores\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.AppSequence",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.app.AppSequence",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:296^9*/
        rs = it.getResultSet();
        int scoreCount = 0;
        while(rs.next())
        {
          ++scoreCount;
          if(scores.length() > 1)
          {
            scores.append(", ");
          }
          scores.append(rs.getString("score"));
        }
        if (scoreCount > 0)
        {
          creditScore = scores.toString();
        }
        else
        {
          creditScore = "--";
        }
      }
    }
    catch (Exception e)
    {
      System.out.println("setAppSeqNum(): " + e.toString());
      logEntry("setAppSeqNum()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      
      if(wasStale)
      {
        cleanUp();
      }
    }
  }
  
  public long getControlNum()
  {
    return controlNum;
  }
  
  public long getMerchNum()
  {
    return merchNum;
  }
  
  public String getMerchName()
  {
    return merchName;
  }
  
  public String getSicCode()
  {
    return sicCode;
  }
  
  public String getInvestigator()
  {
    return investigator;
  }
  
  public String getMetTable()
  {
    return metTable;
  }
  
  public String getCreditScore()
  {
    return creditScore;
  }
  
  public String getTier2()
  {
    return tier2;
  }
  
  public String getMatchData()
  {
    return matchData;
  }
  
  /*
  ** public String getAppSourceType()
  **
  ** RETURNS: appSourceType.
  */
  public String getAppSourceType()
  {
    return appSourceType;
  }
  
  /*
  ** public void setAppSourceType(String appSourceType)
  **
  ** Sets app source type.
  */
  public void setAppSourceType(String appSourceType)
  {
    this.appSourceType = appSourceType;
  }
  
  public int getBankNum()
  {
    return bankNum;
  }
  
  public void setBankNum(int bankNum)
  {
    this.bankNum = bankNum;
  }
  
  /*
  ** public boolean hasNextScreen()
  **
  ** RETURNS: true if there is a next screen in the sequence.
  */
  public boolean hasNextScreen()
  {
    try
    {
      return (getNextScreen() != null);
    }
    catch (SequenceException se) {}
    
    return false;
  }
  
  /*
  ** public boolean hasPrevScreen()
  **
  ** RETURNS: true if there is a previous screen in the sequence.
  */
  public boolean hasPrevScreen()
  {
    try
    {
      return (getPrevScreen() != null);
    }
    catch (SequenceException se) {}
    
    return false;
  }
  
  /*
  ** public String getNextUrl()
  **
  ** Generates an url suitable for use in a link to the next screen in the
  ** sequence, if it exists.  Any identifiers that are needed are included
  ** in the query string.
  **
  ** RETURNS: String containing the next screen url, or null if none found.
  */
  public String getNextUrl()
  {
    try
    {
      SequenceScreen nextScreen = getNextScreen();
      if (nextScreen != null)
      {
        return nextScreen.getJsp() + "?appSeqNum=" + getInstId();
      }
    }
    catch (SequenceException se) {}
    
    return null;
  }

  /*
  ** public String getPrevUrl()
  **
  ** Generates an url suitable for use in a link to the previous screen in the
  ** sequence, if it exists.  Any identifiers that are needed are included
  ** in the query string.
  **
  ** RETURNS: String containing the previous screen url, or null if none found.
  */
  public String getPrevUrl()
  {
    try
    {
      SequenceScreen prevScreen = getPrevScreen();
      if (prevScreen != null)
      {
        return prevScreen.getJsp() + "?appSeqNum=" + getInstId();
      }
    }
    catch (SequenceException se) {}
    
    return null;
  }
}/*@lineinfo:generated-code*/