/*@lineinfo:filename=VaBusiness*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/verisign/VaBusiness.sqlj $

  Description:  
  
  VaBusiness
  
  Data bean used by page 1 of the verisign virtual app.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/11/04 2:35p $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.verisign;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.CaseInsensitiveDropDownField;
import com.mes.forms.DropDownField;
import com.mes.forms.EmailField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.NumberField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TextareaField;
import com.mes.forms.WebAddressField;
import com.mes.forms.ZipField;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class VaBusiness extends VaBase
{
  public VaBusiness()
  {
  }
  
  public VaBusiness(DefaultContext defCtx)
  {
    super(defCtx);
  }
  
  /*
  ** Custom fields
  */
  
  protected class YearField extends NumberField
  {
    public YearField(String fname, boolean nullAllowed)
    {
      super(fname,4,4,nullAllowed,0);
      addValidation(new YearValidation("Invalid year",nullAllowed));
    }

    protected String processData(String rawData)
    {
      if (rawData == null || rawData.equals("0"))
      {
        rawData = "";
      }
      return rawData;
    }
  }

  /*
  ** Drop down tables
  */
  
  protected class MonthTable extends DropDownTable
  {
    public MonthTable()
    {
      addElement("","select");
      addElement("1","Jan");
      addElement("2","Feb");
      addElement("3","Mar");
      addElement("4","Apr");
      addElement("5","May");
      addElement("6","Jun");
      addElement("7","Jul");
      addElement("8","Aug");
      addElement("9","Sep");
      addElement("10","Oct");
      addElement("11","Nov");
      addElement("12","Dec");
    }
  }
  
  protected class BusinessTypeTable extends DropDownTable
  {
    public BusinessTypeTable()
    {
      addElement("","select one");
      addElement("1","Sole Proprietorship");
      addElement("2","Corporation");
      addElement("3","Partnership");
      addElement("4","Medical or Legal Corporation");
      addElement("5","Association/Estate/Trust");
      addElement("6","Tax Exempt");
      addElement("7","Government");
      addElement("8","Limited Liability Company");
    }
  }
  
  protected class ExpandedIndustryTable extends DropDownTable
  {
    public ExpandedIndustryTable()
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      boolean           wasStale = false;
    
      try
      {
        if(isConnectionStale())
        {
          connect();
          wasStale = true;
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:131^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    company_type_code,
//                      company_type_desc
//            from      visa_company_types
//            order by  company_type_desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    company_type_code,\n                    company_type_desc\n          from      visa_company_types\n          order by  company_type_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.verisign.VaBusiness",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.verisign.VaBusiness",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:137^9*/
        rs = it.getResultSet();
        
        addElement("","select one");
        
        while (rs.next())
        {
          addElement(rs);
        }
      }
      catch (Exception e)
      {
        vappLogEntry("ExpandedIndustryTable()",e.toString());
      }
      finally
      {
        try { it.close(); } catch (Exception e) {}
        if(wasStale)
        {
          cleanUp();
        }
      }
    }
  }
        
  public class LocationYearsTable extends DropDownTable
  {
    public LocationYearsTable()
    {
      addElement("","select one");
      addElement("0","< 1 year");
      addElement("1","1 year");
      for (int i = 2; i < 26; ++i)
      {
        String yearNum = Integer.toString(i);
        addElement(yearNum,yearNum + " years");
      }
      addElement("26","> 25 years");
    }
  }
  
  public class CountryDropDownTable extends DropDownTable
  {
    public CountryDropDownTable()
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      boolean           wasStale = true;
    
      try
      {
        if(isConnectionStale())
        {
          connect();
          wasStale = true;
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:194^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  country_code,
//                    country_desc
//            from    country
//            order   by country_desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  country_code,\n                  country_desc\n          from    country\n          order   by country_desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.verisign.VaBusiness",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.verisign.VaBusiness",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:200^9*/
        rs = it.getResultSet();
        
        while (rs.next())
        {
          addElement(rs);
        }
      }
      catch (Exception e)
      {
        vappLogEntry("CountryDropDownTable()",e.toString());
      }
      finally
      {
        try { it.close(); } catch (Exception e) {}
        
        if(wasStale)
        {
          cleanUp();
        }
      }
    }
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // 1 - business info
      FieldGroup gBusInfo = new FieldGroup("gBusInfo");
    
      gBusInfo.add(new Field            ("businessName",      "Business Name (DBA)",25,25,false));
      gBusInfo.add(new Field            ("businessLegalName", "Business Legal Name",50,25,false));
      gBusInfo.add(new NumericOnlyField ("taxpayerId",        "Federal Taxpayer ID",9,9,false));
      gBusInfo.add(new DropDownField    ("establishedMonth",  "Business Established Date",new MonthTable(),false));
      gBusInfo.add(new YearField        ("establishedYear",   false));
      gBusInfo.add(new EasyPhoneField   ("businessPhone",     false));
      gBusInfo.add(new EasyPhoneField   ("businessFax",       true));
      gBusInfo.add(new Field            ("businessAddress1",  "Business Address Line 1",32,25,false));
      gBusInfo.add(new Field            ("businessAddress2",  "Business Address Line 2",32,25,true));
      gBusInfo.add(new Field            ("businessCity",      "Business City",25,25,false));
      gBusInfo.add(new CaseInsensitiveDropDownField    ("businessState",     "Business State",new StateDropDownTable(),false));
      gBusInfo.add(new ZipField         ("businessZip",       "Business Zip",false,fields.getField("businessState")));
      gBusInfo.add(new DropDownField    ("businessCountry",   "Business Country",new CountryDropDownTable(),false));
      gBusInfo.add(new EmailField       ("businessEmail",     "Business Email",75, 25, false));
    
      fields.add(gBusInfo);
    
      // 2 - business contact
      FieldGroup gContact = new FieldGroup("gContact");
    
      gContact.add(new Field            ("contactNameFirst",  "Contact First Name",20,25,false));
      gContact.add(new Field            ("contactNameLast",   "Contact Last Name",20,25,false));
      gContact.add(new EasyPhoneField   ("contactPhone",      false));
      gContact.add(new EmailField       ("contactEmail",      75,25,false));
      gContact.add(new Field            ("mailingName",       "Mailing Address Name",50,25,true));
      gContact.add(new Field            ("mailingAddress1",   "Mailing Address Line 1",32,25,true));
      gContact.add(new Field            ("mailingAddress2",   "Mailing Address Line 2",32,25,true));
      gContact.add(new Field            ("mailingCity",       "Mailing City",24,25,true) );
      gContact.add(new CaseInsensitiveDropDownField    ("mailingState",      "Mailing State",new StateDropDownTable(),true));
      gContact.add(new ZipField         ("mailingZip",        "Mailing Zip",true,fields.getField("mailingState")));
    
      fields.add(gContact);

      // 3 - business premises
      FieldGroup gPremises = new FieldGroup("gPremises");

      gPremises.add(new DropDownField   ("businessType",      "Type of Business",new BusinessTypeTable(),false));
      gPremises.add(new DropDownField   ("locationYears",     "Years at Main Loc.",new LocationYearsTable(),false));
      gPremises.add(new DropDownField   ("industryType",      "Industry",new ExpandedIndustryTable(),false));
      gPremises.add(new TextareaField   ("businessDesc",      "Business Description",180,2,90,false));
      gPremises.add(new WebAddressField ("webUrl",            "Business URL",133,25,false));
      gPremises.add(new Field           ("promotionCode",     "Promotion Code",50,25,true));
    
      fields.add(gPremises);
    
      // submit button
      fields.add(new ButtonField("submit","Submit Application Data","Submit"));
    
      // verisign style error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);

      // set the current screen id
      fields.setData("curScreenId","1");
    }
    catch (Exception e)
    {
      System.out.println(getClass().getName() + "::createFields(): " + e.toString());
      vappLogEntry("createFields()",e.toString());
    }
  }
  
  /*
  ** protected boolean autoSubmit()
  **
  ** Submits app data.
  **
  ** RETURNS: true if submit ok, else false.
  */
  protected boolean autoSubmit()
  {
    boolean submitOk  = false;
    boolean wasStale  = false;
    try
    {
      if(isConnectionStale())
      {
        wasStale = true;
        connect();
      }
      
      try
      {
        // business address
        storeAddressData(mesConstants.ADDR_TYPE_BUSINESS,
                          new Field[]{  fields.getField("businessAddress1"),
                                        fields.getField("businessAddress2"),
                                        fields.getField("businessCity"),
                                        fields.getField("businessState"),
                                        fields.getField("businessZip"),
                                        fields.getField("businessCountry"),
                                        fields.getField("businessPhone"),
                                        fields.getField("businessFax") } );
      }
      catch(Exception e)
      {
        vappLogEntry("autoSubmit(storeBusinessAddr)", e.toString());
      }
    
      try
      {
        // mailing address
        storeAddressData(mesConstants.ADDR_TYPE_MAILING,
                          new Field[]{  fields.getField("mailingAddress1"),
                                        fields.getField("mailingAddress2"),
                                        fields.getField("mailingCity"),
                                        fields.getField("mailingState"),
                                        fields.getField("mailingZip"),
                                        null,
                                        null,
                                        null } );
      }
      catch(Exception e)
      {
        vappLogEntry("autoSubmit(storeMailingAddr)", e.toString());
      }
                                      
      long appSeqNum = fields.getField("appSeqNum").asInteger();
      
      try
      {                                      
        // merchant info
        /*@lineinfo:generated-code*//*@lineinfo:354^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     merch_business_name         = :fields.getData("businessName"),
//                    merch_legal_name            = :fields.getData("businessLegalName"),
//                    merch_federal_tax_id        = :fields.getData("taxpayerId"),
//                    merch_business_establ_month = :fields.getData("establishedMonth"),
//                    merch_business_establ_year  = :fields.getData("establishedYear"),
//                    merch_mailing_name          = :fields.getData("mailingName"),
//                    bustype_code                = :fields.getData("businessType"),
//                    merch_years_at_loc          = :fields.getData("locationYears"),
//                    industype_code              = :fields.getData("industryType"),
//                    merch_busgoodserv_descr     = :fields.getData("businessDesc"),
//                    merch_web_url               = :fields.getData("webUrl"),
//                    merch_email_address         = :fields.getData("businessEmail"),
//                    merch_application_type      = 1,
//                    merch_num_of_locations      = 1,
//                    loctype_code                = 2,
//                    promotion_code              = :fields.getData("promotionCode"),
//                    merch_edc_flag              = 'N'
//            where   app_seq_num                 = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3202 = fields.getData("businessName");
 String __sJT_3203 = fields.getData("businessLegalName");
 String __sJT_3204 = fields.getData("taxpayerId");
 String __sJT_3205 = fields.getData("establishedMonth");
 String __sJT_3206 = fields.getData("establishedYear");
 String __sJT_3207 = fields.getData("mailingName");
 String __sJT_3208 = fields.getData("businessType");
 String __sJT_3209 = fields.getData("locationYears");
 String __sJT_3210 = fields.getData("industryType");
 String __sJT_3211 = fields.getData("businessDesc");
 String __sJT_3212 = fields.getData("webUrl");
 String __sJT_3213 = fields.getData("businessEmail");
 String __sJT_3214 = fields.getData("promotionCode");
   String theSqlTS = "update  merchant\n          set     merch_business_name         =  :1 ,\n                  merch_legal_name            =  :2 ,\n                  merch_federal_tax_id        =  :3 ,\n                  merch_business_establ_month =  :4 ,\n                  merch_business_establ_year  =  :5 ,\n                  merch_mailing_name          =  :6 ,\n                  bustype_code                =  :7 ,\n                  merch_years_at_loc          =  :8 ,\n                  industype_code              =  :9 ,\n                  merch_busgoodserv_descr     =  :10 ,\n                  merch_web_url               =  :11 ,\n                  merch_email_address         =  :12 ,\n                  merch_application_type      = 1,\n                  merch_num_of_locations      = 1,\n                  loctype_code                = 2,\n                  promotion_code              =  :13 ,\n                  merch_edc_flag              = 'N'\n          where   app_seq_num                 =  :14";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.verisign.VaBusiness",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3202);
   __sJT_st.setString(2,__sJT_3203);
   __sJT_st.setString(3,__sJT_3204);
   __sJT_st.setString(4,__sJT_3205);
   __sJT_st.setString(5,__sJT_3206);
   __sJT_st.setString(6,__sJT_3207);
   __sJT_st.setString(7,__sJT_3208);
   __sJT_st.setString(8,__sJT_3209);
   __sJT_st.setString(9,__sJT_3210);
   __sJT_st.setString(10,__sJT_3211);
   __sJT_st.setString(11,__sJT_3212);
   __sJT_st.setString(12,__sJT_3213);
   __sJT_st.setString(13,__sJT_3214);
   __sJT_st.setLong(14,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:375^9*/
      }
      catch(Exception e)
      {
        vappLogEntry("autoSubmit(updateMerchant)", e.toString());
      }
                
      try
      {
        // contact data
        /*@lineinfo:generated-code*//*@lineinfo:385^9*/

//  ************************************************************
//  #sql [Ctx] { delete 
//            from    merchcontact
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n          from    merchcontact\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.verisign.VaBusiness",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:390^9*/
      
        /*@lineinfo:generated-code*//*@lineinfo:392^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchcontact
//            ( app_seq_num,
//              merchcont_prim_first_name,
//              merchcont_prim_last_name,
//              merchcont_prim_phone,
//              merchcont_prim_email )
//            values
//            ( :appSeqNum,
//              :fields.getData("contactNameFirst"),
//              :fields.getData("contactNameLast"),
//              :fields.getData("contactPhone"),
//              :fields.getData("contactEmail") )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3215 = fields.getData("contactNameFirst");
 String __sJT_3216 = fields.getData("contactNameLast");
 String __sJT_3217 = fields.getData("contactPhone");
 String __sJT_3218 = fields.getData("contactEmail");
   String theSqlTS = "insert into merchcontact\n          ( app_seq_num,\n            merchcont_prim_first_name,\n            merchcont_prim_last_name,\n            merchcont_prim_phone,\n            merchcont_prim_email )\n          values\n          (  :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.app.verisign.VaBusiness",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_3215);
   __sJT_st.setString(3,__sJT_3216);
   __sJT_st.setString(4,__sJT_3217);
   __sJT_st.setString(5,__sJT_3218);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:406^9*/
      }
      catch(Exception e)
      {
        vappLogEntry("autoSubmit(contact data)", e.toString());
      }
      
      try
      {
        // merch_pos (internet gateway) data
        /*@lineinfo:generated-code*//*@lineinfo:416^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    merch_pos
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    merch_pos\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.verisign.VaBusiness",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:421^9*/
      
        /*@lineinfo:generated-code*//*@lineinfo:423^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merch_pos
//            ( app_seq_num,
//              pos_code,
//              pos_param )
//            values
//            ( :appSeqNum,
//              :mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK,
//              :fields.getData("webUrl") )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3219 = fields.getData("webUrl");
   String theSqlTS = "insert into merch_pos\n          ( app_seq_num,\n            pos_code,\n            pos_param )\n          values\n          (  :1 ,\n             :2 ,\n             :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.app.verisign.VaBusiness",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MPOS_VERISIGN_PAYFLOW_LINK);
   __sJT_st.setString(3,__sJT_3219);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:433^9*/
      }
      catch(Exception e)
      {
        vappLogEntry("autoSubmit(merch_pos)", e.toString());
      }
      
      submitOk = true;
    }
    catch (Exception e)
    {
      System.out.println(getClass().getName() + "::autoSubmit(): " + e.toString());
      vappLogEntry("autoSubmit()",e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }

    return submitOk;
  }
  
  /*
  ** protected boolean autoLoad()
  **
  ** Loads app data.
  **
  ** RETURNS: true if successful, else false.
  */
  protected boolean autoLoad()
  {
    boolean           loadOk  = false;
    ResultSetIterator it      = null;
    boolean           wasStale  = false;
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }

      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      // merchant info
      /*@lineinfo:generated-code*//*@lineinfo:481^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_business_name         as business_name,
//                  merch_legal_name            as business_legal_name,
//                  merch_federal_tax_id        as taxpayer_id,
//                  merch_business_establ_month as established_month,
//                  merch_business_establ_year  as established_year,
//                  merch_mailing_name          as mailing_name,
//                  bustype_code                as business_type,
//                  merch_years_at_loc          as location_years,
//                  industype_code              as industry_type,
//                  merch_busgoodserv_descr     as business_desc,
//                  merch_web_url               as web_url,
//                  merch_email_address         as business_email,
//                  promotion_code              as promotion_code
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_business_name         as business_name,\n                merch_legal_name            as business_legal_name,\n                merch_federal_tax_id        as taxpayer_id,\n                merch_business_establ_month as established_month,\n                merch_business_establ_year  as established_year,\n                merch_mailing_name          as mailing_name,\n                bustype_code                as business_type,\n                merch_years_at_loc          as location_years,\n                industype_code              as industry_type,\n                merch_busgoodserv_descr     as business_desc,\n                merch_web_url               as web_url,\n                merch_email_address         as business_email,\n                promotion_code              as promotion_code\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.verisign.VaBusiness",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.app.verisign.VaBusiness",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:498^7*/
      setFields(it.getResultSet());
      it.close();
                
      // business address
      /*@lineinfo:generated-code*//*@lineinfo:503^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_phone               as business_phone,
//                  address_fax                 as business_fax,
//                  address_line1               as business_address_1,
//                  address_line2               as business_address_2,
//                  address_city                as business_city,
//                  countrystate_code           as business_state,
//                  address_zip                 as business_zip,
//                  country_code                as business_country
//          from    address
//          where   app_seq_num = :appSeqNum
//                  and addresstype_code = :mesConstants.ADDR_TYPE_BUSINESS
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_phone               as business_phone,\n                address_fax                 as business_fax,\n                address_line1               as business_address_1,\n                address_line2               as business_address_2,\n                address_city                as business_city,\n                countrystate_code           as business_state,\n                address_zip                 as business_zip,\n                country_code                as business_country\n        from    address\n        where   app_seq_num =  :1 \n                and addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.verisign.VaBusiness",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_BUSINESS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.app.verisign.VaBusiness",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:516^7*/
      setFields(it.getResultSet());
      it.close();
        
      // mailing address
      /*@lineinfo:generated-code*//*@lineinfo:521^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1               as mailing_address_1,
//                  address_line2               as mailing_address_2,
//                  address_city                as mailing_city,
//                  countrystate_code           as mailing_state,
//                  address_zip                 as mailing_zip
//          from    address
//          where   app_seq_num = :appSeqNum
//                  and addresstype_code = :mesConstants.ADDR_TYPE_MAILING
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1               as mailing_address_1,\n                address_line2               as mailing_address_2,\n                address_city                as mailing_city,\n                countrystate_code           as mailing_state,\n                address_zip                 as mailing_zip\n        from    address\n        where   app_seq_num =  :1 \n                and addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.verisign.VaBusiness",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_MAILING);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.app.verisign.VaBusiness",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:531^7*/
      setFields(it.getResultSet());
      it.close();

      // contact data
      /*@lineinfo:generated-code*//*@lineinfo:536^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merchcont_prim_first_name   as contact_name_first,
//                  merchcont_prim_last_name    as contact_name_last,
//                  merchcont_prim_phone        as contact_phone,
//                  merchcont_prim_email        as contact_email
//          from    merchcontact
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merchcont_prim_first_name   as contact_name_first,\n                merchcont_prim_last_name    as contact_name_last,\n                merchcont_prim_phone        as contact_phone,\n                merchcont_prim_email        as contact_email\n        from    merchcontact\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.verisign.VaBusiness",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.app.verisign.VaBusiness",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:544^7*/
      setFields(it.getResultSet());
      it.close();
        
      loadOk = true;
    }
    catch (Exception e)
    {
      System.out.println(getClass().getName() + "::autoLoad(): " + e.toString());
      vappLogEntry("autoLoad()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) { }
      if(wasStale)
      {
        cleanUp();
      }
    }

    return loadOk;
  }
  
  public class EasyPhoneField extends Field
  {
    public EasyPhoneField(String fname, boolean nullAllowed)
    {
      super(fname, fname, 13, 13, nullAllowed);
    }
    
    public String processData(String rawData)
    {
      StringBuffer  result = new StringBuffer("");
      try
      {
        for(int i=0; i < rawData.length(); ++i)
        {
          if(Character.isDigit(rawData.charAt(i)))
          {
            result.append(rawData.charAt(i));
          }
        }
      }
      catch(Exception e)
      {
      }
      
      return result.toString();
    }
  }
}/*@lineinfo:generated-code*/