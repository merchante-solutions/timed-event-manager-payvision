/*@lineinfo:filename=VaEnd*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/verisign/VaEnd.sqlj $

  Description:  
  
  VaEnd
  
  Data bean used by page 4 of the verisign virtual app.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 10/01/04 10:49a $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.verisign;

import java.sql.ResultSet;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.HiddenField;
import com.mes.ops.InsertCreditQueue;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class VaEnd extends VaBase
{
  public VaEnd()
  {
  }
  
  public VaEnd(DefaultContext defCtx)
  {
    super(defCtx);
  }
  
  protected void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    fields.add(new HiddenField("agreement"));
    fields.add(new HiddenField("returnUrl"));
    
    // v/mc pricing
    fields.add(new HiddenField("vmcDiscRate"));
    fields.add(new HiddenField("vmcTranFee"));
    fields.add(new HiddenField("vmcDowngradeRate"));
    
    // non-v/mc pricing
    fields.add(new HiddenField("nonVmcTranFee"));
    
    // miscellaneous fees
    fields.add(new HiddenField("setupFee"));
    fields.add(new HiddenField("chargebackFee"));
    fields.add(new HiddenField("monthlyServiceFee"));
    fields.add(new HiddenField("monthlyMinimumFee"));

    // owner information
    fields.add(new HiddenField("owner1Name"));
    fields.add(new HiddenField("owner1Title"));
    fields.add(new HiddenField("owner2Name"));
    fields.add(new HiddenField("owner2Title"));
    
    fields.setData("curScreenId","1");
    fields.setData("vmcDowngradeRate","1.45");
  }
  
  /*
  ** private boolean loadPartnerPricing(int partnerId)
  **
  ** Loads pricing elements from vs_partner_pricing for the given partnerId.
  ** Caller must do database connection and clean up.
  **
  ** RETURN: true if pricing elements loaded, else false.
  */
  private boolean loadPartnerPricing(int partnerId)
  {
    boolean           loadOk  = false;
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:103^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  setup_fee       as setup_fee,
//                  mon_min_fee     as monthly_minimum_fee,
//                  mon_svc_fee     as monthly_service_fee,
//                  vmc_discount    as vmc_disc_rate,
//                  vmc_pertran     as vmc_tran_fee,
//                  te_pertran      as non_vmc_tran_fee,
//                  chargeback_fee  as chargeback_fee
//          from    vs_partner_pricing
//          where   partner_id = :partnerId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  setup_fee       as setup_fee,\n                mon_min_fee     as monthly_minimum_fee,\n                mon_svc_fee     as monthly_service_fee,\n                vmc_discount    as vmc_disc_rate,\n                vmc_pertran     as vmc_tran_fee,\n                te_pertran      as non_vmc_tran_fee,\n                chargeback_fee  as chargeback_fee\n        from    vs_partner_pricing\n        where   partner_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.verisign.VaEnd",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,partnerId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.verisign.VaEnd",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:114^7*/
      rs = it.getResultSet();
      if (rs.next())
      {
        setFields(fields,rs,false);
        loadOk = true;
      }
    }
    catch (Exception e)
    {
      vappLogEntry("loadPartnerPricing()",e.toString());
    }
    finally
    {
      try { rs.close(); } catch (Exception e) {}
      try { it.close(); } catch (Exception e) {}
    }
    return loadOk;
  }
  
  /*
  ** private boolean loadPricing()
  **
  ** Loads pricing elements for a verisign merchant using the partner id found
  ** in vs_linking_table.
  **
  ** RETURNS: true if able to load pricing elements for the partner_id, else false.
  */
  private boolean loadPricing()
  {
    boolean loadOk  = false;
    try
    {
      long appSeqNum  = fields.getField("appSeqNum").asLong();
      int partnerId   = -1;
      
      connect();
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:154^9*/

//  ************************************************************
//  #sql [Ctx] { select  pid
//            
//            from    vs_linking_table
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  pid\n           \n          from    vs_linking_table\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.verisign.VaEnd",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   partnerId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:160^9*/
      }
      catch(java.sql.SQLException se)
      {
      }
      
      loadOk = loadPartnerPricing(partnerId);
      if (!loadOk)
      {
        loadOk = loadPartnerPricing(1026);
      }
    }
    catch (Exception e)
    {
      vappLogEntry("loadPricing()",e.toString());
    }
    finally
    {
      cleanUp();
    }
    return loadOk;
  }
  
  private Vector acceptedCardTypes = new Vector();
  
  /*
  ** private void loadCardsAccepted()
  **
  ** Determines which non-vmc cards are accepted.  The name of each card type
  ** that is accepteed is placed in the acceptedCardTypes vector.
  */
  private void loadCardsAccepted()
  {
    ResultSetIterator it  = null;
    ResultSet         rs  = null;
    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
//      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:201^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code
//          from    merchpayoption
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code\n        from    merchpayoption\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.verisign.VaEnd",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.app.verisign.VaEnd",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:206^7*/
      
      rs = it.getResultSet();
      while (rs.next())
      {
        acceptedCardTypes.add(rs.getInt("cardtype_code"));
      }
    }
    catch (Exception e)
    {
      vappLogEntry("loadPricing()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
//      cleanUp();
    }
  }
  
  /*
  ** protected void submitTranChrg()
  **
  ** Adds transaction pricing info to the database
  */
  protected void submitTranChrg()
  {
    long appSeqNum = fields.getField("appSeqNum").asLong();
    
    try
    {
      // get accepted card types
      loadCardsAccepted();
      
      for(int i=0; i < acceptedCardTypes.size(); ++i)
      {
        int cardType = ((Integer)acceptedCardTypes.elementAt(i)).intValue();
        
        switch(cardType)
        {
          case mesConstants.APP_CT_VISA:
          case mesConstants.APP_CT_MC:
            /*@lineinfo:generated-code*//*@lineinfo:247^13*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//                ( app_seq_num,
//                  cardtype_code,
//                  tranchrg_mmin_chrg,
//                  tranchrg_discrate_type,
//                  tranchrg_disc_rate,
//                  tranchrg_float_disc_flag,
//                  mid_qualification_downgrade,
//                  non_qualification_downgrade )
//                values
//                ( :appSeqNum,
//                  :cardType,
//                  :fields.getData("monthlyMinimumFee"),
//                  :mesConstants.APP_PS_FIXED_RATE,
//                  :fields.getData("vmcDiscRate"),
//                  'N',
//                  .8,
//                  1.65 )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3220 = fields.getData("monthlyMinimumFee");
 String __sJT_3221 = fields.getData("vmcDiscRate");
   String theSqlTS = "insert into tranchrg\n              ( app_seq_num,\n                cardtype_code,\n                tranchrg_mmin_chrg,\n                tranchrg_discrate_type,\n                tranchrg_disc_rate,\n                tranchrg_float_disc_flag,\n                mid_qualification_downgrade,\n                non_qualification_downgrade )\n              values\n              (  :1 ,\n                 :2 ,\n                 :3 ,\n                 :4 ,\n                 :5 ,\n                'N',\n                .8,\n                1.65 )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.verisign.VaEnd",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
   __sJT_st.setString(3,__sJT_3220);
   __sJT_st.setInt(4,mesConstants.APP_PS_FIXED_RATE);
   __sJT_st.setString(5,__sJT_3221);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:267^13*/
            break;
            
          case mesConstants.APP_CT_DINERS_CLUB:
          case mesConstants.APP_CT_DISCOVER:
          case mesConstants.APP_CT_JCB:
          case mesConstants.APP_CT_AMEX:
            /*@lineinfo:generated-code*//*@lineinfo:274^13*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//                (
//                  app_seq_num,
//                  cardtype_code,
//                  tranchrg_per_tran
//                )
//                values 
//                (
//                  :appSeqNum,
//                  :cardType,
//                  :fields.getData("nonVmcTranFee")
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3222 = fields.getData("nonVmcTranFee");
   String theSqlTS = "insert into tranchrg\n              (\n                app_seq_num,\n                cardtype_code,\n                tranchrg_per_tran\n              )\n              values \n              (\n                 :1 ,\n                 :2 ,\n                 :3 \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.app.verisign.VaEnd",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,cardType);
   __sJT_st.setString(3,__sJT_3222);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:288^13*/
            break;
            
        }
      }
      
      // add internet gateway fee which is a card type for some reason
      /*@lineinfo:generated-code*//*@lineinfo:295^7*/

//  ************************************************************
//  #sql [Ctx] { insert into tranchrg
//          ( app_seq_num,
//            cardtype_code,
//            tranchrg_per_tran )
//          values
//          ( :appSeqNum,
//            :mesConstants.APP_CT_INTERNET,
//            :fields.getData("vmcTranFee") )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3223 = fields.getData("vmcTranFee");
   String theSqlTS = "insert into tranchrg\n        ( app_seq_num,\n          cardtype_code,\n          tranchrg_per_tran )\n        values\n        (  :1 ,\n           :2 ,\n           :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.app.verisign.VaEnd",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_CT_INTERNET);
   __sJT_st.setString(3,__sJT_3223);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:305^7*/
      
    }
    catch(Exception e)
    {
      vappLogEntry("submitTranChrg(" + appSeqNum  + ")", e.toString());
    }
  }
  
  /*
  ** protected void submitMiscFees()
  **
  ** Adds miscellaneous fees (setup fee, statement fee, etc) to database.
  */
  protected void submitMiscFees()
  {
    long appSeqNum = fields.getField("appSeqNum").asLong();
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:325^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    miscchrg
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    miscchrg\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.verisign.VaEnd",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:330^7*/
      
      // chargeback fee
      /*@lineinfo:generated-code*//*@lineinfo:333^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          ( app_seq_num,
//            misc_code,
//            misc_chrg_amount )
//          values
//          ( :appSeqNum,
//            :mesConstants.APP_MISC_CHARGE_CHARGEBACK,
//            :fields.getData("chargebackFee") )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3224 = fields.getData("chargebackFee");
   String theSqlTS = "insert into miscchrg\n        ( app_seq_num,\n          misc_code,\n          misc_chrg_amount )\n        values\n        (  :1 ,\n           :2 ,\n           :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.app.verisign.VaEnd",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_CHARGEBACK);
   __sJT_st.setString(3,__sJT_3224);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:343^7*/
      
      // monthly service fee
      /*@lineinfo:generated-code*//*@lineinfo:346^7*/

//  ************************************************************
//  #sql [Ctx] { insert into miscchrg
//          ( app_seq_num,
//            misc_code,
//            misc_chrg_amount )
//          values
//          ( :appSeqNum,
//            :mesConstants.APP_MISC_CHARGE_MONTHLY_SERVICE_FEE,
//            :fields.getData("monthlyServiceFee") )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3225 = fields.getData("monthlyServiceFee");
   String theSqlTS = "insert into miscchrg\n        ( app_seq_num,\n          misc_code,\n          misc_chrg_amount )\n        values\n        (  :1 ,\n           :2 ,\n           :3  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.app.verisign.VaEnd",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.APP_MISC_CHARGE_MONTHLY_SERVICE_FEE);
   __sJT_st.setString(3,__sJT_3225);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:356^7*/
    }
    catch(Exception e)
    {
      vappLogEntry("submitMiscFees(" + appSeqNum + ")", e.toString());
    }
  }
  
  /*
  ** protected boolean autoSubmit()
  **
  ** submits app data.
  ** 
  ** RETURNS: true if submit ok, else false.
  */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    boolean wasStale = false;
    
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      // load partner pricing into fields
      loadPartnerPricing(1026);
      
      // add transaction pricing
      submitTranChrg();
      
      // add miscellaneous fees
      submitMiscFees();
      
      // insert into credit queue
      if(Ctx == null)
      {
        System.out.println("VaEnd: Context is null");
      }
      
      InsertCreditQueue icq = new InsertCreditQueue(Ctx);

      icq.addApp(fields.getField("appSeqNum").asLong(), 129515);
      
      submitOk = true;
    }
    catch(Exception e)
    {
      vappLogEntry("autoSubmit()", e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }
    
    return submitOk;
  }
    
  
  /*
  ** protected boolean autoLoad()
  **
  ** Loads pricing and ownership info that will be displayed on the final virtual
  ** app page.
  **
  ** RETURNS: true if load successful, else false.
  */
  protected boolean autoLoad()
  {
    boolean           loadOk  = false;
    ResultSetIterator it      = null;
    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      if (!loadPricing())
      {
        throw new Exception("Unable to load pricing");
      }
      
      loadCardsAccepted();
      
      connect();
      
      // owner 1 name and title
      /*@lineinfo:generated-code*//*@lineinfo:447^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_first_name || ' ' || busowner_last_name  as owner1_name,
//                  busowner_title                                    as owner1_title
//          from    businessowner
//          where   app_seq_num = :appSeqNum
//                  and busowner_num = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_first_name || ' ' || busowner_last_name  as owner1_name,\n                busowner_title                                    as owner1_title\n        from    businessowner\n        where   app_seq_num =  :1 \n                and busowner_num = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.verisign.VaEnd",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.app.verisign.VaEnd",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:454^7*/
      setFields(it.getResultSet());
      
      // owner 2 name and title
      /*@lineinfo:generated-code*//*@lineinfo:458^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_first_name || ' ' || busowner_last_name  as owner1_name,
//                  busowner_title                                    as owner1_title
//          from    businessowner
//          where   app_seq_num = :appSeqNum
//                  and busowner_num = 2
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_first_name || ' ' || busowner_last_name  as owner1_name,\n                busowner_title                                    as owner1_title\n        from    businessowner\n        where   app_seq_num =  :1 \n                and busowner_num = 2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.verisign.VaEnd",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.app.verisign.VaEnd",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:465^7*/
      setFields(it.getResultSet());
      
      // load agreement and return url
      /*@lineinfo:generated-code*//*@lineinfo:469^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.merch_agreement   as agreement,
//                  v.return_url        as return_url
//          from    merchant m,
//                  vs_linking_table v
//          where   m.app_seq_num = :appSeqNum
//                  and m.app_seq_num = v.app_seq_num(+)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.merch_agreement   as agreement,\n                v.return_url        as return_url\n        from    merchant m,\n                vs_linking_table v\n        where   m.app_seq_num =  :1 \n                and m.app_seq_num = v.app_seq_num(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.app.verisign.VaEnd",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.app.verisign.VaEnd",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:477^7*/
      setFields(it.getResultSet());

      loadOk = true;
    }
    catch (Exception e)
    {
      vappLogEntry("autoLoad()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) { }
      cleanUp();
    }
    return loadOk;
  }
}/*@lineinfo:generated-code*/