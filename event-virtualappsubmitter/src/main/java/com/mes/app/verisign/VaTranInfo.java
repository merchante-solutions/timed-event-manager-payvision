/*@lineinfo:filename=VaTranInfo*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/verisign/VaTranInfo.sqlj $

  Description:  
  
  VaTranInfo
  
  Data bean used by page 2 of the verisign virtual app.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 4/15/04 11:38a $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.verisign;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.CheckboxField;
import com.mes.forms.DisabledCheckboxField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.NumberField;
import com.mes.forms.TextareaField;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class VaTranInfo extends VaBase
{
  public VaTranInfo()
  {
  }
  
  public VaTranInfo(DefaultContext defCtx)
  {
    super(defCtx);
  }
  
  /*
  ** Validation
  */
  
  /*
  ** Custom fields
  */
  
  protected class YearField extends NumberField
  {
    public YearField(String fname, boolean nullAllowed)
    {
      super(fname,4,4,nullAllowed,0);
      addValidation(new YearValidation("Invalid year",nullAllowed));
    }

    protected String processData(String rawData)
    {
      if (rawData == null || rawData.equals("0"))
      {
        rawData = "";
      }
      return rawData;
    }
  }

  /*
  ** Drop down tables
  */
  
  protected class MonthTable extends DropDownTable
  {
    public MonthTable()
    {
      addElement("","select");
      addElement("1","Jan");
      addElement("2","Feb");
      addElement("3","Mar");
      addElement("4","Apr");
      addElement("5","May");
      addElement("6","Jun");
      addElement("7","Jul");
      addElement("8","Aug");
      addElement("9","Sep");
      addElement("10","Oct");
      addElement("11","Nov");
      addElement("12","Dec");
    }
  }
  
  protected class RefundPolicyTable extends DropDownTable
  {
    public RefundPolicyTable()
    {
      // value/name pairs
      addElement("","select one");
      addElement("1","Refund in 30 Days or Less");
      addElement("2","No Refunds or Exchanges");
      addElement("3","Exchanges Only");
      addElement("4","Not Applicable");
    }
  }

  public void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // 7 - merchant history
      FieldGroup gMerchHist = new FieldGroup("gMerchHist");

      gMerchHist.add(new DropDownField      ("haveProcessed",       "Ever Accepted Credit Cards Before?",new YesNoTable(),false));
      gMerchHist.add(new Field              ("previousProcessor",   "If Yes, Name of Previous Processor",40,25,true));
      gMerchHist.add(new DropDownField      ("haveCanceled",        "Ever Had a Merchant Account Canceled?",new YesNoTable(),false));
      gMerchHist.add(new Field              ("canceledProcessor",   "If Yes, Name of Processor",40,25,true));
      gMerchHist.add(new Field              ("canceledReason",      "Reason for Cancellation",40,25,true));
      gMerchHist.add(new DropDownField      ("cancelMonth",         "Date of Cancellation",new MonthTable(),true));
      gMerchHist.add(new YearField          ("cancelYear",          true));

      gMerchHist.getField("previousProcessor")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveProcessed"),
          "Please provide the name of the previous processor"));
      gMerchHist.getField("canceledProcessor")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
          "Please provide the name of the cancelling processor"));
      gMerchHist.getField("canceledReason")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
          "Please provide reason account was cancelled"));
      gMerchHist.getField("cancelMonth")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
          "Please select the month account was cancelled"));
      gMerchHist.getField("cancelYear")
        .addValidation(new IfYesNotBlankValidation(gMerchHist.getField("haveCanceled"),
          "Please provide a valid 4-digit year account was cancelled"));
      
      fields.add(gMerchHist);
      
      // 8 - transaction info
      FieldGroup gTranInfo = new FieldGroup("gTranInfo");
      
      gTranInfo.add(new NumericOnlyField       ("monthlySales",        "Total Estimated Monthly Sales",11,9,false));
      gTranInfo.add(new NumericOnlyField       ("monthlyVMCSales",     "Total Estimated Monthly Visa/MC Sales",11,9,false));
      gTranInfo.add(new NumericOnlyField       ("averageTicket",       "Estimated Average Credit Card Ticket",11,9,false));
      gTranInfo.add(new DropDownField       ("refundPolicy",        "Refund Policy",new RefundPolicyTable(),false));

      fields.add(gTranInfo);
      
      // 9 - payment options
      FieldGroup gPayment = new FieldGroup("gPayment");
      
      gPayment.add(new DisabledCheckboxField("vmcAccepted",         "Visa/MasterCard",true));
      gPayment.add(new CheckboxField        ("dinersAccepted",      "Diners Club",false));
      gPayment.add(new CheckboxField        ("jcbAccepted",         "JCB",false));
      gPayment.add(new CheckboxField        ("amexAccepted",        "American Express",false));
      gPayment.add(new CheckboxField        ("amexNewAccount",      "",false));
      gPayment.add(new DisabledCheckboxField("discoverAccepted",    "Discover",true));
      gPayment.add(new NumberField          ("dinersAcctNum",       "Diners Merchant No.",10,25,true,0));
      gPayment.add(new NumberField          ("jcbAcctNum",          "JCB Merchant No.",25,25,true,0));
      gPayment.add(new NumberField          ("amexAcctNum",         "SE No.",16,25,true,0));
      gPayment.add(new NumberField          ("discoverAcctNum",     "Discover Merchant No.",15,25,true,0));
      
      gPayment.setGroupHtmlExtra("class=\"formText\"");
      
      fields.add(gPayment);
      
      fields.add(new TextareaField          ("comments",            "Comments",300,3,90,true));

      // submit button
      fields.add(new ButtonField("submit","Submit Application Data","Submit"));
      
      // verisign style error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);

      // set the current screen id
      fields.setData("curScreenId","1");
    }
    catch (Exception e)
    {
      vappLogEntry("createFields()",e.toString());
    }
  }

  public static final int CT_VISA     = 0;
  public static final int CT_MC       = 1;
  public static final int CT_DEBIT    = 2;
  public static final int CT_DINERS   = 3;
  public static final int CT_DISCOVER = 4;
  public static final int CT_AMEX     = 5;
  public static final int CT_JCB      = 6;
  public static final int CT_CHECK    = 7;
  public static final int CT_EBT      = 8;
  public static final int CT_TYME     = 9;
  public static final int CT_COUNT    = 10;
  
  // table to map card types to field names
  public static String[][] cardFieldNames =
  {
    { "vmcAccepted",      null              }, // CT_VISA
    { "vmcAccepted",      null              }, // CT_MC
    { "debitAccepted",    null              }, // CT_DEBIT
    { "dinersAccepted",   "dinersAcctNum"   }, // CT_DINERS
    { "discoverAccepted", "discoverAcctNum" }, // CT_DISCOVER
    { "amexAccepted",     "amexAcctNum"     }, // CT_AMEX
    { "jcbAccepted",      "jcbAcctNum"      }, // CT_JCB
    { "checkAccepted",    null              }, // CT_CHECK
    { null,               null              }, // CT_EBT
    { null,               null              }, // CT_TYME
  };

  // table to map card types to db card types
  public static int[] cardDbTypes =
  {
    mesConstants.APP_CT_VISA,             // CT_VISA
    mesConstants.APP_CT_MC,               // CT_MC
    mesConstants.APP_CT_DEBIT,            // CT_DEBIT
    mesConstants.APP_CT_DINERS_CLUB,      // CT_DINERS
    mesConstants.APP_CT_DISCOVER,         // CT_DISCOVER
    mesConstants.APP_CT_AMEX,             // CT_AMEX
    mesConstants.APP_CT_JCB,              // CT_JCB
    mesConstants.APP_CT_CHECK_AUTH,       // CT_CHECK
    mesConstants.APP_CT_EBT,              // CT_EBT
    mesConstants.APP_CT_TYME_NETWORK,     // CT_TYME
  };

  /*
  ** protected void submitCardAcceptance()
  **
  ** Submits card payment type options.
  */
  protected void submitCardAcceptance()
  {
    long    appSeqNum     = fields.getField("appSeqNum").asLong();
    int     appCardType   = 0;
    boolean accepted      = false;
    String  acctId        = null;
    String  fname         = null;
    String  rate          = "";
    String  fee           = "";
    int     recId         = 0;

    try
    {
      for (int ct = 0; ct < CT_COUNT; ++ct)
      {
        // reset the per card type elements
        rate          = null;
        fee           = null;
        acctId        = null;

        // extract the accepted flag, (null pointer exception will cause a skip
        try
        {
          // determine if the card type is accepted
          accepted = fields.getData(cardFieldNames[ct][0]).toUpperCase().equals("Y");

          // extract the account number
          try
          {
            acctId  = fields.getField(cardFieldNames[ct][1]).getData();
          }
          catch( NullPointerException e )
          {
          }

          // get the db card type
          appCardType = cardDbTypes[ct];

          // do any special handling of card types
          switch (ct)
          {
            case CT_DISCOVER:
              if (fields.getData("discoverAccepted").equals("y") &&
                  fields.getData("discoverAcctNum").equals(""))
              {
                rate = "2.44";
                fee = "0.08";
              }
              break;

            case CT_AMEX:
              if (fields.getData("amexAccepted").equals("y") &&
                  fields.getData("amexAcctNum").equals(""))
              {
                rate = "3.2";
              }
              break;
          }

          // clear this card type from merchpayoption before inserting
          /*@lineinfo:generated-code*//*@lineinfo:305^11*/

//  ************************************************************
//  #sql [Ctx] { delete
//              from    merchpayoption
//              where   app_seq_num = :appSeqNum and
//                      cardtype_code = :appCardType
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n            from    merchpayoption\n            where   app_seq_num =  :1  and\n                    cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.verisign.VaTranInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,appCardType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:311^11*/

          // if card accepted, insert a record for card type into merchpayoption
          if (accepted)
          {
            // increment the record count and insert the record
            ++recId;
            /*@lineinfo:generated-code*//*@lineinfo:318^13*/

//  ************************************************************
//  #sql [Ctx] { insert into  merchpayoption
//                ( app_seq_num,
//                  cardtype_code,
//                  merchpo_card_merch_number,
//                  merchpo_provider_name,
//                  card_sr_number,
//                  merchpo_rate,
//                  merchpo_split_dial,
//                  merchpo_pip,
//                  merchpo_fee )
//                values
//                ( :appSeqNum,
//                  :appCardType,
//                  :acctId,
//                  null,           -- always null for this app
//                  :recId,
//                  :rate,
//                  'N',            -- always 'N' for this app
//                  null,           -- always null for this app
//                  :fee )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into  merchpayoption\n              ( app_seq_num,\n                cardtype_code,\n                merchpo_card_merch_number,\n                merchpo_provider_name,\n                card_sr_number,\n                merchpo_rate,\n                merchpo_split_dial,\n                merchpo_pip,\n                merchpo_fee )\n              values\n              (  :1 ,\n                 :2 ,\n                 :3 ,\n                null,           -- always null for this app\n                 :4 ,\n                 :5 ,\n                'N',            -- always 'N' for this app\n                null,           -- always null for this app\n                 :6  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.app.verisign.VaTranInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,appCardType);
   __sJT_st.setString(3,acctId);
   __sJT_st.setInt(4,recId);
   __sJT_st.setString(5,rate);
   __sJT_st.setString(6,fee);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:340^13*/
          }
          // else clear any related records for this card type from tranchrg
          else
          {
            /*@lineinfo:generated-code*//*@lineinfo:345^13*/

//  ************************************************************
//  #sql [Ctx] { delete
//                from    tranchrg
//                where   app_seq_num = :appSeqNum and
//                        cardtype_code = :appCardType
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n              from    tranchrg\n              where   app_seq_num =  :1  and\n                      cardtype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.app.verisign.VaTranInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,appCardType);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:351^13*/
          }
        }
        catch (NullPointerException ne1)
        {
        }
      }
    }
    catch(Exception e)
    {
      addError("submitPayOptions: " + e.toString());
      vappLogEntry("submitPayOptions()", e.toString());
    }
  }

  /*
  ** protected boolean autoSubmit()
  **
  ** Submits app data.
  **
  ** RETURNS: true if submit ok, else false.
  */
  protected boolean autoSubmit()
  {
    boolean submitOk = false;
    boolean wasStale  = false;
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      try
      {
        // store the card types accepted
        submitCardAcceptance();
      }
      catch(Exception e)
      {
        vappLogEntry("autoSubmit(cardAcceptance)", e.toString());
      }
      
      long appSeqNum = fields.getField("appSeqNum").asLong();

      try
      {
        // history and transaction info
        /*@lineinfo:generated-code*//*@lineinfo:400^9*/

//  ************************************************************
//  #sql [Ctx] { update  merchant
//            set     merch_prior_cc_accp_flag    = :fields.getField("haveProcessed").getData(),
//                    merch_prior_processor       = :fields.getField("previousProcessor").getData(),
//                    merch_cc_acct_term_flag     = :fields.getField("haveCanceled").getData(),
//                    merch_cc_term_name          = :fields.getField("canceledProcessor").getData(),
//                    merch_term_reason           = :fields.getField("canceledReason").getData(),
//                    merch_term_month            = :fields.getField("cancelMonth").getData(),
//                    merch_term_year             = :fields.getField("cancelYear").getData(),
//                    merch_month_tot_proj_sales  = :fields.getField("monthlySales").getData(),
//                    merch_month_visa_mc_sales   = :fields.getField("monthlyVMCSales").getData(),
//                    merch_average_cc_tran       = :fields.getField("averageTicket").getData(),
//                    refundtype_code             = :fields.getField("refundPolicy").getData(),
//                    merch_notes                 = :fields.getField("comments").getData(),
//                    merch_mail_phone_sales      = 100,
//                    merch_prior_statements      = 'N'
//            where   app_seq_num                 = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3269 = fields.getField("haveProcessed").getData();
 String __sJT_3270 = fields.getField("previousProcessor").getData();
 String __sJT_3271 = fields.getField("haveCanceled").getData();
 String __sJT_3272 = fields.getField("canceledProcessor").getData();
 String __sJT_3273 = fields.getField("canceledReason").getData();
 String __sJT_3274 = fields.getField("cancelMonth").getData();
 String __sJT_3275 = fields.getField("cancelYear").getData();
 String __sJT_3276 = fields.getField("monthlySales").getData();
 String __sJT_3277 = fields.getField("monthlyVMCSales").getData();
 String __sJT_3278 = fields.getField("averageTicket").getData();
 String __sJT_3279 = fields.getField("refundPolicy").getData();
 String __sJT_3280 = fields.getField("comments").getData();
   String theSqlTS = "update  merchant\n          set     merch_prior_cc_accp_flag    =  :1 ,\n                  merch_prior_processor       =  :2 ,\n                  merch_cc_acct_term_flag     =  :3 ,\n                  merch_cc_term_name          =  :4 ,\n                  merch_term_reason           =  :5 ,\n                  merch_term_month            =  :6 ,\n                  merch_term_year             =  :7 ,\n                  merch_month_tot_proj_sales  =  :8 ,\n                  merch_month_visa_mc_sales   =  :9 ,\n                  merch_average_cc_tran       =  :10 ,\n                  refundtype_code             =  :11 ,\n                  merch_notes                 =  :12 ,\n                  merch_mail_phone_sales      = 100,\n                  merch_prior_statements      = 'N'\n          where   app_seq_num                 =  :13";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.verisign.VaTranInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3269);
   __sJT_st.setString(2,__sJT_3270);
   __sJT_st.setString(3,__sJT_3271);
   __sJT_st.setString(4,__sJT_3272);
   __sJT_st.setString(5,__sJT_3273);
   __sJT_st.setString(6,__sJT_3274);
   __sJT_st.setString(7,__sJT_3275);
   __sJT_st.setString(8,__sJT_3276);
   __sJT_st.setString(9,__sJT_3277);
   __sJT_st.setString(10,__sJT_3278);
   __sJT_st.setString(11,__sJT_3279);
   __sJT_st.setString(12,__sJT_3280);
   __sJT_st.setLong(13,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:418^9*/
      }
      catch(Exception e)
      {
        vappLogEntry("autoSubmit(merchant)", e.toString());
      }

      submitOk = true;
    }
    catch (Exception e)
    {
      vappLogEntry("autoSubmit()",e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }

    return submitOk;
  }

  /*
  ** protected boolean autoLoad()
  **
  ** Loads app data.
  **
  ** RETURNS: true if successful, else false.
  */
  protected boolean autoLoad()
  {
    boolean           loadOk  = false;
    ResultSetIterator it      = null;
    ResultSet         rs      = null;
    boolean           wasStale = false;
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      long appSeqNum = fields.getField("appSeqNum").asLong();

      // history and transaction info
      /*@lineinfo:generated-code*//*@lineinfo:466^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_prior_cc_accp_flag     as have_processed,
//                  merch_prior_processor        as previous_processor,
//                  merch_cc_acct_term_flag      as have_canceled,
//                  merch_cc_term_name           as canceled_processor,
//                  merch_term_reason            as canceled_reason,
//                  merch_term_year              as cancel_year,
//                  merch_term_month             as cancel_month,
//                  merch_month_tot_proj_sales   as monthly_sales,
//                  merch_month_visa_mc_sales    as monthly_vmc_sales,
//                  merch_average_cc_tran        as average_ticket,
//                  refundtype_code              as refund_policy,
//                  merch_notes                  as comments
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_prior_cc_accp_flag     as have_processed,\n                merch_prior_processor        as previous_processor,\n                merch_cc_acct_term_flag      as have_canceled,\n                merch_cc_term_name           as canceled_processor,\n                merch_term_reason            as canceled_reason,\n                merch_term_year              as cancel_year,\n                merch_term_month             as cancel_month,\n                merch_month_tot_proj_sales   as monthly_sales,\n                merch_month_visa_mc_sales    as monthly_vmc_sales,\n                merch_average_cc_tran        as average_ticket,\n                refundtype_code              as refund_policy,\n                merch_notes                  as comments\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.verisign.VaTranInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.app.verisign.VaTranInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:482^7*/
      setFields(it.getResultSet());

      // payment options
      /*@lineinfo:generated-code*//*@lineinfo:486^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code                   as card_type,
//                  merchpo_card_merch_number       as merchant_number,
//                  merchpo_provider_name           as provider_name,
//                  merchpo_rate                    as rate,
//                  merchpo_fee                     as per_item
//          from   merchpayoption
//          where  app_seq_num   = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cardtype_code                   as card_type,\n                merchpo_card_merch_number       as merchant_number,\n                merchpo_provider_name           as provider_name,\n                merchpo_rate                    as rate,\n                merchpo_fee                     as per_item\n        from   merchpayoption\n        where  app_seq_num   =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.app.verisign.VaTranInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.app.verisign.VaTranInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:495^7*/
      rs = it.getResultSet();
      while(rs.next())
      {
        switch(rs.getInt("card_type"))
        {
          case mesConstants.APP_CT_DINERS_CLUB:
            fields.setData("dinersAccepted","y");
            fields.setData("dinersAcctNum",rs.getString("merchant_number"));
            break;

          case mesConstants.APP_CT_JCB:
            fields.setData("jcbAccepted","y");
            fields.setData("jcbAcctNum",rs.getString("merchant_number"));
            break;

          case mesConstants.APP_CT_AMEX:
            fields.setData("amexAccepted","y");
            fields.setData("amexAcctNum",rs.getString("merchant_number"));
            if (rs.getString("merchant_number") == null)
            {
              fields.setData("amexNewAccount","y");
            }
            break;
            
          case mesConstants.APP_CT_DISCOVER:
            fields.setData("discoverAcctNum",rs.getString("merchant_number"));
            break;

          default:    // ignore
            break;
        }
      }
      it.close();
        
      loadOk = true;
    }
    catch (Exception e)
    {
      vappLogEntry("autoLoad()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) { }
      if(wasStale)
      {
        cleanUp();
      }
    }

    return loadOk;
  }
}/*@lineinfo:generated-code*/