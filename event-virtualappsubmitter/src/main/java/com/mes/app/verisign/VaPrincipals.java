/*@lineinfo:filename=VaPrincipals*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/verisign/VaPrincipals.sqlj $

  Description:  
  
  VaPrincipals
  
  Data bean used by page 2 of the verisign virtual app.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 2/10/04 3:27p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.verisign;

import java.sql.ResultSet;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.ButtonField;
import com.mes.forms.CaseInsensitiveDropDownField;
import com.mes.forms.DropDownField;
import com.mes.forms.Field;
import com.mes.forms.FieldGroup;
import com.mes.forms.NumberField;
import com.mes.forms.PhoneField;
import com.mes.forms.StateDropDownTable;
import com.mes.forms.TaxIdField;
import com.mes.forms.Validation;
import com.mes.forms.ZipField;
import com.mes.tools.DropDownTable;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class VaPrincipals extends VaBase
{
  public VaPrincipals()
  {
  }
  
  public VaPrincipals(DefaultContext defCtx)
  {
    super(defCtx);
  }
  
  /*
  ** Validation
  */
  
  public class TransitRoutingValidation implements Validation
  {
    public String getErrorText()
    {
      return "Invalid transit routing #";
    }
    
    public boolean validate(String fdata)
    {
      // put transit routing validation here
      return true;
    }
  }

  /*
  ** Custom fields
  */
  
  protected class YearField extends NumberField
  {
    public YearField(String fname, boolean nullAllowed)
    {
      super(fname,4,4,nullAllowed,0);
      addValidation(new YearValidation("Invalid year",nullAllowed));
    }

    protected String processData(String rawData)
    {
      if (rawData == null || rawData.equals("0"))
      {
        rawData = "";
      }
      return rawData;
    }
  }

  /*
  ** Drop down tables
  */
  
  protected class MonthTable extends DropDownTable
  {
    public MonthTable()
    {
      addElement("","select");
      addElement("1","Jan");
      addElement("2","Feb");
      addElement("3","Mar");
      addElement("4","Apr");
      addElement("5","May");
      addElement("6","Jun");
      addElement("7","Jul");
      addElement("8","Aug");
      addElement("9","Sep");
      addElement("10","Oct");
      addElement("11","Nov");
      addElement("12","Dec");
    }
  }
  
  protected class PercentTable extends DropDownTable
  {
    public PercentTable()
    {
      addElement("","select");
      for (int i = 100; i > 0; --i)
      {
        String percStr = Integer.toString(i);
        addElement(percStr,percStr + "%");
      }
    }
  }
  
  protected class AccountInfoSourceTable extends DropDownTable
  {
    public AccountInfoSourceTable()
      throws java.sql.SQLException
    {
      ResultSetIterator it  = null;
      ResultSet         rs  = null;
      boolean wasStale = false;

      try
      {      
        if(isConnectionStale())
        {
          connect();
          wasStale = true;
        }
        /*@lineinfo:generated-code*//*@lineinfo:147^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  bais.source_id,
//                    bais.source_desc
//            from    bank_account_info_source bais
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bais.source_id,\n                  bais.source_desc\n          from    bank_account_info_source bais";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.verisign.VaPrincipals",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.app.verisign.VaPrincipals",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:152^9*/
        rs = it.getResultSet();

        addElement("","select");
        while (rs.next() )
        {
          addElement(rs);
        }
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        if(wasStale)
        {
          cleanUp();
        }
      }
    }
  }
  
  protected class AccountTypeTable extends DropDownTable
  {
    public AccountTypeTable()
      throws java.sql.SQLException
    {
      ResultSetIterator it  = null;
      ResultSet         rs  = null;
      boolean wasStale = false;

      try
      {      
        if(isConnectionStale())
        {
          connect();
          wasStale = true;
        }
        /*@lineinfo:generated-code*//*@lineinfo:188^9*/

//  ************************************************************
//  #sql [Ctx] it = { select    bankacc_type,
//                      bankacc_description
//            from      bankacctype
//            order by  bankacc_order
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    bankacc_type,\n                    bankacc_description\n          from      bankacctype\n          order by  bankacc_order";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.verisign.VaPrincipals",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.app.verisign.VaPrincipals",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:194^9*/
        rs = it.getResultSet();
        
        addElement("","select");
        while(rs.next() )
        {
          addElement(rs);
        }
      }
      finally
      {
        try{ it.close(); } catch( Exception e ) { }
        if(wasStale)
        {
          cleanUp();
        }
      }
    }
  }
  
  public void createFields(HttpServletRequest request)
  {
    super.createFields(request);
    
    try
    {
      // 4 - primary owner
      FieldGroup gOwner1 = new FieldGroup("gOwner1");
      
      gOwner1.add(new Field           ("owner1FirstName",         "First Name",20,25,false));
      gOwner1.add(new Field           ("owner1LastName",          "Last Name",20,25,false));
      gOwner1.add(new TaxIdField      ("owner1SSN",               "Soc. Security No.",false));
      gOwner1.add(new DropDownField   ("owner1Percent",           "Percent Owned",new PercentTable(),false));
      gOwner1.add(new Field           ("owner1Address1",          "Residence Address",32,25,false));
      gOwner1.add(new Field           ("owner1City",              "City",25,25,false));
      gOwner1.add(new CaseInsensitiveDropDownField   ("owner1State",             "State",new StateDropDownTable(),false));
      gOwner1.add(new ZipField        ("owner1Zip",               "Zip",false,fields.getField("owner1State")));
      gOwner1.add(new PhoneField      ("owner1Phone",             "Phone Number",true));
      gOwner1.add(new DropDownField   ("owner1SinceMonth",        "Owner/Officer Since",new MonthTable(), true));
      gOwner1.add(new YearField       ("owner1SinceYear",         true));
      gOwner1.add(new Field           ("owner1Title",             "Title",40,25,true));

      fields.add(gOwner1);

      // 5 - secondary owner
      FieldGroup gOwner2 = new FieldGroup("gOwner2");
      
      gOwner2.add(new Field           ("owner2FirstName",         "First Name",25,20,true));
      gOwner2.add(new Field           ("owner2LastName",          "Last Name",25,20,true));
      gOwner2.add(new TaxIdField      ("owner2SSN",               "Soc. Security No.",true));
      gOwner1.add(new DropDownField   ("owner2Percent",           "Percent Owned",new PercentTable(),true));
      gOwner2.add(new Field           ("owner2Address1",          "Residence Address",32,25,true));
      gOwner2.add(new Field           ("owner2City",              "City",25,25,true));
      gOwner2.add(new CaseInsensitiveDropDownField   ("owner2State",             "State",new StateDropDownTable(),true));
      gOwner2.add(new ZipField        ("owner2Zip",               "Zip",true,fields.getField("owner1State")));
      gOwner2.add(new PhoneField      ("owner2Phone",             "Phone Number",true));
      gOwner2.add(new DropDownField   ("owner2SinceMonth",        "Owner/Officer Since",new MonthTable(), true));
      gOwner2.add(new YearField       ("owner2SinceYear",         true));
      gOwner2.add(new Field           ("owner2Title",             "Title",40,25,true));

      fields.add(gOwner2);

      // 6 - bank account info
      FieldGroup gBankAcct = new FieldGroup("gBankAcct");

      gBankAcct.add(new Field         ("bankName",                "Name of Bank",30,25,false));
      gBankAcct.add(new NumberField   ("yearsOpen",               "Years Open",3,25,false,0));
      gBankAcct.add(new Field         ("bankAddress",             "Bank Address",32,25,false));
      gBankAcct.add(new Field         ("bankCity",                "Bank City",25,25,false));
      gBankAcct.add(new CaseInsensitiveDropDownField ("bankState",               "Bank State",new StateDropDownTable(),false));
      gBankAcct.add(new ZipField      ("bankZip",                 "Bank Zip",false,fields.getField("bankState")));
      gBankAcct.add(new Field         ("checkingAccount",         "Checking Acct. #",17,25,false));
      gBankAcct.add(new Field         ("confirmCheckingAccount",  "Confirm Checking Acct. #",17,25,false));
      gBankAcct.add(new Field         ("transitRouting",          "Transit Routing #",9,25,false));
      gBankAcct.add(new Field         ("confirmTransitRouting",   "Confirm Transit Routing #",9,25,false));
      gBankAcct.add(new DropDownField ("sourceOfInfo",            "Source of Acct. Info",new AccountInfoSourceTable(),false));
      gBankAcct.add(new DropDownField ("typeOfAcct",              "Type of Acct.",new AccountTypeTable(),false));

      gBankAcct.getField("confirmCheckingAccount")
        .addValidation(new FieldEqualsFieldValidation(
          gBankAcct.getField("checkingAccount"),"Checking Account #"));
      gBankAcct.getField("confirmTransitRouting")
        .addValidation(new FieldEqualsFieldValidation(
          gBankAcct.getField("transitRouting"),"Transit Routing #"));
      gBankAcct.getField("confirmTransitRouting")
        .addValidation(new TransitRoutingValidation());

      fields.add(gBankAcct);

      // submit button
      fields.add(new ButtonField("submit","Submit Application Data","Submit"));
      
      // verisign style error indicator
      fields.setFixImage("/images/arrow1_left.gif",10,10);

      // set the current screen id
      fields.setData("curScreenId","1");
    }
    catch (Exception e)
    {
      vappLogEntry("createFields()",e.toString());
    }
  }
  
  public static final int     FIDX_OWNER_LAST_NAME        = 0;
  public static final int     FIDX_OWNER_FIRST_NAME       = 1;
  public static final int     FIDX_OWNER_SSN              = 2;
  public static final int     FIDX_OWNER_PERCENT          = 3;
  public static final int     FIDX_OWNER_MONTH            = 4;
  public static final int     FIDX_OWNER_YEAR             = 5;
  public static final int     FIDX_OWNER_TITLE            = 6;
  
  /*
  ** protected void storeBusinessOwnerData(int ownerId, Field[] fields)
  **
  ** Stores a set of owner data.
  */
  protected void storeBusinessOwnerData(int ownerId, Field[] ownerFields)
  {
    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      /*@lineinfo:generated-code*//*@lineinfo:317^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from    businessowner
//          where   app_seq_num = :appSeqNum
//                  and busowner_num = :ownerId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from    businessowner\n        where   app_seq_num =  :1 \n                and busowner_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.app.verisign.VaPrincipals",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,ownerId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:323^7*/

      Field ssn = ownerFields[FIDX_OWNER_SSN];
      if (ssn != null && !ssn.isBlank())
      {
        /*@lineinfo:generated-code*//*@lineinfo:328^9*/

//  ************************************************************
//  #sql [Ctx] { insert into businessowner
//            ( app_seq_num,
//              busowner_num,
//              busowner_last_name,
//              busowner_first_name,
//              busowner_ssn,
//              busowner_owner_perc,
//              busowner_period_month,
//              busowner_period_year,
//              busowner_title )
//            values
//            ( :appSeqNum,
//              :ownerId,
//              :(ownerFields[FIDX_OWNER_LAST_NAME]  == null) ? null : ownerFields[FIDX_OWNER_LAST_NAME].getData(),
//              :(ownerFields[FIDX_OWNER_FIRST_NAME] == null) ? null : ownerFields[FIDX_OWNER_FIRST_NAME].getData(),
//              :(ownerFields[FIDX_OWNER_SSN]        == null) ? null : ownerFields[FIDX_OWNER_SSN].getData(),
//              :(ownerFields[FIDX_OWNER_PERCENT]    == null) ? null : ownerFields[FIDX_OWNER_PERCENT].getData(),
//              :(ownerFields[FIDX_OWNER_MONTH]      == null) ? null : ownerFields[FIDX_OWNER_MONTH].getData(),
//              :(ownerFields[FIDX_OWNER_YEAR]       == null) ? null : ownerFields[FIDX_OWNER_YEAR].getData(),
//              :(ownerFields[FIDX_OWNER_TITLE]      == null) ? null : ownerFields[FIDX_OWNER_TITLE].getData() )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3226 = (ownerFields[FIDX_OWNER_LAST_NAME]  == null) ? null : ownerFields[FIDX_OWNER_LAST_NAME].getData();
 String __sJT_3227 = (ownerFields[FIDX_OWNER_FIRST_NAME] == null) ? null : ownerFields[FIDX_OWNER_FIRST_NAME].getData();
 String __sJT_3228 = (ownerFields[FIDX_OWNER_SSN]        == null) ? null : ownerFields[FIDX_OWNER_SSN].getData();
 String __sJT_3229 = (ownerFields[FIDX_OWNER_PERCENT]    == null) ? null : ownerFields[FIDX_OWNER_PERCENT].getData();
 String __sJT_3230 = (ownerFields[FIDX_OWNER_MONTH]      == null) ? null : ownerFields[FIDX_OWNER_MONTH].getData();
 String __sJT_3231 = (ownerFields[FIDX_OWNER_YEAR]       == null) ? null : ownerFields[FIDX_OWNER_YEAR].getData();
 String __sJT_3232 = (ownerFields[FIDX_OWNER_TITLE]      == null) ? null : ownerFields[FIDX_OWNER_TITLE].getData();
   String theSqlTS = "insert into businessowner\n          ( app_seq_num,\n            busowner_num,\n            busowner_last_name,\n            busowner_first_name,\n            busowner_ssn,\n            busowner_owner_perc,\n            busowner_period_month,\n            busowner_period_year,\n            busowner_title )\n          values\n          (  :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.app.verisign.VaPrincipals",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,ownerId);
   __sJT_st.setString(3,__sJT_3226);
   __sJT_st.setString(4,__sJT_3227);
   __sJT_st.setString(5,__sJT_3228);
   __sJT_st.setString(6,__sJT_3229);
   __sJT_st.setString(7,__sJT_3230);
   __sJT_st.setString(8,__sJT_3231);
   __sJT_st.setString(9,__sJT_3232);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:350^9*/
      }
    }
    catch(Exception e)
    {
      addError("storeBusinessOwnerData: " + e.toString());
      vappLogEntry("storeBusinessOwnerData( " + ownerId + " )", e.toString());
    }
  }

  /*
  ** protected boolean autoSubmit()
  **
  ** Submits app data.
  **
  ** RETURNS: true if submit ok, else false.
  */
  protected boolean autoSubmit()
  {
    boolean submitOk  = false;
    boolean wasStale  = false;
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      /*      
      // check to see if transit routing number is just short
      if(fields.getData("transitRouting").length() == 8)
      {
        StringBuffer newTransitRouting = new StringBuffer("0");
        newTransitRouting.append(fields.getData("transitRouting"));
        
        fields.setData("transitRouting", newTransitRouting.toString());
      }
      */
      
      try
      {
        // owner 1
        storeBusinessOwnerData( 1,new Field[]{ fields.getField("owner1LastName"),
                                               fields.getField("owner1FirstName"),
                                               fields.getField("owner1SSN"),
                                               fields.getField("owner1Percent"),
                                               fields.getField("owner1SinceMonth"),
                                               fields.getField("owner1SinceYear"),
                                               fields.getField("owner1Title") } );
      }
      catch(Exception e)
      {
        vappLogEntry("autoSubmit(storeOwner1)", e.toString());
      }

      try
      {
        storeAddressData(mesConstants.ADDR_TYPE_OWNER1,
                                  new Field[]{ fields.getField("owner1Address1"),
                                               null,
                                               fields.getField("owner1City"),
                                               fields.getField("owner1State"),
                                               fields.getField("owner1Zip"),
                                               null,
                                               fields.getField("owner1Phone"),
                                               null } );
      }
      catch(Exception e)
      {
        vappLogEntry("autoSubmit(storeOwner1Addr)", e.toString());
      }

      try
      {
        // owner 2
        storeBusinessOwnerData( 2,new Field[]{ fields.getField("owner2LastName"),
                                               fields.getField("owner2FirstName"),
                                               fields.getField("owner2SSN"),
                                               fields.getField("owner2Percent"),
                                               fields.getField("owner2SinceMonth"),
                                               fields.getField("owner2SinceYear"),
                                               fields.getField("owner2Title") } );
      }
      catch(Exception e)
      {
        vappLogEntry("autoSubmit(storeOwner2)", e.toString());
      }
      
      try
      {                                        
        storeAddressData(mesConstants.ADDR_TYPE_OWNER2,
                                  new Field[]{ fields.getField("owner2Address1"),
                                               null,
                                               fields.getField("owner2City"),
                                               fields.getField("owner2State"),
                                               fields.getField("owner2Zip"),
                                               null,
                                               fields.getField("owner2Phone"),
                                               null } );
      }
      catch(Exception e)
      {
        vappLogEntry("autoSubmit(storeOwner2Addr)", e.toString());
      }
                                             
      try
      {                                       
        // bank info
        storeAddressData(mesConstants.ADDR_TYPE_CHK_ACCT_BANK,
                                  new Field[]{ fields.getField("bankAddress"),
                                               null,
                                               fields.getField("bankCity"),
                                               fields.getField("bankState"),
                                               fields.getField("bankZip"),
                                               null,
                                               null,
                                               null } );
      }
      catch(Exception e)
      {
        vappLogEntry("autoSubmit(storeBankAddr)", e.toString());
      }

      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:478^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    merchbank
//            where   app_seq_num = :appSeqNum and
//                    merchbank_acct_srnum = 1
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    merchbank\n          where   app_seq_num =  :1  and\n                  merchbank_acct_srnum = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.app.verisign.VaPrincipals",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:484^9*/

        /*@lineinfo:generated-code*//*@lineinfo:486^9*/

//  ************************************************************
//  #sql [Ctx] { insert into merchbank
//            (
//              app_seq_num,
//              bankacc_type,
//              merchbank_info_source,
//              merchbank_name,
//              merchbank_acct_num,
//              merchbank_transit_route_num,
//              merchbank_num_years_open,
//              merchbank_acct_srnum
//            )
//            values
//            (
//              :appSeqNum,
//              :fields.getField("typeOfAcct").getData(),
//              :fields.getField("sourceOfInfo").getData(),
//              :fields.getField("bankName").getData(),
//              :fields.getField("checkingAccount").getData(),
//              :fields.getField("transitRouting").getData(),
//              :fields.getField("yearsOpen").getData(),
//              1
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3233 = fields.getField("typeOfAcct").getData();
 String __sJT_3234 = fields.getField("sourceOfInfo").getData();
 String __sJT_3235 = fields.getField("bankName").getData();
 String __sJT_3236 = fields.getField("checkingAccount").getData();
 String __sJT_3237 = fields.getField("transitRouting").getData();
 String __sJT_3238 = fields.getField("yearsOpen").getData();
   String theSqlTS = "insert into merchbank\n          (\n            app_seq_num,\n            bankacc_type,\n            merchbank_info_source,\n            merchbank_name,\n            merchbank_acct_num,\n            merchbank_transit_route_num,\n            merchbank_num_years_open,\n            merchbank_acct_srnum\n          )\n          values\n          (\n             :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n            1\n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.app.verisign.VaPrincipals",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,__sJT_3233);
   __sJT_st.setString(3,__sJT_3234);
   __sJT_st.setString(4,__sJT_3235);
   __sJT_st.setString(5,__sJT_3236);
   __sJT_st.setString(6,__sJT_3237);
   __sJT_st.setString(7,__sJT_3238);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:510^9*/
      }
      catch(Exception e)
      {
        vappLogEntry("autoSubmit(merchbank)", e.toString());
      }

      submitOk = true;
    }
    catch (Exception e)
    {
      vappLogEntry("autoSubmit()",e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }

    return submitOk;
  }

  /*
  ** protected boolean autoLoad()
  **
  ** Loads app data.
  **
  ** RETURNS: true if successful, else false.
  */
  protected boolean autoLoad()
  {
    boolean           loadOk  = false;
    ResultSetIterator it      = null;
    boolean           wasStale  = false;
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }

      long appSeqNum = fields.getField("appSeqNum").asLong();

      // bank account info
      /*@lineinfo:generated-code*//*@lineinfo:557^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  bankacc_type                as type_of_acct,
//                  merchbank_info_source       as source_of_info,
//                  merchbank_name              as bank_name,
//                  merchbank_acct_num          as checking_account,
//                  merchbank_acct_num          as confirm_checking_account,
//                  merchbank_transit_route_num as transit_routing,
//                  merchbank_transit_route_num as confirm_transit_routing,
//                  merchbank_num_years_open    as years_open
//          from    merchbank
//          where   app_seq_num = :appSeqNum and
//                  merchbank_acct_srnum = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  bankacc_type                as type_of_acct,\n                merchbank_info_source       as source_of_info,\n                merchbank_name              as bank_name,\n                merchbank_acct_num          as checking_account,\n                merchbank_acct_num          as confirm_checking_account,\n                merchbank_transit_route_num as transit_routing,\n                merchbank_transit_route_num as confirm_transit_routing,\n                merchbank_num_years_open    as years_open\n        from    merchbank\n        where   app_seq_num =  :1  and\n                merchbank_acct_srnum = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.app.verisign.VaPrincipals",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.app.verisign.VaPrincipals",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:570^7*/
      setFields(it.getResultSet());
      it.close();

      // bank address
      /*@lineinfo:generated-code*//*@lineinfo:575^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_line1               as bank_address,
//                  address_city                as bank_city,
//                  countrystate_code           as bank_state,
//                  address_zip                 as bank_zip
//          from    address
//          where   app_seq_num = :appSeqNum and
//                  addresstype_code = :mesConstants.ADDR_TYPE_CHK_ACCT_BANK
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_line1               as bank_address,\n                address_city                as bank_city,\n                countrystate_code           as bank_state,\n                address_zip                 as bank_zip\n        from    address\n        where   app_seq_num =  :1  and\n                addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.app.verisign.VaPrincipals",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_CHK_ACCT_BANK);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.app.verisign.VaPrincipals",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:584^7*/
      setFields(it.getResultSet());
      it.close();

      // merchant info
      /*@lineinfo:generated-code*//*@lineinfo:589^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  merch_business_name         as business_name,
//                  merch_legal_name            as business_legal_name,
//                  merch_federal_tax_id        as taxpayer_id,
//                  merch_business_establ_month as established_month,
//                  merch_business_establ_year  as established_year,
//                  merch_mailing_name          as mailing_name,
//                  bustype_code                as business_type,
//                  merch_years_at_loc          as location_years,
//                  industype_code              as industry_type,
//                  merch_busgoodserv_descr     as business_desc,
//                  merch_web_url               as web_url,
//                  promotion_code              as promotion_code
//          from    merchant
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  merch_business_name         as business_name,\n                merch_legal_name            as business_legal_name,\n                merch_federal_tax_id        as taxpayer_id,\n                merch_business_establ_month as established_month,\n                merch_business_establ_year  as established_year,\n                merch_mailing_name          as mailing_name,\n                bustype_code                as business_type,\n                merch_years_at_loc          as location_years,\n                industype_code              as industry_type,\n                merch_busgoodserv_descr     as business_desc,\n                merch_web_url               as web_url,\n                promotion_code              as promotion_code\n        from    merchant\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.app.verisign.VaPrincipals",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.app.verisign.VaPrincipals",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:605^7*/
      setFields(it.getResultSet());
      it.close();

      // owner 1 address
      /*@lineinfo:generated-code*//*@lineinfo:610^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_phone               as owner1_phone,
//                  address_line1               as owner1_address_1,
//                  address_city                as owner1_city,
//                  countrystate_code           as owner1_state,
//                  address_zip                 as owner1_zip
//          from    address
//          where   app_seq_num = :appSeqNum
//                  and addresstype_code = :mesConstants.ADDR_TYPE_OWNER1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_phone               as owner1_phone,\n                address_line1               as owner1_address_1,\n                address_city                as owner1_city,\n                countrystate_code           as owner1_state,\n                address_zip                 as owner1_zip\n        from    address\n        where   app_seq_num =  :1 \n                and addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.app.verisign.VaPrincipals",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_OWNER1);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.app.verisign.VaPrincipals",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:620^7*/
      setFields(it.getResultSet());
      it.close();
      
      // owner 1 info
      /*@lineinfo:generated-code*//*@lineinfo:625^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_last_name          as owner1_last_name,
//                  busowner_first_name         as owner1_first_name,
//                  busowner_ssn                as owner1_ssn,
//                  busowner_owner_perc         as owner1_percent,
//                  busowner_period_month       as owner1_since_month,
//                  busowner_period_year        as owner1_since_year,
//                  busowner_title              as owner1_title
//          from    businessowner
//          where   app_seq_num  = :appSeqNum and
//                  busowner_num = :mesConstants.BUS_OWNER_PRIMARY
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_last_name          as owner1_last_name,\n                busowner_first_name         as owner1_first_name,\n                busowner_ssn                as owner1_ssn,\n                busowner_owner_perc         as owner1_percent,\n                busowner_period_month       as owner1_since_month,\n                busowner_period_year        as owner1_since_year,\n                busowner_title              as owner1_title\n        from    businessowner\n        where   app_seq_num  =  :1  and\n                busowner_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.app.verisign.VaPrincipals",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.BUS_OWNER_PRIMARY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.app.verisign.VaPrincipals",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:637^7*/
      setFields(it.getResultSet());
      it.close();

      // owner 2 address
      /*@lineinfo:generated-code*//*@lineinfo:642^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  address_phone               as owner2_phone,
//                  address_line1               as owner2_address_1,
//                  address_city                as owner2_city,
//                  countrystate_code           as owner2_state,
//                  address_zip                 as owner2_zip
//          from    address
//          where   app_seq_num = :appSeqNum
//                  and addresstype_code = :mesConstants.ADDR_TYPE_OWNER2
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  address_phone               as owner2_phone,\n                address_line1               as owner2_address_1,\n                address_city                as owner2_city,\n                countrystate_code           as owner2_state,\n                address_zip                 as owner2_zip\n        from    address\n        where   app_seq_num =  :1 \n                and addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.app.verisign.VaPrincipals",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.ADDR_TYPE_OWNER2);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.app.verisign.VaPrincipals",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:652^7*/
      setFields(it.getResultSet());
      it.close();
      
      // owner 2 info
      /*@lineinfo:generated-code*//*@lineinfo:657^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  busowner_last_name          as owner2_last_name,
//                  busowner_first_name         as owner2_first_name,
//                  busowner_ssn                as owner2_ssn,
//                  busowner_owner_perc         as owner2_percent,
//                  busowner_period_month       as owner2_since_month,
//                  busowner_period_year        as owner2_since_year,
//                  busowner_title              as owner2_title
//          from    businessowner
//          where   app_seq_num  = :appSeqNum and
//                  busowner_num = :mesConstants.BUS_OWNER_SECONDARY
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  busowner_last_name          as owner2_last_name,\n                busowner_first_name         as owner2_first_name,\n                busowner_ssn                as owner2_ssn,\n                busowner_owner_perc         as owner2_percent,\n                busowner_period_month       as owner2_since_month,\n                busowner_period_year        as owner2_since_year,\n                busowner_title              as owner2_title\n        from    businessowner\n        where   app_seq_num  =  :1  and\n                busowner_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.app.verisign.VaPrincipals",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,mesConstants.BUS_OWNER_SECONDARY);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.app.verisign.VaPrincipals",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:669^7*/
      setFields(it.getResultSet());
      it.close();

      loadOk = true;
    }
    catch (Exception e)
    {
      vappLogEntry("autoLoad()",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) { }
      if(wasStale)
      {
        cleanUp();
      }
    }

    return loadOk;
  }
}/*@lineinfo:generated-code*/