/*@lineinfo:filename=VaBase*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/verisign/VaBase.sqlj $

  Description:  
  
  VaBase
  
  Base class for all verisign virtual app page data beans.

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 7/19/04 11:19a $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.app.verisign;

import javax.servlet.http.HttpServletRequest;
import com.mes.app.AppBase;
import com.mes.constants.mesConstants;
import com.mes.forms.CurrencyField;
import com.mes.forms.Field;
import com.mes.support.HttpHelper;
import com.mes.user.UserBean;
import sqlj.runtime.ref.DefaultContext;

public class VaBase extends AppBase
{
  public VaBase()
  {
  }
  
  public VaBase(DefaultContext defCtx)
  {
    super(defCtx);
  }
  
  /*
  ** protected void createFields(HttpServletRequest request)
  **
  ** Sets the virtual app type.
  */
  protected void createFields(HttpServletRequest request)
  {
    // set the verisign virtual app app type 
    appType     = mesConstants.APP_TYPE_VERISIGN_V2;
    curScreenId = 1;
    
    super.createFields(request);
  }
  
  /*
  ** public void initFields(HttpServletRequest request)
  **
  ** Allow external access to createFields to support virtual app
  */
  public void initFields(HttpServletRequest request)
  {
    createFields(request);
  }
  
  
  /*
  ** protected void createNewApp(HttpServletRequest request)
  **
  ** Calls base class method then does some additional verisign specific app
  ** creation tasks.  In particular, the start and end pages need to be marked
  ** as completed.
  */
  protected void createNewApp(HttpServletRequest request)
  {
    // do base app create
    super.createNewApp(request);
    
    try
    {
      appSeq.setAppSeqNum(fields.getField("appSeqNum").asLong());
      appSeq.getFirstScreen().markAsComplete();
      appSeq.getLastScreen().markAsComplete();
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::createNewApp(): "
        + e.toString());
      logEntry( "createNewApp()", e.toString());
    }
  }
  
  /*
  ** protected void postHandleRequest(HttpServletRequest request)
  **
  ** If a user does not exists in the session one is created.  Unlike other apps,
  ** the verisign virtual app has non-users filling out apps.  This creates a
  ** situation that requires a special virtual app user to be generated for use
  ** in the session.  The user will be either the vs-virtualapp user for those
  ** users or the former long app user type.
  */
  protected void postHandleRequest(HttpServletRequest request)
  {
    // see if user exists in session already
    UserBean tmpUser = null;
    
    if(request != null)
    {
      tmpUser = (UserBean)(request.getSession().getAttribute("UserLogin"));
    }
    
    if(tmpUser != null)
    {
      user = tmpUser;
    }
    
    // this will need to have logic added to support long app users...
    if (user == null)
    {
      user = new UserBean();
      user.forceValidate("vsvirtualapp");
      
      if(request != null)
      {
        request.getSession().setAttribute("UserLogin",user);
      }
    }

    // call base beans handler
    super.postHandleRequest(request);
  }

  public static final int     FIDX_ADDR_LINE1             = 0;
  public static final int     FIDX_ADDR_LINE2             = 1;
  public static final int     FIDX_ADDR_CITY              = 2;
  public static final int     FIDX_ADDR_STATE             = 3;
  public static final int     FIDX_ADDR_ZIP               = 4;
  public static final int     FIDX_ADDR_COUNTRY           = 5;
  public static final int     FIDX_ADDR_PHONE             = 6;
  public static final int     FIDX_ADDR_FAX               = 7;
  
  protected void vappLogEntry(String source, String error)
  {
    StringBuffer newError = new StringBuffer("");
    
    try
    {
      newError.append("(");
      newError.append(fields.getData("appSeqNum"));
      newError.append("): ");
      newError.append(error);
      
      if(HttpHelper.isDebug(null))
      {
        System.out.println("*** VirtualApp Error ***");
        System.out.println("source: " + source);
        System.out.println("error:  " + error);
        System.out.println("************************");
      }
    }
    catch(Exception e)
    {}
    
    logEntry(source, error);
  }
  
  /*
  ** protected void storeAddressData(int addressType, Field[] addrFields)
  **
  ** Given an address type and an array of fields containing an address
  ** this method will store an address in the address table.
  */
  protected void storeAddressData(int addressType, Field[] addrFields)
  {
    try
    {
      long appSeqNum = fields.getField("appSeqNum").asLong();
      
      // don't insert if city or address line 1 is missing
      Field addr1 = addrFields[FIDX_ADDR_LINE1];
      Field city  = addrFields[FIDX_ADDR_CITY];
      if (addr1 != null && !addr1.isBlank() && city != null && !city.isBlank())
      {
        /*@lineinfo:generated-code*//*@lineinfo:196^9*/

//  ************************************************************
//  #sql [Ctx] { delete from address
//            where app_seq_num = :appSeqNum
//                  and addresstype_code = :addressType
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from address\n          where app_seq_num =  :1 \n                and addresstype_code =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.app.verisign.VaBase",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setInt(2,addressType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:201^9*/
      
        /*@lineinfo:generated-code*//*@lineinfo:203^9*/

//  ************************************************************
//  #sql [Ctx] { insert into address
//            ( address_line1,
//              address_line2,
//              address_city,
//              countrystate_code,
//              address_zip,
//              country_code,
//              address_phone,
//              address_fax,
//              app_seq_num,
//              addresstype_code )
//            values
//            ( :(addrFields[FIDX_ADDR_LINE1]  == null) ? null : addrFields[FIDX_ADDR_LINE1].getData(),
//              :(addrFields[FIDX_ADDR_LINE2]  == null) ? null : addrFields[FIDX_ADDR_LINE2].getData(),
//              :(addrFields[FIDX_ADDR_CITY]   == null) ? null : addrFields[FIDX_ADDR_CITY].getData(),
//              :(addrFields[FIDX_ADDR_STATE]  == null) ? null : addrFields[FIDX_ADDR_STATE].getData(),
//              :(addrFields[FIDX_ADDR_ZIP]    == null) ? null : addrFields[FIDX_ADDR_ZIP].getData(),
//              :(addrFields[FIDX_ADDR_COUNTRY]== null) ? null : addrFields[FIDX_ADDR_COUNTRY].getData(),
//              :(addrFields[FIDX_ADDR_PHONE]  == null) ? null : addrFields[FIDX_ADDR_PHONE].getData(),
//              :(addrFields[FIDX_ADDR_FAX]    == null) ? null : addrFields[FIDX_ADDR_FAX].getData(),
//              :appSeqNum,
//              :addressType )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3194 = (addrFields[FIDX_ADDR_LINE1]  == null) ? null : addrFields[FIDX_ADDR_LINE1].getData();
 String __sJT_3195 = (addrFields[FIDX_ADDR_LINE2]  == null) ? null : addrFields[FIDX_ADDR_LINE2].getData();
 String __sJT_3196 = (addrFields[FIDX_ADDR_CITY]   == null) ? null : addrFields[FIDX_ADDR_CITY].getData();
 String __sJT_3197 = (addrFields[FIDX_ADDR_STATE]  == null) ? null : addrFields[FIDX_ADDR_STATE].getData();
 String __sJT_3198 = (addrFields[FIDX_ADDR_ZIP]    == null) ? null : addrFields[FIDX_ADDR_ZIP].getData();
 String __sJT_3199 = (addrFields[FIDX_ADDR_COUNTRY]== null) ? null : addrFields[FIDX_ADDR_COUNTRY].getData();
 String __sJT_3200 = (addrFields[FIDX_ADDR_PHONE]  == null) ? null : addrFields[FIDX_ADDR_PHONE].getData();
 String __sJT_3201 = (addrFields[FIDX_ADDR_FAX]    == null) ? null : addrFields[FIDX_ADDR_FAX].getData();
   String theSqlTS = "insert into address\n          ( address_line1,\n            address_line2,\n            address_city,\n            countrystate_code,\n            address_zip,\n            country_code,\n            address_phone,\n            address_fax,\n            app_seq_num,\n            addresstype_code )\n          values\n          (  :1 ,\n             :2 ,\n             :3 ,\n             :4 ,\n             :5 ,\n             :6 ,\n             :7 ,\n             :8 ,\n             :9 ,\n             :10  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.app.verisign.VaBase",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3194);
   __sJT_st.setString(2,__sJT_3195);
   __sJT_st.setString(3,__sJT_3196);
   __sJT_st.setString(4,__sJT_3197);
   __sJT_st.setString(5,__sJT_3198);
   __sJT_st.setString(6,__sJT_3199);
   __sJT_st.setString(7,__sJT_3200);
   __sJT_st.setString(8,__sJT_3201);
   __sJT_st.setLong(9,appSeqNum);
   __sJT_st.setInt(10,addressType);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:227^9*/
      }
    }
    catch(Exception e)
    {
      System.out.println(this.getClass().getName() + "::storeAddressData(): "
        + e.toString());
      logEntry( "storeAddressData (" + addressType + "): ", e.toString());
    }
  }

  public class NumericOnlyField extends CurrencyField
  {
    public NumericOnlyField(String fname, String label, int length, int htmlSize, boolean nullAllowed)
    {
      super(fname, label, length, htmlSize, nullAllowed);
    }
    
    protected String processData(String rawData)
    {
      StringBuffer result = new StringBuffer("");
      try
      {
        // remove all non-digit chars (except decimal point)
        for(int i=0; i < rawData.length(); ++i)
        {
          if(Character.isDigit(rawData.charAt(i)) || rawData.charAt(i) == '.')
          {
            result.append(rawData.charAt(i));
          }
        }
        
        if(result.length() == 0)
        {
          result.append("0");
        }
      }
      catch(Exception e)
      {
      }
      
      return super.processData(result.toString());
    }
  }
}/*@lineinfo:generated-code*/