/*@lineinfo:filename=AppSeqNumGenerator*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/app/AppSeqNumGenerator.sqlj $

  AppSeqNumGenerator
  
  Tool for generating application sequence number and control number
  
  
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 9/23/03 1:59p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.app;

import java.util.Calendar;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ref.DefaultContext;

public class AppSeqNumGenerator extends SQLJConnectionBase
{
  private long    appSeqNum     = 0L;
  private long    controlNumber = 0L;
  
  public AppSeqNumGenerator(DefaultContext Ctx)
  {
    if(Ctx != null)
    {
      this.Ctx = Ctx;
    }
    
    generate();
  }
  
  public AppSeqNumGenerator()
  {
    generate();
  }
  
  public long getAppSeqNum()
  {
    return appSeqNum;
  }
  public long getControlNumber()
  {
    return controlNumber;
  }
  
  private void generate()
  {
    try
    {
      connect();
      
      // get new app seq num
      /*@lineinfo:generated-code*//*@lineinfo:70^7*/

//  ************************************************************
//  #sql [Ctx] { select  application_sequence.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  application_sequence.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.app.AppSeqNumGenerator",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:75^7*/
      
      // create new control number
      Calendar cal = Calendar.getInstance();
      controlNumber = 10000000000L + ((cal.get(Calendar.YEAR) - 2000) * 100000000L);
      controlNumber += (cal.get(Calendar.DAY_OF_YEAR) * 100000L);
      
      /*@lineinfo:generated-code*//*@lineinfo:82^7*/

//  ************************************************************
//  #sql [Ctx] { select  control_num_sequence.nextval + :controlNumber
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  control_num_sequence.nextval +  :1 \n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.app.AppSeqNumGenerator",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,controlNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   controlNumber = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:87^7*/
    }
    catch(Exception e)
    {
      logEntry("generate()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
}/*@lineinfo:generated-code*/