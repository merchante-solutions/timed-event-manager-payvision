/*@lineinfo:filename=SequenceScreen*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/SequenceScreen.sqlj $

  SequenceScreen
  
  Encapsulates a screen within a screen sequence.  Gives access to
  general information about the screen such as screen id and data bean
  classes associated with it.  If an instance id (app seq num) is specified
  then the class allows completeness checking and marking.
    
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002,2003 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.forms;

import java.sql.ResultSet;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class SequenceScreen extends SQLJConnectionBase
{
  public static final int ID_NOT_SET = -1;

  private int     seqId     = ID_NOT_SET;
  private int     screenId  = ID_NOT_SET;
  private long    instId    = ID_NOT_SET;   // appSeqNum
  
  private int     order;
  private String  className;
  private String  jsp;
  private String  description;

  
  public SequenceScreen()
  {
  }
  
  public SequenceScreen(DefaultContext defCtx)
  {
    if(defCtx == null)
    {
      Ctx = defCtx;
    }
  }
  
  /*
  ** private SequenceScreen(int seqId) throws SequenceException
  **
  ** Constructor.
  **
  ** This private constructor is used by the class's static methods that need
  ** an instance that is connected to the database for validations.
  */
  private void initSeq(int seqId)
    throws SequenceException
  {
    setSeqId(seqId);
  
  }
  private SequenceScreen(int seqId) throws SequenceException
  {
    initSeq(seqId);
  }
  
  /*
  ** public SequenceScreen(int seqId, int screenId) throws SequenceException
  **
  ** Constructor.
  */
  private void initSeqScreen(int seqId, int screenId) throws SequenceException
  {
    initSeq(seqId);
    setScreenId(screenId);
    loadDetails();
  }
  public SequenceScreen(int seqId, int screenId) throws SequenceException
  {
    initSeqScreen(seqId, screenId);
  }
  public SequenceScreen(int seqId, int screenId, DefaultContext defCtx)
    throws SequenceException
  {
    if(defCtx != null)
    {
      Ctx = defCtx;
    }
    initSeqScreen(seqId, screenId);
  }
  
  /*
  ** public SequenceScreen(int seqId, ResultSet rs) throws SequenceException
  **
  ** Constructor.
  */
  private void initSeqRs(int seqId, ResultSet rs) throws SequenceException
  {
    try
    {
      this.seqId    = seqId;
      this.screenId = rs.getInt("screen_id");
      loadDetails(rs);
    }
    catch (Exception e)
    {
      throw new SequenceException("Screen constructor error (" + idString()
        + "): " + e.toString());
    }
  }
  public SequenceScreen(int seqId, ResultSet rs) throws SequenceException
  {
    initSeqRs(seqId, rs);
  }
  public SequenceScreen(int seqId, ResultSet rs, DefaultContext defCtx)
    throws SequenceException
  {
    if(defCtx != null)
    {
      Ctx = defCtx;
    }
    initSeqRs(seqId, rs);
  }
  
  /*
  ** private void setSeqId(int seqId) throws SequenceException
  **
  ** Sets the screen sequence id.  Exception is thrown if invalid seq id given.
  */
  private void setSeqId(int seqId) throws SequenceException
  {
    this.seqId = seqId;
  }
  
  /*
  ** private void setScreenId(int screenId) throws SequenceException
  **
  ** Sets the screen id.  Throws exception if screen id is invalid.
  */
  private void setScreenId(int screenId) throws SequenceException
  {
    validateScreenId(screenId);
    this.screenId = screenId;
  }
  
  /*
  ** private void validateScreenId(int screenId) throws SequenceException
  **
  ** Determines if the given screen id is valid within the current sequence.
  ** Throws exception if screen id is invalid.
  */
  private void validateScreenId(int screenId) throws SequenceException
  {
    boolean wasStale = false;
    
    if (seqId == ID_NOT_SET)
    {
      throw new SequenceException("Attempt to validate screen before sequence set");
    }
    
    // validate the screen id
    ResultSetIterator it = null;
    boolean isValidScreen = false;
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      /*@lineinfo:generated-code*//*@lineinfo:187^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  screen_id
//          from    screen_sequence
//          where   screen_sequence_id = :seqId
//                  and screen_id = :screenId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  screen_id\n        from    screen_sequence\n        where   screen_sequence_id =  :1 \n                and screen_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.forms.SequenceScreen",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,seqId);
   __sJT_st.setInt(2,screenId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.forms.SequenceScreen",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:193^7*/
      isValidScreen = it.getResultSet().next();
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::validateScreenId(), " 
        + idString() + ": " + e.toString());
      logEntry("validateScreenId()",idString() + ": " + e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      if(wasStale)
      {
        cleanUp();
      }
    }

    if (!isValidScreen)
    {
      throw new SequenceException("Screen id not valid (" + idString() + ")");
    }
  }
  
  /*
  ** public static void validateScreenId(int seqId, int screenId)
  **   throws SequenceException
  **
  ** Public static method that allows a screen id to be validated against the
  ** given sequence id.
  */
  public static void validateScreenId(int seqId, int screenId)
    throws SequenceException
  {
    (new SequenceScreen(seqId)).validateScreenId(screenId);
  }
  
  /*
  ** public void setInstId(long instId)
  **
  ** Sets the instance id (appSeqNum).
  */
  public void setInstId(long instId)
  {
    this.instId = instId;
  }

  /*
  ** public void loadDetails(ResultSet rs)
  **
  ** Loads screen detail info from the result set.
  */
  public void loadDetails(ResultSet rs)
  {
    try
    {
      setOrder        (rs.getInt   ("screen_order"      ));
      setClassName    (rs.getString("screen_classname"  ));
      setJsp          (rs.getString("screen_jsp"        ));
      setDescription  (rs.getString("screen_description"));
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::loadDetails(rs), "
        + idString() + ": " + e.toString());
      logEntry("loadDetails(rs)",idString() + ": " + e.toString());
    }
  }
  
  /*
  ** private void loadDetails()
  **
  ** Loads screen order, class name, jsp and description for the current sequence
  ** and screen id's.
  */
  private void loadDetails()
  {
    ResultSetIterator it = null;
    ResultSet         rs = null;
    boolean           wasStale = false;
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:281^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  screen_order,
//                  screen_classname,
//                  screen_jsp,
//                  screen_description
//          from    screen_sequence
//          where   screen_sequence_id = :seqId
//                  and screen_id = :screenId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  screen_order,\n                screen_classname,\n                screen_jsp,\n                screen_description\n        from    screen_sequence\n        where   screen_sequence_id =  :1 \n                and screen_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.forms.SequenceScreen",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,seqId);
   __sJT_st.setInt(2,screenId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.forms.SequenceScreen",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:290^7*/
      rs = it.getResultSet();

      if (rs.next())
      {      
        loadDetails(rs);
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::loadDetails(), "
        + idString() + ": " + e.toString());
      logEntry("loadDetails()",idString() + ": " + e.toString());
    }
    finally
    {
      try { it.close();  } catch (Exception e) {}
      if(wasStale)
      {
        cleanUp();
      }
    }
  }

  public void setOrder(int order)
  {
    this.order = order;
  }
  public void setClassName(String className)
  {
    this.className = className;
  }
  public void setJsp(String jsp)
  {
    this.jsp = jsp;
  }
  public void setDescription(String description)
  {
    this.description = description;
  }
  
  public int    getOrder()        { return order; }
  public String getClassName()    { return className; }
  public String getJsp()          { return jsp; }
  public String getDescription()  { return description; }
  
  public long   getInstId()       { return instId; }
  public int    getScreenId()     { return screenId; }
  public int    getSeqId()        { return seqId; }
  
  private String idString()
  {
    return "seqId=" + seqId + ", screenId=" + screenId + ", instId=" + instId;
  }
  
  // local storage for completeness checking
  private boolean bitFieldsLoaded = false;
  private int     bitFields       = 0;

  /*
  ** private int getBitFields() throws SequenceException
  **
  ** Throws exception if id's not set.
  **
  ** RETURNS: the screen's bit fields in screen_progress, -1 if none found.
  */
  private int getBitFields() throws SequenceException
  {
    ResultSetIterator   it    = null;
    ResultSet           rs    = null;
    
    // can't mark completene without id's
    if (instId == ID_NOT_SET || screenId == ID_NOT_SET || seqId == ID_NOT_SET)
    {
      throw new SequenceException("Attempt to retrieve bit fields without "
        + "id(s) set (" + idString() + ")");
    }
    
    // load the bit fields from screen_progress
    if (!bitFieldsLoaded)
    {
      try
      {
        connect(); 
      
        // get current bit set
        /*@lineinfo:generated-code*//*@lineinfo:376^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  screen_complete
//            from    screen_progress
//            where   screen_sequence_id = :seqId
//                    and screen_pk = :instId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  screen_complete\n          from    screen_progress\n          where   screen_sequence_id =  :1 \n                  and screen_pk =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.forms.SequenceScreen",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,seqId);
   __sJT_st.setLong(2,instId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.forms.SequenceScreen",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:382^9*/
        
        rs = it.getResultSet();
        
        if(rs.next())
        {
          bitFields = rs.getInt("screen_complete");
        }
        
        rs.close();
        it.close();
      }
      catch (Exception e)
      {
        System.out.println(this.getClass().getName() + "getBitFields(), "
          + idString() + ": " + e.toString());
        logEntry("getBitFields()",idString() + ": " + e.toString());
      }
      finally
      {
        try { rs.close(); } catch(Exception e) {}
        try { it.close(); } catch(Exception e) {}
        cleanUp();
      }
      bitFieldsLoaded = true;
    }
    
    return bitFields;
  }
  
  /*
  ** public boolean isComplete() throws SequenceException
  **
  ** Determines if a page has been completed (checks screen_progress).
  ** Exception is thrown if no instance id has been assigned to the screen.
  ** This routine uses the new screen id scheme where the bit position is
  ** implied by the id rather than the id BEING the bit flag.
  **
  ** RETURNS: true if page has been marked as completed, else false.
  */
  public boolean isComplete() throws SequenceException
  {
    // make this screen's bit
    int screenBit = (1 << (screenId - 1));
    
    // determine if the screen bit is set in bitFields
    return ((screenBit & getBitFields()) == screenBit);
  }
  
  /*
  ** private void updateBitFields(int bitFields) throws SequenceException
  **
  ** Stores the bit fields value in the data base.  Throws exception if id's
  ** are not set.
  */
  private void updateBitFields(int bitFields) throws SequenceException
  {
    boolean wasStale = false;
    // can't mark completene without id's
    if (instId == ID_NOT_SET || screenId == ID_NOT_SET || seqId == ID_NOT_SET)
    {
      throw new SequenceException("Attempt update bit fields without "
        + "id(s) set (" + idString() + ")");
    }
    
    try
    {
      connect(); 
      
      // update the bit set in the database
      /*@lineinfo:generated-code*//*@lineinfo:452^7*/

//  ************************************************************
//  #sql [Ctx] { update  screen_progress
//          set     screen_complete = :bitFields
//          where   screen_sequence_id = :seqId
//                  and screen_pk = :instId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  screen_progress\n        set     screen_complete =  :1 \n        where   screen_sequence_id =  :2 \n                and screen_pk =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.forms.SequenceScreen",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bitFields);
   __sJT_st.setInt(2,seqId);
   __sJT_st.setLong(3,instId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:458^7*/
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::updateBitFields(), " 
        + idString() + ": " + e.toString());
      logEntry("updateBitFields()",idString() + ": " + e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    this.bitFields = bitFields;
  }
  
  /*
  ** public void markAsComplete() throws SequenceException
  **
  ** Marks a page as complete in screen_progress.  Throws exception if id's have
  ** not been set.
  */
  public void markAsComplete() throws SequenceException
  {
    // update this instance's bit fields to have this screen's bit turned on
    updateBitFields(getBitFields() | (1 << (screenId - 1)));
  }
  
  /*
  ** public void markAsIncomplete() throws SequenceException
  **
  ** Clears a page's bit in screen_progress.  Throws exception if id's have not
  ** been set.
  */
  public void markAsIncomplete() throws SequenceException
  {
    updateBitFields(getBitFields() & (~(1 << (screenId - 1))));
  }
}/*@lineinfo:generated-code*/