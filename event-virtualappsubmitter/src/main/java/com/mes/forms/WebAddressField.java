/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/WebAddressField.java $

  Description:
  
  WebAddressField
  
  A field containing a website address.  Validates address.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.StringTokenizer;

public class WebAddressField extends Field
{
  public class WebAddressValidation implements Validation
  {
    private String errorText = "Invalid web address";
    
    public class ValidationException extends Exception
    {
      public ValidationException(String msg)
      {
        super(msg);
      }
    }
    
    public boolean validate(String fieldData)
    {
      // allow blanks
      if (fieldData == null || fieldData.equals(""))
      {
        return true;
      }
      
      // get the actual website (discard anything after a '/')
      StringTokenizer tok = new StringTokenizer(fieldData,"/");
      String website = tok.nextToken();
      tok = new StringTokenizer(website,".");
      boolean isValid = false;
      String tld = null;
      
      // requires 'www' at front, one or more middle parts 
      // and a top level domain that matches the list of 
      // accepted tld's
      try
      {
        if (!tok.nextToken().equals("www"))
        {
          throw new ValidationException("Must start with 'www'");
        }

        int partCount = 0;
        String part = null;
        while (tok.hasMoreTokens())
        {
          part = tok.nextToken();
          if (part.length() == 0)
          {
            throw new ValidationException("Invalid web address");
          }
          ++partCount;
        }
        if (partCount < 2)
        {
          throw new ValidationException("Invalid web address");
        }
        
        String[] validTlds =
        {
          "com", "edu", "mil", "gov", "net", "org", "aero",
          "biz", "coop", "info", "int", "museum", "name", "pro"
        };

        boolean isValidTld = false;
        tld = part;
        for (int i = 0; i < validTlds.length; ++i)
        {
          if (tld.equals(validTlds[i]))
          {
            isValidTld = true;
            break;
          }
        }
        if (!isValidTld)
        {
          throw new ValidationException("Invalid domain (." + tld + ")");
        }
        
        isValid = true;
      }
      catch (ValidationException ve)
      {
        errorText = ve.getMessage();
      }
      catch (Exception e)
      {
      }

      return isValid;        
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }
  
  public WebAddressField(String fname,
                         String label,
                         int length,
                         int htmlSize,
                         boolean nullAllowed)
  {
    super(fname,label,length,htmlSize,nullAllowed);
    addValidation(new WebAddressValidation());
  }
  public WebAddressField(String fname,
                         int length,
                         int htmlSize,
                         boolean nullAllowed)
  {
    this(fname,fname,length,htmlSize,nullAllowed);
  }
  public WebAddressField(String fname,
                         boolean nullAllowed)
  {
    this(fname,fname,75,32,nullAllowed);
  }

}
