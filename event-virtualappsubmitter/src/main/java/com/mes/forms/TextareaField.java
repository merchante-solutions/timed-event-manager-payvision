/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/TextareaField.java $

  Description:
  
  TextareaField
  
  Renders an html textarea field.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class TextareaField extends Field
{
  private int rows;
  private int cols;
  
  public TextareaField(String fname, 
                       String label,
                       int length, 
                       int rows, 
                       int cols, 
                       boolean nullAllowed)
  {
    super(fname,label,length,0,nullAllowed);
    this.rows = rows;
    this.cols = cols;
  }
  public TextareaField(String fname, 
                       int length, 
                       int rows, 
                       int cols, 
                       boolean nullAllowed)
  {
    this(fname,fname,length,rows,cols,nullAllowed);
  }
  
  public String renderHtmlField()
  {
    StringBuffer html = new StringBuffer();

    html.append("<textarea ");
    html.append("name=\"" + fname + "\" ");
    html.append("rows=\"" + rows + "\" ");
    html.append("cols=\"" + cols + "\" ");
    html.append(getHtmlExtra());
    html.append(">");
    html.append(fdata);
    html.append("</textarea>");
    
    return html.toString();
  }
}
