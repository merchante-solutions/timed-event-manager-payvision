/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/CaseInsensitiveDropDownField.java $

  Description:
  
  DropDownField
  
  Wraps a DropDownTable and provides a renderer for the html select tag.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import com.mes.tools.DropDownTable;

public class CaseInsensitiveDropDownField extends DropDownField
{
  public CaseInsensitiveDropDownField(String fname, String label,
    DropDownTable dropDown, boolean nullAllowed)
  {
    super(fname, label, dropDown, nullAllowed);
  }
  
  public CaseInsensitiveDropDownField(String fname, DropDownTable dropDown,
    boolean nullAllowed)
  {
    super(fname, dropDown, nullAllowed);
  }
  
  protected String processData(String rawData)
  {
    return super.processData(rawData.toUpperCase());
  }
}
