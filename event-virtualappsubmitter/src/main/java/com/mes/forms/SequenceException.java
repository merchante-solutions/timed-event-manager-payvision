/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/SequenceException.java $

  Description:  
  
    Sequence
    
    Provides methods for getting info about a screen sequence, allows
    navigation between individual screens of the sequence, lets screen
    completeness be set and retrieved for specific instances of a
    screen sequence.


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.forms;

public class SequenceException extends Exception
{
  public SequenceException(String message)
  {
    super(message);
  }
}