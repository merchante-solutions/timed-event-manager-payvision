/*@lineinfo:filename=Sequence*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/Sequence.sqlj $

  Sequence
  
  Provides methods for getting info about a screen sequence, allows
  navigation between individual screens of the sequence, lets screen
  completeness be set and retrieved for specific instances of a
  screen sequence.


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001,2002 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.forms;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.Vector;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class Sequence extends SQLJConnectionBase
{
  public static final int ID_NOT_SET = -1;
  
  private int     seqId       = ID_NOT_SET;
  private int     curScreenId = ID_NOT_SET;
  private long    instId      = ID_NOT_SET;   // appSeqNum
  
  private Vector  screens     = null;
  
  /*
  ** public Sequence()
  **
  ** Constructor.
  */
  public Sequence()
  {
  }
  
  public Sequence(DefaultContext defCtx)
  {
    if(defCtx != null)
    {
      this.Ctx = defCtx;
    }
  }
  
  /*
  ** public Sequence(int seqId) throws SequenceException
  **
  ** Constructor.
  **
  ** Sets the seqId.
  */
  public Sequence(int seqId) throws SequenceException
  {
    setSeqId(seqId);
  }
  
  /*
  ** public Sequence(int seqId, int curScreenId) throws SequenceException
  **
  ** Constructor.
  */
  public Sequence(int seqId, int curScreenId) throws SequenceException
  {
    setSeqId(seqId);
    setCurScreenId(curScreenId);
  }
  
  /*
  ** public void setSeqId(int seqId) throws SequenceException
  **
  ** Sets the screen sequence id. Throws exception if seq id does not exist in
  ** screen_sequence.  Setting the sequence causes the current screen and 
  ** instance id to be reset.
  */
  public void setSeqId(int seqId) throws SequenceException
  {
    // ignore if already set to this seq id
    if (this.seqId == seqId)
    {
      return;
    }

    // validate the sequence
    validateSeqId(seqId);
    
    // set the new sequence
    this.seqId = seqId;
    
    // reset screens vector
    screens = new Vector();
    
    // load sequence screens
    ResultSetIterator it = null;
    ResultSet         rs = null;
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:122^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  screen_id,
//                  screen_order,
//                  screen_classname,
//                  screen_jsp,
//                  screen_description
//          from    screen_sequence
//          where   screen_sequence_id = :seqId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  screen_id,\n                screen_order,\n                screen_classname,\n                screen_jsp,\n                screen_description\n        from    screen_sequence\n        where   screen_sequence_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.forms.Sequence",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,seqId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.forms.Sequence",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:131^7*/
      rs = it.getResultSet();
      
      while (rs.next())
      {
        screens.add(new SequenceScreen(seqId,rs,Ctx));
      }
      
      rs.close();
      it.close();
    }
    catch (Exception e)
    {
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    // default cur screen to be first screen in sequence
    curScreenId = getFirstScreen().getScreenId();
    
    instId      = ID_NOT_SET;
  }
  
  /*
  ** private void validateSeqId(int seqId) throws SequenceException
  **
  ** Makes sure the sequence id is valid by checking for it in the
  ** screen_sequence table.  Exception is thrown if not in table.
  */
  private void validateSeqId(int seqId) throws SequenceException
  {
    boolean wasStale = false;
    
    // don't waste time validating seq id if it is the
    // current seq id (has to have been already validated)
    if (this.seqId == seqId)
    {
      return;
    }
    
    // make sure screen id is valid with current seq id
    ResultSetIterator it = null;
    boolean isValidSequence = false;
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      /*@lineinfo:generated-code*//*@lineinfo:186^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  screen_sequence_id
//          from    screen_sequence
//          where   screen_sequence_id = :seqId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  screen_sequence_id\n        from    screen_sequence\n        where   screen_sequence_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.forms.Sequence",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,seqId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.forms.Sequence",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:191^7*/
      isValidSequence = it.getResultSet().next();
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::validateSeqId(): "
        + "Exception validating sequence id " + seqId + ": " + e.toString());
      logEntry("validateSeqId()","Exception validating sequence id " + seqId
        + ": " + e.toString());
      throw new SequenceException("Failed to validate sequence id: " + e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      if(wasStale)
      {
        cleanUp();
      }
    }

    if (!isValidSequence)
    {
      throw new SequenceException("Sequence id " + seqId + " is not valid");
    }
  }
  
  /*
  ** public void static validateSequenceId(int seqId) throws SequenceException
  **
  ** Public convenience method that will validate a sequence id.  Exception is
  ** thrown if sequence id is not valid.
  */
  public static void validateSequenceId(int seqId) throws SequenceException
  {
    (new Sequence()).validateSeqId(seqId);
  }
  
  /*
  ** public void setCurScreenId(int curScreenId) throws SequenceException
  **
  ** Sets the current screen id.  The sequence id must be valid or an exception 
  ** is thrown.  The screen id must be a valid id within the sequence or an
  ** exception is thrown.  The sequence instance id is reset.
  */
  public void setCurScreenId(int curScreenId) throws SequenceException
  {
    if (seqId == ID_NOT_SET)
    {
      throw new SequenceException("Attempt to set screen with no sequence set (" + idString() + ")");
    }
    
    for (Iterator i = screens.iterator(); i.hasNext();)
    {
      SequenceScreen screen = (SequenceScreen)i.next();
      if (screen.getScreenId() == curScreenId)
      {
        this.curScreenId = curScreenId;
        return;
      }
    }
    
    throw new SequenceException("Invalid screen id " + curScreenId + " (" 
      + idString() + ")");
  }
  
  /*
  ** public void setInstId(long instId) throws SequenceException
  **
  ** Sets the sequence instance id.  This is a specific instance of a screen
  ** sequence (i.e. an appSeqNum).  Exception thrown if instance id is invalid.
  ** A record is automatically created in screen_progress if one does not exist.
  ** This is how new screen instances are created.
  */
  public void setInstId(long instId) throws SequenceException
  {
    boolean wasStale = false;
    
    // can't set inst id if a seq id has not been set
    if (seqId == ID_NOT_SET)
    {
      throw new SequenceException("Cannot validate instance id " + instId 
        + " before setting sequence");
    }
    
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      // look for an instance of this seq for this instance
      int count = 0;
      /*@lineinfo:generated-code*//*@lineinfo:285^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(screen_pk)
//          
//          from    screen_progress
//          where   screen_sequence_id = :seqId
//                  and screen_pk = :instId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(screen_pk)\n         \n        from    screen_progress\n        where   screen_sequence_id =  :1 \n                and screen_pk =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.forms.Sequence",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,seqId);
   __sJT_st.setLong(2,instId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:292^7*/
      
      // if no record exists, create one
      if (count == 0)
      {
        /*@lineinfo:generated-code*//*@lineinfo:297^9*/

//  ************************************************************
//  #sql [Ctx] { insert into screen_progress
//            ( screen_sequence_id,
//              screen_pk,
//              screen_complete )
//            values
//            ( :seqId,
//              :instId,
//              0 )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into screen_progress\n          ( screen_sequence_id,\n            screen_pk,\n            screen_complete )\n          values\n          (  :1 ,\n             :2 ,\n            0 )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.forms.Sequence",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,seqId);
   __sJT_st.setLong(2,instId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:307^9*/
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::setInstId(" + instId 
        + "), " + idString() + ": "  + e.toString());
      logEntry("setInstId(" + instId + ")",idString() + ": " + e.toString());
      throw new SequenceException("Failed to set instance id " + instId
        + " (" + idString() + "): " + e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }
    
    // set the instance in each screen object
    for (Iterator i = screens.iterator(); i.hasNext();)
    {
      SequenceScreen screen = (SequenceScreen)i.next();
      screen.setInstId(instId);
    }
    
    // set the actual instId member
    this.instId = instId;
  }
  
  /*
  ** public void validateInstId(long instId) throws SequenceException
  **
  ** Validates an instId.  If the seq id has not yet been set or there is no
  ** entry in screen_progress with the current seq id then an exception is thrown.
  */
  public void validateInstId(long instId) throws SequenceException
  {
    boolean wasStale = false;
    // can't validate inst id if a seq id has not been set
    if (seqId == ID_NOT_SET)
    {
      throw new SequenceException("Cannot validate instance id " + instId 
        + " before setting sequence");
    }
    
    // make sure screen id is valid with current seq id
    ResultSetIterator it = null;
    boolean isValidInstance = false;
    try
    {
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      /*@lineinfo:generated-code*//*@lineinfo:363^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  screen_pk
//          from    screen_progress
//          where   screen_sequence_id = :seqId
//                  and screen_pk = :instId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  screen_pk\n        from    screen_progress\n        where   screen_sequence_id =  :1 \n                and screen_pk =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.forms.Sequence",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,seqId);
   __sJT_st.setLong(2,instId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.forms.Sequence",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:369^7*/
      isValidInstance = it.getResultSet().next();
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::validateInstId(), " 
        + idString() + ": " + e.toString());
      logEntry("validateInstId()",idString() + ": " + e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      if(wasStale)
      {
        cleanUp();
      }
    }

    if (!isValidInstance)
    {
      throw new SequenceException("Attempt to set invalid instance " + instId
        + " (" + idString() + ")");
    }
  }

  public int    getSeqId()        { return seqId; }
  public int    getCurScreenId()  { return curScreenId; }
  public long   getInstId()       { return instId; }

  private String idString()
  {
    return "seqId=" + seqId + ", instId=" + instId;
  }
  
  /*
  ** private int getScreenIndex(int screenId)
  **
  ** Scans screens vector to find vector element index of screen matching the
  ** given screenId.
  **
  ** RETURNS: index of screen in screens Vector, -1 if not found.
  */
  private int getScreenIndex(int screenId)
  {
    for (int i = 0; i < screens.size(); ++i)
    {
      SequenceScreen screen = (SequenceScreen)screens.elementAt(i);
      if (screen.getScreenId() == curScreenId)
      {
        return i;
      }
    }
    return -1;
  }

  /*
  ** private void validateSeqSet() throws SequenceException
  **
  ** Throws exception if seqId has not been set.  Used by get*Screen() methods.
  */
  private void validateSeqSet() throws SequenceException
  {
    if (seqId == ID_NOT_SET)
    {
      throw new SequenceException("Sequence id not set (" + idString() + ")");
    }
  }
    
  /*
  ** private void validateSeqAndScreenSet() throws SequenceException
  **
  ** Throws exception if seqId or curScreenId have not been set.  Used by the
  ** get*Screen() methods.
  */
  private void validateSeqAndScreenSet() throws SequenceException
  {
    validateSeqSet();
    if (curScreenId == ID_NOT_SET)
    {
      throw new SequenceException("Current screen not set (" + idString() + ")");
    }
  }
    
    
  /*
  ** public SequenceScreen getCurrentScreen() throws SequenceException
  **
  ** Returns the current screen object.  Throws exception if seq or screen
  ** ids have not been set.
  **
  ** RETURNS: the current screen object, null if current screen id is not found.
  */
  public SequenceScreen getCurrentScreen() throws SequenceException
  {
    validateSeqAndScreenSet();
    
    // locate the cur screen and return it
    int screenIdx = getScreenIndex(curScreenId);
    if (screenIdx != -1)
    {
      return (SequenceScreen)screens.elementAt(screenIdx);
    }
    return null;
  }
  
  /*
  ** public SequenceScreen getNextScreen() throws SequenceException
  **
  ** Finds the next screen if any in the sequence.  Throws exception if id's
  ** have not been set.
  **
  ** RETURNS: the next screen in the sequence.  If current screen is last in
  **          sequence, null is returned.
  */
  public SequenceScreen getNextScreen() throws SequenceException
  {
    validateSeqAndScreenSet();
    
    // locate the next screen and return it
    int screenIdx = getScreenIndex(curScreenId);
    if (screenIdx != -1 && (screenIdx < (screens.size() - 1)))
    {
      return (SequenceScreen)screens.elementAt(screenIdx + 1);
    }
    return null;
  }

  /*
  ** public SequenceScreen getPrevScreen() throws SequenceException
  **
  ** Finds the previous screen if any in the sequence.  Throws exception if id's
  ** have not been set.
  **
  ** RETURNS: the previous screen in the sequence.  If current screen is first in
  **          sequence, null is returned.
  */
  public SequenceScreen getPrevScreen() throws SequenceException
  {
    validateSeqAndScreenSet();
    
    // locate the next screen and return it
    int screenIdx = getScreenIndex(curScreenId);
    if (screenIdx != -1 && screenIdx > 0)
    {
      return (SequenceScreen)screens.elementAt(screenIdx - 1);
    }
    return null;
  }
  
  /*
  ** public SequenceScreen getFirstScreen() throws SequenceException
  **
  ** RETURNS: the first screen in the sequence.
  */
  public SequenceScreen getFirstScreen() throws SequenceException
  {
    validateSeqSet();
    return (SequenceScreen)screens.firstElement();
  }
  
  /*
  ** public SequenceScreen getLastScreen() throws SequenceException
  **
  ** RETURNS: the last screen in the sequence.
  */
  public SequenceScreen getLastScreen() throws SequenceException
  {
    validateSeqSet();
    return (SequenceScreen)screens.lastElement();
  }
  
  /*
  ** public SequenceScreen getFirstIncompleteScreen() throws SequenceException
  **
  ** RETURNS: first incomplete screen or null if all are complete.
  */
  public SequenceScreen getFirstIncompleteScreen() throws SequenceException
  {
    validateSeqSet();
    for (Iterator i = screens.iterator(); i.hasNext();)
    {
      SequenceScreen screen = (SequenceScreen)i.next();
      if (!screen.isComplete())
      {
        return screen;
      }
    }
    return null;
  }
  
  /*
  ** public SequenceScreen getScreenWithId(int screenId) throws SequenceException
  **
  ** RETURNS: screen with specified id, null if none found.
  */
  public SequenceScreen getScreenWithId(int screenId) throws SequenceException
  {
    validateSeqSet();
    for (Iterator i = screens.iterator(); i.hasNext();)
    {
      SequenceScreen screen = (SequenceScreen)i.next();
      if (screen.getScreenId() == screenId)
      {
        return screen;
      }
    }
    return null;
  }
  
  /*
  ** public Vector getScreens()
  **
  ** RETURNS: the screens Vector.
  */
  public Vector getScreens()
  {
    return screens;
  }
}/*@lineinfo:generated-code*/