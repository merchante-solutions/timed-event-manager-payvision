/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/DisabledCheckboxField.java $

  Description:
  
  CheckboxField
  
  Renders a disabled checkbox html field.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2008-07-07 15:59:41 -0700 (Mon, 07 Jul 2008) $
  Version            : $Revision: 15046 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

public class DisabledCheckboxField extends CheckboxField
{
  public DisabledCheckboxField( String fname, String label, boolean checked )
  {
    super(fname,label,checked);
    addHtmlExtra("disabled");
  }
  
  public DisabledCheckboxField( String fname, String label, boolean checked, boolean doShowLabel )
  {
    super(fname,label,checked,doShowLabel);
    addHtmlExtra("disabled");
  }
  
  /*
  ** public void setHtmlExtra(String htmlExtra)
  **
  ** Overloads base method to keep the disabled attribute in the htmlExrtra
  ** String.
  */
  public void setHtmlExtra(String htmlExtra)
  {
    super.setHtmlExtra("disabled " + htmlExtra);
  }
  
  // overload process so that the checkbox state remains static
  protected String processData(String rawData)
  {
    return( this.fdata );
  }

  // overload reset so that the checkbox state remains static
  public void reset()
  {
  }
}
