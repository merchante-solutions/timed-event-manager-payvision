/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/forms/EmailField.java $

  Description:
  
  EmailField
  
  A field containing an email address.  Validates email format.
  
  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2007-07-13 16:13:30 -0700 (Fri, 13 Jul 2007) $
  Version            : $Revision: 13882 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

***************************************************************************/

package com.mes.forms;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

public class EmailField extends Field
{
  public class EmailAddressValidation implements Validation
  {
    private String errorText = "";

    /*
    ** public boolean validate(String fieldData)
    **
    ** Determines if fieldData contains one or more valid email
    ** addresses.  Multiple addresses may be specified, delimitted
    ** by semi-colons or commas.  Addresses validation consists
    ** of the following:
    **
    **    - must have minimum length of 6
    **    - may not contain invalid chars
    **    - must contain only one '@' which may not be left-most 
    **      or within 4 chars of right most char
    **    - must contain one or more periods (the right most of which must
    **      be either 3rd or 4th from right, and no two of which are allowed
    **      to be next to each other)
    **    - may not contain '.@', '@.', or '..'
    **
    ** RETURNS: true if fieldData contains only valid email address,
    **          else false.
    */
    public boolean validate(String fieldData)
    {
      // treat null or empty field data as valid
      if (fieldData == null || fieldData.length() == 0)
      {
        return true;
      }
      
      boolean isValid = true;
      
      try
      {
        // generate a list of email addresses
        StringTokenizer tok = new StringTokenizer(fieldData,";, ");
        ArrayList addresses = new ArrayList();
        while (tok.hasMoreTokens())
        {
          String address = tok.nextToken();
          addresses.add(address);
        }
      
        // validate each address
        for (Iterator i = addresses.iterator(); i.hasNext();)
        {
          String address = (String)i.next();
        
          // minimum length of 6
          if (address.length() < 6)
          {
            if (address.length() == 0)
            {
              throw new Exception("Invalid empty address");
            }
            throw new Exception("Address '" + address + "' is too short");
          }
        
          // scan characters in email address
          String invalidChars = "!#$%^&*()=+{}[]|\\;:'/?>,< ";
          int atCount = 0;
          int dotCount = 0;
          int rightDotIndex = 0;
          for (int j = 0; j < address.length(); ++j)
          {
            char ch = address.charAt(j);
          
            // no invalid characters
            if (invalidChars.indexOf(ch) != -1)
            {
              throw new Exception("Address '" + address
                + "' contains invalid characters");
            }
            
            // note '.'
            if (ch == '.')
            {
              rightDotIndex = j;
              dotCount++;
              
              if (j == 0)
              {
                throw new Exception("Address '" + address
                  + "' has '.' too far left");
              }
            }
            
            // validate '@'
            if (ch == '@')
            {
              // more than one '@'              
              if (++atCount > 1)
              {
                throw new Exception("Address '" + address
                  + "' contains multiple '@' characters");
              }
              // leftmost '@'
              if (j == 0)
              {
                throw new Exception("Address '" + address
                  + "' has '@' as leftmost");
              }
              // too far right '@'
              if (j > address.length() - 4)
              {
                throw new Exception("Address '" + address
                  + "' has '@' too far right");
              }
            }
          }
          
          // must have at least one '@'
          if (atCount < 1)
          {
            throw new Exception("Address '" + address + "' must contain '@'");
          }
          
          // must have at least one dot
          if (dotCount < 1)
          {
            throw new Exception("Address '" + address
              + "' must contain '.'");
          }
          
          // right most '.' must be followed by two or three or four chars
          if (rightDotIndex != address.length() - 5
              && rightDotIndex != address.length() - 4
              && rightDotIndex != address.length() - 3)
          {
            throw new Exception("Address '" + address
              + "' has an invalid suffix");
          }
          
          // invalid combos
          if (address.indexOf(".@") != -1 || address.indexOf("@.") != -1
              || address.indexOf("..") != -1)
          {
            throw new Exception("Address '" + address
              + "' may not contain '.@', '@.' or '..'");
          }
        }
      }
      catch (Exception e)
      {
        errorText = e.getMessage();
        isValid = false;
      }
      
      return isValid;
    }
    
    public String getErrorText()
    {
      return errorText;
    }
  }

  public EmailField(String fname,
                    String label,
                    int length,
                    int htmlSize,
                    boolean nullAllowed)
  {
    super(fname,label,length,htmlSize,nullAllowed);
    addValidation(new EmailAddressValidation());
  }
  public EmailField(String fname,
                    int length,
                    int htmlSize,
                    boolean nullAllowed)
  {
    this(fname,fname,length,htmlSize,nullAllowed);
  }
  
  public EmailField(String fname,
                    String label,
                    boolean nullAllowed)
  {
    super(fname,label,75,32,nullAllowed);
    addValidation(new EmailAddressValidation());
  }
  public EmailField(String fname,
                    boolean nullAllowed)
  {
    this(fname,fname,nullAllowed);
  }                    
  
  /*
  ** protected String processData(String rawData)
  **
  ** Massages multiple email addresses into better format if needed.
  **
  ** RETURNS: reformatted data.
  */
  protected String processData(String rawData)
  {
    StringTokenizer tok = new StringTokenizer(rawData,";, ");
    StringBuffer processedData = new StringBuffer();
    boolean isFirst = true;
    while (tok.hasMoreTokens())
    {
      if (!isFirst)
      {
        processedData.append(";");
      }
      else
      {
        isFirst = false;
      }
      processedData.append(tok.nextToken());
    }
    
    return processedData.toString();
  }
}
