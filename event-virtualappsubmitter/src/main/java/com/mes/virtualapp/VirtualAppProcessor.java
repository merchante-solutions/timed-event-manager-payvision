/*@lineinfo:filename=VirtualAppProcessor*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/virtualapp/VirtualAppProcessor.sqlj $

  Description:

    VirtualAppProcessor

    Validates and processes an XML Virtual app from an outside vendor

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 6/28/04 4:10p $
  Version            : $Revision: 8 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.virtualapp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;
import javax.servlet.http.HttpServletRequest;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import com.mes.net.MailMessage;
import com.mes.net.XDoc;
import oracle.sql.BLOB;
import sqlj.runtime.ResultSetIterator;
import sqlj.runtime.ref.DefaultContext;

public class VirtualAppProcessor extends XDoc
{
  public static final int     VAPP_XML_REQUEST      = 1;
  public static final int     VAPP_XML_RESPONSE     = 2;
  
  protected String            xmlApp          = "";
  protected String            vendorId        = "";
  protected String            partnerId       = "";
  protected long              virtualSeqNum   = 0L;
  protected long              startMillis     = 0L;
  protected long              appSeqNum       = 0L;
  protected VsXMLAppResponse  response        = null;
  protected VirtualAppXML     dbApp           = null;
  
  public VirtualAppProcessor()
  {
    super(false);
  }
  
  public VirtualAppProcessor(DefaultContext Ctx)
  {
    super(false);
    
    if(Ctx != null)
    {
      this.Ctx = Ctx;
    }
  }

  /*
  ** protected void logVirtualAppPost
  **
  ** Logs the XML that was posted to vapp_xml_post
  */
  protected void logVirtualAppPost()
  {
    try
    {
      // get sequence for xml post log in database
      /*@lineinfo:generated-code*//*@lineinfo:94^7*/

//  ************************************************************
//  #sql [Ctx] { select  vapp_sequence.nextval
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  vapp_sequence.nextval\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.virtualapp.VirtualAppProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   virtualSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:99^7*/
      
      // insert with empty blob
      /*@lineinfo:generated-code*//*@lineinfo:102^7*/

//  ************************************************************
//  #sql [Ctx] { insert into vapp_xml_post
//          (
//            vapp_seq_num,
//            post_date,
//            vendor_id,
//            partner_id,
//            xml_data,
//            xml_response
//          )
//          values
//          (
//            :virtualSeqNum,
//            sysdate,
//            :vendorId,
//            :partnerId,
//            empty_blob(),
//            empty_blob()
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into vapp_xml_post\n        (\n          vapp_seq_num,\n          post_date,\n          vendor_id,\n          partner_id,\n          xml_data,\n          xml_response\n        )\n        values\n        (\n           :1 ,\n          sysdate,\n           :2 ,\n           :3 ,\n          empty_blob(),\n          empty_blob()\n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.virtualapp.VirtualAppProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,virtualSeqNum);
   __sJT_st.setString(2,vendorId);
   __sJT_st.setString(3,partnerId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:122^7*/
      
      // get reference to blob for writing
      BLOB b;
      /*@lineinfo:generated-code*//*@lineinfo:126^7*/

//  ************************************************************
//  #sql [Ctx] { select  xml_data
//          
//          from    vapp_xml_post
//          where   vapp_seq_num = :virtualSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  xml_data\n         \n        from    vapp_xml_post\n        where   vapp_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.virtualapp.VirtualAppProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,virtualSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   b = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:132^7*/
      
      // write xml data into the blob (compressed)
      DeflaterOutputStream dos = new DeflaterOutputStream(b.getBinaryOutputStream());
      XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
      outputter.output(doc, dos);
      dos.flush();
      dos.close();
    }
    catch(Exception e)
    {
      logEntry("logVirtualApp(" + vendorId + ")", e.toString());
    }
  }
  
  /*
  ** protected void logVirtualAppResponse
  **
  ** logs the generated response to vapp_xml_post
  */
  protected void logVirtualAppResponse()
  {
    try
    {
      // get blob handle
      BLOB b;
      /*@lineinfo:generated-code*//*@lineinfo:158^7*/

//  ************************************************************
//  #sql [Ctx] { select  xml_response
//          
//          from    vapp_xml_post
//          where   vapp_seq_num = :virtualSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  xml_response\n         \n        from    vapp_xml_post\n        where   vapp_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.virtualapp.VirtualAppProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,virtualSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   b = (oracle.sql.BLOB)__sJT_rs.getBLOB(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:164^7*/
      
      // write xml data into blob (compressed)
      DeflaterOutputStream dos = new DeflaterOutputStream(b.getBinaryOutputStream());
      XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
      outputter.output(response, dos);
      dos.flush();
      dos.close();
      
      // update approval message and elapsed mllis
      long elapsed = System.currentTimeMillis() - startMillis;
      
      /*@lineinfo:generated-code*//*@lineinfo:176^7*/

//  ************************************************************
//  #sql [Ctx] { update  vapp_xml_post
//          set     response_message = :response.submissionResponseText,
//                  elapsed_millis = :elapsed
//          where   vapp_seq_num = :virtualSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  vapp_xml_post\n        set     response_message =  :1 ,\n                elapsed_millis =  :2 \n        where   vapp_seq_num =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.virtualapp.VirtualAppProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,response.submissionResponseText);
   __sJT_st.setLong(2,elapsed);
   __sJT_st.setLong(3,virtualSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:182^7*/
    }
    catch(Exception e)
    {
      logEntry("logVirtualAppResponse()", e.toString());
    }
  }
  
  /*
  ** public String getPreApprovalMessage
  **
  ** examines the posted XML in order to generate the XML pre-approval response
  **
  ** this method does not actually process the app (add it to the database)
  ** to allow a more timely response to the client
  */
  public String getPreApprovalMessage(String xmlApp, long startMillis)
  {
    ByteArrayOutputStream bos   = null;
    
    try
    {
      this.xmlApp = xmlApp;
      this.startMillis = startMillis;
      
      // create xdoc document to parse data
      SAXBuilder builder = new SAXBuilder();
      doc = builder.build(new ByteArrayInputStream(xmlApp.getBytes()));
      
      vendorId = doc.getRootElement().getChildText("VendorId");
      partnerId = doc.getRootElement().getChildText("PartnerId");
      
      connect();
      
      // log xml app into database
      logVirtualAppPost();
      
      dbApp = new VirtualAppXML(Ctx);
      
      response = dbApp.getApprovalMessage(doc, virtualSeqNum);
      
      // log response
      logVirtualAppResponse();
      
      bos = new ByteArrayOutputStream();
      
      XMLOutputter outputter = new XMLOutputter();
      outputter.output(response, bos);
    }
    catch(Exception e)
    {
      logEntry("getPreApprovalMessage()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return (bos.toString());
  }
  
  /*
  ** public void process()
  **
  ** Addes previously submitted app (via getPreApprovalMessage) to the database
  ** This method must be called after a call to getPreApprovalMessage
  */
  public void process(HttpServletRequest request)
  {
    try
    {
      if(dbApp != null && dbApp.isValidApp())
      {
        dbApp.addAppToDatabase(request);
      }
      else
      {
        if(dbApp == null)
        {
          MailMessage.sendSystemErrorEmail("VirtualAppProcessor::addAppToDatabase Failed!",
            "dbAPP was null");
        }
        else if(!dbApp.isValidApp())
        {
          MailMessage.sendSystemErrorEmail("VirtualAppProcessor::addAppToDatabase Failed!",
            "dbAPP was invalid");
        }
      }
    }
    catch(Exception e)
    {
      logEntry("process()", e.toString());
    }
  }
  
  public String getXMLAppSeq(int requestType, long appSeqNum)
  {
    long virtualSeqNum  = 0L;
    String result       = "";
    
    try
    {
      // get virtual seq num from app seq num
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:287^7*/

//  ************************************************************
//  #sql [Ctx] { select  vapp_seq_num
//          
//          from    vapp_xml_post
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  vapp_seq_num\n         \n        from    vapp_xml_post\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.virtualapp.VirtualAppProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   virtualSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:293^7*/
      
      result = getXML(requestType, virtualSeqNum);
    }   
    catch (SQLException se)
    {
      // maybe app seq num was actually a virtual app seq num
      result = getXML(requestType, appSeqNum);
    }
    catch(Exception e)
    {
      result = e.toString();
      logEntry("getXMLAppSeq(" + appSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return result;
  }
  
  /*
  ** public Document getXMLDoc()
  **
  ** Retrieves actual XML Document from database
  */
  public Document getXMLDoc(long virtualSeqNum)
  {
    Document  d = null;
    
    try
    {
      Blob  b = null;
      
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:330^7*/

//  ************************************************************
//  #sql [Ctx] { select  xml_data
//          
//          from    vapp_xml_post
//          where   vapp_seq_num = :virtualSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  xml_data\n         \n        from    vapp_xml_post\n        where   vapp_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.virtualapp.VirtualAppProcessor",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,virtualSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   b = (java.sql.Blob)__sJT_rs.getBlob(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:336^7*/
      
      if(b != null)
      {
        InflaterInputStream iis = new InflaterInputStream(b.getBinaryStream());
        SAXBuilder  builder = new SAXBuilder();
        d = builder.build(iis);
        iis.close();
      }
    }
    catch(Exception e)
    {
      logEntry("getXMLDoc(" + virtualSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
    
    return d;
  }
  
  /*
  ** public String getXML()
  **
  ** Retrieves XML from the log table (vapp_xml_post)
  */
  public String getXML(int requestType, long virtualSeqNum)
  {
    String            result  = "";
    ResultSet         rs      = null;
    ResultSetIterator it      = null;
    try
    {
      Document  d   = null;
      Blob      b   = null;
      
      connect();
      
      switch(requestType)
      {
        case VAPP_XML_REQUEST:
          /*@lineinfo:generated-code*//*@lineinfo:378^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  xml_data  xml_blob
//              from    vapp_xml_post
//              where   vapp_seq_num = :virtualSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  xml_data  xml_blob\n            from    vapp_xml_post\n            where   vapp_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.virtualapp.VirtualAppProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,virtualSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.virtualapp.VirtualAppProcessor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:383^11*/
          break;
          
        case VAPP_XML_RESPONSE:
          /*@lineinfo:generated-code*//*@lineinfo:387^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  xml_response xml_blob
//              from    vapp_xml_post
//              where   vapp_seq_num = :virtualSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  xml_response xml_blob\n            from    vapp_xml_post\n            where   vapp_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.virtualapp.VirtualAppProcessor",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,virtualSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.virtualapp.VirtualAppProcessor",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:392^11*/
          break;
          
        default:
          result = "Unknown XML Request Type";
          break;
      }
      
      rs = it.getResultSet();
      if(rs.next())
      {
        b = rs.getBlob(1);
      }
      
      rs.close();
      it.close();
      
      if(b == null)
      {
        result = "Requested XML is NULL";
      }
      else
      {
        InflaterInputStream iis = new InflaterInputStream(b.getBinaryStream());
        SAXBuilder  builder = new SAXBuilder();
        d = builder.build(iis);
        iis.close();
      
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
        outputter.output(d, bos);
      
        result = bos.toString();
      }
    }
    catch(Exception e)
    {
      result = e.toString();
      logEntry("getXML(" + requestType + ", " + virtualSeqNum + ")", e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) {}
      try { it.close(); } catch(Exception e) {}
      cleanUp();
    }
    
    return result;
  }

  /*
  ** public static String getLoggedRequest
  **
  ** retrieves the request XML post that is identified by the passed
  ** virtual sequence number.
  */
  public static String getLoggedRequest(long virtualSeqNum)
  {
    VirtualAppProcessor vap = new VirtualAppProcessor();
    
    return(vap.getXML(VAPP_XML_REQUEST, virtualSeqNum));
  }
  
  /*
  ** public static String getLoggedResponse
  **
  ** retrieves the response XML that is identified by the passed
  ** virtual sequence number.
  */
  public static String getLoggedResponse(long virtualSeqNum)
  {
    VirtualAppProcessor vap = new VirtualAppProcessor();
    
    return(vap.getXML(VAPP_XML_RESPONSE, virtualSeqNum));
  }
  
}/*@lineinfo:generated-code*/