/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/virtualapp/VsXMLAppResponse.java $

  Description:

    VsXMLAppResponse

    Stores elements of the XML response to a VeriSign virtual app

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 12/03/03 3:30p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.virtualapp;

import org.jdom.Document;
import org.jdom.Element;

public class VsXMLAppResponse extends Document
{
  public String vendorId                  = "";
  public String submissionResponseText    = "";
  
  public VsXMLAppResponse()
  {
  }
  
  public VsXMLAppResponse(Element root)
  {
    super(root);
  }
}