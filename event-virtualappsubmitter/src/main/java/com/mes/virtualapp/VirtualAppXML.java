/*@lineinfo:filename=VirtualAppXML*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/virtualapp/VirtualAppXML.sqlj $

  Description:

    VirtualAppXML

    Tools for mapping an XML application into the database

  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 9/09/04 4:57p $
  Version            : $Revision: 12 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.virtualapp;

import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import org.jdom.Document;
import org.jdom.Element;
import com.mes.app.verisign.VaBase;
import com.mes.app.verisign.VaBusiness;
import com.mes.app.verisign.VaEnd;
import com.mes.app.verisign.VaPrincipals;
import com.mes.app.verisign.VaTranInfo;
import com.mes.forms.FieldBean;
import sqlj.runtime.ref.DefaultContext;

public class VirtualAppXML extends VaBase
{
  protected String            vendorId        = "";
  protected long              virtualSeqNum   = 0L;
  protected Document          appDoc          = null;
  protected VsXMLAppResponse  response        = new VsXMLAppResponse();
  protected boolean           isValid         = false;

  public VirtualAppXML()
  {
  }
    
  public VirtualAppXML(DefaultContext defCtx)
  {
    super(defCtx);
  }
  
  protected void createNewApp(HttpServletRequest request)
  {
    super.createNewApp(request);
  }
  
  public boolean isValidApp()
  {
    return isValid;
  }
  
  protected boolean checkInvalid()
  {
    String  transitRouting  = "";
    int     fraudTRCount    = 0;
    boolean result          = false;
    
    try
    {
      transitRouting = appDoc.getRootElement().getChild("AccountInfo").getChildText("TransitRouting");
      
      // determine if transit routing number is one of the bad ones
      /*@lineinfo:generated-code*//*@lineinfo:95^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(transit_routing_num)
//          
//          from    rap_app_bank_aba
//          where   transit_routing_num = :transitRouting and
//                  fraud_flag = 'Y'
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(transit_routing_num)\n         \n        from    rap_app_bank_aba\n        where   transit_routing_num =  :1  and\n                fraud_flag = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.virtualapp.VirtualAppXML",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,transitRouting);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   fraudTRCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:102^7*/
      
      if(fraudTRCount > 0)
      {
        response = VirtualAppResponseBuilder.buildErrorResponse(vendorId, 2000, "Invalid Transit Routing Number");
        response.submissionResponseText = "ERROR";
        
        result = true;
      }
      else
      {
        isValid = true;
      }
    }
    catch(Exception e)
    {
      logEntry("checkInvalid()", e.toString());
      response = VirtualAppResponseBuilder.buildErrorResponse(vendorId, 2002, e.toString());
    }
    
    return result;
  }
  
  protected void generateApprovalPendResponse()
  {
    double  monthlySales    = 0.0;
    double  avgTicket       = 0.0;
    
    try
    {
      StringBuffer tempBuf = new StringBuffer("");
      
      String temp = appDoc.getRootElement().getChild("TransactionInformation").getChildText("MonthlyVMCSales");
      tempBuf.setLength(0);
      for(int i=0; i<temp.length(); ++i)
      {
        if(Character.isDigit(temp.charAt(i)) || temp.charAt(i) == '.')
        {
          tempBuf.append(temp.charAt(i));
        }
      }
      
      monthlySales  = Double.parseDouble(tempBuf.toString());
      
      temp = appDoc.getRootElement().getChild("TransactionInformation").getChildText("AverageTicket");
      tempBuf.setLength(0);
      for(int i=0; i<temp.length(); ++i)
      {
        if(Character.isDigit(temp.charAt(i)) || temp.charAt(i) == '.')
        {
          tempBuf.append(temp.charAt(i));
        }
      }
      avgTicket     = Double.parseDouble(temp.toString());
      
      if(monthlySales <= 10000.00)
      {
        response = VirtualAppResponseBuilder.buildApprovalResponse(vendorId);
        
        response.submissionResponseText = "APPROVE";
      }
      else if(avgTicket == 666.66)
      {
        response = VirtualAppResponseBuilder.buildErrorResponse(vendorId, 2001, "Avg Ticket = 666.66");
        
        response.submissionResponseText = "ERROR";
      }
      else
      {
        response = VirtualAppResponseBuilder.buildPendResponse(vendorId, 1000, "Monthly Volume Exceeds $10,000");
        
        response.submissionResponseText = "PEND";
      }
    }
    catch(Exception e)
    {
      logEntry("generateApprovalPendResponse()", e.toString());
      response = VirtualAppResponseBuilder.buildErrorResponse(vendorId, 2003, e.toString());
    }
  }
  
  public VsXMLAppResponse getApprovalMessage(Document doc, long virtualSeqNum)
  {
    try
    {
      appDoc = doc;
      this.virtualSeqNum = virtualSeqNum;
      
      // extract vendor id
      vendorId = appDoc.getRootElement().getChildText("VendorId");
      response.vendorId = vendorId;
      
      if(!checkInvalid())
      {
        generateApprovalPendResponse();
      }
    }
    catch(Exception e)
    {
      logEntry("getApprovalMessage()", e.toString());
    }
    
    return response;
  }
  
  public void addAppManually(Document xmlApp, long vSeqNum)
  {
    try
    {
      appDoc = xmlApp;
      virtualSeqNum = vSeqNum;
      
      addAppToDatabase(null);
    }
    catch(Exception e)
    {
      System.out.println("addAppManually(): " + e.toString());
      logEntry("addAppManually(" + vSeqNum + ")", e.toString());
    }
  }
  
  public void addAppToDatabase(HttpServletRequest request)
  {
    try
    {
      connect();
      
      // create fields in base bean
      createFields(request);
  
      // add business bean
      VaBusiness vb = new VaBusiness(Ctx);
      vb.initFields(request);
      add(vb);
  
      // add traninfo bean
      VaTranInfo vat = new VaTranInfo(Ctx);
      vat.initFields(request);
      add(vat);
  
      // add principals bean
      VaPrincipals vp = new VaPrincipals(Ctx);
      vp.initFields(request);
      add(vp);
  
      // add thankyou/pricing bean
      VaEnd ve = new VaEnd(Ctx);
      ve.initFields(request);
      add(ve);
  
      // set fields from XML document
      setFields(appDoc, FieldBean.MAP_TYPE_VERISIGN, Ctx, virtualSeqNum, request);
  
      // set any defaults that should always be set (card acceptance, pricing, etc)
      setDefaults();
  
      // create new app
      createNewApp(request);
  
      // update vapp_xml_post to show app_seq_num
      /*@lineinfo:generated-code*//*@lineinfo:262^7*/

//  ************************************************************
//  #sql [Ctx] { update  vapp_xml_post
//          set     app_seq_num = :fields.getData("appSeqNum")
//          where   vapp_seq_num = :virtualSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3589 = fields.getData("appSeqNum");
   String theSqlTS = "update  vapp_xml_post\n        set     app_seq_num =  :1 \n        where   vapp_seq_num =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.virtualapp.VirtualAppXML",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3589);
   __sJT_st.setLong(2,virtualSeqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:267^7*/
  
      // initialize appSeq and stuff by setting fields
      for (Iterator i = subBeans.iterator(); i.hasNext();)
      {
        ((FieldBean)i.next()).externalSubmit();
      }
  
      commit();
    }
    catch(Exception e)
    {
      logEntry("addAppToDatabase()", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*
  public VsXMLAppResponse addAppToDatabase(Document doc, long virtualSeqNum, HttpServletRequest request)
  {
    int               appCount  = 0;
    boolean           wasStale  = false;
    
    try
    {
      appDoc = doc;
      this.virtualSeqNum = virtualSeqNum;
      
      // extract vendor Id
      vendorId = appDoc.getRootElement().getChildText("VendorId");
      response.vendorId = vendorId;
      
      if(isConnectionStale())
      {
        connect();
        wasStale = true;
      }
      
      // check for invalid apps
      if(checkInvalid())
      {
        // error response should have been generated by checkInvalid() so 
        // nothing else must be done here
      }
      else
      {
        // determine if we have already received this application
        #sql [Ctx]
        {
          select  count(vendor_id)
          into    :appCount
          from    vapp_xml_post
          where   vendor_id = :vendorId and
                  vapp_seq_num != :virtualSeqNum
        };
      
        if(appCount > 0 && HttpHelper.isProdServer(request))
        {
          // don't create new apps for duplications (only on production server)
          System.out.println("Ignoring duplicate VSVA application");
        }
        else
        {
          System.out.println("SERVER: Submitting app");
          // create fields in base bean
          createFields(request);
        
          // add business bean
          VaBusiness vb = new VaBusiness(Ctx);
          vb.initFields(request);
          add(vb);
        
          // add traninfo bean
          VaTranInfo vat = new VaTranInfo(Ctx);
          vat.initFields(request);
          add(vat);
        
          // add principals bean
          VaPrincipals vp = new VaPrincipals(Ctx);
          vp.initFields(request);
          add(vp);
        
          // add thankyou/pricing bean
          VaEnd ve = new VaEnd(Ctx);
          ve.initFields(request);
          add(ve);
        
          // set fields from XML document
          setFields(appDoc, FieldBean.MAP_TYPE_VERISIGN, Ctx, virtualSeqNum, request);
        
          // set any defaults that should always be set (card acceptance, pricing, etc)
          setDefaults();
        
          // create new app
          createNewApp(request);
        
          // initialize appSeq and stuff by setting fields
          for (Iterator i = subBeans.iterator(); i.hasNext();)
          {
            ((FieldBean)i.next()).externalSubmit();
          }
        }
        
        // update vapp_xml_post to show app_seq_num
        #sql [Ctx]
        {
          update  vapp_xml_post
          set     app_seq_num = :(fields.getData("appSeqNum"))
          where   vapp_seq_num = :virtualSeqNum
        };
        
        commit();
        
        // establish response
        generateApprovalPendResponse();
      }      
    }
    catch(Exception e)
    {
      logEntry("addAppToDatabase()", e.toString());
      response = VirtualAppResponseBuilder.buildErrorResponse(vendorId, 2001, e.toString());
    }
    finally
    {
      if(wasStale)
      {
        cleanUp();
      }
    }
    
    return response;
  }
  */
  
  protected void setDefaults()
  {
    try
    {
      // set visa & mastercard acceptance
      fields.getField("vmcAccepted").setData("Y");
    }
    catch(Exception e)
    {
      logEntry("setDefaults()", e.toString());
    }
  }
  
  public static class VirtualAppResponseBuilder
  {
    VsXMLAppResponse    doc;
    
    public VirtualAppResponseBuilder()
    {
    }
    
    public VsXMLAppResponse generateMessage(String vendorId, int responseCode, String responseText, int responseSubCode, String responseSubText)
    {
      try
      {
        Element root        = new Element("XMLMessageApplicationResponse");
        Element vendor      = new Element("VendorId");
        Element respCode    = new Element("SubmissionResponseCode");
        Element respText    = new Element("SubmissionResponseText");
        Element respSubCode = new Element("SubmissionResponseSubCode");
        Element respSubText = new Element("SubmissionResponseSubText");
        
        vendor.setText(vendorId);
        respCode.setText(Integer.toString(responseCode));
        respText.setText(responseText);
        
        if(responseSubCode > 0)
        {
          respSubCode.setText(Integer.toString(responseSubCode));
          respSubText.setText(responseSubText);
        }
        
        root.addContent(vendor);
        root.addContent(respCode);
        root.addContent(respText);
        root.addContent(respSubCode);
        root.addContent(respSubText);
        
        doc = new VsXMLAppResponse(root);
      }
      catch(Exception e)
      {
        com.mes.support.SyncLog.LogEntry("VirtualAppResponseBuilder::generateMessage()", e.toString());
      }
      
      return doc;
    }
    
    public static VsXMLAppResponse buildApprovalResponse(String vendorId)
    {
      VirtualAppResponseBuilder out = new VirtualAppResponseBuilder();
      
      return out.generateMessage(vendorId, 1, "APPROVE", 0, "");
    }
    
    public static VsXMLAppResponse buildPendResponse(String vendorId, int pendCode, String pendMessage)
    {
      VirtualAppResponseBuilder out = new VirtualAppResponseBuilder();
      
      return out.generateMessage(vendorId, 2, "PEND", pendCode, pendMessage);
    }
    
    public static VsXMLAppResponse buildErrorResponse(String vendorId, int errorCode, String errorMessage)
    {
      VirtualAppResponseBuilder out = new VirtualAppResponseBuilder();
      
      return out.generateMessage(vendorId, 3, "ERROR", errorCode, errorMessage);
    }
  }
}/*@lineinfo:generated-code*/