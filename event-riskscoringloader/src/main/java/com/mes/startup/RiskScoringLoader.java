/*@lineinfo:filename=RiskScoringLoader*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/RiskScoringLoader.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesChargebacks;
import com.mes.constants.MesChargebacks.CBAutoAction;
import com.mes.constants.MesEmails;
import com.mes.constants.MesQueues;
import com.mes.data.transaction.chargebacks.SplitFundUtil;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.reports.RiskScoreDataBean;
import com.mes.reports.RiskTransDataBean;
import com.mes.settlement.SettlementDb;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesCalendar;
import com.mes.support.MesMath;
import com.mes.support.NumberFormatter;
import com.mes.support.TridentTools;
import com.mes.tools.ChargebackTools;
import com.mes.tools.Director;
import com.mes.tools.FileTransmissionLogger;
import com.mes.tools.ReasonCodeDocBuilder;
import sqlj.runtime.ResultSetIterator;

public class RiskScoringLoader extends EventBase
{
  static Logger log = Logger.getLogger(RiskScoringLoader.class);

  public static final int         PT_CHARGEBACK_RETRIEVAL   = 1;  // add cb/retr scores
  public static final int         PT_AUTH_POINTS            = 2;  // add auth related scores
  public static final int         PT_CAPTURE_POINTS         = 3;  // add capture related scores
  public static final int         PT_AUTH_RISK_SUMMARY      = 4;  // summarize incoming auth file
  public static final int         PT_CAPTURE_RISK_SUMMARY   = 5;  // summarize incoming capture file
  public static final int         PT_NEW_ACCOUNT            = 6;  // initial risk score
  public static final int         PT_LOAD_CARD_SUMMARY      = 7;  // load risk duplicates table
  public static final int         PT_LOAD_CHARGEBACK_FILE   = 8;  // load chargeback file
  public static final int         PT_FOREIGN_CARD_POINTS    = 9;  // add points for foreign cards
  public static final int         PT_ACH_REJECT_POINTS      = 10; // add points for ach rejects
  public static final int         PT_LINK_CREDITS           = 11; // link credits to chargebacks

  public static final int         PT_DEBUG                  = 212;// debug
  
  public static Pattern MULTIPLE_TRANSACTIONS_REGEX = Pattern.compile("^MULTIPLE TRANSACTIONS [0-9]{1,3}$");
  public static Pattern REGEX = Pattern.compile("^[0-9]{1,2} [0-9]{6}$");
  public static Pattern AUTH_REGEX = Pattern.compile("^AUTH [0-9]{6}$");
  public static Pattern FNS_COUNT_REGEX = Pattern.compile("^FNS COUNT [0-9]{1,2}$");
  public static Pattern NO_SHOW_REGEX = Pattern.compile("^NO SHOW [0-9]{1,3} [0-9]{1,4}$");
  public static Pattern RS5_REGEX = Pattern.compile("^RS5 [0-9]{1,3} [0-9]{1,4}$");
  public static Pattern RATE_QUOTE_REGEX = Pattern.compile("^[0-9]{1,4}$");
  public static Pattern DIGITAL_GOODS_REGEX = Pattern.compile("^DIGITAL GOODS [0-9]{1,3}$");

  public RiskScoringLoader()
  {
  }

  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    boolean                   fileProcessed     = false;
    ResultSetIterator         it                = null;
    int                       itemCount         = 0;
    String                    loadFilename      = null;
    int                       procType          = -1;
    long                      recId             = 0L;
    ResultSet                 resultSet         = null;
    int                       sequenceId        = 0;

    try
    {
      // get an automated timeout exempt connection
      connect(true);

      try
      {
        procType = Integer.parseInt(getEventArg(0));
      }
      catch ( Exception ee )
      {
        procType = -1;
      }

      if ( procType == -1 )   // process table event
      {
        /*@lineinfo:generated-code*//*@lineinfo:113^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(load_filename)  
//            from    risk_process
//            where   process_sequence = 0
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(load_filename)   \n          from    risk_process\n          where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:118^9*/

        if ( itemCount > 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:122^11*/

//  ************************************************************
//  #sql [Ctx] { select  risk_process_sequence.nextval 
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  risk_process_sequence.nextval  \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   sequenceId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:126^11*/

          /*@lineinfo:generated-code*//*@lineinfo:128^11*/

//  ************************************************************
//  #sql [Ctx] { update  risk_process
//              set     process_sequence = :sequenceId
//              where   process_sequence = 0
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  risk_process\n            set     process_sequence =  :1  \n            where   process_sequence = 0";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sequenceId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:133^11*/

          /*@lineinfo:generated-code*//*@lineinfo:135^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  rec_id,
//                      load_filename,
//                      process_type
//              from    risk_process
//              where   process_sequence = :sequenceId
//              order by process_type
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rec_id,\n                    load_filename,\n                    process_type\n            from    risk_process\n            where   process_sequence =  :1  \n            order by process_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,sequenceId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:143^11*/
          resultSet = it.getResultSet();

          while( resultSet.next() )
          {
            loadFilename  = resultSet.getString("load_filename");
            procType      = resultSet.getInt("process_type");
            recId         = resultSet.getLong("rec_id");

            recordTimestampBegin( recId );

            fileProcessed = true;     // default

            switch( procType )
            {
              case PT_LOAD_CHARGEBACK_FILE:
                fileProcessed = loadChargebackFile(loadFilename);
                break;

              case PT_CHARGEBACK_RETRIEVAL:
                // the linking process will handle assigning
                // the missing merchant numbers and linking
                // the chargeback to the original transaction
                linkChargebacksToTransactions(loadFilename);
                linkRetrievalsToTransactions(loadFilename);

                // link incoming credits to retrievals
                // note, this process does not use the
                // load filename for anything other than
                // the bank # so it could be done on the
                // capture files
                linkCreditsToChargebacks(loadFilename);
                linkCreditsToRetrievals(loadFilename);

                // process the automated e-mail letters
                notifyMerchants(loadFilename);

                // load the points for chargebacks activity
                fileProcessed = loadChargebackRetrievalPoints(loadFilename);
                break;

              case PT_AUTH_POINTS:
                fileProcessed = loadPointsAuth(loadFilename);
                break;

              case PT_CAPTURE_POINTS:
                fileProcessed = loadPointsCapture(loadFilename);
                break;

              case PT_AUTH_RISK_SUMMARY:
                fileProcessed = loadAuthRiskSummary(loadFilename);
                break;

              case PT_CAPTURE_RISK_SUMMARY:
                fileProcessed = loadCaptureRiskSummary(loadFilename);
                break;

              case PT_NEW_ACCOUNT:
                fileProcessed = loadPointsNewAccount( Long.parseLong(loadFilename) );
                break;

              case PT_LOAD_CARD_SUMMARY:
                loadDuplicateCardSummary(loadFilename);
                break;

              case PT_FOREIGN_CARD_POINTS:
                loadPointsForeignCards(loadFilename);
                break;

              case PT_ACH_REJECT_POINTS:
                loadPointsAchRejects(loadFilename);
                break;

              case PT_DEBUG:
                loadDebug(loadFilename);
                break;

              default:      // skip
                continue;
            }
            if ( fileProcessed == true )
            {
              if ( procType != PT_DEBUG )
              {
                FileTransmissionLogger.logStage( loadFilename, FileTransmissionLogger.STAGE_RISK_SUMMARY );
              }
              recordTimestampEnd( recId );
            }
          }
          resultSet.close();
          it.close();
        }
      }
      else    // specific process requested using event argument
      {
        switch( procType )
        {
          case PT_LINK_CREDITS:
            linkCreditsToChargebacks(null);
            linkCreditsToRetrievals(null);
            break;
        }
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
      cleanUp();
    }

    return( true );
  }

  //
  // DEPRECATED:
  //
  // This is a legacy method left to allow us to easily back fill
  // missing merchant number data if necessary.
  //
  // Normally the missing merchant numbers are corrected as
  // part of the transaction linking process.
  //
  protected void fixMissingAcctChargebacks( String loadFilename )
  {
    int                   bankNumber      = 0;
    Date                  batchDate       = null;
    String                cardNum         = null;
    ResultSetIterator     it              = null;
    long                  loadSec         = 0L;
    int                   matchCount      = 0;
    long                  merchantId      = 0L;
    int                   queueType       = 0;
    int                   recCount        = 0;
    String                refNum          = null;
    ResultSet             resultSet       = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:286^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.cb_load_sec                                  as load_sec,
//                  cb.card_number                                  as card_number,
//                  cb.reference_number                             as ref_num,
//                  case
//                    when cb.card_type in ('VS','MC') then nvl(yddd_to_date(substr(cb.reference_number,8,4)),cb.tran_date)
//                    else cb.tran_date
//                  end                                             as batch_date,
//                  cb.reason_code                                  as reason_code,
//                  decode( cb.first_time_chargeback,
//                          'Y', 1, 2 )                             as usage_code,
//                  cb.incoming_date                                as incoming_date,
//                  cb.bank_number                                  as bank_number
//          from    network_chargebacks     cb
//          where   cb.load_filename = :loadFilename
//                  and cb.merchant_number = 0
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.cb_load_sec                                  as load_sec,\n                cb.card_number                                  as card_number,\n                cb.reference_number                             as ref_num,\n                case\n                  when cb.card_type in ('VS','MC') then nvl(yddd_to_date(substr(cb.reference_number,8,4)),cb.tran_date)\n                  else cb.tran_date\n                end                                             as batch_date,\n                cb.reason_code                                  as reason_code,\n                decode( cb.first_time_chargeback,\n                        'Y', 1, 2 )                             as usage_code,\n                cb.incoming_date                                as incoming_date,\n                cb.bank_number                                  as bank_number\n        from    network_chargebacks     cb\n        where   cb.load_filename =  :1  \n                and cb.merchant_number = 0";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:303^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        cardNum     = resultSet.getString("card_number");
        batchDate   = resultSet.getDate("batch_date");
        loadSec     = resultSet.getLong("load_sec");
        refNum      = resultSet.getString("ref_num");
        bankNumber  = resultSet.getInt("bank_number");

        for ( int i = 0; i < 2; ++i )
        {
          try
          {
            if ( i == 0 )     // first try full card number
            {
              /*@lineinfo:generated-code*//*@lineinfo:320^15*/

//  ************************************************************
//  #sql [Ctx] { select  dt.merchant_account_number,
//                          count(dt.merchant_account_number)
//                  
//                  from    mif                  mf,
//                          daily_detail_file_dt dt
//                  where   ((mf.bank_number = :bankNumber) or (:bankNumber = 3941 and mf.bank_number in (3943, 3003))) and
//                          dt.merchant_account_number = mf.merchant_number and
//                          dt.batch_date between  :batchDate and (:batchDate+2) and
//                          dt.reference_number = :refNum and
//                          dt.cardholder_account_number = :cardNum
//                  group by dt.merchant_account_number
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  dt.merchant_account_number,\n                        count(dt.merchant_account_number)\n                 \n                from    mif                  mf,\n                        daily_detail_file_dt dt\n                where   ((mf.bank_number =  :1  ) or ( :2   = 3941 and mf.bank_number in (3943, 3003))) and\n                        dt.merchant_account_number = mf.merchant_number and\n                        dt.batch_date between   :3   and ( :4  +2) and\n                        dt.reference_number =  :5   and\n                        dt.cardholder_account_number =  :6  \n                group by dt.merchant_account_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setDate(3,batchDate);
   __sJT_st.setDate(4,batchDate);
   __sJT_st.setString(5,refNum);
   __sJT_st.setString(6,cardNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   matchCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:333^15*/
            }
            else    // try only first 12 (last 4 change)
            {
              /*@lineinfo:generated-code*//*@lineinfo:337^15*/

//  ************************************************************
//  #sql [Ctx] { select  dt.merchant_account_number,
//                          count(dt.merchant_account_number)
//                  
//                  from    mif                  mf,
//                          daily_detail_file_dt dt
//                  where   ((mf.bank_number = :bankNumber) or (:bankNumber = 3941 and mf.bank_number in (3943, 3003)) ) and
//                          dt.merchant_account_number = mf.merchant_number and
//                          dt.batch_date between  :batchDate and (:batchDate+2) and
//                          dt.reference_number = :refNum and
//                          dt.cardholder_account_number like ( substr(:cardNum,1,12) || '%' )
//                  group by dt.merchant_account_number
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  dt.merchant_account_number,\n                        count(dt.merchant_account_number)\n                 \n                from    mif                  mf,\n                        daily_detail_file_dt dt\n                where   ((mf.bank_number =  :1  ) or ( :2   = 3941 and mf.bank_number in (3943, 3003)) ) and\n                        dt.merchant_account_number = mf.merchant_number and\n                        dt.batch_date between   :3   and ( :4  +2) and\n                        dt.reference_number =  :5   and\n                        dt.cardholder_account_number like ( substr( :6  ,1,12) || '%' )\n                group by dt.merchant_account_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setDate(3,batchDate);
   __sJT_st.setDate(4,batchDate);
   __sJT_st.setString(5,refNum);
   __sJT_st.setString(6,cardNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   matchCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:350^15*/
            }
          }
          catch( Exception sqle )
          {
            // there was either no matches or more than one
            // merchant number match (ambiguous) so skip
            continue;
          }

          // if the query passed then exit the loop
          break;
        }

        /*@lineinfo:generated-code*//*@lineinfo:364^9*/

//  ************************************************************
//  #sql [Ctx] { update network_chargebacks
//            set merchant_number_missing = 'Y',
//                merchant_number = :merchantId
//            where cb_load_sec = :loadSec
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update network_chargebacks\n          set merchant_number_missing = 'Y',\n              merchant_number =  :1  \n          where cb_load_sec =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:370^9*/

        /*@lineinfo:generated-code*//*@lineinfo:372^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//            
//            from    q_data
//            where   item_type = 18
//                    and id = :loadSec
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n           \n          from    q_data\n          where   item_type = 18\n                  and id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:379^9*/

        if ( recCount == 0 )
        {
          // must also assign these chargebacks to a queue
          /*@lineinfo:generated-code*//*@lineinfo:384^11*/

//  ************************************************************
//  #sql [Ctx] { -- get_cb_queue_assignment(bankNumber, merchantId, cardNum, reasonCode, usageCode, actionCode );
//              select  nvl( get_cb_queue_assignment( :bankNumber,
//                                                    :merchantId,
//                                                    :cardNum,
//                                                    :resultSet.getString("reason_code"),
//                                                    :resultSet.getInt("usage_code"),
//                                                    null ), 0 )
//                        
//              from    dual
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4343 = resultSet.getString("reason_code");
 int __sJT_4344 = resultSet.getInt("usage_code");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "-- get_cb_queue_assignment(bankNumber, merchantId, cardNum, reasonCode, usageCode, actionCode );\n            select  nvl( get_cb_queue_assignment(  :1  ,\n                                                   :2  ,\n                                                   :3  ,\n                                                   :4  ,\n                                                   :5  ,\n                                                  null ), 0 )\n                       \n            from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setString(3,cardNum);
   __sJT_st.setString(4,__sJT_4343);
   __sJT_st.setInt(5,__sJT_4344);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   queueType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:395^11*/

          String source = loadFilename.startsWith("chgbk") ? "tsys" : "mes";
          if ( queueType != 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:400^13*/

//  ************************************************************
//  #sql [Ctx] { insert into q_data
//                (
//                  id,
//                  type,
//                  item_type,
//                  owner,
//                  date_created,
//                  source,
//                  affiliate
//                )
//                values
//                (
//                  :loadSec,
//                  :queueType,
//                  18,       -- items are chargebacks
//                  1,
//                  :resultSet.getDate("incoming_date"),
//                  :source,
//                  to_char(:bankNumber)
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_4345 = resultSet.getDate("incoming_date");
   String theSqlTS = "insert into q_data\n              (\n                id,\n                type,\n                item_type,\n                owner,\n                date_created,\n                source,\n                affiliate\n              )\n              values\n              (\n                 :1  ,\n                 :2  ,\n                18,       -- items are chargebacks\n                1,\n                 :3  ,\n                 :4  ,\n                to_char( :5  )\n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"10com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setInt(2,queueType);
   __sJT_st.setDate(3,__sJT_4345);
   __sJT_st.setString(4,source);
   __sJT_st.setInt(5,bankNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:422^13*/
          }
        }
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      // ignore?
    }
    finally
    {
      try { it.close(); } catch(Exception e){}
    }
  }

  private String getCBActionCode( long loadSec )
  {
    String                actionCode    = null;
    ResultSetIterator     it            = null;
    ResultSet             resultSet     = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:447^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cba.action_code   as action_code
//          from    network_chargeback_activity   cba
//          where   cba.cb_load_sec = :loadSec
//          order by cba.action_date desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cba.action_code   as action_code\n        from    network_chargeback_activity   cba\n        where   cba.cb_load_sec =  :1  \n        order by cba.action_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"11com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:453^7*/
      resultSet = it.getResultSet();

      if ( resultSet.next() )
      {
        actionCode = resultSet.getString("action_code");
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry( "getCBActionCode("+loadSec+")",e.toString());
    }
    finally
    {
      try{it.close();}catch(Exception e){}
    }
    return(actionCode);
  }

  protected CBAutoAction getCBAutoAction( int bankNumber, long merchantId, String cardType, String reasonCode, Date tranDate, int usageCode )
  {
    ResultSetIterator     it          = null;
    int                   recCount    = 0;
    ResultSet             resultSet   = null;
    CBAutoAction          retVal      = null;

    try
    {
      // Stage 1: Auto Action List
      //
      // look to see if the current account number is
      // on the auto action list.  if it is and this
      // is an incoming chargeback then automatically
      // create the entry in the activity table
      /*@lineinfo:generated-code*//*@lineinfo:489^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ac.short_action_code   as action_code
//          from    (
//                    select  mf.merchant_number,
//                            th.ancestor,
//                            th.relation
//                    from    mif           mf,
//                            t_hierarchy   th
//                    where   mf.merchant_number = :merchantId
//                            and th.descendent = mf.association_node
//                            and th.hier_type = 1
//                  )                           hier,
//                  network_chargeback_auto_act ncaa,
//                  chargeback_action_codes     ac
//          where   (
//                    -- get entries where the auto action is setup at
//                    -- the merchant level OR the hierarhcy level
//                    (hier.merchant_number = ncaa.hierarchy_node and hier.relation = 0)
//                    or (hier.ancestor = ncaa.hierarchy_node)
//                  )
//                  and ncaa.auto_action = ac.action_code
//          order by hier.relation
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ac.short_action_code   as action_code\n        from    (\n                  select  mf.merchant_number,\n                          th.ancestor,\n                          th.relation\n                  from    mif           mf,\n                          t_hierarchy   th\n                  where   mf.merchant_number =  :1  \n                          and th.descendent = mf.association_node\n                          and th.hier_type = 1\n                )                           hier,\n                network_chargeback_auto_act ncaa,\n                chargeback_action_codes     ac\n        where   (\n                  -- get entries where the auto action is setup at\n                  -- the merchant level OR the hierarhcy level\n                  (hier.merchant_number = ncaa.hierarchy_node and hier.relation = 0)\n                  or (hier.ancestor = ncaa.hierarchy_node)\n                )\n                and ncaa.auto_action = ac.action_code\n        order by hier.relation";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:512^7*/
      resultSet = it.getResultSet();

      if ( resultSet.next() )
      {
        retVal = new CBAutoAction(MesChargebacks.AS_AUTO_LIST,
                                  resultSet.getString("action_code"));
      }
      resultSet.close();
      it.close();

      // Stage 2:  Hierarchy based reason codes
      if ( retVal == null )
      {
        // check to see if this reason codes is an auto-action reason
        /*@lineinfo:generated-code*//*@lineinfo:527^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(ram.reason_code) 
//            from    mif                             mf,
//                    t_hierarchy                     th,
//                    chargeback_reason_auto_mchb     ram
//            where   mf.merchant_number = :merchantId and
//                    th.hier_type = 1 and
//                    th.entity_type = 4 and
//                    th.descendent = mf.association_node and
//                    th.ancestor = ram.hierarchy_node and
//                    ram.card_type = :cardType and
//                    (
//                      ram.reason_code = 0 or
//                      ram.reason_code = :reasonCode
//                    )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(ram.reason_code)  \n          from    mif                             mf,\n                  t_hierarchy                     th,\n                  chargeback_reason_auto_mchb     ram\n          where   mf.merchant_number =  :1   and\n                  th.hier_type = 1 and\n                  th.entity_type = 4 and\n                  th.descendent = mf.association_node and\n                  th.ancestor = ram.hierarchy_node and\n                  ram.card_type =  :2   and\n                  (\n                    ram.reason_code = 0 or\n                    ram.reason_code =  :3  \n                  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setString(2,cardType);
   __sJT_st.setString(3,reasonCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:543^9*/
        if ( recCount > 0 )
        {
          retVal = new CBAutoAction(MesChargebacks.AS_AUTO_REASON_BY_NODE,"D"); // MCHB
        }
      }

      // Stage 3:  Reason Codes
      //
      // does not have an entry in the auto action list and
      // the bank supports automatcially working items by reason
      if ( retVal == null )
      {
        // check to see if this reason codes is an auto-action reason
        /*@lineinfo:generated-code*//*@lineinfo:557^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(reason_code) 
//            from    chargeback_reason_desc    rd
//            where   :bankNumber in (select bank_number from mbs_banks where trait_02 = 3941) and
//                    rd.card_type = :cardType and
//                    rd.item_type = 'C' and
//                    rd.reason_code = :reasonCode and
//                    nvl(rd.auto_mchb,'N') = 'Y'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(reason_code)  \n          from    chargeback_reason_desc    rd\n          where    :1   in (select bank_number from mbs_banks where trait_02 = 3941) and\n                  rd.card_type =  :2   and\n                  rd.item_type = 'C' and\n                  rd.reason_code =  :3   and\n                  nvl(rd.auto_mchb,'N') = 'Y'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setString(2,cardType);
   __sJT_st.setString(3,reasonCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:566^9*/
        if ( recCount > 0 )
        {
          retVal = new CBAutoAction(MesChargebacks.AS_AUTO_REASON,"D"); // MCHB
        }
      }
    }
    catch(Exception e)
    {
      logEntry("getCBAutoAction()",e.toString());
    }
    finally
    {
      try{it.close();}catch(Exception e){}
    }
    return( retVal );
  }

  public int getPoints( long merchantId, int catId, double value )
  {
    return( getPoints( merchantId, catId, value, 0 ) );
  }

  public int getPoints( long merchantId, int catId, double value, int thresholdValue )
  {
    long      nodeId        = merchantId;
    int       retVal        = 0;
    int       rowCount      = 0;

    try
    {
      while( true )
      {
        /*@lineinfo:generated-code*//*@lineinfo:599^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(hierarchy_node) 
//            from    risk_score_system rss
//            where   rss.hierarchy_node = :nodeId and
//                    rss.risk_cat_id = :catId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(hierarchy_node)  \n          from    risk_score_system rss\n          where   rss.hierarchy_node =  :1   and\n                  rss.risk_cat_id =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,catId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:605^9*/

        if( rowCount > 0 )
        {
          break;
        }

        // no rows found for the current node, get the parent node id
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:615^11*/

//  ************************************************************
//  #sql [Ctx] { select  orgp.org_group        
//              from    organization      orgp,
//                      organization      orgc,
//                      parent_org        po
//              where   orgc.org_group = :nodeId and
//                      po.org_num = orgc.org_num and
//                      orgp.org_num = po.parent_org_num
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  orgp.org_group         \n            from    organization      orgp,\n                    organization      orgc,\n                    parent_org        po\n            where   orgc.org_group =  :1   and\n                    po.org_num = orgc.org_num and\n                    orgp.org_num = po.parent_org_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   nodeId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:624^11*/
        }
        catch( java.sql.SQLException e )
        {
          // no parent found, return 0L
          nodeId = 0L;
          break;
        }
      }

      /*@lineinfo:generated-code*//*@lineinfo:634^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(rss.points),
//                  nvl(max(rss.points),0)   
//          from    risk_score_system   rss
//          where   rss.hierarchy_node = :nodeId and
//                  rss.risk_cat_id = :catId and
//                  :value between rss.lower_range and rss.upper_range and
//                  :thresholdValue >= nvl(rss.threshold,0)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(rss.points),\n                nvl(max(rss.points),0)    \n        from    risk_score_system   rss\n        where   rss.hierarchy_node =  :1   and\n                rss.risk_cat_id =  :2   and\n                 :3   between rss.lower_range and rss.upper_range and\n                 :4   >= nvl(rss.threshold,0)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,nodeId);
   __sJT_st.setInt(2,catId);
   __sJT_st.setDouble(3,value);
   __sJT_st.setInt(4,thresholdValue);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   retVal = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:643^7*/
    }
    catch( Exception e )
    {
      StringBuffer    buf = new StringBuffer("getPoints(");

      buf.append( merchantId );
      buf.append( "," );
      buf.append( catId );
      buf.append( "," );
      buf.append( value );
      buf.append( "," );
      buf.append( thresholdValue );
      buf.append( ")" );

      logEntry( buf.toString(),e.toString() );
    }
    return( retVal );
  }

  protected void linkChargebacksToTransactions( String loadFilename )
  {
    int                   bankNumber      = 0;
    Date                  batchDate       = null;
    String                cardNum         = null;
    String                firstTime       = null;
    ResultSetIterator     it              = null;
    long                  loadSec         = 0L;
    int                   matchCount      = 0;
    long                  merchantId      = 0L;
    String                refNum          = null;
    ResultSet             resultSet       = null;
    ResultSetIterator     tranIt          = null;
    ResultSet             tranResultSet   = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:680^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.cb_load_sec                                  as load_sec,
//                  cb.card_number                                  as card_number,
//                  cb.reference_number                             as ref_num,
//                  nvl(cb.merchant_number,0)                       as merchant_number,
//                  case
//                    when cb.card_type in ('VS','MC') then nvl(yddd_to_date(substr(cb.reference_number,8,4)),cb.tran_date)
//                    else cb.tran_date
//                  end                                             as batch_date,
//                  cb.bank_number                                  as bank_number,
//                  cb.first_time_chargeback                        as first_time
//          from    network_chargebacks           cb
//          where   cb.load_filename = :loadFilename and
//                  cb.dt_batch_date is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.cb_load_sec                                  as load_sec,\n                cb.card_number                                  as card_number,\n                cb.reference_number                             as ref_num,\n                nvl(cb.merchant_number,0)                       as merchant_number,\n                case\n                  when cb.card_type in ('VS','MC') then nvl(yddd_to_date(substr(cb.reference_number,8,4)),cb.tran_date)\n                  else cb.tran_date\n                end                                             as batch_date,\n                cb.bank_number                                  as bank_number,\n                cb.first_time_chargeback                        as first_time\n        from    network_chargebacks           cb\n        where   cb.load_filename =  :1   and\n                cb.dt_batch_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"18com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:695^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        cardNum     = resultSet.getString("card_number");
        batchDate   = resultSet.getDate("batch_date");
        loadSec     = resultSet.getLong("load_sec");
        refNum      = resultSet.getString("ref_num");
        merchantId  = resultSet.getLong("merchant_number");
        bankNumber  = resultSet.getInt("bank_number");
        firstTime   = resultSet.getString("first_time");

        if ( merchantId == 0L )
        {
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:712^13*/

//  ************************************************************
//  #sql [Ctx] { select dt.merchant_account_number,
//                       count(dt.merchant_account_number)
//                
//                from   mif                  mf,
//                       daily_detail_file_dt dt
//                where  ((mf.bank_number = :bankNumber) or (:bankNumber = 3941 and mf.bank_number = 3943)) and
//                       dt.merchant_account_number = mf.merchant_number and
//                       dt.batch_date between  :batchDate-2 and (:batchDate+2) and
//                       dt.reference_number = :refNum and
//                       dt.cardholder_account_number like (substr(:cardNum,1,length(:cardNum)-4) || '%')
//                group by dt.merchant_account_number
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select dt.merchant_account_number,\n                     count(dt.merchant_account_number)\n               \n              from   mif                  mf,\n                     daily_detail_file_dt dt\n              where  ((mf.bank_number =  :1  ) or ( :2   = 3941 and mf.bank_number = 3943)) and\n                     dt.merchant_account_number = mf.merchant_number and\n                     dt.batch_date between   :3  -2 and ( :4  +2) and\n                     dt.reference_number =  :5   and\n                     dt.cardholder_account_number like (substr( :6  ,1,length( :7  )-4) || '%')\n              group by dt.merchant_account_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setDate(3,batchDate);
   __sJT_st.setDate(4,batchDate);
   __sJT_st.setString(5,refNum);
   __sJT_st.setString(6,cardNum);
   __sJT_st.setString(7,cardNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   matchCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:725^13*/
          }
          catch( java.sql.SQLException sqle )
          {
            // there was either no matches or more than one
            // merchant number match (ambiguous) so skip
            continue;
          }
        }

        /*@lineinfo:generated-code*//*@lineinfo:735^9*/

//  ************************************************************
//  #sql [Ctx] tranIt = { select dt.batch_date                as batch_date,
//                   dt.batch_number              as batch_number,
//                   dt.ddf_dt_id                 as ddf_dt_id,
//                   dt.trident_tran_id           as trident_tran_id,
//                   dt.purchase_id               as purchase_id,
//                   dt.client_reference_number   as client_ref_num
//            from   daily_detail_file_dt dt
//            where  dt.merchant_account_number = :merchantId and
//                   dt.batch_date between  :batchDate-2 and (:batchDate+3) and
//                   dt.reference_number = :refNum and
//                   dt.cardholder_account_number like
//                    (substr(:cardNum,1,length(:cardNum)-4) || '%') and
//                   not exists
//                   (  select  cb.cb_load_sec
//                      from    network_chargebacks cb
//                      where   cb.dt_batch_date = dt.batch_date and
//                              cb.dt_batch_number = dt.batch_number and
//                              cb.dt_ddf_dt_id = dt.ddf_dt_id and
//                              cb.first_time_chargeback = :firstTime
//                   )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select dt.batch_date                as batch_date,\n                 dt.batch_number              as batch_number,\n                 dt.ddf_dt_id                 as ddf_dt_id,\n                 dt.trident_tran_id           as trident_tran_id,\n                 dt.purchase_id               as purchase_id,\n                 dt.client_reference_number   as client_ref_num\n          from   daily_detail_file_dt dt\n          where  dt.merchant_account_number =  :1   and\n                 dt.batch_date between   :2  -2 and ( :3  +3) and\n                 dt.reference_number =  :4   and\n                 dt.cardholder_account_number like\n                  (substr( :5  ,1,length( :6  )-4) || '%') and\n                 not exists\n                 (  select  cb.cb_load_sec\n                    from    network_chargebacks cb\n                    where   cb.dt_batch_date = dt.batch_date and\n                            cb.dt_batch_number = dt.batch_number and\n                            cb.dt_ddf_dt_id = dt.ddf_dt_id and\n                            cb.first_time_chargeback =  :7  \n                 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,batchDate);
   __sJT_st.setDate(3,batchDate);
   __sJT_st.setString(4,refNum);
   __sJT_st.setString(5,cardNum);
   __sJT_st.setString(6,cardNum);
   __sJT_st.setString(7,firstTime);
   // execute query
   tranIt = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:757^9*/
        tranResultSet = tranIt.getResultSet();

        if ( tranResultSet.next() )
        {
          /*@lineinfo:generated-code*//*@lineinfo:762^11*/

//  ************************************************************
//  #sql [Ctx] { update network_chargebacks
//              set dt_batch_date             = :tranResultSet.getDate("batch_date"),
//                  dt_batch_number           = :tranResultSet.getLong("batch_number"),
//                  dt_ddf_dt_id              = :tranResultSet.getLong("ddf_dt_id"),
//                  trident_tran_id           = :tranResultSet.getString("trident_tran_id"),
//                  dt_purchase_id            = :tranResultSet.getString("purchase_id"),
//                  client_reference_number   = :tranResultSet.getString("client_ref_num")
//              where cb_load_sec = :loadSec
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_4346 = tranResultSet.getDate("batch_date");
 long __sJT_4347 = tranResultSet.getLong("batch_number");
 long __sJT_4348 = tranResultSet.getLong("ddf_dt_id");
 String __sJT_4349 = tranResultSet.getString("trident_tran_id");
 String __sJT_4350 = tranResultSet.getString("purchase_id");
 String __sJT_4351 = tranResultSet.getString("client_ref_num");
   String theSqlTS = "update network_chargebacks\n            set dt_batch_date             =  :1  ,\n                dt_batch_number           =  :2  ,\n                dt_ddf_dt_id              =  :3  ,\n                trident_tran_id           =  :4  ,\n                dt_purchase_id            =  :5  ,\n                client_reference_number   =  :6  \n            where cb_load_sec =  :7 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"21com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_4346);
   __sJT_st.setLong(2,__sJT_4347);
   __sJT_st.setLong(3,__sJT_4348);
   __sJT_st.setString(4,__sJT_4349);
   __sJT_st.setString(5,__sJT_4350);
   __sJT_st.setString(6,__sJT_4351);
   __sJT_st.setLong(7,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:772^11*/
        }
        tranResultSet.close();
        tranIt.close();
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      // ignore?
      logEntry("linkChargebacksToTransactions( " + loadFilename + " )", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e){}
      try { tranIt.close(); } catch(Exception e){}
    }
  }

  protected void linkRetrievalsToTransactions( String loadFilename )
  {
    int                   bankNumber      = 0;
    Date                  batchDate       = null;
    String                cardNum         = null;
    ResultSetIterator     it              = null;
    long                  loadSec         = 0L;
    int                   matchCount      = 0;
    long                  merchantId      = 0L;
    String                refNum          = null;
    ResultSet             resultSet       = null;
    ResultSetIterator     tranIt          = null;
    ResultSet             tranResultSet   = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:808^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  rt.retr_load_sec                                as load_sec,
//                  rt.card_number                                  as card_number,
//                  rt.reference_number                             as ref_num,
//                  nvl(rt.merchant_number,0)                       as merchant_number,
//                  case
//                    when rt.card_type in ('VS','MC') then nvl(yddd_to_date(substr(rt.reference_number,8,4)),rt.tran_date)
//                    else rt.tran_date
//                  end                                             as batch_date,
//                  rt.bank_number                                  as bank_number
//          from    network_retrievals      rt
//          where   rt.load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rt.retr_load_sec                                as load_sec,\n                rt.card_number                                  as card_number,\n                rt.reference_number                             as ref_num,\n                nvl(rt.merchant_number,0)                       as merchant_number,\n                case\n                  when rt.card_type in ('VS','MC') then nvl(yddd_to_date(substr(rt.reference_number,8,4)),rt.tran_date)\n                  else rt.tran_date\n                end                                             as batch_date,\n                rt.bank_number                                  as bank_number\n        from    network_retrievals      rt\n        where   rt.load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"22com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:821^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        cardNum     = resultSet.getString("card_number");
        batchDate   = resultSet.getDate("batch_date");
        loadSec     = resultSet.getLong("load_sec");
        refNum      = resultSet.getString("ref_num");
        merchantId  = resultSet.getLong("merchant_number");
        bankNumber  = resultSet.getInt("bank_number");

        if ( merchantId == 0L )
        {
          try
          {
            /*@lineinfo:generated-code*//*@lineinfo:837^13*/

//  ************************************************************
//  #sql [Ctx] { select dt.merchant_account_number,
//                       count(dt.merchant_account_number)
//                
//                from   mif                  mf,
//                       daily_detail_file_dt dt
//                where  ((mf.bank_number = :bankNumber) or (:bankNumber = 3941 and mf.bank_number = 3943)) and
//                       dt.merchant_account_number = mf.merchant_number and
//                       dt.batch_date between  :batchDate-2 and (:batchDate+2) and
//                       dt.reference_number = :refNum and
//                       dt.cardholder_account_number = :cardNum
//                group by dt.merchant_account_number
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select dt.merchant_account_number,\n                     count(dt.merchant_account_number)\n               \n              from   mif                  mf,\n                     daily_detail_file_dt dt\n              where  ((mf.bank_number =  :1  ) or ( :2   = 3941 and mf.bank_number = 3943)) and\n                     dt.merchant_account_number = mf.merchant_number and\n                     dt.batch_date between   :3  -2 and ( :4  +2) and\n                     dt.reference_number =  :5   and\n                     dt.cardholder_account_number =  :6  \n              group by dt.merchant_account_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"23com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setInt(2,bankNumber);
   __sJT_st.setDate(3,batchDate);
   __sJT_st.setDate(4,batchDate);
   __sJT_st.setString(5,refNum);
   __sJT_st.setString(6,cardNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchantId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   matchCount = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:850^13*/
          }
          catch( java.sql.SQLException sqle )
          {
            // there was either no matches or more than one
            // merchant number match (ambiguous) so skip
            continue;
          }
        }

        /*@lineinfo:generated-code*//*@lineinfo:860^9*/

//  ************************************************************
//  #sql [Ctx] tranIt = { select dt.batch_date                as batch_date,
//                   dt.batch_number              as batch_number,
//                   dt.ddf_dt_id                 as ddf_dt_id,
//                   dt.trident_tran_id           as trident_tran_id,
//                   dt.purchase_id               as purchase_id,
//                   dt.client_reference_number   as client_ref_num
//            from   daily_detail_file_dt dt
//            where  dt.merchant_account_number = :merchantId and
//                   dt.batch_date between  :batchDate and (:batchDate+2) and
//                   dt.reference_number = :refNum and
//                   dt.cardholder_account_number = :cardNum and
//                   not exists
//                   (  select  rt.retr_load_sec
//                      from    network_retrievals    rt
//                      where   rt.dt_batch_date = dt.batch_date and
//                              rt.dt_batch_number = dt.batch_number and
//                              rt.dt_ddf_dt_id = dt.ddf_dt_id
//                   )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select dt.batch_date                as batch_date,\n                 dt.batch_number              as batch_number,\n                 dt.ddf_dt_id                 as ddf_dt_id,\n                 dt.trident_tran_id           as trident_tran_id,\n                 dt.purchase_id               as purchase_id,\n                 dt.client_reference_number   as client_ref_num\n          from   daily_detail_file_dt dt\n          where  dt.merchant_account_number =  :1   and\n                 dt.batch_date between   :2   and ( :3  +2) and\n                 dt.reference_number =  :4   and\n                 dt.cardholder_account_number =  :5   and\n                 not exists\n                 (  select  rt.retr_load_sec\n                    from    network_retrievals    rt\n                    where   rt.dt_batch_date = dt.batch_date and\n                            rt.dt_batch_number = dt.batch_number and\n                            rt.dt_ddf_dt_id = dt.ddf_dt_id\n                 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"24com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,batchDate);
   __sJT_st.setDate(3,batchDate);
   __sJT_st.setString(4,refNum);
   __sJT_st.setString(5,cardNum);
   // execute query
   tranIt = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"24com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:880^9*/
        tranResultSet = tranIt.getResultSet();

        if ( tranResultSet.next() )
        {
          /*@lineinfo:generated-code*//*@lineinfo:885^11*/

//  ************************************************************
//  #sql [Ctx] { update network_retrievals
//              set dt_batch_date           = :tranResultSet.getDate("batch_date"),
//                  dt_batch_number         = :tranResultSet.getLong("batch_number"),
//                  dt_ddf_dt_id            = :tranResultSet.getLong("ddf_dt_id"),
//                  trident_tran_id         = :tranResultSet.getString("trident_tran_id"),
//                  dt_purchase_id          = :tranResultSet.getString("purchase_id"),
//                  client_reference_number = :tranResultSet.getString("client_ref_num")
//              where retr_load_sec = :loadSec
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_4352 = tranResultSet.getDate("batch_date");
 long __sJT_4353 = tranResultSet.getLong("batch_number");
 long __sJT_4354 = tranResultSet.getLong("ddf_dt_id");
 String __sJT_4355 = tranResultSet.getString("trident_tran_id");
 String __sJT_4356 = tranResultSet.getString("purchase_id");
 String __sJT_4357 = tranResultSet.getString("client_ref_num");
   String theSqlTS = "update network_retrievals\n            set dt_batch_date           =  :1  ,\n                dt_batch_number         =  :2  ,\n                dt_ddf_dt_id            =  :3  ,\n                trident_tran_id         =  :4  ,\n                dt_purchase_id          =  :5  ,\n                client_reference_number =  :6  \n            where retr_load_sec =  :7 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_4352);
   __sJT_st.setLong(2,__sJT_4353);
   __sJT_st.setLong(3,__sJT_4354);
   __sJT_st.setString(4,__sJT_4355);
   __sJT_st.setString(5,__sJT_4356);
   __sJT_st.setString(6,__sJT_4357);
   __sJT_st.setLong(7,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:895^11*/
        }
        tranResultSet.close();
        tranIt.close();
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      // ignore?
      logEntry("linkRetrievalsToTransactions( " + loadFilename + " )", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e){}
      try { tranIt.close(); } catch(Exception e){}
    }
  }

  private boolean loadAuthRiskSummary( String loadFilename )
  {
    boolean                     fileProcessed   = false;
    ResultSetIterator           it              = null;
    ResultSet                   resultSet       = null;

    try
    {
      // load the risk summary table for this file
      /*@lineinfo:generated-code*//*@lineinfo:924^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    tc33_risk_summary
//          where   load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    tc33_risk_summary\n        where   load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"26com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:929^7*/

      /*@lineinfo:generated-code*//*@lineinfo:931^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  auth.merchant_number                          as merchant_number,
//                  count( 1 )                                    as total_count,
//                  sum( auth.authorized_amount )                 as total_amount,
//                  sum( decode( auth.response_code,
//                                '00',1,
//                                0))                             as approved_count,
//                  sum( decode( auth.response_code,
//                                '00',auth.authorized_amount,
//                                0))                             as approved_amount,
//                  sum( decode( auth.response_code,
//                                '00',0,
//                                1))                             as declined_count,
//                  sum( decode( auth.response_code,
//                                '00',0,
//                                auth.authorized_amount))        as declined_amount,
//                  sum( decode( auth.avs_result,
//                                null,0,
//                                1))                             as avs_count,
//                  sum( decode( auth.avs_result,
//                                null,0,
//                                auth.authorized_amount))        as avs_amount,
//                  sum( nvl( rc.high_risk_count,0 ))             as bad_auth_count,
//                  sum( nvl( ac.high_risk_count,0 ))             as bad_avs_count,
//                  sum( case when (auth.authorized_amount <= nvl(rss.threshold,1)) then 1
//                            else 0 end)                         as small_auth_count,
//                  max( nvl( rss.threshold,1 ) )                 as small_threshold_amount,
//                  auth.transaction_date                         as tran_date
//          from    tc33_authorization    auth,
//                  ( select  response_code           , 1 as high_risk_count
//                    from    tc33_authorization_resp_code
//                    where   nvl(high_risk,'N') = 'Y'
//                  ) rc,
//                  ( select  avs_result_code         , 1 as high_risk_count
//                    from    tc33_authorization_avs_code
//                    where   nvl(high_risk,'N') = 'Y'
//                  ) ac,
//                  ( select  substr(hierarchy_node,1,4)  as bank_number,
//                            max(nvl(threshold,1))       as threshold
//                    from    risk_score_system
//                    where   hierarchy_node like '%00000' and
//                            risk_cat_id = :RiskScoreDataBean.TT_SMALL_TICKET_AUTH -- 4
//                    group by substr(hierarchy_node,1,4)
//                  ) rss
//          where   auth.load_filename = :loadFilename
//                  and ( nvl(auth.card_type,'??') not in ('AM','DS','JC' )
//                        or (auth.card_type in ('AM')      and auth.merchant_number in
//                                (select merchant_number from mif where substr(nvl(amex_plan     ,'NN'),1,1) != 'N'))
//                        or (auth.card_type in ('DS','JC') and auth.merchant_number in
//                                (select merchant_number from mif where substr(nvl(discover_plan ,'NN'),1,1) != 'N'))
//                      )
//                  and nvl(auth.response_code,'??') <> '85'  -- don't count the "Account # Verification Request" auths
//                  and auth.response_code  = rc.response_code(+)
//                  and auth.avs_result     = ac.avs_result_code(+)
//                  and auth.bank_number    = rss.bank_number(+)
//          group by auth.transaction_date, auth.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  auth.merchant_number                          as merchant_number,\n                count( 1 )                                    as total_count,\n                sum( auth.authorized_amount )                 as total_amount,\n                sum( decode( auth.response_code,\n                              '00',1,\n                              0))                             as approved_count,\n                sum( decode( auth.response_code,\n                              '00',auth.authorized_amount,\n                              0))                             as approved_amount,\n                sum( decode( auth.response_code,\n                              '00',0,\n                              1))                             as declined_count,\n                sum( decode( auth.response_code,\n                              '00',0,\n                              auth.authorized_amount))        as declined_amount,\n                sum( decode( auth.avs_result,\n                              null,0,\n                              1))                             as avs_count,\n                sum( decode( auth.avs_result,\n                              null,0,\n                              auth.authorized_amount))        as avs_amount,\n                sum( nvl( rc.high_risk_count,0 ))             as bad_auth_count,\n                sum( nvl( ac.high_risk_count,0 ))             as bad_avs_count,\n                sum( case when (auth.authorized_amount <= nvl(rss.threshold,1)) then 1\n                          else 0 end)                         as small_auth_count,\n                max( nvl( rss.threshold,1 ) )                 as small_threshold_amount,\n                auth.transaction_date                         as tran_date\n        from    tc33_authorization    auth,\n                ( select  response_code           , 1 as high_risk_count\n                  from    tc33_authorization_resp_code\n                  where   nvl(high_risk,'N') = 'Y'\n                ) rc,\n                ( select  avs_result_code         , 1 as high_risk_count\n                  from    tc33_authorization_avs_code\n                  where   nvl(high_risk,'N') = 'Y'\n                ) ac,\n                ( select  substr(hierarchy_node,1,4)  as bank_number,\n                          max(nvl(threshold,1))       as threshold\n                  from    risk_score_system\n                  where   hierarchy_node like '%00000' and\n                          risk_cat_id =  :1   -- 4\n                  group by substr(hierarchy_node,1,4)\n                ) rss\n        where   auth.load_filename =  :2  \n                and ( nvl(auth.card_type,'??') not in ('AM','DS','JC' )\n                      or (auth.card_type in ('AM')      and auth.merchant_number in\n                              (select merchant_number from mif where substr(nvl(amex_plan     ,'NN'),1,1) != 'N'))\n                      or (auth.card_type in ('DS','JC') and auth.merchant_number in\n                              (select merchant_number from mif where substr(nvl(discover_plan ,'NN'),1,1) != 'N'))\n                    )\n                and nvl(auth.response_code,'??') <> '85'  -- don't count the \"Account # Verification Request\" auths\n                and auth.response_code  = rc.response_code(+)\n                and auth.avs_result     = ac.avs_result_code(+)\n                and auth.bank_number    = rss.bank_number(+)\n        group by auth.transaction_date, auth.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"27com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,RiskScoreDataBean.TT_SMALL_TICKET_AUTH);
   __sJT_st.setString(2,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"27com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:988^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        /*@lineinfo:generated-code*//*@lineinfo:993^9*/

//  ************************************************************
//  #sql [Ctx] { insert into tc33_risk_summary
//            (
//              merchant_number,
//              transaction_date,
//              vmc_auth_approved_count,
//              vmc_auth_approved_amount,
//              vmc_auth_decline_count,
//              vmc_auth_decline_amount,
//              vmc_auth_total_count,
//              vmc_auth_total_amount,
//              vmc_auth_avs_count,
//              vmc_auth_avs_amount,
//              vmc_bad_auth_count,
//              vmc_bad_avs_count,
//              vmc_small_auth_count,
//              vmc_small_threshold_amount,
//              load_filename
//            )
//            values
//            (
//              :resultSet.getLong("merchant_number"),
//              :resultSet.getDate("tran_date"),
//              :resultSet.getLong("approved_count"),
//              :resultSet.getDouble("approved_amount"),
//              :resultSet.getLong("declined_count"),
//              :resultSet.getDouble("declined_amount"),
//              :resultSet.getLong("total_count"),
//              :resultSet.getDouble("total_amount"),
//              :resultSet.getLong("avs_count"),
//              :resultSet.getDouble("avs_amount"),
//              :resultSet.getLong("bad_auth_count"),
//              :resultSet.getLong("bad_avs_count"),
//              :resultSet.getLong("small_auth_count"),
//              :resultSet.getDouble("small_threshold_amount"),
//              :loadFilename
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4358 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4359 = resultSet.getDate("tran_date");
 long __sJT_4360 = resultSet.getLong("approved_count");
 double __sJT_4361 = resultSet.getDouble("approved_amount");
 long __sJT_4362 = resultSet.getLong("declined_count");
 double __sJT_4363 = resultSet.getDouble("declined_amount");
 long __sJT_4364 = resultSet.getLong("total_count");
 double __sJT_4365 = resultSet.getDouble("total_amount");
 long __sJT_4366 = resultSet.getLong("avs_count");
 double __sJT_4367 = resultSet.getDouble("avs_amount");
 long __sJT_4368 = resultSet.getLong("bad_auth_count");
 long __sJT_4369 = resultSet.getLong("bad_avs_count");
 long __sJT_4370 = resultSet.getLong("small_auth_count");
 double __sJT_4371 = resultSet.getDouble("small_threshold_amount");
   String theSqlTS = "insert into tc33_risk_summary\n          (\n            merchant_number,\n            transaction_date,\n            vmc_auth_approved_count,\n            vmc_auth_approved_amount,\n            vmc_auth_decline_count,\n            vmc_auth_decline_amount,\n            vmc_auth_total_count,\n            vmc_auth_total_amount,\n            vmc_auth_avs_count,\n            vmc_auth_avs_amount,\n            vmc_bad_auth_count,\n            vmc_bad_avs_count,\n            vmc_small_auth_count,\n            vmc_small_threshold_amount,\n            load_filename\n          )\n          values\n          (\n             :1  ,\n             :2  ,\n             :3  ,\n             :4  ,\n             :5  ,\n             :6  ,\n             :7  ,\n             :8  ,\n             :9  ,\n             :10  ,\n             :11  ,\n             :12  ,\n             :13  ,\n             :14  ,\n             :15  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"28com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4358);
   __sJT_st.setDate(2,__sJT_4359);
   __sJT_st.setLong(3,__sJT_4360);
   __sJT_st.setDouble(4,__sJT_4361);
   __sJT_st.setLong(5,__sJT_4362);
   __sJT_st.setDouble(6,__sJT_4363);
   __sJT_st.setLong(7,__sJT_4364);
   __sJT_st.setDouble(8,__sJT_4365);
   __sJT_st.setLong(9,__sJT_4366);
   __sJT_st.setDouble(10,__sJT_4367);
   __sJT_st.setLong(11,__sJT_4368);
   __sJT_st.setLong(12,__sJT_4369);
   __sJT_st.setLong(13,__sJT_4370);
   __sJT_st.setDouble(14,__sJT_4371);
   __sJT_st.setString(15,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1031^9*/
      }
      resultSet.close();
      it.close();

      // queue the loading of the auth points
      /*@lineinfo:generated-code*//*@lineinfo:1037^7*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_process
//            ( process_type, load_filename )
//          values
//            ( :PT_AUTH_POINTS, :loadFilename )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_process\n          ( process_type, load_filename )\n        values\n          (  :1  ,  :2   )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"29com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,PT_AUTH_POINTS);
   __sJT_st.setString(2,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1043^7*/

      fileProcessed = true;
    }
    catch( Exception e )
    {
      logEntry("loadAuthRiskSummary( " + loadFilename + " )", e.toString());
    }
    finally
    {
    }
    return( fileProcessed );
  }

  private boolean loadCaptureRiskSummary( String loadFilename )
  {
    boolean             fileProcessed   = false;
    ResultSetIterator   it              = null;
    boolean             loadAuthRisk    = true;
    ResultSet           resultSet       = null;

    try
    {
      // load the risk summary table for this file
      /*@lineinfo:generated-code*//*@lineinfo:1067^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    tc33_risk_summary
//          where   load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    tc33_risk_summary\n        where   load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1072^7*/

      /*@lineinfo:generated-code*//*@lineinfo:1074^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_account_number              as merchant_number,
//                  dt.transaction_date                     as tran_date,
//                  count( 1 )                              as tran_count,
//                  sum( dt.transaction_amount )            as tran_amount
//          from    daily_detail_file_dt        dt
//          where   dt.load_filename = :loadFilename
//                  and dt.debit_credit_indicator = 'D'
//                  and dt.reject_reason is null
//          group by dt.transaction_date, dt.merchant_account_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_account_number              as merchant_number,\n                dt.transaction_date                     as tran_date,\n                count( 1 )                              as tran_count,\n                sum( dt.transaction_amount )            as tran_amount\n        from    daily_detail_file_dt        dt\n        where   dt.load_filename =  :1  \n                and dt.debit_credit_indicator = 'D'\n                and dt.reject_reason is null\n        group by dt.transaction_date, dt.merchant_account_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"31com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1085^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1090^9*/

//  ************************************************************
//  #sql [Ctx] { insert into tc33_risk_summary
//            (
//              merchant_number,
//              transaction_date,
//              vmc_sales_count,
//              vmc_sales_amount,
//              load_filename
//            )
//            values
//            (
//              :resultSet.getLong("merchant_number"),
//              :resultSet.getDate("tran_date"),
//              :resultSet.getLong("tran_count"),
//              :resultSet.getDouble("tran_amount"),
//              :loadFilename
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4372 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4373 = resultSet.getDate("tran_date");
 long __sJT_4374 = resultSet.getLong("tran_count");
 double __sJT_4375 = resultSet.getDouble("tran_amount");
   String theSqlTS = "insert into tc33_risk_summary\n          (\n            merchant_number,\n            transaction_date,\n            vmc_sales_count,\n            vmc_sales_amount,\n            load_filename\n          )\n          values\n          (\n             :1  ,\n             :2  ,\n             :3  ,\n             :4  ,\n             :5  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"32com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4372);
   __sJT_st.setDate(2,__sJT_4373);
   __sJT_st.setLong(3,__sJT_4374);
   __sJT_st.setDouble(4,__sJT_4375);
   __sJT_st.setString(5,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1108^9*/
      }
      resultSet.close();
      it.close();

      // load the credit/purchase matches for this file
      /*@lineinfo:generated-code*//*@lineinfo:1114^7*/

//  ************************************************************
//  #sql [Ctx] { update  daily_detail_file_credits
//          set     purchase_batch_date   = null,
//                  purchase_batch_number = null,
//                  purchase_ddf_dt_id    = null
//          where   load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  daily_detail_file_credits\n        set     purchase_batch_date   = null,\n                purchase_batch_number = null,\n                purchase_ddf_dt_id    = null\n        where   load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"33com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1121^7*/

      for( int goback = 0; goback <= 90; ++goback )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1125^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (dt idx_ddf_dt_bdate_merch_acct) */
//                    cred.batch_date     as credit_batch_date,
//                    cred.batch_number   as credit_batch_number,
//                    cred.ddf_dt_id      as credit_ddf_dt_id,
//                    dt.batch_date       as purch_batch_date,
//                    dt.batch_number     as purch_batch_number,
//                    dt.ddf_dt_id        as purch_ddf_dt_id
//            from    (
//                      select  /*+ index (idx_dt_credits_load_filename) */
//                              *
//                      from    daily_detail_file_credits
//                      where   load_filename = :loadFilename
//                          and purchase_batch_date is null
//                    ) cred,
//                    daily_detail_file_dt dt
//            where   dt.merchant_account_number = cred.merchant_number
//                    and dt.batch_date = (cred.batch_date-:goback)
//                    and dt.debit_credit_indicator = 'D'
//                    and dt.transaction_amount >= (cred.tran_amount * 0.90)
//                    and dt.cardholder_account_number = cred.card_number
//                    and not exists( select  linked.purchase_ddf_dt_id
//                                    from    daily_detail_file_credits linked
//                                    where   linked.purchase_batch_date    = dt.batch_date
//                                        and linked.purchase_batch_number  = dt.batch_number
//                                        and linked.purchase_ddf_dt_id     = dt.ddf_dt_id
//                                  )
//            order by cred.batch_date, cred.batch_number, cred.ddf_dt_id, dt.transaction_date desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (dt idx_ddf_dt_bdate_merch_acct) */\n                  cred.batch_date     as credit_batch_date,\n                  cred.batch_number   as credit_batch_number,\n                  cred.ddf_dt_id      as credit_ddf_dt_id,\n                  dt.batch_date       as purch_batch_date,\n                  dt.batch_number     as purch_batch_number,\n                  dt.ddf_dt_id        as purch_ddf_dt_id\n          from    (\n                    select  /*+ index (idx_dt_credits_load_filename) */\n                            *\n                    from    daily_detail_file_credits\n                    where   load_filename =  :1  \n                        and purchase_batch_date is null\n                  ) cred,\n                  daily_detail_file_dt dt\n          where   dt.merchant_account_number = cred.merchant_number\n                  and dt.batch_date = (cred.batch_date- :2  )\n                  and dt.debit_credit_indicator = 'D'\n                  and dt.transaction_amount >= (cred.tran_amount * 0.90)\n                  and dt.cardholder_account_number = cred.card_number\n                  and not exists( select  linked.purchase_ddf_dt_id\n                                  from    daily_detail_file_credits linked\n                                  where   linked.purchase_batch_date    = dt.batch_date\n                                      and linked.purchase_batch_number  = dt.batch_number\n                                      and linked.purchase_ddf_dt_id     = dt.ddf_dt_id\n                                )\n          order by cred.batch_date, cred.batch_number, cred.ddf_dt_id, dt.transaction_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setInt(2,goback);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"34com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1154^9*/
        resultSet = it.getResultSet();

        Date    lastCreditBatchDate   = null;
        long    lastCreditBatchNumber = -1;
        long    lastCreditDdfDtId     = -1;
        while( resultSet.next() )
        {
          Date    thisCreditBatchDate   = resultSet.getDate("credit_batch_date");
          long    thisCreditBatchNumber = resultSet.getLong("credit_batch_number");
          long    thisCreditDdfDtId     = resultSet.getLong("credit_ddf_dt_id");

          if( thisCreditBatchDate.equals(lastCreditBatchDate)  &&
              thisCreditBatchNumber == lastCreditBatchNumber  &&
              thisCreditDdfDtId     == lastCreditDdfDtId      )
          {
            continue; // we've already matched this credit to the most recent purchase
          }

          lastCreditBatchDate   = thisCreditBatchDate;
          lastCreditBatchNumber = thisCreditBatchNumber;
          lastCreditDdfDtId     = thisCreditDdfDtId;

          /*@lineinfo:generated-code*//*@lineinfo:1177^11*/

//  ************************************************************
//  #sql [Ctx] { update  daily_detail_file_credits
//              set     purchase_batch_date   = :resultSet.getDate("purch_batch_date"),
//                      purchase_batch_number = :resultSet.getLong("purch_batch_number"),
//                      purchase_ddf_dt_id    = :resultSet.getLong("purch_ddf_dt_id")
//              where   batch_date    = :thisCreditBatchDate
//                  and batch_number  = :thisCreditBatchNumber
//                  and ddf_dt_id     = :thisCreditDdfDtId
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_4376 = resultSet.getDate("purch_batch_date");
 long __sJT_4377 = resultSet.getLong("purch_batch_number");
 long __sJT_4378 = resultSet.getLong("purch_ddf_dt_id");
   String theSqlTS = "update  daily_detail_file_credits\n            set     purchase_batch_date   =  :1  ,\n                    purchase_batch_number =  :2  ,\n                    purchase_ddf_dt_id    =  :3  \n            where   batch_date    =  :4  \n                and batch_number  =  :5  \n                and ddf_dt_id     =  :6 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"35com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_4376);
   __sJT_st.setLong(2,__sJT_4377);
   __sJT_st.setLong(3,__sJT_4378);
   __sJT_st.setDate(4,thisCreditBatchDate);
   __sJT_st.setLong(5,thisCreditBatchNumber);
   __sJT_st.setLong(6,thisCreditDdfDtId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1186^11*/
        }
        resultSet.close();
        it.close();

        int numRemaining = 0;
        /*@lineinfo:generated-code*//*@lineinfo:1192^9*/

//  ************************************************************
//  #sql [Ctx] { select  /*+ index (idx_dt_credits_load_filename) */
//                    count(1)
//            
//            from    daily_detail_file_credits
//            where   load_filename = :loadFilename
//                and purchase_batch_date is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  /*+ index (idx_dt_credits_load_filename) */\n                  count(1)\n           \n          from    daily_detail_file_credits\n          where   load_filename =  :1  \n              and purchase_batch_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"36com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   numRemaining = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1200^9*/
        if( numRemaining == 0 )break;
      }

      // load the duplicate card info for this file
      loadDuplicateCardSummary(loadFilename);

      // queue the loading of the capture points
      /*@lineinfo:generated-code*//*@lineinfo:1208^7*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_process
//            ( process_type, load_filename )
//          values
//            ( :PT_CAPTURE_POINTS, :loadFilename )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_process\n          ( process_type, load_filename )\n        values\n          (  :1  ,  :2   )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"37com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,PT_CAPTURE_POINTS);
   __sJT_st.setString(2,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1214^7*/

      fileProcessed = true;
    }
    catch( Exception e )
    {
      logEntry("loadCaptureRiskSummary( " + loadFilename + " )", e.toString());
    }
    finally
    {
    }
    return( fileProcessed );
  }

  protected boolean loadChargebackFile( String loadFilename )
  {
    String                actionCode      = null;
    Date                  actionDate      = null;
    long                  actionLoadSec   = 0L;
    int                   actionSource    = MesChargebacks.AS_NONE;
    int                   bankNumber      = 0;
    Date                  batchDate       = null;
    String                cardNum         = null;
    String                cardType        = null;
    String                cbRefNum        = null;
    int                   curType         = 0;
    SettlementDb          db              = null;
    int                   entryCount      = 0;
    boolean               fileProcessed   = false;
    Date                  incomingDate    = null;
    ResultSetIterator     it              = null;
    long                  loadSec         = 0L;
    long                  merchantId      = 0L;
    int                   queueId         = 0;
    String                reasonCode      = null;
    String                reasonCodeToPersist      = null;
    int                   recCount        = 0;
    String                refNum          = null;
    ResultSet             resultSet       = null;
    int                   strOffset       = 0;
    ResultSetIterator     temp_it         = null;
    ResultSet             temp_rs         = null;
    long                  tempId          = 0L;
    double                tranAmount      = 0.0;
    Date                  tranDate        = null;
    boolean               tranDateMissing = false;
    String                tranId          = null;
    int                   usageCode       = 0;
    String                userMsg         = null;
    String				  dataRecord	  =null;

    List<String> MCInactivateReasonCodes = Arrays.asList(new String[] {"4801","4802","4835","4847","4857"});
    
    try
    {
      db = new SettlementDb();
      db.connect(true);

      /*@lineinfo:generated-code*//*@lineinfo:1268^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    network_chargebacks
//          where   load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    network_chargebacks\n        where   load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"38com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1273^7*/

      /*@lineinfo:generated-code*//*@lineinfo:1275^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    network_chargeback_activity
//          where   load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    network_chargeback_activity\n        where   load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"39com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1280^7*/

      for( int i = 0; i < 2; ++i )    // only Visa & MasterCard
      {
        SplitFundUtil.CardType cb_cardtype = null;
	
        switch(i)
        {
          case 0:     // visa
	    cb_cardtype = SplitFundUtil.CardType.VISA;

            /*@lineinfo:generated-code*//*@lineinfo:1291^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  cbv.load_sec                          as load_sec,
//                        cbv.chargeback_ref_num                as cb_ref_num,
//                        cbv.bank_number                       as bank_number,
//                        cbv.usage_code                        as usage_code,
//                        decode( cbv.usage_code, 1, 'Y', 9 , 'Y', 'N' ) as first_time,
//                        cbv.merchant_account_number           as merchant_number,
//                        cbv.merchant_name                     as merchant_name,
//                        cbv.reference_number                  as ref_num,
//                        cbv.account_number                    as card_number,
//                        nvl(cbv.actual_incoming_date,
//                            trunc(sysdate))                   as incoming_date,
//                        yddd_to_date(substr(cbv.reference_number,8,4))
//                                                              as batch_date,
//                        cbv.transaction_date                  as tran_date,
//                        cbv.transaction_amount                as tran_amount,
//                        CASE
//                        WHEN cbv.reason_code IN ('10', '11', '12', '13')
//                        THEN
//                          CASE
//                          WHEN cbv.dispute_condition IS NOT NULL
//                          THEN
//                            CASE
//                            WHEN LENGTH(TRIM (cbv.dispute_condition)) = 3 AND SUBSTR (cbv.dispute_condition, 2,1)='.'
//                            THEN cbv.reason_code || '.'|| SUBSTR (cbv.dispute_condition, 1,1)
//                            WHEN LENGTH(RTRIM(cbv.dispute_condition)) = 1
//                            THEN cbv.reason_code || '.'|| SUBSTR (RTRIM(cbv.dispute_condition), 1,1)  
//                            ELSE cbv.reason_code || ''           -- dispute_condition length is not 3 or 1 after trim
//                            END
//                          ELSE cbv.reason_code || ''             -- dispute_condition is null                             
//                          END
//                        ELSE cbv.reason_code || ''             -- reason_code <> in (10, 11, 12, 13)
//                        END                                   as reason_code,
//                        nvl(cbv.action_code,
//                            cbv.worked_chgbk_represent_flag)  as action_code,
//                        nvl(cbv.action_date,
//                            cbv.date_worked)                  as action_date,
//                        cbv.member_message_block              as user_message,
//                        cbv.authorization_number              as auth_code,
//                        cbv.trans_id                          as tran_id,
//                        cbv.mes_ref_num                       as mes_ref_num,
//                        cbv.card_number_enc                   as card_number_enc,
//                        cbv.debit_credit_ind                  as debit_credit_ind,
//                        cbv.load_filename                     as load_filename
//                from    network_chargeback_visa   cbv
//                where   cbv.load_filename = :loadFilename
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
	//@formatter:off
   String theSqlTS = " select  cbv.load_sec                          as load_sec, "
   		+ " cbv.chargeback_ref_num                as cb_ref_num, "
   		+ " cbv.bank_number                       as bank_number, "
   		+ " cbv.usage_code                        as usage_code, "
   		+ " decode( cbv.usage_code, 1, 'Y', 9 , 'Y', 'N' ) as first_time, "
   		+ " cbv.merchant_account_number           as merchant_number, "
   		+ " cbv.merchant_name                     as merchant_name, "
   		+ " cbv.reference_number                  as ref_num, "
   		+ " cbv.account_number                    as card_number, "
   		+ " nvl(cbv.actual_incoming_date, "
   		+ " trunc(sysdate))                   as incoming_date, "
   		+ " yddd_to_date(substr(cbv.reference_number,8,4)) as batch_date, "
   		+ " cbv.transaction_date                  as tran_date, "
   		+ " cbv.transaction_amount                as tran_amount, "
   		+ " CASE "
   		+ " WHEN cbv.reason_code IN ('10', '11', '12', '13') "
   		+ " THEN "
   		+ " CASE "
   		+ " WHEN cbv.dispute_condition IS NOT NULL "
   		+ " THEN "
   		+ " CASE "
   		+ " WHEN LENGTH(TRIM (cbv.dispute_condition)) = 3 AND SUBSTR (cbv.dispute_condition, 2,1)='.' "
   		+ " THEN cbv.reason_code || '.'|| SUBSTR (cbv.dispute_condition, 1,1) "
   		+ " WHEN LENGTH(RTRIM(cbv.dispute_condition)) = 1 "
   		+ " THEN cbv.reason_code || '.'|| SUBSTR (RTRIM(cbv.dispute_condition), 1,1) "
   		+ " ELSE cbv.reason_code || ''           -- dispute_condition length is not 3 or 1 after trim\n "
   		+ " END "
   		+ " ELSE cbv.reason_code || ''             -- dispute_condition is null \n"
   		+ " END "
   		+ " ELSE cbv.reason_code || '' -- reason_code <> in (10, 11, 12, 13) \n"
   		+ " END as reason_code, "
   		+ " nvl(cbv.action_code, cbv.worked_chgbk_represent_flag)  as action_code, "
   		+ " nvl(cbv.action_date, cbv.date_worked)                  as action_date, "
   		+ " cbv.member_message_block              as user_message, "
   		+ " cbv.authorization_number              as auth_code, "
   		+ " cbv.trans_id                          as tran_id, "
   		+ " cbv.mes_ref_num                       as mes_ref_num, "
   		+ " cbv.card_number_enc                   as card_number_enc, "
   		+ " cbv.debit_credit_ind                  as debit_credit_ind, "
   		+ " cbv.load_filename                     as load_filename, "
   		+ " NULL                   as data_record "
   		+ " from    network_chargeback_visa   cbv "
   		+ " where   cbv.load_filename =  :1 ";
 //@formatter:on
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"40com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"40com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1338^13*/
            break;

          case 1:   // mc
	    cb_cardtype = SplitFundUtil.CardType.MC;

            /*@lineinfo:generated-code*//*@lineinfo:1344^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  cbm.load_sec                          as load_sec,
//                        cbm.cb_ref_num                        as cb_ref_num,
//                        cbm.bank_number                       as bank_number,
//                        cbm.usage_code                        as usage_code,
//                        decode( cbm.usage_code, 1, 'Y', 'N' ) as first_time,
//                        cbm.merchant_account_number           as merchant_number,
//                        cbm.merchant_name                     as merchant_name,
//                        cbm.reference_number                  as ref_num,
//                        cbm.account_number                    as card_number,
//                        nvl(cbm.actual_incoming_date,
//                            trunc(sysdate))                   as incoming_date,
//                        yddd_to_date(substr(cbm.reference_number,8,4))
//                                                              as batch_date,
//                        cbm.transaction_date                  as tran_date,
//                        cbm.transaction_amount                as tran_amount,
//                        cbm.reason_code                       as reason_code,
//                        nvl(cbm.action_code,
//                            cbm.worked_chgbk_represent_flag)  as action_code,
//                        nvl(cbm.action_date,
//                            cbm.date_worked)                  as action_date,
//                        cbm.member_message_block              as user_message,
//                        cbm.authorization_number              as auth_code,
//                        (cbm.banknet_date || cbm.banknet_ref_num)
//                                                              as tran_id,
//                        cbm.mes_ref_num                       as mes_ref_num,
//                        cbm.card_number_enc                   as card_number_enc,
//                        cbm.debit_credit_ind                  as debit_credit_ind,
//                        cbm.load_filename                     as load_filename
//                from    network_chargeback_mc   cbm
//                where   cbm.load_filename = :loadFilename
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
	//@formatter:off
     String theSqlTS = " select  cbm.load_sec                          as load_sec, "
     		+ "cbm.cb_ref_num                        as cb_ref_num, "
     		+ "cbm.bank_number                       as bank_number, "
     		+ "cbm.usage_code                        as usage_code, "
     		+ "decode( cbm.usage_code, 1, 'Y', 'N' ) as first_time, "
     		+ "cbm.merchant_account_number           as merchant_number, "
     		+ "cbm.merchant_name                     as merchant_name, "
     		+ "cbm.reference_number                  as ref_num, "
     		+ "cbm.account_number                    as card_number, "
     		+ "nvl(cbm.actual_incoming_date, trunc(sysdate))                   as incoming_date, "
     		+ "yddd_to_date(substr(cbm.reference_number,8,4)) as batch_date, "
     		+ "cbm.transaction_date                  as tran_date, "
     		+ "cbm.transaction_amount                as tran_amount, "
     		+ "cbm.reason_code                       as reason_code, "
     		+ "nvl(cbm.action_code, "
     		+ "cbm.worked_chgbk_represent_flag)  as action_code, "
     		+ "nvl(cbm.action_date, "
     		+ "cbm.date_worked)                  as action_date, "
     		+ "cbm.member_message_block              as user_message, "
     		+ "cbm.authorization_number              as auth_code, "
     		+ "(cbm.banknet_date || cbm.banknet_ref_num) as tran_id, "
     		+ "cbm.mes_ref_num                       as mes_ref_num, "
     		+ "cbm.card_number_enc                   as card_number_enc, "
     		+ "cbm.debit_credit_ind                  as debit_credit_ind, "
     		+ "cbm.load_filename                     as load_filename, "
     		+ "cbm.data_record                     as data_record "
     		+ "from    network_chargeback_mc   cbm "
     		+ "where   cbm.load_filename =  :1 ";
   //@formatter:on
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"41com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"41com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1376^13*/
            break;

          default:
            continue;
        }
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          // extract base data
          cbRefNum          = resultSet.getString("cb_ref_num");
          refNum            = resultSet.getString("ref_num");
          merchantId        = resultSet.getLong("merchant_number");
          incomingDate      = resultSet.getDate("incoming_date");
          tranDate          = resultSet.getDate("tran_date");
          tranId            = resultSet.getString("tran_id");
          tranDateMissing   = false;
          tranAmount        = resultSet.getDouble("tran_amount");
          loadSec           = resultSet.getLong("load_sec");
          cardNum           = resultSet.getString("card_number");
          reasonCode        = resultSet.getString("reason_code");
          reasonCodeToPersist  = reasonCode;
          usageCode         = resultSet.getInt("usage_code");
          cardType          = TridentTools.decodeVisakCardType(cardNum);
          bankNumber        = resultSet.getInt("bank_number");

          // extract worked data
          actionLoadSec     = loadSec;    // default
          actionCode        = resultSet.getString("action_code");
          actionDate        = resultSet.getDate("action_date");
          actionSource      = ((actionCode == null) ? MesChargebacks.AS_NONE : MesChargebacks.AS_TSYS);
          userMsg           = resultSet.getString("user_message");
          dataRecord		=resultSet.getString("data_record");

       // added code for getting the reason code MC reason code
          
					if (reasonCodeToPersist != null && dataRecord != null && cardType != null && cardType.equals("MC")) {
						String dataRecordField = dataRecord.toUpperCase();
						if ("4834".equals(reasonCodeToPersist)) {
							if (dataRecordField.startsWith("FIRST REF")) {
								reasonCodeToPersist = "4834.1";
							}
							else if (dataRecordField.equals("POI CURRENCY CONVERSION")) {
								reasonCodeToPersist = "4834.2";
							}
							else if (dataRecordField.equals("INCORRECT CARDHOLDER CURRENCY")) {
								reasonCodeToPersist = "4834.3";
							}
							else if (dataRecordField.equals("LATE PRESENTMENT")) {
								reasonCodeToPersist = "4834.4";
							}

						}
						else if ("4837".equals(reasonCodeToPersist)) {
							if (MULTIPLE_TRANSACTIONS_REGEX.matcher(dataRecordField).find()) {
								reasonCodeToPersist = "4837.1";
							}
							else if (dataRecordField.equals("FNS")) {
								reasonCodeToPersist = "4837.2";
							}
							else if (REGEX.matcher(dataRecordField).find()) {
								reasonCodeToPersist = "4837.3";
							}
							else if (AUTH_REGEX.matcher(dataRecordField).find()) {
								reasonCodeToPersist = "4837.4";
							}
							else if (FNS_COUNT_REGEX.matcher(dataRecordField).find()) {
								reasonCodeToPersist = "4837.5";
							}
						}
						else if ("4853".equals(reasonCodeToPersist)) {

							if (NO_SHOW_REGEX.matcher(dataRecordField).find() || RS5_REGEX.matcher(dataRecordField).find()) {
								reasonCodeToPersist = "4853.1";
							}
							else if (dataRecordField.equals("COUNTERFEIT")) {
								reasonCodeToPersist = "4853.2";
							}
							else if (RATE_QUOTE_REGEX.matcher(dataRecordField).find()) {
								reasonCodeToPersist = "4853.3";
							}
							else if (DIGITAL_GOODS_REGEX.matcher(dataRecordField).find()) {
								reasonCodeToPersist = "4853.4";
							}
							else if (MULTIPLE_TRANSACTIONS_REGEX.matcher(dataRecordField).find()) {
								reasonCodeToPersist = "4853.5";
							}
							else if (dataRecordField.equals("TIMESHARE")) {
								reasonCodeToPersist = "4853.6";
							}

						}
					}
          // reset locals for the new row
          queueId           = 0;

          // use transaction data to resolve the merchant number
          if ( merchantId == 0L )
          {
            merchantId = db.resolveMerchantId(bankNumber,cardNum,refNum,tranId,false);
            if ( merchantId == 0L && bankNumber == 3941)
            {
              merchantId = db.resolveMerchantId(3943,cardNum,refNum,tranId,false);
            }
          }

          // full card with tran id failed, search using full card number and no tran id
          if ( merchantId == 0L )
          {
            merchantId = db.resolveMerchantId(bankNumber,cardNum,refNum,null,false);
            if ( merchantId == 0L && bankNumber == 3941)
            {
              merchantId = db.resolveMerchantId(3943,cardNum,refNum,null,false);
            }
          }

          // full card no tran id failed, search using only the partial card number and no tran id
          if ( merchantId == 0L && tranId != null )
          {
            merchantId = db.resolveMerchantId(bankNumber,cardNum,refNum,null,true);
            if ( merchantId == 0L && bankNumber == 3941 )
            {
              merchantId = db.resolveMerchantId(3943,cardNum,refNum,null,true);
            }
          }

          if ( tranDate == null )
          {
            // tran date was not present in the incoming file so assume that
            // the transaction date was one day before the processing julian
            // date encoded in the transaction reference number.
            /*@lineinfo:generated-code*//*@lineinfo:1448^13*/

//  ************************************************************
//  #sql [Ctx] { select (yddd_to_date(substr(:refNum,8,4)) - 1) 
//                from   dual
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select (yddd_to_date(substr( :1  ,8,4)) - 1)  \n              from   dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"42com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,refNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   tranDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1452^13*/
            tranDateMissing = true;
          }

          // setup the offset into the cb ref num.  this will
          // recover when the operator inserts their initials into
          // the first three digits of the cb ref num.
          try
          {
            Double.parseDouble(cbRefNum);
            strOffset = 1;
          }
          catch(Exception e)
          {
            strOffset = 4;
          }

          /*@lineinfo:generated-code*//*@lineinfo:1469^11*/

//  ************************************************************
//  #sql [Ctx] { -- determine how many chargebacks this action could
//              -- be applied to.  most of the time this will be just 1.
//              select  count( cb.cb_load_sec ) 
//              from    network_chargebacks cb
//              where   ( :cbRefNum is null
//                        or lpad(substr(cb.cb_ref_num,:strOffset),10,'0') =
//                            lpad(substr(:cbRefNum,:strOffset),10,'0') )
//                      and cb.reference_number  = :refNum
//                      and cb.incoming_date     = :incomingDate
//                      and cb.card_number       = :cardNum
//                      and cb.tran_date         = :tranDate
//                      and cb.tran_amount       = :tranAmount
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "-- determine how many chargebacks this action could\n            -- be applied to.  most of the time this will be just 1.\n            select  count( cb.cb_load_sec )  \n            from    network_chargebacks cb\n            where   (  :1   is null\n                      or lpad(substr(cb.cb_ref_num, :2  ),10,'0') =\n                          lpad(substr( :3  , :4  ),10,'0') )\n                    and cb.reference_number  =  :5  \n                    and cb.incoming_date     =  :6  \n                    and cb.card_number       =  :7  \n                    and cb.tran_date         =  :8  \n                    and cb.tran_amount       =  :9 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"43com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,cbRefNum);
   __sJT_st.setInt(2,strOffset);
   __sJT_st.setString(3,cbRefNum);
   __sJT_st.setInt(4,strOffset);
   __sJT_st.setString(5,refNum);
   __sJT_st.setDate(6,incomingDate);
   __sJT_st.setString(7,cardNum);
   __sJT_st.setDate(8,tranDate);
   __sJT_st.setDouble(9,tranAmount);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   entryCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1483^11*/

          //*****************************************************************
          //   3858 chargebacks come in one time in the chgbk3858_*.dat file
          //        all worked activity comes subsequent work3858_*.dat file
          //   3941 chargebacks and subsequent activity all come in the
          //        the chgbk3941_*.dat file.  The actionCode will be null
          //        the first time the chargeback hits the system.
          //
          //   Note:  It is critical that chargebacks are loaded in the order
          //          they were received by Vital.  Any reload of this data
          //          should insure that the records come sequentially.
          //*****************************************************************
          if ( ( actionCode == null ) || ( entryCount == 0 ) )
          {
		// Add split chargeback rules, if necessary.
		boolean splits = SplitFundUtil.addSplitChargebackRules(cb_cardtype, refNum, loadSec, merchantId, incomingDate, tranAmount, cardNum);

            /*@lineinfo:generated-code*//*@lineinfo:1501^13*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargebacks
//                (
//                  cb_load_sec,
//                  cb_ref_num,
//                  merchant_number,
//                  bank_number,
//                  incoming_date,
//                  reference_number,
//                  card_number,
//                  card_type,
//                  tran_date,
//                  tran_amount,
//                  merchant_name,
//                  reason_code,
//                  auth_code,
//                  load_filename,
//                  first_time_chargeback,
//                  mes_ref_num,
//                  card_number_enc,
//                  debit_credit_ind,
//                  tran_date_missing,
//  				split_funding_ind
//                )
//                values
//                (
//                  :loadSec,
//                  :cbRefNum,
//                  :merchantId,
//                  :bankNumber,
//                  :incomingDate,
//                  :refNum,
//                  :cardNum,
//                  :cardType,
//                  :tranDate,
//                  :tranAmount,
//                  :resultSet.getString("merchant_name"),
//                  :reasonCode,
//                  :resultSet.getString("auth_code"),
//                  :loadFilename,
//                  :resultSet.getString("first_time"),
//                  :resultSet.getString("mes_ref_num"),
//                  :resultSet.getString("card_number_enc"),
//                  :resultSet.getString("debit_credit_ind"),
//                  :tranDateMissing ? "Y" : "N",
//  				:splits ? "Y" : "N"
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4379 = resultSet.getString("merchant_name");
 String __sJT_4380 = resultSet.getString("auth_code");
 String __sJT_4381 = resultSet.getString("first_time");
 String __sJT_4382 = resultSet.getString("mes_ref_num");
 String __sJT_4383 = resultSet.getString("card_number_enc");
 String __sJT_4384 = resultSet.getString("debit_credit_ind");
 String __sJT_4385 = tranDateMissing ? "Y" : "N";
 String __sJT_4386 = splits ? "Y" : "N";
//@formatter:off
 String theSqlTS = " insert into network_chargebacks "
 		+ " ( "
 		+ " cb_load_sec, "
 		+ " cb_ref_num, "
 		+ " merchant_number, "
 		+ " bank_number ,"
 		+ " incoming_date, "
 		+ " reference_number, "
 		+ " card_number, "
 		+ " card_type, "
 		+ " tran_date, "
 		+ " tran_amount, "
 		+ " merchant_name, "
 		+ " reason_code, "
 		+ " auth_code, "
 		+ " load_filename, "
 		+ " first_time_chargeback, "
 		+ " mes_ref_num, "
 		+ " card_number_enc, "
 		+ " debit_credit_ind, "
 		+ " tran_date_missing, "
 		+ " split_funding_ind, "
 		+ " data_record "
 		+ " ) "
 		+ " values "
 		+ " ( "
 		+ " :1, "
 		+ " :2, "
 		+ " :3, "
 		+ " :4, "
 		+ " :5, "
 		+ " :6, "
 		+ " :7, "
 		+ " :8, "
 		+ " :9, "
 		+ " :10, "
 		+ " :11, "
 		+ " :12, "
 		+ " :13, "
 		+ " :14, "
 		+ " :15, "
 		+ " :16, "
 		+ " :17, "
 		+ " :18, "
 		+ " :19, "
 		+ " :20, "
 		+ " :21  "
 		+ " ) ";
//@formatter:on
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"44com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,cbRefNum);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setInt(4,bankNumber);
   __sJT_st.setDate(5,incomingDate);
   __sJT_st.setString(6,refNum);
   __sJT_st.setString(7,cardNum);
   __sJT_st.setString(8,cardType);
   __sJT_st.setDate(9,tranDate);
   __sJT_st.setDouble(10,tranAmount);
   __sJT_st.setString(11,__sJT_4379);
   __sJT_st.setString(12,reasonCodeToPersist);
   __sJT_st.setString(13,__sJT_4380);
   __sJT_st.setString(14,loadFilename);
   __sJT_st.setString(15,__sJT_4381);
   __sJT_st.setString(16,__sJT_4382);
   __sJT_st.setString(17,__sJT_4383);
   __sJT_st.setString(18,__sJT_4384);
   __sJT_st.setString(19,__sJT_4385);
   __sJT_st.setString(20,__sJT_4386);
   __sJT_st.setString(21,dataRecord);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1549^13*/
          }

          // if this is an incoming item check for auto action
          // ** only apply auto actions to first time chargebacks
          if ( actionCode == null && (usageCode == 1 || usageCode ==9))
          {
            CBAutoAction autoAction = getCBAutoAction( bankNumber, merchantId, cardType, reasonCode, tranDate, usageCode );

            // if there was an auto action entry for this
            // account or reason code then setup to work
            // this chargeback
            if ( autoAction != null )
            {
              actionCode    = autoAction.ActionCode;
              actionSource  = autoAction.ActionSource;
              autoAction    = null;    // release memory
            }

            // if this entry has an auto action setup the other
            // action fields with the appropriate values
            if ( actionCode != null )
            {
              switch( actionSource )
              {
                case MesChargebacks.AS_AUTO_LIST:
                  userMsg = "Auto Action - List";
                  break;

                case MesChargebacks.AS_AUTO_REASON:
                  userMsg = "Auto Action - Reason";
                  break;

                default:
                  userMsg = "Auto Action";
                  break;
              }

              // the query below sets the action date to the
              // current date (if the time is before 4 PM) or
              // the next day.  if the resulting date is
              // saturday (7) or sunday (1) then the query
              // adds the days necessary so the action will
              // post on the next monday.
              /*@lineinfo:generated-code*//*@lineinfo:1593^15*/

//  ************************************************************
//  #sql [Ctx] { select  ( (trunc(sysdate) + trunc(to_number(to_char(sysdate,'hh24mm'))/1600)) +
//                            decode( to_number(to_char((trunc(sysdate) + trunc(to_number(to_char(sysdate,'hh24mm'))/1600)),'D')),
//                                    7, 2,   -- saturday
//                                    1, 1,   -- sunday
//                                    0 )
//                          )               
//                  from    dual
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  ( (trunc(sysdate) + trunc(to_number(to_char(sysdate,'hh24mm'))/1600)) +\n                          decode( to_number(to_char((trunc(sysdate) + trunc(to_number(to_char(sysdate,'hh24mm'))/1600)),'D')),\n                                  7, 2,   -- saturday\n                                  1, 1,   -- sunday\n                                  0 )\n                        )                \n                from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"45com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   actionDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1602^15*/

              // setup the action load sec to a value
              // other than the incoming file load sec
              // this makes it easier to identify
              // incoming items that were not resolved
              // to the original incoming item
              actionLoadSec = 0L;   // reset
              do
              {
                actionLoadSec++;

                /*@lineinfo:generated-code*//*@lineinfo:1614^17*/

//  ************************************************************
//  #sql [Ctx] { select  count(cba.cb_load_sec) 
//                    from    network_chargeback_activity cba
//                    where   cba.cb_load_sec = :loadSec and
//                            cba.file_load_sec = :actionLoadSec
//                   };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cba.cb_load_sec)  \n                  from    network_chargeback_activity cba\n                  where   cba.cb_load_sec =  :1   and\n                          cba.file_load_sec =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"46com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setLong(2,actionLoadSec);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1620^17*/
              } while ( recCount != 0 );
            }
          }//if actionCode==null

          // check for out of time frame chargebacks
          if ( actionCode != null )
          {
            //check actionCode, if MCHB ('D') then apply extra rules before it
            //gets assigned...
            if( actionCode.equals("D"))
            {
              //
              // RULE: auto REPR any transaction date over 125 days
              // we'll ignore those that had a missing tranDate just to be safe
              // also ignore items worked on TSYS
              //
              if( !tranDateMissing && actionSource != MesChargebacks.AS_TSYS )
              {
                if( MesCalendar.daysBetween(tranDate, incomingDate) > 125d )
                {
                  /* original idea was to auto REPR
                  actionCode = "S"; //REPR code
                  actionSource = MesChargebacks.AS_AUTO_REASON_BY_RULE;
                  userMsg = "Auto Action - Rule";
                  */

                  //want to avoid any auto processing below
                  actionCode = null;

                  //ensure that it goes into proper queue
                  //otherwise method below will assign it to an old queue
                  queueId = MesQueues.Q_CHARGEBACKS_OVER_125;
                }
              }
            }//done rules for auto MCHB items
            else if(actionCode.equals("X"))
            {
              //*************************************************
              //RULE: move these items into the EXCEPTION queue
              //and null the actionCode - DO NOT allow them
              //to be auto processed further (no entry into
              //the network_chargeback_activity below)
              //*************************************************

              queueId     = MesQueues.Q_CHARGEBACKS_EXCEPTION;
              actionCode  = null;
            }
          }
          
          // if MC inactive reason code, then push to New/Incoming charge back queue and pushed
          if("MC".equals(cardType) && MCInactivateReasonCodes.contains(reasonCode)) { 
        	  actionCode=null;
        	  queueId = MesQueues.Q_CHARGEBACKS_NEW;
          }

          // if this item has an action then update the activity table
          if ( actionCode != null )
          {

            // if this action could apply to more than one chargeback,
            // then add the action to the chargeback that has the least
            // actions.  in the event of a tie, apply the action to the
            // chargeback that was received by our system first.
            /*@lineinfo:generated-code*//*@lineinfo:1678^13*/

//  ************************************************************
//  #sql [Ctx] temp_it = { select cb.cb_load_sec               as cb_load_sec,
//                       count( cba.cb_load_sec )     as entry_count
//                from   network_chargebacks          cb,
//                       network_chargeback_activity  cba
//                where   ( :cbRefNum is null
//                          or lpad(substr(cb.cb_ref_num,:strOffset),10,'0') =
//                              lpad(substr(:cbRefNum,:strOffset),10,'0') )
//                       and cb.reference_number  = :refNum
//                       and cb.incoming_date     = :incomingDate
//                       and cb.card_number       = :cardNum
//                       and cb.tran_date         = :tranDate
//                       and cb.tran_amount       = :tranAmount
//                       and cba.cb_load_sec(+)   = cb.cb_load_sec
//                group by cb.cb_load_sec
//                order by entry_count, cb.cb_load_sec
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select cb.cb_load_sec               as cb_load_sec,\n                     count( cba.cb_load_sec )     as entry_count\n              from   network_chargebacks          cb,\n                     network_chargeback_activity  cba\n              where   (  :1   is null\n                        or lpad(substr(cb.cb_ref_num, :2  ),10,'0') =\n                            lpad(substr( :3  , :4  ),10,'0') )\n                     and cb.reference_number  =  :5  \n                     and cb.incoming_date     =  :6  \n                     and cb.card_number       =  :7  \n                     and cb.tran_date         =  :8  \n                     and cb.tran_amount       =  :9  \n                     and cba.cb_load_sec(+)   = cb.cb_load_sec\n              group by cb.cb_load_sec\n              order by entry_count, cb.cb_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"47com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,cbRefNum);
   __sJT_st.setInt(2,strOffset);
   __sJT_st.setString(3,cbRefNum);
   __sJT_st.setInt(4,strOffset);
   __sJT_st.setString(5,refNum);
   __sJT_st.setDate(6,incomingDate);
   __sJT_st.setString(7,cardNum);
   __sJT_st.setDate(8,tranDate);
   __sJT_st.setDouble(9,tranAmount);
   // execute query
   temp_it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"47com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1695^13*/
            temp_rs = temp_it.getResultSet();

            if ( temp_rs.next() )
            {
              // overload the incoming file load sec
              // in favor of the original chargeback load_sec
              loadSec = temp_rs.getLong("cb_load_sec");

              // the first entry is the one we want, it is either
              // the item with the least activity, or the item
              // received first by the system.
              /*@lineinfo:generated-code*//*@lineinfo:1707^15*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargeback_activity
//                  (
//                    cb_load_sec,
//                    file_load_sec,
//                    action_code,
//                    action_date,
//                    user_message,
//                    action_source,
//                    user_login,
//                    load_filename
//                  )
//                  values
//                  (
//                    :loadSec,
//                    :actionLoadSec,
//                    :actionCode,
//                    :actionDate,
//                    :userMsg,
//                    :actionSource,
//                    :(actionSource == MesChargebacks.AS_TSYS) ? "tsys" : "system",
//                    :(actionSource == MesChargebacks.AS_TSYS) ? loadFilename : null
//                  )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4387 = (actionSource == MesChargebacks.AS_TSYS) ? "tsys" : "system";
 String __sJT_4388 = (actionSource == MesChargebacks.AS_TSYS) ? loadFilename : null;
   String theSqlTS = "insert into network_chargeback_activity\n                (\n                  cb_load_sec,\n                  file_load_sec,\n                  action_code,\n                  action_date,\n                  user_message,\n                  action_source,\n                  user_login,\n                  load_filename\n                )\n                values\n                (\n                   :1  ,\n                   :2  ,\n                   :3  ,\n                   :4  ,\n                   :5  ,\n                   :6  ,\n                   :7  ,\n                   :8  \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"48com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setLong(2,actionLoadSec);
   __sJT_st.setString(3,actionCode);
   __sJT_st.setDate(4,actionDate);
   __sJT_st.setString(5,userMsg);
   __sJT_st.setInt(6,actionSource);
   __sJT_st.setString(7,__sJT_4387);
   __sJT_st.setString(8,__sJT_4388);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1731^15*/

              // occassionally a chargeback comes into
              // the system without a merchant number or
              // gets assigned an incorrect merchant #.
              // the correct merchant number is then assigned
              // during the chargeback work process.  this
              // snippet updates the chargeback to match the
              // most recent merchant number received from TSYS.
              /*@lineinfo:generated-code*//*@lineinfo:1740^15*/

//  ************************************************************
//  #sql [Ctx] { update network_chargebacks cb
//                  set merchant_number = :merchantId
//                  where cb.cb_load_sec = :loadSec and
//                        :merchantId != 0 and
//                        cb.merchant_number != :merchantId
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update network_chargebacks cb\n                set merchant_number =  :1  \n                where cb.cb_load_sec =  :2   and\n                       :3   != 0 and\n                      cb.merchant_number !=  :4 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"49com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setLong(2,loadSec);
   __sJT_st.setLong(3,merchantId);
   __sJT_st.setLong(4,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1747^15*/
            }
            temp_rs.close();
            temp_it.close();
          }//if actionCode != null

          // copy any user messages posted on TSYS to the notes table
          if ( userMsg != null )
          {
            /*@lineinfo:generated-code*//*@lineinfo:1756^13*/

//  ************************************************************
//  #sql [Ctx] { insert into q_notes
//                (
//                  id,
//                  type,
//                  item_type,
//                  note_source,
//                  note
//                )
//                values
//                (
//                  :loadSec,
//                  1701,     -- all chargebacks
//                  18,       -- chargeback
//                  'system',
//                  substr(:userMsg,1,500)
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into q_notes\n              (\n                id,\n                type,\n                item_type,\n                note_source,\n                note\n              )\n              values\n              (\n                 :1  ,\n                1701,     -- all chargebacks\n                18,       -- chargeback\n                'system',\n                substr( :2  ,1,500)\n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"50com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setString(2,userMsg);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1774^13*/
          }

          // determine which queue this item should be in
          //if queue hasn't already been assigned above

          if( queueId == 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:1781^13*/

//  ************************************************************
//  #sql [Ctx] { select  get_cb_queue_assignment(:bankNumber,
//                                                :merchantId,
//                                                :cardNum,
//                                                :reasonCode,
//                                                :usageCode,
//                                                :actionCode )
//                
//                from    dual
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  get_cb_queue_assignment( :1  ,\n                                               :2  ,\n                                               :3  ,\n                                               :4  ,\n                                               :5  ,\n                                               :6   )\n               \n              from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"51com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setLong(2,merchantId);
   __sJT_st.setString(3,cardNum);
   __sJT_st.setString(4,reasonCode);
   __sJT_st.setInt(5,usageCode);
   __sJT_st.setString(6,actionCode);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   queueId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1791^13*/
          }
          // found a valid queue for this item
          if ( queueId != 0 )
          {
            // see if this item is already in a queue
            try
            {
              /*@lineinfo:generated-code*//*@lineinfo:1799^15*/

//  ************************************************************
//  #sql [Ctx] { select  nvl(qd.type,0) 
//                  from    q_data      qd
//                  where   qd.item_type = 18 and   -- existing entry
//                          qd.id = :loadSec
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  nvl(qd.type,0)  \n                from    q_data      qd\n                where   qd.item_type = 18 and   -- existing entry\n                        qd.id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"52com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   curType = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1805^15*/
            }
            catch( java.sql.SQLException sqe )
            {
              curType = 0;
            }

            // item does not exist in a queue yet, create new entry
            if ( curType == 0 )
            {
              String source = loadFilename.startsWith("chgbk") ? "tsys" : "mes";
              /*@lineinfo:generated-code*//*@lineinfo:1816^15*/

//  ************************************************************
//  #sql [Ctx] { insert into q_data
//                  (
//                    id,
//                    type,
//                    item_type,
//                    owner,
//                    date_created,
//                    source,
//                    affiliate,
//                    last_changed,
//                    last_user
//                  )
//                  values
//                  (
//                    :loadSec,
//                    :queueId,
//                    18,       -- items are chargebacks
//                    1,
//                    :incomingDate,
//                    :source,
//                    to_char(:bankNumber),
//                    :actionDate,
//                    decode(:actionDate,null,null,'system')
//                  )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into q_data\n                (\n                  id,\n                  type,\n                  item_type,\n                  owner,\n                  date_created,\n                  source,\n                  affiliate,\n                  last_changed,\n                  last_user\n                )\n                values\n                (\n                   :1  ,\n                   :2  ,\n                  18,       -- items are chargebacks\n                  1,\n                   :3  ,\n                   :4  ,\n                  to_char( :5  ),\n                   :6  ,\n                  decode( :7  ,null,null,'system')\n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"53com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadSec);
   __sJT_st.setInt(2,queueId);
   __sJT_st.setDate(3,incomingDate);
   __sJT_st.setString(4,source);
   __sJT_st.setInt(5,bankNumber);
   __sJT_st.setDate(6,actionDate);
   __sJT_st.setDate(7,actionDate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1842^15*/
            }
            else  // update existing queue entry
            {
              /*@lineinfo:generated-code*//*@lineinfo:1846^15*/

//  ************************************************************
//  #sql [Ctx] { update q_data qd
//                  set qd.type = :queueId,
//                      qd.last_changed = :actionDate,
//                      qd.last_user = decode(:actionDate,null,null,'system')
//                  where qd.type = :curType and
//                        qd.id = :loadSec
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update q_data qd\n                set qd.type =  :1  ,\n                    qd.last_changed =  :2  ,\n                    qd.last_user = decode( :3  ,null,null,'system')\n                where qd.type =  :4   and\n                      qd.id =  :5 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"54com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,queueId);
   __sJT_st.setDate(2,actionDate);
   __sJT_st.setDate(3,actionDate);
   __sJT_st.setInt(4,curType);
   __sJT_st.setLong(5,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1854^15*/
            }
          }   // end if ( queueId != 0 )
        }   // end while(resultSet.next())
        resultSet.close();
        it.close();
      }     // end for loop

      // queue the loading of chargeback risk points
      /*@lineinfo:generated-code*//*@lineinfo:1863^7*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_process
//          (
//            process_type,
//            load_filename
//          )
//          values
//          (
//            1,
//            :loadFilename
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_process\n        (\n          process_type,\n          load_filename\n        )\n        values\n        (\n          1,\n           :1  \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"55com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1875^7*/
      fileProcessed = true;
    }
    catch( Exception e )
    {
      // ignore?
      logEntry("loadChargebackFile( " + loadFilename + " )", e.toString());
	log.error("Error processing chargebacks.", e);
    }
    finally
    {
      try { it.close();       } catch(Exception e){}
      try { temp_it.close();  } catch(Exception e){}
      try { db.cleanUp();     } catch(Exception e){}
    }
    return( fileProcessed );
  }

  private boolean loadChargebackRetrievalPoints( String loadFilename )
  {
    Date                        activityDate    = null;
    StringBuffer                desc            = new StringBuffer();
    boolean                     fileProcessed   = false;
    ResultSetIterator           it              = null;
    long                        loadFileId      = 0L;
    int                         points          = 0;
    ResultSet                   resultSet       = null;

    try
    {
      try
      {
        /*@lineinfo:generated-code*//*@lineinfo:1907^9*/

//  ************************************************************
//  #sql [Ctx] { select  max( cb.incoming_date ) 
//            from    network_chargebacks           cb
//            where   cb.load_filename = :loadFilename
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max( cb.incoming_date )  \n          from    network_chargebacks           cb\n          where   cb.load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"56com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1912^9*/
      }
      catch( java.sql.SQLException sqe )
      {
        // this happens then the file is a TSYS
        // worked file and there were no new
        // chargebacks in the file.
      }

      if ( activityDate == null )
      {
        try
        {
          /*@lineinfo:generated-code*//*@lineinfo:1925^11*/

//  ************************************************************
//  #sql [Ctx] { select  max( rt.incoming_date ) 
//              from    network_retrievals            rt
//              where   rt.load_filename = :loadFilename
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max( rt.incoming_date )  \n            from    network_retrievals            rt\n            where   rt.load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"57com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1930^11*/
        }
        catch( java.sql.SQLException sqe )
        {
          // this happens then the file is a TSYS
          // worked file and there were no new
          // chargebacks in the file.
        }
      }

      if ( activityDate != null )
      {
        loadFileId = loadFilenameToLoadFileId(loadFilename);

        /*@lineinfo:generated-code*//*@lineinfo:1944^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    risk_score
//            where   load_file_id = :loadFileId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    risk_score\n          where   load_file_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"58com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1949^9*/

        // load the ratios for all the merchants
        // that have chargebacks over the last 7 days
        /*@lineinfo:generated-code*//*@lineinfo:1953^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.merchant_number          as merchant_number,
//                    sum(cb.tran_amount)         as cb_amount,
//                    count(cb.tran_amount)       as cb_count,
//                    nvl(gn.t1_tot_amount_of_sales,0)  as sales_amount,
//                    round( ( sum(cb.tran_amount)/
//                             decode( gn.t1_tot_amount_of_sales,
//                                      null, 1,
//                                      0, 1,
//                                      gn.t1_tot_amount_of_sales )
//                            ) * 100, 5 )        as ratio
//            from    network_chargebacks         cb,
//                    monthly_extract_gn          gn
//            where   cb.merchant_number in
//                      (select distinct merchant_number from network_chargebacks where load_filename = :loadFilename) and
//                    cb.incoming_date between (:activityDate-7) and :activityDate and
//                    gn.hh_merchant_number(+) = cb.merchant_number and
//                    gn.hh_active_date(+) = trunc( (trunc(:activityDate,'month')-1), 'month' )
//            group by cb.merchant_number, gn.t1_tot_amount_of_sales
//            order by cb.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.merchant_number          as merchant_number,\n                  sum(cb.tran_amount)         as cb_amount,\n                  count(cb.tran_amount)       as cb_count,\n                  nvl(gn.t1_tot_amount_of_sales,0)  as sales_amount,\n                  round( ( sum(cb.tran_amount)/\n                           decode( gn.t1_tot_amount_of_sales,\n                                    null, 1,\n                                    0, 1,\n                                    gn.t1_tot_amount_of_sales )\n                          ) * 100, 5 )        as ratio\n          from    network_chargebacks         cb,\n                  monthly_extract_gn          gn\n          where   cb.merchant_number in\n                    (select distinct merchant_number from network_chargebacks where load_filename =  :1  ) and\n                  cb.incoming_date between ( :2  -7) and  :3   and\n                  gn.hh_merchant_number(+) = cb.merchant_number and\n                  gn.hh_active_date(+) = trunc( (trunc( :4  ,'month')-1), 'month' )\n          group by cb.merchant_number, gn.t1_tot_amount_of_sales\n          order by cb.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"59com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setDate(3,activityDate);
   __sJT_st.setDate(4,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"59com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1974^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          points = getPoints( resultSet.getLong("merchant_number"),
                              RiskScoreDataBean.TT_CHARGEBACK_VOLUME,
                              resultSet.getDouble("ratio"),
                              resultSet.getInt("cb_count") );

          if ( points != 0 )
          {
            desc.setLength(0);
            desc.append("CB: ");
            desc.append( MesMath.toCurrency( resultSet.getDouble("cb_amount") ) );
            desc.append(" Sales: ");
            desc.append( MesMath.toCurrency( resultSet.getDouble("sales_amount") ) );
            desc.append(" Ratio: ");
            desc.append( NumberFormatter.getPercentString( resultSet.getDouble("ratio") * 0.01 ) );

            /*@lineinfo:generated-code*//*@lineinfo:1994^13*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//                (
//                  merchant_number,
//                  activity_date,
//                  risk_cat_id,
//                  points,
//                  score_desc,
//                  load_filename,
//                  load_file_id
//                )
//                values
//                (
//                  :resultSet.getLong("merchant_number"),
//                  :activityDate,
//                  :RiskScoreDataBean.TT_CHARGEBACK_VOLUME,
//                  :points,
//                  substr( :desc.toString(),1,64 ),
//                  :loadFilename,
//                  :loadFileId
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4389 = resultSet.getLong("merchant_number");
 String __sJT_4390 = desc.toString();
   String theSqlTS = "insert into risk_score\n              (\n                merchant_number,\n                activity_date,\n                risk_cat_id,\n                points,\n                score_desc,\n                load_filename,\n                load_file_id\n              )\n              values\n              (\n                 :1  ,\n                 :2  ,\n                 :3  ,\n                 :4  ,\n                substr(  :5  ,1,64 ),\n                 :6  ,\n                 :7  \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"60com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4389);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_CHARGEBACK_VOLUME);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4390);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2016^13*/
          }
        }
        resultSet.close();
        it.close();

        // load the high risk chargebacks from the current file
        /*@lineinfo:generated-code*//*@lineinfo:2023^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.merchant_number    as merchant_number,
//                    1                     as cb_count
//            from    network_chargebacks cb
//            where   cb.load_filename = :loadFilename and
//                    cb.reason_code in
//                      ( select  rd.reason_code
//                        from    chargeback_reason_desc rd
//                        where   nvl(rd.high_risk,'N') = 'Y'
//                                and card_type = cb.card_type
//                      )
//            order by cb.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.merchant_number    as merchant_number,\n                  1                     as cb_count\n          from    network_chargebacks cb\n          where   cb.load_filename =  :1   and\n                  cb.reason_code in\n                    ( select  rd.reason_code\n                      from    chargeback_reason_desc rd\n                      where   nvl(rd.high_risk,'N') = 'Y'\n                              and card_type = cb.card_type\n                    )\n          order by cb.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"61com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"61com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2036^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          points = getPoints( resultSet.getLong("merchant_number"),
                              RiskScoreDataBean.TT_HIGH_RISK_CHARGEBACK,
                              resultSet.getDouble("cb_count") );

          if ( points != 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:2047^13*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//                (
//                  merchant_number,
//                  activity_date,
//                  risk_cat_id,
//                  points,
//                  load_filename,
//                  load_file_id
//                )
//                values
//                (
//                  :resultSet.getLong("merchant_number"),
//                  :activityDate,
//                  :RiskScoreDataBean.TT_HIGH_RISK_CHARGEBACK,
//                  :points,
//                  :loadFilename,
//                  :loadFileId
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4391 = resultSet.getLong("merchant_number");
   String theSqlTS = "insert into risk_score\n              (\n                merchant_number,\n                activity_date,\n                risk_cat_id,\n                points,\n                load_filename,\n                load_file_id\n              )\n              values\n              (\n                 :1  ,\n                 :2  ,\n                 :3  ,\n                 :4  ,\n                 :5  ,\n                 :6  \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"62com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4391);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_HIGH_RISK_CHARGEBACK);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,loadFilename);
   __sJT_st.setLong(6,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2067^13*/
          }
        }
        resultSet.close();
        it.close();

        // load the large ticket chargebacks from the current file
        /*@lineinfo:generated-code*//*@lineinfo:2074^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.merchant_number    as merchant_number,
//                    cb.incoming_date      as incoming_date,
//                    cb.card_number        as card_number,
//                    cb.tran_amount        as tran_amount
//            from    network_chargebacks cb
//            where   cb.load_filename = :loadFilename and
//                    cb.tran_amount >= 500
//            order by tran_amount desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.merchant_number    as merchant_number,\n                  cb.incoming_date      as incoming_date,\n                  cb.card_number        as card_number,\n                  cb.tran_amount        as tran_amount\n          from    network_chargebacks cb\n          where   cb.load_filename =  :1   and\n                  cb.tran_amount >= 500\n          order by tran_amount desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"63com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"63com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2084^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          points = getPoints( resultSet.getLong("merchant_number"),
                              RiskScoreDataBean.TT_LARGE_TICKET_CHARGEBACK,
                              1,
                              resultSet.getInt("tran_amount") );

          if ( points == 0 )
          {
            // process the entire result
            // set in case there is a hierarchy
            // error or overloaded scoring
            continue;
          }

          desc.setLength(0);
          desc.append( resultSet.getString("card_number") );
          desc.append( " " );
          desc.append( MesMath.toCurrency( resultSet.getDouble("tran_amount") ) );

          if ( points != 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:2109^13*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//                (
//                  merchant_number,
//                  activity_date,
//                  risk_cat_id,
//                  points,
//                  score_desc,
//                  load_filename,
//                  load_file_id
//                )
//                values
//                (
//                  :resultSet.getLong("merchant_number"),
//                  :resultSet.getDate("incoming_date"),
//                  :RiskScoreDataBean.TT_LARGE_TICKET_CHARGEBACK,
//                  :points,
//                  substr( :desc.toString(),1,64 ),
//                  :loadFilename,
//                  :loadFileId
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4392 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4393 = resultSet.getDate("incoming_date");
 String __sJT_4394 = desc.toString();
   String theSqlTS = "insert into risk_score\n              (\n                merchant_number,\n                activity_date,\n                risk_cat_id,\n                points,\n                score_desc,\n                load_filename,\n                load_file_id\n              )\n              values\n              (\n                 :1  ,\n                 :2  ,\n                 :3  ,\n                 :4  ,\n                substr(  :5  ,1,64 ),\n                 :6  ,\n                 :7  \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"64com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4392);
   __sJT_st.setDate(2,__sJT_4393);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_LARGE_TICKET_CHARGEBACK);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4394);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2131^13*/
          }
        }
        resultSet.close();
        it.close();


        // ********* RETRIEVALS ************

        // load the ratios for all the merchants
        // that have retrievals over the last 7 days
        /*@lineinfo:generated-code*//*@lineinfo:2142^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  rt.merchant_number          as merchant_number,
//                    sum(rt.tran_amount)         as retr_amount,
//                    count(rt.tran_amount)       as retr_count,
//                    nvl(gn.t1_tot_amount_of_sales,0)  as sales_amount,
//                    round( ( sum(rt.tran_amount)/
//                             decode( gn.t1_tot_amount_of_sales,
//                                      null, 1,
//                                      0, 1,
//                                      gn.t1_tot_amount_of_sales )
//                            ) * 100, 5 )        as ratio
//            from    network_retrievals          rt,
//                    monthly_extract_gn          gn
//            where   rt.merchant_number in
//                      (select distinct merchant_number from network_retrievals where load_filename = :loadFilename) and
//                    rt.incoming_date between (:activityDate-7) and :activityDate and
//                    gn.hh_merchant_number(+) = rt.merchant_number and
//                    gn.hh_active_date(+) = trunc( (trunc(:activityDate,'month')-1), 'month' )
//            group by rt.merchant_number, gn.t1_tot_amount_of_sales
//            order by rt.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rt.merchant_number          as merchant_number,\n                  sum(rt.tran_amount)         as retr_amount,\n                  count(rt.tran_amount)       as retr_count,\n                  nvl(gn.t1_tot_amount_of_sales,0)  as sales_amount,\n                  round( ( sum(rt.tran_amount)/\n                           decode( gn.t1_tot_amount_of_sales,\n                                    null, 1,\n                                    0, 1,\n                                    gn.t1_tot_amount_of_sales )\n                          ) * 100, 5 )        as ratio\n          from    network_retrievals          rt,\n                  monthly_extract_gn          gn\n          where   rt.merchant_number in\n                    (select distinct merchant_number from network_retrievals where load_filename =  :1  ) and\n                  rt.incoming_date between ( :2  -7) and  :3   and\n                  gn.hh_merchant_number(+) = rt.merchant_number and\n                  gn.hh_active_date(+) = trunc( (trunc( :4  ,'month')-1), 'month' )\n          group by rt.merchant_number, gn.t1_tot_amount_of_sales\n          order by rt.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"65com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setDate(3,activityDate);
   __sJT_st.setDate(4,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"65com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2163^9*/
        resultSet = it.getResultSet();

        while( resultSet.next() )
        {
          points = getPoints( resultSet.getLong("merchant_number"),
                              RiskScoreDataBean.TT_RETRIEVAL_VOLUME,
                              resultSet.getDouble("ratio"),
                              resultSet.getInt("retr_count") );

          if ( points != 0 )
          {
            desc.setLength(0);
            desc.append("Retr: ");
            desc.append( MesMath.toCurrency( resultSet.getDouble("retr_amount") ) );
            desc.append(" Sales: ");
            desc.append( MesMath.toCurrency( resultSet.getDouble("sales_amount") ) );
            desc.append(" Ratio: ");
            desc.append( NumberFormatter.getPercentString( resultSet.getDouble("ratio") * 0.01 ) );

            /*@lineinfo:generated-code*//*@lineinfo:2183^13*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//                (
//                  merchant_number,
//                  activity_date,
//                  risk_cat_id,
//                  points,
//                  score_desc,
//                  load_filename,
//                  load_file_id
//                )
//                values
//                (
//                  :resultSet.getLong("merchant_number"),
//                  :activityDate,
//                  :RiskScoreDataBean.TT_RETRIEVAL_VOLUME,
//                  :points,
//                  substr( :desc.toString(),1,64 ),
//                  :loadFilename,
//                  :loadFileId
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4395 = resultSet.getLong("merchant_number");
 String __sJT_4396 = desc.toString();
   String theSqlTS = "insert into risk_score\n              (\n                merchant_number,\n                activity_date,\n                risk_cat_id,\n                points,\n                score_desc,\n                load_filename,\n                load_file_id\n              )\n              values\n              (\n                 :1  ,\n                 :2  ,\n                 :3  ,\n                 :4  ,\n                substr(  :5  ,1,64 ),\n                 :6  ,\n                 :7  \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"66com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4395);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_RETRIEVAL_VOLUME);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4396);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2205^13*/
          }
        }
        resultSet.close();
        it.close();

        /*@lineinfo:generated-code*//*@lineinfo:2211^9*/

//  ************************************************************
//  #sql [Ctx] { commit
//           };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2214^9*/
      }     // end if ( activityDate != null )

      fileProcessed = true;
    }
    catch( Exception e )
    {
      logEntry("loadChargebackRetrievalPoints( " + loadFilename + " )", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return( fileProcessed );
  }

  protected void linkCreditsToChargebacks( String loadFilename )
  {
    Date                  batchDate       = null;
    String                cardNum         = null;
    ResultSetIterator     it              = null;
    long                  loadSec         = 0L;
    long                  merchantId      = 0L;
    ResultSet             resultSet       = null;
    ResultSetIterator     tranIt          = null;
    ResultSet             tranResultSet   = null;

    try
    {
      if ( loadFilename == null )
      {
        /*@lineinfo:generated-code*//*@lineinfo:2245^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.cb_load_sec                          as load_sec,
//                    cb.card_number                          as card_number,
//                    nvl(cb.merchant_number,0)               as merchant_number,
//                    case
//                      when cb.card_type in ('VS','MC') then nvl(yddd_to_date(substr(cb.reference_number,8,4)),cb.tran_date)
//                      else cb.tran_date
//                    end                                     as batch_date
//            from    network_chargebacks     cb
//            where   cb.incoming_date >= (sysdate-120)
//                    and cb.credit_batch_date is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.cb_load_sec                          as load_sec,\n                  cb.card_number                          as card_number,\n                  nvl(cb.merchant_number,0)               as merchant_number,\n                  case\n                    when cb.card_type in ('VS','MC') then nvl(yddd_to_date(substr(cb.reference_number,8,4)),cb.tran_date)\n                    else cb.tran_date\n                  end                                     as batch_date\n          from    network_chargebacks     cb\n          where   cb.incoming_date >= (sysdate-120)\n                  and cb.credit_batch_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"67com.mes.startup.RiskScoringLoader",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"67com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2257^9*/
      }
      else    // only pull items from the requested file
      {
        /*@lineinfo:generated-code*//*@lineinfo:2261^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.cb_load_sec                          as load_sec,
//                    cb.card_number                          as card_number,
//                    nvl(cb.merchant_number,0)               as merchant_number,
//                    case
//                      when cb.card_type in ('VS','MC') then nvl(yddd_to_date(substr(cb.reference_number,8,4)),cb.tran_date)
//                      else cb.tran_date
//                    end                                     as batch_date
//            from    network_chargebacks     cb
//            where   cb.load_filename = :loadFilename
//                    and cb.credit_batch_date is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.cb_load_sec                          as load_sec,\n                  cb.card_number                          as card_number,\n                  nvl(cb.merchant_number,0)               as merchant_number,\n                  case\n                    when cb.card_type in ('VS','MC') then nvl(yddd_to_date(substr(cb.reference_number,8,4)),cb.tran_date)\n                    else cb.tran_date\n                  end                                     as batch_date\n          from    network_chargebacks     cb\n          where   cb.load_filename =  :1  \n                  and cb.credit_batch_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"68com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"68com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2273^9*/
      }
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        cardNum     = resultSet.getString("card_number");
        batchDate   = resultSet.getDate("batch_date");
        loadSec     = resultSet.getLong("load_sec");
        merchantId  = resultSet.getLong("merchant_number");

        /*@lineinfo:generated-code*//*@lineinfo:2284^9*/

//  ************************************************************
//  #sql [Ctx] tranIt = { select cr.batch_date        as batch_date,
//                   cr.batch_number      as batch_number,
//                   cr.ddf_dt_id         as ddf_dt_id
//            from   daily_detail_file_credits    cr
//            where  cr.merchant_number = :merchantId and
//                   cr.batch_date >= :batchDate and
//                   cr.card_number = :cardNum and
//                   not exists
//                   (  select  cb.cb_load_sec
//                      from    network_chargebacks   cb
//                      where   cb.credit_batch_date = cr.batch_date and
//                              cb.credit_batch_number = cr.batch_number and
//                              cb.credit_ddf_dt_id = cr.ddf_dt_id
//                   )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select cr.batch_date        as batch_date,\n                 cr.batch_number      as batch_number,\n                 cr.ddf_dt_id         as ddf_dt_id\n          from   daily_detail_file_credits    cr\n          where  cr.merchant_number =  :1   and\n                 cr.batch_date >=  :2   and\n                 cr.card_number =  :3   and\n                 not exists\n                 (  select  cb.cb_load_sec\n                    from    network_chargebacks   cb\n                    where   cb.credit_batch_date = cr.batch_date and\n                            cb.credit_batch_number = cr.batch_number and\n                            cb.credit_ddf_dt_id = cr.ddf_dt_id\n                 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"69com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,batchDate);
   __sJT_st.setString(3,cardNum);
   // execute query
   tranIt = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"69com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2300^9*/
        tranResultSet = tranIt.getResultSet();

        if ( tranResultSet.next() )
        {
          /*@lineinfo:generated-code*//*@lineinfo:2305^11*/

//  ************************************************************
//  #sql [Ctx] { update network_chargebacks
//              set credit_batch_date   = :tranResultSet.getDate("batch_date"),
//                  credit_batch_number = :tranResultSet.getLong("batch_number"),
//                  credit_ddf_dt_id    = :tranResultSet.getLong("ddf_dt_id")
//              where cb_load_sec = :loadSec
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_4397 = tranResultSet.getDate("batch_date");
 long __sJT_4398 = tranResultSet.getLong("batch_number");
 long __sJT_4399 = tranResultSet.getLong("ddf_dt_id");
   String theSqlTS = "update network_chargebacks\n            set credit_batch_date   =  :1  ,\n                credit_batch_number =  :2  ,\n                credit_ddf_dt_id    =  :3  \n            where cb_load_sec =  :4 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"70com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_4397);
   __sJT_st.setLong(2,__sJT_4398);
   __sJT_st.setLong(3,__sJT_4399);
   __sJT_st.setLong(4,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2312^11*/

          // pend chargeback will handle removing
          // any pending (unprocessed) financial
          // entries and move the chargeback back
          // to a pending queue.
          ChargebackTools.pendChargeback(Ctx,loadSec);
        }
        tranResultSet.close();
        tranIt.close();
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      // ignore?
      logEntry("linkCreditsToChargebacks( " + loadFilename + " )", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e){}
      try { tranIt.close(); } catch(Exception e){}
    }
  }

  protected void linkCreditsToRetrievals( String loadFilename )
  {
    Date                  batchDate       = null;
    String                cardNum         = null;
    ResultSetIterator     it              = null;
    long                  loadSec         = 0L;
    long                  merchantId      = 0L;
    ResultSet             resultSet       = null;
    ResultSetIterator     tranIt          = null;
    ResultSet             tranResultSet   = null;

    try
    {
      if ( loadFilename == null )
      {
        /*@lineinfo:generated-code*//*@lineinfo:2353^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  rt.retr_load_sec                        as load_sec,
//                    rt.card_number                          as card_number,
//                    nvl(rt.merchant_number,0)               as merchant_number,
//                    case
//                      when rt.card_type in ('VS','MC') then nvl(yddd_to_date(substr(rt.reference_number,8,4)),rt.tran_date)
//                      else rt.tran_date
//                    end                                     as batch_date
//            from    network_retrievals      rt
//            where   rt.incoming_date >= (sysdate-45)
//                    and rt.credit_batch_date is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rt.retr_load_sec                        as load_sec,\n                  rt.card_number                          as card_number,\n                  nvl(rt.merchant_number,0)               as merchant_number,\n                  case\n                    when rt.card_type in ('VS','MC') then nvl(yddd_to_date(substr(rt.reference_number,8,4)),rt.tran_date)\n                    else rt.tran_date\n                  end                                     as batch_date\n          from    network_retrievals      rt\n          where   rt.incoming_date >= (sysdate-45)\n                  and rt.credit_batch_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"71com.mes.startup.RiskScoringLoader",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"71com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2365^9*/
      }
      else  // only link items in the current file
      {
        /*@lineinfo:generated-code*//*@lineinfo:2369^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  rt.retr_load_sec                        as load_sec,
//                    rt.card_number                          as card_number,
//                    nvl(rt.merchant_number,0)               as merchant_number,
//                    case
//                      when rt.card_type in ('VS','MC') then nvl(yddd_to_date(substr(rt.reference_number,8,4)),rt.tran_date)
//                      else rt.tran_date
//                    end                                     as batch_date
//            from    network_retrievals      rt
//            where   rt.load_filename = :loadFilename
//                    and rt.credit_batch_date is null
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rt.retr_load_sec                        as load_sec,\n                  rt.card_number                          as card_number,\n                  nvl(rt.merchant_number,0)               as merchant_number,\n                  case\n                    when rt.card_type in ('VS','MC') then nvl(yddd_to_date(substr(rt.reference_number,8,4)),rt.tran_date)\n                    else rt.tran_date\n                  end                                     as batch_date\n          from    network_retrievals      rt\n          where   rt.load_filename =  :1  \n                  and rt.credit_batch_date is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"72com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"72com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2381^9*/
      }
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        cardNum     = resultSet.getString("card_number");
        batchDate   = resultSet.getDate("batch_date");
        loadSec     = resultSet.getLong("load_sec");
        merchantId  = resultSet.getLong("merchant_number");

        /*@lineinfo:generated-code*//*@lineinfo:2392^9*/

//  ************************************************************
//  #sql [Ctx] tranIt = { select cr.batch_date        as batch_date,
//                   cr.batch_number      as batch_number,
//                   cr.ddf_dt_id         as ddf_dt_id
//            from   daily_detail_file_credits    cr
//            where  cr.merchant_number = :merchantId and
//                   cr.batch_date >= :batchDate and
//                   cr.card_number = :cardNum and
//                   not exists
//                   (  select  rt.retr_load_sec
//                      from    network_retrievals    rt
//                      where   rt.credit_batch_date = cr.batch_date and
//                              rt.credit_batch_number = cr.batch_number and
//                              rt.credit_ddf_dt_id = cr.ddf_dt_id
//                   )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select cr.batch_date        as batch_date,\n                 cr.batch_number      as batch_number,\n                 cr.ddf_dt_id         as ddf_dt_id\n          from   daily_detail_file_credits    cr\n          where  cr.merchant_number =  :1   and\n                 cr.batch_date >=  :2   and\n                 cr.card_number =  :3   and\n                 not exists\n                 (  select  rt.retr_load_sec\n                    from    network_retrievals    rt\n                    where   rt.credit_batch_date = cr.batch_date and\n                            rt.credit_batch_number = cr.batch_number and\n                            rt.credit_ddf_dt_id = cr.ddf_dt_id\n                 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"73com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,batchDate);
   __sJT_st.setString(3,cardNum);
   // execute query
   tranIt = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"73com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2408^9*/
        tranResultSet = tranIt.getResultSet();

        if ( tranResultSet.next() )
        {
          /*@lineinfo:generated-code*//*@lineinfo:2413^11*/

//  ************************************************************
//  #sql [Ctx] { update network_retrievals
//              set credit_batch_date   = :tranResultSet.getDate("batch_date"),
//                  credit_batch_number = :tranResultSet.getLong("batch_number"),
//                  credit_ddf_dt_id    = :tranResultSet.getLong("ddf_dt_id")
//              where retr_load_sec = :loadSec
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 java.sql.Date __sJT_4400 = tranResultSet.getDate("batch_date");
 long __sJT_4401 = tranResultSet.getLong("batch_number");
 long __sJT_4402 = tranResultSet.getLong("ddf_dt_id");
   String theSqlTS = "update network_retrievals\n            set credit_batch_date   =  :1  ,\n                credit_batch_number =  :2  ,\n                credit_ddf_dt_id    =  :3  \n            where retr_load_sec =  :4 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"74com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,__sJT_4400);
   __sJT_st.setLong(2,__sJT_4401);
   __sJT_st.setLong(3,__sJT_4402);
   __sJT_st.setLong(4,loadSec);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2420^11*/
        }
        tranResultSet.close();
        tranIt.close();
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      // ignore?
      logEntry("linkCreditsToRetrievals( " + loadFilename + " )", e.toString());
    }
    finally
    {
      try { it.close(); } catch(Exception e){}
      try { tranIt.close(); } catch(Exception e){}
    }
  }

  private void loadDebug( String loadFilename )
  {
    try
    {
      linkCreditsToChargebacks(loadFilename);
      linkCreditsToRetrievals(loadFilename);
//@      linkChargebacksToTransactions(loadFilename);
//@      loadChargebackRetrievalPoints(loadFilename);
//@      linkRetrievalsToTransactions(loadFilename);
//@      notifyMerchants(loadFilename);
    }
    catch( Exception e )
    {
      logEntry("loadDebug( " + loadFilename + " )",e.toString());
    }
    finally
    {
    }
  }

  private void loadDuplicateCardSummary( String loadFilename )
  {
    ResultSetIterator   it              = null;
    long                loadFileId      = 0L;
    ResultSet           resultSet       = null;

    try
    {
      // load the dupliate card summary table
      loadFileId = loadFilenameToLoadFileId( loadFilename );

      /*@lineinfo:generated-code*//*@lineinfo:2471^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    risk_duplicate_card_summary   sm
//          where   sm.load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    risk_duplicate_card_summary   sm\n        where   sm.load_file_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"75com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2476^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2478^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    dt.merchant_account_number            as merchant_number,
//                    dt.batch_date                         as batch_date,
//                    replace(dt.cardholder_account_number,'x','0')   as card_number,
//                    dt.cardholder_account_number          as card_number_string,
//                    sum( decode( dt.debit_credit_indicator,
//                                 'D', 1, 0 ) )            as sales_count,
//                    sum( decode( dt.debit_credit_indicator,
//                                 'D', dt.transaction_amount,
//                                 0 ) )                   as sales_amount,
//                    sum( decode( dt.debit_credit_indicator,
//                                 'C', 1, 0 ) )            as credits_count,
//                    sum( decode( dt.debit_credit_indicator,
//                                 'C', dt.transaction_amount,
//                                 0 ) )                   as credits_amount
//          from      daily_detail_file_dt    dt
//          where     dt.load_filename = :loadFilename and
//                    -- prevent invalid card numbers from being processed
//                    is_number(replace(dt.cardholder_account_number,'x','0')) = 1
//          group by dt.merchant_account_number,
//                   dt.batch_date,
//                   dt.cardholder_account_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    dt.merchant_account_number            as merchant_number,\n                  dt.batch_date                         as batch_date,\n                  replace(dt.cardholder_account_number,'x','0')   as card_number,\n                  dt.cardholder_account_number          as card_number_string,\n                  sum( decode( dt.debit_credit_indicator,\n                               'D', 1, 0 ) )            as sales_count,\n                  sum( decode( dt.debit_credit_indicator,\n                               'D', dt.transaction_amount,\n                               0 ) )                   as sales_amount,\n                  sum( decode( dt.debit_credit_indicator,\n                               'C', 1, 0 ) )            as credits_count,\n                  sum( decode( dt.debit_credit_indicator,\n                               'C', dt.transaction_amount,\n                               0 ) )                   as credits_amount\n        from      daily_detail_file_dt    dt\n        where     dt.load_filename =  :1   and\n                  -- prevent invalid card numbers from being processed\n                  is_number(replace(dt.cardholder_account_number,'x','0')) = 1\n        group by dt.merchant_account_number,\n                 dt.batch_date,\n                 dt.cardholder_account_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"76com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"76com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2501^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {

        /*@lineinfo:generated-code*//*@lineinfo:2507^9*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_duplicate_card_summary
//            (
//              merchant_number,
//              batch_date,
//              card_number,
//              card_number_str,
//              sales_count,
//              sales_amount,
//              credits_count,
//              credits_amount,
//              load_filename,
//              load_file_id
//            )
//            values
//            (
//              :resultSet.getLong("merchant_number"),
//              :resultSet.getDate("batch_date"),
//              :resultSet.getLong("card_number"),
//              :resultSet.getString("card_number_string"),
//              :resultSet.getDouble("sales_count"),
//              :resultSet.getDouble("sales_amount"),
//              :resultSet.getDouble("credits_count"),
//              :resultSet.getDouble("credits_amount"),
//              :loadFilename,
//              :loadFileId
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4403 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4404 = resultSet.getDate("batch_date");
 long __sJT_4405 = resultSet.getLong("card_number");
 String __sJT_4406 = resultSet.getString("card_number_string");
 double __sJT_4407 = resultSet.getDouble("sales_count");
 double __sJT_4408 = resultSet.getDouble("sales_amount");
 double __sJT_4409 = resultSet.getDouble("credits_count");
 double __sJT_4410 = resultSet.getDouble("credits_amount");
   String theSqlTS = "insert into risk_duplicate_card_summary\n          (\n            merchant_number,\n            batch_date,\n            card_number,\n            card_number_str,\n            sales_count,\n            sales_amount,\n            credits_count,\n            credits_amount,\n            load_filename,\n            load_file_id\n          )\n          values\n          (\n             :1  ,\n             :2  ,\n             :3  ,\n             :4  ,\n             :5  ,\n             :6  ,\n             :7  ,\n             :8  ,\n             :9  ,\n             :10  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"77com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4403);
   __sJT_st.setDate(2,__sJT_4404);
   __sJT_st.setLong(3,__sJT_4405);
   __sJT_st.setString(4,__sJT_4406);
   __sJT_st.setDouble(5,__sJT_4407);
   __sJT_st.setDouble(6,__sJT_4408);
   __sJT_st.setDouble(7,__sJT_4409);
   __sJT_st.setDouble(8,__sJT_4410);
   __sJT_st.setString(9,loadFilename);
   __sJT_st.setLong(10,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2535^9*/
      }
      resultSet.close();
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadDuplicateCardSummary( " + loadFilename + " )",e.toString());
    }
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
  }

  private boolean loadPointsAchRejects( String loadFilename )
  {
    StringBuffer        desc            = new StringBuffer();
    boolean             fileProcessed   = false;
    ResultSetIterator   it              = null;
    long                loadFileId      = 0L;
    int                 points          = 0;
    ResultSet           resultSet       = null;


    try
    {
      loadFileId = loadFilenameToLoadFileId(loadFilename);

      /*@lineinfo:generated-code*//*@lineinfo:2564^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    risk_score    rs
//          where   rs.load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    risk_score    rs\n        where   rs.load_file_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"78com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2569^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2571^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  ar.merchant_number          as merchant_number,
//                  ar.settled_date             as reject_date,
//                  count(ar.merchant_number)   as reject_count,
//                  sum(ar.amount)              as reject_amount
//          from    ach_rejects           ar
//          where   ar.load_filename = :loadFilename and
//                  substr(ar.reason_code,1,1) = 'R'
//          group by ar.merchant_number, ar.settled_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  ar.merchant_number          as merchant_number,\n                ar.settled_date             as reject_date,\n                count(ar.merchant_number)   as reject_count,\n                sum(ar.amount)              as reject_amount\n        from    ach_rejects           ar\n        where   ar.load_filename =  :1   and\n                substr(ar.reason_code,1,1) = 'R'\n        group by ar.merchant_number, ar.settled_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"79com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"79com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2581^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        points = getPoints( resultSet.getLong("merchant_number"),
                            RiskScoreDataBean.TT_ACH_REJECT,
                            resultSet.getDouble("reject_count") );

        if ( points != 0 )
        {
          desc.setLength(0);
          desc.append( "ACH Reject: ");
          desc.append( resultSet.getInt("reject_count") );
          desc.append( " for " );
          desc.append( MesMath.toCurrency( resultSet.getDouble("reject_amount") ) );

          /*@lineinfo:generated-code*//*@lineinfo:2598^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//              (
//                merchant_number,
//                activity_date,
//                risk_cat_id,
//                points,
//                score_desc,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :resultSet.getLong("merchant_number"),
//                :resultSet.getDate("reject_date"),
//                :RiskScoreDataBean.TT_ACH_REJECT,
//                :points,
//                substr(:desc.toString(),1,64),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4411 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4412 = resultSet.getDate("reject_date");
 String __sJT_4413 = desc.toString();
   String theSqlTS = "insert into risk_score\n            (\n              merchant_number,\n              activity_date,\n              risk_cat_id,\n              points,\n              score_desc,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n              substr( :5  ,1,64),\n               :6  ,\n               :7  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"80com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4411);
   __sJT_st.setDate(2,__sJT_4412);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_ACH_REJECT);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4413);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2620^11*/
        }
      }
      resultSet.close();
      it.close();

      fileProcessed = true;
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadPointsAchReject()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception ee){}
    }
    return( fileProcessed );
  }

  private boolean loadPointsAuth( String loadFilename )
  {
    StringBuffer                desc            = new StringBuffer();
    boolean                     fileProcessed   = false;
    ResultSetIterator           it              = null;
    long                        loadFileId      = 0L;
    int                         points          = 0;
    ResultSet                   resultSet       = null;
    double                      testValue       = 0;

    try
    {
      loadFileId = loadFilenameToLoadFileId(loadFilename);

      // remove any previous entries in the scoring system
      /*@lineinfo:generated-code*//*@lineinfo:2654^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    risk_score
//          where   load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    risk_score\n        where   load_file_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"81com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2659^7*/

      // collect the data
      /*@lineinfo:generated-code*//*@lineinfo:2662^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                          as merchant_number,
//                  sm.transaction_date                         as tran_date,
//                  sm.vmc_auth_decline_count                   as declined_count,
//                  sm.vmc_auth_decline_amount                  as declined_amount,
//                  sm.vmc_auth_avs_count                       as avs_count,
//                  sm.vmc_auth_avs_amount                      as avs_amount,
//                  sm.vmc_auth_total_count                     as auth_count,
//                  sm.vmc_auth_total_amount                    as auth_amount,
//                  decode( sm.vmc_auth_total_count,
//                          0, 0,
//                          round( ( sm.vmc_auth_decline_count /
//                                   sm.vmc_auth_total_count ), 5 ) * 100 )
//                                                              as decl_auth_ratio,
//                  sm.vmc_bad_auth_count                       as bad_auth_count,
//                  sm.vmc_bad_avs_count                        as bad_avs_count,
//                  sm.vmc_small_auth_count                     as small_auth_count,
//                  sm.vmc_small_threshold_amount               as threshold
//          from    tc33_risk_summary     sm
//          where   sm.load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                          as merchant_number,\n                sm.transaction_date                         as tran_date,\n                sm.vmc_auth_decline_count                   as declined_count,\n                sm.vmc_auth_decline_amount                  as declined_amount,\n                sm.vmc_auth_avs_count                       as avs_count,\n                sm.vmc_auth_avs_amount                      as avs_amount,\n                sm.vmc_auth_total_count                     as auth_count,\n                sm.vmc_auth_total_amount                    as auth_amount,\n                decode( sm.vmc_auth_total_count,\n                        0, 0,\n                        round( ( sm.vmc_auth_decline_count /\n                                 sm.vmc_auth_total_count ), 5 ) * 100 )\n                                                            as decl_auth_ratio,\n                sm.vmc_bad_auth_count                       as bad_auth_count,\n                sm.vmc_bad_avs_count                        as bad_avs_count,\n                sm.vmc_small_auth_count                     as small_auth_count,\n                sm.vmc_small_threshold_amount               as threshold\n        from    tc33_risk_summary     sm\n        where   sm.load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"82com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"82com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2683^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        testValue = resultSet.getDouble("decl_auth_ratio");
        if( testValue != 0 )
        {
          points = getPoints( resultSet.getLong("merchant_number"),
                              RiskScoreDataBean.TT_DECL_AUTH_RATIO,
                              testValue,
                              resultSet.getInt("auth_count") );

          if ( points != 0 )
          {
            desc.setLength(0);
            desc.append("Decl: ");
            desc.append( resultSet.getInt("declined_count") );
            desc.append(" Auth: ");
            desc.append( resultSet.getInt("auth_count") );
            desc.append(" Ratio: ");
            desc.append( NumberFormatter.getPercentString( testValue * 0.01 ) );

            /*@lineinfo:generated-code*//*@lineinfo:2706^13*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//                (
//                  merchant_number,
//                  activity_date,
//                  risk_cat_id,
//                  points,
//                  score_desc,
//                  load_filename,
//                  load_file_id
//                )
//                values
//                (
//                  :resultSet.getLong("merchant_number"),
//                  :resultSet.getDate("tran_date"),
//                  :RiskScoreDataBean.TT_DECL_AUTH_RATIO,
//                  :points,
//                  substr( :desc.toString(),1,64 ),
//                  :loadFilename,
//                  :loadFileId
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4414 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4415 = resultSet.getDate("tran_date");
 String __sJT_4416 = desc.toString();
   String theSqlTS = "insert into risk_score\n              (\n                merchant_number,\n                activity_date,\n                risk_cat_id,\n                points,\n                score_desc,\n                load_filename,\n                load_file_id\n              )\n              values\n              (\n                 :1  ,\n                 :2  ,\n                 :3  ,\n                 :4  ,\n                substr(  :5  ,1,64 ),\n                 :6  ,\n                 :7  \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"83com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4414);
   __sJT_st.setDate(2,__sJT_4415);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_DECL_AUTH_RATIO);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4416);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2728^13*/
          }
        }

        testValue = resultSet.getDouble("bad_auth_count");
        if( testValue != 0 )
        {
          points = getPoints( resultSet.getLong("merchant_number"),
                              RiskScoreDataBean.TT_DECL_REASON_CODES,
                              testValue );

          if ( points != 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:2741^13*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//                (
//                  merchant_number,
//                  activity_date,
//                  risk_cat_id,
//                  points,
//                  load_filename,
//                  load_file_id
//                )
//                values
//                (
//                  :resultSet.getLong("merchant_number"),
//                  :resultSet.getDate("tran_date"),
//                  :RiskScoreDataBean.TT_DECL_REASON_CODES,
//                  :points,
//                  :loadFilename,
//                  :loadFileId
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4417 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4418 = resultSet.getDate("tran_date");
   String theSqlTS = "insert into risk_score\n              (\n                merchant_number,\n                activity_date,\n                risk_cat_id,\n                points,\n                load_filename,\n                load_file_id\n              )\n              values\n              (\n                 :1  ,\n                 :2  ,\n                 :3  ,\n                 :4  ,\n                 :5  ,\n                 :6  \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"84com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4417);
   __sJT_st.setDate(2,__sJT_4418);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_DECL_REASON_CODES);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,loadFilename);
   __sJT_st.setLong(6,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2761^13*/
          }
        }

        testValue = resultSet.getDouble("small_auth_count");
        if( testValue != 0 )
        {
          points = getPoints( resultSet.getLong("merchant_number"),
                              RiskScoreDataBean.TT_SMALL_TICKET_AUTH,
                              testValue );

          if ( points != 0 )
          {
            desc.setLength(0);
            desc.append( resultSet.getInt("small_auth_count") );
            desc.append(" auths <= ");
            desc.append( MesMath.toCurrency(resultSet.getDouble("threshold")) );

            /*@lineinfo:generated-code*//*@lineinfo:2779^13*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//                (
//                  merchant_number,
//                  activity_date,
//                  risk_cat_id,
//                  points,
//                  score_desc,
//                  load_filename,
//                  load_file_id
//                )
//                values
//                (
//                  :resultSet.getLong("merchant_number"),
//                  :resultSet.getDate("tran_date"),
//                  :RiskScoreDataBean.TT_SMALL_TICKET_AUTH,
//                  :points,
//                  substr( :desc.toString(),1,64 ),
//                  :loadFilename,
//                  :loadFileId
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4419 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4420 = resultSet.getDate("tran_date");
 String __sJT_4421 = desc.toString();
   String theSqlTS = "insert into risk_score\n              (\n                merchant_number,\n                activity_date,\n                risk_cat_id,\n                points,\n                score_desc,\n                load_filename,\n                load_file_id\n              )\n              values\n              (\n                 :1  ,\n                 :2  ,\n                 :3  ,\n                 :4  ,\n                substr(  :5  ,1,64 ),\n                 :6  ,\n                 :7  \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"85com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4419);
   __sJT_st.setDate(2,__sJT_4420);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_SMALL_TICKET_AUTH);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4421);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2801^13*/
          }
        }

        testValue = resultSet.getDouble("bad_avs_count");
        if( testValue != 0 )
        {
          points = getPoints( resultSet.getLong("merchant_number"),
                              RiskScoreDataBean.TT_AUTH_AVS_RESULT,
                              testValue );

          if ( points != 0 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:2814^13*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//                (
//                  merchant_number,
//                  activity_date,
//                  risk_cat_id,
//                  points,
//                  load_filename,
//                  load_file_id
//                )
//                values
//                (
//                  :resultSet.getLong("merchant_number"),
//                  :resultSet.getDate("tran_date"),
//                  :RiskScoreDataBean.TT_AUTH_AVS_RESULT,
//                  :points,
//                  :loadFilename,
//                  :loadFileId
//                )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4422 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4423 = resultSet.getDate("tran_date");
   String theSqlTS = "insert into risk_score\n              (\n                merchant_number,\n                activity_date,\n                risk_cat_id,\n                points,\n                load_filename,\n                load_file_id\n              )\n              values\n              (\n                 :1  ,\n                 :2  ,\n                 :3  ,\n                 :4  ,\n                 :5  ,\n                 :6  \n              )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"86com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4422);
   __sJT_st.setDate(2,__sJT_4423);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_AUTH_AVS_RESULT);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,loadFilename);
   __sJT_st.setLong(6,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2834^13*/
          }
        }
      }
      resultSet.close();
      it.close();

      loadPointsSequentialAuths( loadFilename );

      /*@lineinfo:generated-code*//*@lineinfo:2843^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2846^7*/

      fileProcessed = true;
    }
    catch( Exception e )
    {
      logEntry("loadPointsAuth( " + loadFilename + " )", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return( fileProcessed );
  }

  private boolean loadPointsCapture( String loadFilename )
  {
    Date                        activityDate    = null;
    Date                        activityDateMin = null;
    int                         basis           = 0;
    int                         basisType       = 0;
    int                         delta           = 0;
    StringBuffer                desc            = new StringBuffer();
    boolean                     fileProcessed   = false;
    ResultSetIterator           it              = null;
    double                      itemAmount      = 0.0;
    int                         itemCount       = 0;
    long                        loadFileId      = 0L;
    double                      netDeposit      = 0.0;
    int                         points          = 0;
    int                         riskCat         = 0;
    ResultSet                   resultSet       = null;
    int                         volume          = 0;

    try
    {
      log.debug("Entering loadPointsCapture( " + loadFilename + " )");

      // make sure this load filename has an
      // entry in the index table
      loadFileId = loadFilenameToLoadFileId( loadFilename );

      /*@lineinfo:generated-code*//*@lineinfo:2888^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    risk_score
//          where   load_file_id = :loadFileId and
//                  not risk_cat_id in ( 31 )    -- foreign card exception
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    risk_score\n        where   load_file_id =  :1   and\n                not risk_cat_id in ( 31 )    -- foreign card exception";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"87com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2894^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2896^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    risk_avg_ticket_exceptions
//          where   load_file_id = :loadFileId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    risk_avg_ticket_exceptions\n        where   load_file_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"88com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2901^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2903^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2906^7*/

      /*@lineinfo:generated-code*//*@lineinfo:2908^7*/

//  ************************************************************
//  #sql [Ctx] { select  min(dt.batch_date),
//                  max(dt.batch_date) 
//          from    daily_detail_file_dt dt
//          where   dt.load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  min(dt.batch_date),\n                max(dt.batch_date)  \n        from    daily_detail_file_dt dt\n        where   dt.load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"89com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   activityDateMin = (java.sql.Date)__sJT_rs.getDate(1);
   activityDate = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2914^7*/

      // load fraud card numbers
      log.debug("loading fraud card numbers");
      /*@lineinfo:generated-code*//*@lineinfo:2918^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_account_number        as merchant_number,
//                  dt.batch_date                     as batch_date,
//                  dt.cardholder_account_number      as card_number,
//                  ( dt.transaction_amount *
//                    decode(dt.debit_credit_indicator,'C',-1,1) )
//                                                    as tran_amount
//          from    daily_detail_file_dt        dt,
//                  risk_fraud_card_numbers     fcn
//          where   dt.load_filename = :loadFilename and
//                  (fcn.card_number_begin || 'xxxxxx' || lpad(fcn.card_number_end,4,'0')) =
//                    dt.cardholder_account_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_account_number        as merchant_number,\n                dt.batch_date                     as batch_date,\n                dt.cardholder_account_number      as card_number,\n                ( dt.transaction_amount *\n                  decode(dt.debit_credit_indicator,'C',-1,1) )\n                                                  as tran_amount\n        from    daily_detail_file_dt        dt,\n                risk_fraud_card_numbers     fcn\n        where   dt.load_filename =  :1   and\n                (fcn.card_number_begin || 'xxxxxx' || lpad(fcn.card_number_end,4,'0')) =\n                  dt.cardholder_account_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"90com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"90com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2931^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        points = getPoints( resultSet.getLong("merchant_number"),
                            RiskScoreDataBean.TT_FRAUD_CARD_NUMBER,
                            1, 1 );

        if ( points != 0 )
        {
          desc.setLength(0);
          desc.append( "Fraud Card Number: " );
          desc.append( resultSet.getString("card_number") );
          desc.append( " for " );
          desc.append( MesMath.toCurrency( resultSet.getDouble("tran_amount") ) );

          /*@lineinfo:generated-code*//*@lineinfo:2948^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//              (
//                merchant_number,
//                activity_date,
//                risk_cat_id,
//                points,
//                score_desc,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :resultSet.getLong("merchant_number"),
//                :resultSet.getDate("batch_date"),
//                :RiskScoreDataBean.TT_FRAUD_CARD_NUMBER,
//                :points,
//                substr(:desc.toString(),1,64),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4424 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4425 = resultSet.getDate("batch_date");
 String __sJT_4426 = desc.toString();
   String theSqlTS = "insert into risk_score\n            (\n              merchant_number,\n              activity_date,\n              risk_cat_id,\n              points,\n              score_desc,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n              substr( :5  ,1,64),\n               :6  ,\n               :7  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"91com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4424);
   __sJT_st.setDate(2,__sJT_4425);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_FRAUD_CARD_NUMBER);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4426);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2970^11*/
        }
      }
      resultSet.close();
      it.close();

      // load the suspicious account exceptions
      log.debug("loading suspicious account exceptions");
      /*@lineinfo:generated-code*//*@lineinfo:2978^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  sm.merchant_number                 as merchant_number,
//                  sm.batch_date                      as batch_date,
//                  sum( sm.bank_sales_amount +
//                       sm.nonbank_sales_amount )     as daily_volume
//          from    daily_detail_file_summary     sm,
//                  mif                           mf
//          where   sm.load_file_id = :loadFileId
//                  and sm.batch_date between :activityDateMin and :activityDate
//                  and mf.merchant_number = sm.merchant_number
//                  and nvl(mf.dmacctst,' ') = 'S'      -- suspicious account
//          group by sm.merchant_number,sm.batch_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  sm.merchant_number                 as merchant_number,\n                sm.batch_date                      as batch_date,\n                sum( sm.bank_sales_amount +\n                     sm.nonbank_sales_amount )     as daily_volume\n        from    daily_detail_file_summary     sm,\n                mif                           mf\n        where   sm.load_file_id =  :1  \n                and sm.batch_date between  :2   and  :3  \n                and mf.merchant_number = sm.merchant_number\n                and nvl(mf.dmacctst,' ') = 'S'      -- suspicious account\n        group by sm.merchant_number,sm.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"92com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setDate(2,activityDateMin);
   __sJT_st.setDate(3,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"92com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:2991^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        points = getPoints( resultSet.getLong("merchant_number"),
                            RiskScoreDataBean.TT_SUSPICIOUS_ACCOUNT,
                            resultSet.getDouble("daily_volume"),
                            1 );

        if ( points != 0 )
        {
          desc.setLength(0);
          desc.append( "Suspicious Acct with Volume: " );
          desc.append( MesMath.toCurrency( resultSet.getDouble("daily_volume") ) );

          /*@lineinfo:generated-code*//*@lineinfo:3007^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//              (
//                merchant_number,
//                activity_date,
//                risk_cat_id,
//                points,
//                score_desc,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :resultSet.getLong("merchant_number"),
//                :resultSet.getDate("batch_date"),
//                :RiskScoreDataBean.TT_SUSPICIOUS_ACCOUNT,
//                :points,
//                substr(:desc.toString(),1,64),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4427 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4428 = resultSet.getDate("batch_date");
 String __sJT_4429 = desc.toString();
   String theSqlTS = "insert into risk_score\n            (\n              merchant_number,\n              activity_date,\n              risk_cat_id,\n              points,\n              score_desc,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n              substr( :5  ,1,64),\n               :6  ,\n               :7  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"93com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4427);
   __sJT_st.setDate(2,__sJT_4428);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_SUSPICIOUS_ACCOUNT);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4429);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3029^11*/
        }
      }
      resultSet.close();
      it.close();

      // load the daily volume exception for new merchants
      // using the data in the application
      log.debug("loading daily volume exceptions");
      /*@lineinfo:generated-code*//*@lineinfo:3038^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (smp pk_ddf_summary) */
//                  sm.merchant_number                            as merchant_number,
//                  sm.batch_date                                 as batch_date,
//                  (trunc(sysdate)-mf.risk_activation_date)      as days_active,
//                  round( sm.bank_sales_amount +
//                         sm.nonbank_sales_amount )              as daily_volume,
//                  round(nvl(mr.merch_month_tot_proj_sales,0)/21)as app_daily_volume,
//                  decode( max( smp.bank_sales_amount + smp.nonbank_sales_amount ),
//                          null, 3000,
//                          0, 3000,
//                          max( smp.bank_sales_amount + smp.nonbank_sales_amount )
//                        )                                       as act_daily_volume,
//                  decode ( nvl(mr.merch_month_tot_proj_sales,0),
//                           0, 0,
//                           round( (sm.bank_sales_amount + sm.nonbank_sales_amount) /
//                                  (mr.merch_month_tot_proj_sales/21),5) *
//                         100 )                                  as app_ratio,
//                  (
//                    round( (sm.bank_sales_amount + sm.nonbank_sales_amount)/
//                           decode( max( smp.bank_sales_amount + smp.nonbank_sales_amount ),
//                                   null, 3000,
//                                   0, 3000,
//                                   max( smp.bank_sales_amount + smp.nonbank_sales_amount )
//                                  ),
//                           5 ) * 100
//                  )                                             as act_ratio
//          from    daily_detail_file_summary     sm,
//                  mif                           mf,
//                  merchant                      mr,
//                  daily_detail_file_summary     smp
//          where   sm.load_file_id = :loadFileId and
//                  sm.batch_date between :activityDateMin and :activityDate and
//                  mf.merchant_number = sm.merchant_number and
//                  mr.merch_number(+) = sm.merchant_number and
//                  smp.merchant_number(+) = sm.merchant_number and
//                  smp.batch_date(+) between trunc( (trunc(:activityDate,'month')-1), 'month' ) and (trunc(:activityDate,'month')-1)
//          group by sm.merchant_number,
//                   sm.batch_date,
//                   mf.risk_activation_date,
//                   sm.bank_sales_amount, sm.nonbank_sales_amount,
//                   mr.merch_month_tot_proj_sales
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (smp pk_ddf_summary) */\n                sm.merchant_number                            as merchant_number,\n                sm.batch_date                                 as batch_date,\n                (trunc(sysdate)-mf.risk_activation_date)      as days_active,\n                round( sm.bank_sales_amount +\n                       sm.nonbank_sales_amount )              as daily_volume,\n                round(nvl(mr.merch_month_tot_proj_sales,0)/21)as app_daily_volume,\n                decode( max( smp.bank_sales_amount + smp.nonbank_sales_amount ),\n                        null, 3000,\n                        0, 3000,\n                        max( smp.bank_sales_amount + smp.nonbank_sales_amount )\n                      )                                       as act_daily_volume,\n                decode ( nvl(mr.merch_month_tot_proj_sales,0),\n                         0, 0,\n                         round( (sm.bank_sales_amount + sm.nonbank_sales_amount) /\n                                (mr.merch_month_tot_proj_sales/21),5) *\n                       100 )                                  as app_ratio,\n                (\n                  round( (sm.bank_sales_amount + sm.nonbank_sales_amount)/\n                         decode( max( smp.bank_sales_amount + smp.nonbank_sales_amount ),\n                                 null, 3000,\n                                 0, 3000,\n                                 max( smp.bank_sales_amount + smp.nonbank_sales_amount )\n                                ),\n                         5 ) * 100\n                )                                             as act_ratio\n        from    daily_detail_file_summary     sm,\n                mif                           mf,\n                merchant                      mr,\n                daily_detail_file_summary     smp\n        where   sm.load_file_id =  :1   and\n                sm.batch_date between  :2   and  :3   and\n                mf.merchant_number = sm.merchant_number and\n                mr.merch_number(+) = sm.merchant_number and\n                smp.merchant_number(+) = sm.merchant_number and\n                smp.batch_date(+) between trunc( (trunc( :4  ,'month')-1), 'month' ) and (trunc( :5  ,'month')-1)\n        group by sm.merchant_number,\n                 sm.batch_date,\n                 mf.risk_activation_date,\n                 sm.bank_sales_amount, sm.nonbank_sales_amount,\n                 mr.merch_month_tot_proj_sales";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"94com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setDate(2,activityDateMin);
   __sJT_st.setDate(3,activityDate);
   __sJT_st.setDate(4,activityDate);
   __sJT_st.setDate(5,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"94com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3081^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        delta = 0;    // default is unknown

        desc.setLength(0);
        desc.append( "Rcvd: " );
        desc.append( MesMath.toCurrency( resultSet.getDouble("daily_volume") ) );

        // use the month end data
        if ( resultSet.getInt("days_active") >= 90 ||
             resultSet.getInt("app_daily_volume") == 0 )
        {
          if ( resultSet.getInt("act_daily_volume") != 0 )
          {
            desc.append( " LM: " );
            desc.append( MesMath.toCurrency( resultSet.getDouble("act_daily_volume") ) );
            desc.append( " Ratio: " );
            desc.append( NumberFormatter.getPercentString( resultSet.getDouble("act_ratio") * 0.01 ) );

            delta = resultSet.getInt("act_ratio");
          }
        }
        else
        {
          desc.append( " App: " );
          desc.append( MesMath.toCurrency( resultSet.getDouble("app_daily_volume") ) );
          desc.append( " Ratio: " );
          desc.append( NumberFormatter.getPercentString( resultSet.getDouble("app_ratio") * 0.01 ) );

          // new merchant, use application data if possible
          delta = resultSet.getInt("app_ratio");
        }
        points = getPoints( resultSet.getLong("merchant_number"),
                            RiskScoreDataBean.TT_VOLUME_EXCEPTIONS,
                            delta,
                            resultSet.getInt("daily_volume") );

        if ( points != 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:3123^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//              (
//                merchant_number,
//                activity_date,
//                risk_cat_id,
//                points,
//                score_desc,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :resultSet.getLong("merchant_number"),
//                :resultSet.getDate("batch_date"),
//                :RiskScoreDataBean.TT_VOLUME_EXCEPTIONS,
//                :points,
//                substr(:desc.toString(),1,64),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4430 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4431 = resultSet.getDate("batch_date");
 String __sJT_4432 = desc.toString();
   String theSqlTS = "insert into risk_score\n            (\n              merchant_number,\n              activity_date,\n              risk_cat_id,\n              points,\n              score_desc,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n              substr( :5  ,1,64),\n               :6  ,\n               :7  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"95com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4430);
   __sJT_st.setDate(2,__sJT_4431);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_VOLUME_EXCEPTIONS);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4432);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3145^11*/
        }
      }
      resultSet.close();
      it.close();

      // load the zero dollar batch & credit volume exceptions
      log.debug("loading zero dollar batch & credit volume exceptions");
      /*@lineinfo:generated-code*//*@lineinfo:3153^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_account_number            as merchant_number,
//                  dt.batch_date                         as batch_date,
//                  dt.batch_number                       as batch_number,
//                  sum( decode( dt.debit_credit_indicator,
//                               'D', 1,
//                                0 ) )                   as sales_count,
//                  sum( decode( dt.debit_credit_indicator,
//                               'D', dt.transaction_amount,
//                                0 ) )                   as sales_amount,
//                  sum( decode( dt.debit_credit_indicator,
//                               'C', 1,
//                                0 ) )                   as credits_count,
//                  sum( decode( dt.debit_credit_indicator,
//                               'C', dt.transaction_amount,
//                                0 ) )                   as credits_amount,
//                  ( sum( decode( dt.debit_credit_indicator,
//                               'D', dt.transaction_amount,
//                                0 ) ) -
//                    sum( decode( dt.debit_credit_indicator,
//                               'C', dt.transaction_amount,
//                                0 ) ) )                 as net_deposit
//          from    daily_detail_file_dt          dt
//          where   dt.load_filename = :loadFilename
//          group by dt.merchant_account_number, dt.batch_date, dt.batch_number
//          order by net_deposit, sales_amount desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_account_number            as merchant_number,\n                dt.batch_date                         as batch_date,\n                dt.batch_number                       as batch_number,\n                sum( decode( dt.debit_credit_indicator,\n                             'D', 1,\n                              0 ) )                   as sales_count,\n                sum( decode( dt.debit_credit_indicator,\n                             'D', dt.transaction_amount,\n                              0 ) )                   as sales_amount,\n                sum( decode( dt.debit_credit_indicator,\n                             'C', 1,\n                              0 ) )                   as credits_count,\n                sum( decode( dt.debit_credit_indicator,\n                             'C', dt.transaction_amount,\n                              0 ) )                   as credits_amount,\n                ( sum( decode( dt.debit_credit_indicator,\n                             'D', dt.transaction_amount,\n                              0 ) ) -\n                  sum( decode( dt.debit_credit_indicator,\n                             'C', dt.transaction_amount,\n                              0 ) ) )                 as net_deposit\n        from    daily_detail_file_dt          dt\n        where   dt.load_filename =  :1  \n        group by dt.merchant_account_number, dt.batch_date, dt.batch_number\n        order by net_deposit, sales_amount desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"96com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"96com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3180^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        netDeposit = resultSet.getDouble("net_deposit");
        if ( netDeposit > 0.0 )
        {
          break;
        }

        if ( netDeposit < 0 )
        {
          riskCat = RiskScoreDataBean.TT_CREDIT_VOLUME;
          volume  = Math.abs(resultSet.getInt("net_deposit"));
        }
        else    // must be 0
        {
          riskCat = RiskScoreDataBean.TT_ZERO_DOLLAR_BATCH;
          volume  = resultSet.getInt("sales_amount");
        }


        // had a zero dollar or credit batch
        points = getPoints( resultSet.getLong("merchant_number"),
                            riskCat,
                            1,
                            volume );

        if ( points != 0 )
        {
          desc.setLength(0);
          desc.append("Sales: ");
          desc.append( MesMath.toCurrency( resultSet.getDouble("sales_amount") ) );
          desc.append(" Credits: ");
          desc.append( MesMath.toCurrency( resultSet.getDouble("credits_amount") ) );

          /*@lineinfo:generated-code*//*@lineinfo:3217^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//              (
//                merchant_number,
//                activity_date,
//                risk_cat_id,
//                points,
//                score_desc,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :resultSet.getLong("merchant_number"),
//                :resultSet.getDate("batch_date"),
//                :riskCat,
//                :points,
//                substr(:desc.toString(),1,64),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4433 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4434 = resultSet.getDate("batch_date");
 String __sJT_4435 = desc.toString();
   String theSqlTS = "insert into risk_score\n            (\n              merchant_number,\n              activity_date,\n              risk_cat_id,\n              points,\n              score_desc,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n              substr( :5  ,1,64),\n               :6  ,\n               :7  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"97com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4433);
   __sJT_st.setDate(2,__sJT_4434);
   __sJT_st.setInt(3,riskCat);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4435);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3239^11*/
        }
      }
      resultSet.close();
      it.close();

      // load the unmatched credits
      log.debug("loading unmatched credit exceptions");
      /*@lineinfo:generated-code*//*@lineinfo:3247^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cred.merchant_number                      as merchant_number,
//                  cred.batch_date                           as batch_date,
//                  count(cred.batch_date)                    as item_count,
//                  avg(cred.tran_amount)                     as avg_amount,
//                  sum(cred.tran_amount)                     as item_amount
//          from    daily_detail_file_credits     cred,
//                  mif                           mf
//          where   cred.load_filename = :loadFilename and
//                  mf.merchant_number = cred.merchant_number and
//                  mf.risk_activation_date < (trunc(:activityDate)-90) and
//                  cred.tran_amount > 150 and
//                  cred.purchase_batch_date is null
//          group by cred.merchant_number, cred.batch_date
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cred.merchant_number                      as merchant_number,\n                cred.batch_date                           as batch_date,\n                count(cred.batch_date)                    as item_count,\n                avg(cred.tran_amount)                     as avg_amount,\n                sum(cred.tran_amount)                     as item_amount\n        from    daily_detail_file_credits     cred,\n                mif                           mf\n        where   cred.load_filename =  :1   and\n                mf.merchant_number = cred.merchant_number and\n                mf.risk_activation_date < (trunc( :2  )-90) and\n                cred.tran_amount > 150 and\n                cred.purchase_batch_date is null\n        group by cred.merchant_number, cred.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"98com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setDate(2,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"98com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3262^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        points = getPoints( resultSet.getLong("merchant_number"),
                            RiskScoreDataBean.TT_UNMATCHED_CREDITS,
                            resultSet.getDouble("item_count"),
                            resultSet.getInt("avg_amount") );

        if ( points != 0 )
        {
          desc.setLength(0);
          desc.append( resultSet.getInt("item_count") );
          desc.append( " credit" );
          if ( resultSet.getInt("item_count") > 1 )
          {
            desc.append("s");
          }
          desc.append( " for " );
          desc.append( MesMath.toCurrency( resultSet.getDouble("item_amount") ) );

          /*@lineinfo:generated-code*//*@lineinfo:3284^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//              (
//                merchant_number,
//                activity_date,
//                risk_cat_id,
//                points,
//                score_desc,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :resultSet.getLong("merchant_number"),
//                :resultSet.getDate("batch_date"),
//                :RiskScoreDataBean.TT_UNMATCHED_CREDITS,
//                :points,
//                substr( :desc.toString(),1,64 ),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4436 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4437 = resultSet.getDate("batch_date");
 String __sJT_4438 = desc.toString();
   String theSqlTS = "insert into risk_score\n            (\n              merchant_number,\n              activity_date,\n              risk_cat_id,\n              points,\n              score_desc,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n              substr(  :5  ,1,64 ),\n               :6  ,\n               :7  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"99com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4436);
   __sJT_st.setDate(2,__sJT_4437);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_UNMATCHED_CREDITS);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4438);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3306^11*/
        }
      }
      resultSet.close();
      it.close();

      // load the large ticket credits
      log.debug("loading large ticket credits exceptions");
      /*@lineinfo:generated-code*//*@lineinfo:3314^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  cred.merchant_number                      as merchant_number,
//                  cred.batch_date                           as batch_date,
//                  cred.card_number                          as card_number,
//                  cred.tran_amount                          as item_amount
//          from    daily_detail_file_credits     cred
//          where   cred.load_filename = :loadFilename and
//                  cred.tran_amount >= 500
//          order by item_amount desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cred.merchant_number                      as merchant_number,\n                cred.batch_date                           as batch_date,\n                cred.card_number                          as card_number,\n                cred.tran_amount                          as item_amount\n        from    daily_detail_file_credits     cred\n        where   cred.load_filename =  :1   and\n                cred.tran_amount >= 500\n        order by item_amount desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"100com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"100com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3324^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        points = getPoints( resultSet.getLong("merchant_number"),
                            RiskScoreDataBean.TT_LARGE_TICKET_CREDITS,
                            1,
                            resultSet.getInt("item_amount") );

        if ( points == 0 )
        {
          // process the entire result
          // set in case there is a hierarchy
          // error or overloaded scoring
          continue;
        }

        desc.setLength(0);
        desc.append( resultSet.getString("card_number") );
        desc.append( " " );
        desc.append( MesMath.toCurrency( resultSet.getDouble("item_amount") ) );

        /*@lineinfo:generated-code*//*@lineinfo:3347^9*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//            (
//              merchant_number,
//              activity_date,
//              risk_cat_id,
//              points,
//              score_desc,
//              load_filename,
//              load_file_id
//            )
//            values
//            (
//              :resultSet.getLong("merchant_number"),
//              :resultSet.getDate("batch_date"),
//              :RiskScoreDataBean.TT_LARGE_TICKET_CREDITS,
//              :points,
//              substr( :desc.toString(),1,64 ),
//              :loadFilename,
//              :loadFileId
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4439 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4440 = resultSet.getDate("batch_date");
 String __sJT_4441 = desc.toString();
   String theSqlTS = "insert into risk_score\n          (\n            merchant_number,\n            activity_date,\n            risk_cat_id,\n            points,\n            score_desc,\n            load_filename,\n            load_file_id\n          )\n          values\n          (\n             :1  ,\n             :2  ,\n             :3  ,\n             :4  ,\n            substr(  :5  ,1,64 ),\n             :6  ,\n             :7  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"101com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4439);
   __sJT_st.setDate(2,__sJT_4440);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_LARGE_TICKET_CREDITS);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4441);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3369^9*/
      }
      resultSet.close();
      it.close();

      // load the large ticket sales
      log.debug("loading large ticket sales exceptions");
      /*@lineinfo:generated-code*//*@lineinfo:3376^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_account_number                as merchant_number,
//                  dt.batch_date                             as batch_date,
//                  dt.cardholder_account_number              as card_number,
//                  emd.pos_entry_desc                        as entry_mode,
//                  dt.transaction_amount                     as item_amount
//          from    daily_detail_file_dt        dt,
//                  pos_entry_mode_desc         emd
//          where   dt.load_filename = :loadFilename and
//                  dt.debit_credit_indicator = 'D' and
//                  dt.transaction_amount >= 1000 and
//                  emd.pos_entry_code(+) = nvl(dt.pos_entry_mode,'00')
//          order by item_amount desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_account_number                as merchant_number,\n                dt.batch_date                             as batch_date,\n                dt.cardholder_account_number              as card_number,\n                emd.pos_entry_desc                        as entry_mode,\n                dt.transaction_amount                     as item_amount\n        from    daily_detail_file_dt        dt,\n                pos_entry_mode_desc         emd\n        where   dt.load_filename =  :1   and\n                dt.debit_credit_indicator = 'D' and\n                dt.transaction_amount >= 1000 and\n                emd.pos_entry_code(+) = nvl(dt.pos_entry_mode,'00')\n        order by item_amount desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"102com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"102com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3390^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        points = getPoints( resultSet.getLong("merchant_number"),
                            RiskScoreDataBean.TT_LARGE_TICKET_SALES,
                            1,
                            resultSet.getInt("item_amount") );

        if ( points == 0 )
        {
          // process the entire result
          // set in case there is a hierarchy
          // error or overloaded scoring
          continue;
        }

        desc.setLength(0);
        desc.append( resultSet.getString("card_number") );
        desc.append( " " );
        desc.append( MesMath.toCurrency( resultSet.getDouble("item_amount") ) );
        desc.append( " " );
        desc.append( resultSet.getString("entry_mode") );

        /*@lineinfo:generated-code*//*@lineinfo:3415^9*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//            (
//              merchant_number,
//              activity_date,
//              risk_cat_id,
//              points,
//              score_desc,
//              load_filename,
//              load_file_id
//            )
//            values
//            (
//              :resultSet.getLong("merchant_number"),
//              :resultSet.getDate("batch_date"),
//              :RiskScoreDataBean.TT_LARGE_TICKET_SALES,
//              :points,
//              substr( :desc.toString(),1,64 ),
//              :loadFilename,
//              :loadFileId
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4442 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4443 = resultSet.getDate("batch_date");
 String __sJT_4444 = desc.toString();
   String theSqlTS = "insert into risk_score\n          (\n            merchant_number,\n            activity_date,\n            risk_cat_id,\n            points,\n            score_desc,\n            load_filename,\n            load_file_id\n          )\n          values\n          (\n             :1  ,\n             :2  ,\n             :3  ,\n             :4  ,\n            substr(  :5  ,1,64 ),\n             :6  ,\n             :7  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"103com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4442);
   __sJT_st.setDate(2,__sJT_4443);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_LARGE_TICKET_SALES);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4444);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3437^9*/
      }
      resultSet.close();
      it.close();

      // load the duplicate card numbers
      log.debug("loading duplicate card number exceptions");
      /*@lineinfo:generated-code*//*@lineinfo:3444^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  distinct
//                  dt.merchant_account_number        as merchant_number,
//                  dt.cardholder_account_number      as card_number
//          from    daily_detail_file_dt    dt
//          where   dt.load_filename = :loadFilename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  distinct\n                dt.merchant_account_number        as merchant_number,\n                dt.cardholder_account_number      as card_number\n        from    daily_detail_file_dt    dt\n        where   dt.load_filename =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"104com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"104com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3451^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        /*@lineinfo:generated-code*//*@lineinfo:3456^9*/

//  ************************************************************
//  #sql [Ctx] { select nvl( sum( ds.sales_count ),0 ),
//                   nvl( sum( ds.sales_amount),0 ) 
//            from   risk_duplicate_card_summary      ds
//            where  ds.merchant_number = :resultSet.getLong("merchant_number") and
//                   ds.batch_date between (:activityDate - 7) and :activityDate and
//                   ds.card_number_str = :resultSet.getString("card_number") and
//                   ds.sales_count > 0 and
//                   ( ( ds.sales_amount >= 300 ) or
//                     ( (ds.sales_amount/decode(ds.sales_count, 0, 1, ds.sales_count)) > 50 ) )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4445 = resultSet.getLong("merchant_number");
 String __sJT_4446 = resultSet.getString("card_number");
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select nvl( sum( ds.sales_count ),0 ),\n                 nvl( sum( ds.sales_amount),0 )  \n          from   risk_duplicate_card_summary      ds\n          where  ds.merchant_number =  :1   and\n                 ds.batch_date between ( :2   - 7) and  :3   and\n                 ds.card_number_str =  :4   and\n                 ds.sales_count > 0 and\n                 ( ( ds.sales_amount >= 300 ) or\n                   ( (ds.sales_amount/decode(ds.sales_count, 0, 1, ds.sales_count)) > 50 ) )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"105com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4445);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setDate(3,activityDate);
   __sJT_st.setString(4,__sJT_4446);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   itemCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   itemAmount = __sJT_rs.getDouble(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3467^9*/

        if ( itemCount < 2 )
        {
          continue;
        }

        points = getPoints( resultSet.getLong("merchant_number"),
                            RiskScoreDataBean.TT_DUPLICATE_CARD,
                            itemCount );

        if ( points != 0 )
        {
          desc.setLength(0);
          desc.append( resultSet.getString("card_number") );
          desc.append( " " );
          desc.append( itemCount );
          desc.append( " for " );
          desc.append( MesMath.toCurrency( itemAmount ) );

          /*@lineinfo:generated-code*//*@lineinfo:3487^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//              (
//                merchant_number,
//                activity_date,
//                risk_cat_id,
//                points,
//                score_desc,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :resultSet.getLong("merchant_number"),
//                :activityDate,
//                :RiskScoreDataBean.TT_DUPLICATE_CARD,
//                :points,
//                substr( :desc.toString(),1,64),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4447 = resultSet.getLong("merchant_number");
 String __sJT_4448 = desc.toString();
   String theSqlTS = "insert into risk_score\n            (\n              merchant_number,\n              activity_date,\n              risk_cat_id,\n              points,\n              score_desc,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n              substr(  :5  ,1,64),\n               :6  ,\n               :7  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"106com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4447);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_DUPLICATE_CARD);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4448);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3509^11*/
        }
      }
      resultSet.close();
      it.close();

      // load the average ticket exceptions
      log.debug("loading average ticket exceptions");
      /*@lineinfo:generated-code*//*@lineinfo:3517^7*/

//  ************************************************************
//  #sql [Ctx] it = { select ddf_sm.merchant_number                     as merchant_number,
//                 ddf_sm.batch_date                          as batch_date,
//                 (ddf_sm.batch_date-mf.risk_activation_date)as days_active,
//                 decode( ( ddf_sm.bank_sales_amount + ddf_sm.nonbank_sales_amount ),
//                         0, 0,
//                         round( ((ddf_sm.bank_sales_amount + ddf_sm.nonbank_sales_amount)/
//                                 (ddf_sm.bank_sales_count + ddf_sm.nonbank_sales_count)),2 )
//                       )                                    as daily_avg_ticket,
//                 nvl(mr.merch_average_cc_tran,0)            as app_avg_ticket,
//                 (nvl(gn.t1_tot_amount_of_sales,1000) /
//                    decode(gn.t1_tot_number_of_sales,
//                                null, 1,
//                                0,    1,
//                                gn.t1_tot_number_of_sales)) as me_avg_ticket,
//                 ( ddf_sm.bank_sales_count +
//                   ddf_sm.nonbank_sales_count )             as item_count,
//                 ( ddf_sm.bank_sales_amount +
//                   ddf_sm.nonbank_sales_amount )            as item_amount
//          from   daily_detail_file_summary  ddf_sm,
//                 mif                        mf,
//                 merchant                   mr,
//                 monthly_extract_gn         gn
//          where  ddf_sm.load_file_id = :loadFileId and
//                 ddf_sm.batch_date between :activityDateMin and :activityDate and
//                 mf.merchant_number = ddf_sm.merchant_number and
//                 mr.merch_number(+) = mf.merchant_number and
//                 gn.hh_merchant_number(+) = ddf_sm.merchant_number and
//                 gn.hh_active_date(+) = trunc( (trunc(ddf_sm.batch_date,'month')-1),'month' )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select ddf_sm.merchant_number                     as merchant_number,\n               ddf_sm.batch_date                          as batch_date,\n               (ddf_sm.batch_date-mf.risk_activation_date)as days_active,\n               decode( ( ddf_sm.bank_sales_amount + ddf_sm.nonbank_sales_amount ),\n                       0, 0,\n                       round( ((ddf_sm.bank_sales_amount + ddf_sm.nonbank_sales_amount)/\n                               (ddf_sm.bank_sales_count + ddf_sm.nonbank_sales_count)),2 )\n                     )                                    as daily_avg_ticket,\n               nvl(mr.merch_average_cc_tran,0)            as app_avg_ticket,\n               (nvl(gn.t1_tot_amount_of_sales,1000) /\n                  decode(gn.t1_tot_number_of_sales,\n                              null, 1,\n                              0,    1,\n                              gn.t1_tot_number_of_sales)) as me_avg_ticket,\n               ( ddf_sm.bank_sales_count +\n                 ddf_sm.nonbank_sales_count )             as item_count,\n               ( ddf_sm.bank_sales_amount +\n                 ddf_sm.nonbank_sales_amount )            as item_amount\n        from   daily_detail_file_summary  ddf_sm,\n               mif                        mf,\n               merchant                   mr,\n               monthly_extract_gn         gn\n        where  ddf_sm.load_file_id =  :1   and\n               ddf_sm.batch_date between  :2   and  :3   and\n               mf.merchant_number = ddf_sm.merchant_number and\n               mr.merch_number(+) = mf.merchant_number and\n               gn.hh_merchant_number(+) = ddf_sm.merchant_number and\n               gn.hh_active_date(+) = trunc( (trunc(ddf_sm.batch_date,'month')-1),'month' )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"107com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setDate(2,activityDateMin);
   __sJT_st.setDate(3,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"107com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3547^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        delta     = 0;    // default is unknown
        basis     = 0;
        basisType = RiskTransDataBean.AVG_TICKET_BASIS_NONE;

        desc.setLength(0);
        desc.append( "Rcvd: ");
        desc.append( MesMath.toCurrency( resultSet.getDouble("daily_avg_ticket") ) );

        // use the month end data
        if ( resultSet.getInt("days_active") >= 90 ||
             resultSet.getInt("app_avg_ticket") == 0 )
        {
          // use the m/e average ticket
          delta = ( resultSet.getInt("daily_avg_ticket") -
                    resultSet.getInt("me_avg_ticket") );
          basis = resultSet.getInt("me_avg_ticket");
          basisType = RiskTransDataBean.AVG_TICKET_BASIS_ME;

          desc.append( " LM: " );
          desc.append( MesMath.toCurrency( resultSet.getDouble("me_avg_ticket") ) );
          desc.append( " Diff: " );
          desc.append( MesMath.toCurrency( ( resultSet.getDouble("daily_avg_ticket") -
                                             resultSet.getDouble("me_avg_ticket") ) ) );
        }
        else
        {
          // new merchant, use application data if possible
          delta = ( resultSet.getInt("daily_avg_ticket") -
                    resultSet.getInt("app_avg_ticket") );
          basis = resultSet.getInt("app_avg_ticket");
          basisType = RiskTransDataBean.AVG_TICKET_BASIS_APP;

          desc.append( " App: " );
          desc.append( MesMath.toCurrency( resultSet.getDouble("app_avg_ticket") ) );
          desc.append( " Diff: " );
          desc.append( MesMath.toCurrency( ( resultSet.getDouble("daily_avg_ticket") -
                                             resultSet.getDouble("app_avg_ticket") ) ) );
        }
        points = getPoints( resultSet.getLong("merchant_number"),
                            RiskScoreDataBean.TT_AVG_TICKET,
                            delta,
                            resultSet.getInt("item_count") );

        if ( points != 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:3597^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_avg_ticket_exceptions
//              (
//                merchant_number,
//                batch_date,
//                basis_type,
//                basis_amount,
//                avg_ticket_amount,
//                item_total_count,
//                item_total_amount,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :resultSet.getLong("merchant_number"),
//                :resultSet.getDate("batch_date"),
//                :basisType,
//                :basis,
//                :resultSet.getInt("daily_avg_ticket"),
//                :resultSet.getInt("item_count"),
//                :resultSet.getDouble("item_amount"),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4449 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4450 = resultSet.getDate("batch_date");
 int __sJT_4451 = resultSet.getInt("daily_avg_ticket");
 int __sJT_4452 = resultSet.getInt("item_count");
 double __sJT_4453 = resultSet.getDouble("item_amount");
   String theSqlTS = "insert into risk_avg_ticket_exceptions\n            (\n              merchant_number,\n              batch_date,\n              basis_type,\n              basis_amount,\n              avg_ticket_amount,\n              item_total_count,\n              item_total_amount,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n               :5  ,\n               :6  ,\n               :7  ,\n               :8  ,\n               :9  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"108com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4449);
   __sJT_st.setDate(2,__sJT_4450);
   __sJT_st.setInt(3,basisType);
   __sJT_st.setInt(4,basis);
   __sJT_st.setInt(5,__sJT_4451);
   __sJT_st.setInt(6,__sJT_4452);
   __sJT_st.setDouble(7,__sJT_4453);
   __sJT_st.setString(8,loadFilename);
   __sJT_st.setLong(9,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3623^11*/

          /*@lineinfo:generated-code*//*@lineinfo:3625^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//              (
//                merchant_number,
//                activity_date,
//                risk_cat_id,
//                points,
//                score_desc,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :resultSet.getLong("merchant_number"),
//                :resultSet.getDate("batch_date"),
//                :RiskScoreDataBean.TT_AVG_TICKET,
//                :points,
//                substr( :desc.toString(),1,64 ),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4454 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4455 = resultSet.getDate("batch_date");
 String __sJT_4456 = desc.toString();
   String theSqlTS = "insert into risk_score\n            (\n              merchant_number,\n              activity_date,\n              risk_cat_id,\n              points,\n              score_desc,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n              substr(  :5  ,1,64 ),\n               :6  ,\n               :7  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"109com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4454);
   __sJT_st.setDate(2,__sJT_4455);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_AVG_TICKET);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4456);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3647^11*/
        }
      }
      resultSet.close();
      it.close();

      // load keyed volume exceptions
      log.debug("loading keyed volume exceptions");
      /*@lineinfo:generated-code*//*@lineinfo:3655^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    /*+ ordered */
//                    dt.merchant_account_number              as merchant_number,
//                    dt.batch_date                           as batch_date,
//                    sum( decode( nvl(dt.pos_entry_mode,'00'),
//                                 '01',1,
//                                 '81',1,
//                                 0 ) )                      as keyed_count,
//                    count( dt.merchant_account_number )     as total_count,
//                    ( sum( decode( nvl(dt.pos_entry_mode,'00'),
//                                   '01',1,
//                                   '81',1,
//                                   0 ) ) /
//                      count( dt.merchant_account_number ) ) * 100
//                                                            as ratio
//          from      daily_detail_file_dt    dt,
//                    merchant                mr,
//                    transcom_merchant       tm
//          where     dt.load_filename = :loadFilename and
//                    mr.merch_number = dt.merchant_account_number and
//                    tm.app_seq_num(+) = mr.app_seq_num and
//                    tm.app_seq_num is null and  --@
//                    (
//                      ( not tm.app_seq_num is null and -- tps app
//                        nvl(tm.cardholder_not_present,0) < 50 ) or
//                      ( tm.app_seq_num is null and -- normal app
//                        mr.merch_mail_phone_sales < 50 )
//                    )
//          group by dt.merchant_account_number, dt.batch_date
//          order by ratio desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    /*+ ordered */\n                  dt.merchant_account_number              as merchant_number,\n                  dt.batch_date                           as batch_date,\n                  sum( decode( nvl(dt.pos_entry_mode,'00'),\n                               '01',1,\n                               '81',1,\n                               0 ) )                      as keyed_count,\n                  count( dt.merchant_account_number )     as total_count,\n                  ( sum( decode( nvl(dt.pos_entry_mode,'00'),\n                                 '01',1,\n                                 '81',1,\n                                 0 ) ) /\n                    count( dt.merchant_account_number ) ) * 100\n                                                          as ratio\n        from      daily_detail_file_dt    dt,\n                  merchant                mr,\n                  transcom_merchant       tm\n        where     dt.load_filename =  :1   and\n                  mr.merch_number = dt.merchant_account_number and\n                  tm.app_seq_num(+) = mr.app_seq_num and\n                  tm.app_seq_num is null and  --@\n                  (\n                    ( not tm.app_seq_num is null and -- tps app\n                      nvl(tm.cardholder_not_present,0) < 50 ) or\n                    ( tm.app_seq_num is null and -- normal app\n                      mr.merch_mail_phone_sales < 50 )\n                  )\n        group by dt.merchant_account_number, dt.batch_date\n        order by ratio desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"110com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"110com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3686^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        // stop once the ratio goes to 0%
        if ( resultSet.getDouble("ratio") == 0.0 )
        {
          break;
        }

        points = getPoints( resultSet.getLong("merchant_number"),
                            RiskScoreDataBean.TT_KEYED_VOLUME,
                            resultSet.getDouble("ratio"),
                            resultSet.getInt("keyed_count") );

        if ( points == 0 )
        {
          continue;
        }

        desc.setLength(0);
        desc.append( "Keyed: ");
        desc.append( resultSet.getInt("keyed_count") );
        desc.append( " Total: " );
        desc.append( resultSet.getInt("total_count") );
        desc.append( " Ratio: " );
        desc.append( NumberFormatter.getPercentString( resultSet.getDouble("ratio")*0.01, 2 ) );

        /*@lineinfo:generated-code*//*@lineinfo:3715^9*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//            (
//              merchant_number,
//              activity_date,
//              risk_cat_id,
//              points,
//              score_desc,
//              load_filename,
//              load_file_id
//            )
//            values
//            (
//              :resultSet.getLong("merchant_number"),
//              :resultSet.getDate("batch_date"),
//              :RiskScoreDataBean.TT_KEYED_VOLUME,
//              :points,
//              substr( :desc.toString(),1,64 ),
//              :loadFilename,
//              :loadFileId
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4457 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4458 = resultSet.getDate("batch_date");
 String __sJT_4459 = desc.toString();
   String theSqlTS = "insert into risk_score\n          (\n            merchant_number,\n            activity_date,\n            risk_cat_id,\n            points,\n            score_desc,\n            load_filename,\n            load_file_id\n          )\n          values\n          (\n             :1  ,\n             :2  ,\n             :3  ,\n             :4  ,\n            substr(  :5  ,1,64 ),\n             :6  ,\n             :7  \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"111com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4457);
   __sJT_st.setDate(2,__sJT_4458);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_KEYED_VOLUME);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4459);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3737^9*/
      }
      resultSet.close();
      it.close();

      // load the auth/capture ratio exceptions
      log.debug("loading auth/capture ratio exceptions");
      /*@lineinfo:generated-code*//*@lineinfo:3744^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ index (sm pk_tc33_risk_summary) */
//                  sm.merchant_number                  as merchant_number,
//                  sum(sm.vmc_auth_total_count)        as auth_total_count,
//                  sum(sm.vmc_sales_count)             as sales_count,
//                  decode( sum(sm.vmc_sales_count),
//                          0, 0,
//                          round((sum(sm.vmc_auth_total_count)/
//                                 sum(sm.vmc_sales_count)),5) *
//                                 100 )                as ratio
//          from    tc33_risk_summary     sm
//          where   sm.merchant_number in
//                  (
//                    select  distinct dt.merchant_account_number
//                    from    daily_detail_file_dt dt
//                    where   dt.load_filename = :loadFilename
//                  )
//                  and sm.transaction_date between (:activityDate - 3) and :activityDate
//          group by  sm.merchant_number
//          order by ratio desc
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ index (sm pk_tc33_risk_summary) */\n                sm.merchant_number                  as merchant_number,\n                sum(sm.vmc_auth_total_count)        as auth_total_count,\n                sum(sm.vmc_sales_count)             as sales_count,\n                decode( sum(sm.vmc_sales_count),\n                        0, 0,\n                        round((sum(sm.vmc_auth_total_count)/\n                               sum(sm.vmc_sales_count)),5) *\n                               100 )                as ratio\n        from    tc33_risk_summary     sm\n        where   sm.merchant_number in\n                (\n                  select  distinct dt.merchant_account_number\n                  from    daily_detail_file_dt dt\n                  where   dt.load_filename =  :1  \n                )\n                and sm.transaction_date between ( :2   - 3) and  :3  \n        group by  sm.merchant_number\n        order by ratio desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"112com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setDate(3,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"112com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3765^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        if ( resultSet.getDouble("ratio") <= 100 )
        {
          // results are sorted by ratio descending.
          // as soon as there are as many capture as auth (100%),
          // then exit the loop to save time
          break;
        }

        delta = 0;    // default is unknown

        points = getPoints( resultSet.getLong("merchant_number"),
                            RiskScoreDataBean.TT_AUTH_CAPTURE_RATIO,
                            resultSet.getDouble("ratio"),
                            resultSet.getInt("auth_total_count") );

        if ( points != 0 )
        {
          desc.setLength(0);
          desc.append("Auth: ");
          desc.append( resultSet.getInt("auth_total_count") );
          desc.append(" Capt: ");
          desc.append( resultSet.getInt("sales_count") );
          desc.append(" Ratio: ");
          desc.append( NumberFormatter.getPercentString( (resultSet.getDouble("ratio") * 0.01) ) );

          /*@lineinfo:generated-code*//*@lineinfo:3795^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//              (
//                merchant_number,
//                activity_date,
//                risk_cat_id,
//                points,
//                score_desc,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :resultSet.getLong("merchant_number"),
//                :activityDate,
//                :RiskScoreDataBean.TT_AUTH_CAPTURE_RATIO,
//                :points,
//                substr( :desc.toString(),1,64 ),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4460 = resultSet.getLong("merchant_number");
 String __sJT_4461 = desc.toString();
   String theSqlTS = "insert into risk_score\n            (\n              merchant_number,\n              activity_date,\n              risk_cat_id,\n              points,\n              score_desc,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n              substr(  :5  ,1,64 ),\n               :6  ,\n               :7  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"113com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4460);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_AUTH_CAPTURE_RATIO);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4461);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3817^11*/
        }
      }
      resultSet.close();
      it.close();


      // load the credit/sales ratio exceptions
      // load the average daily sales count exception
      log.debug("loading credit/sales ratio exceptions");
      log.debug("loading average daily sales count exceptions");
      /*@lineinfo:generated-code*//*@lineinfo:3828^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ ordered */
//                  sm.merchant_number                                      as merchant_number,
//                  sum(sm.bank_sales_count    + sm.nonbank_sales_count   ) as sales_count,
//                  sum(sm.bank_sales_amount   + sm.nonbank_sales_amount  ) as sales_amount,
//                  sum(sm.bank_credits_count  + sm.nonbank_credits_count ) as credits_count,
//                  sum(sm.bank_credits_amount + sm.nonbank_credits_amount) as credits_amount,
//                  round( ( sum(sm.bank_credits_amount + sm.nonbank_credits_amount)/
//                           decode( sum(sm.bank_sales_amount + sm.nonbank_sales_amount),
//                                   0, 1,
//                                   sum(sm.bank_sales_amount + sm.nonbank_sales_amount) )
//                         ) * 100, 5 )                                     as ratio
//          from    daily_detail_file_summary   sm
//          where   sm.load_file_id = :loadFileId
//                  and sm.batch_date between :activityDateMin and :activityDate
//          group by sm.merchant_number
//          order by ratio desc, sm.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ ordered */\n                sm.merchant_number                                      as merchant_number,\n                sum(sm.bank_sales_count    + sm.nonbank_sales_count   ) as sales_count,\n                sum(sm.bank_sales_amount   + sm.nonbank_sales_amount  ) as sales_amount,\n                sum(sm.bank_credits_count  + sm.nonbank_credits_count ) as credits_count,\n                sum(sm.bank_credits_amount + sm.nonbank_credits_amount) as credits_amount,\n                round( ( sum(sm.bank_credits_amount + sm.nonbank_credits_amount)/\n                         decode( sum(sm.bank_sales_amount + sm.nonbank_sales_amount),\n                                 0, 1,\n                                 sum(sm.bank_sales_amount + sm.nonbank_sales_amount) )\n                       ) * 100, 5 )                                     as ratio\n        from    daily_detail_file_summary   sm\n        where   sm.load_file_id =  :1  \n                and sm.batch_date between  :2   and  :3  \n        group by sm.merchant_number\n        order by ratio desc, sm.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"114com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
   __sJT_st.setDate(2,activityDateMin);
   __sJT_st.setDate(3,activityDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"114com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3846^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        double testValue = resultSet.getDouble("ratio");
        if ( testValue < 5 )
        {
          // results are sorted by ratio descending.
          // as soon as the ratio falls below 5%
          // exit the loop to save time
          break;
        }

        delta = 0;    // default is unknown

        points = getPoints( resultSet.getLong("merchant_number"),
                            RiskScoreDataBean.TT_CREDIT_SALES_RATIO,
                            testValue,
                            resultSet.getInt("credits_amount") );

        if ( points != 0 )
        {
          desc.setLength(0);
          desc.append("Credits: ");
          desc.append( MesMath.toCurrency(resultSet.getDouble("credits_amount")) );
          desc.append(" Sales: ");
          desc.append( MesMath.toCurrency(resultSet.getDouble("sales_amount")) );
          desc.append(" Ratio: ");
          desc.append( NumberFormatter.getPercentString( (testValue * 0.01) ) );

          /*@lineinfo:generated-code*//*@lineinfo:3877^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//              (
//                merchant_number,
//                activity_date,
//                risk_cat_id,
//                points,
//                score_desc,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :resultSet.getLong("merchant_number"),
//                :activityDate,
//                :RiskScoreDataBean.TT_CREDIT_SALES_RATIO,
//                :points,
//                substr( :desc.toString(),1,64 ),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4462 = resultSet.getLong("merchant_number");
 String __sJT_4463 = desc.toString();
   String theSqlTS = "insert into risk_score\n            (\n              merchant_number,\n              activity_date,\n              risk_cat_id,\n              points,\n              score_desc,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n              substr(  :5  ,1,64 ),\n               :6  ,\n               :7  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"115com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4462);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_CREDIT_SALES_RATIO);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4463);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3899^11*/
        }

        if ( testValue < 120 )
        {
          // results are sorted by ratio descending.
          // as soon as the ratio falls below 120%
          // skip the remaining tests for this loop to save time
          continue;
        }

        delta = 0;    // default is unknown

        points = getPoints( resultSet.getLong("merchant_number"),
                            RiskScoreDataBean.TT_AVG_DAILY_SALES_COUNT,
                            testValue,
                            resultSet.getInt("sales_count") );

        if ( points != 0 )
        {
          desc.setLength(0);
          desc.append("Sales: ");
          desc.append( resultSet.getInt("sales_count") );
          desc.append(" for ");
          desc.append( MesMath.toCurrency(resultSet.getDouble("sales_amount")) );
          desc.append(" Avg: ");
          desc.append( resultSet.getInt("prev_month_avg") );
          desc.append(" Ratio: ");
          desc.append( NumberFormatter.getPercentString( (testValue * 0.01) ) );

          /*@lineinfo:generated-code*//*@lineinfo:3929^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//              (
//                merchant_number,
//                activity_date,
//                risk_cat_id,
//                points,
//                score_desc,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :resultSet.getLong("merchant_number"),
//                :activityDate,
//                :RiskScoreDataBean.TT_AVG_DAILY_SALES_COUNT,
//                :points,
//                substr( :desc.toString(),1,64 ),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4464 = resultSet.getLong("merchant_number");
 String __sJT_4465 = desc.toString();
   String theSqlTS = "insert into risk_score\n            (\n              merchant_number,\n              activity_date,\n              risk_cat_id,\n              points,\n              score_desc,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n              substr(  :5  ,1,64 ),\n               :6  ,\n               :7  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"116com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4464);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_AVG_DAILY_SALES_COUNT);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4465);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3951^11*/
        }
      }
      resultSet.close();
      it.close();

      /*@lineinfo:generated-code*//*@lineinfo:3957^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3960^7*/

      fileProcessed = true;
    }
    catch( Exception e )
    {
      logEntry("loadPointsCapture( " + loadFilename + " )", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return( fileProcessed );
  }

  private boolean loadPointsForeignCards( String loadFilename )
  {
    Date                activityDate    = null;
    StringBuffer        desc            = new StringBuffer();
    boolean             fileProcessed   = false;
    ResultSetIterator   it              = null;
    long                loadFileId      = 0L;
    int                 points          = 0;
    ResultSet           resultSet       = null;


    try
    {
      loadFileId = loadFilenameToLoadFileId(loadFilename);

      /*@lineinfo:generated-code*//*@lineinfo:3990^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    risk_score    rs
//          where   rs.load_file_id = :loadFileId and
//                  rs.risk_cat_id in ( 31 )    -- foreign card only
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    risk_score    rs\n        where   rs.load_file_id =  :1   and\n                rs.risk_cat_id in ( 31 )    -- foreign card only";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"117com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:3996^7*/

      /*@lineinfo:generated-code*//*@lineinfo:3998^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_account_number          as merchant_number,
//                  dt.cardholder_account_number        as card_number,
//                  dt.batch_date                       as batch_date,
//                  ( dt.transaction_amount *
//                    decode(dt.debit_credit_indicator,'C',-1,1) )
//                                                      as tran_amount,
//                  dt.transaction_amount               as tran_amount_abs
//          from    daily_detail_file_dt    dt
//          where   dt.load_filename = :loadFilename and
//                  not dt.foreign_card_indicator is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_account_number          as merchant_number,\n                dt.cardholder_account_number        as card_number,\n                dt.batch_date                       as batch_date,\n                ( dt.transaction_amount *\n                  decode(dt.debit_credit_indicator,'C',-1,1) )\n                                                    as tran_amount,\n                dt.transaction_amount               as tran_amount_abs\n        from    daily_detail_file_dt    dt\n        where   dt.load_filename =  :1   and\n                not dt.foreign_card_indicator is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"118com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"118com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4010^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        points = getPoints( resultSet.getLong("merchant_number"),
                            RiskScoreDataBean.TT_FOREIGN_CARD_NUMBER,
                            1, resultSet.getInt("tran_amount_abs") );

        if ( points != 0 )
        {
          desc.setLength(0);
          desc.append( "Foreign Card: " );
          desc.append( resultSet.getString("card_number") );
          desc.append( " for " );
          desc.append( MesMath.toCurrency( resultSet.getDouble("tran_amount") ) );

          /*@lineinfo:generated-code*//*@lineinfo:4027^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//              (
//                merchant_number,
//                activity_date,
//                risk_cat_id,
//                points,
//                score_desc,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :resultSet.getLong("merchant_number"),
//                :resultSet.getDate("batch_date"),
//                :RiskScoreDataBean.TT_FOREIGN_CARD_NUMBER,
//                :points,
//                substr(:desc.toString(),1,64),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4466 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4467 = resultSet.getDate("batch_date");
 String __sJT_4468 = desc.toString();
   String theSqlTS = "insert into risk_score\n            (\n              merchant_number,\n              activity_date,\n              risk_cat_id,\n              points,\n              score_desc,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n              substr( :5  ,1,64),\n               :6  ,\n               :7  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"119com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4466);
   __sJT_st.setDate(2,__sJT_4467);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_FOREIGN_CARD_NUMBER);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,__sJT_4468);
   __sJT_st.setString(6,loadFilename);
   __sJT_st.setLong(7,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4049^11*/
        }
      }
      resultSet.close();
      it.close();

      fileProcessed = true;
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadPointsForeignCards()",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception ee){}
    }
    return( fileProcessed );
  }

  private boolean loadPointsNewAccount( long merchantId )
  {
    boolean         processed         = false;
    int             riskScore         = 100;

    try
    {
      Date dummy = null;

      /*@lineinfo:generated-code*//*@lineinfo:4077^7*/

//  ************************************************************
//  #sql [Ctx] { select sysdate 
//          from dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sysdate  \n        from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"120com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   dummy = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4081^7*/
      processed = true;
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadPointsNewAccount()",e.toString());
    }
    finally
    {
    }
    return( processed );
  }

  private void loadPointsSequentialAuths( String loadFilename )
  {
    Date                    activityDate    = null;
    int                     cardCount       = 0;
    String                  cardNumber      = null;
    ResultSetIterator       it              = null;
    long                    merchantId      = 0L;
    int                     points          = 0;
    ResultSet               resultSet       = null;
    long                    lastMerchantId  = 0L;
    String                  lastCardNumber  = null;
    long                    loadFileId      = 0L;

    try
    {
      loadFileId = loadFilenameToLoadFileId(loadFilename);

      /*@lineinfo:generated-code*//*@lineinfo:4111^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  auth.merchant_number              as merchant_number,
//                  auth.transaction_date             as tran_date,
//                  auth.card_number                  as card_number
//          from    tc33_authorization      auth
//          where   auth.load_filename = :loadFilename and
//                  auth.merchant_number != 0
//          order by auth.merchant_number, auth.transaction_time
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  auth.merchant_number              as merchant_number,\n                auth.transaction_date             as tran_date,\n                auth.card_number                  as card_number\n        from    tc33_authorization      auth\n        where   auth.load_filename =  :1   and\n                auth.merchant_number != 0\n        order by auth.merchant_number, auth.transaction_time";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"121com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"121com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4120^7*/
      resultSet = it.getResultSet();

      while(resultSet.next())
      {
        activityDate  = resultSet.getDate("tran_date");
        merchantId    = resultSet.getLong("merchant_number");
        cardNumber    = resultSet.getString("card_number");

        // handle first time cases
        if( lastMerchantId == 0L )
        {
          lastMerchantId = merchantId;
        }
        if ( lastCardNumber == null )
        {
          lastCardNumber = cardNumber;
        }

        // check for merchant boundary condition
        if ( ( merchantId != lastMerchantId ) ||
             ( !lastCardNumber.equals(cardNumber) ) )
        {
          if ( cardCount > 1 )
          {
            if( ( points = getPoints( lastMerchantId, RiskScoreDataBean.TT_SEQUENTIAL_AUTHS, cardCount )) != 0 )
            {
              /*@lineinfo:generated-code*//*@lineinfo:4147^15*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//                  (
//                    merchant_number,
//                    activity_date,
//                    risk_cat_id,
//                    points,
//                    score_desc,
//                    load_filename,
//                    load_file_id
//                  )
//                  values
//                  (
//                    :lastMerchantId,
//                    :activityDate,
//                    :RiskScoreDataBean.TT_SEQUENTIAL_AUTHS,
//                    :points,
//                    ( :lastCardNumber || ' ' || :cardCount || ' times'),
//                    :loadFilename,
//                    :loadFileId
//                  )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_score\n                (\n                  merchant_number,\n                  activity_date,\n                  risk_cat_id,\n                  points,\n                  score_desc,\n                  load_filename,\n                  load_file_id\n                )\n                values\n                (\n                   :1  ,\n                   :2  ,\n                   :3  ,\n                   :4  ,\n                  (  :5   || ' ' ||  :6   || ' times'),\n                   :7  ,\n                   :8  \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"122com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,lastMerchantId);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_SEQUENTIAL_AUTHS);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,lastCardNumber);
   __sJT_st.setInt(6,cardCount);
   __sJT_st.setString(7,loadFilename);
   __sJT_st.setLong(8,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4169^15*/
            }
          }
          cardCount       = 0;
        }
        ++cardCount;    // increment the sequential auth count

        // store the latest value
        lastCardNumber = cardNumber;
        lastMerchantId = merchantId;
      }

      // boundary condition
      if ( cardCount > 1 )
      {
        if( ( points = getPoints( lastMerchantId, RiskScoreDataBean.TT_SEQUENTIAL_AUTHS, cardCount )) != 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:4186^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_score
//              (
//                merchant_number,
//                activity_date,
//                risk_cat_id,
//                points,
//                score_desc,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :lastMerchantId,
//                :activityDate,
//                :RiskScoreDataBean.TT_SEQUENTIAL_AUTHS,
//                :points,
//                ( :lastCardNumber || ' ' || :cardCount || ' times'),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_score\n            (\n              merchant_number,\n              activity_date,\n              risk_cat_id,\n              points,\n              score_desc,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1  ,\n               :2  ,\n               :3  ,\n               :4  ,\n              (  :5   || ' ' ||  :6   || ' times'),\n               :7  ,\n               :8  \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"123com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,lastMerchantId);
   __sJT_st.setDate(2,activityDate);
   __sJT_st.setInt(3,RiskScoreDataBean.TT_SEQUENTIAL_AUTHS);
   __sJT_st.setInt(4,points);
   __sJT_st.setString(5,lastCardNumber);
   __sJT_st.setInt(6,cardCount);
   __sJT_st.setString(7,loadFilename);
   __sJT_st.setLong(8,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4208^11*/
        }
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadPointsSequentialAuths( " + loadFilename + " )", e.toString());
    }
    finally
    {
      try{it.close();}catch(Exception e){}
    }
  }

  //to allow for Exception handling below when generating
  //the new attachment for notifying merchants
  private void processFileLetter(StringBuffer text, String fileName)
  {

    BufferedReader            file            = null;
    String                    line            = null;
    try
    {
      file = new BufferedReader( new FileReader( fileName ) );
      while( ( line = file.readLine() ) != null )
      {
        text.append(line);
        text.append("\n");
      }
      file.close();
    }
    catch(Exception e)
    {
      //leave it blank
    }

  }

  private void notifyMerchants( String loadFilename )
  {
    ResultSetIterator         it              = null;
    String                    emailAddr       = null;
    int                       fromAddrId      = -1;
    ResultSet                 resultSet       = null;
    StringBuffer              letterBaseText      = new StringBuffer();
    StringBuffer              letterBaseTextAmex  = new StringBuffer();
    String                    letterFilename      = null;
    String                    letterFilenameAmex  = null;
    String                    oldLetterFilename   = null;
    StringBuffer              messageText     = new StringBuffer();
    MailMessage               msg             = null;
    String                    subject         = null;

    boolean                   isCB            = false;
    boolean                   isAmex          = false;
    boolean                   isOptBlue       = false;

    try
    {
      log.debug("loading..."+loadFilename);
      for( int i = 0; i < 4; ++i )
      {
        if ( i == 0 )   // retrievals
        {
          letterFilename      = "letters"+ File.separator + "3941_retr_letter.txt";
          letterFilenameAmex  = "letters"+ File.separator + "3941_retr_letter_amex.txt";
          fromAddrId      = MesEmails.MSG_ADDRS_RETR_REQ_AUTO_NOTIFY;
          subject         = "Urgent - Pending Retrieval Request";

          /*@lineinfo:generated-code*//*@lineinfo:4277^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  rt.retr_load_sec            as control_number,
//                      nvl(rta.case_number,'NA')   as case_number,
//                      rt.merchant_number          as merchant_number,
//                      rt.reference_number         as ref_num,
//                      rt.card_number              as card_number,
//                      rt.card_type                as card_type,
//                      to_char(rt.incoming_date,'mm/dd/yyyy')      as incoming_date,
//                      ltrim(to_char(rt.tran_amount,'999,999.99')) as tran_amount,
//                      to_char(rt.tran_date,'mm/dd/yyyy')          as tran_date,
//                      to_char(rt.incoming_date+14,'mm/dd/yyyy')   as due_date,
//                      nvl(u.email,mr.merch_email_address)         as email,
//                      nvl(rta.reason_code,' ' )                   as reason_code,
//                      nvl(rd.reason_desc,' ')                     as reason_desc,
//                      0                                           as assoc_node,
//                      ltrim(to_char(rta.transaction_amount,'999,999.99')) as amex_tran_amount,
//                      nvl(rta.charge_ref_num, ' ')                as roc_number,
//                      nvl(rta.cardholder_name, ' ')               as cardmember_name
//              from    network_retrievals                rt,
//                      network_retrieval_amex            rta,
//                      (
//                        select  distinct
//                                u.hierarchy_node,
//                                u.email
//                        from    users   u
//                        where   instr(u.email,'@') > 0
//                      )                                 u,
//                      merchant                          mr,
//                      chargeback_reason_desc            rd
//              where   rt.load_filename = :loadFilename and
//                      rt.bank_number in (select bank_number from mbs_banks where trait_02 = 3941) and
//                      exists
//                      (
//                        select  o.org_group
//                        from    network_retrievals_auto_notify  anot,
//                                organization                    o,
//                                group_merchant                  gm
//                        where   o.org_group = anot.hierarchy_node and
//                                gm.org_num = o.org_num and
//                                gm.merchant_number = rt.merchant_number and
//                                rt.incoming_date between anot.valid_date_begin and
//                                                         anot.valid_date_end
//                      ) and
//                      mr.merch_number(+) = rt.merchant_number and
//                      u.hierarchy_node(+) = rt.merchant_number and
//                      rta.load_sec(+) = rt.retr_load_sec and
//                      'AM' = rd.card_type(+) and
//                      rta.reason_code = rd.reason_code(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rt.retr_load_sec            as control_number,\n                    nvl(rta.case_number,'NA')   as case_number,\n                    rt.merchant_number          as merchant_number,\n                    rt.reference_number         as ref_num,\n                    rt.card_number              as card_number,\n                    rt.card_type                as card_type,\n                    to_char(rt.incoming_date,'mm/dd/yyyy')      as incoming_date,\n                    ltrim(to_char(rt.tran_amount,'999,999.99')) as tran_amount,\n                    to_char(rt.tran_date,'mm/dd/yyyy')          as tran_date,\n                    to_char(rt.incoming_date+14,'mm/dd/yyyy')   as due_date,\n                    nvl(u.email,mr.merch_email_address)         as email,\n                    nvl(rta.reason_code,' ' )                   as reason_code,\n                    nvl(rd.reason_desc,' ')                     as reason_desc,\n                    0                                           as assoc_node,\n                    ltrim(to_char(rta.transaction_amount,'999,999.99')) as amex_tran_amount,\n                    nvl(rta.charge_ref_num, ' ')                as roc_number,\n                    nvl(rta.cardholder_name, ' ')               as cardmember_name\n            from    network_retrievals                rt,\n                    network_retrieval_amex            rta,\n                    (\n                      select  distinct\n                              u.hierarchy_node,\n                              u.email\n                      from    users   u\n                      where   instr(u.email,'@') > 0\n                    )                                 u,\n                    merchant                          mr,\n                    chargeback_reason_desc            rd\n            where   rt.load_filename =  :1   and\n                    rt.bank_number in (select bank_number from mbs_banks where trait_02 = 3941) and\n                    exists\n                    (\n                      select  o.org_group\n                      from    network_retrievals_auto_notify  anot,\n                              organization                    o,\n                              group_merchant                  gm\n                      where   o.org_group = anot.hierarchy_node and\n                              gm.org_num = o.org_num and\n                              gm.merchant_number = rt.merchant_number and\n                              rt.incoming_date between anot.valid_date_begin and\n                                                       anot.valid_date_end\n                    ) and\n                    mr.merch_number(+) = rt.merchant_number and\n                    u.hierarchy_node(+) = rt.merchant_number and\n                    rta.load_sec(+) = rt.retr_load_sec and\n                    'AM' = rd.card_type(+) and\n                    rta.reason_code = rd.reason_code(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"124com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"124com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4326^11*/
        }
        else if ( i == 1 )    // chargebacks
        {
          isCB = true;
          oldLetterFilename  = "letters"+ File.separator + "3941_cb_letter_old.txt";
          letterFilename  = "letters"+ File.separator + "3941_cb_letter.txt";
          letterFilenameAmex  = "letters"+ File.separator + "3941_cb_letter_amex.txt";
          fromAddrId      = MesEmails.MSG_ADDRS_CB_AUTO_NOTIFY;
          subject         = "Urgent - Pending Chargeback";

          /*@lineinfo:generated-code*//*@lineinfo:4337^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.cb_load_sec                              as control_number,
//                      cb.merchant_number                          as merchant_number,
//                      cb.reference_number                         as ref_num,
//                      cb.cb_ref_num                               as cb_ref_num,
//                      cb.card_number                              as card_number,
//                      cb.reason_code                              as reason_code,
//                      nvl(cb.first_time_chargeback,'N')           as first_time,
//                      rd.reason_desc                              as reason_desc,
//                      nvl(rd.uta,'N')                             as uta,
//                      to_char(cb.incoming_date,'mm/dd/yyyy')      as incoming_date,
//                      ltrim(to_char(cb.tran_amount,'999,999.99')) as tran_amount,
//                      to_char(cb.tran_date,'mm/dd/yyyy')          as tran_date,
//                      to_char((cb.incoming_date+25),'mm/dd/yyyy') as due_date,
//                      nvl(u.email,mr.merch_email_address)         as email,
//                      mif.association_node                        as assoc_node,
//                      cb.card_type                                as card_type,
//                      'NA'                                        as case_number,
//                      ltrim(to_char(cba.transaction_amount,'999,999.99')) as amex_tran_amount,
//                      nvl(cba.cardholder_name, ' ')               as cardmember_name,
//                      nvl(cb.dt_purchase_id, ' ')                 as roc_number
//              from    network_chargebacks               cb,
//                      network_chargeback_amex          cba,
//                      chargeback_reason_desc            rd,
//                      (
//                        select  distinct
//                                u.hierarchy_node,
//                                u.email
//                        from    users   u
//                        where   instr(u.email,'@') > 0
//                      )                                 u,
//                      merchant                          mr,
//                      mif                               mif
//              where   cb.load_filename = :loadFilename and
//                      cb.bank_number in (select bank_number from mbs_banks where trait_02 = 3941) and
//                      exists
//                      (
//                        select  o.org_group
//                        from    network_chargeback_auto_notify  anot,
//                                organization                    o,
//                                group_merchant                  gm
//                        where   o.org_group = anot.hierarchy_node and
//                                gm.org_num = o.org_num and
//                                gm.merchant_number = cb.merchant_number and
//                                cb.incoming_date between anot.valid_date_begin and
//                                                         anot.valid_date_end
//                      ) and
//                      rd.card_type = cb.card_type and
//                      rd.reason_code = cb.reason_code and
//                      mr.merch_number(+) = cb.merchant_number and
//                      u.hierarchy_node(+) = cb.merchant_number and
//                      nvl(cb.first_time_chargeback,'N') = 'Y' and
//                      mif.merchant_number(+) = cb.merchant_number and
//                      cb.cb_load_sec = cba.load_sec(+)
//  
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.cb_load_sec                              as control_number,\n                    cb.merchant_number                          as merchant_number,\n                    cb.reference_number                         as ref_num,\n                    cb.cb_ref_num                               as cb_ref_num,\n                    cb.card_number                              as card_number,\n                    cb.reason_code                              as reason_code,\n                    nvl(cb.first_time_chargeback,'N')           as first_time,\n                    rd.reason_desc                              as reason_desc,\n                    nvl(rd.uta,'N')                             as uta,\n                    to_char(cb.incoming_date,'mm/dd/yyyy')      as incoming_date,\n                    ltrim(to_char(cb.tran_amount,'999,999.99')) as tran_amount,\n                    to_char(cb.tran_date,'mm/dd/yyyy')          as tran_date,\n                    to_char((cb.incoming_date+25),'mm/dd/yyyy') as due_date,\n                    nvl(u.email,mr.merch_email_address)         as email,\n                    mif.association_node                        as assoc_node,\n                    cb.card_type                                as card_type,\n                    'NA'                                        as case_number,\n                    ltrim(to_char(cba.transaction_amount,'999,999.99')) as amex_tran_amount,\n                    nvl(cba.cardholder_name, ' ')               as cardmember_name,\n                    nvl(cb.dt_purchase_id, ' ')                 as roc_number\n            from    network_chargebacks               cb,\n                    network_chargeback_amex          cba,\n                    chargeback_reason_desc            rd,\n                    (\n                      select  distinct\n                              u.hierarchy_node,\n                              u.email\n                      from    users   u\n                      where   instr(u.email,'@') > 0\n                    )                                 u,\n                    merchant                          mr,\n                    mif                               mif\n            where   cb.load_filename =  :1   and\n                    cb.bank_number in (select bank_number from mbs_banks where trait_02 = 3941) and\n                    exists\n                    (\n                      select  o.org_group\n                      from    network_chargeback_auto_notify  anot,\n                              organization                    o,\n                              group_merchant                  gm\n                      where   o.org_group = anot.hierarchy_node and\n                              gm.org_num = o.org_num and\n                              gm.merchant_number = cb.merchant_number and\n                              cb.incoming_date between anot.valid_date_begin and\n                                                       anot.valid_date_end\n                    ) and\n                    rd.card_type = cb.card_type and\n                    rd.reason_code = cb.reason_code and\n                    mr.merch_number(+) = cb.merchant_number and\n                    u.hierarchy_node(+) = cb.merchant_number and\n                    nvl(cb.first_time_chargeback,'N') = 'Y' and\n                    mif.merchant_number(+) = cb.merchant_number and\n                    cb.cb_load_sec = cba.load_sec(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"125com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"125com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4393^11*/
        }
        else if ( i == 2 )    // Amex OptBlue chargebacks
        {
          isCB = true;
          isOptBlue = true;
          oldLetterFilename  = "letters"+ File.separator + "3941_cb_letter_old.txt";
          letterFilename  = "letters"+ File.separator + "3941_cb_letter.txt";
          letterFilenameAmex  = "letters"+ File.separator + "3941_cb_letter_amex_optblue.txt";
          fromAddrId      = MesEmails.MSG_ADDRS_CB_AUTO_NOTIFY;
          subject         = "Urgent - Pending Chargeback";

          /*@lineinfo:generated-code*//*@lineinfo:4405^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  cb.cb_load_sec                              as control_number,
//                      cb.merchant_number                          as merchant_number,
//                      cb.reference_number                         as ref_num,
//                      cb.cb_ref_num                               as cb_ref_num,
//                      cb.card_number                              as card_number,
//                      cb.reason_code                              as reason_code,
//                      nvl(cb.first_time_chargeback,'N')           as first_time,
//                      rd.reason_desc                              as reason_desc,
//                      nvl(rd.uta,'N')                             as uta,
//                      to_char(cb.incoming_date,'mm/dd/yyyy')      as incoming_date,
//                      ltrim(to_char(cb.tran_amount,'999,999.99')) as tran_amount,
//                      to_char(cb.tran_date,'mm/dd/yyyy')          as tran_date,
//                      to_char((cb.incoming_date+25),'mm/dd/yyyy') as due_date,
//                      nvl(u.email,mr.merch_email_address)         as email,
//                      mif.association_node                        as assoc_node,
//                      cb.card_type                                as card_type,
//                      'NA'                                        as case_number,
//                      ltrim(to_char(cba.billed_amount,'999,999.99')) as amex_tran_amount,
//                      nvl(cba.cm_name1, ' ')                      as cardmember_name,
//                      nvl(cb.dt_purchase_id, ' ')                 as roc_number
//              from    network_chargebacks               cb,
//                      network_chargeback_amex_optb     cba,
//                      chargeback_reason_desc            rd,
//                      (
//                        select  distinct
//                                u.hierarchy_node,
//                                u.email
//                        from    users   u
//                        where   instr(u.email,'@') > 0
//                      )                                 u,
//                      merchant                          mr,
//                      mif                               mif
//              where   cb.load_filename = :loadFilename and
//                      cb.bank_number in (select bank_number from mbs_banks where trait_02 = 3941) and
//                      exists
//                      (
//                        select  o.org_group
//                        from    network_chargeback_auto_notify  anot,
//                                organization                    o,
//                                group_merchant                  gm
//                        where   o.org_group = anot.hierarchy_node and
//                                gm.org_num = o.org_num and
//                                gm.merchant_number = cb.merchant_number and
//                                cb.incoming_date between anot.valid_date_begin and
//                                                         anot.valid_date_end
//                      ) and
//                      rd.card_type = cb.card_type and
//                      rd.reason_code = cb.reason_code and
//                      mr.merch_number(+) = cb.merchant_number and
//                      u.hierarchy_node(+) = cb.merchant_number and
//                      nvl(cb.first_time_chargeback,'N') = 'Y' and
//                      mif.merchant_number(+) = cb.merchant_number and
//                      cb.cb_load_sec = cba.load_sec
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  cb.cb_load_sec                              as control_number,\n                    cb.merchant_number                          as merchant_number,\n                    cb.reference_number                         as ref_num,\n                    cb.cb_ref_num                               as cb_ref_num,\n                    cb.card_number                              as card_number,\n                    cb.reason_code                              as reason_code,\n                    nvl(cb.first_time_chargeback,'N')           as first_time,\n                    rd.reason_desc                              as reason_desc,\n                    nvl(rd.uta,'N')                             as uta,\n                    to_char(cb.incoming_date,'mm/dd/yyyy')      as incoming_date,\n                    ltrim(to_char(cb.tran_amount,'999,999.99')) as tran_amount,\n                    to_char(cb.tran_date,'mm/dd/yyyy')          as tran_date,\n                    to_char((cb.incoming_date+25),'mm/dd/yyyy') as due_date,\n                    nvl(u.email,mr.merch_email_address)         as email,\n                    mif.association_node                        as assoc_node,\n                    cb.card_type                                as card_type,\n                    'NA'                                        as case_number,\n                    ltrim(to_char(cba.billed_amount,'999,999.99')) as amex_tran_amount,\n                    nvl(cba.cm_name1, ' ')                      as cardmember_name,\n                    nvl(cb.dt_purchase_id, ' ')                 as roc_number\n            from    network_chargebacks               cb,\n                    network_chargeback_amex_optb     cba,\n                    chargeback_reason_desc            rd,\n                    (\n                      select  distinct\n                              u.hierarchy_node,\n                              u.email\n                      from    users   u\n                      where   instr(u.email,'@') > 0\n                    )                                 u,\n                    merchant                          mr,\n                    mif                               mif\n            where   cb.load_filename =  :1   and\n                    cb.bank_number in (select bank_number from mbs_banks where trait_02 = 3941) and\n                    exists\n                    (\n                      select  o.org_group\n                      from    network_chargeback_auto_notify  anot,\n                              organization                    o,\n                              group_merchant                  gm\n                      where   o.org_group = anot.hierarchy_node and\n                              gm.org_num = o.org_num and\n                              gm.merchant_number = cb.merchant_number and\n                              cb.incoming_date between anot.valid_date_begin and\n                                                       anot.valid_date_end\n                    ) and\n                    rd.card_type = cb.card_type and\n                    rd.reason_code = cb.reason_code and\n                    mr.merch_number(+) = cb.merchant_number and\n                    u.hierarchy_node(+) = cb.merchant_number and\n                    nvl(cb.first_time_chargeback,'N') = 'Y' and\n                    mif.merchant_number(+) = cb.merchant_number and\n                    cb.cb_load_sec = cba.load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"126com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"126com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4460^11*/
        }
        else if ( i == 3 )    // Amex OptBlue retrievals
        {
          isCB                = false;
          isOptBlue           = true;
          letterFilename      = "letters"+ File.separator + "3941_retr_letter.txt";
          letterFilenameAmex  = "letters"+ File.separator + "3941_retr_letter_amex_optblue.txt";
          fromAddrId      = MesEmails.MSG_ADDRS_RETR_REQ_AUTO_NOTIFY;
          subject         = "Urgent - Pending Retrieval Request";

          /*@lineinfo:generated-code*//*@lineinfo:4471^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  rt.retr_load_sec                                 as control_number,
//                      nvl(rta.inquiry_case_number,'NA')                as case_number,
//                      rt.merchant_number                               as merchant_number,
//                      rt.reference_number                              as ref_num,
//                      rt.card_number                                   as card_number,
//                      rt.card_type                                     as card_type,
//                      to_char(rt.incoming_date,'mm/dd/yyyy')           as incoming_date,
//                      ltrim(to_char(rt.tran_amount,'999,999.99'))      as tran_amount,
//                      to_char(rt.tran_date,'mm/dd/yyyy')               as tran_date,
//                      to_char(rt.incoming_date+14,'mm/dd/yyyy')        as due_date,
//                      nvl(u.email,mr.merch_email_address)              as email,
//                      nvl(rta.inquiry_reason_code,' ' )                as reason_code,
//                      nvl(rd.reason_desc,' ')                          as reason_desc,
//                      0                                                as assoc_node,
//                      ltrim(to_char(rta.charge_amount,'999,999.99'))   as amex_tran_amount,
//                      nvl(rta.reference_number, ' ')                   as roc_number,
//                      nvl(rta.cardmember_name_1, ' ')                  as cardmember_name
//              from    network_retrievals                rt,
//                      network_retrieval_amex_optb       rta,
//                      (
//                        select  distinct
//                                u.hierarchy_node,
//                                u.email
//                        from    users   u
//                        where   instr(u.email,'@') > 0
//                      )                                 u,
//                      merchant                          mr,
//                      chargeback_reason_desc            rd
//              where   rt.load_filename = :loadFilename and
//                      rt.bank_number in (select bank_number from mbs_banks where trait_02 = 3941) and
//                      exists
//                      (
//                        select  o.org_group
//                        from    network_retrievals_auto_notify  anot,
//                                organization                    o,
//                                group_merchant                  gm
//                        where   o.org_group = anot.hierarchy_node and
//                                gm.org_num = o.org_num and
//                                gm.merchant_number = rt.merchant_number and
//                                rt.incoming_date between anot.valid_date_begin and
//                                                         anot.valid_date_end
//                      ) and
//                      mr.merch_number(+) = rt.merchant_number and
//                      u.hierarchy_node(+) = rt.merchant_number and
//                      rta.load_sec = rt.retr_load_sec and
//                      'AM' = rd.card_type(+) and
//                      rta.inquiry_reason_code = rd.reason_code(+)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  rt.retr_load_sec                                 as control_number,\n                    nvl(rta.inquiry_case_number,'NA')                as case_number,\n                    rt.merchant_number                               as merchant_number,\n                    rt.reference_number                              as ref_num,\n                    rt.card_number                                   as card_number,\n                    rt.card_type                                     as card_type,\n                    to_char(rt.incoming_date,'mm/dd/yyyy')           as incoming_date,\n                    ltrim(to_char(rt.tran_amount,'999,999.99'))      as tran_amount,\n                    to_char(rt.tran_date,'mm/dd/yyyy')               as tran_date,\n                    to_char(rt.incoming_date+14,'mm/dd/yyyy')        as due_date,\n                    nvl(u.email,mr.merch_email_address)              as email,\n                    nvl(rta.inquiry_reason_code,' ' )                as reason_code,\n                    nvl(rd.reason_desc,' ')                          as reason_desc,\n                    0                                                as assoc_node,\n                    ltrim(to_char(rta.charge_amount,'999,999.99'))   as amex_tran_amount,\n                    nvl(rta.reference_number, ' ')                   as roc_number,\n                    nvl(rta.cardmember_name_1, ' ')                  as cardmember_name\n            from    network_retrievals                rt,\n                    network_retrieval_amex_optb       rta,\n                    (\n                      select  distinct\n                              u.hierarchy_node,\n                              u.email\n                      from    users   u\n                      where   instr(u.email,'@') > 0\n                    )                                 u,\n                    merchant                          mr,\n                    chargeback_reason_desc            rd\n            where   rt.load_filename =  :1   and\n                    rt.bank_number in (select bank_number from mbs_banks where trait_02 = 3941) and\n                    exists\n                    (\n                      select  o.org_group\n                      from    network_retrievals_auto_notify  anot,\n                              organization                    o,\n                              group_merchant                  gm\n                      where   o.org_group = anot.hierarchy_node and\n                              gm.org_num = o.org_num and\n                              gm.merchant_number = rt.merchant_number and\n                              rt.incoming_date between anot.valid_date_begin and\n                                                       anot.valid_date_end\n                    ) and\n                    mr.merch_number(+) = rt.merchant_number and\n                    u.hierarchy_node(+) = rt.merchant_number and\n                    rta.load_sec = rt.retr_load_sec and\n                    'AM' = rd.card_type(+) and\n                    rta.inquiry_reason_code = rd.reason_code(+)";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"127com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"127com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4520^11*/
        }

        resultSet = it.getResultSet();

        //objects needed to build reason code-specific letter
        //for CBs only - but define here as MailMessage need access
        String aFileName        = "";
        byte[] aFileBody        = null;
        StringBuffer subjectSB  = new StringBuffer();
        StringBuffer errorSB    = new StringBuffer();
        long cbid               = 0L;
        long mid                = 0L;
        /*
        //DEV - local storage of file
        letterFilenameAmex = "c:"+File.separator+
                            "mesweb"+File.separator+
                            "docs"+File.separator+
                            letterFilenameAmex;
        */
        while( resultSet.next() )
        {
          // setup the message body text
          messageText.setLength(0);
          subjectSB.setLength(0);

          cbid  = resultSet.getLong("control_number");
          mid   = resultSet.getLong("merchant_number");

          if("AM".equals(resultSet.getString("card_type")))
          {
            log.debug("in Amex...");
            isAmex = true;

            // load letter text for Amex retr
            letterBaseTextAmex.setLength(0);

            log.debug("letterFilenameAmex = "+letterFilenameAmex);

            processFileLetter(letterBaseTextAmex, letterFilenameAmex);

            messageText.append(letterBaseTextAmex.toString());

            if(isCB)
            {
              subjectSB.append("CHARGEBACK NOTIFICATION - AMERICAN EXPRESS" + (isOptBlue ? " OPTBLUE" : ""));
            }
            else
            {
              subjectSB.append("URGENT TRANSACTION INQUIRY - AMERICAN EXPRESS" + (isOptBlue ? " OPTBLUE" : ""));
              fromAddrId = MesEmails.MSG_ADDRS_CB_AUTO_NOTIFY;
            }
          }
          else
          {
            // load the necessary letter text
            letterBaseText.setLength(0);
            processFileLetter(letterBaseText, letterFilename);
            messageText.append(letterBaseText.toString());

          }

          //only build PDF for non-Amex CBs
          if(isCB && !isAmex)
          {
            log.debug("adding attachment...");

            ReasonCodeDocBuilder b;
            Director d;
            boolean utaFlag = "Y".equals(resultSet.getString("uta"));
            String rc       = resultSet.getString("reason_code");
            int scope       = ReasonCodeDocBuilder.SCOPE_CB;
            String fillText = " ";

            if(utaFlag)
            {
              scope    = ReasonCodeDocBuilder.SCOPE_UTA;
              fillText = "You were recently sent a retrieval request and did not respond within the required time.  This has resulted in an automatic chargeback and all representment rights have been lost.  Nothing further can be done through the chargeback system.";
            }

            try
            {
              //build the reason code letter for attachment
              aFileName=  "cbLetter_"+ rc +".pdf";

              //add this number to the subject line for tracking
              subjectSB.append(subject).append(" ref: ").append(cbid);

              b = new ReasonCodeDocBuilder(cbid,scope);
              b.setFillText(fillText);
              d = new Director(b);
              d.construct();
              aFileBody = (byte[])b.getResult();
            }
            catch(Exception cb)
            {
              //leave the attachment off (null)
              //revert to old letter in body of email
              messageText.setLength(0);
              processFileLetter(messageText, oldLetterFilename);
            }
          }
          else
          {
            log.debug("NOT attaching PDF...");
          }

          if(subjectSB.length()==0)
          {
            subjectSB.append(subject);
          }

          // replace the keywords (if any) in the message text
          replaceKeywords(messageText,resultSet);

          emailAddr   = resultSet.getString("email");

          if ( emailAddr == null || emailAddr.indexOf("@") < 0 )
          {
            errorSB.append("\t").append(cbid).append(" (MID: ").append(mid);
            errorSB.append(")  email = ").append(emailAddr).append("\n");
          }
          else
          {

            msg = new MailMessage();
            msg.setAddresses(fromAddrId);
            msg.setSubject(subjectSB.toString());
            msg.setText( messageText.toString() );
            msg.addTo( emailAddr );

            //10/10 added this quick fix to append CC email address
            //for mids in this assoc node
            long assoc_node = resultSet.getLong("assoc_node");
            if(3941816031L == assoc_node)
            {
              msg.addCC("chargeback@paymate.com");
            }

            if( aFileBody != null )
            {
              msg.addFile(aFileName,aFileBody);
            }

            //@ - Debug address and subject
  //@          msg.addTo("jduncan@merchante-solutions.com");   //@
  //@          msg.setSubject(subject + " (" + emailAddr + ")");//@

            msg.send();

            log.debug("emailAddr = "+emailAddr);
            log.debug("subject = "+subject);
            log.debug(messageText.toString());

            aFileBody = null;
          }
        }

        resultSet.close();
        it.close();

        if(errorSB.length()>0)
        {
          errorSB.insert(0,"The following control numbers have an invalid or null email address:\n\n");
          msg = new MailMessage();
          msg.setAddresses(fromAddrId);
          msg.setSubject("Error messages from CB Auto-Notification Process");
          msg.setText( errorSB.toString() );
          msg.addTo( "retrievals@merchante-solutions.com" );
          msg.send();
        }
      }
    }
    catch( Exception e )
    {
      //e.printStackTrace();
      logEntry("notifyMerchants(" + loadFilename + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e){}
    }
  }

  protected void recordTimestampBegin( long recId )
  {
    Timestamp     beginTime   = null;

    try
    {
      beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());

      /*@lineinfo:generated-code*//*@lineinfo:4712^7*/

//  ************************************************************
//  #sql [Ctx] { update  risk_process
//          set     process_begin_date = :beginTime
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  risk_process\n        set     process_begin_date =  :1  \n        where   rec_id =  :2 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"128com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,beginTime);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4717^7*/
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "recordTimestampBegin()", e.toString());
      setError("recordTimestampBegin(): " + e.toString());
      logEntry("recordTimestampBegin()", e.toString());
    }
  }

  protected void recordTimestampEnd( long recId )
  {
    Timestamp     beginTime   = null;
    long          elapsed     = 0L;
    Timestamp     endTime     = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:4735^7*/

//  ************************************************************
//  #sql [Ctx] { select  process_begin_date 
//          from    risk_process
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  process_begin_date  \n        from    risk_process\n        where   rec_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"129com.mes.startup.RiskScoringLoader",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4740^7*/
      endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      elapsed = (endTime.getTime() - beginTime.getTime());

      /*@lineinfo:generated-code*//*@lineinfo:4744^7*/

//  ************************************************************
//  #sql [Ctx] { update  risk_process
//          set     process_end_date = :endTime,
//                  process_elapsed = :DateTimeFormatter.getFormattedTimestamp(elapsed)
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4469 = DateTimeFormatter.getFormattedTimestamp(elapsed);
   String theSqlTS = "update  risk_process\n        set     process_end_date =  :1  ,\n                process_elapsed =  :2  \n        where   rec_id =  :3 ";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"130com.mes.startup.RiskScoringLoader",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,endTime);
   __sJT_st.setString(2,__sJT_4469);
   __sJT_st.setLong(3,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4750^7*/
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "recordTimestampEnd()", e.toString());
      setError("recordTimestampEnd(): " + e.toString());
      logEntry("recordTimestampEnd()", e.toString());
    }
  }

  protected void replaceKeywords( StringBuffer base, ResultSet data )
    throws java.sql.SQLException
  {
    ResultSetIterator   it            = null;
    String              columnName    = null;
    int                 offsetBegin   = 0;
    int                 offsetEnd     = 0;
    String              keyword       = null;
    ResultSet           resultSet     = null;
    String              temp          = null;

    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:4773^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  field_name, column_name
//          from    network_cb_retr_letter_fields
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  field_name, column_name\n        from    network_cb_retr_letter_fields";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"131com.mes.startup.RiskScoringLoader",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"131com.mes.startup.RiskScoringLoader",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:4777^7*/
      resultSet = it.getResultSet();

      while(resultSet.next() )
      {
        keyword     = resultSet.getString("field_name");
        columnName  = resultSet.getString("column_name");

        while ( ( offsetBegin = base.toString().indexOf(keyword) ) != -1 )
        {
          temp      = data.getString(columnName);
          offsetEnd = (offsetBegin + keyword.length());
          base.replace(offsetBegin,offsetEnd,((temp == null) ? "" : temp));
        }
      }
      resultSet.close();
      it.close();
    }
    catch(Exception e)
    {
      logEntry("replaceKeywords()",e.toString());
    }
    finally
    {
      try{ it.close(); }catch(Exception e){}
    }
  }

  public static void main( String[] args )
  {
        try {
            if (args.length > 0 && args[0].equals("testproperties")) {
                EventBase.printKeyListStatus(new String[]{MesDefaults.ACH_CONFIRMATION_EMAIL_FROM, MesDefaults.ACH_CONFIRMATION_EMAIL_TO,
                        MesDefaults.DK_ACH_CUTOFF, MesDefaults.DK_AMEX_CLEARING_CUTOFF, MesDefaults.DK_DISCOVER_CLEARING_CUTOFF,
                        MesDefaults.DK_MC_GCMS_CUTOFF, MesDefaults.DK_MODBII_CLEARING_CUTOFF, MesDefaults.DK_VISA_BASE_II_CUTOFF});
            }
        } catch (Exception e) {
            System.out.println("main(): " + e.toString());
        }

    RiskScoringLoader   loader = null;

    SQLJConnectionBase.initStandalone("DEBUG");
    try
    {
      loader = new RiskScoringLoader();
      loader.connect();

      if ( args[0].equals("notifyMerchants") )
      {
        loader.notifyMerchants(args[1]);
      }
      else if ( args[0].equals("linkChargebacksToTransactions") )
      {
        loader.linkChargebacksToTransactions(args[1]);
      }
      else if ( args[0].equals("linkRetrievalsToTransactions") )
      {
        loader.linkRetrievalsToTransactions(args[1]);
      }
      else if ( args[0].equals("linkCreditsToChargebacks") )
      {
        loader.linkCreditsToChargebacks(args[1]);
      }
      else if ( args[0].equals("linkCreditsToRetrievals") )
      {
        loader.linkCreditsToRetrievals(args[1]);
      }
      else if ( args[0].equals("loadChargebackFile") )
      {
        loader.loadChargebackFile(args[1]);
      }
      else if ( args[0].equals("fixMissingAcctChargebacks") )
      {
        loader.fixMissingAcctChargebacks(args[1]);
      }
      else if ( args[0].equals("loadAuthRiskSummary") )
      {
        loader.loadAuthRiskSummary(args[1]);
      }
      else if ( args[0].equals("loadCaptureRiskSummary") )
      {
        loader.loadCaptureRiskSummary(args[1]);
      }
      else if ( args[0].equals("loadPointsAuth") )
      {
        loader.loadPointsAuth(args[1]);
      }
      else if ( args[0].equals("loadPointsCapture") )
      {
        loader.loadPointsCapture(args[1]);
      }
    }
    catch( Exception e )
    {
      System.out.println("main(): " + e.toString());
    }
    finally
    {
      try{ loader.cleanUp(); }catch( Exception e ){}
    }
  }
}/*@lineinfo:generated-code*/