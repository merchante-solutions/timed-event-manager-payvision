/*@lineinfo:filename=DocBuilder*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/DocBuilder.sqlj $

  Description:  Builder.java

  Generic Builder interface that allows for the use of a Director
  that controls the generation of a given Product

  //Copyright 2001 by Paulo Soares (iText)

  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 10/13/04 11:04a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import com.mes.database.SQLJConnectionBase;

public abstract class DocBuilder extends SQLJConnectionBase implements Builder
{
  public abstract Object getResult();

  /**
   * The following are all included to offer a default to
   * all method calls - the child does not have to implement
   * every method, only those that it needs. The calling Director
   * will know how to handle the given Builder
   */
  public void buildLogo()
  {
  }

  public void buildHeader()
  {
  }

  public void buildFooter()
  {
  }

  public void buildBody()
  {
  }
  
  //done solely to compile the sqlj
  private final void _compile()
  {
    int test;
    try
    {
      connect();
      /*@lineinfo:generated-code*//*@lineinfo:67^7*/

//  ************************************************************
//  #sql [Ctx] { select 1  from dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select 1   from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.DocBuilder",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   test = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:70^7*/
    }
    catch(Exception e)
    {
    }
    finally
    {
      try{ cleanUp(); }catch(Exception e1){}
    }
  }

}/*@lineinfo:generated-code*/