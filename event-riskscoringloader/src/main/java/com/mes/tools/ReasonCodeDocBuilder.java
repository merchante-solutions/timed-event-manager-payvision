/*@lineinfo:filename=ReasonCodeDocBuilder*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/ReasonCodeDocBuilder.sqlj $

  Description:  ReasonCodeDocBuilder.sqlj

  Designed to handle dynamic building of Reason Code Letters using PDF
  NOTE: this class uses the latest version of iText open source PDF generation
  itext-paulo-132.jar and is, therefore, under restrictions as provided by its
  authors, Bruno Lowagie and Paulo Soares. More information can be found at
  www.lowagie.com/iText/ and http://itextpdf.sourceforge.net/. Of course, PDF is
  a format created by Adobe... so all rights covered on that front too.

  //Copyright 2001 by Paulo Soares (iText)

  Last Modified By   : $Author: mjanbay $
  Last Modified Date : $Date: 2015-04-07 15:05:39 -0700 (Tue, 07 Apr 2015) $
  Version            : $Revision: 23548 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
// log4j
import org.apache.log4j.Logger;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPCellEvent;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.TextField;
import com.mes.constants.MesChargebacks;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;


public class ReasonCodeDocBuilder extends DocBuilder
{

  // create class log category
  static Logger log = Logger.getLogger(ReasonCodeDocBuilder.class);

  //inner class for data storage
  private ChargebackDocData data = null;

  //defines scope of letter (warning, chargeback or unable to assist)
  //IMPORTANT - corresponds to String[] at bottom of page
  public static final int SCOPE_CB     = 0;
  public static final int SCOPE_WARN   = 1;
  public static final int SCOPE_UTA    = 2;
  public static final int SCOPE_UTA_2  = 3;
  public static final int SCOPE_REPR   = 4;
  public static final int SCOPE_INFO   = 5;

  private int scope = SCOPE_WARN;

  //date
  private String dateString;
  private String endDateString;
  public static int DAYS_UNTIL_OVERDUE = 10;

  //PDF objects for doc generation
  private Document document = null;
  private PdfWriter writer = null;
  private ByteArrayOutputStream baos = null;

  //internal flag to remove headers, MES specific info
  private boolean buildSimple = false;

  //allows for a programmable fill for editable field
  private String fillText     = " ";

  //Fonts
  public static Font BIG_HEADER = new Font(Font.TIMES_ROMAN ,16f, Font.BOLD);
  public static Font HEADER_1 = new Font(Font.HELVETICA ,14f, Font.BOLD);
  public static Font HEADER_2 = new Font(Font.HELVETICA ,14f, Font.UNDERLINE);
  public static Font BODY_1 = new Font(Font.HELVETICA ,12f, Font.NORMAL);
  public static Font BODY_2 = new Font(Font.HELVETICA ,12f, Font.BOLD);
  public static Font BODY_3 = new Font(Font.HELVETICA ,10f, Font.NORMAL);
  public static Font BODY_3BOLD = new Font(Font.HELVETICA ,10f, Font.BOLD);
  public static Font BODY_4 = new Font(Font.HELVETICA ,10f, Font.ITALIC);
  public static Font FOOTER = new Font(Font.HELVETICA ,8f, Font.NORMAL);
  public static Font FOOTER_BOLD = new Font(Font.HELVETICA ,8f, Font.NORMAL);
  public static Font LINK_12F = new Font(Font.HELVETICA, 12f,Font.NORMAL, Color.BLUE);
  public static Color BKGROUND_GREY = new Color(235, 235, 235);
  public static Font BOLD_15F = new Font(Font.HELVETICA, 15f, Font.BOLD);


  public ReasonCodeDocBuilder(long cbId, int scope)
  {

    data = new ChargebackDocData(cbId);

    this.scope = scope;

    if(data.reasonCode.equals("4801") || data.reasonCode.equals("79"))
    {
      buildSimple = true;
    }

    init();
  }

  private void init()
  {

    document = new Document(PageSize.A4, 50, 50, 50, 25);
    baos = new ByteArrayOutputStream();
    try
    {
      writer = PdfWriter.getInstance(document, baos);
      //generate date strings
      Calendar cal = Calendar.getInstance();
      cal.setTime(data.incomingDate);
      DateFormat df = DateFormat.getDateInstance(DateFormat.LONG);
      dateString = df.format(cal.getTime());

      cal.add(Calendar.DAY_OF_MONTH, DAYS_UNTIL_OVERDUE);
      endDateString = df.format(cal.getTime());

      /*
      //no more footer - but leave this in case they want to reinstall
      Phrase phrase = new Phrase("Corporate Headquarters ",FOOTER_BOLD);
      phrase.add(new Phrase("3400 Bridge Parkway - Suite 100 - Redwood City - CA - 94065",FOOTER));
      HeaderFooter footer = new HeaderFooter(phrase, false);
      footer.setAlignment(Element.ALIGN_CENTER);
      document.setFooter(footer);
      */

      //open document
      document.open();

    }
    catch (Exception e)
    {
      log.debug("init Exception: "+ e.getMessage());
    }
  }

  public static boolean isChargebackLetterSupported( int bankNumber )
  {
    return( bankNumber == mesConstants.BANK_ID_MES ||
            bankNumber == mesConstants.BANK_ID_NBSC ||
            bankNumber == mesConstants.BANK_ID_CBT );
  }

  public Object getResult()
  {
    if(document.isOpen())
    {
      document.close();
    }

    if( baos != null )
    {
      return baos.toByteArray();
    }
    else
    {
      return null;
      //TODO
      //generate standard exception PDF
    }
  }

  public void buildHeader() 
  {
		try 
		{
			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(100);
			int[] widths = { 55, 45 };
			table.setWidths(widths);
            
            table.addCell(buildTitle());
            
			table.addCell(buildHeaderCell()); // MES address

			table.addCell(buildGreyBarSeparator(2));
			table.addCell(buildInsideAddressCell());
			
            if(scope==SCOPE_INFO)
            {
              //add space for 'Attn To:'
                PdfPCell pdfpcell = new PdfPCell();
                PdfPCellEvent evt = new PdfPCellEvent() {
                public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
                  {
                    PdfContentByte cb = canvases[PdfPTable.TEXTCANVAS];
                    try{
                      TextField app = new TextField(writer,position,"attn");
                      app.setFont(BODY_3.getBaseFont());
                      app.setFontSize(10f);
                      app.setBorderWidth(0);
                      app.setText("Attn To:");
                      //app.setOptions(app.MULTILINE | app.REQUIRED);
                      PdfFormField field = app.getTextField();
                      writer.addAnnotation(field);
                    }catch(Exception e){}
                  }
                };
            pdfpcell.setCellEvent(evt);
            pdfpcell.setMinimumHeight(15f);
            pdfpcell.setBorder(0);
            table.addCell(pdfpcell);
            }
        
			table.addCell(buildHeaderDateCell(writer));

			Phrase phrase = new Phrase("Fax: "+ formatFax(data.faxNumber), BODY_3);
			phrase.add(new Phrase("\n\n"));
			PdfPCell cell = new PdfPCell(phrase);
			cell.setBorder(0);
			table.addCell(cell);

			table.addCell(buildBlankCell());
			table.addCell(buildBlankCell());

			document.add(table);

		} catch (Exception e) {
			log.debug("buildHeader Exception: " + e.getMessage());
		}
	
	}
	
	private PdfPCell buildTitle() 
	{
		  Phrase phrase = new Phrase("\n\n"+TITLE[scope],HEADER_1);
		  if(data.isAmex())
		  {
			phrase.add(new Phrase("     American Express", HEADER_1));
		  }
		phrase.add(new Phrase("\n"));
		PdfPCell cell = new PdfPCell(phrase);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setVerticalAlignment(Element.ALIGN_CENTER);
		cell.setBackgroundColor(BKGROUND_GREY);
		cell.setBorder(0);
		return cell;	
	}
	
	private static PdfPCell buildHeaderCell()
	{
		Phrase phrase = new Phrase("");
		phrase.add(new Phrase("\nMerchant Services \n", BOLD_15F));
		phrase.add(new Phrase(MES_ADDRESS, BODY_3));
		phrase.add(new Phrase("\n\n"));
		PdfPCell cell = new PdfPCell(phrase);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBackgroundColor(BKGROUND_GREY);
		cell.setBorder(0);
		return cell;
	}
	
	private PdfPCell buildGreyBarSeparator(int i) 
	{
		PdfPCell cell = new PdfPCell();
		cell.setFixedHeight(6f);
		cell.setBackgroundColor(Color.LIGHT_GRAY);
		cell.setBorder(0);
		cell.setColspan(2);
		return cell;
	}
	
	private PdfPCell buildInsideAddressCell()
	{
		Phrase phrase = new Phrase("\n");
		phrase.add(new Phrase(data.getAddress(),BODY_3));
		PdfPCell cell = new PdfPCell(phrase);
		cell.setBorder(0);
		return cell;
	}
	
	private PdfPCell buildHeaderDateCell(final PdfWriter pdfWriter)
	{
	   Phrase phrase = new Phrase("\n");
		phrase.add(new Phrase(dateString,BODY_3));
		phrase.add(new Phrase("\n"));
		PdfPCell cell = new PdfPCell(phrase);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBorder(0);
		return cell;
	}
	
	private static String formatFax(String text)
	 {
		if (text == null) {
			return "";
		}
		if (text.length() != 10) {
			return text;
		}
		try {
			StringBuffer sb = new StringBuffer(14);
			sb.append("(");
			sb.append(text.substring(0, 3));
			sb.append(") ");
			sb.append(text.substring(3, 6));
			sb.append("-");
			sb.append(text.substring(6));
			return sb.toString();
		} catch (Exception e) {
			return text;
		}
	}
	
	private PdfPCell buildBlankCell() 
	{
		PdfPCell cell;
		cell = new PdfPCell(new Phrase("\n\n"));
		cell.setBorder(0);
		return cell;
	}
	

  public void buildBody()
  {

    switch(scope)
    {
      case SCOPE_UTA:

        buildUTABody();
        break;

      case SCOPE_UTA_2:

        buildUTA2Body();
        break;

      case SCOPE_REPR:

        buildReprBody();
        break;

      case SCOPE_INFO:

        buildInfoBody();
        break;

      default:
        if(buildSimple)
        {
          buildSimpleReasonBody();
        }
        else
        {
          buildReasonBody();
        }
    };

  }

  private void buildSimpleReasonBody()
  {
    PdfPTable table = new PdfPTable(1);
    table.setWidthPercentage(100);

    PdfPCell cell;
    Phrase phrase;

    try
    {

      phrase = new Phrase(INTRO,BODY_1);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);


      phrase = new Phrase("\n");
      phrase.add(new Phrase(LINE_1[scope],BODY_1));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase("\n");
      phrase.add(new Phrase(LINE_5,BODY_2));
      phrase.add(new Phrase(data.reasonCode+" "+data.reasonDesc,BODY_2));
      if(data.reasonDescDet!=null)
      {
        phrase.add(new Phrase(" - "+data.reasonDescDet,BODY_1));
      }
      if(data.modifier!=null)
      {
        phrase.add(new Phrase(" - "+data.modifier,BODY_1));
      }
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase("\n");
      phrase.add(new Phrase("Chargeback Details:",BODY_2));
      phrase.add(new Phrase("\n\n"));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      table.addCell(makeDetailCell());

      phrase = new Phrase("\n",BODY_1);
      phrase.add(new Phrase(LINE_9,BODY_1));
      phrase.add(new Phrase("\n\n"));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      document.add(table);

    }
    catch(Exception e)
    {
      log.debug("buildSimpleReasonBody Exception: "+ e.getMessage());
    }
  }

  private void buildReasonBody()
  {
    PdfPTable table = new PdfPTable(1);
    table.setWidthPercentage(100);

    PdfPCell cell;
    Phrase phrase;

    try
    {

      phrase = new Phrase(INTRO,BODY_1);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase("\n");
      phrase.add(new Phrase(LINE_1[scope],BODY_1));
      if(data.isAmex())
      {
        phrase.add(new Phrase(AMEX_LINE_2,BODY_1));
        phrase.add(new Phrase(AMEX_LINE_3,BODY_2));
        phrase.add(new Phrase(AMEX_LINE_4,BODY_1));
      }
      else
      {
        phrase.add(new Phrase(LINE_2[scope],BODY_1));
        //phrase.add(new Phrase(endDateString,BODY_2));
        phrase.add(new Phrase(LINE_3,BODY_1));
        phrase.add(new Phrase(LINE_4[scope],BODY_2));
      }
      phrase.add(new Phrase("\n\n"));
      phrase.add(new Phrase("IC Date: ",BODY_2));
      phrase.add(new Phrase(""+ data.incomingDate,BODY_1));
      phrase.add(new Phrase("\n"));
      phrase.add(new Phrase("Control Number: ",BODY_2));
      phrase.add(new Phrase(""+ data.cbId,BODY_1));
      phrase.add(new Phrase("\n\n"));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);
       phrase = new Phrase("Transaction Information",BODY_2);
       phrase.add("\n");
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      cell.setBackgroundColor(BKGROUND_GREY);
      table.addCell(cell);
      
      
      table.addCell(makeDetailCell());


      //We don't give AMEX the following info:
      if(!data.isAmex())
      {
        phrase = new Phrase(LINE_5,BODY_2);
       cell = new PdfPCell(phrase);
      cell.setBorder(0);
      cell.setBackgroundColor(BKGROUND_GREY);
      table.addCell(cell);
        phrase = new Phrase("\n");
        phrase.add(new Phrase(data.reasonCode+" "+data.reasonDesc,BODY_2));
        if(data.reasonDescDet!=null)
        {
          phrase.add(new Phrase(" - "+data.reasonDescDet,BODY_1));
        }
        if(data.modifier!=null)
        {
            phrase.add(new Phrase(" - "+data.modifier,BODY_1));
          }
        
         phrase.add(new Phrase("\n\n"));
         phrase.add(new Phrase("Documents Required: ",BODY_2));
         phrase.add(new Phrase(data.reasonRes));
         phrase.add(new Phrase("\n\n"));
        cell = new PdfPCell(phrase);
        cell.setBorder(0);
        table.addCell(cell);

        phrase = new Phrase(LINE_6,BODY_2);
        cell = new PdfPCell(phrase);
        cell.setBorder(0);
        cell.setBackgroundColor(BKGROUND_GREY);
         table.addCell(cell);
      }

      phrase = new Phrase("\n",BODY_1);
      phrase.add(new Phrase(LINE_7[scope],BODY_1));
      phrase.add(buildMesLinkPhrase());
      phrase.add(" or send by email to ");
      phrase.add(buildChargebackMailToPhrase());
      phrase.add(" with the case Control Number as document title and subject line ");
      phrase.add(new Phrase(LINE_9,BODY_1));
      phrase.add(buildChargebackMailToPhrase());
	  phrase.add(new Phrase("\n\n"));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      document.add(table);

    }
    catch(Exception e)
    {
      log.debug("buildReasonBody Exception: "+ e.getMessage());
    }
  }
  
  	protected static Phrase buildChargebackMailToPhrase() {
		Phrase phrase = new Phrase("", LINK_12F);
		Chunk anchor = new Chunk("chargebacks@merchante-solutions.com");
		anchor.setAnchor("mailto:Chargebacks@merchante-solutions.com");
		anchor.setUnderline(0.2f, -1);
		phrase.add(anchor);
		return phrase;
	}

	protected Phrase buildMesLinkPhrase() {
		Phrase phrase = new Phrase("", LINK_12F);
		Chunk chunk = new Chunk("www.merchante-solutions.com");
		chunk.setAnchor("https://www.merchante-solutions.com/");
		chunk.setUnderline(0.2f, -1);
		phrase.add(chunk);
		return phrase;
	}

  private void buildUTABody()
  {
    PdfPTable table = new PdfPTable(1);
    table.setWidthPercentage(100);

    PdfPCell cell;
    Phrase phrase;
    PdfPCellEvent evt;

    try
    {
      /* Handled in buildHeader()

      phrase = new Phrase("\n\nChargeback Adjustment Notice",HEADER_2);
      phrase.add(new Phrase("\n"));
      cell = new PdfPCell(phrase);
      cell.setHorizontalAlignment(Element.ALIGN_CENTER);
      cell.setBorder(0);
      table.addCell(cell);

      */

      phrase = new Phrase("\n",BODY_1);
      phrase.add(new Phrase("Dear Merchant:\n\n",BODY_1));
      //phrase.add(new Phrase("We have been attempting to resolve the dispute listed below with the Cardholder's bank. Regrettably, the dispute has not been remedied in your favor and we now consider this case closed.",BODY_1));
      phrase.add(new Phrase("We appreciate your business and have been attempting to resolve the dispute listed below with the Cardholder's bank. Regrettably, the dispute has not been remedied in your favor and we now must consider this case closed. (No additional action or documentation is needed from you.)",BODY_1));
      phrase.add(new Phrase("\n\n",BODY_1));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      //cell event here allows the user to add
      //whatever they want to the UTA letter
      phrase = new Phrase("", BODY_1);
      cell = new PdfPCell(phrase);
      evt = new PdfPCellEvent() {
              public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
              {
                PdfContentByte cb = canvases[PdfPTable.TEXTCANVAS];
                try{
                  TextField app = new TextField(writer,position,"textBody");
                  app.setBorderWidth(0);
                  /*
                  try
                  {
                    app.setFont(BaseFont.createFont(BaseFont.HELVETICA,BaseFont.CP1250,BaseFont.NOT_EMBEDDED));
                  }
                  catch(Exception e)
                  {//do nothing
                  }
                  */
                  app.setText(fillText);
                  //app.setAlignment(Element.ALIGN_CENTER);
                  //app.setOptions(app.MULTILINE | app.REQUIRED | app.DO_NOT_SCROLL);
                  app.setOptions(app.MULTILINE);
                  PdfFormField field = app.getTextField();
                  writer.addAnnotation(field);
                }catch(Exception e){}
              }
            };
      cell.setCellEvent(evt);
      cell.setMinimumHeight(50f);
      cell.setBorder(0);
      //cell.setUseAscender(false);
      table.addCell(cell);

      phrase = new Phrase("\n");
      //phrase.add(new Phrase("Your account will be debited for the amount listed below. Please feel free to pursue other avenues for reimbursement.",BODY_1));
      phrase.add(new Phrase("Unfortunately, this means that your account will be debited for the amount listed below. Please feel free to pursue other avenues for reimbursement from the cardholder.",BODY_1));
      phrase.add(new Phrase("\n\n"));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

/*
      phrase = new Phrase("\n");
      phrase.add(new Phrase(data.reasonCode+" "+data.reasonDesc,BODY_2));
      if(data.reasonDescDet!=null)
      {
        phrase.add(new Phrase(" - "+data.reasonDescDet,BODY_1));
      }
      phrase.add(new Phrase("\n\n",BODY_1));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);
*/
/*
      phrase = new Phrase("\n"BODY_1,);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);
*/
      table.addCell(makeDetailCell());

      phrase = new Phrase("\n");
      phrase.add(new Phrase("If you have any further questions, please call our Customer Service Department at 888-288-2692, and inform the Customer Service Representative that you are inquiring about a chargeback.",BODY_1));
      phrase.add(new Phrase("\n\n"));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase("\n\n");
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      document.add(table);

    }
    catch(Exception e)
    {
      log.debug("buildUTABody Exception: "+ e.getMessage());
    }
  }

  private void buildUTA2Body()
  {
    PdfPTable table = new PdfPTable(1);
    table.setWidthPercentage(100);

    PdfPCell cell;
    Phrase phrase;
    PdfPCellEvent evt;

    try
    {
      phrase = new Phrase(INTRO,BODY_1);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase("\n",BODY_1);
      phrase.add(new Phrase(LINE_1[scope],BODY_1));
      //phrase.add(new Phrase("We have been attempting to resolve the dispute listed below with the Cardholder's bank. Regrettably, the dispute has not been remedied in your favor and we now consider this case closed.",BODY_1));
      phrase.add(new Phrase(LINE_4[scope],BODY_1));
      phrase.add(new Phrase("\n\n",BODY_1));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase("\n");
      phrase.add(new Phrase(data.reasonCode+" "+data.reasonDesc,BODY_2));
      if(data.reasonDescDet!=null)
      {
        phrase.add(new Phrase(" - "+data.reasonDescDet,BODY_1));
      }
      phrase.add(new Phrase("\n\n",BODY_1));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

/*    //NOTHING TO BE DONE data?
      phrase = new Phrase("\n"BODY_1,);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);
*/
      table.addCell(makeDetailCell());

      phrase = new Phrase("\n");
      phrase.add(new Phrase(LINE_7[scope],BODY_1));
      phrase.add(new Phrase("\n\n"));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      document.add(table);

    }
    catch(Exception e)
    {
      log.debug("buildUTA2Body Exception: "+ e.getMessage());
    }
  }


  private void buildReprBody()
  {
    PdfPTable table = new PdfPTable(1);
    table.setWidthPercentage(100);

    PdfPCell cell;
    Phrase phrase;
    PdfPCellEvent evt;

    try
    {
      phrase = new Phrase(INTRO,BODY_1);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase("\n",BODY_1);
      phrase.add(new Phrase(LINE_1[scope],BODY_1));
      phrase.add(new Phrase("\n\n",BODY_1));
      phrase.add(new Phrase(LINE_2[scope],BODY_1));
      phrase.add(new Phrase("\n\n",BODY_1));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase("\n");
      phrase.add(new Phrase(data.reasonCode+" "+data.reasonDesc,BODY_2));
      if(data.reasonDescDet!=null)
      {
        phrase.add(new Phrase(" - "+data.reasonDescDet,BODY_1));
      }
      if(data.modifier!=null)
      {
        phrase.add(new Phrase(" - "+data.modifier,BODY_1));
      }
      
      phrase.add(new Phrase("\n\n",BODY_1));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      table.addCell(makeDetailCell());

      phrase = new Phrase("\n");
      phrase.add(new Phrase(LINE_7[scope],BODY_1));
      phrase.add(new Phrase("\n\n"));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      document.add(table);

    }
    catch(Exception e)
    {
      log.debug("buildReprBody Exception: "+ e.getMessage());
    }
  }


  private void buildInfoBody()
  {
    PdfPTable table = new PdfPTable(1);
    table.setWidthPercentage(100);

    PdfPCell cell;
    Phrase phrase;
    PdfPCellEvent evt;

    try
    {
      phrase = new Phrase(INTRO,BODY_1);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase("\n",BODY_1);
      phrase.add(new Phrase(LINE_1[scope],BODY_1));
      phrase.add(new Phrase(LINE_2[scope],BODY_1));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

    //add editable date to paragraph
      phrase = new Phrase("\n\n", BODY_1);
      cell = new PdfPCell(phrase);
      evt = new PdfPCellEvent() {
              public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
              {
                PdfContentByte cb = canvases[PdfPTable.TEXTCANVAS];
                try{
                  TextField app = new TextField(writer,position,"endDateString");
                  app.setFont(BODY_1.getBaseFont());
                  app.setFontSize(10f);
                  app.setBorderWidth(0);
                  app.setText(endDateString);
                  app.setOptions(app.REQUIRED);
                  app.setAlignment(Element.ALIGN_CENTER);
                  PdfFormField field = app.getTextField();
                  writer.addAnnotation(field);
                }catch(Exception e){}
              }
            };
      cell.setCellEvent(evt);
      //cell.setMinimumHeight(15f);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase("\n",BODY_1);
      phrase.add(new Phrase(LINE_4[scope],BODY_1));
      phrase.add(new Phrase("\n\n",BODY_1));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      //cell event here allows the user to add
      //whatever they want to the UTA letter
      phrase = new Phrase("", BODY_1);
      cell = new PdfPCell(phrase);
      evt = new PdfPCellEvent() {
              public void cellLayout(PdfPCell cell, Rectangle position, PdfContentByte[] canvases)
              {
                PdfContentByte cb = canvases[PdfPTable.TEXTCANVAS];
                try{
                  TextField app = new TextField(writer,position,"textBody");
                  app.setBorderWidth(0);
                  /*
                  try
                  {
                    app.setFont(BaseFont.createFont(BaseFont.HELVETICA,BaseFont.CP1250,BaseFont.NOT_EMBEDDED));
                  }
                  catch(Exception e)
                  {//do nothing
                  }
                  */
                  app.setText(" ");
                  //app.setAlignment(Element.ALIGN_CENTER);
                  //app.setOptions(app.MULTILINE | app.REQUIRED | app.DO_NOT_SCROLL);
                  app.setOptions(app.MULTILINE);
                  PdfFormField field = app.getTextField();
                  writer.addAnnotation(field);
                }catch(Exception e){}
              }
            };
      cell.setCellEvent(evt);
      cell.setMinimumHeight(50f);
      cell.setBorder(0);
      //cell.setUseAscender(false);
      table.addCell(cell);


      phrase = new Phrase("\n");
      phrase.add(new Phrase(data.reasonCode+" "+data.reasonDesc,BODY_2));
      if(data.reasonDescDet!=null)
      {
        phrase.add(new Phrase(" - "+data.reasonDescDet,BODY_1));
      }
      if(data.modifier!=null)
      {
        phrase.add(new Phrase(" - "+data.modifier,BODY_1));
      }
      phrase.add(new Phrase("\n\n",BODY_1));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      table.addCell(makeDetailCell());

      phrase = new Phrase("\n");
      phrase.add(new Phrase(LINE_7[scope],BODY_1));
      phrase.add(new Phrase("\n\n"));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase("\n\n");
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      document.add(table);

    }
    catch(Exception e)
    {
      log.debug("buildInfoBody Exception: "+ e.getMessage());
    }
  }

  private PdfPCell makeDetailCell()
  {

    PdfPCell cell;
    Phrase phrase;
    PdfPTable table = new PdfPTable(5);

    try{

      int[] widths = {23,27,1,22,27};
      table.setWidths(widths);
      
      //LINE 1 (5 cells)
      phrase = new Phrase("\n\n");
      phrase = new Phrase("Merchant Number:",BODY_3BOLD);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase(data.merchantNumber,BODY_3);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase("",BODY_3);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase("Card Number:",BODY_3BOLD);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase(data.cardNumber,BODY_3);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);
      
      //LINE 2 (5 cells)
      
      phrase = new Phrase("Transaction Date:",BODY_3BOLD);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase(data.tranDateStr,BODY_3);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);
      
      phrase = new Phrase("",BODY_3);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);
      
       phrase = new Phrase("ARN#:",BODY_3BOLD);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase(data.refNumber,BODY_3);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);
      
      //Line3

      phrase = new Phrase("Transaction Amount:",BODY_3BOLD);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase(data.getTranAmountStr(),BODY_3);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase("",BODY_3);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);
      
       phrase = new Phrase("Dispute Amount:",BODY_3BOLD);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase(data.getDisputeAmountStr(),BODY_3);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);
        

      //LINE 4 (5 cells)
      
      phrase = new Phrase("Purchase ID:",BODY_3BOLD);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase(data.purchaseID,BODY_3);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);
      
      phrase = new Phrase("",BODY_3);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);
      
      phrase = new Phrase("CB Reference #:",BODY_3BOLD);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      phrase = new Phrase(data.cbRefNumber,BODY_3);
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

    
     
      
      if(data.isAmex())
      {
        //LINE 4 (5 cells)
        phrase = new Phrase("Case Number:",BODY_3);
        cell = new PdfPCell(phrase);
        cell.setBorder(0);
        table.addCell(cell);

        phrase = new Phrase(""+data.caseNum,BODY_3);
        cell = new PdfPCell(phrase);
        cell.setBorder(0);
        table.addCell(cell);

        phrase = new Phrase("",BODY_3);
        cell = new PdfPCell(phrase);
        cell.setBorder(0);
        table.addCell(cell);

        phrase = new Phrase("Ticket Number:",BODY_3);
        cell = new PdfPCell(phrase);
        cell.setBorder(0);
        table.addCell(cell);

        phrase = new Phrase(""+data.ticketNum,BODY_3);
        cell = new PdfPCell(phrase);
        cell.setBorder(0);
        table.addCell(cell);

        //LINE 5 (5 cells)
        phrase = new Phrase("Passenger Name:",BODY_3);
        cell = new PdfPCell(phrase);
        cell.setBorder(0);
        table.addCell(cell);

        phrase = new Phrase(data.passName,BODY_3);
        cell = new PdfPCell(phrase);
        cell.setBorder(0);
        table.addCell(cell);

        phrase = new Phrase("",BODY_3);
        cell = new PdfPCell(phrase);
        cell.setBorder(0);
        cell.setColspan(3);
        table.addCell(cell);


      }

    }
    catch(Exception e)
    {
      log.debug("makeDetailCell Exception: "+e.getMessage());
    }

    cell = new PdfPCell(table);
    cell.setBorder(0);
    return cell;
  }

  public void buildFooter()
  {

    PdfPTable table = new PdfPTable(1);
    table.setWidthPercentage(100);

    PdfPCell cell;
    Phrase phrase;

    try
    {

      phrase = new Phrase(CLOSE,BODY_1);
      phrase.add(new Phrase("\n",BODY_1));
      phrase.add(new Phrase(SIGNATURE,BODY_1));
      cell = new PdfPCell(phrase);
      cell.setBorder(0);
      table.addCell(cell);

      document.add(table);

    }
    catch(Exception e)
    {
      log.debug("buildFooter Exception: "+ e.getMessage());
    }
  }

  //Inner Class to hold data for doc generation
  private class ChargebackDocData
  {

    public  long cbId = 0L;
    public  boolean isAmex = false;

    public  String        merchantNumber          = null;
    public  String        cardNumber              = null;
    public  String        cardType                = null;
    public  String        refNumber               = null;
    public  String        cbRefNumber             = null;
    public  String        purchaseID              = null;
    public  String        reasonCode              = null;
    public  String        reasonDesc              = null;
    public  String        reasonDescDet           = null;
    public  String        modifier                = null;
    public  double        tranAmount              = 0.0;
    public  double        disputeAmount           = 0.0;
    public  String        tranDateStr             = null;
    public  Date          tranDate                = null;
    public  Date          incomingDate            = null;
    public  int           bankNumber              = 0;
    public  String        reasonRes               = null;
    public  String        name                    = null;
    public  String        address1                = null;
    public  String        address2                = null;
    public  String        city                    = null;
    public  String        state                   = null;
    public  String        zip                     = null;
    public  String        faxNumber               = null;

    public  String        passName                = "";
    public  String        ticketNum               = "";
    public  String        refNum                  = "";
    public  String        caseNum                 = "";

    ChargebackDocData(long cbId){

      this.cbId = cbId;
      init(cbId);
    }

    private void init(long cbId)
    {
   /*
    Addr 00: business address
    dmaddr
    address_line_3
    dmcity
    dmstate
    dmzip

    Addr 01:  mailing address
    name1_line_1
    addr1_line_1
    addr1_line_2
    city1_line_4
    state1_line_4
    zip1_line_4

    Addr 02:
    name2_line_1
    addr2_line_1
    addr2_line_2
    city2_line_4
    state2_line_4
    zip2_line_4
 */
      ResultSetIterator it;
      ResultSet rs;

      try
      {
        connect();

        /*@lineinfo:generated-code*//*@lineinfo:1199^9*/

//  ************************************************************
//  #sql [Ctx] it = { select
//              cb.merchant_number                      as merchant_number,
//              cb.card_number                          as card_number,
//              cb.reference_number                     as ref_num,
//              cb.cb_ref_num                           as cb_ref_num,
//              cb.dt_purchase_id                       as dt_purchase_id,
//              to_char(cb.tran_date,'mm/dd/yyyy')      as tran_date_str,
//              cb.tran_date                            as tran_date,
//              cb.incoming_date                        as incoming_date,
//              ddf.transaction_amount                  as tran_amount,
//              nvl(rd.reason_code_disp,cb.reason_code)   as reason_code, 
//              cb.tran_amount                          as dispute_amount,
//              rd.reason_desc                          as reason_desc,
//              rd.reason_desc_detail                   as reason_desc_detail,
//              rd.modifier                   		    as modifier,
//              decode(rd.reason_res,null,'Please contact MeS at our toll-free number as outlined below.',rd.reason_res)
//                                                      as reason_res,
//              rd.card_type                            as card_type,
//              mif.bank_number                         as bank_number,
//              mif.dba_name                            as name,
//              mif.dmaddr                              as addr1_mes,
//              mif.address_line_3                      as addr2_mes,
//              mif.dmcity                              as city_mes,
//              mif.dmstate                             as state_mes,
//              mif.dmzip                               as zip_mes,
//              mif.addr1_line_1                        as addr1_oth,
//              mif.addr1_line_2                        as addr2_oth,
//              mif.city1_line_4                        as city_oth,
//              mif.state1_line_4                       as state_oth,
//              mif.zip1_line_4                         as zip_oth,
//              mif.phone_2_fax                         as fax_num,
//              dt.passenger_name                       as passenger_name,
//              dt.ticket_number                        as ticket_number,
//              dt.reference_number                     as ref_num,
//              nvl(cbam.case_number,cbamb.case_number) as case_num
//           from
//              network_chargebacks       cb,
//              chargeback_reason_desc    rd,
//              mif                       mif,
//              network_chargeback_amex   cbam,
//              network_chargeback_amex_optb cbamb,
//              amex_settlement           dt,
//              daily_detail_file_dt      ddf
//            where
//              cb.cb_load_sec = :cbId
//              and mif.merchant_number(+) = cb.merchant_number
//              and rd.card_type(+) = decode_card_type(cb.card_number,0)
//              and rd.item_type(+) = 'C'
//              and rd.reason_code(+) = cb.reason_code
//              and cbam.load_sec(+) = cb.cb_load_sec
//              and cbamb.load_sec(+) = cb.cb_load_sec
//              and dt.rec_id(+) = cbam.settlement_rec_id
//              and ddf.reference_number = cb.reference_number
//              and ddf.DDF_DT_ID = cb.DT_DDF_DT_ID
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select "
   		+ "cb.merchant_number                      as merchant_number, "
   		+ "cb.card_number                          as card_number,  "
   		+ "cb.reference_number                     as ref_num, "
   		+ "cb.cb_ref_num                           as cb_ref_num, "
   		+ "cb.dt_purchase_id                       as dt_purchase_id, "
   		+ "to_char(cb.tran_date,'mm/dd/yyyy')      as tran_date_str, "
   		+ "cb.tran_date                            as tran_date, "
   		+ "cb.incoming_date                        as incoming_date, "
   		+ "ddf.transaction_amount                  as tran_amount, "
   		+ "nvl(rd.reason_code_disp,cb.reason_code)  as reason_code, "
   		+ "cb.tran_amount                          as dispute_amount, "
   		+ "rd.reason_desc                          as reason_desc, "
   		+ "rd.reason_desc_detail                   as reason_desc_detail, "
   		+ "rd.modifier                             as modifier, "
   		+ "decode(rd.reason_res,null,'Please contact MeS at our toll-free number as outlined below.',rd.reason_res)   as reason_res, "
   		+ "rd.card_type                            as card_type, "
   		+ "mif.bank_number                         as bank_number,"
   		+ "mif.dba_name                            as name, "
   		+ "mif.dmaddr                              as addr1_mes, "
   		+ "mif.address_line_3                      as addr2_mes, "
   		+ "mif.dmcity                              as city_mes, "
   		+ "mif.dmstate                             as state_mes, "
   		+ "mif.dmzip                               as zip_mes, "
   		+ "mif.addr1_line_1                        as addr1_oth, "
   		+ "mif.addr1_line_2                        as addr2_oth, "
   		+ "mif.city1_line_4                        as city_oth, "
   		+ "mif.state1_line_4                       as state_oth, "
   		+ " mif.zip1_line_4                         as zip_oth, "
   		+ "mif.phone_2_fax                         as fax_num, "
   		+ "dt.passenger_name                       as passenger_name, "
   		+ "dt.ticket_number                        as ticket_number, "
   		+ "dt.reference_number                     as ref_num, "
   		+ "nvl(cbam.case_number,cbamb.case_number) as case_num "
   		+ "from\n            network_chargebacks       cb, "
   		+ "chargeback_reason_desc    rd, "
   		+ "mif                       mif, "
   		+ "network_chargeback_amex   cbam, "
   		+ "network_chargeback_amex_optb cbamb, "
   		+ "amex_settlement           dt, "
   		+ "daily_detail_file_dt      ddf "
   		+ "where "
   		+ "cb.cb_load_sec =  :1  "
   		+ "and mif.merchant_number(+) = cb.merchant_number "
   		+ "and rd.card_type(+) = decode_card_type(cb.card_number,0) "
   		+ "and rd.item_type(+) = 'C' "
   		+ "and rd.reason_code(+) = cb.reason_code "
   		+ "and cbam.load_sec(+) = cb.cb_load_sec "
   		+ "and cbamb.load_sec(+) = cb.cb_load_sec "
   		+ "and dt.rec_id(+) = cbam.settlement_rec_id "
   		+ "and ddf.reference_number = cb.reference_number "
   		+ "and ddf.DDF_DT_ID = cb.DT_DDF_DT_ID";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.ReasonCodeDocBuilder",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,cbId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.tools.ReasonCodeDocBuilder",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1254^9*/

        rs = it.getResultSet();

        if (rs.next())
        {
          merchantNumber  = rs.getString("merchant_number");
          bankNumber      = rs.getInt("bank_number");
          cardNumber      = rs.getString("card_number");
          cardType        = rs.getString("card_type");
          refNumber       = rs.getString("ref_num");
          reasonCode      = rs.getString("reason_code");
          reasonDesc      = rs.getString("reason_desc");
          reasonDescDet   = rs.getString("reason_desc_detail");
          modifier        = rs.getString("modifier");
          reasonRes       = rs.getString("reason_res");
          tranAmount      = rs.getDouble("tran_amount");
          tranDate        = rs.getDate("tran_date");
          incomingDate    = rs.getDate("incoming_date");
          tranDateStr     = rs.getString("tran_date_str");
          name            = rs.getString("name");
          faxNumber       = formatFax(rs.getString("fax_num"));
          disputeAmount   = rs.getDouble("dispute_amount");
          cbRefNumber     = rs.getString("cb_ref_num");
          purchaseID      = rs.getString("dt_purchase_id");
          

        //generate address - 3858 is different from others
        if (bankNumber == mesConstants.BANK_ID_CBT)
        {
          address1      = rs.getString("addr1_mes");
          address2      = rs.getString("addr2_mes");
          city          = rs.getString("city_mes");
          state         = rs.getString("state_mes");
          zip           = rs.getString("zip_mes");
        }
        //01 for others
        else
        {
          //Now we are sending mailing address (01) for all
          address1      = rs.getString("addr1_oth");
          address2      = rs.getString("addr2_oth");
          city          = rs.getString("city_oth");
          state         = rs.getString("state_oth");
          zip           = rs.getString("zip_oth");
          
          if (city == null || city.trim().length() == 0) {
            address1      = rs.getString("addr1_mes");
            address2      = rs.getString("addr2_mes");
            city          = rs.getString("city_mes");
            state         = rs.getString("state_mes");
            zip           = rs.getString("zip_mes");
          }
        }

          //add Amex info
          if(isAmex())
          {
            passName    = rs.getString("passenger_name");
            ticketNum   = rs.getString("ticket_number");
            refNum      = rs.getString("ref_num");
            caseNum     = rs.getString("case_num");
          }

        }

        rs.close();
        it.close();
      }
      catch(Exception e)
      {
      }
      finally
      {
        try{ cleanUp(); }catch(Exception e1){}
      }
   }

   public String getAddress()
   {
     StringBuffer sb = new StringBuffer(32);
     sb.append(name).append("\n");
     sb.append(address1).append("\n");
     if(address2!=null && !address2.equals(""))
     {
       sb.append(address2).append("\n");
     }
     sb.append(city).append(", ").append(state).append(" ").append(zip);
     return sb.toString();
   }

   public boolean isAmex()
   {
     return cardType.equals(MesChargebacks.CT_AM);
   }

   private String formatFax(String text)
   {
      if(text==null)
      {
        return "";
      }

      if(text.length()!=10)
      {
        return text;
      }

      try
      {
        StringBuffer sb = new StringBuffer(14);
        sb.append("(");
        sb.append(text.substring(0,3));
        sb.append(") ");
        sb.append(text.substring(3,6));
        sb.append("-");
        sb.append(text.substring(6));
        return sb.toString();
      }
      catch(Exception e)
      {
        return text;
      }
   }

   public String getTranAmountStr()
   {
     StringBuffer sb = new StringBuffer(32);
     DecimalFormat curr = new DecimalFormat("$###,##0.00");
     try
      {
        sb.append(curr.format(tranAmount));
      }
      catch(Exception e)
      {
        sb.append(tranAmount);
      }
     return sb.toString();
   }
   
   public String getDisputeAmountStr()
   {
     StringBuffer sb = new StringBuffer(32);
     DecimalFormat curr = new DecimalFormat("$###,##0.00");
     try
      {
        sb.append(curr.format(disputeAmount));
      }
      catch(Exception e)
      {
        sb.append(disputeAmount);
      }
     return sb.toString();
   }

  }

  public void setFillText(String fillText)
  {
    if(fillText != null)
    {
      this.fillText = fillText;
    }
  }


  //Static strings - TODO - move to DB to allow for dynamic revision
  //the String[] are needed to distinguish between
  //notification and adjustment-based text

  public static String MES_ADDRESS = "12825 E Mirabeau Parkway, Suite 101\nSpokane Valley, WA 99216";

  //[] corresponds to int class at top of page SCOPE_CB, SCOPE_WARN, SCOPE_UTA etc
  public static String[] TITLE = { "   Chargeback/Dispute Notification\n",
                                   "CHARGEBACK PRE-NOTIFICATION LETTER\n",
                                   "CHARGEBACK ADJUSTMENT NOTICE\n",
                                   "CHARGEBACK NOTIFICATION\n",
                                   "\n",
                                   "CHARGEBACK NOTICE - MORE INFORMATION NEEDED\n"
                                 };
  public static String INTRO = "Dear Valued Merchant:";
  public static String[] LINE_1 = { "A chargeback has been issued to your account and debited for the amount listed below. This chargeback has already been credited to the cardholder, so you do not need to issue a credit upon receipt of this notification.\n\n",
                                    "Your account may be debited for the chargeback amount shown below. ",
                                    "",
                                    "We appreciate your business.\n\nYour account has been debited for the chargeback amount shown below. ",
                                    "We appreciate your business.",
                                    "We appreciate your business."
                                  };
  public static String[] LINE_2 = { "You will need to supply dispute documentation addressing the cardholder’s concerns ",
                                    "Please supply documentation ",
                                    "Please supply documentation ",
                                    "",
                                    "Thank you for bringing this additional information to our attention. We will represent this case on your behalf and your Merchant account will be given a provisional credit. ",
                                    "In attempting to resolve the dispute listed below with the Cardholder's bank, we find the need to ask for additional items. In order to further assist you with this case, please provide the following information by:"
                                  };
  public static String LINE_3 = "within " + String.valueOf(DAYS_UNTIL_OVERDUE) + " days of the date of this notification to avoid loss of reversal rights established by the card brands. Please ensure that all documents are PCI compliant and DO NOT include any full credit card numbers or CVV. Any documents that contain PCI sensitive data will not be accepted. ";
  public static String[] LINE_4 =   { "Note: responding to a chargeback notification does not automatically resolve the dispute. ",
                                    "Return this letter with your rebuttal documentation for faster processing. ",
                                    "Return this letter with your rebuttal documentation for faster processing. ",
                                    "If a credit has already been issued, please provide a copy of the credit slip showing the date and amount credited. Do Not issue a credit to the cardholder upon receipt of this letter. ",
                                    "",
                                    "If we do not receive this information by this date, your account will remain debited."
                                  };
  public static String AMEX_LINE_2  = "Please supply documentation no later than 10 days from the date of this letter. Please return this letter with your documentation and put the ";
  public static String AMEX_LINE_3  = "Amex Case Number";
  public static String AMEX_LINE_4  = " on all pieces of your documentation for faster processing.\n\nPlease note, responding to this notification does not ensure that your account will be credited for this chargeback.\n";
  public static String LINE_5 = "Reason for Chargeback  ";
  public static String LINE_6 = "How to Respond  ";
  public static String[] LINE_7 = { "Please provide a copy of the sales draft showing the date and amount and retain the original for your records. Upload your response documents, using the Chargeback Adjustment Report at "  ,
                                  "Please fax this page with your documentation to 509-232-5687 or mail to the address shown above. ",
                                  "Please fax this page with your documentation to 509-232-5687 or mail to the address shown above. ",
                                  "Please call 1-888-288-2692 with any questions regarding this notice. ",
                                  "Please call 1-888-288-2692 with any questions regarding this notice. ",
                                  "If you have any further questions, please call our Customer Service Department at 1-888-288-2692 and inform the Customer Service Representative that you are inquiring about a chargeback. "
                                };
  public static String LINE_8 = "Retain your fax transmittal or certified mail receipt as proof you did respond to this document. ";
  public static String LINE_9 = "\n\nFor assistance in responding to this notice, contact Customer Care by phone at 888.288.2692 or by email ";
  public static String CLOSE = "Thank you, ";
  public static String SIGNATURE = "Merchant Services | Chargeback Department\n1.888.288.2692";

}/*@lineinfo:generated-code*/