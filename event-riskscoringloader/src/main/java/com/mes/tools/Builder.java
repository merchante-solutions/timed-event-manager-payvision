/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/Builder.java $

  Description:  Builder.java

  Generic Builder interface that allows for the use of a Director
  that controls the generation of a given Product


  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 10/13/04 11:05a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

public interface Builder
{
  public Object getResult();
}
