/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/tools/Director.java $

  Description:  Director.java

  Generic Director that controls the referenced Builder
  and ,in turn, its generation of a given Product

  //Copyright 2001 by Paulo Soares (iText)

  Last Modified By   : $Author: Rsorensen $
  Last Modified Date : $Date: 10/13/04 11:05a $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.tools;

// log4j
import org.apache.log4j.Category;

public class Director
{
  // create class log category
  static Category log = Category.getInstance(Director.class.getName());

  //contains generic Builder
  private Builder builder;

  //can only be built with a Builder reference
  public Director (Builder builder)
  {
    this.builder = builder;
  }

  //Only method - uses the Builder to construct it's product
  public void construct()
  {
    if(builder instanceof DocBuilder)
    {
      ((DocBuilder)builder).buildHeader();
      ((DocBuilder)builder).buildBody();
      ((DocBuilder)builder).buildFooter();
    }
  }

}