/*@lineinfo:filename=RiskTransDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/RiskTransDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2012-08-06 16:55:08 -0700 (Mon, 06 Aug 2012) $
  Version            : $Revision: 20434 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.mesConstants;
import com.mes.forms.NumberField;
import com.mes.support.DateTimeFormatter;
import com.mes.support.NumberFormatter;
import sqlj.runtime.ResultSetIterator;

public class RiskTransDataBean extends RiskBaseDataBean
{
  public static final int         AVG_TICKET_BASIS_NONE     = 0;  // no comparison
  public static final int         AVG_TICKET_BASIS_APP      = 1;  // compare avg ticket to application value
  public static final int         AVG_TICKET_BASIS_ME       = 2;  // compare avg ticket to month end value
  
  public class RiskAvgTicketRecord extends RiskRecordBase
  {
    public    long                  AppSeqNum       = 0L;
    protected String                BasisDesc       = null;
    protected int                   BasisType       = 0;
    protected boolean               AvgTicketAdj    = false;
    public    double                AvgTicketBasis  = 0.0;
    public    double                AvgTicketCalc   = 0.0;
    public    double                ItemTotalAmount = 0.0;
    public    int                   ItemTotalCount  = 0;
    
    public RiskAvgTicketRecord( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );
             
      AppSeqNum       = resultSet.getLong("app_seq_num");
      BasisType       = resultSet.getInt("basis_type");
      BasisDesc       = resultSet.getString("basis_desc");
      AvgTicketAdj    = (resultSet.getInt("avg_ticket_adj") != 0);
      AvgTicketBasis  = resultSet.getDouble("avg_ticket_basis");
      AvgTicketCalc   = resultSet.getDouble("avg_ticket_calc");  
      ItemTotalAmount = resultSet.getDouble("item_amount");
      ItemTotalCount  = resultSet.getInt("item_count");
    }
    
    public double getAvgTicket()
    {
      return( AvgTicketCalc );
    }
    
    public String getBasisDesc()
    {
      String      retVal    = BasisDesc;
      
      if ( isAvgTicketAdjusted() )
      {
        retVal = "Risk Adjusted Avg Ticket";
      }
      return( retVal );
    }
    
    public String getRatioString()
    {
      return( NumberFormatter.getPercentString( ((AvgTicketCalc-AvgTicketBasis)/AvgTicketBasis),0 ) );
    }
    
    public boolean isAvgTicketAdjusted()
    {
      return ( isAvgTicketBasisApp() && (AvgTicketAdj == true) );
    }
    
    public boolean isAvgTicketBasisApp()
    {
      return( BasisType == AVG_TICKET_BASIS_APP );
    }
  }

  public class RiskTransDetailRecord extends RiskRecordBase
  {
    public  double          Amount          = 0.0;
    public  String          AuthCode        = null;
    public  Date            BatchDate       = null;
    public  String          CardNumber      = null;
    public  String          ReferenceNumber = null;
    public  Date            TranDate        = null;
    
    public RiskTransDetailRecord( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );
      
      Amount          = resultSet.getDouble("tran_amount");
      AuthCode        = resultSet.getString("auth_code");
      BatchDate       = resultSet.getDate("batch_date");
      CardNumber      = resultSet.getString("card_number");
      TranDate        = resultSet.getDate("tran_date");
      ReferenceNumber = resultSet.getString("ref_num");
    }
  }
  
  public class RiskTransSummaryRecord extends RiskRecordBase
  {
    public    String          CardNumber            = null;
    public    double          ItemCompareAmount     = 0.0;
    public    int             ItemCompareCount      = 0;
    public    double          ItemTotalAmount       = 0.0;
    public    int             ItemTotalCount        = 0;
    public    String          UserData              = null; 
    
    public RiskTransSummaryRecord( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );
      
      CardNumber      = resultSet.getString("card_number");
      
      ItemTotalAmount = resultSet.getDouble("item_amount");
      ItemTotalCount  = resultSet.getInt("item_count");
       
      try
      {
        ItemCompareCount  = resultSet.getInt("keyed_count");
        ItemCompareAmount = resultSet.getDouble("keyed_amount");
      }
      catch( java.sql.SQLException e )
      {
        // compare amounts are not always used.  
      } 
    }
  }
  
  public RiskTransDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    
    switch( ReportStyle )
    {
      case RISK_STYLE_UNMATCHED_CREDITS:
      case RISK_STYLE_LARGE_TICKET_KEYED:
      case RISK_STYLE_LARGE_TICKET_SWIPED:
        line.append("\"Merchant Id\",");
        line.append("\"DBA Name\",");
        switch( getReportBankId() )
        {
          case mesConstants.BANK_ID_CERTEGY:
          case mesConstants.BANK_ID_MES:
          case mesConstants.BANK_ID_MES_WF:
            line.append("\"Portfolio Node\",");
            break;
            
          default:
            break;
        }
        line.append("\"Batch Date\",");
        line.append("\"Tran Date\",");
        line.append("\"Card Number\",");
        line.append("\"Amount\",");
        line.append("\"Auth Code\",");
        line.append("\"Reference\"");
        break;
        
      case RISK_STYLE_DUPLICATE_TRANS:
        line.append("\"Merchant Id\",");
        line.append("\"DBA Name\",");
        line.append("\"Card Number\",");
        line.append("\"Item Count\",");
        line.append("\"Item Amount\"");
        break;
        
        
//      case RISK_STYLE_ITEM_RATIO:   // average ticket risk
//      case RISK_STYLE_KEYED_VOLUME:
//      case RISK_STYLE_ITEM_AMOUNT:
//      case RISK_STYLE_CREDITS_AMOUNT:
//      case RISK_STYLE_UNAUTHORIZED_TRANS:
//      case RISK_STYLE_BULLHEAD_DECLINES:
      default:
        break;
    }
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    int                       bankNumber      = 0;
    RiskTransDetailRecord     transDetail     = null;

    line.setLength(0);

    switch( ReportStyle )
    {
      case RISK_STYLE_LARGE_TICKET_SWIPED:
      case RISK_STYLE_LARGE_TICKET_KEYED:
      case RISK_STYLE_UNMATCHED_CREDITS:
        transDetail = (RiskTransDetailRecord)obj;
        line.append( transDetail.MerchantId );
        line.append( ",\"" );
        line.append( transDetail.DbaName );
        line.append( "\"," );
        switch( getReportBankId() )
        {
          case mesConstants.BANK_ID_CERTEGY:
            line.append( encodeHierarchyNode(getMerchantGroup1(transDetail.MerchantId)) );
            line.append(",");
            break;
            
          case mesConstants.BANK_ID_MES:
          case mesConstants.BANK_ID_MES_WF:
            line.append( encodeHierarchyNode(getMerchantGroup2(transDetail.MerchantId)) );
            line.append(",");
            break;
            
          default:
            break;
        }
        line.append( DateTimeFormatter.getFormattedDate( transDetail.BatchDate,"MM/dd/yyyy" ) );
        line.append( "," );
        line.append( DateTimeFormatter.getFormattedDate( transDetail.TranDate,"MM/dd/yyyy" ) );
        line.append( "," );
        line.append( transDetail.CardNumber );
        line.append( "," );
        line.append( transDetail.Amount );
        line.append( ",\"" );
        line.append( transDetail.AuthCode );
        line.append( "\"," );
        line.append( transDetail.ReferenceNumber );
        break;
        
      case RISK_STYLE_DUPLICATE_TRANS:
      {
        RiskTransSummaryRecord tranSummaryEntry = (RiskTransSummaryRecord)obj;
        line.append( encodeHierarchyNode(tranSummaryEntry.MerchantId) );
        line.append(",\"");
        line.append(tranSummaryEntry.DbaName);
        line.append("\",");
        line.append(tranSummaryEntry.CardNumber);
        line.append(",");
        line.append(tranSummaryEntry.ItemTotalCount);
        line.append(",");
        line.append(tranSummaryEntry.ItemTotalAmount);
        break;
      }
        
//      case RISK_STYLE_ITEM_RATIO:   // average ticket risk
//      case RISK_STYLE_KEYED_VOLUME:
//      case RISK_STYLE_ITEM_AMOUNT:
//      case RISK_STYLE_CREDITS_AMOUNT:
//      case RISK_STYLE_UNAUTHORIZED_TRANS:
//      case RISK_STYLE_BULLHEAD_DECLINES:
      default:
        break;
    }
  }
  
  public String getDownloadFilenameBase()
  {
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );

    switch( ReportStyle )
    {
      case RISK_STYLE_UNMATCHED_CREDITS:
        filename.append("_unmatched_credits_");
        break;
        
      case RISK_STYLE_LARGE_TICKET_SWIPED:
        filename.append("_large_ticket_swiped_");
        break;
        
      case RISK_STYLE_LARGE_TICKET_KEYED:
        filename.append("_large_ticket_keyed_");
        break;
        
      case RISK_STYLE_DUPLICATE_TRANS:
        filename.append("_duplicate_trans_");
        break;
        
//      case RISK_STYLE_ITEM_RATIO:   // average ticket risk
//      case RISK_STYLE_KEYED_VOLUME:
//      case RISK_STYLE_ITEM_AMOUNT:
//      case RISK_STYLE_CREDITS_AMOUNT:
//      case RISK_STYLE_UNAUTHORIZED_TRANS:
//      case RISK_STYLE_BULLHEAD_DECLINES:
      default:
        break;
    }
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin,"MMddyyyy" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd,"MMddyyyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public double getTranAmount( long merchantId )
  {
    return( getTranAmount( merchantId, ReportDateBegin, ReportDateEnd ) );
  }
  
  public double getTranAmount( long merchantId, Date beginDate, Date endDate )
  {
    double        retVal          = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:339^7*/

//  ************************************************************
//  #sql [Ctx] { select sum(sm.bank_amount + sm.nonbank_amount) 
//          from   daily_detail_file_summary sm
//          where  sm.merchant_number = :merchantId and
//                 sm.batch_date between :beginDate and :endDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sum(sm.bank_amount + sm.nonbank_amount)  \n        from   daily_detail_file_summary sm\n        where  sm.merchant_number =  :1  and\n               sm.batch_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.RiskTransDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:345^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  public int getTranCount( long merchantId )
  {
    return( getTranCount( merchantId, ReportDateBegin, ReportDateEnd ) );
  }
  
  public int getTranCount( long merchantId, Date beginDate, Date endDate )
  {
    int     retVal = 0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:364^7*/

//  ************************************************************
//  #sql [Ctx] { select sum(sm.bank_count + sm.nonbank_count) 
//          from   daily_detail_file_summary sm
//          where  sm.merchant_number = :merchantId and
//                 sm.batch_date between :beginDate and :endDate
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select sum(sm.bank_count + sm.nonbank_count)  \n        from   daily_detail_file_summary sm\n        where  sm.merchant_number =  :1  and\n               sm.batch_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.RiskTransDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:370^7*/
    }
    catch( java.sql.SQLException e )
    {
    }
    return( retVal );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    if ( fileFormat == FF_CSV )
    {
      switch( ReportStyle )
      {
        case RISK_STYLE_LARGE_TICKET_SWIPED:
        case RISK_STYLE_LARGE_TICKET_KEYED:
        case RISK_STYLE_UNMATCHED_CREDITS:
        case RISK_STYLE_DUPLICATE_TRANS:
          retVal = true;
          break;

//        case RISK_STYLE_ITEM_RATIO:   // average ticket 
//        case RISK_STYLE_KEYED_VOLUME:
//        case RISK_STYLE_ITEM_AMOUNT:
//        case RISK_STYLE_CREDITS_AMOUNT:
//        case RISK_STYLE_UNAUTHORIZED_TRANS:
//        case RISK_STYLE_BULLHEAD_DECLINES:
        default:
          break;
      }
    }
    return(retVal);
  }
  
  public void loadData( int reportStyle, long orgId, Date beginDate, Date endDate, String[] args )
  {
    double                        amountThreshold   = 0;
    ResultSetIterator             it                = null;
    int                           minCount          = 0;
    double                        percentThreshold  = 0;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( reportStyle == RISK_STYLE_ITEM_AMOUNT )
      {
        try
        { 
          percentThreshold = Double.parseDouble( args[0] ); 
        } 
        catch( Exception e ) 
        {
          percentThreshold = 200;
        }
        try
        { 
          amountThreshold = Double.parseDouble( args[1] ); 
        } 
        catch( Exception e ) 
        {
          amountThreshold = 500;
        }
        
        //
        // select transactions whose amounts are larger
        // than percentThreshold of the average ticket
        // specified in the application
        //
        /*@lineinfo:generated-code*//*@lineinfo:442^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ INDEX (dt IDX_DDF_DT_BATCH_DATE) */
//                   dt.MERCHANT_ACCOUNT_NUMBER       as merchant_number,
//                   dt.MERCHANT_NAME                 as dba_name,
//                   mf.SIC_CODE                      as sic_code,
//                   dt.batch_date                    as batch_date,
//                   dt.TRANSACTION_AMOUNT            as tran_amount,
//                   mr.merch_average_cc_tran         as avg_ticket_app,
//                   dt.CARDHOLDER_ACCOUNT_NUMBER     as card_number,
//                   dt.transaction_date              as tran_date,
//                   dt.REFERENCE_NUMBER              as ref_num,
//                   dt.AUTHORIZATION_NUM             as auth_code,
//                   (mf.bank_number || mf.dmagent)   as assoc_number
//            from   group_merchant         gm,
//                   group_rep_merchant     grm,
//                   daily_detail_file_dt   dt,
//                   merchant               mr,
//                   mif                    mf,
//                   risk_adj_avg_ticket    raat
//            where  gm.org_num = :orgId and
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                   dt.batch_date between :beginDate and :endDate and
//                   dt.merchant_account_number = gm.merchant_number and
//                   dt.debit_credit_indicator = 'D' and
//                   dt.transaction_amount > :amountThreshold and
//                   mr.merch_number    = dt.merchant_account_number and
//                   decode( nvl(raat.adj_avg_ticket, mr.merch_average_cc_tran),
//                           0, 0, 
//                          ((dt.transaction_amount-nvl(raat.adj_avg_ticket, mr.merch_average_cc_tran))/
//                            nvl(raat.adj_avg_ticket, mr.merch_average_cc_tran)) ) > ( :percentThreshold * 0.01 ) and
//                   mf.MERCHANT_NUMBER    = dt.MERCHANT_ACCOUNT_NUMBER and
//                   raat.merchant_number(+) = mf.merchant_number
//            order by dt.merchant_name, dt.merchant_account_number, dt.batch_date desc        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ INDEX (dt IDX_DDF_DT_BATCH_DATE) */\n                 dt.MERCHANT_ACCOUNT_NUMBER       as merchant_number,\n                 dt.MERCHANT_NAME                 as dba_name,\n                 mf.SIC_CODE                      as sic_code,\n                 dt.batch_date                    as batch_date,\n                 dt.TRANSACTION_AMOUNT            as tran_amount,\n                 mr.merch_average_cc_tran         as avg_ticket_app,\n                 dt.CARDHOLDER_ACCOUNT_NUMBER     as card_number,\n                 dt.transaction_date              as tran_date,\n                 dt.REFERENCE_NUMBER              as ref_num,\n                 dt.AUTHORIZATION_NUM             as auth_code,\n                 (mf.bank_number || mf.dmagent)   as assoc_number\n          from   group_merchant         gm,\n                 group_rep_merchant     grm,\n                 daily_detail_file_dt   dt,\n                 merchant               mr,\n                 mif                    mf,\n                 risk_adj_avg_ticket    raat\n          where  gm.org_num =  :1  and\n                 grm.user_id(+) =  :2  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :3  = -1 ) and        \n                 dt.batch_date between  :4  and  :5  and\n                 dt.merchant_account_number = gm.merchant_number and\n                 dt.debit_credit_indicator = 'D' and\n                 dt.transaction_amount >  :6  and\n                 mr.merch_number    = dt.merchant_account_number and\n                 decode( nvl(raat.adj_avg_ticket, mr.merch_average_cc_tran),\n                         0, 0, \n                        ((dt.transaction_amount-nvl(raat.adj_avg_ticket, mr.merch_average_cc_tran))/\n                          nvl(raat.adj_avg_ticket, mr.merch_average_cc_tran)) ) > (  :7  * 0.01 ) and\n                 mf.MERCHANT_NUMBER    = dt.MERCHANT_ACCOUNT_NUMBER and\n                 raat.merchant_number(+) = mf.merchant_number\n          order by dt.merchant_name, dt.merchant_account_number, dt.batch_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.RiskTransDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setDouble(6,amountThreshold);
   __sJT_st.setDouble(7,percentThreshold);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.RiskTransDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:478^9*/
      }
      else if ( reportStyle == RISK_STYLE_CREDITS_AMOUNT )
      {
        try
        { 
          amountThreshold = Double.parseDouble( args[0] ); 
        } 
        catch( Exception e ) 
        {
          amountThreshold = 500;
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:491^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ INDEX (dt IDX_DDF_DT_BATCH_DATE) */
//                   dt.MERCHANT_ACCOUNT_NUMBER       as merchant_number,
//                   dt.MERCHANT_NAME                 as dba_name,
//                   mf.SIC_CODE                      as sic_code,
//                   dt.batch_date                    as batch_date,
//                   (dt.TRANSACTION_AMOUNT * -1)     as tran_amount,
//                   dt.CARDHOLDER_ACCOUNT_NUMBER     as card_number,
//                   dt.transaction_date              as tran_date,
//                   dt.REFERENCE_NUMBER              as ref_num,
//                   dt.authorization_num             as auth_code,
//                   (mf.bank_number || mf.dmagent)   as assoc_number
//            from   group_merchant         gm,
//                   group_rep_merchant     grm,
//                   daily_detail_file_dt   dt,
//                   mif                    mf
//            where  gm.org_num = :orgId and
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                   dt.batch_date between :beginDate and :endDate and
//                   dt.merchant_account_number = gm.merchant_number and
//                   dt.transaction_amount > :amountThreshold and
//                   dt.DEBIT_CREDIT_INDICATOR = 'C' and                  
//                   mf.MERCHANT_NUMBER    = dt.MERCHANT_ACCOUNT_NUMBER
//            order by dt.merchant_name, dt.merchant_account_number, dt.batch_date desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ INDEX (dt IDX_DDF_DT_BATCH_DATE) */\n                 dt.MERCHANT_ACCOUNT_NUMBER       as merchant_number,\n                 dt.MERCHANT_NAME                 as dba_name,\n                 mf.SIC_CODE                      as sic_code,\n                 dt.batch_date                    as batch_date,\n                 (dt.TRANSACTION_AMOUNT * -1)     as tran_amount,\n                 dt.CARDHOLDER_ACCOUNT_NUMBER     as card_number,\n                 dt.transaction_date              as tran_date,\n                 dt.REFERENCE_NUMBER              as ref_num,\n                 dt.authorization_num             as auth_code,\n                 (mf.bank_number || mf.dmagent)   as assoc_number\n          from   group_merchant         gm,\n                 group_rep_merchant     grm,\n                 daily_detail_file_dt   dt,\n                 mif                    mf\n          where  gm.org_num =  :1  and\n                 grm.user_id(+) =  :2  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :3  = -1 ) and        \n                 dt.batch_date between  :4  and  :5  and\n                 dt.merchant_account_number = gm.merchant_number and\n                 dt.transaction_amount >  :6  and\n                 dt.DEBIT_CREDIT_INDICATOR = 'C' and                  \n                 mf.MERCHANT_NUMBER    = dt.MERCHANT_ACCOUNT_NUMBER\n          order by dt.merchant_name, dt.merchant_account_number, dt.batch_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.reports.RiskTransDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setDouble(6,amountThreshold);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.reports.RiskTransDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:518^9*/
      }
      else if ( ( reportStyle == RISK_STYLE_LARGE_TICKET_KEYED ) ||
                ( reportStyle == RISK_STYLE_LARGE_TICKET_SWIPED ) )
      {
        int keyedOnly = (reportStyle == RISK_STYLE_LARGE_TICKET_KEYED) ? 1 : 0;
        
        try
        { 
          amountThreshold = Double.parseDouble( args[0] ); 
        } 
        catch( Exception e ) 
        {
          amountThreshold = 500;
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:534^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ INDEX (dt IDX_DDF_DT_BATCH_DATE) */
//                   dt.MERCHANT_ACCOUNT_NUMBER       as merchant_number,
//                   dt.MERCHANT_NAME                 as dba_name,
//                   mf.SIC_CODE                      as sic_code,
//                   dt.batch_date                    as batch_date,
//                   dt.TRANSACTION_AMOUNT            as tran_amount,
//                   dt.CARDHOLDER_ACCOUNT_NUMBER     as card_number,
//                   dt.transaction_date              as tran_date,
//                   dt.REFERENCE_NUMBER              as ref_num,
//                   dt.AUTHORIZATION_NUM             as auth_code,
//                   (mf.bank_number || mf.dmagent)   as assoc_number
//            from   group_merchant         gm,
//                   group_rep_merchant     grm,
//                   daily_detail_file_dt   dt,
//                   mif                    mf
//            where  gm.org_num = :orgId and
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                   dt.batch_date between :beginDate and :endDate and
//                   dt.merchant_account_number = gm.merchant_number and
//                   dt.transaction_amount > :amountThreshold and
//                   dt.debit_credit_indicator = 'D' and
//                   ( ( :keyedOnly = 1 and 
//                       dt.pos_entry_mode in ('01','81') and
//                       dt.MO_TO_INDICATOR is null ) or
//                     ( :keyedOnly = 0 and 
//                       dt.pos_entry_mode not in ('01','81') ) ) and 
//                   mf.MERCHANT_NUMBER    = dt.MERCHANT_ACCOUNT_NUMBER
//            order by dt.merchant_name, dt.merchant_account_number, dt.batch_date desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ INDEX (dt IDX_DDF_DT_BATCH_DATE) */\n                 dt.MERCHANT_ACCOUNT_NUMBER       as merchant_number,\n                 dt.MERCHANT_NAME                 as dba_name,\n                 mf.SIC_CODE                      as sic_code,\n                 dt.batch_date                    as batch_date,\n                 dt.TRANSACTION_AMOUNT            as tran_amount,\n                 dt.CARDHOLDER_ACCOUNT_NUMBER     as card_number,\n                 dt.transaction_date              as tran_date,\n                 dt.REFERENCE_NUMBER              as ref_num,\n                 dt.AUTHORIZATION_NUM             as auth_code,\n                 (mf.bank_number || mf.dmagent)   as assoc_number\n          from   group_merchant         gm,\n                 group_rep_merchant     grm,\n                 daily_detail_file_dt   dt,\n                 mif                    mf\n          where  gm.org_num =  :1  and\n                 grm.user_id(+) =  :2  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :3  = -1 ) and        \n                 dt.batch_date between  :4  and  :5  and\n                 dt.merchant_account_number = gm.merchant_number and\n                 dt.transaction_amount >  :6  and\n                 dt.debit_credit_indicator = 'D' and\n                 ( (  :7  = 1 and \n                     dt.pos_entry_mode in ('01','81') and\n                     dt.MO_TO_INDICATOR is null ) or\n                   (  :8  = 0 and \n                     dt.pos_entry_mode not in ('01','81') ) ) and \n                 mf.MERCHANT_NUMBER    = dt.MERCHANT_ACCOUNT_NUMBER\n          order by dt.merchant_name, dt.merchant_account_number, dt.batch_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.RiskTransDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setDouble(6,amountThreshold);
   __sJT_st.setInt(7,keyedOnly);
   __sJT_st.setInt(8,keyedOnly);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.RiskTransDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:566^9*/
      }
      else if ( reportStyle == RISK_STYLE_CREDITS_RATIO )
      {
        try
        { 
          percentThreshold = Double.parseDouble( args[0] ); 
        } 
        catch( Exception e ) 
        {
          percentThreshold = 1;
        }
        
        try
        { 
          minCount = Integer.parseInt( args[1] ); 
        } 
        catch( Exception e ) 
        {
          minCount = 0;
        }
      
        /*@lineinfo:generated-code*//*@lineinfo:588^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ INDEX (dt IDX_DDF_DT_BATCH_DATE) */
//                   dt.MERCHANT_ACCOUNT_NUMBER       as merchant_number,
//                   dt.MERCHANT_NAME                 as dba_name,
//                   mf.SIC_CODE                      as sic_code,
//                   dt.batch_date                    as batch_date,
//                   (dt.TRANSACTION_AMOUNT * -1)     as tran_amount,
//                   dt.CARDHOLDER_ACCOUNT_NUMBER     as card_number,
//                   dt.transaction_date              as tran_date,
//                   dt.REFERENCE_NUMBER              as ref_num,
//                   dt.authorization_num             as auth_code,
//                   (mf.bank_number || mf.dmagent)   as assoc_number
//            from   group_merchant         gm,
//                   group_rep_merchant     grm,
//                   daily_detail_file_dt   dt,
//                   mif                    mf
//            where  gm.org_num = :orgId and
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and
//                   mf.MERCHANT_NUMBER = gm.merchant_number and
//                   is_risk_merchant( mf.merchant_number, :beginDate, :endDate, :percentThreshold, :RISK_TYPE_TRANSACTIONS, :RISK_STYLE_CREDITS_RATIO, :minCount ) = 1 and
//                   dt.batch_date between :beginDate and :endDate and
//                   dt.MERCHANT_account_NUMBER = mf.merchant_number and
//                   dt.DEBIT_CREDIT_INDICATOR = 'C'
//            order by dt.merchant_name, dt.merchant_account_number , dt.batch_date
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ INDEX (dt IDX_DDF_DT_BATCH_DATE) */\n                 dt.MERCHANT_ACCOUNT_NUMBER       as merchant_number,\n                 dt.MERCHANT_NAME                 as dba_name,\n                 mf.SIC_CODE                      as sic_code,\n                 dt.batch_date                    as batch_date,\n                 (dt.TRANSACTION_AMOUNT * -1)     as tran_amount,\n                 dt.CARDHOLDER_ACCOUNT_NUMBER     as card_number,\n                 dt.transaction_date              as tran_date,\n                 dt.REFERENCE_NUMBER              as ref_num,\n                 dt.authorization_num             as auth_code,\n                 (mf.bank_number || mf.dmagent)   as assoc_number\n          from   group_merchant         gm,\n                 group_rep_merchant     grm,\n                 daily_detail_file_dt   dt,\n                 mif                    mf\n          where  gm.org_num =  :1  and\n                 grm.user_id(+) =  :2  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :3  = -1 ) and\n                 mf.MERCHANT_NUMBER = gm.merchant_number and\n                 is_risk_merchant( mf.merchant_number,  :4 ,  :5 ,  :6 ,  :7 ,  :8 ,  :9  ) = 1 and\n                 dt.batch_date between  :10  and  :11  and\n                 dt.MERCHANT_account_NUMBER = mf.merchant_number and\n                 dt.DEBIT_CREDIT_INDICATOR = 'C'\n          order by dt.merchant_name, dt.merchant_account_number , dt.batch_date";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.reports.RiskTransDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setDouble(6,percentThreshold);
   __sJT_st.setInt(7,RISK_TYPE_TRANSACTIONS);
   __sJT_st.setInt(8,RISK_STYLE_CREDITS_RATIO);
   __sJT_st.setInt(9,minCount);
   __sJT_st.setDate(10,beginDate);
   __sJT_st.setDate(11,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.reports.RiskTransDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:615^9*/
      }
      else if ( reportStyle == RISK_STYLE_KEYED_VOLUME )
      {
        try
        { 
          percentThreshold = Double.parseDouble( args[0] ); 
        } 
        catch( Exception e ) 
        {
          percentThreshold = 20;
        }
    
        /*@lineinfo:generated-code*//*@lineinfo:628^9*/

//  ************************************************************
//  #sql [Ctx] it = { select mf.merchant_number               as merchant_number,
//                   mf.dba_name                      as dba_name,
//                   mf.sic_code                      as sic_code,
//                   mr.app_seq_num                   as app_seq_num,
//                   sum(decode(dt.mo_to_indicator,
//                          null, decode(dt.pos_entry_mode,
//                                        '01',1,
//                                        '81',1,
//                                        0),
//                          0))                       as keyed_count,
//                   sum(decode(dt.mo_to_indicator,
//                          null, decode(dt.pos_entry_mode,
//                                        '01',dt.transaction_amount,
//                                        '81',dt.transaction_amount,
//                                        0),
//                          0))                       as keyed_amount,
//                   null                             as card_number, -- n/a
//                   count(1)                         as item_count,
//                   sum(dt.transaction_amount)       as item_amount,
//                   (mf.bank_number || mf.dmagent)   as assoc_number
//            from   group_merchant               gm,
//                   group_rep_merchant           grm,
//                   daily_detail_file_dt         dt,
//                   merch_pos                    mp,
//                   merchant                     mr,
//                   mif                          mf
//            where  gm.org_num = :orgId and
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                   mf.merchant_number = gm.merchant_number and
//                   is_risk_merchant( mf.merchant_number, :beginDate, :endDate, :percentThreshold, :RISK_TYPE_TRANSACTIONS, :RISK_STYLE_KEYED_VOLUME ) = 1 and
//                   dt.batch_date between :beginDate and :endDate and
//                   dt.merchant_account_number = mf.merchant_number and
//                   dt.debit_credit_indicator = 'D' and
//                   mr.MERCH_NUMBER(+) = mf.merchant_number and
//                   mp.app_seq_num(+) = mr.app_seq_num and
//                   -- try to get the pos_code from the application data
//                   -- if the app is not available, then try to use the
//                   -- product code in user data 3.  (401 == dialpay pos_code)
//                   decode( mp.pos_code,
//                           null, (decode(nvl(substr(mf.user_data_3,3,2),''),'DP',401,0)),
//                           mp.pos_code )  <> 401
//            group by mf.merchant_number, mf.dba_name, mf.sic_code, mr.app_seq_num,
//                     (mf.bank_number || mf.dmagent) 
//            order by (keyed_count/item_count) desc,
//                     keyed_count desc, 
//                     dba_name,
//                     merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select mf.merchant_number               as merchant_number,\n                 mf.dba_name                      as dba_name,\n                 mf.sic_code                      as sic_code,\n                 mr.app_seq_num                   as app_seq_num,\n                 sum(decode(dt.mo_to_indicator,\n                        null, decode(dt.pos_entry_mode,\n                                      '01',1,\n                                      '81',1,\n                                      0),\n                        0))                       as keyed_count,\n                 sum(decode(dt.mo_to_indicator,\n                        null, decode(dt.pos_entry_mode,\n                                      '01',dt.transaction_amount,\n                                      '81',dt.transaction_amount,\n                                      0),\n                        0))                       as keyed_amount,\n                 null                             as card_number, -- n/a\n                 count(1)                         as item_count,\n                 sum(dt.transaction_amount)       as item_amount,\n                 (mf.bank_number || mf.dmagent)   as assoc_number\n          from   group_merchant               gm,\n                 group_rep_merchant           grm,\n                 daily_detail_file_dt         dt,\n                 merch_pos                    mp,\n                 merchant                     mr,\n                 mif                          mf\n          where  gm.org_num =  :1  and\n                 grm.user_id(+) =  :2  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :3  = -1 ) and        \n                 mf.merchant_number = gm.merchant_number and\n                 is_risk_merchant( mf.merchant_number,  :4 ,  :5 ,  :6 ,  :7 ,  :8  ) = 1 and\n                 dt.batch_date between  :9  and  :10  and\n                 dt.merchant_account_number = mf.merchant_number and\n                 dt.debit_credit_indicator = 'D' and\n                 mr.MERCH_NUMBER(+) = mf.merchant_number and\n                 mp.app_seq_num(+) = mr.app_seq_num and\n                 -- try to get the pos_code from the application data\n                 -- if the app is not available, then try to use the\n                 -- product code in user data 3.  (401 == dialpay pos_code)\n                 decode( mp.pos_code,\n                         null, (decode(nvl(substr(mf.user_data_3,3,2),''),'DP',401,0)),\n                         mp.pos_code )  <> 401\n          group by mf.merchant_number, mf.dba_name, mf.sic_code, mr.app_seq_num,\n                   (mf.bank_number || mf.dmagent) \n          order by (keyed_count/item_count) desc,\n                   keyed_count desc, \n                   dba_name,\n                   merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.RiskTransDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setDouble(6,percentThreshold);
   __sJT_st.setInt(7,RISK_TYPE_TRANSACTIONS);
   __sJT_st.setInt(8,RISK_STYLE_KEYED_VOLUME);
   __sJT_st.setDate(9,beginDate);
   __sJT_st.setDate(10,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.RiskTransDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:679^9*/
      }
      else if ( reportStyle == RISK_STYLE_ITEM_RATIO )
      {
        try
        { 
          percentThreshold = Double.parseDouble( args[0] ); 
        } 
        catch( Exception e ) 
        {
          percentThreshold = 100;
        }
        
        try
        { 
          amountThreshold = Double.parseDouble( args[1] ); 
        } 
        catch( Exception e ) 
        {
          amountThreshold = 100;
        }
      
        /*@lineinfo:generated-code*//*@lineinfo:701^9*/

//  ************************************************************
//  #sql [Ctx] it = { select mf.merchant_number             as merchant_number,
//                   mf.dba_name                    as dba_name,
//                   mf.sic_code                    as sic_code,
//                   mr.app_seq_num                 as app_seq_num,
//                   rat.basis_type                 as basis_type,
//                   ratb.basis_desc                as basis_desc,
//                   rat.basis_amount               as avg_ticket_basis,
//                   rat.avg_ticket_amount          as avg_ticket_calc,
//                   nvl(raat.adj_avg_ticket,0)     as avg_ticket_adj,
//                   rat.item_total_count           as item_count,
//                   rat.item_total_amount          as item_amount,
//                   (mf.bank_number || mf.dmagent) as assoc_number
//            from    group_merchant              gm,
//                    group_rep_merchant          grm,
//                    risk_avg_ticket_exceptions  rat,
//                    risk_adj_avg_ticket         raat,
//                    risk_avg_ticket_basis       ratb,
//                    mif                         mf,
//                    merchant                    mr
//            where   gm.org_num = :orgId and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    rat.merchant_number = gm.merchant_number and
//                    rat.batch_date between :beginDate and :endDate and
//                    rat.avg_ticket_amount >= :amountThreshold and
//                    nvl(rat.basis_amount,0) > 0 and
//                    ((rat.avg_ticket_amount-rat.basis_amount)/rat.basis_amount) > (:percentThreshold * 0.01) and
//                    raat.merchant_number(+) = rat.merchant_number and
//                    ratb.basis_type(+) = rat.basis_type and
//                    mf.merchant_number = rat.merchant_number and 
//                    mr.merch_number(+) = mf.merchant_number
//            order by  (rat.avg_ticket_amount/rat.basis_amount) desc,
//                      dba_name, merchant_number        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select mf.merchant_number             as merchant_number,\n                 mf.dba_name                    as dba_name,\n                 mf.sic_code                    as sic_code,\n                 mr.app_seq_num                 as app_seq_num,\n                 rat.basis_type                 as basis_type,\n                 ratb.basis_desc                as basis_desc,\n                 rat.basis_amount               as avg_ticket_basis,\n                 rat.avg_ticket_amount          as avg_ticket_calc,\n                 nvl(raat.adj_avg_ticket,0)     as avg_ticket_adj,\n                 rat.item_total_count           as item_count,\n                 rat.item_total_amount          as item_amount,\n                 (mf.bank_number || mf.dmagent) as assoc_number\n          from    group_merchant              gm,\n                  group_rep_merchant          grm,\n                  risk_avg_ticket_exceptions  rat,\n                  risk_adj_avg_ticket         raat,\n                  risk_avg_ticket_basis       ratb,\n                  mif                         mf,\n                  merchant                    mr\n          where   gm.org_num =  :1  and\n                  grm.user_id(+) =  :2  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :3  = -1 ) and        \n                  rat.merchant_number = gm.merchant_number and\n                  rat.batch_date between  :4  and  :5  and\n                  rat.avg_ticket_amount >=  :6  and\n                  nvl(rat.basis_amount,0) > 0 and\n                  ((rat.avg_ticket_amount-rat.basis_amount)/rat.basis_amount) > ( :7  * 0.01) and\n                  raat.merchant_number(+) = rat.merchant_number and\n                  ratb.basis_type(+) = rat.basis_type and\n                  mf.merchant_number = rat.merchant_number and \n                  mr.merch_number(+) = mf.merchant_number\n          order by  (rat.avg_ticket_amount/rat.basis_amount) desc,\n                    dba_name, merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.reports.RiskTransDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setDouble(6,amountThreshold);
   __sJT_st.setDouble(7,percentThreshold);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.reports.RiskTransDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:737^9*/
      }
      else if ( reportStyle == RISK_STYLE_UNAUTHORIZED_TRANS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:741^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ INDEX (dt IDX_DDF_DT_BATCH_DATE) */
//                   dt.MERCHANT_ACCOUNT_NUMBER       as merchant_number,
//                   dt.MERCHANT_NAME                 as dba_name,
//                   mf.SIC_CODE                      as sic_code,
//                   dt.batch_date                    as batch_date,
//                   dt.TRANSACTION_AMOUNT            as tran_amount,
//                   0.0                              as avg_ticket_app,
//                   dt.CARDHOLDER_ACCOUNT_NUMBER     as card_number,
//                   dt.transaction_date              as tran_date,
//                   dt.REFERENCE_NUMBER              as ref_num,
//                   dt.AUTHORIZATION_NUM             as auth_code,
//                   (mf.bank_number || mf.dmagent)   as assoc_number
//            from   group_merchant         gm,
//                   group_rep_merchant     grm,
//                   daily_detail_file_dt   dt,
//                   mif                    mf
//            where  gm.org_num = :orgId and
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                   dt.batch_date between :beginDate and :endDate and
//                   dt.merchant_account_number = gm.merchant_number and
//                   dt.debit_credit_indicator = 'D' and
//                   (dt.AUTHORIZATION_NUM is null or 
//                     (dt.batch_date >= '1-sep-2011' and dt.auth_rec_id is null)) and
//                   mf.MERCHANT_NUMBER    = dt.MERCHANT_ACCOUNT_NUMBER
//            order by dt.merchant_name, dt.merchant_account_number, dt.batch_date desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ INDEX (dt IDX_DDF_DT_BATCH_DATE) */\n                 dt.MERCHANT_ACCOUNT_NUMBER       as merchant_number,\n                 dt.MERCHANT_NAME                 as dba_name,\n                 mf.SIC_CODE                      as sic_code,\n                 dt.batch_date                    as batch_date,\n                 dt.TRANSACTION_AMOUNT            as tran_amount,\n                 0.0                              as avg_ticket_app,\n                 dt.CARDHOLDER_ACCOUNT_NUMBER     as card_number,\n                 dt.transaction_date              as tran_date,\n                 dt.REFERENCE_NUMBER              as ref_num,\n                 dt.AUTHORIZATION_NUM             as auth_code,\n                 (mf.bank_number || mf.dmagent)   as assoc_number\n          from   group_merchant         gm,\n                 group_rep_merchant     grm,\n                 daily_detail_file_dt   dt,\n                 mif                    mf\n          where  gm.org_num =  :1  and\n                 grm.user_id(+) =  :2  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :3  = -1 ) and        \n                 dt.batch_date between  :4  and  :5  and\n                 dt.merchant_account_number = gm.merchant_number and\n                 dt.debit_credit_indicator = 'D' and\n                 (dt.AUTHORIZATION_NUM is null or \n                   (dt.batch_date >= '1-sep-2011' and dt.auth_rec_id is null)) and\n                 mf.MERCHANT_NUMBER    = dt.MERCHANT_ACCOUNT_NUMBER\n          order by dt.merchant_name, dt.merchant_account_number, dt.batch_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.reports.RiskTransDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.reports.RiskTransDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:770^9*/
      }
      else if ( reportStyle == RISK_STYLE_DUPLICATE_TRANS )
      {
        try
        { 
          amountThreshold = Double.parseDouble( args[0] ); 
        } 
        catch( Exception e ) 
        {
          amountThreshold = 300;
        }
      
        /*@lineinfo:generated-code*//*@lineinfo:783^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ INDEX (dt IDX_DDF_DT_BATCH_DATE) */
//                   dt.MERCHANT_ACCOUNT_NUMBER             as merchant_number,
//                   dt.MERCHANT_NAME                       as dba_name,
//                   mf.SIC_CODE                            as sic_code,
//                   dt.CARDHOLDER_ACCOUNT_NUMBER           as card_number,
//                   count( dt.CARDHOLDER_ACCOUNT_NUMBER )  as item_count,
//                   sum( dt.transaction_amount)            as item_amount,
//                   --dt.batch_date                          as batch_date,
//                   --dt.batch_number                        as batch_number,
//                   (mf.bank_number || mf.dmagent)         as assoc_number
//            from   group_merchant         gm,
//                   group_rep_merchant     grm,
//                   daily_detail_file_dt   dt,
//                   mif                    mf
//            where  gm.org_num = :orgId and
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                   dt.batch_date between :beginDate and :endDate and
//                   dt.merchant_account_number = gm.merchant_number and
//                   dt.debit_credit_indicator = 'D' and
//                   dt.transaction_amount >= :amountThreshold and
//    --               batch_duplicate_items_count( dt.merchant_account_number,
//    --                                            dt.cardholder_account_number,
//    --                                            dt.batch_date, dt.batch_number, 
//    --                                            dt.DDF_DT_ID ) > 1 and
//                   mf.MERCHANT_NUMBER    = dt.MERCHANT_ACCOUNT_NUMBER
//            group by  (mf.bank_number || mf.dmagent),
//                      dt.MERCHANT_ACCOUNT_NUMBER, dt.MERCHANT_NAME,
//                      mf.SIC_CODE, dt.CARDHOLDER_ACCOUNT_NUMBER
//                      --, dt.batch_date, dt.batch_number
//            order by dt.merchant_name, dt.merchant_account_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ INDEX (dt IDX_DDF_DT_BATCH_DATE) */\n                 dt.MERCHANT_ACCOUNT_NUMBER             as merchant_number,\n                 dt.MERCHANT_NAME                       as dba_name,\n                 mf.SIC_CODE                            as sic_code,\n                 dt.CARDHOLDER_ACCOUNT_NUMBER           as card_number,\n                 count( dt.CARDHOLDER_ACCOUNT_NUMBER )  as item_count,\n                 sum( dt.transaction_amount)            as item_amount,\n                 --dt.batch_date                          as batch_date,\n                 --dt.batch_number                        as batch_number,\n                 (mf.bank_number || mf.dmagent)         as assoc_number\n          from   group_merchant         gm,\n                 group_rep_merchant     grm,\n                 daily_detail_file_dt   dt,\n                 mif                    mf\n          where  gm.org_num =  :1  and\n                 grm.user_id(+) =  :2  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :3  = -1 ) and        \n                 dt.batch_date between  :4  and  :5  and\n                 dt.merchant_account_number = gm.merchant_number and\n                 dt.debit_credit_indicator = 'D' and\n                 dt.transaction_amount >=  :6  and\n  --               batch_duplicate_items_count( dt.merchant_account_number,\n  --                                            dt.cardholder_account_number,\n  --                                            dt.batch_date, dt.batch_number, \n  --                                            dt.DDF_DT_ID ) > 1 and\n                 mf.MERCHANT_NUMBER    = dt.MERCHANT_ACCOUNT_NUMBER\n          group by  (mf.bank_number || mf.dmagent),\n                    dt.MERCHANT_ACCOUNT_NUMBER, dt.MERCHANT_NAME,\n                    mf.SIC_CODE, dt.CARDHOLDER_ACCOUNT_NUMBER\n                    --, dt.batch_date, dt.batch_number\n          order by dt.merchant_name, dt.merchant_account_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.reports.RiskTransDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setDouble(6,amountThreshold);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.reports.RiskTransDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:817^9*/
      }
      else if ( reportStyle == RISK_STYLE_BULLHEAD_DECLINES )
      {
      }
      else if ( reportStyle == RISK_STYLE_UNMATCHED_CREDITS )
      {
        /*@lineinfo:generated-code*//*@lineinfo:824^9*/

//  ************************************************************
//  #sql [Ctx] it = { select /*+ INDEX (dt IDX_DDF_DT_BATCH_DATE) */
//                   cr.MERCHANT_NUMBER               as merchant_number,
//                   mf.dba_name                      as dba_name,
//                   mf.sic_code                      as sic_code,
//                   cr.batch_date                    as batch_date,
//                   -cr.TRAN_AMOUNT                  as tran_amount,
//                   0.0                              as avg_ticket_app,
//                   cr.card_number                   as card_number,
//                   cr.tran_date                     as tran_date,
//                   cr.REFERENCE_NUMBER              as ref_num,
//                   null                             as auth_code,
//                   (mf.bank_number || mf.dmagent)   as assoc_number
//            from   group_merchant             gm,
//                   group_rep_merchant         grm,
//                   daily_detail_file_credits  cr,
//                   mif                        mf
//            where  gm.org_num = :orgId and 
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                   cr.merchant_number = gm.merchant_number and
//                   cr.batch_date between :beginDate and :endDate and
//                   cr.tran_amount > 100.00 and
//                   cr.purchase_batch_date is null and                 
//                   mf.MERCHANT_NUMBER    = cr.MERCHANT_NUMBER
//            order by mf.dba_name, cr.merchant_number, cr.batch_date desc       
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select /*+ INDEX (dt IDX_DDF_DT_BATCH_DATE) */\n                 cr.MERCHANT_NUMBER               as merchant_number,\n                 mf.dba_name                      as dba_name,\n                 mf.sic_code                      as sic_code,\n                 cr.batch_date                    as batch_date,\n                 -cr.TRAN_AMOUNT                  as tran_amount,\n                 0.0                              as avg_ticket_app,\n                 cr.card_number                   as card_number,\n                 cr.tran_date                     as tran_date,\n                 cr.REFERENCE_NUMBER              as ref_num,\n                 null                             as auth_code,\n                 (mf.bank_number || mf.dmagent)   as assoc_number\n          from   group_merchant             gm,\n                 group_rep_merchant         grm,\n                 daily_detail_file_credits  cr,\n                 mif                        mf\n          where  gm.org_num =  :1  and \n                 grm.user_id(+) =  :2  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :3  = -1 ) and        \n                 cr.merchant_number = gm.merchant_number and\n                 cr.batch_date between  :4  and  :5  and\n                 cr.tran_amount > 100.00 and\n                 cr.purchase_batch_date is null and                 \n                 mf.MERCHANT_NUMBER    = cr.MERCHANT_NUMBER\n          order by mf.dba_name, cr.merchant_number, cr.batch_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.reports.RiskTransDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.reports.RiskTransDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:852^9*/
      }
      resultSet = it.getResultSet();
  
      while( resultSet.next() )
      {
        switch( reportStyle )
        {
          case RISK_STYLE_ITEM_RATIO:   // average ticket risk
            ReportRows.add( new RiskAvgTicketRecord( resultSet ) );
            break;
            
          case RISK_STYLE_DUPLICATE_TRANS:
            // because of query performance issues, the duplicate
            // transaction query only returns the card # and sum
            // of the matching items.
            if ( resultSet.getInt("item_count") > 1 )
            {
              ReportRows.add( new RiskTransSummaryRecord( resultSet ) );
            }                                                  
            break;
            
          case RISK_STYLE_KEYED_VOLUME:
            ReportRows.add( new RiskTransSummaryRecord( resultSet ) );
            break;
            
//          case RISK_STYLE_ITEM_AMOUNT:
//          case RISK_STYLE_CREDITS_AMOUNT:
//          case RISK_STYLE_LARGE_TICKET_KEYED:
//          case RISK_STYLE_UNAUTHORIZED_TRANS:
//          case RISK_STYLE_BULLHEAD_DECLINES:
//          case RISK_STYLE_UNMATCHED_CREDITS:
          default:
            ReportRows.add( new RiskTransDetailRecord( resultSet ) );
            break;
        }
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  public void setProperties(HttpServletRequest request)
  {
    Calendar        cal           = Calendar.getInstance();
    int             daysBack      = 1;
    
    // load the default report properties
    super.setProperties( request );
    
    if ( usingDefaultReportDates() )
    {
      switch( ReportStyle )
      {
        case RISK_STYLE_DUPLICATE_TRANS:  // duplicate items
          daysBack = 7;
          break;
          
        default: 
          daysBack = 1;
          break;
      }
      
      // setup the default date range
      cal.setTime( ReportDateEnd );
      cal.add( Calendar.DAY_OF_MONTH, -daysBack );
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
    }    
  }
  
  protected void setReportStyle( int reportStyle )
  {
    super.setReportStyle( reportStyle );
    
    switch( ReportStyle )
    {
      case RISK_STYLE_ITEM_AMOUNT:
        ReportTitle       = "Excessively Large Transactions";
        
        QueryParamLabels  = new String[] 
                            { "Item Amount Exceeds Avg Ticket By: %",
                              "Minimum Item Amount: $" };
                              
        // create the input fields
        fields.add( new NumberField("queryParam0",6,6,false,0) );
        fields.add( new NumberField("queryParam1",6,6,false,0) );
    
        // set the default values
        fields.getField("queryParam0").setData("300");
        fields.getField("queryParam1").setData("500");
        break;
        
      case RISK_STYLE_ITEM_RATIO:
        ReportTitle       = "Average Ticket Exceptions";
        QueryParamLabels  = new String[] 
                            { "Avg Ticket Exceeds Expected Value By: %",
                              "Minimum Calculated Avg Ticket: $" };
                              
        // create the input fields
        fields.add( new NumberField("queryParam0",6,6,false,0) );
        fields.add( new NumberField("queryParam1",6,6,false,0) );
    
        // set the default values
        fields.getField("queryParam0").setData("100");
        fields.getField("queryParam1").setData("100");
        break;
        
      case RISK_STYLE_CREDITS_AMOUNT:
        ReportTitle       = "Excessively Large Credits";
        QueryParamLabels  = new String[] { "Item Amount Exceeds: $" };
        
        // create the input fields
        fields.add( new NumberField("queryParam0",6,6,false,0) );
    
        // set the default values
        fields.getField("queryParam0").setData("500");
        break;
        
      case RISK_STYLE_CREDITS_RATIO:
        ReportTitle       = "Excessive Credit Volume";
        QueryParamLabels  = new String[] 
                            { "Ratio of Credits Exceeds: %",
                              "Minimum Number of Credits:" };
                              
        // create the input fields
        fields.add( new NumberField("queryParam0",6,6,false,0) );
        fields.add( new NumberField("queryParam1",6,6,false,0) );
    
        // set the default values
        fields.getField("queryParam0").setData("1");
        fields.getField("queryParam1").setData("3");
        break;                              
        
      case RISK_STYLE_LARGE_TICKET_SWIPED:
      case RISK_STYLE_LARGE_TICKET_KEYED:
        if ( ReportStyle == RISK_STYLE_LARGE_TICKET_KEYED )
        {
          ReportTitle       = "Excessively Large Keyed Items";
          QueryParamLabels  = new String[] { "Keyed Item Amount Exceeds: $" };
        }
        else
        {
          ReportTitle       = "Excessively Large Swiped Items";
          QueryParamLabels  = new String[] { "Swiped Item Amount Exceeds: $" };
        }          

        // create the input fields
        fields.add( new NumberField("queryParam0",6,6,false,0) );
    
        // set the default values
        fields.getField("queryParam0").setData("500");
        break;                              
      
      case RISK_STYLE_KEYED_VOLUME:
        ReportTitle       = "Excessive Key-Entered Volume";
        QueryParamLabels  = new String[] { "Ratio of Keyed Items Exceeds: %" };
        
        // create the input fields
        fields.add( new NumberField("queryParam0",6,6,false,0) );
    
        // set the default values
        fields.getField("queryParam0").setData("20");
        break;
        
      case RISK_STYLE_UNAUTHORIZED_TRANS:
        ReportTitle       = "Unauthorized Transactions";
        break;        
        
      case RISK_STYLE_DUPLICATE_TRANS:
        ReportTitle       = "Duplicate Transactions";
        QueryParamLabels  = new String[] { "Minimum Item Amount: $" };
        
        // create the input fields
        fields.add( new NumberField("queryParam0",6,6,false,0) );
    
        // set the default values
        fields.getField("queryParam0").setData("300");
        break;
        
      case RISK_STYLE_UNMATCHED_CREDITS:
        ReportTitle       = "Unmatched Credits";
        break;
    }
  }
  
  public boolean showContactInfo( )
  {
    return(true);
  }
  
  public void storeDefaultAvgTicket( long merchantId )
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:1053^7*/

//  ************************************************************
//  #sql [Ctx] { delete 
//          from   risk_adj_avg_ticket
//          where  merchant_number = :merchantId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n        from   risk_adj_avg_ticket\n        where  merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.reports.RiskTransDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1058^7*/
      /*@lineinfo:generated-code*//*@lineinfo:1059^7*/

//  ************************************************************
//  #sql [Ctx] { commit  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1059^27*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "storeDefaultAvgTicket()", e.toString() );
    }      
  }
  
  public void storeOverloadedAvgTicket( long merchantId, double avgTicket )
  {
    int       rowCount  = 0;
    
    try
    {
      if ( avgTicket > 0.0 )
      {
        /*@lineinfo:generated-code*//*@lineinfo:1075^9*/

//  ************************************************************
//  #sql [Ctx] { select count( merchant_number ) 
//            from   risk_adj_avg_ticket
//            where  merchant_number = :merchantId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count( merchant_number )  \n          from   risk_adj_avg_ticket\n          where  merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.reports.RiskTransDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   rowCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1080^9*/
      
        if ( rowCount == 0 )
        {
          /*@lineinfo:generated-code*//*@lineinfo:1084^11*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_adj_avg_ticket
//                ( merchant_number, adj_avg_ticket )
//              values
//                ( :merchantId, :avgTicket )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_adj_avg_ticket\n              ( merchant_number, adj_avg_ticket )\n            values\n              (  :1 ,  :2  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.reports.RiskTransDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDouble(2,avgTicket);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1090^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:1094^11*/

//  ************************************************************
//  #sql [Ctx] { update risk_adj_avg_ticket
//              set adj_avg_ticket = :avgTicket
//              where merchant_number = :merchantId 
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update risk_adj_avg_ticket\n            set adj_avg_ticket =  :1 \n            where merchant_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.reports.RiskTransDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDouble(1,avgTicket);
   __sJT_st.setLong(2,merchantId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1099^11*/
        }
        /*@lineinfo:generated-code*//*@lineinfo:1101^9*/

//  ************************************************************
//  #sql [Ctx] { commit };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1101^27*/
      }
      else    // invalid amount
      {
        addError("Failed to store adjusted average ticket because the new value was missing or invalid." );
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "storeOverloadedAvgTicket()", e.toString() );
    }      
  }
}/*@lineinfo:generated-code*/