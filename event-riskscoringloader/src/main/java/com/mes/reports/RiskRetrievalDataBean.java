/*@lineinfo:filename=RiskRetrievalDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/RiskRetrievalDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import com.mes.config.MesDefaults;
import com.mes.forms.NumberField;
import sqlj.runtime.ResultSetIterator;

public class RiskRetrievalDataBean extends RiskBaseDataBean
{
  public class RiskRetrievalRecord extends RiskRecordBase
  {
    public double         Amount            = 0.0;
    public String         CardNumber        = null;
    public Date           IncomingDate      = null;
    public String         ReasonCode        = null;
    public String         ReasonDescription = null;
    public String         ReferenceNumber   = null;
    public Date           TranDate          = null;
    public String         UserMessage       = null;
    
    public RiskRetrievalRecord( ResultSet resultSet ) 
      throws java.sql.SQLException
    {
      super( resultSet );
             
      Amount            = resultSet.getDouble("tran_amount");
      CardNumber        = resultSet.getString("card_number");
      TranDate          = resultSet.getDate("tran_date");
      ReferenceNumber   = resultSet.getString("ref_num");
      IncomingDate      = resultSet.getDate("incoming_date");
      ReasonCode        = resultSet.getString("reason_code");
      ReasonDescription = resultSet.getString("reason_desc");
      UserMessage       = resultSet.getString("user_message");
    }                                   
  
    public void showData( java.io.PrintStream out )
    {
    }  
  }
  
  public RiskRetrievalDataBean( )
  {
  }

  public void loadData( int reportStyle, long orgId, Date beginDate, Date endDate, String[] args )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( reportStyle == RISK_STYLE_ITEM_AMOUNT )
      {
        double  amountThreshold = 500;
        
        try
        { 
          amountThreshold = Double.parseDouble( args[0] ); 
        } 
        catch( Exception e ) 
        {
          try
          {
            amountThreshold = MesDefaults.getDouble( MesDefaults.DK_RISK_OVER_AMOUNT_DEFAULT );
          }
          catch( Exception ee )
          {
            logEntry( "loadData[ amountThreshold ]", ee.toString() );
          }
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:101^9*/

//  ************************************************************
//  #sql [Ctx] it = { select rt.merchant_number               as merchant_number,
//                   mf.dba_name                      as dba_name,
//                   mf.sic_code                      as sic_code,
//                   rt.incoming_date                 as incoming_date,
//                   rt.REASON_CODE                   as reason_code,
//                   rd.REASON_DESC                   as reason_desc,
//                   rt.tran_amount                   as tran_amount,
//                   rt.card_number                   as card_number,
//                   rt.tran_date                     as tran_date,
//                   rt.reference_number              as ref_num,
//                   rt.user_message                  as user_message,
//                   (mf.bank_number || mf.dmagent)   as assoc_number
//            from   group_merchant         gm,
//                   group_rep_merchant     grm,
//                   network_retrievals     rt,
//                   mif                    mf,
//                   chargeback_reason_desc rd
//            where  gm.org_num = :orgId and 
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                   rt.merchant_number = gm.merchant_number and
//                   rt.incoming_date between :beginDate and :endDate and
//                   rt.tran_amount > :amountThreshold and
//                   mf.merchant_number(+) = rt.merchant_number and
//                   rd.reason_code(+) = rt.reason_code and
//                   rd.card_type(+) = decode( substr(rt.card_number,1,1),'4','VS','5','MC','DC') and
//                   rd.item_type(+) = 'R'
//            order by mf.dba_name, rt.merchant_number, rt.incoming_date desc        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select rt.merchant_number               as merchant_number,\n                 mf.dba_name                      as dba_name,\n                 mf.sic_code                      as sic_code,\n                 rt.incoming_date                 as incoming_date,\n                 rt.REASON_CODE                   as reason_code,\n                 rd.REASON_DESC                   as reason_desc,\n                 rt.tran_amount                   as tran_amount,\n                 rt.card_number                   as card_number,\n                 rt.tran_date                     as tran_date,\n                 rt.reference_number              as ref_num,\n                 rt.user_message                  as user_message,\n                 (mf.bank_number || mf.dmagent)   as assoc_number\n          from   group_merchant         gm,\n                 group_rep_merchant     grm,\n                 network_retrievals     rt,\n                 mif                    mf,\n                 chargeback_reason_desc rd\n          where  gm.org_num =  :1  and \n                 grm.user_id(+) =  :2  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :3  = -1 ) and        \n                 rt.merchant_number = gm.merchant_number and\n                 rt.incoming_date between  :4  and  :5  and\n                 rt.tran_amount >  :6  and\n                 mf.merchant_number(+) = rt.merchant_number and\n                 rd.reason_code(+) = rt.reason_code and\n                 rd.card_type(+) = decode( substr(rt.card_number,1,1),'4','VS','5','MC','DC') and\n                 rd.item_type(+) = 'R'\n          order by mf.dba_name, rt.merchant_number, rt.incoming_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.RiskRetrievalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setDouble(6,amountThreshold);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.RiskRetrievalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:132^9*/
      }
      else if ( reportStyle == RISK_STYLE_ITEM_RATIO )
      {
        int     minCount          = 0;
        double  percentThreshold  = 1;
        
        try
        { 
          percentThreshold = Double.parseDouble( args[0] ); 
        } 
        catch( Exception e ) 
        {
          try
          {
            percentThreshold = MesDefaults.getDouble( MesDefaults.DK_RISK_OVER_PERCENT_DEFAULT );
          }
          catch( Exception ee )
          {
            logEntry( "loadData[ percentThreshold ]", ee.toString() );
          }            
        }
        
        try
        { 
          minCount = Integer.parseInt( args[1] ); 
        } 
        catch( Exception e ) 
        {
          minCount = 0;
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:164^9*/

//  ************************************************************
//  #sql [Ctx] it = { select rt.merchant_number               as merchant_number,
//                   mf.dba_name                      as dba_name,
//                   mf.sic_code                      as sic_code,
//                   rt.incoming_date                 as incoming_date,
//                   rt.REASON_CODE                   as reason_code,
//                   rd.REASON_DESC                   as reason_desc,
//                   rt.tran_amount                   as tran_amount,
//                   rt.card_number                   as card_number,
//                   rt.tran_date                     as tran_date,
//                   rt.reference_number              as ref_num,
//                   rt.user_message                  as user_message,
//                   (mf.bank_number || mf.dmagent)   as assoc_number
//            from   group_merchant         gm,
//                   group_rep_merchant     grm,
//                   network_retrievals     rt,
//                   mif                    mf,
//                   chargeback_reason_desc rd
//            where  gm.org_num = :orgId and 
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                   rt.merchant_number = gm.merchant_number and
//                   rt.incoming_date between :beginDate and :endDate and
//                   is_risk_merchant( rt.merchant_number, :beginDate, :endDate, :percentThreshold, :RISK_TYPE_RETRIEVAL, :RISK_STYLE_ITEM_RATIO, :minCount ) = 1 and 
//                   mf.merchant_number(+) = rt.merchant_number and
//                   rd.reason_code(+) = rt.reason_code and
//                   rd.card_type(+) = decode( substr(rt.card_number,1,1),'4','VS','5','MC','DC') and
//                   rd.item_type(+) = 'R'
//            order by mf.dba_name, rt.merchant_number, rt.incoming_date desc        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select rt.merchant_number               as merchant_number,\n                 mf.dba_name                      as dba_name,\n                 mf.sic_code                      as sic_code,\n                 rt.incoming_date                 as incoming_date,\n                 rt.REASON_CODE                   as reason_code,\n                 rd.REASON_DESC                   as reason_desc,\n                 rt.tran_amount                   as tran_amount,\n                 rt.card_number                   as card_number,\n                 rt.tran_date                     as tran_date,\n                 rt.reference_number              as ref_num,\n                 rt.user_message                  as user_message,\n                 (mf.bank_number || mf.dmagent)   as assoc_number\n          from   group_merchant         gm,\n                 group_rep_merchant     grm,\n                 network_retrievals     rt,\n                 mif                    mf,\n                 chargeback_reason_desc rd\n          where  gm.org_num =  :1  and \n                 grm.user_id(+) =  :2  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :3  = -1 ) and        \n                 rt.merchant_number = gm.merchant_number and\n                 rt.incoming_date between  :4  and  :5  and\n                 is_risk_merchant( rt.merchant_number,  :6 ,  :7 ,  :8 ,  :9 ,  :10 ,  :11  ) = 1 and \n                 mf.merchant_number(+) = rt.merchant_number and\n                 rd.reason_code(+) = rt.reason_code and\n                 rd.card_type(+) = decode( substr(rt.card_number,1,1),'4','VS','5','MC','DC') and\n                 rd.item_type(+) = 'R'\n          order by mf.dba_name, rt.merchant_number, rt.incoming_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.RiskRetrievalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setDate(6,beginDate);
   __sJT_st.setDate(7,endDate);
   __sJT_st.setDouble(8,percentThreshold);
   __sJT_st.setInt(9,RISK_TYPE_RETRIEVAL);
   __sJT_st.setInt(10,RISK_STYLE_ITEM_RATIO);
   __sJT_st.setInt(11,minCount);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.RiskRetrievalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:195^9*/
      }
      else if ( reportStyle == RISK_STYLE_DETAILS )
      {      
        /*@lineinfo:generated-code*//*@lineinfo:199^9*/

//  ************************************************************
//  #sql [Ctx] it = { select rt.merchant_number               as merchant_number,
//                   mf.dba_name                      as dba_name,
//                   mf.sic_code                      as sic_code,
//                   rt.incoming_date                 as incoming_date,
//                   rt.REASON_CODE                   as reason_code,
//                   rd.REASON_DESC                   as reason_desc,
//                   rt.tran_amount                   as tran_amount,
//                   rt.card_number                   as card_number,
//                   rt.tran_date                     as tran_date,
//                   rt.reference_number              as ref_num,
//                   rt.user_message                  as user_message,
//                   (mf.bank_number || mf.dmagent)   as assoc_number
//            from   group_merchant         gm,
//                   group_rep_merchant     grm,
//                   network_retrievals     rt,
//                   mif                    mf,
//                   chargeback_reason_desc rd
//            where  gm.org_num = :orgId and 
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                   rt.merchant_number = gm.merchant_number and
//                   rt.incoming_date between :beginDate and :endDate and
//                   mf.merchant_number(+) = rt.merchant_number and
//                   rd.reason_code(+) = rt.reason_code and
//                   rd.card_type(+) = decode( substr(rt.card_number,1,1),'4','VS','5','MC','DC') and
//                   rd.item_type(+) = 'R'
//            order by mf.dba_name, rt.merchant_number, rt.incoming_date desc        
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select rt.merchant_number               as merchant_number,\n                 mf.dba_name                      as dba_name,\n                 mf.sic_code                      as sic_code,\n                 rt.incoming_date                 as incoming_date,\n                 rt.REASON_CODE                   as reason_code,\n                 rd.REASON_DESC                   as reason_desc,\n                 rt.tran_amount                   as tran_amount,\n                 rt.card_number                   as card_number,\n                 rt.tran_date                     as tran_date,\n                 rt.reference_number              as ref_num,\n                 rt.user_message                  as user_message,\n                 (mf.bank_number || mf.dmagent)   as assoc_number\n          from   group_merchant         gm,\n                 group_rep_merchant     grm,\n                 network_retrievals     rt,\n                 mif                    mf,\n                 chargeback_reason_desc rd\n          where  gm.org_num =  :1  and \n                 grm.user_id(+) =  :2  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :3  = -1 ) and        \n                 rt.merchant_number = gm.merchant_number and\n                 rt.incoming_date between  :4  and  :5  and\n                 mf.merchant_number(+) = rt.merchant_number and\n                 rd.reason_code(+) = rt.reason_code and\n                 rd.card_type(+) = decode( substr(rt.card_number,1,1),'4','VS','5','MC','DC') and\n                 rd.item_type(+) = 'R'\n          order by mf.dba_name, rt.merchant_number, rt.incoming_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.RiskRetrievalDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.reports.RiskRetrievalDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:229^9*/
      }        
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.add( new RiskRetrievalRecord( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void setReportStyle( int reportStyle )
  {
    super.setReportStyle( reportStyle );
    
    switch( ReportStyle )
    {
      case RISK_STYLE_ITEM_AMOUNT:
        ReportTitle = "Retrieval Risk Large Tickets";
        QueryParamLabels = new String[] { "Item Amount Exceeds: $" };
        
        // create the input fields
        fields.add( new NumberField("queryParam0",6,6,false,0) );
    
        // set the default values
        fields.getField("queryParam0").setData("500");
        
        break;
        
      case RISK_STYLE_ITEM_RATIO:
        ReportTitle = "Retrieval Risk High Volume";
        QueryParamLabels = new String[] { "Item Amounts Exceed Volume By: %",
                                          "Minimun Number of Items:" };
                             
        // create the input fields
        fields.add( new NumberField("queryParam0",6,6,false,0) );
        fields.add( new NumberField("queryParam1",6,6,false,0) );
    
        // set the default values
        fields.getField("queryParam0").setData("1");
        fields.getField("queryParam1").setData("3");
        break;
        
      case RISK_STYLE_DETAILS:
        ReportTitle = "Retrieval Risk Report";
        break;
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/