/*@lineinfo:filename=RiskBaseDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/RiskBaseDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import com.mes.constants.MesUsers;
import com.mes.forms.Field;
import com.mes.support.DateTimeFormatter;
import com.mes.support.HttpHelper;
import com.mes.tools.HierarchyTree;
import com.mes.user.UserBean;

public abstract class RiskBaseDataBean extends ReportSQLJBean
{
  //
  //  Risk types define the bean that will be used to collect the
  //  data for the JSP report.  
  //  Add a new RISK_TYPE when you are defining a new data bean.
  //
  public static final int     RISK_TYPE_INVALID             = -1;
  public static final int     RISK_TYPE_CHARGEBACK          = 0;
  public static final int     RISK_TYPE_RETRIEVAL           = 1;
  public static final int     RISK_TYPE_TRANSACTIONS        = 2;
  public static final int     RISK_TYPE_ACH                 = 3;
  public static final int     RISK_TYPE_AUTH                = 4;
  public static final int     RISK_TYPE_DEFAULT             = RISK_TYPE_CHARGEBACK;
  
  //
  //  Risk styles define the queries that are used and the 
  //  "label/default param sets" that are returned to the JSP.
  //  Add a new RISK_STYLE when you want to define a new query.
  // 
  public static final int   RISK_STYLE_ITEM_AMOUNT          = 0;
  public static final int   RISK_STYLE_ITEM_RATIO           = 1;
  public static final int   RISK_STYLE_CREDITS_AMOUNT       = 2;
  public static final int   RISK_STYLE_CREDITS_RATIO        = 3;
  public static final int   RISK_STYLE_LARGE_TICKET_KEYED   = 4;
  public static final int   RISK_STYLE_KEYED_VOLUME         = 5;
  public static final int   RISK_STYLE_DETAILS              = 6;
  public static final int   RISK_STYLE_UNAUTHORIZED_TRANS   = 7;
  public static final int   RISK_STYLE_DUPLICATE_TRANS      = 8;
  public static final int   RISK_STYLE_BULLHEAD_DECLINES    = 9;
  public static final int   RISK_STYLE_UNMATCHED_CREDITS    = 10;
  public static final int   RISK_STYLE_LARGE_TICKET_SWIPED  = 11;
  public static final int   RISK_STYLE_NEW_ACCT_DEPOSITS    = 12;
  public static final int   RISK_STYLE_LARGE_DEPOSITS       = 13;
  public static final int   RISK_STYLE_AVG_DEPOSIT          = 14;
  
  // default report style is the details report
  public static final int   RISK_STYLE_DEFAULT            = RISK_STYLE_DETAILS;
  
  // 
  // Risk Thresholds are the end of the line defaults for 
  // report threshold values.  The search path for the value
  // used in the query is:
  //
  //  1) user specified
  //  2) MesDefaults 
  //  3) RISK_THRESHOLD value
  //
  public static final double  RISK_THRESHOLD_LARGE_TRANSACTION    = 200;  // percent
  
  public class RiskRecordBase implements Comparable
  {
    public long           AssocNumber       = 0L;
    public String         DbaName           = null;
    public long           MerchantId        = 0L;
    public int            SicCode           = 0;
    
    public RiskRecordBase( )
    {
    }
    
    public RiskRecordBase( ResultSet resultSet )
      throws java.sql.SQLException
    {
      setData( resultSet );
    }
    
    public int compareTo( Object obj )
    {
      RiskRecordBase      compareObj      = (RiskRecordBase)obj;
      int                 retVal          = 0;
      
      // default sort is by name, by merchant id
      if ( ( retVal = DbaName.compareTo(compareObj.DbaName) ) == 0 )
      {
        retVal = (int)(MerchantId - compareObj.MerchantId);
      }
      
      return( retVal );
    }
    
    public void setData( ResultSet resultSet )
      throws java.sql.SQLException
    {                     
      MerchantId        = resultSet.getLong("merchant_number");
      DbaName           = resultSet.getString("dba_name");
      AssocNumber       = resultSet.getLong("assoc_number");
      SicCode           = resultSet.getInt("sic_code");                                     
    }
  }
  
  //
  // QueryParamLabels : defines the query argument HTML labels.
  // ReportStyle      : defines which query within the bean will be called.
  // RiskReportType   : defines which data bean is being used.
  // ReportTitle      : defines the HTML report title
  // UsingDefaultNode : true if the report node was set to the default (3941).
  //
  protected String[]            QueryParamLabels    = null;
  protected int                 ReportStyle         = RISK_STYLE_DEFAULT;
  protected int                 RiskReportType      = RISK_TYPE_DEFAULT;
  protected String              ReportTitle         = "No Title";
  protected boolean             UsingDefaultNode    = false;
  
  public RiskBaseDataBean( )
  {
  }
  
  public RiskBaseDataBean( boolean enableFieldBean )
  {
    super( enableFieldBean );
  }
  
  public void encodeNodeUrl( StringBuffer buffer, long nodeId, Date beginDate, Date endDate )
  {
    super.encodeNodeUrl( buffer, nodeId, beginDate, endDate );
    buffer.append("&riskReportType=");
    buffer.append(getRiskReportType());
    buffer.append("&reportStyle=");
    buffer.append(getReportStyle());
    
    try
    { 
      
      for( int i = 0; i < getQueryParamCount(); ++i )
      {
        buffer.append("&queryParam");
        buffer.append(i);
        buffer.append("=");
        buffer.append(fields.getField("queryParam"+i).getData());
      }
    }
    catch(Exception e)
    {
    }
  }

  public String getQueryParam( int index )
  {
    String    retVal = "";
    
    try
    {
      retVal = fields.getField("queryParam"+index).getData();
    }
    catch( Exception e )
    {
      // do nothing, just return blank string
    }
    return( retVal );
  }
  
  public int getQueryParamCount( )
  {
    int       retVal  = 0;
    
    try
    {
      retVal = fields.size();
    }
    catch( Exception e )
    {
      // just return 0
    }
    return( retVal );
  }

  public String getQueryParamLabel( int index )
  {
    String          paramLabel = null;
    
    try
    {
      paramLabel = QueryParamLabels[index];
    }
    catch( Exception e )
    {
      paramLabel = ("Invalid index " + index);
    }
    return( paramLabel );
  }
  
  public int getReportStyle( )
  {
    Field     f       = getField("reportStyle");
    int       retVal  = -1;
    
    if ( f != null )
    {
      retVal = f.asInteger();
    }
    else
    {
      retVal = ReportStyle;
    }
    return( retVal );
  }
  
  public String getReportTitle( )
  {
    return( ReportTitle );
  }
  
  public int getRiskReportType()
  {
    Field   f       = getField("riskReportType");
    int     retVal  = -1;
    
    if ( f != null )
    {
      retVal = f.asInteger();
    }
    else
    {
      retVal = RiskReportType;
    }
    return( retVal );
  }
  
  public void loadData( )
  { 
    String[]        queryArgs   = null;
    
    try
    {
      queryArgs = new String[ fields.size() ];
      
      // convert the FieldGroup data into an array
      // of strings for use by the existing queries
      for ( int i = 0; i < fields.size(); ++i )
      {
        queryArgs[i] = fields.getField("queryParam"+i).getData();
      }
    }
    catch( Exception e )
    {
    }
    
    // call loadData in the derived class
    loadData( getReportStyle(), getReportOrgId(), getReportDateBegin(), getReportDateEnd(), queryArgs );
  }
  
  public abstract void loadData( int reportStyle, long orgId, Date beginDate, Date endDate, String[] args );
  
  public void setProperties(HttpServletRequest request)
  {
    Calendar        cal           = Calendar.getInstance();
    String          paramValue    = null;
    
    // load the default report properties
    super.setProperties( request );
    
    // it is important to load the report type and report style
    // before setting up the query args because getQueryParamDefault
    // uses these values to determine what the query param defaults
    // should be.
    RiskReportType  = HttpHelper.getInt(request, "riskReportType", RISK_TYPE_DEFAULT);
    setReportStyle( HttpHelper.getInt(request, "reportStyle", RISK_STYLE_DETAILS) );
    
    // setting the report style has initialized the form 
    // fields so set them with data from the user request.
    setFields(request);

    // if the from date is today, then set the default to rolling previous week
    if ( usingDefaultReportDates() )
    {
      // setup the default date range
      cal.setTime( getReportDateBegin() );
      
      switch( getReportStyle() )
      {
        case RISK_STYLE_DETAILS:
          cal.add( Calendar.DAY_OF_MONTH, -1 );
          break;
      
  //      case RISK_STYLE_ITEM_AMOUNT:
  //      case RISK_STYLE_ITEM_RATIO:
        default:
          cal.add( Calendar.DAY_OF_MONTH, -7 );
          break;
      }
      setReportDateBegin( new java.sql.Date( cal.getTime().getTime() ) );
    }    
    
    // set the default portfolio id
    if ( getReportHierarchyNode() == HierarchyTree.DEFAULT_HIERARCHY_NODE )
    {
      UsingDefaultNode = true;
      setReportHierarchyNode( 394100000 );
    }
  }
  
  protected void setReportStyle( int reportStyle )
  {
    Field   f = getField("reportStyle");
    
    if ( f != null )
    {
      f.setData( String.valueOf(reportStyle) );
    }
    else
    {
      ReportStyle = reportStyle;
    }      
  }
  
  public boolean showContactInfo( )
  {
    return( getReportStyle() >= RISK_STYLE_ITEM_AMOUNT );
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
  
  public boolean usingDefaultNode( )
  {
    return( UsingDefaultNode );
  }
  
  public static String showCIFLink(UserBean user, RiskChargebackDataBean.RiskChargebackRecord chargebackEntry)
  {
    StringBuffer  result = new StringBuffer("");
    
    try
    {
      if( user.hasRight(MesUsers.RIGHT_ACCOUNT_LOOKUP) )
      {
        result.append("<a target=\"_blank\" href=\"/jsp/maintenance/view_account.jsp?merchant=");
        result.append(Long.toString(chargebackEntry.MerchantId));
        result.append("\">");
        result.append(chargebackEntry.DbaName);
        result.append("</a>");
      }
      else
      {
        // just show description, no link
        result.append(chargebackEntry.DbaName);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("RiskBaseDataBean.showCIFLink(" + chargebackEntry.MerchantId + ", " + user.getLoginName() + ")", e.toString());
      result.setLength(0);
      result.append(chargebackEntry.DbaName);
    }
    
    return( result.toString() );
  }
  
  public static String showCIFLink(UserBean user, RiskRetrievalDataBean.RiskRetrievalRecord retrEntry)
  {
    StringBuffer  result = new StringBuffer("");
    
    try
    {
      if( user.hasRight(MesUsers.RIGHT_ACCOUNT_LOOKUP) )
      {
        result.append("<a target=\"_blank\" href=\"/jsp/maintenance/view_account.jsp?merchant=");
        result.append(Long.toString(retrEntry.MerchantId));
        result.append("\">");
        result.append(retrEntry.DbaName);
        result.append("</a>");
      }
      else
      {
        // just show description, no link
        result.append(retrEntry.DbaName);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("RiskBaseDataBean.showCIFLink(" + retrEntry.MerchantId + ", " + user.getLoginName() + ")", e.toString());
      result.setLength(0);
      result.append(retrEntry.DbaName);
    }
    
    return( result.toString() );
  }
  
  public static String showRetrievalLookupLink(UserBean user, RiskChargebackDataBean.RiskChargebackRecord chargebackEntry)
  {
    StringBuffer result = new StringBuffer("");
    try
    {
      if( user.hasRight(MesUsers.RIGHT_REPORT_RISK) || user.hasRight(MesUsers.RIGHT_REPORT_BANK) )
      {
        result.append("<a target=\"retr_lookup\" href=\"/jsp/reports/merch_retrieval_lookup.jsp?ref=");
        result.append(chargebackEntry.ReferenceNumber);
        result.append("&card=");
        result.append(chargebackEntry.CardNumber);
        result.append("&tdate=");
        result.append(DateTimeFormatter.getFormattedDate(chargebackEntry.TranDate,"MMddyyyy"));
        result.append("&merchant=");
        result.append(chargebackEntry.MerchantId);
        result.append("\">");
        result.append(chargebackEntry.ReferenceNumber);
        result.append("</a>");
      }
      else
      {
        result.append(chargebackEntry.ReferenceNumber);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("RiskBaseDataBean.showRetrievalLookupLink(" + chargebackEntry.MerchantId + ", " + user.getLoginName() + ")", e.toString());
      result.setLength(0);
      result.append(chargebackEntry.ReferenceNumber);
    }
    
    return( result.toString() );
  }

  public static String showRetrievalLookupLink(UserBean user, RiskRetrievalDataBean.RiskRetrievalRecord retrEntry)
  {
    StringBuffer result = new StringBuffer("");
    try
    {
      if( user.hasRight(MesUsers.RIGHT_REPORT_RISK) || user.hasRight(MesUsers.RIGHT_REPORT_BANK) )
      {
        result.append("<a target=\"retr_lookup\" href=\"/jsp/reports/merch_retrieval_lookup.jsp?ref=");
        result.append(retrEntry.ReferenceNumber);
        result.append("&card=");
        result.append(retrEntry.CardNumber);
        result.append("&tdate=");
        result.append(DateTimeFormatter.getFormattedDate(retrEntry.TranDate,"MMddyyyy"));
        result.append("&merchant=");
        result.append(retrEntry.MerchantId);
        result.append("\">");
        result.append(retrEntry.ReferenceNumber);
        result.append("</a>");
      }
      else
      {
        result.append(retrEntry.ReferenceNumber);
      }
    }
    catch(Exception e)
    {
      com.mes.support.SyncLog.LogEntry("RiskBaseDataBean.showRetrievalLookupLink(" + retrEntry.MerchantId + ", " + user.getLoginName() + ")", e.toString());
      result.setLength(0);
      result.append(retrEntry.ReferenceNumber);
    }
    
    return( result.toString() );
  }
}/*@lineinfo:generated-code*/