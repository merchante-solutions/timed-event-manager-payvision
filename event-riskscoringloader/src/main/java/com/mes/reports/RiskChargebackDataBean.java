/*@lineinfo:filename=RiskChargebackDataBean*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/reports/RiskChargebackDataBean.sqlj $

  Description:  


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.reports;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Calendar;
import com.mes.config.MesDefaults;
import com.mes.forms.NumberField;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class RiskChargebackDataBean extends RiskBaseDataBean
{
  public class RiskChargebackRecord extends RiskRecordBase
  {
    public String         ActionCode        = null;
    public Date           AdjustmentDate    = null;
    public double         Amount            = 0.0;
    public String         CardNumber        = null;
    public boolean        FirstTime         = false;
    public String         FirstTimeInd      = null;
    public Date           IncomingDate      = null;
    public String         ReasonCode        = null;
    public String         ReasonDescription = null;
    public String         ReferenceNumber   = null;
    public Date           TranDate          = null;
    public String         UserMessage       = null;
    
    public RiskChargebackRecord( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );
      
      Amount            = resultSet.getDouble("tran_amount");
      CardNumber        = resultSet.getString("card_number");
      TranDate          = resultSet.getDate("tran_date");
      ReferenceNumber   = resultSet.getString("ref_num");
      IncomingDate      = resultSet.getDate("incoming_date");
      ReasonCode        = processString(resultSet.getString("reason_code"));
      ReasonDescription = processString(resultSet.getString("reason_desc"));
      AdjustmentDate    = resultSet.getDate("adj_date");
      UserMessage       = resultSet.getString("user_message");
      FirstTime         = resultSet.getString("first_time").equals("Y");
      FirstTimeInd      = resultSet.getString("first_time");
      ActionCode        = resultSet.getString("action_code");
    }                                   
  
    public void showData( java.io.PrintStream out )
    {
    }  
  }
  
  public RiskChargebackDataBean( )
  {
  }
  
  protected void encodeHeaderCSV( StringBuffer line )
  {
    line.setLength(0);
    line.append("\"Merchant Id\",");
    line.append("\"DBA Name\",");
    line.append("\"Assoc\",");
    line.append("\"Incoming Date\",");
    line.append("\"First Time\",");
    line.append("\"Reason Description\",");
    line.append("\"Amount\",");
    line.append("\"Card Number\",");
    line.append("\"Tran Date\",");
    line.append("\"Reference\",");
    line.append("\"Adj Date\",");
    line.append("\"Member Message\"");
  }
  
  protected void encodeLineCSV( Object obj, StringBuffer line )
  {
    RiskChargebackRecord    record = (RiskChargebackRecord)obj;
    
    line.setLength(0);
    line.append( record.MerchantId );
    line.append( ",\"" );
    line.append( record.DbaName );
    line.append( "\"," );
    line.append( record.AssocNumber );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.IncomingDate,"MM/dd/yyyy") );
    line.append( ",\"" );
    line.append( record.FirstTimeInd );
    line.append( "\",\"" );
    line.append( record.ReasonDescription );
    line.append( "\"," );
    line.append( record.Amount );
    line.append( ",\"" );
    line.append( record.CardNumber );
    line.append( "\"," );
    line.append( DateTimeFormatter.getFormattedDate(record.TranDate,"MM/dd/yyyy") );
    line.append( "," );
    line.append( record.ReferenceNumber );
    line.append( "," );
    line.append( DateTimeFormatter.getFormattedDate(record.AdjustmentDate,"MM/dd/yyyy") );
    line.append( ",\"" );
    line.append( record.UserMessage );
    line.append( "\"" );
  }
  
  public String getDownloadFilenameBase()
  {
    Calendar                cal       = Calendar.getInstance();  
    StringBuffer            filename  = new StringBuffer("");
    
    filename.append( getReportHierarchyNode() );
    
    switch( ReportStyle )
    {
      case RISK_STYLE_ITEM_AMOUNT:
        filename.append("_large_ticket_chargebacks_");
        break;
        
      case RISK_STYLE_ITEM_RATIO:
        filename.append("_high_volume_chargebacks_");
        break;
        
//      case RISK_STYLE_DETAILS:
      default:
        filename.append("_risk_chargebacks_");
        break;
    }
    
    // build the first date into the filename
    filename.append( DateTimeFormatter.getFormattedDate( ReportDateBegin,"MMddyy" ) );
    if ( ReportDateBegin.equals(ReportDateEnd) == false )
    {
      filename.append("_to_");
      filename.append( DateTimeFormatter.getFormattedDate( ReportDateEnd,"MMddyy" ) );
    }      
    return ( filename.toString() );
  }
  
  public boolean isEncodingSupported( int fileFormat )
  {
    boolean     retVal = false;
    switch( fileFormat )
    {
      case FF_CSV:
        retVal = true;
        break;
    }
    return(retVal);
  }
  
  public void loadData( int reportStyle, long orgId, Date beginDate, Date endDate, String[] args )
  {
    ResultSetIterator             it                = null;
    ResultSet                     resultSet         = null;
    
    try
    {
      // empty the current contents
      ReportRows.clear();
      
      if ( reportStyle == RISK_STYLE_ITEM_AMOUNT )
      {
        double  amountThreshold = 500;
        
        try
        { 
          amountThreshold = Double.parseDouble( args[0] ); 
        } 
        catch( Exception e ) 
        {
          try
          { 
            amountThreshold = MesDefaults.getDouble( MesDefaults.DK_RISK_OVER_AMOUNT_DEFAULT );
          }
          catch( Exception ee )
          {
            logEntry( "loadData[ amountThreshold ]", ee.toString() );
          }
        }
        
        /*@lineinfo:generated-code*//*@lineinfo:205^9*/

//  ************************************************************
//  #sql [Ctx] it = { select mf.merchant_number                 as merchant_number,
//                   mf.dba_name                        as dba_name,
//                   mf.sic_code                        as sic_code,
//                   cb.incoming_date                   as incoming_date,
//                   cb.REASON_CODE                     as reason_code,
//                   rd.REASON_DESC                     as reason_desc,
//                   cb.tran_amount                     as tran_amount,
//                   cb.card_number                     as card_number,
//                   cb.tran_date                       as tran_date,
//                   nvl(cb.first_time_chargeback,'N')  as first_time,
//                   cb.reference_number                as ref_num,
//                   cba.user_message                   as user_message,
//                   nvl(ac.ACTION_CODE,
//                       nvl(cba.action_code,'PEND'))   as action_code,
//                   cba.adj_batch_date                 as adj_date,
//                   (mf.bank_number || mf.dmagent)     as assoc_number
//            from   group_merchant                 gm,
//                   group_rep_merchant             grm,
//                   network_chargebacks            cb,
//                   network_chargeback_activity    cba,
//                   chargeback_reason_desc         rd,
//                   chargeback_action_codes        ac,
//                   mif                            mf
//            where  gm.org_num = :orgId and 
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                   cb.merchant_number = gm.merchant_number and
//                   cb.incoming_date between :beginDate and :endDate and
//                   cb.tran_amount > :amountThreshold and 
//                   cba.cb_load_sec(+) = cb.cb_load_sec and
//                   rd.card_type(+) = nvl(cb.card_type,decode_card_type(cb.card_number,0)) and
//                   rd.item_type(+) = 'C' and -- chargeback reason                   
//                   rd.reason_code(+) = cb.reason_code and
//                   ac.SHORT_ACTION_CODE(+) = cba.ACTION_CODE and                 
//                   mf.merchant_number = cb.merchant_number
//            order by mf.dba_name, mf.merchant_number , cb.incoming_date desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select mf.merchant_number                 as merchant_number,\n                 mf.dba_name                        as dba_name,\n                 mf.sic_code                        as sic_code,\n                 cb.incoming_date                   as incoming_date,\n                 cb.REASON_CODE                     as reason_code,\n                 rd.REASON_DESC                     as reason_desc,\n                 cb.tran_amount                     as tran_amount,\n                 cb.card_number                     as card_number,\n                 cb.tran_date                       as tran_date,\n                 nvl(cb.first_time_chargeback,'N')  as first_time,\n                 cb.reference_number                as ref_num,\n                 cba.user_message                   as user_message,\n                 nvl(ac.ACTION_CODE,\n                     nvl(cba.action_code,'PEND'))   as action_code,\n                 cba.adj_batch_date                 as adj_date,\n                 (mf.bank_number || mf.dmagent)     as assoc_number\n          from   group_merchant                 gm,\n                 group_rep_merchant             grm,\n                 network_chargebacks            cb,\n                 network_chargeback_activity    cba,\n                 chargeback_reason_desc         rd,\n                 chargeback_action_codes        ac,\n                 mif                            mf\n          where  gm.org_num =  :1  and \n                 grm.user_id(+) =  :2  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :3  = -1 ) and        \n                 cb.merchant_number = gm.merchant_number and\n                 cb.incoming_date between  :4  and  :5  and\n                 cb.tran_amount >  :6  and \n                 cba.cb_load_sec(+) = cb.cb_load_sec and\n                 rd.card_type(+) = nvl(cb.card_type,decode_card_type(cb.card_number,0)) and\n                 rd.item_type(+) = 'C' and -- chargeback reason                   \n                 rd.reason_code(+) = cb.reason_code and\n                 ac.SHORT_ACTION_CODE(+) = cba.ACTION_CODE and                 \n                 mf.merchant_number = cb.merchant_number\n          order by mf.dba_name, mf.merchant_number , cb.incoming_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.reports.RiskChargebackDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   __sJT_st.setDouble(6,amountThreshold);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.reports.RiskChargebackDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:244^9*/
      }
      else if ( reportStyle == RISK_STYLE_ITEM_RATIO )
      {
        Calendar  cal               = Calendar.getInstance();
        double    cbVolAmount       = 0.0;
        int       minCount          = 0;
        double    percentThreshold  = 1;
        long      seqNum            = 0L;
        double    volAmount         = 0.0;
        Date      volDate           = null;
        
        try
        { 
          percentThreshold = Double.parseDouble( args[0] ); 
        } 
        catch( Exception e ) 
        {
          try
          {
            percentThreshold = MesDefaults.getDouble( MesDefaults.DK_RISK_OVER_PERCENT_DEFAULT );
          }
          catch( Exception ee )
          {
            logEntry( "loadData[ percentThreshold ]", ee.toString() );
          }            
        }
        
        try
        { 
          minCount = Integer.parseInt( args[1] ); 
        } 
        catch( Exception e ) 
        {
          minCount = 0;
        }
        
        cal.setTime( endDate );
        cal.add(Calendar.MONTH,-1);
        cal.set(Calendar.DAY_OF_MONTH,1);
        volDate = new java.sql.Date( cal.getTime().getTime() );
        
        /*@lineinfo:generated-code*//*@lineinfo:286^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  /*+ 
//                      index (gm pkgroup_merchant) 
//                    */
//                    cb.merchant_number          as merchant_number,
//                    sum(cb.tran_amount)         as cb_amount,
//                    count(cb.tran_amount)       as cb_count,
//                    bank_sales_amount_monthly(:volDate,cb.merchant_number)
//                                                as vol_amount
//            from    group_merchant              gm,
//                    group_rep_merchant          grm,
//                    network_chargebacks         cb
//            where   gm.org_num = :orgId and
//                    grm.user_id(+) = :AppFilterUserId and
//                    grm.merchant_number(+) = gm.merchant_number and
//                    ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                    cb.merchant_number = gm.merchant_number and
//                    cb.incoming_date between :beginDate and :endDate
//            group by cb.merchant_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  /*+ \n                    index (gm pkgroup_merchant) \n                  */\n                  cb.merchant_number          as merchant_number,\n                  sum(cb.tran_amount)         as cb_amount,\n                  count(cb.tran_amount)       as cb_count,\n                  bank_sales_amount_monthly( :1 ,cb.merchant_number)\n                                              as vol_amount\n          from    group_merchant              gm,\n                  group_rep_merchant          grm,\n                  network_chargebacks         cb\n          where   gm.org_num =  :2  and\n                  grm.user_id(+) =  :3  and\n                  grm.merchant_number(+) = gm.merchant_number and\n                  ( not grm.user_id is null or  :4  = -1 ) and        \n                  cb.merchant_number = gm.merchant_number and\n                  cb.incoming_date between  :5  and  :6 \n          group by cb.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.reports.RiskChargebackDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,volDate);
   __sJT_st.setLong(2,orgId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setLong(4,AppFilterUserId);
   __sJT_st.setDate(5,beginDate);
   __sJT_st.setDate(6,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.reports.RiskChargebackDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:306^9*/
        resultSet = it.getResultSet();
        
        /*@lineinfo:generated-code*//*@lineinfo:309^9*/

//  ************************************************************
//  #sql [Ctx] { select cb_high_vol_sequence.nextval  from dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select cb_high_vol_sequence.nextval   from dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.reports.RiskChargebackDataBean",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   seqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:312^9*/
        
        while( resultSet.next() )
        {
          cbVolAmount = resultSet.getDouble("cb_amount");
          volAmount   = resultSet.getDouble("vol_amount");
          
          // if the volume amount is zero or the ratio of chargebacks
          // to transactions exceeds the threshold percentage and
          // the number of chargebacks exceeds the minimum number
          // required to be triggered, then insert this merchant
          // number into the temporary table.
          if ( ( ( volAmount == 0.0 ) || ( (cbVolAmount/volAmount) > (percentThreshold * 0.01) ) ) &&
               ( resultSet.getInt("cb_count") > minCount ) )
          {               
            /*@lineinfo:generated-code*//*@lineinfo:327^13*/

//  ************************************************************
//  #sql [Ctx] { insert into network_chargeback_high_vol
//                  ( seq_num, merchant_number )
//                values
//                  ( :seqNum, :resultSet.getLong("merchant_number") )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_1256 = resultSet.getLong("merchant_number");
   String theSqlTS = "insert into network_chargeback_high_vol\n                ( seq_num, merchant_number )\n              values\n                (  :1 ,  :2  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.reports.RiskChargebackDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,seqNum);
   __sJT_st.setLong(2,__sJT_1256);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:333^13*/
          }            
        }
        resultSet.close();
        it.close();
        
        /*@lineinfo:generated-code*//*@lineinfo:339^9*/

//  ************************************************************
//  #sql [Ctx] it = { select mf.merchant_number                 as merchant_number,
//                   mf.dba_name                        as dba_name,
//                   mf.sic_code                        as sic_code,
//                   cb.incoming_date                   as incoming_date,
//                   cb.REASON_CODE                     as reason_code,
//                   rd.REASON_DESC                     as reason_desc,
//                   cb.tran_amount                     as tran_amount,
//                   cb.card_number                     as card_number,
//                   cb.tran_date                       as tran_date,
//                   nvl(cb.first_time_chargeback,'N')  as first_time,
//                   cb.reference_number                as ref_num,
//                   cba.user_message                   as user_message,
//                   nvl(ac.ACTION_CODE,
//                       nvl(cba.action_code,'PEND'))   as action_code,
//                   cba.adj_batch_date                 as adj_date,
//                   (mf.bank_number || mf.dmagent)     as assoc_number
//            from   network_chargeback_high_vol    hv,
//                   network_chargebacks            cb,
//                   network_chargeback_activity    cba,
//                   chargeback_reason_desc         rd,
//                   chargeback_action_codes        ac,
//                   mif                            mf
//            where  hv.seq_num = :seqNum and
//                   cb.merchant_number = hv.merchant_number and
//                   cb.incoming_date between :beginDate and :endDate and
//  --                 is_risk_merchant( cb.merchant_number, :beginDate, :endDate, :percentThreshold, :(RISK_TYPE_CHARGEBACK), :(RISK_STYLE_ITEM_RATIO), :minCount ) = 1 and 
//                   cba.cb_load_sec(+) = cb.cb_load_sec and
//                   rd.card_type(+) = nvl(cb.card_type,decode_card_type(cb.card_number,0)) and
//                   rd.item_type(+) = 'C' and -- chargeback reason                   
//                   rd.reason_code(+) = cb.reason_code and
//                   ac.SHORT_ACTION_CODE(+) = cba.ACTION_CODE and                 
//                   mf.merchant_number = cb.merchant_number
//            order by mf.dba_name, mf.merchant_number , cb.incoming_date desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select mf.merchant_number                 as merchant_number,\n                 mf.dba_name                        as dba_name,\n                 mf.sic_code                        as sic_code,\n                 cb.incoming_date                   as incoming_date,\n                 cb.REASON_CODE                     as reason_code,\n                 rd.REASON_DESC                     as reason_desc,\n                 cb.tran_amount                     as tran_amount,\n                 cb.card_number                     as card_number,\n                 cb.tran_date                       as tran_date,\n                 nvl(cb.first_time_chargeback,'N')  as first_time,\n                 cb.reference_number                as ref_num,\n                 cba.user_message                   as user_message,\n                 nvl(ac.ACTION_CODE,\n                     nvl(cba.action_code,'PEND'))   as action_code,\n                 cba.adj_batch_date                 as adj_date,\n                 (mf.bank_number || mf.dmagent)     as assoc_number\n          from   network_chargeback_high_vol    hv,\n                 network_chargebacks            cb,\n                 network_chargeback_activity    cba,\n                 chargeback_reason_desc         rd,\n                 chargeback_action_codes        ac,\n                 mif                            mf\n          where  hv.seq_num =  :1  and\n                 cb.merchant_number = hv.merchant_number and\n                 cb.incoming_date between  :2  and  :3  and\n--                 is_risk_merchant( cb.merchant_number, :beginDate, :endDate, :percentThreshold, :(RISK_TYPE_CHARGEBACK), :(RISK_STYLE_ITEM_RATIO), :minCount ) = 1 and \n                 cba.cb_load_sec(+) = cb.cb_load_sec and\n                 rd.card_type(+) = nvl(cb.card_type,decode_card_type(cb.card_number,0)) and\n                 rd.item_type(+) = 'C' and -- chargeback reason                   \n                 rd.reason_code(+) = cb.reason_code and\n                 ac.SHORT_ACTION_CODE(+) = cba.ACTION_CODE and                 \n                 mf.merchant_number = cb.merchant_number\n          order by mf.dba_name, mf.merchant_number , cb.incoming_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.reports.RiskChargebackDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,seqNum);
   __sJT_st.setDate(2,beginDate);
   __sJT_st.setDate(3,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.reports.RiskChargebackDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:374^9*/
        
        /*@lineinfo:generated-code*//*@lineinfo:376^9*/

//  ************************************************************
//  #sql [Ctx] { delete 
//            from    network_chargeback_high_vol
//            where   seq_num = :seqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete \n          from    network_chargeback_high_vol\n          where   seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.reports.RiskChargebackDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,seqNum);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:381^9*/
      }
      else if ( reportStyle == RISK_STYLE_DETAILS )
      {      
        /*@lineinfo:generated-code*//*@lineinfo:385^9*/

//  ************************************************************
//  #sql [Ctx] it = { select mf.merchant_number                 as merchant_number,
//                   mf.dba_name                        as dba_name,
//                   mf.sic_code                        as sic_code,
//                   cb.incoming_date                   as incoming_date,
//                   cb.REASON_CODE                     as reason_code,
//                   rd.REASON_DESC                     as reason_desc,
//                   cb.tran_amount                     as tran_amount,
//                   cb.card_number                     as card_number,
//                   cb.tran_date                       as tran_date,
//                   nvl(cb.first_time_chargeback,'N')  as first_time,
//                   cb.reference_number                as ref_num,
//                   cba.user_message                   as user_message,
//                   nvl(ac.ACTION_CODE,
//                       nvl(cba.action_code,'PEND'))   as action_code,
//                   cba.adj_batch_date                 as adj_date,
//                   (mf.bank_number || mf.dmagent)     as assoc_number
//            from   group_merchant                 gm,
//                   group_rep_merchant             grm,
//                   network_chargebacks            cb,
//                   network_chargeback_activity    cba,
//                   chargeback_reason_desc         rd,
//                   chargeback_action_codes        ac,
//                   mif                            mf
//            where  gm.org_num = :orgId and 
//                   grm.user_id(+) = :AppFilterUserId and
//                   grm.merchant_number(+) = gm.merchant_number and
//                   ( not grm.user_id is null or :AppFilterUserId = -1 ) and        
//                   cb.merchant_number = gm.merchant_number and
//                   cb.incoming_date between :beginDate and :endDate and
//                   cba.cb_load_sec(+) = cb.cb_load_sec and
//                   rd.card_type(+) = nvl(cb.card_type, decode_card_type(cb.card_number,0)) and
//                   rd.item_type(+) = 'C' and -- chargeback reason
//                   rd.reason_code(+) = cb.reason_code and
//                   ac.SHORT_ACTION_CODE(+) = cba.ACTION_CODE and                 
//                   mf.merchant_number = cb.merchant_number
//            order by mf.dba_name, mf.merchant_number, cb.incoming_date desc
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select mf.merchant_number                 as merchant_number,\n                 mf.dba_name                        as dba_name,\n                 mf.sic_code                        as sic_code,\n                 cb.incoming_date                   as incoming_date,\n                 cb.REASON_CODE                     as reason_code,\n                 rd.REASON_DESC                     as reason_desc,\n                 cb.tran_amount                     as tran_amount,\n                 cb.card_number                     as card_number,\n                 cb.tran_date                       as tran_date,\n                 nvl(cb.first_time_chargeback,'N')  as first_time,\n                 cb.reference_number                as ref_num,\n                 cba.user_message                   as user_message,\n                 nvl(ac.ACTION_CODE,\n                     nvl(cba.action_code,'PEND'))   as action_code,\n                 cba.adj_batch_date                 as adj_date,\n                 (mf.bank_number || mf.dmagent)     as assoc_number\n          from   group_merchant                 gm,\n                 group_rep_merchant             grm,\n                 network_chargebacks            cb,\n                 network_chargeback_activity    cba,\n                 chargeback_reason_desc         rd,\n                 chargeback_action_codes        ac,\n                 mif                            mf\n          where  gm.org_num =  :1  and \n                 grm.user_id(+) =  :2  and\n                 grm.merchant_number(+) = gm.merchant_number and\n                 ( not grm.user_id is null or  :3  = -1 ) and        \n                 cb.merchant_number = gm.merchant_number and\n                 cb.incoming_date between  :4  and  :5  and\n                 cba.cb_load_sec(+) = cb.cb_load_sec and\n                 rd.card_type(+) = nvl(cb.card_type, decode_card_type(cb.card_number,0)) and\n                 rd.item_type(+) = 'C' and -- chargeback reason\n                 rd.reason_code(+) = cb.reason_code and\n                 ac.SHORT_ACTION_CODE(+) = cba.ACTION_CODE and                 \n                 mf.merchant_number = cb.merchant_number\n          order by mf.dba_name, mf.merchant_number, cb.incoming_date desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.reports.RiskChargebackDataBean",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,orgId);
   __sJT_st.setLong(2,AppFilterUserId);
   __sJT_st.setLong(3,AppFilterUserId);
   __sJT_st.setDate(4,beginDate);
   __sJT_st.setDate(5,endDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.reports.RiskChargebackDataBean",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:423^9*/
      }        
      resultSet = it.getResultSet();
    
      while( resultSet.next() )
      {
        ReportRows.add( new RiskChargebackRecord( resultSet ) );
      }
      it.close();   // this will also close the resultSet
    }
    catch( java.sql.SQLException e )
    {
      logEntry( buildMethodName("loadData",orgId,beginDate,endDate), e.toString() );
    }
    finally
    {
      try{ it.close(); } catch( Exception e ) { }
    }
  }
  
  protected void setReportStyle( int reportStyle )
  {
    super.setReportStyle( reportStyle );
    
    switch( ReportStyle )
    {
      case RISK_STYLE_ITEM_AMOUNT:
        ReportTitle = "Chargeback Risk Large Tickets";
        QueryParamLabels = new String[] { "Item Amount Exceeds: $" };
        
        // create the input fields
        fields.add( new NumberField("queryParam0",6,6,false,0) );
    
        // set the default values
        fields.getField("queryParam0").setData("500");
        
        break;
        
      case RISK_STYLE_ITEM_RATIO:
        ReportTitle = "Chargeback Risk High Volume";
        QueryParamLabels = new String[] { "Item Amounts Threshold: %",
                                          "Minimun Number of Items:" };
                             
        // create the input fields
        fields.add( new NumberField("queryParam0",6,6,false,0) );
        fields.add( new NumberField("queryParam1",6,6,false,0) );
    
        // set the default values
        fields.getField("queryParam0").setData("1");
        fields.getField("queryParam1").setData("3");
        break;
        
      case RISK_STYLE_DETAILS:
        ReportTitle = "Chargeback Risk Report";
        break;
    }
  }
  
  public void showData( java.io.PrintStream out )
  {
  }
}/*@lineinfo:generated-code*/