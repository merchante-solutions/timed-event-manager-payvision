package com.mes.startup;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.clearing.utils.ClearingConstants;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.MesQueues;
import com.mes.flatfile.FlatFileRecord;
import com.mes.queues.QueueTools;
import com.mes.support.DateTimeFormatter;

public class AmexOptblueRESPNBuilder extends AmexFileBase {

	private static final long serialVersionUID = 1L;	
	static Logger log = Logger.getLogger(AmexOptblueRESPNBuilder.class);
	
	private String loadFilename = null; 
	private long fileBatchId = 0l; 
	
	/**
	 *  Initializes SFTP endpoints 
	 */
	public AmexOptblueRESPNBuilder() {
		
		DkOutgoingHost = MesDefaults.DK_AMEX_OUTGOING_HOST;
		DkOutgoingUser = MesDefaults.DK_AMEX_OUTGOING_USER;
		DkOutgoingPassword = MesDefaults.DK_AMEX_OUTGOING_PASSWORD;
		DkOutgoingPath = MesDefaults.DK_AMEX_OPTB_OUTGOING_PATH;
		SettlementAddrsNotify = MesEmails.MSG_ADDRS_AMEX_RETR_NOTIFY;;
		SettlementAddrsFailure = MesEmails.MSG_ADDRS_AMEX_RETR_NOTIFY;
		DkOutgoingUseBinary = false;
		DkOutgoingSendFlagFile = true;
	}

	
	/**
	 * Point of entry
	 * @return
	 */
	protected boolean processTransactions() {
		
		String said = "";
		BufferedWriter out = null;
		String outgoingFilename = null;
		
		try {
			connect( true );			
				
			// The loadFilename is internal and is used for marking records to process / interenally 
			if(loadFilename == null) {
				String filePrefix = DisputeResponseFilePrefixes.get(RESPN);
				loadFilename = generateFilename(filePrefix);
			}
			
			if(fileBatchId == 0) {
				getFileBatchId();
			}
			
			for (int i = 0; i < EventArgsVector.size(); i++) {
				said = (String) EventArgsVector.elementAt(i);	            
			}			
			
			log.debug("processing transactions for loadFilename = " + loadFilename + ", fileBatchId = " + fileBatchId + ", SAID = " + said);
			
			markRecordsForProcessing(said);
			
			out = new BufferedWriter(new FileWriter(loadFilename, false));
			
			buildHeader(out, said);			
			int recCount = 2 + buildDetail(out); // The spec indicates that the amex_total_records and se_total_records includes the header and trailer records too
			buildTrailer(out, said, recCount);
			
			out.flush();
			out.close();
			
			//Send file out iff there were records to send out.
			if(recCount > 2) {
				sendFile(loadFilename);
				createRecordForImaging();				
				moveCasesToCompletedQueue(said);
				
			} else {
				File respn = new File( loadFilename );
				respn.delete();
				
				log.info("RESPN : No records hence file created with just Hdr and Trl deleted.");
			}
			
		} catch (IOException e) {
			logEvent( this.getClass().getName(), "processTransactions()", e.toString() );
			logEntry( "processTransactions()", e.toString() );
			
		} finally {
			try {
				if(out != null)
					out.close();
				
			} catch(IOException ioe) {
				logEvent( this.getClass().getName(), "processTransactions()", ioe.toString() );
				logEntry( "processTransactions()", ioe.toString() );
			}
	        
		}
		
		return true;
	}
	
	
	protected void buildHeader(BufferedWriter out, String said) {
		log.debug("RESPN buildHeader");
		
		try {
			FlatFileRecord RESPNHdr = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_RESPN_HDR);
			
			RESPNHdr.setFieldData("record_type", REC_TYPE_HDR);
			RESPNHdr.setFieldData("application_system_code", APPLICATION_SYSTEM_CODE);
			RESPNHdr.setFieldData("file_type_code", FILE_TYPE_CODE);
			RESPNHdr.setFieldData("file_creation_date", DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(), "yyyyMMdd"));			
			RESPNHdr.setFieldData("said", said);
			RESPNHdr.setFieldData("datatype", RESPN);
						
			out.write(RESPNHdr.spew());
			out.newLine();
			
		} catch(IOException ioe) {
			logError( "buildHeader()", ioe.toString() );			
		}	
		 
	}
	
	protected int buildDetail(BufferedWriter out) {
    
		log.debug("RESPN buildDetail");
		
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		
		int paramIndex = 1;
		int recCount = 0;
		
		try {
			FlatFileRecord RESPNDetail_1 = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_RESPN_DTL_1);
			FlatFileRecord RESPNDetail_2 = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_RESPN_DTL_2);
			FlatFileRecord RESPNDetail_3 = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_RESPN_DTL_3);
			
			pStmt = con.prepareStatement( SQL_GET_RESPN_RECORDS.toString() );
			pStmt.setString(paramIndex++, loadFilename);
			pStmt.setLong(paramIndex++, loadFilenameToLoadFileId( loadFilename ));
			
			rs = pStmt.executeQuery();
						
			while(rs.next()) {
				RESPNDetail_1.resetAllFields();				
				RESPNDetail_1.setAllFieldData(rs);
			
				RESPNDetail_2.resetAllFields();				
				RESPNDetail_2.setAllFieldData(rs);

				RESPNDetail_3.resetAllFields();				
				RESPNDetail_3.setAllFieldData(rs);

				// * Fields requiring special handling
				// Seq Number
				RESPNDetail_1.setFieldData("seq_number", DTL_REC_TYPE_SEQ_NUM_1);				
				RESPNDetail_2.setFieldData("seq_number", DTL_REC_TYPE_SEQ_NUM_2);				
				RESPNDetail_3.setFieldData("seq_number", DTL_REC_TYPE_SEQ_NUM_3);
				
				// Card Number
				String cardNumberFull = rs.getString("cm_number_full");
				
				if(cardNumberFull != null){
				    if(cardNumberFull.length() <= 15 ) {
					     RESPNDetail_1.setFieldData("cm_number", cardNumberFull);				
					     RESPNDetail_2.setFieldData("cm_number", cardNumberFull);				
					     RESPNDetail_3.setFieldData("cm_number", cardNumberFull);
				     } else {
					     RESPNDetail_1.setFieldData("cm_account_num_exd", cardNumberFull);				
					     RESPNDetail_2.setFieldData("cm_account_num_exd", cardNumberFull);				
					     RESPNDetail_3.setFieldData("cm_account_num_exd", cardNumberFull);
				   }
				 } else{
				     log.error("cardNumber from the resultSet is NULL");
				 }
				
				// Response Explanation
				String response = rs.getString( "response_explanation" );
				String response1 = "", response2 = "", response3 = ""; 
				
				if(response != null){
				   if(response.length() > 132 ) {
             response1 = response.substring(0, 132);
				   } else {
					   response1 = response;
				  }
				
				  if( response.length() > 385 ) {
					   response2 = response.substring(132, 385);
					   response3 = response.substring(385);
				  }
				}
				
				if(response3 != null && response3.length() > 59 ) {
					response3 = response3.substring(0, 59);
				}
				
				
				RESPNDetail_1.setFieldData("response_explanation_1", response1); 
				RESPNDetail_2.setFieldData("response_explanation_2", response2);
				RESPNDetail_3.setFieldData("response_explanation_3", response3);
				
				out.write( RESPNDetail_1.spew() );
				out.newLine();
				
				out.write( RESPNDetail_2.spew() );
				out.newLine();
				
				out.write( RESPNDetail_3.spew() );
				out.newLine();
				
				recCount = recCount + 3;
			}	
			
		} catch(IOException ioe) {
			logError( "buildDetail()", ioe.toString() );
			
		} catch(SQLException sqe) {
			logError( "buildDetail()", sqe.toString() );
			
		}
		
		return recCount;
	}


	protected void buildTrailer(BufferedWriter out, String said, int recCount) {
		log.debug("RESPN buildTrailer");
		
		try {
			FlatFileRecord RESPNTrl = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_RESPN_TRL);
			
			RESPNTrl.setFieldData("record_type", REC_TYPE_TRL);
			RESPNTrl.setFieldData("application_system_code", APPLICATION_SYSTEM_CODE);
			RESPNTrl.setFieldData("file_type_code", FILE_TYPE_CODE);
			RESPNTrl.setFieldData("file_creation_date", DateTimeFormatter.getFormattedDate(Calendar.getInstance().getTime(), "yyyyMMdd"));			
			RESPNTrl.setFieldData("amex_total_records", recCount); // Had verified with Amex both amex_total_records and se_total_records are the same
			RESPNTrl.setFieldData("se_total_records", recCount);    
			RESPNTrl.setFieldData("said", said);
			RESPNTrl.setFieldData("datatype", RESPN);
									
			out.write( RESPNTrl.spew() );
			out.newLine();
			
		} catch(IOException ioe) {
			logError( "buildTrailer()", ioe.toString() );			
		}	
	}

	protected void getFileBatchId() {
		PreparedStatement pStmt = null;
		ResultSet rs = null;
								
		try {					
			pStmt = con.prepareStatement( SQL_GET_RESPN_BATCHID.toString() );			
			
			rs = pStmt.executeQuery();
			if(rs.next())
				fileBatchId = rs.getLong(1);
			
		} catch (java.sql.SQLException sqe) {
			
		} finally {
			try {
				if(rs != null) rs.close();
			} catch(Exception ignore) {}
			
			try {
				if(pStmt != null) pStmt.close();
			} catch(Exception ignore) {} 
		}		
		
	}
	
	protected int markRecordsForProcessing(String said) {
		
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		
		int paramIndex = 1;
		int count = 0;
		
		try {
			long loadFileId = loadFilenameToLoadFileId( loadFilename );
			
			pStmt = con.prepareStatement( SQL_MARK_OUTGOING_RESPN_RECORDS.toString() );			
			pStmt.setString( paramIndex++, loadFilename );
			pStmt.setLong( paramIndex++, loadFileId );
			pStmt.setLong( paramIndex++, fileBatchId );
			pStmt.setString( paramIndex++, said );
			
			count = pStmt.executeUpdate();
			log.debug(count + "records marked for processing for RESPN");
			
		} catch(SQLException sqe) {
			logError( "markRecordsForProcessing()", sqe.toString() );
			
		}	finally {
			try {
				if(rs != null) rs.close();
			} catch(Exception ignore) {}
			
			try {
				if(pStmt != null) pStmt.close();
			} catch(Exception ignore) {} 
		}		
		
		return count;
	}
	  
	
	
	/*
	 * Helper functions
	 */
	
	/**
	 * Files go out with a specific prefix
	 * MERCHESOBTST.RESPN, MERCHESOBTST.RESPN
	 * @param fileType
	 * @return
	 */
	protected String generateResponseFileName(){    	
    	
    	String filePrefix = isTestMode ? TEST_PREFIX : PROD_PREFIX;    			
		
    	String fileName = filePrefix + RESPN;
		
    	return fileName;
    }	
	
	
	private void createRecordForImaging() {
		
		PreparedStatement pStmt = null;
				
		int paramIndex = 1;
		int count = 0;
		
		try {			
			pStmt = con.prepareStatement( SQL_CREATE_IMGIN_PROCESS_ENTRY.toString() );			
			
			pStmt.setInt( paramIndex++, ClearingConstants.PROC_TYPE_RESPN.getValue());
			pStmt.setString( paramIndex++, loadFilename );
			pStmt.setLong( paramIndex++, fileBatchId );
			
			count = pStmt.executeUpdate();
			log.debug(count + "record inserted in dispute_imgin_process");
			
		} catch(SQLException sqe) {
			logError( "createRecordForImaging()", sqe.toString() );
			
		}	finally {
			
			try {
				if(pStmt != null) pStmt.close();
			} catch(Exception ignore) {} 
		}	
		
	}
	
	
	private void moveCasesToCompletedQueue(String said) {
		
		PreparedStatement pStmt = null;
		ResultSet rs = null;
		
		int paramIndex = 1;
		int colIndex = 1;
		int count = 0;
		
		try {
			long loadFileId = loadFilenameToLoadFileId( loadFilename );
			
			pStmt = con.prepareStatement( SQL_GET_OUTGOING_RESPN_RECORDS.toString() );			
			pStmt.setString( paramIndex++, loadFilename );
			pStmt.setLong( paramIndex++, loadFileId );
			pStmt.setLong( paramIndex++, fileBatchId );
			pStmt.setString( paramIndex++, said );
			
			rs = pStmt.executeQuery();
			while( rs.next() ) {
				QueueTools.moveQueueItem(rs.getLong(colIndex), RESPOND_TO_AMEX_QUEUE, AMEX_FULFILLED_RETRIEVALS_QUEUE, null, "");
				count++;
			}
			log.debug(count + "records moved to Amex Fulfilled Retrieval Queue");
			
		} catch(SQLException sqe) {
			logError( "moveCasesToCompletedQueue()", sqe.toString() );
			
		}	finally {
			try {
				if(rs != null) rs.close();
			} catch(Exception ignore) {}
			
			try {
				if(pStmt != null) pStmt.close();
			} catch(Exception ignore) {} 
		}		
		
	}
	
	public static void main(String args[]) {
		AmexOptblueRESPNBuilder disputeResponseGenerator = null;
		int idx = 0;
		
		try {
			if (args.length > 0 && args[idx].equals("printtestprops")) {
					EventBase.printKeyListStatus(new String[] { MesDefaults.DK_AMEX_HOST, MesDefaults.DK_AMEX_USER,
							MesDefaults.DK_AMEX_PASSWORD, MesDefaults.DK_AMEX_OUTGOING_PATH });				
			} else if (args.length > 0 && args[idx].equals("test")) {
				isTestMode = true;
				idx = 1;
			}

			disputeResponseGenerator = new AmexOptblueRESPNBuilder();
			disputeResponseGenerator.connect();
							
			if ("execute".equals(args[idx])) {
				disputeResponseGenerator.setEventArgs(args[++idx]);

				// Following 2 need to be set in case of retransmission
				if(args.length > ++idx)
					disputeResponseGenerator.loadFilename = args[idx];
				if(args.length > ++idx)
					disputeResponseGenerator.fileBatchId = Long.parseLong(args[idx]);
				
				disputeResponseGenerator.execute();
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				disputeResponseGenerator.cleanUp();
			} catch (Exception e) { }
						
		}
		
		Runtime.getRuntime().exit(0);
	}
	
	
	private final static String	RESPN = "RESPN";
					
	// Mes Defined Dispute File Prefixes
	private static final Map<String, String> DisputeResponseFilePrefixes = new HashMap<String, String>() {
		{
			put( RESPN, "amex_optbrespn" ); 	// Amex Optblue Outgoing RESPN File	
		}
	};
	
	// Amex Defined File Prefixes
	private final static String PROD_PREFIX = "MERCHESOBPRD.";
	private final static String TEST_PREFIX = "MERCHESOBTST.";
	
	// Loading mode
	private static boolean isTestMode = false;
	
	// Record Types
	private final static String REC_TYPE_HDR = "H"; 
	private final static String REC_TYPE_DTL = "D";
	private final static String REC_TYPE_TRL = "T";
	
	private final static String DTL_REC_TYPE_SEQ_NUM_1 = "01";
	private final static String DTL_REC_TYPE_SEQ_NUM_2 = "02";
	private final static String DTL_REC_TYPE_SEQ_NUM_3 = "03";
	
	// Queues
	private final static int RESPOND_TO_AMEX_QUEUE = 3007;	
	private final static int Q_ITEM_TYPE = MesQueues.Q_ITEM_TYPE_RETRIEVAL;
	private final static int AMEX_FULFILLED_RETRIEVALS_QUEUE = MesQueues.Q_RETRIEVALS_AMEX_COMPLETED;
	
	// File Constants
	private final static String APPLICATION_SYSTEM_CODE = "01";
	private final static String FILE_TYPE_CODE = "02";


		
	// ************** SQL Queries
	
	// ** RESPN
	private static final StringBuffer SQL_MARK_OUTGOING_RESPN_RECORDS = new StringBuffer("");
	private static final StringBuffer SQL_GET_RESPN_BATCHID = new StringBuffer("");
	private static final StringBuffer SQL_GET_RESPN_RECORDS = new StringBuffer("");
	private static final StringBuffer SQL_GET_OUTGOING_RESPN_RECORDS = new StringBuffer("");
	
	private static final StringBuffer SQL_CREATE_IMGIN_PROCESS_ENTRY = new StringBuffer("");
	
	private static final String NL = " \n";
	
	static {		
		// ** RESPN : This sequence kind of got 'mis' named. It is not just a chargeback batch id sequence but a general sequence id
		SQL_GET_RESPN_BATCHID.append (
			"select amex_optb_cb_batchid_seq.nextval" + NL +
			"from dual" + NL);
		
		SQL_MARK_OUTGOING_RESPN_RECORDS.append (
			"update network_retrieval_amex_optb nrao" + NL + 
			"set nrao.output_filename = ?, nrao.output_file_id = ?, nrao.batch_id = ? " + NL + 
			"where nrao.output_filename is null" + NL + 
			"and nrao.load_sec in (" + NL + 
			"	select q.id" + NL + 
			"	from q_data q" + NL +  
			"	where q.date_created > sysdate - 30" + NL +  
			"		and q.item_type = " + Q_ITEM_TYPE + NL + 
			"		and q.type = " + RESPOND_TO_AMEX_QUEUE + NL + 
			"		and q.source = 'amex'" + NL +
			" ) " + NL +
			"and nrao.said = ?"
		);
		
		SQL_GET_RESPN_RECORDS.append(
				"select " + NL +
				"    'D' as dtl_rec_type," + NL +
			  "    dukpt_decrypt_wrapper(card_number_enc) as cm_number_full," + NL +  
				"    inquiry_case_number as case_number," + NL +
				"    se_number as se_number," + NL +
				"    cardmember_name_1 as cm_name," + NL +
				"    inquiry_case_number as inquiry_case_number," + NL +
				"    inquiry_action_number as inquiry_action_number," + NL +
				"    amex_id as amex_id," + NL +
				"    nrao.inquiry_response_code as inquiry_action_resolution_code," + NL +
				"    charge_amount as dollar_amount," + NL +
				"    to_char(incoming_date, 'mmddyyyy') as date_action_taken, " + NL +
				"    'Y' as support_sent," + NL +
				"    load_sec as se_tracking," + NL +
				"    qnotes.note as response_explanation, " + NL + // Translate this to response_explanation_1, response_explanation_2 and response_explanation_3
				"    settlement_rec_id as reference_number," + NL +
				"    inquiry_mark as inquiry_mark    " + NL +
				"from network_retrieval_amex_optb nrao," + NL +
				"  ( select  qd.id, qn.note, qn.seq " + NL +
				"    from q_data qd, q_notes qn, q_notes qn_max_seq " + NL +
				"    where qd.type = " + RESPOND_TO_AMEX_QUEUE + NL +
				"    	and qd.item_type = " + Q_ITEM_TYPE + NL +
				"       and qn.id = qd.id " + NL +
				"       and qn.item_type = qd.item_type " + NL +
				"       and qn_max_seq.id = qn.id " + NL +
				"       and qn_max_seq.item_type = qn.item_type " + NL +
				"    group by  qd.id, qn.note, qn.seq " + NL +
				"    having qn.seq = max(qn_max_seq.seq) " + NL +
				"    ) qnotes  " + NL +
				"where nrao.output_filename = ? " + NL +
				"    and nrao.output_file_id = ? " + NL +
				"    and nrao.load_sec = qnotes.id (+) " + NL 			
			);
		
		SQL_CREATE_IMGIN_PROCESS_ENTRY.append(
			"insert into dispute_imgin_process(rec_id, process_type, load_filename, batch_id) " + NL +
			"values (mes.dispute_imgin_process_seq.nextval, ?, ?, ?)"
		);
		
		SQL_GET_OUTGOING_RESPN_RECORDS.append(
			"select load_sec " + NL +
			"from network_retrieval_amex_optb nrao, q_data qd "  + NL + 
			"where nrao.output_filename = ? "  + NL + 
			"and nrao.output_file_id = ? "  + NL + 
			"and nrao.batch_id = ? "  + NL + 
			"and nrao.said = ? "  + NL +
			"and nrao.load_sec = qd.id "  + NL + 
			"and qd.type = " + RESPOND_TO_AMEX_QUEUE + NL 
		);
		
	}	
	
	
}
