package com.mes.persist.vo;

public class BusOwnerData {
	
	private String ownerFirstName;
	private String ownerLastName;
	private String ownerAddress;
	private String ownerState;
	private String ownerCity;
	private String ownerZip;
	private String ownerDob;
	private String ownerSSN;
	private String ownerTitle;
	
	public String getOwnerFirstName() {
		return ownerFirstName;
	}
	public void setOwnerFirstName(String ownerFirstName) {
		this.ownerFirstName = ownerFirstName;
	}
	public String getOwnerLastName() {
		return ownerLastName;
	}
	public void setOwnerLastName(String ownerLastName) {
		this.ownerLastName = ownerLastName;
	}
	public String getOwnerAddress() {
		return ownerAddress;
	}
	public void setOwnerAddress(String ownerAddress) {
		this.ownerAddress = ownerAddress;
	}
	public String getOwnerState() {
		return ownerState;
	}
	public void setOwnerState(String ownerState) {
		this.ownerState = ownerState;
	}
	public String getOwnerCity() {
		return ownerCity;
	}
	public void setOwnerCity(String ownerCity) {
		this.ownerCity = ownerCity;
	}
	public String getOwnerZip() {
		return ownerZip;
	}
	public void setOwnerZip(String ownerZip) {
		this.ownerZip = ownerZip;
	}
	public String getOwnerDob() {
		return ownerDob;
	}
	public void setOwnerDob(String ownerDob) {
		this.ownerDob = ownerDob;
	}
	public String getOwnerSSN() {
		return ownerSSN;
	}
	public void setOwnerSSN(String ownerSSN) {
		this.ownerSSN = ownerSSN;
	}
	public String getOwnerTitle() {
		return ownerTitle;
	}
	public void setOwnerTitle(String ownerTitle) {
		this.ownerTitle = ownerTitle;
	}
	
	

}
