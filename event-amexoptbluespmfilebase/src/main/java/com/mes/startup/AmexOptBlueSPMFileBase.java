/*************************************************************************

  FILE: $URL: /src/main/com/mes/startup/AmexBlueOptSPMFileBase.java $

  Description:

  Created by         : $Author: sceemarla    $
  Created Date		 : $Date:   2016-10-06  $
  Last Modified By   : $Author:             $
  Last Modified Date : $Date:               $
  Version            : $Revision:0.1        $

  Change History:
     See Bitbucket

  Copyright (C) 2000-2014,2015 ,2016 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.mes.config.MesDefaults;
import com.mes.constants.FlatFileDefConstants;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.flatfile.FlatFileRecord;
import com.mes.persist.vo.BusOwnerData;
import com.mes.support.PropertiesFile;
import com.mes.utils.MesStringUtil;
import com.mes.constants.mesConstants;

public class AmexOptBlueSPMFileBase extends AmexFileBase {

    private static final long serialVersionUID = 7049860431345586193L;
    private BufferedWriter out = null;
    private String fname = null;
    private long recordnum = 0;
    private FlatFileRecord ffd = null;
    private HashSet<String> boardedMerchants = new HashSet();
    private int processSequence = 0;
  
    private List<Integer> bankNumbersList = Arrays.asList(3941,3942,3943);
    private List<String> poboxValidation = Arrays.asList("","P.O.B.","P.O. BOX","P.O.BOX","PO BOX","P O BOX","P. O. BOX","P. O, BOX");
    private static final String SELLER_BANK_NUMBER = "3858";
    private String signerTitle = "";
    
    private static final String SQL_GET_AMEX_OPTB_SPM_PROCESS = "select count(1) from amex_optb_spm_process "; 
    private static final String SQL_UPDATE_AMEX_OPTB_SPM_PROCESS = "update amex_optb_spm_process set spm_process_sequence = ?, load_filename = ? "; 
    private static final String SQL_GET_PROCESS_SEQUENCE = "select amex_optb_spm_process_seq.nextval from dual";
    private static final String SQL_UPDATE_PROCESS_TABLE = "update amex_optb_spm_process set sent_flag = 'Y' "
	        + "where merchant_number = ? and spm_process_sequence = ?";
    private static final String SQL_GET_SALES_DATA = "select upper(am.relationship_name) as relationship_name, am.amex_channel_ind, am.iso_reg_number " +
            " from amex_pse_mapping am where pse_banks in (?,?,?,?,?,?,?)";
    
    /**
     * List of owner types for which the SSN should be populated, if SSN is on file.
     */
    private static final List<String> OWNER_TYPES_FOR_POPULATING_SSN = Arrays.asList("D", "F", "M", "Q", "S");
    
    /**
     * This query is used to transpose multiple rows(details of business owner details) of resultset to one row. All the data elements of FlatFileDef are populated by just passing this resultset 
     *  to setAllFieldData method and eliminating the need of iterating multiple rows and setting them to FlatFileDef in the code.
     */
    private static final String SQL_GET_DT2_DETAILS_APP_SEQ_NUM = " WITH owner_details AS (select  "
    		+"   BO.BUSOWNER_FIRST_NAME,BO.BUSOWNER_SSN,BO.BUSOWNER_LAST_NAME,BO.BUSOWNER_DL_DOB,BO.APP_SEQ_NUM,BO.BUSOWNER_NUM, "
    		+"   AD.ADDRESS_LINE1,AD.ADDRESS_LINE2,AD.ADDRESS_CITY,AD.ADDRESS_ZIP,AD.COUNTRYSTATE_CODE "
    		+" from MES.BUSINESSOWNER BO, MES.ADDRESS AD  WHERE BO.APP_SEQ_NUM = AD.APP_SEQ_NUM AND BO.APP_SEQ_NUM =  ?  "
    		+" AND DECODE (BO.BUSOWNER_NUM, 1, 4,  "
    		+"                              2, 5,  "
    		+"                              3, 6,  "
    		+"                              4, 11,"
    		+ "                             5, 12,"
    		+ "                             6, 13, 0) = AD.ADDRESSTYPE_CODE) "
    		+" SELECT	UPPER(temp.APP_SEQ_NUM) APP_SEQ_NUM "
    		+" 		,(SELECT od.BUSOWNER_FIRST_NAME FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 1) owner_1_firstname "
    		+" 		,(SELECT od.BUSOWNER_FIRST_NAME FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 2) owner_2_firstname "
    		+" 		,(SELECT od.BUSOWNER_FIRST_NAME FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 3) owner_3_firstname "
    		+" 		,(SELECT od.BUSOWNER_FIRST_NAME FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 4) owner_4_firstname "
    		+"      ,(SELECT od.BUSOWNER_LAST_NAME FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 1) owner_1_lastname "
    		+" 		,(SELECT od.BUSOWNER_LAST_NAME FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 2) owner_2_lastname "
    		+" 		,(SELECT od.BUSOWNER_LAST_NAME FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 3) owner_3_lastname "
    		+" 		,(SELECT od.BUSOWNER_LAST_NAME FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 4) owner_4_lastname "
    		+"      ,(SELECT od.BUSOWNER_SSN FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 5) auth_signer_identification_num "
    		+" 		,(SELECT od.BUSOWNER_SSN FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 2) owner_2_ssn "
    		+" 		,(SELECT od.BUSOWNER_SSN FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 3) owner_3_ssn  "
    		+" 	    ,(SELECT od.BUSOWNER_SSN FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 4) owner_4_ssn "
    		+"      ,(SELECT od.ADDRESS_LINE1 FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 5) auth_signer_street_address_1 "
    		+"      ,(SELECT od.ADDRESS_LINE1 FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 2) owner_2_address_1 "
    		+"      ,(SELECT od.ADDRESS_LINE1 FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 3) owner_3_address_1 "
    		+"      ,(SELECT od.ADDRESS_LINE1 FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 4) owner_4_address_1 "
    		+"      ,(SELECT od.ADDRESS_CITY FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 5) auth_signer_city "
    		+"      ,(SELECT od.ADDRESS_CITY FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 2) owner_2_city "
    		+"      ,(SELECT od.ADDRESS_CITY FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 3) owner_3_city "
    		+"      ,(SELECT od.ADDRESS_CITY FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 4) owner_4_city "
    		+"      ,(SELECT od.ADDRESS_ZIP FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 5) auth_signer_zip "
    		+"      ,(SELECT od.ADDRESS_ZIP FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 2) owner_2_zip "
    		+"      ,(SELECT od.ADDRESS_ZIP FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 3) owner_3_zip "
    		+"      ,(SELECT od.ADDRESS_ZIP FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 4) owner_4_zip "
    		+"      ,(SELECT od.COUNTRYSTATE_CODE FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 5) auth_signer_state "
    		+"      ,(SELECT od.COUNTRYSTATE_CODE FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 2) owner_2_state "
    		+"      ,(SELECT od.COUNTRYSTATE_CODE FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 3) owner_3_state "
    		+"      ,(SELECT od.COUNTRYSTATE_CODE FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 4) owner_4_state "
    		+"      ,(SELECT trunc(od.BUSOWNER_DL_DOB) FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 5) auth_signer_dob "
    		+"      ,(SELECT trunc(od.BUSOWNER_DL_DOB) FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 2) owner_2_dob "
    		+"      ,(SELECT trunc(od.BUSOWNER_DL_DOB) FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 3) owner_3_dob "
    		+"      ,(SELECT trunc(od.BUSOWNER_DL_DOB) FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 4) owner_4_dob "
    		+"      ,(SELECT 'US' FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 5) auth_signer_country "
    		+"      ,(SELECT 'US' FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 2) owner_2_country "
    	    +"      ,(SELECT 'US' FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 3) owner_3_country "
    	    +"      ,(SELECT 'US' FROM owner_details od WHERE od.APP_SEQ_NUM = temp.APP_SEQ_NUM AND od.BUSOWNER_NUM = 4) owner_4_country "
    		+" FROM	(SELECT	DISTINCT m.APP_SEQ_NUM FROM	owner_details m ) temp   ";

    /**
     * This query is used to get the details of DT2 record without owner details, as few Bustype should not be populated with owner details.
     */
    private static final String SQL_GET_DT2_DETAILS_APP_SEQ_NUM_NO_OWNER_DETAILS = " SELECT "
    		+"   BO.BUSOWNER_SSN auth_signer_identification_num, "
    		+"   BO.BUSOWNER_DL_DOB auth_signer_dob, "
    		+"   AD.ADDRESS_LINE1 auth_signer_street_address_1, "
    		+"   AD.ADDRESS_CITY auth_signer_city, "
    		+"   AD.ADDRESS_ZIP auth_signer_zip, "
    		+"   AD.COUNTRYSTATE_CODE auth_signer_state, "
    		+ "  'US' auth_signer_country"
    		+" FROM MES.BUSINESSOWNER BO, "
    		+"   MES.ADDRESS AD "
    		+" WHERE BO.APP_SEQ_NUM                  = AD.APP_SEQ_NUM "
    		+" AND BO.APP_SEQ_NUM                    = ? "
    		+" AND DECODE (BO.BUSOWNER_NUM, 5, 12, 0) = AD.ADDRESSTYPE_CODE ";
    
    
    private static final String SQL_GET_BUS_OWNER_INFO = ""
			+ "SELECT NVL(bo.busowner_first_name,'') AS busowner_first_name, "
			+ "       NVL(bo.busowner_last_name,'')  AS busowner_last_name, "
			+ "       NVL(bo.busowner_ssn,'')        AS busowner_ssn, "
			+ "NVL(to_char(bo.busowner_dl_dob, 'yyyymmdd'),'')     AS busowner_dl_dob, "
			+ "       NVL(ad.address_city,'')        AS address_city, "
			+ "       NVL(ad.address_zip,'')         AS address_zip, "
			+ "       NVL(ad.address_line1,'')       AS address_line1, "
			+ "       NVL(ad.countrystate_code,'')   AS countrystate_code, "
			+ "       NVL(bo.busowner_title,'')      AS busowner_title, "
			+ "       'US'                           AS busowner_country "
			+ "FROM   mif, "
			+ "       merchant me, "
			+ "       businessowner bo, "
			+ "       address ad "
			+ "WHERE  mif.merchant_number = me.merch_number "
			+ "       AND me.app_seq_num = bo.app_seq_num "
			+ "       AND bo.app_seq_num = ad.app_seq_num "
			+ "       AND bo.busowner_num = ? "
			+ "       AND ad.addresstype_code = ? "
			+ "       AND mif.merchant_number = ?";

    private static final String SQL_GET_AUTHORISED_SIGNER_DETAILS = ""
    		+ "SELECT NVL(bo.busowner_first_name,'') AS signer_first_name, "
    		+ "       NVL(bo.busowner_last_name,'')  AS signer_last_name,"
    		+ "       NVL(bo.busowner_title,'')      AS signer_title "
    		+ "FROM   mif, "
    		+ "       merchant me, "
    		+ "       businessowner bo "
    		+ "WHERE  mif.merchant_number = me.merch_number "
    		+ "       AND me.app_seq_num = bo.app_seq_num "
    		+ "       AND bo.busowner_num = 5 "
    		+ "       AND mif.merchant_number = ?";
    		
	
	
	public AmexOptBlueSPMFileBase() {
		CardTypeFileDesc            = "Amex Optblue SPM file";
		DkOutgoingHost = MesDefaults.DK_AMEX_OUTGOING_HOST;
		DkOutgoingUser = MesDefaults.DK_AMEX_OUTGOING_USER;
		DkOutgoingPassword = MesDefaults.DK_AMEX_OUTGOING_PASSWORD;
		DkOutgoingPath = MesDefaults.DK_AMEX_OPTB_OUTGOING_PATH;
		SettlementAddrsNotify = MesEmails.MSG_ADDRS_AMEX_SPM_RESP;
		SettlementAddrsFailure = MesEmails.MSG_ADDRS_AMEX_SPM_RESP;
		DkOutgoingUseBinary = false;
		DkOutgoingSendFlagFile = true;
	}

	@Override
	protected boolean processTransactions() {
		PreparedStatement ps = null;
        ResultSet rs = null;
        log.debug("Starting AmexOptBlueSPMFileBase:ProcessTransactions().....");

        String query = "select distinct mif.merchant_number  as seller_id,"
                + "        mif.fdr_corp_name   as seller_legal_name,"
                + "        mif.dba_name        as seller_dba_name,"
                + "        mif.federal_tax_id  as seller_fed_taxid,"
                + "        mif.sic_code        as seller_mcc,"
                + "        mif.dmaddr          as seller_address_1," 
                + " nvl(mif.addr1_line_1,'')   as address1,"
                + "        mif.dmcity          as seller_city,"
                + "        mif.dmstate         as seller_state,"
                + " DECODE (mif.bank_number,"+SELLER_BANK_NUMBER+",mif.zip1_line_4,mif.dmzip) as seller_zip,"
                + " concat('1',mif.phone_1)    as seller_bus_phone,"
                + "        mif.bank_number     as bank_number,"
                + "        mif.city1_line_4    as city1_line_4,"
                + "        mif.state1_line_4   as state1_line_4,"
                + "        mpo.cardtype_code   as cardtype_code,"
                + "   me.merch_email_address   as seller_email,"
                + "   me.merch_web_url         as seller_url,"
                + "        mif.owner_name      as owner_name,"
                + "   nvl(dukpt_decrypt_wrapper(mif.ssn_enc), mif.ssn) as owner_ssn,"
                + "        mif.dmaddr          as owner_address_1,"
                + "        mif.dmcity          as owner_city,"
                + "        mif.dmstate         as owner_state,"
                + "        mif.dmzip           as owner_zip,"
                + "        mpo.cardtype_code   as cardtype_code,"
                + " nvl(mpo.AMEX_OPT_BLUE_MKT_INDICATOR , 'N') as marketing_indicator,"
                + "        irs.match_result    as irs_result,"
                + " decode( me.bustype_code,"
                + "          1, 'S',"
                + "          2, 'D',"
                + "          3, 'Q',"
                + "          4, 'D',"
                + "          5, 'M',"
                + "          6, 'F',"
                + "          7, 'G',"
                + "          8, 'M',"
                + "         10, 'D',"
                + "         11, 'Q',"
                + "         12, 'E',"
                + "         13, 'R',"
                + "         14, 'N',"
                + "         15, 'X',"
                + "        'S')                as owner_type_indicator, "
                + "  (case when me.merch_amex_canc_drgy_flag = 'Y' then 'D'" 
                + "  when mif.date_stat_chgd_to_dcb is not null then 'N' else null end) as se_dtl_status_code,"
                + "  to_char(mif.date_stat_chgd_to_dcb, 'yyyymmdd') as se_dtl_status_changedate,me.app_seq_num as app_seq_num, "
                + "  to_char(MIF_DATE_OPENED(mif.date_opened), 'yyyymmdd') as seller_start_date, "
                + "  decode(mif.dmacctst,"
                + "                'O', 02,"
                + "                'D', 04,"
                + "                'C', 04,"
                + "                'B', 05,"
                + "                'Z', 07,"
                + "                'S', 09 ) as seller_status,"
                + "  me.sales_rep_id as sales_rep_id,"
                + " nvl(mif.association_node, 0) as anode,"
                + "   mif.bank_number||nvl(mif.group_1_association,0) as g1node," 
                + "   mif.bank_number||nvl(mif.group_2_association,0) as g2node," 
                + "   mif.bank_number||nvl(mif.group_3_association,0) as g3node," 
                + "   mif.bank_number||nvl(mif.group_4_association,0) as g4node," 
                + "   mif.bank_number||nvl(mif.group_5_association,0) as g5node," 
                + "   mif.bank_number||nvl(mif.group_6_association,0) as g6node" 
                + "  from  mif, merchant me, irs_match_status irs, merchpayoption mpo"
                + "  where mif.merchant_number = me.merch_number"
                + "  and mif.merchant_number = irs.merchant_number"
                + "  and me.app_seq_num = mpo.app_seq_num(+)"
                + "  and mpo.cardtype_code(+) = 16"
                + "  and mif.test_account = 'N'"
                + "  and mif.damexse = ?" 
                + "  and mif.merchant_number in (select distinct merchant_number from amex_optb_spm_process where spm_process_sequence = ?)" 
                + "  order by seller_id , se_dtl_status_changedate desc ";
     
        try {
            connect(true);
            
            generateFile();
            ps = con.prepareStatement(query);
            PropertiesFile props = new PropertiesFile("optblue.properties");
            String seList = props.getString("se_list_"+getEventArg(0)); 
            if (StringUtils.isNotEmpty(seList)) {
                updateProcessSequence();
                String sid = props.getString("sub_" + getEventArg(0));
                List<String> seNumbersList = Arrays.asList(seList.split(","));
                for (String seNumber : seNumbersList) {
                    log.debug("processing transactions for SE:" + seNumber);
                    ps.setString(1, seNumber);
                    ps.setInt(2, processSequence);
                    rs = ps.executeQuery();
                    buildHeader(out, seNumber);
                    buildDetail(out, rs);
                    buildTrailer(out);
                }
            }
			
            updateSentFlag(boardedMerchants); 
            out.flush();
            out.close();
            sendFile(fname);
       } catch (Exception e) {
            logEntry("processTransactions()", e.toString());
            return false;
        } finally {
            try {
                if(ps != null) ps.close();
                if(rs != null) rs.close();
            } catch (Exception e) {
                log.error("Inside Finally Clause" +e.getMessage());
            }
        }
        return true;
	}
	
    protected void buildDetail(BufferedWriter out, ResultSet rs) 
    {
      log.debug("building detail record");
      log.debug("AmexOptBlueSPMFileBase::buildDetail() building detail record");
      
      try 
      {
        ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_SPONSOR_MERCHANT_DETAIL);
        while(rs.next()){
          ffd.resetAllFields();
          String sellerId = rs.getString(FlatFileDefConstants.SELLER_ID);
          boolean noOwnerDetails = false;
          boardedMerchants.add(sellerId);
          ffd.setAllFieldData(rs);
          ffd.setFieldData(FlatFileDefConstants.RECORD_NUMBER, ++recordnum);
          
          setSellerInfo(rs);
          BusOwnerData primaryBusOwnerData =  getBusOwnerData(sellerId,1,4);
          if (primaryBusOwnerData != null && StringUtils.isNotBlank(primaryBusOwnerData.getOwnerLastName())) {
        	  setOwnerInfo(rs, primaryBusOwnerData);
        	  setOwnerAddress(rs, primaryBusOwnerData);
          }
          else {
        	  BusOwnerData controlProngBusOwnerData =  getBusOwnerData(sellerId, 6, 13); 
        	  setOwnerInfo(rs, controlProngBusOwnerData);
        	  setOwnerAddress(rs, controlProngBusOwnerData);
          }
          
          setAuthorisedSigner(sellerId);
          setAlternateAuthSigner(primaryBusOwnerData);
          
          if(rs.getString(FlatFileDefConstants.OWNER_TYPE_INDICATOR).equals("D") && rs.getInt(FlatFileDefConstants.IRS_RESULT) == 7)
              ffd.setFieldData(FlatFileDefConstants.OWNER_TYPE_INDICATOR, "E");
            else if(rs.getString(FlatFileDefConstants.OWNER_TYPE_INDICATOR).equals("M") && rs.getInt(FlatFileDefConstants.IRS_RESULT) == 7)
              ffd.setFieldData(FlatFileDefConstants.OWNER_TYPE_INDICATOR, "N");
            else if(rs.getString(FlatFileDefConstants.OWNER_TYPE_INDICATOR).equals("Q") && rs.getInt(FlatFileDefConstants.IRS_RESULT) == 7)
              ffd.setFieldData(FlatFileDefConstants.OWNER_TYPE_INDICATOR, "R");
          
          out.write(ffd.spew(2));
          out.newLine();
          int appSeqNum = rs.getInt("app_seq_num");
          buildDT2(out,appSeqNum,sellerId,noOwnerDetails,rs);
        }

      } catch (Exception e) {
        logEntry("buildDetail()", e.toString());
        log.error("Exception in AmexOptblueSPMFileBase:buildDetail"+e.getMessage());
      }
   }
    
    
    
    public void setOwnerInfo(ResultSet rs, BusOwnerData busOwnerData) throws SQLException {
    	
		if (busOwnerData != null) {
			// Split owner name to first name and last name
			String ownerFirstName = busOwnerData.getOwnerFirstName();
			if (ownerFirstName != null) {
				ffd.setFieldData(FlatFileDefConstants.OWNER_FIRST_NAME, ownerFirstName);
			}
			else {
				ffd.setFieldData(FlatFileDefConstants.OWNER_FIRST_NAME, FlatFileDefConstants.UNKNOWN);
			}

			String ownerLastName = busOwnerData.getOwnerLastName();
			if (ownerFirstName != null) {
				ffd.setFieldData(FlatFileDefConstants.OWNER_LAST_NAME, ownerLastName);
			}
			else {
				ffd.setFieldData(FlatFileDefConstants.OWNER_LAST_NAME, FlatFileDefConstants.UNKNOWN);
			}

			// OWNER DOB AND SSN
			if (OWNER_TYPES_FOR_POPULATING_SSN.contains(ffd.getFieldData(FlatFileDefConstants.OWNER_TYPE_INDICATOR))) {
				String ownerSSN = setOwnerSSN(rs, busOwnerData);
				if (!ownerSSN.isEmpty()) {
					ffd.setFieldData(FlatFileDefConstants.OWNER_SSN, StringUtils.rightPad(ownerSSN, 30));
				}
				else {
					ffd.setFieldData(FlatFileDefConstants.OWNER_SSN, StringUtils.repeat(StringUtils.SPACE, 30));
				}

				if (StringUtils.isNotBlank(busOwnerData.getOwnerDob())) {
					ffd.setFieldData(FlatFileDefConstants.OWNER_DOB, busOwnerData.getOwnerDob());
				}
				else {
					ffd.setFieldData(FlatFileDefConstants.OWNER_DOB, StringUtils.repeat(StringUtils.SPACE, 8));
				}
			}
		}
    }
    
    public void setOwnerAddress(ResultSet rs,BusOwnerData busOwnerData) throws SQLException{ 
    	
		if (busOwnerData != null) {
			// Owner Address
			String addr = busOwnerData.getOwnerAddress();
			if (addr != null && poboxValidation.contains(addr))	{
				String addr1 = rs.getString("address1");
				if (addr1 !=null && poboxValidation.contains(addr1)) {	
					ffd.setFieldData(FlatFileDefConstants.OWNER_ADDRESS_1, rs.getString(FlatFileDefConstants.SELLER_DBA_NAME));
				}
				else {
					ffd.setFieldData(FlatFileDefConstants.OWNER_ADDRESS_1, addr1);
				}
			}
			else {
				ffd.setFieldData(FlatFileDefConstants.OWNER_ADDRESS_1, addr);
			}

			// Owner zip
			String ownerZip = busOwnerData.getOwnerZip();
			if (busOwnerData.getOwnerZip() != null) {
				ffd.setFieldData(FlatFileDefConstants.OWNER_ZIP, ownerZip);
			}

			// Owner City
			String ownerCity = busOwnerData.getOwnerCity();
			if (ownerCity != null) {
				ffd.setFieldData(FlatFileDefConstants.OWNER_CITY, ownerCity);
			}

			// Owner State
			String ownerState = busOwnerData.getOwnerState();
			if (ownerState != null && (ownerState.equalsIgnoreCase("PR") || ownerState.equalsIgnoreCase("VI"))) {
				ffd.setFieldData(FlatFileDefConstants.SELLER_COUNTRY, ownerState);
				ffd.setFieldData(FlatFileDefConstants.OWNER_COUNTRY, ownerState);
				ffd.setFieldData(FlatFileDefConstants.OWNER_STATE, MesStringUtil.getPaddedString("", 5, StringUtils.SPACE));

			}
			else {
				ffd.setFieldData(FlatFileDefConstants.SELLER_COUNTRY, "US");
				ffd.setFieldData(FlatFileDefConstants.OWNER_COUNTRY, "US");
				ffd.setFieldData(FlatFileDefConstants.OWNER_STATE, busOwnerData.getOwnerState());
			}
		}
    }
	
    /**
     * 
     * @param rs
     * @throws SQLException
     */
    public void setSellerInfo(ResultSet rs) throws SQLException {
    	
    	/**ffd.setFieldData("seller_email", rs.getString("seller_email"));*/   
        if(rs.getString(FlatFileDefConstants.SELLER_FED_TAXID) != null){
          ffd.setFieldData(FlatFileDefConstants.SELLER_FED_TAXID,  MesStringUtil.getPaddedString( rs.getString(FlatFileDefConstants.SELLER_FED_TAXID).trim(), 9, "0"));
        }
    	
      //Seller zip
        if(rs.getString(FlatFileDefConstants.SELLER_ZIP) != null){
          ffd.setFieldData(FlatFileDefConstants.SELLER_ZIP,  MesStringUtil.getPaddedString( rs.getString(FlatFileDefConstants.SELLER_ZIP).trim(), 9, "0"));
        }
        
    	//Seller URL
        if(rs.getString(FlatFileDefConstants.SELLER_MCC).equals("5969")){
          ffd.setFieldData(FlatFileDefConstants.SELLER_URL, rs.getString(FlatFileDefConstants.SELLER_URL));
        }
  
        
    	// Seller_Legal_Name
        if(rs.getString(FlatFileDefConstants.SELLER_LEGAL_NAME) != null){
          ffd.setFieldData(FlatFileDefConstants.SELLER_LEGAL_NAME, rs.getString(FlatFileDefConstants.SELLER_LEGAL_NAME));
        }
	                
        //Seller_Dba_Name
        if(rs.getString(FlatFileDefConstants.SELLER_DBA_NAME) != null){
          ffd.setFieldData(FlatFileDefConstants.SELLER_DBA_NAME, rs.getString(FlatFileDefConstants.SELLER_DBA_NAME));
        }
	                
        //Seller_MCC Code
        if(rs.getString(FlatFileDefConstants.SELLER_MCC) != null){
          ffd.setFieldData(FlatFileDefConstants.SELLER_MCC, rs.getString(FlatFileDefConstants.SELLER_MCC));
        }
	                
        //Seller Business Phone
        if(rs.getString(FlatFileDefConstants.SELLER_BUS_PHONE) != null){
          ffd.setFieldData(FlatFileDefConstants.SELLER_BUS_PHONE, rs.getString(FlatFileDefConstants.SELLER_BUS_PHONE));
        }
        	                
		Integer bankNum = rs.getInt(FlatFileDefConstants.BANK_NUM);
		if (bankNumbersList.contains(bankNum)) {
			ffd.setFieldData(FlatFileDefConstants.SELLER_CITY, rs.getString(FlatFileDefConstants.SELLER_CITY));
			ffd.setFieldData(FlatFileDefConstants.SELLER_STATE, rs.getString(FlatFileDefConstants.SELLER_STATE));
		}
		else{
			ffd.setFieldData(FlatFileDefConstants.SELLER_CITY, rs.getString(FlatFileDefConstants.CITY1_LINE_4));
			ffd.setFieldData(FlatFileDefConstants.SELLER_STATE, rs.getString(FlatFileDefConstants.STATE1_LINE_4));
		}
		Integer cardTypeCode = rs.getInt(FlatFileDefConstants.CARDTYPE_CODE);
		if (cardTypeCode == 15) {
			ffd.setFieldData(FlatFileDefConstants.JCB_INDICATOR, "Y");
		}
		else {
			ffd.setFieldData(FlatFileDefConstants.JCB_INDICATOR, "N");
		}
		setSellerAddress(rs,bankNum);
		
	}
    
    
	public void setSellerAddress(ResultSet rs, Integer bankNum) throws SQLException {

		try {
			String addr = rs.getString(FlatFileDefConstants.SELLER_ADDRESS_1);
			if (bankNumbersList.contains(bankNum) && addr != null && (poboxValidation.contains(addr))) {
				String addr1 = rs.getString("address1");
				if (addr1 != null && poboxValidation.contains(addr1)) {
					ffd.setFieldData(FlatFileDefConstants.SELLER_ADDRESS_1, rs.getString(FlatFileDefConstants.SELLER_DBA_NAME));
				}
				else {
					ffd.setFieldData(FlatFileDefConstants.SELLER_ADDRESS_1, addr1);
				}
			}
			else {
				ffd.setFieldData(FlatFileDefConstants.SELLER_ADDRESS_1, rs.getString(FlatFileDefConstants.SELLER_ADDRESS_1));
			}

		}
		catch (Exception e) {
			log.error("Exception in AmexOptblueSPMFileBase:setSellerAddress:", e);
		}
	}
    
    
	public BusOwnerData getBusOwnerData(String sellerId,Integer busOwnerNum,Integer addrTypeCode) throws SQLException {

		try (PreparedStatement ps = con.prepareStatement(SQL_GET_BUS_OWNER_INFO)) {
			ps.setInt(1, busOwnerNum);
		    ps.setInt(2, addrTypeCode);  
			ps.setString(3, sellerId);

			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					BusOwnerData busOwnerData = new BusOwnerData();
					busOwnerData.setOwnerFirstName(rs.getString(FlatFileDefConstants.BUSOWNER_FIRST_NAME));
					busOwnerData.setOwnerLastName(rs.getString(FlatFileDefConstants.BUSOWNER_LAST_NAME));					
					busOwnerData.setOwnerAddress(rs.getString(FlatFileDefConstants.ADDRESS_LINE1));
					busOwnerData.setOwnerCity(rs.getString(FlatFileDefConstants.ADDRESS_CITY));
					busOwnerData.setOwnerState(rs.getString(FlatFileDefConstants.COUNTRYSTATE_CODE));
					busOwnerData.setOwnerZip(MesStringUtil.getPaddedString(rs.getString(FlatFileDefConstants.ADDRESS_ZIP).trim(), 9, "0"));					
					busOwnerData.setOwnerDob(rs.getString(FlatFileDefConstants.BUSOWNER_DL_DOB));
					String busOwnerSSN = MesStringUtil.getPaddedString(rs.getString(FlatFileDefConstants.BUSOWNER_SSN).trim(), 9, "0");
					busOwnerData.setOwnerSSN(StringUtils.rightPad(busOwnerSSN, 30));
					busOwnerData.setOwnerTitle(rs.getString(FlatFileDefConstants.BUSOWNER_TITLE));
					return busOwnerData;
				}
			}catch (SQLException e) {
	            log.error("Exception in AmexOptblueSPMFileBase:getBusOwnerData:",e);
	        }
		}
		
		return null;
		
	}
	
	/**
	 * 
	 * @param sellerId
	 * @return
	 * @throws SQLException
	 */
	public void setAuthorisedSigner(String sellerId) throws SQLException {

		try (PreparedStatement ps = con.prepareStatement(SQL_GET_AUTHORISED_SIGNER_DETAILS)) {
			ps.setString(1, sellerId);

			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					ffd.setAllFieldData(rs);
				}
			}catch (SQLException e) {
	            log.error("Exception in AmexOptblueSPMFileBase:setAuthorisedSigner:",e);
	        }
		}
	}
    
    /**
     * This will return a 9-position, zero-filled string as the ownerSSN from check either ssn or fedTaxID.
     * @param rs
     * @param ownerSSN
     * @return
     * @throws SQLException
     */
    private String setOwnerSSN(ResultSet rs,BusOwnerData busOwnerData) throws SQLException {
        String ownerSSN = "";
        try {
            if (!StringUtils.isEmpty(busOwnerData.getOwnerSSN())) {
                ownerSSN = busOwnerData.getOwnerSSN();
            } else if (!StringUtils.isEmpty(rs.getString(FlatFileDefConstants.SELLER_FED_TAXID))) {
                ownerSSN = MesStringUtil.getPaddedString(rs.getString(FlatFileDefConstants.SELLER_FED_TAXID).trim(), 9,
                        "0");
            }
        } catch (Exception e) {
            logEntry("setOwnerSSN()", e.toString());
            log.error("Exception in AmexOptblueSPMFileBase:setOwnerSSN " + e.getMessage());
        }

        return ownerSSN;
    }
    
    /**
     * This is builds the DT2 record in outgoing SPM file.
     * @param out
     * @param appSeqNum
     * @param sellerId
     * @param noOwnerDetails
     */
    private void buildDT2(BufferedWriter out,int appSeqNum, String sellerId,boolean noOwnerDetails, ResultSet resultSet) {
        
        log.debug("building detail(DT2) record");
        FlatFileRecord ffdDT2 = null;
        try {
            ffdDT2 = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_SPONSOR_MERCHANT_DT2);
            ffdDT2.resetAllFields();
            if (noOwnerDetails) {
                try (PreparedStatement ps = con.prepareStatement(SQL_GET_DT2_DETAILS_APP_SEQ_NUM_NO_OWNER_DETAILS)) {
                    ps.setInt(1, appSeqNum);
                    try (ResultSet rs = ps.executeQuery()) {
                        if (rs.next()) {
                            ffdDT2.setAllFieldData(rs);
                        }
                    }
                }
            } else {
                try (PreparedStatement ps = con.prepareStatement(SQL_GET_DT2_DETAILS_APP_SEQ_NUM)) {
                    ps.setInt(1, appSeqNum);
                    try (ResultSet rs = ps.executeQuery()) {
                        if(rs.next()) {
                            ffdDT2.setAllFieldData(rs);
                        }
                    }
                }
            }
            setAuthorisedSignerDetail(sellerId, ffdDT2);
            
            ffdDT2.setFieldData(FlatFileDefConstants.RECORD_NUMBER, ++recordnum);
            ffdDT2.setFieldData(FlatFileDefConstants.SELLER_TERMINATION_DATE,resultSet.getString("se_dtl_status_changedate"));
            ffdDT2.setFieldData(FlatFileDefConstants.SELLER_START_DATE, resultSet.getString("seller_start_date"));
            ffdDT2.setFieldData(FlatFileDefConstants.SALES_REP_ID, resultSet.getString("sales_rep_id"));
            ffdDT2.setFieldData(FlatFileDefConstants.SELLER_STATUS, resultSet.getString("seller_status"));
            ffdDT2.setFieldData(FlatFileDefConstants.SELLER_ID, sellerId);
            getSalesInfo(ffdDT2, resultSet);
            out.write(ffdDT2.spew(2));
            out.newLine();
        } catch (Exception e) {
            logEntry("buildDT2()", e.toString());
            log.error("Exception in AmexOptblueSPMFileBase:buildDT2" + e);
        }
}

	public void setAuthorisedSignerDetail(String sellerId, FlatFileRecord ffdDT2) throws SQLException {

		try (PreparedStatement ps = con.prepareStatement(SQL_GET_BUS_OWNER_INFO)) {
			ps.setInt(1, mesConstants.BUS_OWNER_PRIMARY);
			ps.setInt(2, mesConstants.ADDR_TYPE_OWNER1);
			ps.setString(3, sellerId);

			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					if (ffdDT2.getFieldData("auth_signer_dob").trim().isEmpty()) {
						ffdDT2.setFieldData("auth_signer_dob", rs.getString("busowner_dl_dob"));
					}
					if (ffdDT2.getFieldData("auth_signer_country").trim().isEmpty()) {
						ffdDT2.setFieldData("auth_signer_country", rs.getString("busowner_country"));
					}
				}
			} 
            catch (SQLException e) {
				log.error("Exception in AmexOptblueSPMFileBase:setAuthorisedSignerDetails:", e);
			}
		}
	}

	private void setAlternateAuthSigner(BusOwnerData primaryBusOwnerData) {

		if (ffd.getFieldData("signer_first_name").trim().isEmpty()) {
			ffd.setFieldData("signer_first_name", primaryBusOwnerData.getOwnerFirstName());
		}
		if (ffd.getFieldData("signer_last_name").trim().isEmpty()) {
			ffd.setFieldData("signer_last_name", primaryBusOwnerData.getOwnerLastName());
		}
		if (ffd.getFieldData("signer_title").trim().isEmpty()) {
			ffd.setFieldData("signer_title", primaryBusOwnerData.getOwnerTitle());
		}
	}

    protected void buildHeader(BufferedWriter out, String seNum) {
        log.debug("building header for SE#" + seNum);
        recordnum = 1;
        try {
            ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_SPONSOR_MERCHANT_HEADER);
            ffd.setFieldData("se_number", seNum);
            ffd.setFieldData("file_creation_date", Calendar.getInstance().getTime());
            ffd.setFieldData("file_creation_time", Calendar.getInstance().getTime());
            ffd.setFieldData("file_transmission_date", Calendar.getInstance().getTime());
            ffd.setFieldData("file_transmission_time", Calendar.getInstance().getTime());
            out.write(ffd.spew());
            out.newLine();
        } catch (Exception e) {
            logEntry("buildHeader()", e.toString());
        }
    }
       protected void buildTrailer(BufferedWriter out) {
            log.debug("building trailer");
            try {
                ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_SPONSOR_MERCHANT_TRAILER);
                ffd.setFieldData("record_number", ++recordnum);
                ffd.setFieldData("total_count", recordnum);
                out.write(ffd.spew());
                out.newLine();
            } catch (Exception e) {
                logEntry("buildTrailer()", e.toString());
            }
        }
	   
	protected void generateFile() {
        log.debug("generating file name...");
        try {
            fname = generateFilename("am_ob_spm" + getEventArg(0) , ".dat");
            loadFilenameToLoadFileId(fname);
            out = new BufferedWriter(new FileWriter(fname, false));
        } catch (Exception e) {
             logEntry("generateFile()", e.toString());
        } 
    }
	
	
	private void updateProcessSequence(){
	    
	    PreparedStatement ps          = null;
	    ResultSet         rs          = null;
	    int recordCount = 0;
	    
        try {
            String sqlHints = "where spm_process_sequence = 0 and merchant_number in (select merchant_number from mif where bank_number " + (getEventArg(0).equals("3943") ? "" : "not") + " in(3943))"; 
            ps = con.prepareStatement(SQL_GET_AMEX_OPTB_SPM_PROCESS + sqlHints);
            rs = ps.executeQuery();
            
            if(rs.next()){
                recordCount = rs.getInt(1);
            }
            ps.close();
            rs.close();
            
            if(recordCount > 0){
                ps = con.prepareStatement(SQL_GET_PROCESS_SEQUENCE);
                rs = ps.executeQuery();
                
                if(rs.next()){
                    processSequence = rs.getInt(1);
                }
                
                ps.close();
                rs.close();
                
                ps = con.prepareStatement(SQL_UPDATE_AMEX_OPTB_SPM_PROCESS + sqlHints);
                ps.setInt(1, processSequence);
                ps.setString(2, fname);
                ps.executeUpdate();
                
                ps.close();
                rs.close();
            }
            
        } catch (SQLException e) {
            log.error("Exception in AmexOptblueSPMFileBase:updateProcessSequence"+e.getMessage());
        }
	    finally
	    {
	      try{ 
	          if(ps != null) ps.close(); 
	          if(rs != null) rs.close(); 
	      } catch( Exception e ) {
	          log.error("Inside finally clause"+e.getMessage());
	       }
	    }
	}
	
	protected void getSalesInfo(FlatFileRecord ffd, ResultSet rs) {
      try(PreparedStatement ps = con.prepareStatement(SQL_GET_SALES_DATA)){
        ps.setString(1,rs.getString("anode"));
        ps.setString(2,rs.getString("g1node"));
        ps.setString(3,rs.getString("g2node"));
        ps.setString(4,rs.getString("g3node"));
        ps.setString(5,rs.getString("g4node"));
        ps.setString(6,rs.getString("g5node"));
        ps.setString(7,rs.getString("g6node"));
        try(ResultSet resultSet = ps.executeQuery()) {
            if(resultSet.next()){
                ffd.setFieldData("sales_channel_indicator", resultSet.getString("amex_channel_ind") );
                ffd.setFieldData("sales_channel_name", resultSet.getString("relationship_name"));
                ffd.setFieldData("iso_registration_number", resultSet.getString("iso_reg_number"));
                  
              } else {
                ffd.setFieldData("sales_channel_indicator", "DS");
                ffd.setFieldData("sales_channel_name", "INTEGRATED SALES");
              }
        }
        
      }catch(Exception e){
        logEntry("getSalesInfo(FlatFileRecord ffd,ResultSet rs)", e);
      }
    }
	
	private void updateSentFlag(HashSet<String> merchants){
	    PreparedStatement ps          = null;
	    
      try {
          ps = con.prepareStatement(SQL_UPDATE_PROCESS_TABLE);
        
          if(merchants != null){
          
          for(String merchant : merchants){
            ps.setString(1, merchant);
            ps.setInt(2, processSequence);
            ps.addBatch();
        }
            ps.executeBatch();
            ps.close();
     }
    } catch (SQLException e) {
        log.error("Exception in AmexOptblueSPMFileBase:updateProcessTable()" +e.getMessage());
    } finally {
        try{ 
            if(ps != null) ps.close(); 
        }catch( Exception e ) {
            log.error("Inside finally clause"+e.getMessage());
       }
    }
	}
	
	
	public static void main(String[] args) {

		AmexOptBlueSPMFileBase test = null;
		try {

			if (args.length > 0 && args[0].equals("testproperties")) {
				EventBase.printKeyListStatus(
						new String[] { MesDefaults.DK_AMEX_OUTGOING_HOST, MesDefaults.DK_AMEX_OUTGOING_USER,
								MesDefaults.DK_AMEX_OUTGOING_PASSWORD, MesDefaults.DK_AMEX_SPM_OUTGOING_PATH });
			}
			// args[0]: 9999  CB&T cap number
			// args[1]: manual run date, "MM/dd/yyyy" ex:12/11/2016
			test = new AmexOptBlueSPMFileBase();
 			test.setEventArgs(args);
			test.execute();
		} catch (Exception e) {
			log.debug(e.toString());
		} finally {
			try {
			    if(test != null)
				     test.cleanUp();
			} catch (Exception e) {
			    log.info(e.getMessage());
			}
			Runtime.getRuntime().exit(0);
		}

	}

}