/*@lineinfo:filename=AMMonitoringEvent*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/AMMonitoringEvent.sqlj $

  Description:

  Last Modified By   : $Author: swaxman $
  Last Modified Date : $Date: 2014-08-08 $
  Version            : $Revision: 17600 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

 **************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Vector;
import javax.mail.Message.RecipientType;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.support.PropertiesFile;
import com.mes.support.SMTPMail;
import sqlj.runtime.ResultSetIterator;

public class AMMonitoringEvent extends EventBase
{
	static Logger log = Logger.getLogger(AMMonitoringEvent.class);
	private static String defaultMEmailHost;

	public boolean TestMode = false;

	private Vector queryResults = new Vector();
	private SftpFilesCheck sftpobject = null;
	private String senderEmailAddress = "DevOpsTeam@merchante-solutions.com";
	private String[] devOpsNotices =  new String[]{};
	private String[] devOpsAlerts =  new String[]{};

	public boolean allPass;
	public boolean visaReconResult;
	public boolean undiffCountResult;
	public boolean mcReconResult;
	public boolean dsReconResult;
	public boolean amexReconResult;
	public boolean undiffProcResult;
	public boolean cycleProcResult;

	public AMMonitoringEvent() {
	  try {
	    String devOpsNoticesList = MesDefaults.getString(MesDefaults.AM_MONITORING_DEVOPS_NOTICES_EMAIL_LIST);
	    devOpsNotices = devOpsNoticesList.split(",");

	    String devOpsAlertsList = MesDefaults.getString(MesDefaults.AM_MONITORING_DEVOPS_ALERTS_EMAIL_LIST);
	    devOpsAlerts = devOpsAlertsList.split(",");
	  } catch (Exception e) {
	    logEntry("AMMonitoringEvent() constructor - set DevOps notices' emails and alerts' emails", e.toString());
	  }
	}
	
	public void getVisaReconResult(ResultSetIterator it, ResultSet rs) {
		try {
			visaReconResult = false;
			String[] cur = new String[2];
			cur[0] = "VISA - Generally 2 everyday at approx 3:35 AM and 4:00 AM - Many days we had to " +
					"reprocess so you may see them created later in the day. Generally if you do not see the " + 
					"these recons it indicates a undiff file processing issues";
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:84^4*/

//  ************************************************************
//  #sql [Ctx] it = { select substr(load_filename, 14, 6) mdate, to_char(to_date(substr(load_filename, 14, 6), 'mmddyy'), 'DY') mdow, count(*) count
//  					from load_file_index lfi
//  					where load_filename like 'vs_recon3941%'
//  					and creation_date > sysdate - 1
//  					group by substr(load_filename, 14, 6), to_char(to_date(substr(load_filename, 14, 6), 'mmddyy'), 'DY')
//  					order by 1 desc
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select substr(load_filename, 14, 6) mdate, to_char(to_date(substr(load_filename, 14, 6), 'mmddyy'), 'DY') mdow, count(*) count\n\t\t\t\t\tfrom load_file_index lfi\n\t\t\t\t\twhere load_filename like 'vs_recon3941%'\n\t\t\t\t\tand creation_date > sysdate - 1\n\t\t\t\t\tgroup by substr(load_filename, 14, 6), to_char(to_date(substr(load_filename, 14, 6), 'mmddyy'), 'DY')\n\t\t\t\t\torder by 1 desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.AMMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.AMMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:92^5*/

			rs = it.getResultSet();

			boolean pass = false;
			Calendar now = Calendar.getInstance();
			Calendar threeThirty = Calendar.getInstance();
			threeThirty.set(Calendar.HOUR_OF_DAY, 3);
			threeThirty.set(Calendar.MINUTE, 30);
			Calendar fourHundred = Calendar.getInstance();
			fourHundred.set(Calendar.HOUR_OF_DAY, 4);
			fourHundred.set(Calendar.MINUTE, 0);
			if(!rs.next()) {
				cur[1] = "<tr style='background-color:red'><td>No results returned.</td></tr>";
			} else {
				do
				{
					pass = (!rs.getString("mdate").equals(new SimpleDateFormat("MMddyy").format(now.getTime()))) ? (rs.getInt("count") == 2) : (now.after(threeThirty)) ? (now.after(fourHundred)) ? rs.getInt("count") == 2 : rs.getInt("count") == 1 : rs.getInt("count") == 0;
					visaReconResult = pass;
					cur[1] += "<tr style='background-color:" + (pass?"green":"red") + "'><td>" + rs.getString("mdate") + "</td><td>" + rs.getString("mdow") + "</td><td>" + rs.getString("count") + "</td></tr>";
				} while(rs.next());
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:119^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  					creation_date,
//  					text,
//  					achnowledgement,
//  					pass,
//  					error_type
//  				)
//  				values
//  				(
//  					sysdate,
//  					:cur[1],
//  					'N',
//  					:visaReconResult ? "Y" : "N",
//  					1
//  				)
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3642 = cur[1];
 String __sJT_3643 = visaReconResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\tcreation_date,\n\t\t\t\t\ttext,\n\t\t\t\t\tachnowledgement,\n\t\t\t\t\tpass,\n\t\t\t\t\terror_type\n\t\t\t\t)\n\t\t\t\tvalues\n\t\t\t\t(\n\t\t\t\t\tsysdate,\n\t\t\t\t\t :1 ,\n\t\t\t\t\t'N',\n\t\t\t\t\t :2 ,\n\t\t\t\t\t1\n\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.AMMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3642);
   __sJT_st.setString(2,__sJT_3643);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:136^4*/
		} catch (Exception e) {
			logEntry("getVisaReconResult()", e.toString());
		}
	}

	public void getUndiffProcCount(ResultSetIterator it, ResultSet rs) {
		try {
			undiffCountResult = false;
			String[] cur = new String[2];
			cur[0] = "There are about 4-6 undiff files expected everyday, please verify that they are processed";
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:148^4*/

//  ************************************************************
//  #sql [Ctx] it = { select mdate, count(*) count
//  					from (
//  							select distinct trunc(lfi.creation_date) mdate, replace(substr(load_filename, instr(load_filename, '_', -1)), '.dat', '') undiff_files_proc_with_suffix
//  							from load_file_index lfi
//  							where trunc(lfi.creation_date) = trunc(sysdate)
//  							and (load_filename like 'vs_recon%' or load_filename like 'vs_chgbk%'
//  									or load_filename like 'vs_fee%'
//  									or load_filename like 'vs_msg%' or load_filename like 'vs_retr%'
//  									or load_filename like 'vss3941_report%')
//  									and load_filename not like 'vs_fee%_001.ctf')
//  									group by mdate
//  									order by 1 desc
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select mdate, count(*) count\n\t\t\t\t\tfrom (\n\t\t\t\t\t\t\tselect distinct trunc(lfi.creation_date) mdate, replace(substr(load_filename, instr(load_filename, '_', -1)), '.dat', '') undiff_files_proc_with_suffix\n\t\t\t\t\t\t\tfrom load_file_index lfi\n\t\t\t\t\t\t\twhere trunc(lfi.creation_date) = trunc(sysdate)\n\t\t\t\t\t\t\tand (load_filename like 'vs_recon%' or load_filename like 'vs_chgbk%'\n\t\t\t\t\t\t\t\t\tor load_filename like 'vs_fee%'\n\t\t\t\t\t\t\t\t\tor load_filename like 'vs_msg%' or load_filename like 'vs_retr%'\n\t\t\t\t\t\t\t\t\tor load_filename like 'vss3941_report%')\n\t\t\t\t\t\t\t\t\tand load_filename not like 'vs_fee%_001.ctf')\n\t\t\t\t\t\t\t\t\tgroup by mdate\n\t\t\t\t\t\t\t\t\torder by 1 desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.AMMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.AMMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:162^5*/

			rs = it.getResultSet();

			boolean pass = false;
			Calendar now = Calendar.getInstance();
			if(!rs.next()) {
				cur[1] = "<tr style='background-color:red'><td>No results returned.</td></tr>";
			} else {
				do
				{
//					pass = rs.getInt("count") >= 4 && rs.getInt("count") <= 6;
					pass = true;
					undiffCountResult = pass;
					cur[1] += "<tr style='background-color:" + (pass?"green":"red") + "'><td>" + rs.getString("mdate") + "</td><td>" + rs.getString("count") + "</td></tr>";
				} while(rs.next());
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:184^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  						creation_date,
//  						text,
//  						achnowledgement,
//  						pass,
//  						error_type
//  						)
//  						values
//  						(
//  								sysdate,
//  								:cur[1],
//  								'N',
//  								:undiffCountResult ? "Y" : "N",
//  								2
//  								)
//  			 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3644 = cur[1];
 String __sJT_3645 = undiffCountResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\ttext,\n\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\tpass,\n\t\t\t\t\t\terror_type\n\t\t\t\t\t\t)\n\t\t\t\t\t\tvalues\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t2\n\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.AMMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3644);
   __sJT_st.setString(2,__sJT_3645);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:201^4*/
		} catch (Exception e) {
			logEntry("getUndiffProcCount()", e.toString());
		}
	}

	public void getMCReconResult(ResultSetIterator it, ResultSet rs) {
		try {
			mcReconResult = true;
			String[] cur = new String[2];
			cur[0] = " MC - Generally 5 in a day from Monday - Friday at approx 4:30 AM, 6:30 AM, 4:30 PM. 7:30 PM, 10:30 PM " +
					" -- 2 on Saturday  at approx 4:30 AM, 6:30 AM  --  3 on Sunday.at approx 3:00 / 3:30 PM. 7:30 PM, 10:30 PM";
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:214^4*/

//  ************************************************************
//  #sql [Ctx] it = { select substr(load_filename, 14, 6) mdate, to_char(to_date(substr(load_filename, 14, 6), 'mmddyy'), 'DY') mdow, count(*) count
//  					from load_file_index lfi
//  					where load_filename like 'mc_recon3941_%.ipm'
//  					and trunc(creation_date) >= trunc(sysdate - 1)
//  					group by substr(load_filename, 14, 6), to_char(to_date(substr(load_filename, 14, 6), 'mmddyy'), 'DY')
//  					order by 1 desc
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select substr(load_filename, 14, 6) mdate, to_char(to_date(substr(load_filename, 14, 6), 'mmddyy'), 'DY') mdow, count(*) count\n\t\t\t\t\tfrom load_file_index lfi\n\t\t\t\t\twhere load_filename like 'mc_recon3941_%.ipm'\n\t\t\t\t\tand trunc(creation_date) >= trunc(sysdate - 1)\n\t\t\t\t\tgroup by substr(load_filename, 14, 6), to_char(to_date(substr(load_filename, 14, 6), 'mmddyy'), 'DY')\n\t\t\t\t\torder by 1 desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.AMMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"4com.mes.startup.AMMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:222^5*/

			rs = it.getResultSet();

			boolean pass = false;

			Calendar now = Calendar.getInstance();
			Calendar fourThirty = Calendar.getInstance();
			fourThirty.set(Calendar.HOUR_OF_DAY, 4);
			fourThirty.set(Calendar.MINUTE, 30);
			Calendar sixThirty = Calendar.getInstance();
			sixThirty.set(Calendar.HOUR_OF_DAY, 6);
			sixThirty.set(Calendar.MINUTE, 30);
			Calendar fifteenHundred = Calendar.getInstance();
			fifteenHundred.set(Calendar.HOUR_OF_DAY, 15);
			fifteenHundred.set(Calendar.MINUTE, 0);
			Calendar sixteenThirty = Calendar.getInstance();
			sixteenThirty.set(Calendar.HOUR_OF_DAY, 16);
			sixteenThirty.set(Calendar.MINUTE, 30);
			Calendar nineteenThirty = Calendar.getInstance();
			nineteenThirty.set(Calendar.HOUR_OF_DAY, 19);
			nineteenThirty.set(Calendar.MINUTE, 30);
			Calendar twentyTwoThirty = Calendar.getInstance();
			twentyTwoThirty.set(Calendar.HOUR_OF_DAY, 22);
			twentyTwoThirty.set(Calendar.MINUTE, 30);
			if(!rs.next()) {
				cur[1] = "<tr style='background-color:red'><td>No results returned.</td></tr>";
				mcReconResult = false;
			} else {
				String mdow;
				do
				{
					mdow = rs.getString("mdow");
					if (mdow.equals("SUN")) {
						pass = (!rs.getString("mdate").equals(new SimpleDateFormat("MMddyy").format(now.getTime()))) ? (rs.getInt("count") == 3) : (now.after(fifteenHundred)) ? (now.after(nineteenThirty)) ? (now.after(twentyTwoThirty)) ? rs.getInt("count") == 3 : rs.getInt("count") == 2 : rs.getInt("count") == 1 : rs.getInt("count") == 0;
					}
					else if (mdow.equals("SAT")) {
						pass = (!rs.getString("mdate").equals(new SimpleDateFormat("MMddyy").format(now.getTime()))) ? (rs.getInt("count") == 2) : (now.after(fourThirty)) ? (now.after(sixThirty)) ? rs.getInt("count") == 2 : rs.getInt("count") == 1 : rs.getInt("count") == 0;
					} else {
						pass = (!rs.getString("mdate").equals(new SimpleDateFormat("MMddyy").format(now.getTime()))) ? (rs.getInt("count") == 5) : (now.after(fourThirty)) ? (now.after(sixThirty)) ? (now.after(sixteenThirty)) ? (now.after(nineteenThirty)) ? (now.after(twentyTwoThirty)) ? rs.getInt("count") == 5 : rs.getInt("count") == 4 : rs.getInt("count") == 3 : rs.getInt("count") == 2 : rs.getInt("count") == 1 : rs.getInt("count") == 0;
					}
					if(rs.getString("mdate").equals(new SimpleDateFormat("MMddyy").format(now.getTime()))) {
						mcReconResult &= pass;
					}
					cur[1] += "<tr style='background-color:" + (pass?"green":"red") + "'><td>" + rs.getString("mdate") + "</td><td>" + rs.getString("mdow") + "</td><td>" + rs.getString("count") + "</td></tr>";
				} while(rs.next());
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:274^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  						creation_date,
//  						text,
//  						achnowledgement,
//  						pass,
//  						error_type
//  						)
//  						values
//  						(
//  								sysdate,
//  								:cur[1],
//  								'N',
//  								:mcReconResult ? "Y" : "N",
//  								3
//  								)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3646 = cur[1];
 String __sJT_3647 = mcReconResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\ttext,\n\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\tpass,\n\t\t\t\t\t\terror_type\n\t\t\t\t\t\t)\n\t\t\t\t\t\tvalues\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t3\n\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.AMMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3646);
   __sJT_st.setString(2,__sJT_3647);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:292^6*/
		} catch (Exception e) {
			logEntry("getMCReconResult()", e.toString());
		}
	}

	public void getDSReconResult(ResultSetIterator it, ResultSet rs) {
		try {
			dsReconResult = false;
			String[] cur = new String[2];
			cur[0] = "DISCOVER - 5 everyday from Monday - Sunday at approx 6:30 AM - There " + 
					"is a catchup event at 8:30 AM if this does not go through.";
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:305^4*/

//  ************************************************************
//  #sql [Ctx] it = { select substr(load_filename, 14, 6) mdate, to_char(to_date(substr(load_filename, 14, 6), 'mmddyy'), 'DY') mdow, count(*) count
//  					from load_file_index lfi
//  					where load_filename like 'ds_recon%'
//  					and trunc(creation_date) = trunc(sysdate)
//  					group by substr(load_filename, 14, 6), to_char(to_date(substr(load_filename, 14, 6), 'mmddyy'), 'DY')
//  					order by 1 desc
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select substr(load_filename, 14, 6) mdate, to_char(to_date(substr(load_filename, 14, 6), 'mmddyy'), 'DY') mdow, count(*) count\n\t\t\t\t\tfrom load_file_index lfi\n\t\t\t\t\twhere load_filename like 'ds_recon%'\n\t\t\t\t\tand trunc(creation_date) = trunc(sysdate)\n\t\t\t\t\tgroup by substr(load_filename, 14, 6), to_char(to_date(substr(load_filename, 14, 6), 'mmddyy'), 'DY')\n\t\t\t\t\torder by 1 desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.AMMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"6com.mes.startup.AMMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:313^5*/

			rs = it.getResultSet();

			boolean pass = false;
			Calendar now = Calendar.getInstance();
			now.add(Calendar.DAY_OF_MONTH, -1);
			Calendar sixThirty = Calendar.getInstance();
			sixThirty.add(Calendar.DAY_OF_MONTH, -1);
			sixThirty.set(Calendar.HOUR_OF_DAY, 6);
			sixThirty.set(Calendar.MINUTE, 30);
			if(!rs.next()) {
				cur[1] = "<tr style='background-color:red'><td>No results returned.</td></tr>";
			} else {
				do
				{
					pass = (!rs.getString("mdate").equals(new SimpleDateFormat("MMddyy").format(now.getTime()))) ? (rs.getInt("count") == 5) : (now.after(sixThirty)) ? rs.getInt("count") == 5 : rs.getInt("count") == 0;
					dsReconResult = pass;
					cur[1] += "<tr style='background-color:" + (pass?"green":"red") + "'><td>" + rs.getString("mdate") + "</td><td>" + rs.getString("mdow") + "</td><td>" + rs.getString("count") + "</td></tr>";
				} while(rs.next());
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:339^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  						creation_date,
//  						text,
//  						achnowledgement,
//  						pass,
//  						error_type
//  						)
//  						values
//  						(
//  								sysdate,
//  								:cur[1],
//  								'N',
//  								:dsReconResult ? "Y" : "N",
//  								4
//  								)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3648 = cur[1];
 String __sJT_3649 = dsReconResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\ttext,\n\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\tpass,\n\t\t\t\t\t\terror_type\n\t\t\t\t\t\t)\n\t\t\t\t\t\tvalues\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t4\n\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.AMMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3648);
   __sJT_st.setString(2,__sJT_3649);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:357^6*/
		} catch (Exception e) {
			logEntry("getDSReconResult()", e.toString());
		}
	}

	public void getAmexReconResult(ResultSetIterator it, ResultSet rs) {
		try {
			String[] cur = new String[2];
			cur[0] = "AMEX - 1 everyday from Monday - Saturday at approx 6:30 AM - There is NONE on Sunday";
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:368^4*/

//  ************************************************************
//  #sql [Ctx] it = { select substr(load_filename, 16, 6) mdate, to_char(to_date(substr(load_filename, 16, 6), 'mmddyy'), 'DY') mdow, count(*) count
//  					from load_file_index lfi
//  					where load_filename like 'amex_recon%'
//  					and trunc(creation_date) = trunc(sysdate)
//  					group by substr(load_filename, 16, 6), to_char(to_date(substr(load_filename, 16, 6), 'mmddyy'), 'DY')
//  					order by 1 desc
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select substr(load_filename, 16, 6) mdate, to_char(to_date(substr(load_filename, 16, 6), 'mmddyy'), 'DY') mdow, count(*) count\n\t\t\t\t\tfrom load_file_index lfi\n\t\t\t\t\twhere load_filename like 'amex_recon%'\n\t\t\t\t\tand trunc(creation_date) = trunc(sysdate)\n\t\t\t\t\tgroup by substr(load_filename, 16, 6), to_char(to_date(substr(load_filename, 16, 6), 'mmddyy'), 'DY')\n\t\t\t\t\torder by 1 desc";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.AMMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.startup.AMMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:376^5*/

			rs = it.getResultSet();

			boolean pass = false;
			Calendar now = Calendar.getInstance();
			amexReconResult = now.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
			Calendar sixThirty = Calendar.getInstance();
			sixThirty.set(Calendar.HOUR_OF_DAY, 6);
			sixThirty.set(Calendar.MINUTE, 30);
			if(!rs.next()) {
				cur[1] = "<tr style='background-color:" + (amexReconResult?"green":"red") + "'><td>No results returned.</td></tr>";
			} else {
				do
				{
					pass = (!rs.getString("mdate").equals(new SimpleDateFormat("MMddyy").format(now.getTime()))) ? (rs.getInt("count") == 1) : (now.after(sixThirty)) ? rs.getInt("count") == 1 : rs.getInt("count") == 0;
					amexReconResult = pass;
					cur[1] += "<tr style='background-color:" + (pass?"green":"red") + "'><td>" + rs.getString("mdate") + "</td><td>" + rs.getString("mdow") + "</td><td>" + rs.getString("count") + "</td></tr>";
				} while(rs.next());
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:401^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  						creation_date,
//  						text,
//  						achnowledgement,
//  						pass,
//  						error_type
//  						)
//  						values
//  						(
//  								sysdate,
//  								:cur[1],
//  								'N',
//  								:amexReconResult ? "Y" : "N",
//  								5
//  								)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3650 = cur[1];
 String __sJT_3651 = amexReconResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\ttext,\n\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\tpass,\n\t\t\t\t\t\terror_type\n\t\t\t\t\t\t)\n\t\t\t\t\t\tvalues\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t5\n\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.AMMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3650);
   __sJT_st.setString(2,__sJT_3651);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:419^6*/
		} catch (Exception e) {
			logEntry("getAmexReconResult()", e.toString());
		}
	}

	public void getUndiffProcResult(ResultSetIterator it, ResultSet rs) {
		try {
			undiffProcResult = true;
			ArrayList ndmFiles = sftpobject.displayVisaFiles();
			ArrayList epFiles = new ArrayList();
			ArrayList dbFiles = new ArrayList();
			String[] cur = new String[2];
			cur[0] = "The following sql gives you the suffix for all undiff files that were processed. " + 
					"-- Goto EPS server and browse to c:/settlement/visa/inbound/arc. Count the number of undiff " + 
					"files that you see for given day since 12 at night.The count should match what is returned " + 
					"by the following SQL. -- If this query return fewer count than that seen on EPS then just " + 
					"compare the suffix on the undiff files on EPS wioth what you see here and you should be able" + 
					" to identify which was not processed. -- Copy all failed files to inbound/fail/. Then process " +
					"them one at a time by moving a single one to the inbound folder and running the 'Visa Incoming' " +
					"scheduled task, repeat for each unprocessed file. -- If asked about replacing report " +
					"file(vss####_DATE_*.txt) say yes. -- If files needed reprocessing re-run the report in Web " +
					"Reporting System. Found under 'Accounting Reports/MBS Visa Clearing Summary'. Need to re-run for each bank.";
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:443^4*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct replace(substr(load_filename, instr(load_filename, '_', -1)), '.dat', '') undiff_files_proc_with_suffix
//  					from load_file_index lfi
//  					where trunc(lfi.creation_date) = trunc(sysdate)
//  					and (load_filename like 'vs_recon%' or load_filename like 'vs_chgbk%'
//  							or load_filename like 'vs_fee%'
//  							or load_filename like 'vs_msg%' or load_filename like 'vs_retr%'
//  							or load_filename like 'vss3941_report%')
//  							and load_filename not like 'vs_fee%_001.ctf'
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct replace(substr(load_filename, instr(load_filename, '_', -1)), '.dat', '') undiff_files_proc_with_suffix\n\t\t\t\t\tfrom load_file_index lfi\n\t\t\t\t\twhere trunc(lfi.creation_date) = trunc(sysdate)\n\t\t\t\t\tand (load_filename like 'vs_recon%' or load_filename like 'vs_chgbk%'\n\t\t\t\t\t\t\tor load_filename like 'vs_fee%'\n\t\t\t\t\t\t\tor load_filename like 'vs_msg%' or load_filename like 'vs_retr%'\n\t\t\t\t\t\t\tor load_filename like 'vss3941_report%')\n\t\t\t\t\t\t\tand load_filename not like 'vs_fee%_001.ctf'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.AMMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.startup.AMMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:453^5*/
			if(ndmFiles.isEmpty()) {
				cur[1] += "<tr style='background-color:red'><td>No results returned from NDM.</td></tr> ";
				undiffProcResult = false;
			}

			rs = it.getResultSet();

			if(!rs.next()) {
				cur[1] += "<tr style='background-color:red'><td>No results returned from database.</td></tr>";
				undiffProcResult = false;
			} else {
				do
				{
					dbFiles.add(rs.getString("undiff_files_proc_with_suffix"));
				} while(rs.next());
			}

			Collections.sort(dbFiles);
			

			boolean pass = false;
			int ndmCount = ndmFiles.size();
			int dbCount = dbFiles.size();
			int epCount = 0;
			if(ndmCount != dbCount) {
				epFiles = sftpobject.displayProcessedFiles();
				epCount = epFiles.size();
			}
			int max = Math.max(Math.max(ndmCount, dbCount), epCount);
			for(int i = 0; i < max; i++){
				pass = i < ndmCount ? ((i < dbCount ? dbFiles.contains(ndmFiles.get(i)) : false) || (i < epCount ? epFiles.contains(ndmFiles.get(i)) : false)) : false;
				undiffProcResult &= pass;
				cur[1] += "<tr style='background-color:" + (pass?"green":"red") + "'><td>" + (i < ndmCount ? ndmFiles.get(i) : "") + "</td><td>" + (i < dbCount ? dbFiles.get(i) : "") + "</td><td>" + (i < epCount ? epFiles.get(i) : "") + "</td></tr>";
			}
			queryResults.add(cur);

			rs.close();
			it.close();

			/*@lineinfo:generated-code*//*@lineinfo:493^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  						creation_date,
//  						text,
//  						achnowledgement,
//  						pass,
//  						error_type
//  						)
//  						values
//  						(
//  								sysdate,
//  								:cur[1],
//  								'N',
//  								:undiffProcResult ? "Y" : "N",
//  								6
//  								)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3652 = cur[1];
 String __sJT_3653 = undiffProcResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\ttext,\n\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\tpass,\n\t\t\t\t\t\terror_type\n\t\t\t\t\t\t)\n\t\t\t\t\t\tvalues\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t6\n\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"11com.mes.startup.AMMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3652);
   __sJT_st.setString(2,__sJT_3653);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:511^6*/
		} catch (Exception e) {
			logEntry("getUndiffProcResult()", e.toString());
		}
	}

	public void getCycleProcResult(ResultSetIterator it, ResultSet rs) {
		try {
			cycleProcResult = true;
			ArrayList ndmFiles = sftpobject.displayMCFiles();
			ArrayList dbFiles = new ArrayList();
			String[] cur = new String[2];
			cur[0] = " Checking that all MC cycle files were processed. -- Run this query to see " +
					"how many cycle files were generated. -- ** MC - Generally 5 in a day from Monday - " +
					"Friday at approx 4:30 AM, 6:30 AM, 4:30 PM. 7:30 PM, 10:30 PM -- 2 on Saturday  at " + 
					"approx 4:30 AM, 6:30 AM -- 3 on Sunday.at approx 3:00 / 3:30 PM. 7:30 PM, 10:30 PM" + 
					"-- Check in EPS server in  C:/settlement/mc/inbound/arc to see how many cycle files " + 
					"are catually there.If a recon is missing and the ones seen here and the ones in the folder match up." + 
					" -- This implies that you will need to push the file from Orange again. Follow the instruction " +
					"in EPSupportDocumentation to resolve. --  1. sna @ SAC NDM      2.  cd mc/inbound/arc     3. " +
					"ls -alt mc_t112*060414* -- PLEASE regenerate MBS Clearing Summary - MasterCard report for the " + 
					"day that you ran the report as well as day before. -- Generally speaking if a 01, 02, 03 cycle " + 
					"file is reprocessed the report needs to be regenerated for day after -- and if 05, 06 cycle " +
					"file is  reprocessed then report needs to be regenerated for same day.   ";
			cur[1] = "";
			/*@lineinfo:generated-code*//*@lineinfo:536^4*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct trunc(lfi.creation_date), replace(substr(load_filename, instr(load_filename, '_', -1)), '.ipm', '') cycle_files_proc_with_suffix
//  					from load_file_index lfi
//  					where trunc(lfi.creation_date) = trunc(sysdate - 1)
//  					and (load_filename like 'mc_recon%' or load_filename like 'mc_chgbk%'
//  							or load_filename like 'mc_fee%' or load_filename like 'mc_retr%' )
//  							and load_filename not like 'mc_fee%001%'
//  							and to_char(lfi.creation_date, 'HH24') >= '15'
//  
//  							union
//  
//  							select distinct trunc(lfi.creation_date), replace(substr(load_filename, instr(load_filename, '_', -1)), '.ipm', '') cycle_files_proc_with_suffix
//  							from load_file_index lfi
//  							where trunc(lfi.creation_date) = trunc(sysdate)
//  							and (load_filename like 'mc_recon%' or load_filename like 'mc_chgbk%'
//  									or load_filename like 'mc_fee%' or load_filename like 'mc_retr%' )
//  									and load_filename not like 'mc_fee%001%' and to_char(lfi.creation_date, 'HH24') <= 8
//  				 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct trunc(lfi.creation_date), replace(substr(load_filename, instr(load_filename, '_', -1)), '.ipm', '') cycle_files_proc_with_suffix\n\t\t\t\t\tfrom load_file_index lfi\n\t\t\t\t\twhere trunc(lfi.creation_date) = trunc(sysdate - 1)\n\t\t\t\t\tand (load_filename like 'mc_recon%' or load_filename like 'mc_chgbk%'\n\t\t\t\t\t\t\tor load_filename like 'mc_fee%' or load_filename like 'mc_retr%' )\n\t\t\t\t\t\t\tand load_filename not like 'mc_fee%001%'\n\t\t\t\t\t\t\tand to_char(lfi.creation_date, 'HH24') >= '15'\n\n\t\t\t\t\t\t\tunion\n\n\t\t\t\t\t\t\tselect distinct trunc(lfi.creation_date), replace(substr(load_filename, instr(load_filename, '_', -1)), '.ipm', '') cycle_files_proc_with_suffix\n\t\t\t\t\t\t\tfrom load_file_index lfi\n\t\t\t\t\t\t\twhere trunc(lfi.creation_date) = trunc(sysdate)\n\t\t\t\t\t\t\tand (load_filename like 'mc_recon%' or load_filename like 'mc_chgbk%'\n\t\t\t\t\t\t\t\t\tor load_filename like 'mc_fee%' or load_filename like 'mc_retr%' )\n\t\t\t\t\t\t\t\t\tand load_filename not like 'mc_fee%001%' and to_char(lfi.creation_date, 'HH24') <= 8";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.AMMonitoringEvent",theSqlTS);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"12com.mes.startup.AMMonitoringEvent",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:554^5*/
			Calendar now = Calendar.getInstance();
			if(ndmFiles.isEmpty()) {
				cycleProcResult = now.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
				cur[1] += "<tr style='background-color:" + (cycleProcResult?"green":"red") + "'><td>No results returned from NDM.</td></tr> ";
			}

			rs = it.getResultSet();

			if(!rs.next()) {
				cur[1] += "<tr style='background-color:" + (cycleProcResult?"green":"red") + "'><td>No results returned from database.</td></tr>";
				cycleProcResult = now.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
			} else {
				do
				{
					dbFiles.add(rs.getString("cycle_files_proc_with_suffix"));
				} while(rs.next());
			}

			Collections.sort(dbFiles);

			boolean pass = false;
			int ndmCount = ndmFiles.size();
			int dbCount = dbFiles.size();
			int max = Math.max(ndmCount, dbCount);
			for(int i = 0; i < max; i++){
				pass = i < ndmCount && dbFiles.contains(ndmFiles.get(i));
				cycleProcResult &= pass;
				cur[1] += "<tr style='background-color:" + (pass?"green":"red") + "'><td>" + (i < ndmCount ? ndmFiles.get(i) : "") + "</td><td>" + (i < dbCount ? dbFiles.get(i) : "") + "</td></tr>";
			}
			queryResults.add(cur);

			rs.close();
			it.close();
			
			/*@lineinfo:generated-code*//*@lineinfo:589^4*/

//  ************************************************************
//  #sql [Ctx] { insert into mes.monitor_log
//  				(
//  						creation_date,
//  						text,
//  						achnowledgement,
//  						pass,
//  						error_type
//  						)
//  						values
//  						(
//  								sysdate,
//  								:cur[1],
//  								'N',
//  								:cycleProcResult ? "Y" : "N",
//  								7
//  								)
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_3654 = cur[1];
 String __sJT_3655 = cycleProcResult ? "Y" : "N";
   String theSqlTS = "insert into mes.monitor_log\n\t\t\t\t(\n\t\t\t\t\t\tcreation_date,\n\t\t\t\t\t\ttext,\n\t\t\t\t\t\tachnowledgement,\n\t\t\t\t\t\tpass,\n\t\t\t\t\t\terror_type\n\t\t\t\t\t\t)\n\t\t\t\t\t\tvalues\n\t\t\t\t\t\t(\n\t\t\t\t\t\t\t\tsysdate,\n\t\t\t\t\t\t\t\t :1 ,\n\t\t\t\t\t\t\t\t'N',\n\t\t\t\t\t\t\t\t :2 ,\n\t\t\t\t\t\t\t\t7\n\t\t\t\t\t\t\t\t)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"13com.mes.startup.AMMonitoringEvent",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,__sJT_3654);
   __sJT_st.setString(2,__sJT_3655);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:607^6*/
		} catch (Exception e) {
			logEntry("getCycleProcResult()", e.toString());
		}
	}

	/*
	 ** METHOD public void getData()
	 **
	 */
	public synchronized void getData()
	{
		ResultSetIterator it        = null;
		ResultSet         rs        = null;
		boolean           restrict  = false;
		sftpobject = SftpFilesCheck.sftpObject();

		try
		{
			loadProps();
			queryResults.clear();
			allPass = false;
			connect();

			/*@lineinfo:generated-code*//*@lineinfo:631^4*/

//  ************************************************************
//  #sql [Ctx] { delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type in ('1','2','3','4','5','6','7')
//  					 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "delete from mes.monitor_log where trunc(creation_date) = trunc(sysdate) and error_type in ('1','2','3','4','5','6','7')";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"14com.mes.startup.AMMonitoringEvent",theSqlTS);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:634^6*/

					getVisaReconResult(it, rs);
					getUndiffProcCount(it, rs);
					getMCReconResult(it, rs);
					getDSReconResult(it, rs);
					getAmexReconResult(it, rs);
					getUndiffProcResult(it, rs);
					getCycleProcResult(it, rs);
					allPass();
					sendAlertEmail(!allPass, "");

		}
		catch(Exception e)
		{
			logEntry("getData()", e.toString());
		}
		finally
		{
			try { rs.close(); } catch(Exception e) {}
			try { it.close(); } catch(Exception e) {}
			sftpobject.cleanUp();
			cleanUp();
		}
	}

	/**
	 * METHOD allPass
	 * 
	 * Sets the allPass variable
	 */
	public void allPass() {
		allPass = amexReconResult && cycleProcResult && dsReconResult && mcReconResult 
				&& undiffCountResult && undiffProcResult && visaReconResult;
	}

	public void sendAlertEmail(boolean alert, String emailContext) {
		String body = "";

		try {
			// create a mailer object with the host parameters in MesDefaults
			String emailHost = MesDefaults.getString(MesDefaults.DK_SMTP_HOST);
			SMTPMail mailer = new SMTPMail(emailHost);
			mailer.setFrom(senderEmailAddress);

			if (alert){ 
				int count = devOpsAlerts.length;
				for(int i = 0; i < count; i++) {
					mailer.addRecipient(RecipientType.TO, devOpsAlerts[i]);
					mailer.setSubject("AM Monitor Alert");
				}
			}
			else {
				int count = devOpsNotices.length;
				for(int i = 0; i < count; i++) {
					mailer.addRecipient(RecipientType.TO, devOpsNotices[i]);
					mailer.setSubject("AM Monitor Notification");                           
				}
			}

			if (!alert) {
				body += "\n";
				body += "------------------------------------------------------------" + "\n";
				body += "Visa, MC, Amex, and Discvoer, No issue.\n";
				body += "------------------------------------------------------------" + "\n";
				mailer.setText(body);
				mailer.send();
			}
			else {
				body += "\n";
				body += "One or more errors were found by the AM monitoring process.\n";
				body += emailContext;
				body += "\n";
				mailer.setText(body);
				mailer.send();
			}
		} catch(Exception e) {
			logEntry("sendEmail()", e.toString());
		}
	}
	
  private void loadProps()
  {
    log.debug("in loadProps... ");
    try
    {
      PropertiesFile props = new PropertiesFile("monitoring.properties");
      defaultMEmailHost = props.getString("DEFAULT_EMAIL_HOST");
      senderEmailAddress = props.getString("SENDER_EMAIL_ADDRESS", senderEmailAddress);
      devOpsNotices = props.getString("DEV_OPS_NOTICES").split(";");
      devOpsAlerts = props.getString("DEV_OPS_ALERTS").split(";");
    }
    catch(Exception e)
    {
      logEntry("loadProps()", e.toString());
      log.error(e.getMessage());
    }
  }
  
	public boolean execute( )
	{
		boolean result = true;
		getData();
		return( result );
	}

	public static void main(String[] args)
	{
		try
		{
			if (args.length > 0 && args[0].equals("testproperties")) {
				EventBase.printKeyListStatus(new String[]{
						MesDefaults.AM_MONITORING_DEVOPS_NOTICES_EMAIL_LIST,
						MesDefaults.AM_MONITORING_DEVOPS_ALERTS_EMAIL_LIST,
						MesDefaults.DK_SMTP_HOST
				});
			}

			//			com.mes.database.SQLJConnectionBase.initStandalone("DEBUG");
			AMMonitoringEvent worker = new AMMonitoringEvent();

			worker.execute();
		}
		catch(Exception e)
		{
			System.out.println("main(): " + e.toString());
			log.error(e.getMessage());
		}
	}
}/*@lineinfo:generated-code*/