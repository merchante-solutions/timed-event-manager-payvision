/*************************************************************************

  FILE: $URL: /src/main/com/mes/startup/AmexOptBlueSalesPerformFileBase.java $

  Description:

  Created by         : $Author: sceemarla    $
  Created Date		 : $Date:   2016-02-20  $
  Last Modified By   : $Author:             $
  Last Modified Date : $Date:               $
  Version            : $Revision:0.1        $

  Change History:
     See Bitbucket

  Copyright (C) 2000-2014,2015 ,2016 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.flatfile.FlatFileRecord;
import com.mes.support.DateTimeFormatter;
import com.mes.support.PropertiesFile;
import com.mes.utils.MesStringUtil;

public class AmexOptBlueSalesPerformFileBase extends AmexFileBase {

	private static final long serialVersionUID = 3172016783298886579L;

	BufferedWriter out = null;
	String fname = null;
	private long rec_num = 0;
	private FlatFileRecord ffd = null;

	public AmexOptBlueSalesPerformFileBase() {
		CardTypeFileDesc            = "Amex Optblue Sales Performance Report";
		DkOutgoingHost = MesDefaults.DK_AMEX_OUTGOING_HOST;
		DkOutgoingUser = MesDefaults.DK_AMEX_OUTGOING_USER;
		DkOutgoingPassword = MesDefaults.DK_AMEX_OUTGOING_PASSWORD;
		DkOutgoingPath = MesDefaults.DK_AMEX_OPTB_OUTGOING_PATH;
		SettlementAddrsNotify = MesEmails.MSG_ADDRS_AMEX_SPM_RESP;
		SettlementAddrsFailure = MesEmails.MSG_ADDRS_AMEX_SPM_RESP;
		DkOutgoingUseBinary = false;
		DkOutgoingSendFlagFile = true;
	}

	@Override
	protected boolean processTransactions() {

		PreparedStatement ps = null, pst = null,psDate = null;
		ResultSet rs = null, rst = null,rsDate = null;
		String capNumber = null;
		long fileSeqNum = 0;
		String query = null;
		String banksNumbers = null;
		log.debug("Starting AmexOptBlueSalesPerformFileBase:ProcessTransactions().....");
		Date manualRunDate = null;

        try {
          connect(true);
          String argDate = getEventArg(1);
          if(argDate!=null){
        	  manualRunDate = DateTimeFormatter.parseSQLDate( argDate, "MM/dd/yyyy") ;
        	  log.debug("Manual date to run Amex Optblue Sales Performance Report :"+argDate);
          } else {
        	  String dateQuery = " select add_months(trunc(sysdate, 'MM'), -1) " +
                      " from   dual ";
              psDate = con.prepareStatement(dateQuery);
              rsDate = psDate.executeQuery();
              if( rsDate.next() )
              {
            	  manualRunDate = rsDate.getDate(1);
              }
          }
          
          //In the below query 2F(Declined Amex) , 1C(Amex OptBlue) , 3E(PSP) , 1B (ESA)
          query = " select mf.merchant_number as seller_id," +
        		  "   mf.damexse as industry_se_number," +
        		  "   trunc(mf.amex_conversion_date)   as amex_sign_date," +
        		  "   mf.dba_name as seller_dba," +
        		  "   mf.dmstate as seller_region_code," +
        		  "   mf.sic_code as mcc," +
        		  "   CASE" +
        		  "   WHEN(mf.damexse IS NULL) THEN '2F'" +
        		  "   WHEN EXISTS (SELECT mpo.app_seq_num FROM merchpayoption mpo WHERE mpo.app_seq_num = me.app_seq_num AND mpo.cardtype_code = 16 AND mpo.amex_opt_blue ='Y') THEN '1C'" +
        		  "   WHEN EXISTS (SELECT mbs.bank_number FROM mbs_banks mbs WHERE mbs.am_aggregators LIKE CONCAT(CONCAT('%',mf.damexse),'%')) THEN '3E'" +
        		  "   ELSE '1B'" +
        		  "   END AS disposition," +
        		  "   app.app_user_login as sales_representative_id," +
        		  "   nvl(mf.association_node, 0) as anode," +
        		  "   mf.bank_number||nvl(mf.group_1_association,0) as g1node," +
        		  "   mf.bank_number||nvl(mf.group_2_association,0) as g2node," +
        		  "   mf.bank_number||nvl(mf.group_3_association,0) as g3node," +
        		  "   mf.bank_number||nvl(mf.group_4_association,0) as g4node," +
        		  "   mf.bank_number||nvl(mf.group_5_association,0) as g5node," +
        		  "   mf.bank_number||nvl(mf.group_6_association,0) as g6node" +
        		  "   from mif mf, merchant me, application app" +
        		  "   where TRUNC(app.app_created_date, 'MM') = TRUNC(TO_DATE(?,'DD-MON-YY'), 'MM')" +
        		  "   and mf.merchant_number = me.merch_number" +
        		  "   and me.app_seq_num = app.app_seq_num" +
        		  "   and mf.test_account = 'N'" +
        		  "   and 12 * nvl(me.merch_month_visa_mc_sales,0) < 1000000" +
        		  "   and mf.sic_code not in (select sic_code from amex_se_mapping where AMEX_OPTBLUE_SE_NUMBER = 0 and SE_DESCRIPTION = 'PROHIBITED')" +
        		  "   and mf.bank_number in (BANK_LIST)";

          String fileSequenceNumberQuery = "select sales_perf_file_sequence.NEXTVAL from dual";
          generateFile();
          pst = con.prepareStatement(fileSequenceNumberQuery);
          rst = pst.executeQuery();
          if (rst.next())
            fileSeqNum = rst.getLong(1);
          PropertiesFile props = new PropertiesFile("optblue.properties");
          capNumber = props.getString("cap_"+getEventArg(0));
          banksNumbers = props.getString("bank_list");
          if(banksNumbers==null){
        	  log.error("Please check the optblue.properties file, make sure bank_list is present if not add these to the property file");
          }
          log.debug("processing transactions for CAP Number:" + capNumber);
          log.debug("BanksList:" + banksNumbers);
          query = query.replace("BANK_LIST", banksNumbers);
          ps = con.prepareStatement(query);
          ps.setDate(1,manualRunDate);
          rs = ps.executeQuery();
          buildHeader(out, capNumber);
          buildDetail(out, rs);
          buildTrailer(out, fileSeqNum);
          out.flush();
          out.close();
          sendFile(fname);
        } catch (Exception e) {
          e.getStackTrace();
          logEntry("processTransactions()", e.toString());
          return false;
        } finally {
          try {
            ps.close();
            rs.close();
            pst.close();
            rst.close();
            psDate.close();
            rsDate.close();
          } catch (Exception e) {
          }
        }
        return true;
    }

	protected void buildHeader(BufferedWriter out, String capNumber) {
		log.debug("building header for CAP Number#" + capNumber);
		rec_num = 1;
		try {
			ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_SALES_PERFORM_HEADER);
			ffd.setFieldData("participant_cap_number", capNumber);
			out.write(ffd.spew());
			out.newLine();
		} catch (Exception e) {
			logEntry("buildHeader()", e.toString());
		}
	}

	protected void buildTrailer(BufferedWriter out, long fileSequenceNumber) {
		log.debug("building trailer");
		try {
			ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_SALES_PERFORM_TRAILER);
			ffd.setFieldData("file_sequence_number", fileSequenceNumber);
			ffd.setFieldData("file_transmission_date", Calendar.getInstance().getTime());
			ffd.setFieldData("record_count", ++rec_num);
			out.write(ffd.spew());
			out.newLine();
		} catch (Exception e) {
			logEntry("buildTrailer()", e.toString());
		}
	}

	protected void buildDetail(BufferedWriter out, ResultSet rs) {
		log.debug("building detail record");
		try {
			ffd = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_SALES_PERFORM_DETAIL);
			while (rs.next()) {
            	ffd.resetAllFields();

				ffd.setAllFieldData(rs);
				++rec_num;
				getSalesInfo(ffd,  rs);
				String disposition = rs.getString("disposition");
				if (disposition.equals("2F")) {
					ffd.setFieldData("seller_id", MesStringUtil.getPaddedString("", 20, " "));
					ffd.setFieldData("seller_dba", MesStringUtil.getPaddedString("", 25, " "));
					ffd.setFieldData("amex_sign_date", MesStringUtil.getPaddedString("", 8, " "));
				}
				if (!disposition.equals("1C")) {
					ffd.setFieldData("industry_se_number", MesStringUtil.getPaddedString("", 10, " "));
				} 
				/*
				String disposition = rs.getString("disposition");
				if (disposition.equals("1C") || disposition.equals("3E")) {
					ffd.setFieldData("seller_id", rs.getString("seller_id"));
				} else {
					ffd.setFieldData("seller_id", MesStringUtil.getPaddedString("", 20, " "));
				}

				if (disposition.equals("1B") || disposition.equals("1C") || disposition.equals("3E")
						|| disposition.equals("3F")) {
					ffd.setFieldData("seller_dba", rs.getString("seller_dba"));
				} else {
					ffd.setFieldData("seller_dba", MesStringUtil.getPaddedString("", 25, " "));
				} */
				
				// If Sellar state code is PR or VI , country code is setting to
				// sate code , and state code is space
				String ownerCountry = rs.getString("seller_region_code");
				if (ownerCountry != null
						&& (ownerCountry.equalsIgnoreCase("PR") || ownerCountry.equalsIgnoreCase("VI"))) {
					ffd.setFieldData("sellers_country_code", ownerCountry);
					ffd.setFieldData("seller_region_code", MesStringUtil.getPaddedString("", 6, " "));
				} else {
					ffd.setFieldData("sellers_country_code", "US");
					ffd.setFieldData("seller_region_code", rs.getString("seller_region_code"));
				}
				out.write(ffd.spew(2));
				out.newLine();
			}
			
		} catch (Exception e) {
			logEntry("buildDetail()", e.toString());
		}
	}
	/*
	 * Gets sales information for detail record
	 */
    protected void getSalesInfo(FlatFileRecord ffd, ResultSet rs) {
    	PreparedStatement ps = null;
    	ResultSet rs1 = null;
    	try{
    		ps = con.prepareStatement("select upper(am.relationship_name) as relationship_name, am.amex_channel_ind, am.iso_reg_number " +
    				" from amex_pse_mapping am where pse_banks in (?,?,?,?,?,?,?)");
    		ps.setString(1,rs.getString("anode"));
    		ps.setString(2,rs.getString("g1node"));
    		ps.setString(3,rs.getString("g2node"));
    		ps.setString(4,rs.getString("g3node"));
    		ps.setString(5,rs.getString("g4node"));
    		ps.setString(6,rs.getString("g5node"));
    		ps.setString(7,rs.getString("g6node"));
    		rs1 = ps.executeQuery();
    		if(rs1.next()){
    			ffd.setFieldData("sales_channel_indicator", rs1.getString("amex_channel_ind") );
    			ffd.setFieldData("sales_channel_name", rs1.getString("relationship_name")  );
    			ffd.setFieldData("iso_registration_number", rs1.getString("iso_reg_number"));
    		    
    		} else {
    			ffd.setFieldData("sales_channel_indicator", "DS");
    			ffd.setFieldData("sales_channel_name", "INTEGRATED SALES");
    		}
    	}catch(Exception e){
    		logEntry("getSalesInfo(FlatFileRecord ffd,ResultSet rs)", e.toString());
    	}finally{
    		try{
    			ps.close(); 
    			rs1.close();
    		} catch(Exception ignore){}
    	}
    }
    
	protected void generateFile() {
		log.debug("generating file name...");
		try {
			fname = generateFilename("am_ob_sales" + getEventArg(0), ".dat");
			loadFilenameToLoadFileId(fname);
			out = new BufferedWriter(new FileWriter(fname, false));
		} catch (Exception e) {
			logEntry("generateFile()", e.toString());
		}
	}

	public static void main(String[] args) {

		AmexOptBlueSalesPerformFileBase test = null;
		try {

			if (args.length > 0 && args[0].equals("testproperties")) {
				EventBase.printKeyListStatus(
						new String[] { MesDefaults.DK_AMEX_OUTGOING_HOST, MesDefaults.DK_AMEX_OUTGOING_USER,
								MesDefaults.DK_AMEX_OUTGOING_PASSWORD, MesDefaults.DK_AMEX_SPM_OUTGOING_PATH });
			}
			// args[0]: 9999  CB&T cap number
			// args[1]: manual run date, "MM/dd/yyyy" ex:12/11/2016
			test = new AmexOptBlueSalesPerformFileBase();
			test.setEventArgs(args);
			test.execute();
		} catch (Exception e) {
			e.printStackTrace();
			log.debug(e.toString());
		} finally {
			try {
				test.cleanUp();
			} catch (Exception e) {
			}
			Runtime.getRuntime().exit(0);
		}
	}

}
