/*@lineinfo:filename=TPGNonAPIBatchCutoff*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.30/svn/mesweb/trunk/src/main/com/mes/startup/TPGBatchCutoff.sqlj $

  Description:

  Last Modified By   : $Author: rsorensen $
  Last Modified Date : $Date: 2010-02-16 10:14:15 -0800 (Tue, 16 Feb 2010) $
  Version            : $Revision: 16987 $

  Change History:
     See VSS database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.api.ApiDb;
import com.mes.config.MesDefaults;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import sqlj.runtime.ResultSetIterator;

public class TPGNonAPIBatchCutoff extends EventBase
{
  private class BatchCloseInfo
  {
    public    Date          BatchDate     = null;
    public    int           BatchNumber   = 1;
    public    long          MerchantId    = 0L;
    public    String        TerminalId    = null;

    BatchCloseInfo( ResultSet resultSet )
      throws java.sql.SQLException
    {
      TerminalId    = resultSet.getString("terminal_id");
      BatchDate     = resultSet.getDate("batch_date");
      MerchantId    = resultSet.getLong("merchant_number");

      // establish a new batch number for this batch
      BatchNumber   = ((resultSet.getInt("last_batch_number") + 1)%1000);
      if ( BatchNumber == 0 )
      {
        BatchNumber = 1;    // do not allow 0 for a batch number
      }
    }

    public String toString()
    {
      return "TerminalId:: "+TerminalId;
    }
  }

  static Logger log = Logger.getLogger(TPGNonAPIBatchCutoff.class);

  private String        TestTerminalId    = null;

  //This is a duplicate of TPGBatchCutoff
  //focusing on the non-api profiles (for Acculynk currently)
  public boolean execute()
  {
    int                 batchCloseDefault   = 2100;
    Vector              batchesToClose      = new Vector();
    Date                currentDate         = null;
    int                 currentTime         = 0;
    ResultSetIterator   it                  = null;
    ResultSet           resultSet           = null;
    boolean             retVal              = false;

    try
    {
      connect();

      batchCloseDefault = MesDefaults.getInt(MesDefaults.DK_TPG_DEFAULT_BATCH_CLOSE_TIME);

      /*@lineinfo:generated-code*//*@lineinfo:92^7*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(sysdate),
//                  to_number(to_char(sysdate,'hh24mi'))
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc(sysdate),\n                to_number(to_char(sysdate,'hh24mi'))\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TPGNonAPIBatchCutoff",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   currentDate = (java.sql.Date)__sJT_rs.getDate(1);
   currentTime = __sJT_rs.getInt(2); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:98^7*/

      // select the non-api terminal ids that are ready to close their batch
      /*@lineinfo:generated-code*//*@lineinfo:101^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  tp.terminal_id                  as terminal_id,
//                  tp.merchant_number              as merchant_number,
//                  trunc(sysdate)                  as batch_date,
//                  nvl(tpapi.last_batch_number,0)  as last_batch_number
//          from    trident_profile         tp,
//                  trident_profile_api     tpapi
//          where   nvl(tp.api_enabled,'N') = 'N' and
//                  tp.terminal_id in
//                  (
//                    select  tapi.terminal_id
//                    from    trident_capture_api tapi
//                    where   tapi.mesdb_timestamp > sysdate - 4
//                            and tapi.server_name = 'Acculynk'
//                            and tapi.card_type = 'DB'
//                            and tapi.batch_date is null
//                            and nvl(tapi.test_flag,'Y') = 'N'
//                  )
//                  and tpapi.terminal_id(+) = tp.terminal_id and
//                  nvl(tpapi.last_auto_close_date,'01-JAN-2000') < :currentDate and
//                  nvl(tpapi.batch_close_time,:batchCloseDefault) <= :currentTime and
//                  ( :TestTerminalId is null or tp.terminal_id = :TestTerminalId )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tp.terminal_id                  as terminal_id,\n                tp.merchant_number              as merchant_number,\n                trunc(sysdate)                  as batch_date,\n                nvl(tpapi.last_batch_number,0)  as last_batch_number\n        from    trident_profile         tp,\n                trident_profile_api     tpapi\n        where   nvl(tp.api_enabled,'N') = 'N' and\n                tp.terminal_id in\n                (\n                  select  tapi.terminal_id\n                  from    trident_capture_api tapi\n                  where   tapi.mesdb_timestamp > sysdate - 4\n                          and tapi.server_name = 'Acculynk'\n                          and tapi.card_type = 'DB'\n                          and tapi.batch_date is null\n                          and nvl(tapi.test_flag,'Y') = 'N'\n                )\n                and tpapi.terminal_id(+) = tp.terminal_id and\n                nvl(tpapi.last_auto_close_date,'01-JAN-2000') <  :1  and\n                nvl(tpapi.batch_close_time, :2 ) <=  :3  and\n                (  :4  is null or tp.terminal_id =  :5  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.TPGNonAPIBatchCutoff",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,currentDate);
   __sJT_st.setInt(2,batchCloseDefault);
   __sJT_st.setInt(3,currentTime);
   __sJT_st.setString(4,TestTerminalId);
   __sJT_st.setString(5,TestTerminalId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.TPGNonAPIBatchCutoff",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:124^7*/
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        batchesToClose.add( new BatchCloseInfo(resultSet) );
      }
      resultSet.close();
      it.close();

      for( int i = 0; i < batchesToClose.size(); ++i )
      {
        BatchCloseInfo  bci = (BatchCloseInfo)batchesToClose.elementAt(i);

        ApiDb.closeBatch(bci.TerminalId,bci.BatchDate,bci.BatchNumber,currentDate);
      }

      /*@lineinfo:generated-code*//*@lineinfo:141^7*/

//  ************************************************************
//  #sql [Ctx] { commit
//         };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:144^7*/

      if(batchesToClose.size()>0)
      {
        sendBatchCloseEmail(batchesToClose);
      }

      retVal = true;
    }
    catch(Exception e)
    {
      try{ /*@lineinfo:generated-code*//*@lineinfo:155^12*/

//  ************************************************************
//  #sql [Ctx] { rollback  };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleRollback(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:155^34*/ }catch( Exception ee ) {}
      logEntry("execute()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
      cleanUp();
    }
    return( retVal );
  }

  protected void setTestTerminalId( String tid )
  {
    TestTerminalId = tid;
  }

  private void sendBatchCloseEmail(List batchesToClose)
  {
    try
    {
      BatchCloseInfo bci;

      StringBuffer message = new StringBuffer();
      for(Iterator it = batchesToClose.iterator();it.hasNext();)
      {
        bci = (BatchCloseInfo)it.next();
        message.append(bci.toString()).append("\n");
      }

      MailMessage msg = new MailMessage();
      msg.setFrom("Merchant e-Solutions <noreply@merchante-solutions.com>");
      msg.addTo("rsorensen@merchante-solutions.com");
      msg.setSubject("Non-Api Batch Close triggered");
      msg.setText(message.toString());
      msg.send();
    }
    catch(Exception e)
    {}
  }

  public static void main( String[] args )
  {
      try {

      if (args.length > 0 && args[0].equals("testproperties")) {
              EventBase.printKeyListStatus(new String[]{
                      MesDefaults.DK_TPG_DEFAULT_BATCH_CLOSE_TIME
              });
          }
      } catch (Exception e) {
          System.out.println(e.toString());
      }


      TPGNonAPIBatchCutoff      bc  = null;

    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");
      bc = new TPGNonAPIBatchCutoff();
      bc.connect();

      if ( "execute".equals(args[0]) )
      {
        bc.setTestTerminalId(args[1]);
        bc.execute();
      }
    }
    catch( Exception e )
    {
      log.error(e.toString());
    }
    finally
    {
      try{ bc.cleanUp(); } catch( Exception ee ) {}
    }
  }
}/*@lineinfo:generated-code*/