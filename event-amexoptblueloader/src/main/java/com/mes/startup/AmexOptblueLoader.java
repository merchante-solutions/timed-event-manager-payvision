/*************************************************************************

  FILE: $URL: /src/main/com/mes/startup/AmexOptBlueSalesPerformFileBase.java $

  Description:

  Created by         : $Author: Shailaja and Sai$
  Created Date		 : $Date:   2016-03-01  $
  Last Modified By   : $Author:             $
  Last Modified Date : $Date:               $
  Version            : $Revision:0.1        $

  Change History:
     See Bitbucket

  Copyright (C) 2000-2014,2015 ,2016 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.startup;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.database.OracleConnectionPool;
import com.mes.flatfile.FlatFileRecord;
import com.mes.net.MailMessage;
import com.mes.persist.vo.AmexOptbReconFeeRecord;
import com.mes.persist.vo.AmexOptbReconFileTransaction;
import com.mes.persist.vo.AmexOptbReconRocDtl;
import com.mes.persist.vo.AmexOptbReconRocLevel;
import com.mes.persist.vo.AmexOptbReconSummary;
import com.mes.persist.vo.AmexOptbReconSummaryDetail;
import com.mes.persist.vo.AmexOptbReconSummaryLevel;
import com.mes.settlement.ArdefLoader;
import com.mes.support.TridentTools;
import com.mes.tools.FileUtils;

public class AmexOptblueLoader extends EventBase {

	private static final long serialVersionUID = 1L;
	static Logger log = Logger.getLogger(AmexOptblueLoader.class);
	private boolean success = false;
	private String loadFilename;
	private String mailSub;
	private String mailText;
	private int mailId;
	private int delim;
	private static boolean manualRun = false;
	
    public static final String SPORG = "SPORG";
    public static final String SALRP = "SALRP";
    public static final String SCDNO = "SCDNO";
    public static final String SDPNO = "SDPNO";
    public static final String PRBIN = "PRBIN";
    public static final String EPTRN = "EPTRN";
    public static final String PREFIX = "MERCHESOBPRD"; // for test MERCHESOBTST

    public static final String SOC = "10";
    public static final String ROC = "11";
    public static final String SOC_LEVEL = "12";
    public static final String ROC_LEVEL = "13";
    public static final String CHARGEBACK = "20";
    public static final String ADJUSTMENT = "30";
    public static final String ASSEST_BILLING = "40";
    public static final String TAKE_ONE_COMMISSION = "41";
    public static final String OTHER_FEES = "50";
	 
	private static final HashMap<String, String> FilePrefixes = new HashMap<String, String>() {
		{
		    put("SPORG"      , "amex_obsresp" ); // Amex Optblue Sponsored merchant response file
			put("SALRP"      , "amex_saleresp"); // Amex Sales Performance response file
			put("SCDNO"      , "amex_scdno"); // Amex Seller Cancelled derogatory file
			put("SDPNO"      , "amex_sdpno"  );
	        put("PRBIN"      , "amex_bin"     ); // Amex Optblue Bin range file
	        put("EPTRN"      , "amex_ob_recon"); //Amex Optblue TILR file

		}
	};

    protected String getFileMask(String fType){
    	String fileMask = "";
    	if (fType == null)
    		fileMask = "MERCHANTE." + fType + "#.*";
    	else
    		fileMask = PREFIX +  "." +  fType + ".*";
    	return fileMask;
    }
    
	protected void loadFiles(String fType) {
		Sftp sftp = null;
		String filePrefix = null;
		String remoteFilename = null;
		boolean notify = false;
		try {
			// get the Sftp connection
			sftp = getSftp(MesDefaults.getString(MesDefaults.DK_AMEX_HOST),
					MesDefaults.getString(MesDefaults.DK_AMEX_USER),
					MesDefaults.getString(MesDefaults.DK_AMEX_PASSWORD),
					MesDefaults.getString(MesDefaults.DK_AMEX_INCOMING_PATH), false // !binary
			);
			EnumerationIterator enumi = new EnumerationIterator();
			Iterator it = enumi.iterator(sftp.getNameListing(getFileMask(fType)));
			while (it.hasNext()) {

				remoteFilename = (String) it.next();
				filePrefix = (String) FilePrefixes.get(fType);
				loadFilename = generateFilename(filePrefix + "3941");
				if (!sftp.isConnected()) {
					sftp = getSftp(MesDefaults.getString(MesDefaults.DK_AMEX_HOST),
							MesDefaults.getString(MesDefaults.DK_AMEX_USER),
							MesDefaults.getString(MesDefaults.DK_AMEX_PASSWORD),
							MesDefaults.getString(MesDefaults.DK_AMEX_INCOMING_PATH), false // !binary
					);
				}
				sftp.download(loadFilename, remoteFilename);
				if ("PRBIN".equals(fType)) {
					new ArdefLoader().loadArdefFile(loadFilename);
					setMailParams(fType);
					notifyTeam(null);
				} else if ("EPTRN".equals(fType)) {
					if ("recon".equals(getEventArg(1))) {
						loadReconFileData(loadFilename, fType);
					} else {
						loadReconData(loadFilename, fType);
					}
				} else {
					loadResponseFile(loadFilename, fType);
				}
				archiveDailyFile(loadFilename);
				success = true;
			}
		} catch (Exception e) {
			logEntry("loadFiles(" + fType + ")", e.toString());

		}
	}

	protected void loadResponseFile(String loadFilename, String responseFileType) {
		BufferedReader in = null;
		BufferedWriter out = null;
		String csvfile = null;
		String fields[] = null;
		String line = "";
		long file_id = 0L;

		FlatFileRecord ffd = null;
		boolean notify = true;
		log.debug("loading " + responseFileType + " response file");
		try {
			ffd = getFlatFileRecord(responseFileType);
			file_id = loadFilenameToLoadFileId(loadFilename);
			in = new BufferedReader(new FileReader(loadFilename));
			csvfile = loadFilename.replace(".dat", "-csv.csv");
			out = new BufferedWriter(new FileWriter(csvfile, true));
			line = csvResponseHeader(responseFileType);
			fields = line.split(",");
			out.write(line);
			out.newLine();
			notify = parseRespFile(in, ffd, fields, out);
			if (responseFileType.equals(SDPNO))
				storeSDPData(ffd, loadFilename, file_id);
			out.flush();
			log.debug("completed" + responseFileType + "file processing");

			// Needs to remove below commented lines in test/Prod ,
			// time being sending an email disabled by comment below statements
			in.close();
			out.close();
			
			if (responseFileType.equals(SPORG)) {
 				archiveDailyFile(csvfile);
 				notify = false;
 			}
 			if (notify) {
			 	setMailParams(responseFileType);
			 	notifyTeam(csvfile);
 			}
		} catch (Exception e) {
			logEntry("loadResponseFile(" + loadFilename + "," + responseFileType + ")", e.toString());
		}		
	}

	
	protected long findChargebackLoadSec( FlatFileRecord ffd, Date paymentDate){
		String adjRefNumColumnName = null;
		String amountColumnName    = null;
		String dateColumnName      = null;
		long loadSec               = 0L;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		Calendar cal = null;
		
		try{
			if(paymentDate!=null){
				cal = Calendar.getInstance();
				for( int i = 0; i < 2 && loadSec == 0L; ++i ){
					dateColumnName      = (i == 0) ? "incoming_date"        : "reversal_date";
					amountColumnName    = (i == 0) ? "cb_amount"            : "reversal_amount";
					adjRefNumColumnName = (i == 0) ? "cb_adjustment_number" : "reversal_ref_num";
					query = "select cbam.load_sec as load_sec from network_chargeback_amex_optb cbam where cbam.se_numb = ? and " + dateColumnName + 
							" between ? and ? and " +  adjRefNumColumnName + "= ? and " + amountColumnName + " = ?" ;
					ps = con.prepareStatement(query);
					ps.setString(1, ffd.getFieldData("amex_se_number").trim());
					cal.setTime(paymentDate);
					cal.add(Calendar.DAY_OF_MONTH, -3); // paymentdate-3 
					ps.setDate(2, new java.sql.Date(cal.getTime().getTime()));
					cal.setTime(paymentDate);
					cal.add(Calendar.DAY_OF_MONTH, 3); // paymentdate+3
					ps.setDate(3, new java.sql.Date(cal.getTime().getTime()));
					ps.setLong(4, ffd.getFieldAsLong("adjustment_number"));
					ps.setDouble(5, TridentTools.decodeCobolAmount(ffd.getFieldData("adjustment_amount").trim(),2));
					rs = ps.executeQuery();
					if(rs.next())
						loadSec = rs.getLong("load_sec");
				}
			}

		}catch (Exception e) {
			logEntry("findChargebackLoadSec(" + ffd + ")", e.toString());
		} finally
		{
			try{ ps.close(); rs.close(); } catch( Exception e ) {}
		}
		return loadSec;
	}
	protected void loadReconData(String fname, String responseFileType){
      HashMap ffds = new HashMap();
      FlatFileRecord ffd = null;
      long loadFileId = 0L;
      PreparedStatement ps;
      String filename = null;
      BufferedReader in = null;
      String line = null;
      String recKey = null;
      long loadSec = 0L;
      log.debug("loading recon file " + fname);
      String query = "insert into amex_settlement_recon_optblue( amex_payee_number,amex_sort_field_1,amex_sort_field_2,payment_year,payment_number,record_type," +
              "detail_record_type,payment_date,payment_amount,debit_balance_amount,aba_bank_number,se_dda_number,amex_se_number,se_unit_number,se_business_date," +
              "amex_process_date,soc_invoice_number,soc_amount,discount_amount,service_fee_amount,net_soc_amount,discount_rate,service_fee_rate,amex_gross_amount," +
              "amex_roc_count,tracking_id,cpc_indicator,amex_roc_count_poa,base_discount_amount,card_not_present_bpa_amount,card_not_present_pta_amount," +
              "card_not_present_bpa_rate,card_not_present_pta_rate,transaction_fee_amount,transaction_fee_rate,fee_code,fee_description,fee_rate,fee_amount," +
              "roc_amount,cm_ref_no,se_ref,roc_number,tran_date,se_ref_poa,capn_non_compliant_indicator,capn_non_compliant_ec1," +
              "capn_non_compliant_ec2,capn_non_compliant_ec3,capn_non_compliant_ec4,non_swiped_indicator,dq_non_compliant_indicator," +
              "dq_non_compliant_ec,cm_number_19,merchant_number,chargeback_amount,net_chargeback_amount,chargeback_reason," +
              "batch_code,bill_code,adjustment_number,adjustment_amount,net_adjustment_amount,adjustment_reason,asset_billing_amount," +
              "asset_billing_description,take_one_commission_amount,take_one_description,other_fee_amount,other_fee_description,asset_billing_tax,pay_in_gross_indicator," +
              "recon_date,cb_load_sec,card_number,card_number_enc,load_filename,load_fileid) values(?,?,?,?,?,?,?,to_date(?),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," +
              "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,dukpt_encrypt_wrapper(?),?,?)";

      try{
          ffds.put("DFHDR" , new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_EPRAW_HDR) );
          ffds.put("00"    ,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_EPRAW_SUMMARY) );
          ffds.put("10"    ,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_SOC) );
          ffds.put("12"    ,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_PRICING_SOC) );
          ffds.put("11"    ,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_ROC) );
          ffds.put("13"    ,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_PRICING_ROC));
          ffds.put("20"    ,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_CB) );
          ffds.put("30"    ,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_ADJ) );
          ffds.put("DFTRL" ,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_EPRAW_TRL) );
          ffd = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_OTHER );
          ffds.put("40", ffd); //Assets Billing
          ffds.put("41", ffd); //Take-One Commissions
          ffds.put("50", ffd); // Other Fees

          filename  = FileUtils.getNameFromFullPath(fname).toLowerCase();
          loadFileId    = loadFilenameToLoadFileId(filename);
          ps = con.prepareStatement("delete from amex_settlement_recon_optblue where load_fileid = ?" );
          ps.setLong(1, loadFileId);
          ps.executeUpdate();

          if(manualRun) {
        	  deleteDuplicateData(fname);
          }     
          in = new BufferedReader( new FileReader(fname) );
          Date paymentDateFromSummRecord = null;
          while( (line = in.readLine()) != null ){
              recKey  = (line.charAt(0) == 'D' ? line.substring(0,5) : line.substring(43,45));
              String cmNum = "";
              Date paymentDate = null;
              
              ffd = (FlatFileRecord)ffds.get(recKey);
              if(ffd != null){
                  ffd.resetAllFields();
                  ffd.suck(line);
                  if(line.charAt(0) != 'D'){
                      loadSec = 0;
                      switch( ffd.getDefType() )
                      {
                        case MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_ADJ:
                            paymentDate = getPaymentDate(paymentDateFromSummRecord, ffd, filename);
                            loadSec = findChargebackLoadSec(ffd, paymentDate);
                            break;
                        case MesFlatFiles.DEF_TYPE_AMEX_EPRAW_SUMMARY:
                            try {
                                paymentDate = ffd.getFieldAsSqlDate("payment_date");
                            } catch (Exception e) {
                                log.error(e.toString());
                            }
                            if (paymentDate != null) {
                                paymentDateFromSummRecord = paymentDate;
                            }
                            break;
                        default:
                            paymentDate = getPaymentDate(paymentDateFromSummRecord, ffd, filename);
                            break;
                      }
                      ps = con.prepareStatement(query);
                      cmNum = ffd.getFieldData("cm_number").trim();
                      if( cmNum != "" && (cmNum.charAt(0) == '0') && (cmNum.charAt(1) == '0'))
                          cmNum = cmNum.substring(2);

                      int i = 0;
                      ps.setString(++i, ffd.getFieldData("amex_payee_number").trim());
                      ps.setString(++i, ffd.getFieldData("amex_sort_field_1").trim());
                      ps.setString(++i, ffd.getFieldData("amex_sort_field_2").trim());
                      ps.setString(++i, ffd.getFieldData("payment_year").trim());
                      ps.setString(++i, ffd.getFieldData("payment_number").trim());
                      ps.setString(++i, ffd.getFieldData("record_type").trim());
                      ps.setString(++i, ffd.getFieldData("detail_record_type").trim());
                      
                      ps.setDate(++i, paymentDate);
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("payment_amount").trim(),2));
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("debit_balance_amount").trim(),2));
                      ps.setString(++i, ffd.getFieldData("aba_bank_number").trim());
                      ps.setString(++i, ffd.getFieldData("se_dda_number").trim());
                      ps.setString(++i, ffd.getFieldData("amex_se_number").trim());
                      ps.setString(++i, ffd.getFieldData("se_unit_number").trim());
                      ps.setDate(++i, ffd.getFieldAsSqlDate("se_business_date"));
                      ps.setDate(++i, ffd.getFieldAsSqlDate("amex_process_date"));
                      ps.setString(++i, ffd.getFieldData("soc_invoice_number").trim());
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("soc_amount").trim(),2));
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("discount_amount").trim(),2));
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("service_fee_amount").trim(),2));
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("net_soc_amount").trim(),2));
                      ps.setDouble(++i,  ffd.getFieldAsDouble("discount_rate")/1000);
                      ps.setDouble(++i,  ffd.getFieldAsDouble("service_fee_rate")/1000);
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("amex_gross_amount").trim(),2));
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("amex_roc_count").trim(),0));
                      ps.setString(++i, ffd.getFieldData("tracking_id").trim());
                      ps.setString(++i, ffd.getFieldData("cpc_indicator").trim());
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("amex_roc_count_poa").trim(),0));
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("base_discount_amount").trim(),2));
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("card_not_present_bpa_amount").trim(),2));
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("card_not_present_pta_amount").trim(),2));
                      ps.setDouble(++i,  ffd.getFieldAsDouble("card_not_present_bpa_rate")/1000);
                      ps.setDouble(++i,  ffd.getFieldAsDouble("card_not_present_pta_rate")/1000);
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("transaction_fee_amount").trim(),2));
                      ps.setDouble(++i, ffd.getFieldAsDouble("transaction_fee_rate")/1000  );
                      ps.setString(++i, ffd.getFieldData("fee_code").trim());
                      ps.setString(++i, ffd.getFieldData("fee_description").trim());
                      ps.setDouble(++i, ffd.getFieldAsDouble("fee_rate")/1000  );
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("fee_amount").trim(),2));
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("roc_amount").trim(),2));
                  //  ps.setString(++i, ffd.getFieldData("cm_number").trim()); //this value will be inserted to card_number column
                      ps.setString(++i, ffd.getFieldData("cm_ref_no").trim());
                      ps.setString(++i, ffd.getFieldData("se_ref").trim());
                      ps.setString(++i, ffd.getFieldData("roc_number").trim());
                      ps.setDate(++i, ffd.getFieldAsSqlDate("tran_date"));
                      ps.setString(++i, ffd.getFieldData("se_ref_poa").trim());
                      ps.setString(++i, ffd.getFieldData("capn_non-compliant_indicator").trim());
                      ps.setString(++i, ffd.getFieldData("capn_non-compliant_error_code_1").trim());
                      ps.setString(++i, ffd.getFieldData("capn_non-compliant_error_code_2").trim());
                      ps.setString(++i, ffd.getFieldData("capn_non-compliant_error_code_3").trim());
                      ps.setString(++i, ffd.getFieldData("capn_non-compliant_error_code_4").trim());
                      ps.setString(++i, ffd.getFieldData("non-swiped_indicator").trim());
                      ps.setString(++i, ffd.getFieldData("data_quality_non-compliant_indicator").trim());
                      ps.setString(++i, ffd.getFieldData("data_quality_non-compliant_error_code").trim());
                      ps.setString(++i, TridentTools.encodeCardNumber(ffd.getFieldData("cm_number_19").trim()));
                      ps.setString(++i, ffd.getFieldData("merchant_number").trim());
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("chargeback_amount").trim(),2));
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("net_chargeback_amount").trim(),2));
                      ps.setString(++i, ffd.getFieldData("chargeback_reason").trim());
                      ps.setString(++i, ffd.getFieldData("batch_code").trim());
                      ps.setString(++i, ffd.getFieldData("bill_code").trim());
                      ps.setString(++i, ffd.getFieldData("adjustment_number").trim());
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("adjustment_amount").trim(),2));
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("net_adjustment_amount").trim(),2));
                      ps.setString(++i, ffd.getFieldData("adjustment_reason").trim());
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("asset_billing_amount").trim(),2));
                      ps.setString(++i, ffd.getFieldData("asset_billing_description").trim());
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("take_one_commission_amount").trim(),2));
                      ps.setString(++i, ffd.getFieldData("take_one_description").trim());
                      ps.setDouble(++i, TridentTools.decodeCobolAmount(ffd.getFieldData("other_fee_amount").trim(),2));
                      ps.setString(++i, ffd.getFieldData("other_fee_description").trim());
                      ps.setString(++i, ffd.getFieldData("asset_billing_tax").trim());
                      ps.setString(++i, ffd.getFieldData("pay_in_gross_indicator").trim());
                      ps.setDate(++i, paymentDate); // recon_date
                      ps.setLong(++i, loadSec);
                      // card number
                      ps.setString(++i, TridentTools.encodeCardNumber(cmNum));

                      // card_number_enc
                      if(!"".equals(cmNum))
                          ps.setString( ++i, cmNum );
                      else if (!"".equals(ffd.getFieldData("cm_number_19").trim()))
                          ps.setString( ++i, ffd.getFieldData("cm_number_19").trim() );
                      else
                          ps.setString( ++i, null );

                      ps.setString(++i, fname);
                      ps.setLong(++i, loadFileId);
                      ps.executeUpdate();
                      ps.close();
                      if(ffd.getDefType() == MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_ADJ || ffd.getDefType()== MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_CB )
                          updateChgbkOptb(loadSec,paymentDate,ffd.getFieldAsLong("adjustment_number")  );
                  }
              }
          }
          setMailParams(responseFileType);
          notifyTeam(null);
      }catch(Exception e){
          logEntry("loadReconData(" + responseFileType + ")", e.toString());
      }finally {
          try{ in.close(); } catch( Exception e ) {}
      }
      log.debug("Completed loading recon file " + fname);
    }
	
	public void loadReconFileData(String fname, String responseFileType){

		HashMap ffds = new HashMap();
		FlatFileRecord ffd = null;
		long loadFileId = 0L;
		String filename = null;
		BufferedReader in = null;
		String line = null;
		String recKey = null;
		long loadSec = 0L;
		log.debug("loading recon file " + fname);

		AmexOptBlueReconLoaderBO reconLoaderBO = null;
		List<AmexOptbReconFileTransaction> optbReconFileTransactionList = new ArrayList<AmexOptbReconFileTransaction>();
		AmexOptbReconFileTransaction optbReconFileTransaction = null;
		AmexOptbReconSummary amexOptbReconSummary = null;
		AmexOptbReconSummaryDetail amexOptbReconSummaryDetail = null;
		AmexOptbReconSummaryLevel amexOptbReconSummaryLevel = null;
		AmexOptbReconRocDtl amexOptbReconRocDtl = null;
		AmexOptbReconRocLevel amexOptbReconRocLevel = null;
		AmexOptbReconFeeRecord amexOptbReconFeeRecord = null;

		boolean newRecord = false;

		try{
			ffds.put("DFHDR"	 	, new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_EPRAW_HDR) );
			ffds.put("00"    		,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_EPRAW_SUMMARY) );
			ffds.put(SOC    		,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_SOC) );
			ffds.put(SOC_LEVEL    ,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_PRICING_SOC) );
			ffds.put(ROC    		,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_ROC) );
			ffds.put(ROC_LEVEL    ,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_PRICING_ROC));
			ffds.put(CHARGEBACK   ,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_CB) );
			ffds.put(ADJUSTMENT   ,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_ADJ) );
			ffds.put("DFTRL" 		,new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_EPRAW_TRL) );

			ffd = new FlatFileRecord(Ctx,MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_OTHER );
			ffds.put(ASSEST_BILLING, ffd); //Assets Billing
			ffds.put(TAKE_ONE_COMMISSION, ffd); //Take-One Commissions
			ffds.put(OTHER_FEES, ffd); // Other Fees

			filename  = FileUtils.getNameFromFullPath(fname).toLowerCase();
			loadFileId    = loadFilenameToLoadFileId(filename);

			deleteIfRecordExist(loadFileId);
			
			in = new BufferedReader( new FileReader(fname) );
			int recordCount = 0;
			Date paymentDateFromSummRecord = null;
			while( (line = in.readLine()) != null ){
				newRecord = false;
				recKey  = (line.charAt(0) == 'D' ? line.substring(0,5) : line.substring(43,45));
				String cmNum = "";
				Date paymentDate = null;
				ffd = (FlatFileRecord)ffds.get(recKey);
				if(ffd != null){
					ffd.resetAllFields();
					ffd.suck(line);
                    if (line.charAt(0) != 'D') {
                        loadSec = 0;
                        switch (ffd.getDefType()) 
                        {
                            case MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_ADJ:
                                paymentDate = getPaymentDate(paymentDateFromSummRecord, ffd, filename);
                                loadSec = findChargebackLoadSec(ffd, paymentDate);
                                break;
                            case MesFlatFiles.DEF_TYPE_AMEX_EPRAW_SUMMARY:
                                try {
                                    paymentDate = ffd.getFieldAsSqlDate("payment_date");
                                } catch (Exception e) {
                                    log.error(e.toString());
                                }
                                if (paymentDate != null) {
                                    paymentDateFromSummRecord = paymentDate;
                                }
                                newRecord = true;
                                break;
                            default:
                                paymentDate = getPaymentDate(paymentDateFromSummRecord, ffd, filename);
                                break;
                        }

						cmNum = ffd.getFieldData("cm_number").trim();
						if( cmNum != "" && (cmNum.charAt(0) == '0') && (cmNum.charAt(1) == '0'))
							cmNum = cmNum.substring(2);

						String recordType = ffd.getFieldData("record_type").trim();
						String detialRecordType =  ffd.getFieldData("detail_record_type").trim();

						if(newRecord){
							//log.debug("Summary Main Record");
							optbReconFileTransaction = new AmexOptbReconFileTransaction();
							amexOptbReconSummary = new AmexOptbReconSummary();
							//Summary Table data
							//amexOptbReconSummary.setSummaryId(summaryId);
							amexOptbReconSummary.setFileId(loadFileId);
							amexOptbReconSummary.setFileName(fname);
							amexOptbReconSummary.setFileDate(paymentDate);
							amexOptbReconSummary.setPayeeNumber(getLongValueForString(ffd.getFieldData("amex_payee_number").trim()));
							amexOptbReconSummary.setPaymentYear(getLongValueForString(ffd.getFieldData("payment_year").trim()));
							amexOptbReconSummary.setPaymentNumber(ffd.getFieldData("payment_number").trim());
							amexOptbReconSummary.setPaymentDate(paymentDate);//recon date
							amexOptbReconSummary.setPaymentAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("payment_amount").trim(),2));
							amexOptbReconSummary.setDebitBalanceAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("debit_balance_amount").trim(),2));
							amexOptbReconSummary.setAbaBankNumber(ffd.getFieldData("aba_bank_number").trim());
							amexOptbReconSummary.setSeDdaNumber(ffd.getFieldData("se_dda_number").trim());
							//amexOptbReconSummary.setRecordCount(recordCount); -- get this from trail record

							optbReconFileTransaction.setAmexOptbReconSummary(amexOptbReconSummary);
							if(optbReconFileTransaction!=null){
								optbReconFileTransactionList.add(optbReconFileTransaction);
							}
						}

						if(detialRecordType!=null && !"".equals(detialRecordType) && detialRecordType.equals(SOC)){
							//log.debug("Summary Detail Record");
							amexOptbReconSummaryDetail = new AmexOptbReconSummaryDetail();
							//Summary Details Table data
							//amexOptbReconSummaryDetail.setSummaryDetailId(summaryDetailId);
							//amexOptbReconSummaryDetail.setSummaryId(summaryId);
							amexOptbReconSummaryDetail.setSeNumber(getLongValueForString(ffd.getFieldData("amex_se_number").trim()));
							amexOptbReconSummaryDetail.setSeBusinessDate(ffd.getFieldAsSqlDate("se_business_date"));
							amexOptbReconSummaryDetail.setProcessDate(ffd.getFieldAsSqlDate("amex_process_date"));
							amexOptbReconSummaryDetail.setSocInvoiceNumber(getLongValueForString(ffd.getFieldData("soc_invoice_number").trim()));
							amexOptbReconSummaryDetail.setSocAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("soc_amount").trim(),2));
							amexOptbReconSummaryDetail.setDiscountAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("discount_amount").trim(),2));
							amexOptbReconSummaryDetail.setServiceFeeAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("service_fee_amount").trim(),2));
							amexOptbReconSummaryDetail.setNetSocAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("net_soc_amount").trim(),2));
							amexOptbReconSummaryDetail.setGrossAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("amex_gross_amount").trim(),2));
							amexOptbReconSummaryDetail.setTrackingId(getLongValueForString(ffd.getFieldData("tracking_id").trim()));
							amexOptbReconSummaryDetail.setCpcIndicator(ffd.getFieldData("cpc_indicator").trim());
							amexOptbReconSummaryDetail.setBaseDiscountAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("base_discount_amount").trim(),2));
							amexOptbReconSummaryDetail.setCardNotPresentBpaAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("card_not_present_bpa_amount").trim(),2));
							amexOptbReconSummaryDetail.setCardNotPresentPtaAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("card_not_present_pta_amount").trim(),2));
							amexOptbReconSummaryDetail.setTransactionFeeAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("transaction_fee_amount").trim(),2));
							amexOptbReconSummaryDetail.setTransactionFeeRate(ffd.getFieldAsDouble("transaction_fee_rate")/1000); //not sure if we need to add all rates to the new table
							amexOptbReconSummaryDetail.setTransactionFeeRate(ffd.getFieldAsDouble("card_not_present_bpa_rate")/1000);
							amexOptbReconSummaryDetail.setTransactionFeeRate(ffd.getFieldAsDouble("card_not_present_pta_rate")/1000);
							amexOptbReconSummaryDetail.setTransactionFeeRate(ffd.getFieldAsDouble("service_fee_rate")/1000);
							amexOptbReconSummaryDetail.setTransactionFeeRate(ffd.getFieldAsDouble("discount_rate")/1000);
							optbReconFileTransaction.addSummaryDetailList(amexOptbReconSummaryDetail);

						} else if(detialRecordType!=null && !"".equals(detialRecordType) && detialRecordType.equals(SOC_LEVEL)){
							//log.debug("Summary Level Record");
							//Summary level Table data
							amexOptbReconSummaryLevel = new AmexOptbReconSummaryLevel();
							//amexOptbReconSummaryLevel.setSummaryLevelId(summaryLevelId);
							//amexOptbReconSummaryLevel.setSummaryDetailId(summaryDetailId);
							amexOptbReconSummaryLevel.setFeeCode(ffd.getFieldData("fee_code").trim());
							amexOptbReconSummaryLevel.setFeeDesc(ffd.getFieldData("fee_description").trim());
							amexOptbReconSummaryLevel.setDiscountAmount(getStringAmountToDouble(ffd.getFieldData("discount_amount").trim()));
							amexOptbReconSummaryLevel.setFeeAmount(getStringAmountToDouble(ffd.getFieldData("fee_amount").trim()));
							amexOptbReconSummaryLevel.setFeeRate( ffd.getFieldAsDouble("fee_rate")/1000  );
							amexOptbReconSummaryLevel.setDiscountRate( ffd.getFieldAsDouble("discount_rate")/1000  );
							if(amexOptbReconSummaryDetail!=null){
								amexOptbReconSummaryDetail.setReconSummaryLevel(amexOptbReconSummaryLevel);
							}
							//optbReconFileTransaction.addSummaryLevelList(amexOptbReconSummaryLevel);
						} else if(detialRecordType!=null && !"".equals(detialRecordType) && detialRecordType.equals(ROC)){
							//log.debug("ROC Record");
							//ROC Detail Table data
							amexOptbReconRocDtl = new AmexOptbReconRocDtl();
							//amexOptbReconRocDtl.setRocDetailId(rocDetailId);
							//amexOptbReconRocDtl.setSummaryDetailId(summaryDetailId);
							amexOptbReconRocDtl.setCmNumber(TridentTools.encodeCardNumber(cmNum));
							amexOptbReconRocDtl.setCmNumberEnc(cmNum);//added the encrytion db wrapper in the insert query in DAO
							amexOptbReconRocDtl.setCmNumber19(TridentTools.encodeCardNumber(cmNum));
							amexOptbReconRocDtl.setTranDate(ffd.getFieldAsSqlDate("tran_date"));
							amexOptbReconRocDtl.setCmRefNo(ffd.getFieldData("cm_ref_no").trim());
							amexOptbReconRocDtl.setSeRefPoa(ffd.getFieldData("se_ref_poa").trim());//what about SE_REF?
							amexOptbReconRocDtl.setCapnNonCompliantIndicator(ffd.getFieldData("capn_non-compliant_indicator").trim());
							String compliantError = null;
							if(ffd.getFieldData("capn_non-compliant_indicator").trim()!=null){
								compliantError = ffd.getFieldData("capn_non-compliant_indicator").trim();
							} else if(ffd.getFieldData("capn_non-compliant_error_code_1").trim()!=null){
								compliantError = ffd.getFieldData("capn_non-compliant_error_code_1").trim();
							} else if(ffd.getFieldData("capn_non-compliant_error_code_2").trim()!=null){
								compliantError = ffd.getFieldData("capn_non-compliant_error_code_2").trim();
							} else if(ffd.getFieldData("capn_non-compliant_error_code_3").trim()!=null){
								compliantError = ffd.getFieldData("capn_non-compliant_error_code_3").trim();
							} else if(ffd.getFieldData("capn_non-compliant_error_code_4").trim()!=null){
								compliantError = ffd.getFieldData("capn_non-compliant_error_code_4").trim();
							} 
							amexOptbReconRocDtl.setCapnNonCompliantError(compliantError);
							amexOptbReconRocDtl.setDqNonComplaintError(ffd.getFieldData("data_quality_non-compliant_indicator").trim());
							amexOptbReconRocDtl.setDqNonCompliantInd(ffd.getFieldData("data_quality_non-compliant_error_code").trim());
							amexOptbReconRocDtl.setNonSwipedInd(ffd.getFieldData("non-swiped_indicator").trim());
							if(amexOptbReconSummaryDetail!=null){
								amexOptbReconSummaryDetail.addRocDtlList(amexOptbReconRocDtl);
							}
							//optbReconFileTransaction.addRocDtlList(amexOptbReconRocDtl);
						} else if(detialRecordType!=null && !"".equals(detialRecordType) && detialRecordType.equals(ROC_LEVEL)){
							//log.debug("ROC Level record");
							amexOptbReconRocLevel = new AmexOptbReconRocLevel();
							//amexOptbReconRocLevel.setRocLevelId(rocLevelId);
							//amexOptbReconRocLevel.setRocDetailId(rocDetailId);
							amexOptbReconRocLevel.setFeeCode(ffd.getFieldData("fee_code").trim());
							amexOptbReconRocLevel.setFeeDesc(ffd.getFieldData("fee_description").trim());
							amexOptbReconRocLevel.setDiscountAmount(getStringAmountToDouble(ffd.getFieldData("discount_amount").trim()));
							amexOptbReconRocLevel.setFeeAmount(getStringAmountToDouble(ffd.getFieldData("fee_amount").trim()));
							amexOptbReconRocLevel.setFeeRate( ffd.getFieldAsDouble("fee_rate")/1000  );
							amexOptbReconRocLevel.setDiscountRate( ffd.getFieldAsDouble("discount_rate")/1000  );
							if(amexOptbReconRocDtl!=null){
								amexOptbReconRocDtl.addRocLevelList(amexOptbReconRocLevel);
							}
							//optbReconFileTransaction.addRocLevelList(amexOptbReconRocLevel);
						} else if (detialRecordType != null && (detialRecordType.equals(CHARGEBACK)
								|| detialRecordType.equals(ADJUSTMENT) || detialRecordType.equals(ASSEST_BILLING)
								|| detialRecordType.equals(TAKE_ONE_COMMISSION)
								|| detialRecordType.equals(OTHER_FEES))) {
							//log.debug("Other record");
							amexOptbReconFeeRecord = new AmexOptbReconFeeRecord();
							//amexOptbReconFeeRecord.setFeeId(feeId);
							//amexOptbReconFeeRecord.setSummaryId(summaryId);
							amexOptbReconFeeRecord.setSeNumber(getLongValueForString(ffd.getFieldData("amex_se_number").trim()));
							amexOptbReconFeeRecord.setRecType(recordType);
							amexOptbReconFeeRecord.setDetailRecType(detialRecordType);
							amexOptbReconFeeRecord.setProcessDate(ffd.getFieldAsSqlDate("amex_process_date"));
							//below field are not populated for other type of records, can populate if it is needed.
							//discount_amount
							//service_fee_amount
							//discount_rate
							//service_fee_rate							

							amexOptbReconFeeRecord.setLoadSec(loadSec);
							
							amexOptbReconFeeRecord.setBillCode(ffd.getFieldData("bill_code").trim());
							amexOptbReconFeeRecord.setMerchantNumber(getLongValueForString(ffd.getFieldData("merchant_number").trim()));
							if(detialRecordType.equals(CHARGEBACK) || detialRecordType.equals(ADJUSTMENT)){
								amexOptbReconFeeRecord.setBatchCode(ffd.getFieldData("batch_code").trim());
							}

							if(detialRecordType.equals(CHARGEBACK)) {
								amexOptbReconFeeRecord.setChargebackAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("chargeback_amount").trim(),2));
								amexOptbReconFeeRecord.setNetChargebackAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("net_chargeback_amount").trim(),2));
								amexOptbReconFeeRecord.setChargebackReason(ffd.getFieldData("chargeback_reason").trim());
							}

							if(detialRecordType.equals(ADJUSTMENT)){
								amexOptbReconFeeRecord.setAdjustmentNumber(getLongValueForString(ffd.getFieldData("adjustment_number").trim()));
								amexOptbReconFeeRecord.setAdjustmentAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("adjustment_amount").trim(),2));
								amexOptbReconFeeRecord.setNetAdjustmentAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("net_adjustment_amount").trim(),2));
								//amexOptbReconFeeRecord.setPayInGrossIndicator(payInGrossIndicator);
								amexOptbReconFeeRecord.setCmNumber(TridentTools.encodeCardNumber(cmNum));
								amexOptbReconFeeRecord.setAdjustmentReason(ffd.getFieldData("adjustment_reason").trim());
							}

							if(detialRecordType.equals(ASSEST_BILLING)){
								amexOptbReconFeeRecord.setAssetBillingAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("asset_billing_amount").trim(),2));
								amexOptbReconFeeRecord.setAssetBillingDescription(ffd.getFieldData("asset_billing_description").trim());
								amexOptbReconFeeRecord.setAssetBillingTax(ffd.getFieldData("asset_billing_tax").trim());
							}

							if(detialRecordType.equals(TAKE_ONE_COMMISSION)){
								amexOptbReconFeeRecord.setTakeOneCommissionAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("take_one_commission_amount").trim(),2));
								amexOptbReconFeeRecord.setTakeOneDescription(ffd.getFieldData("take_one_description").trim());
							}

							if(detialRecordType.equals(OTHER_FEES)){
								amexOptbReconFeeRecord.setOtherFeeAmount(TridentTools.decodeCobolAmount(ffd.getFieldData("other_fee_amount").trim(),2));
								amexOptbReconFeeRecord.setOtherFeeDescription(ffd.getFieldData("other_fee_description").trim());
								amexOptbReconFeeRecord.setPayInGrossIndicator(ffd.getFieldData("pay_in_gross_indicator").trim());
							}
							if(amexOptbReconSummaryDetail!=null){
								amexOptbReconSummaryDetail.addReconFeeRecordList(amexOptbReconFeeRecord);
							}
							//optbReconFileTransaction.addFeeRecordList(amexOptbReconFeeRecord);
						}
						if(ffd.getDefType() == MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_ADJ || ffd.getDefType()== MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_DTL_CB )
							updateChgbkOptb(loadSec,paymentDate,ffd.getFieldAsLong("adjustment_number")  );
					} else {
						if("DFTRL".equals(recKey)){
							log.debug("DFTRL record of the TILR file.");
							recordCount = getIntValueForString(ffd.getFieldData("df_trl_record_count").trim());
						}
					}
				}
			}
			reconLoaderBO = new AmexOptBlueReconLoaderBO(con);
			reconLoaderBO.insertReconFileData(optbReconFileTransactionList,recordCount);
			setMailParams(responseFileType);
			notifyTeam(null);
		}catch(Exception e){
			logEntry("loadReconFileData(" + responseFileType + ")", e.toString());
		}finally {
			try{ if(in!=null) in.close(); } catch( Exception e ) {}
		}
		log.debug("Completed loading recon file " + fname);

	}
	
	private void deleteIfRecordExist(long loadFileId) {
		log.debug("delete existing recon file id if exists: " + loadFileId);
		try{
			AmexOptBlueReconLoaderBO reconLoaderBO = new AmexOptBlueReconLoaderBO(con);
			reconLoaderBO.deleteReconFileData(loadFileId);	
		} catch (Exception e) {
			logEntry("deleteIfRecordExist(" + loadFileId + ")", e.toString());
		}
		
	}

	protected long getLongValueForString(String strVal){
		long longVal = 0;
		try{
			if(strVal!=null && !"".equals(strVal)){
				longVal = Long.valueOf(strVal);
			}
		} catch (Exception e) {
			log.debug("Exception in AmexOptBlueLoader::getLongValueForString.Exception Message: "+ e.getMessage());
		} 
		return longVal;

	}
	
	protected int getIntValueForString(String strVal){
		int intVal = 0;
		try{
			if(strVal!=null && !"".equals(strVal)){
				intVal = Integer.valueOf(strVal);
			}
		} catch (Exception e) {
			log.debug("Exception in AmexOptBlueLoader::getIntValueForString.Exception Message: "+ e.getMessage());
		} 
		return intVal;

	}
	
	/*
	 * forms a date using payment year and the first 3 digits(julian date) of payment number
	 */
	protected Date convertJulian(FlatFileRecord ffd, boolean suppressErrorLog){
      String query = "";
      PreparedStatement ps = null;
      ResultSet rs = null;
      Date JDate = null;
      String paymentYear = null;
      String paymentNumber = null;

      try{
	    paymentYear = ffd.getFieldData("payment_year").trim();
        paymentNumber = ffd.getFieldData("payment_number").trim();
        query = "select to_date( (" + paymentYear + "||" + paymentNumber.substring(0,3) + ") ,'yyyyddd') as pdate from dual";
        ps = con.prepareStatement(query);
        rs = ps.executeQuery();
        if( rs.next() ) {
          JDate = rs.getDate("pdate");
        }
      }catch(Exception e){
          if(!suppressErrorLog) {
              log.error(e.toString());
          }else {
              log.info("Invalid julian date found. Payment Number: "
                      + ffd.getFieldData("payment_number").trim() + ", Detail Record Type: "
                      + ffd.getFieldData("detail_record_type").trim() + ", and Record Type: "
                      + ffd.getFieldData("record_type").trim());
          }
      }finally {
        try{
          if(ps != null) ps.close();
          if(rs != null) rs.close();
        } catch(Exception e){
          //Ignore
        }
      }

      return JDate;
    }
	/*
	 * updates settlement_date and reversal_date in network_chargeback_amex_optb table
	 */
	
	protected void updateChgbkOptb(long loadSec, Date recDate, long adj){
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		ResultSet rs = null;
		try{
			ps = con.prepareStatement("update network_chargeback_amex_optb set settlement_date =? where load_sec = ?");
			ps.setDate(1, recDate);
			
			if(loadSec == 0){
				ps.setNull(2, java.sql.Types.INTEGER);
			} else
				ps.setLong(2, loadSec);
			
			ps.executeUpdate();
			ps.close();
			if(adj != 0){
				ps = con.prepareStatement("select load_sec from network_chargeback_amex_optb where reversal_ref_num = ?");
				ps.setLong(1, adj);
				rs = ps.executeQuery();
				
				//Updates reversal date in case of reversals
				if(rs.next()){
					ps1 = con.prepareStatement("update network_chargeback_amex_optb set reversal_date =? where load_sec =?");
					ps1.setDate(1, recDate);
					ps1.setLong(2,  rs.getLong(1) );
					ps1.executeUpdate();
				}
			}
			
			
		}catch(Exception e){
			logEntry("updateChgbkOptb(" + loadSec +"," + recDate + "," + adj + ")", e.toString());
		} finally {
			try{
				if(ps != null) ps.close();
				if(ps1 != null) ps1.close();
				if(rs != null) rs.close();
			} catch(Exception e){
				//Ignore
			}
			
		}
		
	}
	
	protected FlatFileRecord getFlatFileRecord(String responseFileType) {
		FlatFileRecord ffr = null;
		switch (responseFileType.toUpperCase()) {
		case SPORG:
		    ffr = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_SPONSOR_MERCHANT_DETAIL);
		    break;
		case SALRP:
			ffr = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_SALES_PERFORM_DETAIL);
			break;
		case SCDNO:
			ffr = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_SELLER_CANCELLATIONS_DETAIL);
			break;	
		case SDPNO:
			ffr = new FlatFileRecord(MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_SELLER_DISPUTES_DETAIL);
			break;	
		default:
			ffr = null;
			break;

		}
		return ffr;
	}

	protected boolean parseRespFile(BufferedReader in, FlatFileRecord ffd, String fields[], BufferedWriter out) 
			throws IOException{
		String line = "";
		boolean notify = false;
		while ((line = in.readLine()) != null){
			if(line.startsWith("D")){
				if( delim == 0 || line.substring(delim).contains("N")){
					notify = true;
					ffd.suck(line);
					for(int i =0;i<fields.length;i++)
						out.write((ffd.getFieldData(fields[i])).replaceAll(",", " ") + ",");
					out.newLine();
				}
			}
		}
		return notify;
	}
	
	protected boolean storeSDPData(FlatFileRecord ffd, String loadFilename, long file_id){
		PreparedStatement ps = null;
		String line = "";
		BufferedReader in;
		String query = "  insert into mes.amex_optblue_scd (" +
						" cap_number, creation_date,se_number," +
				        " merchant_number, prg_indicator, prg_status_indicator," +
				        " prg_effective_date, load_file_id, load_filename)" +
				        "values(?,trunc(sysdate),?,?,?,?,?,?,?)";
		String cap = "";
		boolean notify = false;
		
		try{
			ps = con.prepareStatement(query);
			in = new BufferedReader(new FileReader(loadFilename));
			while ((line = in.readLine()) != null){
				if(line.startsWith("HDR"))
					cap = line.substring(17, 27);
				else if(line.startsWith("DTL")){
					ffd.suck(line);
					
					ps.setString(1, cap);
					ps.setString(2, ffd.getFieldData("se_number"));
					ps.setString(3, ffd.getFieldData("seller_id"));
					ps.setString(4, ffd.getFieldData("prg_indicator"));
					ps.setString(5, ffd.getFieldData("prg_status_indicator"));
					ps.setDate(6, ffd.getFieldAsSqlDate("prg_effective_date")  );
					ps.setLong(7, file_id);
					ps.setString(8, loadFilename);
					ps.executeUpdate();
					notify = true;
				} 
			}
		}catch(Exception e){
			logEntry("storeSDPData()", e.toString());
		}finally {
			try {
				ps.close();
			} catch (Exception e) {
			}
		}
		return notify;
	}
	
	protected String csvResponseHeader(String responseFileType) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select name from flat_file_def where def_Type = ?";
		StringBuffer line = new StringBuffer();
		log.debug("generating Response file header of csv file");

		try {
			ps = con.prepareStatement(query);
			setDefType(ps, responseFileType);
			rs = ps.executeQuery();
			line.setLength(0);
			while (rs.next()) {
				if (rs.getString("name").equals("filler"))
					continue;
				else
					line.append(rs.getString("name") + ",");
			}
			line.deleteCharAt(line.lastIndexOf(","));
		} catch (Exception e) {
			logEntry("csvSPMHeader", e.toString());
		} finally {
			try {
				ps.close();
				rs.close();
			} catch (Exception e) {
			}
		}
		log.debug("generating header for RESPONSE FILE file is completed");
		return line.toString();
	}

	protected void setDefType(PreparedStatement ps, String responseFileType) throws SQLException {
		switch (responseFileType.toUpperCase()) {
		case SPORG:
		    ps.setInt(1, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_SPONSOR_MERCHANT_DETAIL);
		    delim = 1463;
		    break;
		case SALRP:
			ps.setInt(1, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_SALES_PERFORM_DETAIL);
			delim = 308;
			break;
		case SCDNO:
			ps.setInt(1, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_SELLER_CANCELLATIONS_DETAIL);
			break;
		case SDPNO:
			ps.setInt(1, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_SELLER_DISPUTES_DETAIL);
			break;	
		}

	}
	
	protected void setMailParams(String fType){
		switch(fType.toUpperCase()){
		case SPORG:
		    mailId = MesEmails.MSG_ADDRS_AMEX_SPM_RESP;
		    mailText = "Please refer to the attached Daily Amex Optblue Sponsored Merchant file response file";
		    mailSub = "Amex Optblue Sponsored Merchant response file";
		    break;
		case SALRP:
			mailId = MesEmails.MSG_ADDRS_AMEX_SPM_RESP;
			mailSub = "Please refer to the attached Daily Amex Optblue Sales Performance report response file";
			mailText = "Amex Optble Sales Performance report response file";
			break;
		case SCDNO:
			mailId = MesEmails.MSG_ADDRS_AMEX_SPM_RESP;
			mailSub = "Please refer to the attached Amex Optblue Seller Cancelled Derogatory Notification file";
			mailText = "Amex Optblue Seller Cancelled Derogatory Notification";
			break;
		case SDPNO:
			mailId = MesEmails.MSG_ADDRS_AMEX_SDP_RISK;
			mailSub = "Please refer to the attached Amex Optblue Seller Disputes Program Notification file";
			mailText = "Amex Optblue Seller Disputes Program Notification";
			break;
		case PRBIN:
			mailId = MesEmails.MSG_ADDRS_PRODUCT_GROUP; 
			mailSub = "Amex Bin range file is loaded successfully";
			mailText = "Amex Optblue Bin range";
			break;
		case EPTRN:
			mailId = MesEmails.MSG_ADDRS_PRODUCT_GROUP; 
			mailSub = "Amex TILR file is loaded successfully";
			mailText = "Amex Optblue TILR";
			break;
			
		}
	}
	
	
	protected void notifyTeam(String loadFilename) {
		try {
			MailMessage msg = new MailMessage();
			msg.setAddresses(mailId);
			msg.setSubject(mailSub);
			msg.setText(mailText);
			if(loadFilename != null)
				msg.addFile(loadFilename);
			msg.send();
		} catch (Exception e) {
			logEntry("notifyACRDept(" + loadFilename + ")", e.toString());
		}
		log.debug("mail has been sent");
	}

	public boolean execute() {

		try {
			connect(true);
			loadFiles(getEventArg(0));
		} catch (Exception e) {
			logEvent(this.getClass().getName(), "execute()", e.toString());
			logEntry("execute()", e.toString());
		}
		return true;
	}
	
	/**
	 * This method is used to convert two decimal places implied, right justified, zero-filled string to double value 
	 * @param strAmount
	 * @return
	 */
	public double getStringAmountToDouble(String strAmount){
		double doubleVal = 0.00;
		try{
			doubleVal = BigDecimal.valueOf(Long.valueOf(strAmount), 2).doubleValue();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doubleVal;
	}

	public static void main(String[] args) {
		AmexOptblueLoader t = null;

		try {
			
			if (args.length > 0 && args[0].equals("testproperties")) {
				EventBase.printKeyListStatus(new String[] { MesDefaults.DK_AMEX_HOST, MesDefaults.DK_AMEX_USER,
						MesDefaults.DK_AMEX_PASSWORD, MesDefaults.DK_AMEX_INCOMING_PATH, MesDefaults.DK_ACH_CUTOFF,
						MesDefaults.DK_AMEX_CLEARING_CUTOFF, MesDefaults.DK_DISCOVER_CLEARING_CUTOFF,
						MesDefaults.DK_MC_GCMS_CUTOFF, MesDefaults.DK_MODBII_CLEARING_CUTOFF,
						MesDefaults.DK_VISA_BASE_II_CUTOFF });
			}
			
			t = new AmexOptblueLoader();
			t.connect();
			if ("loadoptbspmfile".equals(args[0])) {
			    t.loadResponseFile(args[1], SPORG);
			} 
			else if ("loadsalesperformfile".equals(args[0])) {
				t.loadResponseFile(args[1], SALRP);
			}
			else if("loadbinfile".equals(args[0])){
				new ArdefLoader().loadArdefFile(args[1]);
    			t.setMailParams(PRBIN);
             	t.notifyTeam(null);
			}
			else if("loadscdfile".equals(args[0])){
				t.loadResponseFile(args[1], SCDNO);
			}
			else if("loadsdpfile".equals(args[0])){
				t.loadResponseFile(args[1], SDPNO);
			} else if("loadrecon".equals(args[0])){
				if(args.length>2 && "recon".equals(args[2])) {
					t.loadReconFileData(args[1],EPTRN);
				} else {
					if(args.length>2 && Boolean.valueOf(args[2])) {
						manualRun = true;
					}
					t.loadReconData(args[1],EPTRN);
				}
			}
			else if ("execute".equals(args[0])) {
				if("EPTRN".equals(args[1]) && args.length>2 && "recon".equals(args[2])){
					String[] reconArgs = new String[] {args[1],args[2]};
					t.setEventArgs(reconArgs);
				} else {
					t.setEventArgs(args[1]);
				}
				t.execute();
			}
		} catch (Exception e) {
		} finally {
			try {
				t.cleanUp();
			} catch (Exception e) {
			}
			try {
				OracleConnectionPool.getInstance().cleanUp();
			} catch (Exception e) {
			}
		}
		Runtime.getRuntime().exit(0);
	}
	
    private Date getPaymentDate(Date paymentDateFromSummRecord, FlatFileRecord ffd, String filename) {
        Date paymentDate = convertJulian(ffd, true);
        if (paymentDate == null) {
            if (paymentDateFromSummRecord != null) {
                paymentDate = paymentDateFromSummRecord;
            } else {
                log.error("Payment date from summary record is invalid [File Name]: " + filename);
            }
        }
        return paymentDate;
    }
	
    private void deleteDuplicateData(String filename) {
        String paymentNumber = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        BufferedReader in = null;
        try {
            in = new BufferedReader( new FileReader(filename) );
            String line = in.readLine();
            if((line = in.readLine()) != null && line.charAt(0)!='D') {
                paymentNumber = line.substring(34,42);
    		}
            ps = con.prepareStatement("delete from amex_settlement_recon_optblue where payment_number = ? " );
            ps.setString(1, paymentNumber);
            ps.executeUpdate();
    	}catch(Exception e){
            logEntry("deleteDuplicateData ()", e.toString());
    	}finally {
            try {
            	ps.close();
            	rs.close();
            	in.close();
            } catch(Exception e){
                logEntry("error occured during close connection", e.toString());
            }
    	}
    }
    
}
