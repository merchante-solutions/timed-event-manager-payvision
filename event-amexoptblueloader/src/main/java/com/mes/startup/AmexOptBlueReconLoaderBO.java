package com.mes.startup;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.persist.AmexReconClusterDAO;
import com.mes.persist.vo.AmexOptbReconFeeRecord;
import com.mes.persist.vo.AmexOptbReconFeeRecordCollection;
import com.mes.persist.vo.AmexOptbReconFileTransaction;
import com.mes.persist.vo.AmexOptbReconRocDtl;
import com.mes.persist.vo.AmexOptbReconRocDtlCollection;
import com.mes.persist.vo.AmexOptbReconRocLevel;
import com.mes.persist.vo.AmexOptbReconRocLevelCollection;
import com.mes.persist.vo.AmexOptbReconSummary;
import com.mes.persist.vo.AmexOptbReconSummaryDetail;
import com.mes.persist.vo.AmexOptbReconSummaryDetailCollection;
import com.mes.persist.vo.AmexOptbReconSummaryLevel;
import com.mes.persist.vo.AmexOptbReconSummaryLevelCollection;

public class AmexOptBlueReconLoaderBO {

	private	AmexReconClusterDAO reconClusterDAO = null;
	
	private static final String STR_DELETE = "DELETE";
	
	private static final String STR_INSERT = "INSERT";

	private static final String DELETE_BY_SUMMARY_ID_LIST = "deleteBySummaryIdList";
	
	private static final String RETURN_OBJECT = "returnObject";


	private static Logger log = Logger.getLogger(AmexOptBlueReconLoaderBO.class);

	public AmexOptBlueReconLoaderBO(Connection con) {
		reconClusterDAO = new AmexReconClusterDAO(con);
	}

	public boolean insertReconFileData(List<AmexOptbReconFileTransaction> optbReconFileTransactionList, int recordCount) {
		log.debug("AmexOptBlueReconLoaderBO::insertReconData in AmexOptbRecon cluster");
		AmexOptbReconSummary amexOptbReconSummary = null;
		long reconSummarySeqNum=0;
		try{
			for(AmexOptbReconFileTransaction reconFileTransaction : optbReconFileTransactionList){
				if(reconFileTransaction.getAmexOptbReconSummary()!=null){
					log.debug("Insert into AmexOptbReconSummary table");
					amexOptbReconSummary = reconFileTransaction.getAmexOptbReconSummary();
					amexOptbReconSummary.setRecordCount(recordCount);

					//insert into summary table
					Map returnMap = reconClusterDAO.crudOperation(amexOptbReconSummary,null,STR_INSERT);
					amexOptbReconSummary = (AmexOptbReconSummary) returnMap.get(RETURN_OBJECT);
					reconSummarySeqNum = amexOptbReconSummary.getSummaryId();
					log.debug("Inserting into AmexOptbReconSummary table with reconSummarySeqNum: "+reconSummarySeqNum);
				}

				if(reconFileTransaction.getAmexOptbReconSummaryDetailList()!=null){
					List<AmexOptbReconSummaryDetail> amexOptbReconSummaryDetailList = reconFileTransaction.getAmexOptbReconSummaryDetailList();
					AmexOptbReconSummaryLevel amexOptbReconSummaryLevel = null;
					Map returnMap = null;

					AmexOptbReconSummaryDetailCollection reconSummaryDetailCollection = new AmexOptbReconSummaryDetailCollection();
					AmexOptbReconSummaryLevelCollection reconSummaryLevelCollection = new AmexOptbReconSummaryLevelCollection();
					AmexOptbReconRocDtlCollection reconRocDtlCollection = new AmexOptbReconRocDtlCollection();
					AmexOptbReconRocLevelCollection reconRocLevelCollection = new AmexOptbReconRocLevelCollection();
					AmexOptbReconFeeRecordCollection reconFeeRecordCollection = new AmexOptbReconFeeRecordCollection();

					for(AmexOptbReconSummaryDetail amexOptbReconSummaryDetail :amexOptbReconSummaryDetailList){
						amexOptbReconSummaryDetail.setSummaryId(reconSummarySeqNum);
						reconSummaryDetailCollection.addReconSummaryDetailList(amexOptbReconSummaryDetail);
					}

					//insert into summary detail table
					if(reconSummaryDetailCollection!=null && !reconSummaryDetailCollection.isEmpty()){
						log.debug("Inserting into AmexOptbReconSummaryDetail table with reconSummarySeqNum :"+reconSummarySeqNum);
						returnMap = reconClusterDAO.crudOperation(reconSummaryDetailCollection,null,STR_INSERT);
						reconSummaryDetailCollection = (AmexOptbReconSummaryDetailCollection) returnMap.get(RETURN_OBJECT);
					}

					for(AmexOptbReconSummaryDetail amexOptbReconSummaryDetail :reconSummaryDetailCollection.getAmexOptbReconSummaryDetailList()){
						amexOptbReconSummaryLevel = amexOptbReconSummaryDetail.getReconSummaryLevel();
						if(amexOptbReconSummaryLevel!=null){
							amexOptbReconSummaryLevel.setSummaryDetailId(amexOptbReconSummaryDetail.getSummaryDetailId());
							reconSummaryLevelCollection.addReconSummaryLevelList(amexOptbReconSummaryLevel);
						}

						List<AmexOptbReconRocDtl> reconRocDtlList = amexOptbReconSummaryDetail.getAmexOptbReconRocDtlList();

						for(AmexOptbReconRocDtl reconRocDtl:reconRocDtlList){
							reconRocDtl.setSummaryDetailId(amexOptbReconSummaryDetail.getSummaryDetailId());
							reconRocDtlCollection.addReconRocDtlList(reconRocDtl);
						}

						List<AmexOptbReconFeeRecord> reconFeeRecordList =  amexOptbReconSummaryDetail.getAmexOptbReconFeeRecordList();
						for(AmexOptbReconFeeRecord reconFeeRecord:reconFeeRecordList){
							reconFeeRecord.setSummaryDetailId(amexOptbReconSummaryDetail.getSummaryDetailId());
							reconFeeRecordCollection.addReconFeeRecordList(reconFeeRecord);
						}
					}

					//insert into roc dtl table
					if(reconRocDtlCollection!=null && !reconRocDtlCollection.isEmpty()){
						log.debug("Inserting into AmexOptbReconRocDetail table");
						returnMap = reconClusterDAO.crudOperation(reconRocDtlCollection,null,STR_INSERT);
						reconRocDtlCollection = (AmexOptbReconRocDtlCollection) returnMap.get(RETURN_OBJECT);
					}

					List<AmexOptbReconRocLevel> amexOptbReconRocLevelList = null;
					for (AmexOptbReconRocDtl rocDtl:reconRocDtlCollection.getAmexOptbReconRocDtlList()){
						amexOptbReconRocLevelList = rocDtl.getAmexOptbReconRocLevelList();
						for(AmexOptbReconRocLevel rocLevel:amexOptbReconRocLevelList){
							rocLevel.setRocDetailId(rocDtl.getRocDetailId());
							reconRocLevelCollection.addReconRocDtlList(rocLevel);
						}
					}

					//insert into roc level table
					if(reconRocLevelCollection!=null && !reconRocLevelCollection.isEmpty()){
						log.debug("Inserting into AmexOptbReconRocLevel table");
						returnMap = reconClusterDAO.crudOperation(reconRocLevelCollection,null,STR_INSERT);
						reconRocLevelCollection = (AmexOptbReconRocLevelCollection) returnMap.get(RETURN_OBJECT);
					}

					//insert into summary level table
					if(reconSummaryLevelCollection!=null && !reconSummaryLevelCollection.isEmpty()){
						log.debug("Inserting into AmexOptbReconSummaryLevel table");
						returnMap = reconClusterDAO.crudOperation(reconSummaryLevelCollection,null,STR_INSERT);
						reconSummaryLevelCollection = (AmexOptbReconSummaryLevelCollection) returnMap.get(RETURN_OBJECT);
					}

					//insert into Fee Records table
					if(reconFeeRecordCollection!=null && !reconFeeRecordCollection.isEmpty()){
						log.debug("Inserting into AmexOptbReconFeeRecord table");
						returnMap = reconClusterDAO.crudOperation(reconFeeRecordCollection,null,STR_INSERT);
						reconFeeRecordCollection = (AmexOptbReconFeeRecordCollection) returnMap.get(RETURN_OBJECT);
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception occured  AmexOptBlueReconLoaderBO::insertReconData. Excepiton: "+e.getMessage());
			throw e;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public void deleteReconFileData(long loadFileId) {
		log.debug("AmexOptBlueReconLoaderBO::deleteReconData. LoadFileId: " + loadFileId);
		Map  returnMap= null;
		AmexOptbReconSummary reconSummary = null;
		AmexOptbReconSummaryDetail reconSummaryDtl = null;
		AmexOptbReconSummaryLevel reconSummaryLevel = null;
		AmexOptbReconRocDtl reconRocDtl = null;
		AmexOptbReconRocLevel rocLevel = null;
		AmexOptbReconFeeRecord feeRecord = null;

		List<AmexOptbReconSummary> reconSummaryList = null;
		List<Long> summaryIdList = null;

		try{
			summaryIdList = new ArrayList();
			reconSummary = new AmexOptbReconSummary();
			log.debug("fetch the summary id from AmexOptbReconSummary table for loadId: "+loadFileId);
			reconSummary.setFileId(loadFileId);
			returnMap = reconClusterDAO.crudOperation(reconSummary,"findByLoadFileId","SELECT");
			reconSummaryList = (List) returnMap.get(RETURN_OBJECT);

			for(AmexOptbReconSummary summary: reconSummaryList){
				summaryIdList.add(summary.getSummaryId());
			}

			reconSummaryLevel = new AmexOptbReconSummaryLevel();
			reconSummaryLevel.setSummaryIdList(summaryIdList);
			returnMap = reconClusterDAO.crudOperation(reconSummaryLevel,DELETE_BY_SUMMARY_ID_LIST,STR_DELETE);
			log.debug("deleted record in AmexOptbReconSummaryLevel table:"+returnMap.get(RETURN_OBJECT));

			rocLevel = new AmexOptbReconRocLevel();
			rocLevel.setSummaryIdList(summaryIdList);
			returnMap = reconClusterDAO.crudOperation(rocLevel,DELETE_BY_SUMMARY_ID_LIST,STR_DELETE);
			log.debug("deleted record in AmexOptbReconRocLevel table:"+returnMap.get(RETURN_OBJECT));

			reconRocDtl = new AmexOptbReconRocDtl();
			reconRocDtl.setSummaryIdList(summaryIdList);
			returnMap = reconClusterDAO.crudOperation(reconRocDtl,DELETE_BY_SUMMARY_ID_LIST,STR_DELETE);
			log.debug("deleted record in AmexOptbReconRocDtl table:"+returnMap.get(RETURN_OBJECT));

			feeRecord = new AmexOptbReconFeeRecord();
			feeRecord.setSummaryIdList(summaryIdList);
			returnMap = reconClusterDAO.crudOperation(feeRecord,DELETE_BY_SUMMARY_ID_LIST,STR_DELETE);
			log.debug("deleted record in AmexOptbReconFeeRecord table:"+returnMap.get(RETURN_OBJECT));

			reconSummaryDtl = new AmexOptbReconSummaryDetail();
			reconSummaryDtl.setSummaryIdList(summaryIdList);
			returnMap = reconClusterDAO.crudOperation(reconSummaryDtl,DELETE_BY_SUMMARY_ID_LIST,STR_DELETE);
			log.debug("deleted record in AmexOptbReconSummaryDetail table:"+returnMap.get(RETURN_OBJECT));

			reconSummary = new AmexOptbReconSummary();
			reconSummary.setSummaryIdList(summaryIdList);
			returnMap = reconClusterDAO.crudOperation(reconSummary,DELETE_BY_SUMMARY_ID_LIST,STR_DELETE);
			log.debug("deleted record in AmexOptbReconSummary table:"+returnMap.get(RETURN_OBJECT));

		} catch (Exception e) {
			log.error("Exception occured AmexOptBlueReconLoaderBO::deleteReconFileData.Exception Message: " +e.getMessage());
		}
	}


}
