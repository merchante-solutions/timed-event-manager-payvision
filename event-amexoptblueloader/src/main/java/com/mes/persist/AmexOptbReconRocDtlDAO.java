package com.mes.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.persist.vo.AmexOptbReconRocDtl;
import com.mes.persist.vo.AmexOptbReconRocDtlCollection;

public class AmexOptbReconRocDtlDAO extends AmexReconAbstractDAO{
	
	private static Logger log = Logger.getLogger(AmexOptbReconRocDtlDAO.class);
	
	private Connection mesConn;
	
	private static final int BATCH_COMMIT_SIZE = 1000;
	
	private static final String RETURN_OBJECT = "returnObject";

	private static final String SELECT_PK_SQL = "SELECT RECON_ROC_DETAIL_SEQ.NEXTVAL FROM DUAL";
    
	private static final String INSERT_SQL = "INSERT INTO AMEX_OPTB_RECON_ROC_DTL ( ROC_DETAIL_ID, SUMMARY_DETAIL_ID, CM_NUMBER, CM_NUMBER_ENC, CM_REF_NO,"
											+ " TRAN_DATE, SE_REF_POA, CAPN_NON_COMPLIANT_INDICATOR, CAPN_NON_COMPLIANT_ERROR, DQ_NON_COMPLIANT_IND, DQ_NON_COMPLAINT_ERROR, CM_NUMBER_19,"
											+ " NON_SWIPED_INDICATOR, CREATE_DATE, UPDATE_DATE, CREATED_BY) VALUES (?,?,?,dukpt_encrypt_wrapper(?),?,?,?,?,?,?,?,?,?,SYSDATE,NULL,NULL)";
	
	private static final String DELETE_SQL = "DELETE FROM AMEX_OPTB_RECON_ROC_DTL WHERE SUMMARY_DETAIL_ID IN "
											+ "(SELECT SUMMARY_DETAIL_ID FROM MES.AMEX_OPTB_RECON_SUMMARY_DETAIL "
											+ "WHERE SUMMARY_ID IN (?))";
	
	
	public AmexOptbReconRocDtlDAO (Connection conn){
		super(conn);
		this.mesConn = conn;
	}

	
	@Override
	public Object insert(Object insertObj) throws Exception {
		PreparedStatement statement = null;
		try {
			statement = mesConn.prepareStatement(INSERT_SQL);
			if(insertObj instanceof AmexOptbReconRocDtl){
				AmexOptbReconRocDtl rocDtlVO = (AmexOptbReconRocDtl) insertObj;
				int idx = 1;
				long reconRocDtlSeqNum = setPrimaryKeys(rocDtlVO);
				statement.setLong(idx++,reconRocDtlSeqNum);
				statement.setLong(idx++,rocDtlVO.getSummaryDetailId());
				statement.setString(idx++, rocDtlVO.getCmNumber());
				statement.setString(idx++, rocDtlVO.getCmNumberEnc());
				statement.setString(idx++, rocDtlVO.getCmRefNo());
				statement.setDate(idx++, rocDtlVO.getTranDate());
				statement.setString(idx++, rocDtlVO.getSeRefPoa());
				statement.setString(idx++, rocDtlVO.getCapnNonCompliantIndicator());
				statement.setString(idx++, rocDtlVO.getCapnNonCompliantError());
				statement.setString(idx++, rocDtlVO.getDqNonCompliantInd());
				statement.setString(idx++, rocDtlVO.getDqNonComplaintError());
				statement.setString(idx++, rocDtlVO.getCmNumber19());
				statement.setString(idx++, rocDtlVO.getNonSwipedInd());
				statement.executeUpdate();
				return rocDtlVO;
			} else {
				AmexOptbReconRocDtlCollection reconRocDtlCollection = (AmexOptbReconRocDtlCollection) insertObj;
				AmexOptbReconRocDtlCollection returnCollection = new AmexOptbReconRocDtlCollection(); 
				int count = 0;
				for(AmexOptbReconRocDtl rocDtlVO:reconRocDtlCollection.getAmexOptbReconRocDtlList()){
					int idx = 1;
					long reconRocDtlSeqNum = setPrimaryKeys(rocDtlVO);
					statement.setLong(idx++,reconRocDtlSeqNum);
					statement.setLong(idx++,rocDtlVO.getSummaryDetailId());
					statement.setString(idx++, rocDtlVO.getCmNumber());
					statement.setString(idx++, rocDtlVO.getCmNumberEnc());
					statement.setString(idx++, rocDtlVO.getCmRefNo());
					statement.setDate(idx++, rocDtlVO.getTranDate());
					statement.setString(idx++, rocDtlVO.getSeRefPoa());
					statement.setString(idx++, rocDtlVO.getCapnNonCompliantIndicator());
					statement.setString(idx++, rocDtlVO.getCapnNonCompliantError());
					statement.setString(idx++, rocDtlVO.getDqNonCompliantInd());
					statement.setString(idx++, rocDtlVO.getDqNonComplaintError());
					statement.setString(idx++, rocDtlVO.getCmNumber19());
					statement.setString(idx++, rocDtlVO.getNonSwipedInd());
					statement.addBatch();
					if(++count % BATCH_COMMIT_SIZE == 0) {
						statement.executeBatch();
					}
					returnCollection.addReconRocDtlList(rocDtlVO);
				}
				statement.executeBatch();
				return returnCollection;
			}
		} catch (SQLException e) {
			log.error("Exception occuried at AmexOptbReconRocDtlDAO::insert(). Exception message: " + e.getMessage() );
			throw e;
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconRocDtlDAO::insert() try/catch block. Exception message: " + e.getMessage() );
			}
		}
	}
	
	private long setPrimaryKeys(AmexOptbReconRocDtl reconRocDtlVO){
		PreparedStatement statement = null;
		long seqNum = 0;
		ResultSet rs = null;
		try {
			statement = mesConn.prepareStatement(SELECT_PK_SQL);
			rs = statement.executeQuery();
			rs.next();
			seqNum = rs.getLong(1);
			reconRocDtlVO.setRocDetailId(seqNum);
		}
		catch (SQLException e) {
			log.error("Exception occuried at AmexOptbReconRocDtlDAO::setPrimaryKeys(). Exception message: " + e.getMessage() );
		}
		finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconRocDtlDAO::setPrimaryKeys() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return seqNum;
	}


	@Override
	public void delete(Object deleteObj) {
		//implement this method in future
	}

	@Override
	public void update(Object updateObj) {
		//implement this method in future
	}
	
	public Map findBySummaryDetailId(AmexOptbReconRocDtl reconRocDtl) {
		PreparedStatement statement = null;
		ResultSet rs = null;
		List<AmexOptbReconRocDtl> reconRocDtlList = new ArrayList<>();
		Map returnedObj = new HashMap<>();
		try {
			statement = mesConn.prepareStatement("SELECT ROC_DETAIL_ID FROM AMEX_OPTB_RECON_ROC_DTL WHERE SUMMARY_DETAIL_ID = ?");
			statement.setLong(1,reconRocDtl.getSummaryDetailId());
			rs = statement.executeQuery();

			while(rs.next()){
				AmexOptbReconRocDtl returnVO= new AmexOptbReconRocDtl();
				returnVO.setRocDetailId(rs.getLong("ROC_DETAIL_ID"));
				reconRocDtlList.add(returnVO);
			}
			returnedObj.put(RETURN_OBJECT, reconRocDtlList);
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconRocDtlDAO::findBySummaryDetailId(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconRocDtlDAO::findBySummaryDetailId() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}
	
	public Map deleteBySummaryDetailId(AmexOptbReconRocDtl reconRocDtl) {
		PreparedStatement statement = null;
		int rowCount = 0;
		Map returnedObj = null;
		try {
			statement = mesConn.prepareStatement("DELETE FROM AMEX_OPTB_RECON_ROC_DTL WHERE SUMMARY_DETAIL_ID = ?");
			statement.setLong(1,reconRocDtl.getSummaryDetailId());
			rowCount = statement.executeUpdate();
			if(rowCount>0){
				returnedObj = new HashMap<>();
				returnedObj.put(RETURN_OBJECT,true);
 			} else {
 				returnedObj = new HashMap<>();
				returnedObj.put(RETURN_OBJECT,false);
 			}
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconRocDtlDAO::deleteBySummaryDetailId(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconRocDtlDAO::deleteBySummaryDetailId() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}
	
	public Map deleteBySummaryIdList(AmexOptbReconRocDtl reconRocDtl) {
		PreparedStatement statement = null;
		int[] rowCount = null;
		Map returnedObj = null;
		try {
			returnedObj = new HashMap<>();
			if(reconRocDtl.getSummaryIdList()!=null && !reconRocDtl.getSummaryIdList().isEmpty()){
				statement = mesConn.prepareStatement(DELETE_SQL);

				for (long summaryId:reconRocDtl.getSummaryIdList()){
					statement.setLong(1,summaryId);
					statement.addBatch();
				}
				rowCount = statement.executeBatch();

				if(rowCount!=null && rowCount.length>0){
					returnedObj.put(RETURN_OBJECT,true);
	 			} else {
					returnedObj.put(RETURN_OBJECT,false);
	 			}
			} else {
				returnedObj.put(RETURN_OBJECT,false);
			}
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconRocDtlDAO::deleteBySummaryIdList(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconRocDtlDAO::deleteBySummaryIdList() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}

}
