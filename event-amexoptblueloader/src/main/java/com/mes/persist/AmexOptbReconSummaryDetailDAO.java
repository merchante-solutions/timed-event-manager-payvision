package com.mes.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.persist.vo.AmexOptbReconSummaryDetail;
import com.mes.persist.vo.AmexOptbReconSummaryDetailCollection;

public class AmexOptbReconSummaryDetailDAO extends AmexReconAbstractDAO {

	private static Logger log = Logger.getLogger(AmexOptbReconSummaryDetailDAO.class);

	private Connection mesConn;
	
	private static final int BATCH_COMMIT_SIZE = 1000;
	
	private static final String RETURN_OBJECT = "returnObject";

	private static final String SELECT_PK_SQL = "SELECT RECON_SUM_DETAIL_SEQ.NEXTVAL FROM DUAL";
	
	private static final String INSERT_SQL = "INSERT INTO AMEX_OPTB_RECON_SUMMARY_DETAIL( SUMMARY_DETAIL_ID ,SUMMARY_ID ,SE_NUMBER ,SE_BUSINESS_DATE ,"
			+ "PROCESS_DATE ,SOC_INVOICE_NUMBER ,SOC_AMOUNT ,DISCOUNT_AMOUNT ,SERVICE_FEE_AMOUNT ,NET_SOC_AMOUNT ,GROSS_AMOUNT ,TRACKING_ID ,CPC_INDICATOR"
			+ " ,BASE_DISCOUNT_AMOUNT ,CARD_NOT_PRESENT_BPA_AMOUNT ,CARD_NOT_PRESENT_PTA_AMOUNT ,TRANSACTION_FEE_AMOUNT ,TRANSACTION_FEE_RATE ,"
			+ "CARD_NOT_PRESENT_BPA_RATE ,CARD_NOT_PRESENT_PTA_RATE ,DISCOUNT_RATE ,SERVICE_FEE_RATE ,CREATE_DATE ,UPDATE_DATE ,CREATED_BY) "
			+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE,NULL,NULL)";
	
	private static final String DELETE_SQL = "DELETE FROM AMEX_OPTB_RECON_SUMMARY_DETAIL WHERE SUMMARY_ID IN (?)";


	public AmexOptbReconSummaryDetailDAO(Connection conn) {
		super(conn);
		this.mesConn = conn;
	}

	@Override
	public Object insert(Object insertObj) throws Exception{
		PreparedStatement statement = null;
		try {
			statement = mesConn.prepareStatement(INSERT_SQL);
			if(insertObj instanceof AmexOptbReconSummaryDetail){
				AmexOptbReconSummaryDetail summaryDtlVO = (AmexOptbReconSummaryDetail) insertObj;
				int idx = 1;
				long reconSummaryDtlSeqNum = setPrimaryKeys(summaryDtlVO);
				statement.setLong(idx++,reconSummaryDtlSeqNum);
				statement.setLong(idx++, summaryDtlVO.getSummaryId());
				statement.setLong(idx++, summaryDtlVO.getSeNumber());
				statement.setDate(idx++, summaryDtlVO.getSeBusinessDate());
				statement.setDate(idx++, summaryDtlVO.getProcessDate());
				statement.setLong(idx++, summaryDtlVO.getSocInvoiceNumber());
				statement.setDouble(idx++, summaryDtlVO.getSocAmount());
				statement.setDouble(idx++, summaryDtlVO.getDiscountAmount());
				statement.setDouble(idx++, summaryDtlVO.getServiceFeeAmount());
				statement.setDouble(idx++, summaryDtlVO.getNetSocAmount());
				statement.setDouble(idx++, summaryDtlVO.getGrossAmount());
				statement.setLong(idx++, summaryDtlVO.getTrackingId());
				statement.setString(idx++, summaryDtlVO.getCpcIndicator());
				statement.setDouble(idx++, summaryDtlVO.getBaseDiscountAmount());
				statement.setDouble(idx++, summaryDtlVO.getCardNotPresentBpaAmount());
				statement.setDouble(idx++, summaryDtlVO.getCardNotPresentPtaAmount());
				statement.setDouble(idx++, summaryDtlVO.getTransactionFeeAmount());
				statement.setDouble(idx++, summaryDtlVO.getTransactionFeeRate());
				statement.setDouble(idx++, summaryDtlVO.getCardNotPresentBpaRate());
				statement.setDouble(idx++, summaryDtlVO.getCardNotPresentPtaRate());
				statement.setDouble(idx++, summaryDtlVO.getDiscountRate());
				statement.setDouble(idx++, summaryDtlVO.getServiceFeeRate());
				statement.executeUpdate();
				return summaryDtlVO;
			} else {
				AmexOptbReconSummaryDetailCollection reconSummaryDetailCollection = (AmexOptbReconSummaryDetailCollection) insertObj;
				AmexOptbReconSummaryDetailCollection returnCollection = new AmexOptbReconSummaryDetailCollection(); 
				int count = 0;
				for(AmexOptbReconSummaryDetail summaryDtlVO:reconSummaryDetailCollection.getAmexOptbReconSummaryDetailList()){
					int idx = 1;
					long reconSummaryDtlSeqNum = setPrimaryKeys(summaryDtlVO);
					statement.setLong(idx++,reconSummaryDtlSeqNum);
					statement.setLong(idx++, summaryDtlVO.getSummaryId());
					statement.setLong(idx++, summaryDtlVO.getSeNumber());
					statement.setDate(idx++, summaryDtlVO.getSeBusinessDate());
					statement.setDate(idx++, summaryDtlVO.getProcessDate());
					statement.setLong(idx++, summaryDtlVO.getSocInvoiceNumber());
					statement.setDouble(idx++, summaryDtlVO.getSocAmount());
					statement.setDouble(idx++, summaryDtlVO.getDiscountAmount());
					statement.setDouble(idx++, summaryDtlVO.getServiceFeeAmount());
					statement.setDouble(idx++, summaryDtlVO.getNetSocAmount());
					statement.setDouble(idx++, summaryDtlVO.getGrossAmount());
					statement.setLong(idx++, summaryDtlVO.getTrackingId());
					statement.setString(idx++, summaryDtlVO.getCpcIndicator());
					statement.setDouble(idx++, summaryDtlVO.getBaseDiscountAmount());
					statement.setDouble(idx++, summaryDtlVO.getCardNotPresentBpaAmount());
					statement.setDouble(idx++, summaryDtlVO.getCardNotPresentPtaAmount());
					statement.setDouble(idx++, summaryDtlVO.getTransactionFeeAmount());
					statement.setDouble(idx++, summaryDtlVO.getTransactionFeeRate());
					statement.setDouble(idx++, summaryDtlVO.getCardNotPresentBpaRate());
					statement.setDouble(idx++, summaryDtlVO.getCardNotPresentPtaRate());
					statement.setDouble(idx++, summaryDtlVO.getDiscountRate());
					statement.setDouble(idx++, summaryDtlVO.getServiceFeeRate());
					statement.addBatch();
					if(++count % BATCH_COMMIT_SIZE == 0) {
						statement.executeBatch();
					}
					returnCollection.addReconSummaryDetailList(summaryDtlVO);
				}
				statement.executeBatch();
				return returnCollection;
			}
		} catch (SQLException e) {
			log.error("Exception occuried at AmexOptbReconSummaryDetailDAO::insert(). Exception message: " + e.getMessage() );
			throw e;
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconSummaryDetailDAO::insert() try/catch block. Exception message: " + e.getMessage() );
			}
		}
	}

	private long setPrimaryKeys(AmexOptbReconSummaryDetail reconSummaryDtlVO){
		PreparedStatement statement = null;
		long seqNum = 0;
		ResultSet rs = null;
		try {
			statement = mesConn.prepareStatement(SELECT_PK_SQL);
			rs = statement.executeQuery();
			rs.next();
			seqNum = rs.getLong(1);
			reconSummaryDtlVO.setSummaryDetailId(seqNum);
		}
		catch (SQLException e) {
			log.error("Exception occuried at AmexOptbReconSummaryDetailDAO::setPrimaryKeys(). Exception message: " + e.getMessage() );
		}
		finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconSummaryDetailDAO::setPrimaryKeys() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return seqNum;
	}

	@Override
	public void delete(Object deleteObj) {
		//implement this method in future
	}

	@Override
	public void update(Object updateObj) {
		//implement this method in future
	}
	public Map findBySummaryId(AmexOptbReconSummaryDetail reconSummaryDtl) {
		PreparedStatement statement = null;
		ResultSet rs = null;
		List<AmexOptbReconSummaryDetail> reconSummaryDtlList = new ArrayList<>();
		Map returnedObj = new HashMap<>();
		try {
			statement = mesConn.prepareStatement("SELECT SUMMARY_DETAIL_ID FROM AMEX_OPTB_RECON_SUMMARY_DETAIL WHERE SUMMARY_ID = ?");
			statement.setLong(1,reconSummaryDtl.getSummaryId());
			rs = statement.executeQuery();
			while(rs.next()){
				AmexOptbReconSummaryDetail returnVO= new AmexOptbReconSummaryDetail();
				returnVO.setSummaryDetailId(rs.getLong("SUMMARY_DETAIL_ID"));
				reconSummaryDtlList.add(returnVO);
			}
			returnedObj.put(RETURN_OBJECT, reconSummaryDtlList);
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconSummaryDetailDAO::findBySummaryId(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconSummaryDetailDAO::findBySummaryId() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}

	public Map deleteBySummaryId(AmexOptbReconSummaryDetail reconSummaryDetail) {
		PreparedStatement statement = null;
		int rowCount = 0;
		Map returnedObj = null;
		try {
			statement = mesConn.prepareStatement("DELETE FROM AMEX_OPTB_RECON_SUMMARY_DETAIL WHERE SUMMARY_ID = ?");
			statement.setLong(1,reconSummaryDetail.getSummaryId());
			rowCount = statement.executeUpdate();
			if(rowCount>0){
				returnedObj = new HashMap<>();
				returnedObj.put(RETURN_OBJECT,true);
			} else {
				returnedObj = new HashMap<>();
				returnedObj.put(RETURN_OBJECT,false);
			}
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconSummaryDetailDAO::deleteBySummaryId(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconSummaryDetailDAO::deleteBySummaryId() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}

	public Map deleteBySummaryIdList(AmexOptbReconSummaryDetail reconSummaryDetail) {
		PreparedStatement statement = null;
		int[] rowCount = null;
		Map returnedObj = null;
		try {
			returnedObj = new HashMap<>();
			if(reconSummaryDetail.getSummaryIdList()!=null && !reconSummaryDetail.getSummaryIdList().isEmpty()){
				statement = mesConn.prepareStatement(DELETE_SQL);
				for (long summaryId:reconSummaryDetail.getSummaryIdList()){
					statement.setLong(1,summaryId);
					statement.addBatch();
				}
				rowCount = statement.executeBatch();
				if(rowCount!=null && rowCount.length>0){
					returnedObj.put(RETURN_OBJECT,true);
	 			} else {
					returnedObj.put(RETURN_OBJECT,false);
	 			}
			} else {
				returnedObj.put(RETURN_OBJECT,false);
			}
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconSummaryDetailDAO::deleteBySummaryIdList(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconSummaryDetailDAO::deleteBySummaryIdList() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}

}
