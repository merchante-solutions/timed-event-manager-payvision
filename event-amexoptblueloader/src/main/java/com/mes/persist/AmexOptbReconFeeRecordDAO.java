package com.mes.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.persist.vo.AmexOptbReconFeeRecord;
import com.mes.persist.vo.AmexOptbReconFeeRecordCollection;

public class AmexOptbReconFeeRecordDAO extends AmexReconAbstractDAO{
	
	private static Logger log = Logger.getLogger(AmexOptbReconFeeRecordDAO.class);
	
	private Connection mesConn;
	
	private static final int BATCH_COMMIT_SIZE = 1000;
	
	private static final String RETURN_OBJECT = "returnObject";

	private static final String SELECT_PK_SQL = "SELECT RECON_FEE_SEQ.NEXTVAL FROM DUAL";

	private static final String INSERT_SQL = "INSERT INTO AMEX_OPTB_RECON_FEE_RECORDS ( FEE_ID , SUMMARY_DETAIL_ID , SE_NUMBER , REC_TYPE , DETAIL_REC_TYPE ,"
			+ " ADJUSTMENT_NUMBER , CHARGEBACK_AMOUNT , NET_CHARGEBACK_AMOUNT , CHARGEBACK_REASON , ASSET_BILLING_TAX , BATCH_CODE , BILL_CODE ,"
			+ " MERCHANT_NUMBER , ADJUSTMENT_AMOUNT , NET_ADJUSTMENT_AMOUNT , PAY_IN_GROSS_INDICATOR , CM_NUMBER , ADJUSTMENT_REASON , ASSET_BILLING_AMOUNT"
			+ " , ASSET_BILLING_DESCRIPTION , TAKE_ONE_COMMISSION_AMOUNT , TAKE_ONE_DESCRIPTION , OTHER_FEE_AMOUNT , OTHER_FEE_DESCRIPTION , CREATE_DATE ,"
			+ " UPDATE_DATE , CREATED_BY, LOAD_SEC )  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE,NULL,NULL,?)";
	
	private static final String DELETE_SQL = "DELETE FROM AMEX_OPTB_RECON_FEE_RECORDS WHERE SUMMARY_DETAIL_ID IN "
											+ "(SELECT SUMMARY_DETAIL_ID FROM MES.AMEX_OPTB_RECON_SUMMARY_DETAIL "
											+ "WHERE SUMMARY_ID IN (?))";
	
	public AmexOptbReconFeeRecordDAO (Connection conn){
		super(conn);
		this.mesConn = conn;
	}

	@Override
	public Object insert(Object insertObj) throws Exception {
		PreparedStatement statement = null;
		try {
			statement = mesConn.prepareStatement(INSERT_SQL);
			if(insertObj instanceof AmexOptbReconFeeRecord){
				AmexOptbReconFeeRecord reconFeeRecordVO = (AmexOptbReconFeeRecord) insertObj;
				int idx = 1;
				long reconFeeRecordSeqNum = setPrimaryKeys(reconFeeRecordVO);
				statement.setLong(idx++,reconFeeRecordSeqNum);
				statement.setLong(idx++,reconFeeRecordVO.getSummaryDetailId());
				statement.setLong(idx++,reconFeeRecordVO.getSeNumber());
				statement.setString(idx++, reconFeeRecordVO.getRecType());
				statement.setString(idx++, reconFeeRecordVO.getDetailRecType());
				statement.setLong(idx++,reconFeeRecordVO.getAdjustmentNumber());
				statement.setDouble(idx++,reconFeeRecordVO.getChargebackAmount());
				statement.setDouble(idx++,reconFeeRecordVO.getNetChargebackAmount());
				statement.setString(idx++,reconFeeRecordVO.getChargebackReason());
				statement.setString(idx++,reconFeeRecordVO.getAssetBillingTax());
				statement.setString(idx++,reconFeeRecordVO.getBatchCode());
				statement.setString(idx++,reconFeeRecordVO.getBillCode());
				statement.setLong(idx++,reconFeeRecordVO.getMerchantNumber());
				statement.setDouble(idx++,reconFeeRecordVO.getAdjustmentAmount());
				statement.setDouble(idx++,reconFeeRecordVO.getNetAdjustmentAmount());
				statement.setString(idx++,reconFeeRecordVO.getPayInGrossIndicator());
				statement.setString(idx++,reconFeeRecordVO.getCmNumber());
				statement.setString(idx++,reconFeeRecordVO.getAdjustmentReason());
				statement.setDouble(idx++,reconFeeRecordVO.getAssetBillingAmount());
				statement.setString(idx++,reconFeeRecordVO.getAssetBillingDescription());
				statement.setDouble(idx++,reconFeeRecordVO.getTakeOneCommissionAmount());
				statement.setString(idx++,reconFeeRecordVO.getTakeOneDescription());
				statement.setDouble(idx++,reconFeeRecordVO.getOtherFeeAmount());
				statement.setString(idx++,reconFeeRecordVO.getOtherFeeDescription());
				statement.setLong(idx++, reconFeeRecordVO.getLoadSec());
				statement.executeUpdate();
				return reconFeeRecordVO;
			} else {
				AmexOptbReconFeeRecordCollection reconFeeRecordCollection = (AmexOptbReconFeeRecordCollection) insertObj;
				AmexOptbReconFeeRecordCollection returnCollection = new AmexOptbReconFeeRecordCollection(); 
				int count =0;
				for(AmexOptbReconFeeRecord reconFeeRecordVO:reconFeeRecordCollection.getAmexOptbReconFeeRecordList()){
					int idx = 1;
					long reconFeeRecordSeqNum = setPrimaryKeys(reconFeeRecordVO);
					statement.setLong(idx++,reconFeeRecordSeqNum);
					statement.setLong(idx++,reconFeeRecordVO.getSummaryDetailId());
					statement.setLong(idx++,reconFeeRecordVO.getSeNumber());
					statement.setString(idx++, reconFeeRecordVO.getRecType());
					statement.setString(idx++, reconFeeRecordVO.getDetailRecType());
					statement.setLong(idx++,reconFeeRecordVO.getAdjustmentNumber());
					statement.setDouble(idx++,reconFeeRecordVO.getChargebackAmount());
					statement.setDouble(idx++,reconFeeRecordVO.getNetChargebackAmount());
					statement.setString(idx++,reconFeeRecordVO.getChargebackReason());
					statement.setString(idx++,reconFeeRecordVO.getAssetBillingTax());
					statement.setString(idx++,reconFeeRecordVO.getBatchCode());
					statement.setString(idx++,reconFeeRecordVO.getBillCode());
					statement.setLong(idx++,reconFeeRecordVO.getMerchantNumber());
					statement.setDouble(idx++,reconFeeRecordVO.getAdjustmentAmount());
					statement.setDouble(idx++,reconFeeRecordVO.getNetAdjustmentAmount());
					statement.setString(idx++,reconFeeRecordVO.getPayInGrossIndicator());
					statement.setString(idx++,reconFeeRecordVO.getCmNumber());
					statement.setString(idx++,reconFeeRecordVO.getAdjustmentReason());
					statement.setDouble(idx++,reconFeeRecordVO.getAssetBillingAmount());
					statement.setString(idx++,reconFeeRecordVO.getAssetBillingDescription());
					statement.setDouble(idx++,reconFeeRecordVO.getTakeOneCommissionAmount());
					statement.setString(idx++,reconFeeRecordVO.getTakeOneDescription());
					statement.setDouble(idx++,reconFeeRecordVO.getOtherFeeAmount());
					statement.setString(idx++,reconFeeRecordVO.getOtherFeeDescription());
					statement.setLong(idx++, reconFeeRecordVO.getLoadSec());
					statement.addBatch();
					if(++count % BATCH_COMMIT_SIZE == 0) {
						statement.executeBatch();
					}
					returnCollection.addReconFeeRecordList(reconFeeRecordVO);
				}
				statement.executeBatch();
				return returnCollection;
			}
		} catch (SQLException e) {
			log.error("Exception occuried at AmexOptbReconFeeRecordDAO::insert(). Exception message: " + e.getMessage() );
			throw e;
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconFeeRecordDAO::insert() try/catch block. Exception message: " + e.getMessage() );
			}
		}
	}
	
	private long setPrimaryKeys(AmexOptbReconFeeRecord reconFeeRecordVO){
		PreparedStatement statement = null;
		long seqNum = 0;
		ResultSet rs = null;
		try {
				statement = mesConn.prepareStatement(SELECT_PK_SQL);
				rs = statement.executeQuery();
				rs.next();
				seqNum = rs.getLong(1);
				reconFeeRecordVO.setFeeId(seqNum);
		}
		catch (SQLException e) {
			log.error("Exception occuried at AmexOptbReconFeeRecordDAO::setPrimaryKeys(). Exception message: " + e.getMessage() );
		}
		finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconFeeRecordDAO::setPrimaryKeys() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return seqNum;
	}


	@Override
	public void delete(Object deleteObj) {
		//implement this method in future
	}

	@Override
	public void update(Object updateObj) {
		//implement this method in future
	}
	
	public Map deleteBySummaryId(AmexOptbReconFeeRecord feeRecord) {
		PreparedStatement statement = null;
		int rowCount = 0;
		Map returnedObj = null;
		try {
			statement = mesConn.prepareStatement("DELETE FROM AMEX_OPTB_RECON_FEE_RECORDS WHERE SUMMARY_ID = ?");
			statement.setLong(1,feeRecord.getSummaryId());
			rowCount = statement.executeUpdate();
			if(rowCount>0){
				returnedObj = new HashMap<>();
				returnedObj.put(RETURN_OBJECT,true);
			} else {
				returnedObj = new HashMap<>();
				returnedObj.put(RETURN_OBJECT,false);
			}
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconFeeRecordDAO::deleteBySummaryId(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconFeeRecordDAO::deleteBySummaryId() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}
	
	public Map deleteBySummaryIdList(AmexOptbReconFeeRecord feeRecord) {
		PreparedStatement statement = null;
		int[] rowCount = null;
		Map returnedObj = null;
		try {
			returnedObj = new HashMap<>();
			if(feeRecord.getSummaryIdList()!=null && !feeRecord.getSummaryIdList().isEmpty()){
				statement = mesConn.prepareStatement(DELETE_SQL);
				for (long summaryId:feeRecord.getSummaryIdList()){
					statement.setLong(1,summaryId);
					statement.addBatch();
				}
				rowCount = statement.executeBatch();

				if(rowCount!=null && rowCount.length>0){
					returnedObj.put(RETURN_OBJECT,true);
	 			} else {
					returnedObj.put(RETURN_OBJECT,false);
	 			}
			} else {
				returnedObj.put(RETURN_OBJECT,false);
			}
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconFeeRecordDAO::deleteBySummaryIdList(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconFeeRecordDAO::deleteBySummaryIdList() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}

	
	
}
