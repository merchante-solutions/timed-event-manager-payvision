package com.mes.persist;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.Map;


public abstract class AmexReconAbstractDAO implements IAmexReconCluster {
	
	/**
	 * Field cls
	 */
	private Class cls = null;
	/**
	 * Field noparams
	 */
	private Class noparams[] = {};
	/**
	 * Field connectionClass
	 */
	private Class[] connectionClass = { Connection.class };
	/**
	 * Field paramObj
	 */
	private Class[] paramObj = new Class[1];
	/**
	 * Field mcpConn
	 */
	private Connection mesConn = null;
	/**
	 * Field DAO_PACKAGE
	 */
	private static final String DAO_PACKAGE = "com.mes.persist.";
	/**
	 * Field DAO_PACKAGE
	 */
	private static final String DAO_SUFFIX = "DAO";
	/**
	 * Default Constructor
	 */
	public AmexReconAbstractDAO(){
		
	}
	/**
	 * Constructor with connection arg
	 * @param conn
	 */
	public AmexReconAbstractDAO(Connection conn){
		this.mesConn = conn;
	}

	@Override
	public abstract Object insert(Object insertObj) throws Exception;
	
	@Override
	public abstract void delete(Object deleteObj);

	@Override
	public abstract void update(Object updateObj);

	/* 
	 * This method with delegate to particular DAO and particular finder method is invoked
	 * 
	 */
	@Override
	public Map findBy(Object findByObj, String findBy) {
		Object returnObj = null;
		Map returnMap = null;
		Class cargoClass = null;
		String persister = null;
		try {
			cargoClass = findByObj.getClass();
			persister = cargoClass.getSimpleName();
			String daoName = persister.concat(DAO_SUFFIX); 
			try {
				cls = Class.forName(DAO_PACKAGE+daoName);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			paramObj[0] = cargoClass;
			Constructor cons = cls.getConstructor(connectionClass);
			Object[] conArg = { mesConn };
			Method method = cls.getMethod(findBy, paramObj);
			returnObj = method.invoke(cons.newInstance(conArg),findByObj);
			returnMap = (Map)returnObj;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return returnMap;
	}

}