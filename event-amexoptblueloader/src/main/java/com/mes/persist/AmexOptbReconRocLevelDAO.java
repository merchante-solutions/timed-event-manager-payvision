package com.mes.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.persist.vo.AmexOptbReconRocLevel;
import com.mes.persist.vo.AmexOptbReconRocLevelCollection;

public class AmexOptbReconRocLevelDAO extends AmexReconAbstractDAO{
	
	private static Logger log = Logger.getLogger(AmexOptbReconRocLevelDAO.class);
	
	private Connection mesConn;
	
	private static final int BATCH_COMMIT_SIZE = 1000;
	
	private static final String RETURN_OBJECT = "returnObject";

	private static final String SELECT_PK_SQL = "SELECT RECON_ROC_LEVEL_SEQ.NEXTVAL FROM DUAL";
	
	private static final String INSERT_SQL = "INSERT INTO AMEX_OPTB_RECON_ROC_LEVEL (ROC_LEVEL_ID, ROC_DETAIL_ID, FEE_CODE, FEE_DESC, DISCOUNT_AMOUNT, FEE_AMOUNT, CREATE_DATE, UPDATE_DATE, CREATED_BY, FEE_RATE, DISCOUNT_RATE ) VALUES (?, ?, ?, ?, ?, ?, SYSDATE, NULL, NULL, ?, ? )";
	
	private static final String DELETE_SQL = "DELETE FROM AMEX_OPTB_RECON_ROC_LEVEL WHERE ROC_DETAIL_ID IN "
													+ "(SELECT ROC_DETAIL_ID FROM AMEX_OPTB_RECON_ROC_DTL WHERE SUMMARY_DETAIL_ID IN "
													+ "(SELECT SUMMARY_DETAIL_ID FROM AMEX_OPTB_RECON_SUMMARY_DETAIL "
													+ "WHERE SUMMARY_ID IN (?)))";
	
	public AmexOptbReconRocLevelDAO(Connection conn){
		super(conn);
		this.mesConn = conn;
	}

	@Override
	public Object insert(Object insertObj)  throws Exception{
		PreparedStatement statement = null;
		try {
			statement = mesConn.prepareStatement(INSERT_SQL);
			if(insertObj instanceof AmexOptbReconRocLevel){
				AmexOptbReconRocLevel rocLevelVO = (AmexOptbReconRocLevel) insertObj;
				int idx = 1;
				long reconRocLevelSeqNum = setPrimaryKeys(rocLevelVO);
				statement.setLong(idx++,reconRocLevelSeqNum);
				statement.setLong(idx++,rocLevelVO.getRocDetailId());
				statement.setString(idx++, rocLevelVO.getFeeCode());
				statement.setString(idx++, rocLevelVO.getFeeDesc());
				statement.setDouble(idx++, rocLevelVO.getDiscountAmount());
				statement.setDouble(idx++, rocLevelVO.getFeeAmount());
				statement.setDouble(idx++, rocLevelVO.getFeeRate());
				statement.setDouble(idx++, rocLevelVO.getDiscountRate());
				statement.executeUpdate();
				return rocLevelVO;
			} else {
				AmexOptbReconRocLevelCollection reconRocLevelCollection = (AmexOptbReconRocLevelCollection) insertObj;
				AmexOptbReconRocLevelCollection returnCollection = new AmexOptbReconRocLevelCollection(); 
				int count=0;
				for(AmexOptbReconRocLevel rocLevelVO:reconRocLevelCollection.getAmexOptbReconRocDtlList()){
					int idx = 1;
					long reconRocLevelSeqNum = setPrimaryKeys(rocLevelVO);
					statement.setLong(idx++,reconRocLevelSeqNum);
					statement.setLong(idx++,rocLevelVO.getRocDetailId());
					statement.setString(idx++, rocLevelVO.getFeeCode());
					statement.setString(idx++, rocLevelVO.getFeeDesc());
					statement.setDouble(idx++, rocLevelVO.getDiscountAmount());
					statement.setDouble(idx++, rocLevelVO.getFeeAmount());
					statement.setDouble(idx++, rocLevelVO.getFeeRate());
					statement.setDouble(idx++, rocLevelVO.getDiscountRate());
					statement.addBatch();
					if(++count % BATCH_COMMIT_SIZE == 0) {
						statement.executeBatch();
					}
					returnCollection.addReconRocDtlList(rocLevelVO);
				}
				statement.executeBatch();
				return returnCollection;
			}
		} catch (SQLException e) {
			log.error("Exception occuried at AmexOptbReconRocLevelDAO::insert(). Exception message: " + e.getMessage() );
			throw e;
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconRocLevelDAO::insert() try/catch block. Exception message: " + e.getMessage() );
			}
		}
	}

	private long setPrimaryKeys(AmexOptbReconRocLevel reconRocLevelVO){
		PreparedStatement statement = null;
		long seqNum = 0;
		ResultSet rs = null;
		try {
				statement = mesConn.prepareStatement(SELECT_PK_SQL);
				rs = statement.executeQuery();
				rs.next();
				seqNum = rs.getLong(1);
				reconRocLevelVO.setRocLevelId(seqNum);
		}
		catch (SQLException e) {
			log.error("Exception occuried at AmexOptbReconRocLevelDAO::setPrimaryKeys(). Exception message: " + e.getMessage() );
		}
		finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconRocLevelDAO::setPrimaryKeys() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return seqNum;
	}
	
	public Map deleteByRocDetailId(AmexOptbReconRocLevel rocDtl) {
		PreparedStatement statement = null;
		int rowCount = 0;
		Map returnedObj = null;
		try {
			statement = mesConn.prepareStatement("DELETE FROM AMEX_OPTB_RECON_ROC_LEVEL WHERE ROC_DETAIL_ID = ?");
			statement.setLong(1,rocDtl.getRocDetailId());
			rowCount = statement.executeUpdate();
			if(rowCount>0){
				returnedObj = new HashMap<>();
				returnedObj.put(RETURN_OBJECT,true);
 			} else {
 				returnedObj = new HashMap<>();
				returnedObj.put(RETURN_OBJECT,false);
 			}
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconRocLevelDAO::deleteByRocDetailId(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconRocLevelDAO::deleteByRocDetailId() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}
	
	public Map deleteBySummaryIdList(AmexOptbReconRocLevel reconRocLevel) {
		PreparedStatement statement = null;
		int[] rowCount = null;
		Map returnedObj = null;
		try {
			returnedObj = new HashMap<>();
			if(reconRocLevel.getSummaryIdList()!=null && !reconRocLevel.getSummaryIdList().isEmpty()){
				statement = mesConn.prepareStatement(DELETE_SQL);
				for (long summaryId:reconRocLevel.getSummaryIdList()){
					statement.setLong(1,summaryId);
					statement.addBatch();
				}
				rowCount = statement.executeBatch();

				if(rowCount!=null && rowCount.length>0){
					returnedObj.put(RETURN_OBJECT,true);
	 			} else {
					returnedObj.put(RETURN_OBJECT,false);
	 			}
			} else {
				returnedObj.put(RETURN_OBJECT,false);
			}
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconRocLevelDAO::deleteBySummaryIdList(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconRocLevelDAO::deleteBySummaryIdList() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}

	
	@Override
	public void delete(Object deleteObj) {
		//implement this method in future
	}

	@Override
	public void update(Object updateObj) {
		//implement this method in future
	}

}
