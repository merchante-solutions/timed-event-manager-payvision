package com.mes.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.persist.vo.AmexOptbReconSummary;

public class AmexOptbReconSummaryDAO extends AmexReconAbstractDAO{
	
	private static Logger log = Logger.getLogger(AmexOptbReconSummaryDAO.class);

	private Connection mesConn;
	
	private static final String RETURN_OBJECT = "returnObject";
	
	private static final String SELECT_PK_SQL = "SELECT RECON_SUMMARY_SEQ.NEXTVAL FROM DUAL";
	
	private static final String INSERT_SQL = "INSERT INTO AMEX_OPTB_RECON_SUMMARY (SUMMARY_ID,FILE_ID,FILE_NAME,FILE_DATE,PAYEE_NUMBER,PAYMENT_YEAR,PAYMENT_NUMBER,PAYMENT_DATE ,PAYMENT_AMOUNT ,DEBIT_BALANCE_AMOUNT ,ABA_BANK_NUMBER ,SE_DDA_NUMBER ,RECORD_COUNT ,CREATE_DATE ,UPDATE_DATE ,CREATED_BY) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,SYSDATE,NULL,NULL)";

	private static final String DELETE_SQL = "DELETE FROM AMEX_OPTB_RECON_SUMMARY WHERE SUMMARY_ID IN (?)";
	
	public AmexOptbReconSummaryDAO(Connection conn) {
		super(conn);
		this.mesConn = conn;
	}


	@Override
	public Object insert(Object insertObj) throws Exception{
		AmexOptbReconSummary summaryVO = null;
		PreparedStatement statement = null;
		try {
			statement = mesConn.prepareStatement(INSERT_SQL);
			summaryVO = (AmexOptbReconSummary) insertObj;
			int idx = 1;
			long reconSummarySeqNum = setPrimaryKeys(summaryVO);
			statement.setLong(idx++, reconSummarySeqNum);
			statement.setLong(idx++, summaryVO.getFileId());
			statement.setString(idx++, summaryVO.getFileName());
			statement.setDate(idx++, summaryVO.getFileDate());
			statement.setLong(idx++,summaryVO.getPayeeNumber());
			statement.setLong(idx++, summaryVO.getPaymentYear());
			statement.setString(idx++,summaryVO.getPaymentNumber());
			statement.setDate(idx++, summaryVO.getPaymentDate());
			statement.setDouble(idx++, summaryVO.getPaymentAmount());
			statement.setDouble(idx++,summaryVO.getDebitBalanceAmount());
			statement.setString(idx++,summaryVO.getAbaBankNumber());
			statement.setString(idx++,summaryVO.getSeDdaNumber());
			statement.setLong(idx++, summaryVO.getRecordCount());
			statement.executeUpdate();
		} catch (SQLException e) {
			log.error("Exception occuried at AmexOptbReconSummaryDAO::insert(). Exception message: " + e.getMessage() );
			throw e;
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconSummaryDAO::insert() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return summaryVO;
	}
	private long setPrimaryKeys(AmexOptbReconSummary reconSummaryVO){
		PreparedStatement statement = null;
		long seqNum = 0;
		ResultSet rs = null;
		try {
			statement = mesConn.prepareStatement(SELECT_PK_SQL);
			rs = statement.executeQuery();
			rs.next();
			seqNum = rs.getLong(1);
			reconSummaryVO.setSummaryId(seqNum);
		}
		catch (SQLException e) {
			log.error("Exception occuried at AmexOptbReconSummaryDAO::setPrimaryKeys(). Exception message: " + e.getMessage() );
		}
		finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconSummaryDAO::setPrimaryKeys() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return seqNum;
	}


	@Override
	public void delete(Object deleteObj) {
		//implement this method in future
	}

	@Override
	public void update(Object updateObj) {
		//implement this method in future
	}

	public Map findByLoadFileId(AmexOptbReconSummary reconSummary) {
		PreparedStatement statement = null;
		ResultSet rs = null;
		List<AmexOptbReconSummary> reconSummaryList = new ArrayList<>();
		Map returnedObj = new HashMap<>();
		try {
			statement = mesConn.prepareStatement("SELECT SUMMARY_ID FROM AMEX_OPTB_RECON_SUMMARY WHERE FILE_ID = ?");
			statement.setLong(1,reconSummary.getFileId());
			rs = statement.executeQuery();

			while(rs.next()){
				AmexOptbReconSummary returnVO= new AmexOptbReconSummary();
				returnVO.setSummaryId(rs.getLong("SUMMARY_ID"));
				reconSummaryList.add(returnVO);
			}
			returnedObj.put(RETURN_OBJECT, reconSummaryList);
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconSummaryDAO::findByLoadFileId(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconSummaryDAO::findByLoadFileId() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}


	public Map deleteBySummaryId(AmexOptbReconSummary reconSummary) {
		PreparedStatement statement = null;
		int rowCount = 0;
		Map returnedObj = null;
		try {
			statement = mesConn.prepareStatement("DELETE FROM AMEX_OPTB_RECON_SUMMARY WHERE SUMMARY_ID = ?");
			statement.setLong(1,reconSummary.getSummaryId());
			rowCount = statement.executeUpdate();
			if(rowCount>0){
				returnedObj = new HashMap<>();
				returnedObj.put(RETURN_OBJECT,true);
			} else {
				returnedObj = new HashMap<>();
				returnedObj.put(RETURN_OBJECT,false);
			}
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconSummaryDAO::deleteBySummaryId(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconSummaryDAO::deleteBySummaryId() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}
	
	public Map deleteBySummaryIdList(AmexOptbReconSummary reconSummary) {
		PreparedStatement statement = null;
		int[] rowCount = null;
		Map returnedObj = null;
		try {
			returnedObj = new HashMap<>();
			if(reconSummary.getSummaryIdList()!=null && !reconSummary.getSummaryIdList().isEmpty()){
				statement = mesConn.prepareStatement(DELETE_SQL);
				for (long summaryId:reconSummary.getSummaryIdList()){
					statement.setLong(1,summaryId);
					statement.addBatch();
				}
				rowCount = statement.executeBatch();

				if(rowCount!=null && rowCount.length>0){
					returnedObj.put(RETURN_OBJECT,true);
	 			} else {
					returnedObj.put(RETURN_OBJECT,false);
	 			}
			} else {
				returnedObj.put(RETURN_OBJECT,false);
			}
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconSummaryDAO::deleteBySummaryIdList(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconSummaryDAO::deleteBySummaryIdList() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}

}
