package com.mes.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.mes.persist.vo.AmexOptbReconSummaryLevel;
import com.mes.persist.vo.AmexOptbReconSummaryLevelCollection;

public class AmexOptbReconSummaryLevelDAO extends AmexReconAbstractDAO{
	
	private static Logger log = Logger.getLogger(AmexOptbReconSummaryLevelDAO.class);
	
	private Connection mesConn;
	
	private static final int BATCH_COMMIT_SIZE = 1000;
	
	private static final String RETURN_OBJECT = "returnObject";
	
	private static final String SELECT_PK_SQL = "SELECT RECON_SUM_LEVEL_SEQ.NEXTVAL FROM DUAL";

	private static final String INSERT_SQL = "INSERT INTO AMEX_OPTB_RECON_SUMMARY_LEVEL (SUMMARY_LEVEL_ID , SUMMARY_DETAIL_ID , FEE_CODE , FEE_DESC , DISCOUNT_AMOUNT , FEE_AMOUNT , FEE_RATE , DISCOUNT_RATE , CREATE_DATE , UPDATE_DATE ,CREATED_BY) VALUES(?,?,?,?,?,?,?,?,SYSDATE,NULL,NULL)";
	
	private static final String DELETE_SQL = "DELETE FROM AMEX_OPTB_RECON_SUMMARY_LEVEL WHERE SUMMARY_DETAIL_ID IN "
											+ "(SELECT SUMMARY_DETAIL_ID FROM AMEX_OPTB_RECON_SUMMARY_DETAIL "
											+ "WHERE SUMMARY_ID IN (?))";
	
	public AmexOptbReconSummaryLevelDAO(Connection conn){
		super(conn);
		this.mesConn = conn;
	}

	public Object insert(Object insertObj) throws Exception{
		PreparedStatement statement = null;
		try {
			statement = mesConn.prepareStatement(INSERT_SQL);
			if(insertObj instanceof AmexOptbReconSummaryLevel){
				AmexOptbReconSummaryLevel summaryLevelVO = (AmexOptbReconSummaryLevel) insertObj;
				int idx = 1;
				long reconSummaryLevelSeqNum = setPrimaryKeys(summaryLevelVO);
				statement.setLong(idx++,reconSummaryLevelSeqNum);
				statement.setLong(idx++, summaryLevelVO.getSummaryDetailId());
				statement.setString(idx++, summaryLevelVO.getFeeCode());
				statement.setString(idx++, summaryLevelVO.getFeeDesc());
				statement.setDouble(idx++, summaryLevelVO.getDiscountAmount());
				statement.setDouble(idx++, summaryLevelVO.getFeeAmount());
				statement.setDouble(idx++, summaryLevelVO.getDiscountRate());
				statement.setDouble(idx++, summaryLevelVO.getFeeRate());
				statement.executeUpdate();
				return summaryLevelVO;
			} else {
				AmexOptbReconSummaryLevelCollection reconSummaryLevelCollection = (AmexOptbReconSummaryLevelCollection) insertObj;
				AmexOptbReconSummaryLevelCollection returnCollection = new AmexOptbReconSummaryLevelCollection(); 
				int count = 0;
				for(AmexOptbReconSummaryLevel summaryLevelVO:reconSummaryLevelCollection.getAmexOptbReconSummaryLevelList()){
					int idx = 1;
					long reconSummaryLevelSeqNum = setPrimaryKeys(summaryLevelVO);
					statement.setLong(idx++,reconSummaryLevelSeqNum);
					statement.setLong(idx++, summaryLevelVO.getSummaryDetailId());
					statement.setString(idx++, summaryLevelVO.getFeeCode());
					statement.setString(idx++, summaryLevelVO.getFeeDesc());
					statement.setDouble(idx++, summaryLevelVO.getDiscountAmount());
					statement.setDouble(idx++, summaryLevelVO.getFeeAmount());
					statement.setDouble(idx++, summaryLevelVO.getDiscountRate());
					statement.setDouble(idx++, summaryLevelVO.getFeeRate());
					statement.addBatch();
					if(++count % BATCH_COMMIT_SIZE == 0) {
						statement.executeBatch();
					}
					returnCollection.addReconSummaryLevelList(summaryLevelVO);
				}
				statement.executeBatch();
				return returnCollection;
			}
		} catch (SQLException e) {
			log.error("Exception occuried at AmexOptbReconSummaryLevelDAO::insert(). Exception message: " + e.getMessage() );
			throw e;
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconSummaryLevelDAO::insert() try/catch block. Exception message: " + e.getMessage() );
			}
		}
	}
	
	private long setPrimaryKeys(AmexOptbReconSummaryLevel reconSummaryLevelVO){
		PreparedStatement statement = null;
		long seqNum = 0;
		ResultSet rs = null;
		try {
				statement = mesConn.prepareStatement(SELECT_PK_SQL);
				rs = statement.executeQuery();
				rs.next();
				seqNum = rs.getLong(1);
				reconSummaryLevelVO.setSummaryLevelId(seqNum);
		}
		catch (SQLException e) {
			log.error("Exception occuried at AmexOptbReconSummaryLevelDAO::setPrimaryKeys(). Exception message: " + e.getMessage() );
		}
		finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconSummaryLevelDAO::setPrimaryKeys() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return seqNum;
	}

	@Override
	public void delete(Object deleteObj) {
		//implement this method in future
	}

	@Override
	public void update(Object updateObj) {
		//implement this method in future
	}
	
	
	public Map findBySummaryDetailId(AmexOptbReconSummaryLevel reconSummaryDtlLevel) {
		PreparedStatement statement = null;
		ResultSet rs = null;
		List<AmexOptbReconSummaryLevel> reconSummaryDtlLevelList = new ArrayList<>();
		Map returnedObj = new HashMap<>();
		try {
			statement = mesConn.prepareStatement("SELECT SUMMARY_LEVEL_ID FROM AMEX_OPTB_RECON_SUMMARY_LEVEL WHERE SUMMARY_DETAIL_ID = ?");
			statement.setLong(1,reconSummaryDtlLevel.getSummaryDetailId());
			rs = statement.executeQuery();
			while(rs.next()){
				AmexOptbReconSummaryLevel returnVO= new AmexOptbReconSummaryLevel();
				returnVO.setSummaryLevelId(rs.getLong("SUMMARY_LEVEL_ID"));
				reconSummaryDtlLevelList.add(returnVO);
			}
			returnedObj.put(RETURN_OBJECT, reconSummaryDtlLevelList);
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconSummaryLevelDAO::findBySummaryDetailId(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconSummaryLevelDAO::findBySummaryDetailId() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}
	
	public Map deleteBySummaryDetailId(AmexOptbReconSummaryLevel reconSummaryDtlLevel) {
		PreparedStatement statement = null;
		int rowCount = 0;
		Map returnedObj = null;
		try {
			statement = mesConn.prepareStatement("DELETE FROM AMEX_OPTB_RECON_SUMMARY_LEVEL WHERE SUMMARY_DETAIL_ID = ?");
			statement.setLong(1,reconSummaryDtlLevel.getSummaryDetailId());
			rowCount = statement.executeUpdate();
			if(rowCount>0){
				returnedObj = new HashMap<>();
				returnedObj.put(RETURN_OBJECT,true);
 			} else {
 				returnedObj = new HashMap<>();
				returnedObj.put(RETURN_OBJECT,false);
 			}
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconSummaryLevelDAO::deleteBySummaryDetailId(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconSummaryLevelDAO::deleteBySummaryDetailId() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}
	
	public Map deleteBySummaryIdList(AmexOptbReconSummaryLevel reconSummaryDtlLevel) {
		PreparedStatement statement = null;
		int[] rowCount = null;
		Map returnedObj = null;
		try {
			returnedObj = new HashMap<>();
			if(reconSummaryDtlLevel.getSummaryIdList()!=null && !reconSummaryDtlLevel.getSummaryIdList().isEmpty()){
				statement = mesConn.prepareStatement(DELETE_SQL);
				
				for (long summaryId:reconSummaryDtlLevel.getSummaryIdList()){
					statement.setLong(1,summaryId);
					statement.addBatch();
				}
				rowCount = statement.executeBatch();

				if(rowCount!=null && rowCount.length>0){
					returnedObj.put(RETURN_OBJECT,true);
	 			} else {
					returnedObj.put(RETURN_OBJECT,false);
	 			}
			} else {
				returnedObj.put(RETURN_OBJECT,false);
			}
		} catch (Exception e) {
			log.error("Exception occuried at AmexOptbReconSummaryLevelDAO::deleteBySummaryIdList(). Exception message: " + e.getMessage() );
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
			}
			catch (SQLException e) {
				log.error("Exception occuried at AmexOptbReconSummaryLevelDAO::deleteBySummaryIdList() try/catch block. Exception message: " + e.getMessage() );
			}
		}
		return returnedObj; 
	}
	
}
