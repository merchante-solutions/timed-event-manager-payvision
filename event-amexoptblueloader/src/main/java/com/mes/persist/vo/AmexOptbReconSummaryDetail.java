package com.mes.persist.vo;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;


/**
 * The persistent class for the AMEX_OPTB_RECON_SUMMARY_DETAIL database table.
 * 
 */
public class AmexOptbReconSummaryDetail implements Serializable {

	private static final long serialVersionUID = 1L;

	private AmexOptbReconSummaryLevel reconSummaryLevel;

	private List<AmexOptbReconFeeRecord> reconFeeRecordList;

	private List<AmexOptbReconRocDtl> amexOptbReconRocDtlList;

	private long summaryDetailId;

	private long summaryId;

	private double baseDiscountAmount;

	private double cardNotPresentBpaAmount;

	private double cardNotPresentPtaAmount;

	private String cpcIndicator;

	private double discountAmount;

	private double grossAmount;

	private double netSocAmount;

	private Date processDate;

	private Date seBusinessDate;

	private long seNumber;

	private double serviceFeeAmount;

	private double socAmount;

	private long socInvoiceNumber;

	private long trackingId;

	private double transactionFeeAmount;

	private double transactionFeeRate;

	private double cardNotPresentBpaRate;

	private double cardNotPresentPtaRate;

	private double discountRate;

	private double serviceFeeRate;
	
	private List<Long> summaryIdList;
	
	public AmexOptbReconSummaryDetail(){
		amexOptbReconRocDtlList = new ArrayList<>();
		reconFeeRecordList = new ArrayList<>();
	}


	public long getSummaryDetailId() {
		return this.summaryDetailId;
	}

	public void setSummaryDetailId(long summaryDetailId) {
		this.summaryDetailId = summaryDetailId;
	}

	public long getSummaryId() {
		return this.summaryId;
	}

	public void setSummaryId(long summaryId) {
		this.summaryId = summaryId;
	}

	public double getBaseDiscountAmount() {
		return this.baseDiscountAmount;
	}

	public void setBaseDiscountAmount(double baseDiscountAmount) {
		this.baseDiscountAmount = baseDiscountAmount;
	}

	public double getCardNotPresentBpaAmount() {
		return this.cardNotPresentBpaAmount;
	}

	public void setCardNotPresentBpaAmount(double cardNotPresentBpaAmount) {
		this.cardNotPresentBpaAmount = cardNotPresentBpaAmount;
	}

	public double getCardNotPresentPtaAmount() {
		return this.cardNotPresentPtaAmount;
	}

	public void setCardNotPresentPtaAmount(double cardNotPresentPtaAmount) {
		this.cardNotPresentPtaAmount = cardNotPresentPtaAmount;
	}

	public String getCpcIndicator() {
		return this.cpcIndicator;
	}

	public void setCpcIndicator(String cpcIndicator) {
		this.cpcIndicator = cpcIndicator;
	}

	public double getDiscountAmount() {
		return this.discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public double getGrossAmount() {
		return this.grossAmount;
	}

	public void setGrossAmount(double grossAmount) {
		this.grossAmount = grossAmount;
	}

	public double getNetSocAmount() {
		return this.netSocAmount;
	}

	public void setNetSocAmount(double netSocAmount) {
		this.netSocAmount = netSocAmount;
	}

	public Date getProcessDate() {
		return this.processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public Date getSeBusinessDate() {
		return this.seBusinessDate;
	}

	public void setSeBusinessDate(Date seBusinessDate) {
		this.seBusinessDate = seBusinessDate;
	}

	public long getSeNumber() {
		return this.seNumber;
	}

	public void setSeNumber(long seNumber) {
		this.seNumber = seNumber;
	}

	public double getServiceFeeAmount() {
		return this.serviceFeeAmount;
	}

	public void setServiceFeeAmount(double serviceFeeAmount) {
		this.serviceFeeAmount = serviceFeeAmount;
	}

	public double getSocAmount() {
		return this.socAmount;
	}

	public void setSocAmount(double socAmount) {
		this.socAmount = socAmount;
	}

	public long getSocInvoiceNumber() {
		return this.socInvoiceNumber;
	}

	public void setSocInvoiceNumber(long socInvoiceNumber) {
		this.socInvoiceNumber = socInvoiceNumber;
	}

	public long getTrackingId() {
		return this.trackingId;
	}

	public void setTrackingId(long trackingId) {
		this.trackingId = trackingId;
	}

	public double getTransactionFeeAmount() {
		return this.transactionFeeAmount;
	}

	public void setTransactionFeeAmount(double transactionFeeAmount) {
		this.transactionFeeAmount = transactionFeeAmount;
	}

	public double getTransactionFeeRate() {
		return transactionFeeRate;
	}

	public void setTransactionFeeRate(double transactionFeeRate) {
		this.transactionFeeRate = transactionFeeRate;
	}

	public double getCardNotPresentBpaRate() {
		return cardNotPresentBpaRate;
	}

	public void setCardNotPresentBpaRate(double cardNotPresentBpaRate) {
		this.cardNotPresentBpaRate = cardNotPresentBpaRate;
	}

	public double getCardNotPresentPtaRate() {
		return cardNotPresentPtaRate;
	}

	public void setCardNotPresentPtaRate(double cardNotPresentPtaRate) {
		this.cardNotPresentPtaRate = cardNotPresentPtaRate;
	}

	public double getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(double discountRate) {
		this.discountRate = discountRate;
	}

	public double getServiceFeeRate() {
		return serviceFeeRate;
	}

	public void setServiceFeeRate(double serviceFeeRate) {
		this.serviceFeeRate = serviceFeeRate;
	}

	public AmexOptbReconSummaryLevel getReconSummaryLevel() {
		return reconSummaryLevel;
	}

	public void setReconSummaryLevel(AmexOptbReconSummaryLevel reconSummaryLevel) {
		this.reconSummaryLevel = reconSummaryLevel;
	}

	/**
	 * @param optbReconRocDtl
	 */
	public void addRocDtlList(AmexOptbReconRocDtl optbReconRocDtl) {
		this.amexOptbReconRocDtlList.add(optbReconRocDtl);
	}

	/**
	 * @return
	 */
	public List<AmexOptbReconRocDtl> getAmexOptbReconRocDtlList() {
		return amexOptbReconRocDtlList;
	}
	
	/**
	 * @param optbReconRocDtl
	 */
	public void addReconFeeRecordList(AmexOptbReconFeeRecord optbReconFeeRecord) {
		this.reconFeeRecordList.add(optbReconFeeRecord);
	}

	/**
	 * @return
	 */
	public List<AmexOptbReconFeeRecord> getAmexOptbReconFeeRecordList() {
		return reconFeeRecordList;
	}
	
	public List<Long> getSummaryIdList() {
		return summaryIdList;
	}

	public void setSummaryIdList(List<Long> summaryIdList) {
		this.summaryIdList = summaryIdList;
	}

}