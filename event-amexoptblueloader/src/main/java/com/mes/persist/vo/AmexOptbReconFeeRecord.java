package com.mes.persist.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the AMEX_OPTB_RECON_FEE_RECORDS database table.
 * 
 */
public class AmexOptbReconFeeRecord implements Serializable {
	private static final long serialVersionUID = 1L;

	private long feeId;

	private double adjustmentAmount;

	private long adjustmentNumber;

	private String adjustmentReason;

	private double assetBillingAmount;

	private String assetBillingDescription;

	private String assetBillingTax;

	private String batchCode;

	private String billCode;

	private double chargebackAmount;

	private String chargebackReason;

	private String cmNumber;

	private String detailRecType;

	private long merchantNumber;

	private double netAdjustmentAmount;

	private double netChargebackAmount;

	private double otherFeeAmount;

	private String otherFeeDescription;

	private String payInGrossIndicator;

	private Date processDate;

	private String recType;

	private long seNumber;

	private long summaryId;
	
	private long summaryDetailId;

	private double takeOneCommissionAmount;

	private String takeOneDescription;
	
	private long loadSec;
	
	private List<Long> summaryIdList;

	public long getFeeId() {
		return this.feeId;
	}

	public void setFeeId(long feeId) {
		this.feeId = feeId;
	}

	public double getAdjustmentAmount() {
		return this.adjustmentAmount;
	}

	public void setAdjustmentAmount(double adjustmentAmount) {
		this.adjustmentAmount = adjustmentAmount;
	}

	public long getAdjustmentNumber() {
		return this.adjustmentNumber;
	}

	public void setAdjustmentNumber(long adjustmentNumber) {
		this.adjustmentNumber = adjustmentNumber;
	}

	public String getAdjustmentReason() {
		return this.adjustmentReason;
	}

	public void setAdjustmentReason(String adjustmentReason) {
		this.adjustmentReason = adjustmentReason;
	}

	public double getAssetBillingAmount() {
		return this.assetBillingAmount;
	}

	public void setAssetBillingAmount(double assetBillingAmount) {
		this.assetBillingAmount = assetBillingAmount;
	}

	public String getAssetBillingDescription() {
		return this.assetBillingDescription;
	}

	public void setAssetBillingDescription(String assetBillingDescription) {
		this.assetBillingDescription = assetBillingDescription;
	}

	public String getAssetBillingTax() {
		return this.assetBillingTax;
	}

	public void setAssetBillingTax(String assetBillingTax) {
		this.assetBillingTax = assetBillingTax;
	}

	public String getBatchCode() {
		return this.batchCode;
	}

	public void setBatchCode(String batchCode) {
		this.batchCode = batchCode;
	}

	public String getBillCode() {
		return this.billCode;
	}

	public void setBillCode(String billCode) {
		this.billCode = billCode;
	}

	public double getChargebackAmount() {
		return this.chargebackAmount;
	}

	public void setChargebackAmount(double chargebackAmount) {
		this.chargebackAmount = chargebackAmount;
	}

	public String getChargebackReason() {
		return this.chargebackReason;
	}

	public void setChargebackReason(String chargebackReason) {
		this.chargebackReason = chargebackReason;
	}

	public String getCmNumber() {
		return this.cmNumber;
	}

	public void setCmNumber(String cmNumber) {
		this.cmNumber = cmNumber;
	}

	public String getDetailRecType() {
		return this.detailRecType;
	}

	public void setDetailRecType(String detailRecType) {
		this.detailRecType = detailRecType;
	}

	public long getMerchantNumber() {
		return this.merchantNumber;
	}

	public void setMerchantNumber(long merchantNumber) {
		this.merchantNumber = merchantNumber;
	}

	public double getNetAdjustmentAmount() {
		return this.netAdjustmentAmount;
	}

	public void setNetAdjustmentAmount(double netAdjustmentAmount) {
		this.netAdjustmentAmount = netAdjustmentAmount;
	}

	public double getNetChargebackAmount() {
		return this.netChargebackAmount;
	}

	public void setNetChargebackAmount(double netChargebackAmount) {
		this.netChargebackAmount = netChargebackAmount;
	}

	public double getOtherFeeAmount() {
		return this.otherFeeAmount;
	}

	public void setOtherFeeAmount(double otherFeeAmount) {
		this.otherFeeAmount = otherFeeAmount;
	}

	public String getOtherFeeDescription() {
		return this.otherFeeDescription;
	}

	public void setOtherFeeDescription(String otherFeeDescription) {
		this.otherFeeDescription = otherFeeDescription;
	}

	public String getPayInGrossIndicator() {
		return this.payInGrossIndicator;
	}

	public void setPayInGrossIndicator(String payInGrossIndicator) {
		this.payInGrossIndicator = payInGrossIndicator;
	}

	public Date getProcessDate() {
		return this.processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

	public String getRecType() {
		return this.recType;
	}

	public void setRecType(String recType) {
		this.recType = recType;
	}

	public long getSeNumber() {
		return this.seNumber;
	}

	public void setSeNumber(long seNumber) {
		this.seNumber = seNumber;
	}

	public long getSummaryId() {
		return this.summaryId;
	}

	public void setSummaryId(long summaryId) {
		this.summaryId = summaryId;
	}

	public long getSummaryDetailId() {
		return summaryDetailId;
	}

	public void setSummaryDetailId(long summaryDetailId) {
		this.summaryDetailId = summaryDetailId;
	}

	public double getTakeOneCommissionAmount() {
		return this.takeOneCommissionAmount;
	}

	public void setTakeOneCommissionAmount(double takeOneCommissionAmount) {
		this.takeOneCommissionAmount = takeOneCommissionAmount;
	}

	public String getTakeOneDescription() {
		return this.takeOneDescription;
	}

	public void setTakeOneDescription(String takeOneDescription) {
		this.takeOneDescription = takeOneDescription;
	}
	
	public long getLoadSec() {
		return loadSec;
	}

	public void setLoadSec(long loadSec) {
		this.loadSec = loadSec;
	}

	public List<Long> getSummaryIdList() {
		return summaryIdList;
	}

	public void setSummaryIdList(List<Long> summaryIdList) {
		this.summaryIdList = summaryIdList;
	}

}