package com.mes.persist.vo;

import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the AMEX_OPTB_RECON_SUMMARY_LEVEL database table.
 * 
 */
public class AmexOptbReconSummaryLevel implements Serializable {
	private static final long serialVersionUID = 1L;

	private long summaryLevelId;
	
	private long summaryDetailId;

	private double discountAmount;

	private double feeAmount;

	private String feeCode;

	private String feeDesc;
	
	private double feeRate;
	
	private double discountRate;
	
	private List<Long> summaryIdList;
	
	public long getSummaryLevelId() {
		return this.summaryLevelId;
	}

	public void setSummaryLevelId(long summaryLevelId) {
		this.summaryLevelId = summaryLevelId;
	}
	
	public long getSummaryDetailId() {
		return this.summaryDetailId;
	}

	public void setSummaryDetailId(long summaryDetailId) {
		this.summaryDetailId = summaryDetailId;
	}

	public double getDiscountAmount() {
		return this.discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public double getFeeAmount() {
		return this.feeAmount;
	}

	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public String getFeeCode() {
		return this.feeCode;
	}

	public void setFeeCode(String feeCode) {
		this.feeCode = feeCode;
	}

	public String getFeeDesc() {
		return this.feeDesc;
	}

	public void setFeeDesc(String feeDesc) {
		this.feeDesc = feeDesc;
	}

	public double getFeeRate() {
		return feeRate;
	}

	public void setFeeRate(double feeRate) {
		this.feeRate = feeRate;
	}

	public double getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(double discountRate) {
		this.discountRate = discountRate;
	}
	
	public List<Long> getSummaryIdList() {
		return summaryIdList;
	}

	public void setSummaryIdList(List<Long> summaryIdList) {
		this.summaryIdList = summaryIdList;
	}

}