package com.mes.persist.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 */
public class AmexOptbReconSummaryCollection implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8389336937493708562L;
	
	private List<AmexOptbReconSummary> reconSummaries = null;
	
	public AmexOptbReconSummaryCollection(){
		reconSummaries = new ArrayList<>();
	}
	
	/**
	 * @param reconSummary
	 */
	public void addReconSummaryList(AmexOptbReconSummary reconSummary) {
		this.reconSummaries.add(reconSummary);
	}

	/**
	 * @return
	 */
	public List<AmexOptbReconSummary> getAmexOptbReconSummaryList() {
		return reconSummaries;
	}
	
}