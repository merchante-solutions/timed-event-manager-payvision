package com.mes.persist.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * 
 */
public class AmexOptbReconRocLevelCollection implements Serializable {

	private static Logger log = Logger.getLogger(AmexOptbReconRocLevelCollection.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 5600731053842805629L;
	
	private List<AmexOptbReconRocLevel> reconRocLevels = null;
	
	public AmexOptbReconRocLevelCollection(){
		reconRocLevels = new ArrayList<>();
	}
	
	/**
	 * @param reconRocLevel
	 */
	public void addReconRocDtlList(AmexOptbReconRocLevel reconRocLevel) {
		this.reconRocLevels.add(reconRocLevel);
	}

	/**
	 * @return
	 */
	public List<AmexOptbReconRocLevel> getAmexOptbReconRocDtlList() {
		return reconRocLevels;
	}
	
	public boolean isEmpty(){
		if(reconRocLevels==null){
			return true;
		} else {
			return reconRocLevels.isEmpty();
		}
	}
	
	public int size(){
		int i =0;
		try {
			if(reconRocLevels==null){
				return i;
			} else {
				return reconRocLevels.size();
			}
		} catch (Exception e) {
			log.error("Exception occured at AmexOptbReconRocLevelCollection::size(). Exception Message: "+ e.getMessage() );
		}
		return i;
	}
	
}