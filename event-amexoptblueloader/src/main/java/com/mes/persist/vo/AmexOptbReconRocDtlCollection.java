package com.mes.persist.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;


/**
 * 
 */
public class AmexOptbReconRocDtlCollection implements Serializable {
	
	private static Logger log = Logger.getLogger(AmexOptbReconRocDtlCollection.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1760930920013277907L;

	private List<AmexOptbReconRocDtl> reconRocDtls = null;
	
	public AmexOptbReconRocDtlCollection(){
		reconRocDtls = new ArrayList<>();
	}
	
	/**
	 * @param reconRocDtl
	 */
	public void addReconRocDtlList(AmexOptbReconRocDtl reconRocDtl) {
		this.reconRocDtls.add(reconRocDtl);
	}

	/**
	 * @return
	 */
	public List<AmexOptbReconRocDtl> getAmexOptbReconRocDtlList() {
		return reconRocDtls;
	}
	
	public boolean isEmpty(){
		if(reconRocDtls==null){
			return true;
		} else {
			return reconRocDtls.isEmpty();
		}
	}
	
	public int size(){
		int i =0;
		try {
			if(reconRocDtls==null){
				return i;
			} else {
				return reconRocDtls.size();
			}
		} catch (Exception e) {
			log.error("Exception occured at AmexOptbReconRocDtlCollection::size(). Exception Message: "+ e.getMessage() );
		}
		return i;
	}
	
}