package com.mes.persist.vo;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;


/**
 * The persistent class for the AMEX_OPTB_RECON_SUMMARY database table.
 * 
 */
public class AmexOptbReconSummary implements Serializable {
	private static final long serialVersionUID = 1L;

	private long summaryId;

	private String abaBankNumber;

	private double debitBalanceAmount;

	private Date fileDate;

	private long fileId;

	private String fileName;

	private long payeeNumber;

	private double paymentAmount;

	private Date paymentDate;

	private String paymentNumber;

	private long paymentYear;

	private long recordCount;

	private String seDdaNumber;
	
	private List<Long> summaryIdList;
	
	public List<Long> getSummaryIdList() {
		return summaryIdList;
	}

	public void setSummaryIdList(List<Long> summaryIdList) {
		this.summaryIdList = summaryIdList;
	}

	public AmexOptbReconSummary() {
	}

	public long getSummaryId() {
		return this.summaryId;
	}

	public void setSummaryId(long summaryId) {
		this.summaryId = summaryId;
	}

	public String getAbaBankNumber() {
		return this.abaBankNumber;
	}

	public void setAbaBankNumber(String abaBankNumber) {
		this.abaBankNumber = abaBankNumber;
	}

	public double getDebitBalanceAmount() {
		return this.debitBalanceAmount;
	}

	public void setDebitBalanceAmount(double debitBalanceAmount) {
		this.debitBalanceAmount = debitBalanceAmount;
	}

	public Date getFileDate() {
		return this.fileDate;
	}

	public void setFileDate(Date fileDate) {
		this.fileDate = fileDate;
	}

	public long getFileId() {
		return this.fileId;
	}

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public long getPayeeNumber() {
		return this.payeeNumber;
	}

	public void setPayeeNumber(long payeeNumber) {
		this.payeeNumber = payeeNumber;
	}

	public double getPaymentAmount() {
		return this.paymentAmount;
	}

	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Date getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPaymentNumber() {
		return this.paymentNumber;
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	public long getPaymentYear() {
		return this.paymentYear;
	}

	public void setPaymentYear(long paymentYear) {
		this.paymentYear = paymentYear;
	}

	public long getRecordCount() {
		return this.recordCount;
	}

	public void setRecordCount(long recordCount) {
		this.recordCount = recordCount;
	}

	public String getSeDdaNumber() {
		return this.seDdaNumber;
	}

	public void setSeDdaNumber(String seDdaNumber) {
		this.seDdaNumber = seDdaNumber;
	}

}