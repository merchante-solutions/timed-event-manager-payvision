package com.mes.persist.vo;

import java.util.ArrayList;
import java.util.List;

public class AmexOptbReconFileTransaction {

	/**
	 * Constructor
	 */
	public AmexOptbReconFileTransaction() {
		amexOptbReconSummaryDetailList = new ArrayList<AmexOptbReconSummaryDetail>();
		/*amexOptbReconSummaryLevelList = new ArrayList<AmexOptbReconSummaryLevel>();
		amexOptbReconRocDtlList = new ArrayList<AmexOptbReconRocDtl>();
		amexOptbReconRocLevelList = new ArrayList<AmexOptbReconRocLevel>();
		amexOptbReconFeeRecordList = new ArrayList<AmexOptbReconFeeRecord>();*/
	}

	/**
	 * variable amexOptbReconSummary
	 */
	private AmexOptbReconSummary amexOptbReconSummary = null;
	/**
	 * variable amexOptbReconSummaryDetailList
	 */
	private List<AmexOptbReconSummaryDetail> amexOptbReconSummaryDetailList = null;
	/**
	 * variable amexOptbReconSummaryLevelList
	 *//*
	private List<AmexOptbReconSummaryLevel> amexOptbReconSummaryLevelList = null;
	*//**
	 * variable amexOptbReconRocDtlList
	 *//*
	private List<AmexOptbReconRocDtl> amexOptbReconRocDtlList = null;
	*//**
	 * variable amexOptbReconRocLevelList
	 *//*
	private List<AmexOptbReconRocLevel> amexOptbReconRocLevelList = null;
	*//**
	 * variable amexOptbReconFeeRecordList
	 *//*
	private List<AmexOptbReconFeeRecord> amexOptbReconFeeRecordList = null;
	
	*//**
	 * variable recordCount
	 *//*
	private int recordCount;*/

	/**
	 * @param optbReconSummaryDetail
	 */
	public void addSummaryDetailList(AmexOptbReconSummaryDetail optbReconSummaryDetail) {
		this.amexOptbReconSummaryDetailList.add(optbReconSummaryDetail);
	}

	/**
	 * @param optbReconSummaryLevel
	 *//*
	public void addSummaryLevelList(AmexOptbReconSummaryLevel optbReconSummaryLevel) {
		this.amexOptbReconSummaryLevelList.add(optbReconSummaryLevel);
	}

	*//**
	 * @param optbReconRocDtl
	 *//*
	public void addRocDtlList(AmexOptbReconRocDtl optbReconRocDtl) {
		this.amexOptbReconRocDtlList.add(optbReconRocDtl);
	}

	*//**
	 * @param optbReconRocLevel
	 *//*
	public void addRocLevelList(AmexOptbReconRocLevel optbReconRocLevel) {
		this.amexOptbReconRocLevelList.add(optbReconRocLevel);
	}

	*//**
	 * @param optbReconFeeRecord
	 *//*
	public void addFeeRecordList(AmexOptbReconFeeRecord optbReconFeeRecord) {
		this.amexOptbReconFeeRecordList.add(optbReconFeeRecord);
	}
*/
	/**
	 * @return
	 */
	public AmexOptbReconSummary getAmexOptbReconSummary() {
		return amexOptbReconSummary;
	}

	/**
	 * @param amexOptbReconSummary
	 */
	public void setAmexOptbReconSummary(AmexOptbReconSummary amexOptbReconSummary) {
		this.amexOptbReconSummary = amexOptbReconSummary;
	}

	/**
	 * @return
	 */
	public List<AmexOptbReconSummaryDetail> getAmexOptbReconSummaryDetailList() {
		return amexOptbReconSummaryDetailList;
	}

	/**
	 * @return
	 *//*
	public List<AmexOptbReconSummaryLevel> getAmexOptbReconSummaryLevelList() {
		return amexOptbReconSummaryLevelList;
	}

	*//**
	 * @return
	 *//*
	public List<AmexOptbReconRocDtl> getAmexOptbReconRocDtlList() {
		return amexOptbReconRocDtlList;
	}

	*//**
	 * @return
	 *//*
	public List<AmexOptbReconRocLevel> getAmexOptbReconRocLevelList() {
		return amexOptbReconRocLevelList;
	}

	*//**
	 * @return
	 *//*
	public List<AmexOptbReconFeeRecord> getAmexOptbReconFeeRecordList() {
		return amexOptbReconFeeRecordList;
	}

	public int getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}
*/
}
