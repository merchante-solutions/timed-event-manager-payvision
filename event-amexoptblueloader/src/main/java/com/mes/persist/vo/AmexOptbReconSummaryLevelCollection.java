package com.mes.persist.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;


/**
 * 
 */
public class AmexOptbReconSummaryLevelCollection implements Serializable {
	
	private static Logger log = Logger.getLogger(AmexOptbReconSummaryLevelCollection.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 8547768721066990749L;
	
	private List<AmexOptbReconSummaryLevel> reconSummaryLevels = null;
	
	public AmexOptbReconSummaryLevelCollection(){
		reconSummaryLevels = new ArrayList<>();
	}
	
	/**
	 * @param reconSummaryLevel
	 */
	public void addReconSummaryLevelList(AmexOptbReconSummaryLevel reconSummaryLevel) {
		this.reconSummaryLevels.add(reconSummaryLevel);
	}

	/**
	 * @return
	 */
	public List<AmexOptbReconSummaryLevel> getAmexOptbReconSummaryLevelList() {
		return reconSummaryLevels;
	}
	
	public boolean isEmpty(){
		if(reconSummaryLevels==null){
			return true;
		} else {
			return reconSummaryLevels.isEmpty();
		}
	}
	
	public int size(){
		int i =0;
		try {
			if(reconSummaryLevels==null){
				return i;
			} else {
				return reconSummaryLevels.size();
			}
		} catch (Exception e) {
			log.error("Exception occured at AmexOptbReconSummaryLevelCollection::size(). Exception Message: "+ e.getMessage() );
		}
		return i;
	}
	
}