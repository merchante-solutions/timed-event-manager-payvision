package com.mes.persist.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;


/**
 * 
 */
public class AmexOptbReconSummaryDetailCollection implements Serializable {
	
	private static Logger log = Logger.getLogger(AmexOptbReconSummaryDetailCollection.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = -4055062103979858193L;

	private List<AmexOptbReconSummaryDetail> reconSummaryDetails = null;
	
	public AmexOptbReconSummaryDetailCollection(){
		reconSummaryDetails = new ArrayList<>();
	}
	
	/**
	 * @param reconSummaryDetail
	 */
	public void addReconSummaryDetailList(AmexOptbReconSummaryDetail reconSummaryDetail) {
		this.reconSummaryDetails.add(reconSummaryDetail);
	}

	/**
	 * @return
	 */
	public List<AmexOptbReconSummaryDetail> getAmexOptbReconSummaryDetailList() {
		return reconSummaryDetails;
	}

	public void setReconSummaryDetails(List<AmexOptbReconSummaryDetail> reconSummaryDetails) {
		this.reconSummaryDetails = reconSummaryDetails;
	}
	
	public boolean isEmpty(){
		if(reconSummaryDetails==null){
			return true;
		} else {
			return reconSummaryDetails.isEmpty();
		}
	}
	
	public int size(){
		int i =0;
		try {
			if(reconSummaryDetails==null){
				return i;
			} else {
				return reconSummaryDetails.size();
			}
		} catch (Exception e) {
			log.error("Exception occured at AmexOptbReconSummaryDetailCollection::size(). Exception Message: "+ e.getMessage() );
		}
		return i;
	}
	
}