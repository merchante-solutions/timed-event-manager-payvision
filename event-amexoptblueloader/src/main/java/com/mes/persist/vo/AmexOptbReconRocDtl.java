package com.mes.persist.vo;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;


/**
 * The persistent class for the AMEX_OPTB_RECON_ROC_DTL database table.
 * 
 */
public class AmexOptbReconRocDtl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<AmexOptbReconRocLevel> amexOptbReconRocLevelList;

	private long rocDetailId;
	
	private long summaryDetailId;

	private String capnNonCompliantError;

	private String capnNonCompliantIndicator;

	private String cmNumber;

	private String cmNumber19;

	private String cmNumberEnc;

	private String cmRefNo;

	private String dqNonComplaintError;

	private String dqNonCompliantInd;

	private String seRefPoa;

	private Date tranDate;
	
	private String nonSwipedInd;
	
	private List<Long> summaryIdList;
	
	public List<Long> getSummaryIdList() {
		return summaryIdList;
	}

	public void setSummaryIdList(List<Long> summaryIdList) {
		this.summaryIdList = summaryIdList;
	}

	public AmexOptbReconRocDtl() {
		amexOptbReconRocLevelList = new ArrayList<AmexOptbReconRocLevel>();
	}

	public long getRocDetailId() {
		return this.rocDetailId;
	}

	public void setRocDetailId(long rocDetailId) {
		this.rocDetailId = rocDetailId;
	}
	
	public long getSummaryDetailId() {
		return this.summaryDetailId;
	}

	public void setSummaryDetailId(long summaryDetailId) {
		this.summaryDetailId = summaryDetailId;
	}

	public String getCapnNonCompliantError() {
		return this.capnNonCompliantError;
	}

	public void setCapnNonCompliantError(String capnNonCompliantError) {
		this.capnNonCompliantError = capnNonCompliantError;
	}

	public String getCapnNonCompliantIndicator() {
		return this.capnNonCompliantIndicator;
	}

	public void setCapnNonCompliantIndicator(String capnNonCompliantIndicator) {
		this.capnNonCompliantIndicator = capnNonCompliantIndicator;
	}

	public String getCmNumber() {
		return this.cmNumber;
	}

	public void setCmNumber(String cmNumber) {
		this.cmNumber = cmNumber;
	}

	public String getCmNumber19() {
		return this.cmNumber19;
	}

	public void setCmNumber19(String cmNumber19) {
		this.cmNumber19 = cmNumber19;
	}

	public String getCmNumberEnc() {
		return this.cmNumberEnc;
	}

	public void setCmNumberEnc(String cmNumberEnc) {
		this.cmNumberEnc = cmNumberEnc;
	}

	public String getCmRefNo() {
		return this.cmRefNo;
	}

	public void setCmRefNo(String cmRefNo) {
		this.cmRefNo = cmRefNo;
	}

	public String getDqNonComplaintError() {
		return this.dqNonComplaintError;
	}

	public void setDqNonComplaintError(String dqNonComplaintError) {
		this.dqNonComplaintError = dqNonComplaintError;
	}

	public String getDqNonCompliantInd() {
		return this.dqNonCompliantInd;
	}

	public void setDqNonCompliantInd(String dqNonCompliantInd) {
		this.dqNonCompliantInd = dqNonCompliantInd;
	}

	public String getSeRefPoa() {
		return this.seRefPoa;
	}

	public void setSeRefPoa(String seRefPoa) {
		this.seRefPoa = seRefPoa;
	}

	public Date getTranDate() {
		return this.tranDate;
	}

	public void setTranDate(Date tranDate) {
		this.tranDate = tranDate;
	}

	public String getNonSwipedInd() {
		return nonSwipedInd;
	}

	public void setNonSwipedInd(String nonSwipedInd) {
		this.nonSwipedInd = nonSwipedInd;
	}
	
	/**
	 * @param optbReconRocLevel
	 */
	public void addRocLevelList(AmexOptbReconRocLevel optbReconRocDtl) {
		this.amexOptbReconRocLevelList.add(optbReconRocDtl);
	}

	/**
	 * @return
	 */
	public List<AmexOptbReconRocLevel> getAmexOptbReconRocLevelList() {
		return amexOptbReconRocLevelList;
	}
	
}