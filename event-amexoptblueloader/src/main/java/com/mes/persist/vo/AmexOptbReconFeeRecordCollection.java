package com.mes.persist.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;


/**
 * 
 */
public class AmexOptbReconFeeRecordCollection implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1066044700061353127L;

	private static Logger log = Logger.getLogger(AmexOptbReconFeeRecordCollection.class);

	private List<AmexOptbReconFeeRecord> reconFeeRecords = null;
	
	public AmexOptbReconFeeRecordCollection(){
		reconFeeRecords = new ArrayList<>();
	}
	
	/**
	 * @param reconFeeRecord
	 */
	public void addReconFeeRecordList(AmexOptbReconFeeRecord reconFeeRecord) {
		this.reconFeeRecords.add(reconFeeRecord);
	}

	/**
	 * @return
	 */
	public List<AmexOptbReconFeeRecord> getAmexOptbReconFeeRecordList() {
		return reconFeeRecords;
	}
	
	public boolean isEmpty(){
		if(reconFeeRecords==null){
			return true;
		} else {
			return reconFeeRecords.isEmpty();
		}
	}
	
	public int size(){
		int i =0;
		try {
			if(reconFeeRecords==null){
				return i;
			} else {
				return reconFeeRecords.size();
			}
		} catch (Exception e) {
			log.error("Exception occured at AmexOptbReconFeeRecordCollection::size(). Exception Message: "+ e.getMessage() );
		}
		return i;
	}
	
}