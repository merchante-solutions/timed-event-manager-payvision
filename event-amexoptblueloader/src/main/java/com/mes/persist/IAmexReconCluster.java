package com.mes.persist;

import java.util.Map;

public interface IAmexReconCluster {

	/**
	 * @param insertObj
	 * @return
	 * @throws Exception 
	 */
	public Object insert(Object insertObj) throws Exception;
	/**
	 * @param deleteObj
	 */
	public void delete(Object deleteObj);
	/**
	 * @param updateObj
	 */
	public void update(Object updateObj);
	/**
	 * @param findByObj
	 * @param findBy
	 * @return
	 */
	public Map findBy(Object findByObj,String findBy);
	

}
