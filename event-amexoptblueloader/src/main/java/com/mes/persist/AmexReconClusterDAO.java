package com.mes.persist;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import com.mes.persist.vo.AmexOptbReconFeeRecord;
import com.mes.persist.vo.AmexOptbReconFeeRecordCollection;
import com.mes.persist.vo.AmexOptbReconRocDtl;
import com.mes.persist.vo.AmexOptbReconRocDtlCollection;
import com.mes.persist.vo.AmexOptbReconRocLevel;
import com.mes.persist.vo.AmexOptbReconRocLevelCollection;
import com.mes.persist.vo.AmexOptbReconSummary;
import com.mes.persist.vo.AmexOptbReconSummaryCollection;
import com.mes.persist.vo.AmexOptbReconSummaryDetail;
import com.mes.persist.vo.AmexOptbReconSummaryDetailCollection;
import com.mes.persist.vo.AmexOptbReconSummaryLevel;
import com.mes.persist.vo.AmexOptbReconSummaryLevelCollection;

public class AmexReconClusterDAO {

	/**
	 * Field Conn
	 */
	private Connection mesConn;
	/**
	 * Field abstractDAO
	 */
	private AmexReconAbstractDAO abstractDAO = null;
	/**
	 * USER for MCP Database connection
	 */ 
	private final static String MES_USER = "MES_USER";
	/**
	 * URL for MCP Database Connection
	 */ 
	private final static String MES_URL = "MES_USER";
	/**
	 * Password for MCP Database Connection
	 */ 
	private final static String MES_PASSWORD = "MES_PASSWORD";	
	/**
	 * Field RETURN_OBJECT
	 */
	private final static String RETURN_OBJECT = "returnObject";
	
	/**
	 * enum for CRUD operations
	 */
	private enum Operation { SELECT, INSERT, UPDATE, DELETE };

	/**
	 * Constructor with connection arg
	 * @param mcpCon
	 */
	public AmexReconClusterDAO(Connection mesCon){
		this.mesConn = mesCon;
	}
	/**
	 * Default constructor
	 */
	public AmexReconClusterDAO() {
		this.mesConn = getMESConnection();
	}

	/**
	 * This method is used to get the connections for MCP database
	 * @return Connection
	 */
	public Connection getMESConnection() {
		Connection newConn = null;
		String sURL = null;		    
		String sUSERID = null;		     
		String sPASSWORD = null;		
		try {
			/*Properties props = null; 	
			sUSERID = (String)props.get(MES_USER);		 	
			sPASSWORD = (String)props.get(MES_PASSWORD);	
			sURL = (String)props.get(MES_URL);		 	
			newConn = DriverManager.getConnection(sURL, sUSERID, sPASSWORD);		
			newConn.setAutoCommit(false);*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newConn;	 	  
	}
	
	/**
	 * This method is called from the BO to perform the CRUD operations on the Amex OptBlue Recon tables in mes db
	 * @param dataObj
	 * @param finder
	 * @param operation
	 * @return
	 */
	public Map crudOperation(Object dataObj,String finder,String operation) {
		Map returnMap = null;
		Object returnObject = null; 
		try{
			
			if (dataObj instanceof AmexOptbReconSummary || dataObj instanceof AmexOptbReconSummaryCollection) {
				abstractDAO = new AmexOptbReconSummaryDAO(mesConn);
			} else if (dataObj instanceof AmexOptbReconSummaryDetail || dataObj instanceof AmexOptbReconSummaryDetailCollection) {
				abstractDAO = new AmexOptbReconSummaryDetailDAO(mesConn);
			} else if (dataObj instanceof AmexOptbReconSummaryLevel || dataObj instanceof AmexOptbReconSummaryLevelCollection) {
				abstractDAO = new AmexOptbReconSummaryLevelDAO(mesConn);
			} else if (dataObj instanceof AmexOptbReconRocDtl || dataObj instanceof AmexOptbReconRocDtlCollection) {
				abstractDAO = new AmexOptbReconRocDtlDAO(mesConn);
			} else if (dataObj instanceof AmexOptbReconRocLevel || dataObj instanceof AmexOptbReconRocLevelCollection) {
				abstractDAO = new AmexOptbReconRocLevelDAO(mesConn);
			} else if (dataObj instanceof AmexOptbReconFeeRecord || dataObj instanceof AmexOptbReconFeeRecordCollection) {
				abstractDAO = new AmexOptbReconFeeRecordDAO(mesConn);
			}
			
			switch (Operation.valueOf(operation)) {
			   case INSERT:
				   returnMap = new HashMap();
				   returnObject = abstractDAO.insert(dataObj);
				   returnMap.put(RETURN_OBJECT, returnObject);
				   break;
			   case UPDATE:
				   abstractDAO.update(dataObj);
				   break;
			   case DELETE: 
			   case SELECT:
				   returnMap = abstractDAO.findBy(dataObj,finder);
				   break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnMap;
	}
}