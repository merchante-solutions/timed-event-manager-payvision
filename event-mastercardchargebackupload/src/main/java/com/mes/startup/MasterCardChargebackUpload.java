/*@lineinfo:filename=MasterCardChargebackUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/MasterCardChargebackUpload.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.chargeback.utils.ChargeBackConstants;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.settlement.SettlementRecord;
import sqlj.runtime.ResultSetIterator;

public class MasterCardChargebackUpload extends MasterCardFileBase
{
  public MasterCardChargebackUpload( )
  {
    PropertiesFilename          = "mc-wcb.properties";

    CardTypeTable               = "network_chargeback_mc";
    CardTypeTableActivity       = "network_chargeback_activity";
    CardTypePostTotals          = "mc_wcb";
    CardTypeFileDesc            = "MasterCard Chargeback";
    DkOutgoingHost              = MesDefaults.DK_MC_EP_HOST;
    DkOutgoingUser              = MesDefaults.DK_MC_EP_USER;
    DkOutgoingPassword          = MesDefaults.DK_MC_EP_PASSWORD;
    DkOutgoingPath              = MesDefaults.DK_MC_EP_OUTGOING_PATH;
    DkOutgoingUseBinary         = true;
    DkOutgoingSendFlagFile      = true;
    SettlementAddrsNotify       = MesEmails.MSG_ADDRS_OUTGOING_CB_NOTIFY;
    SettlementAddrsFailure      = MesEmails.MSG_ADDRS_OUTGOING_CB_FAILURE;
    SettlementRecMT             = SettlementRecord.MT_SECOND_PRESENTMENT;
  }
  
  protected boolean processTransactions( )
  {
    Date                      cpd               = null;
    Date                      fileDate          = null;
    ResultSetIterator         it                = null;
    int                       recCount          = 0;
    String                    workFilename      = null;

    String 					  ICA_NUM_DEFAULT   ="0";
    String 					  ICA_NUM_ALL_RECORDS   ="9999";

    try
    {
      if ( TestFilename == null )
      {
        FileTimestamp   = new Timestamp(Calendar.getInstance().getTime().getTime());
        fileDate        = new Date( FileTimestamp.getTime() );
        cpd             = getSettlementDate();

        workFilename    = generateFilename("mc_wcb"                                                     // base name
                                          + String.valueOf(Integer.parseInt( getEventArg(0) )),         // bank number
                                          ".ipm");                                                      // file extension
        
        updateICARecordsForManualProcessing(ICA_NUM_DEFAULT,ICA_NUM_ALL_RECORDS,fileDate,cpd);


        /*@lineinfo:generated-code*//*@lineinfo:77^9*/

//  ************************************************************
//  #sql [Ctx] { update  network_chargeback_activity cba
//            set     cba.load_filename = :workFilename,
//                    cba.settlement_date = :cpd
//            where   cba.cb_load_sec in
//                    (
//                      select  cb.cb_load_sec 
//                      from    network_chargebacks           cb
//                      where   cb.bank_number in (select bank_number from mbs_banks where processor_id = 1)
//                              and cb.incoming_date between (:fileDate-180) and :fileDate 
//                              and cb.merchant_number != 0 
//                              and not cb.card_number_enc is null
//                              and cb.card_type = 'MC'      -- mc only
//                    )
//                    and cba.load_filename is null
//                    and cba.action_code = nvl(:ActionCode,cba.action_code)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  network_chargeback_activity cba\n          set     cba.load_filename =  :1 ,\n                  cba.settlement_date =  :2 \n          where   cba.cb_load_sec in\n                  (\n                    select  cb.cb_load_sec \n                    from    network_chargebacks           cb\n                    where   cb.bank_number in (select bank_number from mbs_banks where processor_id = 1)\n                            and cb.incoming_date between ( :3 -180) and  :4  \n                            and cb.merchant_number != 0 \n                            and not cb.card_number_enc is null\n                            and cb.card_type = 'MC'      -- mc only\n                  )\n                  and cba.load_filename is null\n                  and cba.action_code = nvl( :5 ,cba.action_code)";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.MasterCardChargebackUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setDate(2,cpd);
   __sJT_st.setDate(3,fileDate);
   __sJT_st.setDate(4,fileDate);
   __sJT_st.setString(5,ActionCode);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:94^9*/
        // only process if there were records updated 
        recCount = Ctx.getExecutionContext().getUpdateCount();
      }
      else    // re-build a previous file
      {
        FileTimestamp   = new Timestamp( getFileDate(TestFilename).getTime() );
        fileDate        = new Date( FileTimestamp.getTime() );
        workFilename    = TestFilename;
        recCount        = 1;   // always re-build
      }

      if ( recCount > 0 )
      {
        // select the transactions to process
        /*@lineinfo:generated-code*//*@lineinfo:109^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  0                                           as batch_id,          -- not used in chargeback tables
//                    cb.cb_load_sec                              as batch_record_id,   -- really "cb_load_sec"
//                    cb.card_number                              as card_number,
//                    dukpt_decrypt_wrapper(cb.card_number_enc)   as card_number_full,
//                    cb.card_number_enc                          as card_number_enc,
//                    cb.tran_date                                as transaction_date,
//                    cbm.transaction_date                        as transaction_time,
//                    cb.reference_number                         as reference_number,
//                    cb.bin_number                               as bin_number,
//                    abs( cb.tran_amount )                       as transaction_amount,
//                    cbm.original_amt                            as original_amount,
//                    cbm.original_currency_code                  as currency_code,
//                    cbm.credit_debit_ind                        as debit_credit_indicator,
//                    cbm.cardholder_act_term_ind                 as cat_indicator,
//                    cbm.authorization_number                    as auth_code,
//                    substr(cbm.business_activity_ind,1,3)       as card_program_id,
//                    substr(cbm.business_activity_ind,4,1)       as bsa_type,
//                    substr(cbm.business_activity_ind,5,6)       as bsa_id_code,
//                    substr(cbm.business_activity_ind,11,2)      as ird,
//                    substr(cbm.processing_code,1,2)             as processing_code,
//                    substr(cbm.processing_code,3,2)             as cardholder_from_type,
//                    substr(cbm.processing_code,5,2)             as cardholder_to_type,
//                    cbm.pos_data_code                           as pos_data_code,
//                    cbm.ecomm_security_level_ind                as ecomm_security_level_ind,
//                    cbm.banknet_ref_num                         as auth_banknet_ref_num,
//                    cbm.banknet_date                            as auth_banknet_date,
//                    cba.function_code                           as function_code,
//                    lpad(cba.repr_reason_code,4,'0')            as reason_code,
//                    cbm.class_code                              as sic_code,
//                    cb.merchant_number                          as merchant_number,
//                    nvl(cbm.merchant_name,mf.dba_name)          as dba_name,
//                    nvl(cbm.merchant_city,mf.dmcity)            as dba_city,
//                    nvl(cbm.merchant_state,mf.dmstate)          as dba_state,
//                    mf.dmzip                                    as dba_zip,
//                    cbm.merchant_country                        as country_code,
//                    cbm.mc_ica_num                              as ica_number,
//                    cbm.cb_ref_num                              as cb_ref_num,
//                    cba.document_indicator                      as documentation_ind,
//                    cba.user_message                            as mc_member_message_block,
//                    least((cbm.usage_code+1),3)                 as mc_usage_code,
//                    cba.action_code                             as action_code,
//                    get_cb_represent_date(cb.cb_load_sec)       as batch_date         -- really "cpd"
//            from    network_chargebacks           cb, 
//                    network_chargeback_activity   cba,
//                    chargeback_action_codes       ac,
//                    network_chargeback_mc         cbm,
//                    mif                           mf
//            where   cb.bank_number in (select bank_number from mbs_banks where processor_id = 1)
//                    and cb.incoming_date between (:fileDate-180) and :fileDate
//                    and cb.merchant_number != 0
//                    and not cb.card_number_enc is null
//                    and cba.cb_load_sec = cb.cb_load_sec
//                    and cba.load_filename = :workFilename
//                    and ac.short_action_code = cba.action_code
//                    and nvl(ac.represent,'N') = 'Y'   -- REPR, REMC, REVR only
//                    and cbm.load_sec = cb.cb_load_sec
//                    and mf.merchant_number(+) = cb.merchant_number
//            order by cb.cb_load_sec
//           };
//  ************************************************************

{
	
	
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
	  String  theSqlTS = "select  0                                           as batch_id,          -- not used in chargeback tables\n                  cb.cb_load_sec                              as batch_record_id,   -- really \"cb_load_sec\"\n                  cb.card_number                              as card_number,\n                  dukpt_decrypt_wrapper(cb.card_number_enc)   as card_number_full,\n                  cb.card_number_enc                          as card_number_enc,\n                  cb.tran_date                                as transaction_date,\n                  cbm.transaction_date                        as transaction_time,\n                  cb.reference_number                         as reference_number,\n                  cb.bin_number                               as bin_number,\n                  abs( cb.tran_amount )                       as transaction_amount,\n                  cbm.original_amt                            as original_amount,\n                  cbm.original_currency_code                  as currency_code,\n                  cbm.credit_debit_ind                        as debit_credit_indicator,\n                  cbm.cardholder_act_term_ind                 as cat_indicator,\n                  cbm.authorization_number                    as auth_code,\n                  substr(cbm.business_activity_ind,1,3)       as card_program_id,\n                  substr(cbm.business_activity_ind,4,1)       as bsa_type,\n                  substr(cbm.business_activity_ind,5,6)       as bsa_id_code,\n                  substr(cbm.business_activity_ind,11,2)      as ird,\n                  substr(cbm.processing_code,1,2)             as processing_code,\n                  substr(cbm.processing_code,3,2)             as cardholder_from_type,\n                  substr(cbm.processing_code,5,2)             as cardholder_to_type,\n                  cbm.pos_data_code                           as pos_data_code,\n                  cbm.ecomm_security_level_ind                as ecomm_security_level_ind,\n                  cbm.banknet_ref_num                         as auth_banknet_ref_num,\n                  cbm.banknet_date                            as auth_banknet_date,\n                  cba.function_code                           as function_code,\n                  lpad(cba.repr_reason_code,4,'0')            as reason_code,\n                  cbm.class_code                              as sic_code,\n                  cb.merchant_number                          as merchant_number,\n                  nvl(cbm.merchant_name,mf.dba_name)          as dba_name,\n                  nvl(cbm.merchant_city,mf.dmcity)            as dba_city,\n                  nvl(cbm.merchant_state,mf.dmstate)          as dba_state,\n                  mf.dmzip                                    as dba_zip,\n                  cbm.merchant_country                        as country_code,\n                  cbm.mc_ica_num                              as ica_number,\n                  cbm.cb_ref_num                              as cb_ref_num,\n                  cba.document_indicator                      as documentation_ind,\n                  cba.user_message                            as mc_member_message_block,\n                  least((cbm.usage_code+1),3)                 as mc_usage_code,\n                  cba.action_code                             as action_code,\n                  get_cb_represent_date(cb.cb_load_sec)       as batch_date         -- really \"cpd\"\n          from    network_chargebacks           cb, \n                  network_chargeback_activity   cba,\n                  chargeback_action_codes       ac,\n                  network_chargeback_mc         cbm,\n                  mif                           mf\n          where   cb.bank_number in (select bank_number from mbs_banks where processor_id = 1)\n                  and cb.incoming_date between ( :1 -180) and  :2 \n                  and cb.merchant_number != 0\n                  and not cb.card_number_enc is null\n                  and cba.cb_load_sec = cb.cb_load_sec\n                  and cba.load_filename =  :3 \n                  and ac.short_action_code = cba.action_code\n                  and nvl(ac.represent,'N') = 'Y'   -- REPR, REMC, REVR only\n                  and cbm.load_sec = cb.cb_load_sec\n                  and mf.merchant_number(+) = cb.merchant_number \n          order by cb.cb_load_sec";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.MasterCardChargebackUpload",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,fileDate);
   __sJT_st.setDate(2,fileDate);
   __sJT_st.setString(3,workFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.MasterCardChargebackUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:169^9*/

        FileReferenceId = buildFileReferenceId( workFilename, "2" );
        handleSelectedRecords( it, workFilename );
        addChargebackAdjustmentProcessEntry( workFilename );
        addMbsProcessEntry( ChargeBackConstants.PROC_TYPE_CB_FILE.getIntValue(), workFilename );
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "processTransactions()", e.toString());
      logEntry("processTransactions()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return( true );
  }
  
  

  /**
   * This methd is to update the new file name  mc_mnl_wcbXXXX_MMDDYY_XXX.ipm for the chargeback records based on te.properties configuration
 * @param ICA_Default
 * @param ICA_ALL
 * @param fileDate
 * @param cpd
 */
public void updateICARecordsForManualProcessing(String ICA_Default , String ICA_ALL, Date fileDate, Date cpd) {
	  // TODO Auto-generated method stub
	  String mannualChargeBackFileName=null;
	  try {
		  String exludedICA=MesDefaults.getString("DK_MC_CHBK_MNL_ICA")!=null && !MesDefaults.getString("DK_MC_CHBK_MNL_ICA").isEmpty() ? MesDefaults.getString("DK_MC_CHBK_MNL_ICA") : ICA_Default;

		  
		  //If te.properties file have the configuartion of DK_MC_CHBK_MNL_ICA, then create the new file name.
		  if(! exludedICA.equalsIgnoreCase(ICA_Default))
		  {
			  mannualChargeBackFileName    = generateFilename("mc_mnl_wcb"               // base name
					  + String.valueOf(Integer.parseInt( getEventArg(0) )),         // bank number
					  ".ipm");														  // file extension
		  }
		  
		  if(mannualChargeBackFileName!=null)

		  {
			  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
			  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
			  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());

			  // declare temps
			  String theEXCLUDED_ICA_SqlTS=null;
			  if(!exludedICA.equalsIgnoreCase(ICA_Default) && !exludedICA.contains(ICA_ALL)){
				  theEXCLUDED_ICA_SqlTS = "update  network_chargeback_activity cba "
						  + "\n         set     cba.load_filename =  :1 ,"
						  + "\n                  cba.settlement_date =  :2 "
						  + "\n  where   cba.cb_load_sec in"
						  + "\n        (select  cb.cb_load_sec "
						  + "\n          from    network_chargebacks cb"
						  + "\n          where cb.bank_number in (select bank_number from mbs_banks where processor_id = 1)"
						  + "\n               and cb.incoming_date between ( :3 -180) and  :4  "
						  + "\n               and cb.merchant_number != 0 "
						  + "\n               and not cb.card_number_enc is null"
						  + "\n               and cb.card_type = 'MC'  "
						  + "\n               and cb.cb_load_sec  in (select cbm.load_sec from network_chargeback_mc cbm where cbm.mc_ica_num in ("+exludedICA+" )) "
						  + "\n)"
						  + "\n and cba.load_filename is null"
						  + "\n and cba.action_code = nvl( :5 ,cba.action_code)";

			  }
			  else if(!exludedICA.equalsIgnoreCase(ICA_Default) && exludedICA.contains(ICA_ALL)) {
				  theEXCLUDED_ICA_SqlTS = "update  network_chargeback_activity cba"
						  + "\n          set     cba.load_filename =  :1 ,"
						  + "\n                  cba.settlement_date =  :2 "
						  + "\n          where   cba.cb_load_sec in"
						  + "\n          (select  cb.cb_load_sec"
						  + "\n                   from    network_chargebacks cb"
						  + "\n          where   cb.bank_number in (select bank_number from mbs_banks where processor_id = 1)"
						  + "\n                            and cb.incoming_date between ( :3 -180) and  :4  "
						  + "\n                            and cb.merchant_number != 0"
						  + "\n                           and not cb.card_number_enc is null"
						  + "\n                            and cb.card_type = 'MC'  "
						  + "\n   and cb.cb_load_sec  in (select cbm.load_sec from network_chargeback_mc cbm where cbm.mc_ica_num is not null ))"
						  + "\n   and cba.load_filename is null"
						  + "\n   and cba.action_code = nvl( :5 ,cba.action_code)";
			  }


			  __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.MasterCardChargebackUpload",theEXCLUDED_ICA_SqlTS);
			  __sJT_st.setString(1,mannualChargeBackFileName);
			  __sJT_st.setDate(2,cpd);
			  __sJT_st.setDate(3,fileDate);
			  __sJT_st.setDate(4,fileDate);
			  __sJT_st.setString(5,ActionCode);
			  // execute statement
			  __sJT_ec.oracleExecuteBatchableUpdate();
			  int mannualRecCount = Ctx.getExecutionContext().getUpdateCount();
			  if(mannualRecCount>0)
			  {
				  //updating the table ach_trident_process and mbs_process with the new generated file name
				  	addChargebackAdjustmentProcessEntry( mannualChargeBackFileName );
			        addMbsProcessEntry( ChargeBackConstants.PROC_TYPE_CB_FILE.getIntValue(), mannualChargeBackFileName );
			  }
		  } 
	  }catch( Exception e )
	  {
		  logEvent(this.getClass().getName(), "processTransactions()", e.toString());
		  logEntry("processTransactions()", e.toString());
	  }
	  // set IN parameters

  }



public static void main( String[] args )
  {
    MasterCardChargebackUpload        test          = null;
    
    try
    {
        if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{
                    MesDefaults.DK_MC_EP_HOST,
            MesDefaults.DK_MC_EP_USER,
            MesDefaults.DK_MC_EP_PASSWORD,
            MesDefaults.DK_MC_EP_OUTGOING_PATH
            });
        }

      test = new MasterCardChargebackUpload();
      test.executeCommandLine( args );
    }
    catch( Exception e )
    {
      log.debug(e.toString());
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}/*@lineinfo:generated-code*/