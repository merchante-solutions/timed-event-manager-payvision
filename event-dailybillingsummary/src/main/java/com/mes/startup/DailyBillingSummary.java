/*@lineinfo:filename=DailyBillingSummary*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/DailyBillingSummary.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $LastChangedDate: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Vector;
import com.mes.constants.mesConstants;
import com.mes.database.SQLJConnectionBase;
import com.mes.mbs.BillingDb;
import com.mes.mbs.BillingElement;
import com.mes.mbs.BillingGroup;
import com.mes.mbs.BillingSummaryTask;
import com.mes.mbs.MerchantBilling;
import com.mes.mbs.MerchantBillingElement;
import com.mes.mbs.SimpleSummaryData;
import com.mes.support.DateTimeFormatter;

public class DailyBillingSummary extends EventBase
{
  public static final int       PROC_TYPE_DAILY_ACTIVITY      = 0;
  public static final int       PROC_TYPE_AUTH_FILE           = 1;
  public static final int       PROC_TYPE_CB_FILE             = 2;
  public static final int       PROC_TYPE_MONTH_END_ACTIVITY  = 3;
  public static final int       PROC_TYPE_DDF_DISC            = 4;
  public static final int       PROC_TYPE_ACH                 = 5;
  
  private final static int[] ActiveBanks = 
  {
    mesConstants.BANK_ID_MES,
  };
  
  protected Date          FileActivityDate      = null;
  private   long          MeLoadFileId          = 0L;
  private   String        MeLoadFilename        = null;
  protected Date          TestFileDate          = null;
  
  public DailyBillingSummary( )
  {
    PropertiesFilename = "daily-billing.properties";
  }
  
  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager.
  */
  public boolean execute()
  {
    int                             billingType   = -1;
    ProcessTable.ProcessTableEntry  entry         = null;
    boolean                         retVal        = false;
    ProcessTable                    pt            = null;
  
    try
    {
      // get an automated timeout exempt connection
      connect(true);
      
      billingType = Integer.parseInt(getEventArg(0));
      
      switch( billingType )
      {
        case BillingSummaryTask.BT_FILE_ACTIVITY:
          pt = new ProcessTable("mbs_process","mbs_process_sequence",ProcessTable.PT_ALL,true);
      
          if ( pt.hasPendingEntries() )
          {
            pt.preparePendingEntries();
        
            Vector entries = pt.getEntryVector();
            for( int i = 0; i < entries.size(); ++i )
            {
              entry = (ProcessTable.ProcessTableEntry)entries.elementAt(i);
              entry.recordTimestampBegin();
              storeBillingData( billingType, entry.getProcessType(), entry.getLoadFilename(), entry.getNodeId() );
              entry.recordTimestampEnd();
            }
          }
          break;
          
        case BillingSummaryTask.BT_DAILY_ACTIVITY:
          int procType = Integer.parseInt(getEventArg(1));
          if ( TestFileDate == null )
          {
            /*@lineinfo:generated-code*//*@lineinfo:104^13*/

//  ************************************************************
//  #sql [Ctx] { select  trunc(sysdate)-1 
//                from    dual
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  trunc(sysdate)-1  \n              from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.DailyBillingSummary",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   FileActivityDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:108^13*/
          }
          else
          {
            FileActivityDate = TestFileDate;
          }
          storeBillingData(procType,FileActivityDate);
          break;
      }
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( true );
  }
  
  protected boolean isDdfProcType( int procType )
  {
    return( procType == PROC_TYPE_DDF_DISC );
  }            
  
  public void setMeLoadFilename( String loadFilename ) 
  {
    MeLoadFileId    = (loadFilename == null ? 0L : loadFilenameToLoadFileId(loadFilename));
    MeLoadFilename  = loadFilename;
  }
  
  public void setTestFileDate( Date activityDate )
  {
    TestFileDate = activityDate;
  }
  
  protected void storeBillingData( int procType, Date activityDate )
  {
    FileActivityDate = activityDate;
    storeBillingData( BillingSummaryTask.BT_DAILY_ACTIVITY, procType, generateFilename("mbs_daily3941",FileActivityDate,1), 0L );
  }
  
  protected void storeBillingData( int billingType, int procType, String loadFilename )
  {
    storeBillingData(billingType,procType,loadFilename,0L);
  }
  
  protected void storeBillingData( int billingType, int procType, String loadFilename, long nodeId )
  {
    BillingDb           billingDb       = null;
    BillingGroup        billingGroup    = null;
    Vector              billingGroups   = new Vector();
    String              classname       = null;
    MerchantBilling     mb              = null;
    long                merchantId      = 0L;
    PreparedStatement   ps              = null;
    ResultSet           rs              = null;
    SimpleSummaryData   summaryData     = new SimpleSummaryData();
    HashMap             merchantIds     = null;

    try
    {
      billingDb = new BillingDb();
      billingDb.connect(true);
      
      long loadFileId = loadFilenameToLoadFileId(loadFilename);
    
      billingDb._clearBillingData(loadFileId,nodeId);
      
      if ( nodeId != 0L )
      {
        merchantIds = billingDb._loadMerchantIds(nodeId);
      }
      
      // load all the billing elements that use this type of data.
      // elements will be grouped by query to allow multiple elements
      // to be captured using a single SQL select statement
      billingGroups = billingDb._loadBillingGroups(procType);
      
      for( int i = 0; i < billingGroups.size(); ++i )
      {
        billingGroup  = (BillingGroup)billingGroups.elementAt(i);
        classname     = billingGroup.getBillingClassname();
        
        if ( classname != null )
        { 
          // perform complex data loading
          Class c = Class.forName(classname);
          BillingSummaryTask task = (BillingSummaryTask)c.newInstance();
          if ( !task.doTask(loadFilename,FileActivityDate) )
          {
            // a log entry in here will post an email 
            // to the administrator(s).
            logEntry("" + classname + " doTask() failed",
                     "doTask(" + loadFilename + "," + FileActivityDate + ") - " + task.getErrorDesc());
          }
        }
        else    // simple SQL based data loading
        {
          ps = getPreparedStatement(billingGroup.getSqlText());
          if ( billingType == BillingSummaryTask.BT_FILE_ACTIVITY )
          {
            // daily detail file (ddf) has an index on load_filename
            if( isDdfProcType(procType) )
            {
              ps.setString(1,loadFilename);
              if(billingGroup.getQueryId() == 114) {
            	  ps.setString(2,loadFilename);
              }
            }
            else  // most tables have an index on load_file_id
            {
              ps.setLong(1,loadFileId);
              if(billingGroup.getQueryId() == 223) {
            	  ps.setLong(2,loadFileId);
              }
            }
          }
          else if ( billingType == BillingSummaryTask.BT_DAILY_ACTIVITY )
          {
            ps.setDate(1,FileActivityDate);
            ps.setDate(2,FileActivityDate);
            ps.setDate(3,FileActivityDate);
          }
          rs = ps.executeQuery();
          
          long lastMerchantId = -1;
          while( rs.next() )
          {
            Vector  billingElements = billingGroup.getBillingElements();
            Date    lastActivityDate = null;
            boolean loadMerchBilling;
            
            // for each billing element associated with the query id
            // process the billing information and store in the summary table
            for ( int j = 0; j < billingElements.size(); ++j )
            {
              BillingElement billingElement = (BillingElement)billingElements.elementAt(j);

              loadMerchBilling = false;
              merchantId       = rs.getLong("merchant_number");
              
              if ( nodeId != 0L && merchantIds.get(String.valueOf(merchantId)) == null ) { continue; }
              
              // extract the count/amount data from the result set
              summaryData.clear();
              summaryData.setActivityDate( rs.getDate("activity_date") );
              summaryData.setDataSource( loadFilename );
              summaryData.setDataSourceId( loadFileId );
              summaryData.setMeLoadFilename( MeLoadFilename );
              summaryData.setMeLoadFileId( MeLoadFileId );
              summaryData.setItemSubclass( rs.getString("item_subclass") );
              summaryData.setItemType( billingElement.getItemType() );
              summaryData.setMerchantId( merchantId );

              if( isDdfProcType(procType) )
              {
                summaryData.setSalesAmount( rs.getDouble("sales_amount") );
                summaryData.setSalesCount( rs.getInt("sales_count") );
                summaryData.setCreditsAmount( rs.getDouble("credits_amount") );
                summaryData.setCreditsCount( rs.getInt("credits_count") );
                if( "DISC".equals(billingElement.getItemCategory()) )         // if "discount"
                {
                  summaryData.setRate( rs.getDouble("discount_rate") );
                  summaryData.setPerItem( rs.getDouble("discount_per_item") );
                }
                else if( "IC".equals(billingElement.getItemCategory()) )      // if "interchange billing"
                {
                  summaryData.setIcCat( rs.getString("ic_cat") );
                  summaryData.setExpense( rs.getDouble("expense") );
                  summaryData.setExpenseActual( rs.getDouble("expense_actual") );
                }
                else  // other ddf data (eg debit,ebt,plan,assoc)
                {
                  summaryData.setItemAmount( rs.getDouble(billingElement.getAmountColumnName()) );
                  summaryData.setItemCount( rs.getInt(billingElement.getCountColumnName()) );
                  loadMerchBilling = true;
                }
              }
              else
              {
                summaryData.setItemAmount( rs.getDouble(billingElement.getAmountColumnName()) );
                summaryData.setItemCount( rs.getInt(billingElement.getCountColumnName()) );
              
                // calculate the fees due for this billing element
                if ( billingElement.getItemType() == 5  ||    // chargeback
                     summaryData.getItemCount() != 0    || 
                     summaryData.getItemAmount() != 0.0 )
                {
                  loadMerchBilling = true;
                }
              }

              // some elements (e.g. discount & interchange) have been
              // calculated by other processes because they are needed 
              // to generate the deposit (ACH) entries.  It is only
              // necessary to load the merchant pricing data for elements
              // where the fees due has not already been calculated
              if( loadMerchBilling )
              {
                // change in date or merchant, need to reload pricing
                if ( summaryData.getMerchantId() != lastMerchantId || 
                     !summaryData.getActivityDate().equals(lastActivityDate) )
                {
                  lastMerchantId    = summaryData.getMerchantId();
                  lastActivityDate  = summaryData.getActivityDate();

                  mb = billingDb._loadMerchantBilling(lastMerchantId,lastActivityDate);
                }
              
                MerchantBillingElement el = mb.findItemByType(summaryData.getItemType(),
                                                              summaryData.getItemSubclass());
                if ( el != null )
                {
                  summaryData.setPerItem( el.getPerItem() );
                  summaryData.setRate   ( el.getRate()    );
                  summaryData.setFeesDue( (summaryData.getItemCount() * el.getPerItem()) + 
                                          (summaryData.getItemAmount() * el.getRate() * 0.01) );
                }
                billingDb._storeSimpleSummaryData(summaryData);
              }
              else if( isDdfProcType(procType) )
              {
                summaryData.setFeesDue( rs.getDouble("fees_due") );
                summaryData.setFeesPaid( rs.getDouble("fees_paid") );

                // always store (would not be here if sales & credits were both zero)
                billingDb._storeSimpleSummaryData(summaryData);
              }
            }   // for loop
          }
          rs.close();
          ps.close();
        }
      }
    }
    catch( Exception e )
    {
      logEntry("storeBillingData(" + procType + "," + loadFilename + ")",e.toString());
      e.printStackTrace();
    }
    finally
    {
      try{ billingDb.cleanUp(); } catch (Exception ee){}
    }
  }
  
  public static void main( String[] args )
  {
    DailyBillingSummary        test          = null;
    
    try
    {
      SQLJConnectionBase.initStandalone();
      
      test = new DailyBillingSummary();
      test.connect(true);
      
      // extract test params
      int       billingType     = Integer.parseInt(args[0]);
      int       procType        = Integer.parseInt(args[1]);
      
      if ( billingType == BillingSummaryTask.BT_FILE_ACTIVITY )
      {
        String loadFilename = args[2];
        
        if ( "execute".equals(loadFilename) )
        {
          System.out.println("Executing File Process");
          System.out.println("  ProcessType   : " + procType);
          test.setEventArgs(""+billingType);
          test.execute();
        }
        else
        {
          String meLoadFilename = (args.length > 3 ? args[3] : null);
          System.out.println("Storing File Activity Data");
          System.out.println("  ProcessType   : " + procType);
          System.out.println("  LoadFilename  : " + loadFilename);
          System.out.println("  MeLoadFilename: " + (meLoadFilename == null ? "" : meLoadFilename));
          test.setMeLoadFilename(meLoadFilename);
          test.storeBillingData( billingType, procType, loadFilename );
        }
      }
      else if ( billingType == BillingSummaryTask.BT_DAILY_ACTIVITY )
      {
        Date sqlDate = new Date( DateTimeFormatter.parseDate(args[2],"MM/dd/yyyy").getTime() );
        
        System.out.println("Storing Daily Activity Data");
        System.out.println("  Billing Date  : " + sqlDate);
        test.storeBillingData( procType, sqlDate );
      }
      else
      {
        throw new Exception( "Invalid billing type " + billingType );
      }
      
    }
    catch( Exception e )
    {
      System.out.println(e.toString());
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception ee ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/