/*************************************************************************

  FILE: $URL$

  Description:

  Last Modified By   : $Author$
  Last Modified Date : $LastChangedDate$
  Version            : $Revision$

  Change History:
     See SVN database

  Copyright (C) 2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.mbs;

public abstract class BillingSummaryTask
{
  public static final int   BT_DAILY_ACTIVITY       = 1;
  public static final int   BT_FILE_ACTIVITY        = 2;

  protected   String      ErrorDescription      = null;
  
  public abstract boolean doTask( String loadFilename, java.sql.Date activityDate );
  
  public boolean hasError()
  {
    return( ErrorDescription != null && ErrorDescription.trim().length() > 0 );
  }
  
  public String getErrorDesc()
  {
    return( ((ErrorDescription == null) ? "" : ErrorDescription) );
  }
  
  public void setErrorDesc( String value )
  {
    ErrorDescription = value;
  }
}
