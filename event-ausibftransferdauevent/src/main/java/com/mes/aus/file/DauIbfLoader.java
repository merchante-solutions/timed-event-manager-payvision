package com.mes.aus.file;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.aus.InboundFile;
import com.mes.aus.SysUtil;

public class DauIbfLoader extends AutomatedProcess {
	static Logger log = Logger.getLogger(DauIbfLoader.class);

	private InputStream in;
	private String fileType;
	private String sourceName;
	private long obId;
	private boolean testFlag;

	public DauIbfLoader(InputStream in, String sourceName, boolean directFlag, boolean testFlag) {
		super(directFlag);
		this.in = in;
		this.sourceName = sourceName;
		this.testFlag = testFlag;
	}

	/**
	 * Determine file type and extract obId. Generate file name.
	 */
	private void inspectFileData(InboundFile ibf) throws Exception {
		// file type determined from source file name
		if (sourceName.indexOf("ACCTRESP") != -1) {
			ibf.setFileType(SysUtil.FT_DAU_RSP);
			ibf.setFileName("dau-rsp-" + ibf.getIbId() + ".txt");

			// try to locate obId in response file
			InputStream in = ibf.getInputStream();
			LineNumberReader r = new LineNumberReader(new InputStreamReader(in));
			try {
				// find first detail
				String line = r.readLine();
				while (line != null && !line.startsWith("05")) {
					line = r.readLine();
				}

				// extract obId from detail record
				if (line != null) {
					String obIdStr = line.substring(98, 110).trim();
					log.debug("found obIdStr: " + obIdStr);
					try {
						ibf.setObId(Long.parseLong(obIdStr));
					}
					catch (Exception e) {
						log.error("Unable to parse obfId from '" + obIdStr + "'");
					}
				}
			}
			finally {
				try {
					r.close();
				}
				catch (Exception e) {}
				try {
					in.close();
				}
				catch (Exception e) {}
			}
		}
		else if (sourceName.indexOf("ACCTMAAF") != -1) {
			ibf.setFileType(SysUtil.FT_DAU_RPT1);
			ibf.setFileName("dau-rpt1-" + ibf.getIbId() + ".txt");
		}
	}

	private InboundFile upload() {
		try {
			InboundFile ibf = new InboundFile();
			ibf.setIbId(db.getNewId());
			ibf.setCreateDate(Calendar.getInstance().getTime());
			ibf.setSysCode(SysUtil.SYS_DAU);
			ibf.setTestFlag(testFlag ? ibf.FLAG_YES : ibf.FLAG_NO);
			ibf.setData(in);

			// genrate file name based on file type,
			// extract outbound file id from file
			inspectFileData(ibf);
			db.insertInboundFile(ibf, true);
			return ibf;
		}
		catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "upload(source=" + sourceName + ")");
		}
		finally {
			try {
				in.close();
			}
			catch (Exception e) {}
		}
		return null;
	}

	public static InboundFile uploadFile(InputStream in, String sourceName,
			boolean directFlag, boolean testFlag) {
		return (new DauIbfLoader(in, sourceName, directFlag, testFlag)).upload();
	}
	public static InboundFile uploadFile(InputStream in, String sourceName, boolean testFlag) {
		return uploadFile(in, sourceName, true, testFlag);
	}
}