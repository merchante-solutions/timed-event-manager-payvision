package com.mes.aus.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.jscape.inet.sftp.SftpFile;
import com.jscape.inet.ssh.util.SshParameters;
import com.mes.aus.InboundFile;
import com.mes.aus.Notifier;
import com.mes.config.MesDefaults;
import com.mes.support.PropertiesFile;
import com.mes.support.config.CryptCalloutConfig;

public class DauIbfXfer extends AutomatedProcess {
	static Logger log = Logger.getLogger(DauIbfXfer.class);
	private boolean testFlag;
	protected PropertiesFile  EventProps = null;
	public  static final int  ZIP_BUFFER_LEN      = 4096;

	public DauIbfXfer(boolean directFlag, boolean testFlag) {
		super(directFlag);
		this.testFlag = testFlag;
	}
	/**
	 * Write input stream to a file.
	 */
	private void writeToFile(InputStream in, File f) throws Exception {
		OutputStream out = null;

		try {
			out = new FileOutputStream(f);
			byte[] inBuf = new byte[5000];
			int readCount = 0;
			while ((readCount = in.read(inBuf)) != -1) {
				out.write(inBuf, 0, readCount);
			}
		}
		finally {
			try {
				out.flush();
			}
			catch (Exception e) {}
			try {
				out.close();
			}
			catch (Exception e) {}
		}
	}
  
  public void decryptFile(File pgpFile, File clearFile) throws Exception {
  
    // load the command line string array to invoke a call out to the
    // system to decrypt a file
		CryptCalloutConfig cfg = new CryptCalloutConfig();
		String[] cmd = 
      cfg.getDecryptRuntimeStrs("aus-discover", "" + pgpFile.getAbsolutePath(), "" + clearFile.getAbsolutePath());
    StringBuffer buf = new StringBuffer("Decrypt command elements:");
    for (int i = 0; i < cmd.length; ++i) {
      buf.append(" " + cmd[i]);
    }
    log.debug(""+buf);
    
    // generate a process to run the decrypt command
    // with stderr combined with stdout
    ProcessBuilder procBuilder = new ProcessBuilder(cmd);
    procBuilder.redirectErrorStream(true); // stderr > stdout
    Process process = procBuilder.start();
    
    // capture and log output from the process as it runs
    buf = new StringBuffer();
    BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
    String line;
    while ((line = in.readLine()) != null) {
      buf.append((buf.length() > 0 ? "\n" : "") + line);
    }
		process.waitFor();
    
		log.debug("Encryption command completed with output:\n" + buf);
  } 

	public void transfer() {
		try {
			log.debug("Initiating DAU inbound file transfers...");

			StringBuffer noticeText = new StringBuffer("DAU inbound file transfer activity:\n\n");
			int fileCount = 0;
			int errorCount = 0;
			File pgpFile = null;
			File clearFile = null;

			// establish sftp connection with vau endpoint
			SshParameters sshParms = new SshParameters(
					db.getDauEndpointHost(testFlag), db.getDauIbEndpointUser(),
					db.getDauIbEndpointPassword());

			Sftp sftp = new Sftp(sshParms);
			
			sftp.connect();
			
			sftp.setDir(db.getDauIbXferLoc());
			// sftp.setBinary();

			String fileMask = db.getDauIbXferFileMask();
			SimpleDateFormat sfm = new SimpleDateFormat("yyyyDDD");
	    	  String fileMaskDate = sfm.format(new java.util.Date()); 
	    	  
			fileMask = fileMask + "."+"*"+".J" + fileMaskDate +".*";
			
			log.debug("using file mask: " + fileMask);
			List fileList = Collections.list(sftp.getDirListing(fileMask));
			log.debug("sftp.getDirListing() returned " + fileList.size() + " items");

			for (Iterator i = fileList.iterator(); i.hasNext();) {
				SftpFile sf = (SftpFile) i.next();
				log.debug("found file: " + sf.getFilename());

				try {
					// download pgp encrypted file from dau
					pgpFile = new File(sf.getFilename() + ".pgp");
					// writeToFile(sftp.getInputStream(sf.getFilename(),0),pgpFile);
					log.debug("downloading " + sf.getFilename());
					sftp.download("" + pgpFile, sf.getFilename());

					// issue external command to decrypt file
					clearFile = new File(sf.getFilename());
					decryptFile(pgpFile,clearFile);

					// upload to db
					InboundFile ibf = DauIbfLoader.uploadFile(new FileInputStream(clearFile), "" + clearFile, directFlag, testFlag);
					archiveDataFile(clearFile.getName());
					
					++fileCount;

					noticeText.append("  " + ibf.getFileName() + "(" + clearFile + ")\n");
				}
				catch (Exception ie) {
					log.error("Error: " + ie);
					notifyError(ie, "transfer(" + sf.getFilename() + ")");
					++errorCount;
				}
			}

			// disconnect
			log.debug("disconnecting");
			sftp.disconnect();

			// notify developer inbound files transferred
			if (fileList.size() > 0) {
				noticeText.append("\n" + fileList.size() + " inbound files\n");
				noticeText.append("" + fileCount + " files successfully transferred\n");
				noticeText.append("" + errorCount + " errors");
				Notifier.notifyDeveloper("DAU Inbound File Transfer", "" + noticeText, directFlag);
			}
			else {
				log.debug("No DAU files found.");
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "transfer()");
		}
	}
	
	protected boolean archiveDataFile(String dataFilename)
	{
		String archiveHost = null;
		String archivePass = null;
		String archiveUser = null;
		int bytesRead = 0;
		String flagFilename = null;
		int offset = -1;
		String progress = "";
		boolean success = false;
		byte[] zipData = new byte[ZIP_BUFFER_LEN];
		String zipFilename = null;
		Sftp sftp = null;

		try {

				zipFilename = dataFilename + ".zip";

			// create the archive .zip file
			progress = "preparing files for archive";
			ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFilename));
			zos.setMethod(ZipOutputStream.DEFLATED);

			// zip up the .dat file
			FileInputStream zipInFile = new FileInputStream(dataFilename);
			ZipEntry dataZipEntry = new ZipEntry(dataFilename);
			dataZipEntry.setMethod(ZipEntry.DEFLATED);
			dataZipEntry.setComment("Incoming file for AUS");

			zos.putNextEntry(dataZipEntry);
			while (bytesRead != -1) {
				bytesRead = zipInFile.read(zipData);

				if (bytesRead != -1) {
					// write these bytes to the zip file
					zos.write(zipData, 0, bytesRead);
				}
			}
			zos.closeEntry();
			zipInFile.close();

			// close the zip file
			zos.close();

			if (EventProps != null) // have a timed event properties file
			{
				archiveHost = EventProps.getString("com.mes.kirin.host",
						MesDefaults.getString(MesDefaults.DK_ARCHIVE_HOST));
				archiveUser = EventProps.getString("com.mes.kirin.user",
						MesDefaults.getString(MesDefaults.DK_ARCHIVE_USER));
				archivePass = EventProps.getString("com.mes.kirin.password",
						MesDefaults.getString(MesDefaults.DK_ARCHIVE_PASS));
			} else // use MesDefaults values
			{
				archiveHost = MesDefaults.getString(MesDefaults.DK_ARCHIVE_HOST);
				archiveUser = MesDefaults.getString(MesDefaults.DK_ARCHIVE_USER);
				archivePass = MesDefaults.getString(MesDefaults.DK_ARCHIVE_PASS);
			}

			SshParameters params = new SshParameters(archiveHost, archiveUser, archivePass);
			String archPath = MesDefaults.getString(MesDefaults.DK_ARCHIVE_PATH_DAILY);

			sftp = new Sftp(params);

			progress = "connecting to ftp server";
			sftp.connect();

			if (archPath != null && archPath.length() > 0) {
				progress = "changing directory to " + archPath;
				sftp.setDir(archPath);
			}
			
			progress = "archiving files";
			
			if ( dataFilename.getBytes() != null )
            {
              sftp.upload( zipFilename , dataFilename );
            }
            else
            {
              sftp.upload( dataFilename , dataFilename );
            }

			new File(dataFilename).delete();
			new File(zipFilename).delete();

			success = true;
		} catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "archiveDataFile()");
		} finally {
		}
		return (success);
	}
}