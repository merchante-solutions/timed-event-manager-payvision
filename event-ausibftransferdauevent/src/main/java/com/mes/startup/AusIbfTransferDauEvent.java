package com.mes.startup;

import org.apache.log4j.Logger;
import com.mes.aus.file.DauIbfXfer;

public final class AusIbfTransferDauEvent extends EventBase
{
  static Logger log = Logger.getLogger(AusIbfTransferDauEvent.class);

  public boolean execute()
  {
    try
    {
      (new DauIbfXfer(true,false)).transfer();
      return true;
    }
    catch (Exception e)
    {
      log.error("AUS request file process event error: " + e);
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }

    return false;
  }
}