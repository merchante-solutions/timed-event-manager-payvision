/*@lineinfo:filename=NetSuiteMonthEndUpload*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.support.DateTimeFormatter;
import com.netsuite.webservices.platform.core_2019_1.CustomFieldList;
import com.netsuite.webservices.platform.core_2019_1.CustomFieldRef;
import com.netsuite.webservices.platform.core_2019_1.LongCustomFieldRef;
import com.netsuite.webservices.platform.core_2019_1.Record;
import com.netsuite.webservices.platform.core_2019_1.RecordRef;
import com.netsuite.webservices.platform.core_2019_1.StringCustomFieldRef;
import com.netsuite.webservices.platform.core_2019_1.types.RecordType;
import com.netsuite.webservices.platform.messages_2019_1.WriteResponse;
import com.netsuite.webservices.transactions.general_2019_1.JournalEntry;
import com.netsuite.webservices.transactions.general_2019_1.JournalEntryLine;
import com.netsuite.webservices.transactions.general_2019_1.JournalEntryLineList;
import com.netsuite.webservices.transactions.sales_2019_1.Invoice;
import com.netsuite.webservices.transactions.sales_2019_1.InvoiceItem;
import com.netsuite.webservices.transactions.sales_2019_1.InvoiceItemList;
import sqlj.runtime.ResultSetIterator;

public class NetSuiteMonthEndUpload extends NetSuiteEventBase
{
  static Logger log = Logger.getLogger(NetSuiteMonthEndUpload.class);
  
  public static final String[][] INVOICE_LINE_ITEMS =
  {                                      // Description
    { "17"   , "discount_rev" },         // 410020 Other Discount Income
    { "18"   , "ic_pass_thru_rev" },     // 410030 I/C Pass Thru Direct West
    { "20"   , "debit_rev" },            // 410050 Debit Fees-Other Direct West
    { "19"   , "sys_gen_rev" },          // 410080 System Generated Income
    { "21"   , "auth_rev" },             // 410090 Auth Pass Thru Direct West
  };
  
  public static final String[][] JOURNAL_ENTRY_LINE_ITEMS =
  {                                      // Description
    { "802"  , "vmc_ic_exp" },           // 510010 Interchange VISA
    { "805"  , "amex_ic_exp" },          // 510040 Interchange Amex Card
    { "806"  , "uatp_ic_exp" },          // 510050 Interchange UATP Card
    { "808"  , "amex_disc_exp" },        // 510070 Amex Discount/IC
    { "809"  , "disc_ic_exp" },          // 510080 MBS Discover Interchange expense
    { "810"  , "visa_asmt_exp" },        // 520010 Visa Assessments
    { "811"  , "mc_asmt_exp" },          // 520020 MC Assessment Direct West
    { "812"  , "ds_asmt_exp" },          // 520030 DS Assessment Direct West
    { "813"  , "fanf_fees_exp" },        // 520030 FANF Visa Fees
    { "802"  , "ic_repair_exp" },        // 510010 Interchange VISA
    { "4362" , "clearing_monthly" },     // 9992 Clearing - Monthly Activity
  };
  
  public static final String[][] JE_LINE_CUSTOM_FIELDS =
  {
    { "777"  , "custcol_transaction_volume" },   // ME Transaction Volume
    { "778"  , "custcol_sales_volume" },         // ME Sales Volume
  };
  
  public Vector          JERows           = new Vector();
  public Vector          IVRows           = new Vector();
  
  private static final String SQL_GET_JOURNAL_ENTRY_AND_INVOICE = 
				   "SELECT sm.active_date                                         AS active_date, "
				  + "       gn.hh_bank_number                                      AS bank_number, "
				  + "       o.org_name                                             AS portfolio_name, "
				  + "       nsp.internal_id                                        AS portfolio_id, "
				  + "       SUM(gn.t1_tot_inc_ind_plans)                           AS individual_plan, "
				  + "       SUM(gn.t1_tot_calculated_discount)                     AS disc_billed_to_merch, "
				  + "       SUM(gn.t1_tot_inc_interchange)                         AS interchange_passthru, "
				  + "       SUM(gn.t1_tot_inc_sys_generated)                       AS system_gen, "
				  + "       SUM(gn.t1_tot_inc_debit_networks)                      AS debit_inc, "
				  + "       SUM(gn.t1_tot_inc_authorization)                       AS auth_inc, "
				  + "       SUM(gn.t1_tot_inc_capture)                             AS capture_inc, "
				  + "       SUM(sm.visa_interchange)                               AS visa_ic, "
				  + "       SUM(sm.mc_interchange)                                 AS mc_ic, "
				  + "       SUM(sm.visa_interchange) + SUM(sm.mc_interchange) + SUM( CASE WHEN sm.load_filename LIKE "
				  + "       'mbs_ext%' THEN "
				  + "       (Nvl(sm.interchange_expense, 0) - "
				  + "       Nvl(sm.interchange_expense_enhanced, 0)) ELSE "
				  + "       0 END)                                                 AS vmc_ic, "
				  + "       SUM(Nvl(sm.mc_assessment, 0) - Nvl(sm.mc_fees, 0))     AS mc_assessment, "
				  + "       SUM(Nvl(sm.visa_assessment, 0) - Nvl(sm.visa_fees, 0)) AS visa_assessment , "
				  + "       SUM(Nvl(sm.disc_assessment, 0))                        AS disc_assessment, "
				  + "       SUM(Nvl(sm.amex_interchange, 0))                       AS amex_ic, "
				  + "       SUM(Nvl(sm.disc_interchange, 0))                       AS disc_ic, "
				  + "       SUM(Decode(o.org_group, 3941400059, "
				  + "           Round(Nvl(sm.dinr_sales_amount, 0) * 0.024, 2), "
				  + "                               0))                            AS diners_ic, "
				  + "       SUM(Decode(o.org_group, 3941400059, "
				  + "           Round(Nvl(sm.pl1_sales_amount, 0) * 0.0197, 2), "
				  + "                               0))                            AS uatp_ic, "
				  + "       -1 * SUM(CASE "
				  + "                  WHEN sm.load_filename LIKE 'mbs_ext%' THEN ( "
				  + "                  Nvl(sm.interchange_expense, 0) - "
				  + "                  Nvl(sm.interchange_expense_enhanced, 0) ) "
				  + "                  ELSE 0 "
				  + "                END)                                          AS ic_repair, "
				  + "       SUM(Nvl(fanf.fee, 0))                                  AS fanf_fees, "
				  + "       SUM(gn.t1_tot_amount_of_sales)                         AS total_sales, "
				  + "       SUM(sm.vmc_cash_advance_count "
				  + "           + sm.vmc_sales_count + sm.vmc_credits_count "
				  + "           + sm.amex_ic_sales_count "
				  + "           + sm.amex_ic_credits_count "
				  + "           + sm.disc_ic_sales_count "
				  + "           + Nvl(sm.debit_credits_count, 0) "
				  + "           + Nvl(sm.debit_sales_count, 0) "
				  + "           + Nvl(sm.ebt_credits_count, 0) "
				  + "           + Nvl(sm.ebt_sales_count, 0))                      AS tran_count "
				  + "FROM   monthly_extract_gn gn, "
				  + "       monthly_extract_summary sm, "
				  + "       mif mf, "
				  + "       GROUPS g, "
				  + "       channel_link cl, "
				  + "       netsuite_portfolio nsp, "
				  + "       organization o, "
				  + "       (SELECT vf.merchant_number, "
				  + "               vf.active_date, "
				  + "               SUM(Nvl(vf.fee, 0)) fee "
				  + "        FROM   visa_fanf_summary vf "
				  + "        WHERE  vf.merchant_number IN (SELECT merchant_number "
				  + "                                      FROM   mif "
				  + "                                      WHERE  bank_number IN ( 3941, 3943, 3858, "
				  + "                                                              3003 )) "
				  + "               AND vf.active_date = :1 "
				  + "        GROUP  BY vf.merchant_number, "
				  + "                  vf.active_date) fanf "
				  + "WHERE  gn.hh_bank_number IN ( 3941, 3943, 3858, 3003 ) "
				  + "       AND gn.hh_active_date = :2 "
				  + "       AND sm.merchant_number = gn.hh_merchant_number "
				  + "       AND gn.hh_merchant_number = fanf.merchant_number(+) "
				  + "       AND fanf.active_date(+) = gn.hh_active_date "
				  + "       AND sm.active_date = gn.hh_active_date "
				  + "       AND mf.merchant_number = sm.merchant_number "
				  + "       AND g.assoc_number = mf.association_node "
				  + "       AND o.org_group = cl.hierarchy_node(+) "
				  + "       AND o.org_group = g.group_2 "
				  + "       AND nsp.org_num(+) = o.org_group "
				  + "GROUP  BY gn.hh_bank_number, "
				  + "          sm.active_date, "
				  + "          o.org_name, "
				  + "          nsp.internal_id "
				  + "ORDER  BY gn.hh_bank_number, "
				  + "          nsp.internal_id";	

  private static final String SQL_GET_JOURNAL_ENTRY = 
				   "SELECT sm.active_date                                                     AS active_date, "
				  + "       -1 * SUM(gn.t1_tot_inc_ind_plans)                                  AS individual_plan, "
				  + "       -1 * SUM(gn.t1_tot_calculated_discount)                            AS disc_billed_to_merch, "
				  + "       -1 * SUM(gn.t1_tot_inc_interchange)                                AS interchange_passthru, "
				  + "       -1 * SUM(gn.t1_tot_inc_sys_generated)                              AS system_gen, "
				  + "       -1 * SUM(gn.t1_tot_inc_debit_networks)                             AS debit_inc, "
				  + "       -1 * SUM(gn.t1_tot_inc_authorization)                              AS auth_inc, "
				  + "       -1 * SUM(gn.t1_tot_inc_capture)                                    AS capture_inc, "
				  + "       -1 * SUM(sm.visa_interchange)                                      AS visa_ic, "
				  + "       -1 * SUM(sm.mc_interchange)                                        AS mc_ic, "
				  + "       -1 * ( SUM(sm.visa_interchange) "
				  + "              + SUM(sm.mc_interchange) + SUM( CASE WHEN sm.load_filename LIKE "
				  + "              'mbs_ext%' "
				  + "                   THEN (Nvl(sm.interchange_expense, 0) - "
				  + "                   Nvl(sm.interchange_expense_enhanced, 0)) ELSE 0 END) ) AS vmc_ic, "
				  + "       -1 * SUM(Nvl(sm.mc_assessment, 0) - Nvl(sm.mc_fees, 0))            AS mc_assessment, "
				  + "       -1 * SUM(Nvl(sm.visa_assessment, 0) - Nvl(sm.visa_fees, 0))        AS visa_assessment, "
				  + "       -1 * SUM(Nvl(sm.disc_assessment, 0))                               AS disc_assessment, "
				  + "       -1 * SUM(Nvl(sm.amex_interchange, 0))                              AS amex_ic, "
				  + "       -1 * SUM(Nvl(sm.disc_interchange, 0))                              AS disc_ic, "
				  + "       -1 * SUM(Decode(o.org_group, 3941400059, "
				  + "                     Round(Nvl(sm.dinr_sales_amount, 0) * 0.024, 2), "
				  + "                                    0))                                   AS diners_ic, "
				  + "       -1 * SUM(Decode(o.org_group, 3941400059, "
				  + "                     Round(Nvl(sm.pl1_sales_amount, 0) * 0.0197, 2), "
				  + "                                    0))                                   AS uatp_ic, "
				  + "       SUM(CASE "
				  + "             WHEN sm.load_filename LIKE 'mbs_ext%' THEN ( "
				  + "             Nvl(sm.interchange_expense, 0) - "
				  + "             Nvl(sm.interchange_expense_enhanced, 0) ) "
				  + "             ELSE 0 "
				  + "           END)                                                           AS ic_repair, "
				  + "       -1 * SUM(Nvl(fanf.fee, 0))                                         AS fanf_fees, "
				  + "       SUM(gn.t1_tot_amount_of_sales)                                     AS total_sales, "
				  + "       SUM(sm.vmc_cash_advance_count "
				  + "           + sm.vmc_sales_count + sm.vmc_credits_count "
				  + "           + sm.amex_ic_sales_count "
				  + "           + sm.amex_ic_credits_count "
				  + "           + sm.disc_ic_sales_count "
				  + "           + Nvl(sm.debit_credits_count, 0) "
				  + "           + Nvl(sm.debit_sales_count, 0) "
				  + "           + Nvl(sm.ebt_credits_count, 0) "
				  + "           + Nvl(sm.ebt_sales_count, 0))                                  AS tran_count "
				  + "FROM   monthly_extract_gn gn, "
				  + "       monthly_extract_summary sm, "
				  + "       mif mf, "
				  + "       GROUPS g, "
				  + "       channel_link cl, "
				  + "       organization o, "
				  + "       (SELECT vf.merchant_number, "
				  + "               vf.active_date, "
				  + "               SUM(Nvl(vf.fee, 0)) fee "
				  + "        FROM   visa_fanf_summary vf "
				  + "        WHERE  vf.merchant_number IN (SELECT merchant_number "
				  + "                                      FROM   mif "
				  + "                                      WHERE  bank_number IN ( 3941, 3943, 3858, "
				  + "                                                              3003 )) "
				  + "               AND vf.active_date = :1 "
				  + "        GROUP  BY vf.merchant_number, "
				  + "                  vf.active_date) fanf "
				  + "WHERE  gn.hh_bank_number IN ( 3941, 3943, 3858, 3003 ) "
				  + "       AND gn.hh_active_date = :2 "
				  + "       AND sm.merchant_number = gn.hh_merchant_number "
				  + "       AND gn.hh_merchant_number = fanf.merchant_number(+) "
				  + "       AND fanf.active_date(+) = gn.hh_active_date "
				  + "       AND sm.active_date = gn.hh_active_date "
				  + "       AND mf.merchant_number = sm.merchant_number "
				  + "       AND g.assoc_number = mf.association_node "
				  + "       AND o.org_group = cl.hierarchy_node(+) "
				  + "       AND o.org_group = g.group_2 "
				  + "GROUP  BY sm.active_date";
 
  public class JournalEntryRow extends RowData
  {
    public Vector   CustomFields         = new Vector();
    
    public JournalEntryRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );
      
      getLines().add( new LineItem(getJournalEntryLineItem(0,0),  getJournalEntryLineItem(0,1),  resultSet.getDouble("vmc_ic")) );
      getLines().add( new LineItem(getJournalEntryLineItem(1,0),  getJournalEntryLineItem(1,1),  resultSet.getDouble("amex_ic")) );
      getLines().add( new LineItem(getJournalEntryLineItem(2,0),  getJournalEntryLineItem(2,1),  resultSet.getDouble("uatp_ic")) );
      getLines().add( new LineItem(getJournalEntryLineItem(3,0),  getJournalEntryLineItem(3,1),  resultSet.getDouble("amex_ic")) );
      getLines().add( new LineItem(getJournalEntryLineItem(4,0),  getJournalEntryLineItem(4,1),  resultSet.getDouble("disc_ic")) );
      getLines().add( new LineItem(getJournalEntryLineItem(5,0),  getJournalEntryLineItem(5,1),  resultSet.getDouble("visa_assessment")) );
      getLines().add( new LineItem(getJournalEntryLineItem(6,0),  getJournalEntryLineItem(6,1),  resultSet.getDouble("mc_assessment")) );
      getLines().add( new LineItem(getJournalEntryLineItem(7,0),  getJournalEntryLineItem(7,1),  resultSet.getDouble("disc_assessment")) );
      getLines().add( new LineItem(getJournalEntryLineItem(8,0),  getJournalEntryLineItem(8,1),  resultSet.getDouble("fanf_fees")) );
      getLines().add( new LineItem(getJournalEntryLineItem(9,0),  getJournalEntryLineItem(9,1),  resultSet.getDouble("ic_repair")) );
      
      CustomFields.add( new LineItem(JE_LINE_CUSTOM_FIELDS[0][1], resultSet.getString("tran_count")) );
      CustomFields.add( new LineItem(JE_LINE_CUSTOM_FIELDS[1][1], resultSet.getString("total_sales")) );
    }
    
    public Vector getCustomFields() { return CustomFields; }
  }
  
  public class InvoiceRow extends RowData
  {
    public InvoiceRow( ResultSet resultSet )
      throws java.sql.SQLException
    {
      super( resultSet );
      
      getLines().add( new LineItem(getInvoiceLineItem(0,0),  getInvoiceLineItem(0,1),  resultSet.getDouble("individual_plan") + resultSet.getDouble("disc_billed_to_merch")) );
      getLines().add( new LineItem(getInvoiceLineItem(1,0),  getInvoiceLineItem(1,1),  resultSet.getDouble("interchange_passthru")) );
      getLines().add( new LineItem(getInvoiceLineItem(2,0),  getInvoiceLineItem(2,1),  resultSet.getDouble("debit_inc")) );
      getLines().add( new LineItem(getInvoiceLineItem(3,0),  getInvoiceLineItem(3,1),  resultSet.getDouble("system_gen")) );
      getLines().add( new LineItem(getInvoiceLineItem(4,0),  getInvoiceLineItem(4,1),  resultSet.getDouble("auth_inc")) );
    }
  }
  
  public NetSuiteMonthEndUpload()
  {
  }
  
  public void buildInvList()
  {
    RecordList = new Invoice[4 * INVOICE_LINE_ITEMS.length];
    int idx    = 0;
    
    for( int i = 0; i <  INVOICE_LINE_ITEMS.length; ++i )
    {
      for( int k = 0; k < BANK_NUMBERS.length; ++k )
      {
        RecordList[idx] = buildInvRecord( BANK_NUMBERS[k], getInvoiceLineItem(i, 1) );
        ++idx;
      }
    }
  }
  
  public CustomFieldList buildJeCustomFieldList( JournalEntryRow jeRow )
  {
    LineItem           lineItem       = null;
    CustomFieldRef     fieldRef       = null;
    CustomFieldRef[]   custFields     = null; 
    CustomFieldList    custFieldList  = null;
    
    try
    {
      custFields     = new CustomFieldRef[JE_LINE_CUSTOM_FIELDS.length];
      custFieldList  = new CustomFieldList();
      
      for( int k = 0; k < jeRow.getCustomFields().size() ; ++k )
      {
        lineItem = (LineItem)jeRow.getCustomFields().elementAt(k);
        switch( k )
        {
          case 0:
            fieldRef = new LongCustomFieldRef( lineItem.getItemId() , "0", lineItem.getLongAmount() );
            break;
          case 1:
            fieldRef = new StringCustomFieldRef( lineItem.getItemId() , "0", lineItem.getStringAmount() );
            break;
        }
        custFields[k] = fieldRef;
      }
      custFieldList.setCustomField(custFields);
    }
    catch( Exception e )
    {
      e.printStackTrace();
      custFieldList = null;
    }
    
    return custFieldList;
  }
  
  public void buildJeList()
  {
    RecordList = new JournalEntry[JOURNAL_ENTRY_LINE_ITEMS.length];
    
    for( int i = 0; i < JOURNAL_ENTRY_LINE_ITEMS.length; ++i )
    {
      RecordList[i] = buildJeRecord( getJournalEntryLineItem(i, 1) );
    }
  }
  
  public Record[] builJeClearing()
  {
    Record[] jeRecords = new JournalEntry[1];
    
    jeRecords[0] = buildJeRecord( getJournalEntryLineItem(10, 1) );
    
    return jeRecords;
  }
  
  public Invoice buildInvRecord( String bankNumber, String itemName )
  {
    Vector                 items          = new Vector();
    int                    idx            = 0;
    Invoice                invoice        = null;
    InvoiceRow             row            = null;
    LineItem               lineItem       = null;
    Calendar               cal            = null;
    RecordRef              portfolio      = null;
    RecordRef              entity         = new RecordRef();
    RecordRef              custForm       = new RecordRef();
    try
    {
      invoice    = new Invoice();
      cal        = Calendar.getInstance();
      cal.setTime( ActiveDate );
      custForm.setInternalId( JE_CUSTOM_FORMS[2][0] );
      custForm.setName( JE_CUSTOM_FORMS[2][1] );
      entity.setName(CUSTOMER[0][1]);
      entity.setInternalId(CUSTOMER[0][0]);
      invoice.setCustomForm(custForm);
      invoice.setEntity(entity);
      invoice.setTranDate( cal );
      
      for( int i = 0; i < IVRows.size(); i++ )
      {
        row = (InvoiceRow)IVRows.elementAt(i);
        if( !row.getBankNumber().equals(bankNumber) )
        {
          continue;
        }
        portfolio = new RecordRef();
        portfolio.setName( row.getPortfolioName() );
        portfolio.setInternalId( row.getPortfolioId() );
        
        for( int j = 0; j < row.getLines().size(); j++ )
        {
          lineItem = (LineItem)row.getLines().elementAt(j);
          if(lineItem.getItemName().equals(itemName) )
          {
            RecordRef label = new RecordRef();
            label.setInternalId( lineItem.getItemId() );
            
            InvoiceItem invoiceItem = new InvoiceItem();
            invoiceItem.setItem(label);
            invoiceItem.set_class(portfolio);
            invoiceItem.setAmount(lineItem.getDoubleAmount());
            items.add( invoiceItem );
            break;
          }
        }
      }
      
      InvoiceItem[]    invItems     = new InvoiceItem[items.size()];
      for( int y = 0; y < items.size(); ++y )
      {
        invItems[y] = (InvoiceItem)(items.elementAt(y));
      }
      InvoiceItemList  invList      = new InvoiceItemList(invItems, false);
      invoice.setItemList(invList);
    }
    catch( Exception e )
    {
      logEntry( "buildInvRecord(" + itemName + ")", e.toString() );
    }

    return invoice;
  }

  public JournalEntry buildJeRecord( String itemName )
  {
    String                 accountId      = null;
    double                 amount         = 0.0;
    Calendar               cal            = null; 
    JournalEntry           journalEntry   = null;
    JournalEntryRow        row            = null;
    LineItem               lineItem       = null;
    JournalEntryLineList   jeLineList     = null; 
    JournalEntryLine[]     jeLines        = null;
    CustomFieldList        custFieldList  = null;
    RecordRef              account        = null; 
    RecordRef              portfolio      = null;
    RecordRef              currency       = new RecordRef();
    RecordRef              subsidiary     = new RecordRef();
    RecordRef              custForm       = new RecordRef();
    
    try
    {
      journalEntry  = new JournalEntry();
      jeLines       = new JournalEntryLine[JERows.size()];
      cal           = Calendar.getInstance();
      
      cal.setTime( ActiveDate );
      currency.setInternalId( CURRENCY_ID_USD ) ;
      currency.setType( RecordType.currency );
      subsidiary.setName( SUBSIDIARY[0][1] );
      subsidiary.setInternalId( SUBSIDIARY[0][0] );
      custForm.setInternalId(JE_CUSTOM_FORMS[1][0]);
      
      journalEntry.setTranDate( cal );
      journalEntry.setCurrency( currency );
      journalEntry.setExchangeRate( CURRENCY_XRATE_USD );
      journalEntry.setSubsidiary( subsidiary );
      journalEntry.setCustomForm( custForm );
      
      for( int i = 0; i < JERows.size(); i++ )
      {
        row = (JournalEntryRow)JERows.elementAt(i);
        portfolio = new RecordRef();
        portfolio.setName( row.getPortfolioName() );
        portfolio.setInternalId( row.getPortfolioId() );
        if( itemName.equals("clearing_monthly") )
        {
          accountId = getJournalEntryLineItem(10, 0);
          custFieldList = buildJeCustomFieldList(row);
        }
        else
        {
          for( int j = 0; j < row.getLines().size(); j++ )
          {
            lineItem    = (LineItem)row.getLines().elementAt(j);
            accountId   = null;
            amount      = 0.0;
            if(lineItem.getItemName().equals(itemName) )
            {
              accountId = lineItem.getItemId();
              amount    = lineItem.getDoubleAmount();
              break;
            }
          }
        }
        
        account = new RecordRef();
        account.setInternalId( accountId );
        jeLines[i] = new JournalEntryLine();
        jeLines[i].setAccount( account );
        jeLines[i].setCustomFieldList( custFieldList );
        if( i != JERows.size() -1 )
        {
          jeLines[i].set_class(portfolio);
        }
        if( amount < 0 )
        {
          jeLines[i].setCredit( amount * -1 );
        }
        else
        {
          jeLines[i].setDebit( amount );
        }
      }
      
      jeLineList = new JournalEntryLineList();
      jeLineList.setLine(jeLines);
      journalEntry.setLineList(jeLineList);
    }
    catch( Exception e )
    {
      logEntry( "buildJeRecord(" + itemName + ")", e.toString() );
    }
    
    return journalEntry;
  }
  
  public void extractJeResponseParam()
  {
    String     itemName       = null;
    String     status         = null;
    int        bankNumber     = -1;
    int        itemIdx        = 0;
    
    try
    {
      LogEntryRows.clear();
      
      if( ResponseList == null )
      {
        return;
      }
      
      for( int i = 0; i < ResponseList.length; i++ )
      {
        WriteResponse response = ResponseList[i];
        itemName = getJournalEntryLineItem(i ,1);
        if( response.getStatus().isIsSuccess() )
        {
          status = "success";
        }
        else
        {
          status = getStatusDetails(response.getStatus());
        }
        
        LogEntryRow entry = new LogEntryRow(PT_MONTHLY, REC_TYPE_JE, ActiveDate );
        entry.setItemName( itemName );
        entry.setStatus( status);
        LogEntryRows.add(entry);
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
      logEntry( "extractJeResponseParam()", e.toString() );
    }
  }
  
  public void extractInvResponseParam()
  {
    String     itemName       = null;
    String     status         = null;
    int        bankNumber     = -1;
    int        itemIdx        = 0;
    
    try
    {
      LogEntryRows.clear();
      
      if( ResponseList == null )
      {
        return;
      }
      
      for( int k = 0; k < BANK_NUMBERS.length; ++k )
      {
        itemIdx = 0;
        bankNumber = Integer.parseInt(BANK_NUMBERS[k]);
        for( int i = k; i < ResponseList.length; i = i + 3 )
        {
          WriteResponse response = ResponseList[i];
          itemName = getInvoiceLineItem(itemIdx ,1);
          ++itemIdx;
          if( response.getStatus().isIsSuccess() )
          {
            status = "success";
          }
          else
          {
            status = getStatusDetails(response.getStatus());
          }
          LogEntryRow entry = new LogEntryRow( PT_MONTHLY, REC_TYPE_IV, ActiveDate );
          entry.setBankNumber( bankNumber );
          entry.setItemName( itemName );
          entry.setStatus( status );
          LogEntryRows.add( entry );
        }
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
      logEntry( "extractInvResponseParam()", e.toString() );
    }
  }
  
  public String getInvoiceLineItem(int itemIdx, int itemName )
  {
    String  retVal = "Undefined";
    
    try
    {
      retVal = INVOICE_LINE_ITEMS[itemIdx][itemName];
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
    }
    
    return( retVal );
  }
  
  public String getJournalEntryLineItem(int itemIdx, int itemName )
  {
    String  retVal = "Undefined";
    
    try
    {
      retVal = JOURNAL_ENTRY_LINE_ITEMS[itemIdx][itemName];
    }
    catch( ArrayIndexOutOfBoundsException e )
    {
    }
    
    return( retVal );
  }
  
  public void loadData()
  {
    ResultSetIterator   it          = null;
    ResultSet           rs          = null;
    
    JERows.clear();
    IVRows.clear();
    
    try
    {
      connect();
{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.NetSuiteMonthEndUpload",SQL_GET_JOURNAL_ENTRY_AND_INVOICE);
   // set IN parameters
   __sJT_st.setDate(1,ActiveDate);
   __sJT_st.setDate(2,ActiveDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.NetSuiteMonthEndUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:560^7*/
      
      rs = it.getResultSet();
      while( rs.next() )
      {
        JERows.add( new JournalEntryRow(rs) );
        IVRows.add( new InvoiceRow(rs) );
      }
      
      // total expenses
{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.NetSuiteMonthEndUpload",SQL_GET_JOURNAL_ENTRY);
   // set IN parameters
   __sJT_st.setDate(1,ActiveDate);
   __sJT_st.setDate(2,ActiveDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.NetSuiteMonthEndUpload",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:643^7*/
      
      rs = it.getResultSet();
      while( rs.next() )
      {
        JERows.add( new JournalEntryRow(rs) );
      }
      
      it.close();
    }
    catch( java.sql.SQLException e )
    {
      logEntry( "loadData()", e.toString() );
    }
    finally
    {
      try { it.close(); } catch( Exception e ) { }
    }
  }
  
  public boolean execute()
  {
    Record[]         records       = null;
    WriteResponse[]  responses     = null;
    try
    {
      connect(true);
      setAutoCommit(false);
      
      /*@lineinfo:generated-code*//*@lineinfo:672^7*/

//  ************************************************************
//  #sql [Ctx] { select  add_months(trunc(sysdate,'month'), -1)
//          
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  add_months(trunc(sysdate,'month'), -1)\n         \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.NetSuiteMonthEndUpload",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   ActiveDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:677^7*/
      
      loadData();
      buildInvList();
      uploadData();
      extractInvResponseParam();
      logResponses();
      buildJeList();
      uploadData();
      extractJeResponseParam();
      logResponses();
    }
    catch( Exception e )
    {
      logEntry( "execute()", e.toString() );
    }
    
    return true;
  }
  
  public static void main( String[] args )
  {
    NetSuiteMonthEndUpload  test        = null;
    
      try {
        if (args.length > 0 && args[0].equals("testproperties")) {
          EventBase.printKeyListStatus(new String[]{
                  MesDefaults.DK_NETSUITE_WSDL_URL,
                  MesDefaults.DK_NETSUITE_WS_TEST_URL,
                  MesDefaults.DK_NETSUITE_WS_PROD_URL,
                  MesDefaults.DK_NETSUITE_LOGIN_EMAIL,
                  MesDefaults.DK_NETSUITE_LOGIN_PASSWORD,
                  MesDefaults.DK_NETSUITE_LOGIN_ROLE,
                  MesDefaults.DK_NETSUITE_LOGIN_ACCOUNT
          });
        }
      } catch (Exception e) {
        log.error(e.toString());
      }

    try
    { 
      // turn on debug
      //com.mes.database.SQLJConnectionBase.initStandalone();

      test = new NetSuiteMonthEndUpload();
      test.connect();
      
      // args[0]: NetSuite enviroment
      //          "upload_test", upload to sandbox
      //          "upload_prod", upload to production
      // args[1]: NetSuite record type
      //          "je",  Journal Entry
      //          "iv",  Invoice
      //          "all", Invoice and Journal Entry
      // args[2]: active date, "MM/dd/yyyy"
      
      if( args.length == 3 && (args[0].equals("upload_test") || args[0].equals("upload_prod")) )
      {
        if( args[0].equals("upload_test") )
        {
          test.setTestMode(true);
        }
        
        test.setActiveDate( DateTimeFormatter.parseSQLDate( args[2].toString(), "MM/dd/yyyy") );
        test.loadData();
        
        if( args[1].equals("iv") || args[1].equals("all") )
        {
          test.buildInvList();
          test.uploadData();
          test.extractInvResponseParam();
          test.logResponses();
        }
        
        if( args[1].equals("je") || args[1].equals("all") )
        {
          test.buildJeList();
          test.uploadData();
          test.extractJeResponseParam();
          test.logResponses();
        }
      }
      else
      {
        test.execute();
      }
    }
    catch( Exception e )
    {
      e.printStackTrace();
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}/*@lineinfo:generated-code*/