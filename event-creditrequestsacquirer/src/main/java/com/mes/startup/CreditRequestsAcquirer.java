/*@lineinfo:filename=CreditRequestsAcquirer*//*@lineinfo:user-code*//*@lineinfo:1^1*/package com.mes.startup;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.config.MesDefaults;
import com.mes.ops.CreditReportBean;
import sqlj.runtime.ResultSetIterator;


/**
 * CreditRequestsAcquirer class.
 * 
 * Automation for retreival of FICA Credit Ratings.
 */
public final class CreditRequestsAcquirer extends EventBase
{
  // constants
  public static final int             WAIT_MINUTES                = 2;
    // i.e. the minimum elapsed time in minutes 
    //  to wait before attempting to retreive the actual credit rating(s)
    
  // data members
  private int                         procSequence                = 0;
  private CreditReportBean            crb;
  
  
  // construction
  public CreditRequestsAcquirer()
  {
    crb = null;
  }
  
  private void getProcSequence()
  {
    try
    {
      // get the processSequence
      /*@lineinfo:generated-code*//*@lineinfo:48^7*/

//  ************************************************************
//  #sql [Ctx] { select  creditrequests_sequence.nextval 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  creditrequests_sequence.nextval  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.CreditRequestsAcquirer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procSequence = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:52^7*/
      
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "getProcSequence()", e.toString());
      setError("getProcSequence(): " + e.toString());
      logEntry("getProcSequence()", e.toString());
    }
  }

  private int getNumElidgableRequests()
  {
    // IMPT: seek only those requests that do NOT ALREADY have a credit score!
    //       Otherwise the possibility of overwriting the score exists
    
    int count = 0;
    
    try {

      /*@lineinfo:generated-code*//*@lineinfo:72^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(rec_id)
//          
//          from    credit_requests_process
//                  ,credit_requests
//                  ,credit_scores
//          where   credit_requests.app_seq_num=credit_requests_process.app_seq_num
//                  and credit_scores.app_seq_num(+)=credit_requests_process.app_seq_num                              
//                  and credit_scores.score is null
//                  and output_id is not null
//                  and process_sequence is null
//                  and to_number(to_char(to_date('00','MI') + (sysdate - date_created), 'MI')) > :WAIT_MINUTES
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(rec_id)\n         \n        from    credit_requests_process\n                ,credit_requests\n                ,credit_scores\n        where   credit_requests.app_seq_num=credit_requests_process.app_seq_num\n                and credit_scores.app_seq_num(+)=credit_requests_process.app_seq_num                              \n                and credit_scores.score is null\n                and output_id is not null\n                and process_sequence is null\n                and to_number(to_char(to_date('00','MI') + (sysdate - date_created), 'MI')) >  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.CreditRequestsAcquirer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,WAIT_MINUTES);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   count = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:85^7*/
    }
    catch(Exception e) {
      logEvent(this.getClass().getName(), "getNumEligableRequests()", e.toString());
      logEntry("getNumEligableRequests()", e.toString());
    }

    return count;
  }

  public boolean execute()
  {
    boolean             result  = false;
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    
    try {
      connect();
      
      // see if there are elidgable requests
      if(getNumElidgableRequests()<1)
        return true;    // no penalty

      // get process sequence number
      getProcSequence();
      
      // mark interesting rows with the process sequence
      /*@lineinfo:generated-code*//*@lineinfo:112^7*/

//  ************************************************************
//  #sql [Ctx] { update  credit_requests_process
//          set     process_sequence = :procSequence
//          where   app_seq_num in 
//                  (
//                    select  credit_requests_process.app_seq_num
//                    from    credit_requests_process
//                            ,credit_requests
//                            ,credit_scores
//                    where   credit_requests.app_seq_num=credit_requests_process.app_seq_num
//                            and credit_scores.app_seq_num(+)=credit_requests_process.app_seq_num                              
//                            and credit_scores.score is null
//                            and output_id is not null
//                            and process_sequence is null
//                            and to_number(to_char(to_date('00','MI') + (sysdate - date_created), 'MI')) > :WAIT_MINUTES
//                  )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  credit_requests_process\n        set     process_sequence =  :1 \n        where   app_seq_num in \n                (\n                  select  credit_requests_process.app_seq_num\n                  from    credit_requests_process\n                          ,credit_requests\n                          ,credit_scores\n                  where   credit_requests.app_seq_num=credit_requests_process.app_seq_num\n                          and credit_scores.app_seq_num(+)=credit_requests_process.app_seq_num                              \n                          and credit_scores.score is null\n                          and output_id is not null\n                          and process_sequence is null\n                          and to_number(to_char(to_date('00','MI') + (sysdate - date_created), 'MI')) >  :2 \n                )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.CreditRequestsAcquirer",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   __sJT_st.setInt(2,WAIT_MINUTES);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:129^7*/
      
      // make sure database shows that we are processing
      commit();
      
      // get all the interesting rows into a vector
      /*@lineinfo:generated-code*//*@lineinfo:135^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    rec_id,app_seq_num
//          from      credit_requests_process
//          where     process_sequence = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    rec_id,app_seq_num\n        from      credit_requests_process\n        where     process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.CreditRequestsAcquirer",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.CreditRequestsAcquirer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:140^7*/
      
      rs = it.getResultSet();

      String s;
      long recId,app_seq_num;
      
      while(rs.next()) {
        recId=rs.getLong(1);
        app_seq_num=rs.getLong(2);
        
        recordTimestampBegin(recId);

        // retreive the credit score
        try {
          result=CreditReportBean.sendDataRequest(app_seq_num);
        }
        catch(Exception e) {
          result=false;
          s=e.getMessage();
          logEntry("execute()", s);
        }
        
        if(result) {
          // successful so mark as completed and commit (otherwise don't commit)
          recordTimestampEnd(recId);
          commit();
        }
      }
      
      rs.close();
      it.close();

      // clean up
      /*@lineinfo:generated-code*//*@lineinfo:174^7*/

//  ************************************************************
//  #sql [Ctx] { update  credit_requests_process
//          set     process_sequence = null
//                  ,date_started = null
//          where   process_sequence = :procSequence
//                  and date_completed is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  credit_requests_process\n        set     process_sequence = null\n                ,date_started = null\n        where   process_sequence =  :1 \n                and date_completed is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.CreditRequestsAcquirer",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:181^7*/
      commit();

      
    }
    catch(Exception e) {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally {
      cleanUp();
    }
    
    return result;
  }
  
  protected void recordTimestampBegin( long recId )
  {
    Timestamp     beginTime   = null;
    
    try
    {
      beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());
    
      /*@lineinfo:generated-code*//*@lineinfo:205^7*/

//  ************************************************************
//  #sql [Ctx] { update  credit_requests_process
//          set     date_started = :beginTime
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  credit_requests_process\n        set     date_started =  :1 \n        where   rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.CreditRequestsAcquirer",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,beginTime);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:210^7*/
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "recordTimestampBegin()", e.toString());
      setError("recordTimestampBegin(): " + e.toString());
      logEntry("recordTimestampBegin()", e.toString());
    }
  }
  
  protected void recordTimestampEnd( long recId )
  {
    Timestamp     beginTime   = null;
    long          elapsed     = 0L; 
    Timestamp     endTime     = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:228^7*/

//  ************************************************************
//  #sql [Ctx] { select  date_started 
//          from    credit_requests_process 
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  date_started  \n        from    credit_requests_process \n        where   rec_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.CreditRequestsAcquirer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,recId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   beginTime = (java.sql.Timestamp)__sJT_rs.getTimestamp(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:233^7*/
      endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      elapsed = (endTime.getTime() - beginTime.getTime());
    
      /*@lineinfo:generated-code*//*@lineinfo:237^7*/

//  ************************************************************
//  #sql [Ctx] { update  credit_requests_process
//          set     date_completed = :endTime
//          where   rec_id = :recId
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  credit_requests_process\n        set     date_completed =  :1 \n        where   rec_id =  :2";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.CreditRequestsAcquirer",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,endTime);
   __sJT_st.setLong(2,recId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:242^7*/

    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "recordTimestampEnd()", e.toString());
      setError("recordTimestampEnd(): " + e.toString());
      logEntry("recordTimestampEnd()", e.toString());
    }
  }
  
  public static void main(String[] args)
  {
   try{
       if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{
                    MesDefaults.DK_SYN_XML_TEST_URL
            });
        }
    }
    catch( Exception e )
    {
      log.error(e.toString());
    }

    CreditRequestsAcquirer worker = new CreditRequestsAcquirer();
    
    try
    {
      com.mes.database.SQLJConnectionBase.initStandalone("DEBUG");
      worker.execute();
    }
    catch(Exception e)
    {
      System.out.println("CreditRequestsAcquirer::main(): " + e.toString());
    }
  }

}/*@lineinfo:generated-code*/