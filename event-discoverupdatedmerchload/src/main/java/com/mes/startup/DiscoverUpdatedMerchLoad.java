/*@lineinfo:filename=DiscoverUpdatedMerchLoad*//*@lineinfo:user-code*//*@lineinfo:1^1*/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import org.apache.log4j.Logger;
import com.mes.reports.DiscoverUpdatedMerchRecord;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ScrollableResultSetIterator;

public class DiscoverUpdatedMerchLoad extends EventBase {

  static Logger log = Logger.getLogger(DiscoverUpdatedMerchLoad.class);
  ArrayList updatedMerchRows = new ArrayList();

  private Date manualUpdateDate = null;
  private boolean initialRun    = false;
  
  final static int MERCHANT_NUMBER = 1;
  
  /**
   * 
   */
  public boolean execute() {
    Date updateDate = null;

    Calendar cal = Calendar.getInstance();

    try {
      
      if (manualUpdateDate == null) {
        //default to yesterday
        cal.add(Calendar.DAY_OF_MONTH, -1);
      } else {
        // use user provided date
        cal.setTime(manualUpdateDate);
      }
      
      cal.set(Calendar.HOUR_OF_DAY, 0);
      cal.set(Calendar.MINUTE, 0);
      cal.set(Calendar.SECOND, 0);
      cal.set(Calendar.MILLISECOND, 0);

      updateDate = new java.sql.Date(cal.getTime().getTime());     

      log.info("Calling loadData() of DiscoverUpdatedMerchLoad");
      loadData(updateDate);
    } catch (Exception e) {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logError("execute()", e.toString());
    } 
    return (true);
  }
  /**
   * 
   */
  public void loadData(Date updateDate) {
    try {  
      log.info("Loading DiscoverUpdatedMerchLoad Data for date: " + updateDate);
      connect(true);
      processUpdatedMerchants(updateDate);        
      log.info("Done!");
    } catch (Exception e) {
      logError("loadData()", e.toString());
    } finally {
      cleanUp();
    }

  }
  /**
   * 
   */
  private void createUpdatedMerchantsRecord(ResultSet resultSet, Date updateDate) throws SQLException {
   
    while (resultSet.next()) {
      DiscoverUpdatedMerchRecord discoverUpdatedMerchRecord = new DiscoverUpdatedMerchRecord();
      discoverUpdatedMerchRecord.setMerchantNumber(resultSet.getLong(MERCHANT_NUMBER)); 
      updatedMerchRows.add(discoverUpdatedMerchRecord);
      System.out.println("merchantNumber: "+resultSet.getLong(MERCHANT_NUMBER)); 
      insertDiscoverMapAutoBoard(resultSet.getLong(MERCHANT_NUMBER), updateDate);
      if (isInitialRun()) { 
        updateDiscoverInitialLoad(resultSet.getLong(MERCHANT_NUMBER));
      }
    }    
    log.info("DiscoverUpdatedMerchLoad::createUpdatedMerchantsRecord() No of MID's updated = "+ updatedMerchRows.size());
    System.out.println("DiscoverUpdatedMerchLoad record collection size = " + updatedMerchRows.size());
  }
  /**
   * Insert a record into discover_map_auto_board table with action='02'
   */
  private void insertDiscoverMapAutoBoard(long merchantNumber, Date updateDate) {
    try { 
//    int  count = 0;
//    try {
//      #sql [Ctx]
//      {
//         select count( mf.merchant_number ) into :count
//         from   mif mf
//         where  mf.merchant_number = :merchantNumber and
//                nvl(mf.dmacctst,'A') in ( 'D','C','B' )
//      };
//    }
//    catch (Exception e) {
//      count = 0;
//    }
      String actionUpdate = "02"; //(count == 0 ? "02" : "04");
        /*@lineinfo:generated-code*//*@lineinfo:110^9*/

//  ************************************************************
//  #sql [Ctx] { insert into discover_map_auto_board
//            ( PROCESS_SEQUENCE,
//              MERCHANT_NUMBER,
//              DATE_CREATED,
//              ACTION,           
//              LOAD_FILENAME)
//            values
//            (  0,
//              :merchantNumber,
//              :updateDate,
//              :actionUpdate,
//              ''
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into discover_map_auto_board( PROCESS_SEQUENCE, MERCHANT_NUMBER, DATE_CREATED, ACTION,LOAD_FILENAME) "
   		+ " values (0,:merchantNumber,:updateDate,:actionUpdate,'')";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.DiscoverUpdatedMerchLoad",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
   __sJT_st.setDate(2,updateDate);
   __sJT_st.setString(3,actionUpdate);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:125^9*/
    }
    catch (Exception e) {
      logError("insertDiscoverMapAutoBoard(merchantNumber, updateDate) :: (" + merchantNumber + ", " + updateDate + "): " + e.toString());
    }
  }

  /**
   * Update discover_initial_updated_merch table by setting is_loaded to 'Y' to avoid loading it again in the future
   */
  private void updateDiscoverInitialLoad(long merchantNumber) {
    try { 
        /*@lineinfo:generated-code*//*@lineinfo:137^9*/

//  ************************************************************
//  #sql [Ctx] { update discover_merchant_init
//             set is_loaded = 'Y'
//             where dmdsnum = (select dmdsnum from mif where merchant_number = :merchantNumber) 
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update discover_merchant_init set is_loaded = 'Y' where dmdsnum = (select dmdsnum from mif where merchant_number = :1 )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"1com.mes.startup.DiscoverUpdatedMerchLoad",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,merchantNumber);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:142^9*/
    }
    catch (Exception e) {
      logError("updateDiscoverInitialLoad(merchantNumber) :: (" + merchantNumber + "): " + e.toString());
    }
  }
  /**
   * Process the updated merchants by inserting rows into discover_map_auto_board table with action='02'
   */
  private void processUpdatedMerchants(Date updateDate) {
    ScrollableResultSetIterator   it                = null;  
    ResultSet                     resultSet         = null;
   
    try {
      if (isInitialRun()) {
        /*@lineinfo:generated-code*//*@lineinfo:157^9*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct mf.merchant_number
//              from  discover_merchant_init dmi,
//                    mif mf
//              where dmi.dmdsnum = mf.dmdsnum 
//                    and (dmi.is_loaded is null or dmi.is_loaded = 'N')
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select distinct mf.merchant_number from discover_merchant_init dmi, mif mf where dmi.dmdsnum = mf.dmdsnum "
   		+ "and (dmi.is_loaded is null or dmi.is_loaded = 'N')";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.DiscoverUpdatedMerchLoad",theSqlTS,1004,1007);
   // execute query
   it = new sqlj.runtime.ref.ScrollableResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.DiscoverUpdatedMerchLoad",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:164^11*/
        
      } else {
        /*@lineinfo:generated-code*//*@lineinfo:167^9*/

//  ************************************************************
//  #sql [Ctx] it = { select distinct mifc.merchant_number
//              from mif_changes     mifc, 
//                   merchant        mr, 
//                   merchpayoption  mrp
//              where trunc(mifc.date_changed) = :updateDate
//                   and mifc.merchant_number = mr.merch_number
//                   and mr.app_seq_num = mrp.app_seq_num
//                   and (mrp.cardtype_code = 14 or
//                        (length(mrp.merchpo_card_merch_number) > 6 and substr(mrp.merchpo_card_merch_number, 1, 6) = '601172'))
//                   and (mifc.change_details like '%zip0%' or mifc.change_details like '%merchantStatus%' or 
//                        mifc.change_details like '%discoverNumber%' or mifc.change_details like '%dbaName%' or
//                        mifc.change_details like '%state0%' or mifc.change_details like '%ownerName%' or
//                        mifc.change_details like '%sicCode%' or mifc.change_details like '%phone1%' or
//                        mifc.change_details like '%federalTaxId%' or mifc.change_details like '%corporateName%' or
//                        mifc.change_details like '%addr10%' or mifc.change_details like '%city0%' or
//                        mifc.change_details like '%addr200%')
//                   and not exists (
//                       select merchant_number from discover_map_auto_board
//                       where 
//                            -- process_sequence = 0 and
//                            -- action = '01' and
//                             merchant_number = mifc.merchant_number and
//                             trunc(date_created) = :updateDate )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
	  String theSqlTS = "select distinct mifc.merchant_number from mif_changes mifc,  merchant mr,  merchpayoption  mrp "
	  		+ "where trunc(mifc.date_changed) = :updateDate and mifc.merchant_number = mr.merch_number and mr.app_seq_num = mrp.app_seq_num "
	  		+ "and (mrp.cardtype_code = 14 or (length(mrp.merchpo_card_merch_number) > 6 and substr(mrp.merchpo_card_merch_number, 1, 6) = '601172')) "
	  		+ "and (mifc.change_details like '%zip0%' or mifc.change_details like '%merchantStatus%' or  mifc.change_details like '%discoverNumber%' "
	  		+ "or mifc.change_details like '%dbaName%' or mifc.change_details like '%state0%' or mifc.change_details like '%ownerName%' "
	  		+ "or mifc.change_details like '%sicCode%' or mifc.change_details like '%phone1%' or mifc.change_details like '%federalTaxId%' "
	  		+ "or mifc.change_details like '%corporateName%' or mifc.change_details like '%addr10%' or mifc.change_details like '%city0%' "
	  		+ "or mifc.change_details like '%addr200%') and not exists ( select merchant_number from discover_map_auto_board where "
	  		+ "merchant_number = mifc.merchant_number and trunc(date_created) = :updateDate )";
	  
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.DiscoverUpdatedMerchLoad",theSqlTS,1004,1007);
   // set IN parameters
   __sJT_st.setDate(1,updateDate);
   __sJT_st.setDate(2,updateDate);
   // execute query
   it = new sqlj.runtime.ref.ScrollableResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.DiscoverUpdatedMerchLoad",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:192^11*/
      }
      resultSet = it.getResultSet();
      
      if (resultSet != null) {          
        createUpdatedMerchantsRecord(resultSet, updateDate);
        resultSet.close();  
      }
    
    } catch(Exception e) {
      logError("Exception in getUpdatedMerchants().DiscoverUpdatedMerchLoad failed to run - please re-run it manually with parameter MM/dd/yyyy. Exception details : " + e.toString());
    } finally {
      try{
        logInfo("Closing SQL connection.");
        if(it!=null){
          it.close();
        }        
      } catch(Exception e) {
        logError("Exception in finally block of getUpdatedMerchants() : " + e.toString());
      }
    }
  }
  
  public Date getManualUpdateDate() {
    return manualUpdateDate;
  }

  public void setManualUpdateDate(Date manualUpdateDate) {
    this.manualUpdateDate = manualUpdateDate;
  }
  
  public boolean isInitialRun() {
    return initialRun;
  }

  public void setInitialRun(boolean initialRun) {
    this.initialRun = initialRun;
  }
  
  public static void main(String[] args) {  
    DiscoverUpdatedMerchLoad discoverUpdatedMerchLoad = new DiscoverUpdatedMerchLoad();
     if (args.length > 1 && "execute".equals(args[0])) {
       discoverUpdatedMerchLoad.setManualUpdateDate(new Date(DateTimeFormatter.parseDate(args[1], "MM/dd/yyyy").getTime()));
       discoverUpdatedMerchLoad.setInitialRun((args.length > 2) ? args[2].trim().equalsIgnoreCase("Y"): false);
       System.out.println("Calling Execute()");      
       discoverUpdatedMerchLoad.execute();
       System.exit(0);
     } else {
       System.out.println("Invalid Command");
       System.exit(1);
     }

   }

}/*@lineinfo:generated-code*/
