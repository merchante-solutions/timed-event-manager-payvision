package com.mes.startup;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.jscape.inet.sftp.Sftp;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.MesFlatFiles;
import com.mes.constants.MesQueues;
import com.mes.data.transaction.chargebacks.SplitFundUtil;
import com.mes.flatfile.FlatFileRecord;
import com.mes.net.MailMessage;
import com.mes.queues.QueueNotes;
import com.mes.support.MesEncryption;
import com.mes.support.MesMath;
import com.mes.support.StringUtilities;
import com.mes.support.TridentTools;
import com.mes.tools.ChargebackTools;
import com.mes.tools.FileUtils;
import masthead.util.ByteUtil;

public class AmexOptblueDisputesLoader extends EventBase {

	private static final long serialVersionUID = 1L;	
	static Logger log = Logger.getLogger(AmexOptblueDisputesLoader.class);

	/**
	 * Point of entry
	 * @return
	 */
	public boolean execute() {
		try {
			connect( true );
			
			loadFiles( getEventArg(0) );
			
		} catch (Exception e) {
			logEvent( this.getClass().getName(), "execute()", e.toString() );
			logEntry( "execute()", e.toString() );
		}
		
		return true;
	}
	
	/**
	 * Download the files from Amex SFTP site, load them into MeS system
	 * and archives
	 * @param loadType
	 */
	protected void loadFiles( String loadType ) {
		
		Sftp sftp = null;
		String filePrefix = null;
		String remoteFilename = null;
		String loadFilename = null;
		boolean notify = false;
		
		try {
			
			filePrefix = DisputeFilePrefixes.get( loadType );
			log.debug( "Begining loadType " + loadType );
			
			// get the Sftp connection
			sftp = getAmexSFTPConnection();
			log.debug( "Connected to Amex SFTP site" );
			
			EnumerationIterator enumi = new EnumerationIterator();
			Iterator<String> it = enumi.iterator( sftp.getNameListing( getFileMask( loadType ) ) );
			
			while ( it.hasNext() ) {
				
				// Re-check connection as we loop through because Amex SFTP site has a tendency to time out / drop connection after a couple of 
				// file downloads (as observed in Amex ESA downloads)
				if (!sftp.isConnected() ) {
					
					sftp = getAmexSFTPConnection();
					log.debug("Reconnected to Amex SFTP site");
				}

				remoteFilename = (String) it.next();
				loadFilename = generateFilename(filePrefix + "3941");
				
				sftp.download(loadFilename, remoteFilename);
				log.info("Downloaded " + remoteFilename + " as " + loadFilename);
				
                if ( CBNSP.equals(loadType) ){
                	loadCBNSP( loadFilename );
                	
                } else if(INQ02.equals(loadType)){
                	loadINQ02( loadFilename );
                	
                }
                
				archiveDailyFile(loadFilename);
				log.debug(loadFilename + " archived");
				
				notify( loadFilename );
			}
		} catch (Exception e) {
			logEntry("loadFiles(" + loadType + ")", e.toString());

		}
	}
	
	/**
	 * Amex sends in files with specific prefixes. This method maps the load to the file prefix
	 * @param loadType
	 * @return
	 */
	protected String getFileMask(String loadType){    	
    	String fileMask = "";
    	String filePrefix = isTestMode ? TEST_PREFIX : PROD_PREFIX;
    			
		if(loadType.equals(CBNSP)){
			fileMask = filePrefix + loadType + ".*";
			
		} else if(loadType.equals(INQ02)){
			fileMask = filePrefix + loadType + ".*";
			
		} 
		
    	return fileMask;
    }	
	

	/*
	 * CBNSP - Chargeback Notifications
	 * 
	 */
	protected void loadCBNSP(String loadFilename) {
		loadChargebackFile( loadFilename );
		loadChargebackQueue( loadFilename );
	}

	/**
	 * Reads the incoming file and loads it into NETWORK_CHARGEBACK_AMEX_OPTBLUE table.
	 * If for a given detail record either the cb_resolution_adj_number exists OR if the chargeback already exists in the Mes system 
	 * (based on the cb_adjustment_number, cb_amount, se_numb) and a reversal entry is created
	 * @param inputFilename
	 */
	protected void loadChargebackFile( String inputFilename ) {
		
	    // Flat File Defn for CBNSP		
	    Map<String, FlatFileRecord>  flatFileDefnsCBNSP = 
	    									new HashMap<String, FlatFileRecord>();
	    flatFileDefnsCBNSP.put( REC_TYPE_HEADER, new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_CBNSP_HDR ) );
	    flatFileDefnsCBNSP.put( REC_TYPE_DETAIL, new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_CBNSP_DTL ) );
	    flatFileDefnsCBNSP.put( REC_TYPE_TRAILER, new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_CBNSP_TRL ) );
	    
	    PreparedStatement pStmt = null;
		ResultSet rs = null;
		PreparedStatement pStmt2 = null;
				
		int paramIndex = 1;
	    int colIndex = 1;
	    
	    FlatFileRecord ffr = null;
	    Date incomingDate = null;	    
	    String adjRefNum = "";    	    	    
	    BufferedReader in = null;	    	    	    
	    String line = null;	    
	    long loadSec = 0L;	    
	    String note = null;
	    char recType = '\0';
	    ResultSet resultSet = null;
	    String loadFilename = null;
	    long loadFileId = 0L;
	    String SAID = "";
	    
	    try {
	    	loadFilename = FileUtils.getNameFromFullPath( inputFilename ).toLowerCase();
	    	if(isTestMode)
	    		loadFilename = loadFilename.substring(loadFilename.lastIndexOf('/')+1);		// ** Remove TODO **
	    	loadFileId = loadFilenameToLoadFileId( loadFilename );

	    	in = new BufferedReader( new FileReader(inputFilename) );

	    	while( ( line = in.readLine() ) != null ) {
	    		recType = line.charAt(0);
	    		paramIndex = 1;
	    		colIndex = 1;

	    		ffr = ( FlatFileRecord ) flatFileDefnsCBNSP.get(String.valueOf(recType));

	    		if ( ffr != null ) {
	    			ffr.resetAllFields();
	    			ffr.suck(line);
	    			
	    			if ( REC_TYPE_HEADER.equals(String.valueOf(recType)) ) {	    				
	    				pStmt = con.prepareStatement( SQL_GET_INCOMING_DATE.toString() );
	    				
	    				pStmt.setString( paramIndex++, ffr.getFieldData("ccyyddd") ); 					// *CBNSP Field 5: Amex Creation / Transmission Date
	    				
	    				rs = pStmt.executeQuery();
	    				if(rs.next())
	    					incomingDate = rs.getDate( colIndex++ );
	    				
	    				rs.close();
	    				pStmt.close();
	    				
	    				SAID = ffr.getFieldData("said");
	    				
	    			} else if ( REC_TYPE_DETAIL.equals(String.valueOf(recType)) ) {
	    				String billedAmount = ffr.getFieldData("billed_amount").trim();
	    				adjRefNum = ffr.getFieldData( "cb_adjustment_number" ); 						// *CBNSP Field 24
	    				boolean invalidDetailRecord = "".equals(billedAmount) || billedAmount.matches("0*([^1-9][.]*[^1-9]+)");
	    				if(invalidDetailRecord) {
	    					log.info("Bad detail record. Skipping this detail record from loading as the billing amount is 0. cb_adjustment_number(CBNSP Field 24) :"+ adjRefNum + ", billing amount :" + billedAmount);
	    					continue;
	    				}
	    				note      = buildChargebackNote( ffr );
	            
	    				// A previous chargeback entry with this adjustment number is looked up.
	    				// Based on research for ESA adjustment reference numbers it is seen that these numbers are not unique
	    				// and get repeated anywhere between an year to four years
	    				
	    				pStmt = con.prepareStatement( SQL_CHECK_IF_EXISTING_CHBK.toString() );
	    				pStmt.setString( paramIndex++, ffr.getFieldData("cb_amount").replace(' ','+') ); // *CBNSP Field 23
	    				pStmt.setString( paramIndex++, ffr.getFieldData("cb_amount").replace(' ','+') ); 
	    				pStmt.setString( paramIndex++, ffr.getFieldData("se_numb").trim() );   			 // *CBNSP Field 2
	    				pStmt.setDate( paramIndex++, incomingDate );
	    				pStmt.setString( paramIndex++, adjRefNum ); 
	    				
	    				rs = pStmt.executeQuery();
	    				
	    				if( rs.next() ) {
	    				
	    					loadSec = rs.getLong("load_sec");
	    					String reversalRefNum = ffr.getFieldData("cb_resolution_adj_number").trim();  	 // *CBNSP Field 25
	              
	    					// Apparently sometimes amex uses the same adjustment number with an offset amount rather than issuing a new reversal adjustment number.
	    					// when this is the case, use the original adjustment number as the reversal adjustment number
	    					if ( (reversalRefNum == null || "".equals(reversalRefNum))  && "Y".equals( rs.getString("offset_adjustment") ) ) {
	    						reversalRefNum = adjRefNum;
	    					}
	              
	    					// If this is a reversal and the previous entry has not already been reversed, then issue a reversal for the original
	    					if ( !"".equals( reversalRefNum ) && "N".equals( rs.getString("reversed") ) ) {
	    						ChargebackTools.reverseChargeback(Ctx, "AM", loadSec, incomingDate, rs.getDouble("reversal_amount"));
	                
				                // add the reversal reference number to the chargeback
	    						pStmt2 = con.prepareStatement( SQL_UPDATE_REVERSAL_REF_NUM.toString() );
	    						
	    						paramIndex = 1;
	    						pStmt2.setString( paramIndex++, reversalRefNum ); 
	    	    				pStmt2.setLong( paramIndex++, loadSec ); 
	    	    				
	    	    				pStmt2.executeUpdate(); 
				                pStmt2.close();
				                
				                //updates network_chargebacks.reversal_date and amex_settlement_recon_optblue.cb_load_sec
				                updateNwChgbk(incomingDate,reversalRefNum,loadSec);
				                
				                // if amex changed the case number on this entry, add
				                // a note to the chargeback item with the new case number
				                String oldCaseNum = rs.getString("case_number");
				                int qtype = rs.getInt("qtype");
				              
				                // use default qtype when selected qtype is invalid 
				                qtype = ((qtype == 0) ? MesQueues.Q_CHARGEBACKS_AMEX : qtype);
				                if( !oldCaseNum.equals(ffr.getFieldData("current_case_number").trim()) ) {    	// *CBNSP Field 8
				                	QueueNotes.addNote(loadSec,qtype,"system","Case number updated to " + ffr.getFieldData("current_case_number").trim());
				                }
				              
				                // if there was note data for this chargeback, add note
				                if ( !"".equals(note) ) {
				                	QueueNotes.addNote(loadSec,qtype,"system",note);
				                }
	    					}
	    				}   else if ( !"".equals(adjRefNum) )  { // If this caused an adjustment, create a new chargeback
	            
	    					loadSec = ChargebackTools.getNewLoadSec();
	    					String cmAcctNumb = ffr.getFieldData("cm_acct_numb").trim();

	    					pStmt2 = con.prepareStatement( SQL_INSERT_AMEX_OPTB_CB.toString() );	    					
	    					paramIndex = 1;

    	    				pStmt2.setLong( paramIndex++, loadSec ); 
    	    				//populating merchant_number field with seller_id. This gets updated if it finds a record in amex_settlement(TEM1-961- Fix for merchant_number not populating)
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("seller_id").trim() );  		// This needs to be looked up using date_of_charge, billed_amount, se_numb and cm_acct_numb *TODO - After settlement is done
    	    				pStmt2.setDate( paramIndex++, incomingDate );  										
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("current_case_number").trim() );  		// case_number
    	    				pStmt2.setNull(paramIndex++, Types.VARCHAR); 											// status  				
    	    				pStmt2.setString( paramIndex++, ByteUtil.hexStringN(MesEncryption.getClient().encrypt(cmAcctNumb.getBytes())) );  		
    	    				//pStmt2.setString( paramIndex++, ffr.getFieldData("acq_reference_number").trim() );	// This needs to be looked up using date_of_charge, billed_amount, se_numb and cm_acct_numb  *TODO - After settlement is done
    	    				//pStmt2.setString( paramIndex++, ffr.getFieldData("settlement_rec_id").trim() );  		// This needs to be looked up using date_of_charge, billed_amount, se_numb and cm_acct_numb	 *TODO - After settlement is done
    	    				pStmt2.setString( paramIndex++, "0A" );													// mes_ref_num set to 0A for legacy purposes.  			
    	    				pStmt2.setString( paramIndex++, loadFilename );  		
    	    				pStmt2.setLong( paramIndex++, loadFileId );  		
    	    				//pStmt2.setString( paramIndex++, ffr.getFieldData("settlement_date").trim() );  		// This gets updated via trigger AMEX_SETTLEMENT_RECON.AMEX_SETTLE_RECON_BIFER
    	    				//pStmt2.setString( paramIndex++, ffr.getFieldData("reversal_date").trim() );  			// This gets updated via ChargebackTools when reversal happens
    	    				//pStmt2.setString( paramIndex++, ffr.getFieldData("reversal_amount").replace(' ','+') );  // This gets updated via ChargebackTools when reversal happens 
    	    				//pStmt2.setString( paramIndex++, ffr.getFieldData("reversal_ref_num").trim() ); 		// This gets updated if a reversal comes in
    	    				//pStmt2.setString( paramIndex++, ffr.getFieldData("bank_number").trim() );  			// This needs to be looked up using date_of_charge, billed_amount, se_numb and cm_acct_numb	 *TODO - After settlement is done
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("rec_type").trim() );  				// *CBNSP Field 1
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("fin_ind").trim() );  					// *CBNSP Field 2
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("se_numb").trim() );  					// *CBNSP Field 4
    	    				pStmt2.setString( paramIndex++, TridentTools.encodeCardNumber(cmAcctNumb));    			// *CBNSP Field 6
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("current_case_number").trim() );  		// *CBNSP Field 8
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("current_action_number").trim() ); 	// *CBNSP Field 9
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("previous_case_number").trim() );  	// *CBNSP Field 10
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("previous_action_number").trim() );  	// *CBNSP Field 11
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("resolution").trim() );  				// *CBNSP Field 12
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("from_system").trim() );  				// *CBNSP Field 13
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("rejects_to_system").trim() );  		// *CBNSP Field 14
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("disputes_to_system").trim() );  		// *CBNSP Field 15
    	    				pStmt2.setDate( paramIndex++, ffr.getFieldAsSqlDate("date_of_adjustment") ); 			// *CBNSP Field 16
    	    				pStmt2.setDate( paramIndex++, ffr.getFieldAsSqlDate("date_of_charge") );  				// *CBNSP Field 17
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("amex_id").trim() );  					// *CBNSP Field 18
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("case_type").trim() );  				// *CBNSP Field 20
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("loc_numb").trim() );  				// *CBNSP Field 21
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cb_reas_code").trim() );  			// *CBNSP Field 22
    	    				if(!"".equals(ffr.getFieldData("cb_amount").trim()))
    	    					pStmt2.setString( paramIndex++, ffr.getFieldData("cb_amount").replace(' ','+') );  	// *CBNSP Field 23
    	    				else
    	    					pStmt2.setString( paramIndex++, "+0000000000000.00" );
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cb_adjustment_number").trim() );  	// *CBNSP Field 24
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cb_resolution_adj_number").trim() ); 	// *CBNSP Field 25
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cb_reference_code").trim() );  		// *CBNSP Field 26
    	    				if(!"".equals(ffr.getFieldData("billed_amount").trim()))
    	    					pStmt2.setString( paramIndex++, ffr.getFieldData("billed_amount").replace(' ','+') );  // *CBNSP Field 28
    	    				else
    	    					pStmt2.setString( paramIndex++, "+0000000000000.00" );
    	    				if(!"".equals(ffr.getFieldData("soc_amount").trim()))
    	    					pStmt2.setString( paramIndex++, ffr.getFieldData("soc_amount").replace(' ','+') );  // *CBNSP Field 29
    	    				else
    	    					pStmt2.setString( paramIndex++, "+0000000000000.00" );  							
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("soc_invoice_number").trim() );  		// *CBNSP Field 30
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("roc_invoice_number").trim() );  		// *CBNSP Field 31
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("foreign_amt").trim() );  				// *CBNSP Field 32
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("currency").trim() );  				// *CBNSP Field 33
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("supp_to_follow").trim() );  			// *CBNSP Field 34
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cm_name1").trim() );  				// *CBNSP Field 35
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cm_name2").trim() );  				// *CBNSP Field 36
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cm_addr1").trim() );  				// *CBNSP Field 37
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cm_addr2").trim() );  				// *CBNSP Field 38
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cm_city_state").trim() );  			// *CBNSP Field 39
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cm_zip").trim() );  					// *CBNSP Field 40
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cm_first_name_1").trim() );  			// *CBNSP Field 41
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cm_middle_name_1").trim() );  		// *CBNSP Field 42
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cm_last_name_1").trim() );  			// *CBNSP Field 43
    	    				pStmt2.setString( paramIndex++, ByteUtil.hexStringN(MesEncryption.getClient().encrypt(ffr.getFieldData("cm_orig_acct_num").trim().getBytes())) );  		// *CBNSP Field 44
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cm_orig_name").trim() );  			// *CBNSP Field 45
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cm_orig_first_name").trim() );  		// *CBNSP Field 46
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cm_orig_middle_name").trim() );  		// *CBNSP Field 47
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cm_orig_last_name").trim() );  		// *CBNSP Field 48
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("note1").trim() );  					// *CBNSP Field 49
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("note2").trim() );  					// *CBNSP Field 50
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("note3").trim() );  					// *CBNSP Field 51
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("note4").trim() );  					// *CBNSP Field 52
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("note5").trim() );  					// *CBNSP Field 53
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("note6").trim() );  					// *CBNSP Field 54
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("note7").trim() );  					// *CBNSP Field 55
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("triumph_seq_no").trim() );  			// *CBNSP Field 56
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("seller_id").trim() );  				// *CBNSP Field 58
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("airline_tkt_num").trim() );  			// *CBNSP Field 60
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("al_sequence_number").trim() );  		// *CBNSP Field 61
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("folio_ref").trim() );  				// *CBNSP Field 62
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("merch_order_num").trim() );  			// *CBNSP Field 63
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("merch_order_date").trim() );  		// *CBNSP Field 64
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("canc_num").trim() );  				// *CBNSP Field 65
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("canc_date").trim() );  				// *CBNSP Field 66
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("fincap_tracking_id").trim() );  		// *CBNSP Field 67
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("fincap_file_seq_num").trim() );   	// *CBNSP Field 68
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("fincap_batch_number").trim() );   	// *CBNSP Field 69
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("fincap_batch_invoice_dt").trim() );   // *CBNSP Field 70
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("label1").trim() );  					// *CBNSP Field 71
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("data1").trim() );  					// *CBNSP Field 72
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("label2").trim() );  					// *CBNSP Field 73
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("data2").trim() );  					// *CBNSP Field 74
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("label3").trim() );  					// *CBNSP Field 75
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("data3").trim() );  					// *CBNSP Field 76
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("label4").trim() );  					// *CBNSP Field 77
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("data4").trim() );  					// *CBNSP Field 78
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("label5").trim() );  					// *CBNSP Field 79
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("data5").trim() );  					// *CBNSP Field 80
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("label6").trim() );  					// *CBNSP Field 81
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("data6").trim() );  					// *CBNSP Field 82
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("label7").trim() );  					// *CBNSP Field 83
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("data7").trim() );  					// *CBNSP Field 84
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("label8").trim() );  					// *CBNSP Field 85
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("data8").trim() );  					// *CBNSP Field 86
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("label9").trim() );  					// *CBNSP Field 87
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("data9").trim() );  					// *CBNSP Field 88
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("label10").trim() );  					// *CBNSP Field 89
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("data10").trim() );  					// *CBNSP Field 90
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("label11").trim() );  					// *CBNSP Field 91
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("data11").trim() );  					// *CBNSP Field 92
    	    				pStmt2.setString( paramIndex++, ByteUtil.hexStringN(MesEncryption.getClient().encrypt(ffr.getFieldData("label12").trim().getBytes())) );  					// *CBNSP Field 93
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("data12").trim() );  					// *CBNSP Field 94
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("ind_form_code").trim() );  			// *CBNSP Field 96
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("ind_ref_number").trim() );  			// *CBNSP Field 97
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("loc_ref_number").trim() );  			// *CBNSP Field 99
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("passenger_name").trim() );  			// *CBNSP Field 100
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("passenger_first_name").trim() );  	// *CBNSP Field 101
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("passenger_middle_name").trim() ); 	// *CBNSP Field 102
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("passenger_last_name").trim() );  		// *CBNSP Field 103
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("se_process_date").trim() );  			// *CBNSP Field 104
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("return_date").trim() );  				// *CBNSP Field 105
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("credit_receipt_number").trim() ); 	// *CBNSP Field 106
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("return_to_name").trim() );  			// *CBNSP Field 107
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("return_to_street").trim() );  		// *CBNSP Field 108
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("card_deposit").trim() );  			// *CBNSP Field 109
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("assured_reservation").trim() );   	// *CBNSP Field 110
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("res_cancelled").trim() );  			// *CBNSP Field 111
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("res_cancelled_date").trim() );    	// *CBNSP Field 112
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("cancel_zone").trim() );  				// *CBNSP Field 113
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("reservation_made_for").trim() );  	// *CBNSP Field 114
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("reservation_location").trim() );  	// *CBNSP Field 115
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("reservation_made_on").trim() );   	// *CBNSP Field 116
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("rental_agreement_number").trim() );   // *CBNSP Field 117
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("merchandise_type").trim() );  		// *CBNSP Field 118
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("merchandise_returned").trim() );  	// *CBNSP Field 119
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("returned_name").trim() );  			// *CBNSP Field 120
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("returned_date").trim() );  			// *CBNSP Field 121
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("returned_how").trim() );  			// *CBNSP Field 122
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("returned_reason").trim() );  			// *CBNSP Field 123
    	    				pStmt2.setString( paramIndex++, ffr.getFieldData("store_credit_received").trim() ); 	// *CBNSP Field 124    	    				
    	    				pStmt2.setString( paramIndex++, SAID);													// This is required while sending the file out
    	    				
    	    				pStmt2.executeUpdate();
	    				}
	    				
	    				//links chargeback to merchant
	    				linkChgbktoMerchant(ffr,loadSec);

	    				// if there were notes with this CBNOT entry, add them to the notes for this queue item
	    				if ( !"".equals(note) ) {
	    					QueueNotes.addNote(loadSec, MesQueues.Q_CHARGEBACKS_AMEX, "system", note);
	    				}
	    			} else if ( REC_TYPE_TRAILER.equals(String.valueOf(recType)) ) {
	    				
	    			} else {
	    				// This is neither a Header or Detail record.
	    				logEntry("loadChargebackFile(" + loadFilename + "," + ffr.getFieldData("current_case_number").trim() + ")","WARNING:  Unable to resolve CBNOT entry");
	    			}
	    		} // if (ffr != null)
	        } // while	      
	    }
	    catch(Exception e) {
	      logEntry("loadChargebackFile(" + loadFilename + ")", e.toString());
	    }
	    finally {
	    	try {
	    		if(in != null) in.close();
	    	} catch ( Exception ignore ) {}
	    	
	    	try	{  
	    	  if (pStmt != null ) pStmt.close(); 
	    	} catch( Exception ignore ) {}
	      
	    	try { 
	    	  if (rs != null ) rs.close(); 
	    	} catch( Exception ignore ) {}
	      
	    	try { 
	    	  if (pStmt2 != null ) pStmt2.close(); 
	    	} catch( Exception ignore ) {}	      
	  	}
	}
	
/*
 * Links Chargeback to merchant
 * Links Chargebacks to transactions (in funding table DDF)
 * Links Chargebacks to credit (in funding table DDF)
 */
   protected void linkChgbktoMerchant( FlatFileRecord ffr, Long loadSec){
	   PreparedStatement pStmt1 = null;
	   PreparedStatement pStmt2 = null;
	   ResultSet rset = null;
	   
	   log.error("linking chargeback load sec- " + loadSec + " to merchant");
	   try{
		   String cmAcctNumb = ffr.getFieldData("cm_acct_numb").trim();
		   String cmOrigAcctNumb = ffr.getFieldData("cm_orig_acct_num").trim();
		   pStmt1 = con.prepareStatement(SQL_SELECT_SETTLE_REC.toString());
		   pStmt1.setString(1, ffr.getFieldData("se_numb").trim());
		   pStmt1.setDate(2, ffr.getFieldAsSqlDate("date_of_charge"));
		   pStmt1.setDate(3, ffr.getFieldAsSqlDate("date_of_charge"));
		   pStmt1.setString(4, cmAcctNumb.substring(0,6) + MASKING_CHARS + cmAcctNumb.substring(cmAcctNumb.length() -4));
		   pStmt1.setString(5, cmOrigAcctNumb.substring(0,6) + MASKING_CHARS + cmOrigAcctNumb.substring(cmOrigAcctNumb.length() -4));
		   pStmt1.setString(6, ffr.getFieldData("ind_ref_number").trim());
		   log.error("values for select stmt are:" +"se_number-" +ffr.getFieldData("se_numb").trim() + "dateofcharge-"+ffr.getFieldAsSqlDate("date_of_charge") +"ind_ref_num-"+ffr.getFieldData("ind_ref_number").trim());
		   rset = pStmt1.executeQuery();
		   if(rset.next()){
			   // Adding extra logging to verify the issue of mid, ref_num not populating
			   log.error("Settlement Record found in AMEX_SETTLEMENT --- Updating the values");
			   log.error("merchant_number is:" + rset.getLong("merchant_number"));
			   log.error("reference_number is:" + rset.getString("reference_number"));
			   pStmt2 = con.prepareStatement(SQL_UPDATE_CHBK_MID.toString());
			   pStmt2.setLong(1, rset.getLong("rec_id") );
			   pStmt2.setLong(2, rset.getLong("merchant_number"));
			   pStmt2.setString(3, rset.getString("reference_number"));
			   pStmt2.setInt(4, rset.getInt("bank_number"));
			   pStmt2.setLong(5, loadSec);
			   pStmt2.executeUpdate();
		   }
		   
	   }catch(Exception e) {
		    log.error(e.getMessage());
			logEntry("linkChgbktoMerchant(FlatFileRecord ffr)",e.toString());
			
		  } finally {
			  try	{  
		    	  if (pStmt1 != null ) pStmt1.close();
		    	  if (pStmt2 != null ) pStmt2.close();
		    	  if(rset != null) rset.close();
		    	} catch( Exception ignore ) {}
		  }
   }
   
   /*
    * updates reversal_date in network_chargebacks table.
    * updates cb_load_sec in amex_settlement_recon_optblue
    */
    protected void updateNwChgbk (Date incomingDate, String reversalRefNum, long loadSec){
	   
	   PreparedStatement pStmt = null;
	   PreparedStatement pStmt1 = null;
	   ResultSet rs = null;
	   log.debug("updating network_chrageback table");
	   try{
		   pStmt = con.prepareStatement(SQL_SELECT_RECON.toString());
           pStmt.setDate(1, incomingDate);
           pStmt.setDate(2, incomingDate);
           pStmt.setString(3, reversalRefNum);
           rs = pStmt.executeQuery();
           //if matching found update reversal_date in network_chargebacks table
           if(rs.next()){
        	   pStmt1 = con.prepareStatement(SQL_UPDATE_NW_CHGBK.toString());
        	   pStmt1.setDate(1, rs.getDate("recon_date"));
        	   pStmt1.setLong(2, loadSec);
        	   pStmt1.executeUpdate();
        	   pStmt1.close();
        	 //if cb_load_Sec is null in amex_settlement_recon_optblue then update load_Sec from network_chargeback_amex_optb table
               if("N".equals(rs.getString("linked"))){
            	   pStmt1 = con.prepareStatement(SQL_UPDATE_RECON.toString());
            	   pStmt1.setLong(1, loadSec);
            	   
            	   String rid = rs.getString("row_id");
            	   System.out.println("rowid:"+rs.getString("row_id"));
            	  
            	   pStmt1.setString(2, rid );
            	   pStmt1.executeUpdate();
            	   pStmt1.close();
               }
           }
	   }catch(Exception e) {
			e.printStackTrace();
		   logEntry("updateNwChgbk (Date incomingDate, String reversalRefNum, long loadSec)",e.toString());
			
		  } finally {
			  try	{  
		    	  if (pStmt != null ) pStmt.close();
		    	  if (pStmt1 != null ) pStmt1.close();
		    	  if(rs != null) rs.close();
		    	} catch( Exception ignore ) {}
		  }
   }
   
	/**
	 * For the given loadFilename it pulls data from NETWORK_CHARGEBACK_AMEX_OPTB into NETWORK_CHARGEBACKS, NETWORK_CHARGEBACK_ACTIVITY and Q_DATA
	 * @param loadFilename
	 */
	protected void loadChargebackQueue( String loadFilename ) {
	    
		  PreparedStatement pStmt = null;
		  							
		  int paramIndex = 1;
		  int colIndex = 1;
		  int count = 0;
		  try {
			  
			  loadFilename = loadFilename.substring(loadFilename.lastIndexOf('/')+1);		// ** TODO: Remove as this for testing files loaclly. In normal flpw this will not be needed **
			  pStmt = con.prepareStatement( SQL_INSERT_NETWORK_CB.toString() );
			  pStmt.setString( paramIndex, loadFilename );
			  count = pStmt.executeUpdate();
			  log.debug( count + " rows inserted in NETWORK_CHARGEBACKS" );
			  
			  handleSplitChargeback(loadFilename);
			  pStmt = con.prepareStatement( SQL_INSERT_NETWORK_CB_ACTIVITY.toString() );
			  pStmt.setString( paramIndex, loadFilename );
			  count = pStmt.executeUpdate();
			  log.debug( count + " rows inserted in NETWORK_CHARGEBACK_ACTIVITY" );

			  pStmt = con.prepareStatement( SQL_INSERT_Q_DATA.toString() );
			  pStmt.setString( paramIndex, loadFilename );
			  count = pStmt.executeUpdate();
			  log.debug( count + " rows inserted in Q_DATA" );

			  // queue the loading of chargeback risk points
			  createProcessTableEntries(loadFilename);	     
		  } catch(Exception e) {
			logEntry("loadChargebackSystem(" + loadFilename + ")",e.toString());
			
		  } finally {
			  try	{  
		    	  if (pStmt != null ) pStmt.close(); 
		    	} catch( Exception ignore ) {}
		  }
	}
	  
	protected void handleSplitChargeback(String loadFilename) {
		final StringBuffer SQL_GET_NEW_CHBK = new StringBuffer("");
		SQL_GET_NEW_CHBK.append(
				"select  cb.load_sec          as cb_load_sec," + NL +
				"	cb.incoming_date          as incoming_date," + NL +
				"   nvl(cb.merchant_number,0) as cb_merchant_number," + NL +
				"   ABS(cb.cb_amount) 	      as cb_amount," + NL +
				"   settlement_rec_id         as lookup_id," + NL +
				"   se_numb                   as amex_se_number," + NL +
				"   date_of_charge            as tran_date" + NL +
				"from network_chargeback_amex_optb cb," + NL +
				"	mif                     mf" + NL +
				"where cb.load_file_id = load_filename_to_load_file_id(?) and" + NL +
				"mf.merchant_number(+) = cb.merchant_number");
		
		final StringBuffer SQL_UPDATE_NETWORK_CB = new StringBuffer("");
		SQL_UPDATE_NETWORK_CB.append(
				"update network_chargebacks " + NL + " set split_funding_ind = 'Y' " + NL + " where cb_load_sec in ( ");

		PreparedStatement pStmt = null;
		ResultSet rs = null;
		PreparedStatement pUpStmt = null;
		List<Long> splitCBList = new ArrayList<Long>();
		log.debug("retrieve from network_chargeback_amex_optb table");
		try {
			
			pStmt = con.prepareStatement(SQL_GET_NEW_CHBK.toString());
			pStmt.setString(1, loadFilename);
			rs = pStmt.executeQuery();
			log.debug(SQL_GET_NEW_CHBK.toString());
			while (rs.next()) {
				boolean isSplit = SplitFundUtil.splitChargebackAmex(rs.getLong("amex_se_number"), rs.getDate("tran_date"),rs.getLong("lookup_id"),
						rs.getLong("cb_load_sec"), rs.getLong("cb_merchant_number"), rs.getDate("incoming_date"),rs.getDouble("cb_amount"));
				log.debug(rs.getLong("cb_load_sec") + " has Split " + isSplit);
				if (isSplit) {
					splitCBList.add(rs.getLong("cb_load_sec"));
				}
			}
			if (splitCBList.size() > 0) {
				for (int i = 0; i < splitCBList.size(); i++) {
					SQL_UPDATE_NETWORK_CB.append(splitCBList.get(i));
					// don't need the comma for the first cb_load_seq or the last
					if (i < splitCBList.size() - 1) {
						SQL_UPDATE_NETWORK_CB.append(",");
					}
				}
				SQL_UPDATE_NETWORK_CB.append(" )");
				log.debug("update chargebacks with split_funding_ind to true");
				pUpStmt = con.prepareStatement(SQL_UPDATE_NETWORK_CB.toString());
				pUpStmt.executeUpdate();
			}
		} catch (Exception e) {
			log.error("Error.", e);
			logEntry("handleSplitChargeback (String loadFilename)", e.toString());

		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pStmt != null)
					pStmt.close();
				if (pUpStmt != null)
					pUpStmt.close();
			} catch (Exception ignore) {
			}
		}
	}

	/*
	 * INQ02 - Chargeback Notifications
	 * 
	 */
	protected void loadINQ02(String loadFilename) {
		loadRetrievalFile( loadFilename );
		loadRetrievalQueue( loadFilename );		
	}

	protected void loadRetrievalFile( String inputFilename ) {
		
		HashMap<String, FlatFileRecord> flatFileDefnsINQ02
									= new HashMap<String, FlatFileRecord>();
		long loadFileId = 0L;
	    String loadFilename = null;
	    BufferedReader in = null;
	    String line = null;
	    FlatFileRecord ffr = null;
	    Date incomingDate = null;
	    long loadSecOrig = 0L;
	    int typeOrig = 0;
	    long foreignAmount = 0L;
	    String recKey = null;
	    char recType = '\0';
	    String SAID = "";
	    
	    PreparedStatement pStmt = null;
		ResultSet rs = null;
				
		int paramIndex = 1;
	    int colIndex = 1;
	    
		try {
			flatFileDefnsINQ02.put( "H"      , new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_INQ02_HDR ) );
	    	flatFileDefnsINQ02.put( "D-AIRDS", new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_INQ02_AIRDS ) );
	    	flatFileDefnsINQ02.put( "D-AIRLT", new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_INQ02_AIRLT ) );
	    	flatFileDefnsINQ02.put( "D-AIRRT", new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_INQ02_AIRRT ) );
	    	flatFileDefnsINQ02.put( "D-AIRTB", new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_INQ02_AIRTB ) );
	    	flatFileDefnsINQ02.put( "D-AREXS", new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_INQ02_AREXS ) );
	    	flatFileDefnsINQ02.put( "D-CARRD", new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_INQ02_CARRD ) );
	    	flatFileDefnsINQ02.put( "D-GSDIS", new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_INQ02_GSDIS ) );
	    	flatFileDefnsINQ02.put( "D-NAXMG", new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_INQ02_NAXMG ) );
	    	flatFileDefnsINQ02.put( "D-NAXMR", new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_INQ02_NAXMR ) );
	    	flatFileDefnsINQ02.put( "D-SEDIS", new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_INQ02_SEDIS ) );
	    	flatFileDefnsINQ02.put( "D-FRAUD", new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_INQ02_SEDIS ) );
	    	flatFileDefnsINQ02.put( "D-CRCDW", new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_INQ02_SEDIS ) );
	    	flatFileDefnsINQ02.put( "T"      , new FlatFileRecord( Ctx, MesFlatFiles.DEF_TYPE_AMEX_OPTBLUE_INQ02_TRL ) );

	    	loadFilename  = FileUtils.getNameFromFullPath(inputFilename).toLowerCase();
	    	if(isTestMode)
	    		loadFilename = loadFilename.substring(loadFilename.lastIndexOf('/')+1);		// ** Remove TODO **
	    	loadFileId = loadFilenameToLoadFileId(loadFilename);

	    	in = new BufferedReader( new FileReader(inputFilename) );

	    	while( (line = in.readLine()) != null ) {
	    		paramIndex = 1;
	    		colIndex = 1;

	    		recType = line.charAt(0);
	        	recKey  = (recType == 'D' ? ( recType + "-" + line.substring(60,65)) : String.valueOf( recType ));

	        	ffr = flatFileDefnsINQ02.get( recKey );

	        	if ( ffr != null ) {
	        		ffr.resetAllFields();
	        		ffr.suck(line);
	        		
	        		if ( recType == 'H' ) {
	        			pStmt = con.prepareStatement( SQL_GET_INCOMING_DATE_INQO2.toString() );
	       			  	pStmt.setString( paramIndex, ffr.getFieldData("stars_creation_date") );
	       			  	
	       			  	rs = pStmt.executeQuery();
	       			  	if(rs.next())
	       			  		incomingDate = rs.getDate(colIndex);
	       			  	
	       			  	rs.close();
	    				pStmt.close();
	    				
	       			  	SAID = ffr.getFieldData("said");
	       			  	
	        		} else if ( recType == 'D' ) {
	        			long loadSec = ChargebackTools.getNewLoadSec();
	        			
	        			pStmt = con.prepareStatement( SQL_CHECK_IF_EXISTING_RETR.toString() );
	       			  	pStmt.setInt( paramIndex++, MesQueues.Q_RETRIEVALS_AMEX_INCOMING );
	       			  	pStmt.setString( paramIndex++, ffr.getFieldData("inquiry_case_number").trim() );
	       			  	// At some point maybe the check on card number can be introduced
	       			  	//ffd.getFieldData("cardmember_account_number_19").substring(1,7) + "xxxxxx" + ffd.getFieldData("cardmember_account_number_19").substring(13)).trim();
	       			  	pStmt.setInt( paramIndex++, MesQueues.Q_ITEM_TYPE_RETRIEVAL );
	       			  	
	       			  	rs = pStmt.executeQuery();

	       			  	if ( rs.next() ) {
	       			  		loadSecOrig = rs.getLong("load_sec");
	       			  		typeOrig    = rs.getInt("queue_type");
	       			  	} else {
	       			  		loadSecOrig = 0L;
	       			  	}
	       			  	
			            String  numberStr = "";
			            String  tempStr = ffr.getFieldData("foreign_amount").trim();
			            for( int i = 0; i < tempStr.length(); ++i ) {
			            	if ( Character.isDigit( tempStr.charAt(i) ) ) {
			            		numberStr += tempStr.charAt(i);
			            	}
			            }
			            
			            if ( numberStr.length() > 0 ) {
			              foreignAmount = Long.parseLong(numberStr);
			            }
			            
			            String cmAcctNumb = ffr.getFieldData("cardmember_account_number").trim();
			            String cmOrigAcctNumb = ffr.getFieldData("cardmember_original_number").trim();
			            String cmAcctNumb19 = ffr.getFieldData("cardmember_account_number_19").trim();
			            
			            paramIndex = 1;
			    		colIndex = 1;
			    		
			    		pStmt = con.prepareStatement( SQL_INSERT_AMEX_OPTB_RETR.toString() );
			    		
			    		pStmt.setLong( paramIndex++, loadSec );
	    				//pStmt2.setString( paramIndex++, ffr.getFieldData("merchant_number").trim() );  		// This needs to be looked up using se_number (Field 21), date_of_charge (Field 12), reference_number (Field 45), charge_amount (Field 43) *TODO - After settlement is done
			    		//pStmt2.setString( paramIndex++, ffr.getFieldData("acq_reference_number").trim() );	// This needs to be looked up using se_number (Field 21), date_of_charge (Field 12), reference_number (Field 45), charge_amount (Field 43) *TODO - After settlement is done
			    		//pStmt2.setString( paramIndex++, ffr.getFieldData("settlement_rec_id").trim() );		// This needs to be looked up using se_number (Field 21), date_of_charge (Field 12), reference_number (Field 45), charge_amount (Field 43) *TODO - After settlement is done
			    		//pStmt2.setString( paramIndex++, ffr.getFieldData("mes_ref_num").trim() );				// This is redundant
			    		pStmt.setString( paramIndex++, loadFilename );  		
	    				pStmt.setLong( paramIndex++, loadFileId );  	
	    				pStmt.setDate( paramIndex++, incomingDate );
	    				if(!"".equals(cmAcctNumb))
	    					pStmt.setString( paramIndex++, ByteUtil.hexStringN(MesEncryption.getClient().encrypt(cmAcctNumb.getBytes())) );
	    				else 
	    					pStmt.setString( paramIndex++, ByteUtil.hexStringN(MesEncryption.getClient().encrypt(cmAcctNumb19.getBytes())) );
	    				pStmt.setString( paramIndex++, ffr.getFieldData("rec_type").trim());						// *INQ02 Field 2
	    				pStmt.setString( paramIndex++, TridentTools.encodeCardNumber(cmAcctNumb) );    				// *INQ02 Field 3	    				
	    				pStmt.setString( paramIndex++, ffr.getFieldData("inquiry_case_number").trim());				// *INQ02 Field 4
	    				pStmt.setString( paramIndex++, ffr.getFieldData("airline_ticket_number").trim());			// *INQ02 Field 5
	    				pStmt.setString( paramIndex++, ffr.getFieldData("amex_process_date").trim());				// *INQ02 Field 7
	    				pStmt.setString( paramIndex++, ffr.getFieldData("amex_id").trim());							// *INQ02 Field 8
	    				pStmt.setString( paramIndex++, ffr.getFieldData("case_type").trim());						// *INQ02 Field 9
	    				pStmt.setDate( paramIndex++, ffr.getFieldAsSqlDate("se_reply_by_date"));					// *INQ02 Field 10
	    				pStmt.setString( paramIndex++, ffr.getFieldData("location_number").trim());					// *INQ02 Field 11
	    				pStmt.setDate( paramIndex++, ffr.getFieldAsSqlDate("date_of_charge"));						// *INQ02 Field 12	    				
	    				pStmt.setString( paramIndex++, ffr.getFieldData("cardmember_name_1").trim());				// *INQ02 Field 13
	    				pStmt.setString( paramIndex++, ffr.getFieldData("cardmember_name_2").trim());				// *INQ02 Field 14	
	    				pStmt.setString( paramIndex++, ffr.getFieldData("seller_id").trim());						// *INQ02 Field 15
	    				pStmt.setString( paramIndex++, TridentTools.encodeCardNumber(cmAcctNumb19));				// *INQ02 Field 17
	    				pStmt.setString( paramIndex++, ffr.getFieldData("cardmember_city_province").trim());		// *INQ02 Field 19
	    				pStmt.setString( paramIndex++, ffr.getFieldData("cardmember_postal_code").trim());			// *INQ02 Field 20
	    				pStmt.setString( paramIndex++, ffr.getFieldData("se_number").trim());						// *INQ02 Field 21
	    				pStmt.setString( paramIndex++, ffr.getFieldData("inquiry_reason_code").trim());				// *INQ02 Field 22
	    				pStmt.setString( paramIndex++, ffr.getFieldData("follow_up_reason_code").trim());			// *INQ02 Field 23
	    				pStmt.setString( paramIndex++, ffr.getFieldData("foreign_amount").trim());					// *INQ02 Field 24
	    				pStmt.setString( paramIndex++, ffr.getFieldData("support_to_follow").trim());				// *INQ02 Field 26
	    				pStmt.setString( paramIndex++, ffr.getFieldData("inquiry_action_number").trim());			// *INQ02 Field 27	
	    				pStmt.setString( paramIndex++, ffr.getFieldData("cardmember_first_name_1").trim());			// *INQ02 Field 28
	    				pStmt.setString( paramIndex++, ffr.getFieldData("cardmember_middle_name_1").trim());		// *INQ02 Field 29
	    				pStmt.setString( paramIndex++, ffr.getFieldData("cardmember_last_name_1").trim());			// *INQ02 Field 30	    				
	    				pStmt.setString( paramIndex++, TridentTools.encodeCardNumber(cmOrigAcctNumb) );    			// *INQ02 Field 31	
	    				pStmt.setString( paramIndex++, ffr.getFieldData("cardmember_original_name").trim());		// *INQ02 Field 32
	    				pStmt.setString( paramIndex++, ffr.getFieldData("inquiry_note_1").trim());					// *INQ02 Field 33
	    				pStmt.setString( paramIndex++, ffr.getFieldData("inquiry_note_2").trim());					// *INQ02 Field 34
	    				pStmt.setString( paramIndex++, ffr.getFieldData("inquiry_note_3").trim());					// *INQ02 Field 35
	    				pStmt.setString( paramIndex++, ffr.getFieldData("cardmember_orig_first_name").trim());		// *INQ02 Field 36
	    				pStmt.setString( paramIndex++, ffr.getFieldData("cardmember_orig_middle_name").trim());		// *INQ02 Field 37	
	    				pStmt.setString( paramIndex++, ffr.getFieldData("cardmember_orig_last_name").trim());		// *INQ02 Field 38
	    				pStmt.setString( paramIndex++, ffr.getFieldData("inquiry_note_4").trim());					// *INQ02 Field 39
	    				pStmt.setString( paramIndex++, ffr.getFieldData("inquiry_note_5").trim());					// *INQ02 Field 40
	    				pStmt.setString( paramIndex++, ffr.getFieldData("inquiry_note_6").trim());					// *INQ02 Field 41
	    				pStmt.setString( paramIndex++, ffr.getFieldData("inquiry_note_7").trim());					// *INQ02 Field 42
	    				
	    				// These 3 are common across all case types
	    				pStmt.setDouble( paramIndex++, ffr.getFieldAsDouble("charge_amount") );  					// *INQ02 Field 43
	    				pStmt.setDouble( paramIndex++, ffr.getFieldAsDouble("disputed_amount") ); 					// *INQ02 Field 44	    				    				
	    				pStmt.setString( paramIndex++, ffr.getFieldData("reference_number").trim());				// *INQ02 Field 45

	    				// The following fields are recieved based on the case type
	    				pStmt.setString( paramIndex++, ffr.getFieldData("soc_invoice_number").trim());				// *INQ02 Field 46 - AIRDS, AIRLT, AIRRT, AIRTB, CARRD, NAXMG
	    				pStmt.setString( paramIndex++, ffr.getFieldData("sequence_indicator").trim());				// *INQ02 AIRTB Field 47, AIRRT Field 47, AIRLT Field 47, AIRDS Field 47
	    				pStmt.setString( paramIndex++, ffr.getFieldData("passenger_name").trim());					// *INQ02 AIRTB Field 48, AIRRT Field 48, AIRLT Field 48, AIRDS Field 48
	    				pStmt.setString( paramIndex++, ffr.getFieldData("passenger_first_name").trim());			// *INQ02 AIRTB Field 49, AIRRT Field 49, AIRLT Field 49, AIRDS Field 49
	    				pStmt.setString( paramIndex++, ffr.getFieldData("passenger_middle_name").trim());			// *INQ02 AIRTB Field 50, AIRRT Field 50, AIRLT Field 50, AIRDS Field 50
	    				pStmt.setString( paramIndex++, ffr.getFieldData("passenger_last_name").trim());				// *INQ02 AIRTB Field 51, AIRRT Field 51, AIRLT Field 51, AIRDS Field 51
	    				pStmt.setString( paramIndex++, ffr.getFieldData("se_process_date").trim());					// *INQ02 AIRRT Field 52, AIRLT Field 52, AIRDS Field 52
	    				
	    				if("SEDIS".equals(ffr.getFieldData("case_type").trim())) {
	    					pStmt.setString( paramIndex++, ffr.getFieldData("ind_form_code_1").trim());				// *INQ02 SEDIS Field 47
	    					pStmt.setString( paramIndex++, ffr.getFieldData("ind_ref_number_1").trim());			// *INQ02 SEDIS Field 48
	    					pStmt.setString( paramIndex++, ffr.getFieldData("loc_ref_number_1").trim());			// *INQ02 SEDIS Field 50
	    				} else {
	    					pStmt.setString( paramIndex++, ffr.getFieldData("ind_form_code").trim());				// *INQ02 NAXMR Field 57, NAXMG Field 49, GSDIS Field 47, CARRD Field 50, AREXS Field 58, AIRTB Field 53, AIRRT Field 57, AIRLT Field 55, AIRDS Field 54
		    				pStmt.setString( paramIndex++, ffr.getFieldData("ind_ref_number").trim());				// *INQ02 NAXMR Field 58, NAXMG Field 50, GSDIS Field 48, CARRD Field 51, AREXS Field 59, AIRTB Field 54, AIRRT Field 58, AIRLT Field 56, AIRDS Field 55
		    				pStmt.setString( paramIndex++, ffr.getFieldData("loc_ref_number").trim());				// *INQ02 NAXMR Field 60, NAXMG Field 52, GSDIS Field 50, AREXS Field 61, AIRTB Field 56, AIRRT Field 60, AIRLT Field 58, AIRDS Field 57
	    				}
	    				
	    				pStmt.setString( paramIndex++, ffr.getFieldData("inquiry_mark").trim());					// *INQ02 NAXMR Field 63, NAXMG Field 55, GSDIS Field 53, CARRD Field 54, AREXS Field 64, AIRTB Field 59, AIRRT Field 63, AIRLT Field 61, AIRDS Field 60
	    				pStmt.setString( paramIndex++, ffr.getFieldData("lta_filed_date").trim());					// *INQ02 AIRLT Field 53
	    				pStmt.setString( paramIndex++, ffr.getFieldData("return_date").trim());						// *INQ02 AIRRT Field 53
	    				pStmt.setString( paramIndex++, ffr.getFieldData("credit_receipt_number").trim());			// *INQ02 NAXMR Field 55, AIRRT Field 54
	    				pStmt.setString( paramIndex++, ffr.getFieldData("return_to_name").trim());					// *INQ02 AIRRT Field 55
	    				pStmt.setString( paramIndex++, ffr.getFieldData("return_to_street").trim());				// *INQ02 AIRRT Field 56
	    				pStmt.setString( paramIndex++, ffr.getFieldData("card_deposit").trim());					// *INQ02 AREXS Field 46
	    				pStmt.setString( paramIndex++, ffr.getFieldData("card_canc_number").trim());				// *INQ02 AREXS Field 47
	    				pStmt.setString( paramIndex++, ffr.getFieldData("assured_reservation").trim());				// *INQ02 AREXS Field 48
	    				pStmt.setString( paramIndex++, ffr.getFieldData("res_cancelled").trim());					// *INQ02 AREXS Field 49
	    				pStmt.setString( paramIndex++, ffr.getFieldData("res_cancelled_date").trim());				// *INQ02 AREXS Field 50
	    				pStmt.setString( paramIndex++, ffr.getFieldData("res_cancelled_time").trim());				// *INQ02 AREXS Field 51
	    				pStmt.setString( paramIndex++, ffr.getFieldData("cancel_zone").trim());						// *INQ02 AREXS Field 52
	    				pStmt.setString( paramIndex++, ffr.getFieldData("reservation_made_for").trim());			// *INQ02 AREXS Field 53
	    				pStmt.setString( paramIndex++, ffr.getFieldData("reservation_location").trim());			// *INQ02 AREXS Field 54
	    				pStmt.setString( paramIndex++, ffr.getFieldData("reservation_made_on").trim());				// *INQ02 AREXS Field 55
	    				pStmt.setString( paramIndex++, ffr.getFieldData("folio_ref_number").trim());				// *INQ02 SEDIS Field 46, AREXS Field 56 
	    				pStmt.setString( paramIndex++, ffr.getFieldData("rental_agreement_needed").trim());			// *INQ02 CARRD Field 47
	    				pStmt.setString( paramIndex++, ffr.getFieldData("rental_agreement_number").trim());			// *INQ02 CARRD Field 48
	    				pStmt.setString( paramIndex++, ffr.getFieldData("merchandise_type").trim());				// *INQ02 NAXMR Field 46, NAXMG Field 46
	    				pStmt.setString( paramIndex++, ffr.getFieldData("order_number").trim());					// *INQ02 NAXMR Field 56, NAXMG Field 47
	    				pStmt.setString( paramIndex++, ffr.getFieldData("merchandise_returned").trim());			// *INQ02 NAXMR Field 47
	    				pStmt.setString( paramIndex++, ffr.getFieldData("credit_requested").trim());				// *INQ02 NAXMR Field 48
	    				pStmt.setString( paramIndex++, ffr.getFieldData("replacement_requested").trim());			// *INQ02 NAXMR Field 49
	    				pStmt.setString( paramIndex++, ffr.getFieldData("returned_name").trim());					// *INQ02 NAXMR Field 50
	    				pStmt.setString( paramIndex++, ffr.getFieldData("returned_date").trim());					// *INQ02 NAXMR Field 51
	    				pStmt.setString( paramIndex++, ffr.getFieldData("returned_how").trim());					// *INQ02 NAXMR Field 52
	    				pStmt.setString( paramIndex++, ffr.getFieldData("returned_reason").trim());					// *INQ02 NAXMR Field 53
	    				pStmt.setString( paramIndex++, ffr.getFieldData("store_credit_received").trim());			// *INQ02 NAXMR Field 54	    				
	    				pStmt.setString( paramIndex++, ffr.getFieldData("ind_form_code_2").trim());					// *INQ02 SEDIS Field 52
	    				pStmt.setString( paramIndex++, ffr.getFieldData("ind_ref_number_2").trim());				// *INQ02 SEDIS Field 53
	    				pStmt.setString( paramIndex++, ffr.getFieldData("loc_ref_number_2").trim());				// *INQ02 SEDIS Field 55
	    				
	    				pStmt.setString( paramIndex++, SAID);														// This is required for outgoing file

	    				int count = pStmt.executeUpdate();
	    				log.debug( count + " rows inserted in NETWORK_RETRIEVAL_AMEX_OPTB" );
	    				
	    				//Downstream INQ02 - links retrievals to transactions from amex_settlement
	    				updateRetrievals(ffr, loadSec);

	    				if ( loadSecOrig != 0L )	{
	    					// copy the notes from the old to the new
	    					QueueNotes.copyNotes(loadSecOrig,MesQueues.Q_RETRIEVALS_AMEX_INCOMING,loadSec);
		
			              // add note indicating the second entry to both original and new
			              QueueNotes.addNote(loadSecOrig,typeOrig,"system","Second request received, new control number is " + loadSec);
			              QueueNotes.addNote(loadSec,MesQueues.Q_RETRIEVALS_AMEX_INCOMING,"system","Second request, original control number is " + loadSecOrig);
	    				}
	        		}   // end else if ( recType == 'D' )
	        	}
	    	}
	    }
	    catch(Exception e) {
	      logEntry("loadRetrievalFile("+loadFilename+")",e.toString());
	    }
	    finally {
	    	try { 
	    		if (pStmt != null) 
	    			pStmt.close(); 
	    	} catch( Exception e ) {}
	    	
	    	try { 
	    		if (rs != null) 
	    			rs.close(); 
	    	} catch( Exception e ) {}
	    	
	    	try { 
	    		in.close(); 
	    	} catch( Exception e ) {}
	    }
	}

	// downstream process of linking retrievals to settlement record
	protected void updateRetrievals(FlatFileRecord ffr, long loadSec){
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		ResultSet rs = null;
		log.debug("updating retrieval information");
		try{
			String cmAcctNumb = ffr.getFieldData("cardmember_account_number").trim();
			ps = con.prepareStatement(SQL_SELECT_SETTLE_INQ1.toString());
			ps.setString(1, ffr.getFieldData("se_number")  );
			ps.setDate  (2, ffr.getFieldAsSqlDate("date_of_charge")  );
			ps.setDate  (3, ffr.getFieldAsSqlDate("date_of_charge")  );
			ps.setString(4, ffr.getFieldData("reference_number").trim());
			ps.setDouble(5, ffr.getFieldAsDouble("charge_amount")  );
			ps.setDouble(6, ffr.getFieldAsDouble("charge_amount")  );
			rs = ps.executeQuery();
			//if found, link retrieval
			if(rs.next()){
				linkRetrMerch(rs, loadSec);
			} else {
				//if not found, try again with card number
				ps = con.prepareStatement(SQL_SELECT_SETTLE_INQ2.toString());
				ps.setString(1, ffr.getFieldData("se_number")  );
				ps.setDate  (2, ffr.getFieldAsSqlDate("date_of_charge")  );
				ps.setDate  (3, ffr.getFieldAsSqlDate("date_of_charge")  );
				ps.setString(4, cmAcctNumb.substring(0,6) + "xxxxxx" + cmAcctNumb.substring(cmAcctNumb.length() -4));
				ps.setDouble(5, ffr.getFieldAsDouble("charge_amount")  );
				rs = ps.executeQuery();
				if(rs.next()){
					linkRetrMerch(rs, loadSec);
				} else {
					log.debug("no matching found in settlement for loadsec:" + loadSec );
				}
			}

		}catch(Exception e){
			logEntry("updateRetrievals(FlatFileRecord, long)",e.toString());
		}finally {
			  try	{
		    	  if (ps != null ) ps.close();
		    	  if (ps1 != null ) ps1.close();
		    	  if(rs != null) rs.close();
		    	} catch( Exception ignore ) {}
		  }


	}
	
	//updates settlement_rec_id, merchant_number and acq_reference_number columns in network_retrieval_amex_optb table
	protected void linkRetrMerch(ResultSet rs, long loadSec){
		PreparedStatement ps = null;
		try{
			ps = con.prepareStatement(SQL_UPDATE_RETR.toString());
			ps.setLong(1, rs.getLong("rec_id") );
			ps.setLong(2, rs.getLong("merchant_number"));
			ps.setString(3, rs.getString("reference_number"));
			ps.setLong(4, loadSec);
			ps.executeUpdate();

		}catch(Exception e){
			logEntry("linkRetrMerch(ResultSet, long)",e.toString());
		}finally {
			  try	{
		    	  if (ps != null ) ps.close();
		    	} catch( Exception ignore ) {}
		  }

	}

	
	protected void loadRetrievalQueue( String loadFilename ) {
		
		PreparedStatement pStmt = null;			
		int paramIndex = 1;
		int count = 0;
		    
		try {
			  
			loadFilename = loadFilename.substring(loadFilename.lastIndexOf('/')+1);		// ** TODO: Remove as this for testing files loaclly. In normal flpw this will not be needed **
			  
			pStmt = con.prepareStatement( SQL_INSERT_NETWORK_RETR.toString() );
			pStmt.setString( paramIndex, loadFilename );
			count = pStmt.executeUpdate();
			log.debug( count + " rows inserted in NETWORK_RETRIEVALS" );

			// Retrievals are put into the MES queue system by a trigger on the network_retrievals table

			// queue the retrieval points and the linking of transaction and credits
			createProcessTableEntries(loadFilename);
	    }
	    catch(Exception e) {	      
	      logEntry("loadRetrievalQueue(" + loadFilename + ")", e.toString());
	      
	    }
	    finally {
	    	try { 
	    		if (pStmt != null) 
	    			pStmt.close(); 
	    	} catch( Exception e ) {}	    	
	    }		
	}
	
	
	/*
	 * Helper functions
	 */
	protected Sftp  getAmexSFTPConnection() throws Exception {
		Sftp sftp =  getSftp(	
						MesDefaults.getString( MesDefaults.DK_AMEX_HOST ),
						MesDefaults.getString( MesDefaults.DK_AMEX_USER ),
						MesDefaults.getString( MesDefaults.DK_AMEX_PASSWORD ),
						MesDefaults.getString( MesDefaults.DK_AMEX_INCOMING_PATH ), 
						false // !binary
					);
				
		return sftp;
	}
	
	/**
	 * Builds the chargeback notes from Field 49 to Field 55 (Note1 to Note7)
	 * @param ffr
	 * @return
	 */	
	protected String buildChargebackNote( FlatFileRecord ffr ) {
		StringBuffer chargebackNotes = new StringBuffer("");
	    
	    for( int i = 1; i < 8; ++i ) {
	    	String noteFragment = ffr.getFieldData("note"+i).trim();
	    	
	    	if( !"".equals(noteFragment) ) {
	    		chargebackNotes.append( noteFragment );
	        
	    		if ( noteFragment.length() < ffr.getFieldLength("note"+i) ) {
	    			chargebackNotes.append( NL );
	    		}
	    	}
	    }
	    
	    return( chargebackNotes.toString() );
	}
	
	/**
	 * Notifications to Dispute department regarding the load
	 * @param loadFilename
	 */
	protected void notify( String loadFilename ) {
		
	    PreparedStatement pStmt = null;	    
	    ResultSet rs = null;
	    
	    int paramIndex = 1;
	    int colIndex = 1;
	    
	    int addrId        = -1;
	    String subject = null;
	    String query = null;

	    try {	    	
	    	
	      if ( loadFilename.startsWith( CB_FILE_PREFIX ) ) {
	        addrId    = MesEmails.MSG_ADDRS_AMEX_CB_NOTIFY;
	        subject   = "Incoming Amex OPTB Chargebacks";
	        query = GET_INCOMING_CB_COUNT.toString();
	        
	      }
	      else if ( loadFilename.startsWith( RET_FILE_PREFIX ) ) {
	        addrId = MesEmails.MSG_ADDRS_AMEX_RETR_NOTIFY;
	        subject = "Incoming Amex OPTB Retrievals";
	        query = GET_INCOMING_RET_COUNT.toString();
	        
	      }

	      pStmt = con.prepareStatement( query );
	      pStmt.setString(paramIndex, loadFilename);
	      
	      rs = pStmt.executeQuery();

	      StringBuffer buffer = new StringBuffer();
	      buffer.append( StringUtilities.leftJustify("SE Number", 16,' ') );
	      buffer.append( StringUtilities.rightJustify("Count"    , 9,' ') );
	      buffer.append( StringUtilities.rightJustify("Amount"   ,15,' ') );
	      buffer.append("\n");
	      buffer.append( StringUtilities.leftJustify(""         , 40,'=') );
	      buffer.append("\n");

	      while( rs.next() ) {
	          buffer.append( StringUtilities.leftJustify( rs.getString("se_number")   ,16,' ') );
	          buffer.append( StringUtilities.rightJustify( rs.getString("total_count") , 9,' ') );
	          buffer.append( StringUtilities.rightJustify( MesMath.toCurrency( rs.getDouble("total_amount")), 15,' ') );
	          buffer.append("\n");
	      }
	      rs.close();

	      MailMessage msg = new MailMessage();
	      msg.setAddresses(addrId);
	      msg.setSubject(subject + " - " + loadFilename);
	      msg.setText(buffer.toString());
	      msg.send();
	      
	      log.debug("notify : subject : " + subject + " - " + loadFilename);
	      log.debug("notify : body : " + buffer.toString());
	      
	    } catch( Exception e ) {
	      logEntry("notify(" + loadFilename + ")",e.toString());
	      
	    }
	    finally {
	    	try { 
	    		if (pStmt != null) 
	    			pStmt.close(); 
	    	} catch( Exception e ) {}
	    	
	    }
	}
		
	private void createProcessTableEntries(String loadFileName){
		PreparedStatement pStmt1 = null;
		try{
		pStmt1 = con.prepareStatement(SQL_CREATE_PROCESS_TABLE_ENTRIES.toString());
		pStmt1.setLong(1, RISK_PROCESS_SEQUENCE);
		pStmt1.setInt(2, RISK_PROCESS_TYPE);
		pStmt1.setString(3, loadFileName);
		pStmt1.executeUpdate();
		log.info("Created a ProcessTableEntry in risk_process table for merchant notification");
		}catch(Exception e){
			logEntry("createProcessTableEntries(String)",e.toString());
		}finally {
			  try	{
		    	  if (pStmt1 != null ) pStmt1.close();
		    	} catch( Exception ignore ) {}
		}
	}
	
	public static void main(String args[]) {
		AmexOptblueDisputesLoader disputesLoader = null;
		int idx = 0;
		
		try {
			if (args.length > 0 && args[idx].equals("printtestprops")) {
					EventBase.printKeyListStatus(new String[] { MesDefaults.DK_AMEX_HOST, MesDefaults.DK_AMEX_USER,
							MesDefaults.DK_AMEX_PASSWORD, MesDefaults.DK_AMEX_INCOMING_PATH });				
			} else if (args.length > 0 && args[idx].equals("test")) {
				isTestMode = true;
				idx = 1;
			}

			disputesLoader = new AmexOptblueDisputesLoader();
			disputesLoader.connect();
							
			if ("loadCBNSP".equals(args[idx])) {
				disputesLoader.loadCBNSP(args[++idx]);
					
			} else if ("loadINQ02".equals(args[idx])) {
				disputesLoader.loadINQ02(args[++idx]);
					
			} else if ("execute".equals(args[idx])) {
				disputesLoader.setEventArgs(args[++idx]);
				disputesLoader.execute();
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				disputesLoader.cleanUp();
			} catch (Exception e) { }
						
		}
		
		Runtime.getRuntime().exit(0);
	}
	
	// Load Types
	private final static String	CBNSP = "CBNSP";
	private final static String INQ02 = "INQ02";
	
	// Record Types
	private final static String REC_TYPE_HEADER = "H";
	private final static String REC_TYPE_DETAIL = "D";
	private final static String REC_TYPE_TRAILER = "T";
			
	// Mes Defined Dispute File Prefixes
	private static String CB_FILE_PREFIX = "amex_optb_cb";
	private static String RET_FILE_PREFIX = "amex_optb_ret";
	
	private static final Map<String, String> DisputeFilePrefixes = new HashMap<String, String>() {
		{
			put(CBNSP, CB_FILE_PREFIX ); 	// Amex Optblue Incoming Chargebacks File
			put(INQ02, RET_FILE_PREFIX ); 	// Amex Optblue Incoming Inquiries File
		}
	};
	
	// Amex Defined File Prefixes
	private final static String PROD_PREFIX = "MERCHESOBPRD.";
	private final static String TEST_PREFIX = "MERCHESOBTST.";
	
	// Loading mode
	private static boolean isTestMode = false;

	
	// ************** SQL Queries
	
	// ** CBNSP
	private static final StringBuffer SQL_GET_INCOMING_DATE = new StringBuffer("");
	private static final StringBuffer SQL_CHECK_IF_EXISTING_CHBK = new StringBuffer("");
	private static final StringBuffer SQL_UPDATE_REVERSAL_REF_NUM = new StringBuffer("");
	private static final StringBuffer SQL_INSERT_AMEX_OPTB_CB = new StringBuffer("");
	private static final StringBuffer SQL_INSERT_NETWORK_CB = new StringBuffer(""); 
	private static final StringBuffer SQL_INSERT_NETWORK_CB_ACTIVITY = new StringBuffer("");
	private static final StringBuffer SQL_INSERT_Q_DATA = new StringBuffer("");
	private static final StringBuffer SQL_UPDATE_CHBK_MID = new StringBuffer("");
	private static final StringBuffer SQL_CREATE_PROCESS_TABLE_ENTRIES = new StringBuffer("");
	private static final StringBuffer SQL_SELECT_SETTLE_REC = new StringBuffer("");
	private static final StringBuffer SQL_SELECT_RECON = new StringBuffer("");
	private static final StringBuffer SQL_UPDATE_NW_CHGBK = new StringBuffer("");
	private static final StringBuffer SQL_UPDATE_RECON = new StringBuffer("");
	
	
	// ** INQ02
	private static final StringBuffer SQL_GET_INCOMING_DATE_INQO2 = new StringBuffer("");
	private static final StringBuffer SQL_CHECK_IF_EXISTING_RETR = new StringBuffer("");
	private static final StringBuffer SQL_INSERT_AMEX_OPTB_RETR = new StringBuffer("");
	private static final StringBuffer SQL_INSERT_NETWORK_RETR = new StringBuffer("");
	private static final StringBuffer SQL_SELECT_SETTLE_INQ1 = new StringBuffer("");
	private static final StringBuffer SQL_SELECT_SETTLE_INQ2 = new StringBuffer("");
	private static final StringBuffer SQL_UPDATE_RETR = new StringBuffer("");

	
	// ** For Emails (Notifications)
	private static final StringBuffer GET_INCOMING_CB_COUNT = new StringBuffer("");
	private static final StringBuffer GET_INCOMING_RET_COUNT= new StringBuffer("");
	
	
	private static final String NL = " \n";
	
	private static final int RISK_PROCESS_TYPE = 1;
	private static final long RISK_PROCESS_SEQUENCE = 0L;
	
	private static final String MASKING_CHARS = "xxxxxx";
	
	static {		
		// ** CBNSP
		SQL_GET_INCOMING_DATE.append(
			"select  to_date(?,'yyyyddd')" + NL +
			"from dual"
			);
		
		SQL_CHECK_IF_EXISTING_CHBK.append(
				"select  nvl(qd.type,0)                     as qtype," + NL + 
				"	cbam.load_sec                           as load_sec," + NL + 
				"	cbam.current_case_number                as case_number," + NL + 
				"	cbam.cb_amount                  		as cb_amount," + NL + 
				"	decode(cbam.reversal_date,null,'N','Y') as reversed," + NL + 
				"	to_number(?,'S0000000000000.00') " + NL + 
				"											as reversal_amount," + NL + 
				"	case when (cbam.cb_amount + to_number(?,'S0000000000000.00')) = 0 then 'Y' else 'N' end" + NL + 
				"											as offset_adjustment" + NL + 
				"from network_chargeback_amex_optb cbam," + NL + 
				"	  q_data                  qd" + NL + 
				"where cbam.se_numb = ?" + NL + 
				"	and cbam.incoming_date >= (?-180)" + NL + 
				"	and cbam.cb_adjustment_number = ?" + NL + 
				"	and qd.item_type(+) = 18" + NL + 
				"	and qd.id(+) = cbam.load_sec"
			);
		
		SQL_UPDATE_REVERSAL_REF_NUM.append(
				"update network_chargeback_amex_optb " + NL + 
				"	set reversal_ref_num = ?" + NL + 
				"where load_sec = ?"
			);
		
		SQL_INSERT_AMEX_OPTB_CB.append(
				"insert into network_chargeback_amex_optb (" + NL + 
				"	load_sec," + NL + 
				"	merchant_number," + NL + 
				"	incoming_date," + NL + 
				"	case_number," + NL + 
				"	status," + NL + 
				"	card_number_enc," + NL + 
				//"	acq_reference_number," + NL + 
				//"	settlement_rec_id," + NL + 
				"	mes_ref_num," + NL + 
				"	load_filename," + NL + 
				"	load_file_id," + NL + 
				//"	settlement_date," + NL + 
				//"	reversal_date," + NL + 
				//"	reversal_amount," + NL + 
				//"	reversal_ref_num," + NL + 
				//"	bank_number," + NL + 
				"	rec_type," + NL + 
				"	fin_ind," + NL + 
				"	se_numb," + NL + 
				"	cm_acct_numb," + NL + 
				"	current_case_number," + NL + 
				"	current_action_number," + NL + 
				"	previous_case_number," + NL + 
				"	previous_action_number," + NL + 
				"	resolution," + NL + 
				"	from_system," + NL + 
				"	rejects_to_system," + NL + 
				"	disputes_to_system," + NL + 
				"	date_of_adjustment, " + NL + 
				"	date_of_charge," + NL + 
				"	amex_id," + NL + 
				"	case_type," + NL + 
				"	loc_numb," + NL + 
				"	cb_reas_code," + NL + 
				"	cb_amount," + NL + 
				"	cb_adjustment_number," + NL + 
				"	cb_resolution_adj_number," + NL + 
				"	cb_reference_code," + NL + 
				"	billed_amount," + NL + 
				"	soc_amount," + NL + 
				"	soc_invoice_number," + NL + 
				"	roc_invoice_number," + NL + 
				"	foreign_amt," + NL + 
				"	currency," + NL + 
				"	supp_to_follow," + NL + 
				"	cm_name1," + NL + 
				"	cm_name2," + NL + 
				"	cm_addr1," + NL + 
				"	cm_addr2," + NL + 
				"	cm_city_state," + NL + 
				"	cm_zip," + NL + 
				"	cm_first_name_1," + NL + 
				"	cm_middle_name_1," + NL + 
				"	cm_last_name_1," + NL + 
				"	cm_orig_acct_num," + NL + 
				"	cm_orig_name," + NL + 
				"	cm_orig_first_name," + NL + 
				"	cm_orig_middle_name," + NL + 
				"	cm_orig_last_name," + NL + 
				"	note1," + NL + 
				"	note2," + NL + 
				"	note3, " + NL + 
				"	note4," + NL + 
				"	note5," + NL + 
				"	note6," + NL + 
				"	note7," + NL + 
				"	triumph_seq_no," + NL + 
				"	seller_id," + NL + 
				"	airline_tkt_num," + NL + 
				"	al_sequence_number," + NL + 
				"	folio_ref," + NL + 
				"	merch_order_num," + NL + 
				"	merch_order_date," + NL + 
				"	canc_num," + NL + 
				"	canc_date," + NL + 
				"	fincap_tracking_id," + NL + 
				"	fincap_file_seq_num," + NL + 
				"	fincap_batch_number," + NL + 
				"	fincap_batch_invoice_dt," + NL + 
				"	label1," + NL + 
				"	data1," + NL + 
				"	label2," + NL + 
				"	data2," + NL + 
				"	label3," + NL + 
				"	data3," + NL + 
				"	label4," + NL + 
				"	data4," + NL + 
				"	label5," + NL + 
				"	data5," + NL + 
				"	label6," + NL + 
				"	data6," + NL + 
				"	label7," + NL + 
				"	data7," + NL + 
				"	label8," + NL + 
				"	data8," + NL + 
				"	label9," + NL + 
				"	data9," + NL + 
				"	label10," + NL + 
				"	data10," + NL + 
				"	label11," + NL + 
				"	data11," + NL + 
				"	label12," + NL + 
				"	data12," + NL + 
				"	ind_form_code," + NL + 
				"	ind_ref_number," + NL + 
				"	loc_ref_number," + NL + 
				"	passenger_name," + NL + 
				"	passenger_first_name," + NL + 
				"	passenger_middle_name," + NL + 
				"	passenger_last_name," + NL + 
				"	se_process_date," + NL + 
				"	return_date," + NL + 
				"	credit_receipt_number," + NL + 
				"	return_to_name," + NL + 
				"	return_to_street," + NL + 
				"	card_deposit," + NL + 
				"	assured_reservation," + NL + 
				"	res_cancelled," + NL + 
				"	res_cancelled_date," + NL + 
				"	cancel_zone," + NL + 
				"	reservation_made_for," + NL + 
				"	reservation_location," + NL + 
				"	reservation_made_on," + NL + 
				"	rental_agreement_number," + NL + 
				"	merchandise_type," + NL + 
				"	merchandise_returned," + NL + 
				"	returned_name," + NL + 
				"	returned_date," + NL + 
				"	returned_how," + NL + 
				"	returned_reason," + NL + 
				"	store_credit_received, " + NL +
				"	said " + NL +
				" ) " + NL +
				" values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + NL +
				"?, ?, ?, ?, ?, to_number(?,'S0000000000000.00'), ?, ?, ?, " + NL +
				"to_number(?,'S0000000000000.00'), to_number(?,'S0000000000000.00'), " + NL +
				"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + NL +
				"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  " + NL +
				"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + NL +
				"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + NL +
				"?, ?, ?, ? " + NL +
				" )"
			);
		
		SQL_INSERT_NETWORK_CB.append(
				"insert into network_chargebacks" + NL + 
				"	(" + NL + 
				"	cb_load_sec," + NL + 
				"	cb_ref_num," + NL + 
				"	merchant_number," + NL + 
				"	bank_number," + NL + 
				"	incoming_date," + NL + 
				"	reference_number," + NL + 
				"	card_number," + NL + 
				"	card_type," + NL + 
				"	tran_date," + NL + 
				"	tran_amount," + NL + 
				"	merchant_name," + NL + 
				"	reason_code," + NL + 
				"	load_filename," + NL + 
				"	first_time_chargeback," + NL + 
				"	 mes_ref_num," + NL + 						
				"	debit_credit_ind," + NL + 
				"	tran_date_missing," + NL + 
				"	card_number_enc" + NL + 
				"	)" + NL + 
				"select  cb.load_sec          as load_sec," + NL + 
				"	cb.case_number            as cb_ref_num," + NL + 
				"	nvl(cb.merchant_number,0) as merchant_number," + NL + 
				"	nvl(mf.bank_number,3941)  as bank_number," + NL + 	    // ** TODO REMOVE the nvl(..,3941), had to be inserted for testing purposes. After transactions are linked properly this will not be required 
				"	cb.incoming_date          as incoming_date," + NL + 
				"	nvl(cb.acq_reference_number, 0)" + NL + 				// ** TODO REMOVE the nvl 0, null could not be inserted, hence for testing purposes. After transactions are linked properly this will not be required.  
				"							  as reference_number," + NL + 
				"	cb.cm_acct_numb           as card_number," + NL + 
				"	'AM'                      as card_type," + NL + 
				"	cb.date_of_charge         as tran_date," + NL + 
				"	-- amex raw data shows cb as a credit (to issuer)" + NL + 
				"	(-1 * cb.cb_amount) 	  as tran_amount," + NL + 
				"	mf.dba_name               as merchant_name," + NL + 
				"	cb.cb_reas_code           as reason_code," + NL + 
				"	cb.load_filename          as load_filename," + NL + 
				"	'Y'                       as first_time," + NL + 
				"	 cb.mes_ref_num            as mes_ref_num," + NL + 
				"	decode( (cb.billed_amount/abs(cb.billed_amount))," + NL + 
				"			-1, 'C'," + NL + 
				"	        	'D' )         as debit_credit_ind," + NL + 
				"	'N'                       as tran_date_missing," + NL + 
				"	cb.card_number_enc        as card_number_enc" + NL + 
				"from network_chargeback_amex_optb cb," + NL + 
				"	mif                     mf" + NL + 
				"where cb.load_file_id = load_filename_to_load_file_id(?) and" + NL + 
				"	mf.merchant_number(+) = cb.merchant_number");
		
		
		SQL_INSERT_NETWORK_CB_ACTIVITY.append(
				"insert into network_chargeback_activity" + NL +
				"	(" + NL +
				"	cb_load_sec," + NL +
				"	file_load_sec," + NL +
				"	action_code," + NL +
				"	action_date," + NL +
				"	user_message," + NL +
				"	action_source," + NL +
				"	user_login" + NL +
				")" + NL +
				"select  cb.load_sec," + NL +
				"	1," + NL +
				"	'D'," + NL +
				"	trunc(sysdate)," + NL +
				"	'Auto Action - Amex'," + NL +
				"	8,    -- auto amex" + NL +
				"	'system'" + NL +
				"from network_chargeback_amex_optb cb" + NL +
				"where cb.load_file_id = load_filename_to_load_file_id(?) and" + NL +
				"	not cb.settlement_rec_id is null    -- only work resolved items");
		
		SQL_INSERT_Q_DATA.append(
				"insert into q_data" + NL +
				"	(" + NL +
				"	id," + NL +
				"	type," + NL +
				"	item_type," + NL +
				"	owner," + NL +
				"	date_created," + NL +
				"	source," + NL +
				"	affiliate," + NL +
				"	last_changed," + NL +
				"	last_user" + NL +
				")" + NL +
				"select  cb.load_sec                as id," + NL +
				"	decode(cb.settlement_rec_id," + NL +
				"	    null," + MesQueues.Q_CHARGEBACKS_AMEX + ", " + MesQueues.Q_CHARGEBACKS_MCHB  + NL + 
				"	    )                         	as type," + NL +
				"	18                              as item_type, -- chargeback" + NL +
				"	1                               as owner," + NL +
				"	cb.incoming_date                as date_created," + NL +
				"	'amex'                          as source," + NL +
				"	to_char(nvl(mf.bank_number,3941)) as affiliate," + NL +
				"	sysdate                         as last_changed," + NL +
				"	'system'                        as last_user" + NL +
				"from network_chargeback_amex_optb cb," + NL +
				"		mif mf" + NL +
				"where cb.load_file_id = load_filename_to_load_file_id(?) and" + NL +
				"	mf.merchant_number(+) = cb.merchant_number");
		
		SQL_CREATE_PROCESS_TABLE_ENTRIES.append("insert into risk_process " + NL + 
				" (" + NL +
				"  process_sequence," + NL +
				"  process_type,"     + NL +
				"  load_filename"     + NL +
				"  )"                 + NL +
				"  values(?,?,?)");
		
		SQL_UPDATE_CHBK_MID.append(
				"update network_chargeback_amex_optb " + NL + 
				" set settlement_rec_id = ?," + NL +
				" merchant_number = ?," + NL +
				" acq_reference_number = ?," + NL +
				" bank_number = ?" + NL +
				" where load_sec = ?");
		
		SQL_SELECT_SETTLE_REC.append(
				" select ams.rec_id," + NL +
				"  ams.merchant_number," + NL + 
				" ams.reference_number," + NL +
				" ams.bank_number" + NL +
				" from amex_settlement ams" + NL +
				"  where ams.amex_se_number = ? " + NL +
				" and ams.transaction_date between (?-15) and (?+3) " + NL +
				" and ams.card_number in ( ?, ? ) " + NL +
				" and nvl(ams.roc_number,ams.reference_number) like (? || '%')  ");
		
		SQL_SELECT_RECON.append(
				"select ar.rowid as row_id," + NL +
				" ar.recon_date as recon_date," + NL +
				" decode(ar.cb_load_sec,null,'N','Y') as linked " + NL +
				" from amex_settlement_recon_optblue ar " + NL + 
				" where ar.recon_date between (?-3) and (?+3) " + NL + 
				" and ar.adjustment_number = ? order by recon_date");
		
		SQL_UPDATE_NW_CHGBK.append(
				"update network_chargebacks " + NL + 
		        " set reversal_date = ? " + NL +
				" where cb_load_sec = ? ");
		
		SQL_UPDATE_RECON.append(
				"update  amex_settlement_recon_optblue " + NL +
				" set cb_load_sec = ? " + NL +
				" where rowid = ? ");
		
		//** INQ02
		SQL_GET_INCOMING_DATE_INQO2.append(
				"select  to_date(?,'yyyyddd')" + NL +				
				"from dual");
		
		SQL_CHECK_IF_EXISTING_RETR.append(
				"select nrao.load_sec as load_sec," + NL +
				"	nvl(qd.type, ?)  as queue_type" + NL + 
				"from network_retrieval_amex_optb nrao," + NL +
				"	q_data qd" + NL +
				"where nrao.inquiry_case_number = ?" + NL + 
				//"	and nrao.card_account_number like ?" + NL + // TODO Required / should we? 
				"	and qd.id(+) = nrao.load_sec" + NL + 
				"	and qd.item_type(+) = ?" + NL + 
				"	order by nrao.load_sec");
				
		SQL_INSERT_AMEX_OPTB_RETR.append(
				"insert into network_retrieval_amex_optb (" + NL +
				"	load_sec," + NL + 
				//"	merchant_number," + NL + 
				//"	acq_reference_number," + NL + 
				//"	settlement_rec_id," + NL + 
				//"	mes_ref_num," + NL + 
				"	load_filename," + NL + 
				"	load_file_id," + NL + 
				"	incoming_date," + NL +
				"	card_number_enc," + NL + 
				"	rec_type," + NL + 
				"	cardmember_account_number," + NL + 
				"	inquiry_case_number," + NL + 
				"	airline_ticket_number," + NL + 
				"	amex_process_date," + NL + 
				"	amex_id," + NL + 
				"	case_type," + NL + 
				"	se_reply_by_date," + NL + 
				"	location_number," + NL + 
				"	date_of_charge," + NL + 
				"	cardmember_name_1," + NL + 
				"	cardmember_name_2, " + NL + 
				"	seller_id, " + NL + 
				"	cardmember_account_number_19," + NL + 
				"	cardmember_city_province," + NL + 
				"	cardmember_postal_code," + NL + 
				"	se_number," + NL + 
				"	inquiry_reason_code," + NL + 
				"	follow_up_reason_code," + NL + 
				"	foreign_amount," + NL + 
				"	support_to_follow," + NL + 
				"	inquiry_action_number," + NL + 
				"	cardmember_first_name_1," + NL + 
				"	cardmember_middle_name_1," + NL + 
				"	cardmember_last_name_1," + NL + 
				"	cardmember_original_number," + NL + 
				"	cardmember_original_name," + NL + 
				"	inquiry_note_1," + NL + 
				"	inquiry_note_2," + NL + 
				"	inquiry_note_3," + NL + 
				"	cardmember_orig_first_name," + NL + 
				"	cardmember_orig_middle_name," + NL + 
				"	cardmember_orig_last_name," + NL + 
				"	inquiry_note_4," + NL + 
				"	inquiry_note_5," + NL + 
				"	inquiry_note_6," + NL + 
				"	inquiry_note_7," + NL + 
				"	charge_amount," + NL + 
				"	disputed_amount," + NL + 
				"	reference_number," + NL + 
				"	soc_invoice_number," + NL + 
				"	sequence_indicator," + NL + 
				"	passenger_name," + NL + 
				"	passenger_first_name," + NL + 
				"	passenger_middle_name," + NL + 
				"	passenger_last_name," + NL + 
				"	se_process_date," + NL + 
				"	ind_form_code," + NL + 
				"	ind_ref_number," + NL + 
				"	loc_ref_number," + NL + 
				"	inquiry_mark," + NL + 
				"	lta_filed_date," + NL + 
				"	return_date," + NL + 
				"	credit_receipt_number," + NL + 
				"	return_to_name," + NL + 
				"	return_to_street," + NL + 
				"	card_deposit," + NL + 
				"	card_canc_number," + NL + 
				"	assured_reservation," + NL + 
				"	res_cancelled," + NL + 
				"	res_cancelled_date," + NL + 
				"	res_cancelled_time," + NL + 
				"	cancel_zone," + NL + 
				"	reservation_made_for," + NL + 
				"	reservation_location," + NL + 
				"	reservation_made_on," + NL + 
				"	folio_ref_number," + NL + 
				"	rental_agreement_needed," + NL + 
				"	rental_agreement_number," + NL + 
				"	merchandise_type," + NL + 
				"	order_number," + NL + 
				"	merchandise_returned," + NL + 
				"	credit_requested," + NL + 
				"	replacement_requested," + NL + 
				"	returned_name," + NL + 
				"	returned_date," + NL + 
				"	returned_how," + NL + 
				"	returned_reason," + NL + 
				"	store_credit_received," + NL + 
				"	ind_form_code_2," + NL + 
				"	ind_ref_number_2," + NL + 
				"	loc_ref_number_2," + NL +
				"	said )" + NL +
				"values( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + NL + 
				"	?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + NL +  
				"	?, ?, ?, ?, ?, ?, ?, ?, ?, " + NL +  
				"	?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + NL +
				"   ?, ?, ?, ?,	?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + NL +  
				"   ?, ?, ?, ?, ?, ?, ?, ?)");		
				
		SQL_INSERT_NETWORK_RETR.append(
				"insert into network_retrievals" + NL +
				" (" + NL +
				"	retr_load_sec," + NL +
				"	merchant_number," + NL +
				"	incoming_date," + NL +
				"	reference_number," + NL +
				"	card_number," + NL +
				"	tran_date," + NL +
				"	tran_amount," + NL +
				"	merchant_name," + NL +
				"	reason_code," + NL +
				"	load_filename," + NL +
				"	user_message," + NL +
				"	bank_number," + NL +
				"	mes_ref_num," + NL +
				"	card_number_enc," + NL +
				"	retrieval_request_id," + NL +
				"	merchant_number_missing," + NL +
				"	tran_date_missing" + NL +
				" )" + NL +
				"select  retr.load_sec                as load_sec," + NL +
				"	nvl(retr.merchant_number,0)       as merchant_number," + NL +
				"	retr.incoming_date                as incoming_date," + NL +
				"	nvl(retr.acq_reference_number,0)  as ref_num," + NL +
				"	retr.cardmember_account_number    as card_number," + NL +
				"	retr.date_of_charge               as tran_date," + NL +
				"	retr.disputed_amount              as tran_amount," + NL +
				"	mf.dba_name                       as merchant_name," + NL +
				"	retr.inquiry_reason_code          as reason_code," + NL +
				"	retr.load_filename                as load_filename," + NL +
				"	retr.reference_number             as user_message," + NL +
				"	mf.bank_number                    as bank_number," + NL +
				"	retr.mes_ref_num                  as mes_ref_num," + NL +
				"	retr.card_number_enc              as card_number_enc," + NL +
				"	retr.inquiry_case_number                  as retrieval_request_id," + NL +
				"	decode(retr.merchant_number," + NL +
				"		null,'Y','N')              	  as merchant_number_missing," + NL +
				"	'N'                               as tran_date_missing" + NL +
				"from network_retrieval_amex_optb retr," + NL +
				"	mif mf" + NL +
				"where retr.load_file_id = load_filename_to_load_file_id(?) and" + NL +
				"	mf.merchant_number(+) = retr.merchant_number");

		GET_INCOMING_CB_COUNT.append (
				"select  se_numb           as se_number, " + NL + 
				"    count(1)                    as total_count, " + NL + 
				"    sum(billed_amount)    as total_amount " + NL + 
				"from network_chargeback_amex_optb" + NL + 
				"where load_file_id = load_filename_to_load_file_id(?) " + NL + 
				"group by se_numb");
		
		GET_INCOMING_RET_COUNT.append(
				"select  se_number           as se_number, " + NL + 
				"    count(1)                    as total_count, " + NL + 
				"    sum(charge_amount)    as total_amount " + NL + 
				"from network_retrieval_amex_optb" + NL + 
				"where load_file_id = load_filename_to_load_file_id(?) " + NL + 
				"group by se_number");
		
		SQL_SELECT_SETTLE_INQ1.append(
				"select ams.rec_id," + NL +
				" ams.merchant_number," + NL +
				" ams.reference_number " + NL +
				" from amex_settlement ams" + NL +
				" where ams.amex_se_number = ? " + NL +
				" and ams.transaction_date between (?-7) and (?+3) " + NL +
				" and nvl(ams.roc_number,ams.reference_number) like (? || '%')  " + NL +
				" and ? <= ams.transaction_amount  " + NL +
				" order by (ams.transaction_amount - ?) " );
		
		SQL_SELECT_SETTLE_INQ2.append(
				"select ams.rec_id," + NL +
				" ams.merchant_number," + NL +
				" ams.reference_number " + NL +
				" from amex_settlement ams " + NL +
				" where ams.amex_se_number = ? " + NL +
				" and ams.transaction_date between (?-7) and (?+3) " + NL +
				" and ams.card_number =  ? " + NL +
				" and ams.transaction_amount = ? ");
		
		SQL_UPDATE_RETR.append(
				"update network_retrieval_amex_optb " + NL +
				" set settlement_rec_id = ?," + NL +
				" merchant_number = ?," + NL +
				" acq_reference_number = ? " + NL +
				" where load_sec = ?");



	}	
	
	
}
