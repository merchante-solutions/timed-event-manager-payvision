package com.mes.startup;

import org.apache.log4j.Logger;
import com.mes.aus.file.job.ReqfProcessJob;

public final class AusReqfProcessEvent extends EventBase {
  static Logger log = Logger.getLogger(AusReqfProcessEvent.class);
  public boolean execute() {
    try {
      boolean testFlag = "test".equals(getEventArg(0).toLowerCase());
      (new ReqfProcessJob(testFlag)).run();
      return true;
    }
    catch (Exception e) {
      log.error("AUS request file process event error: " + e);
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    return false;
  }
}