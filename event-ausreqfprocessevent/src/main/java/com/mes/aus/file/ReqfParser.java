package com.mes.aus.file;

import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.aus.AusAccount;
import com.mes.aus.AusDb;
import com.mes.aus.ProcessError;
import com.mes.aus.RequestFile;
import com.mes.aus.RequestRecord;
import com.mes.support.TridentTools;

public class ReqfParser
{
  static Logger log = Logger.getLogger(ReqfParser.class);

  private static final int    LST_HEADER  = 1;
  private static final int    LST_DETAIL  = 2;
  private static final int    LST_TRAILER = 3;
  private static final int    LST_DONE    = 4;

  private AusDb             db;
  private ErrorManager      errorMgr;
  private ReqfBatchHandler  handler;
  private int               batchSize;
  
  public ReqfParser(AusDb db, ReqfBatchHandler handler, 
    ErrorManager errorMgr)
  {
    this.db = db;
    this.handler = handler;
    this.errorMgr = errorMgr;
    batchSize = handler.getBatchSize();
  }

  /**
   * Parse header record.
   */
  private void parseHeader(RequestFile reqf, String line, int lineNum)
  {
    if (!line.startsWith("H1"))
    {
      errorMgr.addError(new ProcessError(
        reqf.getReqfId(),lineNum,0,"Invalid header record type",
        ProcessError.FATAL));
      return;
    }

    if (line.length() != 40)
    {
      errorMgr.addError(new ProcessError(
        reqf.getReqfId(),lineNum,line.length(),"Invalid header record length",
        ProcessError.FATAL));
      return;
    }

    String version = line.substring(2,8).trim();
    if (!version.equals("100000"))
    {
      errorMgr.addError(new ProcessError(
        reqf.getReqfId(),lineNum,line.length(),"Invalid version ID '" 
        + version + "'",ProcessError.FATAL));
      return;
    }

    String merchNum = line.substring(8,40).trim();
    if (merchNum.length() == 0)
    {
      errorMgr.addError(new ProcessError(
        reqf.getReqfId(),lineNum,8,"Missing merchant number in header",
        ProcessError.FATAL));
      return;
    }

    if (!reqf.getMerchNum().equals(merchNum))
    {
      errorMgr.addError(new ProcessError(
        reqf.getReqfId(),lineNum,8,"Merchant number mismatch (file '" + merchNum
          + "', upload '" + reqf.getMerchNum() + "')", ProcessError.FATAL));
      return;
    }
  }

  private boolean validateAccountType(String accountType)
  {
    return AusAccount.AT_VISA.equals(accountType) ||
           AusAccount.AT_MC.equals(accountType) ||
           AusAccount.AT_DISC.equals(accountType);
  }

  private boolean validateAccountNum(String accountType, String accountNum)
  {
    // validate length
    int accountLen = accountNum.length();
    if (AusAccount.AT_VISA.equals(accountType))
    {
      if (accountLen != 13 && accountLen != 16)
      {
        return false;
      }
    }
    else if (AusAccount.AT_MC.equals(accountType))
    {
      if (accountLen != 16)
      {
        return false;
      }
    }
    else if (AusAccount.AT_DISC.equals(accountType))
    {
      if (accountLen != 16)
      {
        return false;
      }
    }

    // luhn check
    if (!TridentTools.mod10Check(accountNum))
    {
      return false;
    }
    return true;
  }

  private boolean validateExpDate(String expDate)
  {
    try
    {
      int yy = Integer.parseInt(expDate.substring(0,2));
      int mm = Integer.parseInt(expDate.substring(2,4));
      if (mm < 1 || mm > 12)
      {
        return false;
      }
    }
    catch (Exception e)
    {
      log.debug("Invalid expiration date '" + expDate + "'");
      return false;
    }

    return true;
  }

  /**
   * Parse detail record.  Generates request objects in the requests list.
   */
  private RequestRecord parseDetail(RequestFile reqf, String line, 
    int lineNum)
  {
    // D1 - standard card number detail
    // D2 - stored card id detail
    boolean cardIdFlag = line.startsWith("D2");
    if (!cardIdFlag && !line.startsWith("D1"))
    {
      errorMgr.addError(new ProcessError(
        reqf.getReqfId(),lineNum,0,"Invalid detail record type"));
      return null;
    }

    if (line.length() != 74)
    {
      errorMgr.addError(new ProcessError(
        reqf.getReqfId(),lineNum,line.length(),"Invalid detail record length"));
      return null;
    }

    // account type
    String accountType = line.substring(2,6).trim();
    if (!validateAccountType(accountType))
    {
      errorMgr.addError(new ProcessError(
        reqf.getReqfId(),lineNum,2,"Invalid account type '" + accountType 
        + "'"));
      return null;
    }

    // acquire account num (have to fetch from card store if cardId)
    String accountNum = line.substring(6,38).trim();
    String cardId = null;
    if (cardIdFlag)
    {
      cardId = accountNum;
      accountNum = db.getStoredAccountNum(reqf.getMerchNum(),cardId);
    }

    // validate card id and/or account num
    if (cardId != null && accountNum == null)
    {
      errorMgr.addError(new ProcessError(
        reqf.getReqfId(),lineNum,4,"Invalid card ID '" 
        + cardId + "'"));
    }
    else if (!validateAccountNum(accountType,accountNum))
    {
      errorMgr.addError(new ProcessError(
        reqf.getReqfId(),lineNum,4,"Invalid account number '" 
        + TridentTools.encodeCardNumber(accountNum) + "'"));
    }

    // exp date
    String expDate = line.substring(38,42).trim();
    if (!validateExpDate(expDate))
    {
      errorMgr.addError(new ProcessError(
        reqf.getReqfId(),lineNum,36,"Invalid expiration date '" + expDate 
        + "', must be YYMM"));
    }

    // discretionary data
    String discData = line.substring(42,74).trim();

    // add a request record for this detail to the list
    RequestRecord request = new RequestRecord();
    request.setReqfId(reqf.getReqfId());
    request.setAccountType(accountType);
    request.setExpDate(expDate);
    request.setCreateDate(Calendar.getInstance().getTime());
    request.setMerchNum(reqf.getMerchNum());
    request.setDiscData(discData);
    if (accountNum != null)
    {
      request.setAccountNum(accountNum);
    }
    if (cardId != null)
    {
      request.setCardId(cardId);
    }
    return request;
  }

  private void parseTrailer(RequestFile reqf, String line, int lineNum, 
    int detailCount)
  {
    if (line.length() != 8)
    {
      errorMgr.addError(new ProcessError(
        reqf.getReqfId(),lineNum,line.length(),"Invalid trailer record length (" 
          + line.length() + ")",ProcessError.FATAL));
      return;
    }

    String countStr = line.substring(2,8).trim();
    try
    {
      int trailerCount = Integer.parseInt(countStr);
      if (trailerCount != detailCount)
      {
        errorMgr.addError(new ProcessError(
          reqf.getReqfId(),lineNum,2,"Trailer count mismatch (" + trailerCount 
            + " <> " + detailCount + " details)",ProcessError.FATAL));
        return;
      }
    }
    catch (NumberFormatException ne)
    {
      errorMgr.addError(new ProcessError(
        reqf.getReqfId(),lineNum,2,"Invalid detail count '" + countStr 
        + "' in trailer",ProcessError.FATAL));
    }
  }
  
  /**
   * Once batch is full it needs to be offloaded to the batch handler
   * for storage and then discarded to free up memory.
   */
  private void storeBatch(RequestFile reqf, List batch)
  {
    // have the batch handler store the request records
    if (!handler.handleRequests(batch))
    {
      // processing error, need to halt
      errorMgr.addError(new ProcessError(
          reqf.getReqfId(),1,1,"Database error",ProcessError.FATAL));
    }
    batch.clear();
  }
  
  /**
   * Parse request file data into requests.  Periodically pass batches of
   * requests to a request batch handler.  Stop when request file data
   * is fully consumed or a fatal error occurs.
   */
  public void parse(RequestFile reqf)
  {
    // initialize 
    LineNumberReader in = null;
    String line = null;
    int detailCount = 0;
    int lineNum = 0;
    int processState = LST_HEADER;
    List batch = new ArrayList();
      
    try
    {
      in = new LineNumberReader(new InputStreamReader(reqf.getInputStream()));
      while ((line = in.readLine()) != null && !errorMgr.hasFatalError())
      {
        lineNum = in.getLineNumber();
        switch (processState)
        {
          case LST_HEADER:
          
            parseHeader(reqf,line,lineNum);
            processState = LST_DETAIL;
            break;

          case LST_DETAIL:
          
            // pass the batch off to the handler 
            if (batch.size() >= batchSize)
            {
              storeBatch(reqf,batch);
            }
            
            if (detailCount % 1000 == 0)
              log.debug(""+detailCount + " details processed in file " + Calendar.getInstance().getTime() + "...");

            // have to check for trailer with every detail
            if (!line.startsWith("T1"))
            {
              RequestRecord request = parseDetail(reqf,line,lineNum);
              if (request != null)
              {
                ++detailCount;
                batch.add(request);
              }
              break;
            }
            
            // found the trailer
            processState = LST_TRAILER;

          case LST_TRAILER:
          
            log.debug(""+detailCount + " details processed in file " + Calendar.getInstance().getTime() + "...");
              
            // handle the final batch if needed
            if (batch.size() > 0)
            {
              storeBatch(reqf,batch);
            }
            
            parseTrailer(reqf,line,lineNum,detailCount);
            processState = LST_DONE;
            break;

          case LST_DONE:
          
            // TODO: should we just ignore data after the trailer?
            errorMgr.addError(new ProcessError(
              reqf.getReqfId(),lineNum,0,"Unexpected data after trailer: '" 
              + line + "'",ProcessError.FATAL));
            break;
        }
      }

      if (!errorMgr.hasFatalError() && processState != LST_DONE)
      {
        errorMgr.addError(new ProcessError(
          reqf.getReqfId(),lineNum,0,"Unexpected end of file",
          ProcessError.FATAL));
      }
    }
    catch (Exception e)
    {
      log.error("Error: " + e);
      e.printStackTrace();
      throw new RuntimeException("Parser error",e);
    }
    finally
    {
      try { in.close(); } catch (Exception e) { }
    }
  }
}