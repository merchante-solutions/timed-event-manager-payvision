package com.mes.aus.file.job;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.aus.AusDb;
import com.mes.aus.RequestFile;
import com.mes.aus.file.ReqfProcessor;

public class ReqfProcessJob extends Job {
  static Logger log = Logger.getLogger(ReqfProcessJob.class);
  private boolean directFlag;
  private boolean testFlag;

  public ReqfProcessJob(boolean directFlag, boolean testFlag) {
    super(directFlag);
    this.testFlag = testFlag;
  }
  public ReqfProcessJob(boolean testFlag) {
    this(true,testFlag);
  }
  public void run() {
    try {
      AusDb db = new AusDb(directFlag);
      List reqfList = db.getRequestFilesWithStatus(RequestFile.ST_UPLOADED,true,testFlag);
      ReqfProcessor rfp = new ReqfProcessor(directFlag);
      
      for (Iterator i = reqfList.iterator(); i.hasNext();) {
    	try{
    	  RequestFile reqf = (RequestFile)i.next();
    	  log.info("Processing request file: " + reqf);
    	  //if (reqf.getFileSize() > 0 && !reqf.empty()) {
    	    rfp.process(reqf);
    	  //}
    	}
    	catch(Exception e){
    	  log.info("Failed to process request file.  Error: " + e.toString());
    	  notifyError(e,"run()");
    	}
      }
    }
    catch (Exception e) {
      log.info("Failed to process request file.  Error: " + e.toString());
      notifyError(e,"run()");
    }
  }
}