package com.mes.aus.file;

import java.util.List;

public interface ReqfBatchHandler {
  public int getBatchSize();
  public boolean handleRequests(List requests);
}