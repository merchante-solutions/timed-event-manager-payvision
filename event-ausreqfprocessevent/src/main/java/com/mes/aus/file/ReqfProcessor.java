package com.mes.aus.file;

import java.util.List;
import org.apache.log4j.Logger;
import com.mes.aus.Notifier;
import com.mes.aus.ProcessError;
import com.mes.aus.RequestFile;

public class ReqfProcessor extends AutomatedProcess implements ReqfBatchHandler
{
  static Logger log = Logger.getLogger(ReqfProcessor.class);  
  private ErrorManager errorMgr;

  public ReqfProcessor(boolean directFlag) {
    super(directFlag);
    errorMgr = new ErrorManager(db);
  }
  
  public ReqfProcessor() {
    this(true);
  }

  /**
   * Clear out prior failed requests and increment the parse count.
   */
  private void prepareForParse(RequestFile reqf) {
    // reset error handler
    errorMgr.resetErrors();
    
    // purge prior request records if they exist for the file
    db.purgeRequestRecords(reqf.getReqfId());

    // increment the parse count
    reqf.setParseNum(reqf.getParseNum() + 1);
  }
  
  public int getBatchSize() {
    // TODO: fetch this from aus config table
    return 1000;
  }
  
  public boolean handleRequests(List requests) {
    return db.insertRequestRecords(requests);
  }
  
  public void process(RequestFile reqf) {
    try {
      // clear prior requests, etc.
      prepareForParse(reqf);
  
      // parse the file, generate request records
      if (reqf.getFileSize() > 0 && !reqf.empty()) {
        (new ReqfParser(db,this,errorMgr)).parse(reqf);
      }
    }
    catch (Exception e) {
      errorMgr.addError(new ProcessError( reqf.getReqfId(),1,1,"Processing error",ProcessError.FATAL));

      notifyError(e,"process(reqfId=" + reqf.getReqfId() + ")");
      
      // TODO: revise so only one error notification is being sent?
      log.error("Failed to Process AUS Request File", e);
    }
    // log any errors
    if (errorMgr.hasError()) {
      errorMgr.logErrors();
    }
    // update request file status    
    reqf.setStatus(errorMgr.hasError() ? reqf.ST_ERROR : reqf.ST_PROCESSED);
    db.updateRequestFile(reqf,false);
  
    // send email notification to user as indicated
    if (errorMgr.hasError()) {
      (new Notifier(directFlag)).notifyRequestFileErrors(reqf,errorMgr.getErrors());
    }else{
    	// send file notification
        Notifier.notifyDeveloper("AUS Request File Process", "Request file processed: " + reqf, directFlag);
    }
  }

  /**
   * ERROR ACCESSORS (see AusUserAction)
   */  
  public boolean hasError() {
    return errorMgr.hasError();
  }
  
  public int getErrorCount() {
    return errorMgr.getErrorCount();
  }
  
  public ProcessError getError(int errorNum) {
    return errorMgr.getError(errorNum);
  }
  
  public boolean hasFatalError() {
    return errorMgr.hasFatalError();
  }
}
