/*@lineinfo:filename=MasterCardFeeCollection*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/MasterCardFeeCollection.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2008,2009 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.settlement.SettlementRecord;
import sqlj.runtime.ResultSetIterator;

public class MasterCardFeeCollection extends MasterCardFileBase
{
  public MasterCardFeeCollection( )
  {
    PropertiesFilename          = "mc-fee-collection.properties";

    CardTypeTable               = "mc_settlement_fee_collect";
    CardTypeTableActivity       = null;
    CardTypePostTotals          = "mc_fee";
    CardTypeFileDesc            = "MC Fee Collect";
    DkOutgoingHost              = MesDefaults.DK_MC_EP_HOST;
    DkOutgoingUser              = MesDefaults.DK_MC_EP_USER;
    DkOutgoingPassword          = MesDefaults.DK_MC_EP_PASSWORD;
    DkOutgoingPath              = MesDefaults.DK_MC_EP_OUTGOING_PATH;
    DkOutgoingUseBinary         = true;
    DkOutgoingSendFlagFile      = true;
    SettlementAddrsNotify       = MesEmails.MSG_ADDRS_OUTGOING_MC_NOTIFY;
    SettlementAddrsFailure      = MesEmails.MSG_ADDRS_OUTGOING_MC_FAILURE;
    SettlementRecMT             = SettlementRecord.MT_FEE_COLLECTION;
  }
  
  protected boolean processTransactions()
  {
    Date                      cpd               = null;
    ResultSetIterator         it                = null;
    long                      workFileId        = 0L;
    String                    workFilename      = null;
    
    try
    {
      if ( TestFilename == null )
      {
        FileTimestamp   = new Timestamp(Calendar.getInstance().getTime().getTime());

        cpd             = getSettlementDate();

        workFilename    = generateFilename("mc_fee"                                                     // base name
                                          + String.valueOf(Integer.parseInt( getEventArg(0) )),         // bank number
                                          ".ipm");                                                      // file extension
        workFileId      = loadFilenameToLoadFileId(workFilename);

        /*@lineinfo:generated-code*//*@lineinfo:76^9*/

//  ************************************************************
//  #sql [Ctx] { update  mc_settlement_fee_collect   fc
//            set     fc.load_filename    = :workFilename,
//                    fc.load_file_id     = :workFileId,
//                    fc.settlement_cycle = 0,
//                    fc.settlement_date  = :cpd,
//                    fc.recon_cycle      = 0,
//                    fc.recon_date       = :cpd
//            where   fc.load_file_id = 0
//                    and fc.tran_type = 1    -- outgoing
//                    and nvl(fc.test_flag,'Y') = 'N'
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mc_settlement_fee_collect   fc\n          set     fc.load_filename    =  :1 ,\n                  fc.load_file_id     =  :2 ,\n                  fc.settlement_cycle = 0,\n                  fc.settlement_date  =  :3 ,\n                  fc.recon_cycle      = 0,\n                  fc.recon_date       =  :4 \n          where   fc.load_file_id = 0\n                  and fc.tran_type = 1    -- outgoing\n                  and nvl(fc.test_flag,'Y') = 'N'";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.MasterCardFeeCollection",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,workFilename);
   __sJT_st.setLong(2,workFileId);
   __sJT_st.setDate(3,cpd);
   __sJT_st.setDate(4,cpd);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:88^9*/
      }
      else    // re-build a previous file
      {
        FileTimestamp   = new Timestamp( getFileDate(TestFilename).getTime() );
        workFilename    = TestFilename;
        workFileId      = loadFilenameToLoadFileId(workFilename);
      }

      // select the transactions to process
      /*@lineinfo:generated-code*//*@lineinfo:98^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  null                                        as action_code,
//                  0                                           as batch_id,          -- not used in fee collection tables
//                  fc.rec_id                                   as batch_record_id,
//                  dukpt_decrypt_wrapper(fc.card_number_enc)   as card_number_full,
//                  fc.function_code                            as function_code,
//                  fc.processing_code                          as processing_code,
//                  fc.reference_number                         as reference_number,
//                  fc.transaction_amount                       as transaction_amount,
//                  nvl(fc.currency_code,'840')                 as currency_code,
//                  fc.reason_code                              as reason_code,
//                  fc.source_ica                               as ica_number,
//                  fc.dest_ica                                 as dest_ica_number,
//                  fc.message_text                             as message_text,
//                  rt.reason_code                              as original_reason_code
//          from    mc_settlement_fee_collect     fc,
//                  network_retrievals            rt
//          where   fc.load_file_id = :workFileId
//                  and rt.retr_load_sec(+) = fc.settle_rec_id
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  null                                        as action_code,\n                0                                           as batch_id,          -- not used in fee collection tables\n                fc.rec_id                                   as batch_record_id,\n                dukpt_decrypt_wrapper(fc.card_number_enc)   as card_number_full,\n                fc.function_code                            as function_code,\n                fc.processing_code                          as processing_code,\n                fc.reference_number                         as reference_number,\n                fc.transaction_amount                       as transaction_amount,\n                nvl(fc.currency_code,'840')                 as currency_code,\n                fc.reason_code                              as reason_code,\n                fc.source_ica                               as ica_number,\n                fc.dest_ica                                 as dest_ica_number,\n                fc.message_text                             as message_text,\n                rt.reason_code                              as original_reason_code\n        from    mc_settlement_fee_collect     fc,\n                network_retrievals            rt\n        where   fc.load_file_id =  :1 \n                and rt.retr_load_sec(+) = fc.settle_rec_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.MasterCardFeeCollection",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,workFileId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.MasterCardFeeCollection",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:118^7*/

      FileReferenceId = buildFileReferenceId( workFilename, "3" );
      handleSelectedRecords( it, workFilename );
    }
    catch( Exception e )
    {
      logEvent(this.getClass().getName(), "processTransactions()", e.toString());
      logEntry("processTransactions()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch(Exception e) {}
    }
    return( true );
  }
  
  public static void main( String[] args )
  {
    MasterCardFeeCollection           test          = null;
    
    try
    { 
        if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{
                    MesDefaults.DK_MC_EP_HOST,
                    MesDefaults.DK_MC_EP_USER,
                    MesDefaults.DK_MC_EP_PASSWORD,
                    MesDefaults.DK_MC_EP_OUTGOING_PATH
            });
        }

      test = new MasterCardFeeCollection();
      test.executeCommandLine( args );
    }
    catch( Exception e )
    {
      log.debug(e.toString());
    }
    finally
    {
      try{ test.cleanUp(); } catch( Exception e ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}/*@lineinfo:generated-code*/