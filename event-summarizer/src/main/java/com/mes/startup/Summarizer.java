/*@lineinfo:filename=Summarizer*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/Summarizer.sqlj $

  Description:

  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-11-12 14:40:05 -0800 (Thu, 12 Nov 2015) $
  Version            : $Revision: 23990 $

  Change History:
     See SVN database

  Copyright (C) 2000-2011,2012 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.DateTimeFormatter;
import com.mes.tools.FileTransmissionLogger;
import sqlj.runtime.ResultSetIterator;

public class Summarizer extends EventBase
{
  static Logger log = Logger.getLogger(Summarizer.class);
  
  public static final int     SUM_TYPE_DDF                = 1;
  public static final int     MAX_DELETE_BEFORE_COMMIT    = 500;

  private static final int    CT_VISA                     = 0;
  private static final int    CT_MC                       = 1;
  private static final int    CT_DEBIT                    = 2;
  private static final int    CT_AMEX                     = 3;
  private static final int    CT_DISC                     = 4;
  private static final int    CT_DINERS                   = 5;
  private static final int    CT_JCB                      = 6;
  private static final int    CT_OTHER                    = 7;
  private static final int    CT_BML                      = 8; //bill me later
  
  private static final int    CT_COUNT                    = 9; 

  private static final int    TT_SALES                    = 0;
  private static final int    TT_CREDITS                  = 1;
  private static final int    TT_COUNT                    = 2;
  
  // data types:  DDF_DT = daily_detail_file_dt, DDF_EXT_DT = daily_detail_file_ext_dt
  private static final int    DT_DDF_DT                   = 0;
  private static final int    DT_DDF_EXT_DT               = 1;

  private static final String[][] CardTypeMap =
  {
    { "VS", Integer.toString(CT_VISA)   },
    { "VB", Integer.toString(CT_VISA)   },
    { "VD", Integer.toString(CT_VISA)   },
    { "MC", Integer.toString(CT_MC)     },
    { "MB", Integer.toString(CT_MC)     },
    { "MD", Integer.toString(CT_MC)     },
    { "DB", Integer.toString(CT_DEBIT)  },
    { "EB", Integer.toString(CT_DEBIT)  },   // EBT
    { "AM", Integer.toString(CT_AMEX)   },
    { "DS", Integer.toString(CT_DISC)   },
    { "DC", Integer.toString(CT_DINERS) },
    { "CB", Integer.toString(CT_DINERS) },
    { "JC", Integer.toString(CT_JCB)    },
    { "BL", Integer.toString(CT_BML)    },
  };


  private int                 procSequence                = 0;
  private Vector              sumItems                    = new Vector();

  private Date                batchDate;
  private long                merchantNumber;
  private int                 batchCount;
  private long                batchNumber;
  private int                 bankCount;
  private double              bankAmount;
  private int                 nonbankCount;
  private double              nonbankAmount;
  private String              loadFilename;
  private int                 LoadFileId                  = 0;
  private boolean             isDailyDetail               = false;
  private int                 bankNumber                  = 0;
  private boolean[]           HasData                     = new boolean[2];

  public Summarizer()
  {
  }
  
  protected void setDataFlags( String loadFilename )
  {
    try
    {
      int       recCount    = 0;
      String    tableName   = null;
      
      for ( int i = 0; i < 2; ++i )
      {
        tableName = ((i == 0) ? "daily_detail_file_dt" : "daily_detail_file_ext_dt");
        
        /*@lineinfo:generated-code*//*@lineinfo:118^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//            
//            from    :tableName
//            where   load_filename = :loadFilename
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   java.lang.StringBuffer __sjT_sb = new java.lang.StringBuffer();
   __sjT_sb.append("select  count(1)\n           \n          from     ");
   __sjT_sb.append(tableName);
   __sjT_sb.append(" \n          where   load_filename =  ?");
   String __sjT_sql = __sjT_sb.toString();
   String __sjT_tag = "0com.mes.startup.Summarizer:" + __sjT_sql;
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = __sjT_sql;
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,__sjT_tag,theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:124^9*/
        
        HasData[i] = (recCount > 0);
      }
    }
    catch( Exception e )
    {
      logEntry("setDataFlags(" + loadFilename + ")",e.toString());
    }
  }

  protected void init()
  {
    merchantNumber = 0L;
    batchDate = null;
    batchCount = 0;
    batchNumber = 0L;
    bankCount = 0;
    bankAmount = 0.0;
    nonbankCount = 0;
    nonbankAmount = 0;
    loadFilename = "";
  }

  /*
  ** METHOD getProcSequence
  **
  ** Establishes the transcom process sequence
  */
  private void getProcSequence()
  {
    try
    {
      // get the processSequence
      /*@lineinfo:generated-code*//*@lineinfo:158^7*/

//  ************************************************************
//  #sql [Ctx] { select  summarizer_process_sequence.nextval 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  summarizer_process_sequence.nextval  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.Summarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   procSequence = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:162^7*/

    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "getProcSequence()", e.toString());
      setError("getProcSequence(): " + e.toString());
      logEntry("getProcSequence()", e.toString());
    }
  }

  public void logFailure(SummarizeItem item, String errString)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:177^7*/

//  ************************************************************
//  #sql [Ctx] { update  summarizer_process
//          set     process_status      = :errString
//          where   file_type           = :item.fileType and
//                  load_filename       = :item.loadFilename and
//                  process_sequence    = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  summarizer_process\n        set     process_status      =  :1 \n        where   file_type           =  :2  and\n                load_filename       =  :3  and\n                process_sequence    =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"2com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,errString);
   __sJT_st.setInt(2,item.fileType);
   __sJT_st.setString(3,item.loadFilename);
   __sJT_st.setInt(4,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:184^7*/
    }
    catch(Exception e)
    {
      logEntry("logFailure(" + item.loadFilename + ")", e.toString());
    }
  }

  /*
  ** METHOD summarize
  **
  ** Handles summarization of a single load file.
  */
  public boolean summarize(SummarizeItem item)
  {
    boolean       success         = false;
    Timestamp     startTime       = new Timestamp(Calendar.getInstance().getTime().getTime());
    Timestamp     endTime         = null;
    long          elapsed         = 0L;

    try
    {
      // update summarizer_process to show start time
      /*@lineinfo:generated-code*//*@lineinfo:207^7*/

//  ************************************************************
//  #sql [Ctx] { update  summarizer_process
//          set     process_start_date  = :startTime
//          where   file_type           = :item.fileType and
//                  load_filename       = :item.loadFilename and
//                  process_sequence    = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  summarizer_process\n        set     process_start_date  =  :1 \n        where   file_type           =  :2  and\n                load_filename       =  :3  and\n                process_sequence    =  :4";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"3com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,startTime);
   __sJT_st.setInt(2,item.fileType);
   __sJT_st.setString(3,item.loadFilename);
   __sJT_st.setInt(4,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:214^7*/

      log.debug("summarizing: " + item.loadFilename);

      // determine which tables have data for this load filename
      setDataFlags(item.loadFilename);
      
      switch(item.fileType)
      {
        case SUM_TYPE_DDF:
          // daily detail file
          success = updateTranSummary(item.loadFilename);

          if( HasData[DT_DDF_DT] )
          {
            // store the daily discount summary data
            storeDiscSummary(item.loadFilename);

            // store the daily interchange summary data
            storeIcSummary(item.loadFilename);
          
            // for all DDF files, load the foreign card exceptions
            /*@lineinfo:generated-code*//*@lineinfo:236^13*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_process
//                  ( process_type, load_filename )
//                values
//                  ( 9, :item.loadFilename )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_process\n                ( process_type, load_filename )\n              values\n                ( 9,  :1  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,item.loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:242^13*/
            
            // score using ddf_dt data
            /*@lineinfo:generated-code*//*@lineinfo:245^13*/

//  ************************************************************
//  #sql [Ctx] { insert into risk_process
//                  ( process_type, load_filename )
//                values
//                  ( 5, :item.loadFilename )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into risk_process\n                ( process_type, load_filename )\n              values\n                ( 5,  :1  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"5com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,item.loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:251^13*/
          }
          break;

        default:
          break;
      }

      if(success)
      {
        endTime = new Timestamp(Calendar.getInstance().getTime().getTime());

        // get time elapsed (in hh:mm:ss)
        elapsed = endTime.getTime() - startTime.getTime();

        // update summarizer_process to show start time, end time, and elapsed
        /*@lineinfo:generated-code*//*@lineinfo:267^9*/

//  ************************************************************
//  #sql [Ctx] { update  summarizer_process
//            set     process_start_date  = :startTime,
//                    process_end_date    = :endTime,
//                    process_elapsed     = :DateTimeFormatter.getFormattedTimestamp(elapsed),
//                    process_status      = 'Success'
//            where   file_type           = :item.fileType and
//                    load_filename       = :item.loadFilename and
//                    process_sequence    = :procSequence
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 String __sJT_4673 = DateTimeFormatter.getFormattedTimestamp(elapsed);
   String theSqlTS = "update  summarizer_process\n          set     process_start_date  =  :1 ,\n                  process_end_date    =  :2 ,\n                  process_elapsed     =  :3 ,\n                  process_status      = 'Success'\n          where   file_type           =  :4  and\n                  load_filename       =  :5  and\n                  process_sequence    =  :6";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setTimestamp(1,startTime);
   __sJT_st.setTimestamp(2,endTime);
   __sJT_st.setString(3,__sJT_4673);
   __sJT_st.setInt(4,item.fileType);
   __sJT_st.setString(5,item.loadFilename);
   __sJT_st.setInt(6,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:277^9*/

        // log file as having been summarized
        FileTransmissionLogger.logStage(item.loadFilename, FileTransmissionLogger.STAGE_TRAN_SUMMARY);
      }
      else
      {
        // mark file as failure
        /*@lineinfo:generated-code*//*@lineinfo:285^9*/

//  ************************************************************
//  #sql [Ctx] { update  summarizer_process
//            set     process_status = 'FAILURE: success was false'
//            where   file_type           = :item.fileType and
//                    load_filename       = :item.loadFilename and
//                    process_sequence    = :procSequence
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  summarizer_process\n          set     process_status = 'FAILURE: success was false'\n          where   file_type           =  :1  and\n                  load_filename       =  :2  and\n                  process_sequence    =  :3";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"7com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,item.fileType);
   __sJT_st.setString(2,item.loadFilename);
   __sJT_st.setInt(3,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:292^9*/

        // set file to retry.  maybe it will work the next time...
        /*@lineinfo:generated-code*//*@lineinfo:295^9*/

//  ************************************************************
//  #sql [Ctx] { insert into summarizer_process
//            (
//              file_type,
//              load_filename
//            )
//            values
//            (
//              :item.fileType,
//              :item.loadFilename
//            )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into summarizer_process\n          (\n            file_type,\n            load_filename\n          )\n          values\n          (\n             :1 ,\n             :2 \n          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"8com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,item.fileType);
   __sJT_st.setString(2,item.loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:307^9*/
      }

      // make sure database shows progress
      commit();
    }
    catch(Exception e)
    {
      logFailure(item, e.toString());
      logEvent(this.getClass().getName(), "summarize(" + item.loadFilename + ")", e.toString());
      setError("summarize(" + item.loadFilename + "): " + e.toString());
      logEntry("summarize(" + item.loadFilename + ")", e.toString());
    }

    return success;
  }

  /*
  ** METHOD execute
  **
  ** Entry point from timed event manager
  */
  public boolean execute()
  {
    ResultSetIterator   it      = null;
    ResultSet           rs      = null;
    boolean             result  = true;

    try
    {
      //@log.debug("\nExecuting Summarizer");

      // turning this into timeout exempt connection due
      // to occasional timeouts on 3941 summarization, etc.
      connect(true);

      // get process sequence number
      //@log.debug("  Getting process sequence");
      getProcSequence();

      // mark interesting rows with the process sequence
      //@log.debug("  Marking records with process sequence");
      /*@lineinfo:generated-code*//*@lineinfo:349^7*/

//  ************************************************************
//  #sql [Ctx] { update  summarizer_process
//          set     process_sequence = :procSequence
//          where   process_sequence is null
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  summarizer_process\n        set     process_sequence =  :1 \n        where   process_sequence is null";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"9com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:354^7*/

      // make sure database shows that we are processing
      commit();

      // get all the interesting rows into a vector
      //@log.debug("  Building Vector");
      /*@lineinfo:generated-code*//*@lineinfo:361^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  file_type,
//                  load_filename
//          from    summarizer_process
//          where   process_sequence = :procSequence
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  file_type,\n                load_filename\n        from    summarizer_process\n        where   process_sequence =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,procSequence);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"10com.mes.startup.Summarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:367^7*/

      rs = it.getResultSet();

      while(rs.next())
      {
        //@log.debug("  Got item: " + rs.getString("load_filename"));
        sumItems.add(new SummarizeItem( rs.getInt("file_type"),
                                        rs.getString("load_filename")));
      }

      rs.close();
      it.close();

      //@log.debug("  Summarizing items");
      if(isConnectionStale())
      {
       //@log.debug("CONNECTION:  Dropped in execute somewhere");
      }

      for(int i=0; i < sumItems.size(); ++i)
      {
        SummarizeItem item = (SummarizeItem)(sumItems.elementAt(i));
        result = summarize(item);
      }
    }
    catch(Exception e)
    {
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }

    return result;
  }

  private void deleteDDF(String loadFilename)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:410^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    daily_detail_file_summary
//          where   load_file_id = load_filename_to_load_file_id(:loadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    daily_detail_file_summary\n        where   load_file_id = load_filename_to_load_file_id( :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:415^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("deleteDDF(" + loadFilename + ")", e.toString());
    }
    finally
    {
    }
  }

  private void deleteEXT(String loadFilename)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:432^7*/

//  ************************************************************
//  #sql [Ctx] { delete
//          from    daily_detail_file_ext_summary
//          where   load_file_id = load_filename_to_load_file_id(:loadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n        from    daily_detail_file_ext_summary\n        where   load_file_id = load_filename_to_load_file_id( :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:437^7*/
      commit();
    }
    catch(Exception e)
    {
      logEntry("deleteEXT(" + loadFilename + ")", e.toString());
    }
    finally
    {
    }
  }

  protected boolean updateTranSummary(String loadFilename)
  {
    Date                        batchDateMax      = null;
    Date                        batchDateMin      = null;
    ResultSetIterator           it                = null;
    long                        loadFileId        = 0L;
    ResultSet                   rs                = null;
    String                      status            = "start update";
    Vector                      sumRecs           = new Vector();
    int                         recCount          = 0;
    boolean                     result            = false;

    // fill the cursor with query data
    try
    {
      log.debug("\n------- Starting summarization ---------");
      // set the bank number
      bankNumber = getFileBankNumber(loadFilename);
      log.debug("Bank Number = " + bankNumber);

      // initialize the load file index
      /*@lineinfo:generated-code*//*@lineinfo:470^7*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init( :loadFilename )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init(  :1  )\n      \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"13com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:473^7*/

      // lookup the index that was assigned to the current file
      /*@lineinfo:generated-code*//*@lineinfo:476^7*/

//  ************************************************************
//  #sql [Ctx] { select  load_filename_to_load_file_id( :loadFilename ) 
//          from    dual
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  load_filename_to_load_file_id(  :1  )  \n        from    dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"14com.mes.startup.Summarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   LoadFileId = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:480^7*/

      for( int i = 0; i < 2; ++i )
      {
        if ( !HasData[i] ) continue;  // skip when there is no data in the table

        if( i == 0 )  // internal
        {
          isDailyDetail = true;

          // summarize transactions by transaction date for IRS 1099 reporting
          summarizeIRS(loadFilename);

          // find any existing records for this load filename
          status = "finding existing ddf summary records";
          /*@lineinfo:generated-code*//*@lineinfo:495^11*/

//  ************************************************************
//  #sql [Ctx] { select  min(batch_date),max(batch_date)
//              
//              from    daily_detail_file_dt
//              where   load_filename = :loadFilename
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  min(batch_date),max(batch_date)\n             \n            from    daily_detail_file_dt\n            where   load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"15com.mes.startup.Summarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   batchDateMin = (java.sql.Date)__sJT_rs.getDate(1);
   batchDateMax = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:501^11*/

          /*@lineinfo:generated-code*//*@lineinfo:503^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//              
//              from    daily_detail_file_summary
//              where   load_file_id = :LoadFileId
//                      and batch_date between :batchDateMin and :batchDateMax
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n             \n            from    daily_detail_file_summary\n            where   load_file_id =  :1 \n                    and batch_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"16com.mes.startup.Summarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,LoadFileId);
   __sJT_st.setDate(2,batchDateMin);
   __sJT_st.setDate(3,batchDateMax);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:510^11*/

          if(recCount > 0)
          {
           //@log.debug("Deleting: " + loadFilename);
            status = "deleting ddf summary records";
            deleteDDF(loadFilename);
          }

          status = "getting new ddf summary records";
          /*@lineinfo:generated-code*//*@lineinfo:520^11*/

//  ************************************************************
//  #sql [Ctx] it = { select    merchant_account_number       merchant_number,
//                        bank_number                   bank_number,
//                        batch_date                    batch_date,
//                        batch_number                  batch_number,
//                        nvl(card_type, 'UK')          card_type,
//                        debit_credit_indicator        debit_credit_ind,
//                        decode(reject_reason,
//                               null,'N','Y')          reject,
//                        count(card_type)              tran_count,
//                        sum(transaction_amount)       tran_amount,
//                        sum(nvl(fx_markup_amount,0))  fx_markup_amount,
//                        load_filename                 load_filename
//              from      daily_detail_file_dt
//              where     load_filename = :loadFilename and
//                        merchant_account_number > 0 and
//                        not ( nvl(pos_entry_mode,'01') = 'NA' and
//                              net_deposit = 0 )
//              group by  batch_date,
//                        merchant_account_number,
//                        bank_number,
//                        batch_number,
//                        card_type,
//                        debit_credit_indicator,
//                        decode(reject_reason,null,'N','Y'),
//                        load_filename
//              order by  batch_date,
//                        merchant_account_number,
//                        batch_number
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    merchant_account_number       merchant_number,\n                      bank_number                   bank_number,\n                      batch_date                    batch_date,\n                      batch_number                  batch_number,\n                      nvl(card_type, 'UK')          card_type,\n                      debit_credit_indicator        debit_credit_ind,\n                      decode(reject_reason,\n                             null,'N','Y')          reject,\n                      count(card_type)              tran_count,\n                      sum(transaction_amount)       tran_amount,\n                      sum(nvl(fx_markup_amount,0))  fx_markup_amount,\n                      load_filename                 load_filename\n            from      daily_detail_file_dt\n            where     load_filename =  :1  and\n                      merchant_account_number > 0 and\n                      not ( nvl(pos_entry_mode,'01') = 'NA' and\n                            net_deposit = 0 )\n            group by  batch_date,\n                      merchant_account_number,\n                      bank_number,\n                      batch_number,\n                      card_type,\n                      debit_credit_indicator,\n                      decode(reject_reason,null,'N','Y'),\n                      load_filename\n            order by  batch_date,\n                      merchant_account_number,\n                      batch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"17com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"17com.mes.startup.Summarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:550^11*/
        }
        else  // external
        {
          isDailyDetail = false;
          
          // find any existing records for this load filename
         //@log.debug("looking for existing records in summary table");
          status = "finding existing ext summary records";
          /*@lineinfo:generated-code*//*@lineinfo:559^11*/

//  ************************************************************
//  #sql [Ctx] { select  min(batch_date),max(batch_date)
//              
//              from    daily_detail_file_ext_dt
//              where   load_filename = :loadFilename
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  min(batch_date),max(batch_date)\n             \n            from    daily_detail_file_ext_dt\n            where   load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"18com.mes.startup.Summarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   batchDateMin = (java.sql.Date)__sJT_rs.getDate(1);
   batchDateMax = (java.sql.Date)__sJT_rs.getDate(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:565^11*/
          /*@lineinfo:generated-code*//*@lineinfo:566^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(merchant_number)
//              
//              from    daily_detail_file_ext_summary
//              where   load_file_id = :LoadFileId
//                      and batch_date between :batchDateMin and :batchDateMax
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(merchant_number)\n             \n            from    daily_detail_file_ext_summary\n            where   load_file_id =  :1 \n                    and batch_date between  :2  and  :3";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"19com.mes.startup.Summarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setInt(1,LoadFileId);
   __sJT_st.setDate(2,batchDateMin);
   __sJT_st.setDate(3,batchDateMax);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:573^11*/

         //@log.debug("found: " + recCount);
          if(recCount > 0)
          {
           //@log.debug("deleting items from summary table");
            status = "deleting ext summary records";
            deleteEXT(loadFilename);
          }

         //@log.debug("getting records to summarize (query)");
          status = "getting new ext summary records";
          /*@lineinfo:generated-code*//*@lineinfo:585^11*/

//  ************************************************************
//  #sql [Ctx] it = { select    dt.merchant_number                merchant_number,
//                        :bankNumber                       bank_number,
//                        dt.batch_date                     batch_date,
//                        dt.batch_number                   batch_number,
//                        dt.card_type                      card_type,
//                        nvl(dt.debit_credit_indicator,'D') debit_credit_ind,
//                        'N'                               reject,
//                        count(dt.card_type)               tran_count,
//                        sum(dt.transaction_amount)        tran_amount,
//                        dt.load_filename                  load_filename
//              from      daily_detail_file_ext_dt  dt
//              where     dt.load_filename = :loadFilename and
//                        (
//                          substr(dt.card_type,1,1) in ('V','M') or      -- bank
//                          dt.card_type in ( 'DB','EB', 'BL',            -- bank
//                                            'AM','DS','DC','JC', 'PL' ) -- non-bank
//                        )
//              --and dt.debit_credit_indicator is not null
//              group by  dt.batch_date, dt.merchant_number, :bankNumber,
//                        dt.batch_number, dt.card_type,
//                        dt.debit_credit_indicator, dt.load_filename
//              order by  dt.batch_date, dt.merchant_number, dt.batch_number
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    dt.merchant_number                merchant_number,\n                       :1                        bank_number,\n                      dt.batch_date                     batch_date,\n                      dt.batch_number                   batch_number,\n                      dt.card_type                      card_type,\n                      nvl(dt.debit_credit_indicator,'D') debit_credit_ind,\n                      'N'                               reject,\n                      count(dt.card_type)               tran_count,\n                      sum(dt.transaction_amount)        tran_amount,\n                      dt.load_filename                  load_filename\n            from      daily_detail_file_ext_dt  dt\n            where     dt.load_filename =  :2  and\n                      (\n                        substr(dt.card_type,1,1) in ('V','M') or      -- bank\n                        dt.card_type in ( 'DB','EB', 'BL',            -- bank\n                                          'AM','DS','DC','JC', 'PL' ) -- non-bank\n                      )\n            --and dt.debit_credit_indicator is not null\n            group by  dt.batch_date, dt.merchant_number,  :3 ,\n                      dt.batch_number, dt.card_type,\n                      dt.debit_credit_indicator, dt.load_filename\n            order by  dt.batch_date, dt.merchant_number, dt.batch_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"20com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   __sJT_st.setString(2,loadFilename);
   __sJT_st.setInt(3,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"20com.mes.startup.Summarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:609^11*/
        }

        status  = "creating summary records vector";
        rs      = it.getResultSet();

       //@log.debug("creating sumRecs vector");
        while(rs.next())
        {
          // add summary records to vector
          sumRecs.add(new SummarizeRecord(rs));
        }

        rs.close();
        it.close();

       //@log.debug("Creating summary records");
        status = "adding summary records to database";
        createSummaryRecords(sumRecs);

        sumRecs.clear();
      }

      // if everything happened without an exception, result was success
      result = true;
    }
    catch(Exception e)
    {
      logEntry("updateTranSummary(" + loadFilename + ")", status + " :: " + e.toString());
    }
    finally
    {
      try { rs.close(); } catch(Exception e) { }
      try { it.close(); } catch(Exception e) { }
      sumRecs.clear();
    }

    return result;
  }
  
  private void summarizeIRS(String loadFilename)
  {
    try
    {
      int recCount = 0;
      
      // see if this file has already been loaded
      /*@lineinfo:generated-code*//*@lineinfo:656^7*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//          
//          from    daily_detail_file_irs_summary
//          where   load_file_id = load_filename_to_load_file_id(:loadFilename)
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n         \n        from    daily_detail_file_irs_summary\n        where   load_file_id = load_filename_to_load_file_id( :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"21com.mes.startup.Summarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   recCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:662^7*/
      
      if( recCount > 0 )
      {
        // remove existing data
        /*@lineinfo:generated-code*//*@lineinfo:667^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    daily_detail_file_irs_summary
//            where   load_file_id = load_filename_to_load_file_id(:loadFilename)
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    daily_detail_file_irs_summary\n          where   load_file_id = load_filename_to_load_file_id( :1 )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"22com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:672^9*/
        
        commit();
      }
      //PROD-1512 new column population CARD_NOT_PRESENT_AMOUNT by Igor M.
      // add summarized records
      /*@lineinfo:generated-code*//*@lineinfo:678^7*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_file_irs_summary
//          (
//            merchant_number,
//            transaction_date,
//            load_file_id,
//            batch_date,
//            load_filename,
//            transaction_count,
//            transaction_amount,
//            CARD_NOT_PRESENT_AMOUNT
//          )
//          select  dt.merchant_account_number,
//                  dt.transaction_date,
//                  load_filename_to_load_file_id(dt.load_filename),
//                  dt.batch_date,
//                  dt.load_filename,
//                  count(1),
//                  sum(dt.transaction_amount),
//                  sum(CASE WHEN CARD_PRESENT= 'N' THEN dt.transaction_amount ELSE 0 END) as CARD_NOT_PRESENT_AMOUNT
//          from    daily_detail_file_dt dt
//          where   dt.load_filename = :loadFilename
//                  and dt.debit_credit_indicator = 'D'
//          group by dt.merchant_account_number, dt.batch_date, dt.transaction_date, load_filename_to_load_file_id(dt.load_filename), dt.load_filename
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into daily_detail_file_irs_summary\n        (\n          merchant_number,\n          transaction_date,\n          load_file_id,\n          batch_date,\n          load_filename,\n          transaction_count,\n          transaction_amount,\n          CARD_NOT_PRESENT_AMOUNT\n        )\n        select  dt.merchant_account_number,\n                dt.transaction_date,\n                load_filename_to_load_file_id(dt.load_filename),\n                dt.batch_date,\n                dt.load_filename,\n                count(1),\n                sum(dt.transaction_amount),\n                sum(CASE WHEN CARD_PRESENT= 'N' THEN dt.transaction_amount ELSE 0 END) as CARD_NOT_PRESENT_AMOUNT\n        from    daily_detail_file_dt dt\n        where   dt.load_filename =  :1 \n                and dt.debit_credit_indicator = 'D'\n        group by dt.merchant_account_number, dt.batch_date, dt.transaction_date, load_filename_to_load_file_id(dt.load_filename), dt.load_filename";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"23com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:703^7*/
      
      commit();
    }
    catch(Exception e)
    {
      logEntry("summarizeIRS(" + loadFilename + ")", e.toString());
    }
  }

  private void createSummaryRecords(Vector sumRecs)
  {
    double        bankCreditsAmt            = 0;
    int           bankCreditsCnt            = 0;
    double        bankSalesAmt              = 0;
    int           bankSalesCnt              = 0;
    double        bankRejectCreditsAmt      = 0;
    int           bankRejectCreditsCnt      = 0;
    double        bankRejectSalesAmt        = 0;
    int           bankRejectSalesCnt        = 0;
    int           cardIndex                 = 0;
    String        cardType                  = null;
    double[]      fxMarkupAmount            = new double[CT_COUNT];
    double        nonBankCreditsAmt         = 0;
    int           nonBankCreditsCnt         = 0;
    double        nonBankSalesAmt           = 0;
    int           nonBankSalesCnt           = 0;
    double        nonBankRejectCreditsAmt   = 0;
    int           nonBankRejectCreditsCnt   = 0;
    double        nonBankRejectSalesAmt     = 0;
    int           nonBankRejectSalesCnt     = 0;
    int           tranIndex                 = 0;
    double[][]    volAmount                 = new double[CT_COUNT][TT_COUNT];
    int[][]       volCount                  = new int[CT_COUNT][TT_COUNT];

    init();

    try
    {
      for(int j=0; j < sumRecs.size(); ++j)
      {
        // get the summarize record from the vector
        SummarizeRecord sr = (SummarizeRecord)(sumRecs.elementAt(j));

        // check for merchant number or batch number change
        if( (merchantNumber != sr.merchantNumber && merchantNumber != 0L) ||
            (batchDate != null && !batchDate.equals(sr.batchDate)) )
        {
          try
          {
            if ( isDailyDetail == true )      // processing ddf
            {
              /*@lineinfo:generated-code*//*@lineinfo:755^15*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_file_summary
//                              (
//                                batch_date,
//                                merchant_number,
//                                bank_number,
//                                bank_count,
//                                bank_amount,
//                                nonbank_count,
//                                nonbank_amount,
//                                load_filename,
//                                load_file_id,
//                                batch_count,
//                                bank_credits_amount,
//                                bank_credits_count,
//                                bank_sales_amount,
//                                bank_sales_count,
//                                nonbank_credits_amount,
//                                nonbank_credits_count,
//                                nonbank_sales_amount,
//                                nonbank_sales_count,
//                                bank_reject_amount,
//                                bank_reject_count,
//                                bank_reject_credits_amount,
//                                bank_reject_credits_count,
//                                bank_reject_sales_amount,
//                                bank_reject_sales_count,
//                                nonbank_reject_amount,
//                                nonbank_reject_count,
//                                nonbank_reject_credits_amount,
//                                nonbank_reject_credits_count,
//                                nonbank_reject_sales_amount,
//                                nonbank_reject_sales_count,
//                                visa_sales_amount,
//                                visa_sales_count,
//                                visa_credits_amount,
//                                visa_credits_count,
//                                mc_sales_amount,
//                                mc_sales_count,
//                                mc_credits_amount,
//                                mc_credits_count,
//                                debit_sales_amount,
//                                debit_sales_count,
//                                debit_credits_amount,
//                                debit_credits_count,
//                                amex_sales_amount,
//                                amex_sales_count,
//                                amex_credits_amount,
//                                amex_credits_count,
//                                disc_sales_amount,
//                                disc_sales_count,
//                                disc_credits_amount,
//                                disc_credits_count,
//                                diners_sales_amount,
//                                diners_sales_count,
//                                diners_credits_amount,
//                                diners_credits_count,
//                                jcb_sales_amount,
//                                jcb_sales_count,
//                                jcb_credits_amount,
//                                jcb_credits_count,
//                                other_sales_amount,
//                                other_sales_count,
//                                other_credits_amount,
//                                other_credits_count,
//                                visa_fx_markup_amount,
//                                mc_fx_markup_amount,
//								  LAST_MODIFIED_DATE            	
//                              )
//                  values      (
//                                :batchDate,
//                                :merchantNumber,
//                                :bankNumber,
//                                :bankCount,
//                                :bankAmount,
//                                :nonbankCount,
//                                :nonbankAmount,
//                                :loadFilename,
//                                :LoadFileId,
//                                :batchCount,
//                                :bankCreditsAmt,
//                                :bankCreditsCnt,
//                                :bankSalesAmt,
//                                :bankSalesCnt,
//                                :nonBankCreditsAmt,
//                                :nonBankCreditsCnt,
//                                :nonBankSalesAmt,
//                                :nonBankSalesCnt,
//                                :bankRejectSalesAmt - bankRejectCreditsAmt,
//                                :bankRejectSalesCnt + bankRejectCreditsCnt,
//                                :bankRejectCreditsAmt,
//                                :bankRejectCreditsCnt,
//                                :bankRejectSalesAmt,
//                                :bankRejectSalesCnt,
//                                :nonBankRejectSalesAmt - nonBankRejectCreditsAmt,
//                                :nonBankRejectSalesCnt + nonBankRejectCreditsCnt,
//                                :nonBankRejectCreditsAmt,
//                                :nonBankRejectCreditsCnt,
//                                :nonBankRejectSalesAmt,
//                                :nonBankRejectSalesCnt,
//                                :volAmount[CT_VISA][TT_SALES],
//                                :volCount[CT_VISA][TT_SALES],
//                                :volAmount[CT_VISA][TT_CREDITS],
//                                :volCount[CT_VISA][TT_CREDITS],
//                                :volAmount[CT_MC][TT_SALES],
//                                :volCount[CT_MC][TT_SALES],
//                                :volAmount[CT_MC][TT_CREDITS],
//                                :volCount[CT_MC][TT_CREDITS],
//                                :volAmount[CT_DEBIT][TT_SALES],
//                                :volCount[CT_DEBIT][TT_SALES],
//                                :volAmount[CT_DEBIT][TT_CREDITS],
//                                :volCount[CT_DEBIT][TT_CREDITS],
//                                :volAmount[CT_AMEX][TT_SALES],
//                                :volCount[CT_AMEX][TT_SALES],
//                                :volAmount[CT_AMEX][TT_CREDITS],
//                                :volCount[CT_AMEX][TT_CREDITS],
//                                :volAmount[CT_DISC][TT_SALES],
//                                :volCount[CT_DISC][TT_SALES],
//                                :volAmount[CT_DISC][TT_CREDITS],
//                                :volCount[CT_DISC][TT_CREDITS],
//                                :volAmount[CT_DINERS][TT_SALES],
//                                :volCount[CT_DINERS][TT_SALES],
//                                :volAmount[CT_DINERS][TT_CREDITS],
//                                :volCount[CT_DINERS][TT_CREDITS],
//                                :volAmount[CT_JCB][TT_SALES],
//                                :volCount[CT_JCB][TT_SALES],
//                                :volAmount[CT_JCB][TT_CREDITS],
//                                :volCount[CT_JCB][TT_CREDITS],
//                                :volAmount[CT_OTHER][TT_SALES],
//                                :volCount[CT_OTHER][TT_SALES],
//                                :volAmount[CT_OTHER][TT_CREDITS],
//                                :volCount[CT_OTHER][TT_CREDITS],
//                                :fxMarkupAmount[CT_VISA],
//                                :fxMarkupAmount[CT_MC],
//								  :trunc(sysdate)            	
//                              )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4674 = bankRejectSalesAmt - bankRejectCreditsAmt;
 int __sJT_4675 = bankRejectSalesCnt + bankRejectCreditsCnt;
 double __sJT_4676 = nonBankRejectSalesAmt - nonBankRejectCreditsAmt;
 int __sJT_4677 = nonBankRejectSalesCnt + nonBankRejectCreditsCnt;
 double __sJT_4678 = volAmount[CT_VISA][TT_SALES];
 int __sJT_4679 = volCount[CT_VISA][TT_SALES];
 double __sJT_4680 = volAmount[CT_VISA][TT_CREDITS];
 int __sJT_4681 = volCount[CT_VISA][TT_CREDITS];
 double __sJT_4682 = volAmount[CT_MC][TT_SALES];
 int __sJT_4683 = volCount[CT_MC][TT_SALES];
 double __sJT_4684 = volAmount[CT_MC][TT_CREDITS];
 int __sJT_4685 = volCount[CT_MC][TT_CREDITS];
 double __sJT_4686 = volAmount[CT_DEBIT][TT_SALES];
 int __sJT_4687 = volCount[CT_DEBIT][TT_SALES];
 double __sJT_4688 = volAmount[CT_DEBIT][TT_CREDITS];
 int __sJT_4689 = volCount[CT_DEBIT][TT_CREDITS];
 double __sJT_4690 = volAmount[CT_AMEX][TT_SALES];
 int __sJT_4691 = volCount[CT_AMEX][TT_SALES];
 double __sJT_4692 = volAmount[CT_AMEX][TT_CREDITS];
 int __sJT_4693 = volCount[CT_AMEX][TT_CREDITS];
 double __sJT_4694 = volAmount[CT_DISC][TT_SALES];
 int __sJT_4695 = volCount[CT_DISC][TT_SALES];
 double __sJT_4696 = volAmount[CT_DISC][TT_CREDITS];
 int __sJT_4697 = volCount[CT_DISC][TT_CREDITS];
 double __sJT_4698 = volAmount[CT_DINERS][TT_SALES];
 int __sJT_4699 = volCount[CT_DINERS][TT_SALES];
 double __sJT_4700 = volAmount[CT_DINERS][TT_CREDITS];
 int __sJT_4701 = volCount[CT_DINERS][TT_CREDITS];
 double __sJT_4702 = volAmount[CT_JCB][TT_SALES];
 int __sJT_4703 = volCount[CT_JCB][TT_SALES];
 double __sJT_4704 = volAmount[CT_JCB][TT_CREDITS];
 int __sJT_4705 = volCount[CT_JCB][TT_CREDITS];
 double __sJT_4706 = volAmount[CT_OTHER][TT_SALES];
 int __sJT_4707 = volCount[CT_OTHER][TT_SALES];
 double __sJT_4708 = volAmount[CT_OTHER][TT_CREDITS];
 int __sJT_4709 = volCount[CT_OTHER][TT_CREDITS];
 double __sJT_4710 = fxMarkupAmount[CT_VISA];
 double __sJT_4711 = fxMarkupAmount[CT_MC];
   String theSqlTS = "insert into daily_detail_file_summary\n                            (\n                              batch_date,\n                              merchant_number,\n                              bank_number,\n                              bank_count,\n                              bank_amount,\n                              nonbank_count,\n                              nonbank_amount,\n                              load_filename,\n                              load_file_id,\n                              batch_count,\n                              bank_credits_amount,\n                              bank_credits_count,\n                              bank_sales_amount,\n                              bank_sales_count,\n                              nonbank_credits_amount,\n                              nonbank_credits_count,\n                              nonbank_sales_amount,\n                              nonbank_sales_count,\n                              bank_reject_amount,\n                              bank_reject_count,\n                              bank_reject_credits_amount,\n                              bank_reject_credits_count,\n                              bank_reject_sales_amount,\n                              bank_reject_sales_count,\n                              nonbank_reject_amount,\n                              nonbank_reject_count,\n                              nonbank_reject_credits_amount,\n                              nonbank_reject_credits_count,\n                              nonbank_reject_sales_amount,\n                              nonbank_reject_sales_count,\n                              visa_sales_amount,\n                              visa_sales_count,\n                              visa_credits_amount,\n                              visa_credits_count,\n                              mc_sales_amount,\n                              mc_sales_count,\n                              mc_credits_amount,\n                              mc_credits_count,\n                              debit_sales_amount,\n                              debit_sales_count,\n                              debit_credits_amount,\n                              debit_credits_count,\n                              amex_sales_amount,\n                              amex_sales_count,\n                              amex_credits_amount,\n                              amex_credits_count,\n                              disc_sales_amount,\n                              disc_sales_count,\n                              disc_credits_amount,\n                              disc_credits_count,\n                              diners_sales_amount,\n                              diners_sales_count,\n                              diners_credits_amount,\n                              diners_credits_count,\n                              jcb_sales_amount,\n                              jcb_sales_count,\n                              jcb_credits_amount,\n                              jcb_credits_count,\n                              other_sales_amount,\n                              other_sales_count,\n                              other_credits_amount,\n                              other_credits_count,\n                              visa_fx_markup_amount,\n                              mc_fx_markup_amount\n ,\n LAST_MODIFIED_DATE \n                           )\n                values      (\n                               :1 ,\n                               :2 ,\n                               :3 ,\n                               :4 ,\n                               :5 ,\n                               :6 ,\n                               :7 ,\n                               :8 ,\n                               :9 ,\n                               :10 ,\n                               :11 ,\n                               :12 ,\n                               :13 ,\n                               :14 ,\n                               :15 ,\n                               :16 ,\n                               :17 ,\n                               :18 ,\n                               :19 ,\n                               :20 ,\n                               :21 ,\n                               :22 ,\n                               :23 ,\n                               :24 ,\n                               :25 ,\n                               :26 ,\n                               :27 ,\n                               :28 ,\n                               :29 ,\n                               :30 ,\n                               :31 ,\n                               :32 ,\n                               :33 ,\n                               :34 ,\n                               :35 ,\n                               :36 ,\n                               :37 ,\n                               :38 ,\n                               :39 ,\n                               :40 ,\n                               :41 ,\n                               :42 ,\n                               :43 ,\n                               :44 ,\n                               :45 ,\n                               :46 ,\n                               :47 ,\n                               :48 ,\n                               :49 ,\n                               :50 ,\n                               :51 ,\n                               :52 ,\n                               :53 ,\n                               :54 ,\n                               :55 ,\n                               :56 ,\n                               :57 ,\n                               :58 ,\n                               :59 ,\n                               :60 ,\n                               :61 ,\n                               :62 ,\n                               :63 ,\n                               :64 \n,trunc(sysdate)  \n  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"24com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,batchDate);
   __sJT_st.setLong(2,merchantNumber);
   __sJT_st.setInt(3,bankNumber);
   __sJT_st.setInt(4,bankCount);
   __sJT_st.setDouble(5,bankAmount);
   __sJT_st.setInt(6,nonbankCount);
   __sJT_st.setDouble(7,nonbankAmount);
   __sJT_st.setString(8,loadFilename);
   __sJT_st.setInt(9,LoadFileId);
   __sJT_st.setInt(10,batchCount);
   __sJT_st.setDouble(11,bankCreditsAmt);
   __sJT_st.setInt(12,bankCreditsCnt);
   __sJT_st.setDouble(13,bankSalesAmt);
   __sJT_st.setInt(14,bankSalesCnt);
   __sJT_st.setDouble(15,nonBankCreditsAmt);
   __sJT_st.setInt(16,nonBankCreditsCnt);
   __sJT_st.setDouble(17,nonBankSalesAmt);
   __sJT_st.setInt(18,nonBankSalesCnt);
   __sJT_st.setDouble(19,__sJT_4674);
   __sJT_st.setInt(20,__sJT_4675);
   __sJT_st.setDouble(21,bankRejectCreditsAmt);
   __sJT_st.setInt(22,bankRejectCreditsCnt);
   __sJT_st.setDouble(23,bankRejectSalesAmt);
   __sJT_st.setInt(24,bankRejectSalesCnt);
   __sJT_st.setDouble(25,__sJT_4676);
   __sJT_st.setInt(26,__sJT_4677);
   __sJT_st.setDouble(27,nonBankRejectCreditsAmt);
   __sJT_st.setInt(28,nonBankRejectCreditsCnt);
   __sJT_st.setDouble(29,nonBankRejectSalesAmt);
   __sJT_st.setInt(30,nonBankRejectSalesCnt);
   __sJT_st.setDouble(31,__sJT_4678);
   __sJT_st.setInt(32,__sJT_4679);
   __sJT_st.setDouble(33,__sJT_4680);
   __sJT_st.setInt(34,__sJT_4681);
   __sJT_st.setDouble(35,__sJT_4682);
   __sJT_st.setInt(36,__sJT_4683);
   __sJT_st.setDouble(37,__sJT_4684);
   __sJT_st.setInt(38,__sJT_4685);
   __sJT_st.setDouble(39,__sJT_4686);
   __sJT_st.setInt(40,__sJT_4687);
   __sJT_st.setDouble(41,__sJT_4688);
   __sJT_st.setInt(42,__sJT_4689);
   __sJT_st.setDouble(43,__sJT_4690);
   __sJT_st.setInt(44,__sJT_4691);
   __sJT_st.setDouble(45,__sJT_4692);
   __sJT_st.setInt(46,__sJT_4693);
   __sJT_st.setDouble(47,__sJT_4694);
   __sJT_st.setInt(48,__sJT_4695);
   __sJT_st.setDouble(49,__sJT_4696);
   __sJT_st.setInt(50,__sJT_4697);
   __sJT_st.setDouble(51,__sJT_4698);
   __sJT_st.setInt(52,__sJT_4699);
   __sJT_st.setDouble(53,__sJT_4700);
   __sJT_st.setInt(54,__sJT_4701);
   __sJT_st.setDouble(55,__sJT_4702);
   __sJT_st.setInt(56,__sJT_4703);
   __sJT_st.setDouble(57,__sJT_4704);
   __sJT_st.setInt(58,__sJT_4705);
   __sJT_st.setDouble(59,__sJT_4706);
   __sJT_st.setInt(60,__sJT_4707);
   __sJT_st.setDouble(61,__sJT_4708);
   __sJT_st.setInt(62,__sJT_4709);
   __sJT_st.setDouble(63,__sJT_4710);
   __sJT_st.setDouble(64,__sJT_4711);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:890^15*/
            }
            else
            {
              // add record to external summary table
              /*@lineinfo:generated-code*//*@lineinfo:895^15*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_file_ext_summary
//                              (
//                                batch_date,
//                                merchant_number,
//                              --bank_number,
//                                bank_count,
//                                bank_amount,
//                                nonbank_count,
//                                nonbank_amount,
//                                load_filename,
//                                load_file_id,
//                                batch_count,
//                                bank_credits_amount,
//                                bank_credits_count,
//                                bank_sales_amount,
//                                bank_sales_count,
//                                nonbank_credits_amount,
//                                nonbank_credits_count,
//                                nonbank_sales_amount,
//                                nonbank_sales_count,
//                                visa_sales_amount,
//                                visa_sales_count,
//                                visa_credits_amount,
//                                visa_credits_count,
//                                mc_sales_amount,
//                                mc_sales_count,
//                                mc_credits_amount,
//                                mc_credits_count,
//                                debit_sales_amount,
//                                debit_sales_count,
//                                debit_credits_amount,
//                                debit_credits_count,
//                                amex_sales_amount,
//                                amex_sales_count,
//                                amex_credits_amount,
//                                amex_credits_count,
//                                disc_sales_amount,
//                                disc_sales_count,
//                                disc_credits_amount,
//                                disc_credits_count,
//                                diners_sales_amount,
//                                diners_sales_count,
//                                diners_credits_amount,
//                                diners_credits_count,
//                                jcb_sales_amount,
//                                jcb_sales_count,
//                                jcb_credits_amount,
//                                jcb_credits_count,
//                                other_sales_amount,
//                                other_sales_count,
//                                other_credits_amount,
//                                other_credits_count
//                              )
//                  values      (
//                                :batchDate,
//                                :merchantNumber,
//                              --:bankNumber,
//                                :bankCount,
//                                :bankAmount,
//                                :nonbankCount,
//                                :nonbankAmount,
//                                :loadFilename,
//                                :LoadFileId,
//                                :batchCount,
//                                :bankCreditsAmt,
//                                :bankCreditsCnt,
//                                :bankSalesAmt,
//                                :bankSalesCnt,
//                                :nonBankCreditsAmt,
//                                :nonBankCreditsCnt,
//                                :nonBankSalesAmt,
//                                :nonBankSalesCnt,
//                                :volAmount[CT_VISA][TT_SALES],
//                                :volCount[CT_VISA][TT_SALES],
//                                :volAmount[CT_VISA][TT_CREDITS],
//                                :volCount[CT_VISA][TT_CREDITS],
//                                :volAmount[CT_MC][TT_SALES],
//                                :volCount[CT_MC][TT_SALES],
//                                :volAmount[CT_MC][TT_CREDITS],
//                                :volCount[CT_MC][TT_CREDITS],
//                                :volAmount[CT_DEBIT][TT_SALES],
//                                :volCount[CT_DEBIT][TT_SALES],
//                                :volAmount[CT_DEBIT][TT_CREDITS],
//                                :volCount[CT_DEBIT][TT_CREDITS],
//                                :volAmount[CT_AMEX][TT_SALES],
//                                :volCount[CT_AMEX][TT_SALES],
//                                :volAmount[CT_AMEX][TT_CREDITS],
//                                :volCount[CT_AMEX][TT_CREDITS],
//                                :volAmount[CT_DISC][TT_SALES],
//                                :volCount[CT_DISC][TT_SALES],
//                                :volAmount[CT_DISC][TT_CREDITS],
//                                :volCount[CT_DISC][TT_CREDITS],
//                                :volAmount[CT_DINERS][TT_SALES],
//                                :volCount[CT_DINERS][TT_SALES],
//                                :volAmount[CT_DINERS][TT_CREDITS],
//                                :volCount[CT_DINERS][TT_CREDITS],
//                                :volAmount[CT_JCB][TT_SALES],
//                                :volCount[CT_JCB][TT_SALES],
//                                :volAmount[CT_JCB][TT_CREDITS],
//                                :volCount[CT_JCB][TT_CREDITS],
//                                :volAmount[CT_OTHER][TT_SALES],
//                                :volCount[CT_OTHER][TT_SALES],
//                                :volAmount[CT_OTHER][TT_CREDITS],
//                                :volCount[CT_OTHER][TT_CREDITS]
//                              )
//                 };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4712 = volAmount[CT_VISA][TT_SALES];
 int __sJT_4713 = volCount[CT_VISA][TT_SALES];
 double __sJT_4714 = volAmount[CT_VISA][TT_CREDITS];
 int __sJT_4715 = volCount[CT_VISA][TT_CREDITS];
 double __sJT_4716 = volAmount[CT_MC][TT_SALES];
 int __sJT_4717 = volCount[CT_MC][TT_SALES];
 double __sJT_4718 = volAmount[CT_MC][TT_CREDITS];
 int __sJT_4719 = volCount[CT_MC][TT_CREDITS];
 double __sJT_4720 = volAmount[CT_DEBIT][TT_SALES];
 int __sJT_4721 = volCount[CT_DEBIT][TT_SALES];
 double __sJT_4722 = volAmount[CT_DEBIT][TT_CREDITS];
 int __sJT_4723 = volCount[CT_DEBIT][TT_CREDITS];
 double __sJT_4724 = volAmount[CT_AMEX][TT_SALES];
 int __sJT_4725 = volCount[CT_AMEX][TT_SALES];
 double __sJT_4726 = volAmount[CT_AMEX][TT_CREDITS];
 int __sJT_4727 = volCount[CT_AMEX][TT_CREDITS];
 double __sJT_4728 = volAmount[CT_DISC][TT_SALES];
 int __sJT_4729 = volCount[CT_DISC][TT_SALES];
 double __sJT_4730 = volAmount[CT_DISC][TT_CREDITS];
 int __sJT_4731 = volCount[CT_DISC][TT_CREDITS];
 double __sJT_4732 = volAmount[CT_DINERS][TT_SALES];
 int __sJT_4733 = volCount[CT_DINERS][TT_SALES];
 double __sJT_4734 = volAmount[CT_DINERS][TT_CREDITS];
 int __sJT_4735 = volCount[CT_DINERS][TT_CREDITS];
 double __sJT_4736 = volAmount[CT_JCB][TT_SALES];
 int __sJT_4737 = volCount[CT_JCB][TT_SALES];
 double __sJT_4738 = volAmount[CT_JCB][TT_CREDITS];
 int __sJT_4739 = volCount[CT_JCB][TT_CREDITS];
 double __sJT_4740 = volAmount[CT_OTHER][TT_SALES];
 int __sJT_4741 = volCount[CT_OTHER][TT_SALES];
 double __sJT_4742 = volAmount[CT_OTHER][TT_CREDITS];
 int __sJT_4743 = volCount[CT_OTHER][TT_CREDITS];
   String theSqlTS = "insert into daily_detail_file_ext_summary\n                            (\n                              batch_date,\n                              merchant_number,\n                            --bank_number,\n                              bank_count,\n                              bank_amount,\n                              nonbank_count,\n                              nonbank_amount,\n                              load_filename,\n                              load_file_id,\n                              batch_count,\n                              bank_credits_amount,\n                              bank_credits_count,\n                              bank_sales_amount,\n                              bank_sales_count,\n                              nonbank_credits_amount,\n                              nonbank_credits_count,\n                              nonbank_sales_amount,\n                              nonbank_sales_count,\n                              visa_sales_amount,\n                              visa_sales_count,\n                              visa_credits_amount,\n                              visa_credits_count,\n                              mc_sales_amount,\n                              mc_sales_count,\n                              mc_credits_amount,\n                              mc_credits_count,\n                              debit_sales_amount,\n                              debit_sales_count,\n                              debit_credits_amount,\n                              debit_credits_count,\n                              amex_sales_amount,\n                              amex_sales_count,\n                              amex_credits_amount,\n                              amex_credits_count,\n                              disc_sales_amount,\n                              disc_sales_count,\n                              disc_credits_amount,\n                              disc_credits_count,\n                              diners_sales_amount,\n                              diners_sales_count,\n                              diners_credits_amount,\n                              diners_credits_count,\n                              jcb_sales_amount,\n                              jcb_sales_count,\n                              jcb_credits_amount,\n                              jcb_credits_count,\n                              other_sales_amount,\n                              other_sales_count,\n                              other_credits_amount,\n                              other_credits_count\n                            )\n                values      (\n                               :1 ,\n                               :2 ,\n                            --:bankNumber,\n                               :3 ,\n                               :4 ,\n                               :5 ,\n                               :6 ,\n                               :7 ,\n                               :8 ,\n                               :9 ,\n                               :10 ,\n                               :11 ,\n                               :12 ,\n                               :13 ,\n                               :14 ,\n                               :15 ,\n                               :16 ,\n                               :17 ,\n                               :18 ,\n                               :19 ,\n                               :20 ,\n                               :21 ,\n                               :22 ,\n                               :23 ,\n                               :24 ,\n                               :25 ,\n                               :26 ,\n                               :27 ,\n                               :28 ,\n                               :29 ,\n                               :30 ,\n                               :31 ,\n                               :32 ,\n                               :33 ,\n                               :34 ,\n                               :35 ,\n                               :36 ,\n                               :37 ,\n                               :38 ,\n                               :39 ,\n                               :40 ,\n                               :41 ,\n                               :42 ,\n                               :43 ,\n                               :44 ,\n                               :45 ,\n                               :46 ,\n                               :47 ,\n                               :48 ,\n                               :49 \n                            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"25com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,batchDate);
   __sJT_st.setLong(2,merchantNumber);
   __sJT_st.setInt(3,bankCount);
   __sJT_st.setDouble(4,bankAmount);
   __sJT_st.setInt(5,nonbankCount);
   __sJT_st.setDouble(6,nonbankAmount);
   __sJT_st.setString(7,loadFilename);
   __sJT_st.setInt(8,LoadFileId);
   __sJT_st.setInt(9,batchCount);
   __sJT_st.setDouble(10,bankCreditsAmt);
   __sJT_st.setInt(11,bankCreditsCnt);
   __sJT_st.setDouble(12,bankSalesAmt);
   __sJT_st.setInt(13,bankSalesCnt);
   __sJT_st.setDouble(14,nonBankCreditsAmt);
   __sJT_st.setInt(15,nonBankCreditsCnt);
   __sJT_st.setDouble(16,nonBankSalesAmt);
   __sJT_st.setInt(17,nonBankSalesCnt);
   __sJT_st.setDouble(18,__sJT_4712);
   __sJT_st.setInt(19,__sJT_4713);
   __sJT_st.setDouble(20,__sJT_4714);
   __sJT_st.setInt(21,__sJT_4715);
   __sJT_st.setDouble(22,__sJT_4716);
   __sJT_st.setInt(23,__sJT_4717);
   __sJT_st.setDouble(24,__sJT_4718);
   __sJT_st.setInt(25,__sJT_4719);
   __sJT_st.setDouble(26,__sJT_4720);
   __sJT_st.setInt(27,__sJT_4721);
   __sJT_st.setDouble(28,__sJT_4722);
   __sJT_st.setInt(29,__sJT_4723);
   __sJT_st.setDouble(30,__sJT_4724);
   __sJT_st.setInt(31,__sJT_4725);
   __sJT_st.setDouble(32,__sJT_4726);
   __sJT_st.setInt(33,__sJT_4727);
   __sJT_st.setDouble(34,__sJT_4728);
   __sJT_st.setInt(35,__sJT_4729);
   __sJT_st.setDouble(36,__sJT_4730);
   __sJT_st.setInt(37,__sJT_4731);
   __sJT_st.setDouble(38,__sJT_4732);
   __sJT_st.setInt(39,__sJT_4733);
   __sJT_st.setDouble(40,__sJT_4734);
   __sJT_st.setInt(41,__sJT_4735);
   __sJT_st.setDouble(42,__sJT_4736);
   __sJT_st.setInt(43,__sJT_4737);
   __sJT_st.setDouble(44,__sJT_4738);
   __sJT_st.setInt(45,__sJT_4739);
   __sJT_st.setDouble(46,__sJT_4740);
   __sJT_st.setInt(47,__sJT_4741);
   __sJT_st.setDouble(48,__sJT_4742);
   __sJT_st.setInt(49,__sJT_4743);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1002^15*/
            }

            commit();

            // reset class variables
            init();

            // reset stack variables
            bankCreditsAmt          = 0.0;
            bankCreditsCnt          = 0;
            bankSalesAmt            = 0.0;
            bankSalesCnt            = 0;
            nonBankCreditsAmt       = 0.0;
            nonBankCreditsCnt       = 0;
            nonBankSalesAmt         = 0.0;
            nonBankSalesCnt         = 0;

            // reset the reject variables
            bankRejectCreditsAmt    = 0.0;
            bankRejectCreditsCnt    = 0;
            bankRejectSalesAmt      = 0.0;
            bankRejectSalesCnt      = 0;
            nonBankRejectCreditsAmt = 0.0;
            nonBankRejectCreditsCnt = 0;
            nonBankRejectSalesAmt   = 0.0;
            nonBankRejectSalesCnt   = 0;

            // reset card type data
            for( int i = 0; i < CT_COUNT; ++i )
            {
              volAmount[i][TT_SALES]    = 0.0;
              volAmount[i][TT_CREDITS]  = 0.0;
              volCount[i][TT_SALES]     = 0;
              volCount[i][TT_CREDITS]   = 0;
            }
            
            // reset fx markup amounts
            for( int i = 0; i < CT_COUNT; ++i )
            {
              fxMarkupAmount[i] = 0.0;
            }
          }
          catch(Exception ie)
          {
            logEntry(1, "TranSummaryTrigger:: createSummaryRecords", "(" + merchantNumber + ", " + batchDate + ") " + ie.toString());
          }
        }

        merchantNumber  = sr.merchantNumber;
        bankNumber      = sr.bankNumber;
        batchDate       = sr.batchDate;
        loadFilename    = sr.loadFilename;

        if(batchNumber != sr.batchNumber)
        {
          ++batchCount;
          batchNumber = sr.batchNumber;
        }

        cardType = sr.cardType;

        // Visa, Mastercard, and DB are always bank cards
//        if(cardType.equals("VS") || cardType.equals("MC") ||
//           cardType.equals("VD") || cardType.equals("MD") ||
//           cardType.equals("VB") || cardType.equals("MB") ||
//           cardType.equals("DB") || cardType.equals("EB"))
        cardIndex = getCardTypeIndex(cardType);
        boolean isNonBank;
        switch( cardIndex )
        {
          case CT_VISA:
          case CT_MC:
          case CT_DEBIT:
          case CT_BML:
            isNonBank = false;
            break;

          case CT_DINERS:   // Diners is sometimes settled by the bank, sometimes not
            switch(bankNumber)
            {
              default:
                isNonBank = true;
                break;
            }
            break;

          case CT_JCB:      // JCB is sometimes settled by the bank, sometimes not
            switch(bankNumber)
            {
              default:
                isNonBank = true;
                break;
              case 3870:      // UBOC settles JCB
                isNonBank = false;
                break;
            }
            break;

          default:
            isNonBank = true;
            break;
        }

        if( isNonBank == false )
        {
          bankCount += sr.tranCount;
          if( sr.debitCreditInd.equals("D"))
          {
            // sales
            bankAmount    += sr.tranAmount;
            bankSalesAmt  += sr.tranAmount;
            bankSalesCnt  += sr.tranCount;

            if ( sr.reject.equals("Y") )
            {
              bankRejectSalesAmt  += sr.tranAmount;
              bankRejectSalesCnt  += sr.tranCount;
            }
          }
          else
          {
            // returns
            bankAmount      -= sr.tranAmount;
            bankCreditsAmt  += sr.tranAmount; // always positive
            bankCreditsCnt  += sr.tranCount;

            if ( sr.reject.equals("Y") )
            {
              bankRejectCreditsAmt  += sr.tranAmount; // always positive
              bankRejectCreditsCnt  += sr.tranCount;
            }
          }
        }
        else
        {
          nonbankCount += sr.tranCount;
          if(sr.debitCreditInd.equals("D"))
          {
            // sales
            nonbankAmount     += sr.tranAmount;
            nonBankSalesAmt   += sr.tranAmount;
            nonBankSalesCnt   += sr.tranCount;

            if ( sr.reject.equals("Y") )
            {
              nonBankRejectSalesAmt   += sr.tranAmount;
              nonBankRejectSalesCnt   += sr.tranCount;
            }
          }
          else
          {
            // returns
            nonbankAmount     -= sr.tranAmount;
            nonBankCreditsAmt += sr.tranAmount;  // always positive
            nonBankCreditsCnt += sr.tranCount;

            if ( sr.reject.equals("Y") )
            {
              nonBankRejectCreditsAmt += sr.tranAmount;  // always positive
              nonBankRejectCreditsCnt += sr.tranCount;
            }
          }
        }

        // store the data by card type by transaction type
        cardIndex = getCardTypeIndex( cardType );
        
        if( sr.debitCreditInd.equals("D"))
        {
          tranIndex = TT_SALES;
        }
        else    // credit
        {
          tranIndex = TT_CREDITS;
        }
        volAmount[cardIndex][tranIndex] += sr.tranAmount;
        volCount[cardIndex][tranIndex]  += sr.tranCount;
        fxMarkupAmount[cardIndex]       += sr.fxMarkupAmount;
      }

      // border condition -- last merchant in file
      if(!(bankCount == 0 && nonbankCount == 0))
      {
        try
        {
          if ( isDailyDetail == true )
          {
            /*@lineinfo:generated-code*//*@lineinfo:1190^13*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_file_summary
//                            (
//                              batch_date,
//                              merchant_number,
//                              bank_number,
//                              bank_count,
//                              bank_amount,
//                              nonbank_count,
//                              nonbank_amount,
//                              load_filename,
//                              load_file_id,
//                              batch_count,
//                              bank_credits_amount,
//                              bank_credits_count,
//                              bank_sales_amount,
//                              bank_sales_count,
//                              nonbank_credits_amount,
//                              nonbank_credits_count,
//                              nonbank_sales_amount,
//                              nonbank_sales_count,
//                              bank_reject_amount,
//                              bank_reject_count,
//                              bank_reject_credits_amount,
//                              bank_reject_credits_count,
//                              bank_reject_sales_amount,
//                              bank_reject_sales_count,
//                              nonbank_reject_amount,
//                              nonbank_reject_count,
//                              nonbank_reject_credits_amount,
//                              nonbank_reject_credits_count,
//                              nonbank_reject_sales_amount,
//                              nonbank_reject_sales_count,
//                              visa_sales_amount,
//                              visa_sales_count,
//                              visa_credits_amount,
//                              visa_credits_count,
//                              mc_sales_amount,
//                              mc_sales_count,
//                              mc_credits_amount,
//                              mc_credits_count,
//                              debit_sales_amount,
//                              debit_sales_count,
//                              debit_credits_amount,
//                              debit_credits_count,
//                              amex_sales_amount,
//                              amex_sales_count,
//                              amex_credits_amount,
//                              amex_credits_count,
//                              disc_sales_amount,
//                              disc_sales_count,
//                              disc_credits_amount,
//                              disc_credits_count,
//                              diners_sales_amount,
//                              diners_sales_count,
//                              diners_credits_amount,
//                              diners_credits_count,
//                              jcb_sales_amount,
//                              jcb_sales_count,
//                              jcb_credits_amount,
//                              jcb_credits_count,
//                              other_sales_amount,
//                              other_sales_count,
//                              other_credits_amount,
//                              other_credits_count,
//                              visa_fx_markup_amount,
//                              mc_fx_markup_amount,
//								LAST_MODIFIED_DATE        	  
//                            )
//                values      (
//                              :batchDate,
//                              :merchantNumber,
//                              :bankNumber,
//                              :bankCount,
//                              :bankAmount,
//                              :nonbankCount,
//                              :nonbankAmount,
//                              :loadFilename,
//                              :LoadFileId,
//                              :batchCount,
//                              :bankCreditsAmt,
//                              :bankCreditsCnt,
//                              :bankSalesAmt,
//                              :bankSalesCnt,
//                              :nonBankCreditsAmt,
//                              :nonBankCreditsCnt,
//                              :nonBankSalesAmt,
//                              :nonBankSalesCnt,
//                              :bankRejectSalesAmt - bankRejectCreditsAmt,
//                              :bankRejectSalesCnt + bankRejectCreditsCnt,
//                              :bankRejectCreditsAmt,
//                              :bankRejectCreditsCnt,
//                              :bankRejectSalesAmt,
//                              :bankRejectSalesCnt,
//                              :nonBankRejectSalesAmt - nonBankRejectCreditsAmt,
//                              :nonBankRejectSalesCnt + nonBankRejectCreditsCnt,
//                              :nonBankRejectCreditsAmt,
//                              :nonBankRejectCreditsCnt,
//                              :nonBankRejectSalesAmt,
//                              :nonBankRejectSalesCnt,
//                              :volAmount[CT_VISA][TT_SALES],
//                              :volCount[CT_VISA][TT_SALES],
//                              :volAmount[CT_VISA][TT_CREDITS],
//                              :volCount[CT_VISA][TT_CREDITS],
//                              :volAmount[CT_MC][TT_SALES],
//                              :volCount[CT_MC][TT_SALES],
//                              :volAmount[CT_MC][TT_CREDITS],
//                              :volCount[CT_MC][TT_CREDITS],
//                              :volAmount[CT_DEBIT][TT_SALES],
//                              :volCount[CT_DEBIT][TT_SALES],
//                              :volAmount[CT_DEBIT][TT_CREDITS],
//                              :volCount[CT_DEBIT][TT_CREDITS],
//                              :volAmount[CT_AMEX][TT_SALES],
//                              :volCount[CT_AMEX][TT_SALES],
//                              :volAmount[CT_AMEX][TT_CREDITS],
//                              :volCount[CT_AMEX][TT_CREDITS],
//                              :volAmount[CT_DISC][TT_SALES],
//                              :volCount[CT_DISC][TT_SALES],
//                              :volAmount[CT_DISC][TT_CREDITS],
//                              :volCount[CT_DISC][TT_CREDITS],
//                              :volAmount[CT_DINERS][TT_SALES],
//                              :volCount[CT_DINERS][TT_SALES],
//                              :volAmount[CT_DINERS][TT_CREDITS],
//                              :volCount[CT_DINERS][TT_CREDITS],
//                              :volAmount[CT_JCB][TT_SALES],
//                              :volCount[CT_JCB][TT_SALES],
//                              :volAmount[CT_JCB][TT_CREDITS],
//                              :volCount[CT_JCB][TT_CREDITS],
//                              :volAmount[CT_OTHER][TT_SALES],
//                              :volCount[CT_OTHER][TT_SALES],
//                              :volAmount[CT_OTHER][TT_CREDITS],
//                              :volCount[CT_OTHER][TT_CREDITS],
//                              :fxMarkupAmount[CT_VISA],
//                              :fxMarkupAmount[CT_MC],
//								:trunc(sysdate)
//                            )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4744 = bankRejectSalesAmt - bankRejectCreditsAmt;
 int __sJT_4745 = bankRejectSalesCnt + bankRejectCreditsCnt;
 double __sJT_4746 = nonBankRejectSalesAmt - nonBankRejectCreditsAmt;
 int __sJT_4747 = nonBankRejectSalesCnt + nonBankRejectCreditsCnt;
 double __sJT_4748 = volAmount[CT_VISA][TT_SALES];
 int __sJT_4749 = volCount[CT_VISA][TT_SALES];
 double __sJT_4750 = volAmount[CT_VISA][TT_CREDITS];
 int __sJT_4751 = volCount[CT_VISA][TT_CREDITS];
 double __sJT_4752 = volAmount[CT_MC][TT_SALES];
 int __sJT_4753 = volCount[CT_MC][TT_SALES];
 double __sJT_4754 = volAmount[CT_MC][TT_CREDITS];
 int __sJT_4755 = volCount[CT_MC][TT_CREDITS];
 double __sJT_4756 = volAmount[CT_DEBIT][TT_SALES];
 int __sJT_4757 = volCount[CT_DEBIT][TT_SALES];
 double __sJT_4758 = volAmount[CT_DEBIT][TT_CREDITS];
 int __sJT_4759 = volCount[CT_DEBIT][TT_CREDITS];
 double __sJT_4760 = volAmount[CT_AMEX][TT_SALES];
 int __sJT_4761 = volCount[CT_AMEX][TT_SALES];
 double __sJT_4762 = volAmount[CT_AMEX][TT_CREDITS];
 int __sJT_4763 = volCount[CT_AMEX][TT_CREDITS];
 double __sJT_4764 = volAmount[CT_DISC][TT_SALES];
 int __sJT_4765 = volCount[CT_DISC][TT_SALES];
 double __sJT_4766 = volAmount[CT_DISC][TT_CREDITS];
 int __sJT_4767 = volCount[CT_DISC][TT_CREDITS];
 double __sJT_4768 = volAmount[CT_DINERS][TT_SALES];
 int __sJT_4769 = volCount[CT_DINERS][TT_SALES];
 double __sJT_4770 = volAmount[CT_DINERS][TT_CREDITS];
 int __sJT_4771 = volCount[CT_DINERS][TT_CREDITS];
 double __sJT_4772 = volAmount[CT_JCB][TT_SALES];
 int __sJT_4773 = volCount[CT_JCB][TT_SALES];
 double __sJT_4774 = volAmount[CT_JCB][TT_CREDITS];
 int __sJT_4775 = volCount[CT_JCB][TT_CREDITS];
 double __sJT_4776 = volAmount[CT_OTHER][TT_SALES];
 int __sJT_4777 = volCount[CT_OTHER][TT_SALES];
 double __sJT_4778 = volAmount[CT_OTHER][TT_CREDITS];
 int __sJT_4779 = volCount[CT_OTHER][TT_CREDITS];
 double __sJT_4780 = fxMarkupAmount[CT_VISA];
 double __sJT_4781 = fxMarkupAmount[CT_MC];
   String theSqlTS = "insert into daily_detail_file_summary\n                          (\n                            batch_date,\n                            merchant_number,\n                            bank_number,\n                            bank_count,\n                            bank_amount,\n                            nonbank_count,\n                            nonbank_amount,\n                            load_filename,\n                            load_file_id,\n                            batch_count,\n                            bank_credits_amount,\n                            bank_credits_count,\n                            bank_sales_amount,\n                            bank_sales_count,\n                            nonbank_credits_amount,\n                            nonbank_credits_count,\n                            nonbank_sales_amount,\n                            nonbank_sales_count,\n                            bank_reject_amount,\n                            bank_reject_count,\n                            bank_reject_credits_amount,\n                            bank_reject_credits_count,\n                            bank_reject_sales_amount,\n                            bank_reject_sales_count,\n                            nonbank_reject_amount,\n                            nonbank_reject_count,\n                            nonbank_reject_credits_amount,\n                            nonbank_reject_credits_count,\n                            nonbank_reject_sales_amount,\n                            nonbank_reject_sales_count,\n                            visa_sales_amount,\n                            visa_sales_count,\n                            visa_credits_amount,\n                            visa_credits_count,\n                            mc_sales_amount,\n                            mc_sales_count,\n                            mc_credits_amount,\n                            mc_credits_count,\n                            debit_sales_amount,\n                            debit_sales_count,\n                            debit_credits_amount,\n                            debit_credits_count,\n                            amex_sales_amount,\n                            amex_sales_count,\n                            amex_credits_amount,\n                            amex_credits_count,\n                            disc_sales_amount,\n                            disc_sales_count,\n                            disc_credits_amount,\n                            disc_credits_count,\n                            diners_sales_amount,\n                            diners_sales_count,\n                            diners_credits_amount,\n                            diners_credits_count,\n                            jcb_sales_amount,\n                            jcb_sales_count,\n                            jcb_credits_amount,\n                            jcb_credits_count,\n                            other_sales_amount,\n                            other_sales_count,\n                            other_credits_amount,\n                            other_credits_count,\n                            visa_fx_markup_amount,\n                            mc_fx_markup_amount\n ,\n LAST_MODIFIED_DATE \n                         )\n              values      (\n                             :1 ,\n                             :2 ,\n                             :3 ,\n                             :4 ,\n                             :5 ,\n                             :6 ,\n                             :7 ,\n                             :8 ,\n                             :9 ,\n                             :10 ,\n                             :11 ,\n                             :12 ,\n                             :13 ,\n                             :14 ,\n                             :15 ,\n                             :16 ,\n                             :17 ,\n                             :18 ,\n                             :19 ,\n                             :20 ,\n                             :21 ,\n                             :22 ,\n                             :23 ,\n                             :24 ,\n                             :25 ,\n                             :26 ,\n                             :27 ,\n                             :28 ,\n                             :29 ,\n                             :30 ,\n                             :31 ,\n                             :32 ,\n                             :33 ,\n                             :34 ,\n                             :35 ,\n                             :36 ,\n                             :37 ,\n                             :38 ,\n                             :39 ,\n                             :40 ,\n                             :41 ,\n                             :42 ,\n                             :43 ,\n                             :44 ,\n                             :45 ,\n                             :46 ,\n                             :47 ,\n                             :48 ,\n                             :49 ,\n                             :50 ,\n                             :51 ,\n                             :52 ,\n                             :53 ,\n                             :54 ,\n                             :55 ,\n                             :56 ,\n                             :57 ,\n                             :58 ,\n                             :59 ,\n                             :60 ,\n                             :61 ,\n                             :62 ,\n                             :63 ,\n                             :64 \n,trunc(sysdate)  \n   )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"26com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,batchDate);
   __sJT_st.setLong(2,merchantNumber);
   __sJT_st.setInt(3,bankNumber);
   __sJT_st.setInt(4,bankCount);
   __sJT_st.setDouble(5,bankAmount);
   __sJT_st.setInt(6,nonbankCount);
   __sJT_st.setDouble(7,nonbankAmount);
   __sJT_st.setString(8,loadFilename);
   __sJT_st.setInt(9,LoadFileId);
   __sJT_st.setInt(10,batchCount);
   __sJT_st.setDouble(11,bankCreditsAmt);
   __sJT_st.setInt(12,bankCreditsCnt);
   __sJT_st.setDouble(13,bankSalesAmt);
   __sJT_st.setInt(14,bankSalesCnt);
   __sJT_st.setDouble(15,nonBankCreditsAmt);
   __sJT_st.setInt(16,nonBankCreditsCnt);
   __sJT_st.setDouble(17,nonBankSalesAmt);
   __sJT_st.setInt(18,nonBankSalesCnt);
   __sJT_st.setDouble(19,__sJT_4744);
   __sJT_st.setInt(20,__sJT_4745);
   __sJT_st.setDouble(21,bankRejectCreditsAmt);
   __sJT_st.setInt(22,bankRejectCreditsCnt);
   __sJT_st.setDouble(23,bankRejectSalesAmt);
   __sJT_st.setInt(24,bankRejectSalesCnt);
   __sJT_st.setDouble(25,__sJT_4746);
   __sJT_st.setInt(26,__sJT_4747);
   __sJT_st.setDouble(27,nonBankRejectCreditsAmt);
   __sJT_st.setInt(28,nonBankRejectCreditsCnt);
   __sJT_st.setDouble(29,nonBankRejectSalesAmt);
   __sJT_st.setInt(30,nonBankRejectSalesCnt);
   __sJT_st.setDouble(31,__sJT_4748);
   __sJT_st.setInt(32,__sJT_4749);
   __sJT_st.setDouble(33,__sJT_4750);
   __sJT_st.setInt(34,__sJT_4751);
   __sJT_st.setDouble(35,__sJT_4752);
   __sJT_st.setInt(36,__sJT_4753);
   __sJT_st.setDouble(37,__sJT_4754);
   __sJT_st.setInt(38,__sJT_4755);
   __sJT_st.setDouble(39,__sJT_4756);
   __sJT_st.setInt(40,__sJT_4757);
   __sJT_st.setDouble(41,__sJT_4758);
   __sJT_st.setInt(42,__sJT_4759);
   __sJT_st.setDouble(43,__sJT_4760);
   __sJT_st.setInt(44,__sJT_4761);
   __sJT_st.setDouble(45,__sJT_4762);
   __sJT_st.setInt(46,__sJT_4763);
   __sJT_st.setDouble(47,__sJT_4764);
   __sJT_st.setInt(48,__sJT_4765);
   __sJT_st.setDouble(49,__sJT_4766);
   __sJT_st.setInt(50,__sJT_4767);
   __sJT_st.setDouble(51,__sJT_4768);
   __sJT_st.setInt(52,__sJT_4769);
   __sJT_st.setDouble(53,__sJT_4770);
   __sJT_st.setInt(54,__sJT_4771);
   __sJT_st.setDouble(55,__sJT_4772);
   __sJT_st.setInt(56,__sJT_4773);
   __sJT_st.setDouble(57,__sJT_4774);
   __sJT_st.setInt(58,__sJT_4775);
   __sJT_st.setDouble(59,__sJT_4776);
   __sJT_st.setInt(60,__sJT_4777);
   __sJT_st.setDouble(61,__sJT_4778);
   __sJT_st.setInt(62,__sJT_4779);
   __sJT_st.setDouble(63,__sJT_4780);
   __sJT_st.setDouble(64,__sJT_4781);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1325^13*/
          }
          else      // external data summary
          {
            /*@lineinfo:generated-code*//*@lineinfo:1329^13*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_file_ext_summary
//                            (
//                              batch_date,
//                              merchant_number,
//                            --bank_number,
//                              bank_count,
//                              bank_amount,
//                              nonbank_count,
//                              nonbank_amount,
//                              load_filename,
//                              load_file_id,
//                              batch_count,
//                              bank_credits_amount,
//                              bank_credits_count,
//                              bank_sales_amount,
//                              bank_sales_count,
//                              nonbank_credits_amount,
//                              nonbank_credits_count,
//                              nonbank_sales_amount,
//                              nonbank_sales_count,
//                              visa_sales_amount,
//                              visa_sales_count,
//                              visa_credits_amount,
//                              visa_credits_count,
//                              mc_sales_amount,
//                              mc_sales_count,
//                              mc_credits_amount,
//                              mc_credits_count,
//                              debit_sales_amount,
//                              debit_sales_count,
//                              debit_credits_amount,
//                              debit_credits_count,
//                              amex_sales_amount,
//                              amex_sales_count,
//                              amex_credits_amount,
//                              amex_credits_count,
//                              disc_sales_amount,
//                              disc_sales_count,
//                              disc_credits_amount,
//                              disc_credits_count,
//                              diners_sales_amount,
//                              diners_sales_count,
//                              diners_credits_amount,
//                              diners_credits_count,
//                              jcb_sales_amount,
//                              jcb_sales_count,
//                              jcb_credits_amount,
//                              jcb_credits_count,
//                              other_sales_amount,
//                              other_sales_count,
//                              other_credits_amount,
//                              other_credits_count
//                            )
//                values      (
//                              :batchDate,
//                              :merchantNumber,
//                            --:bankNumber,
//                              :bankCount,
//                              :bankAmount,
//                              :nonbankCount,
//                              :nonbankAmount,
//                              :loadFilename,
//                              :LoadFileId,
//                              :batchCount,
//                              :bankCreditsAmt,
//                              :bankCreditsCnt,
//                              :bankSalesAmt,
//                              :bankSalesCnt,
//                              :nonBankCreditsAmt,
//                              :nonBankCreditsCnt,
//                              :nonBankSalesAmt,
//                              :nonBankSalesCnt,
//                              :volAmount[CT_VISA][TT_SALES],
//                              :volCount[CT_VISA][TT_SALES],
//                              :volAmount[CT_VISA][TT_CREDITS],
//                              :volCount[CT_VISA][TT_CREDITS],
//                              :volAmount[CT_MC][TT_SALES],
//                              :volCount[CT_MC][TT_SALES],
//                              :volAmount[CT_MC][TT_CREDITS],
//                              :volCount[CT_MC][TT_CREDITS],
//                              :volAmount[CT_DEBIT][TT_SALES],
//                              :volCount[CT_DEBIT][TT_SALES],
//                              :volAmount[CT_DEBIT][TT_CREDITS],
//                              :volCount[CT_DEBIT][TT_CREDITS],
//                              :volAmount[CT_AMEX][TT_SALES],
//                              :volCount[CT_AMEX][TT_SALES],
//                              :volAmount[CT_AMEX][TT_CREDITS],
//                              :volCount[CT_AMEX][TT_CREDITS],
//                              :volAmount[CT_DISC][TT_SALES],
//                              :volCount[CT_DISC][TT_SALES],
//                              :volAmount[CT_DISC][TT_CREDITS],
//                              :volCount[CT_DISC][TT_CREDITS],
//                              :volAmount[CT_DINERS][TT_SALES],
//                              :volCount[CT_DINERS][TT_SALES],
//                              :volAmount[CT_DINERS][TT_CREDITS],
//                              :volCount[CT_DINERS][TT_CREDITS],
//                              :volAmount[CT_JCB][TT_SALES],
//                              :volCount[CT_JCB][TT_SALES],
//                              :volAmount[CT_JCB][TT_CREDITS],
//                              :volCount[CT_JCB][TT_CREDITS],
//                              :volAmount[CT_OTHER][TT_SALES],
//                              :volCount[CT_OTHER][TT_SALES],
//                              :volAmount[CT_OTHER][TT_CREDITS],
//                              :volCount[CT_OTHER][TT_CREDITS]
//                            )
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 double __sJT_4782 = volAmount[CT_VISA][TT_SALES];
 int __sJT_4783 = volCount[CT_VISA][TT_SALES];
 double __sJT_4784 = volAmount[CT_VISA][TT_CREDITS];
 int __sJT_4785 = volCount[CT_VISA][TT_CREDITS];
 double __sJT_4786 = volAmount[CT_MC][TT_SALES];
 int __sJT_4787 = volCount[CT_MC][TT_SALES];
 double __sJT_4788 = volAmount[CT_MC][TT_CREDITS];
 int __sJT_4789 = volCount[CT_MC][TT_CREDITS];
 double __sJT_4790 = volAmount[CT_DEBIT][TT_SALES];
 int __sJT_4791 = volCount[CT_DEBIT][TT_SALES];
 double __sJT_4792 = volAmount[CT_DEBIT][TT_CREDITS];
 int __sJT_4793 = volCount[CT_DEBIT][TT_CREDITS];
 double __sJT_4794 = volAmount[CT_AMEX][TT_SALES];
 int __sJT_4795 = volCount[CT_AMEX][TT_SALES];
 double __sJT_4796 = volAmount[CT_AMEX][TT_CREDITS];
 int __sJT_4797 = volCount[CT_AMEX][TT_CREDITS];
 double __sJT_4798 = volAmount[CT_DISC][TT_SALES];
 int __sJT_4799 = volCount[CT_DISC][TT_SALES];
 double __sJT_4800 = volAmount[CT_DISC][TT_CREDITS];
 int __sJT_4801 = volCount[CT_DISC][TT_CREDITS];
 double __sJT_4802 = volAmount[CT_DINERS][TT_SALES];
 int __sJT_4803 = volCount[CT_DINERS][TT_SALES];
 double __sJT_4804 = volAmount[CT_DINERS][TT_CREDITS];
 int __sJT_4805 = volCount[CT_DINERS][TT_CREDITS];
 double __sJT_4806 = volAmount[CT_JCB][TT_SALES];
 int __sJT_4807 = volCount[CT_JCB][TT_SALES];
 double __sJT_4808 = volAmount[CT_JCB][TT_CREDITS];
 int __sJT_4809 = volCount[CT_JCB][TT_CREDITS];
 double __sJT_4810 = volAmount[CT_OTHER][TT_SALES];
 int __sJT_4811 = volCount[CT_OTHER][TT_SALES];
 double __sJT_4812 = volAmount[CT_OTHER][TT_CREDITS];
 int __sJT_4813 = volCount[CT_OTHER][TT_CREDITS];
   String theSqlTS = "insert into daily_detail_file_ext_summary\n                          (\n                            batch_date,\n                            merchant_number,\n                          --bank_number,\n                            bank_count,\n                            bank_amount,\n                            nonbank_count,\n                            nonbank_amount,\n                            load_filename,\n                            load_file_id,\n                            batch_count,\n                            bank_credits_amount,\n                            bank_credits_count,\n                            bank_sales_amount,\n                            bank_sales_count,\n                            nonbank_credits_amount,\n                            nonbank_credits_count,\n                            nonbank_sales_amount,\n                            nonbank_sales_count,\n                            visa_sales_amount,\n                            visa_sales_count,\n                            visa_credits_amount,\n                            visa_credits_count,\n                            mc_sales_amount,\n                            mc_sales_count,\n                            mc_credits_amount,\n                            mc_credits_count,\n                            debit_sales_amount,\n                            debit_sales_count,\n                            debit_credits_amount,\n                            debit_credits_count,\n                            amex_sales_amount,\n                            amex_sales_count,\n                            amex_credits_amount,\n                            amex_credits_count,\n                            disc_sales_amount,\n                            disc_sales_count,\n                            disc_credits_amount,\n                            disc_credits_count,\n                            diners_sales_amount,\n                            diners_sales_count,\n                            diners_credits_amount,\n                            diners_credits_count,\n                            jcb_sales_amount,\n                            jcb_sales_count,\n                            jcb_credits_amount,\n                            jcb_credits_count,\n                            other_sales_amount,\n                            other_sales_count,\n                            other_credits_amount,\n                            other_credits_count\n                          )\n              values      (\n                             :1 ,\n                             :2 ,\n                          --:bankNumber,\n                             :3 ,\n                             :4 ,\n                             :5 ,\n                             :6 ,\n                             :7 ,\n                             :8 ,\n                             :9 ,\n                             :10 ,\n                             :11 ,\n                             :12 ,\n                             :13 ,\n                             :14 ,\n                             :15 ,\n                             :16 ,\n                             :17 ,\n                             :18 ,\n                             :19 ,\n                             :20 ,\n                             :21 ,\n                             :22 ,\n                             :23 ,\n                             :24 ,\n                             :25 ,\n                             :26 ,\n                             :27 ,\n                             :28 ,\n                             :29 ,\n                             :30 ,\n                             :31 ,\n                             :32 ,\n                             :33 ,\n                             :34 ,\n                             :35 ,\n                             :36 ,\n                             :37 ,\n                             :38 ,\n                             :39 ,\n                             :40 ,\n                             :41 ,\n                             :42 ,\n                             :43 ,\n                             :44 ,\n                             :45 ,\n                             :46 ,\n                             :47 ,\n                             :48 ,\n                             :49 \n                          )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"27com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,batchDate);
   __sJT_st.setLong(2,merchantNumber);
   __sJT_st.setInt(3,bankCount);
   __sJT_st.setDouble(4,bankAmount);
   __sJT_st.setInt(5,nonbankCount);
   __sJT_st.setDouble(6,nonbankAmount);
   __sJT_st.setString(7,loadFilename);
   __sJT_st.setInt(8,LoadFileId);
   __sJT_st.setInt(9,batchCount);
   __sJT_st.setDouble(10,bankCreditsAmt);
   __sJT_st.setInt(11,bankCreditsCnt);
   __sJT_st.setDouble(12,bankSalesAmt);
   __sJT_st.setInt(13,bankSalesCnt);
   __sJT_st.setDouble(14,nonBankCreditsAmt);
   __sJT_st.setInt(15,nonBankCreditsCnt);
   __sJT_st.setDouble(16,nonBankSalesAmt);
   __sJT_st.setInt(17,nonBankSalesCnt);
   __sJT_st.setDouble(18,__sJT_4782);
   __sJT_st.setInt(19,__sJT_4783);
   __sJT_st.setDouble(20,__sJT_4784);
   __sJT_st.setInt(21,__sJT_4785);
   __sJT_st.setDouble(22,__sJT_4786);
   __sJT_st.setInt(23,__sJT_4787);
   __sJT_st.setDouble(24,__sJT_4788);
   __sJT_st.setInt(25,__sJT_4789);
   __sJT_st.setDouble(26,__sJT_4790);
   __sJT_st.setInt(27,__sJT_4791);
   __sJT_st.setDouble(28,__sJT_4792);
   __sJT_st.setInt(29,__sJT_4793);
   __sJT_st.setDouble(30,__sJT_4794);
   __sJT_st.setInt(31,__sJT_4795);
   __sJT_st.setDouble(32,__sJT_4796);
   __sJT_st.setInt(33,__sJT_4797);
   __sJT_st.setDouble(34,__sJT_4798);
   __sJT_st.setInt(35,__sJT_4799);
   __sJT_st.setDouble(36,__sJT_4800);
   __sJT_st.setInt(37,__sJT_4801);
   __sJT_st.setDouble(38,__sJT_4802);
   __sJT_st.setInt(39,__sJT_4803);
   __sJT_st.setDouble(40,__sJT_4804);
   __sJT_st.setInt(41,__sJT_4805);
   __sJT_st.setDouble(42,__sJT_4806);
   __sJT_st.setInt(43,__sJT_4807);
   __sJT_st.setDouble(44,__sJT_4808);
   __sJT_st.setInt(45,__sJT_4809);
   __sJT_st.setDouble(46,__sJT_4810);
   __sJT_st.setInt(47,__sJT_4811);
   __sJT_st.setDouble(48,__sJT_4812);
   __sJT_st.setInt(49,__sJT_4813);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1436^13*/
          }

          commit();

          init();
        }
        catch(Exception ie)
        {
          logEntry("createSummaryRecords(" + merchantNumber + ")", ie.toString());
        }
      }
    }
    catch(Exception e)
    {
      logEntry("createSummaryRecords(" + merchantNumber + ", " + batchDate + ")", e.toString());
    }
  }

  private int getCardTypeIndex( String cardType )
  {
    int     retVal        = CT_OTHER;

    for( int i = 0; i < CardTypeMap.length; ++i )
    {
      if ( cardType.equals( CardTypeMap[i][0] ) )
      {
        retVal = Integer.parseInt( CardTypeMap[i][1] );
        break;
      }
    }
    return( retVal );
  }

  private void storeDiscSummary( String loadFilename )
  {
    ResultSetIterator       it                = null;
    long                    loadFileId        = 0L;
    int                     recCount          = 0;
    ResultSet               resultSet         = null;

    try
    {
      // only load the discount data for files with data in daily_detail_file_dt
      //    TSYS back end daily detail file (ddf)
      //    MES back end daily detail file (mddf)
      if ( HasData[DT_DDF_DT] )
      {
        // get the load file id
        /*@lineinfo:generated-code*//*@lineinfo:1485^9*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init( :loadFilename )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init(  :1  )\n        \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"28com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1488^9*/

        /*@lineinfo:generated-code*//*@lineinfo:1490^9*/

//  ************************************************************
//  #sql [Ctx] { select load_filename_to_load_file_id( :loadFilename ) 
//            from   dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select load_filename_to_load_file_id(  :1  )  \n          from   dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"29com.mes.startup.Summarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1494^9*/

        /*@lineinfo:generated-code*//*@lineinfo:1496^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    daily_detail_disc_summary
//            where   load_file_id    = :loadFileId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    daily_detail_disc_summary\n          where   load_file_id    =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"30com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1501^9*/

        /*@lineinfo:generated-code*//*@lineinfo:1503^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_account_number  as merchant_number,
//                    dt.batch_date               as batch_date,
//                    dt.CARD_TYPE                as card_type,
//                    sum( decode(dt.debit_credit_indicator,
//                                'D',1,0) )      as sales_count,
//                    sum( decode(dt.debit_credit_indicator,
//                                'D',dt.transaction_amount,0) )
//                                                as sales_amount,
//                    sum( decode(dt.debit_credit_indicator,
//                                'C',1,0) )      as credits_count,
//                    sum( decode(dt.debit_credit_indicator,
//                                'C',dt.transaction_amount,0) )
//                                                as credits_amount,
//                    max( decode( decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type),
//                                'VS',mf.visa_disc_rate,
//                                'MC',mf.mastcd_disc_rate,
//                                'AM',mf.amex_discount_rate,
//                                'DS',mf.discvr_disc_rate,
//                                'DC',mf.diners_disc_rate,
//                                'JC',mf.jcb_disc_rate,
//                                0
//                                ) * 0.001 )
//                                                as disc_rate,
//                    max( decode( decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type),
//                                'VS',mf.visa_per_item,
//                                'MC',mf.mastcd_per_item,
//                                'AM',0,
//                                'DS',mf.discvr_per_item,
//                                'DC',mf.diners_per_item,
//                                'JC',mf.jcb_per_item,
//                                0
//                                ) * 0.001 )
//                                                as disc_per_item,
//                    trunc(sysdate)              as disc_date,
//                    round(
//                          (
//                            (
//                              -- discount rate
//                              sum( decode(dt.debit_credit_indicator,
//                                          'D',dt.transaction_amount,0) ) *
//                              max( decode( decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type),
//                                           'VS',mf.visa_disc_rate,
//                                           'MC',mf.mastcd_disc_rate,
//                                           'AM',mf.amex_discount_rate,
//                                           'DS',mf.discvr_disc_rate,
//                                           'DC',mf.diners_disc_rate,
//                                           'JC',mf.jcb_disc_rate,
//                                           0
//                                          ) * 0.001 ) *
//                              0.01
//                            ) +
//                            (
//                              -- per item fees
//                              sum( decode(dt.debit_credit_indicator, 'D',1,0) ) *
//                              max( decode( decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type),
//                                          'VS',mf.visa_per_item,
//                                          'MC',mf.mastcd_per_item,
//                                          'AM',0,
//                                          'DS',mf.discvr_per_item,
//                                          'DC',mf.diners_per_item,
//                                          'JC',mf.jcb_per_item,
//                                          0
//                                         ) * 0.001 )
//                            )
//                          ),2   -- two decimal places (TSYS rule)
//                        )                        as est_disc
//            from    daily_detail_file_dt      dt,
//                    mif                       mf
//            where   dt.load_filename = :loadFilename and
//                    mf.merchant_number(+) = dt.merchant_account_number and
//                    (
//                      substr(dt.card_type,1,1) in ('V','M') or
//                      (
//                        dt.card_type = 'AM' and
//                        substr(nvl(mf.amex_plan,'NN'),1,1) = 'D'
//                      ) or
//                      (
//                        dt.card_type = 'DS' and
//                        substr(nvl(mf.discover_plan,'NN'),1,1) = 'D'
//                      ) or
//                      (
//                        dt.card_type = 'DC' and
//                        substr(nvl(mf.diners_plan,'NN'),1,1) = 'D'
//                      ) or
//                      (
//                        dt.card_type = 'JC' and
//                        substr(nvl(mf.jcb_plan,'NN'),1,1) = 'D'
//                      )
//                    )
//            group by dt.merchant_account_number, dt.batch_date, dt.card_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_account_number  as merchant_number,\n                  dt.batch_date               as batch_date,\n                  dt.CARD_TYPE                as card_type,\n                  sum( decode(dt.debit_credit_indicator,\n                              'D',1,0) )      as sales_count,\n                  sum( decode(dt.debit_credit_indicator,\n                              'D',dt.transaction_amount,0) )\n                                              as sales_amount,\n                  sum( decode(dt.debit_credit_indicator,\n                              'C',1,0) )      as credits_count,\n                  sum( decode(dt.debit_credit_indicator,\n                              'C',dt.transaction_amount,0) )\n                                              as credits_amount,\n                  max( decode( decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type),\n                              'VS',mf.visa_disc_rate,\n                              'MC',mf.mastcd_disc_rate,\n                              'AM',mf.amex_discount_rate,\n                              'DS',mf.discvr_disc_rate,\n                              'DC',mf.diners_disc_rate,\n                              'JC',mf.jcb_disc_rate,\n                              0\n                              ) * 0.001 )\n                                              as disc_rate,\n                  max( decode( decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type),\n                              'VS',mf.visa_per_item,\n                              'MC',mf.mastcd_per_item,\n                              'AM',0,\n                              'DS',mf.discvr_per_item,\n                              'DC',mf.diners_per_item,\n                              'JC',mf.jcb_per_item,\n                              0\n                              ) * 0.001 )\n                                              as disc_per_item,\n                  trunc(sysdate)              as disc_date,\n                  round(\n                        (\n                          (\n                            -- discount rate\n                            sum( decode(dt.debit_credit_indicator,\n                                        'D',dt.transaction_amount,0) ) *\n                            max( decode( decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type),\n                                         'VS',mf.visa_disc_rate,\n                                         'MC',mf.mastcd_disc_rate,\n                                         'AM',mf.amex_discount_rate,\n                                         'DS',mf.discvr_disc_rate,\n                                         'DC',mf.diners_disc_rate,\n                                         'JC',mf.jcb_disc_rate,\n                                         0\n                                        ) * 0.001 ) *\n                            0.01\n                          ) +\n                          (\n                            -- per item fees\n                            sum( decode(dt.debit_credit_indicator, 'D',1,0) ) *\n                            max( decode( decode(substr(dt.card_type,1,1),'V','VS','M','MC',dt.card_type),\n                                        'VS',mf.visa_per_item,\n                                        'MC',mf.mastcd_per_item,\n                                        'AM',0,\n                                        'DS',mf.discvr_per_item,\n                                        'DC',mf.diners_per_item,\n                                        'JC',mf.jcb_per_item,\n                                        0\n                                       ) * 0.001 )\n                          )\n                        ),2   -- two decimal places (TSYS rule)\n                      )                        as est_disc\n          from    daily_detail_file_dt      dt,\n                  mif                       mf\n          where   dt.load_filename =  :1  and\n                  mf.merchant_number(+) = dt.merchant_account_number and\n                  (\n                    substr(dt.card_type,1,1) in ('V','M') or\n                    (\n                      dt.card_type = 'AM' and\n                      substr(nvl(mf.amex_plan,'NN'),1,1) = 'D'\n                    ) or\n                    (\n                      dt.card_type = 'DS' and\n                      substr(nvl(mf.discover_plan,'NN'),1,1) = 'D'\n                    ) or\n                    (\n                      dt.card_type = 'DC' and\n                      substr(nvl(mf.diners_plan,'NN'),1,1) = 'D'\n                    ) or\n                    (\n                      dt.card_type = 'JC' and\n                      substr(nvl(mf.jcb_plan,'NN'),1,1) = 'D'\n                    )\n                  )\n          group by dt.merchant_account_number, dt.batch_date, dt.card_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"31com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"31com.mes.startup.Summarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1595^9*/
        resultSet = it.getResultSet();

        while(resultSet.next())
        {
          /*@lineinfo:generated-code*//*@lineinfo:1600^11*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_disc_summary
//              (
//                merchant_number,
//                batch_date,
//                card_type,
//                sales_count,
//                sales_amount,
//                credits_count,
//                credits_amount,
//                disc_rate,
//                disc_per_item,
//                disc_rate_date,
//                disc_estimated,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :resultSet.getLong("merchant_number"),
//                :resultSet.getDate("batch_date"),
//                :resultSet.getString("card_type"),
//                :resultSet.getInt("sales_count"),
//                :resultSet.getDouble("sales_amount"),
//                :resultSet.getInt("credits_count"),
//                :resultSet.getDouble("credits_amount"),
//                nvl( :resultSet.getDouble("disc_rate"), 0 ),
//                nvl( :resultSet.getDouble("disc_per_item"), 0 ),
//                :resultSet.getDate("disc_date"),
//                nvl( :resultSet.getDouble("est_disc"), 0 ),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4814 = resultSet.getLong("merchant_number");
 java.sql.Date __sJT_4815 = resultSet.getDate("batch_date");
 String __sJT_4816 = resultSet.getString("card_type");
 int __sJT_4817 = resultSet.getInt("sales_count");
 double __sJT_4818 = resultSet.getDouble("sales_amount");
 int __sJT_4819 = resultSet.getInt("credits_count");
 double __sJT_4820 = resultSet.getDouble("credits_amount");
 double __sJT_4821 = resultSet.getDouble("disc_rate");
 double __sJT_4822 = resultSet.getDouble("disc_per_item");
 java.sql.Date __sJT_4823 = resultSet.getDate("disc_date");
 double __sJT_4824 = resultSet.getDouble("est_disc");
   String theSqlTS = "insert into daily_detail_disc_summary\n            (\n              merchant_number,\n              batch_date,\n              card_type,\n              sales_count,\n              sales_amount,\n              credits_count,\n              credits_amount,\n              disc_rate,\n              disc_per_item,\n              disc_rate_date,\n              disc_estimated,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 ,\n               :7 ,\n              nvl(  :8 , 0 ),\n              nvl(  :9 , 0 ),\n               :10 ,\n              nvl(  :11 , 0 ),\n               :12 ,\n               :13 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"32com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4814);
   __sJT_st.setDate(2,__sJT_4815);
   __sJT_st.setString(3,__sJT_4816);
   __sJT_st.setInt(4,__sJT_4817);
   __sJT_st.setDouble(5,__sJT_4818);
   __sJT_st.setInt(6,__sJT_4819);
   __sJT_st.setDouble(7,__sJT_4820);
   __sJT_st.setDouble(8,__sJT_4821);
   __sJT_st.setDouble(9,__sJT_4822);
   __sJT_st.setDate(10,__sJT_4823);
   __sJT_st.setDouble(11,__sJT_4824);
   __sJT_st.setString(12,loadFilename);
   __sJT_st.setLong(13,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1634^11*/

          if ( (++recCount%1000) == 0 )
          {
            // commit after every so often
            /*@lineinfo:generated-code*//*@lineinfo:1639^13*/

//  ************************************************************
//  #sql [Ctx] { commit
//               };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1642^13*/
          }
          //@System.out.print("Records Processed: " + recCount + "\r");//@
        }
        //@log.debug();//@

        resultSet.close();
        it.close();
      }
    }
    catch( java.sql.SQLException e )
    {
      logEntry(0, "storeDiscSummary(" + loadFilename + ")", e.toString());
    }
    finally
    {
      try{ resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
  }

  private void storeIcSummary( String loadFilename )
  {
    ResultSetIterator       it                = null;
    int                     recCount          = 0;
    ResultSet               resultSet         = null;
    long                    loadFileId        = 0L;

    try
    {
      // only load the interchange data for files with data in daily_detail_file_dt
      //    TSYS back end daily detail file (ddf)
      //    MES back end daily detail file (mddf)
      if ( HasData[DT_DDF_DT] )
      {
        // get the load file id
        /*@lineinfo:generated-code*//*@lineinfo:1678^9*/

//  ************************************************************
//  #sql [Ctx] { call load_file_index_init( :loadFilename )
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "BEGIN load_file_index_init(  :1  )\n        \n; END;";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"33com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1681^9*/

        /*@lineinfo:generated-code*//*@lineinfo:1683^9*/

//  ************************************************************
//  #sql [Ctx] { select load_filename_to_load_file_id( :loadFilename ) 
//            from   dual
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select load_filename_to_load_file_id(  :1  )  \n          from   dual";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"34com.mes.startup.Summarizer",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   loadFileId = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1687^9*/

        /*@lineinfo:generated-code*//*@lineinfo:1689^9*/

//  ************************************************************
//  #sql [Ctx] { delete
//            from    daily_detail_ic_summary
//            where   load_file_id = :loadFileId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "delete\n          from    daily_detail_ic_summary\n          where   load_file_id =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"35com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteUpdate();
  } finally { __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1694^9*/

        /*@lineinfo:generated-code*//*@lineinfo:1696^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  dt.merchant_account_number  as merchant_number,
//                    dt.bank_number              as bank_number,
//                    dt.batch_date               as batch_date,
//                    dt.card_type                as card_type,
//                    dt.submitted_interchange2   as ic_cat,
//                    sum( decode(dt.debit_credit_indicator,
//                                'D',1,0) )      as sales_count,
//                    sum( decode(dt.debit_credit_indicator,
//                                'D',dt.transaction_amount,0) )
//                                                as sales_amount,
//                    sum( decode(dt.debit_credit_indicator,
//                                'C',1,0) )      as credits_count,
//                    sum( decode(dt.debit_credit_indicator,
//                                'C',dt.transaction_amount,0) )
//                                                as credits_amount
//            from    daily_detail_file_dt      dt
//            where   dt.load_filename = :loadFilename
//                    and not dt.submitted_interchange2 is null
//            group by dt.merchant_account_number, dt.bank_number, dt.batch_date,
//                     dt.card_type, dt.submitted_interchange2
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  dt.merchant_account_number  as merchant_number,\n                  dt.bank_number              as bank_number,\n                  dt.batch_date               as batch_date,\n                  dt.card_type                as card_type,\n                  dt.submitted_interchange2   as ic_cat,\n                  sum( decode(dt.debit_credit_indicator,\n                              'D',1,0) )      as sales_count,\n                  sum( decode(dt.debit_credit_indicator,\n                              'D',dt.transaction_amount,0) )\n                                              as sales_amount,\n                  sum( decode(dt.debit_credit_indicator,\n                              'C',1,0) )      as credits_count,\n                  sum( decode(dt.debit_credit_indicator,\n                              'C',dt.transaction_amount,0) )\n                                              as credits_amount\n          from    daily_detail_file_dt      dt\n          where   dt.load_filename =  :1 \n                  and not dt.submitted_interchange2 is null\n          group by dt.merchant_account_number, dt.bank_number, dt.batch_date,\n                   dt.card_type, dt.submitted_interchange2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"36com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,loadFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"36com.mes.startup.Summarizer",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1718^9*/
        resultSet = it.getResultSet();

        while(resultSet.next())
        {
          /*@lineinfo:generated-code*//*@lineinfo:1723^11*/

//  ************************************************************
//  #sql [Ctx] { insert into daily_detail_ic_summary
//              (
//                merchant_number,
//                bank_number,
//                batch_date,
//                card_type,
//                ic_cat,
//                sales_count,
//                sales_amount,
//                credits_count,
//                credits_amount,
//                load_filename,
//                load_file_id
//              )
//              values
//              (
//                :resultSet.getLong("merchant_number"),
//                :resultSet.getInt("bank_number"),
//                :resultSet.getDate("batch_date"),
//                :resultSet.getString("card_type"),
//                :resultSet.getString("ic_cat"),
//                :resultSet.getInt("sales_count"),
//                :resultSet.getDouble("sales_amount"),
//                :resultSet.getInt("credits_count"),
//                :resultSet.getDouble("credits_amount"),
//                :loadFilename,
//                :loadFileId
//              )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4825 = resultSet.getLong("merchant_number");
 int __sJT_4826 = resultSet.getInt("bank_number");
 java.sql.Date __sJT_4827 = resultSet.getDate("batch_date");
 String __sJT_4828 = resultSet.getString("card_type");
 String __sJT_4829 = resultSet.getString("ic_cat");
 int __sJT_4830 = resultSet.getInt("sales_count");
 double __sJT_4831 = resultSet.getDouble("sales_amount");
 int __sJT_4832 = resultSet.getInt("credits_count");
 double __sJT_4833 = resultSet.getDouble("credits_amount");
   String theSqlTS = "insert into daily_detail_ic_summary\n            (\n              merchant_number,\n              bank_number,\n              batch_date,\n              card_type,\n              ic_cat,\n              sales_count,\n              sales_amount,\n              credits_count,\n              credits_amount,\n              load_filename,\n              load_file_id\n            )\n            values\n            (\n               :1 ,\n               :2 ,\n               :3 ,\n               :4 ,\n               :5 ,\n               :6 ,\n               :7 ,\n               :8 ,\n               :9 ,\n               :10 ,\n               :11 \n            )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"37com.mes.startup.Summarizer",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4825);
   __sJT_st.setInt(2,__sJT_4826);
   __sJT_st.setDate(3,__sJT_4827);
   __sJT_st.setString(4,__sJT_4828);
   __sJT_st.setString(5,__sJT_4829);
   __sJT_st.setInt(6,__sJT_4830);
   __sJT_st.setDouble(7,__sJT_4831);
   __sJT_st.setInt(8,__sJT_4832);
   __sJT_st.setDouble(9,__sJT_4833);
   __sJT_st.setString(10,loadFilename);
   __sJT_st.setLong(11,loadFileId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1753^11*/

          if ( (++recCount%1000) == 0 )
          {
            // commit after every so often
            /*@lineinfo:generated-code*//*@lineinfo:1758^13*/

//  ************************************************************
//  #sql [Ctx] { commit
//               };
//  ************************************************************

  ((Ctx.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : Ctx.getExecutionContext().getOracleContext()).oracleCommit(Ctx);


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:1761^13*/
          }
          //@System.out.print("Records Processed: " + recCount + "\r");//@
        }
        //@log.debug();//@

        resultSet.close();
        it.close();
      }
    }
    catch( Exception e )
    {
      logEntry(0, "storeIcSummary( " + loadFilename + " )", e.toString());
    }
    finally
    {
      try{ resultSet.close(); } catch(Exception e) {}
      try{ it.close(); } catch(Exception e) {}
    }
  }

  public class SummarizeItem
  {
    public int      fileType;
    public String   loadFilename;

    public SummarizeItem(int fileType, String loadFilename)
    {
      this.fileType     = fileType;
      this.loadFilename = loadFilename;
    }
  }

  public class DeleteItem
  {
    public  long    merchantNumber;
    public  Date    batchDate;
    public  String  loadFilename;

    public DeleteItem(long merchantNumber, Date batchDate, String loadFilename)
    {
      this.merchantNumber = merchantNumber;
      this.batchDate      = batchDate;
      this.loadFilename   = loadFilename;
    }
  }

  public class SummarizeRecord
  {
    public  long    merchantNumber;
    public  int     bankNumber;
    public  Date    batchDate;
    public  long    batchNumber;
    public  String  cardType;
    public  String  debitCreditInd;
    public  double  fxMarkupAmount;
    public  String  reject;
    public  int     tranCount;
    public  double  tranAmount;
    public  String  loadFilename;

    public SummarizeRecord( ResultSet rs )
      throws java.sql.SQLException
    {
      this.merchantNumber = rs.getLong("merchant_number");
      this.bankNumber     = rs.getInt("bank_number");
      this.batchDate      = rs.getDate("batch_date");
      this.batchNumber    = rs.getLong("batch_number");
      this.cardType       = rs.getString("card_type");
      this.debitCreditInd = rs.getString("debit_credit_ind");
      this.reject         = rs.getString("reject");
      this.tranCount      = rs.getInt("tran_count");
      this.tranAmount     = rs.getDouble("tran_amount");
      this.loadFilename   = rs.getString("load_filename");

      // optional result set columns
      try{ this.fxMarkupAmount = rs.getDouble("fx_markup_amount"); } catch( Exception ee ) {}
    }
  }

  public static void main( String[] args )
  {
    Summarizer      bean    = null;

    try
    {
      SQLJConnectionBase.initStandalone("DEBUG");

      bean = new Summarizer();
      
      bean.connect();
      bean.setDataFlags(args[1]);
      
      if( "updateTranSummary".equals(args[0]) )
      {
        bean.updateTranSummary(args[1]);
        log.debug("SUCCESS");
      }
      else if( "storeIcSummary".equals(args[0]) )
      {
        bean.storeIcSummary(args[1]);
        log.debug("SUCCESS");
      }
      else if( "summarizeIRS".equals(args[0]) )
      {
        bean.summarizeIRS(args[1]); 
        log.debug("SUCCESS");
      }
      else
      {
        log.debug("FAIL");
      }
    }
    catch(Exception e)
    {
      log.error(e.toString());
    }
    finally
    {
      try{ bean.cleanUp(); }catch(Exception e){}
    }
  }
}/*@lineinfo:generated-code*/