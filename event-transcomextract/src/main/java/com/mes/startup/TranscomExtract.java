/*@lineinfo:filename=TranscomExtract*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/TranscomExtract.sqlj $

  Description:  
    Utilities for updating the department-level status of an application


  Last Modified By   : $Author: ttran $
  Last Modified Date : $Date: 2015-10-23 17:34:23 -0700 (Fri, 23 Oct 2015) $
  Version            : $Revision: 23901 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Date;
import java.sql.ResultSet;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.CSVFileDisk;
import com.mes.support.DateTimeFormatter;
import sqlj.runtime.ResultSetIterator;

public class TranscomExtract extends TranscomFileProcess
{
  static Logger log = Logger.getLogger(TranscomExtract.class);

  public static final int   VOL_CREDITS           = 0;
  public static final int   VOL_SALES             = 1;
  public static final int   VOL_TYPE_COUNT        = 2;
  
  private Date              ActiveDate    = null;
  
  public TranscomExtract()
  {
    // disable auto-commit
    super(FILE_TYPE_EXTRACT);
  }
  
  public TranscomExtract(String connectionString)
  {
    // disable auto-commit
    super(FILE_TYPE_EXTRACT, connectionString);
  }
  
  protected String getMonthlyFilenameBase()
  {
    return( DateTimeFormatter.getFormattedDate(ActiveDate,"MMyyyy") );
  }
  
  protected double loadChargeYTD( long merchantId, Date activeDate,
                                  String cardType, String chargeType )
  {
    double          retVal        = 0.0;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:75^7*/

//  ************************************************************
//  #sql [Ctx] { select  sum( tes.MTD_INC )       
//          from    transcom_extract_summary tes
//          where   tes.merchant_number = :merchantId and
//                  tes.active_date between to_date( ('01/01/' || to_char( :activeDate, 'YYYY' )), 'mm/dd/yyyy' ) and :activeDate and
//                  ( tes.card_type = :cardType or
//                    :cardType is null ) and
//                  tes.charge_type = :chargeType
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  sum( tes.MTD_INC )        \n        from    transcom_extract_summary tes\n        where   tes.merchant_number =  :1  and\n                tes.active_date between to_date( ('01/01/' || to_char(  :2 , 'YYYY' )), 'mm/dd/yyyy' ) and  :3  and\n                ( tes.card_type =  :4  or\n                   :5  is null ) and\n                tes.charge_type =  :6";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.TranscomExtract",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,merchantId);
   __sJT_st.setDate(2,activeDate);
   __sJT_st.setDate(3,activeDate);
   __sJT_st.setString(4,cardType);
   __sJT_st.setString(5,cardType);
   __sJT_st.setString(6,chargeType);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   retVal = __sJT_rs.getDouble(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:84^7*/
    }
    catch( java.sql.SQLException e )
    {
      logEntry("loadChargeYTD()",e.toString());
    }
    return( retVal );
  }
  
  protected void loadCSVFile(Vector loadNames)
  {
    CSVFileDisk           csvFileDisk = null;
    ResultSetIterator     it          = null;
    String                loadName    = null;
    ResultSet             resultSet   = null;
    
    try
    {
      try
      {
        csvFileDisk = (CSVFileDisk) csvFile;
        csvFileDisk.clear();
      }
      catch( ClassCastException cce )
      {
        // clear the existing file
        csvFile.clear();
        csvFile = null;
        
        // create a new disk based CSV file object
        csvFileDisk = new CSVFileDisk("tps_extract_temp.csv");
        csvFile = csvFileDisk;
      }        
      
      for(int i=0; i < loadNames.size(); ++i)
      {
        loadName = (String)loadNames.elementAt(i);
        
        // store the active date for these records
        /*@lineinfo:generated-code*//*@lineinfo:123^9*/

//  ************************************************************
//  #sql [Ctx] { select  max(tes.active_date)  
//            from    transcom_extract_summary tes
//            where   tes.load_filename = :loadName
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  max(tes.active_date)   \n          from    transcom_extract_summary tes\n          where   tes.load_filename =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.TranscomExtract",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,loadName);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   ActiveDate = (java.sql.Date)__sJT_rs.getDate(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:128^9*/
      
        log.debug("  *   processing " + loadName);   //@
        
        /*@lineinfo:generated-code*//*@lineinfo:132^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  tes.merchant_number                 as ACCTNUMB,
//                    mf.dba_name                         as NAME,
//                    tes.card_type                       as CDTYPE,
//                    tes.charge_type                     as CHGTYPE,
//                    tes.sales_vol_amount                as CHGVOL,
//                    tes.credits_vol_amount              as CREDVOL,
//                    tes.sales_vol_count                 as CHGITEM,
//                    tes.credits_vol_count               as CREDITEM,
//                    tes.disc_rate_revenue               as DISCREV,
//                    tes.disc_per_item_revenue           as ITEMREV,
//                    tes.interchange_revenue             as ICREV,
//                    tes.surcharge_revenue               as SURCHREV,
//                    tes.surcharge_per_item_revenue      as SCHITMREV,
//                    tes.assessment_expense              as ASMTFEE,
//                    tes.interchange_expense             as ICFEE,
//                    tes.item_fee                        as ITEMFEE,
//                    tes.item_count                      as QTY,
//                    tes.equip_revenue                   as EQPTREV,
//                    tes.ticket_count                    as TKTS,
//                    tes.batch_count                     as BATCHES,
//                    tes.auth_count                      as AUTHS,
//                    tes.auth_per_item                   as AUFEE,
//                    tes.misc_revenue                    as MISCREV,
//                    tes.mtd_inc                         as MTDINC,
//  --                  transcom_charge_ytd( tes.merchant_number,
//  --                                       tes.active_date,
//  --                                       tes.card_type,
//  --                                       tes.charge_type )  as YTDINC,
//                    --sum(tes_ytd.mtd_inc)                as YTDINC,
//                    tes_ytd.ytd_inc                     as YTDINC, 
//                    tes.description                     as REFERENCE
//            from  transcom_extract_summary tes,
//                  mif                      mf,
//                  (
//                    select  tes_ytd.merchant_number           as merchant_number,
//                            nvl(tes_ytd.card_type,' ')        as card_type,
//                            tes_ytd.charge_type               as charge_type,
//                            sum(tes_ytd.mtd_inc)              as ytd_inc
//                    from    transcom_extract_summary tes_ytd
//                    where   tes_ytd.active_date between trunc(:ActiveDate,'year') and :ActiveDate
//                    group by tes_ytd.merchant_number, nvl(tes_ytd.card_type,' '), tes_ytd.charge_type
//                  )                        tes_ytd
//            where tes.active_date   = :ActiveDate and
//  --                tes.load_filename = :loadName and
//                  mf.merchant_number = tes.merchant_number and
//                  tes_ytd.merchant_number = tes.merchant_number and
//                  nvl(tes_ytd.card_type,' ') = nvl(tes.card_type,' ') and
//                  tes_ytd.charge_type = tes.charge_type
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  tes.merchant_number                 as ACCTNUMB,\n                  mf.dba_name                         as NAME,\n                  tes.card_type                       as CDTYPE,\n                  tes.charge_type                     as CHGTYPE,\n                  tes.sales_vol_amount                as CHGVOL,\n                  tes.credits_vol_amount              as CREDVOL,\n                  tes.sales_vol_count                 as CHGITEM,\n                  tes.credits_vol_count               as CREDITEM,\n                  tes.disc_rate_revenue               as DISCREV,\n                  tes.disc_per_item_revenue           as ITEMREV,\n                  tes.interchange_revenue             as ICREV,\n                  tes.surcharge_revenue               as SURCHREV,\n                  tes.surcharge_per_item_revenue      as SCHITMREV,\n                  tes.assessment_expense              as ASMTFEE,\n                  tes.interchange_expense             as ICFEE,\n                  tes.item_fee                        as ITEMFEE,\n                  tes.item_count                      as QTY,\n                  tes.equip_revenue                   as EQPTREV,\n                  tes.ticket_count                    as TKTS,\n                  tes.batch_count                     as BATCHES,\n                  tes.auth_count                      as AUTHS,\n                  tes.auth_per_item                   as AUFEE,\n                  tes.misc_revenue                    as MISCREV,\n                  tes.mtd_inc                         as MTDINC,\n--                  transcom_charge_ytd( tes.merchant_number,\n--                                       tes.active_date,\n--                                       tes.card_type,\n--                                       tes.charge_type )  as YTDINC,\n                  --sum(tes_ytd.mtd_inc)                as YTDINC,\n                  tes_ytd.ytd_inc                     as YTDINC, \n                  tes.description                     as REFERENCE\n          from  transcom_extract_summary tes,\n                mif                      mf,\n                (\n                  select  tes_ytd.merchant_number           as merchant_number,\n                          nvl(tes_ytd.card_type,' ')        as card_type,\n                          tes_ytd.charge_type               as charge_type,\n                          sum(tes_ytd.mtd_inc)              as ytd_inc\n                  from    transcom_extract_summary tes_ytd\n                  where   tes_ytd.active_date between trunc( :1 ,'year') and  :2 \n                  group by tes_ytd.merchant_number, nvl(tes_ytd.card_type,' '), tes_ytd.charge_type\n                )                        tes_ytd\n          where tes.active_date   =  :3  and\n--                tes.load_filename = :loadName and\n                mf.merchant_number = tes.merchant_number and\n                tes_ytd.merchant_number = tes.merchant_number and\n                nvl(tes_ytd.card_type,' ') = nvl(tes.card_type,' ') and\n                tes_ytd.charge_type = tes.charge_type";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.TranscomExtract",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,ActiveDate);
   __sJT_st.setDate(2,ActiveDate);
   __sJT_st.setDate(3,ActiveDate);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.TranscomExtract",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:182^9*/
        resultSet = it.getResultSet();
        
        if(i == 0)
        {
          // set the header
          csvFileDisk.setHeader(resultSet);
        }
        
        // add the rows
        csvFileDisk.addRows(resultSet);
        
        resultSet.close();
        it.close();
      }
    }
    catch(Exception e)
    {
      logEntry("loadCSVFile()", e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception e ){}
    }
  }
  
  public static void main( String[] args )
  {
    TranscomExtract       bean            = null;
    Vector                loadFilenames   = new Vector();
  
    try
    {
      for( int i = 0; i < args.length; ++i )
      {
        loadFilenames.addElement( args[i] );
      }
      
      bean = new TranscomExtract(SQLJConnectionBase.getDirectConnectString());
      bean.connect();
      bean.topNode = "3941500004";   //@ hard-coded
      bean.loadCSVFile(loadFilenames);
//@      bean.csvFile.showData(System.out);
    }
    catch( Exception e )
    {
    }
    finally
    {
      try{ bean.cleanUp(); } catch( Exception ee ) {}
      Runtime.getRuntime().exit(0);
    }
  }
}/*@lineinfo:generated-code*/