/*@lineinfo:filename=RiskBatchScannerInfo*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/RiskBatchScannerInfo.sqlj $

  Description:

  Last Modified By   : $Author: jfirman $
  Last Modified Date : $Date: 2013-03-21 11:20:46 -0700 (Thu, 21 Mar 2013) $
  Version            : $Revision: 20997 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.mes.constants.mesConstants;
import com.mes.data.MesDataSourceManager;
import com.mes.database.SQLJConnectionBase;
import sqlj.runtime.ResultSetIterator;

public class RiskBatchScannerInfo
  extends SQLJConnectionBase
{
  protected   long          BatchId                       = 0L;
  protected   Date          BatchDate                     = null;
  protected   Date          BatchDateMax                  = null;
  protected   Date          BatchDateMin                  = null;
  protected   int           BatchDateMinusOpenedDate      = 0;
  protected   int           BatchNumber                   = 0;
  protected   int           BatchType                     = mesConstants.MBS_BT_UNKNOWN;
  protected   int           TranCount                     = 0;
  protected   int           TranCountDebits               = 0;
  protected   int           TranCountCredits              = 0;
  protected   double        AmountNet                     = 0.0;
  protected   double        AmountSum                     = 0.0;
  protected   double        AmountDebits                  = 0.0;
  protected   double        AmountCredits                 = 0.0;
  protected   String        AccountIsTest                 = null;
  protected   String        AccountStatus                 = null;
  protected   String        CurrencyCode                  = null;
  protected   String        DbaName                       = null;
  protected   String        MerchantNumberString          = null;
  protected   String        ProfileIdString               = null;
  protected   String        TridentBatchIdString          = null;
  protected   boolean  		isNonUsCreditExempt		  	  = false; 	

  public void setScannerInfo( long batchId )
  {
    ResultSetIterator   it              = null;
    ResultSet           resultSet       = null;

    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:67^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  mb.merchant_batch_date                                    as batch_date,
//                  mb.detail_timestamp_max                                   as batch_date_max,
//                  mb.detail_timestamp_min                                   as batch_date_min,
//                  trunc(mb.merchant_batch_date
//                          - nvl(mmddyy_to_date(mf.date_opened),sysdate))    as batch_date_minus_opened,
//                  mb.batch_number                                           as batch_number,
//                  mb.mbs_batch_type                                         as batch_type,
//                  mb.total_count                                            as tran_count,
//                  mb.sales_count                                            as tran_count_debits,
//                  mb.credits_count                                          as tran_count_credits,
//                  mb.net_amount                                             as amount_net,
//                  (mb.sales_amount + mb.credits_amount)                     as amount_sum,
//                  mb.sales_amount                                           as amount_debits,
//                  mb.credits_amount                                         as amount_credits,
//                  nvl(mf.test_account,'N')                                  as account_is_test,
//                  decode(mf.test_account,'Y','D',nvl(mf.dmacctst,'-'))      as account_status,
//                  mb.currency_code                                          as currency_code,
//                  mf.dba_name                                               as dba_name,
//                  mb.merchant_number                                        as merchant_number,
//                  mb.terminal_id                                            as profile_id,
//                  mb.trident_batch_id                                       as trident_batch_id
//          from    mbs_batches   mb,
//                  mif           mf
//          where   mb.batch_id = :batchId
//                  and mf.merchant_number = mb.merchant_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mb.merchant_batch_date                                    as batch_date,\n                mb.detail_timestamp_max                                   as batch_date_max,\n                mb.detail_timestamp_min                                   as batch_date_min,\n                trunc(mb.merchant_batch_date\n                        - nvl(mmddyy_to_date(mf.date_opened),sysdate))    as batch_date_minus_opened,\n                mb.batch_number                                           as batch_number,\n                mb.mbs_batch_type                                         as batch_type,\n                mb.total_count                                            as tran_count,\n                mb.sales_count                                            as tran_count_debits,\n                mb.credits_count                                          as tran_count_credits,\n                mb.net_amount                                             as amount_net,\n                (mb.sales_amount + mb.credits_amount)                     as amount_sum,\n                mb.sales_amount                                           as amount_debits,\n                mb.credits_amount                                         as amount_credits,\n                nvl(mf.test_account,'N')                                  as account_is_test,\n                decode(mf.test_account,'Y','D',nvl(mf.dmacctst,'-'))      as account_status,\n                mb.currency_code                                          as currency_code,\n                mf.dba_name                                               as dba_name,\n                mb.merchant_number                                        as merchant_number,\n                mb.terminal_id                                            as profile_id,\n                mb.trident_batch_id                                       as trident_batch_id\n        from    mbs_batches   mb,\n                mif           mf\n        where   mb.batch_id =  :1  \n                and mf.merchant_number = mb.merchant_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.RiskBatchScannerInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,batchId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.RiskBatchScannerInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:94^7*/
      resultSet = it.getResultSet();
      if( resultSet.next() )
      {
        BatchId                       = batchId;
        BatchDate                     = resultSet.getDate   ( "batch_date"                  );
        BatchDateMax                  = resultSet.getDate   ( "batch_date_max"              );
        BatchDateMin                  = resultSet.getDate   ( "batch_date_min"              );
        BatchDateMinusOpenedDate      = resultSet.getInt    ( "batch_date_minus_opened"     );
        BatchNumber                   = resultSet.getInt    ( "batch_number"                );
        BatchType                     = resultSet.getInt    ( "batch_type"                  );
        TranCount                     = resultSet.getInt    ( "tran_count"                  );
        TranCountDebits               = resultSet.getInt    ( "tran_count_debits"           );
        TranCountCredits              = resultSet.getInt    ( "tran_count_credits"          );
        AmountNet                     = resultSet.getDouble ( "amount_net"                  );
        AmountSum                     = resultSet.getDouble ( "amount_sum"                  );
        AmountDebits                  = resultSet.getDouble ( "amount_debits"               );
        AmountCredits                 = resultSet.getDouble ( "amount_credits"              );
        AccountIsTest                 = resultSet.getString ( "account_is_test"             );
        AccountStatus                 = resultSet.getString ( "account_status"              );
        CurrencyCode                  = resultSet.getString ( "currency_code"               );
        DbaName                       = resultSet.getString ( "dba_name"                    );
        MerchantNumberString          = resultSet.getString ( "merchant_number"             );
        ProfileIdString               = resultSet.getString ( "profile_id"                  );
        TridentBatchIdString          = resultSet.getString ( "trident_batch_id"            );
      }
      else
      {
        throw new Exception("Result Set Empty");
      }
      resultSet.close();
      it.close();
    }
    catch( Exception e )
    {
      logEntry("RiskBatchScannerInfo.setScannerInfo(" + batchId + ")",e.toString());
    }
    finally
    {
      try{ resultSet.close(); } catch( Exception ee ) {}
      try{ it.close(); } catch( Exception ee ) {}
      cleanUp();
    }
  }
  
  
  public void setScannerExemptInfo(long merchantNumber, long batchId)
  {
	ResultSet resultSet = null;
	String theSqlTS = " SELECT DECODE(merch_exempt.classname, 'NonUSCredits', :1) AS scanner_index, "
						+" merch_exempt.merchant_number, merch_exempt.valid_date_begin, merch_exempt.valid_date_end "
						+" FROM batch_scanner_merchant_exempt merch_exempt, "
						+" mbs_batches mb "
						+" WHERE mb.batch_id                = :2 "
						+" AND mb.merchant_number           = merch_exempt.merchant_number "
						+" AND merch_exempt.merchant_number = :3 "
						+" AND merch_exempt.valid_date_end  > SYSDATE "
						+" AND to_date(mb.merchant_batch_date,'DD-MM-YYYY') BETWEEN to_date(merch_exempt.valid_date_begin,'DD-MM-YYYY') AND to_date(merch_exempt.valid_date_end,'DD-MM-YYYY') "
						+" ORDER BY merch_exempt.valid_date_begin DESC ";
    
	try(Connection con = MesDataSourceManager.INSTANCE.getDataSource().getConnection();
			PreparedStatement ps = con.prepareStatement(theSqlTS)){
		
		ps.setInt(1, RiskBatchScanner.SCANNER_NON_US_CREDITS);
		ps.setLong(2, batchId);
		ps.setLong(3, merchantNumber);
		
		resultSet = ps.executeQuery();
		
		List<Integer> scannerIndexList = new ArrayList<>();
		while(resultSet.next()){
			scannerIndexList.add(resultSet.getInt("scanner_index"));
		}
		
		if(scannerIndexList.contains(RiskBatchScanner.SCANNER_NON_US_CREDITS)){
			isNonUsCreditExempt = true;
		}
	}
	catch( Exception e ) {
	  logEntry("RiskBatchScannerInfo.setScannerExemptInfo(" + merchantNumber + ")",e);
	}
	finally {
		try {
			if(resultSet != null) {
				resultSet.close();
			}
		} catch (SQLException e) {
			logEntry("RiskBatchScannerInfo.setScannerExemptInfo(" + merchantNumber + ")",e);
		}
	}
  }
}/*@lineinfo:generated-code*/