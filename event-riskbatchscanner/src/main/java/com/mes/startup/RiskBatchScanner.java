/*@lineinfo:filename=RiskBatchScanner*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $URL: http://10.1.4.151/svn/busplatform/branches/te-sac-prod-176/src/main/com/mes/startup/RiskBatchScanner.sqlj $

  Description:

  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See SVN database

  Copyright (C) 2000-2010,2011 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.startup;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;
import org.apache.log4j.Logger;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.mesConstants;
import com.mes.data.merchant.ProcessForcedPostsService;
import com.mes.database.OracleConnectionPool;
import com.mes.database.SQLJConnectionBase;
import com.mes.net.MailMessage;
import com.mes.settlement.SettlementDb;
import com.mes.support.DateTimeFormatter;
import com.mes.support.MesMath;
import com.mes.support.TridentTools;
import com.mes.tools.DccRateUtil;
import masthead.formats.visak.Batch;
import masthead.formats.visak.DetailRecord;
import sqlj.runtime.ResultSetIterator;

public class RiskBatchScanner extends EventBase
{
  // all possible duplicate types
  public static final int DUP_AUTHS                 = 0x0001;   // this one
  public static final int DUP_AUTHS_ALL             = 0x0002;   //   or this one
  public static final int DUP_BATCH_NUM             = 0x0004;
  public static final int DUP_TRANS_ALL             = 0x0008;

  // all possible scanners, and scanner count
  public static final int SCANNER_DISABLED          = -2;
  public static final int SCANNER_UNKNOWN           = -1;
  public static final int SCANNER_DUPLICATES        =  0;
  public static final int SCANNER_TEST_ACCOUNT      =  1;
  public static final int SCANNER_ACCOUNT_STATUS    =  2;
  public static final int SCANNER_NO_ACTIVITY       =  3;
  public static final int SCANNER_HIGH_DOLLAR       =  4;
  public static final int SCANNER_CREDIT_BATCH      =  5;
  public static final int SCANNER_NON_US_CREDITS    =  6;
  public static final int SCANNER_NO_AUTH_LINK      =  7;
  public static final int SCANNER_BATCH_ERR         =  8;
  public static final int SCANNER_FORCE_POST        = 9;
  public static final int SCANNER_COUNT             =  10;   // MUST BE HIGHEST VALUE

  public String []        HoldsByIndex              =  new String [SCANNER_COUNT];
  public String []        HoldsByPriority           =  new String [SCANNER_COUNT];
  public int    []        AddressesByPriority       =  new int    [SCANNER_COUNT];
  public int    []        PrioritiesByIndex         =  new int    [SCANNER_COUNT];

  static Logger log = Logger.getLogger(RiskBatchScanner.class);
  
  protected   String        TestFilename      = null;
  protected   boolean       Verbose           = false;

  public boolean execute()
  {
    boolean               retVal              = false;
    
    try
    {
      connect(true);
      
      scanBatches();

      retVal = true;
    }
    catch(Exception e)
    {
      logEntry("execute()", e.toString());
    }
    finally
    {
      cleanUp();
    }
    return( retVal );
  }
  
  protected boolean inTestMode( )
  {
    return(TestFilename != null);
  }
  
  protected void notifyRisk( RiskBatchScannerInfo rbsInfo, String holdFilename, int msgAddrs )
  {
    String              crlf            = "\r\n";
    
    try
    {
      String btcDate  = DateTimeFormatter.getFormattedDate(rbsInfo.BatchDate,"MM/dd/yyyy");
      String subject  = "Trident Batch Held - " + rbsInfo.TridentBatchIdString;
      String body     =  
        "Trident Batch Held"                                                + crlf +
        "==================================================="               + crlf +
        "Batch ID            : " + rbsInfo.TridentBatchIdString             + crlf +
        "Profile ID          : " + rbsInfo.ProfileIdString                  + crlf +
        "Merchant ID         : " + rbsInfo.MerchantNumberString             + crlf +
        "DBA Name            : " + rbsInfo.DbaName                          + crlf +
        "Detail Record Count : " + rbsInfo.TranCount                        + crlf +
        "Net Amount          : " + MesMath.toCurrency(rbsInfo.AmountNet)    + crlf +
        "Hold Reason         : " + holdFilename                             + crlf + crlf +
        "WRS Link            : https://www.merchante-solutions.com/jsp/reports/trident_batch_summary.jsp" 
                                + "?nodeId="            + rbsInfo.MerchantNumberString
                                + "&reportDateBegin="   + btcDate
                                + "&reportDateEnd="     + btcDate
                                + "&searchValue="       + rbsInfo.MerchantNumberString;
                                
      MailMessage msg = new MailMessage();
      msg.setAddresses(msgAddrs);
      msg.setSubject( subject );
      msg.setText( body );
      msg.send();
    }
    catch( Exception e )
    {
      logEntry("notifyRisk(" + rbsInfo.BatchId + ")",e.toString());
    }
    finally
    {
    }        
  }
  
  protected void scanBatches( )
  {
    int                   bankNumber          = 0;
    Vector                batchesToScan       = new Vector();
    SettlementDb          db                  = null;
    DccRateUtil           rates               = null;
    ResultSetIterator     it                  = null;
    ResultSet             resultSet           = null;
  
    try
    {
      bankNumber    = Integer.parseInt(getEventArg(0)); 

      // select the batch ids that are ready to be scanned
      if( !inTestMode() )
      {
        /*@lineinfo:generated-code*//*@lineinfo:165^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mb.batch_id
//            from    mbs_batches   mb
//            where   mb.merchant_batch_date >= trunc(sysdate-7)
//                    and mb.load_file_id = -1
//                    and mb.load_filename is null
//                    and mb.bank_number = :bankNumber
//                    and mb.response_code = 0    -- only GB's
//                    and mb.test_flag = 'N'      -- only prod batches
//  
//                     -- AND (already waiting for half an hour OR not VisaK OR finished auth linking)
//                    and (mb.mesdb_timestamp < (sysdate-0.02)
//                      or mb.mbs_batch_type != 1 -- :(mesConstants.MBS_BT_VISAK)
//                      or not exists ( select  /*+ index(tdl IDX_TRIDENT_DTL_BATCH_REC_ID) */ 
//                                              tdl.auth_rec_id
//                                      from    trident_detail_lookup   tdl
//                                      where   tdl.batch_rec_id = mb.batch_id
//                                              and tdl.auth_rec_id = 0) )
//            order by mb.mesdb_timestamp, mb.batch_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mb.batch_id\n          from    mbs_batches   mb\n          where   mb.merchant_batch_date >= trunc(sysdate-7)\n                  and mb.load_file_id = -1\n                  and mb.load_filename is null\n                  and mb.bank_number =  :1  \n                  and mb.response_code = 0    -- only GB's\n                  and mb.test_flag = 'N'      -- only prod batches\n\n                   -- AND (already waiting for half an hour OR not VisaK OR finished auth linking)\n                  and (mb.mesdb_timestamp < (sysdate-0.02)\n                    or mb.mbs_batch_type != 1 -- :(mesConstants.MBS_BT_VISAK)\n                    or not exists ( select  /*+ index(tdl IDX_TRIDENT_DTL_BATCH_REC_ID) */ \n                                            tdl.auth_rec_id\n                                    from    trident_detail_lookup   tdl\n                                    where   tdl.batch_rec_id = mb.batch_id\n                                            and tdl.auth_rec_id = 0) )\n          order by mb.mesdb_timestamp, mb.batch_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.startup.RiskBatchScanner",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.startup.RiskBatchScanner",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:185^9*/
      }
      else if( !Character.isDigit(TestFilename.charAt(0)) )
      {
        /*@lineinfo:generated-code*//*@lineinfo:189^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mb.batch_id
//            from    mbs_batches   mb
//            where   mb.load_file_id = load_filename_to_load_file_id(:TestFilename)
//            order by mb.mesdb_timestamp, mb.batch_id
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mb.batch_id\n          from    mbs_batches   mb\n          where   mb.load_file_id = load_filename_to_load_file_id( :1  )\n          order by mb.mesdb_timestamp, mb.batch_id";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.RiskBatchScanner",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,TestFilename);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.startup.RiskBatchScanner",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:195^9*/
      }
      else
      {
        long batchId = Long.valueOf(TestFilename).longValue();
        /*@lineinfo:generated-code*//*@lineinfo:200^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  mb.batch_id
//            from    mbs_batches   mb
//            where   mb.batch_id = :batchId
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mb.batch_id\n          from    mbs_batches   mb\n          where   mb.batch_id =  :1 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.RiskBatchScanner",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,batchId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.startup.RiskBatchScanner",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:205^9*/
      }
      resultSet = it.getResultSet();

      while( resultSet.next() )
      {
        batchesToScan.add(resultSet.getLong("batch_id"));
      }
      resultSet.close();
      it.close();
      
      // if any batches were selected...
      if( batchesToScan.size() > 0 )
      {
        // assume there's at least one Visa K batch to be scanned, and get a SettlementDb
        db = new SettlementDb();
        db.connect(true);

        // clear priorities (to disable scanners)
        for( int idxScanner = 0; idxScanner < SCANNER_COUNT; ++idxScanner )
        {
          PrioritiesByIndex[idxScanner] = SCANNER_UNKNOWN;
        }

        // select the scanners for this bank_number
        /*@lineinfo:generated-code*//*@lineinfo:230^9*/

//  ************************************************************
//  #sql [Ctx] it = { select  decode( rbs.classname,
//                            'Duplicates'          , :SCANNER_DUPLICATES,
//                            'TestAccount'         , :SCANNER_TEST_ACCOUNT,
//                            'ClosedAccount'       , :SCANNER_ACCOUNT_STATUS,
//                            'NoActivity'          , :SCANNER_NO_ACTIVITY,
//                            'HighDollar'          , :SCANNER_HIGH_DOLLAR,
//                            'CreditBatch'         , :SCANNER_CREDIT_BATCH,
//                            'NonUSCredits'        , :SCANNER_NON_US_CREDITS,
//                            'NoAuthLink'          , :SCANNER_NO_AUTH_LINK,
//                                                    :SCANNER_UNKNOWN )  as scanner_index,
//                    nvl(rbs.enabled,'N')                                             as enabled,
//                    nvl(rbs.msg_addrs,:MesEmails.MSG_ADDRS_RISK_BATCH_HOLD_NOTIFY) as msg_addrs
//            from    risk_batch_scanners   rbs
//            where   rbs.bank_number in ( 9999,:bankNumber )
//           order by rbs.priority, rbs.bank_number
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  decode( rbs.classname,\n                          'Duplicates'          ,  :1  ,\n                          'TestAccount'         ,  :2  ,\n                          'ClosedAccount'       ,  :3  ,\n                          'NoActivity'          ,  :4  ,\n                          'HighDollar'          ,  :5  ,\n                          'CreditBatch'         ,  :6  ,\n                          'NonUSCredits'        ,  :7  ,\n                          'NoAuthLink'          ,  :8  ,\n 'ForcePost'           ,  :9 ,\n                                                   :10   )  as scanner_index,\n                  nvl(rbs.enabled,'N')                                             as enabled,\n                  nvl(rbs.msg_addrs, :11  ) as msg_addrs\n          from    risk_batch_scanners   rbs\n          where   rbs.bank_number in ( 9999, :12   )\n         order by rbs.priority, rbs.bank_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.RiskBatchScanner",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,SCANNER_DUPLICATES);
   __sJT_st.setInt(2,SCANNER_TEST_ACCOUNT);
   __sJT_st.setInt(3,SCANNER_ACCOUNT_STATUS);
   __sJT_st.setInt(4,SCANNER_NO_ACTIVITY);
   __sJT_st.setInt(5,SCANNER_HIGH_DOLLAR);
   __sJT_st.setInt(6,SCANNER_CREDIT_BATCH);
   __sJT_st.setInt(7,SCANNER_NON_US_CREDITS);
   __sJT_st.setInt(8,SCANNER_NO_AUTH_LINK);
   __sJT_st.setInt(9, SCANNER_FORCE_POST);
   __sJT_st.setInt(10,SCANNER_UNKNOWN);
   __sJT_st.setInt(11,MesEmails.MSG_ADDRS_RISK_BATCH_HOLD_NOTIFY);
   __sJT_st.setInt(12,bankNumber);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"3com.mes.startup.RiskBatchScanner",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:247^9*/
        resultSet = it.getResultSet();

        int idxPriority = 0;

        // must set "batch error" if cannot complete all risk scanning; make highest priority
        AddressesByPriority [idxPriority]         = MesEmails.MSG_ADDRS_RESEARCH_BATCH_HOLD_NOTIFY;
        PrioritiesByIndex   [SCANNER_BATCH_ERR]   = idxPriority++;

        while ( resultSet.next() )
        {
          int idxScanner = resultSet.getInt("scanner_index");
          if( idxScanner >= 0 && PrioritiesByIndex[idxScanner] == SCANNER_UNKNOWN )
          {
            if( "Y".equals(resultSet.getString("enabled")) )
            {
              AddressesByPriority [idxPriority] = resultSet.getInt("msg_addrs");
              PrioritiesByIndex   [idxScanner]  = idxPriority++;
            }
            else
            {
              PrioritiesByIndex   [idxScanner]  = SCANNER_DISABLED;
            }
          }
        }
        resultSet.close();
        it.close();

        // not all batchTypes have duplicate checks, but all available duplicate scanning must be performed
        if( PrioritiesByIndex[SCANNER_DUPLICATES] < 0 )
        {
          AddressesByPriority [idxPriority]         = MesEmails.MSG_ADDRS_RESEARCH_BATCH_HOLD_NOTIFY;
          PrioritiesByIndex   [SCANNER_DUPLICATES]  = idxPriority++;
        }

        // for each batch...
        RiskBatchScannerInfo rbsInfo = new RiskBatchScannerInfo();
        for( int i = 0; i < batchesToScan.size(); ++i )
        {
          // clear all the hold reasons
          for( int idx = 0; idx < SCANNER_COUNT; ++idx )
          {
            HoldsByPriority [idx] = null;
            HoldsByIndex    [idx] = null;
          }

          long batchId = ((Long)batchesToScan.elementAt(i)).longValue();
          rbsInfo.setScannerInfo(batchId);
          rbsInfo.setScannerExemptInfo(Long.valueOf(rbsInfo.MerchantNumberString), batchId);

          double  dccRate = 1;
          if( !"840".equals(rbsInfo.CurrencyCode) )
          {
            if( rates == null ) rates = DccRateUtil.getInstance();

            dccRate = rates.getBuyRate("MC","840",rbsInfo.CurrencyCode);
            if( dccRate == 0 )
            {
              if( HoldsByIndex[SCANNER_BATCH_ERR] == null )
                HoldsByIndex[SCANNER_BATCH_ERR]       = "cancelled-bad-currency";
              else
                HoldsByIndex[SCANNER_BATCH_ERR]       = "cancelled-bad-currency" + ", " + HoldsByIndex[SCANNER_BATCH_ERR];
              dccRate = 1;
            }
            else
            {
              // try to eliminate the worst-case difference between the "mc buy rate" we're using
              //   and the "mc sell rate" or "vs buy rate" or "vs sell rate" that we should be using
              dccRate += 0.02;
            }
          }

          if ( Verbose ) { System.out.print("Scanning batch id " + batchId + "             \r"); }

          // perform all scans on this batch; will set hold reasons by index
          scanBatch( rbsInfo, db, dccRate );

          // copy "hold reasons by index" to "hold reasons by priority"
          for( int idxScanner = 0; idxScanner < SCANNER_COUNT; ++idxScanner )
          {
            if( PrioritiesByIndex[idxScanner] >= 0 )
              HoldsByPriority[ PrioritiesByIndex[idxScanner] ] = HoldsByIndex[idxScanner];
          }

          // the highest priority that fails will determine email addr and set the hold-name in the data base;
          // the email will list them all, with highest priority first
          String  holdFilename      = null;
          String  holdFilenameList  = null;
          int     msgAddress        = 0;
          for( idxPriority = 0; idxPriority < SCANNER_COUNT; ++idxPriority )
          {
            if( HoldsByPriority[idxPriority] != null )
            {
              if( holdFilename == null )
              {
                msgAddress        = AddressesByPriority[idxPriority];
                holdFilename      = HoldsByPriority[idxPriority];
                holdFilenameList  = HoldsByPriority[idxPriority];
              }
              else
              {
                holdFilenameList  = holdFilenameList + ", " + HoldsByPriority[idxPriority];
              }
            }
          }
        
          // if not in test mode, mark the batch as scanned and set hold filename
          if ( !inTestMode() )
          {
            if( holdFilenameList != null )
            {
              notifyRisk(rbsInfo,holdFilenameList,msgAddress);
              if( holdFilename.length() > 32 )
                holdFilename = holdFilename.substring(0,32);
            }

            /*@lineinfo:generated-code*//*@lineinfo:362^13*/

//  ************************************************************
//  #sql [Ctx] { update  mbs_batches   mb
//                set     mb.load_filename  = :holdFilename,
//                        mb.load_file_id   = 0
//                where   mb.merchant_batch_date = :rbsInfo.BatchDate
//                        and mb.batch_id = :batchId
//                        and mb.load_file_id = -1
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "update  mbs_batches   mb\n              set     mb.load_filename  =  :1  ,\n                      mb.load_file_id   = 0\n              where   mb.merchant_batch_date =  :2  \n                      and mb.batch_id =  :3  \n                      and mb.load_file_id = -1";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"4com.mes.startup.RiskBatchScanner",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,holdFilename);
   __sJT_st.setDate(2,rbsInfo.BatchDate);
   __sJT_st.setLong(3,batchId);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:370^13*/
          }
          else
          {
            if( holdFilenameList != null )
            {
              if( holdFilename.length() > 32 )
                holdFilename = holdFilename.substring(0,32);
              System.out.println();
              System.out.println("held " + batchId + " --> " + msgAddress);
              System.out.println("held " + batchId + " --> " + holdFilename);
              System.out.println("held " + batchId + " --> " + holdFilenameList);
              //notifyRisk(rbsInfo,holdFilenameList,msgAddress);
            }
          }
        }   // for loop through batches

        if ( Verbose ) { System.out.println(); }
      }
    }
    catch( Exception e )
    {
    }
    finally
    {
      try{ db.cleanUp(); } catch( Exception ee ) {}
      try{ it.close(); } catch( Exception ee ) {}
    }
  }
  
  protected void scanBatch( RiskBatchScannerInfo rbsInfo, SettlementDb db, double dccRate )
  {
    ResultSetIterator       it                  = null;
    ResultSet               resultSet           = null;

    ArrayList               tranAmount          = new ArrayList();
    ArrayList               tranCardNumber      = new ArrayList();
    ArrayList               tranCardType        = new ArrayList();

    int                     cntAcctStatProblems = 0;
    int                     cntDupAuths         = 0;
    int                     cntDuplicates       = 0x0000;   // bitfield to hold DUP_ constants
    int                     cntHighDollar       = 0;
    int                     cntNoAuthLink       = 0;
    int                     cntSummaries        = 1;        // 0 means problem; if don't do check, must to be >0
    double                  sumForeignCredits   = 0.0;
    double                  usd_400             =     400.0 / dccRate;
    double                  usd_1000            =    1000.0 / dccRate;
    double                  usd_100000          =  100000.0 / dccRate;
    
    boolean isForcePostEnabled = true;
	int cntForcePostTxns = 0;
    try
    {
      switch( rbsInfo.BatchType )
      {
        case mesConstants.MBS_BT_VISAK:
          // load the raw batch data from Oracle
          Batch       batchData     = db._loadRawBatchDataVisak( rbsInfo.BatchId );
          List        detailRecs    = batchData.getDetailRecords();

          // special code for SCANNER_NO_AUTH_LINK
          // only access if necessary -- trident_detail_lookup
          if( PrioritiesByIndex[SCANNER_NO_AUTH_LINK] >= 0 && rbsInfo.AmountDebits >= usd_1000 )
          {
            /*@lineinfo:generated-code*//*@lineinfo:433^13*/

//  ************************************************************
//  #sql [Ctx] { select  /*+ index(tdl IDX_TRIDENT_DTL_BATCH_REC_ID) */ 
//                        count(1)
//                
//                from    trident_detail_lookup  tdl
//                where   tdl.batch_rec_id = :rbsInfo.BatchId
//                        and tdl.auth_rec_id = -1
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  /*+ index(tdl IDX_TRIDENT_DTL_BATCH_REC_ID) */ \n                      count(1)\n               \n              from    trident_detail_lookup  tdl\n              where   tdl.batch_rec_id =  :1  \n                      and tdl.auth_rec_id = -1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.RiskBatchScanner",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,rbsInfo.BatchId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cntNoAuthLink = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:441^13*/
          }
          
          if (PrioritiesByIndex[SCANNER_FORCE_POST] >= 0) {
              // declare temps
              oracle.jdbc.OraclePreparedStatement __sJT_st = null;
              sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx;
              if (__sJT_cc == null)
                  sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
              sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext() == null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
              oracle.jdbc.OracleResultSet __sJT_rs = null;
              try {
                  String theSqlTS = "select  /*+ index(tdl IDX_TRIDENT_DTL_BATCH_REC_ID) */ " + "\n count(1)" + " from trident_detail_lookup  tdl" + "\n where   tdl.batch_rec_id =  :1  " + "\n and tdl.auth_rec_id = -1 and tdl.debit_credit_indicator != 'C'";
                  __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc, "5com.mes.startup.RiskBatchScanner", theSqlTS);
                  if (__sJT_ec.isNew()) {
                      __sJT_st.setFetchSize(2);
                  }
                  // set IN parameters
                  __sJT_st.setLong(1, rbsInfo.BatchId);
                  // execute query
                  __sJT_rs = __sJT_ec.oracleExecuteQuery();
                  if (__sJT_rs.getMetaData().getColumnCount() != 1)
                      sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1, __sJT_rs.getMetaData().getColumnCount());
                  if (!__sJT_rs.next())
                      sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
                  // retrieve OUT parameters
                  cntForcePostTxns = __sJT_rs.getInt(1);
                  if (__sJT_rs.wasNull())
                      throw new sqlj.runtime.SQLNullException();
                  if (__sJT_rs.next())
                      sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();

                  if (cntForcePostTxns > 0) {
                      ProcessForcedPostsService processForcedPostsService = new ProcessForcedPostsService();
                      isForcePostEnabled = processForcedPostsService.isProcessForcedPostsEnabled(Long.valueOf(rbsInfo.MerchantNumberString));
                  }
              }
              catch (Exception e) {
                  log.error(e.toString());
                  isForcePostEnabled = false;
              }
              finally {
                  if (__sJT_rs != null)
                      __sJT_rs.close();
                  __sJT_ec.oracleClose();
              }
          }

          // special code for SCANNER_DUPLICATES
          // always necessary to access -- trident_detail_lookup
          /*@lineinfo:generated-code*//*@lineinfo:446^11*/

//  ************************************************************
//  #sql [Ctx] { select  /*+ index(tdl IDX_TRIDENT_DTL_BATCH_REC_ID) */ 
//                      count(1)
//              
//              from    trident_detail_lookup  tdl
//              where   tdl.batch_rec_id = :rbsInfo.BatchId
//                      and tdl.auth_rec_id not in ( 0,-1,-2)
//                      and exists ( select   auth_rec_id
//                                   from     trident_detail_lookup
//                                   where    auth_rec_id = tdl.auth_rec_id
//                                            and (batch_rec_id <= tdl.batch_rec_id or link_timestamp < tdl.link_timestamp)
//                                            and rowid != tdl.rowid )
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  /*+ index(tdl IDX_TRIDENT_DTL_BATCH_REC_ID) */ \n                    count(1)\n             \n            from    trident_detail_lookup  tdl\n            where   tdl.batch_rec_id =  :1  \n                    and tdl.auth_rec_id not in ( 0,-1,-2)\n                    and exists ( select   auth_rec_id\n                                 from     trident_detail_lookup\n                                 where    auth_rec_id = tdl.auth_rec_id\n                                          and (batch_rec_id <= tdl.batch_rec_id or link_timestamp < tdl.link_timestamp)\n                                          and rowid != tdl.rowid )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.startup.RiskBatchScanner",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,rbsInfo.BatchId);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cntDupAuths = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:459^11*/

          // special code for SCANNER_DUPLICATES
          // always necessary to access -- mbs_batches
          /*@lineinfo:generated-code*//*@lineinfo:463^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  mb.batch_id,
//                      decode(mb.total_count,:rbsInfo.TranCount,
//                        (decode(mb.net_amount,:rbsInfo.AmountNet,1,2)),2)   as batch_cnt_amt_matches,
//                      decode(mb.batch_number,:rbsInfo.BatchNumber,1,2)      as batch_number_matches
//              from    mbs_batches   mb
//              where   mb.merchant_batch_date between :rbsInfo.BatchDate-7 and :rbsInfo.BatchDate
//                      and mb.terminal_id = :rbsInfo.ProfileIdString
//                      and mb.response_code = 0
//                      and (  (mb.total_count = :rbsInfo.TranCount
//                              and mb.net_amount = :rbsInfo.AmountNet)
//                          or (mb.batch_number = :rbsInfo.BatchNumber)
//                          )
//                      and mb.batch_id < :rbsInfo.BatchId
//                      and mb.mbs_batch_type = :rbsInfo.BatchType
//              order by batch_number_matches, batch_cnt_amt_matches
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  mb.batch_id,\n                    decode(mb.total_count, :1  ,\n                      (decode(mb.net_amount, :2  ,1,2)),2)   as batch_cnt_amt_matches,\n                    decode(mb.batch_number, :3  ,1,2)      as batch_number_matches\n            from    mbs_batches   mb\n            where   mb.merchant_batch_date between  :4  -7 and  :5  \n                    and mb.terminal_id =  :6  \n                    and mb.response_code = 0\n                    and (  (mb.total_count =  :7  \n                            and mb.net_amount =  :8  )\n                        or (mb.batch_number =  :9  )\n                        )\n                    and mb.batch_id <  :10  \n                    and mb.mbs_batch_type =  :11  \n            order by batch_number_matches, batch_cnt_amt_matches";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.RiskBatchScanner",theSqlTS);
   // set IN parameters
   __sJT_st.setInt(1,rbsInfo.TranCount);
   __sJT_st.setDouble(2,rbsInfo.AmountNet);
   __sJT_st.setInt(3,rbsInfo.BatchNumber);
   __sJT_st.setDate(4,rbsInfo.BatchDate);
   __sJT_st.setDate(5,rbsInfo.BatchDate);
   __sJT_st.setString(6,rbsInfo.ProfileIdString);
   __sJT_st.setInt(7,rbsInfo.TranCount);
   __sJT_st.setDouble(8,rbsInfo.AmountNet);
   __sJT_st.setInt(9,rbsInfo.BatchNumber);
   __sJT_st.setLong(10,rbsInfo.BatchId);
   __sJT_st.setInt(11,rbsInfo.BatchType);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"7com.mes.startup.RiskBatchScanner",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:480^11*/
          resultSet = it.getResultSet();
          while( resultSet.next() )
          {
            if( resultSet.getInt("batch_number_matches") == 1 )
            {
              cntDuplicates |= DUP_BATCH_NUM;
              if( resultSet.getInt("batch_cnt_amt_matches") != 1 )
                break;    // duplicate confirmed, exit while loop
            }

            // only access if necessary -- visak detail records (both for this batch && for potential duplicate batch)
            // load the suspected duplicate raw batch data from Oracle
            Batch       batchDataDup  = db._loadRawBatchDataVisak( resultSet.getLong("batch_id") );
            List        detailRecsDup = batchDataDup.getDetailRecords();

            int recIdx = 0;
            for( ; recIdx < detailRecsDup.size(); ++recIdx )
            {
              DetailRecord  detailRec     = (DetailRecord)detailRecs.get(recIdx);
              DetailRecord  detailRecDup  = (DetailRecord)detailRecsDup.get(recIdx);

              // as soon as find a mis-match in these two detail records, exit the for loop
              if( !(detailRec.getApprovalCode().trim()           ).equals(detailRecDup.getApprovalCode().trim()           ) ) break;
              if( !(detailRec.getCardholderAccountNumber().trim()).equals(detailRecDup.getCardholderAccountNumber().trim()) ) break;
              if( !(detailRec.getSettlementAmount()              ).equals(detailRecDup.getSettlementAmount()              ) ) break;
            }
            if( recIdx >= detailRecsDup.size() )
            {
              cntDuplicates |= DUP_TRANS_ALL;
            }

            if( cntDuplicates != 0x0000 )
            {
              break;    // duplicate confirmed, exit while loop
            }
          }
          resultSet.close();
          it.close();

          // special code for SCANNER_HIGH_DOLLAR && SCANNER_NON_US_CREDITS
          // only access if necessary -- visak detail records
          if( (PrioritiesByIndex[SCANNER_HIGH_DOLLAR]    >= 0 && rbsInfo.AmountSum >= usd_100000) ||
              (PrioritiesByIndex[SCANNER_NON_US_CREDITS] >= 0 && rbsInfo.TranCountCredits > 0) )
          {
            int creditsToSave = rbsInfo.TranCountCredits;

            for( int recIdx = 0; recIdx < detailRecs.size(); ++recIdx )
            {
              DetailRecord  detailRec = (DetailRecord)detailRecs.get(recIdx);
              double        amt       = TridentTools.decodeAmount(detailRec.getSettlementAmount());

              // count all high dollar amounts
              if( amt >= usd_100000 )cntHighDollar++;

              if( creditsToSave > 0 && TridentTools.isVisakCredit( detailRec.getTransactionCode() ) )
              {
                creditsToSave -= 1;

                // save all credits; will check if foreign cards later
                tranAmount.add      ( String.valueOf( amt ));
                tranCardNumber.add  ( detailRec.getCardholderAccountNumber().trim() );
                tranCardType.add    ( TridentTools.decodeVisakCardType( detailRec ) );
              }
            }
          }
          break;

        case mesConstants.MBS_BT_PG:
          // special code for SCANNER_HIGH_DOLLAR
          // only access if necessary -- trident_capture_api
          if( PrioritiesByIndex[SCANNER_HIGH_DOLLAR] >= 0 && rbsInfo.AmountSum >= usd_100000 )
          {
            // count all high dollar amounts
            /*@lineinfo:generated-code*//*@lineinfo:554^13*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//                
//                from    trident_capture_api         tapi
//                where   tapi.mesdb_timestamp between :rbsInfo.BatchDateMin and (:rbsInfo.BatchDateMax + 1)
//                        and tapi.batch_id = :rbsInfo.BatchId
//                        and tapi.transaction_amount >= :usd_100000
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n               \n              from    trident_capture_api         tapi\n              where   tapi.mesdb_timestamp between  :1   and ( :2   + 1)\n                      and tapi.batch_id =  :3  \n                      and tapi.transaction_amount >=  :4 ";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.RiskBatchScanner",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setDate(1,rbsInfo.BatchDateMin);
   __sJT_st.setDate(2,rbsInfo.BatchDateMax);
   __sJT_st.setLong(3,rbsInfo.BatchId);
   __sJT_st.setDouble(4,usd_100000);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cntHighDollar = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:562^13*/
          }

          // special code for SCANNER_NON_US_CREDITS
          // only access if necessary -- trident_capture_api
          if( PrioritiesByIndex[SCANNER_NON_US_CREDITS] >= 0 && rbsInfo.TranCountCredits > 0 
        		  && !rbsInfo.isNonUsCreditExempt)
          {
            /*@lineinfo:generated-code*//*@lineinfo:569^13*/

//  ************************************************************
//  #sql [Ctx] it = { select  card_type,
//                        dukpt_decrypt_wrapper(card_number_enc) as card_number_full,
//                        transaction_amount
//                from    trident_capture_api         tapi
//                where   tapi.mesdb_timestamp between :rbsInfo.BatchDateMin and (:rbsInfo.BatchDateMax + 1)
//                        and tapi.batch_id = :rbsInfo.BatchId
//                        and tapi.debit_credit_indicator = 'C'
//               };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  card_type,\n                      dukpt_decrypt_wrapper(card_number_enc) as card_number_full,\n                      transaction_amount\n              from    trident_capture_api         tapi\n              where   tapi.mesdb_timestamp between  :1   and ( :2   + 1)\n                      and tapi.batch_id =  :3  \n                      and tapi.debit_credit_indicator = 'C'";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"9com.mes.startup.RiskBatchScanner",theSqlTS);
   // set IN parameters
   __sJT_st.setDate(1,rbsInfo.BatchDateMin);
   __sJT_st.setDate(2,rbsInfo.BatchDateMax);
   __sJT_st.setLong(3,rbsInfo.BatchId);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"9com.mes.startup.RiskBatchScanner",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:578^13*/
            resultSet = it.getResultSet();
            while( resultSet.next() )
            {
              // save all credits; will check if foreign cards later
              tranAmount.add      ( resultSet.getString("transaction_amount") );
              tranCardNumber.add  ( resultSet.getString("card_number_full") );
              tranCardType.add    ( resultSet.getString("card_type") );
            }
            resultSet.close();
            it.close();
          }
          break;
      }

      // special code for SCANNER_NO_ACTIVITY
      // only access if necessary -- daily_detail_file_summary
      if( PrioritiesByIndex[SCANNER_NO_ACTIVITY] >= 0 )
      {
        if( rbsInfo.BatchDateMinusOpenedDate > 90 )
        {
          int   noActivityDayCount = 90;
          try{  noActivityDayCount = MesDefaults.getInt(MesDefaults.DK_RISK_SCAN_NO_ACTIVITY_DAYS); } catch( Exception ee ){}

          /*@lineinfo:generated-code*//*@lineinfo:602^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(1)
//              
//              from    daily_detail_file_summary   sm
//              where   sm.merchant_number = :rbsInfo.MerchantNumberString
//                      and sm.batch_date between (:rbsInfo.BatchDate-:noActivityDayCount) and (:rbsInfo.BatchDate)
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(1)\n             \n            from    daily_detail_file_summary   sm\n            where   sm.merchant_number =  :1  \n                    and sm.batch_date between ( :2  - :3  ) and ( :4  )";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"10com.mes.startup.RiskBatchScanner",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,rbsInfo.MerchantNumberString);
   __sJT_st.setDate(2,rbsInfo.BatchDate);
   __sJT_st.setInt(3,noActivityDayCount);
   __sJT_st.setDate(4,rbsInfo.BatchDate);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cntSummaries = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:609^11*/
        }
      }

      // special code for SCANNER_ACCOUNT_STATUS
      switch( rbsInfo.AccountStatus.charAt(0) )
      {
        case 'S':   // hold if some/all purchases; do not hold if all returns
          cntAcctStatProblems = rbsInfo.TranCountDebits;
          break;

        case 'B':   // hold
        case 'C':   // hold
        case 'D':   // hold
          cntAcctStatProblems = 1;
          break;

        default:    // do not hold
          break;
      }

      // special code for SCANNER_NON_US_CREDITS
      // only access if necessary -- xxx_ardef_table
      if(!rbsInfo.isNonUsCreditExempt) {
          for( int idx = 0; idx < tranAmount.size(); ++idx )
          {
            String  cardNumber          = (String)tranCardNumber.get(idx);
            String  cardType            = (String)tranCardType.get(idx);
            int     cntForeignCredits   = 0;

            if( "VS".equals(cardType) )
            {
              /*@lineinfo:generated-code*//*@lineinfo:640^11*/

    //  ************************************************************
    //  #sql [Ctx] { select  count(1)
//                  
//                  from    visa_ardef_table    ard
//                  where   to_number(substr(:cardNumber,1,9)) between ard.range_low and ard.range_high
//                          and not ard.issuer_country_code in ( 'US','CA' )
//                 };
    //  ************************************************************

    {
      // declare temps
      oracle.jdbc.OraclePreparedStatement __sJT_st = null;
      sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
      sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
      oracle.jdbc.OracleResultSet __sJT_rs = null;
      try {
       String theSqlTS = "select  count(1)\n             \n            from    visa_ardef_table    ard\n            where   to_number(substr( :1  ,1,9)) between ard.range_low and ard.range_high\n                    and not ard.issuer_country_code in ( 'US','CA' )";
       __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"11com.mes.startup.RiskBatchScanner",theSqlTS);
       if (__sJT_ec.isNew())
       {
         __sJT_st.setFetchSize(2);
       }
       // set IN parameters
       __sJT_st.setString(1,cardNumber);
       // execute query
       __sJT_rs = __sJT_ec.oracleExecuteQuery();
       if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
       if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
       // retrieve OUT parameters
       cntForeignCredits = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
       if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
      } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
    }


    //  ************************************************************

    /*@lineinfo:user-code*//*@lineinfo:647^11*/
            }
            else if( "MC".equals(cardType) )
            {
              /*@lineinfo:generated-code*//*@lineinfo:651^11*/

    //  ************************************************************
    //  #sql [Ctx] { select  count(1)
//                  
//                  from    mc_ardef_table      ard
//                  where   to_number(rpad(:cardNumber,19,'0')) between ard.range_low and ard.range_high
//                          and not ard.country_code_alpha in ( 'USA','CAN' )
//                 };
    //  ************************************************************

    {
      // declare temps
      oracle.jdbc.OraclePreparedStatement __sJT_st = null;
      sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
      sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
      oracle.jdbc.OracleResultSet __sJT_rs = null;
      try {
       String theSqlTS = "select  count(1)\n             \n            from    mc_ardef_table      ard\n            where   to_number(rpad( :1  ,19,'0')) between ard.range_low and ard.range_high\n                    and not ard.country_code_alpha in ( 'USA','CAN' )";
       __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"12com.mes.startup.RiskBatchScanner",theSqlTS);
       if (__sJT_ec.isNew())
       {
         __sJT_st.setFetchSize(2);
       }
       // set IN parameters
       __sJT_st.setString(1,cardNumber);
       // execute query
       __sJT_rs = __sJT_ec.oracleExecuteQuery();
       if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
       if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
       // retrieve OUT parameters
       cntForeignCredits = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
       if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
      } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
    }


    //  ************************************************************

    /*@lineinfo:user-code*//*@lineinfo:658^11*/
            }

            if( cntForeignCredits != 0 )
            {
              sumForeignCredits += Double.valueOf((String)(tranAmount.get(idx))).doubleValue();
              if( sumForeignCredits >= usd_400 )
              {
                HoldsByIndex[SCANNER_NON_US_CREDITS]  = "hold-non-us-credits";
                break;
              }
            }
          }
      }

      // special code for SCANNER_DUPLICATES
      if( cntDupAuths != 0 )
      {
        if( cntDupAuths == rbsInfo.TranCountDebits  ) cntDuplicates |= DUP_AUTHS_ALL;
        else                                          cntDuplicates |= DUP_AUTHS;
      }
      switch( cntDuplicates )
      {
        case (DUP_AUTHS_ALL | DUP_BATCH_NUM | DUP_TRANS_ALL ) : HoldsByIndex[SCANNER_DUPLICATES]  = "hold-dup-auths/batch#/trans"                     ; break;
        case (DUP_AUTHS_ALL |                 DUP_TRANS_ALL ) : HoldsByIndex[SCANNER_DUPLICATES]  = "hold-dup-auths/trans"                            ; break;
        case (DUP_AUTHS_ALL | DUP_BATCH_NUM                 ) : HoldsByIndex[SCANNER_DUPLICATES]  = "hold-dup-auths/batch#"                           ; break;
        case (DUP_AUTHS_ALL                                 ) : HoldsByIndex[SCANNER_DUPLICATES]  = "hold-dup-auths-all"                              ; break;
        case (DUP_AUTHS     | DUP_BATCH_NUM | DUP_TRANS_ALL ) : HoldsByIndex[SCANNER_DUPLICATES]  = "hold-dup-auths-" + cntDupAuths + "/batch#/trans" ; break;
        case (DUP_AUTHS     |                 DUP_TRANS_ALL ) : HoldsByIndex[SCANNER_DUPLICATES]  = "hold-dup-auths-" + cntDupAuths + "/trans"        ; break;
        case (DUP_AUTHS     | DUP_BATCH_NUM                 ) : HoldsByIndex[SCANNER_DUPLICATES]  = "hold-dup-auths-" + cntDupAuths + "/batch#"       ; break;
        case (DUP_AUTHS                                     ) : HoldsByIndex[SCANNER_DUPLICATES]  = "hold-dup-auths-" + cntDupAuths                   ; break;
        case (                DUP_BATCH_NUM | DUP_TRANS_ALL ) : HoldsByIndex[SCANNER_DUPLICATES]  = "hold-dup-batch#/trans"                           ; break;
        case (                                DUP_TRANS_ALL ) : HoldsByIndex[SCANNER_DUPLICATES]  = "hold-dup-trans"                                  ; break;
        case (                DUP_BATCH_NUM                 ) : HoldsByIndex[SCANNER_DUPLICATES]  = "hold-dup-batch#"                                 ; break;
        default : if( cntDuplicates != 0x0000 )                 HoldsByIndex[SCANNER_DUPLICATES]  = "hold-dup-something..." + cntDuplicates           ; break;
      }

      // special code for SCANNER_???
      if( rbsInfo.AccountIsTest.charAt(0) == 'Y'      ) HoldsByIndex[SCANNER_TEST_ACCOUNT]    = "cancelled-test-account";
      if( rbsInfo.AmountNet <= -(usd_400)             ) HoldsByIndex[SCANNER_CREDIT_BATCH]    = "hold-risk";
      if( cntAcctStatProblems > 0                     ) HoldsByIndex[SCANNER_ACCOUNT_STATUS]  = "hold-account-status";
      if( cntHighDollar > 0                           ) HoldsByIndex[SCANNER_HIGH_DOLLAR]     = "hold-large-ticket-" + cntHighDollar;
      if( cntSummaries == 0                           ) HoldsByIndex[SCANNER_NO_ACTIVITY]     = "hold-no-activity";
      if( cntNoAuthLink > 0                           ) HoldsByIndex[SCANNER_NO_AUTH_LINK]    = "hold-no-auth-link-" + cntNoAuthLink;
      if (!isForcePostEnabled                         ) HoldsByIndex[SCANNER_FORCE_POST]      = "hold-offline-not-permitted-" + cntForcePostTxns;
    }
    catch( Exception e )
    {
      logEntry("scanBatch(" + rbsInfo.BatchId + ")",e.toString());
    }
    finally
    {
      try{ it.close(); } catch( Exception ee ) {}
    }
  }

  protected void setTestFilename( String filename )
  {
    TestFilename = ("null".equals(filename) ? null : filename);
  }
  
  public void setVerbose( boolean value )
  {
    Verbose = value;
  }

  public static void main( String[] args )
  {
    RiskBatchScanner      rbs   = null;

    try
    {
        if (args.length > 0 && args[0].equals("testproperties")) {
            EventBase.printKeyListStatus(new String[]{
                    MesDefaults.DK_RISK_SCAN_NO_ACTIVITY_DAYS
            });
        }


      SQLJConnectionBase.initStandalone("DEBUG");
      rbs = new RiskBatchScanner();
      rbs.connect(true);
      rbs.setVerbose(true);

      Timestamp beginTime = new Timestamp(Calendar.getInstance().getTime().getTime());

      if ( "notifyRisk".equals(args[0]) )
      {
        RiskBatchScannerInfo rbsInfo = new RiskBatchScannerInfo();
        rbsInfo.setScannerInfo(Long.parseLong(args[1]));

        rbs.notifyRisk( rbsInfo,null,MesEmails.MSG_ADDRS_RISK_BATCH_HOLD_NOTIFY );
      }
      else if ( "execute".equals(args[0]) )
      {
        rbs.setTestFilename(args[1]);
        rbs.setEventArgs( ((args.length > 2) ? args[2] : "0") );
        rbs.execute();
      }

      Timestamp endTime = new Timestamp(Calendar.getInstance().getTime().getTime());
      long elapsed = (endTime.getTime() - beginTime.getTime());

      System.out.println("Begin Time: " + String.valueOf(beginTime));
      System.out.println("End Time  : " + String.valueOf(endTime));
      System.out.println("Elapsed   : " + DateTimeFormatter.getFormattedTimestamp(elapsed));
    }
    catch( Exception e )
    {
      log.error(e.toString());
    }
    finally
    {
      try{ rbs.cleanUp(); } catch( Exception ee ) {}
      try{ OracleConnectionPool.getInstance().cleanUp(); }catch( Exception e ) {}
    }
    Runtime.getRuntime().exit(0);
  }
}/*@lineinfo:generated-code*/