/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VsXMLRegRemoveCard.java $

  Description:  
  
  VsXMLRegRemoveCard

  VeriSign processor information update message, tells VeriSign merchant
  is no longer accepting a particular card type.  Extends 
  VsXMLRegProcessorInfo, overrides the SupportedTenders generation
  to indicate the removed card type.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 2/03/04 2:20p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import org.jdom.Element;
import com.mes.constants.mesConstants;

public class VsXMLRegRemoveCard extends VsXMLRegProcessorInfo
{
  private int cardType;
  
  public VsXMLRegRemoveCard(long appSeqNum, int cardType)
  {
    super(appSeqNum);
    this.cardType = cardType;
  }
  
  /*
  ** protected void loadCardsAccepted()
  **
  ** Determines which card types a merchant accepts.
  */
  protected void loadCardsAccepted()
  {
  }
  
  /*
  ** protected Element buildSupportedTenders()
  **
  ** Overrides base class to generate the SupportedTenders element contents
  ** that indicate a particular card type is being removed.  Uses cardType
  ** (set in the constructor) to determine which card type is to be removed.
  **
  **  <SupportedTenders>
  **    <SupportedTender>
  **      <TenderType>CreditCard</TenderType>
  **      <TenderVariety>American Express</TenderVariety>
  **      <TransactionType>None</TransactionType>
  **      <ExtendedEntries/>
  **    </SupportedTender>
  **  </SupportedTenders>
  **  <ExtendedEntries/>
  **
  ** RETURNS: SupportedTenders element containing the remove card info.
  */
  protected Element buildSupportedTenders()
  {
    Element supportedTenders = new Element("SupportedTenders");

    supportedTenders.addContent(buildSupportedTender(cardType));
    
    return supportedTenders;
  }
  protected Element buildSupportedTender(int cardType)
  {
    Element supportedTender = new Element("SupportedTender");
    
    String varietyText = "";
    switch(cardType)
    {
      case mesConstants.APP_CT_VISA:
        varietyText = "Visa";
        break;
        
      case mesConstants.APP_CT_MC:
        varietyText = "MasterCard";
        break;
        
      case mesConstants.APP_CT_AMEX:
        varietyText = "American Express";
        break;
        
      case mesConstants.APP_CT_DISCOVER:
        varietyText = "Discover";
        break;
        
      case mesConstants.APP_CT_DINERS_CLUB:
        varietyText = "Diners";
        break;
        
      case mesConstants.APP_CT_JCB:
        varietyText = "JCB";
        break;
        
      case mesConstants.APP_CT_CHECK_AUTH:
        varietyText = "Check";
        break;
        
      default:
        varietyText = "Unsupported Card Type";
        break;
    }
    
    supportedTender.addContent(new Element("TenderType").setText("CreditCard"));
    supportedTender
      .addContent(new Element("TenderVariety").setText(varietyText));
    supportedTender.addContent(new Element("TransactionType").setText("None"));
    supportedTender.addContent(new Element("ExtendedEntries"));

    return supportedTender;
  }

  public String getMessageType()
  {
    return "VsXMLRegRemoveCard(" + cardType + ")";
  }
}
