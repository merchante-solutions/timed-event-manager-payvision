/*@lineinfo:filename=VsXMLRegSender*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VsXMLRegSender.sqlj $

  Description:  
  
    VsXMLRegSender

    Sends XML messages to the Verisign XML Reg system, receives responses.  
    Communications are logged.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 1/28/04 4:18p $
  Version            : $Revision: 1 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.net;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import org.apache.log4j.Logger;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import com.mes.database.SQLJConnectionBase;
import com.mes.tools.AccountKey;

public class VsXMLRegSender extends SQLJConnectionBase
{
	static Logger log = Logger.getLogger(AccountKey.class);

  public static final int         DEFAULT_SOCKET_TIMEOUT      = 0; // i.e.: infinite

  String      jobDescriptor       = null;
  int         socket_timeout      = DEFAULT_SOCKET_TIMEOUT;
  
  public VsXMLRegSender(String newJobDescriptor)
  {
    jobDescriptor = newJobDescriptor;
  }

  public int getSocketTimeout()
  {
    return socket_timeout;
  }

  public void setSocketTimeout(int socket_timeout)
  {
    this.socket_timeout=socket_timeout;
  }
    
  /*
  ** LOGGING
  */
  private void log(XMLMessage message, String details)
  {
    String messageType = "XML message";
    if (message != null)
    {
      messageType = message.getMessageType();
    }
    if (details == null)
    {
      details = "No details";
    }
    
    try
    {
      connect();
      /*@lineinfo:generated-code*//*@lineinfo:82^7*/

//  ************************************************************
//  #sql [Ctx] { insert into xml_log
//          (
//            log_time,
//            job_name,
//            msg_type,
//            details
//          )
//          values
//          (
//            sysdate,
//            :jobDescriptor,
//            :messageType,
//            :details
//          )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into xml_log\n        (\n          log_time,\n          job_name,\n          msg_type,\n          details\n        )\n        values\n        (\n          sysdate,\n           :1 ,\n           :2 ,\n           :3 \n        )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.net.VsXMLRegSender",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,jobDescriptor);
   __sJT_st.setString(2,messageType);
   __sJT_st.setString(3,details);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:98^7*/
    }
    catch (Exception e)
    {
      logEntry("log",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  private void logConnectError(String details)
  {
    log(null,details);
  }

  public String getMessagePostData(XMLMessage msg) throws Exception
	{
		String result = "";
		// simulate html form parameter post
		String prefix = "RequestType=XMLRegRequest&ResponseType=XMLRegResponse&" + "RequestData=";

		// get the message's xml into a buffer
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XMLOutputter fmt = new XMLOutputter();
		fmt.output(msg.getDocument(), baos);

		result = prefix;
		try {
			result += URLEncoder.encode(baos.toString(), StandardCharsets.UTF_8.name());
		} catch (UnsupportedEncodingException e) {
			log.error("UnsupportedEncodingException URLEncoder.encode(baos.toString()", e);
			result += baos.toString();
		}

		// return the xml as part of a parameter string
		return result;
	}
      
  /*
  ** public VSResponse send(XMLMessage outgoingMsg)
  **
  ** Opens a connection to the local Redirector server and sends the
  ** xml message to the message's target.  Creates an xml message
  ** from the response returned.
  **
  ** RETURNS: XMLMessage containing the response returned from the target.
  */
  public XMLMessage send(XMLMessage outgoingMsg)
  {
    XMLMessage returnedMsg = null;
    
    try
    {
      outgoingMsg.setContentType("application/x-www-form-urlencoded");
      
      // connect to redirector
      String host = outgoingMsg.getRedirectorHost();
      int port = outgoingMsg.getRedirectorPort();
      Socket rSocket = new Socket(host,port);
      rSocket.setSoTimeout(socket_timeout);
      PrintWriter rpw = new PrintWriter(rSocket.getOutputStream(),true);
    
      // send the target url and content type
      rpw.println(outgoingMsg.getTargetUrl());
      rpw.println(outgoingMsg.getContentType());
    
      // send the xml message data
      rpw.println(getMessagePostData(outgoingMsg));
      log(outgoingMsg,"Request sent");
    
      // send end_post_data
      rpw.println("end_post_data");
    
      // receive response
      SAXBuilder builder = new SAXBuilder();
      returnedMsg = new XMLMessage(builder.build(rSocket.getInputStream()));
      log(returnedMsg,"Response received");
    }
    catch (Exception e)
    {
      logEntry("send",e.toString());
    }
        
    return returnedMsg;
  }
  
  /*
  ** public static XMLMessage sendMessage(XMLMessage requestMsg)
  **
  ** Instantiates an instance of VsXMLRegSender, sends the request message.
  **
  ** RETURNS: the response message that was returned.
  */  
  public static XMLMessage sendMessage(XMLMessage outgoingMsg, String jobName)
  {
    return (new VsXMLRegSender(jobName)).send(outgoingMsg);
  }

  public static XMLMessage sendMessage(XMLMessage outgoingMsg, String jobName, int socket_timeout)
  {
    VsXMLRegSender sndr = new VsXMLRegSender(jobName);
    sndr.setSocketTimeout(socket_timeout);
    return sndr.send(outgoingMsg);
  }

}/*@lineinfo:generated-code*/