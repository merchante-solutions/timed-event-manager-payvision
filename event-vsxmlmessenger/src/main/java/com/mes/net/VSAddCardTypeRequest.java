/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VSAddCardTypeRequest.sqlj $

  Description:  
  
    VSAddCardTypeRequest

    VeriSign add card type request message.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 7/18/01 4:58p $
  Version            : $Revision: 5 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.util.Iterator;
import org.jdom.Element;
import com.mes.constants.mesConstants;

public class VSAddCardTypeRequest extends VSRequest
{
  private int[] cardTypes = null;
  
  public VSAddCardTypeRequest(long appSeqNum)
  {
    super(appSeqNum);
    insertMessageBody(buildMessageBody());
  }
  
  public VSAddCardTypeRequest(long appSeqNum, int newCardType)
  {
    super(appSeqNum);
    cardTypes = new int[] { newCardType };
    insertMessageBody(buildMessageBody());
  }
  
  public VSAddCardTypeRequest(long appSeqNum, int[] newCardTypes)
  {
    super(appSeqNum);
    cardTypes = newCardTypes;
    insertMessageBody(buildMessageBody());
  }
  
  public void addCardType(int newCardType)
  {
    int oldLength = (cardTypes == null ? 0 : cardTypes.length);
    int[] newTypes = new int[oldLength + 1];
    for (int i = 0; i < oldLength; ++i)
    {
      newTypes[i] = cardTypes[i];
    }
    newTypes[oldLength] = newCardType;
    cardTypes = newTypes;

    try
    {
      doc.
        getRootElement().
          getChild("AddCardTypeRequest").
            getChild("AcceptedCardTypes").
              addContent(buildCard(newCardType));
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() +
        ".addCardType(newCardType = " + newCardType + ")" + e.toString());
      logEntry("addCardType(newCardType = " + newCardType + ")", e.toString());
    }
  }
  
  /*
  ** METHOD public void buildMessageBody()
  **
  ** Builds an AddCardTypeRequest xml message body.
  **
  **  <AddCardTypeRequest>
  **
  **    <Vid>12345</Vid>
  **
  **    <AcceptedCardTypes>
  **      <AcceptedCard>
  **        <Card>American Express</Card>
  **        <CardProcessor>Vital</CardProcessor>
  **      </AcceptedCard>
  **    </AcceptedCardTypes>
  **
  **  </AddCardTypeRequest>
  */
  private Element buildMessageBody()
  {
    Element addRequest = new Element("AddCardTypeRequest");

    addRequest.addContent(buildVid());    
    addRequest.addContent(buildCardTypes());
    
    return addRequest;
  }
  public Element buildCardTypes()
  {
    Element addCardTypes = new Element("AcceptedCardTypes");
    
    if (cardTypes != null)
    {
      for (int i = cardTypes.length - 1; i >= 0; --i)
      {
        addCardTypes.addContent(buildCard(cardTypes[i]));
      }
    }

    return addCardTypes;
  }
  public Element buildCard(int cardType)
  {
    Element cardAccepted   = new Element("AcceptedCard");
    Element cardProcessor  = new Element("CardProcessor").setText("Vital");
    Element card           = new Element("Card");
    
    switch(cardType)
    {
      case mesConstants.APP_CT_VISA:
      default:
        card.setText("Visa");
        break;
        
      case mesConstants.APP_CT_MC:
        card.setText("Mastercard");
        break;
        
      case mesConstants.APP_CT_AMEX:
        card.setText("American Express");
        break;
        
      case mesConstants.APP_CT_DISCOVER:
        card.setText("Discover");
        break;
        
      case mesConstants.APP_CT_DINERS_CLUB:
        card.setText("Diners");
        break;
        
      case mesConstants.APP_CT_JCB:
        card.setText("JCB");
        break;
        
      case mesConstants.APP_CT_CHECK_AUTH:
        card.setText("Check");
        break;
    }
    
    cardAccepted.addContent(card);
    cardAccepted.addContent(cardProcessor);
    
    return cardAccepted;
  }
  
  private Element getCardTypes()
  {
    return (Element)getMessageBody().getChildren().get(1);
  }
  
  public String getDetails()
  {
    StringBuffer details = new StringBuffer("adding card types: ");
    for (Iterator i = getCardTypes().getChildren().iterator(); i.hasNext();)
    {
      details.append(((Element)i.next()).getChild("Card").getText() + " ");
    }
    
    return details.toString() + super.getDetails();
  }
}
