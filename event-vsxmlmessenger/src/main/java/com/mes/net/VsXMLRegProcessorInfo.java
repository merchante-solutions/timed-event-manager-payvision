/*@lineinfo:filename=VsXMLRegProcessorInfo*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VsXMLRegProcessorInfo.sqlj $

  Description:  
  
  VsXMLRegProcessorInfo

  VeriSign processor information update message.
  
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/01/04 9:49a $
  Version            : $Revision: 3 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Vector;
import org.jdom.Element;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class VsXMLRegProcessorInfo extends VsXMLRegRequest
{
  private String    businessName;
  private String    merchNumber;
  private String    sicCode;

  private String    city;
  private String    state;
  private String    zip;
  private String    phone;

  private String    acquirerBin;
  private String    agentBin;
  private String    agentChain;

  private boolean   amexAccepted      = false;
  private boolean   discoverAccepted  = false;
  private boolean   dinersAccepted    = false;
  private boolean   jcbAccepted       = false;
  private boolean   checksAccepted    = false;
  
  private String    vNumber;
  private String    terminalNumber;
  private String    storeNumber;
  private String    timeZone;
  private String    locationNumber;
  
  public VsXMLRegProcessorInfo(long appSeqNum)
  {
    super(appSeqNum);
    loadProcessorInfo();
  }
  
  protected String stripNonNumeric(String data)
  {
    StringBuffer replaceBuf = new StringBuffer();
    
    for (int i = 0; i < data.length(); ++i)
    {
      char ch = data.charAt(i);
      if (Character.isDigit(ch))
      {
        replaceBuf.append(ch);
      }
    }
    
    return replaceBuf.toString();
  }
  
  protected String replaceAmpersands(String data)
  {
    StringBuffer replaceBuf = new StringBuffer();
    
    for (int i = 0; i < data.length(); ++i)
    {
      if (data.charAt(i) == '&')
      {
        replaceBuf.append("and");
      }
      else
      {
        replaceBuf.append(data.charAt(i));
      }
    }
    
    return replaceBuf.toString();
  }
  
  /*
  ** protected void loadMerchantData()
  **
  ** Loads merchant data corresponding with appseqNum.
  */
  protected void loadMerchantData()
  {
    ResultSetIterator it = null;
    
    try
    {
      connect();
      
      /*@lineinfo:generated-code*//*@lineinfo:123^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.merch_business_name,
//                  m.merch_number,
//                  m.sic_code,
//                  addr.address_city,
//                  addr.countrystate_code,
//                  addr.address_zip,
//                  addr.address_phone
//                  
//          from    merchant          m,
//                  address           addr
//                  
//          where   m.app_seq_num = :appSeqNum and
//                  m.app_seq_num = addr.app_seq_num and 
//                  addr.addresstype_code = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.merch_business_name,\n                m.merch_number,\n                m.sic_code,\n                addr.address_city,\n                addr.countrystate_code,\n                addr.address_zip,\n                addr.address_phone\n                \n        from    merchant          m,\n                address           addr\n                \n        where   m.app_seq_num =  :1  and\n                m.app_seq_num = addr.app_seq_num and \n                addr.addresstype_code = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.net.VsXMLRegProcessorInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.net.VsXMLRegProcessorInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:139^7*/
      
      ResultSet rs = it.getResultSet();
      if (rs.next())
      {
        businessName      = rs.getString("merch_business_name");
        merchNumber       = rs.getString("merch_number");
        sicCode           = rs.getString("sic_code");
        city              = rs.getString("address_city");
        state             = rs.getString("countrystate_code");
        zip               = rs.getString("address_zip");
        phone             = rs.getString("address_phone");
      }
      
      acquirerBin = MesDefaults.getString(MesDefaults.DK_VS_ACQUIRER_BIN);
      agentBin    = MesDefaults.getString(MesDefaults.DK_VS_AGENT_BIN);
      agentChain  = MesDefaults.getString(MesDefaults.DK_VS_AGENT_CHAIN);
        
      // remove illegal characters from name and zip
      businessName = replaceAmpersands(businessName);
      if (businessName.length() > 25)
      {
        businessName = businessName.substring(0,25);
      }
      zip = stripNonNumeric(zip);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".loadMerchantData(appSeqNum = " + appSeqNum + "): " +
        e.toString());
      logEntry("loadMerchantData(appSeqNum = " + appSeqNum + ")",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** protected void loadCardsAccepted()
  **
  ** Determines which card types a merchant accepts.
  */
  protected void loadCardsAccepted()
  {
    ResultSetIterator it = null;
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:190^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    cardtype_code
//          from      merchpayoption
//          where     app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    cardtype_code\n        from      merchpayoption\n        where     app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.net.VsXMLRegProcessorInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.net.VsXMLRegProcessorInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:195^7*/
    
      ResultSet rs = it.getResultSet();
      while (rs.next())
      {
        switch(rs.getInt("cardtype_code"))
        {
          case mesConstants.APP_CT_AMEX:
            amexAccepted      = true;
            break;
          
          case mesConstants.APP_CT_DISCOVER:
            discoverAccepted  = true;
            break;
          
          case mesConstants.APP_CT_DINERS_CLUB:
            dinersAccepted    = true;
            break;
          
          case mesConstants.APP_CT_JCB:
            jcbAccepted       = true;
            break;
          
          case mesConstants.APP_CT_CHECK_AUTH:
            checksAccepted    = true;
            break;
        }
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".loadCardsAccepted(appseqnum = " + appSeqNum + "): " +
        e.toString());
      logEntry("loadCardsAccepted(appseqnum = " + appSeqNum + ")",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** protected void loadVNumber()
  **
  ** Loads merchant's VNumber.
  */
  protected void loadVNumber()
  {
    ResultSetIterator it = null;
    
    vNumber = "";
    
    try
    {
      connect();

      /*@lineinfo:generated-code*//*@lineinfo:253^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    vnumber,
//                    terminal_number,
//                    store_number,
//                    time_zone,
//                    location_number
//                    
//          from      merch_vnumber
//          
//          where     app_seq_num = :appSeqNum
//          
//          order by  terminal_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    vnumber,\n                  terminal_number,\n                  store_number,\n                  time_zone,\n                  location_number\n                  \n        from      merch_vnumber\n        \n        where     app_seq_num =  :1 \n        \n        order by  terminal_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.net.VsXMLRegProcessorInfo",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.net.VsXMLRegProcessorInfo",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:266^7*/

      // get the first row (lowest terminal number)
      ResultSet rs = it.getResultSet();
      if(rs.next())
      {
        DecimalFormat df = new DecimalFormat();
        df.applyPattern("0000");
        vNumber         = rs.getString("vnumber");
        terminalNumber  = df.format(rs.getInt("terminal_number"));
        storeNumber     = df.format(rs.getInt("store_number"));
        timeZone        = rs.getString("time_zone");
        df.applyPattern("00000");
        locationNumber  = df.format(rs.getInt("location_number"));
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".loadVNumber(appseqnum = " + appSeqNum + "): " +
        e.toString());
      logEntry("loadVNumber(appseqnum = " + appSeqNum + ")",e.toString());
    }
    finally
    {
      try { it.close(); } catch (Exception e) {}
      cleanUp();
    }
  }
  
  /*
  ** protected void loadProcessorInfo()
  **
  ** Loads all the processor content associated with the appSeqNum.
  */
  protected void loadProcessorInfo()
  {
    try
    {
      connect();
      
      loadMerchantData();
      loadCardsAccepted();
      loadVNumber();
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".loadProcessorInfo(appseqnum = " + appSeqNum + "): " +
        e.toString());
      logEntry("loadProcessorInfo(appseqnum = " + appSeqNum + ")",e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  protected String getRegistrationType()
  {
    return "UpdateProcessor";
  }
  
  /*
  ** protected List getProcessorContent()
  **
  ** Creates the xml content of the processor tag for an "UpdateProcessor"
  ** request message.
  **
  **  <MerchantID>123456789012</MerchantID>
  **  <AcquirerBIN>449279</AcquirerBIN>
  **  <CurrencyCode>840</CurrencyCode>
  **  <SettlementHour>20</SettlementHour>
  **  <SettlementMinute>00</SettlementMinute>
  **  <AVSDenyAddress>0</AVSDenyAddress>
  **  <AVSDenyZip>0</AVSDenyZip>
  **  <StoreNumber>0001</StoreNumber>
  **  <TerminalID>0001</TerminalID>
  **  <CountryCode>840</CountryCode>
  **  <ZipCode>12345</ZipCode>
  **  <TimeZone>708</TimeZone>
  **  <Category>5999</Category>
  **  <MerchantName>Stmpjmpr.com</MerchantName>
  **  <MerchantPhone>650-426-5413</MerchantPhone>
  **  <MerchantState>CA</MerchantState>
  **  <MerchantLocationNumber>00001</MerchantLocationNumber>
  **  <AgentBin>000000</AgentBin>
  **  <AgentChainNumber>000000</AgentChainNumber>
  **  <VNumber>V5305865</VNumber>
  **  <Acquirer>
  **    <Name>Merchant e-Solutions</Name>
  **    <Phone>18882882692</Phone>
  **    <Account>12345</Account>
  **    <Contacts>
  **      <Contact>
  **        <Rank>primary</Rank>
  **        <Name>MeS HELP DESK</Name>
  **        <Address/>
  **        <PhoneNo>18882882692</PhoneNo>
  **        <FaxNo></FaxNo>
  **        <Email>shill@verisign.com</Email>
  **      </Contact>
  **    </Contacts>
  **  </Acquirer>
  **  <SupportedTenders>
  **    <SupportedTender>
  **      <TenderType>CreditCard</TenderType>
  **      <TenderVariety>Visa</TenderVariety>
  **      <TransactionType>Any</TransactionType>
  **      <ExtendedEntries/>
  **    </SupportedTender>
  **    <SupportedTender>
  **      <TenderType>CreditCard</TenderType>
  **      <TenderVariety>MasterCard</TenderVariety>
  **      <TransactionType>Any</TransactionType>
  **      <ExtendedEntries/>
  **    </SupportedTender>
  **  </SupportedTenders>
  **  <ExtendedEntries/>
  **
  ** RETURNS: List of Elements to place in the Processor elements of the 
  **          message.
  **
  */
  protected List getProcessorContent()
  {
    Vector content = new Vector();
    
    content.add(new Element("MerchantID")       .setText(merchNumber    ));
    content.add(new Element("AcquirerBIN")      .setText(acquirerBin    ));
    content.add(new Element("CurrencyCode")     .setText("840"          ));
    content.add(new Element("SettlementHour")   .setText("20"           ));
    content.add(new Element("SettlementMinute") .setText("00"           ));
    content.add(new Element("AVSDenyAddress")   .setText("0"            ));
    content.add(new Element("AVSDenyZip")       .setText("0"            ));
    content.add(new Element("StoreNumber")      .setText(storeNumber    ));
    content.add(new Element("TerminalID")       .setText(terminalNumber ));
    content.add(new Element("CountryCode")      .setText("840"          ));
    content.add(new Element("ZipCode")          .setText(zip            ));
    content.add(new Element("TimeZone")         .setText(timeZone       ));
    content.add(new Element("Category")         .setText(sicCode        ));
    content.add(new Element("MerchantName")     .setText(businessName   ));
    content.add(new Element("MerchantPhone")    .setText(phone          ));
    content.add(new Element("MerchantState")    .setText(state          ));
    content.add(new Element("MerchantLocationNumber")
                                                .setText(locationNumber ));
    content.add(new Element("AgentBin")         .setText(agentBin       ));
    content.add(new Element("AgentChainNumber") .setText(agentChain     ));
    content.add(new Element("VNumber")          .setText(vNumber        ));
    
    content.add(buildAcquirer());
    content.add(buildSupportedTenders());
    
    content.add(new Element("ExtendedEntries"));
    
    return content;
  }
  protected Element buildAcquirer()
  {
    Element acquirer = new Element("Acquirer");
    
    try
    {
      acquirer.addContent(new Element("Name")
        .setText(MesDefaults.getString(MesDefaults.DK_VS_ACQUIRER_NAME)));
      acquirer.addContent(new Element("Phone")
        .setText(MesDefaults.getString(MesDefaults.DK_VS_ACQUIRER_PHONE)));
      acquirer.addContent(new Element("Account").setText("12345"));
      acquirer.addContent(new Element("Status").setText("Approved"));
      acquirer.addContent(buildContacts());
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::buildAcquirer()" 
        + e.toString());
      logEntry("buildAcquirer()", e.toString());
    }
    
    return acquirer;
  }
  protected Element buildContacts()
  {
    Element contacts  = new Element("Contacts");
    Element contact   = new Element("Contact");

    try
    {
      contact.addContent(new Element("Rank").setText("primary"));
      contact.addContent(new Element("Name")
        .setText(MesDefaults.getString(MesDefaults.DK_VS_ACQUIRER_CONTACT)));
      contact.addContent(new Element("Address"));
      contact.addContent(new Element("PhoneNo")
        .setText(MesDefaults.getString(MesDefaults.DK_VS_ACQUIRER_PHONE)));
      contact.addContent(new Element("FaxNo"));
      contact.addContent(new Element("Email"));
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + "::buildContacts()" 
        + e.toString());
      logEntry("buildContacts()", e.toString());
    }
    
    return contacts.addContent(contact);
  }
  protected Element buildSupportedTenders()
  {
    Element supportedTenders = new Element("SupportedTenders");

    supportedTenders.addContent(buildSupportedTender(mesConstants.APP_CT_VISA));
    supportedTenders.addContent(buildSupportedTender(mesConstants.APP_CT_MC));
    if (amexAccepted)
    {
      supportedTenders.addContent(
        buildSupportedTender(mesConstants.APP_CT_AMEX));
    }
    if (discoverAccepted)
    {
      supportedTenders.addContent(
        buildSupportedTender(mesConstants.APP_CT_DISCOVER));
    }
    if (dinersAccepted)
    {
      supportedTenders.addContent(
        buildSupportedTender(mesConstants.APP_CT_DINERS_CLUB));
    }
    if (jcbAccepted)
    {
      supportedTenders.addContent(
        buildSupportedTender(mesConstants.APP_CT_JCB));
    }
    if (checksAccepted)
    {
      supportedTenders.addContent(
        buildSupportedTender(mesConstants.APP_CT_CHECK_AUTH));
    }
    
    return supportedTenders;
  }
  protected Element buildSupportedTender(int cardType)
  {
    Element supportedTender = new Element("SupportedTender");
    
    String varietyText = "";
    switch(cardType)
    {
      case mesConstants.APP_CT_VISA:
      default:
        varietyText = "Visa";
        break;
        
      case mesConstants.APP_CT_MC:
        varietyText = "MasterCard";
        break;
        
      case mesConstants.APP_CT_AMEX:
        varietyText = "American Express";
        break;
        
      case mesConstants.APP_CT_DISCOVER:
        varietyText = "Discover";
        break;
        
      case mesConstants.APP_CT_DINERS_CLUB:
        varietyText = "Diners";
        break;
        
      case mesConstants.APP_CT_JCB:
        varietyText = "JCB";
        break;
        
      case mesConstants.APP_CT_CHECK_AUTH:
        varietyText = "Check";
        break;
    }
    
    supportedTender.addContent(new Element("TenderType").setText("CreditCard"));
    supportedTender
      .addContent(new Element("TenderVariety").setText(varietyText));
    supportedTender.addContent(new Element("TransactionType").setText("Any"));
    supportedTender.addContent(new Element("ExtendedEntries"));

    return supportedTender;
  }
  
  public String getMessageType()
  {
    return "VsXMLRegProcessorInfo";
  }
}/*@lineinfo:generated-code*/