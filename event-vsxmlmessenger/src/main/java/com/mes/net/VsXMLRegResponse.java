/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VsXMLRegResponse.java $

  Description:  
  
    VSResponse

    Contains an xml response message from VeriSign and an accessor to
    get and set the response message result code.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 2/03/04 2:23p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/
package com.mes.net;

import org.jdom.Document;
import org.jdom.Element;

public class VsXMLRegResponse extends XMLMessage
{
  public VsXMLRegResponse(XMLMessage msg)
  {
    super(msg);
  }
  public VsXMLRegResponse(Document doc)
  {
    super(doc);
  }
  
  protected Element getRegistrationResult()
  {
    Element regResults = null;
    try
    {
      regResults  = doc.getRootElement()
                      .getChild("ResponseData")
                      .getChild("RegistrationResults")
                      .getChild("RegistrationResult");
    }
    catch (Exception e)
    {
    }
    return regResults;
  }
  
  protected Element getResult()
  {
    Element result = null;
    try
    {
      result  = getRegistrationResult().getChild("Result");
    }
    catch (Exception e)
    {
    }
    return result;
  }
  
  protected Element getMessage()
  {
    Element result = null;
    try
    {
      result  = getRegistrationResult().getChild("Message");
    }
    catch (Exception e)
    {
    }
    return result;
  }
  
  public int getResultCode()
  {
    int result = -1;
    try
    {
      result = Integer.parseInt(getResult().getText());
    }
    catch (Exception e)
    {
    }
    return result;
  }
  
  public boolean isSuccessful()
  {
    return getResultCode() == 0;
  }
  
  public String getMessageText()
  {
    return getMessage().getText();
  }
}
