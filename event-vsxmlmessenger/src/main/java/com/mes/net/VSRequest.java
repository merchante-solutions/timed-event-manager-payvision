/*@lineinfo:filename=VSRequest*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VSRequest.sqlj $

  Description:  
  
    VSRequest

    Base VeriSign request message.  Contains routines to generate header
    element in VeriSign messages.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 2/04/02 10:38a $
  Version            : $Revision: 14 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.sql.ResultSet;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import com.mes.config.MesDefaults;
import sqlj.runtime.ResultSetIterator;


public class VSRequest extends VSMessage
{
  static Logger log = Logger.getLogger(VSRequest.class);

  protected String verisignId   = "";
  protected long   appSeqNum;
  
  public VSRequest(long appSeqNum)
  {
    this.appSeqNum = appSeqNum;
    loadVerisignId(appSeqNum);
    buildDocument();
  }
  
  /*
  ** protected void loadVerisignId(long appSeqNum)
  **
  ** Loads the VeriSign ID corresponding with the given app seq num.  This
  ** is a common data element found in most VeriSign requests.
  */
  private void loadVerisignId(long appSeqNum)
  {
    try
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      
      connect();
      /*@lineinfo:generated-code*//*@lineinfo:69^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  verisign_id
//          from    vs_linking_table
//          where   app_seq_num = :appSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  verisign_id\n        from    vs_linking_table\n        where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.net.VSRequest",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.net.VSRequest",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:74^7*/
      
      rs = it.getResultSet();
      if (rs.next())
      {
        verisignId  = rs.getString("verisign_id");    // xml vid
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".loadVerisignId(appseqnum = " + appSeqNum + "): " +
        e.toString());
      com.mes.support.SyncLog.LogEntry(this.getClass().getName(), 
        "loadVerisignId(appseqnum = " + appSeqNum + "): " +
        e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  protected String stripNonNumeric(String data)
  {
    StringBuffer replaceBuf = new StringBuffer();
    
    for (int i = 0; i < data.length(); ++i)
    {
      char ch = data.charAt(i);
      if (Character.isDigit(ch))
      {
        replaceBuf.append(ch);
      }
    }
    
    return replaceBuf.toString();
  }
  
  protected String replaceAmpersands(String data)
  {
    StringBuffer replaceBuf = new StringBuffer();
    
    for (int i = 0; i < data.length(); ++i)
    {
      if (data.charAt(i) == '&')
      {
        replaceBuf.append("and");
      }
      else
      {
        replaceBuf.append(data.charAt(i));
      }
    }
    
    return replaceBuf.toString();
  }
  
  /*
  ** protected void buildDocument()
  **
  ** Generates a standard Verisign message.
  **
  **  <XMLReg>
  **
  **    <Header>
  **      <From>
  **        <Name>Joe Blow</Name>
  **        <Password>password</Password>
  **        <Partner>partern Id</Partner>
  **      </From>
  **    </Header>
  **
  **    <MESSAGEBODY>
  **      ..
  **      ..
  **      ..
  **    </MESSAGEBODY>
  **
  **  </XMLReg>
  */
  protected void buildDocument()
  {
    doc = new Document(buildMessage());
  }
  protected Element buildMessage()
  {
    Element message = new Element("XMLReg");
    
    message.addContent(buildHeader());
    
    return message;
  }
  protected Element buildHeader()
  {
    Element header = new Element("Header");
    
    header.addContent(buildFrom());
    
    return header;
  }
  private Element buildFrom()
  {
    Element from = new Element("From");
    
    try
    {
      from.addContent(new Element("Name").
        setText(MesDefaults.getString(MesDefaults.DK_VS_XML_HDR_NAME)));
      from.addContent(new Element("Password").
        setText(MesDefaults.getString(MesDefaults.DK_VS_XML_HDR_PASSWORD)));
      from.addContent(new Element("Partner").
        setText(MesDefaults.getString(MesDefaults.DK_VS_XML_HDR_PARTNER)));
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".buildFrom, " + e.toString());
      logEntry("buildFrom",e.toString());
    }
    
    return from;
  }
  protected Element buildVid()
  {
    return new Element("Vid").setText(verisignId);
  }
  
  protected void insertMessageBody(Element messageBody)
  {
    doc.getRootElement().addContent(messageBody);
  }
  
  public long getAppSeqNum()
  {
    return appSeqNum;
  }
  
  public String getDetails()
  {
    return "appSeqNum = " + appSeqNum;
  }
}/*@lineinfo:generated-code*/