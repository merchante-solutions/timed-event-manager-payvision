/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VSResponse.sqlj $

  Description:  
  
    VSResponse

    Contains an xml response message from VeriSign and an accessor to
    get and set the response message result code.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 7/18/01 4:58p $
  Version            : $Revision: 7 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import org.jdom.Document;
import org.jdom.Element;

public class VSResponse extends VSMessage
{
  public VSResponse(XMLMessage msg)
  {
    super(msg);
  }
  public VSResponse(Document newDoc)
  {
    super(newDoc);
  }
  
  private Element getResultElement()
  {
    Element resultElement = null;
    
    try
    {
      resultElement = getMessageBody().getChild("Result");
    }
    catch (Exception e)
    {
    }
    
    return resultElement;
  }
  
  public void setResult(int newResult)
  {
    try
    {
      getResultElement().setText(Integer.toString(newResult));
    }
    catch (Exception e)
    {
    }
  }
  
  public int getResult()
  {
    int result = 0;
    
    try
    {
      result = Integer.parseInt(getResultElement().getText());
    }
    catch (Exception e)
    {
    }
    
    return result;
  }
  
  public void setMsg(String newMsg)
  {
    try
    {
      getMsgElement().setText(newMsg);
    }
    catch (Exception e)
    {
    }
  }
  
  private Element getMsgElement()
  {
    Element msgElement = null;
    
    try
    {
      msgElement = getMessageBody().getChild("Msg");
    }
    catch (Exception e)
    {
      msgElement = new Element("Msg").setText("No message body");
    }
    
    if (msgElement == null)
    {
      msgElement = new Element("Msg").setText("No Msg element found");
    }
    
    return msgElement;
  }
  
  public String getMsg()
  {
    return getMsgElement().getText();
  }
  
  public String getDetails()
  {
    return "result code: " + getResult() + ", result msg: " + getMsg();
  }
}
