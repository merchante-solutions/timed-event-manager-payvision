/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VsXMLRegStatusUpdate.java $

  Description:  
  
  VsXMLRegStatusUpdate

  VeriSign Status update request.
  
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 2/03/04 2:20p $
  Version            : $Revision: 2 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.util.List;
import java.util.Vector;
import org.jdom.Element;

public class VsXMLRegStatusUpdate extends VsXMLRegRequest
{
  private String statusMsg = null;
  
  public VsXMLRegStatusUpdate(long appSeqNum, String statusMsg)
  {
    super(appSeqNum);
    this.statusMsg = statusMsg;
  }
  
  protected String getRegistrationType()
  {
    return "UpdateAcquirerStatus";
  }
  
  /*
  ** protected List getProcessorContent()
  **
  **    <Acquirer>
  **      <Status>...statusMsg...</Status>
  **    </Acquirer>
  **
  ** RETURNS: "Processor" element contents for a status update message.
  **
  */
  protected List getProcessorContent()
  {
    Vector content = new Vector();
    
    Element acquirer  = new Element("Acquirer");
    Element status    = new Element("Status").setText(statusMsg);
    
    acquirer.addContent(status);
    content.add(acquirer);
    
    return content;    
  }

  public String getMessageType()
  {
    return "VsXMLRegStatusUpdate";
  }
}
