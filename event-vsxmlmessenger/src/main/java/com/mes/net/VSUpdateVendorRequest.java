/*@lineinfo:filename=VSUpdateVendorRequest*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VSUpdateVendorRequest.sqlj $

  Description:  
  
    VSUpdateVendorRequest
    
    Builds an xml message to send to VeriSign to set up a new merchant.

    Generic base of all VeriSign xml messages.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 3/19/02 10:44a $
  Version            : $Revision: 12 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.sql.ResultSet;
import java.text.DecimalFormat;
import org.jdom.Element;
import com.mes.config.MesDefaults;
import com.mes.constants.mesConstants;
import sqlj.runtime.ResultSetIterator;

public class VSUpdateVendorRequest extends VSRequest
{
  private long      appSeqNum;
  
  private String    businessName;
  private String    merchNumber;
  private String    sicCode;
  private String    acquirerBIN;
  private String    agentBIN;
  private String    agentChain;

  private String    city;
  private String    state;
  private String    zip;
  private String    phone;

  private String    contactFirstName;
  private String    contactLastName;
  private String    contactPhone;

  private String    posParam;

  private boolean   amexAccepted      = false;
  private boolean   discoverAccepted  = false;
  private boolean   dinersAccepted    = false;
  private boolean   jcbAccepted       = false;
  private boolean   checksAccepted    = false;
  
  private String    vNumber;
  private String    terminalNumber;
  private String    storeNumber;
  private String    timezone;
  private String    locationNumber;
  
  /*
  ** CONSTRUCTOR public VSUpdateVendorRequest(long getAppSeqNum)
  **
  ** Given an app seq num this VSMessage based class loads an xml document
  ** with the data for a vendor update request.
  */
  public VSUpdateVendorRequest(long getAppSeqNum)
  {
    super(getAppSeqNum);
    
    try
    {
      connect();
      loadMerchantData(getAppSeqNum);
      loadCardsAccepted(getAppSeqNum);
      loadVNumber(getAppSeqNum);
      insertMessageBody(buildMessageBody());
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".VSUpdateVendorRequest(appseqnum = " + getAppSeqNum + "): " +
        e.toString());
      logEntry("VSUpdateVendorRequest(appseqnum = " + getAppSeqNum + ")", e.toString());
    }
    finally
    {
      cleanUp();
    }
  }
  
  /*
  ** METHOD public void loadMerchantData(long getAppSeqNum)
  **
  ** Loads merchant data corresponding with getAppseqNum.
  */
  public void loadMerchantData(long getAppSeqNum)
  {
    try
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      
      /*@lineinfo:generated-code*//*@lineinfo:120^7*/

//  ************************************************************
//  #sql [Ctx] it = { select  m.app_seq_num,
//                  m.merch_business_name,
//                  m.merch_number,
//                  m.sic_code,
//                  addr.address_city,
//                  addr.countrystate_code,
//                  addr.address_zip,
//                  addr.address_phone,
//                  mc.merchcont_prim_first_name,
//                  mc.merchcont_prim_last_name,
//                  mc.merchcont_prim_phone,
//                  mp.pos_param,
//                  vs.verisign_id,
//                  m.acquirer_bin,
//                  m.agent_bin,
//                  m.agent_chain
//          from    merchant          m,
//                  address           addr,
//                  merchcontact      mc,
//                  merch_pos         mp,
//                  vs_linking_table  vs
//          where   m.app_seq_num = :getAppSeqNum and
//                  m.app_seq_num = mc.app_seq_num and  
//                  m.app_seq_num = mp.app_seq_num and
//                  m.app_seq_num = vs.app_seq_num and
//                  m.app_seq_num = addr.app_seq_num and 
//                  addr.addresstype_code = 1
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select  m.app_seq_num,\n                m.merch_business_name,\n                m.merch_number,\n                m.sic_code,\n                addr.address_city,\n                addr.countrystate_code,\n                addr.address_zip,\n                addr.address_phone,\n                mc.merchcont_prim_first_name,\n                mc.merchcont_prim_last_name,\n                mc.merchcont_prim_phone,\n                mp.pos_param,\n                vs.verisign_id,\n                m.acquirer_bin,\n                m.agent_bin,\n                m.agent_chain\n        from    merchant          m,\n                address           addr,\n                merchcontact      mc,\n                merch_pos         mp,\n                vs_linking_table  vs\n        where   m.app_seq_num =  :1  and\n                m.app_seq_num = mc.app_seq_num and  \n                m.app_seq_num = mp.app_seq_num and\n                m.app_seq_num = vs.app_seq_num and\n                m.app_seq_num = addr.app_seq_num and \n                addr.addresstype_code = 1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.net.VSUpdateVendorRequest",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,getAppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"0com.mes.net.VSUpdateVendorRequest",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:149^7*/
      
      rs = it.getResultSet();
      if (rs.next())
      {
        appSeqNum         = rs.getLong  ("app_seq_num");
        businessName      = rs.getString("merch_business_name");
        merchNumber       = rs.getString("merch_number");
        sicCode           = rs.getString("sic_code");
        city              = rs.getString("address_city");
        state             = rs.getString("countrystate_code");
        zip               = rs.getString("address_zip");
        phone             = rs.getString("address_phone");
        contactFirstName  = rs.getString("merchcont_prim_first_name");
        contactLastName   = rs.getString("merchcont_prim_last_name");
        contactPhone      = rs.getString("merchcont_prim_phone");
        posParam          = rs.getString("pos_param");
        verisignId        = rs.getString("verisign_id");
        acquirerBIN       = // rs.getString(13);
          MesDefaults.getString(MesDefaults.DK_VS_ACQUIRER_BIN);
        agentBIN          = // rs.getString(14);
          MesDefaults.getString(MesDefaults.DK_VS_AGENT_BIN);
        agentChain        = // rs.getString(15);
          MesDefaults.getString(MesDefaults.DK_VS_AGENT_CHAIN);
      }
      
      businessName = replaceAmpersands(businessName);
      if (businessName.length() > 25)
      {
        businessName = businessName.substring(0,25);
      }
      zip = stripNonNumeric(zip);
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".loadMerchantData(appseqnum = " + getAppSeqNum + "): " +
        e.toString());
      logEntry("loadMerchantData(appseqnum = " + getAppSeqNum + ")",e.toString());
    }
  }
  
  /*
  ** METHOD public void loadCardsAccepted(long getAppSeqNum)
  **
  ** Determines which card types a merchant accepts.
  */
  public void loadCardsAccepted(long getAppSeqNum)
  {
    try
    {
      ResultSetIterator it = null;
      ResultSet         rs = null;
      
      /*@lineinfo:generated-code*//*@lineinfo:203^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    cardtype_code
//          from      merchpayoption
//          where     app_seq_num = :getAppSeqNum
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    cardtype_code\n        from      merchpayoption\n        where     app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.net.VSUpdateVendorRequest",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,getAppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"1com.mes.net.VSUpdateVendorRequest",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:208^7*/
    
      rs = it.getResultSet();
      while (rs.next())
      {
        switch(rs.getInt("cardtype_code"))
        {
          case mesConstants.APP_CT_AMEX:
            amexAccepted      = true;
            break;
          
          case mesConstants.APP_CT_DISCOVER:
            discoverAccepted  = true;
            break;
          
          case mesConstants.APP_CT_DINERS_CLUB:
            dinersAccepted    = true;
            break;
          
          case mesConstants.APP_CT_JCB:
            jcbAccepted       = true;
            break;
          
          case mesConstants.APP_CT_CHECK_AUTH:
            checksAccepted    = true;
            break;
        }
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".loadCardsAccepted(appseqnum = " + getAppSeqNum + "): " +
        e.toString());
      logEntry("loadCardsAccepted(appseqnum = " + getAppSeqNum + ")",e.toString());
    }
  }
  
  /*
  ** METHOD public void loadVNumber(long getAppSeqNum)
  **
  ** Loads merchant's VNumber.
  */
  public void loadVNumber(long getAppSeqNum)
  {
    ResultSetIterator it              = null;
    ResultSet         rs              = null;
    
    vNumber = "";
    
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:260^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    vnumber,
//                    terminal_number,
//                    store_number,
//                    time_zone,
//                    location_number
//                    
//          from      merch_vnumber
//          
//          where     app_seq_num = :getAppSeqNum
//          
//          order by  terminal_number
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    vnumber,\n                  terminal_number,\n                  store_number,\n                  time_zone,\n                  location_number\n                  \n        from      merch_vnumber\n        \n        where     app_seq_num =  :1 \n        \n        order by  terminal_number";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.net.VSUpdateVendorRequest",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,getAppSeqNum);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"2com.mes.net.VSUpdateVendorRequest",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:273^7*/

      // get the first row (lowest terminal number)
      rs = it.getResultSet();
      if(rs.next())
      {
        DecimalFormat df = new DecimalFormat();
        df.applyPattern("0000");
        vNumber         = rs.getString("vnumber");
        terminalNumber  = df.format(rs.getInt("terminal_number"));
        storeNumber     = df.format(rs.getInt("store_number"));
        timezone        = rs.getString("time_zone");
        df.applyPattern("00000");
        locationNumber  = df.format(rs.getInt("location_number"));
      }
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
        ".loadVNumber(appseqnum = " + getAppSeqNum + "): " +
        e.toString());
      logEntry("loadVNumber(appseqnum = " + getAppSeqNum + ")",e.toString());
    }
  }
  
  /*
  ** METHOD public void buildDocument()
  **
  ** Builds an UpdateVendorRequest xml message body.
  **
  **  <UpdateVendorRequest>
  **
  **    <Vid>V12345678</Vid>
  **
  **    <AcceptedCardTypes>
  **      <AcceptedCard>
  **        <Card>Visa</Card>
  **        <CardProcessor>Vital</CardProcessor>
  **      </AcceptedCard>
  **      <AcceptedCard>
  **        <Card>Mastercard</Card>
  **        <CardProcessor>Vital</CardProcessor>
  **      </AcceptedCard>
  **      <AcceptedCard>
  **        <Card>American Express</Card>
  **        <CardProcessor>Vital</CardProcessor>
  **      </AcceptedCard>
  **      <AcceptedCard>
  **        <Card>Discover</Card>
  **        <CardProcessor>Vital</CardProcessor>
  **      </AcceptedCard>
  **    </AcceptedCardTypes>
  **
  **    <SelectedProcessors>
  **      <Processor>
  **        <Vital>
  **          <VitaAcquirerBIN></VitaAcquirerBIN>
  **          <VitaMerchantID></VitaMerchantID>
  **          <VitaStoreNumber></VitaStoreNumber>
  **          <VitaTerminalNumber></VitaTerminalNumber>
  **          <VitaZipCode></VitaZipCode>
  **          <VitaTimeZone></VitaTimeZone>
  **          <VitaCategory></VitaCategory>
  **          <VitaMerchantName></VitaMerchantName>
  **          <VitaMerchantPhone></VitaMerchantPhone>
  **          <VitaMerchantState></VitaMerchantState>
  **          <VitaAgentBin></VitaAgentBin>
  **          <VitaAgentChainNumber></VitaAgentChainNumber>
  **          <VitaMerchantLocationNumber></VitaMerchantLocationNumber>
  **          <VitaVNumber></VitaVNumber>
  **        </Vital>
  **        <AcquirerInformation>
  **          <Acquirer>Merchant e-Solutions</Acquirer>
  **          <AcquirerContactName></AcquirerContactName>
  **          <AcquirerContactPhone></AcquirerContactPhone>
  **          <MerchantAccountNumber></MerchantAccountNumber>
  **        </AcquirerInformation>
  **      </Processor>
  **    </SelectedProcessors>
  **
  **  </UpdateVendorRequest>
  */
  public Element buildMessageBody()
  {
    Element vendorRequest = new Element("UpdateVendorRequest");
    
    vendorRequest.addContent(new Element("Vid").setText(verisignId));
    vendorRequest.addContent(buildCardTypes());
    vendorRequest.addContent(buildProcessors());
    
    return vendorRequest;
  }
  public Element buildCardTypes()
  {
    Element cardTypes = new Element("AcceptedCardTypes");
    
    cardTypes.addContent(buildCard(mesConstants.APP_CT_VISA));
    cardTypes.addContent(buildCard(mesConstants.APP_CT_MC));
    if (amexAccepted)
    {
      cardTypes.addContent(buildCard(mesConstants.APP_CT_AMEX));
    }
    if (discoverAccepted)
    {
      cardTypes.addContent(buildCard(mesConstants.APP_CT_DISCOVER));
    }
    if (dinersAccepted)
    {
      cardTypes.addContent(buildCard(mesConstants.APP_CT_DINERS_CLUB));
    }
    if (jcbAccepted)
    {
      cardTypes.addContent(buildCard(mesConstants.APP_CT_JCB));
    }
    if (checksAccepted)
    {
      cardTypes.addContent(buildCard(mesConstants.APP_CT_CHECK_AUTH));
    }

    return cardTypes;
  }
  public Element buildCard(int cardType)
  {
    Element cardAccepted   = new Element("AcceptedCard");
    Element cardProcessor  = new Element("CardProcessor").setText("Vital");
    Element card           = new Element("Card");
    
    switch(cardType)
    {
      case mesConstants.APP_CT_VISA:
      default:
        card.setText("Visa");
        break;
        
      case mesConstants.APP_CT_MC:
        card.setText("Mastercard");
        break;
        
      case mesConstants.APP_CT_AMEX:
        card.setText("American Express");
        break;
        
      case mesConstants.APP_CT_DISCOVER:
        card.setText("Discover");
        break;
        
      case mesConstants.APP_CT_DINERS_CLUB:
        card.setText("Diners");
        break;
        
      case mesConstants.APP_CT_JCB:
        card.setText("JCB");
        break;
        
      case mesConstants.APP_CT_CHECK_AUTH:
        card.setText("Check");
        break;
    }
    
    cardAccepted.addContent(card);
    cardAccepted.addContent(cardProcessor);
    
    return cardAccepted;
  }
  public Element buildProcessors()
  {
    Element processors = new Element("SelectedProcessors");
    
    processors.addContent(buildProcessor());
    
    return processors;
  }
  public Element buildProcessor()
  {
    Element processor = new Element("Processor");
    
    processor.addContent(buildVital());
    processor.addContent(buildAcquirerInformation());
    
    return processor;
  }
  public Element buildVital()
  {
    Element       vital = new Element("Vital");

    vital.addContent(new Element("VitaAcquirerBIN").setText(acquirerBIN));
    vital.addContent(new Element("VitaMerchantID").setText(merchNumber));
    vital.addContent(new Element("VitaStoreNumber").setText(storeNumber));
    vital.addContent(new Element("VitaTerminalNumber").setText(terminalNumber));
    vital.addContent(new Element("VitaZipCode").setText(zip));
    vital.addContent(new Element("VitaTimeZone").setText(timezone));
    vital.addContent(new Element("VitaCategory").setText(sicCode));
    vital.addContent(new Element("VitaMerchantName").setText(businessName));
    vital.addContent(new Element("VitaMerchantPhone").setText(phone));
    vital.addContent(new Element("VitaMerchantState").setText(state));
    vital.addContent(new Element("VitaAgentBin").setText(agentBIN));
    vital.addContent(new Element("VitaAgentChainNumber").setText(agentChain));
    vital.addContent(new Element("VitaMerchantLocationNumber").setText(locationNumber));
    vital.addContent(new Element("VitaVNumber").setText(vNumber));
    vital.addContent(new Element("VitaCurrencyCode").setText("840"));
    vital.addContent(new Element("VitaCountryCode").setText("840"));
    
    return vital;
  }
  public Element buildAcquirerInformation()
  {
    Element acquirer = new Element("AcquirerInformation");
    
    try
    {
      acquirer.addContent(new Element("Acquirer").setText(MesDefaults.getString(MesDefaults.DK_VS_ACQUIRER_NAME)));
      acquirer.addContent(new Element("AcquirerContactName").setText(MesDefaults.getString(MesDefaults.DK_VS_ACQUIRER_CONTACT)));
      acquirer.addContent(new Element("AcquirerContactPhone").setText(MesDefaults.getString(MesDefaults.DK_VS_ACQUIRER_PHONE)));
      acquirer.addContent(new Element("MerchantAccountNumber").setText(merchNumber));
    }
    catch (Exception e)
    {
      System.out.println(this.getClass().getName() + 
                         "::buildAcquirerInformation()" +
                         e.toString());
      logEntry("buildAcquirerInformation()", e.toString());
    }
    
    return acquirer;
  }
}/*@lineinfo:generated-code*/