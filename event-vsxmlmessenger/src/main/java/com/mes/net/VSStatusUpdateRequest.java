/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VSStatusUpdateRequest.sqlj $

  Description:  
  
    VSStatusUpdate

    VeriSign status update message.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 7/18/01 4:58p $
  Version            : $Revision: 6 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import org.jdom.Element;

public class VSStatusUpdateRequest extends VSRequest
{
  public static final int VS_STAT_APP_UNKNOWN     = 0;
  public static final int VS_STAT_APP_DONE        = 1;
  public static final int VS_STAT_APP_TERMINATED  = 2;
  public static final int VS_STAT_APP_CANCELLED   = 3;
  
  private int status = VS_STAT_APP_UNKNOWN;
  
  public VSStatusUpdateRequest(long appSeqNum, int newStatus)
  {
    super(appSeqNum);
    
    status = newStatus;
    insertMessageBody(buildMessageBody());
  }
  
  /*
  ** METHOD public void buildMessageBody()
  **
  ** Builds an StatusUpdateRequest xml message body.
  **
  **  <StatusUpdateRequest>
  **    <Vid>12345</Vid>
  **    <Status>1</Status>
  **  </StatusUpdateRequest>
  */
  private Element buildMessageBody()
  {
    Element statusUpdate = new Element("StatusUpdateRequest");
    
    statusUpdate.addContent(new Element("Vid").setText(verisignId));
    statusUpdate.addContent(new Element("Status").setText(Integer.toString(status)));
    
    return statusUpdate;
  }
  
  public String getDetails()
  {
    return "status: " + getMessageBody().getChild("Status").getText() 
            + ", " + super.getDetails();
  }
}
