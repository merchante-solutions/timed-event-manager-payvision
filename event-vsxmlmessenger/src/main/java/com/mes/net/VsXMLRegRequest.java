/*@lineinfo:filename=VsXMLRegRequest*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VsXMLRegRequest.sqlj $

  Description:  
  
  VsXMLRegRequest

  Base Verisign XML Reg request message.  This is an abstract class that
  generates the base framework of a request message.
  
  Last Modified By   : $Author: Jfirman $
  Last Modified Date : $Date: 3/11/04 10:42a $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import java.util.Iterator;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import com.mes.config.MesDefaults;

public abstract class VsXMLRegRequest extends XMLMessage
{
  protected String  vendorId      = null;
  protected String  partnerId     = null;
  protected long    appSeqNum     = -1L;
  
  public VsXMLRegRequest()
  {
  }
  public VsXMLRegRequest(Document newDoc)
  {
    super(newDoc);
  }
  public VsXMLRegRequest(VsXMLRegRequest thatMsg)
  {
    super(thatMsg);
    
    try
    {
      setAppSeqNum(thatMsg.getAppSeqNum());
    }
    catch (Exception e)
    {
    }
  }
  public VsXMLRegRequest(long appSeqNum)
  {
    try
    {
      setAppSeqNum(appSeqNum);
    }
    catch(Exception e)
    {
    }
  }
  
  /*
  ** public String getVendorId() throws Exception
  **
  ** Throws exception if appSeqNum has not been set.  If vendorId has not
  ** already been loaded it is loaded.
  **
  ** RETURNS: the vendorId associated with the appSeqNum.
  */
  public String getVendorId() throws Exception
  {
    if (appSeqNum == -1L)
    {
      throw new Exception("Vendor ID cannot be loaded without appSeqNum");
    }
    
    if (vendorId == null)
    {
      try
      {
        connect();
        
        // determine whether this is old or new
        int regCnt = 0;
        
        /*@lineinfo:generated-code*//*@lineinfo:97^9*/

//  ************************************************************
//  #sql [Ctx] { select  count(vendor_id)
//            
//            from    vapp_xml_post
//            where   app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(vendor_id)\n           \n          from    vapp_xml_post\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.net.VsXMLRegRequest",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   regCnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:103^9*/
        
        if(regCnt > 0)
        {
          /*@lineinfo:generated-code*//*@lineinfo:107^11*/

//  ************************************************************
//  #sql [Ctx] { select  vendor_id,
//                      partner_id
//              
//              from    vapp_xml_post
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  vendor_id,\n                    partner_id\n             \n            from    vapp_xml_post\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.net.VsXMLRegRequest",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vendorId = (String)__sJT_rs.getString(1);
   partnerId = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:115^11*/
        }
        else
        {
          /*@lineinfo:generated-code*//*@lineinfo:119^11*/

//  ************************************************************
//  #sql [Ctx] { select  vendor_id,
//                      partner_id
//              
//              from    vapp_xml_post_iso
//              where   app_seq_num = :appSeqNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  vendor_id,\n                    partner_id\n             \n            from    vapp_xml_post_iso\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.net.VsXMLRegRequest",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   vendorId = (String)__sJT_rs.getString(1);
   partnerId = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:127^11*/
        }
      }
      catch(Exception e)
      {
        logEntry("getVendorId(" + appSeqNum + ")", e.toString());
      }
      finally
      {
        cleanUp();
      }
    }
    return vendorId;
  }
  public void setVendorId(String vendorId)
  {
    this.vendorId = vendorId;
  }

  public long getAppSeqNum()
  {
    return appSeqNum;
  }
  public void setAppSeqNum(long appSeqNum)
    throws Exception
  {
    this.appSeqNum = appSeqNum;
    
    // set vendor id and partner id
    getVendorId();
  }
  
  /*
  ** protected void buildDocument()
  **
  ** Generates the foundation structure of a VeriSign XMLReg message.
  **
  ** There are two basic flavors of requests based on this foundation.  The
  ** first has the Registration attribute type set to "UpdateProcessor"
  ** and is used to send account setup information and to add and remove
  ** card acceptance.  The second is "UpdateAcquirerStatus" and is used
  ** to send a simple status update (app complete, app canceled, app
  ** terminated).
  **
  **  <PaymentServicesMerchantRequest>
  **
  **    <RequestAuth>
  **
  **      <UserPass>
  **        <UserName />
  **        <UserDomain />
  **        <Password />
  **      </UserPass>
  **
  **    </RequestAuth>
  **
  **    <RequestData>
  **
  **      <Reseller>VeriSign</Reseller>
  **      <Registrations>
  **        <Registration Id="app_seq_num" type="child class specified">
  **          <Merchant>
  **            <VendorId>118510</VendorId>
  **            <ResellerRefId>VeriSign</ResellerRefId>
  **            <ExtendedEntries/>
  **          </Merchant>
  **          <Processors>
  **            <Processor>
  **              <ProcessorId>VITA</ProcessorId>
  **
  **              ... child process defined ...
  **
  **            </Processor>
  **          </Processors>
  **        </Registration>
  **      </Registrations>
  **
  **    </RequestData>
  **
  **  </PaymentServicesMerchantRequest>
  ** 
  */
  protected void buildDocument()
  {
    doc = new Document(buildMessage());
  }
  protected Element buildMessage()
  {
    Element message = new Element("PaymentServicesMerchantRequest");
    
    message.setAttribute("Timeout","60000");
      
    message.addContent(buildRequestData());
    message.addContent(buildRequestAuth());
    
    return message;
  }
  protected Element buildRequestAuth()
  {
    Element auth = new Element("RequestAuth");
    auth.addContent(buildUserPass());
    
    return auth;
  }
  protected Element buildUserPass()
  {
    Element userPass = new Element("UserPass");
    
    try
    {
      userPass.addContent(new Element("UserName").setText(MesDefaults.getString(MesDefaults.DK_VS_XML_REG_USER_NAME)));
      userPass.addContent(new Element("UserDomain").setText(MesDefaults.getString(MesDefaults.DK_VS_XML_REG_USER_DOMAIN)));
      userPass.addContent(new Element("Password").setText(MesDefaults.getString(MesDefaults.DK_VS_XML_REG_PASSWORD)));
    }
    catch(Exception e)
    {
      logEntry("buildUserPass()", e.toString());
    }
    
    return userPass;
  }
  protected Element buildRequestData()
  {
    Element requestData = new Element("RequestData");
    
    try
    {
      if(partnerId == null || partnerId.equals(""))
      {
        requestData.addContent(new Element("Reseller")
          .setText(MesDefaults.getString(MesDefaults.DK_VS_XML_REG_RESELLER)));
      }
      else
      {
        requestData.addContent(new Element("Reseller")
          .setText(partnerId));
      }
      requestData.addContent(buildRegistrations());
    }
    catch(Exception e)
    {
      logEntry("buildRequestData()", e.toString());
    }
    
    return requestData;
  }
  protected Element buildRegistrations()
  {
    Element registrations = new Element("Registrations");
    
    try
    {
      registrations.addContent(buildRegistration());
    }
    catch(Exception e)
    {
      logEntry("buildRegistrations()", e.toString());
    }
    
    return registrations;
  }
  protected Element buildRegistration()
  {
    Element registration = new Element("Registration");
    
    try
    {
      registration.setAttribute("Id",   Long.toString(appSeqNum));
      registration.setAttribute("Type", getRegistrationType());
      
      registration.addContent(buildMerchant());
      registration.addContent(buildProcessors());
    }
    catch(Exception e)
    {
      logEntry("buildRegistration(" + appSeqNum + ")", e.toString());
    }
    
    return registration;
  }
  protected Element buildMerchant()
  {
    Element merchant = new Element("Merchant");
    
    try
    {
      merchant.addContent(new Element("VendorId").setText(vendorId));
      
      if(partnerId == null || partnerId.equals(""))
      {
        merchant.addContent(new Element("ResellerRefId")
          .setText(MesDefaults.getString(MesDefaults.DK_VS_XML_REG_RESELLER)));
      }
      else
      {
        merchant.addContent(new Element("ResellerRefId")
          .setText(partnerId));
      }
      merchant.addContent(new Element("ExtendedEntries"));
    }
    catch(Exception e)
    {
      logEntry("buildMerchant()", e.toString());
    }
    
    return merchant;
  }
  protected Element buildProcessors()
  {
    Element processors = new Element("Processors");

    try
    {
      Element processor = new Element("Processor");
      processor = processor.setAttribute("Status", "Live");

      List processorContent = getProcessorContent();
      
      processor.addContent(new Element("ProcessorId").setText("VITA"));
      for (Iterator i = processorContent.iterator(); i.hasNext();)
      {
        Element el = (Element)i.next();
        processor.addContent(el);
      }
      
      processors.addContent(processor);
    }
    catch(Exception e)
    {
      logEntry("buildProcessors()", e.toString());
    }

    return processors;
  }
  
  /*
  ** Child classes must define these methods.  They determine the registration
  ** type and the corresponding registration content that goes with that type.
  ** 
  ** protected String getRegistrationType();
  **
  **   Should return "UpdateProcessor" or "UpdateAcquirerStatus"
  **
  ** protected List getProcessorContent();
  **
  **   Gets the processor content as a List of Elements.  The content will
  **   vary based on the message type.
  */
  
  protected abstract String getRegistrationType();
  protected abstract List getProcessorContent();
  
  /*
  ** Accessors
  */
  
  protected Element getRegistrations()
  {
    Element registrations = null;
    
    try
    {
      registrations = doc.getRootElement()
                        .getChild("RequestData")
                        .getChild("Registrations");
    }
    catch(Exception e)
    {
      logEntry("getRegistrations()", e.toString());
    }
    
    return registrations;
  }
}/*@lineinfo:generated-code*/