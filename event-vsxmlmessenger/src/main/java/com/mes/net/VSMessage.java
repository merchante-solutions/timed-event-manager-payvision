/*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/net/VSMessage.sqlj $

  Description:  
  
    VSMessage

    Base message for VeriSign xml messages.  Contains some standard
    logging routines.
    
  Last Modified By   : $Author: Tbaker $
  Last Modified Date : $Date: 7/18/01 4:58p $
  Version            : $Revision: 4 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.net;

import org.jdom.Document;
import org.jdom.Element;

public class VSMessage extends XMLMessage
{
  /*
  ** CONSTRUCTORS
  */
  public VSMessage()
  {
  }
  public VSMessage(Document newDoc)
  {
    super(newDoc);
  }
  public VSMessage(XMLMessage thatMsg)
  {
    super(thatMsg);
  }

  public Element getMessageBody()
  {
    Element messageBody = null;
    try
    {
      messageBody = (Element)(doc.getRootElement().getChildren().get(1));
    }
    catch (Exception e)
    {
    }
    
    return messageBody;
  }
  public String getMessageType()
  {
    return getMessageBody().getName();
  }

  public String toString()
  {
    return getMessageType();
  }

  public String getDetails()
  {
    return "No details";
  }
}