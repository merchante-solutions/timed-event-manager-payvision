package com.mes.tools;

import javax.servlet.http.HttpServletRequest;


/**
 * -------------
 * Key interface
 * -------------
 * 
 * Used to house multiple [primary] keys that uniquely identify an associated entity.
 * Usu. a db table.
 * 
 * E.g.: merchant account: both appSeqNum and merchant number reference a merchant account.
 * 
 * Keys may be hierarchical.  Keys may themselves contain keys referred to as sub-keys.
 * Keys may contain items.  Items are the data members constituting key data.
 * 
 * 
 * NOTE: Maintain and employ com.mes.tools.KeyBuilder for construction of Keys.
 */
public interface Key
{
  public boolean                    isSet();  
    // meaning key has items and these items have data.
  public boolean                    exists() throws NonExistantKeyException;
    // meaning key exists at persistence level (usu. db)
  public boolean                    equals(Object obj);
  public void                       clear();
  public String                     toString();
  public String                     toQueryString();
  public boolean                    build(HttpServletRequest request);

  /**
   * Sub-key related functions.
   */
  public boolean                    hasSubKeys();
  public String[]                   getSubKeyNames();
  public Key                        getSubKey(String keyName) 
    throws KeyException;
  
  /**
   * Key item related functions.
   */
  public boolean                    hasItems();
  public String[]                   getItemNames();
  public String                     getItemStringValue(String itemName)
    throws KeyException;
  
  public class KeyException extends Exception
  {
    public KeyException()
    {
      super("Key exception.");
    }
    public KeyException(String msg)
    {
      super(msg);
    }
  }
  public class UnrecognizedKeyNameException extends KeyException
  {
    // construction
    public  UnrecognizedKeyNameException()
    {
      super("Unrecognized key name exception.");
    }
    public  UnrecognizedKeyNameException(String msg)
    { 
      super(msg);
    }
  }
  public class NonExistantKeyException extends KeyException
  {
    // construction
    public  NonExistantKeyException()
    {
      super("Non-existant key exception.");
    }
    public  NonExistantKeyException(String msg)
    { 
      super(msg);
    }
  }
  public class AmbiguousKeyException extends KeyException
  {
    // construction
    public  AmbiguousKeyException()
    {
      super("Ambiguous key exception.");
    }
    public  AmbiguousKeyException(String msg)
    { 
      super(msg);
    }
  }

}
