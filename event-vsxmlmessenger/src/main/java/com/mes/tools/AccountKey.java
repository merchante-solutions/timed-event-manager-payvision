/*@lineinfo:filename=AccountKey*//*@lineinfo:user-code*//*@lineinfo:1^1*/
package com.mes.tools;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.mes.database.SQLJConnectionBase;
import com.mes.support.HttpHelper;


/**
 * AccountKey
 * 
 * Wrapper around multiple [merchant] account primary keys.
 * Used to establish a single point of reference to identify an account.
 */
public final class AccountKey extends SQLJConnectionBase implements Key
{
	static Logger log = Logger.getLogger(AccountKey.class);

  // constants
  private static final String[] ITEM_NAMES = 
  {
     "merchNum"
    ,"appSeqNum"
    ,"mercCntrlNumber"
  };
  
  
  // data members
  private String   merchNum         = "";
  private long     appSeqNum        = 0L;
  private String   mercCntrlNumber  = "";
    
  
  // construction
  public  AccountKey()
  {
  }
  public AccountKey(String merchNum, long appSeqNum, String mercCntrlNumber)
  {
    this.merchNum         = merchNum;
    this.appSeqNum        = appSeqNum;
    this.mercCntrlNumber  = mercCntrlNumber;
  }
  public  AccountKey(String s, boolean isMerchNumOrCntrlNum)
  {
    if(isMerchNumOrCntrlNum)
      this.merchNum   = s;
    else
      this.mercCntrlNumber   = s;
  }
  public  AccountKey(long appSeqNum)
  {
    this.appSeqNum  = appSeqNum;
  }

  public boolean isSet()
  {
    return (merchNum.length()>0 || appSeqNum>0L || mercCntrlNumber.length()>0);
  }

  public boolean exists() throws Key.NonExistantKeyException
  {
    if(!isSet())
      throw new NonExistantKeyException();

    int cnt = 0;

    try {
      connect();
      
      if(appSeqNum > 0L) {
        /*@lineinfo:generated-code*//*@lineinfo:73^9*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//            
//            from   merchant m
//            where  m.app_seq_num = :appSeqNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n           \n          from   merchant m\n          where  m.app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"0com.mes.tools.AccountKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:79^9*/
      
      } else if(merchNum.length()>0) {
        /*@lineinfo:generated-code*//*@lineinfo:82^9*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//            
//            from   merchant m
//            where  m.merch_number = :merchNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n           \n          from   merchant m\n          where  m.merch_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.tools.AccountKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:88^9*/

        if(cnt==0) {
          /*@lineinfo:generated-code*//*@lineinfo:91^11*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//              
//              from   mif m
//              where  m.merchant_number = :merchNum
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n             \n            from   mif m\n            where  m.merchant_number =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.tools.AccountKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:97^11*/
        }
      
      } else if(mercCntrlNumber.length()>0) {
          /*@lineinfo:generated-code*//*@lineinfo:101^11*/

//  ************************************************************
//  #sql [Ctx] { select count(*)
//              
//              from   mif m
//              where  m.MERC_CNTRL_NUMBER = :mercCntrlNumber
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select count(*)\n             \n            from   mif m\n            where  m.MERC_CNTRL_NUMBER =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.tools.AccountKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,mercCntrlNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   cnt = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:107^11*/
      }

    }
    catch(Exception e) {
      logEntry("exists()", e.toString());
    }
    finally {
      cleanUp();
    }
    
    return (cnt>0);
  }
  
  // accessors
  public String getMerchNum()
    throws KeyException
  {
    // retreive this key?
    if(merchNum.length()<1 && (appSeqNum>0L || mercCntrlNumber.length()>0)) {
      try {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:129^9*/

//  ************************************************************
//  #sql [Ctx] { select merch_number
//            
//            from   merchant
//            where  app_seq_num = :appSeqNum or merc_cntrl_number = :mercCntrlNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select merch_number\n           \n          from   merchant\n          where  app_seq_num =  :1  or merc_cntrl_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.tools.AccountKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,mercCntrlNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchNum = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:135^9*/
      }
      catch(SQLException sqle) {
        if(sqle.getMessage().indexOf("multiple rows found for select into statement")>0)
          throw new Key.AmbiguousKeyException("Multiple records found for AccountKey: "+toString());
        else
          throw new Key.NonExistantKeyException("Merchant Number non-existant for AccountKey: "+toString());
      }
      catch(Exception e) {
      }
      finally {
        cleanUp();
      }
    }
      
    return merchNum;
  }
  public long getAppSeqNum()
    throws KeyException
  {
    // retreive this key?
    if(appSeqNum<=0L && (merchNum.length()>0 || mercCntrlNumber.length()>0)) {
      try {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:159^9*/

//  ************************************************************
//  #sql [Ctx] { select app_seq_num
//            
//            from   merchant
//            where  merch_number = :merchNum or merc_cntrl_number = :mercCntrlNumber
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select app_seq_num\n           \n          from   merchant\n          where  merch_number =  :1  or merc_cntrl_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.tools.AccountKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setString(1,merchNum);
   __sJT_st.setString(2,mercCntrlNumber);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appSeqNum = __sJT_rs.getLong(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:165^9*/
      }
      catch(SQLException sqle) {
        if(sqle.getMessage().indexOf("multiple rows found for select into statement")>0)
          throw new Key.AmbiguousKeyException("Multiple records found for AccountKey: "+toString());
        else
          throw new Key.NonExistantKeyException("App Seq Num non-existant for AccountKey: "+toString());
      }
      catch(Exception e) {
      }
      finally {
        cleanUp();
      }
    }
      
    return appSeqNum;
  }
  public String getMercCntrlNumber()
    throws KeyException
  {
    // retreive this key?
    if(mercCntrlNumber.length()<1 && (appSeqNum>0L || merchNum.length()>0)) {
      try {
        connect();
        /*@lineinfo:generated-code*//*@lineinfo:189^9*/

//  ************************************************************
//  #sql [Ctx] { select merc_cntrl_number
//            
//            from   merchant
//            where  app_seq_num = :appSeqNum or merch_number = :merchNum
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select merc_cntrl_number\n           \n          from   merchant\n          where  app_seq_num =  :1  or merch_number =  :2";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"6com.mes.tools.AccountKey",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,merchNum);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   mercCntrlNumber = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:195^9*/
      }
      catch(SQLException sqle) {
        if(sqle.getMessage().indexOf("multiple rows found for select into statement")>0)
          throw new Key.AmbiguousKeyException("Multiple records found for AccountKey: "+toString());
        else
          throw new Key.NonExistantKeyException("Merchant Control Number non-existant for AccountKey: "+toString());
      }
      catch(Exception e) {
      }
      finally {
        cleanUp();
      }
    }
      
    return mercCntrlNumber;
  }
  
  // mutators
  public void setMerchNum(String v)
  {
    if(v==null || this.merchNum.equals(v))
      return;
    merchNum = v;
    appSeqNum = 0L;
    mercCntrlNumber = "";
  }
  public void setAppSeqNum(long v)
  {
    if(this.appSeqNum == v)
      return;
    merchNum = "";
    appSeqNum = v;
    mercCntrlNumber = "";
  }
  public void setMercCntrlNumber(String v)
  {
    if(v==null || this.mercCntrlNumber.equals(v))
      return;
    mercCntrlNumber = v;
    merchNum = "";
    appSeqNum = 0L;
  }
    
  public boolean equals(Object o)
  {
    if(!(o instanceof AccountKey))
      return false;
      
    AccountKey ac = (AccountKey)o;
      
    if(ac.merchNum != null && this.merchNum.equals(ac.merchNum))
      return true;
    if(ac.appSeqNum > 0L && this.appSeqNum == ac.appSeqNum)
      return true;
    if(ac.mercCntrlNumber != null && this.mercCntrlNumber.equals(ac.mercCntrlNumber))
      return true;
      
    return false;
  }
    
  public void clear()
  {
    this.merchNum         = "";
    this.appSeqNum        = 0L;
    this.mercCntrlNumber  = "";
  }

  public String toString()
  {
    if(!isSet())
      return "No account key loaded";
    
    StringBuffer sb = new StringBuffer();

    if(merchNum.length()>0) {
      sb.append(", Merchant Number: ");
      sb.append(merchNum);
    }

    if(appSeqNum>0L) {
      sb.append(", App Seq Num: ");
      sb.append(appSeqNum);
    }

    if(mercCntrlNumber.length()>0) {
      sb.append(", Merchant Control Number: ");
      sb.append(mercCntrlNumber);
    }

    return sb.toString().substring(2);
  }

  public String toQueryString()
	{
		StringBuffer sb = new StringBuffer();

		if (merchNum.length() > 0) {
			sb.append('&');
			sb.append(ITEM_NAMES[0]);
			sb.append('=');
			try {
				sb.append(URLEncoder.encode(merchNum, StandardCharsets.UTF_8.name()));
			} catch (UnsupportedEncodingException e) {
				log.error("URLEncoder.encode(merchNum UnsupportedEncodingException", e);
				sb.append(merchNum);
			}
		}

		if (appSeqNum > 0L) {
			sb.append('&');
			sb.append(ITEM_NAMES[1]);
			sb.append('=');
			sb.append(appSeqNum);
		}

		if (mercCntrlNumber.length() > 0) {
			sb.append('&');
			sb.append(ITEM_NAMES[2]);
			sb.append('=');
			try {
				sb.append(URLEncoder.encode(mercCntrlNumber, StandardCharsets.UTF_8.name()));
			} catch (UnsupportedEncodingException e) {
				log.error("URLEncoder.encode(mercCntrlNumber UnsupportedEncodingException", e);
				sb.append(mercCntrlNumber);
			}
		}

		return sb.length() > 0 ? sb.toString().substring(1) : "";
	}
  
  public boolean build(HttpServletRequest request)
  {
    this.merchNum         = HttpHelper.getString(request,ITEM_NAMES[0],"");
    this.appSeqNum        = HttpHelper.getLong(request,ITEM_NAMES[1],0L);
    this.mercCntrlNumber  = HttpHelper.getString(request,ITEM_NAMES[2],"");
    return isSet();
  }

  public boolean                    hasSubKeys()
  {
    return false;
  }
  public String[]                   getSubKeyNames()
  {
    return null;
  }
  public Key                        getSubKey(String keyName) 
    throws KeyException
  {
    throw new Key.NonExistantKeyException("AccountKey has no sub-keys.");
  }

  public boolean                    hasItems()
  {
    return true;
  }
  public String[]                   getItemNames()
  {
    return ITEM_NAMES;
  }
  public String                     getItemStringValue(String itemName)
    throws KeyException
  {
    if(itemName==null)
      return null;

    if(itemName.equals(ITEM_NAMES[0]))
      return getMerchNum();

    if(itemName.equals(ITEM_NAMES[1]))
      return Long.toString(getAppSeqNum());

    if(itemName.equals(ITEM_NAMES[2]))
      return getMercCntrlNumber();

    throw new KeyException("Unrecognized key item name: '"+itemName+"'.");
  }

}/*@lineinfo:generated-code*/