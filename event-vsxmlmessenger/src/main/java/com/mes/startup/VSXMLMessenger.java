/*@lineinfo:filename=VSXMLMessenger*//*@lineinfo:user-code*//*@lineinfo:1^1*//*************************************************************************

  FILE: $Archive: /Java/beans/com/mes/startup/VSXMLMessenger.sqlj $

  Description:  
  
    Send queued XML messages to verisign.


  Last Modified By   : $Author: vbannikov $
  Last Modified Date : $Date: 2015-06-22 09:43:59 -0700 (Mon, 22 Jun 2015) $
  Version            : $Revision: 23700 $

  Change History:
     See VSS database

  Copyright (C) 2000,2001 by Merchant e-Solutions Inc.
  All rights reserved, Unauthorized distribution prohibited.

  This document contains information which is the proprietary
  property of Merchant e-Solutions, Inc.  This document is received in
  confidence and its contents may not be disclosed without the
  prior written consent of Merchant e-Solutions, Inc.

**************************************************************************/

package com.mes.startup;

import java.io.PrintWriter;
import java.net.Socket;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.log4j.Logger;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import com.mes.config.MesDefaults;
import com.mes.constants.MesEmails;
import com.mes.constants.mesConstants;
import com.mes.net.MailMessage;
import com.mes.net.VSAddCardTypeRequest;
import com.mes.net.VSRemoveCardTypeRequest;
import com.mes.net.VSRequest;
import com.mes.net.VSResponse;
import com.mes.net.VSStatusUpdateRequest;
import com.mes.net.VSUpdateVendorRequest;
import com.mes.net.VsXMLRegAddCard;
import com.mes.net.VsXMLRegProcessorInfo;
import com.mes.net.VsXMLRegRemoveCard;
import com.mes.net.VsXMLRegRequest;
import com.mes.net.VsXMLRegResponse;
import com.mes.net.VsXMLRegSender;
import com.mes.net.VsXMLRegStatusUpdate;
import com.mes.net.XMLMessage;
import sqlj.runtime.ResultSetIterator;

public class VSXMLMessenger extends EventBase
{
  static Logger log = Logger.getLogger(VSXMLMessenger.class);

  private static boolean working = false;
  
  /*
  ** QueueIterator
  */
  /*@lineinfo:generated-code*//*@lineinfo:62^3*/

//  ************************************************************
//  SQLJ iterator declaration:
//  ************************************************************

public static class QueueIterator
extends sqlj.runtime.ref.ResultSetIterImpl
implements sqlj.runtime.NamedIterator
{
  public QueueIterator(sqlj.runtime.profile.RTResultSet resultSet)
    throws java.sql.SQLException
  {
    super(resultSet);
    queueSeqNumNdx = findColumn("queueSeqNum");
    appSeqNumNdx = findColumn("appSeqNum");
    msgTypeNdx = findColumn("msgType");
    cardTypeNdx = findColumn("cardType");
    xmlSuccessNdx = findColumn("xmlSuccess");
    xmlFailuresNdx = findColumn("xmlFailures");
    sideSuccessNdx = findColumn("sideSuccess");
    sideFailuresNdx = findColumn("sideFailures");
    formatTypeNdx = findColumn("formatType");
    testAppNdx = findColumn("testApp");
    m_rs = (oracle.jdbc.OracleResultSet) resultSet.getJDBCResultSet();
  }
  private oracle.jdbc.OracleResultSet m_rs;
  public long queueSeqNum()
    throws java.sql.SQLException
  {
    long __sJtmp = m_rs.getLong(queueSeqNumNdx);
    if (m_rs.wasNull()) throw new sqlj.runtime.SQLNullException(); else return __sJtmp;
  }
  private int queueSeqNumNdx;
  public long appSeqNum()
    throws java.sql.SQLException
  {
    long __sJtmp = m_rs.getLong(appSeqNumNdx);
    if (m_rs.wasNull()) throw new sqlj.runtime.SQLNullException(); else return __sJtmp;
  }
  private int appSeqNumNdx;
  public int msgType()
    throws java.sql.SQLException
  {
    int __sJtmp = m_rs.getInt(msgTypeNdx);
    if (m_rs.wasNull()) throw new sqlj.runtime.SQLNullException(); else return __sJtmp;
  }
  private int msgTypeNdx;
  public int cardType()
    throws java.sql.SQLException
  {
    int __sJtmp = m_rs.getInt(cardTypeNdx);
    if (m_rs.wasNull()) throw new sqlj.runtime.SQLNullException(); else return __sJtmp;
  }
  private int cardTypeNdx;
  public String xmlSuccess()
    throws java.sql.SQLException
  {
    return (String)m_rs.getString(xmlSuccessNdx);
  }
  private int xmlSuccessNdx;
  public int xmlFailures()
    throws java.sql.SQLException
  {
    int __sJtmp = m_rs.getInt(xmlFailuresNdx);
    if (m_rs.wasNull()) throw new sqlj.runtime.SQLNullException(); else return __sJtmp;
  }
  private int xmlFailuresNdx;
  public String sideSuccess()
    throws java.sql.SQLException
  {
    return (String)m_rs.getString(sideSuccessNdx);
  }
  private int sideSuccessNdx;
  public int sideFailures()
    throws java.sql.SQLException
  {
    int __sJtmp = m_rs.getInt(sideFailuresNdx);
    if (m_rs.wasNull()) throw new sqlj.runtime.SQLNullException(); else return __sJtmp;
  }
  private int sideFailuresNdx;
  public int formatType()
    throws java.sql.SQLException
  {
    int __sJtmp = m_rs.getInt(formatTypeNdx);
    if (m_rs.wasNull()) throw new sqlj.runtime.SQLNullException(); else return __sJtmp;
  }
  private int formatTypeNdx;
  public String testApp()
    throws java.sql.SQLException
  {
    return (String)m_rs.getString(testAppNdx);
  }
  private int testAppNdx;
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:71^62*/

  private static String[] messageTypes = 
  {
    "invalid type (0)",
    "APP_STATUS_INCOMPLETE (1)",
    "APP_STATUS_COMPLETE (2)",
    "APP_STATUS_APPROVED (3)",
    "APP_STATUS_DECLINED (4)",
    "APP_STATUS_PENDED (5)",
    "APP_STATUS_CANCELLED (6)",
    "APP_STATUS_SETUP_COMPLETE (7)",
    "APP_STATUS_ADDED_CARD (8)",
    "APP_STATUS_REMOVED_CARD (9)"
  };
  
  /*
  ** private String getMessageType(int which)
  **
  ** RETURNS: a string descriptor of a given message type.
  */
  private String getMessageType(int which)
  {
    String msgType = null;
    try
    {
      msgType = messageTypes[which];
    }
    catch (Exception e)
    {
      msgType = messageTypes[0];
    }
    
    return msgType;
  }
  
  /*
  ** Logging
  */
  private void xmlLog(long appSeqNum, String jobName, String msgType, String details)
  {
    try
    {
      /*@lineinfo:generated-code*//*@lineinfo:114^7*/

//  ************************************************************
//  #sql [Ctx] { insert into xml_log
//            ( app_seq_num,
//              log_time,
//              job_name,
//              msg_type,
//              details )
//          values
//            ( :appSeqNum,
//              sysdate,
//              :jobName,
//              :msgType,
//              :details )
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
   String theSqlTS = "insert into xml_log\n          ( app_seq_num,\n            log_time,\n            job_name,\n            msg_type,\n            details )\n        values\n          (  :1 ,\n            sysdate,\n             :2 ,\n             :3 ,\n             :4  )";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"0com.mes.startup.VSXMLMessenger",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,appSeqNum);
   __sJT_st.setString(2,jobName);
   __sJT_st.setString(3,msgType);
   __sJT_st.setString(4,details);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:128^7*/
    }
    catch (Exception e)
    {
      // resort to java_log if trouble logging to xml_log
      logEntry("xmlLog",e.toString());
      log.debug("Exception in xmlLog: " + e.toString());
    }
  }
  
  private void xmlLogError(String jobName, QueueIterator it, Exception e)
  {
    try
    {
      StringBuffer details = new StringBuffer();
      details.append("ERROR");
      details.append(", qsn: " + it.queueSeqNum());
      details.append(", asn: " + it.appSeqNum());
      details.append(", ct: " + it.cardType());
      if (e != null)
      {
        details.append(", e: " + e);
      }
    
      if (details.length() > 240)
      {
        details.setLength(240);
      }
    
      xmlLog(it.appSeqNum(),jobName,getMessageType(it.msgType()),details.toString());
    }
    catch (Exception le)
    {
      logEntry("xmlLogError(1)",le.toString());
    }
  }
  private void xmlLogError(String         jobName,
                           QueueIterator  it,
                           XMLMessage     request,
                           XMLMessage     response,
                           long           duration,
                           Exception      e)
  {
    try
    {
      StringBuffer details = new StringBuffer();
      details.append("ERROR");
      details.append(", qsn: " + it.queueSeqNum());
      details.append(", asn: " + it.appSeqNum());
      details.append(", ct: " + it.cardType());
      if (request != null)
      {
        details.append(", req: " + request.getMessageType());
      }
      if (response != null)
      {
        details.append(", resp: " + response.getMessageType());
      }
      details.append(", dur: " + duration + "ms");
      if (e != null)
      {
        details.append(", e: " + e);
      }
    
      if (details.length() > 240)
      {
        details.setLength(240);
      }
    
      xmlLog(it.appSeqNum(),jobName,getMessageType(it.msgType()),details.toString());
    }
    catch (Exception le)
    {
      logEntry("xmlLogError(2)",le.toString());
    }
  }
  private void xmlLogError(String jobName, Exception e)
  {
    try
    {
      StringBuffer details = new StringBuffer();
      details.append("ERROR");
      if (e != null)
      {
        details.append(", e: " + e);
      }
    
      if (details.length() > 240)
      {
        details.setLength(240);
      }

      xmlLog(0,jobName,getMessageType(0),details.toString());
    }
    catch (Exception le)
    {
      logEntry("xmlLogError(3)",le.toString());
    }
  }
  private void xmlLogSuccess(String         jobName,
                             QueueIterator  it,
                             XMLMessage     request,
                             XMLMessage     response,
                             long           duration)
  {
    try
    {
      StringBuffer details = new StringBuffer();
      details.append("SUCCESS");
      details.append(", qsn: " + it.queueSeqNum());
      details.append(", asn: " + it.appSeqNum());
      details.append(", ct: " + it.cardType());
      if (request != null)
      {
        details.append(", req: " + request.getMessageType());
      }
      if (response != null)
      {
        details.append(", resp: " + response.getMessageType());
      }
      details.append(", dur: " + duration + "ms");
    
      if (details.length() > 240)
      {
        details.setLength(240);
      }
    
      xmlLog(it.appSeqNum(),jobName,getMessageType(it.msgType()),details.toString());
    }
    catch (Exception e)
    {
      logEntry("xmlLogSuccess",e.toString());
    }
  }
      
  /*
  ** private boolean doSideEffects(QueueIterator it)
  **
  ** Handles any special tasks that need to be done after an xml message has
  ** successfully been sent.
  **
  ** RETURNS: true if no side effects or side effects successfully done,
  **          else false.
  */
  private boolean doSideEffects(QueueIterator qit)
  {
    boolean sideOk = false;
    try
    {
      switch (qit.msgType())
      {
        case mesConstants.APP_STATUS_INCOMPLETE:
        case mesConstants.APP_STATUS_APPROVED:
        case mesConstants.APP_STATUS_PENDED:
        case mesConstants.APP_STATUS_DECLINED:
        case mesConstants.APP_STATUS_CANCELLED:
        case mesConstants.APP_STATUS_ADDED_CARD:
        case mesConstants.APP_STATUS_REMOVED_CARD:
          // no side effects
          sideOk = true;
          break;
          
        case mesConstants.APP_STATUS_COMPLETE:
          // send email to notify that this virtual app is complete
          String merchName;
          /*@lineinfo:generated-code*//*@lineinfo:293^11*/

//  ************************************************************
//  #sql [Ctx] { select  merch_business_name
//              
//              from    merchant
//              where   app_seq_num = :qit.appSeqNum()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4889 = qit.appSeqNum();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_business_name\n             \n            from    merchant\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"1com.mes.startup.VSXMLMessenger",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4889);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:299^11*/
          
          String details = 
            "Virtual App Complete: " + merchName + " (" + qit.appSeqNum() + ")";
                           
          MailMessage msg = new MailMessage();
          msg.setAddresses(MesEmails.MSG_ADDRS_VS_VIRTUAL_APP_COMPLETE);
          msg.setSubject(details);
          msg.setText(details);
          msg.send();
          
          sideOk = true;
          break;
          
        case mesConstants.APP_STATUS_SETUP_COMPLETE:
          // send setup complete notification to merchant
          String merchEmail;
          String merchNumber;
          /*@lineinfo:generated-code*//*@lineinfo:317^11*/

//  ************************************************************
//  #sql [Ctx] { select  merch_email_address,
//                      merch_number
//              
//              from    merchant
//              where   app_seq_num = :qit.appSeqNum()
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4890 = qit.appSeqNum();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_email_address,\n                    merch_number\n             \n            from    merchant\n            where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"2com.mes.startup.VSXMLMessenger",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4890);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 2) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(2,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchEmail = (String)__sJT_rs.getString(1);
   merchNumber = (String)__sJT_rs.getString(2);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:325^11*/
          
          int acceptedCount = 0;
          /*@lineinfo:generated-code*//*@lineinfo:328^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(cardtype_code)
//              
//              from    merchpayoption
//              where   app_seq_num = :qit.appSeqNum() and
//                      merchpo_rate is null
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4891 = qit.appSeqNum();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cardtype_code)\n             \n            from    merchpayoption\n            where   app_seq_num =  :1  and\n                    merchpo_rate is null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"3com.mes.startup.VSXMLMessenger",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4891);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   acceptedCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:335^11*/
          
          int appliedCount = 0;
          /*@lineinfo:generated-code*//*@lineinfo:338^11*/

//  ************************************************************
//  #sql [Ctx] { select  count(cardtype_code)
//              
//              from    merchpayoption
//              where   app_seq_num = :qit.appSeqNum() and
//                      merchpo_rate is not null
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4892 = qit.appSeqNum();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  count(cardtype_code)\n             \n            from    merchpayoption\n            where   app_seq_num =  :1  and\n                    merchpo_rate is not null";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"4com.mes.startup.VSXMLMessenger",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4892);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   appliedCount = __sJT_rs.getInt(1); if (__sJT_rs.wasNull()) throw new sqlj.runtime.SQLNullException();
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:345^11*/
          
          StringBuffer acceptedCards = new StringBuffer();
          StringBuffer appliedCards = new StringBuffer();
          ResultSetIterator it = null;
          /*@lineinfo:generated-code*//*@lineinfo:350^11*/

//  ************************************************************
//  #sql [Ctx] it = { select  cardtype_code,
//                      merchpo_rate
//              from    merchpayoption
//              where   app_seq_num = :qit.appSeqNum()
//              order by cardtype_code
//             };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4893 = qit.appSeqNum();
  try {
   String theSqlTS = "select  cardtype_code,\n                    merchpo_rate\n            from    merchpayoption\n            where   app_seq_num =  :1 \n            order by cardtype_code";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"5com.mes.startup.VSXMLMessenger",theSqlTS);
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4893);
   // execute query
   it = new sqlj.runtime.ref.ResultSetIterImpl(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"5com.mes.startup.VSXMLMessenger",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:357^11*/
          ResultSet rs = it.getResultSet();
          while (rs.next())
          {
            String cardName = "";
            boolean newCard = (rs.getString("merchpo_rate") != null);
            switch (rs.getInt("cardtype_code"))
            {
              case 1:
                cardName = "Visa";
                break;
                
              case 4:
                cardName = "MasterCard";
                break;
                
              case 10:
                cardName = "Diners Club";
                break;
                
              case 14:
                cardName = "Discover";
                break;
                
              case 15:
                cardName = "JCB";
                break;
            
              case 16:
                cardName = "American Express";
                break;
            }
            
            if (newCard)
            {
              appliedCards.append(cardName);
              --appliedCount;
              if (appliedCount > 1)
              {
                appliedCards.append(", ");
              }
              else if (appliedCount == 1)
              {
                appliedCards.append(" and ");
              }
            }
            else
            {
              acceptedCards.append(cardName);
              --acceptedCount;
              if (acceptedCount > 1)
              {
                acceptedCards.append(", ");
              }
              else if (acceptedCount == 1)
              {
                acceptedCards.append(" and ");
              }
            }
          }
          
          MailMessage m = new MailMessage();
          m.setAddresses(MesEmails.MSG_ADDRS_VS_AUTO_VNUM);
          m.addTo(merchEmail);
          m.setSubject("Your Merchant Account Setup - " + merchNumber);
          StringBuffer bodyText = new StringBuffer();
          bodyText.append("Merchant e-Solutions, through our partnership with ");
          bodyText.append("VeriSign, is pleased to inform you that an account ");
          bodyText.append("has been established for your business to process ");
          bodyText.append("credit cards through us. We are grateful for the ");
          bodyText.append("opportunity to assist you and look forward to ");
          bodyText.append("providing for your bankcard processing service now ");
          bodyText.append("and in the years to come.\n\n");
          bodyText.append("Currently ");
          bodyText.append("you are approved for " + acceptedCards);
          if (appliedCards.length() > 0)
          {
            bodyText.append(" and your application for " + appliedCards + " ");
            bodyText.append("is pending");
          }
          bodyText.append(".\n\n");
          bodyText.append("Within a couple of hours your account with ");
          bodyText.append("VeriSign will be activated and you will be able ");
          bodyText.append("to process both live and test transactions for the ");
          bodyText.append("card types you have been approved for.  To access ");
          bodyText.append("your VeriSign account, please go to ");
          bodyText.append("https://manager.verisign.com and login.  If your ");
          bodyText.append("account has not been activated within 24 hours or ");
          bodyText.append("your are unable to login, please call ");
          bodyText.append("(888) 883-9770  or email ");
          bodyText.append("vps-support@verisign.com.\n\n");
          bodyText.append("If you have any questions  concerning  your credit ");
          bodyText.append("card sales, web reporting or merchant statements ");
          bodyText.append("please call (toll-free) 1-888-288-2692 or e-mail ");
          bodyText.append("us at help@merchante-solutions.com.\n\n");
          bodyText.append("We look forward to providing you with superior, ");
          bodyText.append("convenient, on-line services.\n\n\n");
          bodyText.append("Sincerely,\n\n");
          bodyText.append("Merchant e-Solutions\n\n");
          bodyText.append("Merchant e-Solutions working in partnership with VeriSign");
          m.setText(bodyText.toString());
          m.send();
          
          sideOk = true;
          break;
      }
    }
    catch (Exception e)
    {
      xmlLogError("doSideEffects",qit,e);
    }
    return sideOk;
  }

  private static final int   VS_STATUS_APP_DONE        = 1;
  private static final int   VS_STATUS_APP_TERMINATED  = 2;
  private static final int   VS_STATUS_APP_CANCELLED   = 3;
  private static final int   VS_STATUS_APP_DECLINED    = 4;
  
  /*
  ** private VSRequest buildVSRequest(QueueIterator it)
  **
  ** Builds a VSRequest message based on the current record in the iterator.
  **
  ** RETURNS: the request message.
  */
  private VSRequest buildVSRequest(QueueIterator it)
  {
    VSRequest reqMsg = null;
    try
    {
      switch(it.msgType())
      {
        case mesConstants.APP_STATUS_INCOMPLETE:
        case mesConstants.APP_STATUS_APPROVED:
        case mesConstants.APP_STATUS_PENDED:
          // no message needed
          break;
          
        case mesConstants.APP_STATUS_COMPLETE:
          reqMsg = 
            new VSStatusUpdateRequest(it.appSeqNum(),VS_STATUS_APP_DONE);
          break;
          
        case mesConstants.APP_STATUS_DECLINED:
          reqMsg = 
            new VSStatusUpdateRequest(it.appSeqNum(),VS_STATUS_APP_DECLINED);
          break;
          
        case mesConstants.APP_STATUS_CANCELLED:
          reqMsg = 
            new VSStatusUpdateRequest(it.appSeqNum(),VS_STATUS_APP_TERMINATED);
          break;
          
        case mesConstants.APP_STATUS_SETUP_COMPLETE:
          reqMsg = new VSUpdateVendorRequest(it.appSeqNum());
          break;
          
        case mesConstants.APP_STATUS_ADDED_CARD:
          reqMsg = new VSAddCardTypeRequest(it.appSeqNum(),it.cardType());
          break;
          
        case mesConstants.APP_STATUS_REMOVED_CARD:
          reqMsg = new VSRemoveCardTypeRequest(it.appSeqNum(),it.cardType());
          break;
      }
      
      if (reqMsg != null)
      {
        reqMsg.setTargetUrl(
          MesDefaults.getString(MesDefaults.DK_VS_XML_LISTENER_URL));
      }
    }
    catch(Exception e)
    {
      xmlLogError("buildVSRequest",it,e);
    }
    
    return reqMsg;
  }
  
  /*
  ** private int sendMessage(QueueIterator it)
  **
  ** Opens a connection with the redirector and sends a request.  The
  ** response returned is used to create a VSResponse message.  The results
  ** of the send are logged.
  **
  ** RETURNS: the response code if one is obtained, else -1 to 
  **          indicate failure.
  */
  private int sendMessage(QueueIterator it)
  {
    VSRequest   request   = null;
    VSResponse  response  = null;
    long        startTime = 0L;
    long        endTime   = 0L;
    
    try
    {
      // create the request
      request = buildVSRequest(it);
    
      // send it, get a response
      if (request != null)
      {
        // connect to redirector
        Socket rSocket = null;
        PrintWriter rpw = null;
        try
        {
          String host = request.getRedirectorHost();
          int port = request.getRedirectorPort();
          rSocket = new Socket(host,port);
          rpw = new PrintWriter(rSocket.getOutputStream(),true);
        }
        catch (Exception e)
        {
          throw new Exception("Failed to connect with Redirector (" + e + ")");
        }
    
        // send the request
        try
        {
          // send the target url
          rpw.println(request.getTargetUrl());
          rpw.println(request.getContentType());
    
          // send the xml message data
          XMLOutputter fmt = new XMLOutputter();
          fmt.output(request.getDocument(),rpw);
    
          // send end_post_data
          rpw.println("end_post_data");
        }
        catch (Exception e)
        {
          throw new Exception("Failed to send request (" + e + ")");
        }
        
        // mark starting time
        startTime = Calendar.getInstance().getTime().getTime();
    
        // receive response
        try
        {
          SAXBuilder builder = new SAXBuilder();
          response = new VSResponse(builder.build(rSocket.getInputStream()));
        }
        catch (Exception e)
        {
          throw new Exception("Failed to receive response (" + e + ")");
        }
        
        // mark end time
        endTime = Calendar.getInstance().getTime().getTime();
      }
      else
      {
        throw new Exception("Failed to create VSRequest.");
      }
      
      if (response.getResult() != 1)
      {
        throw new Exception("Response not OK - " + response.getDetails());
      }
      
      xmlLogSuccess("sendMessage",it,request,response,endTime - startTime);
    }
    catch (Exception e)
    {
      endTime = Calendar.getInstance().getTime().getTime();
      xmlLogError("sendMessage",it,request,response,endTime - startTime,e);
    }
    
    if (response != null)
    {
      return response.getResult();
    }
    
    return -1;
  }
  
  /*
  ** private VsXMLRegRequest buildXMLRegRequest(QueueIterator it)
  **
  ** Builds a VsXMLRegRequest message based on the current record in the 
  ** iterator.
  **
  ** RETURNS: the VsXMLRegRequest message object created.
  */
  private VsXMLRegRequest buildXMLRegRequest(QueueIterator it)
  {
    VsXMLRegRequest reqMsg = null;
    try
    {
      switch(it.msgType())
      {
        case mesConstants.APP_STATUS_INCOMPLETE:
        case mesConstants.APP_STATUS_APPROVED:
        case mesConstants.APP_STATUS_PENDED:
          // no message needed
          break;
          
        case mesConstants.APP_STATUS_COMPLETE:
          reqMsg = 
            new VsXMLRegStatusUpdate(it.appSeqNum(),"Complete");
          break;
          
        case mesConstants.APP_STATUS_DECLINED:
          reqMsg = 
            new VsXMLRegStatusUpdate(it.appSeqNum(),"Terminated");
          break;
          
        case mesConstants.APP_STATUS_CANCELLED:
          reqMsg = 
            new VsXMLRegStatusUpdate(it.appSeqNum(),"Canceled");
          break;
          
        case mesConstants.APP_STATUS_SETUP_COMPLETE:
          reqMsg = new VsXMLRegProcessorInfo(it.appSeqNum());
          break;
          
        case mesConstants.APP_STATUS_ADDED_CARD:
          reqMsg = new VsXMLRegAddCard(it.appSeqNum(),it.cardType());
          break;
          
        case mesConstants.APP_STATUS_REMOVED_CARD:
          reqMsg = new VsXMLRegRemoveCard(it.appSeqNum(),it.cardType());
          break;
      }
      
      if (reqMsg != null)
      {
        String url = "";
        
        if(it.testApp().equals("Y"))
        {
          url = MesDefaults.getString(MesDefaults.DK_VPS_ISO_XML_REG_TEST_URL);
        }
        else
        {
          url = MesDefaults.getString(MesDefaults.DK_VS_XML_REG_LISTENER_URL);
        }
        
        log.debug("setting url to: " + url);
        reqMsg.setTargetUrl(url);
      }
    }
    catch(Exception e)
    {
      xmlLogError("buildVSRequest",it,e);
    }
    
    return reqMsg;
  }
  
  /*
  ** private boolean sendXMLRegMessage(QueueIterator it)
  **
  ** Creates an XML Reg format message and sends it.
  **
  ** RETURNS: true if success response received, else false.
  */
  private boolean sendXMLRegMessage(QueueIterator it)
  {
    VsXMLRegRequest   request   = null;
    VsXMLRegResponse  response  = null;
    long              startTime = 0L;
    long              endTime   = 0L;
    
    try
    {
      // create the request
      request = buildXMLRegRequest(it);
    
      // send it, get a response
      if (request != null)
      {
        try
        {
          // mark starting time
          startTime = Calendar.getInstance().getTime().getTime();
        
          // send the message, capture the response
          response = new VsXMLRegResponse(
                      VsXMLRegSender.sendMessage(request,"sendXMLRegMessage"));

          // mark end time
          endTime = Calendar.getInstance().getTime().getTime();
        }
        catch (Exception e)
        {
          throw new Exception("Failed during send of XMLRegRequest (" 
            + e + ")");
        }
      }
      else
      {
        throw new Exception("Failed to create XMLRegRequest.");
      }
      
      if (!response.isSuccessful())
      {
        throw new Exception("Response not OK - (" + response.getResultCode() 
          + ") " + response.getMessageText());
      }
      
      xmlLogSuccess("sendXMLRegMessage",it,request,response,
        endTime - startTime);
    }
    catch (Exception e)
    {
      endTime = Calendar.getInstance().getTime().getTime();
      xmlLogError("sendXMLRegMessage",it,request,response,
        endTime - startTime,e);
    }
    
    if (response != null)
    {
      return response.isSuccessful();
    }
    
    return false;
  }
  
  /*
  ** 
  ** private boolean updateQueueRecord(QueueIterator it, 
  **                                   boolean xmlOk, 
  **                                   boolean sideOk)
  **
  ** Updates the queue record specified in it with results of latest attempt
  ** to process the record.
  **
  ** RETURNS: true if successful, else false.
  */
  private boolean updateQueueRecord(QueueIterator it, 
                                    boolean xmlOk, 
                                    boolean sideOk)
  {
    boolean updateOk = false;
    try
    {
      // analyze results of queue record processing
      int xmlFailures = it.xmlFailures();
      xmlFailures += (xmlOk ? 0 : 1);
      int sideFailures = it.sideFailures();
      sideFailures += (sideOk || !xmlOk ? 0 : 1);
      
      boolean notifyHold = false;
      
      String xmlSuccess = "Y";
      if (!xmlOk)
      {
        if (xmlFailures > MesDefaults.getInt(MesDefaults.DK_VS_XML_RETRY_MAX))
        {
          xmlSuccess = "H";
          notifyHold = true;
        }
        else
        {
          xmlSuccess = "N";
        }
      }
      
      String sideSuccess = "Y";
      if (!sideOk)
      {
        if (sideFailures > MesDefaults.getInt(MesDefaults.DK_VS_XML_RETRY_MAX))
        {
          sideSuccess = "H";
          notifyHold = true;
        }
        else
        {
          sideSuccess = "N";
        }
      }
    
      // update the queue record with the results
      /*@lineinfo:generated-code*//*@lineinfo:838^7*/

//  ************************************************************
//  #sql [Ctx] { update  vs_xml_queue
//        
//          set     xml_success     = :xmlSuccess,
//                  xml_failures    = :xmlFailures,
//                  side_success    = :sideSuccess,
//                  side_failures   = :sideFailures,
//                  last_attempt    = sysdate
//                
//          where   queue_seq_num   = :it.queueSeqNum()
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4894 = it.queueSeqNum();
   String theSqlTS = "update  vs_xml_queue\n      \n        set     xml_success     =  :1 ,\n                xml_failures    =  :2 ,\n                side_success    =  :3 ,\n                side_failures   =  :4 ,\n                last_attempt    = sysdate\n              \n        where   queue_seq_num   =  :5";
   __sJT_st = __sJT_ec.prepareOracleBatchableStatement(__sJT_cc,"6com.mes.startup.VSXMLMessenger",theSqlTS);
   // set IN parameters
   __sJT_st.setString(1,xmlSuccess);
   __sJT_st.setInt(2,xmlFailures);
   __sJT_st.setString(3,sideSuccess);
   __sJT_st.setInt(4,sideFailures);
   __sJT_st.setLong(5,__sJT_4894);
  // execute statement
   __sJT_ec.oracleExecuteBatchableUpdate();
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:849^7*/
      
      // if putting this queue record on hold, notify dev via email
      if (notifyHold)
      {
        String merchName;
        /*@lineinfo:generated-code*//*@lineinfo:855^9*/

//  ************************************************************
//  #sql [Ctx] { select  merch_business_name
//            
//            from    merchant
//            where   app_seq_num = :it.appSeqNum()
//           };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
 long __sJT_4895 = it.appSeqNum();
  oracle.jdbc.OracleResultSet __sJT_rs = null;
  try {
   String theSqlTS = "select  merch_business_name\n           \n          from    merchant\n          where   app_seq_num =  :1";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"7com.mes.startup.VSXMLMessenger",theSqlTS);
   if (__sJT_ec.isNew())
   {
     __sJT_st.setFetchSize(2);
   }
   // set IN parameters
   __sJT_st.setLong(1,__sJT_4895);
   // execute query
   __sJT_rs = __sJT_ec.oracleExecuteQuery();
   if (__sJT_rs.getMetaData().getColumnCount() != 1) sqlj.runtime.error.RuntimeRefErrors.raise_WRONG_NUM_COLS(1,__sJT_rs.getMetaData().getColumnCount());
   if (!__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_NO_ROW_SELECT_INTO();
   // retrieve OUT parameters
   merchName = (String)__sJT_rs.getString(1);
   if (__sJT_rs.next()) sqlj.runtime.error.RuntimeRefErrors.raise_MULTI_ROW_SELECT_INTO();
  } finally { if (__sJT_rs!=null) __sJT_rs.close(); __sJT_ec.oracleClose(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:861^9*/
        
        String subject  = "VS XML Message on hold: " + merchName + " (asn " 
                          + it.appSeqNum() + ", qsn " + it.queueSeqNum() + ")";
        String bodyText = "An XML message in the VS XML queue "
                          + "has been placed on hold.\n"
                          + "  XML Message Status:  " + xmlSuccess
                          + ", " + xmlFailures + " failed attempts.\n"
                          + "  Side Effects Status: " + sideSuccess
                          + ", " + sideFailures + " failed attempts.\n";
                         
        MailMessage msg = new MailMessage();
        msg.setAddresses(MesEmails.MSG_ADDRS_XML_ERROR);
        msg.setSubject(subject);
        msg.setText(bodyText);
        msg.send();
      }
      
      updateOk = true;
    }
    catch (Exception e)
    {
      xmlLogError("updateQueueRecord",it,e);
    }
    return updateOk;
  }
  
  /*
  ** private boolean processRecord(QueueIterator it)
  **
  ** Constructs a message based on the current record in the iterator
  ** and tries to send it to the verisign xml listener.  The success
  ** or failure is logged and the queue record is updated to reflect
  ** the outcome.  Any post xml tasks are handled and the overall send
  ** success depends on the post xml task success as well.
  **
  ** RETURNS: true if message sent without error, else false.
  */
  private boolean processRecord(QueueIterator it)
  {
    boolean sendOk = false;
    try
    {
      // if xml message needs to be sent
      boolean xmlOk = it.xmlSuccess().equals("Y");
      if (!xmlOk)
      {
        if (it.formatType() == mesConstants.VS_XML_FMT_ORIGINAL)
        {
          xmlOk = (sendMessage(it) == 1);
        }
        else if (it.formatType() == mesConstants.VS_XML_FMT_XML_REG)
        {
          xmlOk = sendXMLRegMessage(it);
        }
      }
      
      // do any side effects that are 
      // associated with the message type
      boolean sideOk = false;
      if (xmlOk)
      {
        sideOk = doSideEffects(it);
      }
      
      // update the queue record with results
      updateQueueRecord(it,xmlOk,sideOk);
      
      // send ok depends on both xml and side effects
      sendOk = (xmlOk && sideOk);
    }
    catch (Exception e)
    {
      xmlLogError("processRecord",it,e);
    }
    return sendOk;
  }
                                
  /*
  ** private void processQueue()
  **
  ** Queries vs_xml_queue for unsent messages and attempts to send them
  ** to the verising xml listener.  Messages in the queue are sent in
  ** the order they are added.  If a message for a particular app_seq_num
  ** fails to be sent no further messages for the app_seq_num will be 
  ** attempted.
  */
  private void processQueue()
  {
    try
    {
      // get unprocessed messages
      QueueIterator it = null;
      /*@lineinfo:generated-code*//*@lineinfo:954^7*/

//  ************************************************************
//  #sql [Ctx] it = { select    vxq.queue_seq_num       queueSeqNum,
//                    vxq.app_seq_num         appSeqNum,
//                    vxq.msg_type            msgType,
//                    vxq.card_type           cardType,
//                    vxq.xml_success         xmlSuccess,
//                    vxq.xml_failures        xmlFailures,
//                    vxq.side_success        sideSuccess,
//                    vxq.side_failures       sideFailures,
//                    vxq.format_type         formatType,
//                    nvl(vtat.is_test, 'N')  testApp
//          from      vs_xml_queue vxq,
//                    vs_xml_test_app_types vtat,
//                    application app
//          where     (vxq.xml_success <> 'Y' or side_success <> 'Y') and
//                    vxq.app_seq_num = app.app_seq_num and
//                    app.app_type = vtat.app_type(+) 
//          order by  vxq.app_seq_num,
//                    vxq.queue_seq_num      
//         };
//  ************************************************************

{
  // declare temps
  oracle.jdbc.OraclePreparedStatement __sJT_st = null;
  sqlj.runtime.ref.DefaultContext __sJT_cc = Ctx; if (__sJT_cc==null) sqlj.runtime.error.RuntimeRefErrors.raise_NULL_CONN_CTX();
  sqlj.runtime.ExecutionContext.OracleContext __sJT_ec = ((__sJT_cc.getExecutionContext()==null) ? sqlj.runtime.ExecutionContext.raiseNullExecCtx() : __sJT_cc.getExecutionContext().getOracleContext());
  try {
   String theSqlTS = "select    vxq.queue_seq_num       queueSeqNum,\n                  vxq.app_seq_num         appSeqNum,\n                  vxq.msg_type            msgType,\n                  vxq.card_type           cardType,\n                  vxq.xml_success         xmlSuccess,\n                  vxq.xml_failures        xmlFailures,\n                  vxq.side_success        sideSuccess,\n                  vxq.side_failures       sideFailures,\n                  vxq.format_type         formatType,\n                  nvl(vtat.is_test, 'N')  testApp\n        from      vs_xml_queue vxq,\n                  vs_xml_test_app_types vtat,\n                  application app\n        where     (vxq.xml_success <> 'Y' or side_success <> 'Y') and\n                  vxq.app_seq_num = app.app_seq_num and\n                  app.app_type = vtat.app_type(+) \n        order by  vxq.app_seq_num,\n                  vxq.queue_seq_num";
   __sJT_st = __sJT_ec.prepareOracleStatement(__sJT_cc,"8com.mes.startup.VSXMLMessenger",theSqlTS);
   // execute query
   it = new com.mes.startup.VSXMLMessenger.QueueIterator(new sqlj.runtime.ref.OraRTResultSet(__sJT_ec.oracleExecuteQuery(),__sJT_st,"8com.mes.startup.VSXMLMessenger",null));
  } finally { __sJT_ec.oracleCloseQuery(); }
}


//  ************************************************************

/*@lineinfo:user-code*//*@lineinfo:974^7*/
      
      // send the messages
      long curAppSeqNum = 0L;
      boolean sendOk = false;
      while (it.next())
      {
        // if app seq num has been successfully sent or this is new 
        // app seq num attempt see if this record can be processed
        if (curAppSeqNum != it.appSeqNum() || sendOk)
        {
          curAppSeqNum = it.appSeqNum();
          sendOk = false;
          if (!it.xmlSuccess().equals("H") && !it.sideSuccess().equals("H"))
          {
            sendOk = processRecord(it);
          }
        }
      }

      // release the iterator      
      it.close();
    }
    catch (Exception e)
    {
      xmlLogError("processQueue",e);
    }
  }
  
  /*
  ** public boolean execute()
  **
  ** Entry point for the timed event.  Causes messages in the verisign xml
  ** queue to be sent.
  */
  public boolean execute()
  {
    // only send xml if no other instance is already running
    if (!working)
    {
      try
      {
        working = true;
        connect();
      
        // get current date and time
        Calendar cal = Calendar.getInstance();
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        DateFormat tf = new SimpleDateFormat("hh:mm:ss");
        String dateString = df.format(cal.getTime());
        String timeString = tf.format(cal.getTime());
      
        // debug
        //log.debug("VSXMLMessenger executing: " + timeString);

        // send xml messages waiting in the queue
        processQueue();
      }
      catch(Exception e)
      {
        log.debug(this.getClass().getName() + 
          "::execute(): " + e);
        logEntry("execute()",e.toString());
      }
      finally
      {
        working = false;
        cleanUp();
      }
    }

    return true;
  }


    public static void main( String[] args ) {
        try {
            if (args.length > 0 && args[0].equals("testproperties")) {
                EventBase.printKeyListStatus(new String[]{
                        MesDefaults.DK_VS_XML_LISTENER_URL,
                        MesDefaults.DK_VPS_ISO_XML_REG_TEST_URL,
                        MesDefaults.DK_VS_XML_REG_LISTENER_URL,
                        MesDefaults.DK_VS_XML_RETRY_MAX
                });
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }

    }


}/*@lineinfo:generated-code*/