package com.mes.startup;

import org.apache.log4j.Logger;
import com.mes.aus.file.job.IbfProcessorJob;

public final class AusIbfProcessorEvent extends EventBase
{
  static Logger log = Logger.getLogger(AusIbfProcessorEvent.class);

  public boolean execute()
  {
    try
    {
      String sysCodes = getEventArg(0);
      (new IbfProcessorJob(sysCodes)).run();

      return true;
    }
    catch (Exception e)
    {
      log.error("AUS inbound file processor event error: " + e);
      logEvent(this.getClass().getName(), "execute()", e.toString());
      logEntry("execute()", e.toString());
    }

    return false;
  }
}