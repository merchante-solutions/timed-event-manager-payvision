package com.mes.aus.file.job;

import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import com.mes.aus.InboundFile;
import com.mes.aus.file.AbuIbfProcessor;
import com.mes.aus.file.DauIbfRspProcessor;
import com.mes.aus.file.VauIbfRpt2Notifier;
import com.mes.aus.file.VauIbfRpt2Processor;
import com.mes.aus.file.VauIbfRspProcessor;

public class IbfProcessorJob extends Job {
	static Logger log = Logger.getLogger(IbfProcessorJob.class);
	private String sysStr;

	public IbfProcessorJob(String sysStr, boolean directFlag) {
		super(directFlag);
		this.sysStr = sysStr;
	}
	public IbfProcessorJob(String sysStr) {
		// default direct flag to true
		this(sysStr, true);
	}
	public void run() {
		try {
			// process inbound files for each specified system
			List ibIdList = db.getUnprocessedInboundFileIds(sysStr);
			
			for (Iterator i = ibIdList.iterator(); i.hasNext();) {
				long ibId = ((Long) i.next()).longValue();
				InboundFile ibf = db.getInboundFile(ibId, true);
				log.info("processing inbound file: " + ibf);
				try {
					if ("ABURSP".equals(ibf.getFileType())) {
						log.info("process ABU response file"); 
						(new AbuIbfProcessor(directFlag)).process(ibf);
					}
					else if ("VAURSP".equals(ibf.getFileType())) {
						log.info("process VAU response file");
						(new VauIbfRspProcessor(directFlag)).process(ibf);
					}
					else if ("VAURPT2".equals(ibf.getFileType())) {
						log.info("process VAU reject report");
						(new VauIbfRpt2Processor(directFlag)).process(ibf);

						// HACK: refetch inbound file due to it being released...
						ibf = db.getInboundFile(ibId, true);
						(new VauIbfRpt2Notifier(directFlag)).process(ibf);
					}
					else if ("DAURSP".equals(ibf.getFileType())) {
						log.info("process DAU response file");
						(new DauIbfRspProcessor(directFlag)).process(ibf);
					}
				}
				catch (Exception ie) {
					log.error("Error: " + ie);
					notifyError(ie, "inner run exception, " + ibf);
				}
				finally {
					// always turn on the process flag
					// to avoid repeated failed attempts
					db.setInboundFileProcessFlag(ibf, true);
				}
			}
		}
		catch (Exception e) {
			log.error("Error: " + e);
			notifyError(e, "run()");
		}
	}
}